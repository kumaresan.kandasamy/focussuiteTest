﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.IO;
using Focus.Data.Core;
using Focus.Data.Migration;
using Focus.Migrations.ServiceContracts;
using Focus.Migrations.ServiceImplementations;
using Mindscape.LightSpeed;
using StructureMap;
using StructureMap.Configuration.DSL;
using StructureMap.Pipeline;
using Framework.Core;
using Framework.Email;
using Framework.Email.Providers;
using Framework.Logging;
using Framework.Logging.Providers;
using Framework.Caching;
using Framework.Caching.Providers;
using Focus.Services.ServiceContracts;
using Focus.Services.ServiceImplementations;

#endregion

namespace Focus.Migrations
{
	public class ServiceBootStrapper
	{
		/// <summary>
		/// Initializes the Services.
		/// </summary>
		/// <param name="dumpPath">The dump path.</param>
		public static void Initialize(string dumpPath)
		{
			// Initialise Structure Map
			ObjectFactory.Initialize(x =>
			{
        x.AddRegistry(new PerRequestContextRegistry<FocusUnitOfWork>("Focus"));
        x.AddRegistry(new PerRequestContextRegistry<FocusMigrationUnitOfWork>("FocusMigration"));

				x.AddRegistry(new ServiceRegistry());
			});

			// Dump what we have added to Structure Map
			try
			{
				var whatDoIHave = ObjectFactory.WhatDoIHave();
				File.WriteAllText(dumpPath, whatDoIHave);
			}
			catch { }

			var coreService = ObjectFactory.GetInstance<MigrationCoreService>();
			var appSettings = coreService.AppSettings;

			// Initialise the Logger
			// Read and assign application wide logging severity
			Logger.Severity = appSettings.LogSeverity;

			// Send log / profile events to a database. 
			var application = appSettings.Application;
			var errorEmailTo = appSettings.OnErrorEmail;

			var logToDatabase = new LogToDatabase(application, "FocusLog");

			// Attach LogToEmail first so we get an email if the Db is down!
			// If LogToDatabase is first the event handler will bomb out if the 
			// Logging / Profiling Db can't be found so LogToEmail is first
			if (errorEmailTo.IsNotNullOrEmpty()) Logger.Instance.Attach(new LogToEmail(application, errorEmailTo));
			Logger.Instance.Attach(logToDatabase);

			// Initialise the Profiler
			if (appSettings.ProfilingEnabled)
				Profiler.Instance.Attach(logToDatabase);

			// Initialise the Cacher
			Cacher.Initialize(new CacheToHttpRuntime(appSettings.CachePrefix));

			// Initialise the Emailer
			if (appSettings.EmailsEnabled)
				Emailer.Instance.Attach(new EmailToSmtp());
			else
				Emailer.Instance.Attach(new EmailToFile());
		}

		#region Service Registry

		private class ServiceRegistry : Registry
		{
			/// <summary>
			/// A structure map registry for creating a Services.
			/// </summary>
			public ServiceRegistry()
			{
				// Repositories
				For<IFocusRepository>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest)).Use(x => new FocusRepository(ObjectFactory.GetInstance<UnitOfWorkScopeBase<FocusUnitOfWork>>().Current));
				For<IFocusMigrationRepository>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest)).Use(x => new FocusMigrationRepository(ObjectFactory.GetInstance<UnitOfWorkScopeBase<FocusMigrationUnitOfWork>>().Current));
				
				// Focus Services
				For<CoreService>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest)).Use(x => new CoreService());
				For<ICoreService>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest)).Use(x => new CoreService());
				For<IAuthenticationService>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest)).Use(x => new AuthenticationService());
				For<IAccountService>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest)).Use(x => new AccountService());
				For<IJobService>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest)).Use(x => new JobService());
				For<ISearchService>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest)).Use(x => new SearchService());
				For<ICandidateService>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest)).Use(x => new CandidateService());
				For<IEmployerService>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest)).Use(x => new EmployerService());
				For<IEmployeeService>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest)).Use(x => new EmployeeService());
				For<IReportService>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest)).Use(x => new ReportService());
				For<IStaffService>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest)).Use(x => new StaffService());
				For<IExplorerService>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest)).Use(x => new ExplorerService());
        For<IProcessorService>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest)).Use(x => new ProcessorService());

				For<IMigrationService>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest)).Use(x => new MigrationService());

			}
		}

		#endregion
	}
}
