﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Data.Core;
using Focus.Data.Migration;
using Focus.Migrations.ServiceContracts;
using Focus.Services.ServiceImplementations;

#endregion

namespace Focus.Migrations.ServiceImplementations
{
	public class MigrationCoreService : ServiceBase, IMigrationCoreService
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CoreService"/> class.
		/// </summary>
		public MigrationCoreService()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="CoreService"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		public MigrationCoreService(IFocusRepository repository, IFocusMigrationRepository migrationRepository)
			: base(repository, migrationRepository)
		{ }
	}
}
