﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Mindscape.LightSpeed;
using StructureMap;

using Framework.Core;
using Framework.Caching;
using Framework.Logging;

using Focus.Data.Core;
using Focus.Services.Views;

using Focus.Core;
using Focus.Services.Core;
using Focus.Data.Migration;
using Focus.Migrations.Messages;
using Focus.Services;

#endregion 

namespace Focus.Migrations.ServiceImplementations
{

	public class ServiceBase 
	{
		protected IFocusRepository _repository;
		protected IFocusMigrationRepository _migrationRepository;
		protected static readonly object _syncObject = new object();
		private static bool _bootStrapperInitialized;

		/// <summary>
		/// Initializes a new instance of the <see cref="ServiceBase"/> class.
		/// </summary>
		protected ServiceBase()
		{
			//_repository = ObjectFactory.GetInstance<IFocusRepository>();

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ServiceBase"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		public ServiceBase(IFocusRepository repository, IFocusMigrationRepository migrationRepository)
		{
			_migrationRepository = migrationRepository;
			_repository = repository;
		}

		/// <summary>
		/// Formats an error message.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="error">The error.</param>
		/// <param name="args">The args.</param>
		/// <returns></returns>
		public string FormatErrorMessage(ServiceRequest request, ErrorTypes error, params object[] args)
		{
			var s = "";
			if (args != null)
				foreach (var arg in args)
				{
					if (s.Length > 0) s += ", ";
					s += arg;
				}

			return "ErrorType." + error + (s.Length > 0 ? " - Arguments : " + s : "");
		}

		/// <summary>
		/// Logs an action.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="entity">The entity.</param>
		/// <param name="additionalDetails">The additional details.</param>
		protected void LogAction(ServiceRequest request, ActionTypes actionType, Entity<long> entity, string additionalDetails = null)
		{
			string entityName = null;
			long? entityId = null;

			if (entity.IsNotNull())
			{
				entityName = entity.GetType().Name;
				entityId = entity.Id;
			}

			LogAction(request, actionType, entityName, entityId, additionalDetails);
		}

		/// <summary>
		/// Logs the action.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="entityId">The entity id.</param>
		/// <param name="additionalDetails">The additional details.</param>
		protected void LogAction(ServiceRequest request, ActionTypes actionType, string entityName, long? entityId, string additionalDetails = null)
		{
			var action = _repository.ActionTypes.Where(at => at.Name == actionType.ToString()).FirstOrDefault();

			if (action == null)
			{
				action = new ActionType { Name = actionType.ToString() };
				_repository.Add(action);
			}

			EntityType entityType = null;

			if (entityName.IsNotNullOrEmpty())
			{
				entityType = _repository.EntityTypes.Where(et => et.Name == entityName).FirstOrDefault();

				if (entityType == null)
				{
					entityType = new EntityType { Name = entityName };
					_repository.Add(entityType);
				}
			}

			var actionEvent = new ActionEvent { ActionedOn = DateTime.Now, ActionType = action, EntityId = entityId, EntityType = entityType, AdditionalDetails = additionalDetails };

			_repository.Add(actionEvent);
			_repository.SaveChanges();
		}

		/// <summary>
		/// Validate 2 security levels for a request: ClientTag
		/// </summary>
		/// <param name="request">The request message.</param>
		/// <param name="response">The response message.</param>
		/// <param name="validate">The validation that needs to take place.</param>
		/// <returns></returns>
		protected bool ValidateRequest(ServiceRequest request, ServiceResponse response, Validate validate)
		{
			// Validate client tag. 
			if ((Validate.ClientTag & validate) == Validate.ClientTag)
			{
				if (!AppSettings.ValidClientTags.Contains(request.ClientTag))
				{
					response.Acknowledgement = AcknowledgementType.Failure;
					response.Message = FormatErrorMessage(request, ErrorTypes.UnknownClientTag);
					return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Initialises the boot strapper.
		/// </summary>
		protected void InitialiseBootStrapper()
		{
			if (!_bootStrapperInitialized)
				lock (_syncObject)
				{
					var path = AppDomain.CurrentDomain.BaseDirectory + "StructureMapDump.txt";
					ServiceBootStrapper.Initialize(path);

					_bootStrapperInitialized = true;
				}
		}

		#region Application Settings

		/// <summary>
		/// Gets the application settings.
		/// </summary>
		/// <returns></returns>
		internal AppSettings AppSettings
		{
			get
			{
				var configurationItems = Cacher.Get<List<ConfigurationItemView>>(Constants.CacheKeys.Configuration);

				if (configurationItems == null)
				{
					lock (_syncObject)
					{
						var keySet = (from ci in _repository.ConfigurationItems
													orderby ci.Key
													select ci).ToList();

						configurationItems = (from ci in keySet
																	select new ConfigurationItemView { Key = ci.Key, Value = ci.Value }
																 ).ToList();

						Cacher.Set(Constants.CacheKeys.Configuration, configurationItems);
					}
				}

				return new AppSettings(configurationItems);
			}
		}

		/// <summary>
		/// Refreshes the app settings.
		/// </summary>
		internal void RefreshAppSettings()
		{
			lock (_syncObject)
			{
				var keySet = (from ci in _repository.ConfigurationItems
											orderby ci.Key
											select ci).ToList();

				var configurationItems = (from ci in keySet
																	select new ConfigurationItemView { Key = ci.Key, Value = ci.Value }
														 ).ToList();

				Cacher.Set(Constants.CacheKeys.Configuration, configurationItems);
			}
		}

		#endregion


		#region Enums

		/// <summary>
		/// Validation options enum. Used in validation of messages.
		/// </summary>
		[Flags]
		protected enum Validate
		{
			License = 0x0001,
			ClientTag = 0x0002,
			SessionId = 0x0004,
			UserCredentials = 0x0008,
			Core = ClientTag | SessionId | UserCredentials,
			Explorer = License | ClientTag | SessionId,
			All = License | ClientTag | SessionId | UserCredentials
		}

		#endregion
	}
}
