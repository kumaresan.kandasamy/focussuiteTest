﻿#region Copyright © 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Data.Core;
using Focus.Data.Migration;
using Focus.Migrations.Messages;
using Focus.Services.Messages;
using Framework.Core;
using StructureMap;

using Focus.Migrations.MigrationProviders;
using Focus.Migrations.MigrationProviders.Interfaces;
using Focus.Migrations.ServiceContracts;

#endregion

namespace Focus.Migrations.ServiceImplementations
{
  public class MigrationService : MigrationCoreService, IMigrationService
  {
  	private readonly IMigrationProvider _migrationProvider;
  	private readonly int _batchsize = 0;

  	public MigrationService()
  	{
			//_focusMigrationRepository = ObjectFactory.GetInstance<IFocusMigrationRepository>();
			//_focusRepository = ObjectFactory.GetInstance<IFocusRepository>();
			//_migrationProvider = MigrationProviderFactory.LoadMigrationProvider(AppSettings.MigrationProvider);
  	}

  	public MigrationService(IFocusRepository focusRepository, IFocusMigrationRepository focusMigrationRepository)
  	{
  		_repository = focusRepository;
  		_migrationRepository = focusMigrationRepository;
			_migrationProvider = MigrationProviderFactory.LoadMigrationProvider(AppSettings.MigrationProvider);
  		_batchsize = AppSettings.MigrationBatchSize;
  	}




		public MigrationResponse Migrate(MigrationRequest migrationRequest)
		{

		  var employerProvider = _migrationProvider as IEmployer;
			// If the provider implements IEmployer process employers
      if (employerProvider != null)
			{
				var employers = employerProvider.MigrateEmployers(_batchsize);

				while (employers.Count > 0)
				{
					foreach (var employer in employers)
					{
						ProcessEmployer(employer);
					}
				
					employers = employerProvider.MigrateEmployers(_batchsize);
				}
			}

			if (_migrationProvider is IJobOrder)
			{

			}

			if (_migrationProvider is IJobSeeker)
			{

			}

			if (_migrationProvider is IUser)
			{

			}

			return new MigrationResponse(); //TODO
		}

		private bool ProcessEmployer(RegisterTalentUserRequest registerTalentUserRequest)
		{

			// check to see if the employer is in the Migration Database
			var employerRequest = _migrationRepository.Query<EmployerRequest>().Where(x => x.ExternalId == registerTalentUserRequest.EmployerExternalId).FirstOrDefault();

			if (employerRequest.IsNull())
			{
				// This is a new record, create them 
				employerRequest = new EmployerRequest
				                  	{
				                  		SourceId = 0,
				                  		Request = "Serialised registerTalentUserRequest",
				                  		Status = MigrationStatus.ToBeProcessed,
				                  		ExternalId = registerTalentUserRequest.EmployerExternalId
				                  	};


				// Serialise & Save to MigrationDb
				_migrationRepository.Add(employerRequest);
				_migrationRepository.SaveChanges();

			}
			
			// Update as Processed In SourceDB - we should do this for any record getting here unless the save fails

			return true;
		}

	}
}
