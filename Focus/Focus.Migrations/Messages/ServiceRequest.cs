﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

using Focus.Core;
using Focus.Services.Core;

#endregion

namespace Focus.Migrations.Messages
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public abstract class ServiceRequest
	{
		/// <summary>
		/// A client tag identifies the client application performing the call.
		/// </summary>
		[DataMember]
		public string ClientTag { get; set; }

		/// <summary>
		/// Each service request carries a session identifier as an extra level of security.
		/// Session identifier's are issued when users are coming online / applications are using the service. 
		/// 
		/// They can expire if necessary.
		/// 
		/// The Session identifier will be passed to the profiling and logging processes to group
		/// sets of profiled and logged data togother
		/// 
		/// </summary>
		[DataMember]
		public Guid SessionId { get; set; }

		/// <summary>
		/// A unique identifier issued by the client representing the instance 
		/// of the request. Avoids rapid-fire processing of the same request over and over 
		/// in denial-of-service type attacks.
		/// 
		/// The RequestId will be passed to the profiling and logging processes
		/// 
		/// </summary>
		[DataMember]
		public Guid RequestId { get; set; }

		/// <summary>
		/// Gets or sets the user.
		/// </summary>
		/// <value>The user.</value>
		[DataMember]
		public UserContext UserContext { get; set; }
	}
}