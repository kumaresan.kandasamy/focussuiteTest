﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core;

#endregion

namespace Focus.Migrations.Messages
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class MigrationRequest : ServiceRequest
	{
		[DataMember]
		public bool Debug { get; set; }

		[DataMember]
		public int BatchSize { get; set; }

		[DataMember]
		public long[] EmployerIds { get; set; }

		[DataMember]
		public long[] JobOrderIds { get; set; }

		[DataMember]
		public long[] JobSeekerIds { get; set; }

		[DataMember]
		public long[] UserIds { get; set; }

	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class MigrationResponse : ServiceResponse
	{
	}
}
