﻿namespace Library_Data_Importer
{
  partial class LibraryDataImporter
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LibraryDataImporter));
      this.DataTypeComboBox = new System.Windows.Forms.ComboBox();
      this.DataTypeLabel = new System.Windows.Forms.Label();
      this.ProgressTextBox = new System.Windows.Forms.TextBox();
      this.statusStrip1 = new System.Windows.Forms.StatusStrip();
      this.CSVPickerOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
      this.textBox1 = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.textBox2 = new System.Windows.Forms.TextBox();
      this.textBox3 = new System.Windows.Forms.TextBox();
      this.textBox4 = new System.Windows.Forms.TextBox();
      this.textBox5 = new System.Windows.Forms.TextBox();
      this.ProgramAreaButton = new System.Windows.Forms.Button();
      this.MajorCodesButton = new System.Windows.Forms.Button();
      this.AliasesButton = new System.Windows.Forms.Button();
      this.JobDegreeButton = new System.Windows.Forms.Button();
      this.JobDegreeDemandButton = new System.Windows.Forms.Button();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.ClientLabel = new System.Windows.Forms.Label();
      this.ClientComboBox = new System.Windows.Forms.ComboBox();
      this.ImportButton = new System.Windows.Forms.Button();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // DataTypeComboBox
      // 
      this.DataTypeComboBox.FormattingEnabled = true;
      this.DataTypeComboBox.Location = new System.Drawing.Point(75, 10);
      this.DataTypeComboBox.Name = "DataTypeComboBox";
      this.DataTypeComboBox.Size = new System.Drawing.Size(121, 21);
      this.DataTypeComboBox.TabIndex = 0;
      // 
      // DataTypeLabel
      // 
      this.DataTypeLabel.AutoSize = true;
      this.DataTypeLabel.Location = new System.Drawing.Point(12, 13);
      this.DataTypeLabel.Name = "DataTypeLabel";
      this.DataTypeLabel.Size = new System.Drawing.Size(57, 13);
      this.DataTypeLabel.TabIndex = 1;
      this.DataTypeLabel.Text = "Data Type";
      // 
      // ProgressTextBox
      // 
      this.ProgressTextBox.Location = new System.Drawing.Point(15, 233);
      this.ProgressTextBox.Multiline = true;
      this.ProgressTextBox.Name = "ProgressTextBox";
      this.ProgressTextBox.ReadOnly = true;
      this.ProgressTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this.ProgressTextBox.Size = new System.Drawing.Size(479, 184);
      this.ProgressTextBox.TabIndex = 2;
      // 
      // statusStrip1
      // 
      this.statusStrip1.Location = new System.Drawing.Point(0, 431);
      this.statusStrip1.Name = "statusStrip1";
      this.statusStrip1.Size = new System.Drawing.Size(507, 22);
      this.statusStrip1.TabIndex = 3;
      this.statusStrip1.Text = "statusStrip1";
      // 
      // CSVPickerOpenFileDialog
      // 
      this.CSVPickerOpenFileDialog.FileName = "openFileDialog1";
      this.CSVPickerOpenFileDialog.Filter = "CSV files (*csv)|*.csv";
      this.CSVPickerOpenFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.CSVPickerOpenFileDialog_FileOk);
      // 
      // textBox1
      // 
      this.textBox1.Location = new System.Drawing.Point(125, 18);
      this.textBox1.Name = "textBox1";
      this.textBox1.Size = new System.Drawing.Size(246, 20);
      this.textBox1.TabIndex = 4;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(19, 28);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(71, 13);
      this.label1.TabIndex = 5;
      this.label1.Text = "Program Area";
      // 
      // textBox2
      // 
      this.textBox2.Location = new System.Drawing.Point(125, 45);
      this.textBox2.Name = "textBox2";
      this.textBox2.Size = new System.Drawing.Size(246, 20);
      this.textBox2.TabIndex = 6;
      // 
      // textBox3
      // 
      this.textBox3.Location = new System.Drawing.Point(125, 72);
      this.textBox3.Name = "textBox3";
      this.textBox3.Size = new System.Drawing.Size(246, 20);
      this.textBox3.TabIndex = 7;
      // 
      // textBox4
      // 
      this.textBox4.Location = new System.Drawing.Point(125, 99);
      this.textBox4.Name = "textBox4";
      this.textBox4.Size = new System.Drawing.Size(246, 20);
      this.textBox4.TabIndex = 8;
      // 
      // textBox5
      // 
      this.textBox5.Location = new System.Drawing.Point(125, 126);
      this.textBox5.Name = "textBox5";
      this.textBox5.Size = new System.Drawing.Size(246, 20);
      this.textBox5.TabIndex = 9;
      // 
      // ProgramAreaButton
      // 
      this.ProgramAreaButton.Location = new System.Drawing.Point(378, 18);
      this.ProgramAreaButton.Name = "ProgramAreaButton";
      this.ProgramAreaButton.Size = new System.Drawing.Size(75, 23);
      this.ProgramAreaButton.TabIndex = 10;
      this.ProgramAreaButton.Text = "Browse";
      this.ProgramAreaButton.UseVisualStyleBackColor = true;
      this.ProgramAreaButton.Click += new System.EventHandler(this.ProgramAreaButton_Click);
      // 
      // MajorCodesButton
      // 
      this.MajorCodesButton.Location = new System.Drawing.Point(378, 41);
      this.MajorCodesButton.Name = "MajorCodesButton";
      this.MajorCodesButton.Size = new System.Drawing.Size(75, 23);
      this.MajorCodesButton.TabIndex = 11;
      this.MajorCodesButton.Text = "Browse";
      this.MajorCodesButton.UseVisualStyleBackColor = true;
      // 
      // AliasesButton
      // 
      this.AliasesButton.Location = new System.Drawing.Point(378, 68);
      this.AliasesButton.Name = "AliasesButton";
      this.AliasesButton.Size = new System.Drawing.Size(75, 23);
      this.AliasesButton.TabIndex = 12;
      this.AliasesButton.Text = "Browse";
      this.AliasesButton.UseVisualStyleBackColor = true;
      // 
      // JobDegreeButton
      // 
      this.JobDegreeButton.Location = new System.Drawing.Point(378, 95);
      this.JobDegreeButton.Name = "JobDegreeButton";
      this.JobDegreeButton.Size = new System.Drawing.Size(75, 23);
      this.JobDegreeButton.TabIndex = 13;
      this.JobDegreeButton.Text = "Browse";
      this.JobDegreeButton.UseVisualStyleBackColor = true;
      // 
      // JobDegreeDemandButton
      // 
      this.JobDegreeDemandButton.Location = new System.Drawing.Point(378, 122);
      this.JobDegreeDemandButton.Name = "JobDegreeDemandButton";
      this.JobDegreeDemandButton.Size = new System.Drawing.Size(75, 23);
      this.JobDegreeDemandButton.TabIndex = 14;
      this.JobDegreeDemandButton.Text = "Browse";
      this.JobDegreeDemandButton.UseVisualStyleBackColor = true;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(19, 52);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(66, 13);
      this.label2.TabIndex = 15;
      this.label2.Text = "Major Codes";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(19, 79);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(40, 13);
      this.label3.TabIndex = 16;
      this.label3.Text = "Aliases";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(19, 106);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(62, 13);
      this.label4.TabIndex = 17;
      this.label4.Text = "Job Degree";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(19, 133);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(105, 13);
      this.label5.TabIndex = 18;
      this.label5.Text = "Job Degree Demand";
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.textBox5);
      this.groupBox1.Controls.Add(this.label5);
      this.groupBox1.Controls.Add(this.textBox1);
      this.groupBox1.Controls.Add(this.label4);
      this.groupBox1.Controls.Add(this.label1);
      this.groupBox1.Controls.Add(this.label3);
      this.groupBox1.Controls.Add(this.textBox2);
      this.groupBox1.Controls.Add(this.label2);
      this.groupBox1.Controls.Add(this.textBox3);
      this.groupBox1.Controls.Add(this.JobDegreeDemandButton);
      this.groupBox1.Controls.Add(this.textBox4);
      this.groupBox1.Controls.Add(this.JobDegreeButton);
      this.groupBox1.Controls.Add(this.ProgramAreaButton);
      this.groupBox1.Controls.Add(this.AliasesButton);
      this.groupBox1.Controls.Add(this.MajorCodesButton);
      this.groupBox1.Location = new System.Drawing.Point(15, 37);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(479, 160);
      this.groupBox1.TabIndex = 19;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "groupBox1";
      // 
      // ClientLabel
      // 
      this.ClientLabel.AutoSize = true;
      this.ClientLabel.Location = new System.Drawing.Point(202, 13);
      this.ClientLabel.Name = "ClientLabel";
      this.ClientLabel.Size = new System.Drawing.Size(33, 13);
      this.ClientLabel.TabIndex = 20;
      this.ClientLabel.Text = "Client";
      // 
      // ClientComboBox
      // 
      this.ClientComboBox.FormattingEnabled = true;
      this.ClientComboBox.Location = new System.Drawing.Point(244, 10);
      this.ClientComboBox.Name = "ClientComboBox";
      this.ClientComboBox.Size = new System.Drawing.Size(121, 21);
      this.ClientComboBox.TabIndex = 21;
      // 
      // ImportButton
      // 
      this.ImportButton.Location = new System.Drawing.Point(419, 203);
      this.ImportButton.Name = "ImportButton";
      this.ImportButton.Size = new System.Drawing.Size(75, 23);
      this.ImportButton.TabIndex = 22;
      this.ImportButton.Text = "Import";
      this.ImportButton.UseVisualStyleBackColor = true;
      // 
      // LibraryDataImporter
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(507, 453);
      this.Controls.Add(this.ImportButton);
      this.Controls.Add(this.ClientComboBox);
      this.Controls.Add(this.ClientLabel);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.statusStrip1);
      this.Controls.Add(this.ProgressTextBox);
      this.Controls.Add(this.DataTypeLabel);
      this.Controls.Add(this.DataTypeComboBox);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "LibraryDataImporter";
      this.Text = "Library Data Importer";
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ComboBox DataTypeComboBox;
    private System.Windows.Forms.Label DataTypeLabel;
    private System.Windows.Forms.TextBox ProgressTextBox;
    private System.Windows.Forms.StatusStrip statusStrip1;
    private System.Windows.Forms.OpenFileDialog CSVPickerOpenFileDialog;
    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox textBox2;
    private System.Windows.Forms.TextBox textBox3;
    private System.Windows.Forms.TextBox textBox4;
    private System.Windows.Forms.TextBox textBox5;
    private System.Windows.Forms.Button ProgramAreaButton;
    private System.Windows.Forms.Button MajorCodesButton;
    private System.Windows.Forms.Button AliasesButton;
    private System.Windows.Forms.Button JobDegreeButton;
    private System.Windows.Forms.Button JobDegreeDemandButton;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Label ClientLabel;
    private System.Windows.Forms.ComboBox ClientComboBox;
    private System.Windows.Forms.Button ImportButton;
  }
}