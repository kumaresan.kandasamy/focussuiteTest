﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceProcess;

using Focus.MessageBus.Helpers;

#endregion

namespace Focus.MessageBus
{
	partial class MessageBusService : ServiceBase
	{
		private readonly RunnerHelper _runner;
		private MessageBus _messageBus;

		/// <summary>
		/// Initializes a new instance of the <see cref="MessageBusService"/> class.
		/// </summary>
		/// <param name="runner">The runner.</param>
		public MessageBusService(RunnerHelper runner)
		{
			InitializeComponent();
			ServiceName = Program.Settings.ServiceName;
			
			_runner = runner;
		}

		/// <summary>
		/// When implemented in a derived class, executes when a Start command is sent to the service by the Service Control Manager (SCM) or when the operating system starts (for a service that starts automatically). Specifies actions to take when the service starts.
		/// </summary>
		/// <param name="args">Data passed by the start command.</param>
		protected override void OnStart(string[] args)
		{
      Program.Settings.ParseArguments(args);

			_messageBus = new MessageBus(Program.Settings.WorkerThreadCount);
			_messageBus.Start();

			_runner.Start();
		}

		/// <summary>
		/// When implemented in a derived class, executes when a Stop command is sent to the service by the Service Control Manager (SCM). Specifies actions to take when a service stops running.
		/// </summary>
		protected override void OnStop()
		{
				_messageBus.Stop();	
			_runner.Stop();
		}
	}
}
