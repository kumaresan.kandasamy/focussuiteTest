﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Net;
using System.ServiceProcess;

using Focus.MessageBus.Helpers;
using Focus.Services;

#endregion

namespace Focus.MessageBus
{
	static class Program
	{
		private static MessageBusSettings _settings;
		private static RunnerHelper _runner;

		private static readonly object _sync = new object();

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main(string[] args)
		{
            System.Net.ServicePointManager.SecurityProtocol = (SecurityProtocolType)48 | (SecurityProtocolType)192 | (SecurityProtocolType)768 | (SecurityProtocolType)3072;
			var appDomain = AppDomain.CurrentDomain;
			appDomain.UnhandledException += UnhandledExceptionHandler;

			// Get the configuration
			Settings = new MessageBusSettings(args);

			// Set up the runner
			Runner = new RunnerHelper();

			// Set up the services runtime
			ServicesRuntime = new ServicesRuntime<MessageBusStructureMapServiceLocator>(ServiceRuntimeEnvironment.DllOrExe, new MessageBusStructureMapServiceLocator());
			ServicesRuntime.Start();

			if (!Environment.UserInteractive)
			{
				// Startup as service.
				var servicesToRun = new ServiceBase[] { new MessageBusService(Runner) };
				ServiceBase.Run(servicesToRun);
			}
			else
			{
				// Startup as console (only runs once)
				try
				{
					using (var messageBus = new MessageBus(Settings.WorkerThreadCount))
					{
						messageBus.Start();

						Console.WriteLine("Press Enter to quit.");
						Console.ReadLine();

						messageBus.Stop();
					}

					Console.WriteLine(@"Done!");
				}
				catch (Exception exception)
				{
					Console.WriteLine(exception.Message);
				}
			}

			// Shutdown the service runtime
			if (ServicesRuntime != null)
				ServicesRuntime.Shutdown();			
		}

		/// <summary>
		/// Gets or sets the settings.
		/// </summary>
		internal static MessageBusSettings Settings
		{
			get { return _settings; }
			set
			{
				lock (_sync)
					_settings = (value ?? new MessageBusSettings(null));
			}
		}

		/// <summary>
		/// Gets the runner.
		/// </summary>
		internal static RunnerHelper Runner
		{
			get { return _runner; }
			set
			{
				lock (_sync)
					_runner = value;
			}
		}

		/// <summary>
		/// Gets the services runtime.
		/// </summary>
		internal static ServicesRuntime<MessageBusStructureMapServiceLocator> ServicesRuntime { get; private set; }

		/// <summary>
		/// Unhandled exception handler.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="args">The <see cref="UnhandledExceptionEventArgs"/> instance containing the event data.</param>
		private static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs args)
		{
			var e = (Exception)args.ExceptionObject;
			Logger.LogException("Focus.MessageBus: Unhandled Exception", "MessageBus", e);
		}
	}
}
