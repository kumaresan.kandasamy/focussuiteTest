﻿@ECHO OFF

REM The following directory is for .NET 4.0
set DOTNETFX4=%SystemRoot%\Microsoft.NET\Framework\v4.0.30319
set PATH=%DOTNETFX4%;%PATH%

echo Installing Focus Message Bus ...
echo ---------------------------------------------------
InstallUtil /i Focus.MessageBus.exe 
echo ---------------------------------------------------
echo Done.