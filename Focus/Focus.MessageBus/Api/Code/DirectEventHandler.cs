﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Diagnostics;

using Framework.Messaging;

using Focus.Core;

#endregion

namespace Focus.MessageBus.Api.Code
{
	internal class DirectEventHandler
	{
		private readonly bool _handleAsync;

		/// <summary>
		/// Initializes a new instance of the <see cref="DirectEventHandler" /> class.
		/// </summary>
		/// <param name="handleAsync">if set to <c>true</c> the event will be handled asynchronous.</param>
		public DirectEventHandler(bool handleAsync)
		{
			_handleAsync = handleAsync;			
		}

		private delegate void MethodInvoker(Guid messageId);

		/// <summary>
		/// Handles the event.
		/// </summary>
		/// <param name="messageId">The message identifier.</param>
		public void HandleEvent(Guid messageId)
		{
			Logger.Info("DirectEventHandler.HandleEvent : Handling '" + messageId + "' - " + (_handleAsync ? "Asynchronously" : "Synchronously"));

			var simpleDelegate = new MethodInvoker(Process);

			if (_handleAsync)
				simpleDelegate.BeginInvoke(messageId, null, null);
			else
				simpleDelegate.Invoke(messageId);
		}

		/// <summary>
		/// Processes the specified message.
		/// </summary>
		/// <param name="messageId">The message identifier.</param>
		private static void Process(Guid messageId)
		{
			Logger.Debug("DirectEventHandler.Process : Processing event message '" + messageId + "'");

			var stopwatch = Stopwatch.StartNew();

			try
			{
				var messageStore = new MessageStore();
				var dequeuedMessage = messageStore.PeekMessage(messageId);

				if (dequeuedMessage != null)
				{
					var message = dequeuedMessage.Item1;
					var messageEntity = dequeuedMessage.Item2;

					Logger.Debug("DirectEventHandler.Process: Handling  " + message.GetType().Name + " - '" + message.Id + "'");

					Program.ServicesRuntime.BeginThreadRequest();

					Logger.Debug("DirectEventHandler.Process: Runtime Services started for message '" + message.Id + "'");

					try
					{
						Logger.Debug("DirectEventHandler.Process: Executing Handle for message '" + message.Id + "'");

						var messageBusHandler = new MessageBusHandler(messageStore);
						messageBusHandler.Handle(messageEntity, message as dynamic, message.Version);

						Logger.Debug("DirectEventHandler.Process: Executed Handle for message '" + message.Id + "'");
					}
					finally
					{
						Program.ServicesRuntime.EndThreadRequest();

						Logger.Debug("DirectEventHandler.Process: Runtime Services - Request ended for message '" + message.Id + "'");
					}

					stopwatch.Stop();

					Logger.Info("DirectEventHandler.Process: Handled " + message.GetType().Name + " - '" + message.Id + "' in " + stopwatch.ElapsedMilliseconds + " ms");
				}
				else
				{
					stopwatch.Stop();

					Logger.Warn("DirectEventHandler.Process: Unable to handle message '" + messageId + "'");
				}
			}
			catch (Exception ex)
			{
				stopwatch.Stop();

				Logger.Error("DirectEventHandler.Process : Error handling message '" + messageId + ".  It was executing for " + stopwatch.ElapsedMilliseconds + " ms and failed with'" + ex.Message + "'");
				
				throw;
			}
		}
	}
}
