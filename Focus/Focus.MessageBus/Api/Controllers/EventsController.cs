﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Framework.Core;
using Framework.Messaging;

using Focus.MessageBus.Api.Code;
using Focus.MessageBus.Api.Messages;

#endregion

namespace Focus.MessageBus.Api.Controllers
{
	public class EventsController : ApiController
	{
		public HttpResponseMessage Get(Guid id, string include = null)
		{
			var messageStore = new MessageStore();
			var peekedMessage = messageStore.PeekMessage(id);

			if (peekedMessage == null)
				return Request.CreateResponse(HttpStatusCode.NotFound);

			var messageEntity = peekedMessage.Item2;

			return Request.CreateResponse(HttpStatusCode.OK, messageEntity.AsResponse(include));
		}

		public HttpResponseMessage Post([FromBody] PublishEventRequest request)
		{
			if (!ModelState.IsValid)
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

			var messageType = Type.GetType(request.MessageType);
			var message = (IMessage)request.Message.DeserializeJson(messageType);

			try
			{
				var messageStore = new MessageStore();
				messageStore.Enqueue(message, markDequeued: request.Priority == MessagePriority.Direct);

				// If this was direct then this message will need to be handled right now!!
				if (request.Priority == MessagePriority.Direct)
				{				
					var isBlocking = Request.Headers.Contains("X-IsBlocking") && (Request.Headers.GetValues("X-IsBlocking").First() ?? "").ToLower() == "true";
					Logger.Debug("EventsController.Post - " + (isBlocking ? "Is Blocking request" : "Is Non-Blocking request"));

					var handleAsync = Program.Settings.HandleDirectEventsAsynchronously && !isBlocking;
						
					var directEventHandler = new DirectEventHandler(handleAsync);
					directEventHandler.HandleEvent(message.Id);
				}

				var response = Request.CreateResponse(HttpStatusCode.Accepted);

				var uri = Url.Link("DefaultApi", new { id = message.Id });
				response.Headers.Location = new Uri(uri);

				return response;
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
		}
	}
}
