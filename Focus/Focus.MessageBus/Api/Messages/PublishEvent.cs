﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.ComponentModel.DataAnnotations;

using Framework.Messaging;

#endregion

namespace Focus.MessageBus.Api.Messages
{
	public class PublishEventRequest
	{
		[Required]
		public MessagePriority Priority { get; set; }

		[Required]
		public string MessageType { get; set; }
		
		public DateTime? ProcessOn { get; set; }
		public string EntityName { get; set; }
		public long? EntityKey { get; set; }
		public bool UpdateIfExists { get; set; }

		[Required]
		public string Message { get; set; }
	}
}
