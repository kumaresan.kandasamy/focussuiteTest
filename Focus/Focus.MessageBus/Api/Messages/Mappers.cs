﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;

using Framework.Core;
using Framework.Messaging.Entities;

#endregion

namespace Focus.MessageBus.Api.Messages
{
	public static class Mappers
	{
		public static GetEventResponse AsResponse(this MessageEntity entity, string include)
		{
			var response = new GetEventResponse
			{
				Id = entity.Id,
				MessageType = entity.MessageType.TypeName,
				Status = entity.Status,
				Priority = entity.Priority,
				Version = entity.Version,
				EnqueuedOn = entity.EnqueuedOn,
				DequeuedOn = entity.DequeuedOn,
				HandledOn = entity.HandledOn,
				ExecutionMilliseconds = entity.ExecutionMilliseconds,
				Failed = entity.Failed,
				ProcessOn = entity.ProcessOn,
				EntityName = entity.EntityName,
				EntityKey = entity.EntityKey				
			};

			if (include.IncludeContains("message"))
				response.Message = entity.MessagePayload.Data;

			return response;
		}

		/// <summary>
		/// Checks if the Include value contains the include value.
		/// </summary>
		/// <returns></returns>
		private static bool IncludeContains(this string include, string includeValue)
		{
			return include.IsNotNullOrEmpty() && (new[] { "all", includeValue }).Any(x => include.Contains(x, StringComparison.InvariantCultureIgnoreCase));
		}
	}
}
