﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Framework.Messaging;

#endregion

namespace Focus.MessageBus.Api.Messages
{
	public class GetEventResponse
	{
		public Guid Id { get; set; }
		public string MessageType { get; set; }
		public MessageStatus Status { get; set; }
		public MessagePriority Priority { get; set; }
		public string Version { get; set; }
		public DateTime EnqueuedOn { get; set; }
		public DateTime? DequeuedOn { get; set; }
		public DateTime? HandledOn { get; set; }
		public long ExecutionMilliseconds { get; set; }
		public bool Failed { get; set; }
		public DateTime? ProcessOn { get; set; }
		public string EntityName { get; set; }
		public long? EntityKey { get; set; }
		public string Message { get; set; }
	}
}
