﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Mail;
using System.Threading;

using Framework.Core;
using Framework.Logging;

#endregion

namespace Focus.MessageBus
{
	internal sealed class Logger
	{
		private static readonly object _sync = new object();

        private static long doSendMailAtFault = 0;

		/// <summary>
		/// Log.
		/// </summary>
		/// <param name="level">The level.</param>
		/// <param name="format">The format.</param>
		/// <param name="args">The args.</param>
		internal static void Log(LogLevel level, string format, params object[] args)
		{
			if (level > Program.Settings.LoggingLevel)
				return;

			var output = String.Format("{0:yyyyMMdd-HH:mm.ss} [{1,-5}] #{2,-4} {3}", DateTime.UtcNow, level.Name.ToUpper(), Thread.CurrentThread.ManagedThreadId, String.Format(format, args));
			
			if (Environment.UserInteractive)
				Console.WriteLine(output);

			lock (_sync)
				using (TextWriter logFile = new StreamWriter(GetLogFileName(), true))
					logFile.WriteLine(output);
		}

		/// <summary>
		/// Debug.
		/// </summary>
		/// <param name="format">The format.</param>
		/// <param name="args">The arguments.</param>
		internal static void Debug(string format, params object[] args)
		{
			Log(LogLevel.Debug, format, args);
		}

		/// <summary>
		/// Info.
		/// </summary>
		/// <param name="format">The format.</param>
		/// <param name="args">The arguments.</param>
		internal static void Info(string format, params object[] args)
		{
			Log(LogLevel.Info, format, args);
		}

		/// <summary>
		/// Warn.
		/// </summary>
		/// <param name="format">The format.</param>
		/// <param name="args">The arguments.</param>
		internal static void Warn(string format, params object[] args)
		{
			Log(LogLevel.Warn, format, args);
		}

		/// <summary>
		/// Error.
		/// </summary>
		/// <param name="format">The format.</param>
		/// <param name="args">The arguments.</param>
		internal static void Error(string format, params object[] args)
		{
			Log(LogLevel.Error, format, args);
		}

		/// <summary>
		/// Log an exception.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="category">The category.</param>
		/// <param name="exception">The exception.</param>
		internal static void LogException(string source, string category, Exception exception)
		{
			var exceptionDetails = String.Format("{0:yyyyMMdd-HH:mm.ss} [ERROR] #{1,-4} {2}-{3} Exception: {4}", DateTime.UtcNow, Thread.CurrentThread.ManagedThreadId, source, category, exception);

			lock (_sync)
				using (TextWriter logFile = new StreamWriter(GetLogFileName(), true))
					logFile.WriteLine(exceptionDetails);

			// This is called to autostop the runner if we have exceeded the 
			// maximum number of operation faults
			Program.Runner.IncrementOperationFaults();

            var faultCount = Program.Runner.GetOperationFaultCount();
            if (faultCount > Program.Settings.MaxAllowedOperationFaults)
            {
                if (faultCount > doSendMailAtFault)
                    doSendMailAtFault = faultCount * 3; //increment doSendMailAtFault exponentially
                //send mail when faultCount is at doSendMailAtFault
                if (faultCount == doSendMailAtFault)
                    SendSupportMail("Message Bus Exceptions", String.Format("Message Bus: \n Handling of messages in Message Bus has failed.\n Number of failed messages: {0} \n", faultCount));
                return;
            }

            //reset doSendMailAtFault since the operation fault would have been reset.
            doSendMailAtFault = 0;

			// Send an email out
			SendSupportMail(String.Format("{0}-{1} : Exception", source, category), "Details: \r\n\r\n" + exceptionDetails);
		}

		/// <summary>
		/// Log the start of a Profile operation
		/// </summary>
		internal static void LogProfileStart(long profileId, string source, string category)
		{
		  if (!Program.Settings.ProfilingEnabled)
		    return;

			lock (_sync)
				using (TextWriter logFile = new StreamWriter(GetLogFileName(), true))
					logFile.WriteLine("{0:yyyyMMdd-HH:mm.ss} [START] #{1,-4} {2}-{3} >> {4}", DateTime.UtcNow, Thread.CurrentThread.ManagedThreadId, source, category, profileId);
		}

		/// <summary>
		/// Log the start of a Profile operation
		/// </summary>
		internal static void LogProfileEnd(long profileId, string source, string category, long milliseconds)
		{
      if (!Program.Settings.ProfilingEnabled)
        return;

      lock (_sync)
				using (TextWriter logFile = new StreamWriter(GetLogFileName(), true))
					logFile.WriteLine("{0:yyyyMMdd-HH:mm.ss} [END  ] #{1,-4} {2}-{3} {4} ms >> {5}", DateTime.UtcNow, Thread.CurrentThread.ManagedThreadId, source, category, milliseconds, profileId);
		}

		/// <summary>
		/// Gets the name of the log file.
		/// </summary>
		/// <returns></returns>
		private static string GetLogFileName()
		{
      var currentFolder = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

			var logFolder = string.Concat(currentFolder, "\\Logs");
			if (!Directory.Exists(logFolder)) Directory.CreateDirectory(logFolder);

			// Going to use daily log files
			var logFileName = String.Format("{0} Log-{1}.txt", Program.Settings.ServiceName,  DateTime.Now.ToString("yyyyMMdd"));
			var logFilePath = String.Format(@"{0}\{1}", logFolder, logFileName);

			return logFilePath;
		}

		/// <summary>
		/// Sends the support mail.
		/// </summary>
		/// <param name="subject">The subject.</param>
		/// <param name="body">The body.</param>
		/// <param name="priority">The priority.</param>
		internal static void SendSupportMail(string subject, string body, MailPriority priority = MailPriority.Normal)
		{
			var from = Program.Settings.SupportFromEmailAddress;
			var to = Program.Settings.SupportToEmailAddress;

			if (String.IsNullOrEmpty(to) || String.IsNullOrEmpty(from))
				return;

			Exception sendException = null;

			try
			{
				using (var smtpClient = new SmtpClient())
				{
					var msg = new MailMessage {From = new MailAddress(from)};
					var toAddresses = to.Split(',', ';');

					foreach (var toAddress in toAddresses)
						msg.To.Add(new MailAddress(toAddress));

					var processedSubject = Program.Settings.ServiceName + " [" + Environment.MachineName + "] " + DateTime.Now.ToString("yyyyMMdd-HH:mm").Replace('\r', ' ').Replace('\n', ' ');
					msg.Subject = processedSubject + (subject.IsNotNullOrEmpty() ? " - " + subject : "");

					var processedBody = String.Format("Server:{0}{1}{1}{2}", Environment.MachineName, Environment.NewLine, body).Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>");
					msg.Body = processedBody;

					msg.Priority = priority;
					msg.IsBodyHtml = true;

					smtpClient.Send(msg);
				}
			}
			catch (Exception ex)
			{
				sendException = ex;
			}

			if (sendException != null)
				Log(LogLevel.Fatal, "FAILED to send support email:{0}{0}Exception: {1}{0}Subject: {2}{0}Body:{0}{3}", Environment.NewLine, sendException, subject, body);
		}
	}

	internal sealed class LogLevel
	{
		private readonly int _index;
		private readonly string _name;
		private readonly SourceLevels _sourceLevel;
		private readonly TraceEventType _traceEventType;

		public static readonly LogLevel All = new LogLevel("All", 6, SourceLevels.All, TraceEventType.Verbose);
		public static readonly LogLevel Debug = new LogLevel("Debug", 5, SourceLevels.Verbose, TraceEventType.Verbose);
		public static readonly LogLevel Error = new LogLevel("Error", 2, SourceLevels.Error, TraceEventType.Error);
		public static readonly LogLevel Fatal = new LogLevel("Fatal", 1, SourceLevels.Critical, TraceEventType.Critical);
		public static readonly LogLevel Info = new LogLevel("Info", 4, SourceLevels.Information, TraceEventType.Information);
		public static readonly LogLevel None = new LogLevel("None", 0, SourceLevels.Off, TraceEventType.Critical);
		public static readonly LogLevel Warn = new LogLevel("Warn", 3, SourceLevels.Warning, TraceEventType.Warning);

		/// <summary>
		/// Prevents a default instance of the <see cref="LogLevel" /> class from being created.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="index">The index.</param>
		/// <param name="sourceLevel">The source level.</param>
		/// <param name="traceEventType">Type of the trace event.</param>
		private LogLevel(string name, int index, SourceLevels sourceLevel, TraceEventType traceEventType)
		{
			_name = name;
			_index = index;
			_sourceLevel = sourceLevel;
			_traceEventType = traceEventType;
		}

		/// <summary>
		/// Gets the values.
		/// </summary>
		public static IEnumerable<LogLevel> Values
		{
			get
			{
				yield return All;
				yield return Debug;
				yield return Info;
				yield return Warn;
				yield return Error;
				yield return Fatal;
				yield return None;
			}
		}

		/// <summary>
		/// Gets the type of the trace event.
		/// </summary>
		public TraceEventType TraceEventType
		{
			get { return _traceEventType; }
		}

		/// <summary>
		/// Gets the name.
		/// </summary>
		public string Name
		{
			get { return _name; }
		}

		/// <summary>
		/// Gets the source level.
		/// </summary>
		public SourceLevels SourceLevel
		{
			get { return _sourceLevel; }
		}

		/// <summary>
		/// Returns a <see cref="System.String" /> that represents this instance.
		/// </summary>
		public override string ToString()
		{
			return _name;
		}

		/// <summary>
		/// Greater than operator.
		/// </summary>
		/// <param name="left">The left.</param>
		/// <param name="right">The right.</param>
		public static bool operator >(LogLevel left, LogLevel right)
		{
			return right != null && (left != null && left._index > right._index);
		}

		/// <summary>
		/// Less than operator.
		/// </summary>
		/// <param name="left">The left.</param>
		/// <param name="right">The right.</param>
		public static bool operator <(LogLevel left, LogLevel right)
		{
			return right != null && (left != null && left._index < right._index);
		}

		/// <summary>
		/// Greater than or equal operator.
		/// </summary>
		/// <param name="left">The left.</param>
		/// <param name="right">The right.</param>
		public static bool operator >=(LogLevel left, LogLevel right)
		{
			return right != null && (left != null && left._index >= right._index);
		}

		/// <summary>
		/// Less than or equal operator.
		/// </summary>
		/// <param name="left">The left.</param>
		/// <param name="right">The right.</param>
		public static bool operator <=(LogLevel left, LogLevel right)
		{
			return right != null && (left != null && left._index <= right._index);
		}

		/// <summary>
		/// Converts froms a source level.
		/// </summary>
		/// <param name="level">The level.</param>
		/// <returns></returns>
		public static LogLevel FromSourceLevels(SourceLevels level)
		{
			switch (level)
			{
				case SourceLevels.Information:
					return Info;

				case SourceLevels.Verbose:
					return Debug;

				case ~SourceLevels.Off:
					return Debug;

				case SourceLevels.Critical:
					return Fatal;

				case SourceLevels.Error:
					return Error;

				case SourceLevels.Warning:
					return Warn;

				default:
					return None;
			}
		}

    /// <summary>
    /// Converts froms a Log Severity.
    /// </summary>
    /// <param name="level">The level.</param>
    /// <returns></returns>
    public static LogLevel FromLogSeverity(LogSeverity level)
    {
      switch (level)
      {
        case LogSeverity.Info:
          return Info;

        case LogSeverity.Debug:
          return Debug;

        case LogSeverity.Warning:
          return Warn;

        case LogSeverity.Error:
          return Error;

        case LogSeverity.Fatal:
          return Fatal;

        default:
          return None;
      }
    }
	}

	/// <summary>
	/// Log class using Profile method to time a block of code.
	/// Should be used as follows:
	/// 
	/// Profiler.Profile("Tester", "Test", () => 
	/// {
	///   ... Code to Time ...
	/// });
	/// 
	/// </summary>
	internal static class Profiler
	{
		private static long _profileId;
		private static readonly object Sync = new object();

		/// <summary>
		/// Profiles a block of code.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="category">The category.</param>
		/// <param name="action">The action.</param>
		public static void Profile(string source, string category, Action action)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			if (category == null)
				throw new ArgumentNullException("category");

			if (action == null)
				throw new ArgumentNullException("action");

			var profileId = GetNextProfileId();
			Exception exception = null;

			Logger.LogProfileStart(profileId, source, category);

			var startDate = DateTime.UtcNow;

			try
			{
				action();
			}
			catch (Exception ex)
			{
				exception = ex;
				//Logger.LogException(source, category, exception);
			}

			Logger.LogProfileEnd(profileId, source, category, Convert.ToInt64(TimeSpan.FromTicks(DateTime.UtcNow.Ticks - startDate.Ticks).TotalMilliseconds));

			if (exception != null)
				throw exception;
		}

		/// <summary>
		/// Gets the next profile id.
		/// </summary>
		/// <returns></returns>
		private static long GetNextProfileId()
		{
			lock (Sync)
				return ++_profileId;
		}
	}
}

