﻿@ECHO OFF

REM The following directory is for .NET 4.0
set DOTNETFX4=%SystemRoot%\Microsoft.NET\Framework\v4.0.30319
set PATH=%DOTNETFX4%;%PATH%

echo Uninstalling Focus Message Bus ....
echo ---------------------------------------------------
InstallUtil /u Focus.MessageBus.exe
echo ---------------------------------------------------
echo Done.