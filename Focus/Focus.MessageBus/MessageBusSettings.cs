﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using Focus.Core;
using Framework.Core;
using Framework.Logging;

#endregion

namespace Focus.MessageBus
{
	internal sealed class MessageBusSettings
	{
		public class Keys
		{
      public const string ServiceNamePrefix = "Focus-MessageBus-ServiceName";
      public const string ServiceName = "Focus.MessageBus";
			public const string Version = "Focus.MessageBus-Version";
			public const string SupportFromEmailAddress = "Focus-MessageBus-SupportFromEmailAddress";
			public const string SupportToEmailAddress = "Focus-MessageBus-SupportToEmailAddress";
			public const string MaxAllowedServiceFaults = "Focus-MessageBus-MaxAllowedServiceFaults";
			public const string MaxAllowedOperationFaults = "Focus-MessageBus-MaxAllowedOperationFaults";
			public const string ShowDebug = "Focus-MessageBus-ShowDebug";
			public const string ProcessInterval = "Focus-MessageBus-ProcessInterval";
			public const string ClientTag = "Focus-ClientTag";
			public const string LogSeverity = "Focus-MessageBus-LogSeverity";
			public const string ProfilingEnabled = "Focus-MessageBus-ProfilingEnabled";
			public const string WorkerThreadCount = "Focus-MessageBus-WorkerThreadCount";
			public const string WorkerThreadStartDelay = "Focus-MessageBus-WorkerThreadStartDelay";
			public const string WebApiUrlBaseAddress = "Focus-MessageBus-WebApiUrlBaseAddress";
			public const string WebApiMaxRequestSize = "Focus-MessageBus-WebApiMaxRequestSize";
			public const string HandleDirectEventsAsynchronously = "Focus-MessageBus-HandleDirectEventsAsynchronously";
			public const string TargetMessageTypes = "Focus-MessageBus-TargetMessageTypes";
			public const string ExcludeMessageTypes = "Focus-MessageBus-ExcludeMessageTypes";
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="MessageBusSettings"/> class.
		/// </summary>
		/// <param name="args">The args.</param>
		public MessageBusSettings(string[] args)
		{
      // Bind to a specific app config
      var service = Assembly.GetAssembly(typeof(MessageBusInstaller));
      var configFilePath = Path.GetDirectoryName(service.Location) + "\\Focus.MessageBus.exe.config";

      AppDomain.CurrentDomain.SetData("APP_CONFIG_FILE", configFilePath);
      ResetConfigMechanism();

			ParseArguments(args);

      var serviceNameConfigKey = GetServiceNameConfigKey();
			ServiceName = GetStringConfigValue(serviceNameConfigKey, "Focus Message Bus [?]");

			Version = GetStringConfigValue(Keys.Version, Constants.SystemVersion);

      SupportFromEmailAddress = GetStringConfigValue(Keys.SupportFromEmailAddress, String.Empty);
      SupportToEmailAddress = GetStringConfigValue(Keys.SupportToEmailAddress, String.Empty);
      MaxAllowedServiceFaults = GetIntConfigValue(Keys.MaxAllowedServiceFaults, 5);
      MaxAllowedOperationFaults = GetIntConfigValue(Keys.MaxAllowedOperationFaults, 5);
      ShowDebug = GetBoolConfigValue(Keys.ShowDebug, false);
      ProcessInterval = GetIntConfigValue(Keys.ProcessInterval, 10000); // 10 Seconds

			if (ExcludeMessageTypes.IsNullOrEmpty())
				ExcludeMessageTypes = GetStringConfigValue(Keys.ExcludeMessageTypes, "").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(arg => new Guid(arg)).ToList();
				
			if (MessageTypes.IsNullOrEmpty())
				MessageTypes = GetStringConfigValue(Keys.TargetMessageTypes, "").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(arg => new Guid(arg)).ToList();
		
			WorkerThreadCount = GetIntConfigValue(Keys.WorkerThreadCount, 1); // 1 worker thread
			if (WorkerThreadCount < 1 || WorkerThreadCount > 256)
				WorkerThreadCount = 1;

			WebApiUrlBaseAddress = GetStringConfigValue(Keys.WebApiUrlBaseAddress, String.Empty);
			WebApiMaxRequestSize = GetLongConfigValue(Keys.WebApiMaxRequestSize, 2147483647);

			HandleDirectEventsAsynchronously = GetBoolConfigValue(Keys.HandleDirectEventsAsynchronously, true);

			WorkerThreadStartDelay = GetIntConfigValue(Keys.WorkerThreadStartDelay, 0);

      ProfilingEnabled = GetBoolConfigValue(Keys.ProfilingEnabled, false);
      LoggingLevel = LogLevel.FromLogSeverity(GetEnumConfigValue(Keys.LogSeverity, LogSeverity.Error));

      ClientTag = GetStringConfigValue(Keys.ClientTag, String.Empty);
		}

		/// <summary>
		/// Gets the service name.
		/// </summary>
		public string ServiceName { get; private set; }

		/// <summary>
		/// Gets the version.
		/// </summary>
		public string Version { get; private set; }

		/// <summary>
		/// Gets the support from email address.
		/// </summary>
		public string SupportFromEmailAddress { get; private set; }

		/// <summary>
		/// Gets the support to email address.
		/// </summary>
		public string SupportToEmailAddress { get; private set; }

		/// <summary>
		/// Gets the maximum number of service faults before the service is shut down.
		/// </summary>
		public int MaxAllowedServiceFaults { get; private set; }

		/// <summary>
		/// Gets the maximum number of service faults before the service is shut down.
		/// </summary>
		public int MaxAllowedOperationFaults { get; private set; }

		/// <summary>
		/// Gets or sets a value indicating whether to show debug information.
		/// </summary>
		public bool ShowDebug { get; private set; }

		/// <summary>
		/// Gets or sets the process interval.
		/// </summary>
		public int ProcessInterval { get; private set; }

		/// <summary>
		/// Gets the worker thread count.
		/// </summary>
		public int WorkerThreadCount { get; private set; }

		/// <summary>
		/// Gets the worker thread start delay.
		/// </summary>
		public int WorkerThreadStartDelay { get; private set; }

		/// <summary>
		/// Gets the Web Api Url base address.
		/// </summary>
		public string WebApiUrlBaseAddress { get; private set; }

		/// <summary>
		/// Gets the Web Api maximum request size
		/// </summary>
		public long WebApiMaxRequestSize { get; private set; }

		/// <summary>
		/// Gets a value indicating whether to handle Direct events asynchronously.
		/// 
		/// NOTE: If this is false, the calling application will have a wait placed against it
		///       while the direct events / messages are being handled
		/// </summary>
		public bool HandleDirectEventsAsynchronously { get; private set; }

		/// <summary>
		/// Gets the client tag.
		/// </summary>
		public string ClientTag { get; private set; }

		/// <summary>
		/// Gets the args string.
		/// </summary>
		public string[] Args { get; private set; }

    /// <summary>
    /// A list of message types to target
    /// </summary>
    public List<Guid> MessageTypes { get; private set; }

    /// <summary>
    /// A list of message types to ignore
    /// </summary>
    public List<Guid> ExcludeMessageTypes { get; private set; }

    public bool ProfilingEnabled { get; private set; }

    public LogLevel LoggingLevel { get; private set; }

		#region Private Methods

    /// <summary>
    /// Resets the config mechanism.
    /// </summary>
    private static void ResetConfigMechanism()
    {
      typeof(ConfigurationManager).GetField("s_initState", BindingFlags.NonPublic | BindingFlags.Static).SetValue(null, 0);
      typeof(ConfigurationManager).GetField("s_configSystem", BindingFlags.NonPublic | BindingFlags.Static).SetValue(null, null);
      typeof(ConfigurationManager).Assembly.GetTypes().Where(x => x.FullName == "System.Configuration.ClientConfigPaths").First()
        .GetField("s_current", BindingFlags.NonPublic | BindingFlags.Static).SetValue(null, null);
    }

		/// <summary>
		/// Gets the string config value.
		/// </summary>
		/// <param name="key">A key.</param>
		/// <returns></returns>
		private static string GetStringConfigValue(string key)
		{
			return ConfigurationManager.AppSettings[key];
		}

    /// <summary>
    /// Gets the service name configuration key.
    /// </summary>
    /// <returns></returns>
    private static string GetServiceNameConfigKey()
    {
      var executableName = Path.GetFileNameWithoutExtension(Assembly.GetAssembly(typeof(MessageBusInstaller)).Location);
      return String.Concat(Keys.ServiceNamePrefix, ":", (executableName.IsNotNullOrEmpty() ? executableName : Keys.ServiceName));
    }

		/// <summary>
		/// Gets the int config value.
		/// </summary>
		/// <param name="key">A key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		private static int GetIntConfigValue(string key, int defaultValue)
		{
			var stringValue = GetStringConfigValue(key);
			return string.IsNullOrEmpty(stringValue) ? defaultValue : Convert.ToInt32(stringValue);
		}

		/// <summary>
		/// Gets the long config value.
		/// </summary>
		/// <param name="key">A key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		private static long GetLongConfigValue(string key, int defaultValue)
		{
			var stringValue = GetStringConfigValue(key);
			return string.IsNullOrEmpty(stringValue) ? defaultValue : Convert.ToInt64(stringValue);
		}

		/// <summary>
		/// Gets the bool config value.
		/// </summary>
		/// <param name="key">A key.</param>
		/// <param name="defaultValue">if set to <c>true</c> [default value].</param>
		/// <returns></returns>
		private static bool GetBoolConfigValue(string key, bool defaultValue)
		{
			var stringValue = GetStringConfigValue(key);
			return string.IsNullOrEmpty(stringValue) ? defaultValue : Convert.ToBoolean(stringValue);
		}

		/// <summary>
		/// Gets the string config value.
		/// </summary>
		/// <param name="key">A key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		private static string GetStringConfigValue(string key, string defaultValue)
		{
			var stringValue = GetStringConfigValue(key);
			return string.IsNullOrEmpty(stringValue) ? defaultValue : stringValue;
		}

		/// <summary>
		/// Gets the enum configuration value.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
    private static T GetEnumConfigValue<T>(string key, T defaultValue) where T:struct
    {
      var stringValue = GetStringConfigValue(key);

      T result;
      if (!Enum.TryParse(stringValue, true, out result))
        result = defaultValue;

      return result;
    }

    /// <summary>
    /// Parses the arguments.
    /// </summary>
    /// <param name="args">The args.</param>
    internal void ParseArguments(string[] args)
    {
      Args = args;
      
      if (args == null)
        return;

      var argCount = args.Length;
      for (var argIndex = 0; argIndex < argCount; argIndex++)
      {
        var argData = args[argIndex];

        var pos = argData.IndexOf(":", StringComparison.Ordinal);

        var argSwitch = pos >= 0
          ? argData.Substring(0, pos).ToLower()
          : argData.ToLower();

        var argValue = pos >= 0
          ? argData.Substring(pos + 1).Trim()
          : string.Empty;

        switch (argSwitch)
        {
          case "/e":
            ExcludeMessageTypes = argValue.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(arg => new Guid(arg)).ToList();						
          break;

          case "/t":
            MessageTypes = argValue.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(arg => new Guid(arg)).ToList();					
            break;

          default:
            throw new Exception(string.Format("Unknown paramater: {0}", argData));
        }
      }
    }
		
		#endregion
	}
}
