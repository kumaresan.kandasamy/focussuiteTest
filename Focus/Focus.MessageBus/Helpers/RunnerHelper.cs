﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;

#endregion

namespace Focus.MessageBus.Helpers
{
	internal class RunnerHelper : HelperBase
	{
		private int _operationFaults;
		
		/// <summary>
		/// Gets or sets the runner status.
		/// </summary>
		/// <value>The status.</value>
		public MessageBusServiceRunnerStatus Status { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="RunnerHelper"/> class.
		/// </summary>
		public RunnerHelper()
		{
			Status = MessageBusServiceRunnerStatus.Stopped;
		}

		#region Public methods

		/// <summary>
		/// Starts this instance.
		/// </summary>
		public void Start()
		{
			if (Status == MessageBusServiceRunnerStatus.Started)
				return;

			lock(SyncLock)
			{
				Status = MessageBusServiceRunnerStatus.Started;

				Logger.Debug("Message Bus started.");
				Logger.Debug("Args: {0}", Program.Settings.Args.Aggregate(String.Empty, (current, arg) => current + (arg + " ")));
			}
		}

		/// <summary>
		/// Stops this instance.
		/// </summary>
		/// <param name="forceStop">if set to <c>true</c> [force stop].</param>
		/// <exception cref="System.Exception"></exception>
		public void Stop(bool forceStop = false)
		{
			if (!forceStop && Status == MessageBusServiceRunnerStatus.Stopped)
				return;

			lock (SyncLock)
			{
				Status = MessageBusServiceRunnerStatus.Stopped;
				Logger.Debug("Message Bus stopped.");
			}
		}

		/// <summary>
		/// Increments the operation faults.
		/// </summary>
		public void IncrementOperationFaults()
		{
			lock (SyncLock)
			{
				_operationFaults++;
			}
		}

        public int GetOperationFaultCount()
        {
            return _operationFaults;
        }

        public void ResetOperationFault()
        {
            lock (SyncLock)
            {
                _operationFaults = 0;
            }
        }

		#endregion
	}

	internal enum MessageBusServiceRunnerStatus
	{
		Stopped,
		Started,
		Failed
	}
}
