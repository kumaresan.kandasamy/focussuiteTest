﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.IO;
using System.Reflection;
using System.ServiceProcess;

using Framework.Core;

#endregion

namespace Focus.MessageBus
{
	[RunInstaller(true)]
	public partial class MessageBusInstaller : Installer
	{
    private static MessageBusSettings _settings;

    /// <summary>
		/// Initializes a new instance of the <see cref="MessageBusInstaller"/> class.
		/// </summary>
		public MessageBusInstaller()
		{
      // Get the configuration
      _settings = new MessageBusSettings(null);

      InitializeComponent();

			Installers.Add(GetMessageBusInstaller());
			Installers.Add(GetServiceProcessInstaller());
		}

		/// <summary>
		/// Gets the message bus installer.
		/// </summary>
		/// <returns></returns>
		private static ServiceInstaller GetMessageBusInstaller()
		{
      var installer = new ServiceInstaller { ServiceName = _settings.ServiceName, StartType = ServiceStartMode.Automatic };
			return installer;
		}

		/// <summary>
		/// Gets the service process installer.
		/// </summary>
		/// <returns></returns>
		private static ServiceProcessInstaller GetServiceProcessInstaller()
		{
			var installer = new ServiceProcessInstaller { Account = ServiceAccount.LocalSystem };
			return installer;
		}
	}
}
