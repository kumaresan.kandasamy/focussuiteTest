﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Threading;
using System.Web.Http;
using System.Web.Http.SelfHost;
using System.Web.Http.Validation.Providers;
using Framework.Runtime;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

using StructureMap;
using StructureMap.Configuration.DSL;

using Framework.Core;
using Framework.Messaging;
using Framework.Messaging.Entities;
using Framework.ServiceLocation;

using Focus.Services;
using Focus.Services.Helpers;
using StructureMap.Pipeline;

#endregion

namespace Focus.MessageBus
{
	internal class MessageBus : IDisposable
	{		
		private HttpSelfHostServer _httpSelfHostServer;
		private List<Thread> _processors;
		private readonly int _numberOfProcessors;
		private bool _stopping, _stopped;

		private readonly object _locker = new object();

		/// <summary>
		/// Initializes a new instance of the <see cref="MessageBus" /> class.
		/// </summary>
		/// <param name="numberOfProcessors">The number of processors.</param>
		public MessageBus(int numberOfProcessors = 10)
		{
			_numberOfProcessors = numberOfProcessors;

			using (var runtimeContext = new RuntimeContext(ServiceRuntimeEnvironment.DllOrExe))
			{
				var messagingHelper = new MessagingHelper(runtimeContext);
				messagingHelper.Initialise();
			}
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <exception cref="System.NotImplementedException"></exception>
		public void Dispose()
		{
			Stop(true);
		}

		/// <summary>
		/// Starts this instance.
		/// </summary>
		public void Start()
		{
			Logger.Info("MessageBus.Start - Processing Version: " + Program.Settings.Version);

			_processors = new List<Thread>();

			for (var i = 1; i <= _numberOfProcessors; ++i)
			{
				var processor = new Thread(Handler) { Name = String.Format("#{0:00}", i) };
				_processors.Add(processor);
				
				processor.Start();				
			}

			_stopping = _stopped = false;

			if (Program.Settings.WebApiUrlBaseAddress.IsNotNullOrEmpty())
			{
				Logger.Info("MessageBus.Start - WebApi Starting (" + Program.Settings.WebApiUrlBaseAddress + ")");

				_httpSelfHostServer = new HttpSelfHostServer(GetWebApiConfig(Program.Settings.WebApiUrlBaseAddress, Program.Settings.WebApiMaxRequestSize));
				_httpSelfHostServer.OpenAsync().Wait();

				Logger.Info("MessageBus.Start - WebApi Started");
			}
			else
				Logger.Info("MessageBus.Start - WebApi unable to start as no WebUrl is defined in the app.config");
		}

		/// <summary>
		/// Stops this instance.
		/// </summary>
		public void Stop()
		{
			if (_stopped)
				return;

			Stop(false);
		}

		/// <summary>
		/// Stops the specified is disposing.
		/// </summary>
		/// <param name="isDisposing">if set to <c>true</c> then we are disposing.</param>
		public void Stop(bool isDisposing)
		{
			if (_stopped)
				return;

			Logger.Info("MessageBus.Stop (isDisposing = " + isDisposing + ")");

			var waitForThreads = false;

			lock (_locker)
			{
				if (!_stopping)
				{
					if (isDisposing)
						GC.SuppressFinalize(this);

					// Stop the Web Api
					if (_httpSelfHostServer != null)
					{
						Logger.Info("MessageBus.Stop (isDisposing = " + isDisposing + "): Stopping Web Api");

						_httpSelfHostServer.CloseAsync();
						_httpSelfHostServer.Dispose();
						_httpSelfHostServer = null;
					}

					// We are now stopping
					_stopping = true;

					// We want to wait for threads to end
					waitForThreads = true;
				}
			}

			if (waitForThreads)
			{
				Logger.Debug("MessageBus.Stop (isDisposing = " + isDisposing + "): Allowing threads to end gracefully");

				while (true)
				{
					try
					{
						var processor = _processors[0];
						_processors.RemoveAt(0);

						Logger.Debug("MessageBus.Stop (isDisposing = " + isDisposing + "): Joining Processor Thread '" + processor.Name + "'");
						processor.Join();
					}
					catch
					{
						if (_processors.Count == 0)
							break;
					}
				}
			}

			_stopped = true;
		}

		/// <summary>
		/// Handler.
		/// </summary>
		private void Handler()
		{
			Logger.Info("MessageBus.Handler : Starting and initialising services");			
			var sleptAtLeastOnce = false;
					
			// Loop through doing work
			while (true)
			{
				if (sleptAtLeastOnce)
					Logger.Debug("MessageBus.Handler: Waking up");

				// If the service is stopping then quit here
				if (_stopping)
				{
					Logger.Debug("MessageBus.Handler: Has stopped.  Shutting down thread");
					return;
				}

				#region Create a Runtime and process a message 

				try
				{
					Tuple<IMessage, MessageEntity> dequeuedMessage = null;

					do
					{						
						Profiler.Profile("MessageBus", "Handler -> Process", () =>
						{
							Program.ServicesRuntime.BeginThreadRequest();

							try
							{
								if (!_stopping)
								{
									var messageBusHandler = new MessageBusHandler();
									dequeuedMessage = messageBusHandler.Dequeue(null, Program.Settings.Version, Program.Settings.MessageTypes, Program.Settings.ExcludeMessageTypes);

									if (dequeuedMessage != null)
									{
										var message = dequeuedMessage.Item1;
										var messageEntity = dequeuedMessage.Item2;

										Logger.Info("MessageBus.Handler: Handling: " + message.GetType().Name + " - " + message.Id);

										// Do the work
										try
										{
											Profiler.Profile("MessageBus", "Handler -> Handle", () => messageBusHandler.Handle(messageEntity, message as dynamic, messageEntity.Version));

											Logger.Info("MessageBus.Handler: Handling succeeded: " + message.GetType().Name + " - " + message.Id);

                                            //reset the operation faults for every successfull message.
                                            Program.Runner.ResetOperationFault();
										}
										catch (Exception ex)
										{
											Logger.LogException("MessageBus.Handler: Handling failed: " + message.GetType().Name + " - " + message.Id, "MessageBus", ex);
										}
									}
								}
								else
									dequeuedMessage = null;
							}
							catch (Exception ex)
							{
								Logger.LogException("MessageBus.Handler: Failed dequeuing the next message", "MessageBus", ex);
							}
							finally
							{
								Program.ServicesRuntime.EndThreadRequest();
							}
						});
					} while (dequeuedMessage != null);
				}
				catch (Exception ex)
				{
					Logger.LogException("MessageBus.Handler: Services Runtime Exception", "MessageBus", ex);
				}

				#endregion

				// Go to sleep, either not our turn or no task to process
				Logger.Debug("MessageBus.Handler: Going to sleep");
				sleptAtLeastOnce = true;

				// If the service is stopping then quit here
				if (_stopping)
				{
					Logger.Debug("MessageBus.Handler: Has stopped.  Shutting down thread");
					return;
				}

				// Now lets go to sleep
				Thread.Sleep(Program.Settings.ProcessInterval);
			}
		}

		/// <summary>
		/// Gets the web API configuration.
		/// </summary>
		/// <param name="baseAddress">The base address.</param>
		/// <param name="maxRequestSize">Maximum size of the request.</param>
		/// <returns></returns>
		private static HttpSelfHostConfiguration GetWebApiConfig(string baseAddress, long maxRequestSize)
		{
			var config = new HttpSelfHostConfiguration(baseAddress);

			config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });
			config.MaxReceivedMessageSize = maxRequestSize;

			// Clear the XML Formatter from the API
			config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();

			// Configure the Json formatter
			var jsonFormatter = config.Formatters.JsonFormatter;
			jsonFormatter.Indent = true;
			jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
			jsonFormatter.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
			jsonFormatter.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
			jsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());

			// Remove the aggressive validator
			config.Services.RemoveAll(typeof(System.Web.Http.Validation.ModelValidatorProvider), v => v is InvalidModelValidatorProvider);

			return config;
		}
	}

	internal class MessageBusStructureMapServiceLocator : IServiceLocator
	{
		private static bool _isDisposing;

		/// <summary>
		/// Initializes a new instance of the <see cref="MessageBusStructureMapServiceLocator"/> class.
		/// </summary>
		public MessageBusStructureMapServiceLocator() : this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="MessageBusStructureMapServiceLocator"/> class.
		/// </summary>
		/// <param name="container">The container.</param>
		public MessageBusStructureMapServiceLocator(IContainer container)
		{
			ObjectFactory.Initialize(x => x.AddRegistry(new Registry()));
			Container = ObjectFactory.Container;
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			if (_isDisposing) return;
			if (Container == null) return;

			_isDisposing = true;

			Container.Dispose();
			Container = null;
		}

		protected IContainer Container { get; private set; }

		#region Registration, resolution, release, reset, inject and teardown methods

		/// <summary>
		/// Resolves this instance.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public T Resolve<T>() where T : class
		{
			try
			{
				return Container.GetInstance<T>();
			}
			catch (Exception ex)
			{
				throw new ServiceResolutionException(typeof(T), ex);
			}
		}

		/// <summary>
		/// Resolves the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public T Resolve<T>(string key) where T : class
		{
			try
			{
				return Container.GetInstance<T>(key);
			}
			catch (Exception ex)
			{
				throw new ServiceResolutionException(typeof(T), ex);
			}
		}

		/// <summary>
		/// Resolves the specified type.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		public object Resolve(Type type)
		{
			try
			{
				return Container.GetInstance(type);
			}
			catch (Exception ex)
			{
				throw new ServiceResolutionException(type, ex);
			}
		}

		/// <summary>
		/// Resolves the services.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public IList<T> ResolveServices<T>() where T : class
		{
			return Container.GetAllInstances<T>();
		}

		/// <summary>
		/// Registers the specified impl type.
		/// </summary>
		/// <typeparam name="TInterface">The type of the interface.</typeparam>
		/// <param name="implType">Type of the impl.</param>
		public void Register<TInterface>(Type implType) where TInterface : class
		{
			var key = string.Format("{0}-{1}", typeof(TInterface).Name, implType.FullName);
			Container.Configure(x => x.For(implType).LifecycleIs(new ThreadLocalStorageLifecycle()).Use(implType).Named(key));

			// Work-around, also register this implementation to service mapping
			// without the generated key above.
			Container.Configure(x => x.For(implType).LifecycleIs(new ThreadLocalStorageLifecycle()).Use(implType));
		}

		/// <summary>
		/// Registers this instance.
		/// </summary>
		/// <typeparam name="TInterface">The type of the interface.</typeparam>
		/// <typeparam name="TImplementation">The type of the implementation.</typeparam>
		public void Register<TInterface, TImplementation>() where TImplementation : class, TInterface
		{
			Container.Configure(x => x.For<TInterface>().LifecycleIs(new ThreadLocalStorageLifecycle()).Use<TImplementation>());
		}

		/// <summary>
		/// Registers the specified key.
		/// </summary>
		/// <typeparam name="TInterface">The type of the interface.</typeparam>
		/// <typeparam name="TImplementation">The type of the implementation.</typeparam>
		/// <param name="key">The key.</param>
		public void Register<TInterface, TImplementation>(string key) where TImplementation : class, TInterface
		{
			Container.Configure(x => x.For<TInterface>().LifecycleIs(new ThreadLocalStorageLifecycle()).Use<TImplementation>().Named(key));
		}

		/// <summary>
		/// Registers the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="type">The type.</param>
		public void Register(string key, Type type)
		{
			Container.Configure(x => x.For(type).LifecycleIs(new ThreadLocalStorageLifecycle()).Use(type).Named(key));
		}

		/// <summary>
		/// Registers the specified service type.
		/// </summary>
		/// <param name="serviceType">Type of the service.</param>
		/// <param name="implType">Type of the impl.</param>
		public void Register(Type serviceType, Type implType)
		{
			Container.Configure(x => x.For(serviceType).LifecycleIs(new ThreadLocalStorageLifecycle()).Use(implType));
		}

		/// <summary>
		/// Registers the specified instance.
		/// </summary>
		/// <typeparam name="TInterface">The type of the interface.</typeparam>
		/// <param name="instance">The instance.</param>
		public void Register<TInterface>(TInterface instance) where TInterface : class
		{
			Container.Configure(x => x.For<TInterface>().LifecycleIs(new ThreadLocalStorageLifecycle()).Use(instance));
		}

		/// <summary>
		/// Registers the specified factory method.
		/// </summary>
		/// <typeparam name="TInterface">The type of the interface.</typeparam>
		/// <param name="factoryMethod">The factory method.</param>
		public void Register<TInterface>(Func<TInterface> factoryMethod) where TInterface : class
		{
			Container.Configure(x => x.For<TInterface>().LifecycleIs(new ThreadLocalStorageLifecycle()).Use(factoryMethod));
		}

		/// <summary>
		/// Releases the specified instance.
		/// </summary>
		/// <param name="instance">The instance.</param>
		public void Release(object instance)
		{
			// Not needed for StructureMap it doesn't keep references beyond the life cycle that was configured.
		}

		/// <summary>
		/// Resets this instance.
		/// </summary>
		public void Reset()
		{
			throw new NotSupportedException("StructureMap does not support reset");
		}

		/// <summary>
		/// Injects the specified instance.
		/// </summary>
		/// <typeparam name="TService">The type of the service.</typeparam>
		/// <param name="instance">The instance.</param>
		/// <returns></returns>
		public TService Inject<TService>(TService instance) where TService : class
		{
			if (instance == null)
				return null;

			Container.BuildUp(instance);

			return instance;
		}

		/// <summary>
		/// Tears down.
		/// </summary>
		/// <typeparam name="TService">The type of the service.</typeparam>
		/// <param name="instance">The instance.</param>
		public void TearDown<TService>(TService instance) where TService : class
		{
			// Not needed for StructureMap it doesn't keep references beyond the life cycle that was configured.
		}

		#endregion
	}

	internal static class ServiceRuntimeExtensions
	{
		/// <summary>
		/// Begins the request.
		/// </summary>
		public static void BeginThreadRequest(this IRuntime runtime)
		{
			runtime.BeginRequest();

			try
			{
				var lifecycle = new ThreadLocalStorageLifecycle();

				Logger.Debug("Begining Lifecycle #" + lifecycle.FindCache().GetHashCode());
			}
			catch { /* Do Nothing */ }
		}

		/// <summary>
		/// Ends the request.
		/// </summary>
		public static void EndThreadRequest(this IRuntime runtime) 
		{
			runtime.EndRequest();

			try
			{
				var lifecycle = new ThreadLocalStorageLifecycle();

				Logger.Debug("Ending (Ejecting) Lifecycle #" + lifecycle.FindCache().GetHashCode());

				lifecycle.EjectAll();
			}
			catch { /* Do Nothing */ }
		}
	}
}
