﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Focus.Core;
using Focus.Data.Core;
using Focus.Data.Core.Entities;
using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;

#endregion

namespace Focus.Data
{
  public interface ICoreRepository
  {
    #region Core Methods

    /// <summary>
    /// Adds the specified entity to the repository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="entity">The entity.</param>
    void Add<T>(T entity) where T : Entity;

    /// <summary>
    /// Removes the specified entity from the repository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="entity">The entity.</param>
    void Remove<T>(T entity) where T : Entity;

    /// <summary>
    /// Removes the entity / entities from the repository from the specified where expression.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="selector">The selector.</param>
    void Remove<T>(Expression<Func<T, bool>> selector) where T : Entity;

    /// <summary>
    /// Removes the entity / entities from the repository using the specified query.
    /// </summary>
    /// <param name="query">The query.</param>
    void Remove(Query query);

    /// <summary>
    /// Updates the entity / entities in the repository using the specified query.
    /// </summary>
    /// <param name="query">The query.</param>
    /// <param name="attributes">The attributes.</param>
    void Update(Query query, object attributes);

    /// <summary>
    /// Counts all the entities in the repository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    long Count<T>() where T : Entity;

    /// <summary>
    /// Counts the entities in the repository for the specified where expression.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="selector">The selector.</param>
    /// <returns></returns>
    long Count<T>(Expression<Func<T, bool>> selector) where T : Entity;

    /// <summary>
    /// Checks if the entities exists in the repository for the specified where expression.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="selector">The selector.</param>
    /// <returns></returns>
    bool Exists<T>(Expression<Func<T, bool>> selector) where T : Entity;

    /// <summary>
    /// Finds the entity by the id.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="id">The id.</param>
    /// <returns></returns>
    T FindById<T>(object id) where T : Entity;

    /// <summary>
    /// Finds a list of all entities in the repository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    IList<T> Find<T>() where T : Entity;

    /// <summary>
    /// Finds a list of entities in the repository using the given the specified where expression.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="selector">The selector.</param>
    /// <returns></returns>
    IList<T> Find<T>(Expression<Func<T, bool>> selector) where T : Entity;

    /// <summary>
    /// Queries this repository using a Linq query.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    IQueryable<T> Query<T>() where T : Entity;

    /// <summary>
    /// Saves the changes.
    /// </summary>
    void SaveChanges();

    /// <summary>
    /// Saves the changes to the repository.
    /// </summary>
    /// <param name="reset">if set to <c>true</c> then the repository is reset.</param>
    void SaveChanges(bool reset);

    /// <summary>
    /// Finds and pages a list of entities in the repository using the given the specified where expression.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="query">The query.</param>
    /// <param name="pageIndex">Index of the page.</param>
    /// <param name="pageSize">Size of the page.</param>
    /// <returns></returns>
    PagedList<T> FindPaged<T>(IQueryable<T> query, int pageIndex, int pageSize) where T : Entity;

    #endregion

    #region Entity Queryable Helpers

    /// <summary>
    /// Gets the action events.
    /// </summary>
    /// <value>The action events.</value>
    IQueryable<ActionEvent> ActionEvents { get; }

    /// <summary>
    /// Gets the action types.
    /// </summary>
    /// <value>The action types.</value>
    IQueryable<ActionType> ActionTypes { get; }

		/// <summary>
		/// Gets the applications.
		/// </summary>
		/// <value>The applications.</value>
		IQueryable<Application> Applications { get; }

    /// <summary>
    /// Gets the application images.
    /// </summary>
    /// <value>The application images.</value>
    IQueryable<ApplicationImage> ApplicationImages { get; }

    /// <summary>
    /// Gets the business units.
    /// </summary>
    /// <value>The business units.</value>
    IQueryable<BusinessUnit> BusinessUnits { get; }

		/// <summary>
		/// Gets the business unit addresses.
		/// </summary>
		/// <value>The business unit addresses.</value>
  	IQueryable<BusinessUnitAddress> BusinessUnitAddresses { get; }

		/// <summary>
		/// Gets the business unit descriptions.
		/// </summary>
		/// <value>The business unit descriptions.</value>
  	IQueryable<BusinessUnitDescription> BusinessUnitDescriptions { get; }

		/// <summary>
		/// Gets the business unit logos.
		/// </summary>
		/// <value>The business unit logos.</value>
  	IQueryable<BusinessUnitLogo> BusinessUnitLogos { get; } 
			
		/// <summary>
    /// Gets the certificate and licenses.
    /// </summary>
    /// <value>The certificate licenses.</value>
    IQueryable<CertificateLicence> CertificateLicenses { get; }

    /// <summary>
    /// Gets the certificate licence localisation items.
    /// </summary>
    /// <value>The certificate licence localisation items.</value>
    IQueryable<CertificateLicenceLocalisationItem> CertificateLicenceLocalisationItems { get; }

    /// <summary>
    /// Gets the code groups.
    /// </summary>
    /// <value>The code groups.</value>
    IQueryable<CodeGroup> CodeGroups { get; }

    /// <summary>
    /// Gets the code group items.
    /// </summary>
    /// <value>The code group items.</value>
    IQueryable<CodeGroupItem> CodeGroupItems { get; }

    /// <summary>
    /// Gets the code items.
    /// </summary>
    /// <value>The code items.</value>
    IQueryable<CodeItem> CodeItems { get; }

    /// <summary>
    /// Gets the configuration items.
    /// </summary>
    /// <value>The configuration items.</value>
    IQueryable<ConfigurationItem> ConfigurationItems { get; }

    /// <summary>
    /// Gets the dismissed messages.
    /// </summary>
    /// <value>The dismissed messages.</value>
    IQueryable<DismissedMessage> DismissedMessages { get; }

    /// <summary>
    /// Gets the email templates.
    /// </summary>
    /// <value>The email templates.</value>
    IQueryable<EmailTemplate> EmailTemplates { get; }

    /// <summary>
    /// Gets the employees.
    /// </summary>
    /// <value>The employees.</value>
    IQueryable<Employee> Employees { get; }

		/// <summary>
		/// Gets the employee business units.
		/// </summary>
		/// <value>The employee business units.</value>
  	IQueryable<EmployeeBusinessUnit> EmployeeBusinessUnits { get; }

		/// <summary>
    /// Gets the employers.
    /// </summary>
    /// <value>The employers.</value>
    IQueryable<Employer> Employers { get; }

    /// <summary>
    /// Gets the employer addresses.
    /// </summary>
    /// <value>The employer addresses.</value>
    IQueryable<EmployerAddress> EmployerAddresses { get; }

    /// <summary>
    /// Gets the entity types.
    /// </summary>
    /// <value>The entity types.</value>
    IQueryable<EntityType> EntityTypes { get; }

		/// <summary>
		/// Gets the excluded lens postings.
		/// </summary>
		/// <value> The excluded lens postings. </value>
		IQueryable<ExcludedLensPosting> ExcludedLensPostings { get; }

    /// <summary>
    /// Gets the jobs.
    /// </summary>
    /// <value>The jobs.</value>
    IQueryable<Job> Jobs { get; set; }

		/// <summary>
		/// Gets the jobs.
		/// </summary>
		/// <value>The jobs.</value>
		IQueryable<JobPrevData> JobPrevDatas { get; set; }

    /// <summary>
    /// Gets or sets the job certificates.
    /// </summary>
    /// <value>The job certificates.</value>
    IQueryable<JobCertificate> JobCertificates { get; }

    /// <summary>
    /// Gets the job addresses.
    /// </summary>
    /// <value>The job addresses.</value>
    IQueryable<JobAddress> JobAddresses { get; }

    /// <summary>
    /// Gets the job languages.
    /// </summary>
    /// <value>The job languages.</value>
    IQueryable<JobLanguage> JobLanguages { get; }

    /// <summary>
    /// Gets the job licenses.
    /// </summary>
    /// <value>The job licenses.</value>
    IQueryable<JobLicence> JobLicences { get; }

    /// <summary>
    /// Gets the job locations.
    /// </summary>
    /// <value>The job locations.</value>
    IQueryable<JobLocation> JobLocations { get; }

    /// <summary>
    /// Gets the job notes.
    /// </summary>
    /// <value>The job notes.</value>
    IQueryable<JobNote> JobNotes { get; }

    /// <summary>
    /// Gets the job skills.
    /// </summary>
    /// <value>The job skills.</value>
    IQueryable<JobSkill> JobSkills { get; }

    /// <summary>
    /// Gets the job special requirements.
    /// </summary>
    /// <value>The job special requirements.</value>
    IQueryable<JobSpecialRequirement> JobSpecialRequirements { get; }

    /// <summary>
    /// Gets the job tasks.
    /// </summary>
    /// <value>The job tasks.</value>
    IQueryable<JobTask> JobTasks { get; }

    /// <summary>
    /// Gets the job task localisation items.
    /// </summary>
    /// <value>The job task localisation items.</value>
    IQueryable<JobTaskLocalisationItem> JobTaskLocalisationItems { get; }

    /// <summary>
    /// Gets the job task multi options.
    /// </summary>
    /// <value>The job task multi options.</value>
    IQueryable<JobTaskMultiOption> JobTaskMultiOptions { get; }


    /// <summary>
    /// Gets the localisations.
    /// </summary>
    /// <value>The localisations.</value>
    IQueryable<Localisation> Localisations { get; }

    /// <summary>
    /// Gets the localisation items.
    /// </summary>
    /// <value>The localisation items.</value>
    IQueryable<LocalisationItem> LocalisationItems { get; }

		/// <summary>
		/// Gets the military occupations.
		/// </summary>
		/// <value>The military occupations.</value>
  	IQueryable<MilitaryOccupation> MilitaryOccupations { get; }


		/// <summary>
		/// Gets the military occupation groups.
		/// </summary>
		/// <value>The military occupation groups.</value>
		IQueryable<MilitaryOccupationGroup> MilitaryOccupationGroups { get; }
			
		/// <summary>
		/// Gets the note reminders.
		/// </summary>
		/// <value>The note reminders.</value>
  	IQueryable<NoteReminder> NoteReminders { get; } 
			
		/// <summary>
    /// Gets the onets.
    /// </summary>
    /// <value>The onets.</value>
    IQueryable<Onet> Onets { get; }

    /// <summary>
    /// Gets the onet localisation items.
    /// </summary>
    /// <value>The onet localisation items.</value>
    IQueryable<OnetLocalisationItem> OnetLocalisationItems { get; }

    /// <summary>
    /// Gets the onet phrases.
    /// </summary>
    /// <value>The onet phrases.</value>
    IQueryable<OnetPhrase> OnetPhrases { get; }

    /// <summary>
    /// Gets the onet rings.
    /// </summary>
    /// <value>The onet rings.</value>
    IQueryable<OnetRing> OnetRings { get; }

    /// <summary>
    /// Gets the onet tasks.
    /// </summary>
    /// <value>The onet tasks.</value>
    IQueryable<OnetTask> OnetTasks { get; }

    /// <summary>
    /// Gets the onet words.
    /// </summary>
    /// <value>The onet words.</value>
    IQueryable<OnetWord> OnetWords { get; }

    /// <summary>
    /// Gets the messages.
    /// </summary>
    /// <value>The messages.</value>
    IQueryable<Message> Messages { get; }

    /// <summary>
    /// Gets the message texts.
    /// </summary>
    /// <value>The message texts.</value>
    IQueryable<MessageText> MessageTexts { get; }

    /// <summary>
    /// Gets the notes.
    /// </summary>
    /// <value>The notes.</value>
    IQueryable<NAICS> NAICS { get; }

    /// <summary>
    /// Gets the notes.
    /// </summary>
    /// <value>The notes.</value>
    IQueryable<Note> Notes { get; }

		/// <summary>
		/// Gets the onet commodities.
		/// </summary>
		/// <value>
		/// The onet commodities.
		/// </value>
		IQueryable<OnetCommodity> OnetCommodities { get; }

		/// <summary>
    /// Gets the persons.
    /// </summary>
    /// <value>The persons.</value>
    IQueryable<Person> Persons { get; }

		/// <summary>
		/// Gets the person notes.
		/// </summary>
		/// <value>The person notes.</value>
		IQueryable<PersonNote> PersonNotes { get; }

    /// <summary>
    /// Gets the phone numbers.
    /// </summary>
    /// <value>The phone numbers.</value>
    IQueryable<PhoneNumber> PhoneNumbers { get; }

    /// <summary>
    /// Gets the person addresses.
    /// </summary>
    /// <value>The person addresses.</value>
    IQueryable<PersonAddress> PersonAddresses { get; }

    /// <summary>
    /// Gets the person lists.
    /// </summary>
    /// <value>The person lists.</value>
    IQueryable<PersonList> PersonLists { get; }

		/// <summary>
		/// Gets the person list people.
		/// </summary>
		/// <value>The person list people.</value>
		IQueryable<PersonListPerson> PersonListPeople { get; }

		/// <summary>
		/// Gets the person posting matches.
		/// </summary>
		/// <value>The person posting matches.</value>
		IQueryable<PersonPostingMatch> PersonPostingMatches { get; }

		/// <summary>
		/// Gets the person posting matches to ignore.
		/// </summary>
		/// <value>The person posting matches to ignore.</value>
		IQueryable<PersonPostingMatchToIgnore> PersonPostingMatchToIgnore { get; }

		/// <summary>
		/// Gets the postings.
		/// </summary>
		/// <value>The postings.</value>
		IQueryable<Posting> Postings { get; }

    /// <summary>
    /// Gets the posting surveys.
    /// </summary>
    /// <value>The posting surveys.</value>
    IQueryable<PostingSurvey> PostingSurveys { get; }

		/// <summary>
		/// Gets the open position match to ignore.
		/// </summary>
		/// <value>The open position match to ignore.</value>
		IQueryable<RecentlyPlaced> RecentlyPlaced { get; }

		/// <summary>
		/// Gets the open position match to ignore.
		/// </summary>
		/// <value>The open position match to ignore.</value>
		IQueryable<RecentlyPlacedMatch> RecentlyPlacedMatches { get; }

		/// <summary>
		/// Gets the resumes.
		/// </summary>
		/// <value>
		/// The resumes.
		/// </value>
		IQueryable<Resume> Resumes { get; }

		/// <summary>
		/// Gets the resume documents.
		/// </summary>
		/// <value>
		/// The resume documents.
		/// </value>
		IQueryable<ResumeDocument> ResumeDocuments { get; }

		/// <summary>
		/// Gets the resume jobs.
		/// </summary>
		/// <value>The resume jobs.</value>
		IQueryable<ResumeJob> ResumeJobs { get; }

    /// <summary>
    /// Gets the roles.
    /// </summary>
    /// <value>The roles.</value>
    IQueryable<Role> Roles { get; }

    /// <summary>
    /// Gets the saved messages.
    /// </summary>
    /// <value>The saved messages.</value>
    IQueryable<SavedMessage> SavedMessages { get; }

    /// <summary>
    /// Gets the saved message texts.
    /// </summary>
    /// <value>The saved message texts.</value>
    IQueryable<SavedMessageText> SavedMessageTexts { get; }

    /// <summary>
    /// Gets the saved searches.
    /// </summary>
    /// <value>The saved searches.</value>
    IQueryable<SavedSearch> SavedSearches { get; }

    /// <summary>
    /// Gets the saved search employees.
    /// </summary>
    /// <value>The saved search employees.</value>
    IQueryable<SavedSearchUser> SavedSearchUsers { get; }

    /// <summary>
    /// Gets the single sign ons.
    /// </summary>
    /// <value>The single sign ons.</value>
    IQueryable<SingleSignOn> SingleSignOns { get; }

		/// <summary>
		/// Gets the skills.
		/// </summary>
		/// <value>The skills.</value>
  	IQueryable<Skill> Skills { get; }
		
		/// <summary>
		/// Gets the standard industrial classifications.
		/// </summary>
		/// <value>The standard industrial classifications.</value>
  	IQueryable<StandardIndustrialClassification> StandardIndustrialClassifications { get; }
 
		/// <summary>
		/// Gets the status logs.
		/// </summary>
		/// <value>The status logs.</value>
  	IQueryable<StatusLog> StatusLogs { get; } 
		
		/// <summary>
    /// Gets the users.
    /// </summary>
    /// <value>The users.</value>
    IQueryable<User> Users { get; }

    /// <summary>
    /// Gets the user roles.
    /// </summary>
    /// <value>The user roles.</value>
    IQueryable<UserRole> UserRoles { get; }

		/// <summary>
		/// Gets the viewed postings.
		/// </summary>
		/// <value>
		/// The viewed postings.
		/// </value>
  	IQueryable<ViewedPosting> ViewedPostings { get; }

		/// <summary>
		/// Gets the invite to apply.
		/// </summary>
		/// <value>The invite to apply.</value>
		IQueryable<InviteToApply> InviteToApply { get; }

		#endregion

    #region View Queryable Helpers
      
    /// <summary>
    /// Gets the application views.
    /// </summary>
    /// <value>The application views.</value>
    IQueryable<ApplicationView> ApplicationViews { get; }

		/// <summary>
    /// Gets the job views.
    /// </summary>
    /// <value>The job view</value>
    IQueryable<JobView> JobViews { get; }

    /// <summary>
    /// Gets the employee views.
    /// </summary>
    /// <value>The employee views.</value>
    IQueryable<EmployeeSearchView> EmployeeSearchViews { get; }

    /// <summary>
    /// Gets the job seeker referral views.
    /// </summary>
    /// <value>The job seeker referral views.</value>
    IQueryable<JobSeekerReferralView> JobSeekerReferralViews { get; }

    /// <summary>
    /// Gets the employer account referral views.
    /// </summary>
    /// <value>The employer account referral views.</value>
    IQueryable<EmployerAccountReferralView> EmployerAccountReferralViews { get; }

    /// <summary>
    /// Gets the job posting referral views.
    /// </summary>
    /// <value>The job posting referral views.</value>
    IQueryable<JobPostingReferralView> JobPostingReferralViews { get; }

    /// <summary>
    /// Gets the staff search views.
    /// </summary>
    /// <value>The staff search views.</value>
    IQueryable<StaffSearchView> StaffSearchViews { get; }

    /// <summary>
    /// Gets the lookup items views.
    /// </summary>
    /// <value>The lookup items views.</value>
    IQueryable<LookupItemsView> LookupItemsViews { get; }

    /// <summary>
    /// Gets the candidate note views.
    /// </summary>
    /// <value>The candidate note views.</value>
    IQueryable<CandidateNoteView> CandidateNoteViews { get; }

    /// <summary>
    /// Gets the person list candidate views. 
    /// </summary>
    /// <value>The person list candidate views.</value>
    IQueryable<PersonListCandidateView> PersonListCandidateViews { get; }

    /// <summary>
    /// Gets the expiring job views.
    /// </summary>
    /// <value>The expiring job views.</value>
    IQueryable<ExpiringJobView> ExpiringJobViews { get; }

		/// <summary>
		/// Gets the note reminder view.
		/// </summary>
		/// <value>The note reminder view.</value>
  	IQueryable<NoteReminderView> NoteReminderViews { get; }

		/// <summary>
		/// Gets the application status log views.
		/// </summary>
		/// <value>The application status log views.</value>
  	IQueryable<ApplicationStatusLogView> ApplicationStatusLogViews { get; }

		/// <summary>
		/// Gets the active search alert views.
		/// </summary>
		/// <value>The active search alert views.</value>
  	IQueryable<ActiveSearchAlertView> ActiveSearchAlertViews { get; }

		/// <summary>
		/// Gets the hiring manager activity view.
		/// </summary>
		/// <value>The hiring manager activity view.</value>
		IQueryable<HiringManagerActivityView> HiringManagerActivityView { get; }

		/// <summary>
		/// Gets the assist user activity views.
		/// </summary>
		/// <value>The assist user activity views.</value>
	  IQueryable<AssistUserActivityView> AssistUserActivityViews { get; }

		/// <summary>
		/// Gets the assist user employers assisted views.
		/// </summary>
		/// <value>The assist user employers assisted views.</value>
		IQueryable<AssistUserEmployersAssistedView> AssistUserEmployersAssistedViews { get; }

		/// <summary>
		/// Gets the assist user employers assisted views.
		/// </summary>
		/// <value>The assist user employers assisted views.</value>
		IQueryable<AssistUserJobSeekersAssistedView> AssistUserJobSeekersAssistedViews { get; }

    /// <summary>
    /// Gets the user alert settings.
    /// </summary>
    /// <value>The user alert settings.</value>
    IQueryable<UserAlert> UserAlerts { get; }

    /// <summary>
    /// Gets the active user alert settings, including information on Users.
    /// </summary>
    /// <value>The active user alert views.</value>
    IQueryable<ActiveUserAlertView> ActiveUserAlertViews { get; }

		/// <summary>
		/// Gets the candidate view.
		/// </summary>
		/// <value>The candidate view.</value>
		IQueryable<CandidateView> CandidateViews { get; }
		
		/// <summary>
		/// Gets the recently placed views.
		/// </summary>
		/// <value>The recently placed views.</value>
		IQueryable<RecentlyPlacedMatchesToIgnore> RecentlyPlacedMatchesToIgnore { get; }
		
		/// <summary>
		/// Gets the job seeker activity action views.
		/// </summary>
		/// <value>The job seeker activity action views.</value>
		IQueryable<JobSeekerActivityActionView> JobSeekerActivityActionViews { get; }

		/// <summary>
		/// Gets the candidate click how to apply view.
		/// </summary>
		/// <value>The candidate click how to apply view.</value>
		IQueryable<CandidateClickHowToApplyView> CandidateClickHowToApplyView { get; }

		/// <summary>
		/// Gets the candidate did not click how to apply view.
		/// </summary>
		/// <value>The candidate did not click how to apply view.</value>
		IQueryable<CandidateDidNotClickHowToApplyView> CandidateDidNotClickHowToApplyView { get; }

		/// <summary>
		/// Gets the viewed resume views.
		/// </summary>
		/// <value>The viewed resume views.</value>
		IQueryable<ViewedResumeView> ViewedResumeViews { get; }

		/// <summary>
		/// Gets the military occupation job title views.
		/// </summary>
		/// <value>The military occupation job title views.</value>
  	IQueryable<MilitaryOccupationJobTitleView> MilitaryOccupationJobTitleViews { get; }

		/// <summary>
		/// Gets the business unit activity.
		/// </summary>
		/// <value>The business unit activity.</value>
		IQueryable<BusinessUnitActivityView> BusinessUnitActivity { get; }

		/// <summary>
		/// Gets the business unit placements.
		/// </summary>
		/// <value>The business unit placements.</value>
		IQueryable<BusinessUnitPlacementsView> BusinessUnitPlacements { get; }

		/// <summary>
		/// Gets the open position matched.
		/// </summary>
		/// <value>The open position matched.</value>
		IQueryable<OpenPositionMatchesView> OpenPositionMatchViews { get; }

		#endregion

		#region Stored Procedure Queryable Helpers

		IQueryable<RecentlyPlacedMatchView> GetRecentlyPlacedPersonMatch(long userId);

		#endregion

  }
}
