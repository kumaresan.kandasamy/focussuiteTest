﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Linq;
using Mindscape.LightSpeed.Querying;

namespace Focus.Data.Reporting
{
  public class FocusReportingRepository: IFocusReportingRepository
  {
    public static readonly LightSpeedContext<FocusReportingUnitOfWork> DataContext = new LightSpeedContext<FocusReportingUnitOfWork>("FocusReporting") { PluralizeTableNames = false };
    protected IUnitOfWork UnitOfWork { get; private set; }

    public FocusReportingRepository(IUnitOfWork unitOfWork)
    {
      UnitOfWork = unitOfWork;
    }

  	public IQueryable<T> GenericQuery<T>()
  	{
  		throw new NotImplementedException();
  	}

  	public FocusReportingUnitOfWork GetReportingUnitOfWork()
		{
			return (FocusReportingUnitOfWork)UnitOfWork;
		}

		public IQueryable<JobSeeker> JobSeekers
		{
			get { return Query<JobSeeker>(); }
		}

  	public IQueryable<ReportDate> Configurations
  	{
			get { return Query<ReportDate>(); }
  	}

    #region Core Members
    /// <summary>
    /// Adds the specified entity to the repository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="entity">The entity.</param>
    public void Add<T>(T entity) where T : Entity
    {
      UnitOfWork.Add(entity);
    }

    /// <summary>
    /// Removes the specified entity from the repository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="entity">The entity.</param>
    public void Remove<T>(T entity) where T : Entity
    {
      UnitOfWork.Remove(entity);
    }

    /// <summary>
    /// Removes the entity / entities from the repository from the specified where expression.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="selector">The selector.</param>
    public void Remove<T>(Expression<Func<T, bool>> selector) where T : Entity
    {
      foreach (var entity in UnitOfWork.Query<T>().Where(selector))
        UnitOfWork.Remove(entity);
    }

    /// <summary>
    /// Removes the entity / entities from the repository using the specified query.
    /// </summary>
    /// <param name="query">The query.</param>
    public void Remove(Query query)
    {
      UnitOfWork.Remove(query);
    }

    /// <summary>
    /// Updates the entity / entities in the repository using the specified query.
    /// </summary>
    /// <param name="query">The query.</param>
    /// <param name="attributes">The attributes.</param>
    public void Update(Query query, object attributes)
    {
      UnitOfWork.Update(query, attributes);
    }

    /// <summary>
    /// Counts all the entities in the repository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public long Count<T>() where T : Entity
    {
      return UnitOfWork.Count<T>();
    }

    /// <summary>
    /// Counts the entities in the repository for the specified where expression.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="selector">The selector.</param>
    /// <returns></returns>
    public long Count<T>(Expression<Func<T, bool>> selector) where T : Entity
    {
      return UnitOfWork.Query<T>().Count(selector);
    }

    /// <summary>
    /// Checks if the entities exists in the repository for the specified where expression.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="selector">The selector.</param>
    /// <returns></returns>
    public bool Exists<T>(Expression<Func<T, bool>> selector) where T : Entity
    {
      return Count(selector) > 0;
    }

    /// <summary>
    /// Finds the entity by the id.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="id">The id.</param>
    /// <returns></returns>
    public T FindById<T>(object id) where T : Entity
    {
      return UnitOfWork.FindById<T>(id);
    }

    /// <summary>
    /// Finds a list of all entities in the repository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IList<T> Find<T>() where T : Entity
    {
      return UnitOfWork.Find<T>();
    }

    /// <summary>
    /// Finds a list of entities in the repository using the given the specified where expression.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="selector">The selector.</param>
    /// <returns></returns>
    public IList<T> Find<T>(Expression<Func<T, bool>> selector) where T : Entity
    {
      return UnitOfWork.Query<T>().Where(selector).ToList();
    }

    /// <summary>
    /// Queries this repository using a Linq query.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IQueryable<T> Query<T>() where T : Entity
    {
      return UnitOfWork.Query<T>();
    }

    /// <summary>
    /// Saves the changes.
    /// </summary>
    public void SaveChanges()
    {
      UnitOfWork.SaveChanges();
    }

    /// <summary>
    /// Saves the changes to the repository.
    /// </summary>
    /// <param name="reset">if set to <c>true</c> then the repository is reset.</param>
    public void SaveChanges(bool reset)
    {
      UnitOfWork.SaveChanges(reset);
    }

		/// <summary>
		/// Executes the SQL.
		/// </summary>
		/// <param name="sql">The SQL.</param>
		/// <returns></returns>
		public int ExecuteNonQuery(string sql)
		{
			using (var cmd = UnitOfWork.Context.DataProviderObjectFactory.CreateCommand())
			{
				cmd.CommandText = sql;
				cmd.CommandType = CommandType.Text;

				return UnitOfWork.PrepareCommand(cmd).ExecuteNonQuery();
			}
		}
		
		#endregion

		///// <summary>
		///// Finds and pages a list of entities in the repository using the given the specified where expression.
		///// </summary>
		///// <typeparam name="T"></typeparam>
		///// <param name="query">The query.</param>
		///// <param name="pageIndex">Index of the page.</param>
		///// <param name="pageSize">Size of the page.</param>
		///// <returns></returns>
		//public PagedList<T> FindPaged<T>(IQueryable<T> query, int pageIndex, int pageSize) where T : Entity
		//{
		//  throw new NotImplementedException();
		//}

    /// <summary>
    /// Gets the job seeker.
    /// </summary>
    /// <value>The job seeker.</value>
    public IQueryable<JobSeeker> JobSeeker
    {
      get { return Query<JobSeeker>(); }
    }

   
  }
}
