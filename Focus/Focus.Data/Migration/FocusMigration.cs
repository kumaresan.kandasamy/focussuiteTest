using System;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Validation;
using Mindscape.LightSpeed.Linq;

#region Entities

namespace Focus.Data.Migration
{
  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table(IdentityMethod=IdentityMethod.KeyTable)]
  public partial class EmployerRequest : Entity<long>
  {
    #region Fields
  
    private long _sourceId;
    private long _focusId;
    private string _request;
    private Focus.Core.MigrationStatus _status;
    [ValidateLength(0, 200)]
    private string _externalId;
    [ValidatePresence]
    private string _message;
    private Focus.Core.MigrationEmployerRecordType _recordType;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SourceId entity attribute.</summary>
    public const string SourceIdField = "SourceId";
    /// <summary>Identifies the FocusId entity attribute.</summary>
    public const string FocusIdField = "FocusId";
    /// <summary>Identifies the Request entity attribute.</summary>
    public const string RequestField = "Request";
    /// <summary>Identifies the Status entity attribute.</summary>
    public const string StatusField = "Status";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";
    /// <summary>Identifies the Message entity attribute.</summary>
    public const string MessageField = "Message";
    /// <summary>Identifies the RecordType entity attribute.</summary>
    public const string RecordTypeField = "RecordType";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public long SourceId
    {
      get { return Get(ref _sourceId, "SourceId"); }
      set { Set(ref _sourceId, value, "SourceId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long FocusId
    {
      get { return Get(ref _focusId, "FocusId"); }
      set { Set(ref _focusId, value, "FocusId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Request
    {
      get { return Get(ref _request, "Request"); }
      set { Set(ref _request, value, "Request"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.MigrationStatus Status
    {
      get { return Get(ref _status, "Status"); }
      set { Set(ref _status, value, "Status"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Message
    {
      get { return Get(ref _message, "Message"); }
      set { Set(ref _message, value, "Message"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.MigrationEmployerRecordType RecordType
    {
      get { return Get(ref _recordType, "RecordType"); }
      set { Set(ref _recordType, value, "RecordType"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table(IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobOrderRequest : Entity<long>
  {
    #region Fields
  
    private long _sourceId;
    private long _focusId;
    private string _request;
    private Focus.Core.MigrationStatus _status;
    [ValidateLength(0, 200)]
    private string _externalId;
    private Focus.Core.MigrationJobOrderRecordType _recordType;
    [ValidatePresence]
    private string _message;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SourceId entity attribute.</summary>
    public const string SourceIdField = "SourceId";
    /// <summary>Identifies the FocusId entity attribute.</summary>
    public const string FocusIdField = "FocusId";
    /// <summary>Identifies the Request entity attribute.</summary>
    public const string RequestField = "Request";
    /// <summary>Identifies the Status entity attribute.</summary>
    public const string StatusField = "Status";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";
    /// <summary>Identifies the RecordType entity attribute.</summary>
    public const string RecordTypeField = "RecordType";
    /// <summary>Identifies the Message entity attribute.</summary>
    public const string MessageField = "Message";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public long SourceId
    {
      get { return Get(ref _sourceId, "SourceId"); }
      set { Set(ref _sourceId, value, "SourceId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long FocusId
    {
      get { return Get(ref _focusId, "FocusId"); }
      set { Set(ref _focusId, value, "FocusId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Request
    {
      get { return Get(ref _request, "Request"); }
      set { Set(ref _request, value, "Request"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.MigrationStatus Status
    {
      get { return Get(ref _status, "Status"); }
      set { Set(ref _status, value, "Status"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.MigrationJobOrderRecordType RecordType
    {
      get { return Get(ref _recordType, "RecordType"); }
      set { Set(ref _recordType, value, "RecordType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Message
    {
      get { return Get(ref _message, "Message"); }
      set { Set(ref _message, value, "Message"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table(IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeekerRequest : Entity<long>
  {
    #region Fields
  
    private long _sourceId;
    private long _focusId;
    [ValidatePresence]
    private string _request;
    private Focus.Core.MigrationStatus _status;
    [ValidatePresence]
    private string _externalId;
    [ValidatePresence]
    private string _message;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SourceId entity attribute.</summary>
    public const string SourceIdField = "SourceId";
    /// <summary>Identifies the FocusId entity attribute.</summary>
    public const string FocusIdField = "FocusId";
    /// <summary>Identifies the Request entity attribute.</summary>
    public const string RequestField = "Request";
    /// <summary>Identifies the Status entity attribute.</summary>
    public const string StatusField = "Status";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";
    /// <summary>Identifies the Message entity attribute.</summary>
    public const string MessageField = "Message";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public long SourceId
    {
      get { return Get(ref _sourceId, "SourceId"); }
      set { Set(ref _sourceId, value, "SourceId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long FocusId
    {
      get { return Get(ref _focusId, "FocusId"); }
      set { Set(ref _focusId, value, "FocusId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Request
    {
      get { return Get(ref _request, "Request"); }
      set { Set(ref _request, value, "Request"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.MigrationStatus Status
    {
      get { return Get(ref _status, "Status"); }
      set { Set(ref _status, value, "Status"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Message
    {
      get { return Get(ref _message, "Message"); }
      set { Set(ref _message, value, "Message"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table(IdentityMethod=IdentityMethod.KeyTable)]
  public partial class UserRequest : Entity<long>
  {
    #region Fields
  
    private long _sourceId;
    private long _focusId;
    [ValidatePresence]
    private string _request;
    private Focus.Core.MigrationStatus _status;
    [ValidatePresence]
    private string _externalId;
    [ValidatePresence]
    private string _message;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SourceId entity attribute.</summary>
    public const string SourceIdField = "SourceId";
    /// <summary>Identifies the FocusId entity attribute.</summary>
    public const string FocusIdField = "FocusId";
    /// <summary>Identifies the Request entity attribute.</summary>
    public const string RequestField = "Request";
    /// <summary>Identifies the Status entity attribute.</summary>
    public const string StatusField = "Status";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";
    /// <summary>Identifies the Message entity attribute.</summary>
    public const string MessageField = "Message";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public long SourceId
    {
      get { return Get(ref _sourceId, "SourceId"); }
      set { Set(ref _sourceId, value, "SourceId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long FocusId
    {
      get { return Get(ref _focusId, "FocusId"); }
      set { Set(ref _focusId, value, "FocusId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Request
    {
      get { return Get(ref _request, "Request"); }
      set { Set(ref _request, value, "Request"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.MigrationStatus Status
    {
      get { return Get(ref _status, "Status"); }
      set { Set(ref _status, value, "Status"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Message
    {
      get { return Get(ref _message, "Message"); }
      set { Set(ref _message, value, "Message"); }
    }

    #endregion
  }
	


  /// <summary>
  /// Provides a strong-typed unit of work for working with the FocusMigration model.
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  public partial class FocusMigrationUnitOfWork : Mindscape.LightSpeed.UnitOfWork
  {

    public System.Linq.IQueryable<EmployerRequest> EmployerRequests
    {
      get { return this.Query<EmployerRequest>(); }
    }
    
    public System.Linq.IQueryable<JobOrderRequest> JobOrderRequests
    {
      get { return this.Query<JobOrderRequest>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerRequest> JobSeekerRequests
    {
      get { return this.Query<JobSeekerRequest>(); }
    }
    
    public System.Linq.IQueryable<UserRequest> UserRequests
    {
      get { return this.Query<UserRequest>(); }
    }
    
  }
}

#endregion


#region Data Transfer Objects - Classes

/*
namespace Focus.Core.DataTransferObjects.FocusMigration
{
  using System;
  using System.Runtime.Serialization;

    [Serializable]
    [DataContract(Name="EmployerRequest", Namespace = Constants.DataContractNamespace)]
    public class EmployerRequestDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long SourceId { get; set; }
      [DataMember]
      public long FocusId { get; set; }
      [DataMember]
      public string Request { get; set; }
      [DataMember]
      public MigrationStatus Status { get; set; }
      [DataMember]
      public string ExternalId { get; set; }
      [DataMember]
      public string Message { get; set; }
      [DataMember]
      public MigrationEmployerRecordType RecordType { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobOrderRequest", Namespace = Constants.DataContractNamespace)]
    public class JobOrderRequestDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long SourceId { get; set; }
      [DataMember]
      public long FocusId { get; set; }
      [DataMember]
      public string Request { get; set; }
      [DataMember]
      public MigrationStatus Status { get; set; }
      [DataMember]
      public string ExternalId { get; set; }
      [DataMember]
      public MigrationJobOrderRecordType RecordType { get; set; }
      [DataMember]
      public string Message { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeekerRequest", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerRequestDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long SourceId { get; set; }
      [DataMember]
      public long FocusId { get; set; }
      [DataMember]
      public string Request { get; set; }
      [DataMember]
      public MigrationStatus Status { get; set; }
      [DataMember]
      public string ExternalId { get; set; }
      [DataMember]
      public string Message { get; set; }
    }

    [Serializable]
    [DataContract(Name="UserRequest", Namespace = Constants.DataContractNamespace)]
    public class UserRequestDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long SourceId { get; set; }
      [DataMember]
      public long FocusId { get; set; }
      [DataMember]
      public string Request { get; set; }
      [DataMember]
      public MigrationStatus Status { get; set; }
      [DataMember]
      public string ExternalId { get; set; }
      [DataMember]
      public string Message { get; set; }
    }


}
*/

#endregion

#region Data Transfer Objects - Mappers

/*
namespace Focus.Services.DtoMappers
{
    using Focus.Core.DataTransferObjects.FocusMigration;
    using Focus.Data.Migration;

    public static class FocusMigrationMappers
    {

      #region EmployerRequestDto Mappers

      public static EmployerRequestDto AsDto(this EmployerRequest entity)
      {
        var dto = new EmployerRequestDto
        {
          SourceId = entity.SourceId,
          FocusId = entity.FocusId,
          Request = entity.Request,
          Status = entity.Status,
          ExternalId = entity.ExternalId,
          Message = entity.Message,
          RecordType = entity.RecordType,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static EmployerRequest CopyTo(this EmployerRequestDto dto, EmployerRequest entity)
      {
        entity.SourceId = dto.SourceId;
        entity.FocusId = dto.FocusId;
        entity.Request = dto.Request;
        entity.Status = dto.Status;
        entity.ExternalId = dto.ExternalId;
        entity.Message = dto.Message;
        entity.RecordType = dto.RecordType;
        return entity;
      }

      public static EmployerRequest CopyTo(this EmployerRequestDto dto)
      {
        var entity = new EmployerRequest();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobOrderRequestDto Mappers

      public static JobOrderRequestDto AsDto(this JobOrderRequest entity)
      {
        var dto = new JobOrderRequestDto
        {
          SourceId = entity.SourceId,
          FocusId = entity.FocusId,
          Request = entity.Request,
          Status = entity.Status,
          ExternalId = entity.ExternalId,
          RecordType = entity.RecordType,
          Message = entity.Message,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobOrderRequest CopyTo(this JobOrderRequestDto dto, JobOrderRequest entity)
      {
        entity.SourceId = dto.SourceId;
        entity.FocusId = dto.FocusId;
        entity.Request = dto.Request;
        entity.Status = dto.Status;
        entity.ExternalId = dto.ExternalId;
        entity.RecordType = dto.RecordType;
        entity.Message = dto.Message;
        return entity;
      }

      public static JobOrderRequest CopyTo(this JobOrderRequestDto dto)
      {
        var entity = new JobOrderRequest();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeekerRequestDto Mappers

      public static JobSeekerRequestDto AsDto(this JobSeekerRequest entity)
      {
        var dto = new JobSeekerRequestDto
        {
          SourceId = entity.SourceId,
          FocusId = entity.FocusId,
          Request = entity.Request,
          Status = entity.Status,
          ExternalId = entity.ExternalId,
          Message = entity.Message,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeekerRequest CopyTo(this JobSeekerRequestDto dto, JobSeekerRequest entity)
      {
        entity.SourceId = dto.SourceId;
        entity.FocusId = dto.FocusId;
        entity.Request = dto.Request;
        entity.Status = dto.Status;
        entity.ExternalId = dto.ExternalId;
        entity.Message = dto.Message;
        return entity;
      }

      public static JobSeekerRequest CopyTo(this JobSeekerRequestDto dto)
      {
        var entity = new JobSeekerRequest();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region UserRequestDto Mappers

      public static UserRequestDto AsDto(this UserRequest entity)
      {
        var dto = new UserRequestDto
        {
          SourceId = entity.SourceId,
          FocusId = entity.FocusId,
          Request = entity.Request,
          Status = entity.Status,
          ExternalId = entity.ExternalId,
          Message = entity.Message,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static UserRequest CopyTo(this UserRequestDto dto, UserRequest entity)
      {
        entity.SourceId = dto.SourceId;
        entity.FocusId = dto.FocusId;
        entity.Request = dto.Request;
        entity.Status = dto.Status;
        entity.ExternalId = dto.ExternalId;
        entity.Message = dto.Message;
        return entity;
      }

      public static UserRequest CopyTo(this UserRequestDto dto)
      {
        var entity = new UserRequest();
        return dto.CopyTo(entity);
      }	

      #endregion

    }
}
*/

#endregion
