﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Linq;
using Mindscape.LightSpeed.Querying;

#endregion

namespace Focus.Data.Migration
{
  public class FocusMigrationRepository : IFocusMigrationRepository
  {
    public static readonly LightSpeedContext<FocusMigrationUnitOfWork> DataContext = new LightSpeedContext<FocusMigrationUnitOfWork>("FocusMigration") { PluralizeTableNames = false };
    protected IUnitOfWork UnitOfWork { get; private set; }

    public FocusMigrationRepository()
    {
      UnitOfWork = DataContext.CreateUnitOfWork();
    }
    public FocusMigrationRepository(IUnitOfWork unitOfWork)
    {
      UnitOfWork = unitOfWork;
    }

    #region Core Members
    /// <summary>
    /// Adds the specified entity to the repository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="entity">The entity.</param>
    public void Add<T>(T entity) where T : Entity
    {
      UnitOfWork.Add(entity);
    }

    /// <summary>
    /// Removes the specified entity from the repository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="entity">The entity.</param>
    public void Remove<T>(T entity) where T : Entity
    {
      UnitOfWork.Remove(entity);
    }

    /// <summary>
    /// Removes the entity / entities from the repository from the specified where expression.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="selector">The selector.</param>
    public void Remove<T>(Expression<Func<T, bool>> selector) where T : Entity
    {
      foreach (var entity in UnitOfWork.Query<T>().Where(selector))
        UnitOfWork.Remove(entity);
    }

    /// <summary>
    /// Removes the entity / entities from the repository using the specified query.
    /// </summary>
    /// <param name="query">The query.</param>
    public void Remove(Query query)
    {
      UnitOfWork.Remove(query);
    }

    /// <summary>
    /// Updates the entity / entities in the repository using the specified query.
    /// </summary>
    /// <param name="query">The query.</param>
    /// <param name="attributes">The attributes.</param>
    public void Update(Query query, object attributes)
    {
      UnitOfWork.Update(query, attributes);
    }

    /// <summary>
    /// Counts all the entities in the repository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public long Count<T>() where T : Entity
    {
      return UnitOfWork.Count<T>();
    }

    /// <summary>
    /// Counts the entities in the repository for the specified where expression.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="selector">The selector.</param>
    /// <returns></returns>
    public long Count<T>(Expression<Func<T, bool>> selector) where T : Entity
    {
      return UnitOfWork.Query<T>().Count(selector);
    }

    /// <summary>
    /// Checks if the entities exists in the repository for the specified where expression.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="selector">The selector.</param>
    /// <returns></returns>
    public bool Exists<T>(Expression<Func<T, bool>> selector) where T : Entity
    {
      return Count(selector) > 0;
    }

    /// <summary>
    /// Finds the entity by the id.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="id">The id.</param>
    /// <returns></returns>
    public T FindById<T>(object id) where T : Entity
    {
      return UnitOfWork.FindById<T>(id);
    }

    /// <summary>
    /// Finds a list of all entities in the repository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IList<T> Find<T>() where T : Entity
    {
      return UnitOfWork.Find<T>();
    }

    /// <summary>
    /// Finds a list of entities in the repository using the given the specified where expression.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="selector">The selector.</param>
    /// <returns></returns>
    public IList<T> Find<T>(Expression<Func<T, bool>> selector) where T : Entity
    {
      return UnitOfWork.Query<T>().Where(selector).ToList();
    }

    /// <summary>
    /// Queries this repository using a Linq query.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IQueryable<T> Query<T>() where T : Entity
    {
      return UnitOfWork.Query<T>();
    }

    /// <summary>
    /// Saves the changes.
    /// </summary>
    public void SaveChanges()
    {
      UnitOfWork.SaveChanges();
    }

    /// <summary>
    /// Saves the changes to the repository.
    /// </summary>
    /// <param name="reset">if set to <c>true</c> then the repository is reset.</param>
    public void SaveChanges(bool reset)
    {
      UnitOfWork.SaveChanges(reset);
    }

    #endregion


    #region Entity Queryable Helpers

    /// <summary>
    /// Gets the Employer Requests.
    /// </summary>
    /// <value>The Employer Request.</value>
    public IQueryable<EmployerRequest> EmployerRequests
    {
      get { return Query<EmployerRequest>(); }
    }

    /// <summary>
    /// Gets the JobOrder Requests.
    /// </summary>
    /// <value>The JobOrder Request.</value>
    public IQueryable<JobOrderRequest> JobOrderRequests
    {
      get { return Query<JobOrderRequest>(); }
    }

    /// <summary>
    /// Gets the JobOrder Requests.
    /// </summary>
    /// <value>The JobOrder Request.</value>
    public IQueryable<JobSeekerRequest> JobSeekerRequests
    {
      get { return Query<JobSeekerRequest>(); }
    }

    /// <summary>
    /// Gets the User Requests.
    /// </summary>
    /// <value>The Assist User Request.</value>
    public IQueryable<UserRequest> UserRequests
    {
      get { return Query<UserRequest>(); }
    }
    #endregion
  }
}
