using System;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Validation;
using Mindscape.LightSpeed.Linq;

#region Entities

namespace Focus.Data.Migration.Entities
{
  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Migration.EmployerRequest", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class EmployerRequest : Entity<long>
  {
    #region Fields
  
    private long _sourceId;
    private long _focusId;
    private string _request;
    [ValueField]
    private Focus.Core.MigrationStatus _status;
    [ValidateLength(0, 200)]
    private string _externalId;
    private string _message;
    [ValueField]
    private Focus.Core.MigrationEmployerRecordType _recordType;
    private System.Nullable<System.DateTime> _processedBy;
    [ValidateLength(0, 200)]
    private string _parentExternalId;
    private bool _isUpdate;
    private bool _emailRequired;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SourceId entity attribute.</summary>
    public const string SourceIdField = "SourceId";
    /// <summary>Identifies the FocusId entity attribute.</summary>
    public const string FocusIdField = "FocusId";
    /// <summary>Identifies the Request entity attribute.</summary>
    public const string RequestField = "Request";
    /// <summary>Identifies the Status entity attribute.</summary>
    public const string StatusField = "Status";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";
    /// <summary>Identifies the Message entity attribute.</summary>
    public const string MessageField = "Message";
    /// <summary>Identifies the RecordType entity attribute.</summary>
    public const string RecordTypeField = "RecordType";
    /// <summary>Identifies the ProcessedBy entity attribute.</summary>
    public const string ProcessedByField = "ProcessedBy";
    /// <summary>Identifies the ParentExternalId entity attribute.</summary>
    public const string ParentExternalIdField = "ParentExternalId";
    /// <summary>Identifies the IsUpdate entity attribute.</summary>
    public const string IsUpdateField = "IsUpdate";
    /// <summary>Identifies the EmailRequired entity attribute.</summary>
    public const string EmailRequiredField = "EmailRequired";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SourceId
    {
      get { return Get(ref _sourceId, "SourceId"); }
      set { Set(ref _sourceId, value, "SourceId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long FocusId
    {
      get { return Get(ref _focusId, "FocusId"); }
      set { Set(ref _focusId, value, "FocusId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Request
    {
      get { return Get(ref _request, "Request"); }
      set { Set(ref _request, value, "Request"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.MigrationStatus Status
    {
      get { return Get(ref _status, "Status"); }
      set { Set(ref _status, value, "Status"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Message
    {
      get { return Get(ref _message, "Message"); }
      set { Set(ref _message, value, "Message"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.MigrationEmployerRecordType RecordType
    {
      get { return Get(ref _recordType, "RecordType"); }
      set { Set(ref _recordType, value, "RecordType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ProcessedBy
    {
      get { return Get(ref _processedBy, "ProcessedBy"); }
      set { Set(ref _processedBy, value, "ProcessedBy"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ParentExternalId
    {
      get { return Get(ref _parentExternalId, "ParentExternalId"); }
      set { Set(ref _parentExternalId, value, "ParentExternalId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsUpdate
    {
      get { return Get(ref _isUpdate, "IsUpdate"); }
      set { Set(ref _isUpdate, value, "IsUpdate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool EmailRequired
    {
      get { return Get(ref _emailRequired, "EmailRequired"); }
      set { Set(ref _emailRequired, value, "EmailRequired"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Migration.JobOrderRequest", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobOrderRequest : Entity<long>
  {
    #region Fields
  
    private long _sourceId;
    private long _focusId;
    private string _request;
    [ValueField]
    private Focus.Core.MigrationStatus _status;
    [ValidateLength(0, 200)]
    private string _externalId;
    [ValueField]
    private Focus.Core.MigrationJobOrderRecordType _recordType;
    private string _message;
    private System.Nullable<System.DateTime> _processedBy;
    [ValidateLength(0, 200)]
    private string _parentExternalId;
    private bool _isUpdate;
    private bool _emailRequired;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SourceId entity attribute.</summary>
    public const string SourceIdField = "SourceId";
    /// <summary>Identifies the FocusId entity attribute.</summary>
    public const string FocusIdField = "FocusId";
    /// <summary>Identifies the Request entity attribute.</summary>
    public const string RequestField = "Request";
    /// <summary>Identifies the Status entity attribute.</summary>
    public const string StatusField = "Status";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";
    /// <summary>Identifies the RecordType entity attribute.</summary>
    public const string RecordTypeField = "RecordType";
    /// <summary>Identifies the Message entity attribute.</summary>
    public const string MessageField = "Message";
    /// <summary>Identifies the ProcessedBy entity attribute.</summary>
    public const string ProcessedByField = "ProcessedBy";
    /// <summary>Identifies the ParentExternalId entity attribute.</summary>
    public const string ParentExternalIdField = "ParentExternalId";
    /// <summary>Identifies the IsUpdate entity attribute.</summary>
    public const string IsUpdateField = "IsUpdate";
    /// <summary>Identifies the EmailRequired entity attribute.</summary>
    public const string EmailRequiredField = "EmailRequired";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SourceId
    {
      get { return Get(ref _sourceId, "SourceId"); }
      set { Set(ref _sourceId, value, "SourceId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long FocusId
    {
      get { return Get(ref _focusId, "FocusId"); }
      set { Set(ref _focusId, value, "FocusId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Request
    {
      get { return Get(ref _request, "Request"); }
      set { Set(ref _request, value, "Request"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.MigrationStatus Status
    {
      get { return Get(ref _status, "Status"); }
      set { Set(ref _status, value, "Status"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.MigrationJobOrderRecordType RecordType
    {
      get { return Get(ref _recordType, "RecordType"); }
      set { Set(ref _recordType, value, "RecordType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Message
    {
      get { return Get(ref _message, "Message"); }
      set { Set(ref _message, value, "Message"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ProcessedBy
    {
      get { return Get(ref _processedBy, "ProcessedBy"); }
      set { Set(ref _processedBy, value, "ProcessedBy"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ParentExternalId
    {
      get { return Get(ref _parentExternalId, "ParentExternalId"); }
      set { Set(ref _parentExternalId, value, "ParentExternalId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsUpdate
    {
      get { return Get(ref _isUpdate, "IsUpdate"); }
      set { Set(ref _isUpdate, value, "IsUpdate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool EmailRequired
    {
      get { return Get(ref _emailRequired, "EmailRequired"); }
      set { Set(ref _emailRequired, value, "EmailRequired"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Migration.JobSeekerRequest", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeekerRequest : Entity<long>
  {
    #region Fields
  
    private long _sourceId;
    private long _focusId;
    [ValidatePresence]
    private string _request;
    [ValueField]
    private Focus.Core.MigrationStatus _status;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _externalId;
    private string _message;
    private System.Nullable<System.DateTime> _processedBy;
    [ValueField]
    private Focus.Core.MigrationJobSeekerRecordType _recordType;
    [ValidateLength(0, 200)]
    private string _parentExternalId;
    private bool _isUpdate;
    private bool _emailRequired;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SourceId entity attribute.</summary>
    public const string SourceIdField = "SourceId";
    /// <summary>Identifies the FocusId entity attribute.</summary>
    public const string FocusIdField = "FocusId";
    /// <summary>Identifies the Request entity attribute.</summary>
    public const string RequestField = "Request";
    /// <summary>Identifies the Status entity attribute.</summary>
    public const string StatusField = "Status";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";
    /// <summary>Identifies the Message entity attribute.</summary>
    public const string MessageField = "Message";
    /// <summary>Identifies the ProcessedBy entity attribute.</summary>
    public const string ProcessedByField = "ProcessedBy";
    /// <summary>Identifies the RecordType entity attribute.</summary>
    public const string RecordTypeField = "RecordType";
    /// <summary>Identifies the ParentExternalId entity attribute.</summary>
    public const string ParentExternalIdField = "ParentExternalId";
    /// <summary>Identifies the IsUpdate entity attribute.</summary>
    public const string IsUpdateField = "IsUpdate";
    /// <summary>Identifies the EmailRequired entity attribute.</summary>
    public const string EmailRequiredField = "EmailRequired";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SourceId
    {
      get { return Get(ref _sourceId, "SourceId"); }
      set { Set(ref _sourceId, value, "SourceId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long FocusId
    {
      get { return Get(ref _focusId, "FocusId"); }
      set { Set(ref _focusId, value, "FocusId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Request
    {
      get { return Get(ref _request, "Request"); }
      set { Set(ref _request, value, "Request"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.MigrationStatus Status
    {
      get { return Get(ref _status, "Status"); }
      set { Set(ref _status, value, "Status"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Message
    {
      get { return Get(ref _message, "Message"); }
      set { Set(ref _message, value, "Message"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ProcessedBy
    {
      get { return Get(ref _processedBy, "ProcessedBy"); }
      set { Set(ref _processedBy, value, "ProcessedBy"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.MigrationJobSeekerRecordType RecordType
    {
      get { return Get(ref _recordType, "RecordType"); }
      set { Set(ref _recordType, value, "RecordType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ParentExternalId
    {
      get { return Get(ref _parentExternalId, "ParentExternalId"); }
      set { Set(ref _parentExternalId, value, "ParentExternalId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsUpdate
    {
      get { return Get(ref _isUpdate, "IsUpdate"); }
      set { Set(ref _isUpdate, value, "IsUpdate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool EmailRequired
    {
      get { return Get(ref _emailRequired, "EmailRequired"); }
      set { Set(ref _emailRequired, value, "EmailRequired"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Migration.UserRequest", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class UserRequest : Entity<long>
  {
    #region Fields
  
    private long _sourceId;
    private long _focusId;
    [ValidatePresence]
    private string _request;
    [ValueField]
    private Focus.Core.MigrationStatus _status;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _externalId;
    private string _message;
    private System.Nullable<System.DateTime> _processedBy;
    [ValueField]
    private Focus.Core.MigrationAssistUserRecordType _recordType;
    [ValidateLength(0, 200)]
    private string _parentExternalId;
    private bool _isUpdate;
    private bool _emailRequired;
    [ValidateLength(0, 200)]
    private string _secondaryExternalId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SourceId entity attribute.</summary>
    public const string SourceIdField = "SourceId";
    /// <summary>Identifies the FocusId entity attribute.</summary>
    public const string FocusIdField = "FocusId";
    /// <summary>Identifies the Request entity attribute.</summary>
    public const string RequestField = "Request";
    /// <summary>Identifies the Status entity attribute.</summary>
    public const string StatusField = "Status";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";
    /// <summary>Identifies the Message entity attribute.</summary>
    public const string MessageField = "Message";
    /// <summary>Identifies the ProcessedBy entity attribute.</summary>
    public const string ProcessedByField = "ProcessedBy";
    /// <summary>Identifies the RecordType entity attribute.</summary>
    public const string RecordTypeField = "RecordType";
    /// <summary>Identifies the ParentExternalId entity attribute.</summary>
    public const string ParentExternalIdField = "ParentExternalId";
    /// <summary>Identifies the IsUpdate entity attribute.</summary>
    public const string IsUpdateField = "IsUpdate";
    /// <summary>Identifies the EmailRequired entity attribute.</summary>
    public const string EmailRequiredField = "EmailRequired";
    /// <summary>Identifies the SecondaryExternalId entity attribute.</summary>
    public const string SecondaryExternalIdField = "SecondaryExternalId";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SourceId
    {
      get { return Get(ref _sourceId, "SourceId"); }
      set { Set(ref _sourceId, value, "SourceId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long FocusId
    {
      get { return Get(ref _focusId, "FocusId"); }
      set { Set(ref _focusId, value, "FocusId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Request
    {
      get { return Get(ref _request, "Request"); }
      set { Set(ref _request, value, "Request"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.MigrationStatus Status
    {
      get { return Get(ref _status, "Status"); }
      set { Set(ref _status, value, "Status"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Message
    {
      get { return Get(ref _message, "Message"); }
      set { Set(ref _message, value, "Message"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ProcessedBy
    {
      get { return Get(ref _processedBy, "ProcessedBy"); }
      set { Set(ref _processedBy, value, "ProcessedBy"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.MigrationAssistUserRecordType RecordType
    {
      get { return Get(ref _recordType, "RecordType"); }
      set { Set(ref _recordType, value, "RecordType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ParentExternalId
    {
      get { return Get(ref _parentExternalId, "ParentExternalId"); }
      set { Set(ref _parentExternalId, value, "ParentExternalId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsUpdate
    {
      get { return Get(ref _isUpdate, "IsUpdate"); }
      set { Set(ref _isUpdate, value, "IsUpdate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool EmailRequired
    {
      get { return Get(ref _emailRequired, "EmailRequired"); }
      set { Set(ref _emailRequired, value, "EmailRequired"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SecondaryExternalId
    {
      get { return Get(ref _secondaryExternalId, "SecondaryExternalId"); }
      set { Set(ref _secondaryExternalId, value, "SecondaryExternalId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Migration.BatchRecord", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class BatchRecord : Entity<long>
  {
    #region Fields
  
    [ValueField]
    private Focus.Core.MigrationEntityType _entityType;
    private int _batchNumber;
    private long _firstId;
    private long _lastId;
    private int _recordType;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the EntityType entity attribute.</summary>
    public const string EntityTypeField = "EntityType";
    /// <summary>Identifies the BatchNumber entity attribute.</summary>
    public const string BatchNumberField = "BatchNumber";
    /// <summary>Identifies the FirstId entity attribute.</summary>
    public const string FirstIdField = "FirstId";
    /// <summary>Identifies the LastId entity attribute.</summary>
    public const string LastIdField = "LastId";
    /// <summary>Identifies the RecordType entity attribute.</summary>
    public const string RecordTypeField = "RecordType";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.MigrationEntityType EntityType
    {
      get { return Get(ref _entityType, "EntityType"); }
      set { Set(ref _entityType, value, "EntityType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int BatchNumber
    {
      get { return Get(ref _batchNumber, "BatchNumber"); }
      set { Set(ref _batchNumber, value, "BatchNumber"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long FirstId
    {
      get { return Get(ref _firstId, "FirstId"); }
      set { Set(ref _firstId, value, "FirstId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long LastId
    {
      get { return Get(ref _lastId, "LastId"); }
      set { Set(ref _lastId, value, "LastId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int RecordType
    {
      get { return Get(ref _recordType, "RecordType"); }
      set { Set(ref _recordType, value, "RecordType"); }
    }

    #endregion
  }
	


  /// <summary>
  /// Provides a strong-typed unit of work for working with the DataMigration model.
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  public partial class DataMigrationUnitOfWork : Mindscape.LightSpeed.UnitOfWork
  {

    public System.Linq.IQueryable<EmployerRequest> EmployerRequests
    {
      get { return this.Query<EmployerRequest>(); }
    }
    
    public System.Linq.IQueryable<JobOrderRequest> JobOrderRequests
    {
      get { return this.Query<JobOrderRequest>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerRequest> JobSeekerRequests
    {
      get { return this.Query<JobSeekerRequest>(); }
    }
    
    public System.Linq.IQueryable<UserRequest> UserRequests
    {
      get { return this.Query<UserRequest>(); }
    }
    
    public System.Linq.IQueryable<BatchRecord> BatchRecords
    {
      get { return this.Query<BatchRecord>(); }
    }
    
  }
}

#endregion


#region Data Transfer Objects - Classes

/*
namespace Focus.Core.DataTransferObjects.FocusMigration.Entities
{
  using System;
  using System.Runtime.Serialization;

    [Serializable]
    [DataContract(Name="EmployerRequest", Namespace = Constants.DataContractNamespace)]
    public class EmployerRequestDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long SourceId { get; set; }
      [DataMember]
      public long FocusId { get; set; }
      [DataMember]
      public string Request { get; set; }
      [DataMember]
      public MigrationStatus Status { get; set; }
      [DataMember]
      public string ExternalId { get; set; }
      [DataMember]
      public string Message { get; set; }
      [DataMember]
      public MigrationEmployerRecordType RecordType { get; set; }
      [DataMember]
      public DateTime? ProcessedBy { get; set; }
      [DataMember]
      public string ParentExternalId { get; set; }
      [DataMember]
      public bool IsUpdate { get; set; }
      [DataMember]
      public bool EmailRequired { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobOrderRequest", Namespace = Constants.DataContractNamespace)]
    public class JobOrderRequestDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long SourceId { get; set; }
      [DataMember]
      public long FocusId { get; set; }
      [DataMember]
      public string Request { get; set; }
      [DataMember]
      public MigrationStatus Status { get; set; }
      [DataMember]
      public string ExternalId { get; set; }
      [DataMember]
      public MigrationJobOrderRecordType RecordType { get; set; }
      [DataMember]
      public string Message { get; set; }
      [DataMember]
      public DateTime? ProcessedBy { get; set; }
      [DataMember]
      public string ParentExternalId { get; set; }
      [DataMember]
      public bool IsUpdate { get; set; }
      [DataMember]
      public bool EmailRequired { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeekerRequest", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerRequestDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long SourceId { get; set; }
      [DataMember]
      public long FocusId { get; set; }
      [DataMember]
      public string Request { get; set; }
      [DataMember]
      public MigrationStatus Status { get; set; }
      [DataMember]
      public string ExternalId { get; set; }
      [DataMember]
      public string Message { get; set; }
      [DataMember]
      public DateTime? ProcessedBy { get; set; }
      [DataMember]
      public MigrationJobSeekerRecordType RecordType { get; set; }
      [DataMember]
      public string ParentExternalId { get; set; }
      [DataMember]
      public bool IsUpdate { get; set; }
      [DataMember]
      public bool EmailRequired { get; set; }
    }

    [Serializable]
    [DataContract(Name="UserRequest", Namespace = Constants.DataContractNamespace)]
    public class UserRequestDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long SourceId { get; set; }
      [DataMember]
      public long FocusId { get; set; }
      [DataMember]
      public string Request { get; set; }
      [DataMember]
      public MigrationStatus Status { get; set; }
      [DataMember]
      public string ExternalId { get; set; }
      [DataMember]
      public string Message { get; set; }
      [DataMember]
      public DateTime? ProcessedBy { get; set; }
      [DataMember]
      public MigrationAssistUserRecordType RecordType { get; set; }
      [DataMember]
      public string ParentExternalId { get; set; }
      [DataMember]
      public bool IsUpdate { get; set; }
      [DataMember]
      public bool EmailRequired { get; set; }
      [DataMember]
      public string SecondaryExternalId { get; set; }
    }

    [Serializable]
    [DataContract(Name="BatchRecord", Namespace = Constants.DataContractNamespace)]
    public class BatchRecordDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public MigrationEntityType EntityType { get; set; }
      [DataMember]
      public int BatchNumber { get; set; }
      [DataMember]
      public long FirstId { get; set; }
      [DataMember]
      public long LastId { get; set; }
      [DataMember]
      public int RecordType { get; set; }
    }


}
*/

#endregion

#region Data Transfer Objects - Mappers

/*
namespace Focus.Services.DtoMappers
{
    using Focus.Core.DataTransferObjects.FocusMigration.Entities;
    using Focus.Data.Migration.Entities;

    public static class FocusMigration.EntitiesMappers
    {

      #region EmployerRequestDto Mappers

      public static EmployerRequestDto AsDto(this EmployerRequest entity)
      {
        var dto = new EmployerRequestDto
        {
          SourceId = entity.SourceId,
          FocusId = entity.FocusId,
          Request = entity.Request,
          Status = entity.Status,
          ExternalId = entity.ExternalId,
          Message = entity.Message,
          RecordType = entity.RecordType,
          ProcessedBy = entity.ProcessedBy,
          ParentExternalId = entity.ParentExternalId,
          IsUpdate = entity.IsUpdate,
          EmailRequired = entity.EmailRequired,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static EmployerRequest CopyTo(this EmployerRequestDto dto, EmployerRequest entity)
      {
        entity.SourceId = dto.SourceId;
        entity.FocusId = dto.FocusId;
        entity.Request = dto.Request;
        entity.Status = dto.Status;
        entity.ExternalId = dto.ExternalId;
        entity.Message = dto.Message;
        entity.RecordType = dto.RecordType;
        entity.ProcessedBy = dto.ProcessedBy;
        entity.ParentExternalId = dto.ParentExternalId;
        entity.IsUpdate = dto.IsUpdate;
        entity.EmailRequired = dto.EmailRequired;
        return entity;
      }

      public static EmployerRequest CopyTo(this EmployerRequestDto dto)
      {
        var entity = new EmployerRequest();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobOrderRequestDto Mappers

      public static JobOrderRequestDto AsDto(this JobOrderRequest entity)
      {
        var dto = new JobOrderRequestDto
        {
          SourceId = entity.SourceId,
          FocusId = entity.FocusId,
          Request = entity.Request,
          Status = entity.Status,
          ExternalId = entity.ExternalId,
          RecordType = entity.RecordType,
          Message = entity.Message,
          ProcessedBy = entity.ProcessedBy,
          ParentExternalId = entity.ParentExternalId,
          IsUpdate = entity.IsUpdate,
          EmailRequired = entity.EmailRequired,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobOrderRequest CopyTo(this JobOrderRequestDto dto, JobOrderRequest entity)
      {
        entity.SourceId = dto.SourceId;
        entity.FocusId = dto.FocusId;
        entity.Request = dto.Request;
        entity.Status = dto.Status;
        entity.ExternalId = dto.ExternalId;
        entity.RecordType = dto.RecordType;
        entity.Message = dto.Message;
        entity.ProcessedBy = dto.ProcessedBy;
        entity.ParentExternalId = dto.ParentExternalId;
        entity.IsUpdate = dto.IsUpdate;
        entity.EmailRequired = dto.EmailRequired;
        return entity;
      }

      public static JobOrderRequest CopyTo(this JobOrderRequestDto dto)
      {
        var entity = new JobOrderRequest();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeekerRequestDto Mappers

      public static JobSeekerRequestDto AsDto(this JobSeekerRequest entity)
      {
        var dto = new JobSeekerRequestDto
        {
          SourceId = entity.SourceId,
          FocusId = entity.FocusId,
          Request = entity.Request,
          Status = entity.Status,
          ExternalId = entity.ExternalId,
          Message = entity.Message,
          ProcessedBy = entity.ProcessedBy,
          RecordType = entity.RecordType,
          ParentExternalId = entity.ParentExternalId,
          IsUpdate = entity.IsUpdate,
          EmailRequired = entity.EmailRequired,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeekerRequest CopyTo(this JobSeekerRequestDto dto, JobSeekerRequest entity)
      {
        entity.SourceId = dto.SourceId;
        entity.FocusId = dto.FocusId;
        entity.Request = dto.Request;
        entity.Status = dto.Status;
        entity.ExternalId = dto.ExternalId;
        entity.Message = dto.Message;
        entity.ProcessedBy = dto.ProcessedBy;
        entity.RecordType = dto.RecordType;
        entity.ParentExternalId = dto.ParentExternalId;
        entity.IsUpdate = dto.IsUpdate;
        entity.EmailRequired = dto.EmailRequired;
        return entity;
      }

      public static JobSeekerRequest CopyTo(this JobSeekerRequestDto dto)
      {
        var entity = new JobSeekerRequest();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region UserRequestDto Mappers

      public static UserRequestDto AsDto(this UserRequest entity)
      {
        var dto = new UserRequestDto
        {
          SourceId = entity.SourceId,
          FocusId = entity.FocusId,
          Request = entity.Request,
          Status = entity.Status,
          ExternalId = entity.ExternalId,
          Message = entity.Message,
          ProcessedBy = entity.ProcessedBy,
          RecordType = entity.RecordType,
          ParentExternalId = entity.ParentExternalId,
          IsUpdate = entity.IsUpdate,
          EmailRequired = entity.EmailRequired,
          SecondaryExternalId = entity.SecondaryExternalId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static UserRequest CopyTo(this UserRequestDto dto, UserRequest entity)
      {
        entity.SourceId = dto.SourceId;
        entity.FocusId = dto.FocusId;
        entity.Request = dto.Request;
        entity.Status = dto.Status;
        entity.ExternalId = dto.ExternalId;
        entity.Message = dto.Message;
        entity.ProcessedBy = dto.ProcessedBy;
        entity.RecordType = dto.RecordType;
        entity.ParentExternalId = dto.ParentExternalId;
        entity.IsUpdate = dto.IsUpdate;
        entity.EmailRequired = dto.EmailRequired;
        entity.SecondaryExternalId = dto.SecondaryExternalId;
        return entity;
      }

      public static UserRequest CopyTo(this UserRequestDto dto)
      {
        var entity = new UserRequest();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region BatchRecordDto Mappers

      public static BatchRecordDto AsDto(this BatchRecord entity)
      {
        var dto = new BatchRecordDto
        {
          EntityType = entity.EntityType,
          BatchNumber = entity.BatchNumber,
          FirstId = entity.FirstId,
          LastId = entity.LastId,
          RecordType = entity.RecordType,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static BatchRecord CopyTo(this BatchRecordDto dto, BatchRecord entity)
      {
        entity.EntityType = dto.EntityType;
        entity.BatchNumber = dto.BatchNumber;
        entity.FirstId = dto.FirstId;
        entity.LastId = dto.LastId;
        entity.RecordType = dto.RecordType;
        return entity;
      }

      public static BatchRecord CopyTo(this BatchRecordDto dto)
      {
        var entity = new BatchRecord();
        return dto.CopyTo(entity);
      }	

      #endregion

    }
}
*/

#endregion
