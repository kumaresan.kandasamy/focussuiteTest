using System;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Validation;
using Mindscape.LightSpeed.Linq;

#region Entities

namespace Focus.Data.Library.Entities
{
  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.Localisation", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class Localisation : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 5)]
    private string _culture;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Culture entity attribute.</summary>
    public const string CultureField = "Culture";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Localisation")]
    private readonly EntityCollection<OnetLocalisationItem> _onetLocalisationItems = new EntityCollection<OnetLocalisationItem>();
    [ReverseAssociation("Localisation")]
    private readonly EntityCollection<JobTaskLocalisationItem> _jobTaskLocalisationItems = new EntityCollection<JobTaskLocalisationItem>();
    [ReverseAssociation("Localisation")]
    private readonly EntityCollection<LocalisationItem> _localisationItems = new EntityCollection<LocalisationItem>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<OnetLocalisationItem> OnetLocalisationItems
    {
      get { return Get(_onetLocalisationItems); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobTaskLocalisationItem> JobTaskLocalisationItems
    {
      get { return Get(_jobTaskLocalisationItems); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<LocalisationItem> LocalisationItems
    {
      get { return Get(_localisationItems); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Culture
    {
      get { return Get(ref _culture, "Culture"); }
      set { Set(ref _culture, value, "Culture"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.State", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class State : Entity<long>
  {
    #region Fields
  
    private long _countryId;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _name;
    [ValidatePresence]
    [ValidateLength(0, 2)]
    private string _code;
    private int _sortOrder;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the CountryId entity attribute.</summary>
    public const string CountryIdField = "CountryId";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the Code entity attribute.</summary>
    public const string CodeField = "Code";
    /// <summary>Identifies the SortOrder entity attribute.</summary>
    public const string SortOrderField = "SortOrder";


    #endregion
    
    #region Relationships

    [ReverseAssociation("State")]
    private readonly EntityCollection<StateArea> _stateAreasByState = new EntityCollection<StateArea>();

    private ThroughAssociation<StateArea, Area> _areas;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<StateArea> StateAreasByState
    {
      get { return Get(_stateAreasByState); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<StateArea, Area> Areas
    {
      get
      {
        if (_areas == null)
        {
          _areas = new ThroughAssociation<StateArea, Area>(_stateAreasByState);
        }
        return Get(_areas);
      }
    }
    

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long CountryId
    {
      get { return Get(ref _countryId, "CountryId"); }
      set { Set(ref _countryId, value, "CountryId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Code
    {
      get { return Get(ref _code, "Code"); }
      set { Set(ref _code, value, "Code"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int SortOrder
    {
      get { return Get(ref _sortOrder, "SortOrder"); }
      set { Set(ref _sortOrder, value, "SortOrder"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.Area", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class Area : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _name;
    private int _sortOrder;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the SortOrder entity attribute.</summary>
    public const string SortOrderField = "SortOrder";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Area")]
    private readonly EntityCollection<StateArea> _stateAreasByArea = new EntityCollection<StateArea>();

    private ThroughAssociation<StateArea, State> _states;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<StateArea> StateAreasByArea
    {
      get { return Get(_stateAreasByArea); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<StateArea, State> States
    {
      get
      {
        if (_states == null)
        {
          _states = new ThroughAssociation<StateArea, State>(_stateAreasByArea);
        }
        return Get(_states);
      }
    }
    

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int SortOrder
    {
      get { return Get(ref _sortOrder, "SortOrder"); }
      set { Set(ref _sortOrder, value, "SortOrder"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobReport", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class JobReport : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private int _hiringDemand;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _salaryTrend;
    private decimal _salaryTrendPercentile;
    private decimal _salaryTrendAverage;
    private long _localisationId;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _growthTrend;
    private decimal _growthPercentile;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _hiringTrend;
    private decimal _hiringTrendPercentile;
    private int _salaryTrendMin;
    private int _salaryTrendMax;
    [ValidateLength(0, 10)]
    private string _salaryTrendRealtime;
    private System.Nullable<int> _salaryTrendRealtimeAverage;
    private System.Nullable<int> _salaryTrendRealtimeMin;
    private System.Nullable<int> _salaryTrendRealtimeMax;
    [ValidatePresence]
    [ValidateLength(0, 4)]
    private string _hiringTrendSubLevel;
    private long _jobId;
    private long _stateAreaId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the HiringDemand entity attribute.</summary>
    public const string HiringDemandField = "HiringDemand";
    /// <summary>Identifies the SalaryTrend entity attribute.</summary>
    public const string SalaryTrendField = "SalaryTrend";
    /// <summary>Identifies the SalaryTrendPercentile entity attribute.</summary>
    public const string SalaryTrendPercentileField = "SalaryTrendPercentile";
    /// <summary>Identifies the SalaryTrendAverage entity attribute.</summary>
    public const string SalaryTrendAverageField = "SalaryTrendAverage";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";
    /// <summary>Identifies the GrowthTrend entity attribute.</summary>
    public const string GrowthTrendField = "GrowthTrend";
    /// <summary>Identifies the GrowthPercentile entity attribute.</summary>
    public const string GrowthPercentileField = "GrowthPercentile";
    /// <summary>Identifies the HiringTrend entity attribute.</summary>
    public const string HiringTrendField = "HiringTrend";
    /// <summary>Identifies the HiringTrendPercentile entity attribute.</summary>
    public const string HiringTrendPercentileField = "HiringTrendPercentile";
    /// <summary>Identifies the SalaryTrendMin entity attribute.</summary>
    public const string SalaryTrendMinField = "SalaryTrendMin";
    /// <summary>Identifies the SalaryTrendMax entity attribute.</summary>
    public const string SalaryTrendMaxField = "SalaryTrendMax";
    /// <summary>Identifies the SalaryTrendRealtime entity attribute.</summary>
    public const string SalaryTrendRealtimeField = "SalaryTrendRealtime";
    /// <summary>Identifies the SalaryTrendRealtimeAverage entity attribute.</summary>
    public const string SalaryTrendRealtimeAverageField = "SalaryTrendRealtimeAverage";
    /// <summary>Identifies the SalaryTrendRealtimeMin entity attribute.</summary>
    public const string SalaryTrendRealtimeMinField = "SalaryTrendRealtimeMin";
    /// <summary>Identifies the SalaryTrendRealtimeMax entity attribute.</summary>
    public const string SalaryTrendRealtimeMaxField = "SalaryTrendRealtimeMax";
    /// <summary>Identifies the HiringTrendSubLevel entity attribute.</summary>
    public const string HiringTrendSubLevelField = "HiringTrendSubLevel";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the StateAreaId entity attribute.</summary>
    public const string StateAreaIdField = "StateAreaId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobReports")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();
    [ReverseAssociation("JobReports")]
    private readonly EntityHolder<StateArea> _stateArea = new EntityHolder<StateArea>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public StateArea StateArea
    {
      get { return Get(_stateArea); }
      set { Set(_stateArea, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int HiringDemand
    {
      get { return Get(ref _hiringDemand, "HiringDemand"); }
      set { Set(ref _hiringDemand, value, "HiringDemand"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SalaryTrend
    {
      get { return Get(ref _salaryTrend, "SalaryTrend"); }
      set { Set(ref _salaryTrend, value, "SalaryTrend"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public decimal SalaryTrendPercentile
    {
      get { return Get(ref _salaryTrendPercentile, "SalaryTrendPercentile"); }
      set { Set(ref _salaryTrendPercentile, value, "SalaryTrendPercentile"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public decimal SalaryTrendAverage
    {
      get { return Get(ref _salaryTrendAverage, "SalaryTrendAverage"); }
      set { Set(ref _salaryTrendAverage, value, "SalaryTrendAverage"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string GrowthTrend
    {
      get { return Get(ref _growthTrend, "GrowthTrend"); }
      set { Set(ref _growthTrend, value, "GrowthTrend"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public decimal GrowthPercentile
    {
      get { return Get(ref _growthPercentile, "GrowthPercentile"); }
      set { Set(ref _growthPercentile, value, "GrowthPercentile"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string HiringTrend
    {
      get { return Get(ref _hiringTrend, "HiringTrend"); }
      set { Set(ref _hiringTrend, value, "HiringTrend"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public decimal HiringTrendPercentile
    {
      get { return Get(ref _hiringTrendPercentile, "HiringTrendPercentile"); }
      set { Set(ref _hiringTrendPercentile, value, "HiringTrendPercentile"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int SalaryTrendMin
    {
      get { return Get(ref _salaryTrendMin, "SalaryTrendMin"); }
      set { Set(ref _salaryTrendMin, value, "SalaryTrendMin"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int SalaryTrendMax
    {
      get { return Get(ref _salaryTrendMax, "SalaryTrendMax"); }
      set { Set(ref _salaryTrendMax, value, "SalaryTrendMax"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SalaryTrendRealtime
    {
      get { return Get(ref _salaryTrendRealtime, "SalaryTrendRealtime"); }
      set { Set(ref _salaryTrendRealtime, value, "SalaryTrendRealtime"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> SalaryTrendRealtimeAverage
    {
      get { return Get(ref _salaryTrendRealtimeAverage, "SalaryTrendRealtimeAverage"); }
      set { Set(ref _salaryTrendRealtimeAverage, value, "SalaryTrendRealtimeAverage"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> SalaryTrendRealtimeMin
    {
      get { return Get(ref _salaryTrendRealtimeMin, "SalaryTrendRealtimeMin"); }
      set { Set(ref _salaryTrendRealtimeMin, value, "SalaryTrendRealtimeMin"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> SalaryTrendRealtimeMax
    {
      get { return Get(ref _salaryTrendRealtimeMax, "SalaryTrendRealtimeMax"); }
      set { Set(ref _salaryTrendRealtimeMax, value, "SalaryTrendRealtimeMax"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string HiringTrendSubLevel
    {
      get { return Get(ref _hiringTrendSubLevel, "HiringTrendSubLevel"); }
      set { Set(ref _hiringTrendSubLevel, value, "HiringTrendSubLevel"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="StateArea" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StateAreaId
    {
      get { return Get(ref _stateAreaId, "StateAreaId"); }
      set { Set(ref _stateAreaId, value, "StateAreaId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.Job", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class Job : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _name;
    [ValidatePresence]
    [ValidateLength(0, 20)]
    private string _rOnet;
    private long _localisationId;
    [ValidateLength(0, 1000)]
    private string _degreeIntroStatement;
    private string _description;
    [ValueField]
    private System.Nullable<Focus.Core.StarterJobLevel> _starterJob;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the ROnet entity attribute.</summary>
    public const string ROnetField = "ROnet";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";
    /// <summary>Identifies the DegreeIntroStatement entity attribute.</summary>
    public const string DegreeIntroStatementField = "DegreeIntroStatement";
    /// <summary>Identifies the Description entity attribute.</summary>
    public const string DescriptionField = "Description";
    /// <summary>Identifies the StarterJob entity attribute.</summary>
    public const string StarterJobField = "StarterJob";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobReport> _jobReports = new EntityCollection<JobReport>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<EmployerJob> _employerJobs = new EntityCollection<EmployerJob>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobCareerArea> _jobCareerAreasByJob = new EntityCollection<JobCareerArea>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobSkill> _jobSkillsByJob = new EntityCollection<JobSkill>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobRelatedJob> _jobRelatedJobs = new EntityCollection<JobRelatedJob>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobDegreeLevel> _jobDegreeLevels = new EntityCollection<JobDegreeLevel>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobJobTitle> _jobJobTitles = new EntityCollection<JobJobTitle>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobEducationRequirement> _jobEducationRequirements = new EntityCollection<JobEducationRequirement>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobExperienceLevel> _jobExperienceLevels = new EntityCollection<JobExperienceLevel>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobCertification> _jobCertifications = new EntityCollection<JobCertification>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobDegreeEducationLevel> _jobDegreeEducationLevels = new EntityCollection<JobDegreeEducationLevel>();
    [ReverseAssociation("CurrentJob")]
    private readonly EntityCollection<JobCareerPathTo> _jobCareerPathTosByCurrentJob = new EntityCollection<JobCareerPathTo>();
    [ReverseAssociation("Next1Job")]
    private readonly EntityCollection<JobCareerPathTo> _jobCareerPathTosByNext1Job = new EntityCollection<JobCareerPathTo>();
    [ReverseAssociation("Next2Job")]
    private readonly EntityCollection<JobCareerPathTo> _jobCareerPathTosByNext2Job = new EntityCollection<JobCareerPathTo>();
    [ReverseAssociation("FromJob")]
    private readonly EntityCollection<JobCareerPathFrom> _jobCareerPathFromsByFromJob = new EntityCollection<JobCareerPathFrom>();
    [ReverseAssociation("ToJob")]
    private readonly EntityCollection<JobCareerPathFrom> _jobCareerPathFromsByToJob = new EntityCollection<JobCareerPathFrom>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobEmployerSkill> _jobEmployerSkills = new EntityCollection<JobEmployerSkill>();

    private ThroughAssociation<JobCareerArea, CareerArea> _careerAreas;
    private ThroughAssociation<JobDegreeEducationLevel, DegreeEducationLevel> _degreeEducationLevels;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobReport> JobReports
    {
      get { return Get(_jobReports); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<EmployerJob> EmployerJobs
    {
      get { return Get(_employerJobs); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobCareerArea> JobCareerAreasByJob
    {
      get { return Get(_jobCareerAreasByJob); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSkill> JobSkillsByJob
    {
      get { return Get(_jobSkillsByJob); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobRelatedJob> JobRelatedJobs
    {
      get { return Get(_jobRelatedJobs); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobDegreeLevel> JobDegreeLevels
    {
      get { return Get(_jobDegreeLevels); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobJobTitle> JobJobTitles
    {
      get { return Get(_jobJobTitles); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobEducationRequirement> JobEducationRequirements
    {
      get { return Get(_jobEducationRequirements); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobExperienceLevel> JobExperienceLevels
    {
      get { return Get(_jobExperienceLevels); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobCertification> JobCertifications
    {
      get { return Get(_jobCertifications); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobDegreeEducationLevel> JobDegreeEducationLevels
    {
      get { return Get(_jobDegreeEducationLevels); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobCareerPathTo> JobCareerPathTosByCurrentJob
    {
      get { return Get(_jobCareerPathTosByCurrentJob); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobCareerPathTo> JobCareerPathTosByNext1Job
    {
      get { return Get(_jobCareerPathTosByNext1Job); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobCareerPathTo> JobCareerPathTosByNext2Job
    {
      get { return Get(_jobCareerPathTosByNext2Job); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobCareerPathFrom> JobCareerPathFromsByFromJob
    {
      get { return Get(_jobCareerPathFromsByFromJob); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobCareerPathFrom> JobCareerPathFromsByToJob
    {
      get { return Get(_jobCareerPathFromsByToJob); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobEmployerSkill> JobEmployerSkills
    {
      get { return Get(_jobEmployerSkills); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<JobCareerArea, CareerArea> CareerAreas
    {
      get
      {
        if (_careerAreas == null)
        {
          _careerAreas = new ThroughAssociation<JobCareerArea, CareerArea>(_jobCareerAreasByJob);
        }
        return Get(_careerAreas);
      }
    }
    
    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<JobDegreeEducationLevel, DegreeEducationLevel> DegreeEducationLevels
    {
      get
      {
        if (_degreeEducationLevels == null)
        {
          _degreeEducationLevels = new ThroughAssociation<JobDegreeEducationLevel, DegreeEducationLevel>(_jobDegreeEducationLevels);
        }
        return Get(_degreeEducationLevels);
      }
    }
    

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ROnet
    {
      get { return Get(ref _rOnet, "ROnet"); }
      set { Set(ref _rOnet, value, "ROnet"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string DegreeIntroStatement
    {
      get { return Get(ref _degreeIntroStatement, "DegreeIntroStatement"); }
      set { Set(ref _degreeIntroStatement, value, "DegreeIntroStatement"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Description
    {
      get { return Get(ref _description, "Description"); }
      set { Set(ref _description, value, "Description"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.StarterJobLevel> StarterJob
    {
      get { return Get(ref _starterJob, "StarterJob"); }
      set { Set(ref _starterJob, value, "StarterJob"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.StudyPlace", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class StudyPlace : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _name;
    private long _stateAreaId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the StateAreaId entity attribute.</summary>
    public const string StateAreaIdField = "StateAreaId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("StudyPlace")]
    private readonly EntityCollection<StudyPlaceDegreeEducationLevel> _studyPlaceDegreeEducationLevels = new EntityCollection<StudyPlaceDegreeEducationLevel>();
    [ReverseAssociation("StudyPlaces")]
    private readonly EntityHolder<StateArea> _stateArea = new EntityHolder<StateArea>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<StudyPlaceDegreeEducationLevel> StudyPlaceDegreeEducationLevels
    {
      get { return Get(_studyPlaceDegreeEducationLevels); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public StateArea StateArea
    {
      get { return Get(_stateArea); }
      set { Set(_stateArea, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="StateArea" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StateAreaId
    {
      get { return Get(ref _stateAreaId, "StateAreaId"); }
      set { Set(ref _stateAreaId, value, "StateAreaId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.Employer", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class Employer : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _name;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Employer")]
    private readonly EntityCollection<EmployerJob> _employerJobs = new EntityCollection<EmployerJob>();
    [ReverseAssociation("Employer")]
    private readonly EntityCollection<EmployerSkill> _employerSkillsByEmployer = new EntityCollection<EmployerSkill>();
    [ReverseAssociation("Employer")]
    private readonly EntityCollection<InternshipEmployer> _internshipEmployers = new EntityCollection<InternshipEmployer>();
    [ReverseAssociation("Employer")]
    private readonly EntityCollection<JobEmployerSkill> _jobEmployerSkills = new EntityCollection<JobEmployerSkill>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<EmployerJob> EmployerJobs
    {
      get { return Get(_employerJobs); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<EmployerSkill> EmployerSkillsByEmployer
    {
      get { return Get(_employerSkillsByEmployer); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<InternshipEmployer> InternshipEmployers
    {
      get { return Get(_internshipEmployers); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobEmployerSkill> JobEmployerSkills
    {
      get { return Get(_jobEmployerSkills); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.EmployerJob", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class EmployerJob : Entity<long>
  {
    #region Fields
  
    private int _positions;
    private long _jobId;
    private long _employerId;
    private long _stateAreaId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Positions entity attribute.</summary>
    public const string PositionsField = "Positions";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the StateAreaId entity attribute.</summary>
    public const string StateAreaIdField = "StateAreaId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("EmployerJobs")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();
    [ReverseAssociation("EmployerJobs")]
    private readonly EntityHolder<Employer> _employer = new EntityHolder<Employer>();
    [ReverseAssociation("EmployerJobs")]
    private readonly EntityHolder<StateArea> _stateArea = new EntityHolder<StateArea>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Employer Employer
    {
      get { return Get(_employer); }
      set { Set(_employer, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public StateArea StateArea
    {
      get { return Get(_stateArea); }
      set { Set(_stateArea, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Positions
    {
      get { return Get(ref _positions, "Positions"); }
      set { Set(ref _positions, value, "Positions"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Employer" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="StateArea" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StateAreaId
    {
      get { return Get(ref _stateAreaId, "StateAreaId"); }
      set { Set(ref _stateAreaId, value, "StateAreaId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.EmployerJobView")]
  public partial class EmployerJobView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _employerName;
    private long _jobId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _jobName;
    private int _positions;
    private long _employerId;
    private long _stateAreaId;
    [ValidatePresence]
    [ValidateLength(0, 20)]
    private string _rOnet;
    [ValueField]
    private System.Nullable<Focus.Core.StarterJobLevel> _starterJob;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the EmployerName entity attribute.</summary>
    public const string EmployerNameField = "EmployerName";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the JobName entity attribute.</summary>
    public const string JobNameField = "JobName";
    /// <summary>Identifies the Positions entity attribute.</summary>
    public const string PositionsField = "Positions";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the StateAreaId entity attribute.</summary>
    public const string StateAreaIdField = "StateAreaId";
    /// <summary>Identifies the ROnet entity attribute.</summary>
    public const string ROnetField = "ROnet";
    /// <summary>Identifies the StarterJob entity attribute.</summary>
    public const string StarterJobField = "StarterJob";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerName
    {
      get { return Get(ref _employerName, "EmployerName"); }
      set { Set(ref _employerName, value, "EmployerName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string JobName
    {
      get { return Get(ref _jobName, "JobName"); }
      set { Set(ref _jobName, value, "JobName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Positions
    {
      get { return Get(ref _positions, "Positions"); }
      set { Set(ref _positions, value, "Positions"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StateAreaId
    {
      get { return Get(ref _stateAreaId, "StateAreaId"); }
      set { Set(ref _stateAreaId, value, "StateAreaId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ROnet
    {
      get { return Get(ref _rOnet, "ROnet"); }
      set { Set(ref _rOnet, value, "ROnet"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.StarterJobLevel> StarterJob
    {
      get { return Get(ref _starterJob, "StarterJob"); }
      set { Set(ref _starterJob, value, "StarterJob"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobSkillView")]
  public partial class JobSkillView : Entity<long>
  {
    #region Fields
  
    private long _jobId;
    [ValueField]
    private Focus.Core.JobSkillTypes _jobSkillType;
    [ValueField]
    private Focus.Core.SkillTypes _skillType;
    private long _skillId;
    private decimal _demandPercentile;
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _skillName;
    private int _rank;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the JobSkillType entity attribute.</summary>
    public const string JobSkillTypeField = "JobSkillType";
    /// <summary>Identifies the SkillType entity attribute.</summary>
    public const string SkillTypeField = "SkillType";
    /// <summary>Identifies the SkillId entity attribute.</summary>
    public const string SkillIdField = "SkillId";
    /// <summary>Identifies the DemandPercentile entity attribute.</summary>
    public const string DemandPercentileField = "DemandPercentile";
    /// <summary>Identifies the SkillName entity attribute.</summary>
    public const string SkillNameField = "SkillName";
    /// <summary>Identifies the Rank entity attribute.</summary>
    public const string RankField = "Rank";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.JobSkillTypes JobSkillType
    {
      get { return Get(ref _jobSkillType, "JobSkillType"); }
      set { Set(ref _jobSkillType, value, "JobSkillType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.SkillTypes SkillType
    {
      get { return Get(ref _skillType, "SkillType"); }
      set { Set(ref _skillType, value, "SkillType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillId
    {
      get { return Get(ref _skillId, "SkillId"); }
      set { Set(ref _skillId, value, "SkillId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public decimal DemandPercentile
    {
      get { return Get(ref _demandPercentile, "DemandPercentile"); }
      set { Set(ref _demandPercentile, value, "DemandPercentile"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SkillName
    {
      get { return Get(ref _skillName, "SkillName"); }
      set { Set(ref _skillName, value, "SkillName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Rank
    {
      get { return Get(ref _rank, "Rank"); }
      set { Set(ref _rank, value, "Rank"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.SkillCategory", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class SkillCategory : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 255)]
    private string _name;
    private long _localisationId;
    private System.Nullable<long> _parentId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";
    /// <summary>Identifies the ParentId entity attribute.</summary>
    public const string ParentIdField = "ParentId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Parent")]
    private readonly EntityCollection<SkillCategory> _skillCategories = new EntityCollection<SkillCategory>();
    [ReverseAssociation("SkillCategory")]
    private readonly EntityCollection<SkillCategorySkill> _skillCategorySkills = new EntityCollection<SkillCategorySkill>();
    [ReverseAssociation("SkillCategories")]
    private readonly EntityHolder<SkillCategory> _parent = new EntityHolder<SkillCategory>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<SkillCategory> SkillCategories
    {
      get { return Get(_skillCategories); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<SkillCategorySkill> SkillCategorySkills
    {
      get { return Get(_skillCategorySkills); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public SkillCategory Parent
    {
      get { return Get(_parent); }
      set { Set(_parent, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Parent" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> ParentId
    {
      get { return Get(ref _parentId, "ParentId"); }
      set { Set(ref _parentId, value, "ParentId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.SkillCategorySkill", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class SkillCategorySkill : Entity<long>
  {
    #region Fields
  
    private long _skillCategoryId;
    private long _skillId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SkillCategoryId entity attribute.</summary>
    public const string SkillCategoryIdField = "SkillCategoryId";
    /// <summary>Identifies the SkillId entity attribute.</summary>
    public const string SkillIdField = "SkillId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("SkillCategorySkills")]
    private readonly EntityHolder<SkillCategory> _skillCategory = new EntityHolder<SkillCategory>();
    [ReverseAssociation("SkillCategorySkills")]
    private readonly EntityHolder<Skill> _skill = new EntityHolder<Skill>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public SkillCategory SkillCategory
    {
      get { return Get(_skillCategory); }
      set { Set(_skillCategory, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Skill Skill
    {
      get { return Get(_skill); }
      set { Set(_skill, value); }
    }


    /// <summary>Gets or sets the ID for the <see cref="SkillCategory" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillCategoryId
    {
      get { return Get(ref _skillCategoryId, "SkillCategoryId"); }
      set { Set(ref _skillCategoryId, value, "SkillCategoryId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Skill" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillId
    {
      get { return Get(ref _skillId, "SkillId"); }
      set { Set(ref _skillId, value, "SkillId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.CareerArea", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class CareerArea : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _name;
    private long _localisationId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("CareerArea")]
    private readonly EntityCollection<JobCareerArea> _jobCareerAreasByCareerArea = new EntityCollection<JobCareerArea>();

    private ThroughAssociation<JobCareerArea, Job> _jobs;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobCareerArea> JobCareerAreasByCareerArea
    {
      get { return Get(_jobCareerAreasByCareerArea); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<JobCareerArea, Job> Jobs
    {
      get
      {
        if (_jobs == null)
        {
          _jobs = new ThroughAssociation<JobCareerArea, Job>(_jobCareerAreasByCareerArea);
        }
        return Get(_jobs);
      }
    }
    

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobCareerArea", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class JobCareerArea : Entity<long>
  {
    #region Fields
  
    private long _jobId;
    private long _careerAreaId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the CareerAreaId entity attribute.</summary>
    public const string CareerAreaIdField = "CareerAreaId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobCareerAreasByJob")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();
    [ReverseAssociation("JobCareerAreasByCareerArea")]
    private readonly EntityHolder<CareerArea> _careerArea = new EntityHolder<CareerArea>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public CareerArea CareerArea
    {
      get { return Get(_careerArea); }
      set { Set(_careerArea, value); }
    }


    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="CareerArea" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long CareerAreaId
    {
      get { return Get(ref _careerAreaId, "CareerAreaId"); }
      set { Set(ref _careerAreaId, value, "CareerAreaId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobSkill", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class JobSkill : Entity<long>
  {
    #region Fields
  
    [ValueField]
    private Focus.Core.JobSkillTypes _jobSkillType;
    private decimal _demandPercentile;
    private int _rank;
    private long _jobId;
    private long _skillId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobSkillType entity attribute.</summary>
    public const string JobSkillTypeField = "JobSkillType";
    /// <summary>Identifies the DemandPercentile entity attribute.</summary>
    public const string DemandPercentileField = "DemandPercentile";
    /// <summary>Identifies the Rank entity attribute.</summary>
    public const string RankField = "Rank";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the SkillId entity attribute.</summary>
    public const string SkillIdField = "SkillId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSkillsByJob")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();
    [ReverseAssociation("JobSkills")]
    private readonly EntityHolder<Skill> _skill = new EntityHolder<Skill>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Skill Skill
    {
      get { return Get(_skill); }
      set { Set(_skill, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.JobSkillTypes JobSkillType
    {
      get { return Get(ref _jobSkillType, "JobSkillType"); }
      set { Set(ref _jobSkillType, value, "JobSkillType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public decimal DemandPercentile
    {
      get { return Get(ref _demandPercentile, "DemandPercentile"); }
      set { Set(ref _demandPercentile, value, "DemandPercentile"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Rank
    {
      get { return Get(ref _rank, "Rank"); }
      set { Set(ref _rank, value, "Rank"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Skill" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillId
    {
      get { return Get(ref _skillId, "SkillId"); }
      set { Set(ref _skillId, value, "SkillId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.Skill", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class Skill : Entity<long>
  {
    #region Fields
  
    [ValueField]
    private Focus.Core.SkillTypes _skillType;
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _name;
    private long _localisationId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SkillType entity attribute.</summary>
    public const string SkillTypeField = "SkillType";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Skill")]
    private readonly EntityCollection<JobSkill> _jobSkills = new EntityCollection<JobSkill>();
    [ReverseAssociation("Skill")]
    private readonly EntityCollection<SkillCategorySkill> _skillCategorySkills = new EntityCollection<SkillCategorySkill>();
    [ReverseAssociation("Skill")]
    private readonly EntityCollection<EmployerSkill> _employerSkillsBySkill = new EntityCollection<EmployerSkill>();
    [ReverseAssociation("Skill")]
    private readonly EntityCollection<SkillReport> _skillReportsBySkill = new EntityCollection<SkillReport>();
    [ReverseAssociation("Skill")]
    private readonly EntityCollection<InternshipSkill> _internshipSkills = new EntityCollection<InternshipSkill>();
    [ReverseAssociation("Skill")]
    private readonly EntityCollection<JobEmployerSkill> _jobEmployerSkills = new EntityCollection<JobEmployerSkill>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSkill> JobSkills
    {
      get { return Get(_jobSkills); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<SkillCategorySkill> SkillCategorySkills
    {
      get { return Get(_skillCategorySkills); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<EmployerSkill> EmployerSkillsBySkill
    {
      get { return Get(_employerSkillsBySkill); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<SkillReport> SkillReportsBySkill
    {
      get { return Get(_skillReportsBySkill); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<InternshipSkill> InternshipSkills
    {
      get { return Get(_internshipSkills); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobEmployerSkill> JobEmployerSkills
    {
      get { return Get(_jobEmployerSkills); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.SkillTypes SkillType
    {
      get { return Get(ref _skillType, "SkillType"); }
      set { Set(ref _skillType, value, "SkillType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobRelatedJob", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class JobRelatedJob : Entity<long>
  {
    #region Fields
  
    private long _relatedJobId;
    private int _priority;
    private long _jobId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the RelatedJobId entity attribute.</summary>
    public const string RelatedJobIdField = "RelatedJobId";
    /// <summary>Identifies the Priority entity attribute.</summary>
    public const string PriorityField = "Priority";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobRelatedJobs")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long RelatedJobId
    {
      get { return Get(ref _relatedJobId, "RelatedJobId"); }
      set { Set(ref _relatedJobId, value, "RelatedJobId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Priority
    {
      get { return Get(ref _priority, "Priority"); }
      set { Set(ref _priority, value, "Priority"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.StateArea", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class StateArea : Entity<long>
  {
    #region Fields
  
    private string _areaCode;
    private bool _display;
    private bool _isDefault;
    private long _stateId;
    private long _areaId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the AreaCode entity attribute.</summary>
    public const string AreaCodeField = "AreaCode";
    /// <summary>Identifies the Display entity attribute.</summary>
    public const string DisplayField = "Display";
    /// <summary>Identifies the IsDefault entity attribute.</summary>
    public const string IsDefaultField = "IsDefault";
    /// <summary>Identifies the StateId entity attribute.</summary>
    public const string StateIdField = "StateId";
    /// <summary>Identifies the AreaId entity attribute.</summary>
    public const string AreaIdField = "AreaId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("StateArea")]
    private readonly EntityCollection<EmployerJob> _employerJobs = new EntityCollection<EmployerJob>();
    [ReverseAssociation("StateArea")]
    private readonly EntityCollection<InternshipReport> _internshipReports = new EntityCollection<InternshipReport>();
    [ReverseAssociation("StateArea")]
    private readonly EntityCollection<JobReport> _jobReports = new EntityCollection<JobReport>();
    [ReverseAssociation("StateArea")]
    private readonly EntityCollection<SkillReport> _skillReports = new EntityCollection<SkillReport>();
    [ReverseAssociation("StateArea")]
    private readonly EntityCollection<StudyPlace> _studyPlaces = new EntityCollection<StudyPlace>();
    [ReverseAssociation("StateArea")]
    private readonly EntityCollection<JobDegreeEducationLevelReport> _jobDegreeEducationLevelReportsByStateArea = new EntityCollection<JobDegreeEducationLevelReport>();
    [ReverseAssociation("StateArea")]
    private readonly EntityCollection<InternshipEmployer> _internshipEmployers = new EntityCollection<InternshipEmployer>();
    [ReverseAssociation("StateAreasByState")]
    private readonly EntityHolder<State> _state = new EntityHolder<State>();
    [ReverseAssociation("StateAreasByArea")]
    private readonly EntityHolder<Area> _area = new EntityHolder<Area>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<EmployerJob> EmployerJobs
    {
      get { return Get(_employerJobs); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<InternshipReport> InternshipReports
    {
      get { return Get(_internshipReports); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobReport> JobReports
    {
      get { return Get(_jobReports); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<SkillReport> SkillReports
    {
      get { return Get(_skillReports); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<StudyPlace> StudyPlaces
    {
      get { return Get(_studyPlaces); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobDegreeEducationLevelReport> JobDegreeEducationLevelReportsByStateArea
    {
      get { return Get(_jobDegreeEducationLevelReportsByStateArea); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<InternshipEmployer> InternshipEmployers
    {
      get { return Get(_internshipEmployers); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public State State
    {
      get { return Get(_state); }
      set { Set(_state, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Area Area
    {
      get { return Get(_area); }
      set { Set(_area, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string AreaCode
    {
      get { return Get(ref _areaCode, "AreaCode"); }
      set { Set(ref _areaCode, value, "AreaCode"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool Display
    {
      get { return Get(ref _display, "Display"); }
      set { Set(ref _display, value, "Display"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsDefault
    {
      get { return Get(ref _isDefault, "IsDefault"); }
      set { Set(ref _isDefault, value, "IsDefault"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="State" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StateId
    {
      get { return Get(ref _stateId, "StateId"); }
      set { Set(ref _stateId, value, "StateId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Area" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long AreaId
    {
      get { return Get(ref _areaId, "AreaId"); }
      set { Set(ref _areaId, value, "AreaId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobReportView")]
  public partial class JobReportView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private int _hiringDemand;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _growthTrend;
    private decimal _growthPercentile;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _salaryTrend;
    private decimal _salaryTrendPercentile;
    private decimal _salaryTrendAverage;
    private long _jobId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _name;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _hiringTrend;
    private decimal _hiringTrendPercentile;
    private int _salaryTrendMin;
    private int _salaryTrendMax;
    [ValidateLength(0, 10)]
    private string _salaryTrendRealtime;
    private System.Nullable<int> _salaryTrendRealtimeAverage;
    private System.Nullable<int> _salaryTrendRealtimeMin;
    private System.Nullable<int> _salaryTrendRealtimeMax;
    private long _stateAreaId;
    [ValidateLength(0, 1000)]
    private string _degreeIntroStatement;
    [ValidatePresence]
    [ValidateLength(0, 20)]
    private string _rOnet;
    [ValueField]
    private System.Nullable<Focus.Core.StarterJobLevel> _starterJob;
    [ValidatePresence]
    [ValidateLength(0, 4)]
    private string _hiringTrendSubLevel;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the HiringDemand entity attribute.</summary>
    public const string HiringDemandField = "HiringDemand";
    /// <summary>Identifies the GrowthTrend entity attribute.</summary>
    public const string GrowthTrendField = "GrowthTrend";
    /// <summary>Identifies the GrowthPercentile entity attribute.</summary>
    public const string GrowthPercentileField = "GrowthPercentile";
    /// <summary>Identifies the SalaryTrend entity attribute.</summary>
    public const string SalaryTrendField = "SalaryTrend";
    /// <summary>Identifies the SalaryTrendPercentile entity attribute.</summary>
    public const string SalaryTrendPercentileField = "SalaryTrendPercentile";
    /// <summary>Identifies the SalaryTrendAverage entity attribute.</summary>
    public const string SalaryTrendAverageField = "SalaryTrendAverage";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the HiringTrend entity attribute.</summary>
    public const string HiringTrendField = "HiringTrend";
    /// <summary>Identifies the HiringTrendPercentile entity attribute.</summary>
    public const string HiringTrendPercentileField = "HiringTrendPercentile";
    /// <summary>Identifies the SalaryTrendMin entity attribute.</summary>
    public const string SalaryTrendMinField = "SalaryTrendMin";
    /// <summary>Identifies the SalaryTrendMax entity attribute.</summary>
    public const string SalaryTrendMaxField = "SalaryTrendMax";
    /// <summary>Identifies the SalaryTrendRealtime entity attribute.</summary>
    public const string SalaryTrendRealtimeField = "SalaryTrendRealtime";
    /// <summary>Identifies the SalaryTrendRealtimeAverage entity attribute.</summary>
    public const string SalaryTrendRealtimeAverageField = "SalaryTrendRealtimeAverage";
    /// <summary>Identifies the SalaryTrendRealtimeMin entity attribute.</summary>
    public const string SalaryTrendRealtimeMinField = "SalaryTrendRealtimeMin";
    /// <summary>Identifies the SalaryTrendRealtimeMax entity attribute.</summary>
    public const string SalaryTrendRealtimeMaxField = "SalaryTrendRealtimeMax";
    /// <summary>Identifies the StateAreaId entity attribute.</summary>
    public const string StateAreaIdField = "StateAreaId";
    /// <summary>Identifies the DegreeIntroStatement entity attribute.</summary>
    public const string DegreeIntroStatementField = "DegreeIntroStatement";
    /// <summary>Identifies the ROnet entity attribute.</summary>
    public const string ROnetField = "ROnet";
    /// <summary>Identifies the StarterJob entity attribute.</summary>
    public const string StarterJobField = "StarterJob";
    /// <summary>Identifies the HiringTrendSubLevel entity attribute.</summary>
    public const string HiringTrendSubLevelField = "HiringTrendSubLevel";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int HiringDemand
    {
      get { return Get(ref _hiringDemand, "HiringDemand"); }
      set { Set(ref _hiringDemand, value, "HiringDemand"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string GrowthTrend
    {
      get { return Get(ref _growthTrend, "GrowthTrend"); }
      set { Set(ref _growthTrend, value, "GrowthTrend"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public decimal GrowthPercentile
    {
      get { return Get(ref _growthPercentile, "GrowthPercentile"); }
      set { Set(ref _growthPercentile, value, "GrowthPercentile"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SalaryTrend
    {
      get { return Get(ref _salaryTrend, "SalaryTrend"); }
      set { Set(ref _salaryTrend, value, "SalaryTrend"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public decimal SalaryTrendPercentile
    {
      get { return Get(ref _salaryTrendPercentile, "SalaryTrendPercentile"); }
      set { Set(ref _salaryTrendPercentile, value, "SalaryTrendPercentile"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public decimal SalaryTrendAverage
    {
      get { return Get(ref _salaryTrendAverage, "SalaryTrendAverage"); }
      set { Set(ref _salaryTrendAverage, value, "SalaryTrendAverage"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string HiringTrend
    {
      get { return Get(ref _hiringTrend, "HiringTrend"); }
      set { Set(ref _hiringTrend, value, "HiringTrend"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public decimal HiringTrendPercentile
    {
      get { return Get(ref _hiringTrendPercentile, "HiringTrendPercentile"); }
      set { Set(ref _hiringTrendPercentile, value, "HiringTrendPercentile"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int SalaryTrendMin
    {
      get { return Get(ref _salaryTrendMin, "SalaryTrendMin"); }
      set { Set(ref _salaryTrendMin, value, "SalaryTrendMin"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int SalaryTrendMax
    {
      get { return Get(ref _salaryTrendMax, "SalaryTrendMax"); }
      set { Set(ref _salaryTrendMax, value, "SalaryTrendMax"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SalaryTrendRealtime
    {
      get { return Get(ref _salaryTrendRealtime, "SalaryTrendRealtime"); }
      set { Set(ref _salaryTrendRealtime, value, "SalaryTrendRealtime"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> SalaryTrendRealtimeAverage
    {
      get { return Get(ref _salaryTrendRealtimeAverage, "SalaryTrendRealtimeAverage"); }
      set { Set(ref _salaryTrendRealtimeAverage, value, "SalaryTrendRealtimeAverage"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> SalaryTrendRealtimeMin
    {
      get { return Get(ref _salaryTrendRealtimeMin, "SalaryTrendRealtimeMin"); }
      set { Set(ref _salaryTrendRealtimeMin, value, "SalaryTrendRealtimeMin"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> SalaryTrendRealtimeMax
    {
      get { return Get(ref _salaryTrendRealtimeMax, "SalaryTrendRealtimeMax"); }
      set { Set(ref _salaryTrendRealtimeMax, value, "SalaryTrendRealtimeMax"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StateAreaId
    {
      get { return Get(ref _stateAreaId, "StateAreaId"); }
      set { Set(ref _stateAreaId, value, "StateAreaId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string DegreeIntroStatement
    {
      get { return Get(ref _degreeIntroStatement, "DegreeIntroStatement"); }
      set { Set(ref _degreeIntroStatement, value, "DegreeIntroStatement"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ROnet
    {
      get { return Get(ref _rOnet, "ROnet"); }
      set { Set(ref _rOnet, value, "ROnet"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.StarterJobLevel> StarterJob
    {
      get { return Get(ref _starterJob, "StarterJob"); }
      set { Set(ref _starterJob, value, "StarterJob"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string HiringTrendSubLevel
    {
      get { return Get(ref _hiringTrendSubLevel, "HiringTrendSubLevel"); }
      set { Set(ref _hiringTrendSubLevel, value, "HiringTrendSubLevel"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobDegreeLevel", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class JobDegreeLevel : Entity<long>
  {
    #region Fields
  
    [ValueField]
    private Focus.Core.DegreeLevels _degreeLevel;
    private long _jobId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the DegreeLevel entity attribute.</summary>
    public const string DegreeLevelField = "DegreeLevel";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobDegreeLevels")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.DegreeLevels DegreeLevel
    {
      get { return Get(ref _degreeLevel, "DegreeLevel"); }
      set { Set(ref _degreeLevel, value, "DegreeLevel"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.SkillCategorySkillView")]
  public partial class SkillCategorySkillView : Entity<long>
  {
    #region Fields
  
    private long _skillCategoryId;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _skillCategoryName;
    private long _skillId;
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _skillName;
    private System.Nullable<long> _skillCategoryParentId;
    [ValueField]
    private Focus.Core.SkillTypes _skillType;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SkillCategoryId entity attribute.</summary>
    public const string SkillCategoryIdField = "SkillCategoryId";
    /// <summary>Identifies the SkillCategoryName entity attribute.</summary>
    public const string SkillCategoryNameField = "SkillCategoryName";
    /// <summary>Identifies the SkillId entity attribute.</summary>
    public const string SkillIdField = "SkillId";
    /// <summary>Identifies the SkillName entity attribute.</summary>
    public const string SkillNameField = "SkillName";
    /// <summary>Identifies the SkillCategoryParentId entity attribute.</summary>
    public const string SkillCategoryParentIdField = "SkillCategoryParentId";
    /// <summary>Identifies the SkillType entity attribute.</summary>
    public const string SkillTypeField = "SkillType";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillCategoryId
    {
      get { return Get(ref _skillCategoryId, "SkillCategoryId"); }
      set { Set(ref _skillCategoryId, value, "SkillCategoryId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SkillCategoryName
    {
      get { return Get(ref _skillCategoryName, "SkillCategoryName"); }
      set { Set(ref _skillCategoryName, value, "SkillCategoryName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillId
    {
      get { return Get(ref _skillId, "SkillId"); }
      set { Set(ref _skillId, value, "SkillId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SkillName
    {
      get { return Get(ref _skillName, "SkillName"); }
      set { Set(ref _skillName, value, "SkillName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> SkillCategoryParentId
    {
      get { return Get(ref _skillCategoryParentId, "SkillCategoryParentId"); }
      set { Set(ref _skillCategoryParentId, value, "SkillCategoryParentId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.SkillTypes SkillType
    {
      get { return Get(ref _skillType, "SkillType"); }
      set { Set(ref _skillType, value, "SkillType"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobJobTitle", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class JobJobTitle : Entity<long>
  {
    #region Fields
  
    private decimal _demandPercentile;
    private long _jobId;
    private long _jobTitleId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the DemandPercentile entity attribute.</summary>
    public const string DemandPercentileField = "DemandPercentile";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the JobTitleId entity attribute.</summary>
    public const string JobTitleIdField = "JobTitleId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobJobTitles")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();
    [ReverseAssociation("JobJobTitles")]
    private readonly EntityHolder<JobTitle> _jobTitle = new EntityHolder<JobTitle>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public JobTitle JobTitle
    {
      get { return Get(_jobTitle); }
      set { Set(_jobTitle, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public decimal DemandPercentile
    {
      get { return Get(ref _demandPercentile, "DemandPercentile"); }
      set { Set(ref _demandPercentile, value, "DemandPercentile"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobTitle" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobTitleId
    {
      get { return Get(ref _jobTitleId, "JobTitleId"); }
      set { Set(ref _jobTitleId, value, "JobTitleId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobTitle", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class JobTitle : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 255)]
    private string _name;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobTitle")]
    private readonly EntityCollection<JobJobTitle> _jobJobTitles = new EntityCollection<JobJobTitle>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobJobTitle> JobJobTitles
    {
      get { return Get(_jobJobTitles); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobEducationRequirement", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class JobEducationRequirement : Entity<long>
  {
    #region Fields
  
    [ValueField]
    private Focus.Core.EducationRequirementTypes _educationRequirementType;
    private decimal _requirementPercentile;
    private long _jobId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the EducationRequirementType entity attribute.</summary>
    public const string EducationRequirementTypeField = "EducationRequirementType";
    /// <summary>Identifies the RequirementPercentile entity attribute.</summary>
    public const string RequirementPercentileField = "RequirementPercentile";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobEducationRequirements")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.EducationRequirementTypes EducationRequirementType
    {
      get { return Get(ref _educationRequirementType, "EducationRequirementType"); }
      set { Set(ref _educationRequirementType, value, "EducationRequirementType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public decimal RequirementPercentile
    {
      get { return Get(ref _requirementPercentile, "RequirementPercentile"); }
      set { Set(ref _requirementPercentile, value, "RequirementPercentile"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobExperienceLevel", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class JobExperienceLevel : Entity<long>
  {
    #region Fields
  
    [ValueField]
    private Focus.Core.ExperienceLevels _experienceLevel;
    private decimal _experiencePercentile;
    private long _jobId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ExperienceLevel entity attribute.</summary>
    public const string ExperienceLevelField = "ExperienceLevel";
    /// <summary>Identifies the ExperiencePercentile entity attribute.</summary>
    public const string ExperiencePercentileField = "ExperiencePercentile";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobExperienceLevels")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ExperienceLevels ExperienceLevel
    {
      get { return Get(ref _experienceLevel, "ExperienceLevel"); }
      set { Set(ref _experienceLevel, value, "ExperienceLevel"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public decimal ExperiencePercentile
    {
      get { return Get(ref _experiencePercentile, "ExperiencePercentile"); }
      set { Set(ref _experiencePercentile, value, "ExperiencePercentile"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.EmployerSkillView")]
  public partial class EmployerSkillView : Entity<long>
  {
    #region Fields
  
    private long _skillId;
    [ValueField]
    private Focus.Core.SkillTypes _skillType;
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _skillName;
    private long _employerId;
    private int _postings;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SkillId entity attribute.</summary>
    public const string SkillIdField = "SkillId";
    /// <summary>Identifies the SkillType entity attribute.</summary>
    public const string SkillTypeField = "SkillType";
    /// <summary>Identifies the SkillName entity attribute.</summary>
    public const string SkillNameField = "SkillName";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the Postings entity attribute.</summary>
    public const string PostingsField = "Postings";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillId
    {
      get { return Get(ref _skillId, "SkillId"); }
      set { Set(ref _skillId, value, "SkillId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.SkillTypes SkillType
    {
      get { return Get(ref _skillType, "SkillType"); }
      set { Set(ref _skillType, value, "SkillType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SkillName
    {
      get { return Get(ref _skillName, "SkillName"); }
      set { Set(ref _skillName, value, "SkillName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Postings
    {
      get { return Get(ref _postings, "Postings"); }
      set { Set(ref _postings, value, "Postings"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.SkillReportView")]
  public partial class SkillReportView : Entity<long>
  {
    #region Fields
  
    [ValueField]
    private Focus.Core.SkillTypes _skillType;
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _skillName;
    private long _skillId;
    private int _frequency;
    private long _stateAreaId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SkillType entity attribute.</summary>
    public const string SkillTypeField = "SkillType";
    /// <summary>Identifies the SkillName entity attribute.</summary>
    public const string SkillNameField = "SkillName";
    /// <summary>Identifies the SkillId entity attribute.</summary>
    public const string SkillIdField = "SkillId";
    /// <summary>Identifies the Frequency entity attribute.</summary>
    public const string FrequencyField = "Frequency";
    /// <summary>Identifies the StateAreaId entity attribute.</summary>
    public const string StateAreaIdField = "StateAreaId";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.SkillTypes SkillType
    {
      get { return Get(ref _skillType, "SkillType"); }
      set { Set(ref _skillType, value, "SkillType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SkillName
    {
      get { return Get(ref _skillName, "SkillName"); }
      set { Set(ref _skillName, value, "SkillName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillId
    {
      get { return Get(ref _skillId, "SkillId"); }
      set { Set(ref _skillId, value, "SkillId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Frequency
    {
      get { return Get(ref _frequency, "Frequency"); }
      set { Set(ref _frequency, value, "Frequency"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StateAreaId
    {
      get { return Get(ref _stateAreaId, "StateAreaId"); }
      set { Set(ref _stateAreaId, value, "StateAreaId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.EmployerSkill", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class EmployerSkill : Entity<long>
  {
    #region Fields
  
    private int _postings;
    private long _employerId;
    private long _skillId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Postings entity attribute.</summary>
    public const string PostingsField = "Postings";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the SkillId entity attribute.</summary>
    public const string SkillIdField = "SkillId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("EmployerSkillsByEmployer")]
    private readonly EntityHolder<Employer> _employer = new EntityHolder<Employer>();
    [ReverseAssociation("EmployerSkillsBySkill")]
    private readonly EntityHolder<Skill> _skill = new EntityHolder<Skill>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Employer Employer
    {
      get { return Get(_employer); }
      set { Set(_employer, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Skill Skill
    {
      get { return Get(_skill); }
      set { Set(_skill, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Postings
    {
      get { return Get(ref _postings, "Postings"); }
      set { Set(ref _postings, value, "Postings"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Employer" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Skill" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillId
    {
      get { return Get(ref _skillId, "SkillId"); }
      set { Set(ref _skillId, value, "SkillId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.SkillReport", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class SkillReport : Entity<long>
  {
    #region Fields
  
    private int _frequency;
    private long _skillId;
    private long _stateAreaId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Frequency entity attribute.</summary>
    public const string FrequencyField = "Frequency";
    /// <summary>Identifies the SkillId entity attribute.</summary>
    public const string SkillIdField = "SkillId";
    /// <summary>Identifies the StateAreaId entity attribute.</summary>
    public const string StateAreaIdField = "StateAreaId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("SkillReportsBySkill")]
    private readonly EntityHolder<Skill> _skill = new EntityHolder<Skill>();
    [ReverseAssociation("SkillReports")]
    private readonly EntityHolder<StateArea> _stateArea = new EntityHolder<StateArea>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Skill Skill
    {
      get { return Get(_skill); }
      set { Set(_skill, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public StateArea StateArea
    {
      get { return Get(_stateArea); }
      set { Set(_stateArea, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Frequency
    {
      get { return Get(ref _frequency, "Frequency"); }
      set { Set(ref _frequency, value, "Frequency"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Skill" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillId
    {
      get { return Get(ref _skillId, "SkillId"); }
      set { Set(ref _skillId, value, "SkillId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="StateArea" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StateAreaId
    {
      get { return Get(ref _stateAreaId, "StateAreaId"); }
      set { Set(ref _stateAreaId, value, "StateAreaId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobCareerAreaSkillView")]
  public partial class JobCareerAreaSkillView : Entity<long>
  {
    #region Fields
  
    private long _skillId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SkillId entity attribute.</summary>
    public const string SkillIdField = "SkillId";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillId
    {
      get { return Get(ref _skillId, "SkillId"); }
      set { Set(ref _skillId, value, "SkillId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobDegreeLevelSkillView")]
  public partial class JobDegreeLevelSkillView : Entity<long>
  {
    #region Fields
  
    private long _skillId;
    [ValueField]
    private Focus.Core.DegreeLevels _degreeLevel;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SkillId entity attribute.</summary>
    public const string SkillIdField = "SkillId";
    /// <summary>Identifies the DegreeLevel entity attribute.</summary>
    public const string DegreeLevelField = "DegreeLevel";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillId
    {
      get { return Get(ref _skillId, "SkillId"); }
      set { Set(ref _skillId, value, "SkillId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.DegreeLevels DegreeLevel
    {
      get { return Get(ref _degreeLevel, "DegreeLevel"); }
      set { Set(ref _degreeLevel, value, "DegreeLevel"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobJobTitleSkillView")]
  public partial class JobJobTitleSkillView : Entity<long>
  {
    #region Fields
  
    private long _skillId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SkillId entity attribute.</summary>
    public const string SkillIdField = "SkillId";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillId
    {
      get { return Get(ref _skillId, "SkillId"); }
      set { Set(ref _skillId, value, "SkillId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.InternshipCategory", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class InternshipCategory : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _name;
    private long _localisationId;
    private int _lensFilterId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";
    /// <summary>Identifies the LensFilterId entity attribute.</summary>
    public const string LensFilterIdField = "LensFilterId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("InternshipCategory")]
    private readonly EntityCollection<InternshipEmployer> _internshipEmployers = new EntityCollection<InternshipEmployer>();
    [ReverseAssociation("InternshipCategory")]
    private readonly EntityCollection<InternshipJobTitle> _internshipJobTitles = new EntityCollection<InternshipJobTitle>();
    [ReverseAssociation("InternshipCategory")]
    private readonly EntityCollection<InternshipReport> _internshipReports = new EntityCollection<InternshipReport>();
    [ReverseAssociation("InternshipCategory")]
    private readonly EntityCollection<InternshipSkill> _internshipSkills = new EntityCollection<InternshipSkill>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<InternshipEmployer> InternshipEmployers
    {
      get { return Get(_internshipEmployers); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<InternshipJobTitle> InternshipJobTitles
    {
      get { return Get(_internshipJobTitles); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<InternshipReport> InternshipReports
    {
      get { return Get(_internshipReports); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<InternshipSkill> InternshipSkills
    {
      get { return Get(_internshipSkills); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int LensFilterId
    {
      get { return Get(ref _lensFilterId, "LensFilterId"); }
      set { Set(ref _lensFilterId, value, "LensFilterId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.InternshipSkill", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class InternshipSkill : Entity<long>
  {
    #region Fields
  
    private int _rank;
    private long _skillId;
    private long _internshipCategoryId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Rank entity attribute.</summary>
    public const string RankField = "Rank";
    /// <summary>Identifies the SkillId entity attribute.</summary>
    public const string SkillIdField = "SkillId";
    /// <summary>Identifies the InternshipCategoryId entity attribute.</summary>
    public const string InternshipCategoryIdField = "InternshipCategoryId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("InternshipSkills")]
    private readonly EntityHolder<Skill> _skill = new EntityHolder<Skill>();
    [ReverseAssociation("InternshipSkills")]
    private readonly EntityHolder<InternshipCategory> _internshipCategory = new EntityHolder<InternshipCategory>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Skill Skill
    {
      get { return Get(_skill); }
      set { Set(_skill, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public InternshipCategory InternshipCategory
    {
      get { return Get(_internshipCategory); }
      set { Set(_internshipCategory, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Rank
    {
      get { return Get(ref _rank, "Rank"); }
      set { Set(ref _rank, value, "Rank"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Skill" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillId
    {
      get { return Get(ref _skillId, "SkillId"); }
      set { Set(ref _skillId, value, "SkillId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="InternshipCategory" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long InternshipCategoryId
    {
      get { return Get(ref _internshipCategoryId, "InternshipCategoryId"); }
      set { Set(ref _internshipCategoryId, value, "InternshipCategoryId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.InternshipEmployer", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class InternshipEmployer : Entity<long>
  {
    #region Fields
  
    private int _frequency;
    private long _employerId;
    private System.Nullable<long> _stateAreaId;
    private long _internshipCategoryId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Frequency entity attribute.</summary>
    public const string FrequencyField = "Frequency";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the StateAreaId entity attribute.</summary>
    public const string StateAreaIdField = "StateAreaId";
    /// <summary>Identifies the InternshipCategoryId entity attribute.</summary>
    public const string InternshipCategoryIdField = "InternshipCategoryId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("InternshipEmployers")]
    private readonly EntityHolder<Employer> _employer = new EntityHolder<Employer>();
    [ReverseAssociation("InternshipEmployers")]
    private readonly EntityHolder<StateArea> _stateArea = new EntityHolder<StateArea>();
    [ReverseAssociation("InternshipEmployers")]
    private readonly EntityHolder<InternshipCategory> _internshipCategory = new EntityHolder<InternshipCategory>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Employer Employer
    {
      get { return Get(_employer); }
      set { Set(_employer, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public StateArea StateArea
    {
      get { return Get(_stateArea); }
      set { Set(_stateArea, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public InternshipCategory InternshipCategory
    {
      get { return Get(_internshipCategory); }
      set { Set(_internshipCategory, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Frequency
    {
      get { return Get(ref _frequency, "Frequency"); }
      set { Set(ref _frequency, value, "Frequency"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Employer" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="StateArea" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> StateAreaId
    {
      get { return Get(ref _stateAreaId, "StateAreaId"); }
      set { Set(ref _stateAreaId, value, "StateAreaId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="InternshipCategory" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long InternshipCategoryId
    {
      get { return Get(ref _internshipCategoryId, "InternshipCategoryId"); }
      set { Set(ref _internshipCategoryId, value, "InternshipCategoryId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.InternshipJobTitle", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class InternshipJobTitle : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 255)]
    private string _jobTitle;
    private int _frequency;
    private long _internshipCategoryId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobTitle entity attribute.</summary>
    public const string JobTitleField = "JobTitle";
    /// <summary>Identifies the Frequency entity attribute.</summary>
    public const string FrequencyField = "Frequency";
    /// <summary>Identifies the InternshipCategoryId entity attribute.</summary>
    public const string InternshipCategoryIdField = "InternshipCategoryId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("InternshipJobTitles")]
    private readonly EntityHolder<InternshipCategory> _internshipCategory = new EntityHolder<InternshipCategory>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public InternshipCategory InternshipCategory
    {
      get { return Get(_internshipCategory); }
      set { Set(_internshipCategory, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string JobTitle
    {
      get { return Get(ref _jobTitle, "JobTitle"); }
      set { Set(ref _jobTitle, value, "JobTitle"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Frequency
    {
      get { return Get(ref _frequency, "Frequency"); }
      set { Set(ref _frequency, value, "Frequency"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="InternshipCategory" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long InternshipCategoryId
    {
      get { return Get(ref _internshipCategoryId, "InternshipCategoryId"); }
      set { Set(ref _internshipCategoryId, value, "InternshipCategoryId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.InternshipReport", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class InternshipReport : Entity<long>
  {
    #region Fields
  
    private int _numberAvailable;
    private long _stateAreaId;
    private long _internshipCategoryId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the NumberAvailable entity attribute.</summary>
    public const string NumberAvailableField = "NumberAvailable";
    /// <summary>Identifies the StateAreaId entity attribute.</summary>
    public const string StateAreaIdField = "StateAreaId";
    /// <summary>Identifies the InternshipCategoryId entity attribute.</summary>
    public const string InternshipCategoryIdField = "InternshipCategoryId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("InternshipReports")]
    private readonly EntityHolder<StateArea> _stateArea = new EntityHolder<StateArea>();
    [ReverseAssociation("InternshipReports")]
    private readonly EntityHolder<InternshipCategory> _internshipCategory = new EntityHolder<InternshipCategory>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public StateArea StateArea
    {
      get { return Get(_stateArea); }
      set { Set(_stateArea, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public InternshipCategory InternshipCategory
    {
      get { return Get(_internshipCategory); }
      set { Set(_internshipCategory, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int NumberAvailable
    {
      get { return Get(ref _numberAvailable, "NumberAvailable"); }
      set { Set(ref _numberAvailable, value, "NumberAvailable"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="StateArea" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StateAreaId
    {
      get { return Get(ref _stateAreaId, "StateAreaId"); }
      set { Set(ref _stateAreaId, value, "StateAreaId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="InternshipCategory" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long InternshipCategoryId
    {
      get { return Get(ref _internshipCategoryId, "InternshipCategoryId"); }
      set { Set(ref _internshipCategoryId, value, "InternshipCategoryId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.InternshipEmployerView")]
  public partial class InternshipEmployerView : Entity<long>
  {
    #region Fields
  
    private long _employerId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _name;
    private int _frequency;
    private long _internshipCategoryId;
    private System.Nullable<long> _stateAreaId;
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _internshipCategoryName;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the Frequency entity attribute.</summary>
    public const string FrequencyField = "Frequency";
    /// <summary>Identifies the InternshipCategoryId entity attribute.</summary>
    public const string InternshipCategoryIdField = "InternshipCategoryId";
    /// <summary>Identifies the StateAreaId entity attribute.</summary>
    public const string StateAreaIdField = "StateAreaId";
    /// <summary>Identifies the InternshipCategoryName entity attribute.</summary>
    public const string InternshipCategoryNameField = "InternshipCategoryName";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Frequency
    {
      get { return Get(ref _frequency, "Frequency"); }
      set { Set(ref _frequency, value, "Frequency"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long InternshipCategoryId
    {
      get { return Get(ref _internshipCategoryId, "InternshipCategoryId"); }
      set { Set(ref _internshipCategoryId, value, "InternshipCategoryId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> StateAreaId
    {
      get { return Get(ref _stateAreaId, "StateAreaId"); }
      set { Set(ref _stateAreaId, value, "StateAreaId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string InternshipCategoryName
    {
      get { return Get(ref _internshipCategoryName, "InternshipCategoryName"); }
      set { Set(ref _internshipCategoryName, value, "InternshipCategoryName"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.InternshipReportView")]
  public partial class InternshipReportView : Entity<long>
  {
    #region Fields
  
    private long _internshipCategoryId;
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _name;
    private int _numberAvailable;
    private long _stateAreaId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the InternshipCategoryId entity attribute.</summary>
    public const string InternshipCategoryIdField = "InternshipCategoryId";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the NumberAvailable entity attribute.</summary>
    public const string NumberAvailableField = "NumberAvailable";
    /// <summary>Identifies the StateAreaId entity attribute.</summary>
    public const string StateAreaIdField = "StateAreaId";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long InternshipCategoryId
    {
      get { return Get(ref _internshipCategoryId, "InternshipCategoryId"); }
      set { Set(ref _internshipCategoryId, value, "InternshipCategoryId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int NumberAvailable
    {
      get { return Get(ref _numberAvailable, "NumberAvailable"); }
      set { Set(ref _numberAvailable, value, "NumberAvailable"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StateAreaId
    {
      get { return Get(ref _stateAreaId, "StateAreaId"); }
      set { Set(ref _stateAreaId, value, "StateAreaId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.InternshipSkillView")]
  public partial class InternshipSkillView : Entity<long>
  {
    #region Fields
  
    private long _skillId;
    [ValueField]
    private Focus.Core.SkillTypes _skillType;
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _skillName;
    private long _internshipCategoryId;
    private int _rank;
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _internshipCategoryName;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SkillId entity attribute.</summary>
    public const string SkillIdField = "SkillId";
    /// <summary>Identifies the SkillType entity attribute.</summary>
    public const string SkillTypeField = "SkillType";
    /// <summary>Identifies the SkillName entity attribute.</summary>
    public const string SkillNameField = "SkillName";
    /// <summary>Identifies the InternshipCategoryId entity attribute.</summary>
    public const string InternshipCategoryIdField = "InternshipCategoryId";
    /// <summary>Identifies the Rank entity attribute.</summary>
    public const string RankField = "Rank";
    /// <summary>Identifies the InternshipCategoryName entity attribute.</summary>
    public const string InternshipCategoryNameField = "InternshipCategoryName";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillId
    {
      get { return Get(ref _skillId, "SkillId"); }
      set { Set(ref _skillId, value, "SkillId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.SkillTypes SkillType
    {
      get { return Get(ref _skillType, "SkillType"); }
      set { Set(ref _skillType, value, "SkillType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SkillName
    {
      get { return Get(ref _skillName, "SkillName"); }
      set { Set(ref _skillName, value, "SkillName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long InternshipCategoryId
    {
      get { return Get(ref _internshipCategoryId, "InternshipCategoryId"); }
      set { Set(ref _internshipCategoryId, value, "InternshipCategoryId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Rank
    {
      get { return Get(ref _rank, "Rank"); }
      set { Set(ref _rank, value, "Rank"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string InternshipCategoryName
    {
      get { return Get(ref _internshipCategoryName, "InternshipCategoryName"); }
      set { Set(ref _internshipCategoryName, value, "InternshipCategoryName"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.StateAreaView")]
  public partial class StateAreaView : Entity<long>
  {
    #region Fields
  
    private long _stateId;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _stateName;
    [ValidatePresence]
    [ValidateLength(0, 2)]
    private string _stateCode;
    private int _stateSortOrder;
    private long _areaId;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _areaName;
    private int _areaSortOrder;
    [ValidatePresence]
    [ValidateLength(0, 102)]
    private string _stateAreaName;
    private string _areaCode;
    private bool _isDefault;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the StateId entity attribute.</summary>
    public const string StateIdField = "StateId";
    /// <summary>Identifies the StateName entity attribute.</summary>
    public const string StateNameField = "StateName";
    /// <summary>Identifies the StateCode entity attribute.</summary>
    public const string StateCodeField = "StateCode";
    /// <summary>Identifies the StateSortOrder entity attribute.</summary>
    public const string StateSortOrderField = "StateSortOrder";
    /// <summary>Identifies the AreaId entity attribute.</summary>
    public const string AreaIdField = "AreaId";
    /// <summary>Identifies the AreaName entity attribute.</summary>
    public const string AreaNameField = "AreaName";
    /// <summary>Identifies the AreaSortOrder entity attribute.</summary>
    public const string AreaSortOrderField = "AreaSortOrder";
    /// <summary>Identifies the StateAreaName entity attribute.</summary>
    public const string StateAreaNameField = "StateAreaName";
    /// <summary>Identifies the AreaCode entity attribute.</summary>
    public const string AreaCodeField = "AreaCode";
    /// <summary>Identifies the IsDefault entity attribute.</summary>
    public const string IsDefaultField = "IsDefault";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StateId
    {
      get { return Get(ref _stateId, "StateId"); }
      set { Set(ref _stateId, value, "StateId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string StateName
    {
      get { return Get(ref _stateName, "StateName"); }
      set { Set(ref _stateName, value, "StateName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string StateCode
    {
      get { return Get(ref _stateCode, "StateCode"); }
      set { Set(ref _stateCode, value, "StateCode"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int StateSortOrder
    {
      get { return Get(ref _stateSortOrder, "StateSortOrder"); }
      set { Set(ref _stateSortOrder, value, "StateSortOrder"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long AreaId
    {
      get { return Get(ref _areaId, "AreaId"); }
      set { Set(ref _areaId, value, "AreaId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string AreaName
    {
      get { return Get(ref _areaName, "AreaName"); }
      set { Set(ref _areaName, value, "AreaName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int AreaSortOrder
    {
      get { return Get(ref _areaSortOrder, "AreaSortOrder"); }
      set { Set(ref _areaSortOrder, value, "AreaSortOrder"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string StateAreaName
    {
      get { return Get(ref _stateAreaName, "StateAreaName"); }
      set { Set(ref _stateAreaName, value, "StateAreaName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string AreaCode
    {
      get { return Get(ref _areaCode, "AreaCode"); }
      set { Set(ref _areaCode, value, "AreaCode"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsDefault
    {
      get { return Get(ref _isDefault, "IsDefault"); }
      set { Set(ref _isDefault, value, "IsDefault"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.Certification", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class Certification : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _name;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Certification")]
    private readonly EntityCollection<JobCertification> _jobCertifications = new EntityCollection<JobCertification>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobCertification> JobCertifications
    {
      get { return Get(_jobCertifications); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobCertification", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class JobCertification : Entity<long>
  {
    #region Fields
  
    private decimal _demandPercentile;
    private long _jobId;
    private long _certificationId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the DemandPercentile entity attribute.</summary>
    public const string DemandPercentileField = "DemandPercentile";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the CertificationId entity attribute.</summary>
    public const string CertificationIdField = "CertificationId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobCertifications")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();
    [ReverseAssociation("JobCertifications")]
    private readonly EntityHolder<Certification> _certification = new EntityHolder<Certification>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Certification Certification
    {
      get { return Get(_certification); }
      set { Set(_certification, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public decimal DemandPercentile
    {
      get { return Get(ref _demandPercentile, "DemandPercentile"); }
      set { Set(ref _demandPercentile, value, "DemandPercentile"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Certification" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long CertificationId
    {
      get { return Get(ref _certificationId, "CertificationId"); }
      set { Set(ref _certificationId, value, "CertificationId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.Degree", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class Degree : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _name;
    [ValidateLength(0, 10)]
    private string _rcipCode;
    private bool _isDegreeArea;
    private bool _isClientData;
    [ValidateLength(0, 100)]
    private string _clientDataTag;
    private System.Nullable<int> _tier;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the RcipCode entity attribute.</summary>
    public const string RcipCodeField = "RcipCode";
    /// <summary>Identifies the IsDegreeArea entity attribute.</summary>
    public const string IsDegreeAreaField = "IsDegreeArea";
    /// <summary>Identifies the IsClientData entity attribute.</summary>
    public const string IsClientDataField = "IsClientData";
    /// <summary>Identifies the ClientDataTag entity attribute.</summary>
    public const string ClientDataTagField = "ClientDataTag";
    /// <summary>Identifies the Tier entity attribute.</summary>
    public const string TierField = "Tier";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Degree")]
    private readonly EntityCollection<DegreeEducationLevel> _degreeEducationLevels = new EntityCollection<DegreeEducationLevel>();
    [ReverseAssociation("Degree")]
    private readonly EntityCollection<DegreeAlias> _degreeAliases = new EntityCollection<DegreeAlias>();
    [ReverseAssociation("Degree")]
    private readonly EntityCollection<ProgramAreaDegree> _programAreaDegrees = new EntityCollection<ProgramAreaDegree>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<DegreeEducationLevel> DegreeEducationLevels
    {
      get { return Get(_degreeEducationLevels); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<DegreeAlias> DegreeAliases
    {
      get { return Get(_degreeAliases); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<ProgramAreaDegree> ProgramAreaDegrees
    {
      get { return Get(_programAreaDegrees); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string RcipCode
    {
      get { return Get(ref _rcipCode, "RcipCode"); }
      set { Set(ref _rcipCode, value, "RcipCode"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsDegreeArea
    {
      get { return Get(ref _isDegreeArea, "IsDegreeArea"); }
      set { Set(ref _isDegreeArea, value, "IsDegreeArea"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsClientData
    {
      get { return Get(ref _isClientData, "IsClientData"); }
      set { Set(ref _isClientData, value, "IsClientData"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ClientDataTag
    {
      get { return Get(ref _clientDataTag, "ClientDataTag"); }
      set { Set(ref _clientDataTag, value, "ClientDataTag"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> Tier
    {
      get { return Get(ref _tier, "Tier"); }
      set { Set(ref _tier, value, "Tier"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.DegreeEducationLevel", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class DegreeEducationLevel : Entity<long>
  {
    #region Fields
  
    [ValueField]
    private Focus.Core.ExplorerEducationLevels _educationLevel;
    private int _degreesAwarded;
    [ValidateLength(0, 200)]
    private string _name;
    private bool _excludeFromReport;
    private long _degreeId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the EducationLevel entity attribute.</summary>
    public const string EducationLevelField = "EducationLevel";
    /// <summary>Identifies the DegreesAwarded entity attribute.</summary>
    public const string DegreesAwardedField = "DegreesAwarded";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the ExcludeFromReport entity attribute.</summary>
    public const string ExcludeFromReportField = "ExcludeFromReport";
    /// <summary>Identifies the DegreeId entity attribute.</summary>
    public const string DegreeIdField = "DegreeId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("DegreeEducationLevel")]
    private readonly EntityCollection<JobDegreeEducationLevel> _jobDegreeEducationLevels = new EntityCollection<JobDegreeEducationLevel>();
    [ReverseAssociation("DegreeEducationLevel")]
    private readonly EntityCollection<StudyPlaceDegreeEducationLevel> _studyPlaceDegreeEducationLevels = new EntityCollection<StudyPlaceDegreeEducationLevel>();
    [ReverseAssociation("DegreeEducationLevel")]
    private readonly EntityCollection<DegreeEducationLevelExt> _degreeEducationLevelExts = new EntityCollection<DegreeEducationLevelExt>();
    [ReverseAssociation("DegreeEducationLevel")]
    private readonly EntityCollection<DegreeAlias> _degreeAliases = new EntityCollection<DegreeAlias>();
    [ReverseAssociation("DegreeEducationLevels")]
    private readonly EntityHolder<Degree> _degree = new EntityHolder<Degree>();

    private ThroughAssociation<JobDegreeEducationLevel, Job> _jobs;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobDegreeEducationLevel> JobDegreeEducationLevels
    {
      get { return Get(_jobDegreeEducationLevels); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<StudyPlaceDegreeEducationLevel> StudyPlaceDegreeEducationLevels
    {
      get { return Get(_studyPlaceDegreeEducationLevels); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<DegreeEducationLevelExt> DegreeEducationLevelExts
    {
      get { return Get(_degreeEducationLevelExts); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<DegreeAlias> DegreeAliases
    {
      get { return Get(_degreeAliases); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Degree Degree
    {
      get { return Get(_degree); }
      set { Set(_degree, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<JobDegreeEducationLevel, Job> Jobs
    {
      get
      {
        if (_jobs == null)
        {
          _jobs = new ThroughAssociation<JobDegreeEducationLevel, Job>(_jobDegreeEducationLevels);
        }
        return Get(_jobs);
      }
    }
    

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ExplorerEducationLevels EducationLevel
    {
      get { return Get(ref _educationLevel, "EducationLevel"); }
      set { Set(ref _educationLevel, value, "EducationLevel"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int DegreesAwarded
    {
      get { return Get(ref _degreesAwarded, "DegreesAwarded"); }
      set { Set(ref _degreesAwarded, value, "DegreesAwarded"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool ExcludeFromReport
    {
      get { return Get(ref _excludeFromReport, "ExcludeFromReport"); }
      set { Set(ref _excludeFromReport, value, "ExcludeFromReport"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Degree" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeId
    {
      get { return Get(ref _degreeId, "DegreeId"); }
      set { Set(ref _degreeId, value, "DegreeId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobDegreeEducationLevel", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class JobDegreeEducationLevel : Entity<long>
  {
    #region Fields
  
    private bool _excludeFromJob;
    private System.Nullable<int> _tier;
    private long _jobId;
    private long _degreeEducationLevelId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ExcludeFromJob entity attribute.</summary>
    public const string ExcludeFromJobField = "ExcludeFromJob";
    /// <summary>Identifies the Tier entity attribute.</summary>
    public const string TierField = "Tier";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the DegreeEducationLevelId entity attribute.</summary>
    public const string DegreeEducationLevelIdField = "DegreeEducationLevelId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobDegreeEducationLevel")]
    private readonly EntityCollection<JobDegreeEducationLevelReport> _jobDegreeEducationLevelReportsByJobDegreeEducationLevel = new EntityCollection<JobDegreeEducationLevelReport>();
    [ReverseAssociation("JobDegreeEducationLevels")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();
    [ReverseAssociation("JobDegreeEducationLevels")]
    private readonly EntityHolder<DegreeEducationLevel> _degreeEducationLevel = new EntityHolder<DegreeEducationLevel>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobDegreeEducationLevelReport> JobDegreeEducationLevelReportsByJobDegreeEducationLevel
    {
      get { return Get(_jobDegreeEducationLevelReportsByJobDegreeEducationLevel); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public DegreeEducationLevel DegreeEducationLevel
    {
      get { return Get(_degreeEducationLevel); }
      set { Set(_degreeEducationLevel, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool ExcludeFromJob
    {
      get { return Get(ref _excludeFromJob, "ExcludeFromJob"); }
      set { Set(ref _excludeFromJob, value, "ExcludeFromJob"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> Tier
    {
      get { return Get(ref _tier, "Tier"); }
      set { Set(ref _tier, value, "Tier"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="DegreeEducationLevel" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeEducationLevelId
    {
      get { return Get(ref _degreeEducationLevelId, "DegreeEducationLevelId"); }
      set { Set(ref _degreeEducationLevelId, value, "DegreeEducationLevelId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.StudyPlaceDegreeEducationLevel", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class StudyPlaceDegreeEducationLevel : Entity<long>
  {
    #region Fields
  
    private long _studyPlaceId;
    private long _degreeEducationLevelId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the StudyPlaceId entity attribute.</summary>
    public const string StudyPlaceIdField = "StudyPlaceId";
    /// <summary>Identifies the DegreeEducationLevelId entity attribute.</summary>
    public const string DegreeEducationLevelIdField = "DegreeEducationLevelId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("StudyPlaceDegreeEducationLevels")]
    private readonly EntityHolder<StudyPlace> _studyPlace = new EntityHolder<StudyPlace>();
    [ReverseAssociation("StudyPlaceDegreeEducationLevels")]
    private readonly EntityHolder<DegreeEducationLevel> _degreeEducationLevel = new EntityHolder<DegreeEducationLevel>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public StudyPlace StudyPlace
    {
      get { return Get(_studyPlace); }
      set { Set(_studyPlace, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public DegreeEducationLevel DegreeEducationLevel
    {
      get { return Get(_degreeEducationLevel); }
      set { Set(_degreeEducationLevel, value); }
    }


    /// <summary>Gets or sets the ID for the <see cref="StudyPlace" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StudyPlaceId
    {
      get { return Get(ref _studyPlaceId, "StudyPlaceId"); }
      set { Set(ref _studyPlaceId, value, "StudyPlaceId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="DegreeEducationLevel" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeEducationLevelId
    {
      get { return Get(ref _degreeEducationLevelId, "DegreeEducationLevelId"); }
      set { Set(ref _degreeEducationLevelId, value, "DegreeEducationLevelId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.DegreeEducationLevelSkillView")]
  public partial class DegreeEducationLevelSkillView : Entity<long>
  {
    #region Fields
  
    private long _stateAreaId;
    private long _skillId;
    [ValueField]
    private Focus.Core.SkillTypes _skillType;
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _skillName;
    private System.Nullable<decimal> _demandPercentile;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the StateAreaId entity attribute.</summary>
    public const string StateAreaIdField = "StateAreaId";
    /// <summary>Identifies the SkillId entity attribute.</summary>
    public const string SkillIdField = "SkillId";
    /// <summary>Identifies the SkillType entity attribute.</summary>
    public const string SkillTypeField = "SkillType";
    /// <summary>Identifies the SkillName entity attribute.</summary>
    public const string SkillNameField = "SkillName";
    /// <summary>Identifies the DemandPercentile entity attribute.</summary>
    public const string DemandPercentileField = "DemandPercentile";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StateAreaId
    {
      get { return Get(ref _stateAreaId, "StateAreaId"); }
      set { Set(ref _stateAreaId, value, "StateAreaId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillId
    {
      get { return Get(ref _skillId, "SkillId"); }
      set { Set(ref _skillId, value, "SkillId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.SkillTypes SkillType
    {
      get { return Get(ref _skillType, "SkillType"); }
      set { Set(ref _skillType, value, "SkillType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SkillName
    {
      get { return Get(ref _skillName, "SkillName"); }
      set { Set(ref _skillName, value, "SkillName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> DemandPercentile
    {
      get { return Get(ref _demandPercentile, "DemandPercentile"); }
      set { Set(ref _demandPercentile, value, "DemandPercentile"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.DegreeEducationLevelView")]
  public partial class DegreeEducationLevelView : Entity<long>
  {
    #region Fields
  
    private long _degreeId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _degreeName;
    [ValueField]
    private Focus.Core.ExplorerEducationLevels _educationLevel;
    [ValidateLength(0, 200)]
    private string _degreeEducationLevelName;
    private bool _isDegreeArea;
    private bool _isClientData;
    [ValidateLength(0, 100)]
    private string _clientDataTag;
    private System.Nullable<int> _tier;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the DegreeId entity attribute.</summary>
    public const string DegreeIdField = "DegreeId";
    /// <summary>Identifies the DegreeName entity attribute.</summary>
    public const string DegreeNameField = "DegreeName";
    /// <summary>Identifies the EducationLevel entity attribute.</summary>
    public const string EducationLevelField = "EducationLevel";
    /// <summary>Identifies the DegreeEducationLevelName entity attribute.</summary>
    public const string DegreeEducationLevelNameField = "DegreeEducationLevelName";
    /// <summary>Identifies the IsDegreeArea entity attribute.</summary>
    public const string IsDegreeAreaField = "IsDegreeArea";
    /// <summary>Identifies the IsClientData entity attribute.</summary>
    public const string IsClientDataField = "IsClientData";
    /// <summary>Identifies the ClientDataTag entity attribute.</summary>
    public const string ClientDataTagField = "ClientDataTag";
    /// <summary>Identifies the Tier entity attribute.</summary>
    public const string TierField = "Tier";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeId
    {
      get { return Get(ref _degreeId, "DegreeId"); }
      set { Set(ref _degreeId, value, "DegreeId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string DegreeName
    {
      get { return Get(ref _degreeName, "DegreeName"); }
      set { Set(ref _degreeName, value, "DegreeName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ExplorerEducationLevels EducationLevel
    {
      get { return Get(ref _educationLevel, "EducationLevel"); }
      set { Set(ref _educationLevel, value, "EducationLevel"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string DegreeEducationLevelName
    {
      get { return Get(ref _degreeEducationLevelName, "DegreeEducationLevelName"); }
      set { Set(ref _degreeEducationLevelName, value, "DegreeEducationLevelName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsDegreeArea
    {
      get { return Get(ref _isDegreeArea, "IsDegreeArea"); }
      set { Set(ref _isDegreeArea, value, "IsDegreeArea"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsClientData
    {
      get { return Get(ref _isClientData, "IsClientData"); }
      set { Set(ref _isClientData, value, "IsClientData"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ClientDataTag
    {
      get { return Get(ref _clientDataTag, "ClientDataTag"); }
      set { Set(ref _clientDataTag, value, "ClientDataTag"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> Tier
    {
      get { return Get(ref _tier, "Tier"); }
      set { Set(ref _tier, value, "Tier"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobDegreeEducationLevelSkillView")]
  public partial class JobDegreeEducationLevelSkillView : Entity<long>
  {
    #region Fields
  
    private long _skillId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SkillId entity attribute.</summary>
    public const string SkillIdField = "SkillId";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillId
    {
      get { return Get(ref _skillId, "SkillId"); }
      set { Set(ref _skillId, value, "SkillId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobDegreeEducationLevelView")]
  public partial class JobDegreeEducationLevelView : Entity<long>
  {
    #region Fields
  
    private long _jobId;
    private long _degreeEducationLevelId;
    private long _degreeId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _degreeName;
    private int _degreesAwarded;
    [ValidateLength(0, 200)]
    private string _degreeEducationLevelName;
    [ValueField]
    private Focus.Core.ExplorerEducationLevels _educationLevel;
    private bool _isDegreeArea;
    private bool _isClientData;
    [ValidateLength(0, 100)]
    private string _clientDataTag;
    private bool _excludeFromJob;
    private System.Nullable<int> _tier;
    [ValidatePresence]
    [ValidateLength(0, 20)]
    private string _rOnet;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the DegreeEducationLevelId entity attribute.</summary>
    public const string DegreeEducationLevelIdField = "DegreeEducationLevelId";
    /// <summary>Identifies the DegreeId entity attribute.</summary>
    public const string DegreeIdField = "DegreeId";
    /// <summary>Identifies the DegreeName entity attribute.</summary>
    public const string DegreeNameField = "DegreeName";
    /// <summary>Identifies the DegreesAwarded entity attribute.</summary>
    public const string DegreesAwardedField = "DegreesAwarded";
    /// <summary>Identifies the DegreeEducationLevelName entity attribute.</summary>
    public const string DegreeEducationLevelNameField = "DegreeEducationLevelName";
    /// <summary>Identifies the EducationLevel entity attribute.</summary>
    public const string EducationLevelField = "EducationLevel";
    /// <summary>Identifies the IsDegreeArea entity attribute.</summary>
    public const string IsDegreeAreaField = "IsDegreeArea";
    /// <summary>Identifies the IsClientData entity attribute.</summary>
    public const string IsClientDataField = "IsClientData";
    /// <summary>Identifies the ClientDataTag entity attribute.</summary>
    public const string ClientDataTagField = "ClientDataTag";
    /// <summary>Identifies the ExcludeFromJob entity attribute.</summary>
    public const string ExcludeFromJobField = "ExcludeFromJob";
    /// <summary>Identifies the Tier entity attribute.</summary>
    public const string TierField = "Tier";
    /// <summary>Identifies the ROnet entity attribute.</summary>
    public const string ROnetField = "ROnet";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeEducationLevelId
    {
      get { return Get(ref _degreeEducationLevelId, "DegreeEducationLevelId"); }
      set { Set(ref _degreeEducationLevelId, value, "DegreeEducationLevelId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeId
    {
      get { return Get(ref _degreeId, "DegreeId"); }
      set { Set(ref _degreeId, value, "DegreeId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string DegreeName
    {
      get { return Get(ref _degreeName, "DegreeName"); }
      set { Set(ref _degreeName, value, "DegreeName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int DegreesAwarded
    {
      get { return Get(ref _degreesAwarded, "DegreesAwarded"); }
      set { Set(ref _degreesAwarded, value, "DegreesAwarded"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string DegreeEducationLevelName
    {
      get { return Get(ref _degreeEducationLevelName, "DegreeEducationLevelName"); }
      set { Set(ref _degreeEducationLevelName, value, "DegreeEducationLevelName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ExplorerEducationLevels EducationLevel
    {
      get { return Get(ref _educationLevel, "EducationLevel"); }
      set { Set(ref _educationLevel, value, "EducationLevel"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsDegreeArea
    {
      get { return Get(ref _isDegreeArea, "IsDegreeArea"); }
      set { Set(ref _isDegreeArea, value, "IsDegreeArea"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsClientData
    {
      get { return Get(ref _isClientData, "IsClientData"); }
      set { Set(ref _isClientData, value, "IsClientData"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ClientDataTag
    {
      get { return Get(ref _clientDataTag, "ClientDataTag"); }
      set { Set(ref _clientDataTag, value, "ClientDataTag"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool ExcludeFromJob
    {
      get { return Get(ref _excludeFromJob, "ExcludeFromJob"); }
      set { Set(ref _excludeFromJob, value, "ExcludeFromJob"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> Tier
    {
      get { return Get(ref _tier, "Tier"); }
      set { Set(ref _tier, value, "Tier"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ROnet
    {
      get { return Get(ref _rOnet, "ROnet"); }
      set { Set(ref _rOnet, value, "ROnet"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobCertificationView")]
  public partial class JobCertificationView : Entity<long>
  {
    #region Fields
  
    private long _jobId;
    private long _certificationId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _certificationName;
    private decimal _demandPercentile;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the CertificationId entity attribute.</summary>
    public const string CertificationIdField = "CertificationId";
    /// <summary>Identifies the CertificationName entity attribute.</summary>
    public const string CertificationNameField = "CertificationName";
    /// <summary>Identifies the DemandPercentile entity attribute.</summary>
    public const string DemandPercentileField = "DemandPercentile";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long CertificationId
    {
      get { return Get(ref _certificationId, "CertificationId"); }
      set { Set(ref _certificationId, value, "CertificationId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string CertificationName
    {
      get { return Get(ref _certificationName, "CertificationName"); }
      set { Set(ref _certificationName, value, "CertificationName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public decimal DemandPercentile
    {
      get { return Get(ref _demandPercentile, "DemandPercentile"); }
      set { Set(ref _demandPercentile, value, "DemandPercentile"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobDegreeEducationLevelReport", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class JobDegreeEducationLevelReport : Entity<long>
  {
    #region Fields
  
    private int _hiringDemand;
    private int _degreesAwarded;
    private long _stateAreaId;
    private long _jobDegreeEducationLevelId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the HiringDemand entity attribute.</summary>
    public const string HiringDemandField = "HiringDemand";
    /// <summary>Identifies the DegreesAwarded entity attribute.</summary>
    public const string DegreesAwardedField = "DegreesAwarded";
    /// <summary>Identifies the StateAreaId entity attribute.</summary>
    public const string StateAreaIdField = "StateAreaId";
    /// <summary>Identifies the JobDegreeEducationLevelId entity attribute.</summary>
    public const string JobDegreeEducationLevelIdField = "JobDegreeEducationLevelId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobDegreeEducationLevelReportsByStateArea")]
    private readonly EntityHolder<StateArea> _stateArea = new EntityHolder<StateArea>();
    [ReverseAssociation("JobDegreeEducationLevelReportsByJobDegreeEducationLevel")]
    private readonly EntityHolder<JobDegreeEducationLevel> _jobDegreeEducationLevel = new EntityHolder<JobDegreeEducationLevel>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public StateArea StateArea
    {
      get { return Get(_stateArea); }
      set { Set(_stateArea, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public JobDegreeEducationLevel JobDegreeEducationLevel
    {
      get { return Get(_jobDegreeEducationLevel); }
      set { Set(_jobDegreeEducationLevel, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int HiringDemand
    {
      get { return Get(ref _hiringDemand, "HiringDemand"); }
      set { Set(ref _hiringDemand, value, "HiringDemand"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int DegreesAwarded
    {
      get { return Get(ref _degreesAwarded, "DegreesAwarded"); }
      set { Set(ref _degreesAwarded, value, "DegreesAwarded"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="StateArea" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StateAreaId
    {
      get { return Get(ref _stateAreaId, "StateAreaId"); }
      set { Set(ref _stateAreaId, value, "StateAreaId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobDegreeEducationLevel" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobDegreeEducationLevelId
    {
      get { return Get(ref _jobDegreeEducationLevelId, "JobDegreeEducationLevelId"); }
      set { Set(ref _jobDegreeEducationLevelId, value, "JobDegreeEducationLevelId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobDegreeEducationLevelReportView")]
  public partial class JobDegreeEducationLevelReportView : Entity<long>
  {
    #region Fields
  
    private long _stateAreaId;
    private long _jobDegreeEducationLevelId;
    private int _hiringDemand;
    private int _degreesAwarded;
    private long _jobId;
    private long _degreeEducationLevelId;
    [ValueField]
    private Focus.Core.ExplorerEducationLevels _educationLevel;
    private long _degreeId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _degreeName;
    private bool _isDegreeArea;
    [ValidateLength(0, 200)]
    private string _degreeEducationLevelName;
    private bool _isClientData;
    [ValidateLength(0, 100)]
    private string _clientDataTag;
    private bool _excludeFromReport;
    private bool _excludeFromJob;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the StateAreaId entity attribute.</summary>
    public const string StateAreaIdField = "StateAreaId";
    /// <summary>Identifies the JobDegreeEducationLevelId entity attribute.</summary>
    public const string JobDegreeEducationLevelIdField = "JobDegreeEducationLevelId";
    /// <summary>Identifies the HiringDemand entity attribute.</summary>
    public const string HiringDemandField = "HiringDemand";
    /// <summary>Identifies the DegreesAwarded entity attribute.</summary>
    public const string DegreesAwardedField = "DegreesAwarded";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the DegreeEducationLevelId entity attribute.</summary>
    public const string DegreeEducationLevelIdField = "DegreeEducationLevelId";
    /// <summary>Identifies the EducationLevel entity attribute.</summary>
    public const string EducationLevelField = "EducationLevel";
    /// <summary>Identifies the DegreeId entity attribute.</summary>
    public const string DegreeIdField = "DegreeId";
    /// <summary>Identifies the DegreeName entity attribute.</summary>
    public const string DegreeNameField = "DegreeName";
    /// <summary>Identifies the IsDegreeArea entity attribute.</summary>
    public const string IsDegreeAreaField = "IsDegreeArea";
    /// <summary>Identifies the DegreeEducationLevelName entity attribute.</summary>
    public const string DegreeEducationLevelNameField = "DegreeEducationLevelName";
    /// <summary>Identifies the IsClientData entity attribute.</summary>
    public const string IsClientDataField = "IsClientData";
    /// <summary>Identifies the ClientDataTag entity attribute.</summary>
    public const string ClientDataTagField = "ClientDataTag";
    /// <summary>Identifies the ExcludeFromReport entity attribute.</summary>
    public const string ExcludeFromReportField = "ExcludeFromReport";
    /// <summary>Identifies the ExcludeFromJob entity attribute.</summary>
    public const string ExcludeFromJobField = "ExcludeFromJob";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StateAreaId
    {
      get { return Get(ref _stateAreaId, "StateAreaId"); }
      set { Set(ref _stateAreaId, value, "StateAreaId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobDegreeEducationLevelId
    {
      get { return Get(ref _jobDegreeEducationLevelId, "JobDegreeEducationLevelId"); }
      set { Set(ref _jobDegreeEducationLevelId, value, "JobDegreeEducationLevelId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int HiringDemand
    {
      get { return Get(ref _hiringDemand, "HiringDemand"); }
      set { Set(ref _hiringDemand, value, "HiringDemand"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int DegreesAwarded
    {
      get { return Get(ref _degreesAwarded, "DegreesAwarded"); }
      set { Set(ref _degreesAwarded, value, "DegreesAwarded"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeEducationLevelId
    {
      get { return Get(ref _degreeEducationLevelId, "DegreeEducationLevelId"); }
      set { Set(ref _degreeEducationLevelId, value, "DegreeEducationLevelId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ExplorerEducationLevels EducationLevel
    {
      get { return Get(ref _educationLevel, "EducationLevel"); }
      set { Set(ref _educationLevel, value, "EducationLevel"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeId
    {
      get { return Get(ref _degreeId, "DegreeId"); }
      set { Set(ref _degreeId, value, "DegreeId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string DegreeName
    {
      get { return Get(ref _degreeName, "DegreeName"); }
      set { Set(ref _degreeName, value, "DegreeName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsDegreeArea
    {
      get { return Get(ref _isDegreeArea, "IsDegreeArea"); }
      set { Set(ref _isDegreeArea, value, "IsDegreeArea"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string DegreeEducationLevelName
    {
      get { return Get(ref _degreeEducationLevelName, "DegreeEducationLevelName"); }
      set { Set(ref _degreeEducationLevelName, value, "DegreeEducationLevelName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsClientData
    {
      get { return Get(ref _isClientData, "IsClientData"); }
      set { Set(ref _isClientData, value, "IsClientData"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ClientDataTag
    {
      get { return Get(ref _clientDataTag, "ClientDataTag"); }
      set { Set(ref _clientDataTag, value, "ClientDataTag"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool ExcludeFromReport
    {
      get { return Get(ref _excludeFromReport, "ExcludeFromReport"); }
      set { Set(ref _excludeFromReport, value, "ExcludeFromReport"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool ExcludeFromJob
    {
      get { return Get(ref _excludeFromJob, "ExcludeFromJob"); }
      set { Set(ref _excludeFromJob, value, "ExcludeFromJob"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.DegreeEducationLevelExt", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class DegreeEducationLevelExt : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _name;
    [ValidatePresence]
    [ValidateUri]
    [ValidateLength(0, 200)]
    private string _url;
    private string _description;
    private long _degreeEducationLevelId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the Url entity attribute.</summary>
    public const string UrlField = "Url";
    /// <summary>Identifies the Description entity attribute.</summary>
    public const string DescriptionField = "Description";
    /// <summary>Identifies the DegreeEducationLevelId entity attribute.</summary>
    public const string DegreeEducationLevelIdField = "DegreeEducationLevelId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("DegreeEducationLevelExts")]
    private readonly EntityHolder<DegreeEducationLevel> _degreeEducationLevel = new EntityHolder<DegreeEducationLevel>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public DegreeEducationLevel DegreeEducationLevel
    {
      get { return Get(_degreeEducationLevel); }
      set { Set(_degreeEducationLevel, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Url
    {
      get { return Get(ref _url, "Url"); }
      set { Set(ref _url, value, "Url"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Description
    {
      get { return Get(ref _description, "Description"); }
      set { Set(ref _description, value, "Description"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="DegreeEducationLevel" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeEducationLevelId
    {
      get { return Get(ref _degreeEducationLevelId, "DegreeEducationLevelId"); }
      set { Set(ref _degreeEducationLevelId, value, "DegreeEducationLevelId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.DegreeEducationLevelStateAreaView")]
  public partial class DegreeEducationLevelStateAreaView : Entity<long>
  {
    #region Fields
  
    private long _degreeId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _degreeName;
    private bool _isDegreeArea;
    private bool _isClientData;
    private long _stateAreaId;
    [ValidateLength(0, 200)]
    private string _degreeEducationLevelName;
    [ValidateLength(0, 100)]
    private string _clientDataTag;
    [ValueField]
    private Focus.Core.ExplorerEducationLevels _educationLevel;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the DegreeId entity attribute.</summary>
    public const string DegreeIdField = "DegreeId";
    /// <summary>Identifies the DegreeName entity attribute.</summary>
    public const string DegreeNameField = "DegreeName";
    /// <summary>Identifies the IsDegreeArea entity attribute.</summary>
    public const string IsDegreeAreaField = "IsDegreeArea";
    /// <summary>Identifies the IsClientData entity attribute.</summary>
    public const string IsClientDataField = "IsClientData";
    /// <summary>Identifies the StateAreaId entity attribute.</summary>
    public const string StateAreaIdField = "StateAreaId";
    /// <summary>Identifies the DegreeEducationLevelName entity attribute.</summary>
    public const string DegreeEducationLevelNameField = "DegreeEducationLevelName";
    /// <summary>Identifies the ClientDataTag entity attribute.</summary>
    public const string ClientDataTagField = "ClientDataTag";
    /// <summary>Identifies the EducationLevel entity attribute.</summary>
    public const string EducationLevelField = "EducationLevel";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeId
    {
      get { return Get(ref _degreeId, "DegreeId"); }
      set { Set(ref _degreeId, value, "DegreeId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string DegreeName
    {
      get { return Get(ref _degreeName, "DegreeName"); }
      set { Set(ref _degreeName, value, "DegreeName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsDegreeArea
    {
      get { return Get(ref _isDegreeArea, "IsDegreeArea"); }
      set { Set(ref _isDegreeArea, value, "IsDegreeArea"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsClientData
    {
      get { return Get(ref _isClientData, "IsClientData"); }
      set { Set(ref _isClientData, value, "IsClientData"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StateAreaId
    {
      get { return Get(ref _stateAreaId, "StateAreaId"); }
      set { Set(ref _stateAreaId, value, "StateAreaId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string DegreeEducationLevelName
    {
      get { return Get(ref _degreeEducationLevelName, "DegreeEducationLevelName"); }
      set { Set(ref _degreeEducationLevelName, value, "DegreeEducationLevelName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ClientDataTag
    {
      get { return Get(ref _clientDataTag, "ClientDataTag"); }
      set { Set(ref _clientDataTag, value, "ClientDataTag"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ExplorerEducationLevels EducationLevel
    {
      get { return Get(ref _educationLevel, "EducationLevel"); }
      set { Set(ref _educationLevel, value, "EducationLevel"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobStateAreaView")]
  public partial class JobStateAreaView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _name;
    private long _stateAreaId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the StateAreaId entity attribute.</summary>
    public const string StateAreaIdField = "StateAreaId";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StateAreaId
    {
      get { return Get(ref _stateAreaId, "StateAreaId"); }
      set { Set(ref _stateAreaId, value, "StateAreaId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.DegreeAlias", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class DegreeAlias : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 400)]
    private string _aliasExtended;
    [ValueField]
    private Focus.Core.ExplorerEducationLevels _educationLevel;
    private string _alias;
    private long _degreeId;
    private long _degreeEducationLevelId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the AliasExtended entity attribute.</summary>
    public const string AliasExtendedField = "AliasExtended";
    /// <summary>Identifies the EducationLevel entity attribute.</summary>
    public const string EducationLevelField = "EducationLevel";
    /// <summary>Identifies the Alias entity attribute.</summary>
    public const string AliasField = "Alias";
    /// <summary>Identifies the DegreeId entity attribute.</summary>
    public const string DegreeIdField = "DegreeId";
    /// <summary>Identifies the DegreeEducationLevelId entity attribute.</summary>
    public const string DegreeEducationLevelIdField = "DegreeEducationLevelId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("DegreeAliases")]
    private readonly EntityHolder<Degree> _degree = new EntityHolder<Degree>();
    [ReverseAssociation("DegreeAliases")]
    private readonly EntityHolder<DegreeEducationLevel> _degreeEducationLevel = new EntityHolder<DegreeEducationLevel>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Degree Degree
    {
      get { return Get(_degree); }
      set { Set(_degree, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public DegreeEducationLevel DegreeEducationLevel
    {
      get { return Get(_degreeEducationLevel); }
      set { Set(_degreeEducationLevel, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string AliasExtended
    {
      get { return Get(ref _aliasExtended, "AliasExtended"); }
      set { Set(ref _aliasExtended, value, "AliasExtended"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ExplorerEducationLevels EducationLevel
    {
      get { return Get(ref _educationLevel, "EducationLevel"); }
      set { Set(ref _educationLevel, value, "EducationLevel"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Alias
    {
      get { return Get(ref _alias, "Alias"); }
      set { Set(ref _alias, value, "Alias"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Degree" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeId
    {
      get { return Get(ref _degreeId, "DegreeId"); }
      set { Set(ref _degreeId, value, "DegreeId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="DegreeEducationLevel" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeEducationLevelId
    {
      get { return Get(ref _degreeEducationLevelId, "DegreeEducationLevelId"); }
      set { Set(ref _degreeEducationLevelId, value, "DegreeEducationLevelId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.DegreeAliasView")]
  public partial class DegreeAliasView : Entity<long>
  {
    #region Fields
  
    [ValueField]
    private Focus.Core.ExplorerEducationLevels _educationLevel;
    private long _degreeId;
    private bool _isClientData;
    [ValidateLength(0, 100)]
    private string _clientDataTag;
    private long _degreeEducationLevelId;
    [ValidatePresence]
    [ValidateLength(0, 400)]
    private string _aliasExtended;
    [ValidatePresence]
    private string _alias;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the EducationLevel entity attribute.</summary>
    public const string EducationLevelField = "EducationLevel";
    /// <summary>Identifies the DegreeId entity attribute.</summary>
    public const string DegreeIdField = "DegreeId";
    /// <summary>Identifies the IsClientData entity attribute.</summary>
    public const string IsClientDataField = "IsClientData";
    /// <summary>Identifies the ClientDataTag entity attribute.</summary>
    public const string ClientDataTagField = "ClientDataTag";
    /// <summary>Identifies the DegreeEducationLevelId entity attribute.</summary>
    public const string DegreeEducationLevelIdField = "DegreeEducationLevelId";
    /// <summary>Identifies the AliasExtended entity attribute.</summary>
    public const string AliasExtendedField = "AliasExtended";
    /// <summary>Identifies the Alias entity attribute.</summary>
    public const string AliasField = "Alias";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ExplorerEducationLevels EducationLevel
    {
      get { return Get(ref _educationLevel, "EducationLevel"); }
      set { Set(ref _educationLevel, value, "EducationLevel"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeId
    {
      get { return Get(ref _degreeId, "DegreeId"); }
      set { Set(ref _degreeId, value, "DegreeId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsClientData
    {
      get { return Get(ref _isClientData, "IsClientData"); }
      set { Set(ref _isClientData, value, "IsClientData"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ClientDataTag
    {
      get { return Get(ref _clientDataTag, "ClientDataTag"); }
      set { Set(ref _clientDataTag, value, "ClientDataTag"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeEducationLevelId
    {
      get { return Get(ref _degreeEducationLevelId, "DegreeEducationLevelId"); }
      set { Set(ref _degreeEducationLevelId, value, "DegreeEducationLevelId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string AliasExtended
    {
      get { return Get(ref _aliasExtended, "AliasExtended"); }
      set { Set(ref _aliasExtended, value, "AliasExtended"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Alias
    {
      get { return Get(ref _alias, "Alias"); }
      set { Set(ref _alias, value, "Alias"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobCareerPathTo", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class JobCareerPathTo : Entity<long>
  {
    #region Fields
  
    private int _rank;
    private long _currentJobId;
    private long _next1JobId;
    private System.Nullable<long> _next2JobId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Rank entity attribute.</summary>
    public const string RankField = "Rank";
    /// <summary>Identifies the CurrentJobId entity attribute.</summary>
    public const string CurrentJobIdField = "CurrentJobId";
    /// <summary>Identifies the Next1JobId entity attribute.</summary>
    public const string Next1JobIdField = "Next1JobId";
    /// <summary>Identifies the Next2JobId entity attribute.</summary>
    public const string Next2JobIdField = "Next2JobId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobCareerPathTosByCurrentJob")]
    private readonly EntityHolder<Job> _currentJob = new EntityHolder<Job>();
    [ReverseAssociation("JobCareerPathTosByNext1Job")]
    private readonly EntityHolder<Job> _next1Job = new EntityHolder<Job>();
    [ReverseAssociation("JobCareerPathTosByNext2Job")]
    private readonly EntityHolder<Job> _next2Job = new EntityHolder<Job>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job CurrentJob
    {
      get { return Get(_currentJob); }
      set { Set(_currentJob, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Next1Job
    {
      get { return Get(_next1Job); }
      set { Set(_next1Job, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Next2Job
    {
      get { return Get(_next2Job); }
      set { Set(_next2Job, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Rank
    {
      get { return Get(ref _rank, "Rank"); }
      set { Set(ref _rank, value, "Rank"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="CurrentJob" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long CurrentJobId
    {
      get { return Get(ref _currentJobId, "CurrentJobId"); }
      set { Set(ref _currentJobId, value, "CurrentJobId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Next1Job" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long Next1JobId
    {
      get { return Get(ref _next1JobId, "Next1JobId"); }
      set { Set(ref _next1JobId, value, "Next1JobId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Next2Job" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> Next2JobId
    {
      get { return Get(ref _next2JobId, "Next2JobId"); }
      set { Set(ref _next2JobId, value, "Next2JobId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobCareerPathFrom", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class JobCareerPathFrom : Entity<long>
  {
    #region Fields
  
    private int _rank;
    private long _fromJobId;
    private long _toJobId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Rank entity attribute.</summary>
    public const string RankField = "Rank";
    /// <summary>Identifies the FromJobId entity attribute.</summary>
    public const string FromJobIdField = "FromJobId";
    /// <summary>Identifies the ToJobId entity attribute.</summary>
    public const string ToJobIdField = "ToJobId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobCareerPathFromsByFromJob")]
    private readonly EntityHolder<Job> _fromJob = new EntityHolder<Job>();
    [ReverseAssociation("JobCareerPathFromsByToJob")]
    private readonly EntityHolder<Job> _toJob = new EntityHolder<Job>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job FromJob
    {
      get { return Get(_fromJob); }
      set { Set(_fromJob, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Job ToJob
    {
      get { return Get(_toJob); }
      set { Set(_toJob, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Rank
    {
      get { return Get(ref _rank, "Rank"); }
      set { Set(ref _rank, value, "Rank"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="FromJob" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long FromJobId
    {
      get { return Get(ref _fromJobId, "FromJobId"); }
      set { Set(ref _fromJobId, value, "FromJobId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="ToJob" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long ToJobId
    {
      get { return Get(ref _toJobId, "ToJobId"); }
      set { Set(ref _toJobId, value, "ToJobId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.StudyPlaceDegreeEducationLevelView")]
  public partial class StudyPlaceDegreeEducationLevelView : Entity<long>
  {
    #region Fields
  
    private long _studyPlaceId;
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _studyPlaceName;
    private long _stateAreaId;
    private long _degreeEducationLevelId;
    private long _degreeId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _degreeName;
    private bool _isClientData;
    [ValidateLength(0, 100)]
    private string _clientDataTag;
    [ValidateLength(0, 200)]
    private string _degreeEducationLevelName;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the StudyPlaceId entity attribute.</summary>
    public const string StudyPlaceIdField = "StudyPlaceId";
    /// <summary>Identifies the StudyPlaceName entity attribute.</summary>
    public const string StudyPlaceNameField = "StudyPlaceName";
    /// <summary>Identifies the StateAreaId entity attribute.</summary>
    public const string StateAreaIdField = "StateAreaId";
    /// <summary>Identifies the DegreeEducationLevelId entity attribute.</summary>
    public const string DegreeEducationLevelIdField = "DegreeEducationLevelId";
    /// <summary>Identifies the DegreeId entity attribute.</summary>
    public const string DegreeIdField = "DegreeId";
    /// <summary>Identifies the DegreeName entity attribute.</summary>
    public const string DegreeNameField = "DegreeName";
    /// <summary>Identifies the IsClientData entity attribute.</summary>
    public const string IsClientDataField = "IsClientData";
    /// <summary>Identifies the ClientDataTag entity attribute.</summary>
    public const string ClientDataTagField = "ClientDataTag";
    /// <summary>Identifies the DegreeEducationLevelName entity attribute.</summary>
    public const string DegreeEducationLevelNameField = "DegreeEducationLevelName";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StudyPlaceId
    {
      get { return Get(ref _studyPlaceId, "StudyPlaceId"); }
      set { Set(ref _studyPlaceId, value, "StudyPlaceId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string StudyPlaceName
    {
      get { return Get(ref _studyPlaceName, "StudyPlaceName"); }
      set { Set(ref _studyPlaceName, value, "StudyPlaceName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StateAreaId
    {
      get { return Get(ref _stateAreaId, "StateAreaId"); }
      set { Set(ref _stateAreaId, value, "StateAreaId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeEducationLevelId
    {
      get { return Get(ref _degreeEducationLevelId, "DegreeEducationLevelId"); }
      set { Set(ref _degreeEducationLevelId, value, "DegreeEducationLevelId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeId
    {
      get { return Get(ref _degreeId, "DegreeId"); }
      set { Set(ref _degreeId, value, "DegreeId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string DegreeName
    {
      get { return Get(ref _degreeName, "DegreeName"); }
      set { Set(ref _degreeName, value, "DegreeName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsClientData
    {
      get { return Get(ref _isClientData, "IsClientData"); }
      set { Set(ref _isClientData, value, "IsClientData"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ClientDataTag
    {
      get { return Get(ref _clientDataTag, "ClientDataTag"); }
      set { Set(ref _clientDataTag, value, "ClientDataTag"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string DegreeEducationLevelName
    {
      get { return Get(ref _degreeEducationLevelName, "DegreeEducationLevelName"); }
      set { Set(ref _degreeEducationLevelName, value, "DegreeEducationLevelName"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobEmployerSkill", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class JobEmployerSkill : Entity<long>
  {
    #region Fields
  
    private int _skillCount;
    private int _rank;
    private long _jobId;
    private long _employerId;
    private long _skillId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SkillCount entity attribute.</summary>
    public const string SkillCountField = "SkillCount";
    /// <summary>Identifies the Rank entity attribute.</summary>
    public const string RankField = "Rank";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the SkillId entity attribute.</summary>
    public const string SkillIdField = "SkillId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobEmployerSkills")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();
    [ReverseAssociation("JobEmployerSkills")]
    private readonly EntityHolder<Employer> _employer = new EntityHolder<Employer>();
    [ReverseAssociation("JobEmployerSkills")]
    private readonly EntityHolder<Skill> _skill = new EntityHolder<Skill>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Employer Employer
    {
      get { return Get(_employer); }
      set { Set(_employer, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Skill Skill
    {
      get { return Get(_skill); }
      set { Set(_skill, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int SkillCount
    {
      get { return Get(ref _skillCount, "SkillCount"); }
      set { Set(ref _skillCount, value, "SkillCount"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Rank
    {
      get { return Get(ref _rank, "Rank"); }
      set { Set(ref _rank, value, "Rank"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Employer" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Skill" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillId
    {
      get { return Get(ref _skillId, "SkillId"); }
      set { Set(ref _skillId, value, "SkillId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobEmployerSkillView")]
  public partial class JobEmployerSkillView : Entity<long>
  {
    #region Fields
  
    private long _jobId;
    private long _employerId;
    private long _skillId;
    [ValueField]
    private Focus.Core.SkillTypes _skillType;
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _skillName;
    private int _skillCount;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the SkillId entity attribute.</summary>
    public const string SkillIdField = "SkillId";
    /// <summary>Identifies the SkillType entity attribute.</summary>
    public const string SkillTypeField = "SkillType";
    /// <summary>Identifies the SkillName entity attribute.</summary>
    public const string SkillNameField = "SkillName";
    /// <summary>Identifies the SkillCount entity attribute.</summary>
    public const string SkillCountField = "SkillCount";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillId
    {
      get { return Get(ref _skillId, "SkillId"); }
      set { Set(ref _skillId, value, "SkillId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.SkillTypes SkillType
    {
      get { return Get(ref _skillType, "SkillType"); }
      set { Set(ref _skillType, value, "SkillType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SkillName
    {
      get { return Get(ref _skillName, "SkillName"); }
      set { Set(ref _skillName, value, "SkillName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int SkillCount
    {
      get { return Get(ref _skillCount, "SkillCount"); }
      set { Set(ref _skillCount, value, "SkillCount"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.ProgramArea", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class ProgramArea : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 255)]
    private string _name;
    [ValidatePresence]
    [ValidateLength(0, 1000)]
    private string _description;
    private bool _isClientData;
    [ValidateLength(0, 255)]
    private string _clientDataTag;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the Description entity attribute.</summary>
    public const string DescriptionField = "Description";
    /// <summary>Identifies the IsClientData entity attribute.</summary>
    public const string IsClientDataField = "IsClientData";
    /// <summary>Identifies the ClientDataTag entity attribute.</summary>
    public const string ClientDataTagField = "ClientDataTag";


    #endregion
    
    #region Relationships

    [ReverseAssociation("ProgramArea")]
    private readonly EntityCollection<ProgramAreaDegree> _programAreaDegrees = new EntityCollection<ProgramAreaDegree>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<ProgramAreaDegree> ProgramAreaDegrees
    {
      get { return Get(_programAreaDegrees); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Description
    {
      get { return Get(ref _description, "Description"); }
      set { Set(ref _description, value, "Description"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsClientData
    {
      get { return Get(ref _isClientData, "IsClientData"); }
      set { Set(ref _isClientData, value, "IsClientData"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ClientDataTag
    {
      get { return Get(ref _clientDataTag, "ClientDataTag"); }
      set { Set(ref _clientDataTag, value, "ClientDataTag"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.ProgramAreaDegree", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class ProgramAreaDegree : Entity<long>
  {
    #region Fields
  
    private System.Nullable<long> _degreeId;
    private System.Nullable<long> _programAreaId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the DegreeId entity attribute.</summary>
    public const string DegreeIdField = "DegreeId";
    /// <summary>Identifies the ProgramAreaId entity attribute.</summary>
    public const string ProgramAreaIdField = "ProgramAreaId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("ProgramAreaDegrees")]
    private readonly EntityHolder<Degree> _degree = new EntityHolder<Degree>();
    [ReverseAssociation("ProgramAreaDegrees")]
    private readonly EntityHolder<ProgramArea> _programArea = new EntityHolder<ProgramArea>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Degree Degree
    {
      get { return Get(_degree); }
      set { Set(_degree, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ProgramArea ProgramArea
    {
      get { return Get(_programArea); }
      set { Set(_programArea, value); }
    }


    /// <summary>Gets or sets the ID for the <see cref="Degree" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> DegreeId
    {
      get { return Get(ref _degreeId, "DegreeId"); }
      set { Set(ref _degreeId, value, "DegreeId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="ProgramArea" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> ProgramAreaId
    {
      get { return Get(ref _programAreaId, "ProgramAreaId"); }
      set { Set(ref _programAreaId, value, "ProgramAreaId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.ProgramAreaDegreeView")]
  public partial class ProgramAreaDegreeView : Entity<long>
  {
    #region Fields
  
    private long _programAreaId;
    [ValidatePresence]
    [ValidateLength(0, 255)]
    private string _programAreaName;
    private long _degreeId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _degreeName;
    private bool _isClientData;
    [ValidateLength(0, 100)]
    private string _clientDataTag;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ProgramAreaId entity attribute.</summary>
    public const string ProgramAreaIdField = "ProgramAreaId";
    /// <summary>Identifies the ProgramAreaName entity attribute.</summary>
    public const string ProgramAreaNameField = "ProgramAreaName";
    /// <summary>Identifies the DegreeId entity attribute.</summary>
    public const string DegreeIdField = "DegreeId";
    /// <summary>Identifies the DegreeName entity attribute.</summary>
    public const string DegreeNameField = "DegreeName";
    /// <summary>Identifies the IsClientData entity attribute.</summary>
    public const string IsClientDataField = "IsClientData";
    /// <summary>Identifies the ClientDataTag entity attribute.</summary>
    public const string ClientDataTagField = "ClientDataTag";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long ProgramAreaId
    {
      get { return Get(ref _programAreaId, "ProgramAreaId"); }
      set { Set(ref _programAreaId, value, "ProgramAreaId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ProgramAreaName
    {
      get { return Get(ref _programAreaName, "ProgramAreaName"); }
      set { Set(ref _programAreaName, value, "ProgramAreaName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeId
    {
      get { return Get(ref _degreeId, "DegreeId"); }
      set { Set(ref _degreeId, value, "DegreeId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string DegreeName
    {
      get { return Get(ref _degreeName, "DegreeName"); }
      set { Set(ref _degreeName, value, "DegreeName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsClientData
    {
      get { return Get(ref _isClientData, "IsClientData"); }
      set { Set(ref _isClientData, value, "IsClientData"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ClientDataTag
    {
      get { return Get(ref _clientDataTag, "ClientDataTag"); }
      set { Set(ref _clientDataTag, value, "ClientDataTag"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.ProgramAreaDegreeEducationLevelView")]
  public partial class ProgramAreaDegreeEducationLevelView : Entity<System.Guid>
  {
    #region Fields
  
    private long _programAreaId;
    [ValidatePresence]
    [ValidateLength(0, 255)]
    private string _programAreaName;
    private long _degreeId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _degreeName;
    private long _degreeEducationLevelId;
    [ValueField]
    private Focus.Core.ExplorerEducationLevels _educationLevel;
    [ValidateLength(0, 200)]
    private string _degreeEducationLevelName;
    private bool _isClientData;
    [ValidateLength(0, 100)]
    private string _clientDataTag;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ProgramAreaId entity attribute.</summary>
    public const string ProgramAreaIdField = "ProgramAreaId";
    /// <summary>Identifies the ProgramAreaName entity attribute.</summary>
    public const string ProgramAreaNameField = "ProgramAreaName";
    /// <summary>Identifies the DegreeId entity attribute.</summary>
    public const string DegreeIdField = "DegreeId";
    /// <summary>Identifies the DegreeName entity attribute.</summary>
    public const string DegreeNameField = "DegreeName";
    /// <summary>Identifies the DegreeEducationLevelId entity attribute.</summary>
    public const string DegreeEducationLevelIdField = "DegreeEducationLevelId";
    /// <summary>Identifies the EducationLevel entity attribute.</summary>
    public const string EducationLevelField = "EducationLevel";
    /// <summary>Identifies the DegreeEducationLevelName entity attribute.</summary>
    public const string DegreeEducationLevelNameField = "DegreeEducationLevelName";
    /// <summary>Identifies the IsClientData entity attribute.</summary>
    public const string IsClientDataField = "IsClientData";
    /// <summary>Identifies the ClientDataTag entity attribute.</summary>
    public const string ClientDataTagField = "ClientDataTag";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long ProgramAreaId
    {
      get { return Get(ref _programAreaId, "ProgramAreaId"); }
      set { Set(ref _programAreaId, value, "ProgramAreaId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ProgramAreaName
    {
      get { return Get(ref _programAreaName, "ProgramAreaName"); }
      set { Set(ref _programAreaName, value, "ProgramAreaName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeId
    {
      get { return Get(ref _degreeId, "DegreeId"); }
      set { Set(ref _degreeId, value, "DegreeId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string DegreeName
    {
      get { return Get(ref _degreeName, "DegreeName"); }
      set { Set(ref _degreeName, value, "DegreeName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeEducationLevelId
    {
      get { return Get(ref _degreeEducationLevelId, "DegreeEducationLevelId"); }
      set { Set(ref _degreeEducationLevelId, value, "DegreeEducationLevelId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ExplorerEducationLevels EducationLevel
    {
      get { return Get(ref _educationLevel, "EducationLevel"); }
      set { Set(ref _educationLevel, value, "EducationLevel"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string DegreeEducationLevelName
    {
      get { return Get(ref _degreeEducationLevelName, "DegreeEducationLevelName"); }
      set { Set(ref _degreeEducationLevelName, value, "DegreeEducationLevelName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsClientData
    {
      get { return Get(ref _isClientData, "IsClientData"); }
      set { Set(ref _isClientData, value, "IsClientData"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ClientDataTag
    {
      get { return Get(ref _clientDataTag, "ClientDataTag"); }
      set { Set(ref _clientDataTag, value, "ClientDataTag"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.Onet", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class Onet : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 10)]
    private string _onetCode;
    [ValidateLength(0, 200)]
    private string _key;
    private long _jobFamilyId;
    private int _jobZone;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the OnetCode entity attribute.</summary>
    public const string OnetCodeField = "OnetCode";
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the JobFamilyId entity attribute.</summary>
    public const string JobFamilyIdField = "JobFamilyId";
    /// <summary>Identifies the JobZone entity attribute.</summary>
    public const string JobZoneField = "JobZone";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Onet")]
    private readonly EntityCollection<OnetTask> _onetTasks = new EntityCollection<OnetTask>();
    [ReverseAssociation("Onet")]
    private readonly EntityCollection<JobTask> _jobTasks = new EntityCollection<JobTask>();
    [ReverseAssociation("Onet")]
    private readonly EntityCollection<OnetWord> _onetWords = new EntityCollection<OnetWord>();
    [ReverseAssociation("Onet")]
    private readonly EntityCollection<OnetPhrase> _onetPhrases = new EntityCollection<OnetPhrase>();
    [ReverseAssociation("Onet")]
    private readonly EntityCollection<OnetCommodity> _onetCommodities = new EntityCollection<OnetCommodity>();
    [ReverseAssociation("Onet")]
    private readonly EntityCollection<OnetROnet> _onetROnets = new EntityCollection<OnetROnet>();
    [ReverseAssociation("Onet")]
    private readonly EntityCollection<OnetSkills> _onetSkills = new EntityCollection<OnetSkills>();
    [ReverseAssociation("Onet")]
    private readonly EntityCollection<Onet17Onet12Mapping> _onet17Onet12Mappings = new EntityCollection<Onet17Onet12Mapping>();
    [ReverseAssociation("Onet")]
    private readonly EntityCollection<ROnetOnet> _rOnetOnets = new EntityCollection<ROnetOnet>();
    [ReverseAssociation("Onet")]
    private readonly EntityCollection<OnetSOC> _onetSOCs = new EntityCollection<OnetSOC>();
    [ReverseAssociation("Onet")]
    private readonly EntityHolder<ROnet> _rOnet = new EntityHolder<ROnet>();

    private ThroughAssociation<OnetROnet, ROnet> _rOnets;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<OnetTask> OnetTasks
    {
      get { return Get(_onetTasks); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobTask> JobTasks
    {
      get { return Get(_jobTasks); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<OnetWord> OnetWords
    {
      get { return Get(_onetWords); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<OnetPhrase> OnetPhrases
    {
      get { return Get(_onetPhrases); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<OnetCommodity> OnetCommodities
    {
      get { return Get(_onetCommodities); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<OnetROnet> OnetROnets
    {
      get { return Get(_onetROnets); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<OnetSkills> OnetSkills
    {
      get { return Get(_onetSkills); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Onet17Onet12Mapping> Onet17Onet12Mappings
    {
      get { return Get(_onet17Onet12Mappings); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<ROnetOnet> ROnetOnets
    {
      get { return Get(_rOnetOnets); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<OnetSOC> OnetSOCs
    {
      get { return Get(_onetSOCs); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ROnet ROnet
    {
      get { return Get(_rOnet); }
      set { Set(_rOnet, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<OnetROnet, ROnet> ROnets
    {
      get
      {
        if (_rOnets == null)
        {
          _rOnets = new ThroughAssociation<OnetROnet, ROnet>(_onetROnets);
        }
        return Get(_rOnets);
      }
    }
    

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetCode
    {
      get { return Get(ref _onetCode, "OnetCode"); }
      set { Set(ref _onetCode, value, "OnetCode"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobFamilyId
    {
      get { return Get(ref _jobFamilyId, "JobFamilyId"); }
      set { Set(ref _jobFamilyId, value, "JobFamilyId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int JobZone
    {
      get { return Get(ref _jobZone, "JobZone"); }
      set { Set(ref _jobZone, value, "JobZone"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.OnetTask", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class OnetTask : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _key;
    private long _onetId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("OnetTasks")]
    private readonly EntityHolder<Onet> _onet = new EntityHolder<Onet>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Onet Onet
    {
      get { return Get(_onet); }
      set { Set(_onet, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Onet" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobTask", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class JobTask : Entity<long>
  {
    #region Fields
  
    [ValueField]
    private Focus.Core.JobTaskTypes _jobTaskType;
    [ValidateLength(0, 200)]
    private string _key;
    [ValueField]
    private Focus.Core.JobTaskScopes _scope;
    private bool _certificate;
    private long _onetId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobTaskType entity attribute.</summary>
    public const string JobTaskTypeField = "JobTaskType";
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the Scope entity attribute.</summary>
    public const string ScopeField = "Scope";
    /// <summary>Identifies the Certificate entity attribute.</summary>
    public const string CertificateField = "Certificate";
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobTask")]
    private readonly EntityCollection<JobTaskMultiOption> _jobTaskMultiOptions = new EntityCollection<JobTaskMultiOption>();
    [ReverseAssociation("JobTasks")]
    private readonly EntityHolder<Onet> _onet = new EntityHolder<Onet>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobTaskMultiOption> JobTaskMultiOptions
    {
      get { return Get(_jobTaskMultiOptions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Onet Onet
    {
      get { return Get(_onet); }
      set { Set(_onet, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.JobTaskTypes JobTaskType
    {
      get { return Get(ref _jobTaskType, "JobTaskType"); }
      set { Set(ref _jobTaskType, value, "JobTaskType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.JobTaskScopes Scope
    {
      get { return Get(ref _scope, "Scope"); }
      set { Set(ref _scope, value, "Scope"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool Certificate
    {
      get { return Get(ref _certificate, "Certificate"); }
      set { Set(ref _certificate, value, "Certificate"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Onet" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobTaskMultiOption", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class JobTaskMultiOption : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _key;
    private long _jobTaskId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the JobTaskId entity attribute.</summary>
    public const string JobTaskIdField = "JobTaskId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobTaskMultiOptions")]
    private readonly EntityHolder<JobTask> _jobTask = new EntityHolder<JobTask>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public JobTask JobTask
    {
      get { return Get(_jobTask); }
      set { Set(_jobTask, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobTask" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobTaskId
    {
      get { return Get(ref _jobTaskId, "JobTaskId"); }
      set { Set(ref _jobTaskId, value, "JobTaskId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.OnetLocalisationItem", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class OnetLocalisationItem : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _key;
    private string _primaryValue;
    private string _secondaryValue;
    private long _localisationId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the PrimaryValue entity attribute.</summary>
    public const string PrimaryValueField = "PrimaryValue";
    /// <summary>Identifies the SecondaryValue entity attribute.</summary>
    public const string SecondaryValueField = "SecondaryValue";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("OnetLocalisationItems")]
    private readonly EntityHolder<Localisation> _localisation = new EntityHolder<Localisation>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Localisation Localisation
    {
      get { return Get(_localisation); }
      set { Set(_localisation, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string PrimaryValue
    {
      get { return Get(ref _primaryValue, "PrimaryValue"); }
      set { Set(ref _primaryValue, value, "PrimaryValue"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SecondaryValue
    {
      get { return Get(ref _secondaryValue, "SecondaryValue"); }
      set { Set(ref _secondaryValue, value, "SecondaryValue"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Localisation" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobTaskLocalisationItem", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class JobTaskLocalisationItem : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _key;
    private string _primaryValue;
    private string _secondaryValue;
    private long _localisationId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the PrimaryValue entity attribute.</summary>
    public const string PrimaryValueField = "PrimaryValue";
    /// <summary>Identifies the SecondaryValue entity attribute.</summary>
    public const string SecondaryValueField = "SecondaryValue";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobTaskLocalisationItems")]
    private readonly EntityHolder<Localisation> _localisation = new EntityHolder<Localisation>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Localisation Localisation
    {
      get { return Get(_localisation); }
      set { Set(_localisation, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string PrimaryValue
    {
      get { return Get(ref _primaryValue, "PrimaryValue"); }
      set { Set(ref _primaryValue, value, "PrimaryValue"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SecondaryValue
    {
      get { return Get(ref _secondaryValue, "SecondaryValue"); }
      set { Set(ref _secondaryValue, value, "SecondaryValue"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Localisation" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.OnetWord", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class OnetWord : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _key;
    [ValidateLength(0, 30)]
    private string _word;
    [ValidateLength(0, 30)]
    private string _stem;
    private System.Nullable<int> _frequency;
    [ValidateLength(0, 10)]
    private string _onetSoc;
    private long _onetId;
    private long _onetRingId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the Word entity attribute.</summary>
    public const string WordField = "Word";
    /// <summary>Identifies the Stem entity attribute.</summary>
    public const string StemField = "Stem";
    /// <summary>Identifies the Frequency entity attribute.</summary>
    public const string FrequencyField = "Frequency";
    /// <summary>Identifies the OnetSoc entity attribute.</summary>
    public const string OnetSocField = "OnetSoc";
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";
    /// <summary>Identifies the OnetRingId entity attribute.</summary>
    public const string OnetRingIdField = "OnetRingId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("OnetWords")]
    private readonly EntityHolder<Onet> _onet = new EntityHolder<Onet>();
    [ReverseAssociation("OnetWords")]
    private readonly EntityHolder<OnetRing> _onetRing = new EntityHolder<OnetRing>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Onet Onet
    {
      get { return Get(_onet); }
      set { Set(_onet, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public OnetRing OnetRing
    {
      get { return Get(_onetRing); }
      set { Set(_onetRing, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Word
    {
      get { return Get(ref _word, "Word"); }
      set { Set(ref _word, value, "Word"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Stem
    {
      get { return Get(ref _stem, "Stem"); }
      set { Set(ref _stem, value, "Stem"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> Frequency
    {
      get { return Get(ref _frequency, "Frequency"); }
      set { Set(ref _frequency, value, "Frequency"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetSoc
    {
      get { return Get(ref _onetSoc, "OnetSoc"); }
      set { Set(ref _onetSoc, value, "OnetSoc"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Onet" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="OnetRing" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetRingId
    {
      get { return Get(ref _onetRingId, "OnetRingId"); }
      set { Set(ref _onetRingId, value, "OnetRingId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.OnetRing", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class OnetRing : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 30)]
    private string _name;
    private int _weighting;
    private int _ringMax;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the Weighting entity attribute.</summary>
    public const string WeightingField = "Weighting";
    /// <summary>Identifies the RingMax entity attribute.</summary>
    public const string RingMaxField = "RingMax";


    #endregion
    
    #region Relationships

    [ReverseAssociation("OnetRing")]
    private readonly EntityCollection<OnetWord> _onetWords = new EntityCollection<OnetWord>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<OnetWord> OnetWords
    {
      get { return Get(_onetWords); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Weighting
    {
      get { return Get(ref _weighting, "Weighting"); }
      set { Set(ref _weighting, value, "Weighting"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int RingMax
    {
      get { return Get(ref _ringMax, "RingMax"); }
      set { Set(ref _ringMax, value, "RingMax"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.OnetPhrase", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class OnetPhrase : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 500)]
    private string _phrase;
    private System.Nullable<int> _equivalent;
    [ValidateLength(0, 10)]
    private string _onetSoc;
    private long _onetId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Phrase entity attribute.</summary>
    public const string PhraseField = "Phrase";
    /// <summary>Identifies the Equivalent entity attribute.</summary>
    public const string EquivalentField = "Equivalent";
    /// <summary>Identifies the OnetSoc entity attribute.</summary>
    public const string OnetSocField = "OnetSoc";
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("OnetPhrases")]
    private readonly EntityHolder<Onet> _onet = new EntityHolder<Onet>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Onet Onet
    {
      get { return Get(_onet); }
      set { Set(_onet, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Phrase
    {
      get { return Get(ref _phrase, "Phrase"); }
      set { Set(ref _phrase, value, "Phrase"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> Equivalent
    {
      get { return Get(ref _equivalent, "Equivalent"); }
      set { Set(ref _equivalent, value, "Equivalent"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetSoc
    {
      get { return Get(ref _onetSoc, "OnetSoc"); }
      set { Set(ref _onetSoc, value, "OnetSoc"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Onet" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobTaskView")]
  public partial class JobTaskView : Entity<long>
  {
    #region Fields
  
    [ValueField]
    private Focus.Core.JobTaskTypes _jobTaskType;
    private long _onetId;
    [ValidatePresence]
    [ValidateLength(0, 5)]
    private string _culture;
    [ValidatePresence]
    private string _prompt;
    private string _response;
    [ValueField]
    private Focus.Core.JobTaskScopes _scope;
    private bool _certificate;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _onetCode;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobTaskType entity attribute.</summary>
    public const string JobTaskTypeField = "JobTaskType";
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";
    /// <summary>Identifies the Culture entity attribute.</summary>
    public const string CultureField = "Culture";
    /// <summary>Identifies the Prompt entity attribute.</summary>
    public const string PromptField = "Prompt";
    /// <summary>Identifies the Response entity attribute.</summary>
    public const string ResponseField = "Response";
    /// <summary>Identifies the Scope entity attribute.</summary>
    public const string ScopeField = "Scope";
    /// <summary>Identifies the Certificate entity attribute.</summary>
    public const string CertificateField = "Certificate";
    /// <summary>Identifies the OnetCode entity attribute.</summary>
    public const string OnetCodeField = "OnetCode";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.JobTaskTypes JobTaskType
    {
      get { return Get(ref _jobTaskType, "JobTaskType"); }
      set { Set(ref _jobTaskType, value, "JobTaskType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Culture
    {
      get { return Get(ref _culture, "Culture"); }
      set { Set(ref _culture, value, "Culture"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Prompt
    {
      get { return Get(ref _prompt, "Prompt"); }
      set { Set(ref _prompt, value, "Prompt"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Response
    {
      get { return Get(ref _response, "Response"); }
      set { Set(ref _response, value, "Response"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.JobTaskScopes Scope
    {
      get { return Get(ref _scope, "Scope"); }
      set { Set(ref _scope, value, "Scope"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool Certificate
    {
      get { return Get(ref _certificate, "Certificate"); }
      set { Set(ref _certificate, value, "Certificate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetCode
    {
      get { return Get(ref _onetCode, "OnetCode"); }
      set { Set(ref _onetCode, value, "OnetCode"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.JobTaskMultiOptionView")]
  public partial class JobTaskMultiOptionView : Entity<long>
  {
    #region Fields
  
    private long _onetId;
    [ValidatePresence]
    [ValidateLength(0, 5)]
    private string _culture;
    [ValidatePresence]
    private string _prompt;
    private long _jobTaskId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";
    /// <summary>Identifies the Culture entity attribute.</summary>
    public const string CultureField = "Culture";
    /// <summary>Identifies the Prompt entity attribute.</summary>
    public const string PromptField = "Prompt";
    /// <summary>Identifies the JobTaskId entity attribute.</summary>
    public const string JobTaskIdField = "JobTaskId";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Culture
    {
      get { return Get(ref _culture, "Culture"); }
      set { Set(ref _culture, value, "Culture"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Prompt
    {
      get { return Get(ref _prompt, "Prompt"); }
      set { Set(ref _prompt, value, "Prompt"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobTaskId
    {
      get { return Get(ref _jobTaskId, "JobTaskId"); }
      set { Set(ref _jobTaskId, value, "JobTaskId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.OnetView")]
  public partial class OnetView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _key;
    [ValidatePresence]
    private string _occupation;
    private long _jobFamilyId;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _onetCode;
    [ValidatePresence]
    [ValidateLength(0, 5)]
    private string _culture;
    private string _description;
    private bool _jobTasksAvailable;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the Occupation entity attribute.</summary>
    public const string OccupationField = "Occupation";
    /// <summary>Identifies the JobFamilyId entity attribute.</summary>
    public const string JobFamilyIdField = "JobFamilyId";
    /// <summary>Identifies the OnetCode entity attribute.</summary>
    public const string OnetCodeField = "OnetCode";
    /// <summary>Identifies the Culture entity attribute.</summary>
    public const string CultureField = "Culture";
    /// <summary>Identifies the Description entity attribute.</summary>
    public const string DescriptionField = "Description";
    /// <summary>Identifies the JobTasksAvailable entity attribute.</summary>
    public const string JobTasksAvailableField = "JobTasksAvailable";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Occupation
    {
      get { return Get(ref _occupation, "Occupation"); }
      set { Set(ref _occupation, value, "Occupation"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobFamilyId
    {
      get { return Get(ref _jobFamilyId, "JobFamilyId"); }
      set { Set(ref _jobFamilyId, value, "JobFamilyId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetCode
    {
      get { return Get(ref _onetCode, "OnetCode"); }
      set { Set(ref _onetCode, value, "OnetCode"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Culture
    {
      get { return Get(ref _culture, "Culture"); }
      set { Set(ref _culture, value, "Culture"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Description
    {
      get { return Get(ref _description, "Description"); }
      set { Set(ref _description, value, "Description"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool JobTasksAvailable
    {
      get { return Get(ref _jobTasksAvailable, "JobTasksAvailable"); }
      set { Set(ref _jobTasksAvailable, value, "JobTasksAvailable"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.NAICS", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class NAICS : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 6)]
    private string _code;
    [ValidateLength(0, 200)]
    private string _name;
    private long _parentId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Code entity attribute.</summary>
    public const string CodeField = "Code";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the ParentId entity attribute.</summary>
    public const string ParentIdField = "ParentId";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Code
    {
      get { return Get(ref _code, "Code"); }
      set { Set(ref _code, value, "Code"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long ParentId
    {
      get { return Get(ref _parentId, "ParentId"); }
      set { Set(ref _parentId, value, "ParentId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.OnetTaskView")]
  public partial class OnetTaskView : Entity<long>
  {
    #region Fields
  
    private long _onetId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _taskKey;
    [ValidatePresence]
    private string _task;
    [ValidatePresence]
    [ValidateLength(0, 5)]
    private string _culture;
    private string _taskPastTense;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";
    /// <summary>Identifies the TaskKey entity attribute.</summary>
    public const string TaskKeyField = "TaskKey";
    /// <summary>Identifies the Task entity attribute.</summary>
    public const string TaskField = "Task";
    /// <summary>Identifies the Culture entity attribute.</summary>
    public const string CultureField = "Culture";
    /// <summary>Identifies the TaskPastTense entity attribute.</summary>
    public const string TaskPastTenseField = "TaskPastTense";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string TaskKey
    {
      get { return Get(ref _taskKey, "TaskKey"); }
      set { Set(ref _taskKey, value, "TaskKey"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Task
    {
      get { return Get(ref _task, "Task"); }
      set { Set(ref _task, value, "Task"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Culture
    {
      get { return Get(ref _culture, "Culture"); }
      set { Set(ref _culture, value, "Culture"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string TaskPastTense
    {
      get { return Get(ref _taskPastTense, "TaskPastTense"); }
      set { Set(ref _taskPastTense, value, "TaskPastTense"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.PostalCode", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class PostalCode : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 8)]
    private string _code;
    private double _longitude;
    private double _latitude;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _countryKey;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _stateKey;
    [ValidateLength(0, 200)]
    private string _countyKey;
    [ValidateLength(0, 50)]
    private string _cityName;
    [ValidateLength(0, 200)]
    private string _officeKey;
    [ValidateLength(0, 200)]
    private string _wibLocationKey;
    [ValidateLength(0, 36)]
    private string _msaId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Code entity attribute.</summary>
    public const string CodeField = "Code";
    /// <summary>Identifies the Longitude entity attribute.</summary>
    public const string LongitudeField = "Longitude";
    /// <summary>Identifies the Latitude entity attribute.</summary>
    public const string LatitudeField = "Latitude";
    /// <summary>Identifies the CountryKey entity attribute.</summary>
    public const string CountryKeyField = "CountryKey";
    /// <summary>Identifies the StateKey entity attribute.</summary>
    public const string StateKeyField = "StateKey";
    /// <summary>Identifies the CountyKey entity attribute.</summary>
    public const string CountyKeyField = "CountyKey";
    /// <summary>Identifies the CityName entity attribute.</summary>
    public const string CityNameField = "CityName";
    /// <summary>Identifies the OfficeKey entity attribute.</summary>
    public const string OfficeKeyField = "OfficeKey";
    /// <summary>Identifies the WibLocationKey entity attribute.</summary>
    public const string WibLocationKeyField = "WibLocationKey";
    /// <summary>Identifies the MsaId entity attribute.</summary>
    public const string MsaIdField = "MsaId";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Code
    {
      get { return Get(ref _code, "Code"); }
      set { Set(ref _code, value, "Code"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public double Longitude
    {
      get { return Get(ref _longitude, "Longitude"); }
      set { Set(ref _longitude, value, "Longitude"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public double Latitude
    {
      get { return Get(ref _latitude, "Latitude"); }
      set { Set(ref _latitude, value, "Latitude"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string CountryKey
    {
      get { return Get(ref _countryKey, "CountryKey"); }
      set { Set(ref _countryKey, value, "CountryKey"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string StateKey
    {
      get { return Get(ref _stateKey, "StateKey"); }
      set { Set(ref _stateKey, value, "StateKey"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string CountyKey
    {
      get { return Get(ref _countyKey, "CountyKey"); }
      set { Set(ref _countyKey, value, "CountyKey"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string CityName
    {
      get { return Get(ref _cityName, "CityName"); }
      set { Set(ref _cityName, value, "CityName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string OfficeKey
    {
      get { return Get(ref _officeKey, "OfficeKey"); }
      set { Set(ref _officeKey, value, "OfficeKey"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string WibLocationKey
    {
      get { return Get(ref _wibLocationKey, "WibLocationKey"); }
      set { Set(ref _wibLocationKey, value, "WibLocationKey"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string MsaId
    {
      get { return Get(ref _msaId, "MsaId"); }
      set { Set(ref _msaId, value, "MsaId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.PostalCodeView")]
  public partial class PostalCodeView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 8)]
    private string _code;
    private double _longitude;
    private double _latitude;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _countryKey;
    [ValidatePresence]
    private string _countryName;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _stateKey;
    [ValidatePresence]
    private string _stateName;
    [ValidatePresence]
    [ValidateLength(0, 5)]
    private string _culture;
    [ValidateLength(0, 200)]
    private string _countyKey;
    private string _countyName;
    [ValidateLength(0, 50)]
    private string _cityName;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Code entity attribute.</summary>
    public const string CodeField = "Code";
    /// <summary>Identifies the Longitude entity attribute.</summary>
    public const string LongitudeField = "Longitude";
    /// <summary>Identifies the Latitude entity attribute.</summary>
    public const string LatitudeField = "Latitude";
    /// <summary>Identifies the CountryKey entity attribute.</summary>
    public const string CountryKeyField = "CountryKey";
    /// <summary>Identifies the CountryName entity attribute.</summary>
    public const string CountryNameField = "CountryName";
    /// <summary>Identifies the StateKey entity attribute.</summary>
    public const string StateKeyField = "StateKey";
    /// <summary>Identifies the StateName entity attribute.</summary>
    public const string StateNameField = "StateName";
    /// <summary>Identifies the Culture entity attribute.</summary>
    public const string CultureField = "Culture";
    /// <summary>Identifies the CountyKey entity attribute.</summary>
    public const string CountyKeyField = "CountyKey";
    /// <summary>Identifies the CountyName entity attribute.</summary>
    public const string CountyNameField = "CountyName";
    /// <summary>Identifies the CityName entity attribute.</summary>
    public const string CityNameField = "CityName";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Code
    {
      get { return Get(ref _code, "Code"); }
      set { Set(ref _code, value, "Code"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public double Longitude
    {
      get { return Get(ref _longitude, "Longitude"); }
      set { Set(ref _longitude, value, "Longitude"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public double Latitude
    {
      get { return Get(ref _latitude, "Latitude"); }
      set { Set(ref _latitude, value, "Latitude"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string CountryKey
    {
      get { return Get(ref _countryKey, "CountryKey"); }
      set { Set(ref _countryKey, value, "CountryKey"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string CountryName
    {
      get { return Get(ref _countryName, "CountryName"); }
      set { Set(ref _countryName, value, "CountryName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string StateKey
    {
      get { return Get(ref _stateKey, "StateKey"); }
      set { Set(ref _stateKey, value, "StateKey"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string StateName
    {
      get { return Get(ref _stateName, "StateName"); }
      set { Set(ref _stateName, value, "StateName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Culture
    {
      get { return Get(ref _culture, "Culture"); }
      set { Set(ref _culture, value, "Culture"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string CountyKey
    {
      get { return Get(ref _countyKey, "CountyKey"); }
      set { Set(ref _countyKey, value, "CountyKey"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string CountyName
    {
      get { return Get(ref _countyName, "CountyName"); }
      set { Set(ref _countyName, value, "CountyName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string CityName
    {
      get { return Get(ref _cityName, "CityName"); }
      set { Set(ref _cityName, value, "CityName"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.OnetWordJobTaskView")]
  public partial class OnetWordJobTaskView : Entity<long>
  {
    #region Fields
  
    private long _onetId;
    private long _onetRingId;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _onetSoc;
    [ValidatePresence]
    [ValidateLength(0, 30)]
    private string _stem;
    [ValidatePresence]
    [ValidateLength(0, 30)]
    private string _word;
    private long _jobFamilyId;
    private int _jobZone;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _onetKey;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _onetCode;
    private System.Nullable<int> _frequency;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";
    /// <summary>Identifies the OnetRingId entity attribute.</summary>
    public const string OnetRingIdField = "OnetRingId";
    /// <summary>Identifies the OnetSoc entity attribute.</summary>
    public const string OnetSocField = "OnetSoc";
    /// <summary>Identifies the Stem entity attribute.</summary>
    public const string StemField = "Stem";
    /// <summary>Identifies the Word entity attribute.</summary>
    public const string WordField = "Word";
    /// <summary>Identifies the JobFamilyId entity attribute.</summary>
    public const string JobFamilyIdField = "JobFamilyId";
    /// <summary>Identifies the JobZone entity attribute.</summary>
    public const string JobZoneField = "JobZone";
    /// <summary>Identifies the OnetKey entity attribute.</summary>
    public const string OnetKeyField = "OnetKey";
    /// <summary>Identifies the OnetCode entity attribute.</summary>
    public const string OnetCodeField = "OnetCode";
    /// <summary>Identifies the Frequency entity attribute.</summary>
    public const string FrequencyField = "Frequency";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetRingId
    {
      get { return Get(ref _onetRingId, "OnetRingId"); }
      set { Set(ref _onetRingId, value, "OnetRingId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetSoc
    {
      get { return Get(ref _onetSoc, "OnetSoc"); }
      set { Set(ref _onetSoc, value, "OnetSoc"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Stem
    {
      get { return Get(ref _stem, "Stem"); }
      set { Set(ref _stem, value, "Stem"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Word
    {
      get { return Get(ref _word, "Word"); }
      set { Set(ref _word, value, "Word"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobFamilyId
    {
      get { return Get(ref _jobFamilyId, "JobFamilyId"); }
      set { Set(ref _jobFamilyId, value, "JobFamilyId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int JobZone
    {
      get { return Get(ref _jobZone, "JobZone"); }
      set { Set(ref _jobZone, value, "JobZone"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetKey
    {
      get { return Get(ref _onetKey, "OnetKey"); }
      set { Set(ref _onetKey, value, "OnetKey"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetCode
    {
      get { return Get(ref _onetCode, "OnetCode"); }
      set { Set(ref _onetCode, value, "OnetCode"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> Frequency
    {
      get { return Get(ref _frequency, "Frequency"); }
      set { Set(ref _frequency, value, "Frequency"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.OnetWordView")]
  public partial class OnetWordView : Entity<long>
  {
    #region Fields
  
    private long _onetId;
    private long _onetRingId;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _onetSoc;
    [ValidatePresence]
    [ValidateLength(0, 30)]
    private string _stem;
    [ValidatePresence]
    [ValidateLength(0, 30)]
    private string _word;
    private long _jobFamilyId;
    private int _jobZone;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _onetKey;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _onetCode;
    private System.Nullable<int> _frequency;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";
    /// <summary>Identifies the OnetRingId entity attribute.</summary>
    public const string OnetRingIdField = "OnetRingId";
    /// <summary>Identifies the OnetSoc entity attribute.</summary>
    public const string OnetSocField = "OnetSoc";
    /// <summary>Identifies the Stem entity attribute.</summary>
    public const string StemField = "Stem";
    /// <summary>Identifies the Word entity attribute.</summary>
    public const string WordField = "Word";
    /// <summary>Identifies the JobFamilyId entity attribute.</summary>
    public const string JobFamilyIdField = "JobFamilyId";
    /// <summary>Identifies the JobZone entity attribute.</summary>
    public const string JobZoneField = "JobZone";
    /// <summary>Identifies the OnetKey entity attribute.</summary>
    public const string OnetKeyField = "OnetKey";
    /// <summary>Identifies the OnetCode entity attribute.</summary>
    public const string OnetCodeField = "OnetCode";
    /// <summary>Identifies the Frequency entity attribute.</summary>
    public const string FrequencyField = "Frequency";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetRingId
    {
      get { return Get(ref _onetRingId, "OnetRingId"); }
      set { Set(ref _onetRingId, value, "OnetRingId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetSoc
    {
      get { return Get(ref _onetSoc, "OnetSoc"); }
      set { Set(ref _onetSoc, value, "OnetSoc"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Stem
    {
      get { return Get(ref _stem, "Stem"); }
      set { Set(ref _stem, value, "Stem"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Word
    {
      get { return Get(ref _word, "Word"); }
      set { Set(ref _word, value, "Word"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobFamilyId
    {
      get { return Get(ref _jobFamilyId, "JobFamilyId"); }
      set { Set(ref _jobFamilyId, value, "JobFamilyId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int JobZone
    {
      get { return Get(ref _jobZone, "JobZone"); }
      set { Set(ref _jobZone, value, "JobZone"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetKey
    {
      get { return Get(ref _onetKey, "OnetKey"); }
      set { Set(ref _onetKey, value, "OnetKey"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetCode
    {
      get { return Get(ref _onetCode, "OnetCode"); }
      set { Set(ref _onetCode, value, "OnetCode"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> Frequency
    {
      get { return Get(ref _frequency, "Frequency"); }
      set { Set(ref _frequency, value, "Frequency"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.OnetCommodity", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class OnetCommodity : Entity<long>
  {
    #region Fields
  
    [ValueField]
    private Focus.Core.ToolTechnologyTypes _toolTechnologyType;
    private string _toolTechnologyExample;
    private string _code;
    private string _title;
    private long _onetId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ToolTechnologyType entity attribute.</summary>
    public const string ToolTechnologyTypeField = "ToolTechnologyType";
    /// <summary>Identifies the ToolTechnologyExample entity attribute.</summary>
    public const string ToolTechnologyExampleField = "ToolTechnologyExample";
    /// <summary>Identifies the Code entity attribute.</summary>
    public const string CodeField = "Code";
    /// <summary>Identifies the Title entity attribute.</summary>
    public const string TitleField = "Title";
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("OnetCommodities")]
    private readonly EntityHolder<Onet> _onet = new EntityHolder<Onet>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Onet Onet
    {
      get { return Get(_onet); }
      set { Set(_onet, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ToolTechnologyTypes ToolTechnologyType
    {
      get { return Get(ref _toolTechnologyType, "ToolTechnologyType"); }
      set { Set(ref _toolTechnologyType, value, "ToolTechnologyType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ToolTechnologyExample
    {
      get { return Get(ref _toolTechnologyExample, "ToolTechnologyExample"); }
      set { Set(ref _toolTechnologyExample, value, "ToolTechnologyExample"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Code
    {
      get { return Get(ref _code, "Code"); }
      set { Set(ref _code, value, "Code"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Title
    {
      get { return Get(ref _title, "Title"); }
      set { Set(ref _title, value, "Title"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Onet" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.StandardIndustrialClassification", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class StandardIndustrialClassification : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _name;
    [ValidateLength(0, 4)]
    private string _sicPrimary;
    [ValidateLength(0, 4)]
    private string _sicSecondary;
    [ValidateLength(0, 20)]
    private string _naicsPrimary;
    [ValidateLength(0, 20)]
    private string _naicsSecondary;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the SicPrimary entity attribute.</summary>
    public const string SicPrimaryField = "SicPrimary";
    /// <summary>Identifies the SicSecondary entity attribute.</summary>
    public const string SicSecondaryField = "SicSecondary";
    /// <summary>Identifies the NaicsPrimary entity attribute.</summary>
    public const string NaicsPrimaryField = "NaicsPrimary";
    /// <summary>Identifies the NaicsSecondary entity attribute.</summary>
    public const string NaicsSecondaryField = "NaicsSecondary";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SicPrimary
    {
      get { return Get(ref _sicPrimary, "SicPrimary"); }
      set { Set(ref _sicPrimary, value, "SicPrimary"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SicSecondary
    {
      get { return Get(ref _sicSecondary, "SicSecondary"); }
      set { Set(ref _sicSecondary, value, "SicSecondary"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string NaicsPrimary
    {
      get { return Get(ref _naicsPrimary, "NaicsPrimary"); }
      set { Set(ref _naicsPrimary, value, "NaicsPrimary"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string NaicsSecondary
    {
      get { return Get(ref _naicsSecondary, "NaicsSecondary"); }
      set { Set(ref _naicsSecondary, value, "NaicsSecondary"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.ROnet", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class ROnet : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 10)]
    private string _code;
    [ValidateLength(0, 200)]
    private string _key;
    private System.Nullable<long> _onetId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Code entity attribute.</summary>
    public const string CodeField = "Code";
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";


    #endregion
    
    #region Relationships

    [EagerLoad]
    [ReverseAssociation("ROnet")]
    private readonly EntityCollection<OnetROnet> _onetROnets = new EntityCollection<OnetROnet>();
    [ReverseAssociation("ROnet")]
    private readonly EntityCollection<MilitaryOccupationGroupROnet> _militaryOccupationGroupROnets = new EntityCollection<MilitaryOccupationGroupROnet>();
    [ReverseAssociation("ROnet")]
    private readonly EntityCollection<MilitaryOccupationROnet> _militaryOccupationROnets = new EntityCollection<MilitaryOccupationROnet>();
    [ReverseAssociation("ROnet")]
    private readonly EntityCollection<ROnetOnet> _rOnetOnets = new EntityCollection<ROnetOnet>();
    [ReverseAssociation("ROnet")]
    private readonly EntityHolder<Onet> _onet = new EntityHolder<Onet>();

    private ThroughAssociation<OnetROnet, Onet> _sourceOnets;
    private ThroughAssociation<MilitaryOccupationROnet, MilitaryOccupation> _militaryOccupations;
    private ThroughAssociation<MilitaryOccupationGroupROnet, MilitaryOccupationGroup> _militaryOccupationGroups;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<OnetROnet> OnetROnets
    {
      get { return Get(_onetROnets); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<MilitaryOccupationGroupROnet> MilitaryOccupationGroupROnets
    {
      get { return Get(_militaryOccupationGroupROnets); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<MilitaryOccupationROnet> MilitaryOccupationROnets
    {
      get { return Get(_militaryOccupationROnets); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<ROnetOnet> ROnetOnets
    {
      get { return Get(_rOnetOnets); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Onet Onet
    {
      get { return Get(_onet); }
      set { Set(_onet, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<OnetROnet, Onet> SourceOnets
    {
      get
      {
        if (_sourceOnets == null)
        {
          _sourceOnets = new ThroughAssociation<OnetROnet, Onet>(_onetROnets);
        }
        return Get(_sourceOnets);
      }
    }
    
    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<MilitaryOccupationROnet, MilitaryOccupation> MilitaryOccupations
    {
      get
      {
        if (_militaryOccupations == null)
        {
          _militaryOccupations = new ThroughAssociation<MilitaryOccupationROnet, MilitaryOccupation>(_militaryOccupationROnets);
        }
        return Get(_militaryOccupations);
      }
    }
    
    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<MilitaryOccupationGroupROnet, MilitaryOccupationGroup> MilitaryOccupationGroups
    {
      get
      {
        if (_militaryOccupationGroups == null)
        {
          _militaryOccupationGroups = new ThroughAssociation<MilitaryOccupationGroupROnet, MilitaryOccupationGroup>(_militaryOccupationGroupROnets);
        }
        return Get(_militaryOccupationGroups);
      }
    }
    

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Code
    {
      get { return Get(ref _code, "Code"); }
      set { Set(ref _code, value, "Code"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Onet" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.OnetROnet", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class OnetROnet : Entity<long>
  {
    #region Fields
  
    private bool _bestFit;
    private long _onetId;
    private long _rOnetId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the BestFit entity attribute.</summary>
    public const string BestFitField = "BestFit";
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";
    /// <summary>Identifies the ROnetId entity attribute.</summary>
    public const string ROnetIdField = "ROnetId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("OnetROnets")]
    private readonly EntityHolder<Onet> _onet = new EntityHolder<Onet>();
    [ReverseAssociation("OnetROnets")]
    private readonly EntityHolder<ROnet> _rOnet = new EntityHolder<ROnet>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Onet Onet
    {
      get { return Get(_onet); }
      set { Set(_onet, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ROnet ROnet
    {
      get { return Get(_rOnet); }
      set { Set(_rOnet, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool BestFit
    {
      get { return Get(ref _bestFit, "BestFit"); }
      set { Set(ref _bestFit, value, "BestFit"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Onet" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="ROnet" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long ROnetId
    {
      get { return Get(ref _rOnetId, "ROnetId"); }
      set { Set(ref _rOnetId, value, "ROnetId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.MilitaryOccupation", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class MilitaryOccupation : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 10)]
    private string _code;
    [ValidateLength(0, 200)]
    private string _key;
    private long _branchOfServiceId;
    private bool _isCommissioned;
    private long _militaryOccupationGroupId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Code entity attribute.</summary>
    public const string CodeField = "Code";
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the BranchOfServiceId entity attribute.</summary>
    public const string BranchOfServiceIdField = "BranchOfServiceId";
    /// <summary>Identifies the IsCommissioned entity attribute.</summary>
    public const string IsCommissionedField = "IsCommissioned";
    /// <summary>Identifies the MilitaryOccupationGroupId entity attribute.</summary>
    public const string MilitaryOccupationGroupIdField = "MilitaryOccupationGroupId";


    #endregion
    
    #region Relationships

    [EagerLoad]
    [ReverseAssociation("MilitaryOccupation")]
    private readonly EntityCollection<MilitaryOccupationROnet> _militaryOccupationROnets = new EntityCollection<MilitaryOccupationROnet>();
    [ReverseAssociation("MilitaryOccupations")]
    private readonly EntityHolder<MilitaryOccupationGroup> _militaryOccupationGroup = new EntityHolder<MilitaryOccupationGroup>();

    private ThroughAssociation<MilitaryOccupationROnet, ROnet> _rOnets;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<MilitaryOccupationROnet> MilitaryOccupationROnets
    {
      get { return Get(_militaryOccupationROnets); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public MilitaryOccupationGroup MilitaryOccupationGroup
    {
      get { return Get(_militaryOccupationGroup); }
      set { Set(_militaryOccupationGroup, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<MilitaryOccupationROnet, ROnet> ROnets
    {
      get
      {
        if (_rOnets == null)
        {
          _rOnets = new ThroughAssociation<MilitaryOccupationROnet, ROnet>(_militaryOccupationROnets);
        }
        return Get(_rOnets);
      }
    }
    

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Code
    {
      get { return Get(ref _code, "Code"); }
      set { Set(ref _code, value, "Code"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long BranchOfServiceId
    {
      get { return Get(ref _branchOfServiceId, "BranchOfServiceId"); }
      set { Set(ref _branchOfServiceId, value, "BranchOfServiceId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsCommissioned
    {
      get { return Get(ref _isCommissioned, "IsCommissioned"); }
      set { Set(ref _isCommissioned, value, "IsCommissioned"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="MilitaryOccupationGroup" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long MilitaryOccupationGroupId
    {
      get { return Get(ref _militaryOccupationGroupId, "MilitaryOccupationGroupId"); }
      set { Set(ref _militaryOccupationGroupId, value, "MilitaryOccupationGroupId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.MilitaryOccupationGroup", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class MilitaryOccupationGroup : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 50)]
    private string _name;
    private bool _isCommissioned;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the IsCommissioned entity attribute.</summary>
    public const string IsCommissionedField = "IsCommissioned";


    #endregion
    
    #region Relationships

    [ReverseAssociation("MilitaryOccupationGroup")]
    private readonly EntityCollection<MilitaryOccupation> _militaryOccupations = new EntityCollection<MilitaryOccupation>();
    [EagerLoad]
    [ReverseAssociation("MilitaryOccupationGroup")]
    private readonly EntityCollection<MilitaryOccupationGroupROnet> _militaryOccupationGroupROnets = new EntityCollection<MilitaryOccupationGroupROnet>();

    private ThroughAssociation<MilitaryOccupationGroupROnet, ROnet> _rOnets;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<MilitaryOccupation> MilitaryOccupations
    {
      get { return Get(_militaryOccupations); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<MilitaryOccupationGroupROnet> MilitaryOccupationGroupROnets
    {
      get { return Get(_militaryOccupationGroupROnets); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<MilitaryOccupationGroupROnet, ROnet> ROnets
    {
      get
      {
        if (_rOnets == null)
        {
          _rOnets = new ThroughAssociation<MilitaryOccupationGroupROnet, ROnet>(_militaryOccupationGroupROnets);
        }
        return Get(_rOnets);
      }
    }
    

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsCommissioned
    {
      get { return Get(ref _isCommissioned, "IsCommissioned"); }
      set { Set(ref _isCommissioned, value, "IsCommissioned"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.MilitaryOccupationJobTitleView")]
  public partial class MilitaryOccupationJobTitleView : Entity<long>
  {
    #region Fields
  
    private long _militaryOccupationId;
    [ValidatePresence]
    private string _jobTitle;
    private long _branchOfServiceId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the MilitaryOccupationId entity attribute.</summary>
    public const string MilitaryOccupationIdField = "MilitaryOccupationId";
    /// <summary>Identifies the JobTitle entity attribute.</summary>
    public const string JobTitleField = "JobTitle";
    /// <summary>Identifies the BranchOfServiceId entity attribute.</summary>
    public const string BranchOfServiceIdField = "BranchOfServiceId";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long MilitaryOccupationId
    {
      get { return Get(ref _militaryOccupationId, "MilitaryOccupationId"); }
      set { Set(ref _militaryOccupationId, value, "MilitaryOccupationId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string JobTitle
    {
      get { return Get(ref _jobTitle, "JobTitle"); }
      set { Set(ref _jobTitle, value, "JobTitle"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long BranchOfServiceId
    {
      get { return Get(ref _branchOfServiceId, "BranchOfServiceId"); }
      set { Set(ref _branchOfServiceId, value, "BranchOfServiceId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.OnetSkills", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class OnetSkills : Entity<long>
  {
    #region Fields
  
    private string _skill;
    private string _skillType;
    private long _onetId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Skill entity attribute.</summary>
    public const string SkillField = "Skill";
    /// <summary>Identifies the SkillType entity attribute.</summary>
    public const string SkillTypeField = "SkillType";
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("OnetSkills")]
    private readonly EntityHolder<Onet> _onet = new EntityHolder<Onet>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Onet Onet
    {
      get { return Get(_onet); }
      set { Set(_onet, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Skill
    {
      get { return Get(ref _skill, "Skill"); }
      set { Set(ref _skill, value, "Skill"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SkillType
    {
      get { return Get(ref _skillType, "SkillType"); }
      set { Set(ref _skillType, value, "SkillType"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Onet" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.MilitaryOccupationGroupROnet", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class MilitaryOccupationGroupROnet : Entity<long>
  {
    #region Fields
  
    private long _rOnetId;
    private long _militaryOccupationGroupId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ROnetId entity attribute.</summary>
    public const string ROnetIdField = "ROnetId";
    /// <summary>Identifies the MilitaryOccupationGroupId entity attribute.</summary>
    public const string MilitaryOccupationGroupIdField = "MilitaryOccupationGroupId";


    #endregion
    
    #region Relationships

    [EagerLoad]
    [ReverseAssociation("MilitaryOccupationGroupROnets")]
    private readonly EntityHolder<ROnet> _rOnet = new EntityHolder<ROnet>();
    [ReverseAssociation("MilitaryOccupationGroupROnets")]
    private readonly EntityHolder<MilitaryOccupationGroup> _militaryOccupationGroup = new EntityHolder<MilitaryOccupationGroup>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public ROnet ROnet
    {
      get { return Get(_rOnet); }
      set { Set(_rOnet, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public MilitaryOccupationGroup MilitaryOccupationGroup
    {
      get { return Get(_militaryOccupationGroup); }
      set { Set(_militaryOccupationGroup, value); }
    }


    /// <summary>Gets or sets the ID for the <see cref="ROnet" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long ROnetId
    {
      get { return Get(ref _rOnetId, "ROnetId"); }
      set { Set(ref _rOnetId, value, "ROnetId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="MilitaryOccupationGroup" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long MilitaryOccupationGroupId
    {
      get { return Get(ref _militaryOccupationGroupId, "MilitaryOccupationGroupId"); }
      set { Set(ref _militaryOccupationGroupId, value, "MilitaryOccupationGroupId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.MilitaryOccupationROnet", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class MilitaryOccupationROnet : Entity<long>
  {
    #region Fields
  
    private long _rOnetId;
    private long _militaryOccupationId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ROnetId entity attribute.</summary>
    public const string ROnetIdField = "ROnetId";
    /// <summary>Identifies the MilitaryOccupationId entity attribute.</summary>
    public const string MilitaryOccupationIdField = "MilitaryOccupationId";


    #endregion
    
    #region Relationships

    [EagerLoad]
    [ReverseAssociation("MilitaryOccupationROnets")]
    private readonly EntityHolder<ROnet> _rOnet = new EntityHolder<ROnet>();
    [ReverseAssociation("MilitaryOccupationROnets")]
    private readonly EntityHolder<MilitaryOccupation> _militaryOccupation = new EntityHolder<MilitaryOccupation>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public ROnet ROnet
    {
      get { return Get(_rOnet); }
      set { Set(_rOnet, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public MilitaryOccupation MilitaryOccupation
    {
      get { return Get(_militaryOccupation); }
      set { Set(_militaryOccupation, value); }
    }


    /// <summary>Gets or sets the ID for the <see cref="ROnet" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long ROnetId
    {
      get { return Get(ref _rOnetId, "ROnetId"); }
      set { Set(ref _rOnetId, value, "ROnetId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="MilitaryOccupation" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long MilitaryOccupationId
    {
      get { return Get(ref _militaryOccupationId, "MilitaryOccupationId"); }
      set { Set(ref _militaryOccupationId, value, "MilitaryOccupationId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.LocalisationItem", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class LocalisationItem : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _key;
    private string _value;
    private long _localisationId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the Value entity attribute.</summary>
    public const string ValueField = "Value";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("LocalisationItems")]
    private readonly EntityHolder<Localisation> _localisation = new EntityHolder<Localisation>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Localisation Localisation
    {
      get { return Get(_localisation); }
      set { Set(_localisation, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Value
    {
      get { return Get(ref _value, "Value"); }
      set { Set(ref _value, value, "Value"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Localisation" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.GenericJobTitle", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class GenericJobTitle : Entity<long>
  {
    #region Fields
  
    [ValidateFormat("<=150")]
    private string _value;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Value entity attribute.</summary>
    public const string ValueField = "Value";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Value
    {
      get { return Get(ref _value, "Value"); }
      set { Set(ref _value, value, "Value"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.ResumeSkill", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class ResumeSkill : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _name;
    [ValueField]
    private Focus.Core.SkillType _type;
    [ValueField]
    private Focus.Core.SkillCategories _category;
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _stem;
    private int _noiseSkill;
    private int _cluster;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the Type entity attribute.</summary>
    public const string TypeField = "Type";
    /// <summary>Identifies the Category entity attribute.</summary>
    public const string CategoryField = "Category";
    /// <summary>Identifies the Stem entity attribute.</summary>
    public const string StemField = "Stem";
    /// <summary>Identifies the NoiseSkill entity attribute.</summary>
    public const string NoiseSkillField = "NoiseSkill";
    /// <summary>Identifies the Cluster entity attribute.</summary>
    public const string ClusterField = "Cluster";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.SkillType Type
    {
      get { return Get(ref _type, "Type"); }
      set { Set(ref _type, value, "Type"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.SkillCategories Category
    {
      get { return Get(ref _category, "Category"); }
      set { Set(ref _category, value, "Category"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Stem
    {
      get { return Get(ref _stem, "Stem"); }
      set { Set(ref _stem, value, "Stem"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int NoiseSkill
    {
      get { return Get(ref _noiseSkill, "NoiseSkill"); }
      set { Set(ref _noiseSkill, value, "NoiseSkill"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Cluster
    {
      get { return Get(ref _cluster, "Cluster"); }
      set { Set(ref _cluster, value, "Cluster"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.EducationInternshipCategory", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class EducationInternshipCategory : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 100)]
    private string _name;
    private long _localisationId;
    [ValueField]
    private Focus.Core.EducationSkillCategories _categoryType;
    [ValidateLength(0, 200)]
    private string _pastTenseStatement;
    [ValidateLength(0, 200)]
    private string _presentTenseStatement;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";
    /// <summary>Identifies the CategoryType entity attribute.</summary>
    public const string CategoryTypeField = "CategoryType";
    /// <summary>Identifies the PastTenseStatement entity attribute.</summary>
    public const string PastTenseStatementField = "PastTenseStatement";
    /// <summary>Identifies the PresentTenseStatement entity attribute.</summary>
    public const string PresentTenseStatementField = "PresentTenseStatement";


    #endregion
    
    #region Relationships

    [ReverseAssociation("EducationInternshipCategory")]
    private readonly EntityCollection<EducationInternshipSkill> _educationInternshipSkills = new EntityCollection<EducationInternshipSkill>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<EducationInternshipSkill> EducationInternshipSkills
    {
      get { return Get(_educationInternshipSkills); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.EducationSkillCategories CategoryType
    {
      get { return Get(ref _categoryType, "CategoryType"); }
      set { Set(ref _categoryType, value, "CategoryType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string PastTenseStatement
    {
      get { return Get(ref _pastTenseStatement, "PastTenseStatement"); }
      set { Set(ref _pastTenseStatement, value, "PastTenseStatement"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string PresentTenseStatement
    {
      get { return Get(ref _presentTenseStatement, "PresentTenseStatement"); }
      set { Set(ref _presentTenseStatement, value, "PresentTenseStatement"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.EducationInternshipSkill", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class EducationInternshipSkill : Entity<long>
  {
    #region Fields
  
    private string _name;
    private long _localisationId;
    private long _educationInternshipCategoryId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";
    /// <summary>Identifies the EducationInternshipCategoryId entity attribute.</summary>
    public const string EducationInternshipCategoryIdField = "EducationInternshipCategoryId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("EducationInternshipSkill")]
    private readonly EntityCollection<EducationInternshipStatement> _educationInternshipStatements = new EntityCollection<EducationInternshipStatement>();
    [ReverseAssociation("EducationInternshipSkills")]
    private readonly EntityHolder<EducationInternshipCategory> _educationInternshipCategory = new EntityHolder<EducationInternshipCategory>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<EducationInternshipStatement> EducationInternshipStatements
    {
      get { return Get(_educationInternshipStatements); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EducationInternshipCategory EducationInternshipCategory
    {
      get { return Get(_educationInternshipCategory); }
      set { Set(_educationInternshipCategory, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="EducationInternshipCategory" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long EducationInternshipCategoryId
    {
      get { return Get(ref _educationInternshipCategoryId, "EducationInternshipCategoryId"); }
      set { Set(ref _educationInternshipCategoryId, value, "EducationInternshipCategoryId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.EducationInternshipStatement", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class EducationInternshipStatement : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _pastTenseStatement;
    private long _localisationId;
    [ValidateLength(0, 200)]
    private string _presentTenseStatement;
    private long _educationInternshipSkillId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the PastTenseStatement entity attribute.</summary>
    public const string PastTenseStatementField = "PastTenseStatement";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";
    /// <summary>Identifies the PresentTenseStatement entity attribute.</summary>
    public const string PresentTenseStatementField = "PresentTenseStatement";
    /// <summary>Identifies the EducationInternshipSkillId entity attribute.</summary>
    public const string EducationInternshipSkillIdField = "EducationInternshipSkillId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("EducationInternshipStatements")]
    private readonly EntityHolder<EducationInternshipSkill> _educationInternshipSkill = new EntityHolder<EducationInternshipSkill>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EducationInternshipSkill EducationInternshipSkill
    {
      get { return Get(_educationInternshipSkill); }
      set { Set(_educationInternshipSkill, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string PastTenseStatement
    {
      get { return Get(ref _pastTenseStatement, "PastTenseStatement"); }
      set { Set(ref _pastTenseStatement, value, "PastTenseStatement"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string PresentTenseStatement
    {
      get { return Get(ref _presentTenseStatement, "PresentTenseStatement"); }
      set { Set(ref _presentTenseStatement, value, "PresentTenseStatement"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="EducationInternshipSkill" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long EducationInternshipSkillId
    {
      get { return Get(ref _educationInternshipSkillId, "EducationInternshipSkillId"); }
      set { Set(ref _educationInternshipSkillId, value, "EducationInternshipSkillId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.SkillsForJobsView")]
  public partial class SkillsForJobsView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _name;
    [ValueField]
    private Focus.Core.SkillTypes _skillType;
    private System.Nullable<int> _rank;
    private System.Nullable<decimal> _demandPercentile;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the SkillType entity attribute.</summary>
    public const string SkillTypeField = "SkillType";
    /// <summary>Identifies the Rank entity attribute.</summary>
    public const string RankField = "Rank";
    /// <summary>Identifies the DemandPercentile entity attribute.</summary>
    public const string DemandPercentileField = "DemandPercentile";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.SkillTypes SkillType
    {
      get { return Get(ref _skillType, "SkillType"); }
      set { Set(ref _skillType, value, "SkillType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> Rank
    {
      get { return Get(ref _rank, "Rank"); }
      set { Set(ref _rank, value, "Rank"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> DemandPercentile
    {
      get { return Get(ref _demandPercentile, "DemandPercentile"); }
      set { Set(ref _demandPercentile, value, "DemandPercentile"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.DegreeEducationLevelROnetView")]
  public partial class DegreeEducationLevelROnetView : Entity<long>
  {
    #region Fields
  
    private long _degreeEducationLevelId;
    [ValidatePresence]
    [ValidateLength(0, 20)]
    private string _rOnet;
    private System.Nullable<long> _programAreaId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the DegreeEducationLevelId entity attribute.</summary>
    public const string DegreeEducationLevelIdField = "DegreeEducationLevelId";
    /// <summary>Identifies the ROnet entity attribute.</summary>
    public const string ROnetField = "ROnet";
    /// <summary>Identifies the ProgramAreaId entity attribute.</summary>
    public const string ProgramAreaIdField = "ProgramAreaId";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeEducationLevelId
    {
      get { return Get(ref _degreeEducationLevelId, "DegreeEducationLevelId"); }
      set { Set(ref _degreeEducationLevelId, value, "DegreeEducationLevelId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ROnet
    {
      get { return Get(ref _rOnet, "ROnet"); }
      set { Set(ref _rOnet, value, "ROnet"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> ProgramAreaId
    {
      get { return Get(ref _programAreaId, "ProgramAreaId"); }
      set { Set(ref _programAreaId, value, "ProgramAreaId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.CareerAreaJobDegreeView")]
  public partial class CareerAreaJobDegreeView : Entity<System.Guid>
  {
    #region Fields
  
    private long _careerAreaId;
    private long _jobId;
    private long _degreeEducationLevelId;
    [ValueField]
    private Focus.Core.ExplorerEducationLevels _educationLevel;
    private long _degreeId;
    private bool _isClientData;
    [ValidateLength(0, 100)]
    private string _clientDataTag;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the CareerAreaId entity attribute.</summary>
    public const string CareerAreaIdField = "CareerAreaId";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the DegreeEducationLevelId entity attribute.</summary>
    public const string DegreeEducationLevelIdField = "DegreeEducationLevelId";
    /// <summary>Identifies the EducationLevel entity attribute.</summary>
    public const string EducationLevelField = "EducationLevel";
    /// <summary>Identifies the DegreeId entity attribute.</summary>
    public const string DegreeIdField = "DegreeId";
    /// <summary>Identifies the IsClientData entity attribute.</summary>
    public const string IsClientDataField = "IsClientData";
    /// <summary>Identifies the ClientDataTag entity attribute.</summary>
    public const string ClientDataTagField = "ClientDataTag";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long CareerAreaId
    {
      get { return Get(ref _careerAreaId, "CareerAreaId"); }
      set { Set(ref _careerAreaId, value, "CareerAreaId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeEducationLevelId
    {
      get { return Get(ref _degreeEducationLevelId, "DegreeEducationLevelId"); }
      set { Set(ref _degreeEducationLevelId, value, "DegreeEducationLevelId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ExplorerEducationLevels EducationLevel
    {
      get { return Get(ref _educationLevel, "EducationLevel"); }
      set { Set(ref _educationLevel, value, "EducationLevel"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeId
    {
      get { return Get(ref _degreeId, "DegreeId"); }
      set { Set(ref _degreeId, value, "DegreeId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsClientData
    {
      get { return Get(ref _isClientData, "IsClientData"); }
      set { Set(ref _isClientData, value, "IsClientData"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ClientDataTag
    {
      get { return Get(ref _clientDataTag, "ClientDataTag"); }
      set { Set(ref _clientDataTag, value, "ClientDataTag"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.DegreeEducationLevelLensMapping")]
  public partial class DegreeEducationLevelLensMapping : Entity<int>
  {
    #region Fields
  
    private int _lensId;
    private long _degreeEducationLevelId;
    [ValidateLength(0, 10)]
    private string _rcipCode;
    private System.Nullable<long> _programAreaId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the LensId entity attribute.</summary>
    public const string LensIdField = "LensId";
    /// <summary>Identifies the DegreeEducationLevelId entity attribute.</summary>
    public const string DegreeEducationLevelIdField = "DegreeEducationLevelId";
    /// <summary>Identifies the RcipCode entity attribute.</summary>
    public const string RcipCodeField = "RcipCode";
    /// <summary>Identifies the ProgramAreaId entity attribute.</summary>
    public const string ProgramAreaIdField = "ProgramAreaId";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int LensId
    {
      get { return Get(ref _lensId, "LensId"); }
      set { Set(ref _lensId, value, "LensId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long DegreeEducationLevelId
    {
      get { return Get(ref _degreeEducationLevelId, "DegreeEducationLevelId"); }
      set { Set(ref _degreeEducationLevelId, value, "DegreeEducationLevelId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string RcipCode
    {
      get { return Get(ref _rcipCode, "RcipCode"); }
      set { Set(ref _rcipCode, value, "RcipCode"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> ProgramAreaId
    {
      get { return Get(ref _programAreaId, "ProgramAreaId"); }
      set { Set(ref _programAreaId, value, "ProgramAreaId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.Onet17Onet12Mapping", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class Onet17Onet12Mapping : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 10)]
    private string _onet17Code;
    private long _onetId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Onet17Code entity attribute.</summary>
    public const string Onet17CodeField = "Onet17Code";
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Onet17Onet12Mappings")]
    private readonly EntityHolder<Onet> _onet = new EntityHolder<Onet>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Onet Onet
    {
      get { return Get(_onet); }
      set { Set(_onet, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Onet17Code
    {
      get { return Get(ref _onet17Code, "Onet17Code"); }
      set { Set(ref _onet17Code, value, "Onet17Code"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Onet" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.Onet17Onet12MappingView")]
  public partial class Onet17Onet12MappingView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _onet17Code;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _onet12Code;
    private long _onetId;
    private string _occupation;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Onet17Code entity attribute.</summary>
    public const string Onet17CodeField = "Onet17Code";
    /// <summary>Identifies the Onet12Code entity attribute.</summary>
    public const string Onet12CodeField = "Onet12Code";
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";
    /// <summary>Identifies the Occupation entity attribute.</summary>
    public const string OccupationField = "Occupation";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Onet17Code
    {
      get { return Get(ref _onet17Code, "Onet17Code"); }
      set { Set(ref _onet17Code, value, "Onet17Code"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Onet12Code
    {
      get { return Get(ref _onet12Code, "Onet12Code"); }
      set { Set(ref _onet12Code, value, "Onet12Code"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Occupation
    {
      get { return Get(ref _occupation, "Occupation"); }
      set { Set(ref _occupation, value, "Occupation"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.ROnetOnet", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class ROnetOnet : Entity<long>
  {
    #region Fields
  
    private bool _bestFit;
    private long _onetId;
    private long _rOnetId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the BestFit entity attribute.</summary>
    public const string BestFitField = "BestFit";
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";
    /// <summary>Identifies the ROnetId entity attribute.</summary>
    public const string ROnetIdField = "ROnetId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("ROnetOnets")]
    private readonly EntityHolder<Onet> _onet = new EntityHolder<Onet>();
    [ReverseAssociation("ROnetOnets")]
    private readonly EntityHolder<ROnet> _rOnet = new EntityHolder<ROnet>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Onet Onet
    {
      get { return Get(_onet); }
      set { Set(_onet, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ROnet ROnet
    {
      get { return Get(_rOnet); }
      set { Set(_rOnet, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool BestFit
    {
      get { return Get(ref _bestFit, "BestFit"); }
      set { Set(ref _bestFit, value, "BestFit"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Onet" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="ROnet" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long ROnetId
    {
      get { return Get(ref _rOnetId, "ROnetId"); }
      set { Set(ref _rOnetId, value, "ROnetId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.OnetToROnetConversionView")]
  public partial class OnetToROnetConversionView : Entity<long>
  {
    #region Fields
  
    private long _onetId;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _onetCode;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _onetKey;
    private long _rOnetId;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _rOnetCode;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _rOnetKey;
    private bool _bestFit;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";
    /// <summary>Identifies the OnetCode entity attribute.</summary>
    public const string OnetCodeField = "OnetCode";
    /// <summary>Identifies the OnetKey entity attribute.</summary>
    public const string OnetKeyField = "OnetKey";
    /// <summary>Identifies the ROnetId entity attribute.</summary>
    public const string ROnetIdField = "ROnetId";
    /// <summary>Identifies the ROnetCode entity attribute.</summary>
    public const string ROnetCodeField = "ROnetCode";
    /// <summary>Identifies the ROnetKey entity attribute.</summary>
    public const string ROnetKeyField = "ROnetKey";
    /// <summary>Identifies the BestFit entity attribute.</summary>
    public const string BestFitField = "BestFit";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetCode
    {
      get { return Get(ref _onetCode, "OnetCode"); }
      set { Set(ref _onetCode, value, "OnetCode"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetKey
    {
      get { return Get(ref _onetKey, "OnetKey"); }
      set { Set(ref _onetKey, value, "OnetKey"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long ROnetId
    {
      get { return Get(ref _rOnetId, "ROnetId"); }
      set { Set(ref _rOnetId, value, "ROnetId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ROnetCode
    {
      get { return Get(ref _rOnetCode, "ROnetCode"); }
      set { Set(ref _rOnetCode, value, "ROnetCode"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ROnetKey
    {
      get { return Get(ref _rOnetKey, "ROnetKey"); }
      set { Set(ref _rOnetKey, value, "ROnetKey"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool BestFit
    {
      get { return Get(ref _bestFit, "BestFit"); }
      set { Set(ref _bestFit, value, "BestFit"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.CareerAreaJobReportView")]
  public partial class CareerAreaJobReportView : Entity<System.Guid>
  {
    #region Fields
  
    private long _careerAreaId;
    private long _stateAreaId;
    private long _jobId;
    private int _hiringDemand;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the CareerAreaId entity attribute.</summary>
    public const string CareerAreaIdField = "CareerAreaId";
    /// <summary>Identifies the StateAreaId entity attribute.</summary>
    public const string StateAreaIdField = "StateAreaId";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the HiringDemand entity attribute.</summary>
    public const string HiringDemandField = "HiringDemand";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long CareerAreaId
    {
      get { return Get(ref _careerAreaId, "CareerAreaId"); }
      set { Set(ref _careerAreaId, value, "CareerAreaId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long StateAreaId
    {
      get { return Get(ref _stateAreaId, "StateAreaId"); }
      set { Set(ref _stateAreaId, value, "StateAreaId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int HiringDemand
    {
      get { return Get(ref _hiringDemand, "HiringDemand"); }
      set { Set(ref _hiringDemand, value, "HiringDemand"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.SOC", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class SOC : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 10)]
    private string _soc2010Code;
    [ValidateLength(0, 200)]
    private string _key;
    [ValidateLength(0, 255)]
    private string _title;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Soc2010Code entity attribute.</summary>
    public const string Soc2010CodeField = "Soc2010Code";
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the Title entity attribute.</summary>
    public const string TitleField = "Title";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Soc")]
    private readonly EntityCollection<OnetSOC> _onetSocs = new EntityCollection<OnetSOC>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<OnetSOC> OnetSocs
    {
      get { return Get(_onetSocs); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Soc2010Code
    {
      get { return Get(ref _soc2010Code, "Soc2010Code"); }
      set { Set(ref _soc2010Code, value, "Soc2010Code"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Title
    {
      get { return Get(ref _title, "Title"); }
      set { Set(ref _title, value, "Title"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Library.OnetSOC", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class OnetSOC : Entity<long>
  {
    #region Fields
  
    private long _onetId;
    private long _socId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";
    /// <summary>Identifies the SocId entity attribute.</summary>
    public const string SocIdField = "SocId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("OnetSOCs")]
    private readonly EntityHolder<Onet> _onet = new EntityHolder<Onet>();
    [ReverseAssociation("OnetSocs")]
    private readonly EntityHolder<SOC> _soc = new EntityHolder<SOC>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Onet Onet
    {
      get { return Get(_onet); }
      set { Set(_onet, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public SOC Soc
    {
      get { return Get(_soc); }
      set { Set(_soc, value); }
    }


    /// <summary>Gets or sets the ID for the <see cref="Onet" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Soc" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long SocId
    {
      get { return Get(ref _socId, "SocId"); }
      set { Set(ref _socId, value, "SocId"); }
    }

    #endregion
  }
	


  /// <summary>
  /// Provides a strong-typed unit of work for working with the Library model.
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  public partial class LibraryUnitOfWork : Mindscape.LightSpeed.UnitOfWork
  {

    public System.Linq.IQueryable<Localisation> Localisations
    {
      get { return this.Query<Localisation>(); }
    }
    
    public System.Linq.IQueryable<State> States
    {
      get { return this.Query<State>(); }
    }
    
    public System.Linq.IQueryable<Area> Areas
    {
      get { return this.Query<Area>(); }
    }
    
    public System.Linq.IQueryable<JobReport> JobReports
    {
      get { return this.Query<JobReport>(); }
    }
    
    public System.Linq.IQueryable<Job> Jobs
    {
      get { return this.Query<Job>(); }
    }
    
    public System.Linq.IQueryable<StudyPlace> StudyPlaces
    {
      get { return this.Query<StudyPlace>(); }
    }
    
    public System.Linq.IQueryable<Employer> Employers
    {
      get { return this.Query<Employer>(); }
    }
    
    public System.Linq.IQueryable<EmployerJob> EmployerJobs
    {
      get { return this.Query<EmployerJob>(); }
    }
    
    public System.Linq.IQueryable<EmployerJobView> EmployerJobViews
    {
      get { return this.Query<EmployerJobView>(); }
    }
    
    public System.Linq.IQueryable<JobSkillView> JobSkillViews
    {
      get { return this.Query<JobSkillView>(); }
    }
    
    public System.Linq.IQueryable<SkillCategory> SkillCategories
    {
      get { return this.Query<SkillCategory>(); }
    }
    
    public System.Linq.IQueryable<SkillCategorySkill> SkillCategorySkills
    {
      get { return this.Query<SkillCategorySkill>(); }
    }
    
    public System.Linq.IQueryable<CareerArea> CareerAreas
    {
      get { return this.Query<CareerArea>(); }
    }
    
    public System.Linq.IQueryable<JobCareerArea> JobCareerAreas
    {
      get { return this.Query<JobCareerArea>(); }
    }
    
    public System.Linq.IQueryable<JobSkill> JobSkills
    {
      get { return this.Query<JobSkill>(); }
    }
    
    public System.Linq.IQueryable<Skill> Skills
    {
      get { return this.Query<Skill>(); }
    }
    
    public System.Linq.IQueryable<JobRelatedJob> JobRelatedJobs
    {
      get { return this.Query<JobRelatedJob>(); }
    }
    
    public System.Linq.IQueryable<StateArea> StateAreas
    {
      get { return this.Query<StateArea>(); }
    }
    
    public System.Linq.IQueryable<JobReportView> JobReportViews
    {
      get { return this.Query<JobReportView>(); }
    }
    
    public System.Linq.IQueryable<JobDegreeLevel> JobDegreeLevels
    {
      get { return this.Query<JobDegreeLevel>(); }
    }
    
    public System.Linq.IQueryable<SkillCategorySkillView> SkillCategorySkillViews
    {
      get { return this.Query<SkillCategorySkillView>(); }
    }
    
    public System.Linq.IQueryable<JobJobTitle> JobJobTitles
    {
      get { return this.Query<JobJobTitle>(); }
    }
    
    public System.Linq.IQueryable<JobTitle> JobTitles
    {
      get { return this.Query<JobTitle>(); }
    }
    
    public System.Linq.IQueryable<JobEducationRequirement> JobEducationRequirements
    {
      get { return this.Query<JobEducationRequirement>(); }
    }
    
    public System.Linq.IQueryable<JobExperienceLevel> JobExperienceLevels
    {
      get { return this.Query<JobExperienceLevel>(); }
    }
    
    public System.Linq.IQueryable<EmployerSkillView> EmployerSkillViews
    {
      get { return this.Query<EmployerSkillView>(); }
    }
    
    public System.Linq.IQueryable<SkillReportView> SkillReportViews
    {
      get { return this.Query<SkillReportView>(); }
    }
    
    public System.Linq.IQueryable<EmployerSkill> EmployerSkills
    {
      get { return this.Query<EmployerSkill>(); }
    }
    
    public System.Linq.IQueryable<SkillReport> SkillReports
    {
      get { return this.Query<SkillReport>(); }
    }
    
    public System.Linq.IQueryable<JobCareerAreaSkillView> JobCareerAreaSkillViews
    {
      get { return this.Query<JobCareerAreaSkillView>(); }
    }
    
    public System.Linq.IQueryable<JobDegreeLevelSkillView> JobDegreeLevelSkillViews
    {
      get { return this.Query<JobDegreeLevelSkillView>(); }
    }
    
    public System.Linq.IQueryable<JobJobTitleSkillView> JobJobTitleSkillViews
    {
      get { return this.Query<JobJobTitleSkillView>(); }
    }
    
    public System.Linq.IQueryable<InternshipCategory> InternshipCategories
    {
      get { return this.Query<InternshipCategory>(); }
    }
    
    public System.Linq.IQueryable<InternshipSkill> InternshipSkills
    {
      get { return this.Query<InternshipSkill>(); }
    }
    
    public System.Linq.IQueryable<InternshipEmployer> InternshipEmployers
    {
      get { return this.Query<InternshipEmployer>(); }
    }
    
    public System.Linq.IQueryable<InternshipJobTitle> InternshipJobTitles
    {
      get { return this.Query<InternshipJobTitle>(); }
    }
    
    public System.Linq.IQueryable<InternshipReport> InternshipReports
    {
      get { return this.Query<InternshipReport>(); }
    }
    
    public System.Linq.IQueryable<InternshipEmployerView> InternshipEmployerViews
    {
      get { return this.Query<InternshipEmployerView>(); }
    }
    
    public System.Linq.IQueryable<InternshipReportView> InternshipReportViews
    {
      get { return this.Query<InternshipReportView>(); }
    }
    
    public System.Linq.IQueryable<InternshipSkillView> InternshipSkillViews
    {
      get { return this.Query<InternshipSkillView>(); }
    }
    
    public System.Linq.IQueryable<StateAreaView> StateAreaViews
    {
      get { return this.Query<StateAreaView>(); }
    }
    
    public System.Linq.IQueryable<Certification> Certifications
    {
      get { return this.Query<Certification>(); }
    }
    
    public System.Linq.IQueryable<JobCertification> JobCertifications
    {
      get { return this.Query<JobCertification>(); }
    }
    
    public System.Linq.IQueryable<Degree> Degrees
    {
      get { return this.Query<Degree>(); }
    }
    
    public System.Linq.IQueryable<DegreeEducationLevel> DegreeEducationLevels
    {
      get { return this.Query<DegreeEducationLevel>(); }
    }
    
    public System.Linq.IQueryable<JobDegreeEducationLevel> JobDegreeEducationLevels
    {
      get { return this.Query<JobDegreeEducationLevel>(); }
    }
    
    public System.Linq.IQueryable<StudyPlaceDegreeEducationLevel> StudyPlaceDegreeEducationLevels
    {
      get { return this.Query<StudyPlaceDegreeEducationLevel>(); }
    }
    
    public System.Linq.IQueryable<DegreeEducationLevelSkillView> DegreeEducationLevelSkillViews
    {
      get { return this.Query<DegreeEducationLevelSkillView>(); }
    }
    
    public System.Linq.IQueryable<DegreeEducationLevelView> DegreeEducationLevelViews
    {
      get { return this.Query<DegreeEducationLevelView>(); }
    }
    
    public System.Linq.IQueryable<JobDegreeEducationLevelSkillView> JobDegreeEducationLevelSkillViews
    {
      get { return this.Query<JobDegreeEducationLevelSkillView>(); }
    }
    
    public System.Linq.IQueryable<JobDegreeEducationLevelView> JobDegreeEducationLevelViews
    {
      get { return this.Query<JobDegreeEducationLevelView>(); }
    }
    
    public System.Linq.IQueryable<JobCertificationView> JobCertificationViews
    {
      get { return this.Query<JobCertificationView>(); }
    }
    
    public System.Linq.IQueryable<JobDegreeEducationLevelReport> JobDegreeEducationLevelReports
    {
      get { return this.Query<JobDegreeEducationLevelReport>(); }
    }
    
    public System.Linq.IQueryable<JobDegreeEducationLevelReportView> JobDegreeEducationLevelReportViews
    {
      get { return this.Query<JobDegreeEducationLevelReportView>(); }
    }
    
    public System.Linq.IQueryable<DegreeEducationLevelExt> DegreeEducationLevelExts
    {
      get { return this.Query<DegreeEducationLevelExt>(); }
    }
    
    public System.Linq.IQueryable<DegreeEducationLevelStateAreaView> DegreeEducationLevelStateAreaViews
    {
      get { return this.Query<DegreeEducationLevelStateAreaView>(); }
    }
    
    public System.Linq.IQueryable<JobStateAreaView> JobStateAreaViews
    {
      get { return this.Query<JobStateAreaView>(); }
    }
    
    public System.Linq.IQueryable<DegreeAlias> DegreeAliases
    {
      get { return this.Query<DegreeAlias>(); }
    }
    
    public System.Linq.IQueryable<DegreeAliasView> DegreeAliasViews
    {
      get { return this.Query<DegreeAliasView>(); }
    }
    
    public System.Linq.IQueryable<JobCareerPathTo> JobCareerPathTos
    {
      get { return this.Query<JobCareerPathTo>(); }
    }
    
    public System.Linq.IQueryable<JobCareerPathFrom> JobCareerPathFroms
    {
      get { return this.Query<JobCareerPathFrom>(); }
    }
    
    public System.Linq.IQueryable<StudyPlaceDegreeEducationLevelView> StudyPlaceDegreeEducationLevelViews
    {
      get { return this.Query<StudyPlaceDegreeEducationLevelView>(); }
    }
    
    public System.Linq.IQueryable<JobEmployerSkill> JobEmployerSkills
    {
      get { return this.Query<JobEmployerSkill>(); }
    }
    
    public System.Linq.IQueryable<JobEmployerSkillView> JobEmployerSkillViews
    {
      get { return this.Query<JobEmployerSkillView>(); }
    }
    
    public System.Linq.IQueryable<ProgramArea> ProgramAreas
    {
      get { return this.Query<ProgramArea>(); }
    }
    
    public System.Linq.IQueryable<ProgramAreaDegree> ProgramAreaDegrees
    {
      get { return this.Query<ProgramAreaDegree>(); }
    }
    
    public System.Linq.IQueryable<ProgramAreaDegreeView> ProgramAreaDegreeViews
    {
      get { return this.Query<ProgramAreaDegreeView>(); }
    }
    
    public System.Linq.IQueryable<ProgramAreaDegreeEducationLevelView> ProgramAreaDegreeEducationLevelViews
    {
      get { return this.Query<ProgramAreaDegreeEducationLevelView>(); }
    }
    
    public System.Linq.IQueryable<Onet> Onets
    {
      get { return this.Query<Onet>(); }
    }
    
    public System.Linq.IQueryable<OnetTask> OnetTasks
    {
      get { return this.Query<OnetTask>(); }
    }
    
    public System.Linq.IQueryable<JobTask> JobTasks
    {
      get { return this.Query<JobTask>(); }
    }
    
    public System.Linq.IQueryable<JobTaskMultiOption> JobTaskMultiOptions
    {
      get { return this.Query<JobTaskMultiOption>(); }
    }
    
    public System.Linq.IQueryable<OnetLocalisationItem> OnetLocalisationItems
    {
      get { return this.Query<OnetLocalisationItem>(); }
    }
    
    public System.Linq.IQueryable<JobTaskLocalisationItem> JobTaskLocalisationItems
    {
      get { return this.Query<JobTaskLocalisationItem>(); }
    }
    
    public System.Linq.IQueryable<OnetWord> OnetWords
    {
      get { return this.Query<OnetWord>(); }
    }
    
    public System.Linq.IQueryable<OnetRing> OnetRings
    {
      get { return this.Query<OnetRing>(); }
    }
    
    public System.Linq.IQueryable<OnetPhrase> OnetPhrases
    {
      get { return this.Query<OnetPhrase>(); }
    }
    
    public System.Linq.IQueryable<JobTaskView> JobTaskViews
    {
      get { return this.Query<JobTaskView>(); }
    }
    
    public System.Linq.IQueryable<JobTaskMultiOptionView> JobTaskMultiOptionViews
    {
      get { return this.Query<JobTaskMultiOptionView>(); }
    }
    
    public System.Linq.IQueryable<OnetView> OnetViews
    {
      get { return this.Query<OnetView>(); }
    }
    
    public System.Linq.IQueryable<NAICS> NAICs
    {
      get { return this.Query<NAICS>(); }
    }
    
    public System.Linq.IQueryable<OnetTaskView> OnetTaskViews
    {
      get { return this.Query<OnetTaskView>(); }
    }
    
    public System.Linq.IQueryable<PostalCode> PostalCodes
    {
      get { return this.Query<PostalCode>(); }
    }
    
    public System.Linq.IQueryable<PostalCodeView> PostalCodeViews
    {
      get { return this.Query<PostalCodeView>(); }
    }
    
    public System.Linq.IQueryable<OnetWordJobTaskView> OnetWordJobTaskViews
    {
      get { return this.Query<OnetWordJobTaskView>(); }
    }
    
    public System.Linq.IQueryable<OnetWordView> OnetWordViews
    {
      get { return this.Query<OnetWordView>(); }
    }
    
    public System.Linq.IQueryable<OnetCommodity> OnetCommodities
    {
      get { return this.Query<OnetCommodity>(); }
    }
    
    public System.Linq.IQueryable<StandardIndustrialClassification> StandardIndustrialClassifications
    {
      get { return this.Query<StandardIndustrialClassification>(); }
    }
    
    public System.Linq.IQueryable<ROnet> ROnets
    {
      get { return this.Query<ROnet>(); }
    }
    
    public System.Linq.IQueryable<OnetROnet> OnetROnets
    {
      get { return this.Query<OnetROnet>(); }
    }
    
    public System.Linq.IQueryable<MilitaryOccupation> MilitaryOccupations
    {
      get { return this.Query<MilitaryOccupation>(); }
    }
    
    public System.Linq.IQueryable<MilitaryOccupationGroup> MilitaryOccupationGroups
    {
      get { return this.Query<MilitaryOccupationGroup>(); }
    }
    
    public System.Linq.IQueryable<MilitaryOccupationJobTitleView> MilitaryOccupationJobTitleViews
    {
      get { return this.Query<MilitaryOccupationJobTitleView>(); }
    }
    
    public System.Linq.IQueryable<OnetSkills> OnetSkills
    {
      get { return this.Query<OnetSkills>(); }
    }
    
    public System.Linq.IQueryable<MilitaryOccupationGroupROnet> MilitaryOccupationGroupROnets
    {
      get { return this.Query<MilitaryOccupationGroupROnet>(); }
    }
    
    public System.Linq.IQueryable<MilitaryOccupationROnet> MilitaryOccupationROnets
    {
      get { return this.Query<MilitaryOccupationROnet>(); }
    }
    
    public System.Linq.IQueryable<LocalisationItem> LocalisationItems
    {
      get { return this.Query<LocalisationItem>(); }
    }
    
    public System.Linq.IQueryable<GenericJobTitle> GenericJobTitles
    {
      get { return this.Query<GenericJobTitle>(); }
    }
    
    public System.Linq.IQueryable<ResumeSkill> ResumeSkills
    {
      get { return this.Query<ResumeSkill>(); }
    }
    
    public System.Linq.IQueryable<EducationInternshipCategory> EducationInternshipCategories
    {
      get { return this.Query<EducationInternshipCategory>(); }
    }
    
    public System.Linq.IQueryable<EducationInternshipSkill> EducationInternshipSkills
    {
      get { return this.Query<EducationInternshipSkill>(); }
    }
    
    public System.Linq.IQueryable<EducationInternshipStatement> EducationInternshipStatements
    {
      get { return this.Query<EducationInternshipStatement>(); }
    }
    
    public System.Linq.IQueryable<SkillsForJobsView> SkillsForJobsViews
    {
      get { return this.Query<SkillsForJobsView>(); }
    }
    
    public System.Linq.IQueryable<DegreeEducationLevelROnetView> DegreeEducationLevelROnetViews
    {
      get { return this.Query<DegreeEducationLevelROnetView>(); }
    }
    
    public System.Linq.IQueryable<CareerAreaJobDegreeView> CareerAreaJobDegreeViews
    {
      get { return this.Query<CareerAreaJobDegreeView>(); }
    }
    
    public System.Linq.IQueryable<DegreeEducationLevelLensMapping> DegreeEducationLevelLensMappings
    {
      get { return this.Query<DegreeEducationLevelLensMapping>(); }
    }
    
    public System.Linq.IQueryable<Onet17Onet12Mapping> Onet17Onet12Mappings
    {
      get { return this.Query<Onet17Onet12Mapping>(); }
    }
    
    public System.Linq.IQueryable<Onet17Onet12MappingView> Onet17Onet12MappingViews
    {
      get { return this.Query<Onet17Onet12MappingView>(); }
    }
    
    public System.Linq.IQueryable<ROnetOnet> ROnetOnets
    {
      get { return this.Query<ROnetOnet>(); }
    }
    
    public System.Linq.IQueryable<OnetToROnetConversionView> OnetToROnetConversionViews
    {
      get { return this.Query<OnetToROnetConversionView>(); }
    }
    
    public System.Linq.IQueryable<CareerAreaJobReportView> CareerAreaJobReportViews
    {
      get { return this.Query<CareerAreaJobReportView>(); }
    }
    
    public System.Linq.IQueryable<SOC> SOCs
    {
      get { return this.Query<SOC>(); }
    }
    
    public System.Linq.IQueryable<OnetSOC> OnetSOCs
    {
      get { return this.Query<OnetSOC>(); }
    }
    
  }
}

#endregion


#region Data Transfer Objects - Classes

/*
namespace Focus.Core.DataTransferObjects.FocusLibrary.Entities
{
  using System;
  using System.Runtime.Serialization;

    [Serializable]
    [DataContract(Name="Localisation", Namespace = Constants.DataContractNamespace)]
    public class LocalisationDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Culture { get; set; }
    }

    [Serializable]
    [DataContract(Name="State", Namespace = Constants.DataContractNamespace)]
    public class StateDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long CountryId { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public string Code { get; set; }
      [DataMember]
      public int SortOrder { get; set; }
    }

    [Serializable]
    [DataContract(Name="Area", Namespace = Constants.DataContractNamespace)]
    public class AreaDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public int SortOrder { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobReport", Namespace = Constants.DataContractNamespace)]
    public class JobReportDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public int HiringDemand { get; set; }
      [DataMember]
      public string SalaryTrend { get; set; }
      [DataMember]
      public decimal SalaryTrendPercentile { get; set; }
      [DataMember]
      public decimal SalaryTrendAverage { get; set; }
      [DataMember]
      public long LocalisationId { get; set; }
      [DataMember]
      public string GrowthTrend { get; set; }
      [DataMember]
      public decimal GrowthPercentile { get; set; }
      [DataMember]
      public string HiringTrend { get; set; }
      [DataMember]
      public decimal HiringTrendPercentile { get; set; }
      [DataMember]
      public int SalaryTrendMin { get; set; }
      [DataMember]
      public int SalaryTrendMax { get; set; }
      [DataMember]
      public string SalaryTrendRealtime { get; set; }
      [DataMember]
      public int? SalaryTrendRealtimeAverage { get; set; }
      [DataMember]
      public int? SalaryTrendRealtimeMin { get; set; }
      [DataMember]
      public int? SalaryTrendRealtimeMax { get; set; }
      [DataMember]
      public string HiringTrendSubLevel { get; set; }
      [DataMember]
      public long JobId { get; set; }
      [DataMember]
      public long StateAreaId { get; set; }
    }

    [Serializable]
    [DataContract(Name="Job", Namespace = Constants.DataContractNamespace)]
    public class JobDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public string ROnet { get; set; }
      [DataMember]
      public long LocalisationId { get; set; }
      [DataMember]
      public string DegreeIntroStatement { get; set; }
      [DataMember]
      public string Description { get; set; }
      [DataMember]
      public StarterJobLevel? StarterJob { get; set; }
    }

    [Serializable]
    [DataContract(Name="StudyPlace", Namespace = Constants.DataContractNamespace)]
    public class StudyPlaceDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public long StateAreaId { get; set; }
    }

    [Serializable]
    [DataContract(Name="Employer", Namespace = Constants.DataContractNamespace)]
    public class EmployerDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
    }

    [Serializable]
    [DataContract(Name="EmployerJob", Namespace = Constants.DataContractNamespace)]
    public class EmployerJobDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public int Positions { get; set; }
      [DataMember]
      public long JobId { get; set; }
      [DataMember]
      public long EmployerId { get; set; }
      [DataMember]
      public long StateAreaId { get; set; }
    }

    [Serializable]
    [DataContract(Name="EmployerJobView", Namespace = Constants.DataContractNamespace)]
    public class EmployerJobViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string EmployerName { get; set; }
      [DataMember]
      public long JobId { get; set; }
      [DataMember]
      public string JobName { get; set; }
      [DataMember]
      public int Positions { get; set; }
      [DataMember]
      public long EmployerId { get; set; }
      [DataMember]
      public long StateAreaId { get; set; }
      [DataMember]
      public string ROnet { get; set; }
      [DataMember]
      public StarterJobLevel? StarterJob { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSkillView", Namespace = Constants.DataContractNamespace)]
    public class JobSkillViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long JobId { get; set; }
      [DataMember]
      public JobSkillTypes JobSkillType { get; set; }
      [DataMember]
      public SkillTypes SkillType { get; set; }
      [DataMember]
      public long SkillId { get; set; }
      [DataMember]
      public decimal DemandPercentile { get; set; }
      [DataMember]
      public string SkillName { get; set; }
      [DataMember]
      public int Rank { get; set; }
    }

    [Serializable]
    [DataContract(Name="SkillCategory", Namespace = Constants.DataContractNamespace)]
    public class SkillCategoryDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public long LocalisationId { get; set; }
      [DataMember]
      public long? ParentId { get; set; }
    }

    [Serializable]
    [DataContract(Name="SkillCategorySkill", Namespace = Constants.DataContractNamespace)]
    public class SkillCategorySkillDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long SkillCategoryId { get; set; }
      [DataMember]
      public long SkillId { get; set; }
    }

    [Serializable]
    [DataContract(Name="CareerArea", Namespace = Constants.DataContractNamespace)]
    public class CareerAreaDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public long LocalisationId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobCareerArea", Namespace = Constants.DataContractNamespace)]
    public class JobCareerAreaDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long JobId { get; set; }
      [DataMember]
      public long CareerAreaId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSkill", Namespace = Constants.DataContractNamespace)]
    public class JobSkillDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public JobSkillTypes JobSkillType { get; set; }
      [DataMember]
      public decimal DemandPercentile { get; set; }
      [DataMember]
      public int Rank { get; set; }
      [DataMember]
      public long JobId { get; set; }
      [DataMember]
      public long SkillId { get; set; }
    }

    [Serializable]
    [DataContract(Name="Skill", Namespace = Constants.DataContractNamespace)]
    public class SkillDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public SkillTypes SkillType { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public long LocalisationId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobRelatedJob", Namespace = Constants.DataContractNamespace)]
    public class JobRelatedJobDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long RelatedJobId { get; set; }
      [DataMember]
      public int Priority { get; set; }
      [DataMember]
      public long JobId { get; set; }
    }

    [Serializable]
    [DataContract(Name="StateArea", Namespace = Constants.DataContractNamespace)]
    public class StateAreaDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string AreaCode { get; set; }
      [DataMember]
      public bool Display { get; set; }
      [DataMember]
      public bool IsDefault { get; set; }
      [DataMember]
      public long StateId { get; set; }
      [DataMember]
      public long AreaId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobReportView", Namespace = Constants.DataContractNamespace)]
    public class JobReportViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public int HiringDemand { get; set; }
      [DataMember]
      public string GrowthTrend { get; set; }
      [DataMember]
      public decimal GrowthPercentile { get; set; }
      [DataMember]
      public string SalaryTrend { get; set; }
      [DataMember]
      public decimal SalaryTrendPercentile { get; set; }
      [DataMember]
      public decimal SalaryTrendAverage { get; set; }
      [DataMember]
      public long JobId { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public string HiringTrend { get; set; }
      [DataMember]
      public decimal HiringTrendPercentile { get; set; }
      [DataMember]
      public int SalaryTrendMin { get; set; }
      [DataMember]
      public int SalaryTrendMax { get; set; }
      [DataMember]
      public string SalaryTrendRealtime { get; set; }
      [DataMember]
      public int? SalaryTrendRealtimeAverage { get; set; }
      [DataMember]
      public int? SalaryTrendRealtimeMin { get; set; }
      [DataMember]
      public int? SalaryTrendRealtimeMax { get; set; }
      [DataMember]
      public long StateAreaId { get; set; }
      [DataMember]
      public string DegreeIntroStatement { get; set; }
      [DataMember]
      public string ROnet { get; set; }
      [DataMember]
      public StarterJobLevel? StarterJob { get; set; }
      [DataMember]
      public string HiringTrendSubLevel { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobDegreeLevel", Namespace = Constants.DataContractNamespace)]
    public class JobDegreeLevelDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public DegreeLevels DegreeLevel { get; set; }
      [DataMember]
      public long JobId { get; set; }
    }

    [Serializable]
    [DataContract(Name="SkillCategorySkillView", Namespace = Constants.DataContractNamespace)]
    public class SkillCategorySkillViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long SkillCategoryId { get; set; }
      [DataMember]
      public string SkillCategoryName { get; set; }
      [DataMember]
      public long SkillId { get; set; }
      [DataMember]
      public string SkillName { get; set; }
      [DataMember]
      public long? SkillCategoryParentId { get; set; }
      [DataMember]
      public SkillTypes SkillType { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobJobTitle", Namespace = Constants.DataContractNamespace)]
    public class JobJobTitleDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public decimal DemandPercentile { get; set; }
      [DataMember]
      public long JobId { get; set; }
      [DataMember]
      public long JobTitleId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobTitle", Namespace = Constants.DataContractNamespace)]
    public class JobTitleDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobEducationRequirement", Namespace = Constants.DataContractNamespace)]
    public class JobEducationRequirementDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public EducationRequirementTypes EducationRequirementType { get; set; }
      [DataMember]
      public decimal RequirementPercentile { get; set; }
      [DataMember]
      public long JobId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobExperienceLevel", Namespace = Constants.DataContractNamespace)]
    public class JobExperienceLevelDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public ExperienceLevels ExperienceLevel { get; set; }
      [DataMember]
      public decimal ExperiencePercentile { get; set; }
      [DataMember]
      public long JobId { get; set; }
    }

    [Serializable]
    [DataContract(Name="EmployerSkillView", Namespace = Constants.DataContractNamespace)]
    public class EmployerSkillViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long SkillId { get; set; }
      [DataMember]
      public SkillTypes SkillType { get; set; }
      [DataMember]
      public string SkillName { get; set; }
      [DataMember]
      public long EmployerId { get; set; }
      [DataMember]
      public int Postings { get; set; }
    }

    [Serializable]
    [DataContract(Name="SkillReportView", Namespace = Constants.DataContractNamespace)]
    public class SkillReportViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public SkillTypes SkillType { get; set; }
      [DataMember]
      public string SkillName { get; set; }
      [DataMember]
      public long SkillId { get; set; }
      [DataMember]
      public int Frequency { get; set; }
      [DataMember]
      public long StateAreaId { get; set; }
    }

    [Serializable]
    [DataContract(Name="EmployerSkill", Namespace = Constants.DataContractNamespace)]
    public class EmployerSkillDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public int Postings { get; set; }
      [DataMember]
      public long EmployerId { get; set; }
      [DataMember]
      public long SkillId { get; set; }
    }

    [Serializable]
    [DataContract(Name="SkillReport", Namespace = Constants.DataContractNamespace)]
    public class SkillReportDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public int Frequency { get; set; }
      [DataMember]
      public long SkillId { get; set; }
      [DataMember]
      public long StateAreaId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobCareerAreaSkillView", Namespace = Constants.DataContractNamespace)]
    public class JobCareerAreaSkillViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long SkillId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobDegreeLevelSkillView", Namespace = Constants.DataContractNamespace)]
    public class JobDegreeLevelSkillViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long SkillId { get; set; }
      [DataMember]
      public DegreeLevels DegreeLevel { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobJobTitleSkillView", Namespace = Constants.DataContractNamespace)]
    public class JobJobTitleSkillViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long SkillId { get; set; }
    }

    [Serializable]
    [DataContract(Name="InternshipCategory", Namespace = Constants.DataContractNamespace)]
    public class InternshipCategoryDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public long LocalisationId { get; set; }
      [DataMember]
      public int LensFilterId { get; set; }
    }

    [Serializable]
    [DataContract(Name="InternshipSkill", Namespace = Constants.DataContractNamespace)]
    public class InternshipSkillDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public int Rank { get; set; }
      [DataMember]
      public long SkillId { get; set; }
      [DataMember]
      public long InternshipCategoryId { get; set; }
    }

    [Serializable]
    [DataContract(Name="InternshipEmployer", Namespace = Constants.DataContractNamespace)]
    public class InternshipEmployerDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public int Frequency { get; set; }
      [DataMember]
      public long EmployerId { get; set; }
      [DataMember]
      public long? StateAreaId { get; set; }
      [DataMember]
      public long InternshipCategoryId { get; set; }
    }

    [Serializable]
    [DataContract(Name="InternshipJobTitle", Namespace = Constants.DataContractNamespace)]
    public class InternshipJobTitleDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string JobTitle { get; set; }
      [DataMember]
      public int Frequency { get; set; }
      [DataMember]
      public long InternshipCategoryId { get; set; }
    }

    [Serializable]
    [DataContract(Name="InternshipReport", Namespace = Constants.DataContractNamespace)]
    public class InternshipReportDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public int NumberAvailable { get; set; }
      [DataMember]
      public long StateAreaId { get; set; }
      [DataMember]
      public long InternshipCategoryId { get; set; }
    }

    [Serializable]
    [DataContract(Name="InternshipEmployerView", Namespace = Constants.DataContractNamespace)]
    public class InternshipEmployerViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long EmployerId { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public int Frequency { get; set; }
      [DataMember]
      public long InternshipCategoryId { get; set; }
      [DataMember]
      public long? StateAreaId { get; set; }
      [DataMember]
      public string InternshipCategoryName { get; set; }
    }

    [Serializable]
    [DataContract(Name="InternshipReportView", Namespace = Constants.DataContractNamespace)]
    public class InternshipReportViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long InternshipCategoryId { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public int NumberAvailable { get; set; }
      [DataMember]
      public long StateAreaId { get; set; }
    }

    [Serializable]
    [DataContract(Name="InternshipSkillView", Namespace = Constants.DataContractNamespace)]
    public class InternshipSkillViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long SkillId { get; set; }
      [DataMember]
      public SkillTypes SkillType { get; set; }
      [DataMember]
      public string SkillName { get; set; }
      [DataMember]
      public long InternshipCategoryId { get; set; }
      [DataMember]
      public int Rank { get; set; }
      [DataMember]
      public string InternshipCategoryName { get; set; }
    }

    [Serializable]
    [DataContract(Name="StateAreaView", Namespace = Constants.DataContractNamespace)]
    public class StateAreaViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long StateId { get; set; }
      [DataMember]
      public string StateName { get; set; }
      [DataMember]
      public string StateCode { get; set; }
      [DataMember]
      public int StateSortOrder { get; set; }
      [DataMember]
      public long AreaId { get; set; }
      [DataMember]
      public string AreaName { get; set; }
      [DataMember]
      public int AreaSortOrder { get; set; }
      [DataMember]
      public string StateAreaName { get; set; }
      [DataMember]
      public string AreaCode { get; set; }
      [DataMember]
      public bool IsDefault { get; set; }
    }

    [Serializable]
    [DataContract(Name="Certification", Namespace = Constants.DataContractNamespace)]
    public class CertificationDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobCertification", Namespace = Constants.DataContractNamespace)]
    public class JobCertificationDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public decimal DemandPercentile { get; set; }
      [DataMember]
      public long JobId { get; set; }
      [DataMember]
      public long CertificationId { get; set; }
    }

    [Serializable]
    [DataContract(Name="Degree", Namespace = Constants.DataContractNamespace)]
    public class DegreeDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public string RcipCode { get; set; }
      [DataMember]
      public bool IsDegreeArea { get; set; }
      [DataMember]
      public bool IsClientData { get; set; }
      [DataMember]
      public string ClientDataTag { get; set; }
      [DataMember]
      public int? Tier { get; set; }
    }

    [Serializable]
    [DataContract(Name="DegreeEducationLevel", Namespace = Constants.DataContractNamespace)]
    public class DegreeEducationLevelDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public ExplorerEducationLevels EducationLevel { get; set; }
      [DataMember]
      public int DegreesAwarded { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public bool ExcludeFromReport { get; set; }
      [DataMember]
      public long DegreeId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobDegreeEducationLevel", Namespace = Constants.DataContractNamespace)]
    public class JobDegreeEducationLevelDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public bool ExcludeFromJob { get; set; }
      [DataMember]
      public int? Tier { get; set; }
      [DataMember]
      public long JobId { get; set; }
      [DataMember]
      public long DegreeEducationLevelId { get; set; }
    }

    [Serializable]
    [DataContract(Name="StudyPlaceDegreeEducationLevel", Namespace = Constants.DataContractNamespace)]
    public class StudyPlaceDegreeEducationLevelDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long StudyPlaceId { get; set; }
      [DataMember]
      public long DegreeEducationLevelId { get; set; }
    }

    [Serializable]
    [DataContract(Name="DegreeEducationLevelSkillView", Namespace = Constants.DataContractNamespace)]
    public class DegreeEducationLevelSkillViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long StateAreaId { get; set; }
      [DataMember]
      public long SkillId { get; set; }
      [DataMember]
      public SkillTypes SkillType { get; set; }
      [DataMember]
      public string SkillName { get; set; }
      [DataMember]
      public decimal? DemandPercentile { get; set; }
    }

    [Serializable]
    [DataContract(Name="DegreeEducationLevelView", Namespace = Constants.DataContractNamespace)]
    public class DegreeEducationLevelViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long DegreeId { get; set; }
      [DataMember]
      public string DegreeName { get; set; }
      [DataMember]
      public ExplorerEducationLevels EducationLevel { get; set; }
      [DataMember]
      public string DegreeEducationLevelName { get; set; }
      [DataMember]
      public bool IsDegreeArea { get; set; }
      [DataMember]
      public bool IsClientData { get; set; }
      [DataMember]
      public string ClientDataTag { get; set; }
      [DataMember]
      public int? Tier { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobDegreeEducationLevelSkillView", Namespace = Constants.DataContractNamespace)]
    public class JobDegreeEducationLevelSkillViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long SkillId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobDegreeEducationLevelView", Namespace = Constants.DataContractNamespace)]
    public class JobDegreeEducationLevelViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long JobId { get; set; }
      [DataMember]
      public long DegreeEducationLevelId { get; set; }
      [DataMember]
      public long DegreeId { get; set; }
      [DataMember]
      public string DegreeName { get; set; }
      [DataMember]
      public int DegreesAwarded { get; set; }
      [DataMember]
      public string DegreeEducationLevelName { get; set; }
      [DataMember]
      public ExplorerEducationLevels EducationLevel { get; set; }
      [DataMember]
      public bool IsDegreeArea { get; set; }
      [DataMember]
      public bool IsClientData { get; set; }
      [DataMember]
      public string ClientDataTag { get; set; }
      [DataMember]
      public bool ExcludeFromJob { get; set; }
      [DataMember]
      public int? Tier { get; set; }
      [DataMember]
      public string ROnet { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobCertificationView", Namespace = Constants.DataContractNamespace)]
    public class JobCertificationViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long JobId { get; set; }
      [DataMember]
      public long CertificationId { get; set; }
      [DataMember]
      public string CertificationName { get; set; }
      [DataMember]
      public decimal DemandPercentile { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobDegreeEducationLevelReport", Namespace = Constants.DataContractNamespace)]
    public class JobDegreeEducationLevelReportDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public int HiringDemand { get; set; }
      [DataMember]
      public int DegreesAwarded { get; set; }
      [DataMember]
      public long StateAreaId { get; set; }
      [DataMember]
      public long JobDegreeEducationLevelId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobDegreeEducationLevelReportView", Namespace = Constants.DataContractNamespace)]
    public class JobDegreeEducationLevelReportViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long StateAreaId { get; set; }
      [DataMember]
      public long JobDegreeEducationLevelId { get; set; }
      [DataMember]
      public int HiringDemand { get; set; }
      [DataMember]
      public int DegreesAwarded { get; set; }
      [DataMember]
      public long JobId { get; set; }
      [DataMember]
      public long DegreeEducationLevelId { get; set; }
      [DataMember]
      public ExplorerEducationLevels EducationLevel { get; set; }
      [DataMember]
      public long DegreeId { get; set; }
      [DataMember]
      public string DegreeName { get; set; }
      [DataMember]
      public bool IsDegreeArea { get; set; }
      [DataMember]
      public string DegreeEducationLevelName { get; set; }
      [DataMember]
      public bool IsClientData { get; set; }
      [DataMember]
      public string ClientDataTag { get; set; }
      [DataMember]
      public bool ExcludeFromReport { get; set; }
      [DataMember]
      public bool ExcludeFromJob { get; set; }
    }

    [Serializable]
    [DataContract(Name="DegreeEducationLevelExt", Namespace = Constants.DataContractNamespace)]
    public class DegreeEducationLevelExtDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public string Url { get; set; }
      [DataMember]
      public string Description { get; set; }
      [DataMember]
      public long DegreeEducationLevelId { get; set; }
    }

    [Serializable]
    [DataContract(Name="DegreeEducationLevelStateAreaView", Namespace = Constants.DataContractNamespace)]
    public class DegreeEducationLevelStateAreaViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long DegreeId { get; set; }
      [DataMember]
      public string DegreeName { get; set; }
      [DataMember]
      public bool IsDegreeArea { get; set; }
      [DataMember]
      public bool IsClientData { get; set; }
      [DataMember]
      public long StateAreaId { get; set; }
      [DataMember]
      public string DegreeEducationLevelName { get; set; }
      [DataMember]
      public string ClientDataTag { get; set; }
      [DataMember]
      public ExplorerEducationLevels EducationLevel { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobStateAreaView", Namespace = Constants.DataContractNamespace)]
    public class JobStateAreaViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public long StateAreaId { get; set; }
    }

    [Serializable]
    [DataContract(Name="DegreeAlias", Namespace = Constants.DataContractNamespace)]
    public class DegreeAliasDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string AliasExtended { get; set; }
      [DataMember]
      public ExplorerEducationLevels EducationLevel { get; set; }
      [DataMember]
      public string Alias { get; set; }
      [DataMember]
      public long DegreeId { get; set; }
      [DataMember]
      public long DegreeEducationLevelId { get; set; }
    }

    [Serializable]
    [DataContract(Name="DegreeAliasView", Namespace = Constants.DataContractNamespace)]
    public class DegreeAliasViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public ExplorerEducationLevels EducationLevel { get; set; }
      [DataMember]
      public long DegreeId { get; set; }
      [DataMember]
      public bool IsClientData { get; set; }
      [DataMember]
      public string ClientDataTag { get; set; }
      [DataMember]
      public long DegreeEducationLevelId { get; set; }
      [DataMember]
      public string AliasExtended { get; set; }
      [DataMember]
      public string Alias { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobCareerPathTo", Namespace = Constants.DataContractNamespace)]
    public class JobCareerPathToDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public int Rank { get; set; }
      [DataMember]
      public long CurrentJobId { get; set; }
      [DataMember]
      public long Next1JobId { get; set; }
      [DataMember]
      public long? Next2JobId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobCareerPathFrom", Namespace = Constants.DataContractNamespace)]
    public class JobCareerPathFromDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public int Rank { get; set; }
      [DataMember]
      public long FromJobId { get; set; }
      [DataMember]
      public long ToJobId { get; set; }
    }

    [Serializable]
    [DataContract(Name="StudyPlaceDegreeEducationLevelView", Namespace = Constants.DataContractNamespace)]
    public class StudyPlaceDegreeEducationLevelViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long StudyPlaceId { get; set; }
      [DataMember]
      public string StudyPlaceName { get; set; }
      [DataMember]
      public long StateAreaId { get; set; }
      [DataMember]
      public long DegreeEducationLevelId { get; set; }
      [DataMember]
      public long DegreeId { get; set; }
      [DataMember]
      public string DegreeName { get; set; }
      [DataMember]
      public bool IsClientData { get; set; }
      [DataMember]
      public string ClientDataTag { get; set; }
      [DataMember]
      public string DegreeEducationLevelName { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobEmployerSkill", Namespace = Constants.DataContractNamespace)]
    public class JobEmployerSkillDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public int SkillCount { get; set; }
      [DataMember]
      public int Rank { get; set; }
      [DataMember]
      public long JobId { get; set; }
      [DataMember]
      public long EmployerId { get; set; }
      [DataMember]
      public long SkillId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobEmployerSkillView", Namespace = Constants.DataContractNamespace)]
    public class JobEmployerSkillViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long JobId { get; set; }
      [DataMember]
      public long EmployerId { get; set; }
      [DataMember]
      public long SkillId { get; set; }
      [DataMember]
      public SkillTypes SkillType { get; set; }
      [DataMember]
      public string SkillName { get; set; }
      [DataMember]
      public int SkillCount { get; set; }
    }

    [Serializable]
    [DataContract(Name="ProgramArea", Namespace = Constants.DataContractNamespace)]
    public class ProgramAreaDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public string Description { get; set; }
      [DataMember]
      public bool IsClientData { get; set; }
      [DataMember]
      public string ClientDataTag { get; set; }
    }

    [Serializable]
    [DataContract(Name="ProgramAreaDegree", Namespace = Constants.DataContractNamespace)]
    public class ProgramAreaDegreeDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long? DegreeId { get; set; }
      [DataMember]
      public long? ProgramAreaId { get; set; }
    }

    [Serializable]
    [DataContract(Name="ProgramAreaDegreeView", Namespace = Constants.DataContractNamespace)]
    public class ProgramAreaDegreeViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long ProgramAreaId { get; set; }
      [DataMember]
      public string ProgramAreaName { get; set; }
      [DataMember]
      public long DegreeId { get; set; }
      [DataMember]
      public string DegreeName { get; set; }
      [DataMember]
      public bool IsClientData { get; set; }
      [DataMember]
      public string ClientDataTag { get; set; }
    }

    [Serializable]
    [DataContract(Name="ProgramAreaDegreeEducationLevelView", Namespace = Constants.DataContractNamespace)]
    public class ProgramAreaDegreeEducationLevelViewDto
    {
      [DataMember]
      public System.Guid? Id { get; set; }
      [DataMember]
      public long ProgramAreaId { get; set; }
      [DataMember]
      public string ProgramAreaName { get; set; }
      [DataMember]
      public long DegreeId { get; set; }
      [DataMember]
      public string DegreeName { get; set; }
      [DataMember]
      public long DegreeEducationLevelId { get; set; }
      [DataMember]
      public ExplorerEducationLevels EducationLevel { get; set; }
      [DataMember]
      public string DegreeEducationLevelName { get; set; }
      [DataMember]
      public bool IsClientData { get; set; }
      [DataMember]
      public string ClientDataTag { get; set; }
    }

    [Serializable]
    [DataContract(Name="Onet", Namespace = Constants.DataContractNamespace)]
    public class OnetDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string OnetCode { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public long JobFamilyId { get; set; }
      [DataMember]
      public int JobZone { get; set; }
    }

    [Serializable]
    [DataContract(Name="OnetTask", Namespace = Constants.DataContractNamespace)]
    public class OnetTaskDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public long OnetId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobTask", Namespace = Constants.DataContractNamespace)]
    public class JobTaskDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public JobTaskTypes JobTaskType { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public JobTaskScopes Scope { get; set; }
      [DataMember]
      public bool Certificate { get; set; }
      [DataMember]
      public long OnetId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobTaskMultiOption", Namespace = Constants.DataContractNamespace)]
    public class JobTaskMultiOptionDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public long JobTaskId { get; set; }
    }

    [Serializable]
    [DataContract(Name="OnetLocalisationItem", Namespace = Constants.DataContractNamespace)]
    public class OnetLocalisationItemDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public string PrimaryValue { get; set; }
      [DataMember]
      public string SecondaryValue { get; set; }
      [DataMember]
      public long LocalisationId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobTaskLocalisationItem", Namespace = Constants.DataContractNamespace)]
    public class JobTaskLocalisationItemDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public string PrimaryValue { get; set; }
      [DataMember]
      public string SecondaryValue { get; set; }
      [DataMember]
      public long LocalisationId { get; set; }
    }

    [Serializable]
    [DataContract(Name="OnetWord", Namespace = Constants.DataContractNamespace)]
    public class OnetWordDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public string Word { get; set; }
      [DataMember]
      public string Stem { get; set; }
      [DataMember]
      public int? Frequency { get; set; }
      [DataMember]
      public string OnetSoc { get; set; }
      [DataMember]
      public long OnetId { get; set; }
      [DataMember]
      public long OnetRingId { get; set; }
    }

    [Serializable]
    [DataContract(Name="OnetRing", Namespace = Constants.DataContractNamespace)]
    public class OnetRingDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public int Weighting { get; set; }
      [DataMember]
      public int RingMax { get; set; }
    }

    [Serializable]
    [DataContract(Name="OnetPhrase", Namespace = Constants.DataContractNamespace)]
    public class OnetPhraseDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Phrase { get; set; }
      [DataMember]
      public int? Equivalent { get; set; }
      [DataMember]
      public string OnetSoc { get; set; }
      [DataMember]
      public long OnetId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobTaskView", Namespace = Constants.DataContractNamespace)]
    public class JobTaskViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public JobTaskTypes JobTaskType { get; set; }
      [DataMember]
      public long OnetId { get; set; }
      [DataMember]
      public string Culture { get; set; }
      [DataMember]
      public string Prompt { get; set; }
      [DataMember]
      public string Response { get; set; }
      [DataMember]
      public JobTaskScopes Scope { get; set; }
      [DataMember]
      public bool Certificate { get; set; }
      [DataMember]
      public string OnetCode { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobTaskMultiOptionView", Namespace = Constants.DataContractNamespace)]
    public class JobTaskMultiOptionViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long OnetId { get; set; }
      [DataMember]
      public string Culture { get; set; }
      [DataMember]
      public string Prompt { get; set; }
      [DataMember]
      public long JobTaskId { get; set; }
    }

    [Serializable]
    [DataContract(Name="OnetView", Namespace = Constants.DataContractNamespace)]
    public class OnetViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public string Occupation { get; set; }
      [DataMember]
      public long JobFamilyId { get; set; }
      [DataMember]
      public string OnetCode { get; set; }
      [DataMember]
      public string Culture { get; set; }
      [DataMember]
      public string Description { get; set; }
      [DataMember]
      public bool JobTasksAvailable { get; set; }
    }

    [Serializable]
    [DataContract(Name="NAICS", Namespace = Constants.DataContractNamespace)]
    public class NAICSDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Code { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public long ParentId { get; set; }
    }

    [Serializable]
    [DataContract(Name="OnetTaskView", Namespace = Constants.DataContractNamespace)]
    public class OnetTaskViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long OnetId { get; set; }
      [DataMember]
      public string TaskKey { get; set; }
      [DataMember]
      public string Task { get; set; }
      [DataMember]
      public string Culture { get; set; }
      [DataMember]
      public string TaskPastTense { get; set; }
    }

    [Serializable]
    [DataContract(Name="PostalCode", Namespace = Constants.DataContractNamespace)]
    public class PostalCodeDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Code { get; set; }
      [DataMember]
      public double Longitude { get; set; }
      [DataMember]
      public double Latitude { get; set; }
      [DataMember]
      public string CountryKey { get; set; }
      [DataMember]
      public string StateKey { get; set; }
      [DataMember]
      public string CountyKey { get; set; }
      [DataMember]
      public string CityName { get; set; }
      [DataMember]
      public string OfficeKey { get; set; }
      [DataMember]
      public string WibLocationKey { get; set; }
      [DataMember]
      public string MsaId { get; set; }
    }

    [Serializable]
    [DataContract(Name="PostalCodeView", Namespace = Constants.DataContractNamespace)]
    public class PostalCodeViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Code { get; set; }
      [DataMember]
      public double Longitude { get; set; }
      [DataMember]
      public double Latitude { get; set; }
      [DataMember]
      public string CountryKey { get; set; }
      [DataMember]
      public string CountryName { get; set; }
      [DataMember]
      public string StateKey { get; set; }
      [DataMember]
      public string StateName { get; set; }
      [DataMember]
      public string Culture { get; set; }
      [DataMember]
      public string CountyKey { get; set; }
      [DataMember]
      public string CountyName { get; set; }
      [DataMember]
      public string CityName { get; set; }
    }

    [Serializable]
    [DataContract(Name="OnetWordJobTaskView", Namespace = Constants.DataContractNamespace)]
    public class OnetWordJobTaskViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long OnetId { get; set; }
      [DataMember]
      public long OnetRingId { get; set; }
      [DataMember]
      public string OnetSoc { get; set; }
      [DataMember]
      public string Stem { get; set; }
      [DataMember]
      public string Word { get; set; }
      [DataMember]
      public long JobFamilyId { get; set; }
      [DataMember]
      public int JobZone { get; set; }
      [DataMember]
      public string OnetKey { get; set; }
      [DataMember]
      public string OnetCode { get; set; }
      [DataMember]
      public int? Frequency { get; set; }
    }

    [Serializable]
    [DataContract(Name="OnetWordView", Namespace = Constants.DataContractNamespace)]
    public class OnetWordViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long OnetId { get; set; }
      [DataMember]
      public long OnetRingId { get; set; }
      [DataMember]
      public string OnetSoc { get; set; }
      [DataMember]
      public string Stem { get; set; }
      [DataMember]
      public string Word { get; set; }
      [DataMember]
      public long JobFamilyId { get; set; }
      [DataMember]
      public int JobZone { get; set; }
      [DataMember]
      public string OnetKey { get; set; }
      [DataMember]
      public string OnetCode { get; set; }
      [DataMember]
      public int? Frequency { get; set; }
    }

    [Serializable]
    [DataContract(Name="OnetCommodity", Namespace = Constants.DataContractNamespace)]
    public class OnetCommodityDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public ToolTechnologyTypes ToolTechnologyType { get; set; }
      [DataMember]
      public string ToolTechnologyExample { get; set; }
      [DataMember]
      public string Code { get; set; }
      [DataMember]
      public string Title { get; set; }
      [DataMember]
      public long OnetId { get; set; }
    }

    [Serializable]
    [DataContract(Name="StandardIndustrialClassification", Namespace = Constants.DataContractNamespace)]
    public class StandardIndustrialClassificationDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public string SicPrimary { get; set; }
      [DataMember]
      public string SicSecondary { get; set; }
      [DataMember]
      public string NaicsPrimary { get; set; }
      [DataMember]
      public string NaicsSecondary { get; set; }
    }

    [Serializable]
    [DataContract(Name="ROnet", Namespace = Constants.DataContractNamespace)]
    public class ROnetDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Code { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public long? OnetId { get; set; }
    }

    [Serializable]
    [DataContract(Name="OnetROnet", Namespace = Constants.DataContractNamespace)]
    public class OnetROnetDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public bool BestFit { get; set; }
      [DataMember]
      public long OnetId { get; set; }
      [DataMember]
      public long ROnetId { get; set; }
    }

    [Serializable]
    [DataContract(Name="MilitaryOccupation", Namespace = Constants.DataContractNamespace)]
    public class MilitaryOccupationDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Code { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public long BranchOfServiceId { get; set; }
      [DataMember]
      public bool IsCommissioned { get; set; }
      [DataMember]
      public long MilitaryOccupationGroupId { get; set; }
    }

    [Serializable]
    [DataContract(Name="MilitaryOccupationGroup", Namespace = Constants.DataContractNamespace)]
    public class MilitaryOccupationGroupDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public bool IsCommissioned { get; set; }
    }

    [Serializable]
    [DataContract(Name="MilitaryOccupationJobTitleView", Namespace = Constants.DataContractNamespace)]
    public class MilitaryOccupationJobTitleViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long MilitaryOccupationId { get; set; }
      [DataMember]
      public string JobTitle { get; set; }
      [DataMember]
      public long BranchOfServiceId { get; set; }
    }

    [Serializable]
    [DataContract(Name="OnetSkills", Namespace = Constants.DataContractNamespace)]
    public class OnetSkillsDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Skill { get; set; }
      [DataMember]
      public string SkillType { get; set; }
      [DataMember]
      public long OnetId { get; set; }
    }

    [Serializable]
    [DataContract(Name="MilitaryOccupationGroupROnet", Namespace = Constants.DataContractNamespace)]
    public class MilitaryOccupationGroupROnetDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long ROnetId { get; set; }
      [DataMember]
      public long MilitaryOccupationGroupId { get; set; }
    }

    [Serializable]
    [DataContract(Name="MilitaryOccupationROnet", Namespace = Constants.DataContractNamespace)]
    public class MilitaryOccupationROnetDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long ROnetId { get; set; }
      [DataMember]
      public long MilitaryOccupationId { get; set; }
    }

    [Serializable]
    [DataContract(Name="LocalisationItem", Namespace = Constants.DataContractNamespace)]
    public class LocalisationItemDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public string Value { get; set; }
      [DataMember]
      public long LocalisationId { get; set; }
    }

    [Serializable]
    [DataContract(Name="GenericJobTitle", Namespace = Constants.DataContractNamespace)]
    public class GenericJobTitleDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Value { get; set; }
    }

    [Serializable]
    [DataContract(Name="ResumeSkill", Namespace = Constants.DataContractNamespace)]
    public class ResumeSkillDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public SkillType Type { get; set; }
      [DataMember]
      public SkillCategories Category { get; set; }
      [DataMember]
      public string Stem { get; set; }
      [DataMember]
      public int NoiseSkill { get; set; }
      [DataMember]
      public int Cluster { get; set; }
    }

    [Serializable]
    [DataContract(Name="EducationInternshipCategory", Namespace = Constants.DataContractNamespace)]
    public class EducationInternshipCategoryDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public long LocalisationId { get; set; }
      [DataMember]
      public EducationSkillCategories CategoryType { get; set; }
      [DataMember]
      public string PastTenseStatement { get; set; }
      [DataMember]
      public string PresentTenseStatement { get; set; }
    }

    [Serializable]
    [DataContract(Name="EducationInternshipSkill", Namespace = Constants.DataContractNamespace)]
    public class EducationInternshipSkillDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public long LocalisationId { get; set; }
      [DataMember]
      public long EducationInternshipCategoryId { get; set; }
    }

    [Serializable]
    [DataContract(Name="EducationInternshipStatement", Namespace = Constants.DataContractNamespace)]
    public class EducationInternshipStatementDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string PastTenseStatement { get; set; }
      [DataMember]
      public long LocalisationId { get; set; }
      [DataMember]
      public string PresentTenseStatement { get; set; }
      [DataMember]
      public long EducationInternshipSkillId { get; set; }
    }

    [Serializable]
    [DataContract(Name="SkillsForJobsView", Namespace = Constants.DataContractNamespace)]
    public class SkillsForJobsViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public SkillTypes SkillType { get; set; }
      [DataMember]
      public int? Rank { get; set; }
      [DataMember]
      public decimal? DemandPercentile { get; set; }
    }

    [Serializable]
    [DataContract(Name="DegreeEducationLevelROnetView", Namespace = Constants.DataContractNamespace)]
    public class DegreeEducationLevelROnetViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long DegreeEducationLevelId { get; set; }
      [DataMember]
      public string ROnet { get; set; }
      [DataMember]
      public long? ProgramAreaId { get; set; }
    }

    [Serializable]
    [DataContract(Name="CareerAreaJobDegreeView", Namespace = Constants.DataContractNamespace)]
    public class CareerAreaJobDegreeViewDto
    {
      [DataMember]
      public System.Guid? Id { get; set; }
      [DataMember]
      public long CareerAreaId { get; set; }
      [DataMember]
      public long JobId { get; set; }
      [DataMember]
      public long DegreeEducationLevelId { get; set; }
      [DataMember]
      public ExplorerEducationLevels EducationLevel { get; set; }
      [DataMember]
      public long DegreeId { get; set; }
      [DataMember]
      public bool IsClientData { get; set; }
      [DataMember]
      public string ClientDataTag { get; set; }
    }

    [Serializable]
    [DataContract(Name="DegreeEducationLevelLensMapping", Namespace = Constants.DataContractNamespace)]
    public class DegreeEducationLevelLensMappingDto
    {
      [DataMember]
      public int? Id { get; set; }
      [DataMember]
      public int LensId { get; set; }
      [DataMember]
      public long DegreeEducationLevelId { get; set; }
      [DataMember]
      public string RcipCode { get; set; }
      [DataMember]
      public long? ProgramAreaId { get; set; }
    }

    [Serializable]
    [DataContract(Name="Onet17Onet12Mapping", Namespace = Constants.DataContractNamespace)]
    public class Onet17Onet12MappingDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Onet17Code { get; set; }
      [DataMember]
      public long OnetId { get; set; }
    }

    [Serializable]
    [DataContract(Name="Onet17Onet12MappingView", Namespace = Constants.DataContractNamespace)]
    public class Onet17Onet12MappingViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Onet17Code { get; set; }
      [DataMember]
      public string Onet12Code { get; set; }
      [DataMember]
      public long OnetId { get; set; }
      [DataMember]
      public string Occupation { get; set; }
    }

    [Serializable]
    [DataContract(Name="ROnetOnet", Namespace = Constants.DataContractNamespace)]
    public class ROnetOnetDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public bool BestFit { get; set; }
      [DataMember]
      public long OnetId { get; set; }
      [DataMember]
      public long ROnetId { get; set; }
    }

    [Serializable]
    [DataContract(Name="OnetToROnetConversionView", Namespace = Constants.DataContractNamespace)]
    public class OnetToROnetConversionViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long OnetId { get; set; }
      [DataMember]
      public string OnetCode { get; set; }
      [DataMember]
      public string OnetKey { get; set; }
      [DataMember]
      public long ROnetId { get; set; }
      [DataMember]
      public string ROnetCode { get; set; }
      [DataMember]
      public string ROnetKey { get; set; }
      [DataMember]
      public bool BestFit { get; set; }
    }

    [Serializable]
    [DataContract(Name="CareerAreaJobReportView", Namespace = Constants.DataContractNamespace)]
    public class CareerAreaJobReportViewDto
    {
      [DataMember]
      public System.Guid? Id { get; set; }
      [DataMember]
      public long CareerAreaId { get; set; }
      [DataMember]
      public long StateAreaId { get; set; }
      [DataMember]
      public long JobId { get; set; }
      [DataMember]
      public int HiringDemand { get; set; }
    }

    [Serializable]
    [DataContract(Name="SOC", Namespace = Constants.DataContractNamespace)]
    public class SOCDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Soc2010Code { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public string Title { get; set; }
    }

    [Serializable]
    [DataContract(Name="OnetSOC", Namespace = Constants.DataContractNamespace)]
    public class OnetSOCDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long OnetId { get; set; }
      [DataMember]
      public long SocId { get; set; }
    }


}
*/

#endregion

#region Data Transfer Objects - Mappers

/*
namespace Focus.Services.DtoMappers
{
    using Focus.Core.DataTransferObjects.FocusLibrary.Entities;
    using Focus.Data.Library.Entities;

    public static class FocusLibrary.EntitiesMappers
    {

      #region LocalisationDto Mappers

      public static LocalisationDto AsDto(this Localisation entity)
      {
        var dto = new LocalisationDto
        {
          Culture = entity.Culture,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static Localisation CopyTo(this LocalisationDto dto, Localisation entity)
      {
        entity.Culture = dto.Culture;
        return entity;
      }

      public static Localisation CopyTo(this LocalisationDto dto)
      {
        var entity = new Localisation();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region StateDto Mappers

      public static StateDto AsDto(this State entity)
      {
        var dto = new StateDto
        {
          CountryId = entity.CountryId,
          Name = entity.Name,
          Code = entity.Code,
          SortOrder = entity.SortOrder,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static State CopyTo(this StateDto dto, State entity)
      {
        entity.CountryId = dto.CountryId;
        entity.Name = dto.Name;
        entity.Code = dto.Code;
        entity.SortOrder = dto.SortOrder;
        return entity;
      }

      public static State CopyTo(this StateDto dto)
      {
        var entity = new State();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region AreaDto Mappers

      public static AreaDto AsDto(this Area entity)
      {
        var dto = new AreaDto
        {
          Name = entity.Name,
          SortOrder = entity.SortOrder,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static Area CopyTo(this AreaDto dto, Area entity)
      {
        entity.Name = dto.Name;
        entity.SortOrder = dto.SortOrder;
        return entity;
      }

      public static Area CopyTo(this AreaDto dto)
      {
        var entity = new Area();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobReportDto Mappers

      public static JobReportDto AsDto(this JobReport entity)
      {
        var dto = new JobReportDto
        {
          HiringDemand = entity.HiringDemand,
          SalaryTrend = entity.SalaryTrend,
          SalaryTrendPercentile = entity.SalaryTrendPercentile,
          SalaryTrendAverage = entity.SalaryTrendAverage,
          LocalisationId = entity.LocalisationId,
          GrowthTrend = entity.GrowthTrend,
          GrowthPercentile = entity.GrowthPercentile,
          HiringTrend = entity.HiringTrend,
          HiringTrendPercentile = entity.HiringTrendPercentile,
          SalaryTrendMin = entity.SalaryTrendMin,
          SalaryTrendMax = entity.SalaryTrendMax,
          SalaryTrendRealtime = entity.SalaryTrendRealtime,
          SalaryTrendRealtimeAverage = entity.SalaryTrendRealtimeAverage,
          SalaryTrendRealtimeMin = entity.SalaryTrendRealtimeMin,
          SalaryTrendRealtimeMax = entity.SalaryTrendRealtimeMax,
          HiringTrendSubLevel = entity.HiringTrendSubLevel,
          JobId = entity.JobId,
          StateAreaId = entity.StateAreaId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobReport CopyTo(this JobReportDto dto, JobReport entity)
      {
        entity.HiringDemand = dto.HiringDemand;
        entity.SalaryTrend = dto.SalaryTrend;
        entity.SalaryTrendPercentile = dto.SalaryTrendPercentile;
        entity.SalaryTrendAverage = dto.SalaryTrendAverage;
        entity.LocalisationId = dto.LocalisationId;
        entity.GrowthTrend = dto.GrowthTrend;
        entity.GrowthPercentile = dto.GrowthPercentile;
        entity.HiringTrend = dto.HiringTrend;
        entity.HiringTrendPercentile = dto.HiringTrendPercentile;
        entity.SalaryTrendMin = dto.SalaryTrendMin;
        entity.SalaryTrendMax = dto.SalaryTrendMax;
        entity.SalaryTrendRealtime = dto.SalaryTrendRealtime;
        entity.SalaryTrendRealtimeAverage = dto.SalaryTrendRealtimeAverage;
        entity.SalaryTrendRealtimeMin = dto.SalaryTrendRealtimeMin;
        entity.SalaryTrendRealtimeMax = dto.SalaryTrendRealtimeMax;
        entity.HiringTrendSubLevel = dto.HiringTrendSubLevel;
        entity.JobId = dto.JobId;
        entity.StateAreaId = dto.StateAreaId;
        return entity;
      }

      public static JobReport CopyTo(this JobReportDto dto)
      {
        var entity = new JobReport();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobDto Mappers

      public static JobDto AsDto(this Job entity)
      {
        var dto = new JobDto
        {
          Name = entity.Name,
          ROnet = entity.ROnet,
          LocalisationId = entity.LocalisationId,
          DegreeIntroStatement = entity.DegreeIntroStatement,
          Description = entity.Description,
          StarterJob = entity.StarterJob,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static Job CopyTo(this JobDto dto, Job entity)
      {
        entity.Name = dto.Name;
        entity.ROnet = dto.ROnet;
        entity.LocalisationId = dto.LocalisationId;
        entity.DegreeIntroStatement = dto.DegreeIntroStatement;
        entity.Description = dto.Description;
        entity.StarterJob = dto.StarterJob;
        return entity;
      }

      public static Job CopyTo(this JobDto dto)
      {
        var entity = new Job();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region StudyPlaceDto Mappers

      public static StudyPlaceDto AsDto(this StudyPlace entity)
      {
        var dto = new StudyPlaceDto
        {
          Name = entity.Name,
          StateAreaId = entity.StateAreaId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static StudyPlace CopyTo(this StudyPlaceDto dto, StudyPlace entity)
      {
        entity.Name = dto.Name;
        entity.StateAreaId = dto.StateAreaId;
        return entity;
      }

      public static StudyPlace CopyTo(this StudyPlaceDto dto)
      {
        var entity = new StudyPlace();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region EmployerDto Mappers

      public static EmployerDto AsDto(this Employer entity)
      {
        var dto = new EmployerDto
        {
          Name = entity.Name,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static Employer CopyTo(this EmployerDto dto, Employer entity)
      {
        entity.Name = dto.Name;
        return entity;
      }

      public static Employer CopyTo(this EmployerDto dto)
      {
        var entity = new Employer();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region EmployerJobDto Mappers

      public static EmployerJobDto AsDto(this EmployerJob entity)
      {
        var dto = new EmployerJobDto
        {
          Positions = entity.Positions,
          JobId = entity.JobId,
          EmployerId = entity.EmployerId,
          StateAreaId = entity.StateAreaId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static EmployerJob CopyTo(this EmployerJobDto dto, EmployerJob entity)
      {
        entity.Positions = dto.Positions;
        entity.JobId = dto.JobId;
        entity.EmployerId = dto.EmployerId;
        entity.StateAreaId = dto.StateAreaId;
        return entity;
      }

      public static EmployerJob CopyTo(this EmployerJobDto dto)
      {
        var entity = new EmployerJob();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region EmployerJobViewDto Mappers

      public static EmployerJobViewDto AsDto(this EmployerJobView entity)
      {
        var dto = new EmployerJobViewDto
        {
          EmployerName = entity.EmployerName,
          JobId = entity.JobId,
          JobName = entity.JobName,
          Positions = entity.Positions,
          EmployerId = entity.EmployerId,
          StateAreaId = entity.StateAreaId,
          ROnet = entity.ROnet,
          StarterJob = entity.StarterJob,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static EmployerJobView CopyTo(this EmployerJobViewDto dto, EmployerJobView entity)
      {
        entity.EmployerName = dto.EmployerName;
        entity.JobId = dto.JobId;
        entity.JobName = dto.JobName;
        entity.Positions = dto.Positions;
        entity.EmployerId = dto.EmployerId;
        entity.StateAreaId = dto.StateAreaId;
        entity.ROnet = dto.ROnet;
        entity.StarterJob = dto.StarterJob;
        return entity;
      }

      public static EmployerJobView CopyTo(this EmployerJobViewDto dto)
      {
        var entity = new EmployerJobView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSkillViewDto Mappers

      public static JobSkillViewDto AsDto(this JobSkillView entity)
      {
        var dto = new JobSkillViewDto
        {
          JobId = entity.JobId,
          JobSkillType = entity.JobSkillType,
          SkillType = entity.SkillType,
          SkillId = entity.SkillId,
          DemandPercentile = entity.DemandPercentile,
          SkillName = entity.SkillName,
          Rank = entity.Rank,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSkillView CopyTo(this JobSkillViewDto dto, JobSkillView entity)
      {
        entity.JobId = dto.JobId;
        entity.JobSkillType = dto.JobSkillType;
        entity.SkillType = dto.SkillType;
        entity.SkillId = dto.SkillId;
        entity.DemandPercentile = dto.DemandPercentile;
        entity.SkillName = dto.SkillName;
        entity.Rank = dto.Rank;
        return entity;
      }

      public static JobSkillView CopyTo(this JobSkillViewDto dto)
      {
        var entity = new JobSkillView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region SkillCategoryDto Mappers

      public static SkillCategoryDto AsDto(this SkillCategory entity)
      {
        var dto = new SkillCategoryDto
        {
          Name = entity.Name,
          LocalisationId = entity.LocalisationId,
          ParentId = entity.ParentId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static SkillCategory CopyTo(this SkillCategoryDto dto, SkillCategory entity)
      {
        entity.Name = dto.Name;
        entity.LocalisationId = dto.LocalisationId;
        entity.ParentId = dto.ParentId;
        return entity;
      }

      public static SkillCategory CopyTo(this SkillCategoryDto dto)
      {
        var entity = new SkillCategory();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region SkillCategorySkillDto Mappers

      public static SkillCategorySkillDto AsDto(this SkillCategorySkill entity)
      {
        var dto = new SkillCategorySkillDto
        {
          SkillCategoryId = entity.SkillCategoryId,
          SkillId = entity.SkillId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static SkillCategorySkill CopyTo(this SkillCategorySkillDto dto, SkillCategorySkill entity)
      {
        entity.SkillCategoryId = dto.SkillCategoryId;
        entity.SkillId = dto.SkillId;
        return entity;
      }

      public static SkillCategorySkill CopyTo(this SkillCategorySkillDto dto)
      {
        var entity = new SkillCategorySkill();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region CareerAreaDto Mappers

      public static CareerAreaDto AsDto(this CareerArea entity)
      {
        var dto = new CareerAreaDto
        {
          Name = entity.Name,
          LocalisationId = entity.LocalisationId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static CareerArea CopyTo(this CareerAreaDto dto, CareerArea entity)
      {
        entity.Name = dto.Name;
        entity.LocalisationId = dto.LocalisationId;
        return entity;
      }

      public static CareerArea CopyTo(this CareerAreaDto dto)
      {
        var entity = new CareerArea();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobCareerAreaDto Mappers

      public static JobCareerAreaDto AsDto(this JobCareerArea entity)
      {
        var dto = new JobCareerAreaDto
        {
          JobId = entity.JobId,
          CareerAreaId = entity.CareerAreaId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobCareerArea CopyTo(this JobCareerAreaDto dto, JobCareerArea entity)
      {
        entity.JobId = dto.JobId;
        entity.CareerAreaId = dto.CareerAreaId;
        return entity;
      }

      public static JobCareerArea CopyTo(this JobCareerAreaDto dto)
      {
        var entity = new JobCareerArea();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSkillDto Mappers

      public static JobSkillDto AsDto(this JobSkill entity)
      {
        var dto = new JobSkillDto
        {
          JobSkillType = entity.JobSkillType,
          DemandPercentile = entity.DemandPercentile,
          Rank = entity.Rank,
          JobId = entity.JobId,
          SkillId = entity.SkillId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSkill CopyTo(this JobSkillDto dto, JobSkill entity)
      {
        entity.JobSkillType = dto.JobSkillType;
        entity.DemandPercentile = dto.DemandPercentile;
        entity.Rank = dto.Rank;
        entity.JobId = dto.JobId;
        entity.SkillId = dto.SkillId;
        return entity;
      }

      public static JobSkill CopyTo(this JobSkillDto dto)
      {
        var entity = new JobSkill();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region SkillDto Mappers

      public static SkillDto AsDto(this Skill entity)
      {
        var dto = new SkillDto
        {
          SkillType = entity.SkillType,
          Name = entity.Name,
          LocalisationId = entity.LocalisationId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static Skill CopyTo(this SkillDto dto, Skill entity)
      {
        entity.SkillType = dto.SkillType;
        entity.Name = dto.Name;
        entity.LocalisationId = dto.LocalisationId;
        return entity;
      }

      public static Skill CopyTo(this SkillDto dto)
      {
        var entity = new Skill();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobRelatedJobDto Mappers

      public static JobRelatedJobDto AsDto(this JobRelatedJob entity)
      {
        var dto = new JobRelatedJobDto
        {
          RelatedJobId = entity.RelatedJobId,
          Priority = entity.Priority,
          JobId = entity.JobId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobRelatedJob CopyTo(this JobRelatedJobDto dto, JobRelatedJob entity)
      {
        entity.RelatedJobId = dto.RelatedJobId;
        entity.Priority = dto.Priority;
        entity.JobId = dto.JobId;
        return entity;
      }

      public static JobRelatedJob CopyTo(this JobRelatedJobDto dto)
      {
        var entity = new JobRelatedJob();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region StateAreaDto Mappers

      public static StateAreaDto AsDto(this StateArea entity)
      {
        var dto = new StateAreaDto
        {
          AreaCode = entity.AreaCode,
          Display = entity.Display,
          IsDefault = entity.IsDefault,
          StateId = entity.StateId,
          AreaId = entity.AreaId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static StateArea CopyTo(this StateAreaDto dto, StateArea entity)
      {
        entity.AreaCode = dto.AreaCode;
        entity.Display = dto.Display;
        entity.IsDefault = dto.IsDefault;
        entity.StateId = dto.StateId;
        entity.AreaId = dto.AreaId;
        return entity;
      }

      public static StateArea CopyTo(this StateAreaDto dto)
      {
        var entity = new StateArea();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobReportViewDto Mappers

      public static JobReportViewDto AsDto(this JobReportView entity)
      {
        var dto = new JobReportViewDto
        {
          HiringDemand = entity.HiringDemand,
          GrowthTrend = entity.GrowthTrend,
          GrowthPercentile = entity.GrowthPercentile,
          SalaryTrend = entity.SalaryTrend,
          SalaryTrendPercentile = entity.SalaryTrendPercentile,
          SalaryTrendAverage = entity.SalaryTrendAverage,
          JobId = entity.JobId,
          Name = entity.Name,
          HiringTrend = entity.HiringTrend,
          HiringTrendPercentile = entity.HiringTrendPercentile,
          SalaryTrendMin = entity.SalaryTrendMin,
          SalaryTrendMax = entity.SalaryTrendMax,
          SalaryTrendRealtime = entity.SalaryTrendRealtime,
          SalaryTrendRealtimeAverage = entity.SalaryTrendRealtimeAverage,
          SalaryTrendRealtimeMin = entity.SalaryTrendRealtimeMin,
          SalaryTrendRealtimeMax = entity.SalaryTrendRealtimeMax,
          StateAreaId = entity.StateAreaId,
          DegreeIntroStatement = entity.DegreeIntroStatement,
          ROnet = entity.ROnet,
          StarterJob = entity.StarterJob,
          HiringTrendSubLevel = entity.HiringTrendSubLevel,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobReportView CopyTo(this JobReportViewDto dto, JobReportView entity)
      {
        entity.HiringDemand = dto.HiringDemand;
        entity.GrowthTrend = dto.GrowthTrend;
        entity.GrowthPercentile = dto.GrowthPercentile;
        entity.SalaryTrend = dto.SalaryTrend;
        entity.SalaryTrendPercentile = dto.SalaryTrendPercentile;
        entity.SalaryTrendAverage = dto.SalaryTrendAverage;
        entity.JobId = dto.JobId;
        entity.Name = dto.Name;
        entity.HiringTrend = dto.HiringTrend;
        entity.HiringTrendPercentile = dto.HiringTrendPercentile;
        entity.SalaryTrendMin = dto.SalaryTrendMin;
        entity.SalaryTrendMax = dto.SalaryTrendMax;
        entity.SalaryTrendRealtime = dto.SalaryTrendRealtime;
        entity.SalaryTrendRealtimeAverage = dto.SalaryTrendRealtimeAverage;
        entity.SalaryTrendRealtimeMin = dto.SalaryTrendRealtimeMin;
        entity.SalaryTrendRealtimeMax = dto.SalaryTrendRealtimeMax;
        entity.StateAreaId = dto.StateAreaId;
        entity.DegreeIntroStatement = dto.DegreeIntroStatement;
        entity.ROnet = dto.ROnet;
        entity.StarterJob = dto.StarterJob;
        entity.HiringTrendSubLevel = dto.HiringTrendSubLevel;
        return entity;
      }

      public static JobReportView CopyTo(this JobReportViewDto dto)
      {
        var entity = new JobReportView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobDegreeLevelDto Mappers

      public static JobDegreeLevelDto AsDto(this JobDegreeLevel entity)
      {
        var dto = new JobDegreeLevelDto
        {
          DegreeLevel = entity.DegreeLevel,
          JobId = entity.JobId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobDegreeLevel CopyTo(this JobDegreeLevelDto dto, JobDegreeLevel entity)
      {
        entity.DegreeLevel = dto.DegreeLevel;
        entity.JobId = dto.JobId;
        return entity;
      }

      public static JobDegreeLevel CopyTo(this JobDegreeLevelDto dto)
      {
        var entity = new JobDegreeLevel();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region SkillCategorySkillViewDto Mappers

      public static SkillCategorySkillViewDto AsDto(this SkillCategorySkillView entity)
      {
        var dto = new SkillCategorySkillViewDto
        {
          SkillCategoryId = entity.SkillCategoryId,
          SkillCategoryName = entity.SkillCategoryName,
          SkillId = entity.SkillId,
          SkillName = entity.SkillName,
          SkillCategoryParentId = entity.SkillCategoryParentId,
          SkillType = entity.SkillType,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static SkillCategorySkillView CopyTo(this SkillCategorySkillViewDto dto, SkillCategorySkillView entity)
      {
        entity.SkillCategoryId = dto.SkillCategoryId;
        entity.SkillCategoryName = dto.SkillCategoryName;
        entity.SkillId = dto.SkillId;
        entity.SkillName = dto.SkillName;
        entity.SkillCategoryParentId = dto.SkillCategoryParentId;
        entity.SkillType = dto.SkillType;
        return entity;
      }

      public static SkillCategorySkillView CopyTo(this SkillCategorySkillViewDto dto)
      {
        var entity = new SkillCategorySkillView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobJobTitleDto Mappers

      public static JobJobTitleDto AsDto(this JobJobTitle entity)
      {
        var dto = new JobJobTitleDto
        {
          DemandPercentile = entity.DemandPercentile,
          JobId = entity.JobId,
          JobTitleId = entity.JobTitleId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobJobTitle CopyTo(this JobJobTitleDto dto, JobJobTitle entity)
      {
        entity.DemandPercentile = dto.DemandPercentile;
        entity.JobId = dto.JobId;
        entity.JobTitleId = dto.JobTitleId;
        return entity;
      }

      public static JobJobTitle CopyTo(this JobJobTitleDto dto)
      {
        var entity = new JobJobTitle();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobTitleDto Mappers

      public static JobTitleDto AsDto(this JobTitle entity)
      {
        var dto = new JobTitleDto
        {
          Name = entity.Name,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobTitle CopyTo(this JobTitleDto dto, JobTitle entity)
      {
        entity.Name = dto.Name;
        return entity;
      }

      public static JobTitle CopyTo(this JobTitleDto dto)
      {
        var entity = new JobTitle();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobEducationRequirementDto Mappers

      public static JobEducationRequirementDto AsDto(this JobEducationRequirement entity)
      {
        var dto = new JobEducationRequirementDto
        {
          EducationRequirementType = entity.EducationRequirementType,
          RequirementPercentile = entity.RequirementPercentile,
          JobId = entity.JobId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobEducationRequirement CopyTo(this JobEducationRequirementDto dto, JobEducationRequirement entity)
      {
        entity.EducationRequirementType = dto.EducationRequirementType;
        entity.RequirementPercentile = dto.RequirementPercentile;
        entity.JobId = dto.JobId;
        return entity;
      }

      public static JobEducationRequirement CopyTo(this JobEducationRequirementDto dto)
      {
        var entity = new JobEducationRequirement();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobExperienceLevelDto Mappers

      public static JobExperienceLevelDto AsDto(this JobExperienceLevel entity)
      {
        var dto = new JobExperienceLevelDto
        {
          ExperienceLevel = entity.ExperienceLevel,
          ExperiencePercentile = entity.ExperiencePercentile,
          JobId = entity.JobId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobExperienceLevel CopyTo(this JobExperienceLevelDto dto, JobExperienceLevel entity)
      {
        entity.ExperienceLevel = dto.ExperienceLevel;
        entity.ExperiencePercentile = dto.ExperiencePercentile;
        entity.JobId = dto.JobId;
        return entity;
      }

      public static JobExperienceLevel CopyTo(this JobExperienceLevelDto dto)
      {
        var entity = new JobExperienceLevel();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region EmployerSkillViewDto Mappers

      public static EmployerSkillViewDto AsDto(this EmployerSkillView entity)
      {
        var dto = new EmployerSkillViewDto
        {
          SkillId = entity.SkillId,
          SkillType = entity.SkillType,
          SkillName = entity.SkillName,
          EmployerId = entity.EmployerId,
          Postings = entity.Postings,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static EmployerSkillView CopyTo(this EmployerSkillViewDto dto, EmployerSkillView entity)
      {
        entity.SkillId = dto.SkillId;
        entity.SkillType = dto.SkillType;
        entity.SkillName = dto.SkillName;
        entity.EmployerId = dto.EmployerId;
        entity.Postings = dto.Postings;
        return entity;
      }

      public static EmployerSkillView CopyTo(this EmployerSkillViewDto dto)
      {
        var entity = new EmployerSkillView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region SkillReportViewDto Mappers

      public static SkillReportViewDto AsDto(this SkillReportView entity)
      {
        var dto = new SkillReportViewDto
        {
          SkillType = entity.SkillType,
          SkillName = entity.SkillName,
          SkillId = entity.SkillId,
          Frequency = entity.Frequency,
          StateAreaId = entity.StateAreaId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static SkillReportView CopyTo(this SkillReportViewDto dto, SkillReportView entity)
      {
        entity.SkillType = dto.SkillType;
        entity.SkillName = dto.SkillName;
        entity.SkillId = dto.SkillId;
        entity.Frequency = dto.Frequency;
        entity.StateAreaId = dto.StateAreaId;
        return entity;
      }

      public static SkillReportView CopyTo(this SkillReportViewDto dto)
      {
        var entity = new SkillReportView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region EmployerSkillDto Mappers

      public static EmployerSkillDto AsDto(this EmployerSkill entity)
      {
        var dto = new EmployerSkillDto
        {
          Postings = entity.Postings,
          EmployerId = entity.EmployerId,
          SkillId = entity.SkillId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static EmployerSkill CopyTo(this EmployerSkillDto dto, EmployerSkill entity)
      {
        entity.Postings = dto.Postings;
        entity.EmployerId = dto.EmployerId;
        entity.SkillId = dto.SkillId;
        return entity;
      }

      public static EmployerSkill CopyTo(this EmployerSkillDto dto)
      {
        var entity = new EmployerSkill();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region SkillReportDto Mappers

      public static SkillReportDto AsDto(this SkillReport entity)
      {
        var dto = new SkillReportDto
        {
          Frequency = entity.Frequency,
          SkillId = entity.SkillId,
          StateAreaId = entity.StateAreaId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static SkillReport CopyTo(this SkillReportDto dto, SkillReport entity)
      {
        entity.Frequency = dto.Frequency;
        entity.SkillId = dto.SkillId;
        entity.StateAreaId = dto.StateAreaId;
        return entity;
      }

      public static SkillReport CopyTo(this SkillReportDto dto)
      {
        var entity = new SkillReport();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobCareerAreaSkillViewDto Mappers

      public static JobCareerAreaSkillViewDto AsDto(this JobCareerAreaSkillView entity)
      {
        var dto = new JobCareerAreaSkillViewDto
        {
          SkillId = entity.SkillId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobCareerAreaSkillView CopyTo(this JobCareerAreaSkillViewDto dto, JobCareerAreaSkillView entity)
      {
        entity.SkillId = dto.SkillId;
        return entity;
      }

      public static JobCareerAreaSkillView CopyTo(this JobCareerAreaSkillViewDto dto)
      {
        var entity = new JobCareerAreaSkillView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobDegreeLevelSkillViewDto Mappers

      public static JobDegreeLevelSkillViewDto AsDto(this JobDegreeLevelSkillView entity)
      {
        var dto = new JobDegreeLevelSkillViewDto
        {
          SkillId = entity.SkillId,
          DegreeLevel = entity.DegreeLevel,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobDegreeLevelSkillView CopyTo(this JobDegreeLevelSkillViewDto dto, JobDegreeLevelSkillView entity)
      {
        entity.SkillId = dto.SkillId;
        entity.DegreeLevel = dto.DegreeLevel;
        return entity;
      }

      public static JobDegreeLevelSkillView CopyTo(this JobDegreeLevelSkillViewDto dto)
      {
        var entity = new JobDegreeLevelSkillView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobJobTitleSkillViewDto Mappers

      public static JobJobTitleSkillViewDto AsDto(this JobJobTitleSkillView entity)
      {
        var dto = new JobJobTitleSkillViewDto
        {
          SkillId = entity.SkillId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobJobTitleSkillView CopyTo(this JobJobTitleSkillViewDto dto, JobJobTitleSkillView entity)
      {
        entity.SkillId = dto.SkillId;
        return entity;
      }

      public static JobJobTitleSkillView CopyTo(this JobJobTitleSkillViewDto dto)
      {
        var entity = new JobJobTitleSkillView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region InternshipCategoryDto Mappers

      public static InternshipCategoryDto AsDto(this InternshipCategory entity)
      {
        var dto = new InternshipCategoryDto
        {
          Name = entity.Name,
          LocalisationId = entity.LocalisationId,
          LensFilterId = entity.LensFilterId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static InternshipCategory CopyTo(this InternshipCategoryDto dto, InternshipCategory entity)
      {
        entity.Name = dto.Name;
        entity.LocalisationId = dto.LocalisationId;
        entity.LensFilterId = dto.LensFilterId;
        return entity;
      }

      public static InternshipCategory CopyTo(this InternshipCategoryDto dto)
      {
        var entity = new InternshipCategory();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region InternshipSkillDto Mappers

      public static InternshipSkillDto AsDto(this InternshipSkill entity)
      {
        var dto = new InternshipSkillDto
        {
          Rank = entity.Rank,
          SkillId = entity.SkillId,
          InternshipCategoryId = entity.InternshipCategoryId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static InternshipSkill CopyTo(this InternshipSkillDto dto, InternshipSkill entity)
      {
        entity.Rank = dto.Rank;
        entity.SkillId = dto.SkillId;
        entity.InternshipCategoryId = dto.InternshipCategoryId;
        return entity;
      }

      public static InternshipSkill CopyTo(this InternshipSkillDto dto)
      {
        var entity = new InternshipSkill();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region InternshipEmployerDto Mappers

      public static InternshipEmployerDto AsDto(this InternshipEmployer entity)
      {
        var dto = new InternshipEmployerDto
        {
          Frequency = entity.Frequency,
          EmployerId = entity.EmployerId,
          StateAreaId = entity.StateAreaId,
          InternshipCategoryId = entity.InternshipCategoryId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static InternshipEmployer CopyTo(this InternshipEmployerDto dto, InternshipEmployer entity)
      {
        entity.Frequency = dto.Frequency;
        entity.EmployerId = dto.EmployerId;
        entity.StateAreaId = dto.StateAreaId;
        entity.InternshipCategoryId = dto.InternshipCategoryId;
        return entity;
      }

      public static InternshipEmployer CopyTo(this InternshipEmployerDto dto)
      {
        var entity = new InternshipEmployer();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region InternshipJobTitleDto Mappers

      public static InternshipJobTitleDto AsDto(this InternshipJobTitle entity)
      {
        var dto = new InternshipJobTitleDto
        {
          JobTitle = entity.JobTitle,
          Frequency = entity.Frequency,
          InternshipCategoryId = entity.InternshipCategoryId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static InternshipJobTitle CopyTo(this InternshipJobTitleDto dto, InternshipJobTitle entity)
      {
        entity.JobTitle = dto.JobTitle;
        entity.Frequency = dto.Frequency;
        entity.InternshipCategoryId = dto.InternshipCategoryId;
        return entity;
      }

      public static InternshipJobTitle CopyTo(this InternshipJobTitleDto dto)
      {
        var entity = new InternshipJobTitle();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region InternshipReportDto Mappers

      public static InternshipReportDto AsDto(this InternshipReport entity)
      {
        var dto = new InternshipReportDto
        {
          NumberAvailable = entity.NumberAvailable,
          StateAreaId = entity.StateAreaId,
          InternshipCategoryId = entity.InternshipCategoryId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static InternshipReport CopyTo(this InternshipReportDto dto, InternshipReport entity)
      {
        entity.NumberAvailable = dto.NumberAvailable;
        entity.StateAreaId = dto.StateAreaId;
        entity.InternshipCategoryId = dto.InternshipCategoryId;
        return entity;
      }

      public static InternshipReport CopyTo(this InternshipReportDto dto)
      {
        var entity = new InternshipReport();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region InternshipEmployerViewDto Mappers

      public static InternshipEmployerViewDto AsDto(this InternshipEmployerView entity)
      {
        var dto = new InternshipEmployerViewDto
        {
          EmployerId = entity.EmployerId,
          Name = entity.Name,
          Frequency = entity.Frequency,
          InternshipCategoryId = entity.InternshipCategoryId,
          StateAreaId = entity.StateAreaId,
          InternshipCategoryName = entity.InternshipCategoryName,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static InternshipEmployerView CopyTo(this InternshipEmployerViewDto dto, InternshipEmployerView entity)
      {
        entity.EmployerId = dto.EmployerId;
        entity.Name = dto.Name;
        entity.Frequency = dto.Frequency;
        entity.InternshipCategoryId = dto.InternshipCategoryId;
        entity.StateAreaId = dto.StateAreaId;
        entity.InternshipCategoryName = dto.InternshipCategoryName;
        return entity;
      }

      public static InternshipEmployerView CopyTo(this InternshipEmployerViewDto dto)
      {
        var entity = new InternshipEmployerView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region InternshipReportViewDto Mappers

      public static InternshipReportViewDto AsDto(this InternshipReportView entity)
      {
        var dto = new InternshipReportViewDto
        {
          InternshipCategoryId = entity.InternshipCategoryId,
          Name = entity.Name,
          NumberAvailable = entity.NumberAvailable,
          StateAreaId = entity.StateAreaId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static InternshipReportView CopyTo(this InternshipReportViewDto dto, InternshipReportView entity)
      {
        entity.InternshipCategoryId = dto.InternshipCategoryId;
        entity.Name = dto.Name;
        entity.NumberAvailable = dto.NumberAvailable;
        entity.StateAreaId = dto.StateAreaId;
        return entity;
      }

      public static InternshipReportView CopyTo(this InternshipReportViewDto dto)
      {
        var entity = new InternshipReportView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region InternshipSkillViewDto Mappers

      public static InternshipSkillViewDto AsDto(this InternshipSkillView entity)
      {
        var dto = new InternshipSkillViewDto
        {
          SkillId = entity.SkillId,
          SkillType = entity.SkillType,
          SkillName = entity.SkillName,
          InternshipCategoryId = entity.InternshipCategoryId,
          Rank = entity.Rank,
          InternshipCategoryName = entity.InternshipCategoryName,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static InternshipSkillView CopyTo(this InternshipSkillViewDto dto, InternshipSkillView entity)
      {
        entity.SkillId = dto.SkillId;
        entity.SkillType = dto.SkillType;
        entity.SkillName = dto.SkillName;
        entity.InternshipCategoryId = dto.InternshipCategoryId;
        entity.Rank = dto.Rank;
        entity.InternshipCategoryName = dto.InternshipCategoryName;
        return entity;
      }

      public static InternshipSkillView CopyTo(this InternshipSkillViewDto dto)
      {
        var entity = new InternshipSkillView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region StateAreaViewDto Mappers

      public static StateAreaViewDto AsDto(this StateAreaView entity)
      {
        var dto = new StateAreaViewDto
        {
          StateId = entity.StateId,
          StateName = entity.StateName,
          StateCode = entity.StateCode,
          StateSortOrder = entity.StateSortOrder,
          AreaId = entity.AreaId,
          AreaName = entity.AreaName,
          AreaSortOrder = entity.AreaSortOrder,
          StateAreaName = entity.StateAreaName,
          AreaCode = entity.AreaCode,
          IsDefault = entity.IsDefault,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static StateAreaView CopyTo(this StateAreaViewDto dto, StateAreaView entity)
      {
        entity.StateId = dto.StateId;
        entity.StateName = dto.StateName;
        entity.StateCode = dto.StateCode;
        entity.StateSortOrder = dto.StateSortOrder;
        entity.AreaId = dto.AreaId;
        entity.AreaName = dto.AreaName;
        entity.AreaSortOrder = dto.AreaSortOrder;
        entity.StateAreaName = dto.StateAreaName;
        entity.AreaCode = dto.AreaCode;
        entity.IsDefault = dto.IsDefault;
        return entity;
      }

      public static StateAreaView CopyTo(this StateAreaViewDto dto)
      {
        var entity = new StateAreaView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region CertificationDto Mappers

      public static CertificationDto AsDto(this Certification entity)
      {
        var dto = new CertificationDto
        {
          Name = entity.Name,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static Certification CopyTo(this CertificationDto dto, Certification entity)
      {
        entity.Name = dto.Name;
        return entity;
      }

      public static Certification CopyTo(this CertificationDto dto)
      {
        var entity = new Certification();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobCertificationDto Mappers

      public static JobCertificationDto AsDto(this JobCertification entity)
      {
        var dto = new JobCertificationDto
        {
          DemandPercentile = entity.DemandPercentile,
          JobId = entity.JobId,
          CertificationId = entity.CertificationId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobCertification CopyTo(this JobCertificationDto dto, JobCertification entity)
      {
        entity.DemandPercentile = dto.DemandPercentile;
        entity.JobId = dto.JobId;
        entity.CertificationId = dto.CertificationId;
        return entity;
      }

      public static JobCertification CopyTo(this JobCertificationDto dto)
      {
        var entity = new JobCertification();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region DegreeDto Mappers

      public static DegreeDto AsDto(this Degree entity)
      {
        var dto = new DegreeDto
        {
          Name = entity.Name,
          RcipCode = entity.RcipCode,
          IsDegreeArea = entity.IsDegreeArea,
          IsClientData = entity.IsClientData,
          ClientDataTag = entity.ClientDataTag,
          Tier = entity.Tier,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static Degree CopyTo(this DegreeDto dto, Degree entity)
      {
        entity.Name = dto.Name;
        entity.RcipCode = dto.RcipCode;
        entity.IsDegreeArea = dto.IsDegreeArea;
        entity.IsClientData = dto.IsClientData;
        entity.ClientDataTag = dto.ClientDataTag;
        entity.Tier = dto.Tier;
        return entity;
      }

      public static Degree CopyTo(this DegreeDto dto)
      {
        var entity = new Degree();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region DegreeEducationLevelDto Mappers

      public static DegreeEducationLevelDto AsDto(this DegreeEducationLevel entity)
      {
        var dto = new DegreeEducationLevelDto
        {
          EducationLevel = entity.EducationLevel,
          DegreesAwarded = entity.DegreesAwarded,
          Name = entity.Name,
          ExcludeFromReport = entity.ExcludeFromReport,
          DegreeId = entity.DegreeId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static DegreeEducationLevel CopyTo(this DegreeEducationLevelDto dto, DegreeEducationLevel entity)
      {
        entity.EducationLevel = dto.EducationLevel;
        entity.DegreesAwarded = dto.DegreesAwarded;
        entity.Name = dto.Name;
        entity.ExcludeFromReport = dto.ExcludeFromReport;
        entity.DegreeId = dto.DegreeId;
        return entity;
      }

      public static DegreeEducationLevel CopyTo(this DegreeEducationLevelDto dto)
      {
        var entity = new DegreeEducationLevel();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobDegreeEducationLevelDto Mappers

      public static JobDegreeEducationLevelDto AsDto(this JobDegreeEducationLevel entity)
      {
        var dto = new JobDegreeEducationLevelDto
        {
          ExcludeFromJob = entity.ExcludeFromJob,
          Tier = entity.Tier,
          JobId = entity.JobId,
          DegreeEducationLevelId = entity.DegreeEducationLevelId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobDegreeEducationLevel CopyTo(this JobDegreeEducationLevelDto dto, JobDegreeEducationLevel entity)
      {
        entity.ExcludeFromJob = dto.ExcludeFromJob;
        entity.Tier = dto.Tier;
        entity.JobId = dto.JobId;
        entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
        return entity;
      }

      public static JobDegreeEducationLevel CopyTo(this JobDegreeEducationLevelDto dto)
      {
        var entity = new JobDegreeEducationLevel();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region StudyPlaceDegreeEducationLevelDto Mappers

      public static StudyPlaceDegreeEducationLevelDto AsDto(this StudyPlaceDegreeEducationLevel entity)
      {
        var dto = new StudyPlaceDegreeEducationLevelDto
        {
          StudyPlaceId = entity.StudyPlaceId,
          DegreeEducationLevelId = entity.DegreeEducationLevelId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static StudyPlaceDegreeEducationLevel CopyTo(this StudyPlaceDegreeEducationLevelDto dto, StudyPlaceDegreeEducationLevel entity)
      {
        entity.StudyPlaceId = dto.StudyPlaceId;
        entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
        return entity;
      }

      public static StudyPlaceDegreeEducationLevel CopyTo(this StudyPlaceDegreeEducationLevelDto dto)
      {
        var entity = new StudyPlaceDegreeEducationLevel();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region DegreeEducationLevelSkillViewDto Mappers

      public static DegreeEducationLevelSkillViewDto AsDto(this DegreeEducationLevelSkillView entity)
      {
        var dto = new DegreeEducationLevelSkillViewDto
        {
          StateAreaId = entity.StateAreaId,
          SkillId = entity.SkillId,
          SkillType = entity.SkillType,
          SkillName = entity.SkillName,
          DemandPercentile = entity.DemandPercentile,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static DegreeEducationLevelSkillView CopyTo(this DegreeEducationLevelSkillViewDto dto, DegreeEducationLevelSkillView entity)
      {
        entity.StateAreaId = dto.StateAreaId;
        entity.SkillId = dto.SkillId;
        entity.SkillType = dto.SkillType;
        entity.SkillName = dto.SkillName;
        entity.DemandPercentile = dto.DemandPercentile;
        return entity;
      }

      public static DegreeEducationLevelSkillView CopyTo(this DegreeEducationLevelSkillViewDto dto)
      {
        var entity = new DegreeEducationLevelSkillView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region DegreeEducationLevelViewDto Mappers

      public static DegreeEducationLevelViewDto AsDto(this DegreeEducationLevelView entity)
      {
        var dto = new DegreeEducationLevelViewDto
        {
          DegreeId = entity.DegreeId,
          DegreeName = entity.DegreeName,
          EducationLevel = entity.EducationLevel,
          DegreeEducationLevelName = entity.DegreeEducationLevelName,
          IsDegreeArea = entity.IsDegreeArea,
          IsClientData = entity.IsClientData,
          ClientDataTag = entity.ClientDataTag,
          Tier = entity.Tier,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static DegreeEducationLevelView CopyTo(this DegreeEducationLevelViewDto dto, DegreeEducationLevelView entity)
      {
        entity.DegreeId = dto.DegreeId;
        entity.DegreeName = dto.DegreeName;
        entity.EducationLevel = dto.EducationLevel;
        entity.DegreeEducationLevelName = dto.DegreeEducationLevelName;
        entity.IsDegreeArea = dto.IsDegreeArea;
        entity.IsClientData = dto.IsClientData;
        entity.ClientDataTag = dto.ClientDataTag;
        entity.Tier = dto.Tier;
        return entity;
      }

      public static DegreeEducationLevelView CopyTo(this DegreeEducationLevelViewDto dto)
      {
        var entity = new DegreeEducationLevelView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobDegreeEducationLevelSkillViewDto Mappers

      public static JobDegreeEducationLevelSkillViewDto AsDto(this JobDegreeEducationLevelSkillView entity)
      {
        var dto = new JobDegreeEducationLevelSkillViewDto
        {
          SkillId = entity.SkillId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobDegreeEducationLevelSkillView CopyTo(this JobDegreeEducationLevelSkillViewDto dto, JobDegreeEducationLevelSkillView entity)
      {
        entity.SkillId = dto.SkillId;
        return entity;
      }

      public static JobDegreeEducationLevelSkillView CopyTo(this JobDegreeEducationLevelSkillViewDto dto)
      {
        var entity = new JobDegreeEducationLevelSkillView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobDegreeEducationLevelViewDto Mappers

      public static JobDegreeEducationLevelViewDto AsDto(this JobDegreeEducationLevelView entity)
      {
        var dto = new JobDegreeEducationLevelViewDto
        {
          JobId = entity.JobId,
          DegreeEducationLevelId = entity.DegreeEducationLevelId,
          DegreeId = entity.DegreeId,
          DegreeName = entity.DegreeName,
          DegreesAwarded = entity.DegreesAwarded,
          DegreeEducationLevelName = entity.DegreeEducationLevelName,
          EducationLevel = entity.EducationLevel,
          IsDegreeArea = entity.IsDegreeArea,
          IsClientData = entity.IsClientData,
          ClientDataTag = entity.ClientDataTag,
          ExcludeFromJob = entity.ExcludeFromJob,
          Tier = entity.Tier,
          ROnet = entity.ROnet,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobDegreeEducationLevelView CopyTo(this JobDegreeEducationLevelViewDto dto, JobDegreeEducationLevelView entity)
      {
        entity.JobId = dto.JobId;
        entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
        entity.DegreeId = dto.DegreeId;
        entity.DegreeName = dto.DegreeName;
        entity.DegreesAwarded = dto.DegreesAwarded;
        entity.DegreeEducationLevelName = dto.DegreeEducationLevelName;
        entity.EducationLevel = dto.EducationLevel;
        entity.IsDegreeArea = dto.IsDegreeArea;
        entity.IsClientData = dto.IsClientData;
        entity.ClientDataTag = dto.ClientDataTag;
        entity.ExcludeFromJob = dto.ExcludeFromJob;
        entity.Tier = dto.Tier;
        entity.ROnet = dto.ROnet;
        return entity;
      }

      public static JobDegreeEducationLevelView CopyTo(this JobDegreeEducationLevelViewDto dto)
      {
        var entity = new JobDegreeEducationLevelView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobCertificationViewDto Mappers

      public static JobCertificationViewDto AsDto(this JobCertificationView entity)
      {
        var dto = new JobCertificationViewDto
        {
          JobId = entity.JobId,
          CertificationId = entity.CertificationId,
          CertificationName = entity.CertificationName,
          DemandPercentile = entity.DemandPercentile,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobCertificationView CopyTo(this JobCertificationViewDto dto, JobCertificationView entity)
      {
        entity.JobId = dto.JobId;
        entity.CertificationId = dto.CertificationId;
        entity.CertificationName = dto.CertificationName;
        entity.DemandPercentile = dto.DemandPercentile;
        return entity;
      }

      public static JobCertificationView CopyTo(this JobCertificationViewDto dto)
      {
        var entity = new JobCertificationView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobDegreeEducationLevelReportDto Mappers

      public static JobDegreeEducationLevelReportDto AsDto(this JobDegreeEducationLevelReport entity)
      {
        var dto = new JobDegreeEducationLevelReportDto
        {
          HiringDemand = entity.HiringDemand,
          DegreesAwarded = entity.DegreesAwarded,
          StateAreaId = entity.StateAreaId,
          JobDegreeEducationLevelId = entity.JobDegreeEducationLevelId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobDegreeEducationLevelReport CopyTo(this JobDegreeEducationLevelReportDto dto, JobDegreeEducationLevelReport entity)
      {
        entity.HiringDemand = dto.HiringDemand;
        entity.DegreesAwarded = dto.DegreesAwarded;
        entity.StateAreaId = dto.StateAreaId;
        entity.JobDegreeEducationLevelId = dto.JobDegreeEducationLevelId;
        return entity;
      }

      public static JobDegreeEducationLevelReport CopyTo(this JobDegreeEducationLevelReportDto dto)
      {
        var entity = new JobDegreeEducationLevelReport();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobDegreeEducationLevelReportViewDto Mappers

      public static JobDegreeEducationLevelReportViewDto AsDto(this JobDegreeEducationLevelReportView entity)
      {
        var dto = new JobDegreeEducationLevelReportViewDto
        {
          StateAreaId = entity.StateAreaId,
          JobDegreeEducationLevelId = entity.JobDegreeEducationLevelId,
          HiringDemand = entity.HiringDemand,
          DegreesAwarded = entity.DegreesAwarded,
          JobId = entity.JobId,
          DegreeEducationLevelId = entity.DegreeEducationLevelId,
          EducationLevel = entity.EducationLevel,
          DegreeId = entity.DegreeId,
          DegreeName = entity.DegreeName,
          IsDegreeArea = entity.IsDegreeArea,
          DegreeEducationLevelName = entity.DegreeEducationLevelName,
          IsClientData = entity.IsClientData,
          ClientDataTag = entity.ClientDataTag,
          ExcludeFromReport = entity.ExcludeFromReport,
          ExcludeFromJob = entity.ExcludeFromJob,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobDegreeEducationLevelReportView CopyTo(this JobDegreeEducationLevelReportViewDto dto, JobDegreeEducationLevelReportView entity)
      {
        entity.StateAreaId = dto.StateAreaId;
        entity.JobDegreeEducationLevelId = dto.JobDegreeEducationLevelId;
        entity.HiringDemand = dto.HiringDemand;
        entity.DegreesAwarded = dto.DegreesAwarded;
        entity.JobId = dto.JobId;
        entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
        entity.EducationLevel = dto.EducationLevel;
        entity.DegreeId = dto.DegreeId;
        entity.DegreeName = dto.DegreeName;
        entity.IsDegreeArea = dto.IsDegreeArea;
        entity.DegreeEducationLevelName = dto.DegreeEducationLevelName;
        entity.IsClientData = dto.IsClientData;
        entity.ClientDataTag = dto.ClientDataTag;
        entity.ExcludeFromReport = dto.ExcludeFromReport;
        entity.ExcludeFromJob = dto.ExcludeFromJob;
        return entity;
      }

      public static JobDegreeEducationLevelReportView CopyTo(this JobDegreeEducationLevelReportViewDto dto)
      {
        var entity = new JobDegreeEducationLevelReportView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region DegreeEducationLevelExtDto Mappers

      public static DegreeEducationLevelExtDto AsDto(this DegreeEducationLevelExt entity)
      {
        var dto = new DegreeEducationLevelExtDto
        {
          Name = entity.Name,
          Url = entity.Url,
          Description = entity.Description,
          DegreeEducationLevelId = entity.DegreeEducationLevelId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static DegreeEducationLevelExt CopyTo(this DegreeEducationLevelExtDto dto, DegreeEducationLevelExt entity)
      {
        entity.Name = dto.Name;
        entity.Url = dto.Url;
        entity.Description = dto.Description;
        entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
        return entity;
      }

      public static DegreeEducationLevelExt CopyTo(this DegreeEducationLevelExtDto dto)
      {
        var entity = new DegreeEducationLevelExt();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region DegreeEducationLevelStateAreaViewDto Mappers

      public static DegreeEducationLevelStateAreaViewDto AsDto(this DegreeEducationLevelStateAreaView entity)
      {
        var dto = new DegreeEducationLevelStateAreaViewDto
        {
          DegreeId = entity.DegreeId,
          DegreeName = entity.DegreeName,
          IsDegreeArea = entity.IsDegreeArea,
          IsClientData = entity.IsClientData,
          StateAreaId = entity.StateAreaId,
          DegreeEducationLevelName = entity.DegreeEducationLevelName,
          ClientDataTag = entity.ClientDataTag,
          EducationLevel = entity.EducationLevel,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static DegreeEducationLevelStateAreaView CopyTo(this DegreeEducationLevelStateAreaViewDto dto, DegreeEducationLevelStateAreaView entity)
      {
        entity.DegreeId = dto.DegreeId;
        entity.DegreeName = dto.DegreeName;
        entity.IsDegreeArea = dto.IsDegreeArea;
        entity.IsClientData = dto.IsClientData;
        entity.StateAreaId = dto.StateAreaId;
        entity.DegreeEducationLevelName = dto.DegreeEducationLevelName;
        entity.ClientDataTag = dto.ClientDataTag;
        entity.EducationLevel = dto.EducationLevel;
        return entity;
      }

      public static DegreeEducationLevelStateAreaView CopyTo(this DegreeEducationLevelStateAreaViewDto dto)
      {
        var entity = new DegreeEducationLevelStateAreaView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobStateAreaViewDto Mappers

      public static JobStateAreaViewDto AsDto(this JobStateAreaView entity)
      {
        var dto = new JobStateAreaViewDto
        {
          Name = entity.Name,
          StateAreaId = entity.StateAreaId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobStateAreaView CopyTo(this JobStateAreaViewDto dto, JobStateAreaView entity)
      {
        entity.Name = dto.Name;
        entity.StateAreaId = dto.StateAreaId;
        return entity;
      }

      public static JobStateAreaView CopyTo(this JobStateAreaViewDto dto)
      {
        var entity = new JobStateAreaView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region DegreeAliasDto Mappers

      public static DegreeAliasDto AsDto(this DegreeAlias entity)
      {
        var dto = new DegreeAliasDto
        {
          AliasExtended = entity.AliasExtended,
          EducationLevel = entity.EducationLevel,
          Alias = entity.Alias,
          DegreeId = entity.DegreeId,
          DegreeEducationLevelId = entity.DegreeEducationLevelId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static DegreeAlias CopyTo(this DegreeAliasDto dto, DegreeAlias entity)
      {
        entity.AliasExtended = dto.AliasExtended;
        entity.EducationLevel = dto.EducationLevel;
        entity.Alias = dto.Alias;
        entity.DegreeId = dto.DegreeId;
        entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
        return entity;
      }

      public static DegreeAlias CopyTo(this DegreeAliasDto dto)
      {
        var entity = new DegreeAlias();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region DegreeAliasViewDto Mappers

      public static DegreeAliasViewDto AsDto(this DegreeAliasView entity)
      {
        var dto = new DegreeAliasViewDto
        {
          EducationLevel = entity.EducationLevel,
          DegreeId = entity.DegreeId,
          IsClientData = entity.IsClientData,
          ClientDataTag = entity.ClientDataTag,
          DegreeEducationLevelId = entity.DegreeEducationLevelId,
          AliasExtended = entity.AliasExtended,
          Alias = entity.Alias,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static DegreeAliasView CopyTo(this DegreeAliasViewDto dto, DegreeAliasView entity)
      {
        entity.EducationLevel = dto.EducationLevel;
        entity.DegreeId = dto.DegreeId;
        entity.IsClientData = dto.IsClientData;
        entity.ClientDataTag = dto.ClientDataTag;
        entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
        entity.AliasExtended = dto.AliasExtended;
        entity.Alias = dto.Alias;
        return entity;
      }

      public static DegreeAliasView CopyTo(this DegreeAliasViewDto dto)
      {
        var entity = new DegreeAliasView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobCareerPathToDto Mappers

      public static JobCareerPathToDto AsDto(this JobCareerPathTo entity)
      {
        var dto = new JobCareerPathToDto
        {
          Rank = entity.Rank,
          CurrentJobId = entity.CurrentJobId,
          Next1JobId = entity.Next1JobId,
          Next2JobId = entity.Next2JobId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobCareerPathTo CopyTo(this JobCareerPathToDto dto, JobCareerPathTo entity)
      {
        entity.Rank = dto.Rank;
        entity.CurrentJobId = dto.CurrentJobId;
        entity.Next1JobId = dto.Next1JobId;
        entity.Next2JobId = dto.Next2JobId;
        return entity;
      }

      public static JobCareerPathTo CopyTo(this JobCareerPathToDto dto)
      {
        var entity = new JobCareerPathTo();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobCareerPathFromDto Mappers

      public static JobCareerPathFromDto AsDto(this JobCareerPathFrom entity)
      {
        var dto = new JobCareerPathFromDto
        {
          Rank = entity.Rank,
          FromJobId = entity.FromJobId,
          ToJobId = entity.ToJobId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobCareerPathFrom CopyTo(this JobCareerPathFromDto dto, JobCareerPathFrom entity)
      {
        entity.Rank = dto.Rank;
        entity.FromJobId = dto.FromJobId;
        entity.ToJobId = dto.ToJobId;
        return entity;
      }

      public static JobCareerPathFrom CopyTo(this JobCareerPathFromDto dto)
      {
        var entity = new JobCareerPathFrom();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region StudyPlaceDegreeEducationLevelViewDto Mappers

      public static StudyPlaceDegreeEducationLevelViewDto AsDto(this StudyPlaceDegreeEducationLevelView entity)
      {
        var dto = new StudyPlaceDegreeEducationLevelViewDto
        {
          StudyPlaceId = entity.StudyPlaceId,
          StudyPlaceName = entity.StudyPlaceName,
          StateAreaId = entity.StateAreaId,
          DegreeEducationLevelId = entity.DegreeEducationLevelId,
          DegreeId = entity.DegreeId,
          DegreeName = entity.DegreeName,
          IsClientData = entity.IsClientData,
          ClientDataTag = entity.ClientDataTag,
          DegreeEducationLevelName = entity.DegreeEducationLevelName,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static StudyPlaceDegreeEducationLevelView CopyTo(this StudyPlaceDegreeEducationLevelViewDto dto, StudyPlaceDegreeEducationLevelView entity)
      {
        entity.StudyPlaceId = dto.StudyPlaceId;
        entity.StudyPlaceName = dto.StudyPlaceName;
        entity.StateAreaId = dto.StateAreaId;
        entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
        entity.DegreeId = dto.DegreeId;
        entity.DegreeName = dto.DegreeName;
        entity.IsClientData = dto.IsClientData;
        entity.ClientDataTag = dto.ClientDataTag;
        entity.DegreeEducationLevelName = dto.DegreeEducationLevelName;
        return entity;
      }

      public static StudyPlaceDegreeEducationLevelView CopyTo(this StudyPlaceDegreeEducationLevelViewDto dto)
      {
        var entity = new StudyPlaceDegreeEducationLevelView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobEmployerSkillDto Mappers

      public static JobEmployerSkillDto AsDto(this JobEmployerSkill entity)
      {
        var dto = new JobEmployerSkillDto
        {
          SkillCount = entity.SkillCount,
          Rank = entity.Rank,
          JobId = entity.JobId,
          EmployerId = entity.EmployerId,
          SkillId = entity.SkillId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobEmployerSkill CopyTo(this JobEmployerSkillDto dto, JobEmployerSkill entity)
      {
        entity.SkillCount = dto.SkillCount;
        entity.Rank = dto.Rank;
        entity.JobId = dto.JobId;
        entity.EmployerId = dto.EmployerId;
        entity.SkillId = dto.SkillId;
        return entity;
      }

      public static JobEmployerSkill CopyTo(this JobEmployerSkillDto dto)
      {
        var entity = new JobEmployerSkill();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobEmployerSkillViewDto Mappers

      public static JobEmployerSkillViewDto AsDto(this JobEmployerSkillView entity)
      {
        var dto = new JobEmployerSkillViewDto
        {
          JobId = entity.JobId,
          EmployerId = entity.EmployerId,
          SkillId = entity.SkillId,
          SkillType = entity.SkillType,
          SkillName = entity.SkillName,
          SkillCount = entity.SkillCount,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobEmployerSkillView CopyTo(this JobEmployerSkillViewDto dto, JobEmployerSkillView entity)
      {
        entity.JobId = dto.JobId;
        entity.EmployerId = dto.EmployerId;
        entity.SkillId = dto.SkillId;
        entity.SkillType = dto.SkillType;
        entity.SkillName = dto.SkillName;
        entity.SkillCount = dto.SkillCount;
        return entity;
      }

      public static JobEmployerSkillView CopyTo(this JobEmployerSkillViewDto dto)
      {
        var entity = new JobEmployerSkillView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region ProgramAreaDto Mappers

      public static ProgramAreaDto AsDto(this ProgramArea entity)
      {
        var dto = new ProgramAreaDto
        {
          Name = entity.Name,
          Description = entity.Description,
          IsClientData = entity.IsClientData,
          ClientDataTag = entity.ClientDataTag,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static ProgramArea CopyTo(this ProgramAreaDto dto, ProgramArea entity)
      {
        entity.Name = dto.Name;
        entity.Description = dto.Description;
        entity.IsClientData = dto.IsClientData;
        entity.ClientDataTag = dto.ClientDataTag;
        return entity;
      }

      public static ProgramArea CopyTo(this ProgramAreaDto dto)
      {
        var entity = new ProgramArea();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region ProgramAreaDegreeDto Mappers

      public static ProgramAreaDegreeDto AsDto(this ProgramAreaDegree entity)
      {
        var dto = new ProgramAreaDegreeDto
        {
          DegreeId = entity.DegreeId,
          ProgramAreaId = entity.ProgramAreaId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static ProgramAreaDegree CopyTo(this ProgramAreaDegreeDto dto, ProgramAreaDegree entity)
      {
        entity.DegreeId = dto.DegreeId;
        entity.ProgramAreaId = dto.ProgramAreaId;
        return entity;
      }

      public static ProgramAreaDegree CopyTo(this ProgramAreaDegreeDto dto)
      {
        var entity = new ProgramAreaDegree();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region ProgramAreaDegreeViewDto Mappers

      public static ProgramAreaDegreeViewDto AsDto(this ProgramAreaDegreeView entity)
      {
        var dto = new ProgramAreaDegreeViewDto
        {
          ProgramAreaId = entity.ProgramAreaId,
          ProgramAreaName = entity.ProgramAreaName,
          DegreeId = entity.DegreeId,
          DegreeName = entity.DegreeName,
          IsClientData = entity.IsClientData,
          ClientDataTag = entity.ClientDataTag,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static ProgramAreaDegreeView CopyTo(this ProgramAreaDegreeViewDto dto, ProgramAreaDegreeView entity)
      {
        entity.ProgramAreaId = dto.ProgramAreaId;
        entity.ProgramAreaName = dto.ProgramAreaName;
        entity.DegreeId = dto.DegreeId;
        entity.DegreeName = dto.DegreeName;
        entity.IsClientData = dto.IsClientData;
        entity.ClientDataTag = dto.ClientDataTag;
        return entity;
      }

      public static ProgramAreaDegreeView CopyTo(this ProgramAreaDegreeViewDto dto)
      {
        var entity = new ProgramAreaDegreeView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region ProgramAreaDegreeEducationLevelViewDto Mappers

      public static ProgramAreaDegreeEducationLevelViewDto AsDto(this ProgramAreaDegreeEducationLevelView entity)
      {
        var dto = new ProgramAreaDegreeEducationLevelViewDto
        {
          ProgramAreaId = entity.ProgramAreaId,
          ProgramAreaName = entity.ProgramAreaName,
          DegreeId = entity.DegreeId,
          DegreeName = entity.DegreeName,
          DegreeEducationLevelId = entity.DegreeEducationLevelId,
          EducationLevel = entity.EducationLevel,
          DegreeEducationLevelName = entity.DegreeEducationLevelName,
          IsClientData = entity.IsClientData,
          ClientDataTag = entity.ClientDataTag,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static ProgramAreaDegreeEducationLevelView CopyTo(this ProgramAreaDegreeEducationLevelViewDto dto, ProgramAreaDegreeEducationLevelView entity)
      {
        entity.ProgramAreaId = dto.ProgramAreaId;
        entity.ProgramAreaName = dto.ProgramAreaName;
        entity.DegreeId = dto.DegreeId;
        entity.DegreeName = dto.DegreeName;
        entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
        entity.EducationLevel = dto.EducationLevel;
        entity.DegreeEducationLevelName = dto.DegreeEducationLevelName;
        entity.IsClientData = dto.IsClientData;
        entity.ClientDataTag = dto.ClientDataTag;
        return entity;
      }

      public static ProgramAreaDegreeEducationLevelView CopyTo(this ProgramAreaDegreeEducationLevelViewDto dto)
      {
        var entity = new ProgramAreaDegreeEducationLevelView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region OnetDto Mappers

      public static OnetDto AsDto(this Onet entity)
      {
        var dto = new OnetDto
        {
          OnetCode = entity.OnetCode,
          Key = entity.Key,
          JobFamilyId = entity.JobFamilyId,
          JobZone = entity.JobZone,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static Onet CopyTo(this OnetDto dto, Onet entity)
      {
        entity.OnetCode = dto.OnetCode;
        entity.Key = dto.Key;
        entity.JobFamilyId = dto.JobFamilyId;
        entity.JobZone = dto.JobZone;
        return entity;
      }

      public static Onet CopyTo(this OnetDto dto)
      {
        var entity = new Onet();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region OnetTaskDto Mappers

      public static OnetTaskDto AsDto(this OnetTask entity)
      {
        var dto = new OnetTaskDto
        {
          Key = entity.Key,
          OnetId = entity.OnetId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static OnetTask CopyTo(this OnetTaskDto dto, OnetTask entity)
      {
        entity.Key = dto.Key;
        entity.OnetId = dto.OnetId;
        return entity;
      }

      public static OnetTask CopyTo(this OnetTaskDto dto)
      {
        var entity = new OnetTask();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobTaskDto Mappers

      public static JobTaskDto AsDto(this JobTask entity)
      {
        var dto = new JobTaskDto
        {
          JobTaskType = entity.JobTaskType,
          Key = entity.Key,
          Scope = entity.Scope,
          Certificate = entity.Certificate,
          OnetId = entity.OnetId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobTask CopyTo(this JobTaskDto dto, JobTask entity)
      {
        entity.JobTaskType = dto.JobTaskType;
        entity.Key = dto.Key;
        entity.Scope = dto.Scope;
        entity.Certificate = dto.Certificate;
        entity.OnetId = dto.OnetId;
        return entity;
      }

      public static JobTask CopyTo(this JobTaskDto dto)
      {
        var entity = new JobTask();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobTaskMultiOptionDto Mappers

      public static JobTaskMultiOptionDto AsDto(this JobTaskMultiOption entity)
      {
        var dto = new JobTaskMultiOptionDto
        {
          Key = entity.Key,
          JobTaskId = entity.JobTaskId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobTaskMultiOption CopyTo(this JobTaskMultiOptionDto dto, JobTaskMultiOption entity)
      {
        entity.Key = dto.Key;
        entity.JobTaskId = dto.JobTaskId;
        return entity;
      }

      public static JobTaskMultiOption CopyTo(this JobTaskMultiOptionDto dto)
      {
        var entity = new JobTaskMultiOption();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region OnetLocalisationItemDto Mappers

      public static OnetLocalisationItemDto AsDto(this OnetLocalisationItem entity)
      {
        var dto = new OnetLocalisationItemDto
        {
          Key = entity.Key,
          PrimaryValue = entity.PrimaryValue,
          SecondaryValue = entity.SecondaryValue,
          LocalisationId = entity.LocalisationId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static OnetLocalisationItem CopyTo(this OnetLocalisationItemDto dto, OnetLocalisationItem entity)
      {
        entity.Key = dto.Key;
        entity.PrimaryValue = dto.PrimaryValue;
        entity.SecondaryValue = dto.SecondaryValue;
        entity.LocalisationId = dto.LocalisationId;
        return entity;
      }

      public static OnetLocalisationItem CopyTo(this OnetLocalisationItemDto dto)
      {
        var entity = new OnetLocalisationItem();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobTaskLocalisationItemDto Mappers

      public static JobTaskLocalisationItemDto AsDto(this JobTaskLocalisationItem entity)
      {
        var dto = new JobTaskLocalisationItemDto
        {
          Key = entity.Key,
          PrimaryValue = entity.PrimaryValue,
          SecondaryValue = entity.SecondaryValue,
          LocalisationId = entity.LocalisationId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobTaskLocalisationItem CopyTo(this JobTaskLocalisationItemDto dto, JobTaskLocalisationItem entity)
      {
        entity.Key = dto.Key;
        entity.PrimaryValue = dto.PrimaryValue;
        entity.SecondaryValue = dto.SecondaryValue;
        entity.LocalisationId = dto.LocalisationId;
        return entity;
      }

      public static JobTaskLocalisationItem CopyTo(this JobTaskLocalisationItemDto dto)
      {
        var entity = new JobTaskLocalisationItem();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region OnetWordDto Mappers

      public static OnetWordDto AsDto(this OnetWord entity)
      {
        var dto = new OnetWordDto
        {
          Key = entity.Key,
          Word = entity.Word,
          Stem = entity.Stem,
          Frequency = entity.Frequency,
          OnetSoc = entity.OnetSoc,
          OnetId = entity.OnetId,
          OnetRingId = entity.OnetRingId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static OnetWord CopyTo(this OnetWordDto dto, OnetWord entity)
      {
        entity.Key = dto.Key;
        entity.Word = dto.Word;
        entity.Stem = dto.Stem;
        entity.Frequency = dto.Frequency;
        entity.OnetSoc = dto.OnetSoc;
        entity.OnetId = dto.OnetId;
        entity.OnetRingId = dto.OnetRingId;
        return entity;
      }

      public static OnetWord CopyTo(this OnetWordDto dto)
      {
        var entity = new OnetWord();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region OnetRingDto Mappers

      public static OnetRingDto AsDto(this OnetRing entity)
      {
        var dto = new OnetRingDto
        {
          Name = entity.Name,
          Weighting = entity.Weighting,
          RingMax = entity.RingMax,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static OnetRing CopyTo(this OnetRingDto dto, OnetRing entity)
      {
        entity.Name = dto.Name;
        entity.Weighting = dto.Weighting;
        entity.RingMax = dto.RingMax;
        return entity;
      }

      public static OnetRing CopyTo(this OnetRingDto dto)
      {
        var entity = new OnetRing();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region OnetPhraseDto Mappers

      public static OnetPhraseDto AsDto(this OnetPhrase entity)
      {
        var dto = new OnetPhraseDto
        {
          Phrase = entity.Phrase,
          Equivalent = entity.Equivalent,
          OnetSoc = entity.OnetSoc,
          OnetId = entity.OnetId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static OnetPhrase CopyTo(this OnetPhraseDto dto, OnetPhrase entity)
      {
        entity.Phrase = dto.Phrase;
        entity.Equivalent = dto.Equivalent;
        entity.OnetSoc = dto.OnetSoc;
        entity.OnetId = dto.OnetId;
        return entity;
      }

      public static OnetPhrase CopyTo(this OnetPhraseDto dto)
      {
        var entity = new OnetPhrase();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobTaskViewDto Mappers

      public static JobTaskViewDto AsDto(this JobTaskView entity)
      {
        var dto = new JobTaskViewDto
        {
          JobTaskType = entity.JobTaskType,
          OnetId = entity.OnetId,
          Culture = entity.Culture,
          Prompt = entity.Prompt,
          Response = entity.Response,
          Scope = entity.Scope,
          Certificate = entity.Certificate,
          OnetCode = entity.OnetCode,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobTaskView CopyTo(this JobTaskViewDto dto, JobTaskView entity)
      {
        entity.JobTaskType = dto.JobTaskType;
        entity.OnetId = dto.OnetId;
        entity.Culture = dto.Culture;
        entity.Prompt = dto.Prompt;
        entity.Response = dto.Response;
        entity.Scope = dto.Scope;
        entity.Certificate = dto.Certificate;
        entity.OnetCode = dto.OnetCode;
        return entity;
      }

      public static JobTaskView CopyTo(this JobTaskViewDto dto)
      {
        var entity = new JobTaskView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobTaskMultiOptionViewDto Mappers

      public static JobTaskMultiOptionViewDto AsDto(this JobTaskMultiOptionView entity)
      {
        var dto = new JobTaskMultiOptionViewDto
        {
          OnetId = entity.OnetId,
          Culture = entity.Culture,
          Prompt = entity.Prompt,
          JobTaskId = entity.JobTaskId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobTaskMultiOptionView CopyTo(this JobTaskMultiOptionViewDto dto, JobTaskMultiOptionView entity)
      {
        entity.OnetId = dto.OnetId;
        entity.Culture = dto.Culture;
        entity.Prompt = dto.Prompt;
        entity.JobTaskId = dto.JobTaskId;
        return entity;
      }

      public static JobTaskMultiOptionView CopyTo(this JobTaskMultiOptionViewDto dto)
      {
        var entity = new JobTaskMultiOptionView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region OnetViewDto Mappers

      public static OnetViewDto AsDto(this OnetView entity)
      {
        var dto = new OnetViewDto
        {
          Key = entity.Key,
          Occupation = entity.Occupation,
          JobFamilyId = entity.JobFamilyId,
          OnetCode = entity.OnetCode,
          Culture = entity.Culture,
          Description = entity.Description,
          JobTasksAvailable = entity.JobTasksAvailable,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static OnetView CopyTo(this OnetViewDto dto, OnetView entity)
      {
        entity.Key = dto.Key;
        entity.Occupation = dto.Occupation;
        entity.JobFamilyId = dto.JobFamilyId;
        entity.OnetCode = dto.OnetCode;
        entity.Culture = dto.Culture;
        entity.Description = dto.Description;
        entity.JobTasksAvailable = dto.JobTasksAvailable;
        return entity;
      }

      public static OnetView CopyTo(this OnetViewDto dto)
      {
        var entity = new OnetView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region NAICSDto Mappers

      public static NAICSDto AsDto(this NAICS entity)
      {
        var dto = new NAICSDto
        {
          Code = entity.Code,
          Name = entity.Name,
          ParentId = entity.ParentId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static NAICS CopyTo(this NAICSDto dto, NAICS entity)
      {
        entity.Code = dto.Code;
        entity.Name = dto.Name;
        entity.ParentId = dto.ParentId;
        return entity;
      }

      public static NAICS CopyTo(this NAICSDto dto)
      {
        var entity = new NAICS();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region OnetTaskViewDto Mappers

      public static OnetTaskViewDto AsDto(this OnetTaskView entity)
      {
        var dto = new OnetTaskViewDto
        {
          OnetId = entity.OnetId,
          TaskKey = entity.TaskKey,
          Task = entity.Task,
          Culture = entity.Culture,
          TaskPastTense = entity.TaskPastTense,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static OnetTaskView CopyTo(this OnetTaskViewDto dto, OnetTaskView entity)
      {
        entity.OnetId = dto.OnetId;
        entity.TaskKey = dto.TaskKey;
        entity.Task = dto.Task;
        entity.Culture = dto.Culture;
        entity.TaskPastTense = dto.TaskPastTense;
        return entity;
      }

      public static OnetTaskView CopyTo(this OnetTaskViewDto dto)
      {
        var entity = new OnetTaskView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region PostalCodeDto Mappers

      public static PostalCodeDto AsDto(this PostalCode entity)
      {
        var dto = new PostalCodeDto
        {
          Code = entity.Code,
          Longitude = entity.Longitude,
          Latitude = entity.Latitude,
          CountryKey = entity.CountryKey,
          StateKey = entity.StateKey,
          CountyKey = entity.CountyKey,
          CityName = entity.CityName,
          OfficeKey = entity.OfficeKey,
          WibLocationKey = entity.WibLocationKey,
          MsaId = entity.MsaId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static PostalCode CopyTo(this PostalCodeDto dto, PostalCode entity)
      {
        entity.Code = dto.Code;
        entity.Longitude = dto.Longitude;
        entity.Latitude = dto.Latitude;
        entity.CountryKey = dto.CountryKey;
        entity.StateKey = dto.StateKey;
        entity.CountyKey = dto.CountyKey;
        entity.CityName = dto.CityName;
        entity.OfficeKey = dto.OfficeKey;
        entity.WibLocationKey = dto.WibLocationKey;
        entity.MsaId = dto.MsaId;
        return entity;
      }

      public static PostalCode CopyTo(this PostalCodeDto dto)
      {
        var entity = new PostalCode();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region PostalCodeViewDto Mappers

      public static PostalCodeViewDto AsDto(this PostalCodeView entity)
      {
        var dto = new PostalCodeViewDto
        {
          Code = entity.Code,
          Longitude = entity.Longitude,
          Latitude = entity.Latitude,
          CountryKey = entity.CountryKey,
          CountryName = entity.CountryName,
          StateKey = entity.StateKey,
          StateName = entity.StateName,
          Culture = entity.Culture,
          CountyKey = entity.CountyKey,
          CountyName = entity.CountyName,
          CityName = entity.CityName,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static PostalCodeView CopyTo(this PostalCodeViewDto dto, PostalCodeView entity)
      {
        entity.Code = dto.Code;
        entity.Longitude = dto.Longitude;
        entity.Latitude = dto.Latitude;
        entity.CountryKey = dto.CountryKey;
        entity.CountryName = dto.CountryName;
        entity.StateKey = dto.StateKey;
        entity.StateName = dto.StateName;
        entity.Culture = dto.Culture;
        entity.CountyKey = dto.CountyKey;
        entity.CountyName = dto.CountyName;
        entity.CityName = dto.CityName;
        return entity;
      }

      public static PostalCodeView CopyTo(this PostalCodeViewDto dto)
      {
        var entity = new PostalCodeView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region OnetWordJobTaskViewDto Mappers

      public static OnetWordJobTaskViewDto AsDto(this OnetWordJobTaskView entity)
      {
        var dto = new OnetWordJobTaskViewDto
        {
          OnetId = entity.OnetId,
          OnetRingId = entity.OnetRingId,
          OnetSoc = entity.OnetSoc,
          Stem = entity.Stem,
          Word = entity.Word,
          JobFamilyId = entity.JobFamilyId,
          JobZone = entity.JobZone,
          OnetKey = entity.OnetKey,
          OnetCode = entity.OnetCode,
          Frequency = entity.Frequency,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static OnetWordJobTaskView CopyTo(this OnetWordJobTaskViewDto dto, OnetWordJobTaskView entity)
      {
        entity.OnetId = dto.OnetId;
        entity.OnetRingId = dto.OnetRingId;
        entity.OnetSoc = dto.OnetSoc;
        entity.Stem = dto.Stem;
        entity.Word = dto.Word;
        entity.JobFamilyId = dto.JobFamilyId;
        entity.JobZone = dto.JobZone;
        entity.OnetKey = dto.OnetKey;
        entity.OnetCode = dto.OnetCode;
        entity.Frequency = dto.Frequency;
        return entity;
      }

      public static OnetWordJobTaskView CopyTo(this OnetWordJobTaskViewDto dto)
      {
        var entity = new OnetWordJobTaskView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region OnetWordViewDto Mappers

      public static OnetWordViewDto AsDto(this OnetWordView entity)
      {
        var dto = new OnetWordViewDto
        {
          OnetId = entity.OnetId,
          OnetRingId = entity.OnetRingId,
          OnetSoc = entity.OnetSoc,
          Stem = entity.Stem,
          Word = entity.Word,
          JobFamilyId = entity.JobFamilyId,
          JobZone = entity.JobZone,
          OnetKey = entity.OnetKey,
          OnetCode = entity.OnetCode,
          Frequency = entity.Frequency,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static OnetWordView CopyTo(this OnetWordViewDto dto, OnetWordView entity)
      {
        entity.OnetId = dto.OnetId;
        entity.OnetRingId = dto.OnetRingId;
        entity.OnetSoc = dto.OnetSoc;
        entity.Stem = dto.Stem;
        entity.Word = dto.Word;
        entity.JobFamilyId = dto.JobFamilyId;
        entity.JobZone = dto.JobZone;
        entity.OnetKey = dto.OnetKey;
        entity.OnetCode = dto.OnetCode;
        entity.Frequency = dto.Frequency;
        return entity;
      }

      public static OnetWordView CopyTo(this OnetWordViewDto dto)
      {
        var entity = new OnetWordView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region OnetCommodityDto Mappers

      public static OnetCommodityDto AsDto(this OnetCommodity entity)
      {
        var dto = new OnetCommodityDto
        {
          ToolTechnologyType = entity.ToolTechnologyType,
          ToolTechnologyExample = entity.ToolTechnologyExample,
          Code = entity.Code,
          Title = entity.Title,
          OnetId = entity.OnetId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static OnetCommodity CopyTo(this OnetCommodityDto dto, OnetCommodity entity)
      {
        entity.ToolTechnologyType = dto.ToolTechnologyType;
        entity.ToolTechnologyExample = dto.ToolTechnologyExample;
        entity.Code = dto.Code;
        entity.Title = dto.Title;
        entity.OnetId = dto.OnetId;
        return entity;
      }

      public static OnetCommodity CopyTo(this OnetCommodityDto dto)
      {
        var entity = new OnetCommodity();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region StandardIndustrialClassificationDto Mappers

      public static StandardIndustrialClassificationDto AsDto(this StandardIndustrialClassification entity)
      {
        var dto = new StandardIndustrialClassificationDto
        {
          Name = entity.Name,
          SicPrimary = entity.SicPrimary,
          SicSecondary = entity.SicSecondary,
          NaicsPrimary = entity.NaicsPrimary,
          NaicsSecondary = entity.NaicsSecondary,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static StandardIndustrialClassification CopyTo(this StandardIndustrialClassificationDto dto, StandardIndustrialClassification entity)
      {
        entity.Name = dto.Name;
        entity.SicPrimary = dto.SicPrimary;
        entity.SicSecondary = dto.SicSecondary;
        entity.NaicsPrimary = dto.NaicsPrimary;
        entity.NaicsSecondary = dto.NaicsSecondary;
        return entity;
      }

      public static StandardIndustrialClassification CopyTo(this StandardIndustrialClassificationDto dto)
      {
        var entity = new StandardIndustrialClassification();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region ROnetDto Mappers

      public static ROnetDto AsDto(this ROnet entity)
      {
        var dto = new ROnetDto
        {
          Code = entity.Code,
          Key = entity.Key,
          OnetId = entity.OnetId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static ROnet CopyTo(this ROnetDto dto, ROnet entity)
      {
        entity.Code = dto.Code;
        entity.Key = dto.Key;
        entity.OnetId = dto.OnetId;
        return entity;
      }

      public static ROnet CopyTo(this ROnetDto dto)
      {
        var entity = new ROnet();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region OnetROnetDto Mappers

      public static OnetROnetDto AsDto(this OnetROnet entity)
      {
        var dto = new OnetROnetDto
        {
          BestFit = entity.BestFit,
          OnetId = entity.OnetId,
          ROnetId = entity.ROnetId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static OnetROnet CopyTo(this OnetROnetDto dto, OnetROnet entity)
      {
        entity.BestFit = dto.BestFit;
        entity.OnetId = dto.OnetId;
        entity.ROnetId = dto.ROnetId;
        return entity;
      }

      public static OnetROnet CopyTo(this OnetROnetDto dto)
      {
        var entity = new OnetROnet();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region MilitaryOccupationDto Mappers

      public static MilitaryOccupationDto AsDto(this MilitaryOccupation entity)
      {
        var dto = new MilitaryOccupationDto
        {
          Code = entity.Code,
          Key = entity.Key,
          BranchOfServiceId = entity.BranchOfServiceId,
          IsCommissioned = entity.IsCommissioned,
          MilitaryOccupationGroupId = entity.MilitaryOccupationGroupId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static MilitaryOccupation CopyTo(this MilitaryOccupationDto dto, MilitaryOccupation entity)
      {
        entity.Code = dto.Code;
        entity.Key = dto.Key;
        entity.BranchOfServiceId = dto.BranchOfServiceId;
        entity.IsCommissioned = dto.IsCommissioned;
        entity.MilitaryOccupationGroupId = dto.MilitaryOccupationGroupId;
        return entity;
      }

      public static MilitaryOccupation CopyTo(this MilitaryOccupationDto dto)
      {
        var entity = new MilitaryOccupation();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region MilitaryOccupationGroupDto Mappers

      public static MilitaryOccupationGroupDto AsDto(this MilitaryOccupationGroup entity)
      {
        var dto = new MilitaryOccupationGroupDto
        {
          Name = entity.Name,
          IsCommissioned = entity.IsCommissioned,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static MilitaryOccupationGroup CopyTo(this MilitaryOccupationGroupDto dto, MilitaryOccupationGroup entity)
      {
        entity.Name = dto.Name;
        entity.IsCommissioned = dto.IsCommissioned;
        return entity;
      }

      public static MilitaryOccupationGroup CopyTo(this MilitaryOccupationGroupDto dto)
      {
        var entity = new MilitaryOccupationGroup();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region MilitaryOccupationJobTitleViewDto Mappers

      public static MilitaryOccupationJobTitleViewDto AsDto(this MilitaryOccupationJobTitleView entity)
      {
        var dto = new MilitaryOccupationJobTitleViewDto
        {
          MilitaryOccupationId = entity.MilitaryOccupationId,
          JobTitle = entity.JobTitle,
          BranchOfServiceId = entity.BranchOfServiceId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static MilitaryOccupationJobTitleView CopyTo(this MilitaryOccupationJobTitleViewDto dto, MilitaryOccupationJobTitleView entity)
      {
        entity.MilitaryOccupationId = dto.MilitaryOccupationId;
        entity.JobTitle = dto.JobTitle;
        entity.BranchOfServiceId = dto.BranchOfServiceId;
        return entity;
      }

      public static MilitaryOccupationJobTitleView CopyTo(this MilitaryOccupationJobTitleViewDto dto)
      {
        var entity = new MilitaryOccupationJobTitleView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region OnetSkillsDto Mappers

      public static OnetSkillsDto AsDto(this OnetSkills entity)
      {
        var dto = new OnetSkillsDto
        {
          Skill = entity.Skill,
          SkillType = entity.SkillType,
          OnetId = entity.OnetId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static OnetSkills CopyTo(this OnetSkillsDto dto, OnetSkills entity)
      {
        entity.Skill = dto.Skill;
        entity.SkillType = dto.SkillType;
        entity.OnetId = dto.OnetId;
        return entity;
      }

      public static OnetSkills CopyTo(this OnetSkillsDto dto)
      {
        var entity = new OnetSkills();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region MilitaryOccupationGroupROnetDto Mappers

      public static MilitaryOccupationGroupROnetDto AsDto(this MilitaryOccupationGroupROnet entity)
      {
        var dto = new MilitaryOccupationGroupROnetDto
        {
          ROnetId = entity.ROnetId,
          MilitaryOccupationGroupId = entity.MilitaryOccupationGroupId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static MilitaryOccupationGroupROnet CopyTo(this MilitaryOccupationGroupROnetDto dto, MilitaryOccupationGroupROnet entity)
      {
        entity.ROnetId = dto.ROnetId;
        entity.MilitaryOccupationGroupId = dto.MilitaryOccupationGroupId;
        return entity;
      }

      public static MilitaryOccupationGroupROnet CopyTo(this MilitaryOccupationGroupROnetDto dto)
      {
        var entity = new MilitaryOccupationGroupROnet();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region MilitaryOccupationROnetDto Mappers

      public static MilitaryOccupationROnetDto AsDto(this MilitaryOccupationROnet entity)
      {
        var dto = new MilitaryOccupationROnetDto
        {
          ROnetId = entity.ROnetId,
          MilitaryOccupationId = entity.MilitaryOccupationId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static MilitaryOccupationROnet CopyTo(this MilitaryOccupationROnetDto dto, MilitaryOccupationROnet entity)
      {
        entity.ROnetId = dto.ROnetId;
        entity.MilitaryOccupationId = dto.MilitaryOccupationId;
        return entity;
      }

      public static MilitaryOccupationROnet CopyTo(this MilitaryOccupationROnetDto dto)
      {
        var entity = new MilitaryOccupationROnet();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region LocalisationItemDto Mappers

      public static LocalisationItemDto AsDto(this LocalisationItem entity)
      {
        var dto = new LocalisationItemDto
        {
          Key = entity.Key,
          Value = entity.Value,
          LocalisationId = entity.LocalisationId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static LocalisationItem CopyTo(this LocalisationItemDto dto, LocalisationItem entity)
      {
        entity.Key = dto.Key;
        entity.Value = dto.Value;
        entity.LocalisationId = dto.LocalisationId;
        return entity;
      }

      public static LocalisationItem CopyTo(this LocalisationItemDto dto)
      {
        var entity = new LocalisationItem();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region GenericJobTitleDto Mappers

      public static GenericJobTitleDto AsDto(this GenericJobTitle entity)
      {
        var dto = new GenericJobTitleDto
        {
          Value = entity.Value,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static GenericJobTitle CopyTo(this GenericJobTitleDto dto, GenericJobTitle entity)
      {
        entity.Value = dto.Value;
        return entity;
      }

      public static GenericJobTitle CopyTo(this GenericJobTitleDto dto)
      {
        var entity = new GenericJobTitle();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region ResumeSkillDto Mappers

      public static ResumeSkillDto AsDto(this ResumeSkill entity)
      {
        var dto = new ResumeSkillDto
        {
          Name = entity.Name,
          Type = entity.Type,
          Category = entity.Category,
          Stem = entity.Stem,
          NoiseSkill = entity.NoiseSkill,
          Cluster = entity.Cluster,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static ResumeSkill CopyTo(this ResumeSkillDto dto, ResumeSkill entity)
      {
        entity.Name = dto.Name;
        entity.Type = dto.Type;
        entity.Category = dto.Category;
        entity.Stem = dto.Stem;
        entity.NoiseSkill = dto.NoiseSkill;
        entity.Cluster = dto.Cluster;
        return entity;
      }

      public static ResumeSkill CopyTo(this ResumeSkillDto dto)
      {
        var entity = new ResumeSkill();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region EducationInternshipCategoryDto Mappers

      public static EducationInternshipCategoryDto AsDto(this EducationInternshipCategory entity)
      {
        var dto = new EducationInternshipCategoryDto
        {
          Name = entity.Name,
          LocalisationId = entity.LocalisationId,
          CategoryType = entity.CategoryType,
          PastTenseStatement = entity.PastTenseStatement,
          PresentTenseStatement = entity.PresentTenseStatement,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static EducationInternshipCategory CopyTo(this EducationInternshipCategoryDto dto, EducationInternshipCategory entity)
      {
        entity.Name = dto.Name;
        entity.LocalisationId = dto.LocalisationId;
        entity.CategoryType = dto.CategoryType;
        entity.PastTenseStatement = dto.PastTenseStatement;
        entity.PresentTenseStatement = dto.PresentTenseStatement;
        return entity;
      }

      public static EducationInternshipCategory CopyTo(this EducationInternshipCategoryDto dto)
      {
        var entity = new EducationInternshipCategory();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region EducationInternshipSkillDto Mappers

      public static EducationInternshipSkillDto AsDto(this EducationInternshipSkill entity)
      {
        var dto = new EducationInternshipSkillDto
        {
          Name = entity.Name,
          LocalisationId = entity.LocalisationId,
          EducationInternshipCategoryId = entity.EducationInternshipCategoryId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static EducationInternshipSkill CopyTo(this EducationInternshipSkillDto dto, EducationInternshipSkill entity)
      {
        entity.Name = dto.Name;
        entity.LocalisationId = dto.LocalisationId;
        entity.EducationInternshipCategoryId = dto.EducationInternshipCategoryId;
        return entity;
      }

      public static EducationInternshipSkill CopyTo(this EducationInternshipSkillDto dto)
      {
        var entity = new EducationInternshipSkill();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region EducationInternshipStatementDto Mappers

      public static EducationInternshipStatementDto AsDto(this EducationInternshipStatement entity)
      {
        var dto = new EducationInternshipStatementDto
        {
          PastTenseStatement = entity.PastTenseStatement,
          LocalisationId = entity.LocalisationId,
          PresentTenseStatement = entity.PresentTenseStatement,
          EducationInternshipSkillId = entity.EducationInternshipSkillId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static EducationInternshipStatement CopyTo(this EducationInternshipStatementDto dto, EducationInternshipStatement entity)
      {
        entity.PastTenseStatement = dto.PastTenseStatement;
        entity.LocalisationId = dto.LocalisationId;
        entity.PresentTenseStatement = dto.PresentTenseStatement;
        entity.EducationInternshipSkillId = dto.EducationInternshipSkillId;
        return entity;
      }

      public static EducationInternshipStatement CopyTo(this EducationInternshipStatementDto dto)
      {
        var entity = new EducationInternshipStatement();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region SkillsForJobsViewDto Mappers

      public static SkillsForJobsViewDto AsDto(this SkillsForJobsView entity)
      {
        var dto = new SkillsForJobsViewDto
        {
          Name = entity.Name,
          SkillType = entity.SkillType,
          Rank = entity.Rank,
          DemandPercentile = entity.DemandPercentile,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static SkillsForJobsView CopyTo(this SkillsForJobsViewDto dto, SkillsForJobsView entity)
      {
        entity.Name = dto.Name;
        entity.SkillType = dto.SkillType;
        entity.Rank = dto.Rank;
        entity.DemandPercentile = dto.DemandPercentile;
        return entity;
      }

      public static SkillsForJobsView CopyTo(this SkillsForJobsViewDto dto)
      {
        var entity = new SkillsForJobsView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region DegreeEducationLevelROnetViewDto Mappers

      public static DegreeEducationLevelROnetViewDto AsDto(this DegreeEducationLevelROnetView entity)
      {
        var dto = new DegreeEducationLevelROnetViewDto
        {
          DegreeEducationLevelId = entity.DegreeEducationLevelId,
          ROnet = entity.ROnet,
          ProgramAreaId = entity.ProgramAreaId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static DegreeEducationLevelROnetView CopyTo(this DegreeEducationLevelROnetViewDto dto, DegreeEducationLevelROnetView entity)
      {
        entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
        entity.ROnet = dto.ROnet;
        entity.ProgramAreaId = dto.ProgramAreaId;
        return entity;
      }

      public static DegreeEducationLevelROnetView CopyTo(this DegreeEducationLevelROnetViewDto dto)
      {
        var entity = new DegreeEducationLevelROnetView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region CareerAreaJobDegreeViewDto Mappers

      public static CareerAreaJobDegreeViewDto AsDto(this CareerAreaJobDegreeView entity)
      {
        var dto = new CareerAreaJobDegreeViewDto
        {
          CareerAreaId = entity.CareerAreaId,
          JobId = entity.JobId,
          DegreeEducationLevelId = entity.DegreeEducationLevelId,
          EducationLevel = entity.EducationLevel,
          DegreeId = entity.DegreeId,
          IsClientData = entity.IsClientData,
          ClientDataTag = entity.ClientDataTag,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static CareerAreaJobDegreeView CopyTo(this CareerAreaJobDegreeViewDto dto, CareerAreaJobDegreeView entity)
      {
        entity.CareerAreaId = dto.CareerAreaId;
        entity.JobId = dto.JobId;
        entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
        entity.EducationLevel = dto.EducationLevel;
        entity.DegreeId = dto.DegreeId;
        entity.IsClientData = dto.IsClientData;
        entity.ClientDataTag = dto.ClientDataTag;
        return entity;
      }

      public static CareerAreaJobDegreeView CopyTo(this CareerAreaJobDegreeViewDto dto)
      {
        var entity = new CareerAreaJobDegreeView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region DegreeEducationLevelLensMappingDto Mappers

      public static DegreeEducationLevelLensMappingDto AsDto(this DegreeEducationLevelLensMapping entity)
      {
        var dto = new DegreeEducationLevelLensMappingDto
        {
          LensId = entity.LensId,
          DegreeEducationLevelId = entity.DegreeEducationLevelId,
          RcipCode = entity.RcipCode,
          ProgramAreaId = entity.ProgramAreaId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static DegreeEducationLevelLensMapping CopyTo(this DegreeEducationLevelLensMappingDto dto, DegreeEducationLevelLensMapping entity)
      {
        entity.LensId = dto.LensId;
        entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
        entity.RcipCode = dto.RcipCode;
        entity.ProgramAreaId = dto.ProgramAreaId;
        return entity;
      }

      public static DegreeEducationLevelLensMapping CopyTo(this DegreeEducationLevelLensMappingDto dto)
      {
        var entity = new DegreeEducationLevelLensMapping();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region Onet17Onet12MappingDto Mappers

      public static Onet17Onet12MappingDto AsDto(this Onet17Onet12Mapping entity)
      {
        var dto = new Onet17Onet12MappingDto
        {
          Onet17Code = entity.Onet17Code,
          OnetId = entity.OnetId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static Onet17Onet12Mapping CopyTo(this Onet17Onet12MappingDto dto, Onet17Onet12Mapping entity)
      {
        entity.Onet17Code = dto.Onet17Code;
        entity.OnetId = dto.OnetId;
        return entity;
      }

      public static Onet17Onet12Mapping CopyTo(this Onet17Onet12MappingDto dto)
      {
        var entity = new Onet17Onet12Mapping();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region Onet17Onet12MappingViewDto Mappers

      public static Onet17Onet12MappingViewDto AsDto(this Onet17Onet12MappingView entity)
      {
        var dto = new Onet17Onet12MappingViewDto
        {
          Onet17Code = entity.Onet17Code,
          Onet12Code = entity.Onet12Code,
          OnetId = entity.OnetId,
          Occupation = entity.Occupation,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static Onet17Onet12MappingView CopyTo(this Onet17Onet12MappingViewDto dto, Onet17Onet12MappingView entity)
      {
        entity.Onet17Code = dto.Onet17Code;
        entity.Onet12Code = dto.Onet12Code;
        entity.OnetId = dto.OnetId;
        entity.Occupation = dto.Occupation;
        return entity;
      }

      public static Onet17Onet12MappingView CopyTo(this Onet17Onet12MappingViewDto dto)
      {
        var entity = new Onet17Onet12MappingView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region ROnetOnetDto Mappers

      public static ROnetOnetDto AsDto(this ROnetOnet entity)
      {
        var dto = new ROnetOnetDto
        {
          BestFit = entity.BestFit,
          OnetId = entity.OnetId,
          ROnetId = entity.ROnetId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static ROnetOnet CopyTo(this ROnetOnetDto dto, ROnetOnet entity)
      {
        entity.BestFit = dto.BestFit;
        entity.OnetId = dto.OnetId;
        entity.ROnetId = dto.ROnetId;
        return entity;
      }

      public static ROnetOnet CopyTo(this ROnetOnetDto dto)
      {
        var entity = new ROnetOnet();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region OnetToROnetConversionViewDto Mappers

      public static OnetToROnetConversionViewDto AsDto(this OnetToROnetConversionView entity)
      {
        var dto = new OnetToROnetConversionViewDto
        {
          OnetId = entity.OnetId,
          OnetCode = entity.OnetCode,
          OnetKey = entity.OnetKey,
          ROnetId = entity.ROnetId,
          ROnetCode = entity.ROnetCode,
          ROnetKey = entity.ROnetKey,
          BestFit = entity.BestFit,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static OnetToROnetConversionView CopyTo(this OnetToROnetConversionViewDto dto, OnetToROnetConversionView entity)
      {
        entity.OnetId = dto.OnetId;
        entity.OnetCode = dto.OnetCode;
        entity.OnetKey = dto.OnetKey;
        entity.ROnetId = dto.ROnetId;
        entity.ROnetCode = dto.ROnetCode;
        entity.ROnetKey = dto.ROnetKey;
        entity.BestFit = dto.BestFit;
        return entity;
      }

      public static OnetToROnetConversionView CopyTo(this OnetToROnetConversionViewDto dto)
      {
        var entity = new OnetToROnetConversionView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region CareerAreaJobReportViewDto Mappers

      public static CareerAreaJobReportViewDto AsDto(this CareerAreaJobReportView entity)
      {
        var dto = new CareerAreaJobReportViewDto
        {
          CareerAreaId = entity.CareerAreaId,
          StateAreaId = entity.StateAreaId,
          JobId = entity.JobId,
          HiringDemand = entity.HiringDemand,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static CareerAreaJobReportView CopyTo(this CareerAreaJobReportViewDto dto, CareerAreaJobReportView entity)
      {
        entity.CareerAreaId = dto.CareerAreaId;
        entity.StateAreaId = dto.StateAreaId;
        entity.JobId = dto.JobId;
        entity.HiringDemand = dto.HiringDemand;
        return entity;
      }

      public static CareerAreaJobReportView CopyTo(this CareerAreaJobReportViewDto dto)
      {
        var entity = new CareerAreaJobReportView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region SOCDto Mappers

      public static SOCDto AsDto(this SOC entity)
      {
        var dto = new SOCDto
        {
          Soc2010Code = entity.Soc2010Code,
          Key = entity.Key,
          Title = entity.Title,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static SOC CopyTo(this SOCDto dto, SOC entity)
      {
        entity.Soc2010Code = dto.Soc2010Code;
        entity.Key = dto.Key;
        entity.Title = dto.Title;
        return entity;
      }

      public static SOC CopyTo(this SOCDto dto)
      {
        var entity = new SOC();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region OnetSOCDto Mappers

      public static OnetSOCDto AsDto(this OnetSOC entity)
      {
        var dto = new OnetSOCDto
        {
          OnetId = entity.OnetId,
          SocId = entity.SocId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static OnetSOC CopyTo(this OnetSOCDto dto, OnetSOC entity)
      {
        entity.OnetId = dto.OnetId;
        entity.SocId = dto.SocId;
        return entity;
      }

      public static OnetSOC CopyTo(this OnetSOCDto dto)
      {
        var entity = new OnetSOC();
        return dto.CopyTo(entity);
      }	

      #endregion

    }
}
*/

#endregion
