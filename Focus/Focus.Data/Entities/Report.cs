using System;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Validation;
using Mindscape.LightSpeed.Querying;
using Mindscape.LightSpeed.Linq;

#region Entities

namespace Focus.Data.Report.Entities
{
  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.JobSeeker", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeeker : Entity<long>
  {
    #region Fields
  
    private long _focusPersonId;
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _firstName;
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _lastName;
    private System.Nullable<long> _countyId;
    [ValidateLength(0, 400)]
    private string _county;
    private System.Nullable<long> _stateId;
    [ValidateLength(0, 400)]
    private string _state;
    [ValidateLength(0, 40)]
    private string _postalCode;
    [ValidateLength(0, 400)]
    private string _office;
    private System.DateTime _accountCreationDate;
    private System.Nullable<decimal> _salary;
    private System.Nullable<Focus.Core.ReportSalaryFrequency> _salaryFrequency;
    private System.Nullable<Focus.Core.Models.Career.EmploymentStatus> _employmentStatus;
    private System.Nullable<Focus.Core.Models.Career.VeteranEra> _veteranType;
    private System.Nullable<Focus.Core.Models.Career.VeteranDisabilityStatus> _veteranDisability;
    private System.Nullable<Focus.Core.Models.Career.VeteranTransitionType> _veteranTransitionType;
    private System.Nullable<Focus.Core.ReportMilitaryDischargeType> _veteranMilitaryDischarge;
    private System.Nullable<Focus.Core.ReportMilitaryBranchOfService> _veteranBranchOfService;
    private System.Nullable<bool> _campaignVeteran;
    private System.Nullable<System.DateTime> _dateOfBirth;
    private System.Nullable<Focus.Core.Genders> _gender;
    [ValidateLength(0, 500)]
    private string _emailAddress;
    private System.Nullable<Focus.Core.Models.Career.DisabilityStatus> _disabilityStatus;
    private System.Nullable<Focus.Core.ReportDisabilityType> _disabilityType;
    private System.Nullable<Focus.Core.ReportEthnicHeritage> _ethnicHeritage;
    private System.Nullable<Focus.Core.ReportRace> _race;
    private System.Nullable<bool> _resumeSearchable;
    private int _resumeCount;
    private System.Nullable<bool> _willingToWorkOvertime;
    private System.Nullable<bool> _willingToRelocate;
    private System.Nullable<Focus.Core.EducationLevel> _minimumEducationLevel;
    private double _latitudeRadians;
    private double _longitudeRadians;
    private bool _hasPendingResume;
    private System.Nullable<Focus.Core.SchoolStatus> _enrollmentStatus;
    private System.Nullable<System.DateTime> _militaryStartDate;
    private System.Nullable<System.DateTime> _militaryEndDate;
    private System.Nullable<bool> _isHomelessVeteran;
    [Column(ConverterType=typeof(Framework.Lightspeed.EncryptedStringConverter))]
    private string _socialSecurityNumber;
    private string _middleInitial;
    private string _addressLine1;
    private System.Nullable<bool> _isCitizen;
    private System.Nullable<System.DateTime> _alienCardExpires;
    private string _alienCardId;
    private System.Nullable<bool> _veteranAttendedTapCourse;
    private System.Nullable<short> _age;
    [ValidateLength(0, 50)]
    private string _ncrcLevel;
    private bool _unsubscribed;
    [ValidateLength(0, 25)]
    private string _suffix;
    private bool _hasAttendedTap;
    private System.Nullable<System.DateTime> _attendedTapDate;
    private System.Nullable<int> _accountType;
    private string _veteranRank;
    private bool _enabled;
    private bool _blocked;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    private readonly System.DateTime _updatedOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the FocusPersonId entity attribute.</summary>
    public const string FocusPersonIdField = "FocusPersonId";
    /// <summary>Identifies the FirstName entity attribute.</summary>
    public const string FirstNameField = "FirstName";
    /// <summary>Identifies the LastName entity attribute.</summary>
    public const string LastNameField = "LastName";
    /// <summary>Identifies the CountyId entity attribute.</summary>
    public const string CountyIdField = "CountyId";
    /// <summary>Identifies the County entity attribute.</summary>
    public const string CountyField = "County";
    /// <summary>Identifies the StateId entity attribute.</summary>
    public const string StateIdField = "StateId";
    /// <summary>Identifies the State entity attribute.</summary>
    public const string StateField = "State";
    /// <summary>Identifies the PostalCode entity attribute.</summary>
    public const string PostalCodeField = "PostalCode";
    /// <summary>Identifies the Office entity attribute.</summary>
    public const string OfficeField = "Office";
    /// <summary>Identifies the AccountCreationDate entity attribute.</summary>
    public const string AccountCreationDateField = "AccountCreationDate";
    /// <summary>Identifies the Salary entity attribute.</summary>
    public const string SalaryField = "Salary";
    /// <summary>Identifies the SalaryFrequency entity attribute.</summary>
    public const string SalaryFrequencyField = "SalaryFrequency";
    /// <summary>Identifies the EmploymentStatus entity attribute.</summary>
    public const string EmploymentStatusField = "EmploymentStatus";
    /// <summary>Identifies the VeteranType entity attribute.</summary>
    public const string VeteranTypeField = "VeteranType";
    /// <summary>Identifies the VeteranDisability entity attribute.</summary>
    public const string VeteranDisabilityField = "VeteranDisability";
    /// <summary>Identifies the VeteranTransitionType entity attribute.</summary>
    public const string VeteranTransitionTypeField = "VeteranTransitionType";
    /// <summary>Identifies the VeteranMilitaryDischarge entity attribute.</summary>
    public const string VeteranMilitaryDischargeField = "VeteranMilitaryDischarge";
    /// <summary>Identifies the VeteranBranchOfService entity attribute.</summary>
    public const string VeteranBranchOfServiceField = "VeteranBranchOfService";
    /// <summary>Identifies the CampaignVeteran entity attribute.</summary>
    public const string CampaignVeteranField = "CampaignVeteran";
    /// <summary>Identifies the DateOfBirth entity attribute.</summary>
    public const string DateOfBirthField = "DateOfBirth";
    /// <summary>Identifies the Gender entity attribute.</summary>
    public const string GenderField = "Gender";
    /// <summary>Identifies the EmailAddress entity attribute.</summary>
    public const string EmailAddressField = "EmailAddress";
    /// <summary>Identifies the DisabilityStatus entity attribute.</summary>
    public const string DisabilityStatusField = "DisabilityStatus";
    /// <summary>Identifies the DisabilityType entity attribute.</summary>
    public const string DisabilityTypeField = "DisabilityType";
    /// <summary>Identifies the EthnicHeritage entity attribute.</summary>
    public const string EthnicHeritageField = "EthnicHeritage";
    /// <summary>Identifies the Race entity attribute.</summary>
    public const string RaceField = "Race";
    /// <summary>Identifies the ResumeSearchable entity attribute.</summary>
    public const string ResumeSearchableField = "ResumeSearchable";
    /// <summary>Identifies the ResumeCount entity attribute.</summary>
    public const string ResumeCountField = "ResumeCount";
    /// <summary>Identifies the WillingToWorkOvertime entity attribute.</summary>
    public const string WillingToWorkOvertimeField = "WillingToWorkOvertime";
    /// <summary>Identifies the WillingToRelocate entity attribute.</summary>
    public const string WillingToRelocateField = "WillingToRelocate";
    /// <summary>Identifies the MinimumEducationLevel entity attribute.</summary>
    public const string MinimumEducationLevelField = "MinimumEducationLevel";
    /// <summary>Identifies the LatitudeRadians entity attribute.</summary>
    public const string LatitudeRadiansField = "LatitudeRadians";
    /// <summary>Identifies the LongitudeRadians entity attribute.</summary>
    public const string LongitudeRadiansField = "LongitudeRadians";
    /// <summary>Identifies the HasPendingResume entity attribute.</summary>
    public const string HasPendingResumeField = "HasPendingResume";
    /// <summary>Identifies the EnrollmentStatus entity attribute.</summary>
    public const string EnrollmentStatusField = "EnrollmentStatus";
    /// <summary>Identifies the MilitaryStartDate entity attribute.</summary>
    public const string MilitaryStartDateField = "MilitaryStartDate";
    /// <summary>Identifies the MilitaryEndDate entity attribute.</summary>
    public const string MilitaryEndDateField = "MilitaryEndDate";
    /// <summary>Identifies the IsHomelessVeteran entity attribute.</summary>
    public const string IsHomelessVeteranField = "IsHomelessVeteran";
    /// <summary>Identifies the SocialSecurityNumber entity attribute.</summary>
    public const string SocialSecurityNumberField = "SocialSecurityNumber";
    /// <summary>Identifies the MiddleInitial entity attribute.</summary>
    public const string MiddleInitialField = "MiddleInitial";
    /// <summary>Identifies the AddressLine1 entity attribute.</summary>
    public const string AddressLine1Field = "AddressLine1";
    /// <summary>Identifies the IsCitizen entity attribute.</summary>
    public const string IsCitizenField = "IsCitizen";
    /// <summary>Identifies the AlienCardExpires entity attribute.</summary>
    public const string AlienCardExpiresField = "AlienCardExpires";
    /// <summary>Identifies the AlienCardId entity attribute.</summary>
    public const string AlienCardIdField = "AlienCardId";
    /// <summary>Identifies the VeteranAttendedTapCourse entity attribute.</summary>
    public const string VeteranAttendedTapCourseField = "VeteranAttendedTapCourse";
    /// <summary>Identifies the Age entity attribute.</summary>
    public const string AgeField = "Age";
    /// <summary>Identifies the NcrcLevel entity attribute.</summary>
    public const string NcrcLevelField = "NcrcLevel";
    /// <summary>Identifies the Unsubscribed entity attribute.</summary>
    public const string UnsubscribedField = "Unsubscribed";
    /// <summary>Identifies the Suffix entity attribute.</summary>
    public const string SuffixField = "Suffix";
    /// <summary>Identifies the HasAttendedTap entity attribute.</summary>
    public const string HasAttendedTapField = "HasAttendedTap";
    /// <summary>Identifies the AttendedTapDate entity attribute.</summary>
    public const string AttendedTapDateField = "AttendedTapDate";
    /// <summary>Identifies the AccountType entity attribute.</summary>
    public const string AccountTypeField = "AccountType";
    /// <summary>Identifies the VeteranRank entity attribute.</summary>
    public const string VeteranRankField = "VeteranRank";
    /// <summary>Identifies the Enabled entity attribute.</summary>
    public const string EnabledField = "Enabled";
    /// <summary>Identifies the Blocked entity attribute.</summary>
    public const string BlockedField = "Blocked";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSeeker")]
    private readonly EntityCollection<JobSeekerData> _jobSeekerData = new EntityCollection<JobSeekerData>();
    [ReverseAssociation("JobSeeker")]
    private readonly EntityCollection<JobSeekerActivityAssignment> _jobSeekerActivityAssignments = new EntityCollection<JobSeekerActivityAssignment>();
    [ReverseAssociation("JobSeeker")]
    private readonly EntityCollection<JobSeekerAction> _jobSeekerActions = new EntityCollection<JobSeekerAction>();
    [ReverseAssociation("JobSeeker")]
    private readonly EntityCollection<JobSeekerOffice> _jobSeekerOffices = new EntityCollection<JobSeekerOffice>();
    [ReverseAssociation("JobSeeker")]
    private readonly EntityCollection<JobSeekerJobHistory> _jobSeekerJobHistories = new EntityCollection<JobSeekerJobHistory>();
    [ReverseAssociation("JobSeeker")]
    private readonly EntityCollection<JobSeekerActivityLog> _jobSeekerActivityLogs = new EntityCollection<JobSeekerActivityLog>();
    [ReverseAssociation("JobSeeker")]
    private readonly EntityCollection<JobSeekerMilitaryHistory> _jobSeekerMilitaryHistories = new EntityCollection<JobSeekerMilitaryHistory>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSeekerData> JobSeekerData
    {
      get { return Get(_jobSeekerData); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSeekerActivityAssignment> JobSeekerActivityAssignments
    {
      get { return Get(_jobSeekerActivityAssignments); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSeekerAction> JobSeekerActions
    {
      get { return Get(_jobSeekerActions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSeekerOffice> JobSeekerOffices
    {
      get { return Get(_jobSeekerOffices); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSeekerJobHistory> JobSeekerJobHistories
    {
      get { return Get(_jobSeekerJobHistories); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSeekerActivityLog> JobSeekerActivityLogs
    {
      get { return Get(_jobSeekerActivityLogs); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSeekerMilitaryHistory> JobSeekerMilitaryHistories
    {
      get { return Get(_jobSeekerMilitaryHistories); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public long FocusPersonId
    {
      get { return Get(ref _focusPersonId, "FocusPersonId"); }
      set { Set(ref _focusPersonId, value, "FocusPersonId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string FirstName
    {
      get { return Get(ref _firstName, "FirstName"); }
      set { Set(ref _firstName, value, "FirstName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LastName
    {
      get { return Get(ref _lastName, "LastName"); }
      set { Set(ref _lastName, value, "LastName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> CountyId
    {
      get { return Get(ref _countyId, "CountyId"); }
      set { Set(ref _countyId, value, "CountyId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string County
    {
      get { return Get(ref _county, "County"); }
      set { Set(ref _county, value, "County"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> StateId
    {
      get { return Get(ref _stateId, "StateId"); }
      set { Set(ref _stateId, value, "StateId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string State
    {
      get { return Get(ref _state, "State"); }
      set { Set(ref _state, value, "State"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PostalCode
    {
      get { return Get(ref _postalCode, "PostalCode"); }
      set { Set(ref _postalCode, value, "PostalCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Office
    {
      get { return Get(ref _office, "Office"); }
      set { Set(ref _office, value, "Office"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime AccountCreationDate
    {
      get { return Get(ref _accountCreationDate, "AccountCreationDate"); }
      set { Set(ref _accountCreationDate, value, "AccountCreationDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> Salary
    {
      get { return Get(ref _salary, "Salary"); }
      set { Set(ref _salary, value, "Salary"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ReportSalaryFrequency> SalaryFrequency
    {
      get { return Get(ref _salaryFrequency, "SalaryFrequency"); }
      set { Set(ref _salaryFrequency, value, "SalaryFrequency"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.Models.Career.EmploymentStatus> EmploymentStatus
    {
      get { return Get(ref _employmentStatus, "EmploymentStatus"); }
      set { Set(ref _employmentStatus, value, "EmploymentStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.Models.Career.VeteranEra> VeteranType
    {
      get { return Get(ref _veteranType, "VeteranType"); }
      set { Set(ref _veteranType, value, "VeteranType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.Models.Career.VeteranDisabilityStatus> VeteranDisability
    {
      get { return Get(ref _veteranDisability, "VeteranDisability"); }
      set { Set(ref _veteranDisability, value, "VeteranDisability"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.Models.Career.VeteranTransitionType> VeteranTransitionType
    {
      get { return Get(ref _veteranTransitionType, "VeteranTransitionType"); }
      set { Set(ref _veteranTransitionType, value, "VeteranTransitionType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ReportMilitaryDischargeType> VeteranMilitaryDischarge
    {
      get { return Get(ref _veteranMilitaryDischarge, "VeteranMilitaryDischarge"); }
      set { Set(ref _veteranMilitaryDischarge, value, "VeteranMilitaryDischarge"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ReportMilitaryBranchOfService> VeteranBranchOfService
    {
      get { return Get(ref _veteranBranchOfService, "VeteranBranchOfService"); }
      set { Set(ref _veteranBranchOfService, value, "VeteranBranchOfService"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> CampaignVeteran
    {
      get { return Get(ref _campaignVeteran, "CampaignVeteran"); }
      set { Set(ref _campaignVeteran, value, "CampaignVeteran"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> DateOfBirth
    {
      get { return Get(ref _dateOfBirth, "DateOfBirth"); }
      set { Set(ref _dateOfBirth, value, "DateOfBirth"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.Genders> Gender
    {
      get { return Get(ref _gender, "Gender"); }
      set { Set(ref _gender, value, "Gender"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmailAddress
    {
      get { return Get(ref _emailAddress, "EmailAddress"); }
      set { Set(ref _emailAddress, value, "EmailAddress"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.Models.Career.DisabilityStatus> DisabilityStatus
    {
      get { return Get(ref _disabilityStatus, "DisabilityStatus"); }
      set { Set(ref _disabilityStatus, value, "DisabilityStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ReportDisabilityType> DisabilityType
    {
      get { return Get(ref _disabilityType, "DisabilityType"); }
      set { Set(ref _disabilityType, value, "DisabilityType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ReportEthnicHeritage> EthnicHeritage
    {
      get { return Get(ref _ethnicHeritage, "EthnicHeritage"); }
      set { Set(ref _ethnicHeritage, value, "EthnicHeritage"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ReportRace> Race
    {
      get { return Get(ref _race, "Race"); }
      set { Set(ref _race, value, "Race"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> ResumeSearchable
    {
      get { return Get(ref _resumeSearchable, "ResumeSearchable"); }
      set { Set(ref _resumeSearchable, value, "ResumeSearchable"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int ResumeCount
    {
      get { return Get(ref _resumeCount, "ResumeCount"); }
      set { Set(ref _resumeCount, value, "ResumeCount"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> WillingToWorkOvertime
    {
      get { return Get(ref _willingToWorkOvertime, "WillingToWorkOvertime"); }
      set { Set(ref _willingToWorkOvertime, value, "WillingToWorkOvertime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> WillingToRelocate
    {
      get { return Get(ref _willingToRelocate, "WillingToRelocate"); }
      set { Set(ref _willingToRelocate, value, "WillingToRelocate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.EducationLevel> MinimumEducationLevel
    {
      get { return Get(ref _minimumEducationLevel, "MinimumEducationLevel"); }
      set { Set(ref _minimumEducationLevel, value, "MinimumEducationLevel"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public double LatitudeRadians
    {
      get { return Get(ref _latitudeRadians, "LatitudeRadians"); }
      set { Set(ref _latitudeRadians, value, "LatitudeRadians"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public double LongitudeRadians
    {
      get { return Get(ref _longitudeRadians, "LongitudeRadians"); }
      set { Set(ref _longitudeRadians, value, "LongitudeRadians"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool HasPendingResume
    {
      get { return Get(ref _hasPendingResume, "HasPendingResume"); }
      set { Set(ref _hasPendingResume, value, "HasPendingResume"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.SchoolStatus> EnrollmentStatus
    {
      get { return Get(ref _enrollmentStatus, "EnrollmentStatus"); }
      set { Set(ref _enrollmentStatus, value, "EnrollmentStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> MilitaryStartDate
    {
      get { return Get(ref _militaryStartDate, "MilitaryStartDate"); }
      set { Set(ref _militaryStartDate, value, "MilitaryStartDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> MilitaryEndDate
    {
      get { return Get(ref _militaryEndDate, "MilitaryEndDate"); }
      set { Set(ref _militaryEndDate, value, "MilitaryEndDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> IsHomelessVeteran
    {
      get { return Get(ref _isHomelessVeteran, "IsHomelessVeteran"); }
      set { Set(ref _isHomelessVeteran, value, "IsHomelessVeteran"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string SocialSecurityNumber
    {
      get { return Get(ref _socialSecurityNumber, "SocialSecurityNumber"); }
      set { Set(ref _socialSecurityNumber, value, "SocialSecurityNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string MiddleInitial
    {
      get { return Get(ref _middleInitial, "MiddleInitial"); }
      set { Set(ref _middleInitial, value, "MiddleInitial"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AddressLine1
    {
      get { return Get(ref _addressLine1, "AddressLine1"); }
      set { Set(ref _addressLine1, value, "AddressLine1"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> IsCitizen
    {
      get { return Get(ref _isCitizen, "IsCitizen"); }
      set { Set(ref _isCitizen, value, "IsCitizen"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> AlienCardExpires
    {
      get { return Get(ref _alienCardExpires, "AlienCardExpires"); }
      set { Set(ref _alienCardExpires, value, "AlienCardExpires"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AlienCardId
    {
      get { return Get(ref _alienCardId, "AlienCardId"); }
      set { Set(ref _alienCardId, value, "AlienCardId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> VeteranAttendedTapCourse
    {
      get { return Get(ref _veteranAttendedTapCourse, "VeteranAttendedTapCourse"); }
      set { Set(ref _veteranAttendedTapCourse, value, "VeteranAttendedTapCourse"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<short> Age
    {
      get { return Get(ref _age, "Age"); }
      set { Set(ref _age, value, "Age"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string NcrcLevel
    {
      get { return Get(ref _ncrcLevel, "NcrcLevel"); }
      set { Set(ref _ncrcLevel, value, "NcrcLevel"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool Unsubscribed
    {
      get { return Get(ref _unsubscribed, "Unsubscribed"); }
      set { Set(ref _unsubscribed, value, "Unsubscribed"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Suffix
    {
      get { return Get(ref _suffix, "Suffix"); }
      set { Set(ref _suffix, value, "Suffix"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool HasAttendedTap
    {
      get { return Get(ref _hasAttendedTap, "HasAttendedTap"); }
      set { Set(ref _hasAttendedTap, value, "HasAttendedTap"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> AttendedTapDate
    {
      get { return Get(ref _attendedTapDate, "AttendedTapDate"); }
      set { Set(ref _attendedTapDate, value, "AttendedTapDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> AccountType
    {
      get { return Get(ref _accountType, "AccountType"); }
      set { Set(ref _accountType, value, "AccountType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string VeteranRank
    {
      get { return Get(ref _veteranRank, "VeteranRank"); }
      set { Set(ref _veteranRank, value, "VeteranRank"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool Enabled
    {
      get { return Get(ref _enabled, "Enabled"); }
      set { Set(ref _enabled, value, "Enabled"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool Blocked
    {
      get { return Get(ref _blocked, "Blocked"); }
      set { Set(ref _blocked, value, "Blocked"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }
    }

    /// <summary>Gets the time the entity was last updated</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime UpdatedOn
    {
      get { return _updatedOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.Employer", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class Employer : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 400)]
    private string _name;
    [ValidatePresence]
    [ValidateLength(0, 72)]
    private string _federalEmployerIdentificationNumber;
    private System.Nullable<long> _stateId;
    [ValidateLength(0, 40)]
    private string _postalCode;
    [ValidateLength(0, 400)]
    private string _office;
    private System.Nullable<System.DateTime> _creationDateTime;
    private System.Nullable<long> _countyId;
    private long _focusBusinessUnitId;
    [ValidateLength(0, 400)]
    private string _state;
    [ValidateLength(0, 400)]
    private string _county;
    private double _latitudeRadians;
    private double _longitudeRadians;
    private System.Nullable<long> _focusEmployerId;
    private System.Nullable<long> _accountTypeId;
    private System.Nullable<long> _noOfEmployees;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    private readonly System.DateTime _updatedOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the FederalEmployerIdentificationNumber entity attribute.</summary>
    public const string FederalEmployerIdentificationNumberField = "FederalEmployerIdentificationNumber";
    /// <summary>Identifies the StateId entity attribute.</summary>
    public const string StateIdField = "StateId";
    /// <summary>Identifies the PostalCode entity attribute.</summary>
    public const string PostalCodeField = "PostalCode";
    /// <summary>Identifies the Office entity attribute.</summary>
    public const string OfficeField = "Office";
    /// <summary>Identifies the CreationDateTime entity attribute.</summary>
    public const string CreationDateTimeField = "CreationDateTime";
    /// <summary>Identifies the CountyId entity attribute.</summary>
    public const string CountyIdField = "CountyId";
    /// <summary>Identifies the FocusBusinessUnitId entity attribute.</summary>
    public const string FocusBusinessUnitIdField = "FocusBusinessUnitId";
    /// <summary>Identifies the State entity attribute.</summary>
    public const string StateField = "State";
    /// <summary>Identifies the County entity attribute.</summary>
    public const string CountyField = "County";
    /// <summary>Identifies the LatitudeRadians entity attribute.</summary>
    public const string LatitudeRadiansField = "LatitudeRadians";
    /// <summary>Identifies the LongitudeRadians entity attribute.</summary>
    public const string LongitudeRadiansField = "LongitudeRadians";
    /// <summary>Identifies the FocusEmployerId entity attribute.</summary>
    public const string FocusEmployerIdField = "FocusEmployerId";
    /// <summary>Identifies the AccountTypeId entity attribute.</summary>
    public const string AccountTypeIdField = "AccountTypeId";
    /// <summary>Identifies the NoOfEmployees entity attribute.</summary>
    public const string NoOfEmployeesField = "NoOfEmployees";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Employer")]
    private readonly EntityCollection<EmployerAction> _employerActions = new EntityCollection<EmployerAction>();
    [ReverseAssociation("Employer")]
    private readonly EntityCollection<EmployerOffice> _employerOffices = new EntityCollection<EmployerOffice>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<EmployerAction> EmployerActions
    {
      get { return Get(_employerActions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<EmployerOffice> EmployerOffices
    {
      get { return Get(_employerOffices); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string FederalEmployerIdentificationNumber
    {
      get { return Get(ref _federalEmployerIdentificationNumber, "FederalEmployerIdentificationNumber"); }
      set { Set(ref _federalEmployerIdentificationNumber, value, "FederalEmployerIdentificationNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> StateId
    {
      get { return Get(ref _stateId, "StateId"); }
      set { Set(ref _stateId, value, "StateId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PostalCode
    {
      get { return Get(ref _postalCode, "PostalCode"); }
      set { Set(ref _postalCode, value, "PostalCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Office
    {
      get { return Get(ref _office, "Office"); }
      set { Set(ref _office, value, "Office"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> CreationDateTime
    {
      get { return Get(ref _creationDateTime, "CreationDateTime"); }
      set { Set(ref _creationDateTime, value, "CreationDateTime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> CountyId
    {
      get { return Get(ref _countyId, "CountyId"); }
      set { Set(ref _countyId, value, "CountyId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long FocusBusinessUnitId
    {
      get { return Get(ref _focusBusinessUnitId, "FocusBusinessUnitId"); }
      set { Set(ref _focusBusinessUnitId, value, "FocusBusinessUnitId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string State
    {
      get { return Get(ref _state, "State"); }
      set { Set(ref _state, value, "State"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string County
    {
      get { return Get(ref _county, "County"); }
      set { Set(ref _county, value, "County"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public double LatitudeRadians
    {
      get { return Get(ref _latitudeRadians, "LatitudeRadians"); }
      set { Set(ref _latitudeRadians, value, "LatitudeRadians"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public double LongitudeRadians
    {
      get { return Get(ref _longitudeRadians, "LongitudeRadians"); }
      set { Set(ref _longitudeRadians, value, "LongitudeRadians"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> FocusEmployerId
    {
      get { return Get(ref _focusEmployerId, "FocusEmployerId"); }
      set { Set(ref _focusEmployerId, value, "FocusEmployerId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> AccountTypeId
    {
      get { return Get(ref _accountTypeId, "AccountTypeId"); }
      set { Set(ref _accountTypeId, value, "AccountTypeId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> NoOfEmployees
    {
      get { return Get(ref _noOfEmployees, "NoOfEmployees"); }
      set { Set(ref _noOfEmployees, value, "NoOfEmployees"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }
    }

    /// <summary>Gets the time the entity was last updated</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime UpdatedOn
    {
      get { return _updatedOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.JobOrder", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobOrder : Entity<long>
  {
    #region Fields
  
    private long _focusJobId;
    private long _employerId;
    [ValidatePresence]
    [ValidateLength(0, 400)]
    private string _jobTitle;
    private Focus.Core.JobStatuses _jobStatus;
    private System.Nullable<long> _countyId;
    private System.Nullable<long> _stateId;
    [ValidateLength(0, 40)]
    private string _postalCode;
    [ValidateLength(0, 400)]
    private string _office;
    private System.Nullable<Focus.Core.EducationLevels> _minimumEducationLevel;
    private System.Nullable<Focus.Core.JobPostingFlags> _postingFlags;
    private System.Nullable<Focus.Core.WorkOpportunitiesTaxCreditCategories> _workOpportunitiesTaxCreditHires;
    private System.Nullable<bool> _foreignLabourCertification;
    private System.Nullable<Focus.Core.ScreeningPreferences> _screeningPreferences;
    private System.Nullable<decimal> _minSalary;
    private System.Nullable<decimal> _maxSalary;
    private System.Nullable<Focus.Core.ReportSalaryFrequency> _salaryFrequency;
    [ValidateLength(0, 20)]
    private string _onetCode;
    private System.Nullable<System.DateTime> _postedOn;
    private System.Nullable<System.DateTime> _closingOn;
    private double _latitudeRadians;
    private double _longitudeRadians;
    [ValidateLength(0, 400)]
    private string _county;
    [ValidateLength(0, 400)]
    private string _state;
    private string _description;
    private System.Nullable<Focus.Core.ForeignLaborTypes> _foreignLabourType;
    private System.Nullable<bool> _federalContractor;
    private System.Nullable<bool> _courtOrderedAffirmativeAction;
    private System.Nullable<long> _focusBusinessUnitId;
    [ValidateLength(0, 50)]
    private string _ncrcLevel;
    private System.Nullable<bool> _ncrcLevelRequired;
    private System.Nullable<int> _numberOfOpenings;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    private readonly System.DateTime _updatedOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the FocusJobId entity attribute.</summary>
    public const string FocusJobIdField = "FocusJobId";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the JobTitle entity attribute.</summary>
    public const string JobTitleField = "JobTitle";
    /// <summary>Identifies the JobStatus entity attribute.</summary>
    public const string JobStatusField = "JobStatus";
    /// <summary>Identifies the CountyId entity attribute.</summary>
    public const string CountyIdField = "CountyId";
    /// <summary>Identifies the StateId entity attribute.</summary>
    public const string StateIdField = "StateId";
    /// <summary>Identifies the PostalCode entity attribute.</summary>
    public const string PostalCodeField = "PostalCode";
    /// <summary>Identifies the Office entity attribute.</summary>
    public const string OfficeField = "Office";
    /// <summary>Identifies the MinimumEducationLevel entity attribute.</summary>
    public const string MinimumEducationLevelField = "MinimumEducationLevel";
    /// <summary>Identifies the PostingFlags entity attribute.</summary>
    public const string PostingFlagsField = "PostingFlags";
    /// <summary>Identifies the WorkOpportunitiesTaxCreditHires entity attribute.</summary>
    public const string WorkOpportunitiesTaxCreditHiresField = "WorkOpportunitiesTaxCreditHires";
    /// <summary>Identifies the ForeignLabourCertification entity attribute.</summary>
    public const string ForeignLabourCertificationField = "ForeignLabourCertification";
    /// <summary>Identifies the ScreeningPreferences entity attribute.</summary>
    public const string ScreeningPreferencesField = "ScreeningPreferences";
    /// <summary>Identifies the MinSalary entity attribute.</summary>
    public const string MinSalaryField = "MinSalary";
    /// <summary>Identifies the MaxSalary entity attribute.</summary>
    public const string MaxSalaryField = "MaxSalary";
    /// <summary>Identifies the SalaryFrequency entity attribute.</summary>
    public const string SalaryFrequencyField = "SalaryFrequency";
    /// <summary>Identifies the OnetCode entity attribute.</summary>
    public const string OnetCodeField = "OnetCode";
    /// <summary>Identifies the PostedOn entity attribute.</summary>
    public const string PostedOnField = "PostedOn";
    /// <summary>Identifies the ClosingOn entity attribute.</summary>
    public const string ClosingOnField = "ClosingOn";
    /// <summary>Identifies the LatitudeRadians entity attribute.</summary>
    public const string LatitudeRadiansField = "LatitudeRadians";
    /// <summary>Identifies the LongitudeRadians entity attribute.</summary>
    public const string LongitudeRadiansField = "LongitudeRadians";
    /// <summary>Identifies the County entity attribute.</summary>
    public const string CountyField = "County";
    /// <summary>Identifies the State entity attribute.</summary>
    public const string StateField = "State";
    /// <summary>Identifies the Description entity attribute.</summary>
    public const string DescriptionField = "Description";
    /// <summary>Identifies the ForeignLabourType entity attribute.</summary>
    public const string ForeignLabourTypeField = "ForeignLabourType";
    /// <summary>Identifies the FederalContractor entity attribute.</summary>
    public const string FederalContractorField = "FederalContractor";
    /// <summary>Identifies the CourtOrderedAffirmativeAction entity attribute.</summary>
    public const string CourtOrderedAffirmativeActionField = "CourtOrderedAffirmativeAction";
    /// <summary>Identifies the FocusBusinessUnitId entity attribute.</summary>
    public const string FocusBusinessUnitIdField = "FocusBusinessUnitId";
    /// <summary>Identifies the NcrcLevel entity attribute.</summary>
    public const string NcrcLevelField = "NcrcLevel";
    /// <summary>Identifies the NcrcLevelRequired entity attribute.</summary>
    public const string NcrcLevelRequiredField = "NcrcLevelRequired";
    /// <summary>Identifies the NumberOfOpenings entity attribute.</summary>
    public const string NumberOfOpeningsField = "NumberOfOpenings";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobOrder")]
    private readonly EntityCollection<JobOrderAction> _jobOrderActions = new EntityCollection<JobOrderAction>();
    [ReverseAssociation("JobOrder")]
    private readonly EntityCollection<JobOrderOffice> _jobOrderOffices = new EntityCollection<JobOrderOffice>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobOrderAction> JobOrderActions
    {
      get { return Get(_jobOrderActions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobOrderOffice> JobOrderOffices
    {
      get { return Get(_jobOrderOffices); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public long FocusJobId
    {
      get { return Get(ref _focusJobId, "FocusJobId"); }
      set { Set(ref _focusJobId, value, "FocusJobId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string JobTitle
    {
      get { return Get(ref _jobTitle, "JobTitle"); }
      set { Set(ref _jobTitle, value, "JobTitle"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.JobStatuses JobStatus
    {
      get { return Get(ref _jobStatus, "JobStatus"); }
      set { Set(ref _jobStatus, value, "JobStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> CountyId
    {
      get { return Get(ref _countyId, "CountyId"); }
      set { Set(ref _countyId, value, "CountyId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> StateId
    {
      get { return Get(ref _stateId, "StateId"); }
      set { Set(ref _stateId, value, "StateId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PostalCode
    {
      get { return Get(ref _postalCode, "PostalCode"); }
      set { Set(ref _postalCode, value, "PostalCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Office
    {
      get { return Get(ref _office, "Office"); }
      set { Set(ref _office, value, "Office"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.EducationLevels> MinimumEducationLevel
    {
      get { return Get(ref _minimumEducationLevel, "MinimumEducationLevel"); }
      set { Set(ref _minimumEducationLevel, value, "MinimumEducationLevel"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.JobPostingFlags> PostingFlags
    {
      get { return Get(ref _postingFlags, "PostingFlags"); }
      set { Set(ref _postingFlags, value, "PostingFlags"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.WorkOpportunitiesTaxCreditCategories> WorkOpportunitiesTaxCreditHires
    {
      get { return Get(ref _workOpportunitiesTaxCreditHires, "WorkOpportunitiesTaxCreditHires"); }
      set { Set(ref _workOpportunitiesTaxCreditHires, value, "WorkOpportunitiesTaxCreditHires"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> ForeignLabourCertification
    {
      get { return Get(ref _foreignLabourCertification, "ForeignLabourCertification"); }
      set { Set(ref _foreignLabourCertification, value, "ForeignLabourCertification"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ScreeningPreferences> ScreeningPreferences
    {
      get { return Get(ref _screeningPreferences, "ScreeningPreferences"); }
      set { Set(ref _screeningPreferences, value, "ScreeningPreferences"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> MinSalary
    {
      get { return Get(ref _minSalary, "MinSalary"); }
      set { Set(ref _minSalary, value, "MinSalary"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> MaxSalary
    {
      get { return Get(ref _maxSalary, "MaxSalary"); }
      set { Set(ref _maxSalary, value, "MaxSalary"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ReportSalaryFrequency> SalaryFrequency
    {
      get { return Get(ref _salaryFrequency, "SalaryFrequency"); }
      set { Set(ref _salaryFrequency, value, "SalaryFrequency"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetCode
    {
      get { return Get(ref _onetCode, "OnetCode"); }
      set { Set(ref _onetCode, value, "OnetCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> PostedOn
    {
      get { return Get(ref _postedOn, "PostedOn"); }
      set { Set(ref _postedOn, value, "PostedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ClosingOn
    {
      get { return Get(ref _closingOn, "ClosingOn"); }
      set { Set(ref _closingOn, value, "ClosingOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public double LatitudeRadians
    {
      get { return Get(ref _latitudeRadians, "LatitudeRadians"); }
      set { Set(ref _latitudeRadians, value, "LatitudeRadians"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public double LongitudeRadians
    {
      get { return Get(ref _longitudeRadians, "LongitudeRadians"); }
      set { Set(ref _longitudeRadians, value, "LongitudeRadians"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string County
    {
      get { return Get(ref _county, "County"); }
      set { Set(ref _county, value, "County"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string State
    {
      get { return Get(ref _state, "State"); }
      set { Set(ref _state, value, "State"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Description
    {
      get { return Get(ref _description, "Description"); }
      set { Set(ref _description, value, "Description"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ForeignLaborTypes> ForeignLabourType
    {
      get { return Get(ref _foreignLabourType, "ForeignLabourType"); }
      set { Set(ref _foreignLabourType, value, "ForeignLabourType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> FederalContractor
    {
      get { return Get(ref _federalContractor, "FederalContractor"); }
      set { Set(ref _federalContractor, value, "FederalContractor"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> CourtOrderedAffirmativeAction
    {
      get { return Get(ref _courtOrderedAffirmativeAction, "CourtOrderedAffirmativeAction"); }
      set { Set(ref _courtOrderedAffirmativeAction, value, "CourtOrderedAffirmativeAction"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> FocusBusinessUnitId
    {
      get { return Get(ref _focusBusinessUnitId, "FocusBusinessUnitId"); }
      set { Set(ref _focusBusinessUnitId, value, "FocusBusinessUnitId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string NcrcLevel
    {
      get { return Get(ref _ncrcLevel, "NcrcLevel"); }
      set { Set(ref _ncrcLevel, value, "NcrcLevel"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> NcrcLevelRequired
    {
      get { return Get(ref _ncrcLevelRequired, "NcrcLevelRequired"); }
      set { Set(ref _ncrcLevelRequired, value, "NcrcLevelRequired"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> NumberOfOpenings
    {
      get { return Get(ref _numberOfOpenings, "NumberOfOpenings"); }
      set { Set(ref _numberOfOpenings, value, "NumberOfOpenings"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }
    }

    /// <summary>Gets the time the entity was last updated</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime UpdatedOn
    {
      get { return _updatedOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.JobSeekerData", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeekerData : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    private string _description;
    private Focus.Core.ReportJobSeekerDataType _dataType;
    private long _jobSeekerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Description entity attribute.</summary>
    public const string DescriptionField = "Description";
    /// <summary>Identifies the DataType entity attribute.</summary>
    public const string DataTypeField = "DataType";
    /// <summary>Identifies the JobSeekerId entity attribute.</summary>
    public const string JobSeekerIdField = "JobSeekerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSeekerData")]
    private readonly EntityHolder<JobSeeker> _jobSeeker = new EntityHolder<JobSeeker>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public JobSeeker JobSeeker
    {
      get { return Get(_jobSeeker); }
      set { Set(_jobSeeker, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Description
    {
      get { return Get(ref _description, "Description"); }
      set { Set(ref _description, value, "Description"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ReportJobSeekerDataType DataType
    {
      get { return Get(ref _dataType, "DataType"); }
      set { Set(ref _dataType, value, "DataType"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobSeeker" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobSeekerId
    {
      get { return Get(ref _jobSeekerId, "JobSeekerId"); }
      set { Set(ref _jobSeekerId, value, "JobSeekerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.JobSeekerAction", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeekerAction : Entity<long>
  {
    #region Fields
  
    private System.DateTime _actionDate;
    private int _postingsViewed;
    private int _logins;
    private int _referralRequests;
    private int _selfReferrals;
    private int _staffReferrals;
    private int _notesAdded;
    private int _addedToLists;
    private int _activitiesAssigned;
    private int _staffAssignments;
    private int _emailsSent;
    private int _followUpIssues;
    private int _issuesResolved;
    private int _hired;
    private int _notHired;
    private int _failedToApply;
    private int _failedToReportToInterview;
    private int _interviewDenied;
    private int _interviewScheduled;
    private int _newApplicant;
    private int _recommended;
    private int _refusedOffer;
    private int _underConsideration;
    private int _usedOnlineResumeHelp;
    private int _savedJobAlerts;
    private int _targetingHighGrowthSectors;
    private int _selfReferralsExternal;
    private int _staffReferralsExternal;
    private int _referralsApproved;
    [Transient]
    private int _activityServices;
    private int _findJobsForSeeker;
    private int _failedToReportToJob;
    private int _failedToRespondToInvitation;
    private int _foundJobFromOtherSource;
    private int _jobAlreadyFilled;
    private int _notQualified;
    private int _notYetPlaced;
    private int _refusedReferral;
    private int _surveySatisfied;
    private int _surveyDissatisfied;
    private int _surveyNoUnexpectedMatches;
    private int _surveyUnexpectedMatches;
    private int _surveyReceivedInvitations;
    private int _surveyDidNotReceiveInvitations;
    private int _surveyVerySatisfied;
    private int _surveyVeryDissatisfied;
    private long _jobSeekerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ActionDate entity attribute.</summary>
    public const string ActionDateField = "ActionDate";
    /// <summary>Identifies the PostingsViewed entity attribute.</summary>
    public const string PostingsViewedField = "PostingsViewed";
    /// <summary>Identifies the Logins entity attribute.</summary>
    public const string LoginsField = "Logins";
    /// <summary>Identifies the ReferralRequests entity attribute.</summary>
    public const string ReferralRequestsField = "ReferralRequests";
    /// <summary>Identifies the SelfReferrals entity attribute.</summary>
    public const string SelfReferralsField = "SelfReferrals";
    /// <summary>Identifies the StaffReferrals entity attribute.</summary>
    public const string StaffReferralsField = "StaffReferrals";
    /// <summary>Identifies the NotesAdded entity attribute.</summary>
    public const string NotesAddedField = "NotesAdded";
    /// <summary>Identifies the AddedToLists entity attribute.</summary>
    public const string AddedToListsField = "AddedToLists";
    /// <summary>Identifies the ActivitiesAssigned entity attribute.</summary>
    public const string ActivitiesAssignedField = "ActivitiesAssigned";
    /// <summary>Identifies the StaffAssignments entity attribute.</summary>
    public const string StaffAssignmentsField = "StaffAssignments";
    /// <summary>Identifies the EmailsSent entity attribute.</summary>
    public const string EmailsSentField = "EmailsSent";
    /// <summary>Identifies the FollowUpIssues entity attribute.</summary>
    public const string FollowUpIssuesField = "FollowUpIssues";
    /// <summary>Identifies the IssuesResolved entity attribute.</summary>
    public const string IssuesResolvedField = "IssuesResolved";
    /// <summary>Identifies the Hired entity attribute.</summary>
    public const string HiredField = "Hired";
    /// <summary>Identifies the NotHired entity attribute.</summary>
    public const string NotHiredField = "NotHired";
    /// <summary>Identifies the FailedToApply entity attribute.</summary>
    public const string FailedToApplyField = "FailedToApply";
    /// <summary>Identifies the FailedToReportToInterview entity attribute.</summary>
    public const string FailedToReportToInterviewField = "FailedToReportToInterview";
    /// <summary>Identifies the InterviewDenied entity attribute.</summary>
    public const string InterviewDeniedField = "InterviewDenied";
    /// <summary>Identifies the InterviewScheduled entity attribute.</summary>
    public const string InterviewScheduledField = "InterviewScheduled";
    /// <summary>Identifies the NewApplicant entity attribute.</summary>
    public const string NewApplicantField = "NewApplicant";
    /// <summary>Identifies the Recommended entity attribute.</summary>
    public const string RecommendedField = "Recommended";
    /// <summary>Identifies the RefusedOffer entity attribute.</summary>
    public const string RefusedOfferField = "RefusedOffer";
    /// <summary>Identifies the UnderConsideration entity attribute.</summary>
    public const string UnderConsiderationField = "UnderConsideration";
    /// <summary>Identifies the UsedOnlineResumeHelp entity attribute.</summary>
    public const string UsedOnlineResumeHelpField = "UsedOnlineResumeHelp";
    /// <summary>Identifies the SavedJobAlerts entity attribute.</summary>
    public const string SavedJobAlertsField = "SavedJobAlerts";
    /// <summary>Identifies the TargetingHighGrowthSectors entity attribute.</summary>
    public const string TargetingHighGrowthSectorsField = "TargetingHighGrowthSectors";
    /// <summary>Identifies the SelfReferralsExternal entity attribute.</summary>
    public const string SelfReferralsExternalField = "SelfReferralsExternal";
    /// <summary>Identifies the StaffReferralsExternal entity attribute.</summary>
    public const string StaffReferralsExternalField = "StaffReferralsExternal";
    /// <summary>Identifies the ReferralsApproved entity attribute.</summary>
    public const string ReferralsApprovedField = "ReferralsApproved";
    /// <summary>Identifies the ActivityServices entity attribute.</summary>
    public const string ActivityServicesField = "ActivityServices";
    /// <summary>Identifies the FindJobsForSeeker entity attribute.</summary>
    public const string FindJobsForSeekerField = "FindJobsForSeeker";
    /// <summary>Identifies the FailedToReportToJob entity attribute.</summary>
    public const string FailedToReportToJobField = "FailedToReportToJob";
    /// <summary>Identifies the FailedToRespondToInvitation entity attribute.</summary>
    public const string FailedToRespondToInvitationField = "FailedToRespondToInvitation";
    /// <summary>Identifies the FoundJobFromOtherSource entity attribute.</summary>
    public const string FoundJobFromOtherSourceField = "FoundJobFromOtherSource";
    /// <summary>Identifies the JobAlreadyFilled entity attribute.</summary>
    public const string JobAlreadyFilledField = "JobAlreadyFilled";
    /// <summary>Identifies the NotQualified entity attribute.</summary>
    public const string NotQualifiedField = "NotQualified";
    /// <summary>Identifies the NotYetPlaced entity attribute.</summary>
    public const string NotYetPlacedField = "NotYetPlaced";
    /// <summary>Identifies the RefusedReferral entity attribute.</summary>
    public const string RefusedReferralField = "RefusedReferral";
    /// <summary>Identifies the SurveySatisfied entity attribute.</summary>
    public const string SurveySatisfiedField = "SurveySatisfied";
    /// <summary>Identifies the SurveyDissatisfied entity attribute.</summary>
    public const string SurveyDissatisfiedField = "SurveyDissatisfied";
    /// <summary>Identifies the SurveyNoUnexpectedMatches entity attribute.</summary>
    public const string SurveyNoUnexpectedMatchesField = "SurveyNoUnexpectedMatches";
    /// <summary>Identifies the SurveyUnexpectedMatches entity attribute.</summary>
    public const string SurveyUnexpectedMatchesField = "SurveyUnexpectedMatches";
    /// <summary>Identifies the SurveyReceivedInvitations entity attribute.</summary>
    public const string SurveyReceivedInvitationsField = "SurveyReceivedInvitations";
    /// <summary>Identifies the SurveyDidNotReceiveInvitations entity attribute.</summary>
    public const string SurveyDidNotReceiveInvitationsField = "SurveyDidNotReceiveInvitations";
    /// <summary>Identifies the SurveyVerySatisfied entity attribute.</summary>
    public const string SurveyVerySatisfiedField = "SurveyVerySatisfied";
    /// <summary>Identifies the SurveyVeryDissatisfied entity attribute.</summary>
    public const string SurveyVeryDissatisfiedField = "SurveyVeryDissatisfied";
    /// <summary>Identifies the JobSeekerId entity attribute.</summary>
    public const string JobSeekerIdField = "JobSeekerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSeekerActions")]
    private readonly EntityHolder<JobSeeker> _jobSeeker = new EntityHolder<JobSeeker>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public JobSeeker JobSeeker
    {
      get { return Get(_jobSeeker); }
      set { Set(_jobSeeker, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ActionDate
    {
      get { return Get(ref _actionDate, "ActionDate"); }
      set { Set(ref _actionDate, value, "ActionDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int PostingsViewed
    {
      get { return Get(ref _postingsViewed, "PostingsViewed"); }
      set { Set(ref _postingsViewed, value, "PostingsViewed"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int Logins
    {
      get { return Get(ref _logins, "Logins"); }
      set { Set(ref _logins, value, "Logins"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int ReferralRequests
    {
      get { return Get(ref _referralRequests, "ReferralRequests"); }
      set { Set(ref _referralRequests, value, "ReferralRequests"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SelfReferrals
    {
      get { return Get(ref _selfReferrals, "SelfReferrals"); }
      set { Set(ref _selfReferrals, value, "SelfReferrals"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int StaffReferrals
    {
      get { return Get(ref _staffReferrals, "StaffReferrals"); }
      set { Set(ref _staffReferrals, value, "StaffReferrals"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int NotesAdded
    {
      get { return Get(ref _notesAdded, "NotesAdded"); }
      set { Set(ref _notesAdded, value, "NotesAdded"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int AddedToLists
    {
      get { return Get(ref _addedToLists, "AddedToLists"); }
      set { Set(ref _addedToLists, value, "AddedToLists"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int ActivitiesAssigned
    {
      get { return Get(ref _activitiesAssigned, "ActivitiesAssigned"); }
      set { Set(ref _activitiesAssigned, value, "ActivitiesAssigned"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int StaffAssignments
    {
      get { return Get(ref _staffAssignments, "StaffAssignments"); }
      set { Set(ref _staffAssignments, value, "StaffAssignments"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int EmailsSent
    {
      get { return Get(ref _emailsSent, "EmailsSent"); }
      set { Set(ref _emailsSent, value, "EmailsSent"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int FollowUpIssues
    {
      get { return Get(ref _followUpIssues, "FollowUpIssues"); }
      set { Set(ref _followUpIssues, value, "FollowUpIssues"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int IssuesResolved
    {
      get { return Get(ref _issuesResolved, "IssuesResolved"); }
      set { Set(ref _issuesResolved, value, "IssuesResolved"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int Hired
    {
      get { return Get(ref _hired, "Hired"); }
      set { Set(ref _hired, value, "Hired"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int NotHired
    {
      get { return Get(ref _notHired, "NotHired"); }
      set { Set(ref _notHired, value, "NotHired"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int FailedToApply
    {
      get { return Get(ref _failedToApply, "FailedToApply"); }
      set { Set(ref _failedToApply, value, "FailedToApply"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int FailedToReportToInterview
    {
      get { return Get(ref _failedToReportToInterview, "FailedToReportToInterview"); }
      set { Set(ref _failedToReportToInterview, value, "FailedToReportToInterview"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int InterviewDenied
    {
      get { return Get(ref _interviewDenied, "InterviewDenied"); }
      set { Set(ref _interviewDenied, value, "InterviewDenied"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int InterviewScheduled
    {
      get { return Get(ref _interviewScheduled, "InterviewScheduled"); }
      set { Set(ref _interviewScheduled, value, "InterviewScheduled"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int NewApplicant
    {
      get { return Get(ref _newApplicant, "NewApplicant"); }
      set { Set(ref _newApplicant, value, "NewApplicant"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int Recommended
    {
      get { return Get(ref _recommended, "Recommended"); }
      set { Set(ref _recommended, value, "Recommended"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int RefusedOffer
    {
      get { return Get(ref _refusedOffer, "RefusedOffer"); }
      set { Set(ref _refusedOffer, value, "RefusedOffer"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int UnderConsideration
    {
      get { return Get(ref _underConsideration, "UnderConsideration"); }
      set { Set(ref _underConsideration, value, "UnderConsideration"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int UsedOnlineResumeHelp
    {
      get { return Get(ref _usedOnlineResumeHelp, "UsedOnlineResumeHelp"); }
      set { Set(ref _usedOnlineResumeHelp, value, "UsedOnlineResumeHelp"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SavedJobAlerts
    {
      get { return Get(ref _savedJobAlerts, "SavedJobAlerts"); }
      set { Set(ref _savedJobAlerts, value, "SavedJobAlerts"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int TargetingHighGrowthSectors
    {
      get { return Get(ref _targetingHighGrowthSectors, "TargetingHighGrowthSectors"); }
      set { Set(ref _targetingHighGrowthSectors, value, "TargetingHighGrowthSectors"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SelfReferralsExternal
    {
      get { return Get(ref _selfReferralsExternal, "SelfReferralsExternal"); }
      set { Set(ref _selfReferralsExternal, value, "SelfReferralsExternal"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int StaffReferralsExternal
    {
      get { return Get(ref _staffReferralsExternal, "StaffReferralsExternal"); }
      set { Set(ref _staffReferralsExternal, value, "StaffReferralsExternal"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int ReferralsApproved
    {
      get { return Get(ref _referralsApproved, "ReferralsApproved"); }
      set { Set(ref _referralsApproved, value, "ReferralsApproved"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int ActivityServices
    {
      get { return Get(ref _activityServices, "ActivityServices"); }
      set { Set(ref _activityServices, value, "ActivityServices"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int FindJobsForSeeker
    {
      get { return Get(ref _findJobsForSeeker, "FindJobsForSeeker"); }
      set { Set(ref _findJobsForSeeker, value, "FindJobsForSeeker"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int FailedToReportToJob
    {
      get { return Get(ref _failedToReportToJob, "FailedToReportToJob"); }
      set { Set(ref _failedToReportToJob, value, "FailedToReportToJob"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int FailedToRespondToInvitation
    {
      get { return Get(ref _failedToRespondToInvitation, "FailedToRespondToInvitation"); }
      set { Set(ref _failedToRespondToInvitation, value, "FailedToRespondToInvitation"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int FoundJobFromOtherSource
    {
      get { return Get(ref _foundJobFromOtherSource, "FoundJobFromOtherSource"); }
      set { Set(ref _foundJobFromOtherSource, value, "FoundJobFromOtherSource"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int JobAlreadyFilled
    {
      get { return Get(ref _jobAlreadyFilled, "JobAlreadyFilled"); }
      set { Set(ref _jobAlreadyFilled, value, "JobAlreadyFilled"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int NotQualified
    {
      get { return Get(ref _notQualified, "NotQualified"); }
      set { Set(ref _notQualified, value, "NotQualified"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int NotYetPlaced
    {
      get { return Get(ref _notYetPlaced, "NotYetPlaced"); }
      set { Set(ref _notYetPlaced, value, "NotYetPlaced"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int RefusedReferral
    {
      get { return Get(ref _refusedReferral, "RefusedReferral"); }
      set { Set(ref _refusedReferral, value, "RefusedReferral"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SurveySatisfied
    {
      get { return Get(ref _surveySatisfied, "SurveySatisfied"); }
      set { Set(ref _surveySatisfied, value, "SurveySatisfied"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SurveyDissatisfied
    {
      get { return Get(ref _surveyDissatisfied, "SurveyDissatisfied"); }
      set { Set(ref _surveyDissatisfied, value, "SurveyDissatisfied"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SurveyNoUnexpectedMatches
    {
      get { return Get(ref _surveyNoUnexpectedMatches, "SurveyNoUnexpectedMatches"); }
      set { Set(ref _surveyNoUnexpectedMatches, value, "SurveyNoUnexpectedMatches"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SurveyUnexpectedMatches
    {
      get { return Get(ref _surveyUnexpectedMatches, "SurveyUnexpectedMatches"); }
      set { Set(ref _surveyUnexpectedMatches, value, "SurveyUnexpectedMatches"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SurveyReceivedInvitations
    {
      get { return Get(ref _surveyReceivedInvitations, "SurveyReceivedInvitations"); }
      set { Set(ref _surveyReceivedInvitations, value, "SurveyReceivedInvitations"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SurveyDidNotReceiveInvitations
    {
      get { return Get(ref _surveyDidNotReceiveInvitations, "SurveyDidNotReceiveInvitations"); }
      set { Set(ref _surveyDidNotReceiveInvitations, value, "SurveyDidNotReceiveInvitations"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SurveyVerySatisfied
    {
      get { return Get(ref _surveyVerySatisfied, "SurveyVerySatisfied"); }
      set { Set(ref _surveyVerySatisfied, value, "SurveyVerySatisfied"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SurveyVeryDissatisfied
    {
      get { return Get(ref _surveyVeryDissatisfied, "SurveyVeryDissatisfied"); }
      set { Set(ref _surveyVeryDissatisfied, value, "SurveyVeryDissatisfied"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobSeeker" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobSeekerId
    {
      get { return Get(ref _jobSeekerId, "JobSeekerId"); }
      set { Set(ref _jobSeekerId, value, "JobSeekerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.JobSeekerActivityAssignment", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeekerActivityAssignment : Entity<long>
  {
    #region Fields
  
    private System.DateTime _assignDate;
    private int _assignmentsMade;
    [ValidateLength(0, 400)]
    private string _activity;
    [ValidateLength(0, 400)]
    private string _activityCategory;
    private long _jobSeekerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the AssignDate entity attribute.</summary>
    public const string AssignDateField = "AssignDate";
    /// <summary>Identifies the AssignmentsMade entity attribute.</summary>
    public const string AssignmentsMadeField = "AssignmentsMade";
    /// <summary>Identifies the Activity entity attribute.</summary>
    public const string ActivityField = "Activity";
    /// <summary>Identifies the ActivityCategory entity attribute.</summary>
    public const string ActivityCategoryField = "ActivityCategory";
    /// <summary>Identifies the JobSeekerId entity attribute.</summary>
    public const string JobSeekerIdField = "JobSeekerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSeekerActivityAssignments")]
    private readonly EntityHolder<JobSeeker> _jobSeeker = new EntityHolder<JobSeeker>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public JobSeeker JobSeeker
    {
      get { return Get(_jobSeeker); }
      set { Set(_jobSeeker, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime AssignDate
    {
      get { return Get(ref _assignDate, "AssignDate"); }
      set { Set(ref _assignDate, value, "AssignDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int AssignmentsMade
    {
      get { return Get(ref _assignmentsMade, "AssignmentsMade"); }
      set { Set(ref _assignmentsMade, value, "AssignmentsMade"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Activity
    {
      get { return Get(ref _activity, "Activity"); }
      set { Set(ref _activity, value, "Activity"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ActivityCategory
    {
      get { return Get(ref _activityCategory, "ActivityCategory"); }
      set { Set(ref _activityCategory, value, "ActivityCategory"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobSeeker" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobSeekerId
    {
      get { return Get(ref _jobSeekerId, "JobSeekerId"); }
      set { Set(ref _jobSeekerId, value, "JobSeekerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.LookupJobSeekerCertificateView")]
  public partial class LookupJobSeekerCertificateView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    private string _description;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Description entity attribute.</summary>
    public const string DescriptionField = "Description";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Description
    {
      get { return Get(ref _description, "Description"); }
      set { Set(ref _description, value, "Description"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.LookupJobSeekerEducationView")]
  public partial class LookupJobSeekerEducationView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    private string _description;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Description entity attribute.</summary>
    public const string DescriptionField = "Description";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Description
    {
      get { return Get(ref _description, "Description"); }
      set { Set(ref _description, value, "Description"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.LookupJobSeekerInternshipView")]
  public partial class LookupJobSeekerInternshipView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    private string _description;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Description entity attribute.</summary>
    public const string DescriptionField = "Description";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Description
    {
      get { return Get(ref _description, "Description"); }
      set { Set(ref _description, value, "Description"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.LookupJobSeekerSkillView")]
  public partial class LookupJobSeekerSkillView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    private string _description;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Description entity attribute.</summary>
    public const string DescriptionField = "Description";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Description
    {
      get { return Get(ref _description, "Description"); }
      set { Set(ref _description, value, "Description"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.LookupJobSeekerMilitaryOccupationCodeView")]
  public partial class LookupJobSeekerMilitaryOccupationCodeView : Entity<long>
  {
    #region Fields
  
    private string _militaryOccupationCode;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the MilitaryOccupationCode entity attribute.</summary>
    public const string MilitaryOccupationCodeField = "MilitaryOccupationCode";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string MilitaryOccupationCode
    {
      get { return Get(ref _militaryOccupationCode, "MilitaryOccupationCode"); }
      set { Set(ref _militaryOccupationCode, value, "MilitaryOccupationCode"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.LookupJobSeekerMilitaryOccupationTitleView")]
  public partial class LookupJobSeekerMilitaryOccupationTitleView : Entity<long>
  {
    #region Fields
  
    private string _militaryOccupationTitle;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the MilitaryOccupationTitle entity attribute.</summary>
    public const string MilitaryOccupationTitleField = "MilitaryOccupationTitle";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string MilitaryOccupationTitle
    {
      get { return Get(ref _militaryOccupationTitle, "MilitaryOccupationTitle"); }
      set { Set(ref _militaryOccupationTitle, value, "MilitaryOccupationTitle"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class KeywordFilterView : Entity<long>
  {
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.SavedReport", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class SavedReport : Entity<long>
  {
    #region Fields
  
    private Focus.Core.ReportType _reportType;
    private string _criteria;
    private System.Nullable<long> _userId;
    [ValidateLength(0, 200)]
    private string _name;
    private System.DateTime _reportDate;
    private bool _displayOnDashboard;
    private bool _isSessionReport;
    private System.Nullable<Focus.Core.ReportDisplayType> _reportDisplayType;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ReportType entity attribute.</summary>
    public const string ReportTypeField = "ReportType";
    /// <summary>Identifies the Criteria entity attribute.</summary>
    public const string CriteriaField = "Criteria";
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the ReportDate entity attribute.</summary>
    public const string ReportDateField = "ReportDate";
    /// <summary>Identifies the DisplayOnDashboard entity attribute.</summary>
    public const string DisplayOnDashboardField = "DisplayOnDashboard";
    /// <summary>Identifies the IsSessionReport entity attribute.</summary>
    public const string IsSessionReportField = "IsSessionReport";
    /// <summary>Identifies the ReportDisplayType entity attribute.</summary>
    public const string ReportDisplayTypeField = "ReportDisplayType";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ReportType ReportType
    {
      get { return Get(ref _reportType, "ReportType"); }
      set { Set(ref _reportType, value, "ReportType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Criteria
    {
      get { return Get(ref _criteria, "Criteria"); }
      set { Set(ref _criteria, value, "Criteria"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ReportDate
    {
      get { return Get(ref _reportDate, "ReportDate"); }
      set { Set(ref _reportDate, value, "ReportDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool DisplayOnDashboard
    {
      get { return Get(ref _displayOnDashboard, "DisplayOnDashboard"); }
      set { Set(ref _displayOnDashboard, value, "DisplayOnDashboard"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsSessionReport
    {
      get { return Get(ref _isSessionReport, "IsSessionReport"); }
      set { Set(ref _isSessionReport, value, "IsSessionReport"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ReportDisplayType> ReportDisplayType
    {
      get { return Get(ref _reportDisplayType, "ReportDisplayType"); }
      set { Set(ref _reportDisplayType, value, "ReportDisplayType"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.JobOrderAction", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobOrderAction : Entity<long>
  {
    #region Fields
  
    private System.DateTime _actionDate;
    private int _staffReferrals;
    private int _selfReferrals;
    private int _employerInvitationsSent;
    private int _applicantsInterviewed;
    private int _applicantsFailedToShow;
    private int _applicantsDeniedInterviews;
    private int _applicantsHired;
    private int _jobOffersRefused;
    private int _applicantsDidNotApply;
    private int _invitedJobSeekerViewed;
    private int _invitedJobSeekerClicked;
    private int _referralsRequested;
    private int _applicantsNotHired;
    private int _applicantsNotYetPlaced;
    private int _failedToRespondToInvitation;
    private int _failedToReportToJob;
    private int _foundJobFromOtherSource;
    private int _jobAlreadyFilled;
    private int _newApplicant;
    private int _notQualified;
    private int _applicantRecommended;
    private int _refusedReferral;
    private int _underConsideration;
    private long _jobOrderId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ActionDate entity attribute.</summary>
    public const string ActionDateField = "ActionDate";
    /// <summary>Identifies the StaffReferrals entity attribute.</summary>
    public const string StaffReferralsField = "StaffReferrals";
    /// <summary>Identifies the SelfReferrals entity attribute.</summary>
    public const string SelfReferralsField = "SelfReferrals";
    /// <summary>Identifies the EmployerInvitationsSent entity attribute.</summary>
    public const string EmployerInvitationsSentField = "EmployerInvitationsSent";
    /// <summary>Identifies the ApplicantsInterviewed entity attribute.</summary>
    public const string ApplicantsInterviewedField = "ApplicantsInterviewed";
    /// <summary>Identifies the ApplicantsFailedToShow entity attribute.</summary>
    public const string ApplicantsFailedToShowField = "ApplicantsFailedToShow";
    /// <summary>Identifies the ApplicantsDeniedInterviews entity attribute.</summary>
    public const string ApplicantsDeniedInterviewsField = "ApplicantsDeniedInterviews";
    /// <summary>Identifies the ApplicantsHired entity attribute.</summary>
    public const string ApplicantsHiredField = "ApplicantsHired";
    /// <summary>Identifies the JobOffersRefused entity attribute.</summary>
    public const string JobOffersRefusedField = "JobOffersRefused";
    /// <summary>Identifies the ApplicantsDidNotApply entity attribute.</summary>
    public const string ApplicantsDidNotApplyField = "ApplicantsDidNotApply";
    /// <summary>Identifies the InvitedJobSeekerViewed entity attribute.</summary>
    public const string InvitedJobSeekerViewedField = "InvitedJobSeekerViewed";
    /// <summary>Identifies the InvitedJobSeekerClicked entity attribute.</summary>
    public const string InvitedJobSeekerClickedField = "InvitedJobSeekerClicked";
    /// <summary>Identifies the ReferralsRequested entity attribute.</summary>
    public const string ReferralsRequestedField = "ReferralsRequested";
    /// <summary>Identifies the ApplicantsNotHired entity attribute.</summary>
    public const string ApplicantsNotHiredField = "ApplicantsNotHired";
    /// <summary>Identifies the ApplicantsNotYetPlaced entity attribute.</summary>
    public const string ApplicantsNotYetPlacedField = "ApplicantsNotYetPlaced";
    /// <summary>Identifies the FailedToRespondToInvitation entity attribute.</summary>
    public const string FailedToRespondToInvitationField = "FailedToRespondToInvitation";
    /// <summary>Identifies the FailedToReportToJob entity attribute.</summary>
    public const string FailedToReportToJobField = "FailedToReportToJob";
    /// <summary>Identifies the FoundJobFromOtherSource entity attribute.</summary>
    public const string FoundJobFromOtherSourceField = "FoundJobFromOtherSource";
    /// <summary>Identifies the JobAlreadyFilled entity attribute.</summary>
    public const string JobAlreadyFilledField = "JobAlreadyFilled";
    /// <summary>Identifies the NewApplicant entity attribute.</summary>
    public const string NewApplicantField = "NewApplicant";
    /// <summary>Identifies the NotQualified entity attribute.</summary>
    public const string NotQualifiedField = "NotQualified";
    /// <summary>Identifies the ApplicantRecommended entity attribute.</summary>
    public const string ApplicantRecommendedField = "ApplicantRecommended";
    /// <summary>Identifies the RefusedReferral entity attribute.</summary>
    public const string RefusedReferralField = "RefusedReferral";
    /// <summary>Identifies the UnderConsideration entity attribute.</summary>
    public const string UnderConsiderationField = "UnderConsideration";
    /// <summary>Identifies the JobOrderId entity attribute.</summary>
    public const string JobOrderIdField = "JobOrderId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobOrderActions")]
    private readonly EntityHolder<JobOrder> _jobOrder = new EntityHolder<JobOrder>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public JobOrder JobOrder
    {
      get { return Get(_jobOrder); }
      set { Set(_jobOrder, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ActionDate
    {
      get { return Get(ref _actionDate, "ActionDate"); }
      set { Set(ref _actionDate, value, "ActionDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int StaffReferrals
    {
      get { return Get(ref _staffReferrals, "StaffReferrals"); }
      set { Set(ref _staffReferrals, value, "StaffReferrals"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SelfReferrals
    {
      get { return Get(ref _selfReferrals, "SelfReferrals"); }
      set { Set(ref _selfReferrals, value, "SelfReferrals"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int EmployerInvitationsSent
    {
      get { return Get(ref _employerInvitationsSent, "EmployerInvitationsSent"); }
      set { Set(ref _employerInvitationsSent, value, "EmployerInvitationsSent"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int ApplicantsInterviewed
    {
      get { return Get(ref _applicantsInterviewed, "ApplicantsInterviewed"); }
      set { Set(ref _applicantsInterviewed, value, "ApplicantsInterviewed"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int ApplicantsFailedToShow
    {
      get { return Get(ref _applicantsFailedToShow, "ApplicantsFailedToShow"); }
      set { Set(ref _applicantsFailedToShow, value, "ApplicantsFailedToShow"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int ApplicantsDeniedInterviews
    {
      get { return Get(ref _applicantsDeniedInterviews, "ApplicantsDeniedInterviews"); }
      set { Set(ref _applicantsDeniedInterviews, value, "ApplicantsDeniedInterviews"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int ApplicantsHired
    {
      get { return Get(ref _applicantsHired, "ApplicantsHired"); }
      set { Set(ref _applicantsHired, value, "ApplicantsHired"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int JobOffersRefused
    {
      get { return Get(ref _jobOffersRefused, "JobOffersRefused"); }
      set { Set(ref _jobOffersRefused, value, "JobOffersRefused"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int ApplicantsDidNotApply
    {
      get { return Get(ref _applicantsDidNotApply, "ApplicantsDidNotApply"); }
      set { Set(ref _applicantsDidNotApply, value, "ApplicantsDidNotApply"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int InvitedJobSeekerViewed
    {
      get { return Get(ref _invitedJobSeekerViewed, "InvitedJobSeekerViewed"); }
      set { Set(ref _invitedJobSeekerViewed, value, "InvitedJobSeekerViewed"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int InvitedJobSeekerClicked
    {
      get { return Get(ref _invitedJobSeekerClicked, "InvitedJobSeekerClicked"); }
      set { Set(ref _invitedJobSeekerClicked, value, "InvitedJobSeekerClicked"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int ReferralsRequested
    {
      get { return Get(ref _referralsRequested, "ReferralsRequested"); }
      set { Set(ref _referralsRequested, value, "ReferralsRequested"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int ApplicantsNotHired
    {
      get { return Get(ref _applicantsNotHired, "ApplicantsNotHired"); }
      set { Set(ref _applicantsNotHired, value, "ApplicantsNotHired"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int ApplicantsNotYetPlaced
    {
      get { return Get(ref _applicantsNotYetPlaced, "ApplicantsNotYetPlaced"); }
      set { Set(ref _applicantsNotYetPlaced, value, "ApplicantsNotYetPlaced"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int FailedToRespondToInvitation
    {
      get { return Get(ref _failedToRespondToInvitation, "FailedToRespondToInvitation"); }
      set { Set(ref _failedToRespondToInvitation, value, "FailedToRespondToInvitation"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int FailedToReportToJob
    {
      get { return Get(ref _failedToReportToJob, "FailedToReportToJob"); }
      set { Set(ref _failedToReportToJob, value, "FailedToReportToJob"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int FoundJobFromOtherSource
    {
      get { return Get(ref _foundJobFromOtherSource, "FoundJobFromOtherSource"); }
      set { Set(ref _foundJobFromOtherSource, value, "FoundJobFromOtherSource"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int JobAlreadyFilled
    {
      get { return Get(ref _jobAlreadyFilled, "JobAlreadyFilled"); }
      set { Set(ref _jobAlreadyFilled, value, "JobAlreadyFilled"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int NewApplicant
    {
      get { return Get(ref _newApplicant, "NewApplicant"); }
      set { Set(ref _newApplicant, value, "NewApplicant"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int NotQualified
    {
      get { return Get(ref _notQualified, "NotQualified"); }
      set { Set(ref _notQualified, value, "NotQualified"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int ApplicantRecommended
    {
      get { return Get(ref _applicantRecommended, "ApplicantRecommended"); }
      set { Set(ref _applicantRecommended, value, "ApplicantRecommended"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int RefusedReferral
    {
      get { return Get(ref _refusedReferral, "RefusedReferral"); }
      set { Set(ref _refusedReferral, value, "RefusedReferral"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int UnderConsideration
    {
      get { return Get(ref _underConsideration, "UnderConsideration"); }
      set { Set(ref _underConsideration, value, "UnderConsideration"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobOrder" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobOrderId
    {
      get { return Get(ref _jobOrderId, "JobOrderId"); }
      set { Set(ref _jobOrderId, value, "JobOrderId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.EmployerAction", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class EmployerAction : Entity<long>
  {
    #region Fields
  
    private System.DateTime _actionDate;
    private int _jobOrdersEdited;
    private int _jobSeekersInterviewed;
    private int _jobSeekersHired;
    private int _jobOrdersCreated;
    private int _jobOrdersPosted;
    private int _jobOrdersPutOnHold;
    private int _jobOrdersRefreshed;
    private int _jobOrdersClosed;
    private int _invitationsSent;
    private int _jobSeekersNotHired;
    private int _selfReferrals;
    private int _staffReferrals;
    private long _employerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ActionDate entity attribute.</summary>
    public const string ActionDateField = "ActionDate";
    /// <summary>Identifies the JobOrdersEdited entity attribute.</summary>
    public const string JobOrdersEditedField = "JobOrdersEdited";
    /// <summary>Identifies the JobSeekersInterviewed entity attribute.</summary>
    public const string JobSeekersInterviewedField = "JobSeekersInterviewed";
    /// <summary>Identifies the JobSeekersHired entity attribute.</summary>
    public const string JobSeekersHiredField = "JobSeekersHired";
    /// <summary>Identifies the JobOrdersCreated entity attribute.</summary>
    public const string JobOrdersCreatedField = "JobOrdersCreated";
    /// <summary>Identifies the JobOrdersPosted entity attribute.</summary>
    public const string JobOrdersPostedField = "JobOrdersPosted";
    /// <summary>Identifies the JobOrdersPutOnHold entity attribute.</summary>
    public const string JobOrdersPutOnHoldField = "JobOrdersPutOnHold";
    /// <summary>Identifies the JobOrdersRefreshed entity attribute.</summary>
    public const string JobOrdersRefreshedField = "JobOrdersRefreshed";
    /// <summary>Identifies the JobOrdersClosed entity attribute.</summary>
    public const string JobOrdersClosedField = "JobOrdersClosed";
    /// <summary>Identifies the InvitationsSent entity attribute.</summary>
    public const string InvitationsSentField = "InvitationsSent";
    /// <summary>Identifies the JobSeekersNotHired entity attribute.</summary>
    public const string JobSeekersNotHiredField = "JobSeekersNotHired";
    /// <summary>Identifies the SelfReferrals entity attribute.</summary>
    public const string SelfReferralsField = "SelfReferrals";
    /// <summary>Identifies the StaffReferrals entity attribute.</summary>
    public const string StaffReferralsField = "StaffReferrals";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("EmployerActions")]
    private readonly EntityHolder<Employer> _employer = new EntityHolder<Employer>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Employer Employer
    {
      get { return Get(_employer); }
      set { Set(_employer, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ActionDate
    {
      get { return Get(ref _actionDate, "ActionDate"); }
      set { Set(ref _actionDate, value, "ActionDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int JobOrdersEdited
    {
      get { return Get(ref _jobOrdersEdited, "JobOrdersEdited"); }
      set { Set(ref _jobOrdersEdited, value, "JobOrdersEdited"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int JobSeekersInterviewed
    {
      get { return Get(ref _jobSeekersInterviewed, "JobSeekersInterviewed"); }
      set { Set(ref _jobSeekersInterviewed, value, "JobSeekersInterviewed"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int JobSeekersHired
    {
      get { return Get(ref _jobSeekersHired, "JobSeekersHired"); }
      set { Set(ref _jobSeekersHired, value, "JobSeekersHired"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int JobOrdersCreated
    {
      get { return Get(ref _jobOrdersCreated, "JobOrdersCreated"); }
      set { Set(ref _jobOrdersCreated, value, "JobOrdersCreated"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int JobOrdersPosted
    {
      get { return Get(ref _jobOrdersPosted, "JobOrdersPosted"); }
      set { Set(ref _jobOrdersPosted, value, "JobOrdersPosted"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int JobOrdersPutOnHold
    {
      get { return Get(ref _jobOrdersPutOnHold, "JobOrdersPutOnHold"); }
      set { Set(ref _jobOrdersPutOnHold, value, "JobOrdersPutOnHold"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int JobOrdersRefreshed
    {
      get { return Get(ref _jobOrdersRefreshed, "JobOrdersRefreshed"); }
      set { Set(ref _jobOrdersRefreshed, value, "JobOrdersRefreshed"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int JobOrdersClosed
    {
      get { return Get(ref _jobOrdersClosed, "JobOrdersClosed"); }
      set { Set(ref _jobOrdersClosed, value, "JobOrdersClosed"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int InvitationsSent
    {
      get { return Get(ref _invitationsSent, "InvitationsSent"); }
      set { Set(ref _invitationsSent, value, "InvitationsSent"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int JobSeekersNotHired
    {
      get { return Get(ref _jobSeekersNotHired, "JobSeekersNotHired"); }
      set { Set(ref _jobSeekersNotHired, value, "JobSeekersNotHired"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SelfReferrals
    {
      get { return Get(ref _selfReferrals, "SelfReferrals"); }
      set { Set(ref _selfReferrals, value, "SelfReferrals"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int StaffReferrals
    {
      get { return Get(ref _staffReferrals, "StaffReferrals"); }
      set { Set(ref _staffReferrals, value, "StaffReferrals"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Employer" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.JobOrderView")]
  public partial class JobOrderView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 400)]
    private string _jobTitle;
    [ValidatePresence]
    [ValidateLength(0, 400)]
    private string _employerName;
    [ValidateLength(0, 400)]
    private string _state;
    [ValidateLength(0, 400)]
    private string _office;
    private long _focusJobId;
    private long _employerId;
    private Focus.Core.JobStatuses _jobStatus;
    private double _latitudeRadians;
    private double _longitudeRadians;
    private string _description;
    [ValidateLength(0, 400)]
    private string _county;
    [ValidateLength(0, 40)]
    private string _postalCode;
    private System.Nullable<Focus.Core.EducationLevels> _minimumEducationLevel;
    private System.Nullable<Focus.Core.JobPostingFlags> _postingFlags;
    private System.Nullable<Focus.Core.WorkOpportunitiesTaxCreditCategories> _workOpportunitiesTaxCreditHires;
    private System.Nullable<bool> _foreignLabourCertification;
    private System.Nullable<Focus.Core.ScreeningPreferences> _screeningPreferences;
    private System.Nullable<decimal> _minSalary;
    private System.Nullable<decimal> _maxSalary;
    private System.Nullable<Focus.Core.ReportSalaryFrequency> _salaryFrequency;
    [ValidateLength(0, 20)]
    private string _onetCode;
    private System.Nullable<System.DateTime> _postedOn;
    private System.Nullable<System.DateTime> _closingOn;
    private System.Nullable<long> _countyId;
    private System.Nullable<long> _stateId;
    [ValidatePresence]
    [ValidateLength(0, 72)]
    private string _federalEmployerIdentificationNumber;
    private System.Nullable<Focus.Core.ForeignLaborTypes> _foreignLabourType;
    private System.Nullable<bool> _federalContractor;
    private System.Nullable<bool> _courtOrderedAffirmativeAction;
    private System.Nullable<long> _accountTypeId;
    [ValidateLength(0, 50)]
    private string _ncrcLevel;
    private System.Nullable<bool> _ncrcLevelRequired;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    private readonly System.DateTime _updatedOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobTitle entity attribute.</summary>
    public const string JobTitleField = "JobTitle";
    /// <summary>Identifies the EmployerName entity attribute.</summary>
    public const string EmployerNameField = "EmployerName";
    /// <summary>Identifies the State entity attribute.</summary>
    public const string StateField = "State";
    /// <summary>Identifies the Office entity attribute.</summary>
    public const string OfficeField = "Office";
    /// <summary>Identifies the FocusJobId entity attribute.</summary>
    public const string FocusJobIdField = "FocusJobId";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the JobStatus entity attribute.</summary>
    public const string JobStatusField = "JobStatus";
    /// <summary>Identifies the LatitudeRadians entity attribute.</summary>
    public const string LatitudeRadiansField = "LatitudeRadians";
    /// <summary>Identifies the LongitudeRadians entity attribute.</summary>
    public const string LongitudeRadiansField = "LongitudeRadians";
    /// <summary>Identifies the Description entity attribute.</summary>
    public const string DescriptionField = "Description";
    /// <summary>Identifies the County entity attribute.</summary>
    public const string CountyField = "County";
    /// <summary>Identifies the PostalCode entity attribute.</summary>
    public const string PostalCodeField = "PostalCode";
    /// <summary>Identifies the MinimumEducationLevel entity attribute.</summary>
    public const string MinimumEducationLevelField = "MinimumEducationLevel";
    /// <summary>Identifies the PostingFlags entity attribute.</summary>
    public const string PostingFlagsField = "PostingFlags";
    /// <summary>Identifies the WorkOpportunitiesTaxCreditHires entity attribute.</summary>
    public const string WorkOpportunitiesTaxCreditHiresField = "WorkOpportunitiesTaxCreditHires";
    /// <summary>Identifies the ForeignLabourCertification entity attribute.</summary>
    public const string ForeignLabourCertificationField = "ForeignLabourCertification";
    /// <summary>Identifies the ScreeningPreferences entity attribute.</summary>
    public const string ScreeningPreferencesField = "ScreeningPreferences";
    /// <summary>Identifies the MinSalary entity attribute.</summary>
    public const string MinSalaryField = "MinSalary";
    /// <summary>Identifies the MaxSalary entity attribute.</summary>
    public const string MaxSalaryField = "MaxSalary";
    /// <summary>Identifies the SalaryFrequency entity attribute.</summary>
    public const string SalaryFrequencyField = "SalaryFrequency";
    /// <summary>Identifies the OnetCode entity attribute.</summary>
    public const string OnetCodeField = "OnetCode";
    /// <summary>Identifies the PostedOn entity attribute.</summary>
    public const string PostedOnField = "PostedOn";
    /// <summary>Identifies the ClosingOn entity attribute.</summary>
    public const string ClosingOnField = "ClosingOn";
    /// <summary>Identifies the CountyId entity attribute.</summary>
    public const string CountyIdField = "CountyId";
    /// <summary>Identifies the StateId entity attribute.</summary>
    public const string StateIdField = "StateId";
    /// <summary>Identifies the FederalEmployerIdentificationNumber entity attribute.</summary>
    public const string FederalEmployerIdentificationNumberField = "FederalEmployerIdentificationNumber";
    /// <summary>Identifies the ForeignLabourType entity attribute.</summary>
    public const string ForeignLabourTypeField = "ForeignLabourType";
    /// <summary>Identifies the FederalContractor entity attribute.</summary>
    public const string FederalContractorField = "FederalContractor";
    /// <summary>Identifies the CourtOrderedAffirmativeAction entity attribute.</summary>
    public const string CourtOrderedAffirmativeActionField = "CourtOrderedAffirmativeAction";
    /// <summary>Identifies the AccountTypeId entity attribute.</summary>
    public const string AccountTypeIdField = "AccountTypeId";
    /// <summary>Identifies the NcrcLevel entity attribute.</summary>
    public const string NcrcLevelField = "NcrcLevel";
    /// <summary>Identifies the NcrcLevelRequired entity attribute.</summary>
    public const string NcrcLevelRequiredField = "NcrcLevelRequired";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobOrderView")]
    private readonly EntityCollection<JobOrderOffice> _jobOrderOffices = new EntityCollection<JobOrderOffice>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobOrderOffice> JobOrderOffices
    {
      get { return Get(_jobOrderOffices); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string JobTitle
    {
      get { return Get(ref _jobTitle, "JobTitle"); }
      set { Set(ref _jobTitle, value, "JobTitle"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerName
    {
      get { return Get(ref _employerName, "EmployerName"); }
      set { Set(ref _employerName, value, "EmployerName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string State
    {
      get { return Get(ref _state, "State"); }
      set { Set(ref _state, value, "State"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Office
    {
      get { return Get(ref _office, "Office"); }
      set { Set(ref _office, value, "Office"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long FocusJobId
    {
      get { return Get(ref _focusJobId, "FocusJobId"); }
      set { Set(ref _focusJobId, value, "FocusJobId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.JobStatuses JobStatus
    {
      get { return Get(ref _jobStatus, "JobStatus"); }
      set { Set(ref _jobStatus, value, "JobStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public double LatitudeRadians
    {
      get { return Get(ref _latitudeRadians, "LatitudeRadians"); }
      set { Set(ref _latitudeRadians, value, "LatitudeRadians"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public double LongitudeRadians
    {
      get { return Get(ref _longitudeRadians, "LongitudeRadians"); }
      set { Set(ref _longitudeRadians, value, "LongitudeRadians"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Description
    {
      get { return Get(ref _description, "Description"); }
      set { Set(ref _description, value, "Description"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string County
    {
      get { return Get(ref _county, "County"); }
      set { Set(ref _county, value, "County"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PostalCode
    {
      get { return Get(ref _postalCode, "PostalCode"); }
      set { Set(ref _postalCode, value, "PostalCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.EducationLevels> MinimumEducationLevel
    {
      get { return Get(ref _minimumEducationLevel, "MinimumEducationLevel"); }
      set { Set(ref _minimumEducationLevel, value, "MinimumEducationLevel"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.JobPostingFlags> PostingFlags
    {
      get { return Get(ref _postingFlags, "PostingFlags"); }
      set { Set(ref _postingFlags, value, "PostingFlags"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.WorkOpportunitiesTaxCreditCategories> WorkOpportunitiesTaxCreditHires
    {
      get { return Get(ref _workOpportunitiesTaxCreditHires, "WorkOpportunitiesTaxCreditHires"); }
      set { Set(ref _workOpportunitiesTaxCreditHires, value, "WorkOpportunitiesTaxCreditHires"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> ForeignLabourCertification
    {
      get { return Get(ref _foreignLabourCertification, "ForeignLabourCertification"); }
      set { Set(ref _foreignLabourCertification, value, "ForeignLabourCertification"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ScreeningPreferences> ScreeningPreferences
    {
      get { return Get(ref _screeningPreferences, "ScreeningPreferences"); }
      set { Set(ref _screeningPreferences, value, "ScreeningPreferences"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> MinSalary
    {
      get { return Get(ref _minSalary, "MinSalary"); }
      set { Set(ref _minSalary, value, "MinSalary"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> MaxSalary
    {
      get { return Get(ref _maxSalary, "MaxSalary"); }
      set { Set(ref _maxSalary, value, "MaxSalary"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ReportSalaryFrequency> SalaryFrequency
    {
      get { return Get(ref _salaryFrequency, "SalaryFrequency"); }
      set { Set(ref _salaryFrequency, value, "SalaryFrequency"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetCode
    {
      get { return Get(ref _onetCode, "OnetCode"); }
      set { Set(ref _onetCode, value, "OnetCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> PostedOn
    {
      get { return Get(ref _postedOn, "PostedOn"); }
      set { Set(ref _postedOn, value, "PostedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ClosingOn
    {
      get { return Get(ref _closingOn, "ClosingOn"); }
      set { Set(ref _closingOn, value, "ClosingOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> CountyId
    {
      get { return Get(ref _countyId, "CountyId"); }
      set { Set(ref _countyId, value, "CountyId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> StateId
    {
      get { return Get(ref _stateId, "StateId"); }
      set { Set(ref _stateId, value, "StateId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string FederalEmployerIdentificationNumber
    {
      get { return Get(ref _federalEmployerIdentificationNumber, "FederalEmployerIdentificationNumber"); }
      set { Set(ref _federalEmployerIdentificationNumber, value, "FederalEmployerIdentificationNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ForeignLaborTypes> ForeignLabourType
    {
      get { return Get(ref _foreignLabourType, "ForeignLabourType"); }
      set { Set(ref _foreignLabourType, value, "ForeignLabourType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> FederalContractor
    {
      get { return Get(ref _federalContractor, "FederalContractor"); }
      set { Set(ref _federalContractor, value, "FederalContractor"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> CourtOrderedAffirmativeAction
    {
      get { return Get(ref _courtOrderedAffirmativeAction, "CourtOrderedAffirmativeAction"); }
      set { Set(ref _courtOrderedAffirmativeAction, value, "CourtOrderedAffirmativeAction"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> AccountTypeId
    {
      get { return Get(ref _accountTypeId, "AccountTypeId"); }
      set { Set(ref _accountTypeId, value, "AccountTypeId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string NcrcLevel
    {
      get { return Get(ref _ncrcLevel, "NcrcLevel"); }
      set { Set(ref _ncrcLevel, value, "NcrcLevel"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> NcrcLevelRequired
    {
      get { return Get(ref _ncrcLevelRequired, "NcrcLevelRequired"); }
      set { Set(ref _ncrcLevelRequired, value, "NcrcLevelRequired"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }
    }

    /// <summary>Gets the time the entity was last updated</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime UpdatedOn
    {
      get { return _updatedOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.EmployerView")]
  public partial class EmployerView : Entity<long>
  {
    #region Fields
  
    private long _focusBusinessUnitId;
    [ValidatePresence]
    [ValidateLength(0, 400)]
    private string _name;
    private double _latitudeRadians;
    private double _longitudeRadians;
    [ValidatePresence]
    [ValidateLength(0, 72)]
    private string _federalEmployerIdentificationNumber;
    private System.Nullable<Focus.Core.WorkOpportunitiesTaxCreditCategories> _workOpportunitiesTaxCreditHires;
    private System.Nullable<bool> _foreignLabourCertification;
    private System.Nullable<Focus.Core.ForeignLaborTypes> _foreignLabourType;
    private System.Nullable<bool> _federalContractor;
    private System.Nullable<bool> _courtOrderedAffirmativeAction;
    private System.Nullable<long> _countyId;
    [ValidateLength(0, 400)]
    private string _county;
    private System.Nullable<long> _stateId;
    [ValidateLength(0, 400)]
    private string _state;
    [ValidateLength(0, 40)]
    private string _postalCode;
    [ValidateLength(0, 400)]
    private string _office;
    [ValidatePresence]
    [ValidateLength(0, 400)]
    private string _jobTitle;
    private System.Nullable<decimal> _minSalary;
    private System.Nullable<decimal> _maxSalary;
    private System.Nullable<Focus.Core.ReportSalaryFrequency> _salaryFrequency;
    private System.Nullable<Focus.Core.JobStatuses> _jobStatus;
    private System.Nullable<Focus.Core.EducationLevels> _minimumEducationLevel;
    private System.Nullable<long> _focusEmployerId;
    private string _jobDescription;
    private System.Nullable<long> _noOfEmployees;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the FocusBusinessUnitId entity attribute.</summary>
    public const string FocusBusinessUnitIdField = "FocusBusinessUnitId";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the LatitudeRadians entity attribute.</summary>
    public const string LatitudeRadiansField = "LatitudeRadians";
    /// <summary>Identifies the LongitudeRadians entity attribute.</summary>
    public const string LongitudeRadiansField = "LongitudeRadians";
    /// <summary>Identifies the FederalEmployerIdentificationNumber entity attribute.</summary>
    public const string FederalEmployerIdentificationNumberField = "FederalEmployerIdentificationNumber";
    /// <summary>Identifies the WorkOpportunitiesTaxCreditHires entity attribute.</summary>
    public const string WorkOpportunitiesTaxCreditHiresField = "WorkOpportunitiesTaxCreditHires";
    /// <summary>Identifies the ForeignLabourCertification entity attribute.</summary>
    public const string ForeignLabourCertificationField = "ForeignLabourCertification";
    /// <summary>Identifies the ForeignLabourType entity attribute.</summary>
    public const string ForeignLabourTypeField = "ForeignLabourType";
    /// <summary>Identifies the FederalContractor entity attribute.</summary>
    public const string FederalContractorField = "FederalContractor";
    /// <summary>Identifies the CourtOrderedAffirmativeAction entity attribute.</summary>
    public const string CourtOrderedAffirmativeActionField = "CourtOrderedAffirmativeAction";
    /// <summary>Identifies the CountyId entity attribute.</summary>
    public const string CountyIdField = "CountyId";
    /// <summary>Identifies the County entity attribute.</summary>
    public const string CountyField = "County";
    /// <summary>Identifies the StateId entity attribute.</summary>
    public const string StateIdField = "StateId";
    /// <summary>Identifies the State entity attribute.</summary>
    public const string StateField = "State";
    /// <summary>Identifies the PostalCode entity attribute.</summary>
    public const string PostalCodeField = "PostalCode";
    /// <summary>Identifies the Office entity attribute.</summary>
    public const string OfficeField = "Office";
    /// <summary>Identifies the JobTitle entity attribute.</summary>
    public const string JobTitleField = "JobTitle";
    /// <summary>Identifies the MinSalary entity attribute.</summary>
    public const string MinSalaryField = "MinSalary";
    /// <summary>Identifies the MaxSalary entity attribute.</summary>
    public const string MaxSalaryField = "MaxSalary";
    /// <summary>Identifies the SalaryFrequency entity attribute.</summary>
    public const string SalaryFrequencyField = "SalaryFrequency";
    /// <summary>Identifies the JobStatus entity attribute.</summary>
    public const string JobStatusField = "JobStatus";
    /// <summary>Identifies the MinimumEducationLevel entity attribute.</summary>
    public const string MinimumEducationLevelField = "MinimumEducationLevel";
    /// <summary>Identifies the FocusEmployerId entity attribute.</summary>
    public const string FocusEmployerIdField = "FocusEmployerId";
    /// <summary>Identifies the JobDescription entity attribute.</summary>
    public const string JobDescriptionField = "JobDescription";
    /// <summary>Identifies the NoOfEmployees entity attribute.</summary>
    public const string NoOfEmployeesField = "NoOfEmployees";


    #endregion
    
    #region Relationships

    [ReverseAssociation("EmployerView")]
    private readonly EntityCollection<EmployerOffice> _employerOffices = new EntityCollection<EmployerOffice>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<EmployerOffice> EmployerOffices
    {
      get { return Get(_employerOffices); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public long FocusBusinessUnitId
    {
      get { return Get(ref _focusBusinessUnitId, "FocusBusinessUnitId"); }
      set { Set(ref _focusBusinessUnitId, value, "FocusBusinessUnitId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public double LatitudeRadians
    {
      get { return Get(ref _latitudeRadians, "LatitudeRadians"); }
      set { Set(ref _latitudeRadians, value, "LatitudeRadians"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public double LongitudeRadians
    {
      get { return Get(ref _longitudeRadians, "LongitudeRadians"); }
      set { Set(ref _longitudeRadians, value, "LongitudeRadians"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string FederalEmployerIdentificationNumber
    {
      get { return Get(ref _federalEmployerIdentificationNumber, "FederalEmployerIdentificationNumber"); }
      set { Set(ref _federalEmployerIdentificationNumber, value, "FederalEmployerIdentificationNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.WorkOpportunitiesTaxCreditCategories> WorkOpportunitiesTaxCreditHires
    {
      get { return Get(ref _workOpportunitiesTaxCreditHires, "WorkOpportunitiesTaxCreditHires"); }
      set { Set(ref _workOpportunitiesTaxCreditHires, value, "WorkOpportunitiesTaxCreditHires"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> ForeignLabourCertification
    {
      get { return Get(ref _foreignLabourCertification, "ForeignLabourCertification"); }
      set { Set(ref _foreignLabourCertification, value, "ForeignLabourCertification"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ForeignLaborTypes> ForeignLabourType
    {
      get { return Get(ref _foreignLabourType, "ForeignLabourType"); }
      set { Set(ref _foreignLabourType, value, "ForeignLabourType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> FederalContractor
    {
      get { return Get(ref _federalContractor, "FederalContractor"); }
      set { Set(ref _federalContractor, value, "FederalContractor"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> CourtOrderedAffirmativeAction
    {
      get { return Get(ref _courtOrderedAffirmativeAction, "CourtOrderedAffirmativeAction"); }
      set { Set(ref _courtOrderedAffirmativeAction, value, "CourtOrderedAffirmativeAction"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> CountyId
    {
      get { return Get(ref _countyId, "CountyId"); }
      set { Set(ref _countyId, value, "CountyId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string County
    {
      get { return Get(ref _county, "County"); }
      set { Set(ref _county, value, "County"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> StateId
    {
      get { return Get(ref _stateId, "StateId"); }
      set { Set(ref _stateId, value, "StateId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string State
    {
      get { return Get(ref _state, "State"); }
      set { Set(ref _state, value, "State"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PostalCode
    {
      get { return Get(ref _postalCode, "PostalCode"); }
      set { Set(ref _postalCode, value, "PostalCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Office
    {
      get { return Get(ref _office, "Office"); }
      set { Set(ref _office, value, "Office"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string JobTitle
    {
      get { return Get(ref _jobTitle, "JobTitle"); }
      set { Set(ref _jobTitle, value, "JobTitle"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> MinSalary
    {
      get { return Get(ref _minSalary, "MinSalary"); }
      set { Set(ref _minSalary, value, "MinSalary"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> MaxSalary
    {
      get { return Get(ref _maxSalary, "MaxSalary"); }
      set { Set(ref _maxSalary, value, "MaxSalary"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ReportSalaryFrequency> SalaryFrequency
    {
      get { return Get(ref _salaryFrequency, "SalaryFrequency"); }
      set { Set(ref _salaryFrequency, value, "SalaryFrequency"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.JobStatuses> JobStatus
    {
      get { return Get(ref _jobStatus, "JobStatus"); }
      set { Set(ref _jobStatus, value, "JobStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.EducationLevels> MinimumEducationLevel
    {
      get { return Get(ref _minimumEducationLevel, "MinimumEducationLevel"); }
      set { Set(ref _minimumEducationLevel, value, "MinimumEducationLevel"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> FocusEmployerId
    {
      get { return Get(ref _focusEmployerId, "FocusEmployerId"); }
      set { Set(ref _focusEmployerId, value, "FocusEmployerId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string JobDescription
    {
      get { return Get(ref _jobDescription, "JobDescription"); }
      set { Set(ref _jobDescription, value, "JobDescription"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> NoOfEmployees
    {
      get { return Get(ref _noOfEmployees, "NoOfEmployees"); }
      set { Set(ref _noOfEmployees, value, "NoOfEmployees"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.LookupEmployerFEINView")]
  public partial class LookupEmployerFEINView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 72)]
    private string _federalEmployerIdentificationNumber;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the FederalEmployerIdentificationNumber entity attribute.</summary>
    public const string FederalEmployerIdentificationNumberField = "FederalEmployerIdentificationNumber";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string FederalEmployerIdentificationNumber
    {
      get { return Get(ref _federalEmployerIdentificationNumber, "FederalEmployerIdentificationNumber"); }
      set { Set(ref _federalEmployerIdentificationNumber, value, "FederalEmployerIdentificationNumber"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.LookupEmployerNameView")]
  public partial class LookupEmployerNameView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 400)]
    private string _name;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.LookupJobOrderTitleView")]
  public partial class LookupJobOrderTitleView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 400)]
    private string _jobTitle;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobTitle entity attribute.</summary>
    public const string JobTitleField = "JobTitle";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string JobTitle
    {
      get { return Get(ref _jobTitle, "JobTitle"); }
      set { Set(ref _jobTitle, value, "JobTitle"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class JobSeekerActivityAssignmentsTotal : Entity<long>
  {
    #region Fields
  
    private int _assignmentsMade;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the AssignmentsMade entity attribute.</summary>
    public const string AssignmentsMadeField = "AssignmentsMade";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public int AssignmentsMade
    {
      get { return Get(ref _assignmentsMade, "AssignmentsMade"); }
      set { Set(ref _assignmentsMade, value, "AssignmentsMade"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.JobSeekerOffice", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeekerOffice : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _officeName;
    private System.Nullable<long> _officeId;
    private System.Nullable<System.DateTime> _assignedDate;
    private long _jobSeekerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the OfficeName entity attribute.</summary>
    public const string OfficeNameField = "OfficeName";
    /// <summary>Identifies the OfficeId entity attribute.</summary>
    public const string OfficeIdField = "OfficeId";
    /// <summary>Identifies the AssignedDate entity attribute.</summary>
    public const string AssignedDateField = "AssignedDate";
    /// <summary>Identifies the JobSeekerId entity attribute.</summary>
    public const string JobSeekerIdField = "JobSeekerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSeekerOffices")]
    private readonly EntityHolder<JobSeeker> _jobSeeker = new EntityHolder<JobSeeker>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public JobSeeker JobSeeker
    {
      get { return Get(_jobSeeker); }
      set { Set(_jobSeeker, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string OfficeName
    {
      get { return Get(ref _officeName, "OfficeName"); }
      set { Set(ref _officeName, value, "OfficeName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> OfficeId
    {
      get { return Get(ref _officeId, "OfficeId"); }
      set { Set(ref _officeId, value, "OfficeId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> AssignedDate
    {
      get { return Get(ref _assignedDate, "AssignedDate"); }
      set { Set(ref _assignedDate, value, "AssignedDate"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobSeeker" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobSeekerId
    {
      get { return Get(ref _jobSeekerId, "JobSeekerId"); }
      set { Set(ref _jobSeekerId, value, "JobSeekerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.JobOrderOffice", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobOrderOffice : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _officeName;
    private System.Nullable<long> _officeId;
    private long _jobOrderId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the OfficeName entity attribute.</summary>
    public const string OfficeNameField = "OfficeName";
    /// <summary>Identifies the OfficeId entity attribute.</summary>
    public const string OfficeIdField = "OfficeId";
    /// <summary>Identifies the JobOrderId entity attribute.</summary>
    public const string JobOrderIdField = "JobOrderId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobOrderOffices")]
    private readonly EntityHolder<JobOrder> _jobOrder = new EntityHolder<JobOrder>();
    [ForeignKeyField("JobOrderId")]
    [ReverseAssociation("JobOrderOffices")]
    private readonly EntityHolder<JobOrderView> _jobOrderView = new EntityHolder<JobOrderView>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public JobOrder JobOrder
    {
      get { return Get(_jobOrder); }
      set { Set(_jobOrder, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public JobOrderView JobOrderView
    {
      get { return Get(_jobOrderView); }
      set { Set(_jobOrderView, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string OfficeName
    {
      get { return Get(ref _officeName, "OfficeName"); }
      set { Set(ref _officeName, value, "OfficeName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> OfficeId
    {
      get { return Get(ref _officeId, "OfficeId"); }
      set { Set(ref _officeId, value, "OfficeId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobOrder" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobOrderId
    {
      get { return Get(ref _jobOrderId, "JobOrderId"); }
      set { Set(ref _jobOrderId, value, "JobOrderId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.EmployerOffice", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class EmployerOffice : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _officeName;
    private System.Nullable<long> _officeId;
    private long _employerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the OfficeName entity attribute.</summary>
    public const string OfficeNameField = "OfficeName";
    /// <summary>Identifies the OfficeId entity attribute.</summary>
    public const string OfficeIdField = "OfficeId";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("EmployerOffices")]
    private readonly EntityHolder<Employer> _employer = new EntityHolder<Employer>();
    [ForeignKeyField("EmployerId")]
    [ReverseAssociation("EmployerOffices")]
    private readonly EntityHolder<EmployerView> _employerView = new EntityHolder<EmployerView>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Employer Employer
    {
      get { return Get(_employer); }
      set { Set(_employer, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EmployerView EmployerView
    {
      get { return Get(_employerView); }
      set { Set(_employerView, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string OfficeName
    {
      get { return Get(ref _officeName, "OfficeName"); }
      set { Set(ref _officeName, value, "OfficeName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> OfficeId
    {
      get { return Get(ref _officeId, "OfficeId"); }
      set { Set(ref _officeId, value, "OfficeId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Employer" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.JobSeekerJobHistory", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeekerJobHistory : Entity<long>
  {
    #region Fields
  
    private string _employerName;
    private System.Nullable<System.DateTime> _employmentStartDate;
    private System.Nullable<System.DateTime> _employmentEndDate;
    private string _jobTitle;
    private string _employerCity;
    private string _employerState;
    private long _jobSeekerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the EmployerName entity attribute.</summary>
    public const string EmployerNameField = "EmployerName";
    /// <summary>Identifies the EmploymentStartDate entity attribute.</summary>
    public const string EmploymentStartDateField = "EmploymentStartDate";
    /// <summary>Identifies the EmploymentEndDate entity attribute.</summary>
    public const string EmploymentEndDateField = "EmploymentEndDate";
    /// <summary>Identifies the JobTitle entity attribute.</summary>
    public const string JobTitleField = "JobTitle";
    /// <summary>Identifies the EmployerCity entity attribute.</summary>
    public const string EmployerCityField = "EmployerCity";
    /// <summary>Identifies the EmployerState entity attribute.</summary>
    public const string EmployerStateField = "EmployerState";
    /// <summary>Identifies the JobSeekerId entity attribute.</summary>
    public const string JobSeekerIdField = "JobSeekerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSeekerJobHistories")]
    private readonly EntityHolder<JobSeeker> _jobSeeker = new EntityHolder<JobSeeker>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public JobSeeker JobSeeker
    {
      get { return Get(_jobSeeker); }
      set { Set(_jobSeeker, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerName
    {
      get { return Get(ref _employerName, "EmployerName"); }
      set { Set(ref _employerName, value, "EmployerName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> EmploymentStartDate
    {
      get { return Get(ref _employmentStartDate, "EmploymentStartDate"); }
      set { Set(ref _employmentStartDate, value, "EmploymentStartDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> EmploymentEndDate
    {
      get { return Get(ref _employmentEndDate, "EmploymentEndDate"); }
      set { Set(ref _employmentEndDate, value, "EmploymentEndDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string JobTitle
    {
      get { return Get(ref _jobTitle, "JobTitle"); }
      set { Set(ref _jobTitle, value, "JobTitle"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerCity
    {
      get { return Get(ref _employerCity, "EmployerCity"); }
      set { Set(ref _employerCity, value, "EmployerCity"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerState
    {
      get { return Get(ref _employerState, "EmployerState"); }
      set { Set(ref _employerState, value, "EmployerState"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobSeeker" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobSeekerId
    {
      get { return Get(ref _jobSeekerId, "JobSeekerId"); }
      set { Set(ref _jobSeekerId, value, "JobSeekerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.StaffMember", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class StaffMember : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 100)]
    private string _firstName;
    [ValidateLength(0, 100)]
    private string _lastName;
    private System.Nullable<long> _countyId;
    [ValidateLength(0, 400)]
    private string _county;
    private System.Nullable<long> _stateId;
    [ValidateLength(0, 400)]
    private string _state;
    [ValidateLength(0, 40)]
    private string _postalCode;
    [ValidateLength(0, 2000)]
    private string _office;
    private long _focusPersonId;
    private System.DateTime _accountCreationDate;
    private double _latitudeRadians;
    private double _longitudeRadians;
    [ValidateLength(0, 200)]
    private string _addressLine1;
    [ValidatePresence]
    [ValidateEmailAddress]
    [ValidateLength(0, 500)]
    private string _emailAddress;
    private bool _blocked;
    [ValidateLength(0, 255)]
    private string _externalStaffId;
    private System.Nullable<bool> _localVeteranEmploymentRepresentative;
    private System.Nullable<bool> _disabledVeteransOutreachProgramSpecialist;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    private readonly System.DateTime _updatedOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the FirstName entity attribute.</summary>
    public const string FirstNameField = "FirstName";
    /// <summary>Identifies the LastName entity attribute.</summary>
    public const string LastNameField = "LastName";
    /// <summary>Identifies the CountyId entity attribute.</summary>
    public const string CountyIdField = "CountyId";
    /// <summary>Identifies the County entity attribute.</summary>
    public const string CountyField = "County";
    /// <summary>Identifies the StateId entity attribute.</summary>
    public const string StateIdField = "StateId";
    /// <summary>Identifies the State entity attribute.</summary>
    public const string StateField = "State";
    /// <summary>Identifies the PostalCode entity attribute.</summary>
    public const string PostalCodeField = "PostalCode";
    /// <summary>Identifies the Office entity attribute.</summary>
    public const string OfficeField = "Office";
    /// <summary>Identifies the FocusPersonId entity attribute.</summary>
    public const string FocusPersonIdField = "FocusPersonId";
    /// <summary>Identifies the AccountCreationDate entity attribute.</summary>
    public const string AccountCreationDateField = "AccountCreationDate";
    /// <summary>Identifies the LatitudeRadians entity attribute.</summary>
    public const string LatitudeRadiansField = "LatitudeRadians";
    /// <summary>Identifies the LongitudeRadians entity attribute.</summary>
    public const string LongitudeRadiansField = "LongitudeRadians";
    /// <summary>Identifies the AddressLine1 entity attribute.</summary>
    public const string AddressLine1Field = "AddressLine1";
    /// <summary>Identifies the EmailAddress entity attribute.</summary>
    public const string EmailAddressField = "EmailAddress";
    /// <summary>Identifies the Blocked entity attribute.</summary>
    public const string BlockedField = "Blocked";
    /// <summary>Identifies the ExternalStaffId entity attribute.</summary>
    public const string ExternalStaffIdField = "ExternalStaffId";
    /// <summary>Identifies the LocalVeteranEmploymentRepresentative entity attribute.</summary>
    public const string LocalVeteranEmploymentRepresentativeField = "LocalVeteranEmploymentRepresentative";
    /// <summary>Identifies the DisabledVeteransOutreachProgramSpecialist entity attribute.</summary>
    public const string DisabledVeteransOutreachProgramSpecialistField = "DisabledVeteransOutreachProgramSpecialist";


    #endregion
    
    #region Relationships

    [ReverseAssociation("StaffMember")]
    private readonly EntityCollection<StaffMemberOffice> _staffMemberOffices = new EntityCollection<StaffMemberOffice>();
    [ReverseAssociation("StaffMember")]
    private readonly EntityCollection<StaffMemberCurrentOffice> _staffMemberCurrentOffices = new EntityCollection<StaffMemberCurrentOffice>();
    [ReverseAssociation("StaffMember")]
    private readonly EntityCollection<JobSeekerActivityLog> _jobSeekerActivityLogs = new EntityCollection<JobSeekerActivityLog>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<StaffMemberOffice> StaffMemberOffices
    {
      get { return Get(_staffMemberOffices); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<StaffMemberCurrentOffice> StaffMemberCurrentOffices
    {
      get { return Get(_staffMemberCurrentOffices); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSeekerActivityLog> JobSeekerActivityLogs
    {
      get { return Get(_jobSeekerActivityLogs); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string FirstName
    {
      get { return Get(ref _firstName, "FirstName"); }
      set { Set(ref _firstName, value, "FirstName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LastName
    {
      get { return Get(ref _lastName, "LastName"); }
      set { Set(ref _lastName, value, "LastName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> CountyId
    {
      get { return Get(ref _countyId, "CountyId"); }
      set { Set(ref _countyId, value, "CountyId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string County
    {
      get { return Get(ref _county, "County"); }
      set { Set(ref _county, value, "County"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> StateId
    {
      get { return Get(ref _stateId, "StateId"); }
      set { Set(ref _stateId, value, "StateId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string State
    {
      get { return Get(ref _state, "State"); }
      set { Set(ref _state, value, "State"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PostalCode
    {
      get { return Get(ref _postalCode, "PostalCode"); }
      set { Set(ref _postalCode, value, "PostalCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Office
    {
      get { return Get(ref _office, "Office"); }
      set { Set(ref _office, value, "Office"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long FocusPersonId
    {
      get { return Get(ref _focusPersonId, "FocusPersonId"); }
      set { Set(ref _focusPersonId, value, "FocusPersonId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime AccountCreationDate
    {
      get { return Get(ref _accountCreationDate, "AccountCreationDate"); }
      set { Set(ref _accountCreationDate, value, "AccountCreationDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public double LatitudeRadians
    {
      get { return Get(ref _latitudeRadians, "LatitudeRadians"); }
      set { Set(ref _latitudeRadians, value, "LatitudeRadians"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public double LongitudeRadians
    {
      get { return Get(ref _longitudeRadians, "LongitudeRadians"); }
      set { Set(ref _longitudeRadians, value, "LongitudeRadians"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AddressLine1
    {
      get { return Get(ref _addressLine1, "AddressLine1"); }
      set { Set(ref _addressLine1, value, "AddressLine1"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmailAddress
    {
      get { return Get(ref _emailAddress, "EmailAddress"); }
      set { Set(ref _emailAddress, value, "EmailAddress"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool Blocked
    {
      get { return Get(ref _blocked, "Blocked"); }
      set { Set(ref _blocked, value, "Blocked"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalStaffId
    {
      get { return Get(ref _externalStaffId, "ExternalStaffId"); }
      set { Set(ref _externalStaffId, value, "ExternalStaffId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> LocalVeteranEmploymentRepresentative
    {
      get { return Get(ref _localVeteranEmploymentRepresentative, "LocalVeteranEmploymentRepresentative"); }
      set { Set(ref _localVeteranEmploymentRepresentative, value, "LocalVeteranEmploymentRepresentative"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> DisabledVeteransOutreachProgramSpecialist
    {
      get { return Get(ref _disabledVeteransOutreachProgramSpecialist, "DisabledVeteransOutreachProgramSpecialist"); }
      set { Set(ref _disabledVeteransOutreachProgramSpecialist, value, "DisabledVeteransOutreachProgramSpecialist"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }
    }

    /// <summary>Gets the time the entity was last updated</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime UpdatedOn
    {
      get { return _updatedOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.StaffMemberOffice", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class StaffMemberOffice : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _officeName;
    private System.Nullable<long> _officeId;
    private System.Nullable<System.DateTime> _assignedDate;
    private long _staffMemberId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the OfficeName entity attribute.</summary>
    public const string OfficeNameField = "OfficeName";
    /// <summary>Identifies the OfficeId entity attribute.</summary>
    public const string OfficeIdField = "OfficeId";
    /// <summary>Identifies the AssignedDate entity attribute.</summary>
    public const string AssignedDateField = "AssignedDate";
    /// <summary>Identifies the StaffMemberId entity attribute.</summary>
    public const string StaffMemberIdField = "StaffMemberId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("StaffMemberOffices")]
    private readonly EntityHolder<StaffMember> _staffMember = new EntityHolder<StaffMember>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public StaffMember StaffMember
    {
      get { return Get(_staffMember); }
      set { Set(_staffMember, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string OfficeName
    {
      get { return Get(ref _officeName, "OfficeName"); }
      set { Set(ref _officeName, value, "OfficeName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> OfficeId
    {
      get { return Get(ref _officeId, "OfficeId"); }
      set { Set(ref _officeId, value, "OfficeId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> AssignedDate
    {
      get { return Get(ref _assignedDate, "AssignedDate"); }
      set { Set(ref _assignedDate, value, "AssignedDate"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="StaffMember" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long StaffMemberId
    {
      get { return Get(ref _staffMemberId, "StaffMemberId"); }
      set { Set(ref _staffMemberId, value, "StaffMemberId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.StaffMemberCurrentOffice", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class StaffMemberCurrentOffice : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _officeName;
    private System.Nullable<long> _officeId;
    private System.Nullable<System.DateTime> _startDate;
    private long _staffMemberId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the OfficeName entity attribute.</summary>
    public const string OfficeNameField = "OfficeName";
    /// <summary>Identifies the OfficeId entity attribute.</summary>
    public const string OfficeIdField = "OfficeId";
    /// <summary>Identifies the StartDate entity attribute.</summary>
    public const string StartDateField = "StartDate";
    /// <summary>Identifies the StaffMemberId entity attribute.</summary>
    public const string StaffMemberIdField = "StaffMemberId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("StaffMemberCurrentOffices")]
    private readonly EntityHolder<StaffMember> _staffMember = new EntityHolder<StaffMember>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public StaffMember StaffMember
    {
      get { return Get(_staffMember); }
      set { Set(_staffMember, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string OfficeName
    {
      get { return Get(ref _officeName, "OfficeName"); }
      set { Set(ref _officeName, value, "OfficeName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> OfficeId
    {
      get { return Get(ref _officeId, "OfficeId"); }
      set { Set(ref _officeId, value, "OfficeId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> StartDate
    {
      get { return Get(ref _startDate, "StartDate"); }
      set { Set(ref _startDate, value, "StartDate"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="StaffMember" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long StaffMemberId
    {
      get { return Get(ref _staffMemberId, "StaffMemberId"); }
      set { Set(ref _staffMemberId, value, "StaffMemberId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.JobSeekerActivityLog", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeekerActivityLog : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _actionTypeName;
    private System.DateTime _actionDate;
    private long _focusUserId;
    [ValidateLength(0, 255)]
    private string _staffExternalId;
    private System.Nullable<long> _officeId;
    [ValidateLength(0, 100)]
    private string _officeName;
    private long _jobSeekerId;
    private System.Nullable<long> _staffMemberId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ActionTypeName entity attribute.</summary>
    public const string ActionTypeNameField = "ActionTypeName";
    /// <summary>Identifies the ActionDate entity attribute.</summary>
    public const string ActionDateField = "ActionDate";
    /// <summary>Identifies the FocusUserId entity attribute.</summary>
    public const string FocusUserIdField = "FocusUserId";
    /// <summary>Identifies the StaffExternalId entity attribute.</summary>
    public const string StaffExternalIdField = "StaffExternalId";
    /// <summary>Identifies the OfficeId entity attribute.</summary>
    public const string OfficeIdField = "OfficeId";
    /// <summary>Identifies the OfficeName entity attribute.</summary>
    public const string OfficeNameField = "OfficeName";
    /// <summary>Identifies the JobSeekerId entity attribute.</summary>
    public const string JobSeekerIdField = "JobSeekerId";
    /// <summary>Identifies the StaffMemberId entity attribute.</summary>
    public const string StaffMemberIdField = "StaffMemberId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSeekerActivityLogs")]
    private readonly EntityHolder<JobSeeker> _jobSeeker = new EntityHolder<JobSeeker>();
    [ReverseAssociation("JobSeekerActivityLogs")]
    private readonly EntityHolder<StaffMember> _staffMember = new EntityHolder<StaffMember>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public JobSeeker JobSeeker
    {
      get { return Get(_jobSeeker); }
      set { Set(_jobSeeker, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public StaffMember StaffMember
    {
      get { return Get(_staffMember); }
      set { Set(_staffMember, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string ActionTypeName
    {
      get { return Get(ref _actionTypeName, "ActionTypeName"); }
      set { Set(ref _actionTypeName, value, "ActionTypeName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ActionDate
    {
      get { return Get(ref _actionDate, "ActionDate"); }
      set { Set(ref _actionDate, value, "ActionDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long FocusUserId
    {
      get { return Get(ref _focusUserId, "FocusUserId"); }
      set { Set(ref _focusUserId, value, "FocusUserId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string StaffExternalId
    {
      get { return Get(ref _staffExternalId, "StaffExternalId"); }
      set { Set(ref _staffExternalId, value, "StaffExternalId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> OfficeId
    {
      get { return Get(ref _officeId, "OfficeId"); }
      set { Set(ref _officeId, value, "OfficeId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string OfficeName
    {
      get { return Get(ref _officeName, "OfficeName"); }
      set { Set(ref _officeName, value, "OfficeName"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobSeeker" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobSeekerId
    {
      get { return Get(ref _jobSeekerId, "JobSeekerId"); }
      set { Set(ref _jobSeekerId, value, "JobSeekerId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="StaffMember" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> StaffMemberId
    {
      get { return Get(ref _staffMemberId, "StaffMemberId"); }
      set { Set(ref _staffMemberId, value, "StaffMemberId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.Statistic")]
  public partial class Statistic : Entity<long>
  {
    #region Fields
  
    private Focus.Core.StatisticType _statisticType;
    private string _value;
    private System.DateTime _createdOn;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the StatisticType entity attribute.</summary>
    public const string StatisticTypeField = "StatisticType";
    /// <summary>Identifies the Value entity attribute.</summary>
    public const string ValueField = "Value";
    /// <summary>Identifies the CreatedOn entity attribute.</summary>
    public const string CreatedOnField = "CreatedOn";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.StatisticType StatisticType
    {
      get { return Get(ref _statisticType, "StatisticType"); }
      set { Set(ref _statisticType, value, "StatisticType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Value
    {
      get { return Get(ref _value, "Value"); }
      set { Set(ref _value, value, "Value"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return Get(ref _createdOn, "CreatedOn"); }
      set { Set(ref _createdOn, value, "CreatedOn"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.JobSeekerMilitaryHistory", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeekerMilitaryHistory : Entity<long>
  {
    #region Fields
  
    private System.Nullable<Focus.Core.Models.Career.VeteranEra> _veteranType;
    private System.Nullable<Focus.Core.Models.Career.VeteranDisabilityStatus> _veteranDisability;
    private System.Nullable<Focus.Core.Models.Career.VeteranTransitionType> _veteranTransitionType;
    private System.Nullable<Focus.Core.ReportMilitaryDischargeType> _veteranMilitaryDischarge;
    private System.Nullable<Focus.Core.ReportMilitaryBranchOfService> _veteranBranchOfService;
    private System.Nullable<bool> _campaignVeteran;
    private System.Nullable<System.DateTime> _militaryStartDate;
    private System.Nullable<System.DateTime> _militaryEndDate;
    private string _veteranRank;
    private long _jobSeekerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the VeteranType entity attribute.</summary>
    public const string VeteranTypeField = "VeteranType";
    /// <summary>Identifies the VeteranDisability entity attribute.</summary>
    public const string VeteranDisabilityField = "VeteranDisability";
    /// <summary>Identifies the VeteranTransitionType entity attribute.</summary>
    public const string VeteranTransitionTypeField = "VeteranTransitionType";
    /// <summary>Identifies the VeteranMilitaryDischarge entity attribute.</summary>
    public const string VeteranMilitaryDischargeField = "VeteranMilitaryDischarge";
    /// <summary>Identifies the VeteranBranchOfService entity attribute.</summary>
    public const string VeteranBranchOfServiceField = "VeteranBranchOfService";
    /// <summary>Identifies the CampaignVeteran entity attribute.</summary>
    public const string CampaignVeteranField = "CampaignVeteran";
    /// <summary>Identifies the MilitaryStartDate entity attribute.</summary>
    public const string MilitaryStartDateField = "MilitaryStartDate";
    /// <summary>Identifies the MilitaryEndDate entity attribute.</summary>
    public const string MilitaryEndDateField = "MilitaryEndDate";
    /// <summary>Identifies the VeteranRank entity attribute.</summary>
    public const string VeteranRankField = "VeteranRank";
    /// <summary>Identifies the JobSeekerId entity attribute.</summary>
    public const string JobSeekerIdField = "JobSeekerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSeekerMilitaryHistories")]
    private readonly EntityHolder<JobSeeker> _jobSeeker = new EntityHolder<JobSeeker>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public JobSeeker JobSeeker
    {
      get { return Get(_jobSeeker); }
      set { Set(_jobSeeker, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.Models.Career.VeteranEra> VeteranType
    {
      get { return Get(ref _veteranType, "VeteranType"); }
      set { Set(ref _veteranType, value, "VeteranType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.Models.Career.VeteranDisabilityStatus> VeteranDisability
    {
      get { return Get(ref _veteranDisability, "VeteranDisability"); }
      set { Set(ref _veteranDisability, value, "VeteranDisability"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.Models.Career.VeteranTransitionType> VeteranTransitionType
    {
      get { return Get(ref _veteranTransitionType, "VeteranTransitionType"); }
      set { Set(ref _veteranTransitionType, value, "VeteranTransitionType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ReportMilitaryDischargeType> VeteranMilitaryDischarge
    {
      get { return Get(ref _veteranMilitaryDischarge, "VeteranMilitaryDischarge"); }
      set { Set(ref _veteranMilitaryDischarge, value, "VeteranMilitaryDischarge"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ReportMilitaryBranchOfService> VeteranBranchOfService
    {
      get { return Get(ref _veteranBranchOfService, "VeteranBranchOfService"); }
      set { Set(ref _veteranBranchOfService, value, "VeteranBranchOfService"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> CampaignVeteran
    {
      get { return Get(ref _campaignVeteran, "CampaignVeteran"); }
      set { Set(ref _campaignVeteran, value, "CampaignVeteran"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> MilitaryStartDate
    {
      get { return Get(ref _militaryStartDate, "MilitaryStartDate"); }
      set { Set(ref _militaryStartDate, value, "MilitaryStartDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> MilitaryEndDate
    {
      get { return Get(ref _militaryEndDate, "MilitaryEndDate"); }
      set { Set(ref _militaryEndDate, value, "MilitaryEndDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string VeteranRank
    {
      get { return Get(ref _veteranRank, "VeteranRank"); }
      set { Set(ref _veteranRank, value, "VeteranRank"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobSeeker" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobSeekerId
    {
      get { return Get(ref _jobSeekerId, "JobSeekerId"); }
      set { Set(ref _jobSeekerId, value, "JobSeekerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.JobSeeker_NonPurged", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeeker_NonPurged : Entity<long>
  {
    #region Fields
  
    private long _jobSeekerReportId;
    private System.Nullable<System.DateTime> _dateOfBirth;
    private System.Nullable<Focus.Core.Genders> _gender;
    private System.Nullable<Focus.Core.Models.Career.DisabilityStatus> _disabilityStatus;
    private System.Nullable<Focus.Core.ReportDisabilityType> _disabilityType;
    private System.Nullable<Focus.Core.ReportEthnicHeritage> _ethnicHeritage;
    private System.Nullable<Focus.Core.ReportRace> _race;
    private System.Nullable<Focus.Core.EducationLevel> _minimumEducationLevel;
    private System.Nullable<Focus.Core.SchoolStatus> _enrollmentStatus;
    private System.Nullable<Focus.Core.Models.Career.EmploymentStatus> _employmentStatus;
    private System.Nullable<short> _age;
    private System.Nullable<bool> _isCitizen;
    private System.Nullable<System.DateTime> _alienCardExpires;
    private string _alienCardId;
    private System.Nullable<long> _countyId;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    private readonly System.DateTime _updatedOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobSeekerReportId entity attribute.</summary>
    public const string JobSeekerReportIdField = "JobSeekerReportId";
    /// <summary>Identifies the DateOfBirth entity attribute.</summary>
    public const string DateOfBirthField = "DateOfBirth";
    /// <summary>Identifies the Gender entity attribute.</summary>
    public const string GenderField = "Gender";
    /// <summary>Identifies the DisabilityStatus entity attribute.</summary>
    public const string DisabilityStatusField = "DisabilityStatus";
    /// <summary>Identifies the DisabilityType entity attribute.</summary>
    public const string DisabilityTypeField = "DisabilityType";
    /// <summary>Identifies the EthnicHeritage entity attribute.</summary>
    public const string EthnicHeritageField = "EthnicHeritage";
    /// <summary>Identifies the Race entity attribute.</summary>
    public const string RaceField = "Race";
    /// <summary>Identifies the MinimumEducationLevel entity attribute.</summary>
    public const string MinimumEducationLevelField = "MinimumEducationLevel";
    /// <summary>Identifies the EnrollmentStatus entity attribute.</summary>
    public const string EnrollmentStatusField = "EnrollmentStatus";
    /// <summary>Identifies the EmploymentStatus entity attribute.</summary>
    public const string EmploymentStatusField = "EmploymentStatus";
    /// <summary>Identifies the Age entity attribute.</summary>
    public const string AgeField = "Age";
    /// <summary>Identifies the IsCitizen entity attribute.</summary>
    public const string IsCitizenField = "IsCitizen";
    /// <summary>Identifies the AlienCardExpires entity attribute.</summary>
    public const string AlienCardExpiresField = "AlienCardExpires";
    /// <summary>Identifies the AlienCardId entity attribute.</summary>
    public const string AlienCardIdField = "AlienCardId";
    /// <summary>Identifies the CountyId entity attribute.</summary>
    public const string CountyIdField = "CountyId";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public long JobSeekerReportId
    {
      get { return Get(ref _jobSeekerReportId, "JobSeekerReportId"); }
      set { Set(ref _jobSeekerReportId, value, "JobSeekerReportId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> DateOfBirth
    {
      get { return Get(ref _dateOfBirth, "DateOfBirth"); }
      set { Set(ref _dateOfBirth, value, "DateOfBirth"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.Genders> Gender
    {
      get { return Get(ref _gender, "Gender"); }
      set { Set(ref _gender, value, "Gender"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.Models.Career.DisabilityStatus> DisabilityStatus
    {
      get { return Get(ref _disabilityStatus, "DisabilityStatus"); }
      set { Set(ref _disabilityStatus, value, "DisabilityStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ReportDisabilityType> DisabilityType
    {
      get { return Get(ref _disabilityType, "DisabilityType"); }
      set { Set(ref _disabilityType, value, "DisabilityType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ReportEthnicHeritage> EthnicHeritage
    {
      get { return Get(ref _ethnicHeritage, "EthnicHeritage"); }
      set { Set(ref _ethnicHeritage, value, "EthnicHeritage"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ReportRace> Race
    {
      get { return Get(ref _race, "Race"); }
      set { Set(ref _race, value, "Race"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.EducationLevel> MinimumEducationLevel
    {
      get { return Get(ref _minimumEducationLevel, "MinimumEducationLevel"); }
      set { Set(ref _minimumEducationLevel, value, "MinimumEducationLevel"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.SchoolStatus> EnrollmentStatus
    {
      get { return Get(ref _enrollmentStatus, "EnrollmentStatus"); }
      set { Set(ref _enrollmentStatus, value, "EnrollmentStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.Models.Career.EmploymentStatus> EmploymentStatus
    {
      get { return Get(ref _employmentStatus, "EmploymentStatus"); }
      set { Set(ref _employmentStatus, value, "EmploymentStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<short> Age
    {
      get { return Get(ref _age, "Age"); }
      set { Set(ref _age, value, "Age"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> IsCitizen
    {
      get { return Get(ref _isCitizen, "IsCitizen"); }
      set { Set(ref _isCitizen, value, "IsCitizen"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> AlienCardExpires
    {
      get { return Get(ref _alienCardExpires, "AlienCardExpires"); }
      set { Set(ref _alienCardExpires, value, "AlienCardExpires"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AlienCardId
    {
      get { return Get(ref _alienCardId, "AlienCardId"); }
      set { Set(ref _alienCardId, value, "AlienCardId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> CountyId
    {
      get { return Get(ref _countyId, "CountyId"); }
      set { Set(ref _countyId, value, "CountyId"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }
    }

    /// <summary>Gets the time the entity was last updated</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime UpdatedOn
    {
      get { return _updatedOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Report.JobSeekerMilitaryHistory_NonPurged", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeekerMilitaryHistory_NonPurged : Entity<long>
  {
    #region Fields
  
    private System.Nullable<Focus.Core.Models.Career.VeteranDisabilityStatus> _veteranDisability;
    private System.Nullable<Focus.Core.Models.Career.VeteranTransitionType> _veteranTransitionType;
    private System.Nullable<Focus.Core.ReportMilitaryDischargeType> _veteranMilitaryDischarge;
    private System.Nullable<Focus.Core.ReportMilitaryBranchOfService> _veteranBranchOfService;
    private System.Nullable<bool> _campaignVeteran;
    private System.Nullable<System.DateTime> _militaryStartDate;
    private System.Nullable<System.DateTime> _militaryEndDate;
    private System.Nullable<long> _jobSeekerId;
    private System.Nullable<Focus.Core.Models.Career.VeteranEra> _veteranType;
    private string _veteranRank;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    private readonly System.DateTime _updatedOn;
    private readonly System.Nullable<System.DateTime> _deletedOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the VeteranDisability entity attribute.</summary>
    public const string VeteranDisabilityField = "VeteranDisability";
    /// <summary>Identifies the VeteranTransitionType entity attribute.</summary>
    public const string VeteranTransitionTypeField = "VeteranTransitionType";
    /// <summary>Identifies the VeteranMilitaryDischarge entity attribute.</summary>
    public const string VeteranMilitaryDischargeField = "VeteranMilitaryDischarge";
    /// <summary>Identifies the VeteranBranchOfService entity attribute.</summary>
    public const string VeteranBranchOfServiceField = "VeteranBranchOfService";
    /// <summary>Identifies the CampaignVeteran entity attribute.</summary>
    public const string CampaignVeteranField = "CampaignVeteran";
    /// <summary>Identifies the MilitaryStartDate entity attribute.</summary>
    public const string MilitaryStartDateField = "MilitaryStartDate";
    /// <summary>Identifies the MilitaryEndDate entity attribute.</summary>
    public const string MilitaryEndDateField = "MilitaryEndDate";
    /// <summary>Identifies the JobSeekerId entity attribute.</summary>
    public const string JobSeekerIdField = "JobSeekerId";
    /// <summary>Identifies the VeteranType entity attribute.</summary>
    public const string VeteranTypeField = "VeteranType";
    /// <summary>Identifies the VeteranRank entity attribute.</summary>
    public const string VeteranRankField = "VeteranRank";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.Models.Career.VeteranDisabilityStatus> VeteranDisability
    {
      get { return Get(ref _veteranDisability, "VeteranDisability"); }
      set { Set(ref _veteranDisability, value, "VeteranDisability"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.Models.Career.VeteranTransitionType> VeteranTransitionType
    {
      get { return Get(ref _veteranTransitionType, "VeteranTransitionType"); }
      set { Set(ref _veteranTransitionType, value, "VeteranTransitionType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ReportMilitaryDischargeType> VeteranMilitaryDischarge
    {
      get { return Get(ref _veteranMilitaryDischarge, "VeteranMilitaryDischarge"); }
      set { Set(ref _veteranMilitaryDischarge, value, "VeteranMilitaryDischarge"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ReportMilitaryBranchOfService> VeteranBranchOfService
    {
      get { return Get(ref _veteranBranchOfService, "VeteranBranchOfService"); }
      set { Set(ref _veteranBranchOfService, value, "VeteranBranchOfService"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> CampaignVeteran
    {
      get { return Get(ref _campaignVeteran, "CampaignVeteran"); }
      set { Set(ref _campaignVeteran, value, "CampaignVeteran"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> MilitaryStartDate
    {
      get { return Get(ref _militaryStartDate, "MilitaryStartDate"); }
      set { Set(ref _militaryStartDate, value, "MilitaryStartDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> MilitaryEndDate
    {
      get { return Get(ref _militaryEndDate, "MilitaryEndDate"); }
      set { Set(ref _militaryEndDate, value, "MilitaryEndDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> JobSeekerId
    {
      get { return Get(ref _jobSeekerId, "JobSeekerId"); }
      set { Set(ref _jobSeekerId, value, "JobSeekerId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.Models.Career.VeteranEra> VeteranType
    {
      get { return Get(ref _veteranType, "VeteranType"); }
      set { Set(ref _veteranType, value, "VeteranType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string VeteranRank
    {
      get { return Get(ref _veteranRank, "VeteranRank"); }
      set { Set(ref _veteranRank, value, "VeteranRank"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }
    }

    /// <summary>Gets the time the entity was last updated</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime UpdatedOn
    {
      get { return _updatedOn; }
    }

    /// <summary>Gets the time the entity was soft-deleted</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> DeletedOn
    {
      get { return _deletedOn; }
    }

    #endregion
  }
	


  /// <summary>
  /// Provides a strong-typed unit of work for working with the Report model.
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  public partial class ReportUnitOfWork : Mindscape.LightSpeed.UnitOfWork
  {
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    public System.Collections.Generic.IList<JobSeekerAction> JobSeekerActionTotals(System.DateTime fromDate, System.DateTime toDate)
    {
      ProcedureParameter fromDateParameter = new ProcedureParameter("FromDate", fromDate);
      ProcedureParameter toDateParameter = new ProcedureParameter("ToDate", toDate);
      ProcedureQuery procedureQuery = new ProcedureQuery("[Report.JobSeekerActionTotals]", fromDateParameter, toDateParameter);
      System.Collections.Generic.IList<JobSeekerAction> valueToReturn = Find<JobSeekerAction>(procedureQuery);
      
      return valueToReturn;
    }
    /// <param name="keywords"></param>
    /// <param name="typesToCheck"></param>
    /// <param name="searchAll"></param>
    public System.Collections.Generic.IList<KeywordFilterView> JobSeekerKeywordFilter(string keywords, string typesToCheck, bool searchAll)
    {
      ProcedureParameter keywordsParameter = new ProcedureParameter("Keywords", keywords);
      ProcedureParameter typesToCheckParameter = new ProcedureParameter("TypesToCheck", typesToCheck);
      ProcedureParameter searchAllParameter = new ProcedureParameter("SearchAll", searchAll);
      ProcedureQuery procedureQuery = new ProcedureQuery("[Report.JobSeekerKeywordFilter]", keywordsParameter, typesToCheckParameter, searchAllParameter);
      System.Collections.Generic.IList<KeywordFilterView> valueToReturn = Find<KeywordFilterView>(procedureQuery);
      
      return valueToReturn;
    }
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <param name="jobSeekerIds"></param>
    public System.Collections.Generic.IList<JobSeekerAction> JobSeekerActionTotalsForIds(System.DateTime fromDate, System.DateTime toDate, string jobSeekerIds)
    {
      ProcedureParameter fromDateParameter = new ProcedureParameter("FromDate", fromDate);
      ProcedureParameter toDateParameter = new ProcedureParameter("ToDate", toDate);
      ProcedureParameter jobSeekerIdsParameter = new ProcedureParameter("JobSeekerIds", jobSeekerIds);
      ProcedureQuery procedureQuery = new ProcedureQuery("[Report.JobSeekerActionTotalsForIds]", fromDateParameter, toDateParameter, jobSeekerIdsParameter);
      System.Collections.Generic.IList<JobSeekerAction> valueToReturn = Find<JobSeekerAction>(procedureQuery);
      
      return valueToReturn;
    }
    /// <param name="keywords"></param>
    /// <param name="checkTitle"></param>
    /// <param name="checkDescription"></param>
    /// <param name="checkEmployer"></param>
    /// <param name="searchAll"></param>
    public System.Collections.Generic.IList<KeywordFilterView> JobOrderKeywordFilter(string keywords, bool checkTitle, bool checkDescription, bool checkEmployer, bool searchAll)
    {
      ProcedureParameter keywordsParameter = new ProcedureParameter("Keywords", keywords);
      ProcedureParameter checkTitleParameter = new ProcedureParameter("CheckTitle", checkTitle);
      ProcedureParameter checkDescriptionParameter = new ProcedureParameter("CheckDescription", checkDescription);
      ProcedureParameter checkEmployerParameter = new ProcedureParameter("CheckEmployer", checkEmployer);
      ProcedureParameter searchAllParameter = new ProcedureParameter("SearchAll", searchAll);
      ProcedureQuery procedureQuery = new ProcedureQuery("[Report.JobOrderKeywordFilter]", keywordsParameter, checkTitleParameter, checkDescriptionParameter, checkEmployerParameter, searchAllParameter);
      System.Collections.Generic.IList<KeywordFilterView> valueToReturn = Find<KeywordFilterView>(procedureQuery);
      
      return valueToReturn;
    }
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    public System.Collections.Generic.IList<JobOrderAction> JobOrderActionTotals(System.DateTime fromDate, System.DateTime toDate)
    {
      ProcedureParameter fromDateParameter = new ProcedureParameter("FromDate", fromDate);
      ProcedureParameter toDateParameter = new ProcedureParameter("ToDate", toDate);
      ProcedureQuery procedureQuery = new ProcedureQuery("[Report.JobOrderActionTotals]", fromDateParameter, toDateParameter);
      System.Collections.Generic.IList<JobOrderAction> valueToReturn = Find<JobOrderAction>(procedureQuery);
      
      return valueToReturn;
    }
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <param name="jobOrderIds"></param>
    public System.Collections.Generic.IList<JobOrderAction> JobOrderActionTotalsForIds(System.DateTime fromDate, System.DateTime toDate, string jobOrderIds)
    {
      ProcedureParameter fromDateParameter = new ProcedureParameter("FromDate", fromDate);
      ProcedureParameter toDateParameter = new ProcedureParameter("ToDate", toDate);
      ProcedureParameter jobOrderIdsParameter = new ProcedureParameter("JobOrderIds", jobOrderIds);
      ProcedureQuery procedureQuery = new ProcedureQuery("[Report.JobOrderActionTotalsForIds]", fromDateParameter, toDateParameter, jobOrderIdsParameter);
      System.Collections.Generic.IList<JobOrderAction> valueToReturn = Find<JobOrderAction>(procedureQuery);
      
      return valueToReturn;
    }
    /// <param name="keywords"></param>
    /// <param name="checkName"></param>
    /// <param name="checkJobTitle"></param>
    /// <param name="checkDescription"></param>
    /// <param name="searchAll"></param>
    public System.Collections.Generic.IList<KeywordFilterView> EmployerKeywordFilter(string keywords, bool checkName, bool checkJobTitle, bool checkDescription, bool searchAll)
    {
      ProcedureParameter keywordsParameter = new ProcedureParameter("Keywords", keywords);
      ProcedureParameter checkNameParameter = new ProcedureParameter("CheckName", checkName);
      ProcedureParameter checkJobTitleParameter = new ProcedureParameter("CheckJobTitle", checkJobTitle);
      ProcedureParameter checkDescriptionParameter = new ProcedureParameter("CheckDescription", checkDescription);
      ProcedureParameter searchAllParameter = new ProcedureParameter("SearchAll", searchAll);
      ProcedureQuery procedureQuery = new ProcedureQuery("[Report.EmployerKeywordFilter]", keywordsParameter, checkNameParameter, checkJobTitleParameter, checkDescriptionParameter, searchAllParameter);
      System.Collections.Generic.IList<KeywordFilterView> valueToReturn = Find<KeywordFilterView>(procedureQuery);
      
      return valueToReturn;
    }
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    public System.Collections.Generic.IList<EmployerAction> EmployerActionTotals(object fromDate, System.DateTime toDate)
    {
      ProcedureParameter fromDateParameter = new ProcedureParameter("FromDate", fromDate);
      ProcedureParameter toDateParameter = new ProcedureParameter("ToDate", toDate);
      ProcedureQuery procedureQuery = new ProcedureQuery("[Report.EmployerActionTotals]", fromDateParameter, toDateParameter);
      System.Collections.Generic.IList<EmployerAction> valueToReturn = Find<EmployerAction>(procedureQuery);
      
      return valueToReturn;
    }
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <param name="employerIds"></param>
    public System.Collections.Generic.IList<EmployerAction> EmployerActionTotalsForIds(System.DateTime fromDate, System.DateTime toDate, string employerIds)
    {
      ProcedureParameter fromDateParameter = new ProcedureParameter("FromDate", fromDate);
      ProcedureParameter toDateParameter = new ProcedureParameter("ToDate", toDate);
      ProcedureParameter employerIdsParameter = new ProcedureParameter("EmployerIds", employerIds);
      ProcedureQuery procedureQuery = new ProcedureQuery("[Report.EmployerActionTotalsForIds]", fromDateParameter, toDateParameter, employerIdsParameter);
      System.Collections.Generic.IList<EmployerAction> valueToReturn = Find<EmployerAction>(procedureQuery);
      
      return valueToReturn;
    }
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <param name="category"></param>
    /// <param name="activity"></param>
    public System.Collections.Generic.IList<JobSeekerActivityAssignmentsTotal> JobSeekerActivityTotals(System.DateTime fromDate, System.DateTime toDate, string category, string activity)
    {
      ProcedureParameter fromDateParameter = new ProcedureParameter("FromDate", fromDate);
      ProcedureParameter toDateParameter = new ProcedureParameter("ToDate", toDate);
      ProcedureParameter categoryParameter = new ProcedureParameter("Category", category);
      ProcedureParameter activityParameter = new ProcedureParameter("Activity", activity);
      ProcedureQuery procedureQuery = new ProcedureQuery("[Report.JobSeekerActivityTotals]", fromDateParameter, toDateParameter, categoryParameter, activityParameter);
      System.Collections.Generic.IList<JobSeekerActivityAssignmentsTotal> valueToReturn = Find<JobSeekerActivityAssignmentsTotal>(procedureQuery);
      
      return valueToReturn;
    }
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <param name="category"></param>
    /// <param name="activity"></param>
    /// <param name="jobSeekerIds"></param>
    public System.Collections.Generic.IList<JobSeekerActivityAssignmentsTotal> JobSeekerActivityTotalsForIds(System.DateTime fromDate, System.DateTime toDate, string category, string activity, string jobSeekerIds)
    {
      ProcedureParameter fromDateParameter = new ProcedureParameter("FromDate", fromDate);
      ProcedureParameter toDateParameter = new ProcedureParameter("ToDate", toDate);
      ProcedureParameter categoryParameter = new ProcedureParameter("Category", category);
      ProcedureParameter activityParameter = new ProcedureParameter("Activity", activity);
      ProcedureParameter jobSeekerIdsParameter = new ProcedureParameter("JobSeekerIds", jobSeekerIds);
      ProcedureQuery procedureQuery = new ProcedureQuery("[Report.JobSeekerActivityTotalsForIds]", fromDateParameter, toDateParameter, categoryParameter, activityParameter, jobSeekerIdsParameter);
      System.Collections.Generic.IList<JobSeekerActivityAssignmentsTotal> valueToReturn = Find<JobSeekerActivityAssignmentsTotal>(procedureQuery);
      
      return valueToReturn;
    }
    /// <param name="OfficeId"></param>
    public int JobSeekerOfficeNameUpdate(long OfficeId)
    {
      ProcedureParameter OfficeIdParameter = new ProcedureParameter("OfficeId", OfficeId);
      ProcedureQuery procedureQuery = new ProcedureQuery("[Report.JobSeekerOfficeNameUpdate]", OfficeIdParameter);
      int valueToReturn = Execute(procedureQuery);
      
      return valueToReturn;
    }
    /// <param name="OfficeId"></param>
    public int EmployerOfficeNameUpdate(long OfficeId)
    {
      ProcedureParameter OfficeIdParameter = new ProcedureParameter("OfficeId", OfficeId);
      ProcedureQuery procedureQuery = new ProcedureQuery("[Report.EmployerOfficeNameUpdate]", OfficeIdParameter);
      int valueToReturn = Execute(procedureQuery);
      
      return valueToReturn;
    }
    /// <param name="OfficeId"></param>
    public int JobOrderOfficeNameUpdate(long OfficeId)
    {
      ProcedureParameter OfficeIdParameter = new ProcedureParameter("OfficeId", OfficeId);
      ProcedureQuery procedureQuery = new ProcedureQuery("[Report.JobOrderOfficeNameUpdate]", OfficeIdParameter);
      int valueToReturn = Execute(procedureQuery);
      
      return valueToReturn;
    }
    /// <param name="OfficeId"></param>
    public int StaffMemberOfficeNameUpdate(long OfficeId)
    {
      ProcedureParameter OfficeIdParameter = new ProcedureParameter("OfficeId", OfficeId);
      ProcedureQuery procedureQuery = new ProcedureQuery("[Report.StaffMemberOfficeNameUpdate]", OfficeIdParameter);
      int valueToReturn = Execute(procedureQuery);
      
      return valueToReturn;
    }

    public System.Linq.IQueryable<JobSeeker> JobSeekers
    {
      get { return this.Query<JobSeeker>(); }
    }
    
    public System.Linq.IQueryable<Employer> Employers
    {
      get { return this.Query<Employer>(); }
    }
    
    public System.Linq.IQueryable<JobOrder> JobOrders
    {
      get { return this.Query<JobOrder>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerData> JobSeekerDatas
    {
      get { return this.Query<JobSeekerData>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerAction> JobSeekerActions
    {
      get { return this.Query<JobSeekerAction>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerActivityAssignment> JobSeekerActivityAssignments
    {
      get { return this.Query<JobSeekerActivityAssignment>(); }
    }
    
    public System.Linq.IQueryable<LookupJobSeekerCertificateView> LookupJobSeekerCertificateViews
    {
      get { return this.Query<LookupJobSeekerCertificateView>(); }
    }
    
    public System.Linq.IQueryable<LookupJobSeekerEducationView> LookupJobSeekerEducationViews
    {
      get { return this.Query<LookupJobSeekerEducationView>(); }
    }
    
    public System.Linq.IQueryable<LookupJobSeekerInternshipView> LookupJobSeekerInternshipViews
    {
      get { return this.Query<LookupJobSeekerInternshipView>(); }
    }
    
    public System.Linq.IQueryable<LookupJobSeekerSkillView> LookupJobSeekerSkillViews
    {
      get { return this.Query<LookupJobSeekerSkillView>(); }
    }
    
    public System.Linq.IQueryable<LookupJobSeekerMilitaryOccupationCodeView> LookupJobSeekerMilitaryOccupationCodeViews
    {
      get { return this.Query<LookupJobSeekerMilitaryOccupationCodeView>(); }
    }
    
    public System.Linq.IQueryable<LookupJobSeekerMilitaryOccupationTitleView> LookupJobSeekerMilitaryOccupationTitleViews
    {
      get { return this.Query<LookupJobSeekerMilitaryOccupationTitleView>(); }
    }
    
    public System.Linq.IQueryable<KeywordFilterView> KeywordFilterViews
    {
      get { return this.Query<KeywordFilterView>(); }
    }
    
    public System.Linq.IQueryable<SavedReport> SavedReports
    {
      get { return this.Query<SavedReport>(); }
    }
    
    public System.Linq.IQueryable<JobOrderAction> JobOrderActions
    {
      get { return this.Query<JobOrderAction>(); }
    }
    
    public System.Linq.IQueryable<EmployerAction> EmployerActions
    {
      get { return this.Query<EmployerAction>(); }
    }
    
    public System.Linq.IQueryable<JobOrderView> JobOrderViews
    {
      get { return this.Query<JobOrderView>(); }
    }
    
    public System.Linq.IQueryable<EmployerView> EmployerViews
    {
      get { return this.Query<EmployerView>(); }
    }
    
    public System.Linq.IQueryable<LookupEmployerFEINView> LookupEmployerFEINViews
    {
      get { return this.Query<LookupEmployerFEINView>(); }
    }
    
    public System.Linq.IQueryable<LookupEmployerNameView> LookupEmployerNameViews
    {
      get { return this.Query<LookupEmployerNameView>(); }
    }
    
    public System.Linq.IQueryable<LookupJobOrderTitleView> LookupJobOrderTitleViews
    {
      get { return this.Query<LookupJobOrderTitleView>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerActivityAssignmentsTotal> JobSeekerActivityAssignmentsTotals
    {
      get { return this.Query<JobSeekerActivityAssignmentsTotal>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerOffice> JobSeekerOffices
    {
      get { return this.Query<JobSeekerOffice>(); }
    }
    
    public System.Linq.IQueryable<JobOrderOffice> JobOrderOffices
    {
      get { return this.Query<JobOrderOffice>(); }
    }
    
    public System.Linq.IQueryable<EmployerOffice> EmployerOffices
    {
      get { return this.Query<EmployerOffice>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerJobHistory> JobSeekerJobHistories
    {
      get { return this.Query<JobSeekerJobHistory>(); }
    }
    
    public System.Linq.IQueryable<StaffMember> StaffMembers
    {
      get { return this.Query<StaffMember>(); }
    }
    
    public System.Linq.IQueryable<StaffMemberOffice> StaffMemberOffices
    {
      get { return this.Query<StaffMemberOffice>(); }
    }
    
    public System.Linq.IQueryable<StaffMemberCurrentOffice> StaffMemberCurrentOffices
    {
      get { return this.Query<StaffMemberCurrentOffice>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerActivityLog> JobSeekerActivityLogs
    {
      get { return this.Query<JobSeekerActivityLog>(); }
    }
    
    public System.Linq.IQueryable<Statistic> Statistics
    {
      get { return this.Query<Statistic>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerMilitaryHistory> JobSeekerMilitaryHistories
    {
      get { return this.Query<JobSeekerMilitaryHistory>(); }
    }
    
    public System.Linq.IQueryable<JobSeeker_NonPurged> JobSeeker_NonPurgeds
    {
      get { return this.Query<JobSeeker_NonPurged>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerMilitaryHistory_NonPurged> JobSeekerMilitaryHistory_NonPurgeds
    {
      get { return this.Query<JobSeekerMilitaryHistory_NonPurged>(); }
    }
    
  }
}

#endregion


#region Data Transfer Objects - Classes

/*
namespace Focus.Core.DataTransferObjects.FocusReport.Entities
{
  using System;
  using System.Runtime.Serialization;

    [Serializable]
    [DataContract(Name="JobSeeker", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public DateTime CreatedOn { get; set; }
      [DataMember]
      public DateTime UpdatedOn { get; set; }
      [DataMember]
      public long FocusPersonId { get; set; }
      [DataMember]
      public string FirstName { get; set; }
      [DataMember]
      public string LastName { get; set; }
      [DataMember]
      public long? CountyId { get; set; }
      [DataMember]
      public string County { get; set; }
      [DataMember]
      public long? StateId { get; set; }
      [DataMember]
      public string State { get; set; }
      [DataMember]
      public string PostalCode { get; set; }
      [DataMember]
      public string Office { get; set; }
      [DataMember]
      public DateTime AccountCreationDate { get; set; }
      [DataMember]
      public decimal? Salary { get; set; }
      [DataMember]
      public ReportSalaryFrequency? SalaryFrequency { get; set; }
      [DataMember]
      public Models.Career.EmploymentStatus? EmploymentStatus { get; set; }
      [DataMember]
      public Models.Career.VeteranEra? VeteranType { get; set; }
      [DataMember]
      public Models.Career.VeteranDisabilityStatus? VeteranDisability { get; set; }
      [DataMember]
      public Models.Career.VeteranTransitionType? VeteranTransitionType { get; set; }
      [DataMember]
      public ReportMilitaryDischargeType? VeteranMilitaryDischarge { get; set; }
      [DataMember]
      public ReportMilitaryBranchOfService? VeteranBranchOfService { get; set; }
      [DataMember]
      public bool? CampaignVeteran { get; set; }
      [DataMember]
      public DateTime? DateOfBirth { get; set; }
      [DataMember]
      public Genders? Gender { get; set; }
      [DataMember]
      public string EmailAddress { get; set; }
      [DataMember]
      public Models.Career.DisabilityStatus? DisabilityStatus { get; set; }
      [DataMember]
      public ReportDisabilityType? DisabilityType { get; set; }
      [DataMember]
      public ReportEthnicHeritage? EthnicHeritage { get; set; }
      [DataMember]
      public ReportRace? Race { get; set; }
      [DataMember]
      public bool? ResumeSearchable { get; set; }
      [DataMember]
      public int ResumeCount { get; set; }
      [DataMember]
      public bool? WillingToWorkOvertime { get; set; }
      [DataMember]
      public bool? WillingToRelocate { get; set; }
      [DataMember]
      public EducationLevel? MinimumEducationLevel { get; set; }
      [DataMember]
      public double LatitudeRadians { get; set; }
      [DataMember]
      public double LongitudeRadians { get; set; }
      [DataMember]
      public bool HasPendingResume { get; set; }
      [DataMember]
      public SchoolStatus? EnrollmentStatus { get; set; }
      [DataMember]
      public DateTime? MilitaryStartDate { get; set; }
      [DataMember]
      public DateTime? MilitaryEndDate { get; set; }
      [DataMember]
      public bool? IsHomelessVeteran { get; set; }
      [DataMember]
      public string SocialSecurityNumber { get; set; }
      [DataMember]
      public string MiddleInitial { get; set; }
      [DataMember]
      public string AddressLine1 { get; set; }
      [DataMember]
      public bool? IsCitizen { get; set; }
      [DataMember]
      public DateTime? AlienCardExpires { get; set; }
      [DataMember]
      public string AlienCardId { get; set; }
      [DataMember]
      public bool? VeteranAttendedTapCourse { get; set; }
      [DataMember]
      public short? Age { get; set; }
      [DataMember]
      public string NcrcLevel { get; set; }
      [DataMember]
      public bool Unsubscribed { get; set; }
      [DataMember]
      public string Suffix { get; set; }
      [DataMember]
      public bool HasAttendedTap { get; set; }
      [DataMember]
      public DateTime? AttendedTapDate { get; set; }
      [DataMember]
      public int? AccountType { get; set; }
      [DataMember]
      public string VeteranRank { get; set; }
      [DataMember]
      public bool Enabled { get; set; }
      [DataMember]
      public bool Blocked { get; set; }
    }

    [Serializable]
    [DataContract(Name="Employer", Namespace = Constants.DataContractNamespace)]
    public class EmployerDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public DateTime CreatedOn { get; set; }
      [DataMember]
      public DateTime UpdatedOn { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public string FederalEmployerIdentificationNumber { get; set; }
      [DataMember]
      public long? StateId { get; set; }
      [DataMember]
      public string PostalCode { get; set; }
      [DataMember]
      public string Office { get; set; }
      [DataMember]
      public DateTime? CreationDateTime { get; set; }
      [DataMember]
      public long? CountyId { get; set; }
      [DataMember]
      public long FocusBusinessUnitId { get; set; }
      [DataMember]
      public string State { get; set; }
      [DataMember]
      public string County { get; set; }
      [DataMember]
      public double LatitudeRadians { get; set; }
      [DataMember]
      public double LongitudeRadians { get; set; }
      [DataMember]
      public long? FocusEmployerId { get; set; }
      [DataMember]
      public long? AccountTypeId { get; set; }
      [DataMember]
      public long? NoOfEmployees { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobOrder", Namespace = Constants.DataContractNamespace)]
    public class JobOrderDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public DateTime CreatedOn { get; set; }
      [DataMember]
      public DateTime UpdatedOn { get; set; }
      [DataMember]
      public long FocusJobId { get; set; }
      [DataMember]
      public long EmployerId { get; set; }
      [DataMember]
      public string JobTitle { get; set; }
      [DataMember]
      public JobStatuses JobStatus { get; set; }
      [DataMember]
      public long? CountyId { get; set; }
      [DataMember]
      public long? StateId { get; set; }
      [DataMember]
      public string PostalCode { get; set; }
      [DataMember]
      public string Office { get; set; }
      [DataMember]
      public EducationLevels? MinimumEducationLevel { get; set; }
      [DataMember]
      public JobPostingFlags? PostingFlags { get; set; }
      [DataMember]
      public WorkOpportunitiesTaxCreditCategories? WorkOpportunitiesTaxCreditHires { get; set; }
      [DataMember]
      public bool? ForeignLabourCertification { get; set; }
      [DataMember]
      public ScreeningPreferences? ScreeningPreferences { get; set; }
      [DataMember]
      public decimal? MinSalary { get; set; }
      [DataMember]
      public decimal? MaxSalary { get; set; }
      [DataMember]
      public ReportSalaryFrequency? SalaryFrequency { get; set; }
      [DataMember]
      public string OnetCode { get; set; }
      [DataMember]
      public DateTime? PostedOn { get; set; }
      [DataMember]
      public DateTime? ClosingOn { get; set; }
      [DataMember]
      public double LatitudeRadians { get; set; }
      [DataMember]
      public double LongitudeRadians { get; set; }
      [DataMember]
      public string County { get; set; }
      [DataMember]
      public string State { get; set; }
      [DataMember]
      public string Description { get; set; }
      [DataMember]
      public ForeignLaborTypes? ForeignLabourType { get; set; }
      [DataMember]
      public bool? FederalContractor { get; set; }
      [DataMember]
      public bool? CourtOrderedAffirmativeAction { get; set; }
      [DataMember]
      public long? FocusBusinessUnitId { get; set; }
      [DataMember]
      public string NcrcLevel { get; set; }
      [DataMember]
      public bool? NcrcLevelRequired { get; set; }
      [DataMember]
      public int? NumberOfOpenings { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeekerData", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerDataDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Description { get; set; }
      [DataMember]
      public ReportJobSeekerDataType DataType { get; set; }
      [DataMember]
      public long JobSeekerId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeekerAction", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerActionDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public DateTime ActionDate { get; set; }
      [DataMember]
      public int PostingsViewed { get; set; }
      [DataMember]
      public int Logins { get; set; }
      [DataMember]
      public int ReferralRequests { get; set; }
      [DataMember]
      public int SelfReferrals { get; set; }
      [DataMember]
      public int StaffReferrals { get; set; }
      [DataMember]
      public int NotesAdded { get; set; }
      [DataMember]
      public int AddedToLists { get; set; }
      [DataMember]
      public int ActivitiesAssigned { get; set; }
      [DataMember]
      public int StaffAssignments { get; set; }
      [DataMember]
      public int EmailsSent { get; set; }
      [DataMember]
      public int FollowUpIssues { get; set; }
      [DataMember]
      public int IssuesResolved { get; set; }
      [DataMember]
      public int Hired { get; set; }
      [DataMember]
      public int NotHired { get; set; }
      [DataMember]
      public int FailedToApply { get; set; }
      [DataMember]
      public int FailedToReportToInterview { get; set; }
      [DataMember]
      public int InterviewDenied { get; set; }
      [DataMember]
      public int InterviewScheduled { get; set; }
      [DataMember]
      public int NewApplicant { get; set; }
      [DataMember]
      public int Recommended { get; set; }
      [DataMember]
      public int RefusedOffer { get; set; }
      [DataMember]
      public int UnderConsideration { get; set; }
      [DataMember]
      public int UsedOnlineResumeHelp { get; set; }
      [DataMember]
      public int SavedJobAlerts { get; set; }
      [DataMember]
      public int TargetingHighGrowthSectors { get; set; }
      [DataMember]
      public int SelfReferralsExternal { get; set; }
      [DataMember]
      public int StaffReferralsExternal { get; set; }
      [DataMember]
      public int ReferralsApproved { get; set; }
      [DataMember]
      public int ActivityServices { get; set; }
      [DataMember]
      public int FindJobsForSeeker { get; set; }
      [DataMember]
      public int FailedToReportToJob { get; set; }
      [DataMember]
      public int FailedToRespondToInvitation { get; set; }
      [DataMember]
      public int FoundJobFromOtherSource { get; set; }
      [DataMember]
      public int JobAlreadyFilled { get; set; }
      [DataMember]
      public int NotQualified { get; set; }
      [DataMember]
      public int NotYetPlaced { get; set; }
      [DataMember]
      public int RefusedReferral { get; set; }
      [DataMember]
      public int SurveySatisfied { get; set; }
      [DataMember]
      public int SurveyDissatisfied { get; set; }
      [DataMember]
      public int SurveyNoUnexpectedMatches { get; set; }
      [DataMember]
      public int SurveyUnexpectedMatches { get; set; }
      [DataMember]
      public int SurveyReceivedInvitations { get; set; }
      [DataMember]
      public int SurveyDidNotReceiveInvitations { get; set; }
      [DataMember]
      public int SurveyVerySatisfied { get; set; }
      [DataMember]
      public int SurveyVeryDissatisfied { get; set; }
      [DataMember]
      public long JobSeekerId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeekerActivityAssignment", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerActivityAssignmentDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public DateTime AssignDate { get; set; }
      [DataMember]
      public int AssignmentsMade { get; set; }
      [DataMember]
      public string Activity { get; set; }
      [DataMember]
      public string ActivityCategory { get; set; }
      [DataMember]
      public long JobSeekerId { get; set; }
    }

    [Serializable]
    [DataContract(Name="LookupJobSeekerCertificateView", Namespace = Constants.DataContractNamespace)]
    public class LookupJobSeekerCertificateViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Description { get; set; }
    }

    [Serializable]
    [DataContract(Name="LookupJobSeekerEducationView", Namespace = Constants.DataContractNamespace)]
    public class LookupJobSeekerEducationViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Description { get; set; }
    }

    [Serializable]
    [DataContract(Name="LookupJobSeekerInternshipView", Namespace = Constants.DataContractNamespace)]
    public class LookupJobSeekerInternshipViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Description { get; set; }
    }

    [Serializable]
    [DataContract(Name="LookupJobSeekerSkillView", Namespace = Constants.DataContractNamespace)]
    public class LookupJobSeekerSkillViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Description { get; set; }
    }

    [Serializable]
    [DataContract(Name="LookupJobSeekerMilitaryOccupationCodeView", Namespace = Constants.DataContractNamespace)]
    public class LookupJobSeekerMilitaryOccupationCodeViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string MilitaryOccupationCode { get; set; }
    }

    [Serializable]
    [DataContract(Name="LookupJobSeekerMilitaryOccupationTitleView", Namespace = Constants.DataContractNamespace)]
    public class LookupJobSeekerMilitaryOccupationTitleViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string MilitaryOccupationTitle { get; set; }
    }

    [Serializable]
    [DataContract(Name="KeywordFilterView", Namespace = Constants.DataContractNamespace)]
    public class KeywordFilterViewDto
    {
      [DataMember]
      public long? Id { get; set; }
    }

    [Serializable]
    [DataContract(Name="SavedReport", Namespace = Constants.DataContractNamespace)]
    public class SavedReportDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public ReportType ReportType { get; set; }
      [DataMember]
      public string Criteria { get; set; }
      [DataMember]
      public long? UserId { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public DateTime ReportDate { get; set; }
      [DataMember]
      public bool DisplayOnDashboard { get; set; }
      [DataMember]
      public bool IsSessionReport { get; set; }
      [DataMember]
      public ReportDisplayType? ReportDisplayType { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobOrderAction", Namespace = Constants.DataContractNamespace)]
    public class JobOrderActionDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public DateTime ActionDate { get; set; }
      [DataMember]
      public int StaffReferrals { get; set; }
      [DataMember]
      public int SelfReferrals { get; set; }
      [DataMember]
      public int EmployerInvitationsSent { get; set; }
      [DataMember]
      public int ApplicantsInterviewed { get; set; }
      [DataMember]
      public int ApplicantsFailedToShow { get; set; }
      [DataMember]
      public int ApplicantsDeniedInterviews { get; set; }
      [DataMember]
      public int ApplicantsHired { get; set; }
      [DataMember]
      public int JobOffersRefused { get; set; }
      [DataMember]
      public int ApplicantsDidNotApply { get; set; }
      [DataMember]
      public int InvitedJobSeekerViewed { get; set; }
      [DataMember]
      public int InvitedJobSeekerClicked { get; set; }
      [DataMember]
      public int ReferralsRequested { get; set; }
      [DataMember]
      public int ApplicantsNotHired { get; set; }
      [DataMember]
      public int ApplicantsNotYetPlaced { get; set; }
      [DataMember]
      public int FailedToRespondToInvitation { get; set; }
      [DataMember]
      public int FailedToReportToJob { get; set; }
      [DataMember]
      public int FoundJobFromOtherSource { get; set; }
      [DataMember]
      public int JobAlreadyFilled { get; set; }
      [DataMember]
      public int NewApplicant { get; set; }
      [DataMember]
      public int NotQualified { get; set; }
      [DataMember]
      public int ApplicantRecommended { get; set; }
      [DataMember]
      public int RefusedReferral { get; set; }
      [DataMember]
      public int UnderConsideration { get; set; }
      [DataMember]
      public long JobOrderId { get; set; }
    }

    [Serializable]
    [DataContract(Name="EmployerAction", Namespace = Constants.DataContractNamespace)]
    public class EmployerActionDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public DateTime ActionDate { get; set; }
      [DataMember]
      public int JobOrdersEdited { get; set; }
      [DataMember]
      public int JobSeekersInterviewed { get; set; }
      [DataMember]
      public int JobSeekersHired { get; set; }
      [DataMember]
      public int JobOrdersCreated { get; set; }
      [DataMember]
      public int JobOrdersPosted { get; set; }
      [DataMember]
      public int JobOrdersPutOnHold { get; set; }
      [DataMember]
      public int JobOrdersRefreshed { get; set; }
      [DataMember]
      public int JobOrdersClosed { get; set; }
      [DataMember]
      public int InvitationsSent { get; set; }
      [DataMember]
      public int JobSeekersNotHired { get; set; }
      [DataMember]
      public int SelfReferrals { get; set; }
      [DataMember]
      public int StaffReferrals { get; set; }
      [DataMember]
      public long EmployerId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobOrderView", Namespace = Constants.DataContractNamespace)]
    public class JobOrderViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public DateTime CreatedOn { get; set; }
      [DataMember]
      public DateTime UpdatedOn { get; set; }
      [DataMember]
      public string JobTitle { get; set; }
      [DataMember]
      public string EmployerName { get; set; }
      [DataMember]
      public string State { get; set; }
      [DataMember]
      public string Office { get; set; }
      [DataMember]
      public long FocusJobId { get; set; }
      [DataMember]
      public long EmployerId { get; set; }
      [DataMember]
      public JobStatuses JobStatus { get; set; }
      [DataMember]
      public double LatitudeRadians { get; set; }
      [DataMember]
      public double LongitudeRadians { get; set; }
      [DataMember]
      public string Description { get; set; }
      [DataMember]
      public string County { get; set; }
      [DataMember]
      public string PostalCode { get; set; }
      [DataMember]
      public EducationLevels? MinimumEducationLevel { get; set; }
      [DataMember]
      public JobPostingFlags? PostingFlags { get; set; }
      [DataMember]
      public WorkOpportunitiesTaxCreditCategories? WorkOpportunitiesTaxCreditHires { get; set; }
      [DataMember]
      public bool? ForeignLabourCertification { get; set; }
      [DataMember]
      public ScreeningPreferences? ScreeningPreferences { get; set; }
      [DataMember]
      public decimal? MinSalary { get; set; }
      [DataMember]
      public decimal? MaxSalary { get; set; }
      [DataMember]
      public ReportSalaryFrequency? SalaryFrequency { get; set; }
      [DataMember]
      public string OnetCode { get; set; }
      [DataMember]
      public DateTime? PostedOn { get; set; }
      [DataMember]
      public DateTime? ClosingOn { get; set; }
      [DataMember]
      public long? CountyId { get; set; }
      [DataMember]
      public long? StateId { get; set; }
      [DataMember]
      public string FederalEmployerIdentificationNumber { get; set; }
      [DataMember]
      public ForeignLaborTypes? ForeignLabourType { get; set; }
      [DataMember]
      public bool? FederalContractor { get; set; }
      [DataMember]
      public bool? CourtOrderedAffirmativeAction { get; set; }
      [DataMember]
      public long? AccountTypeId { get; set; }
      [DataMember]
      public string NcrcLevel { get; set; }
      [DataMember]
      public bool? NcrcLevelRequired { get; set; }
    }

    [Serializable]
    [DataContract(Name="EmployerView", Namespace = Constants.DataContractNamespace)]
    public class EmployerViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long FocusBusinessUnitId { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public double LatitudeRadians { get; set; }
      [DataMember]
      public double LongitudeRadians { get; set; }
      [DataMember]
      public string FederalEmployerIdentificationNumber { get; set; }
      [DataMember]
      public WorkOpportunitiesTaxCreditCategories? WorkOpportunitiesTaxCreditHires { get; set; }
      [DataMember]
      public bool? ForeignLabourCertification { get; set; }
      [DataMember]
      public ForeignLaborTypes? ForeignLabourType { get; set; }
      [DataMember]
      public bool? FederalContractor { get; set; }
      [DataMember]
      public bool? CourtOrderedAffirmativeAction { get; set; }
      [DataMember]
      public long? CountyId { get; set; }
      [DataMember]
      public string County { get; set; }
      [DataMember]
      public long? StateId { get; set; }
      [DataMember]
      public string State { get; set; }
      [DataMember]
      public string PostalCode { get; set; }
      [DataMember]
      public string Office { get; set; }
      [DataMember]
      public string JobTitle { get; set; }
      [DataMember]
      public decimal? MinSalary { get; set; }
      [DataMember]
      public decimal? MaxSalary { get; set; }
      [DataMember]
      public ReportSalaryFrequency? SalaryFrequency { get; set; }
      [DataMember]
      public JobStatuses? JobStatus { get; set; }
      [DataMember]
      public EducationLevels? MinimumEducationLevel { get; set; }
      [DataMember]
      public long? FocusEmployerId { get; set; }
      [DataMember]
      public string JobDescription { get; set; }
      [DataMember]
      public long? NoOfEmployees { get; set; }
    }

    [Serializable]
    [DataContract(Name="LookupEmployerFEINView", Namespace = Constants.DataContractNamespace)]
    public class LookupEmployerFEINViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string FederalEmployerIdentificationNumber { get; set; }
    }

    [Serializable]
    [DataContract(Name="LookupEmployerNameView", Namespace = Constants.DataContractNamespace)]
    public class LookupEmployerNameViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
    }

    [Serializable]
    [DataContract(Name="LookupJobOrderTitleView", Namespace = Constants.DataContractNamespace)]
    public class LookupJobOrderTitleViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string JobTitle { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeekerActivityAssignmentsTotal", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerActivityAssignmentsTotalDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public int AssignmentsMade { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeekerOffice", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerOfficeDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string OfficeName { get; set; }
      [DataMember]
      public long? OfficeId { get; set; }
      [DataMember]
      public DateTime? AssignedDate { get; set; }
      [DataMember]
      public long JobSeekerId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobOrderOffice", Namespace = Constants.DataContractNamespace)]
    public class JobOrderOfficeDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string OfficeName { get; set; }
      [DataMember]
      public long? OfficeId { get; set; }
      [DataMember]
      public long JobOrderId { get; set; }
    }

    [Serializable]
    [DataContract(Name="EmployerOffice", Namespace = Constants.DataContractNamespace)]
    public class EmployerOfficeDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string OfficeName { get; set; }
      [DataMember]
      public long? OfficeId { get; set; }
      [DataMember]
      public long EmployerId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeekerJobHistory", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerJobHistoryDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string EmployerName { get; set; }
      [DataMember]
      public DateTime? EmploymentStartDate { get; set; }
      [DataMember]
      public DateTime? EmploymentEndDate { get; set; }
      [DataMember]
      public string JobTitle { get; set; }
      [DataMember]
      public string EmployerCity { get; set; }
      [DataMember]
      public string EmployerState { get; set; }
      [DataMember]
      public long JobSeekerId { get; set; }
    }

    [Serializable]
    [DataContract(Name="StaffMember", Namespace = Constants.DataContractNamespace)]
    public class StaffMemberDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public DateTime CreatedOn { get; set; }
      [DataMember]
      public DateTime UpdatedOn { get; set; }
      [DataMember]
      public string FirstName { get; set; }
      [DataMember]
      public string LastName { get; set; }
      [DataMember]
      public long? CountyId { get; set; }
      [DataMember]
      public string County { get; set; }
      [DataMember]
      public long? StateId { get; set; }
      [DataMember]
      public string State { get; set; }
      [DataMember]
      public string PostalCode { get; set; }
      [DataMember]
      public string Office { get; set; }
      [DataMember]
      public long FocusPersonId { get; set; }
      [DataMember]
      public DateTime AccountCreationDate { get; set; }
      [DataMember]
      public double LatitudeRadians { get; set; }
      [DataMember]
      public double LongitudeRadians { get; set; }
      [DataMember]
      public string AddressLine1 { get; set; }
      [DataMember]
      public string EmailAddress { get; set; }
      [DataMember]
      public bool Blocked { get; set; }
      [DataMember]
      public string ExternalStaffId { get; set; }
      [DataMember]
      public bool? LocalVeteranEmploymentRepresentative { get; set; }
      [DataMember]
      public bool? DisabledVeteransOutreachProgramSpecialist { get; set; }
    }

    [Serializable]
    [DataContract(Name="StaffMemberOffice", Namespace = Constants.DataContractNamespace)]
    public class StaffMemberOfficeDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string OfficeName { get; set; }
      [DataMember]
      public long? OfficeId { get; set; }
      [DataMember]
      public DateTime? AssignedDate { get; set; }
      [DataMember]
      public long StaffMemberId { get; set; }
    }

    [Serializable]
    [DataContract(Name="StaffMemberCurrentOffice", Namespace = Constants.DataContractNamespace)]
    public class StaffMemberCurrentOfficeDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string OfficeName { get; set; }
      [DataMember]
      public long? OfficeId { get; set; }
      [DataMember]
      public DateTime? StartDate { get; set; }
      [DataMember]
      public long StaffMemberId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeekerActivityLog", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerActivityLogDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string ActionTypeName { get; set; }
      [DataMember]
      public DateTime ActionDate { get; set; }
      [DataMember]
      public long FocusUserId { get; set; }
      [DataMember]
      public string StaffExternalId { get; set; }
      [DataMember]
      public long? OfficeId { get; set; }
      [DataMember]
      public string OfficeName { get; set; }
      [DataMember]
      public long JobSeekerId { get; set; }
      [DataMember]
      public long? StaffMemberId { get; set; }
    }

    [Serializable]
    [DataContract(Name="Statistic", Namespace = Constants.DataContractNamespace)]
    public class StatisticDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public StatisticType StatisticType { get; set; }
      [DataMember]
      public string Value { get; set; }
      [DataMember]
      public DateTime CreatedOn { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeekerMilitaryHistory", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerMilitaryHistoryDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public Models.Career.VeteranEra? VeteranType { get; set; }
      [DataMember]
      public Models.Career.VeteranDisabilityStatus? VeteranDisability { get; set; }
      [DataMember]
      public Models.Career.VeteranTransitionType? VeteranTransitionType { get; set; }
      [DataMember]
      public ReportMilitaryDischargeType? VeteranMilitaryDischarge { get; set; }
      [DataMember]
      public ReportMilitaryBranchOfService? VeteranBranchOfService { get; set; }
      [DataMember]
      public bool? CampaignVeteran { get; set; }
      [DataMember]
      public DateTime? MilitaryStartDate { get; set; }
      [DataMember]
      public DateTime? MilitaryEndDate { get; set; }
      [DataMember]
      public string VeteranRank { get; set; }
      [DataMember]
      public long JobSeekerId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeeker_NonPurged", Namespace = Constants.DataContractNamespace)]
    public class JobSeeker_NonPurgedDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public DateTime CreatedOn { get; set; }
      [DataMember]
      public DateTime UpdatedOn { get; set; }
      [DataMember]
      public long JobSeekerReportId { get; set; }
      [DataMember]
      public DateTime? DateOfBirth { get; set; }
      [DataMember]
      public Genders? Gender { get; set; }
      [DataMember]
      public Models.Career.DisabilityStatus? DisabilityStatus { get; set; }
      [DataMember]
      public ReportDisabilityType? DisabilityType { get; set; }
      [DataMember]
      public ReportEthnicHeritage? EthnicHeritage { get; set; }
      [DataMember]
      public ReportRace? Race { get; set; }
      [DataMember]
      public EducationLevel? MinimumEducationLevel { get; set; }
      [DataMember]
      public SchoolStatus? EnrollmentStatus { get; set; }
      [DataMember]
      public Models.Career.EmploymentStatus? EmploymentStatus { get; set; }
      [DataMember]
      public short? Age { get; set; }
      [DataMember]
      public bool? IsCitizen { get; set; }
      [DataMember]
      public DateTime? AlienCardExpires { get; set; }
      [DataMember]
      public string AlienCardId { get; set; }
      [DataMember]
      public long? CountyId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeekerMilitaryHistory_NonPurged", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerMilitaryHistory_NonPurgedDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public DateTime CreatedOn { get; set; }
      [DataMember]
      public DateTime UpdatedOn { get; set; }
      [DataMember]
      public DateTime? DeletedOn { get; set; }
      [DataMember]
      public Models.Career.VeteranDisabilityStatus? VeteranDisability { get; set; }
      [DataMember]
      public Models.Career.VeteranTransitionType? VeteranTransitionType { get; set; }
      [DataMember]
      public ReportMilitaryDischargeType? VeteranMilitaryDischarge { get; set; }
      [DataMember]
      public ReportMilitaryBranchOfService? VeteranBranchOfService { get; set; }
      [DataMember]
      public bool? CampaignVeteran { get; set; }
      [DataMember]
      public DateTime? MilitaryStartDate { get; set; }
      [DataMember]
      public DateTime? MilitaryEndDate { get; set; }
      [DataMember]
      public long? JobSeekerId { get; set; }
      [DataMember]
      public Models.Career.VeteranEra? VeteranType { get; set; }
      [DataMember]
      public string VeteranRank { get; set; }
    }


}
*/

#endregion

#region Data Transfer Objects - Mappers

/*
namespace Focus.Services.DtoMappers
{
    using Focus.Core.DataTransferObjects.FocusReport.Entities;
    using Focus.Data.Report.Entities;

    public static class FocusReport.EntitiesMappers
    {

      #region JobSeekerDto Mappers

      public static JobSeekerDto AsDto(this JobSeeker entity)
      {
        var dto = new JobSeekerDto
        {
				  CreatedOn =  entity.CreatedOn,
				  UpdatedOn =  entity.UpdatedOn,
          FocusPersonId = entity.FocusPersonId,
          FirstName = entity.FirstName,
          LastName = entity.LastName,
          CountyId = entity.CountyId,
          County = entity.County,
          StateId = entity.StateId,
          State = entity.State,
          PostalCode = entity.PostalCode,
          Office = entity.Office,
          AccountCreationDate = entity.AccountCreationDate,
          Salary = entity.Salary,
          SalaryFrequency = entity.SalaryFrequency,
          EmploymentStatus = entity.EmploymentStatus,
          VeteranType = entity.VeteranType,
          VeteranDisability = entity.VeteranDisability,
          VeteranTransitionType = entity.VeteranTransitionType,
          VeteranMilitaryDischarge = entity.VeteranMilitaryDischarge,
          VeteranBranchOfService = entity.VeteranBranchOfService,
          CampaignVeteran = entity.CampaignVeteran,
          DateOfBirth = entity.DateOfBirth,
          Gender = entity.Gender,
          EmailAddress = entity.EmailAddress,
          DisabilityStatus = entity.DisabilityStatus,
          DisabilityType = entity.DisabilityType,
          EthnicHeritage = entity.EthnicHeritage,
          Race = entity.Race,
          ResumeSearchable = entity.ResumeSearchable,
          ResumeCount = entity.ResumeCount,
          WillingToWorkOvertime = entity.WillingToWorkOvertime,
          WillingToRelocate = entity.WillingToRelocate,
          MinimumEducationLevel = entity.MinimumEducationLevel,
          LatitudeRadians = entity.LatitudeRadians,
          LongitudeRadians = entity.LongitudeRadians,
          HasPendingResume = entity.HasPendingResume,
          EnrollmentStatus = entity.EnrollmentStatus,
          MilitaryStartDate = entity.MilitaryStartDate,
          MilitaryEndDate = entity.MilitaryEndDate,
          IsHomelessVeteran = entity.IsHomelessVeteran,
          SocialSecurityNumber = entity.SocialSecurityNumber,
          MiddleInitial = entity.MiddleInitial,
          AddressLine1 = entity.AddressLine1,
          IsCitizen = entity.IsCitizen,
          AlienCardExpires = entity.AlienCardExpires,
          AlienCardId = entity.AlienCardId,
          VeteranAttendedTapCourse = entity.VeteranAttendedTapCourse,
          Age = entity.Age,
          NcrcLevel = entity.NcrcLevel,
          Unsubscribed = entity.Unsubscribed,
          Suffix = entity.Suffix,
          HasAttendedTap = entity.HasAttendedTap,
          AttendedTapDate = entity.AttendedTapDate,
          AccountType = entity.AccountType,
          VeteranRank = entity.VeteranRank,
          Enabled = entity.Enabled,
          Blocked = entity.Blocked,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeeker CopyTo(this JobSeekerDto dto, JobSeeker entity)
      {
        entity.FocusPersonId = dto.FocusPersonId;
        entity.FirstName = dto.FirstName;
        entity.LastName = dto.LastName;
        entity.CountyId = dto.CountyId;
        entity.County = dto.County;
        entity.StateId = dto.StateId;
        entity.State = dto.State;
        entity.PostalCode = dto.PostalCode;
        entity.Office = dto.Office;
        entity.AccountCreationDate = dto.AccountCreationDate;
        entity.Salary = dto.Salary;
        entity.SalaryFrequency = dto.SalaryFrequency;
        entity.EmploymentStatus = dto.EmploymentStatus;
        entity.VeteranType = dto.VeteranType;
        entity.VeteranDisability = dto.VeteranDisability;
        entity.VeteranTransitionType = dto.VeteranTransitionType;
        entity.VeteranMilitaryDischarge = dto.VeteranMilitaryDischarge;
        entity.VeteranBranchOfService = dto.VeteranBranchOfService;
        entity.CampaignVeteran = dto.CampaignVeteran;
        entity.DateOfBirth = dto.DateOfBirth;
        entity.Gender = dto.Gender;
        entity.EmailAddress = dto.EmailAddress;
        entity.DisabilityStatus = dto.DisabilityStatus;
        entity.DisabilityType = dto.DisabilityType;
        entity.EthnicHeritage = dto.EthnicHeritage;
        entity.Race = dto.Race;
        entity.ResumeSearchable = dto.ResumeSearchable;
        entity.ResumeCount = dto.ResumeCount;
        entity.WillingToWorkOvertime = dto.WillingToWorkOvertime;
        entity.WillingToRelocate = dto.WillingToRelocate;
        entity.MinimumEducationLevel = dto.MinimumEducationLevel;
        entity.LatitudeRadians = dto.LatitudeRadians;
        entity.LongitudeRadians = dto.LongitudeRadians;
        entity.HasPendingResume = dto.HasPendingResume;
        entity.EnrollmentStatus = dto.EnrollmentStatus;
        entity.MilitaryStartDate = dto.MilitaryStartDate;
        entity.MilitaryEndDate = dto.MilitaryEndDate;
        entity.IsHomelessVeteran = dto.IsHomelessVeteran;
        entity.SocialSecurityNumber = dto.SocialSecurityNumber;
        entity.MiddleInitial = dto.MiddleInitial;
        entity.AddressLine1 = dto.AddressLine1;
        entity.IsCitizen = dto.IsCitizen;
        entity.AlienCardExpires = dto.AlienCardExpires;
        entity.AlienCardId = dto.AlienCardId;
        entity.VeteranAttendedTapCourse = dto.VeteranAttendedTapCourse;
        entity.Age = dto.Age;
        entity.NcrcLevel = dto.NcrcLevel;
        entity.Unsubscribed = dto.Unsubscribed;
        entity.Suffix = dto.Suffix;
        entity.HasAttendedTap = dto.HasAttendedTap;
        entity.AttendedTapDate = dto.AttendedTapDate;
        entity.AccountType = dto.AccountType;
        entity.VeteranRank = dto.VeteranRank;
        entity.Enabled = dto.Enabled;
        entity.Blocked = dto.Blocked;
        return entity;
      }

      public static JobSeeker CopyTo(this JobSeekerDto dto)
      {
        var entity = new JobSeeker();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region EmployerDto Mappers

      public static EmployerDto AsDto(this Employer entity)
      {
        var dto = new EmployerDto
        {
				  CreatedOn =  entity.CreatedOn,
				  UpdatedOn =  entity.UpdatedOn,
          Name = entity.Name,
          FederalEmployerIdentificationNumber = entity.FederalEmployerIdentificationNumber,
          StateId = entity.StateId,
          PostalCode = entity.PostalCode,
          Office = entity.Office,
          CreationDateTime = entity.CreationDateTime,
          CountyId = entity.CountyId,
          FocusBusinessUnitId = entity.FocusBusinessUnitId,
          State = entity.State,
          County = entity.County,
          LatitudeRadians = entity.LatitudeRadians,
          LongitudeRadians = entity.LongitudeRadians,
          FocusEmployerId = entity.FocusEmployerId,
          AccountTypeId = entity.AccountTypeId,
          NoOfEmployees = entity.NoOfEmployees,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static Employer CopyTo(this EmployerDto dto, Employer entity)
      {
        entity.Name = dto.Name;
        entity.FederalEmployerIdentificationNumber = dto.FederalEmployerIdentificationNumber;
        entity.StateId = dto.StateId;
        entity.PostalCode = dto.PostalCode;
        entity.Office = dto.Office;
        entity.CreationDateTime = dto.CreationDateTime;
        entity.CountyId = dto.CountyId;
        entity.FocusBusinessUnitId = dto.FocusBusinessUnitId;
        entity.State = dto.State;
        entity.County = dto.County;
        entity.LatitudeRadians = dto.LatitudeRadians;
        entity.LongitudeRadians = dto.LongitudeRadians;
        entity.FocusEmployerId = dto.FocusEmployerId;
        entity.AccountTypeId = dto.AccountTypeId;
        entity.NoOfEmployees = dto.NoOfEmployees;
        return entity;
      }

      public static Employer CopyTo(this EmployerDto dto)
      {
        var entity = new Employer();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobOrderDto Mappers

      public static JobOrderDto AsDto(this JobOrder entity)
      {
        var dto = new JobOrderDto
        {
				  CreatedOn =  entity.CreatedOn,
				  UpdatedOn =  entity.UpdatedOn,
          FocusJobId = entity.FocusJobId,
          EmployerId = entity.EmployerId,
          JobTitle = entity.JobTitle,
          JobStatus = entity.JobStatus,
          CountyId = entity.CountyId,
          StateId = entity.StateId,
          PostalCode = entity.PostalCode,
          Office = entity.Office,
          MinimumEducationLevel = entity.MinimumEducationLevel,
          PostingFlags = entity.PostingFlags,
          WorkOpportunitiesTaxCreditHires = entity.WorkOpportunitiesTaxCreditHires,
          ForeignLabourCertification = entity.ForeignLabourCertification,
          ScreeningPreferences = entity.ScreeningPreferences,
          MinSalary = entity.MinSalary,
          MaxSalary = entity.MaxSalary,
          SalaryFrequency = entity.SalaryFrequency,
          OnetCode = entity.OnetCode,
          PostedOn = entity.PostedOn,
          ClosingOn = entity.ClosingOn,
          LatitudeRadians = entity.LatitudeRadians,
          LongitudeRadians = entity.LongitudeRadians,
          County = entity.County,
          State = entity.State,
          Description = entity.Description,
          ForeignLabourType = entity.ForeignLabourType,
          FederalContractor = entity.FederalContractor,
          CourtOrderedAffirmativeAction = entity.CourtOrderedAffirmativeAction,
          FocusBusinessUnitId = entity.FocusBusinessUnitId,
          NcrcLevel = entity.NcrcLevel,
          NcrcLevelRequired = entity.NcrcLevelRequired,
          NumberOfOpenings = entity.NumberOfOpenings,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobOrder CopyTo(this JobOrderDto dto, JobOrder entity)
      {
        entity.FocusJobId = dto.FocusJobId;
        entity.EmployerId = dto.EmployerId;
        entity.JobTitle = dto.JobTitle;
        entity.JobStatus = dto.JobStatus;
        entity.CountyId = dto.CountyId;
        entity.StateId = dto.StateId;
        entity.PostalCode = dto.PostalCode;
        entity.Office = dto.Office;
        entity.MinimumEducationLevel = dto.MinimumEducationLevel;
        entity.PostingFlags = dto.PostingFlags;
        entity.WorkOpportunitiesTaxCreditHires = dto.WorkOpportunitiesTaxCreditHires;
        entity.ForeignLabourCertification = dto.ForeignLabourCertification;
        entity.ScreeningPreferences = dto.ScreeningPreferences;
        entity.MinSalary = dto.MinSalary;
        entity.MaxSalary = dto.MaxSalary;
        entity.SalaryFrequency = dto.SalaryFrequency;
        entity.OnetCode = dto.OnetCode;
        entity.PostedOn = dto.PostedOn;
        entity.ClosingOn = dto.ClosingOn;
        entity.LatitudeRadians = dto.LatitudeRadians;
        entity.LongitudeRadians = dto.LongitudeRadians;
        entity.County = dto.County;
        entity.State = dto.State;
        entity.Description = dto.Description;
        entity.ForeignLabourType = dto.ForeignLabourType;
        entity.FederalContractor = dto.FederalContractor;
        entity.CourtOrderedAffirmativeAction = dto.CourtOrderedAffirmativeAction;
        entity.FocusBusinessUnitId = dto.FocusBusinessUnitId;
        entity.NcrcLevel = dto.NcrcLevel;
        entity.NcrcLevelRequired = dto.NcrcLevelRequired;
        entity.NumberOfOpenings = dto.NumberOfOpenings;
        return entity;
      }

      public static JobOrder CopyTo(this JobOrderDto dto)
      {
        var entity = new JobOrder();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeekerDataDto Mappers

      public static JobSeekerDataDto AsDto(this JobSeekerData entity)
      {
        var dto = new JobSeekerDataDto
        {
          Description = entity.Description,
          DataType = entity.DataType,
          JobSeekerId = entity.JobSeekerId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeekerData CopyTo(this JobSeekerDataDto dto, JobSeekerData entity)
      {
        entity.Description = dto.Description;
        entity.DataType = dto.DataType;
        entity.JobSeekerId = dto.JobSeekerId;
        return entity;
      }

      public static JobSeekerData CopyTo(this JobSeekerDataDto dto)
      {
        var entity = new JobSeekerData();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeekerActionDto Mappers

      public static JobSeekerActionDto AsDto(this JobSeekerAction entity)
      {
        var dto = new JobSeekerActionDto
        {
          ActionDate = entity.ActionDate,
          PostingsViewed = entity.PostingsViewed,
          Logins = entity.Logins,
          ReferralRequests = entity.ReferralRequests,
          SelfReferrals = entity.SelfReferrals,
          StaffReferrals = entity.StaffReferrals,
          NotesAdded = entity.NotesAdded,
          AddedToLists = entity.AddedToLists,
          ActivitiesAssigned = entity.ActivitiesAssigned,
          StaffAssignments = entity.StaffAssignments,
          EmailsSent = entity.EmailsSent,
          FollowUpIssues = entity.FollowUpIssues,
          IssuesResolved = entity.IssuesResolved,
          Hired = entity.Hired,
          NotHired = entity.NotHired,
          FailedToApply = entity.FailedToApply,
          FailedToReportToInterview = entity.FailedToReportToInterview,
          InterviewDenied = entity.InterviewDenied,
          InterviewScheduled = entity.InterviewScheduled,
          NewApplicant = entity.NewApplicant,
          Recommended = entity.Recommended,
          RefusedOffer = entity.RefusedOffer,
          UnderConsideration = entity.UnderConsideration,
          UsedOnlineResumeHelp = entity.UsedOnlineResumeHelp,
          SavedJobAlerts = entity.SavedJobAlerts,
          TargetingHighGrowthSectors = entity.TargetingHighGrowthSectors,
          SelfReferralsExternal = entity.SelfReferralsExternal,
          StaffReferralsExternal = entity.StaffReferralsExternal,
          ReferralsApproved = entity.ReferralsApproved,
          ActivityServices = entity.ActivityServices,
          FindJobsForSeeker = entity.FindJobsForSeeker,
          FailedToReportToJob = entity.FailedToReportToJob,
          FailedToRespondToInvitation = entity.FailedToRespondToInvitation,
          FoundJobFromOtherSource = entity.FoundJobFromOtherSource,
          JobAlreadyFilled = entity.JobAlreadyFilled,
          NotQualified = entity.NotQualified,
          NotYetPlaced = entity.NotYetPlaced,
          RefusedReferral = entity.RefusedReferral,
          SurveySatisfied = entity.SurveySatisfied,
          SurveyDissatisfied = entity.SurveyDissatisfied,
          SurveyNoUnexpectedMatches = entity.SurveyNoUnexpectedMatches,
          SurveyUnexpectedMatches = entity.SurveyUnexpectedMatches,
          SurveyReceivedInvitations = entity.SurveyReceivedInvitations,
          SurveyDidNotReceiveInvitations = entity.SurveyDidNotReceiveInvitations,
          SurveyVerySatisfied = entity.SurveyVerySatisfied,
          SurveyVeryDissatisfied = entity.SurveyVeryDissatisfied,
          JobSeekerId = entity.JobSeekerId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeekerAction CopyTo(this JobSeekerActionDto dto, JobSeekerAction entity)
      {
        entity.ActionDate = dto.ActionDate;
        entity.PostingsViewed = dto.PostingsViewed;
        entity.Logins = dto.Logins;
        entity.ReferralRequests = dto.ReferralRequests;
        entity.SelfReferrals = dto.SelfReferrals;
        entity.StaffReferrals = dto.StaffReferrals;
        entity.NotesAdded = dto.NotesAdded;
        entity.AddedToLists = dto.AddedToLists;
        entity.ActivitiesAssigned = dto.ActivitiesAssigned;
        entity.StaffAssignments = dto.StaffAssignments;
        entity.EmailsSent = dto.EmailsSent;
        entity.FollowUpIssues = dto.FollowUpIssues;
        entity.IssuesResolved = dto.IssuesResolved;
        entity.Hired = dto.Hired;
        entity.NotHired = dto.NotHired;
        entity.FailedToApply = dto.FailedToApply;
        entity.FailedToReportToInterview = dto.FailedToReportToInterview;
        entity.InterviewDenied = dto.InterviewDenied;
        entity.InterviewScheduled = dto.InterviewScheduled;
        entity.NewApplicant = dto.NewApplicant;
        entity.Recommended = dto.Recommended;
        entity.RefusedOffer = dto.RefusedOffer;
        entity.UnderConsideration = dto.UnderConsideration;
        entity.UsedOnlineResumeHelp = dto.UsedOnlineResumeHelp;
        entity.SavedJobAlerts = dto.SavedJobAlerts;
        entity.TargetingHighGrowthSectors = dto.TargetingHighGrowthSectors;
        entity.SelfReferralsExternal = dto.SelfReferralsExternal;
        entity.StaffReferralsExternal = dto.StaffReferralsExternal;
        entity.ReferralsApproved = dto.ReferralsApproved;
        entity.ActivityServices = dto.ActivityServices;
        entity.FindJobsForSeeker = dto.FindJobsForSeeker;
        entity.FailedToReportToJob = dto.FailedToReportToJob;
        entity.FailedToRespondToInvitation = dto.FailedToRespondToInvitation;
        entity.FoundJobFromOtherSource = dto.FoundJobFromOtherSource;
        entity.JobAlreadyFilled = dto.JobAlreadyFilled;
        entity.NotQualified = dto.NotQualified;
        entity.NotYetPlaced = dto.NotYetPlaced;
        entity.RefusedReferral = dto.RefusedReferral;
        entity.SurveySatisfied = dto.SurveySatisfied;
        entity.SurveyDissatisfied = dto.SurveyDissatisfied;
        entity.SurveyNoUnexpectedMatches = dto.SurveyNoUnexpectedMatches;
        entity.SurveyUnexpectedMatches = dto.SurveyUnexpectedMatches;
        entity.SurveyReceivedInvitations = dto.SurveyReceivedInvitations;
        entity.SurveyDidNotReceiveInvitations = dto.SurveyDidNotReceiveInvitations;
        entity.SurveyVerySatisfied = dto.SurveyVerySatisfied;
        entity.SurveyVeryDissatisfied = dto.SurveyVeryDissatisfied;
        entity.JobSeekerId = dto.JobSeekerId;
        return entity;
      }

      public static JobSeekerAction CopyTo(this JobSeekerActionDto dto)
      {
        var entity = new JobSeekerAction();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeekerActivityAssignmentDto Mappers

      public static JobSeekerActivityAssignmentDto AsDto(this JobSeekerActivityAssignment entity)
      {
        var dto = new JobSeekerActivityAssignmentDto
        {
          AssignDate = entity.AssignDate,
          AssignmentsMade = entity.AssignmentsMade,
          Activity = entity.Activity,
          ActivityCategory = entity.ActivityCategory,
          JobSeekerId = entity.JobSeekerId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeekerActivityAssignment CopyTo(this JobSeekerActivityAssignmentDto dto, JobSeekerActivityAssignment entity)
      {
        entity.AssignDate = dto.AssignDate;
        entity.AssignmentsMade = dto.AssignmentsMade;
        entity.Activity = dto.Activity;
        entity.ActivityCategory = dto.ActivityCategory;
        entity.JobSeekerId = dto.JobSeekerId;
        return entity;
      }

      public static JobSeekerActivityAssignment CopyTo(this JobSeekerActivityAssignmentDto dto)
      {
        var entity = new JobSeekerActivityAssignment();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region LookupJobSeekerCertificateViewDto Mappers

      public static LookupJobSeekerCertificateViewDto AsDto(this LookupJobSeekerCertificateView entity)
      {
        var dto = new LookupJobSeekerCertificateViewDto
        {
          Description = entity.Description,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static LookupJobSeekerCertificateView CopyTo(this LookupJobSeekerCertificateViewDto dto, LookupJobSeekerCertificateView entity)
      {
        entity.Description = dto.Description;
        return entity;
      }

      public static LookupJobSeekerCertificateView CopyTo(this LookupJobSeekerCertificateViewDto dto)
      {
        var entity = new LookupJobSeekerCertificateView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region LookupJobSeekerEducationViewDto Mappers

      public static LookupJobSeekerEducationViewDto AsDto(this LookupJobSeekerEducationView entity)
      {
        var dto = new LookupJobSeekerEducationViewDto
        {
          Description = entity.Description,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static LookupJobSeekerEducationView CopyTo(this LookupJobSeekerEducationViewDto dto, LookupJobSeekerEducationView entity)
      {
        entity.Description = dto.Description;
        return entity;
      }

      public static LookupJobSeekerEducationView CopyTo(this LookupJobSeekerEducationViewDto dto)
      {
        var entity = new LookupJobSeekerEducationView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region LookupJobSeekerInternshipViewDto Mappers

      public static LookupJobSeekerInternshipViewDto AsDto(this LookupJobSeekerInternshipView entity)
      {
        var dto = new LookupJobSeekerInternshipViewDto
        {
          Description = entity.Description,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static LookupJobSeekerInternshipView CopyTo(this LookupJobSeekerInternshipViewDto dto, LookupJobSeekerInternshipView entity)
      {
        entity.Description = dto.Description;
        return entity;
      }

      public static LookupJobSeekerInternshipView CopyTo(this LookupJobSeekerInternshipViewDto dto)
      {
        var entity = new LookupJobSeekerInternshipView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region LookupJobSeekerSkillViewDto Mappers

      public static LookupJobSeekerSkillViewDto AsDto(this LookupJobSeekerSkillView entity)
      {
        var dto = new LookupJobSeekerSkillViewDto
        {
          Description = entity.Description,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static LookupJobSeekerSkillView CopyTo(this LookupJobSeekerSkillViewDto dto, LookupJobSeekerSkillView entity)
      {
        entity.Description = dto.Description;
        return entity;
      }

      public static LookupJobSeekerSkillView CopyTo(this LookupJobSeekerSkillViewDto dto)
      {
        var entity = new LookupJobSeekerSkillView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region LookupJobSeekerMilitaryOccupationCodeViewDto Mappers

      public static LookupJobSeekerMilitaryOccupationCodeViewDto AsDto(this LookupJobSeekerMilitaryOccupationCodeView entity)
      {
        var dto = new LookupJobSeekerMilitaryOccupationCodeViewDto
        {
          MilitaryOccupationCode = entity.MilitaryOccupationCode,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static LookupJobSeekerMilitaryOccupationCodeView CopyTo(this LookupJobSeekerMilitaryOccupationCodeViewDto dto, LookupJobSeekerMilitaryOccupationCodeView entity)
      {
        entity.MilitaryOccupationCode = dto.MilitaryOccupationCode;
        return entity;
      }

      public static LookupJobSeekerMilitaryOccupationCodeView CopyTo(this LookupJobSeekerMilitaryOccupationCodeViewDto dto)
      {
        var entity = new LookupJobSeekerMilitaryOccupationCodeView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region LookupJobSeekerMilitaryOccupationTitleViewDto Mappers

      public static LookupJobSeekerMilitaryOccupationTitleViewDto AsDto(this LookupJobSeekerMilitaryOccupationTitleView entity)
      {
        var dto = new LookupJobSeekerMilitaryOccupationTitleViewDto
        {
          MilitaryOccupationTitle = entity.MilitaryOccupationTitle,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static LookupJobSeekerMilitaryOccupationTitleView CopyTo(this LookupJobSeekerMilitaryOccupationTitleViewDto dto, LookupJobSeekerMilitaryOccupationTitleView entity)
      {
        entity.MilitaryOccupationTitle = dto.MilitaryOccupationTitle;
        return entity;
      }

      public static LookupJobSeekerMilitaryOccupationTitleView CopyTo(this LookupJobSeekerMilitaryOccupationTitleViewDto dto)
      {
        var entity = new LookupJobSeekerMilitaryOccupationTitleView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region KeywordFilterViewDto Mappers

      public static KeywordFilterViewDto AsDto(this KeywordFilterView entity)
      {
        var dto = new KeywordFilterViewDto
        {
				  Id = entity.Id
        };
        return dto;
      }
      
      public static KeywordFilterView CopyTo(this KeywordFilterViewDto dto, KeywordFilterView entity)
      {
        return entity;
      }

      public static KeywordFilterView CopyTo(this KeywordFilterViewDto dto)
      {
        var entity = new KeywordFilterView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region SavedReportDto Mappers

      public static SavedReportDto AsDto(this SavedReport entity)
      {
        var dto = new SavedReportDto
        {
          ReportType = entity.ReportType,
          Criteria = entity.Criteria,
          UserId = entity.UserId,
          Name = entity.Name,
          ReportDate = entity.ReportDate,
          DisplayOnDashboard = entity.DisplayOnDashboard,
          IsSessionReport = entity.IsSessionReport,
          ReportDisplayType = entity.ReportDisplayType,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static SavedReport CopyTo(this SavedReportDto dto, SavedReport entity)
      {
        entity.ReportType = dto.ReportType;
        entity.Criteria = dto.Criteria;
        entity.UserId = dto.UserId;
        entity.Name = dto.Name;
        entity.ReportDate = dto.ReportDate;
        entity.DisplayOnDashboard = dto.DisplayOnDashboard;
        entity.IsSessionReport = dto.IsSessionReport;
        entity.ReportDisplayType = dto.ReportDisplayType;
        return entity;
      }

      public static SavedReport CopyTo(this SavedReportDto dto)
      {
        var entity = new SavedReport();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobOrderActionDto Mappers

      public static JobOrderActionDto AsDto(this JobOrderAction entity)
      {
        var dto = new JobOrderActionDto
        {
          ActionDate = entity.ActionDate,
          StaffReferrals = entity.StaffReferrals,
          SelfReferrals = entity.SelfReferrals,
          EmployerInvitationsSent = entity.EmployerInvitationsSent,
          ApplicantsInterviewed = entity.ApplicantsInterviewed,
          ApplicantsFailedToShow = entity.ApplicantsFailedToShow,
          ApplicantsDeniedInterviews = entity.ApplicantsDeniedInterviews,
          ApplicantsHired = entity.ApplicantsHired,
          JobOffersRefused = entity.JobOffersRefused,
          ApplicantsDidNotApply = entity.ApplicantsDidNotApply,
          InvitedJobSeekerViewed = entity.InvitedJobSeekerViewed,
          InvitedJobSeekerClicked = entity.InvitedJobSeekerClicked,
          ReferralsRequested = entity.ReferralsRequested,
          ApplicantsNotHired = entity.ApplicantsNotHired,
          ApplicantsNotYetPlaced = entity.ApplicantsNotYetPlaced,
          FailedToRespondToInvitation = entity.FailedToRespondToInvitation,
          FailedToReportToJob = entity.FailedToReportToJob,
          FoundJobFromOtherSource = entity.FoundJobFromOtherSource,
          JobAlreadyFilled = entity.JobAlreadyFilled,
          NewApplicant = entity.NewApplicant,
          NotQualified = entity.NotQualified,
          ApplicantRecommended = entity.ApplicantRecommended,
          RefusedReferral = entity.RefusedReferral,
          UnderConsideration = entity.UnderConsideration,
          JobOrderId = entity.JobOrderId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobOrderAction CopyTo(this JobOrderActionDto dto, JobOrderAction entity)
      {
        entity.ActionDate = dto.ActionDate;
        entity.StaffReferrals = dto.StaffReferrals;
        entity.SelfReferrals = dto.SelfReferrals;
        entity.EmployerInvitationsSent = dto.EmployerInvitationsSent;
        entity.ApplicantsInterviewed = dto.ApplicantsInterviewed;
        entity.ApplicantsFailedToShow = dto.ApplicantsFailedToShow;
        entity.ApplicantsDeniedInterviews = dto.ApplicantsDeniedInterviews;
        entity.ApplicantsHired = dto.ApplicantsHired;
        entity.JobOffersRefused = dto.JobOffersRefused;
        entity.ApplicantsDidNotApply = dto.ApplicantsDidNotApply;
        entity.InvitedJobSeekerViewed = dto.InvitedJobSeekerViewed;
        entity.InvitedJobSeekerClicked = dto.InvitedJobSeekerClicked;
        entity.ReferralsRequested = dto.ReferralsRequested;
        entity.ApplicantsNotHired = dto.ApplicantsNotHired;
        entity.ApplicantsNotYetPlaced = dto.ApplicantsNotYetPlaced;
        entity.FailedToRespondToInvitation = dto.FailedToRespondToInvitation;
        entity.FailedToReportToJob = dto.FailedToReportToJob;
        entity.FoundJobFromOtherSource = dto.FoundJobFromOtherSource;
        entity.JobAlreadyFilled = dto.JobAlreadyFilled;
        entity.NewApplicant = dto.NewApplicant;
        entity.NotQualified = dto.NotQualified;
        entity.ApplicantRecommended = dto.ApplicantRecommended;
        entity.RefusedReferral = dto.RefusedReferral;
        entity.UnderConsideration = dto.UnderConsideration;
        entity.JobOrderId = dto.JobOrderId;
        return entity;
      }

      public static JobOrderAction CopyTo(this JobOrderActionDto dto)
      {
        var entity = new JobOrderAction();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region EmployerActionDto Mappers

      public static EmployerActionDto AsDto(this EmployerAction entity)
      {
        var dto = new EmployerActionDto
        {
          ActionDate = entity.ActionDate,
          JobOrdersEdited = entity.JobOrdersEdited,
          JobSeekersInterviewed = entity.JobSeekersInterviewed,
          JobSeekersHired = entity.JobSeekersHired,
          JobOrdersCreated = entity.JobOrdersCreated,
          JobOrdersPosted = entity.JobOrdersPosted,
          JobOrdersPutOnHold = entity.JobOrdersPutOnHold,
          JobOrdersRefreshed = entity.JobOrdersRefreshed,
          JobOrdersClosed = entity.JobOrdersClosed,
          InvitationsSent = entity.InvitationsSent,
          JobSeekersNotHired = entity.JobSeekersNotHired,
          SelfReferrals = entity.SelfReferrals,
          StaffReferrals = entity.StaffReferrals,
          EmployerId = entity.EmployerId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static EmployerAction CopyTo(this EmployerActionDto dto, EmployerAction entity)
      {
        entity.ActionDate = dto.ActionDate;
        entity.JobOrdersEdited = dto.JobOrdersEdited;
        entity.JobSeekersInterviewed = dto.JobSeekersInterviewed;
        entity.JobSeekersHired = dto.JobSeekersHired;
        entity.JobOrdersCreated = dto.JobOrdersCreated;
        entity.JobOrdersPosted = dto.JobOrdersPosted;
        entity.JobOrdersPutOnHold = dto.JobOrdersPutOnHold;
        entity.JobOrdersRefreshed = dto.JobOrdersRefreshed;
        entity.JobOrdersClosed = dto.JobOrdersClosed;
        entity.InvitationsSent = dto.InvitationsSent;
        entity.JobSeekersNotHired = dto.JobSeekersNotHired;
        entity.SelfReferrals = dto.SelfReferrals;
        entity.StaffReferrals = dto.StaffReferrals;
        entity.EmployerId = dto.EmployerId;
        return entity;
      }

      public static EmployerAction CopyTo(this EmployerActionDto dto)
      {
        var entity = new EmployerAction();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobOrderViewDto Mappers

      public static JobOrderViewDto AsDto(this JobOrderView entity)
      {
        var dto = new JobOrderViewDto
        {
				  CreatedOn =  entity.CreatedOn,
				  UpdatedOn =  entity.UpdatedOn,
          JobTitle = entity.JobTitle,
          EmployerName = entity.EmployerName,
          State = entity.State,
          Office = entity.Office,
          FocusJobId = entity.FocusJobId,
          EmployerId = entity.EmployerId,
          JobStatus = entity.JobStatus,
          LatitudeRadians = entity.LatitudeRadians,
          LongitudeRadians = entity.LongitudeRadians,
          Description = entity.Description,
          County = entity.County,
          PostalCode = entity.PostalCode,
          MinimumEducationLevel = entity.MinimumEducationLevel,
          PostingFlags = entity.PostingFlags,
          WorkOpportunitiesTaxCreditHires = entity.WorkOpportunitiesTaxCreditHires,
          ForeignLabourCertification = entity.ForeignLabourCertification,
          ScreeningPreferences = entity.ScreeningPreferences,
          MinSalary = entity.MinSalary,
          MaxSalary = entity.MaxSalary,
          SalaryFrequency = entity.SalaryFrequency,
          OnetCode = entity.OnetCode,
          PostedOn = entity.PostedOn,
          ClosingOn = entity.ClosingOn,
          CountyId = entity.CountyId,
          StateId = entity.StateId,
          FederalEmployerIdentificationNumber = entity.FederalEmployerIdentificationNumber,
          ForeignLabourType = entity.ForeignLabourType,
          FederalContractor = entity.FederalContractor,
          CourtOrderedAffirmativeAction = entity.CourtOrderedAffirmativeAction,
          AccountTypeId = entity.AccountTypeId,
          NcrcLevel = entity.NcrcLevel,
          NcrcLevelRequired = entity.NcrcLevelRequired,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobOrderView CopyTo(this JobOrderViewDto dto, JobOrderView entity)
      {
        entity.JobTitle = dto.JobTitle;
        entity.EmployerName = dto.EmployerName;
        entity.State = dto.State;
        entity.Office = dto.Office;
        entity.FocusJobId = dto.FocusJobId;
        entity.EmployerId = dto.EmployerId;
        entity.JobStatus = dto.JobStatus;
        entity.LatitudeRadians = dto.LatitudeRadians;
        entity.LongitudeRadians = dto.LongitudeRadians;
        entity.Description = dto.Description;
        entity.County = dto.County;
        entity.PostalCode = dto.PostalCode;
        entity.MinimumEducationLevel = dto.MinimumEducationLevel;
        entity.PostingFlags = dto.PostingFlags;
        entity.WorkOpportunitiesTaxCreditHires = dto.WorkOpportunitiesTaxCreditHires;
        entity.ForeignLabourCertification = dto.ForeignLabourCertification;
        entity.ScreeningPreferences = dto.ScreeningPreferences;
        entity.MinSalary = dto.MinSalary;
        entity.MaxSalary = dto.MaxSalary;
        entity.SalaryFrequency = dto.SalaryFrequency;
        entity.OnetCode = dto.OnetCode;
        entity.PostedOn = dto.PostedOn;
        entity.ClosingOn = dto.ClosingOn;
        entity.CountyId = dto.CountyId;
        entity.StateId = dto.StateId;
        entity.FederalEmployerIdentificationNumber = dto.FederalEmployerIdentificationNumber;
        entity.ForeignLabourType = dto.ForeignLabourType;
        entity.FederalContractor = dto.FederalContractor;
        entity.CourtOrderedAffirmativeAction = dto.CourtOrderedAffirmativeAction;
        entity.AccountTypeId = dto.AccountTypeId;
        entity.NcrcLevel = dto.NcrcLevel;
        entity.NcrcLevelRequired = dto.NcrcLevelRequired;
        return entity;
      }

      public static JobOrderView CopyTo(this JobOrderViewDto dto)
      {
        var entity = new JobOrderView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region EmployerViewDto Mappers

      public static EmployerViewDto AsDto(this EmployerView entity)
      {
        var dto = new EmployerViewDto
        {
          FocusBusinessUnitId = entity.FocusBusinessUnitId,
          Name = entity.Name,
          LatitudeRadians = entity.LatitudeRadians,
          LongitudeRadians = entity.LongitudeRadians,
          FederalEmployerIdentificationNumber = entity.FederalEmployerIdentificationNumber,
          WorkOpportunitiesTaxCreditHires = entity.WorkOpportunitiesTaxCreditHires,
          ForeignLabourCertification = entity.ForeignLabourCertification,
          ForeignLabourType = entity.ForeignLabourType,
          FederalContractor = entity.FederalContractor,
          CourtOrderedAffirmativeAction = entity.CourtOrderedAffirmativeAction,
          CountyId = entity.CountyId,
          County = entity.County,
          StateId = entity.StateId,
          State = entity.State,
          PostalCode = entity.PostalCode,
          Office = entity.Office,
          JobTitle = entity.JobTitle,
          MinSalary = entity.MinSalary,
          MaxSalary = entity.MaxSalary,
          SalaryFrequency = entity.SalaryFrequency,
          JobStatus = entity.JobStatus,
          MinimumEducationLevel = entity.MinimumEducationLevel,
          FocusEmployerId = entity.FocusEmployerId,
          JobDescription = entity.JobDescription,
          NoOfEmployees = entity.NoOfEmployees,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static EmployerView CopyTo(this EmployerViewDto dto, EmployerView entity)
      {
        entity.FocusBusinessUnitId = dto.FocusBusinessUnitId;
        entity.Name = dto.Name;
        entity.LatitudeRadians = dto.LatitudeRadians;
        entity.LongitudeRadians = dto.LongitudeRadians;
        entity.FederalEmployerIdentificationNumber = dto.FederalEmployerIdentificationNumber;
        entity.WorkOpportunitiesTaxCreditHires = dto.WorkOpportunitiesTaxCreditHires;
        entity.ForeignLabourCertification = dto.ForeignLabourCertification;
        entity.ForeignLabourType = dto.ForeignLabourType;
        entity.FederalContractor = dto.FederalContractor;
        entity.CourtOrderedAffirmativeAction = dto.CourtOrderedAffirmativeAction;
        entity.CountyId = dto.CountyId;
        entity.County = dto.County;
        entity.StateId = dto.StateId;
        entity.State = dto.State;
        entity.PostalCode = dto.PostalCode;
        entity.Office = dto.Office;
        entity.JobTitle = dto.JobTitle;
        entity.MinSalary = dto.MinSalary;
        entity.MaxSalary = dto.MaxSalary;
        entity.SalaryFrequency = dto.SalaryFrequency;
        entity.JobStatus = dto.JobStatus;
        entity.MinimumEducationLevel = dto.MinimumEducationLevel;
        entity.FocusEmployerId = dto.FocusEmployerId;
        entity.JobDescription = dto.JobDescription;
        entity.NoOfEmployees = dto.NoOfEmployees;
        return entity;
      }

      public static EmployerView CopyTo(this EmployerViewDto dto)
      {
        var entity = new EmployerView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region LookupEmployerFEINViewDto Mappers

      public static LookupEmployerFEINViewDto AsDto(this LookupEmployerFEINView entity)
      {
        var dto = new LookupEmployerFEINViewDto
        {
          FederalEmployerIdentificationNumber = entity.FederalEmployerIdentificationNumber,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static LookupEmployerFEINView CopyTo(this LookupEmployerFEINViewDto dto, LookupEmployerFEINView entity)
      {
        entity.FederalEmployerIdentificationNumber = dto.FederalEmployerIdentificationNumber;
        return entity;
      }

      public static LookupEmployerFEINView CopyTo(this LookupEmployerFEINViewDto dto)
      {
        var entity = new LookupEmployerFEINView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region LookupEmployerNameViewDto Mappers

      public static LookupEmployerNameViewDto AsDto(this LookupEmployerNameView entity)
      {
        var dto = new LookupEmployerNameViewDto
        {
          Name = entity.Name,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static LookupEmployerNameView CopyTo(this LookupEmployerNameViewDto dto, LookupEmployerNameView entity)
      {
        entity.Name = dto.Name;
        return entity;
      }

      public static LookupEmployerNameView CopyTo(this LookupEmployerNameViewDto dto)
      {
        var entity = new LookupEmployerNameView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region LookupJobOrderTitleViewDto Mappers

      public static LookupJobOrderTitleViewDto AsDto(this LookupJobOrderTitleView entity)
      {
        var dto = new LookupJobOrderTitleViewDto
        {
          JobTitle = entity.JobTitle,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static LookupJobOrderTitleView CopyTo(this LookupJobOrderTitleViewDto dto, LookupJobOrderTitleView entity)
      {
        entity.JobTitle = dto.JobTitle;
        return entity;
      }

      public static LookupJobOrderTitleView CopyTo(this LookupJobOrderTitleViewDto dto)
      {
        var entity = new LookupJobOrderTitleView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeekerActivityAssignmentsTotalDto Mappers

      public static JobSeekerActivityAssignmentsTotalDto AsDto(this JobSeekerActivityAssignmentsTotal entity)
      {
        var dto = new JobSeekerActivityAssignmentsTotalDto
        {
          AssignmentsMade = entity.AssignmentsMade,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeekerActivityAssignmentsTotal CopyTo(this JobSeekerActivityAssignmentsTotalDto dto, JobSeekerActivityAssignmentsTotal entity)
      {
        entity.AssignmentsMade = dto.AssignmentsMade;
        return entity;
      }

      public static JobSeekerActivityAssignmentsTotal CopyTo(this JobSeekerActivityAssignmentsTotalDto dto)
      {
        var entity = new JobSeekerActivityAssignmentsTotal();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeekerOfficeDto Mappers

      public static JobSeekerOfficeDto AsDto(this JobSeekerOffice entity)
      {
        var dto = new JobSeekerOfficeDto
        {
          OfficeName = entity.OfficeName,
          OfficeId = entity.OfficeId,
          AssignedDate = entity.AssignedDate,
          JobSeekerId = entity.JobSeekerId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeekerOffice CopyTo(this JobSeekerOfficeDto dto, JobSeekerOffice entity)
      {
        entity.OfficeName = dto.OfficeName;
        entity.OfficeId = dto.OfficeId;
        entity.AssignedDate = dto.AssignedDate;
        entity.JobSeekerId = dto.JobSeekerId;
        return entity;
      }

      public static JobSeekerOffice CopyTo(this JobSeekerOfficeDto dto)
      {
        var entity = new JobSeekerOffice();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobOrderOfficeDto Mappers

      public static JobOrderOfficeDto AsDto(this JobOrderOffice entity)
      {
        var dto = new JobOrderOfficeDto
        {
          OfficeName = entity.OfficeName,
          OfficeId = entity.OfficeId,
          JobOrderId = entity.JobOrderId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobOrderOffice CopyTo(this JobOrderOfficeDto dto, JobOrderOffice entity)
      {
        entity.OfficeName = dto.OfficeName;
        entity.OfficeId = dto.OfficeId;
        entity.JobOrderId = dto.JobOrderId;
        return entity;
      }

      public static JobOrderOffice CopyTo(this JobOrderOfficeDto dto)
      {
        var entity = new JobOrderOffice();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region EmployerOfficeDto Mappers

      public static EmployerOfficeDto AsDto(this EmployerOffice entity)
      {
        var dto = new EmployerOfficeDto
        {
          OfficeName = entity.OfficeName,
          OfficeId = entity.OfficeId,
          EmployerId = entity.EmployerId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static EmployerOffice CopyTo(this EmployerOfficeDto dto, EmployerOffice entity)
      {
        entity.OfficeName = dto.OfficeName;
        entity.OfficeId = dto.OfficeId;
        entity.EmployerId = dto.EmployerId;
        return entity;
      }

      public static EmployerOffice CopyTo(this EmployerOfficeDto dto)
      {
        var entity = new EmployerOffice();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeekerJobHistoryDto Mappers

      public static JobSeekerJobHistoryDto AsDto(this JobSeekerJobHistory entity)
      {
        var dto = new JobSeekerJobHistoryDto
        {
          EmployerName = entity.EmployerName,
          EmploymentStartDate = entity.EmploymentStartDate,
          EmploymentEndDate = entity.EmploymentEndDate,
          JobTitle = entity.JobTitle,
          EmployerCity = entity.EmployerCity,
          EmployerState = entity.EmployerState,
          JobSeekerId = entity.JobSeekerId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeekerJobHistory CopyTo(this JobSeekerJobHistoryDto dto, JobSeekerJobHistory entity)
      {
        entity.EmployerName = dto.EmployerName;
        entity.EmploymentStartDate = dto.EmploymentStartDate;
        entity.EmploymentEndDate = dto.EmploymentEndDate;
        entity.JobTitle = dto.JobTitle;
        entity.EmployerCity = dto.EmployerCity;
        entity.EmployerState = dto.EmployerState;
        entity.JobSeekerId = dto.JobSeekerId;
        return entity;
      }

      public static JobSeekerJobHistory CopyTo(this JobSeekerJobHistoryDto dto)
      {
        var entity = new JobSeekerJobHistory();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region StaffMemberDto Mappers

      public static StaffMemberDto AsDto(this StaffMember entity)
      {
        var dto = new StaffMemberDto
        {
				  CreatedOn =  entity.CreatedOn,
				  UpdatedOn =  entity.UpdatedOn,
          FirstName = entity.FirstName,
          LastName = entity.LastName,
          CountyId = entity.CountyId,
          County = entity.County,
          StateId = entity.StateId,
          State = entity.State,
          PostalCode = entity.PostalCode,
          Office = entity.Office,
          FocusPersonId = entity.FocusPersonId,
          AccountCreationDate = entity.AccountCreationDate,
          LatitudeRadians = entity.LatitudeRadians,
          LongitudeRadians = entity.LongitudeRadians,
          AddressLine1 = entity.AddressLine1,
          EmailAddress = entity.EmailAddress,
          Blocked = entity.Blocked,
          ExternalStaffId = entity.ExternalStaffId,
          LocalVeteranEmploymentRepresentative = entity.LocalVeteranEmploymentRepresentative,
          DisabledVeteransOutreachProgramSpecialist = entity.DisabledVeteransOutreachProgramSpecialist,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static StaffMember CopyTo(this StaffMemberDto dto, StaffMember entity)
      {
        entity.FirstName = dto.FirstName;
        entity.LastName = dto.LastName;
        entity.CountyId = dto.CountyId;
        entity.County = dto.County;
        entity.StateId = dto.StateId;
        entity.State = dto.State;
        entity.PostalCode = dto.PostalCode;
        entity.Office = dto.Office;
        entity.FocusPersonId = dto.FocusPersonId;
        entity.AccountCreationDate = dto.AccountCreationDate;
        entity.LatitudeRadians = dto.LatitudeRadians;
        entity.LongitudeRadians = dto.LongitudeRadians;
        entity.AddressLine1 = dto.AddressLine1;
        entity.EmailAddress = dto.EmailAddress;
        entity.Blocked = dto.Blocked;
        entity.ExternalStaffId = dto.ExternalStaffId;
        entity.LocalVeteranEmploymentRepresentative = dto.LocalVeteranEmploymentRepresentative;
        entity.DisabledVeteransOutreachProgramSpecialist = dto.DisabledVeteransOutreachProgramSpecialist;
        return entity;
      }

      public static StaffMember CopyTo(this StaffMemberDto dto)
      {
        var entity = new StaffMember();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region StaffMemberOfficeDto Mappers

      public static StaffMemberOfficeDto AsDto(this StaffMemberOffice entity)
      {
        var dto = new StaffMemberOfficeDto
        {
          OfficeName = entity.OfficeName,
          OfficeId = entity.OfficeId,
          AssignedDate = entity.AssignedDate,
          StaffMemberId = entity.StaffMemberId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static StaffMemberOffice CopyTo(this StaffMemberOfficeDto dto, StaffMemberOffice entity)
      {
        entity.OfficeName = dto.OfficeName;
        entity.OfficeId = dto.OfficeId;
        entity.AssignedDate = dto.AssignedDate;
        entity.StaffMemberId = dto.StaffMemberId;
        return entity;
      }

      public static StaffMemberOffice CopyTo(this StaffMemberOfficeDto dto)
      {
        var entity = new StaffMemberOffice();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region StaffMemberCurrentOfficeDto Mappers

      public static StaffMemberCurrentOfficeDto AsDto(this StaffMemberCurrentOffice entity)
      {
        var dto = new StaffMemberCurrentOfficeDto
        {
          OfficeName = entity.OfficeName,
          OfficeId = entity.OfficeId,
          StartDate = entity.StartDate,
          StaffMemberId = entity.StaffMemberId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static StaffMemberCurrentOffice CopyTo(this StaffMemberCurrentOfficeDto dto, StaffMemberCurrentOffice entity)
      {
        entity.OfficeName = dto.OfficeName;
        entity.OfficeId = dto.OfficeId;
        entity.StartDate = dto.StartDate;
        entity.StaffMemberId = dto.StaffMemberId;
        return entity;
      }

      public static StaffMemberCurrentOffice CopyTo(this StaffMemberCurrentOfficeDto dto)
      {
        var entity = new StaffMemberCurrentOffice();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeekerActivityLogDto Mappers

      public static JobSeekerActivityLogDto AsDto(this JobSeekerActivityLog entity)
      {
        var dto = new JobSeekerActivityLogDto
        {
          ActionTypeName = entity.ActionTypeName,
          ActionDate = entity.ActionDate,
          FocusUserId = entity.FocusUserId,
          StaffExternalId = entity.StaffExternalId,
          OfficeId = entity.OfficeId,
          OfficeName = entity.OfficeName,
          JobSeekerId = entity.JobSeekerId,
          StaffMemberId = entity.StaffMemberId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeekerActivityLog CopyTo(this JobSeekerActivityLogDto dto, JobSeekerActivityLog entity)
      {
        entity.ActionTypeName = dto.ActionTypeName;
        entity.ActionDate = dto.ActionDate;
        entity.FocusUserId = dto.FocusUserId;
        entity.StaffExternalId = dto.StaffExternalId;
        entity.OfficeId = dto.OfficeId;
        entity.OfficeName = dto.OfficeName;
        entity.JobSeekerId = dto.JobSeekerId;
        entity.StaffMemberId = dto.StaffMemberId;
        return entity;
      }

      public static JobSeekerActivityLog CopyTo(this JobSeekerActivityLogDto dto)
      {
        var entity = new JobSeekerActivityLog();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region StatisticDto Mappers

      public static StatisticDto AsDto(this Statistic entity)
      {
        var dto = new StatisticDto
        {
          StatisticType = entity.StatisticType,
          Value = entity.Value,
          CreatedOn = entity.CreatedOn,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static Statistic CopyTo(this StatisticDto dto, Statistic entity)
      {
        entity.StatisticType = dto.StatisticType;
        entity.Value = dto.Value;
        entity.CreatedOn = dto.CreatedOn;
        return entity;
      }

      public static Statistic CopyTo(this StatisticDto dto)
      {
        var entity = new Statistic();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeekerMilitaryHistoryDto Mappers

      public static JobSeekerMilitaryHistoryDto AsDto(this JobSeekerMilitaryHistory entity)
      {
        var dto = new JobSeekerMilitaryHistoryDto
        {
          VeteranType = entity.VeteranType,
          VeteranDisability = entity.VeteranDisability,
          VeteranTransitionType = entity.VeteranTransitionType,
          VeteranMilitaryDischarge = entity.VeteranMilitaryDischarge,
          VeteranBranchOfService = entity.VeteranBranchOfService,
          CampaignVeteran = entity.CampaignVeteran,
          MilitaryStartDate = entity.MilitaryStartDate,
          MilitaryEndDate = entity.MilitaryEndDate,
          VeteranRank = entity.VeteranRank,
          JobSeekerId = entity.JobSeekerId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeekerMilitaryHistory CopyTo(this JobSeekerMilitaryHistoryDto dto, JobSeekerMilitaryHistory entity)
      {
        entity.VeteranType = dto.VeteranType;
        entity.VeteranDisability = dto.VeteranDisability;
        entity.VeteranTransitionType = dto.VeteranTransitionType;
        entity.VeteranMilitaryDischarge = dto.VeteranMilitaryDischarge;
        entity.VeteranBranchOfService = dto.VeteranBranchOfService;
        entity.CampaignVeteran = dto.CampaignVeteran;
        entity.MilitaryStartDate = dto.MilitaryStartDate;
        entity.MilitaryEndDate = dto.MilitaryEndDate;
        entity.VeteranRank = dto.VeteranRank;
        entity.JobSeekerId = dto.JobSeekerId;
        return entity;
      }

      public static JobSeekerMilitaryHistory CopyTo(this JobSeekerMilitaryHistoryDto dto)
      {
        var entity = new JobSeekerMilitaryHistory();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeeker_NonPurgedDto Mappers

      public static JobSeeker_NonPurgedDto AsDto(this JobSeeker_NonPurged entity)
      {
        var dto = new JobSeeker_NonPurgedDto
        {
				  CreatedOn =  entity.CreatedOn,
				  UpdatedOn =  entity.UpdatedOn,
          JobSeekerReportId = entity.JobSeekerReportId,
          DateOfBirth = entity.DateOfBirth,
          Gender = entity.Gender,
          DisabilityStatus = entity.DisabilityStatus,
          DisabilityType = entity.DisabilityType,
          EthnicHeritage = entity.EthnicHeritage,
          Race = entity.Race,
          MinimumEducationLevel = entity.MinimumEducationLevel,
          EnrollmentStatus = entity.EnrollmentStatus,
          EmploymentStatus = entity.EmploymentStatus,
          Age = entity.Age,
          IsCitizen = entity.IsCitizen,
          AlienCardExpires = entity.AlienCardExpires,
          AlienCardId = entity.AlienCardId,
          CountyId = entity.CountyId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeeker_NonPurged CopyTo(this JobSeeker_NonPurgedDto dto, JobSeeker_NonPurged entity)
      {
        entity.JobSeekerReportId = dto.JobSeekerReportId;
        entity.DateOfBirth = dto.DateOfBirth;
        entity.Gender = dto.Gender;
        entity.DisabilityStatus = dto.DisabilityStatus;
        entity.DisabilityType = dto.DisabilityType;
        entity.EthnicHeritage = dto.EthnicHeritage;
        entity.Race = dto.Race;
        entity.MinimumEducationLevel = dto.MinimumEducationLevel;
        entity.EnrollmentStatus = dto.EnrollmentStatus;
        entity.EmploymentStatus = dto.EmploymentStatus;
        entity.Age = dto.Age;
        entity.IsCitizen = dto.IsCitizen;
        entity.AlienCardExpires = dto.AlienCardExpires;
        entity.AlienCardId = dto.AlienCardId;
        entity.CountyId = dto.CountyId;
        return entity;
      }

      public static JobSeeker_NonPurged CopyTo(this JobSeeker_NonPurgedDto dto)
      {
        var entity = new JobSeeker_NonPurged();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeekerMilitaryHistory_NonPurgedDto Mappers

      public static JobSeekerMilitaryHistory_NonPurgedDto AsDto(this JobSeekerMilitaryHistory_NonPurged entity)
      {
        var dto = new JobSeekerMilitaryHistory_NonPurgedDto
        {
				  CreatedOn =  entity.CreatedOn,
				  UpdatedOn =  entity.UpdatedOn,
				  DeletedOn =  entity.DeletedOn,
          VeteranDisability = entity.VeteranDisability,
          VeteranTransitionType = entity.VeteranTransitionType,
          VeteranMilitaryDischarge = entity.VeteranMilitaryDischarge,
          VeteranBranchOfService = entity.VeteranBranchOfService,
          CampaignVeteran = entity.CampaignVeteran,
          MilitaryStartDate = entity.MilitaryStartDate,
          MilitaryEndDate = entity.MilitaryEndDate,
          JobSeekerId = entity.JobSeekerId,
          VeteranType = entity.VeteranType,
          VeteranRank = entity.VeteranRank,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeekerMilitaryHistory_NonPurged CopyTo(this JobSeekerMilitaryHistory_NonPurgedDto dto, JobSeekerMilitaryHistory_NonPurged entity)
      {
        entity.VeteranDisability = dto.VeteranDisability;
        entity.VeteranTransitionType = dto.VeteranTransitionType;
        entity.VeteranMilitaryDischarge = dto.VeteranMilitaryDischarge;
        entity.VeteranBranchOfService = dto.VeteranBranchOfService;
        entity.CampaignVeteran = dto.CampaignVeteran;
        entity.MilitaryStartDate = dto.MilitaryStartDate;
        entity.MilitaryEndDate = dto.MilitaryEndDate;
        entity.JobSeekerId = dto.JobSeekerId;
        entity.VeteranType = dto.VeteranType;
        entity.VeteranRank = dto.VeteranRank;
        return entity;
      }

      public static JobSeekerMilitaryHistory_NonPurged CopyTo(this JobSeekerMilitaryHistory_NonPurgedDto dto)
      {
        var entity = new JobSeekerMilitaryHistory_NonPurged();
        return dto.CopyTo(entity);
      }	

      #endregion

    }
}
*/

#endregion
