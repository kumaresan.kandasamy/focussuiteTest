using System;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Validation;
using Mindscape.LightSpeed.Linq;

#region Entities

namespace Focus.Data.Core.Entities
{
  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Core.SingleSignOn", IdentityMethod=IdentityMethod.Guid)]
  public partial class SingleSignOn : Entity<System.Guid>
  {
    #region Fields
  
    private long _userId;
    private long _actionerId;
    [ValidateLength(0, 20)]
    private string _externalAdminId;
    [ValidateLength(0, 20)]
    private string _externalOfficeId;
    [ValidateLength(0, 20)]
    private string _externalPassword;
    [ValidateLength(0, 20)]
    private string _externalUsername;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the ActionerId entity attribute.</summary>
    public const string ActionerIdField = "ActionerId";
    /// <summary>Identifies the ExternalAdminId entity attribute.</summary>
    public const string ExternalAdminIdField = "ExternalAdminId";
    /// <summary>Identifies the ExternalOfficeId entity attribute.</summary>
    public const string ExternalOfficeIdField = "ExternalOfficeId";
    /// <summary>Identifies the ExternalPassword entity attribute.</summary>
    public const string ExternalPasswordField = "ExternalPassword";
    /// <summary>Identifies the ExternalUsername entity attribute.</summary>
    public const string ExternalUsernameField = "ExternalUsername";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public long UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long ActionerId
    {
      get { return Get(ref _actionerId, "ActionerId"); }
      set { Set(ref _actionerId, value, "ActionerId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalAdminId
    {
      get { return Get(ref _externalAdminId, "ExternalAdminId"); }
      set { Set(ref _externalAdminId, value, "ExternalAdminId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalOfficeId
    {
      get { return Get(ref _externalOfficeId, "ExternalOfficeId"); }
      set { Set(ref _externalOfficeId, value, "ExternalOfficeId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalPassword
    {
      get { return Get(ref _externalPassword, "ExternalPassword"); }
      set { Set(ref _externalPassword, value, "ExternalPassword"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalUsername
    {
      get { return Get(ref _externalUsername, "ExternalUsername"); }
      set { Set(ref _externalUsername, value, "ExternalUsername"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Core.ActionType")]
  public partial class ActionType : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    [ValidatePresence]
    private string _name;
    private bool _reportOn;
    private System.Nullable<bool> _assistAction;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the ReportOn entity attribute.</summary>
    public const string ReportOnField = "ReportOn";
    /// <summary>Identifies the AssistAction entity attribute.</summary>
    public const string AssistActionField = "AssistAction";


    #endregion
    
    #region Relationships

    [ReverseAssociation("ActionType")]
    private readonly EntityCollection<ActionEvent> _actionEvents = new EntityCollection<ActionEvent>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<ActionEvent> ActionEvents
    {
      get { return Get(_actionEvents); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool ReportOn
    {
      get { return Get(ref _reportOn, "ReportOn"); }
      set { Set(ref _reportOn, value, "ReportOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> AssistAction
    {
      get { return Get(ref _assistAction, "AssistAction"); }
      set { Set(ref _assistAction, value, "AssistAction"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Core.EntityType")]
  public partial class EntityType : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _name;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";


    #endregion
    
    #region Relationships

    [ReverseAssociation("EntityType")]
    private readonly EntityCollection<ActionEvent> _actionEvents = new EntityCollection<ActionEvent>();
    [ReverseAssociation("EntityType")]
    private readonly EntityCollection<StatusLog> _statusLogs = new EntityCollection<StatusLog>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<ActionEvent> ActionEvents
    {
      get { return Get(_actionEvents); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<StatusLog> StatusLogs
    {
      get { return Get(_statusLogs); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Core.ActionEvent")]
  public partial class ActionEvent : Entity<long>
  {
    #region Fields
  
    private System.Guid _sessionId;
    private System.Guid _requestId;
    private long _userId;
    private System.DateTime _actionedOn;
    private System.Nullable<long> _entityId;
    private System.Nullable<long> _entityIdAdditional01;
    private System.Nullable<long> _entityIdAdditional02;
    private string _additionalDetails;
    private System.DateTime _createdOn;
    private Focus.Core.ActionEventStatus _activityStatusId;
    private System.Nullable<System.DateTime> _activityDeletedOn;
    private System.Nullable<long> _activityUpdatedBy;
    private long _actionTypeId;
    private System.Nullable<long> _entityTypeId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SessionId entity attribute.</summary>
    public const string SessionIdField = "SessionId";
    /// <summary>Identifies the RequestId entity attribute.</summary>
    public const string RequestIdField = "RequestId";
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the ActionedOn entity attribute.</summary>
    public const string ActionedOnField = "ActionedOn";
    /// <summary>Identifies the EntityId entity attribute.</summary>
    public const string EntityIdField = "EntityId";
    /// <summary>Identifies the EntityIdAdditional01 entity attribute.</summary>
    public const string EntityIdAdditional01Field = "EntityIdAdditional01";
    /// <summary>Identifies the EntityIdAdditional02 entity attribute.</summary>
    public const string EntityIdAdditional02Field = "EntityIdAdditional02";
    /// <summary>Identifies the AdditionalDetails entity attribute.</summary>
    public const string AdditionalDetailsField = "AdditionalDetails";
    /// <summary>Identifies the CreatedOn entity attribute.</summary>
    public const string CreatedOnField = "CreatedOn";
    /// <summary>Identifies the ActivityStatusId entity attribute.</summary>
    public const string ActivityStatusIdField = "ActivityStatusId";
    /// <summary>Identifies the ActivityDeletedOn entity attribute.</summary>
    public const string ActivityDeletedOnField = "ActivityDeletedOn";
    /// <summary>Identifies the ActivityUpdatedBy entity attribute.</summary>
    public const string ActivityUpdatedByField = "ActivityUpdatedBy";
    /// <summary>Identifies the ActionTypeId entity attribute.</summary>
    public const string ActionTypeIdField = "ActionTypeId";
    /// <summary>Identifies the EntityTypeId entity attribute.</summary>
    public const string EntityTypeIdField = "EntityTypeId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("ActionEvents")]
    private readonly EntityHolder<ActionType> _actionType = new EntityHolder<ActionType>();
    [ReverseAssociation("ActionEvents")]
    private readonly EntityHolder<EntityType> _entityType = new EntityHolder<EntityType>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public ActionType ActionType
    {
      get { return Get(_actionType); }
      set { Set(_actionType, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityType EntityType
    {
      get { return Get(_entityType); }
      set { Set(_entityType, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public System.Guid SessionId
    {
      get { return Get(ref _sessionId, "SessionId"); }
      set { Set(ref _sessionId, value, "SessionId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Guid RequestId
    {
      get { return Get(ref _requestId, "RequestId"); }
      set { Set(ref _requestId, value, "RequestId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ActionedOn
    {
      get { return Get(ref _actionedOn, "ActionedOn"); }
      set { Set(ref _actionedOn, value, "ActionedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EntityId
    {
      get { return Get(ref _entityId, "EntityId"); }
      set { Set(ref _entityId, value, "EntityId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EntityIdAdditional01
    {
      get { return Get(ref _entityIdAdditional01, "EntityIdAdditional01"); }
      set { Set(ref _entityIdAdditional01, value, "EntityIdAdditional01"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EntityIdAdditional02
    {
      get { return Get(ref _entityIdAdditional02, "EntityIdAdditional02"); }
      set { Set(ref _entityIdAdditional02, value, "EntityIdAdditional02"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AdditionalDetails
    {
      get { return Get(ref _additionalDetails, "AdditionalDetails"); }
      set { Set(ref _additionalDetails, value, "AdditionalDetails"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return Get(ref _createdOn, "CreatedOn"); }
      set { Set(ref _createdOn, value, "CreatedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ActionEventStatus ActivityStatusId
    {
      get { return Get(ref _activityStatusId, "ActivityStatusId"); }
      set { Set(ref _activityStatusId, value, "ActivityStatusId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ActivityDeletedOn
    {
      get { return Get(ref _activityDeletedOn, "ActivityDeletedOn"); }
      set { Set(ref _activityDeletedOn, value, "ActivityDeletedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> ActivityUpdatedBy
    {
      get { return Get(ref _activityUpdatedBy, "ActivityUpdatedBy"); }
      set { Set(ref _activityUpdatedBy, value, "ActivityUpdatedBy"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="ActionType" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long ActionTypeId
    {
      get { return Get(ref _actionTypeId, "ActionTypeId"); }
      set { Set(ref _actionTypeId, value, "ActionTypeId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="EntityType" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EntityTypeId
    {
      get { return Get(ref _entityTypeId, "EntityTypeId"); }
      set { Set(ref _entityTypeId, value, "EntityTypeId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Core.StatusLog")]
  public partial class StatusLog : Entity<long>
  {
    #region Fields
  
    private long _entityId;
    private System.Nullable<long> _originalStatus;
    private long _newStatus;
    private long _userId;
    private System.DateTime _actionedOn;
    private long _entityTypeId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the EntityId entity attribute.</summary>
    public const string EntityIdField = "EntityId";
    /// <summary>Identifies the OriginalStatus entity attribute.</summary>
    public const string OriginalStatusField = "OriginalStatus";
    /// <summary>Identifies the NewStatus entity attribute.</summary>
    public const string NewStatusField = "NewStatus";
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the ActionedOn entity attribute.</summary>
    public const string ActionedOnField = "ActionedOn";
    /// <summary>Identifies the EntityTypeId entity attribute.</summary>
    public const string EntityTypeIdField = "EntityTypeId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("StatusLogs")]
    private readonly EntityHolder<EntityType> _entityType = new EntityHolder<EntityType>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityType EntityType
    {
      get { return Get(_entityType); }
      set { Set(_entityType, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public long EntityId
    {
      get { return Get(ref _entityId, "EntityId"); }
      set { Set(ref _entityId, value, "EntityId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> OriginalStatus
    {
      get { return Get(ref _originalStatus, "OriginalStatus"); }
      set { Set(ref _originalStatus, value, "OriginalStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long NewStatus
    {
      get { return Get(ref _newStatus, "NewStatus"); }
      set { Set(ref _newStatus, value, "NewStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ActionedOn
    {
      get { return Get(ref _actionedOn, "ActionedOn"); }
      set { Set(ref _actionedOn, value, "ActionedOn"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="EntityType" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long EntityTypeId
    {
      get { return Get(ref _entityTypeId, "EntityTypeId"); }
      set { Set(ref _entityTypeId, value, "EntityTypeId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Core.Session")]
  public partial class Session : Entity<System.Guid>
  {
    #region Fields
  
    private long _userId;
    private System.DateTime _lastRequestOn;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the LastRequestOn entity attribute.</summary>
    public const string LastRequestOnField = "LastRequestOn";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Session")]
    private readonly EntityCollection<SessionState> _sessionStates = new EntityCollection<SessionState>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<SessionState> SessionStates
    {
      get { return Get(_sessionStates); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public long UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime LastRequestOn
    {
      get { return Get(ref _lastRequestOn, "LastRequestOn"); }
      set { Set(ref _lastRequestOn, value, "LastRequestOn"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Core.SessionState")]
  public partial class SessionState : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 100)]
    [ValidatePresence]
    private string _key;
    private byte[] _value;
    [ValidateLength(0, 200)]
    private string _typeOf;
    private long _size;
    private System.Guid _sessionId;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.Nullable<System.DateTime> _deletedOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the Value entity attribute.</summary>
    public const string ValueField = "Value";
    /// <summary>Identifies the TypeOf entity attribute.</summary>
    public const string TypeOfField = "TypeOf";
    /// <summary>Identifies the Size entity attribute.</summary>
    public const string SizeField = "Size";
    /// <summary>Identifies the SessionId entity attribute.</summary>
    public const string SessionIdField = "SessionId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("SessionStates")]
    private readonly EntityHolder<Session> _session = new EntityHolder<Session>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Session Session
    {
      get { return Get(_session); }
      set { Set(_session, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public byte[] Value
    {
      get { return Get(ref _value, "Value"); }
      set { Set(ref _value, value, "Value"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string TypeOf
    {
      get { return Get(ref _typeOf, "TypeOf"); }
      set { Set(ref _typeOf, value, "TypeOf"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long Size
    {
      get { return Get(ref _size, "Size"); }
      set { Set(ref _size, value, "Size"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Session" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Guid SessionId
    {
      get { return Get(ref _sessionId, "SessionId"); }
      set { Set(ref _sessionId, value, "SessionId"); }
    }
    /// <summary>Gets the time the entity was soft-deleted</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> DeletedOn
    {
      get { return _deletedOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Core.IntegrationLog", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class IntegrationLog : Entity<long>
  {
    #region Fields
  
    private Focus.Core.IntegrationPoint _integrationPoint;
    private long _requestededBy;
    private System.DateTime _requestedOn;
    private System.DateTime _requestStart;
    private System.DateTime _requestEnd;
    private Focus.Core.IntegrationOutcome _outcome;
    private System.Nullable<System.Guid> _requestId;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    private readonly System.DateTime _updatedOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the IntegrationPoint entity attribute.</summary>
    public const string IntegrationPointField = "IntegrationPoint";
    /// <summary>Identifies the RequestededBy entity attribute.</summary>
    public const string RequestededByField = "RequestededBy";
    /// <summary>Identifies the RequestedOn entity attribute.</summary>
    public const string RequestedOnField = "RequestedOn";
    /// <summary>Identifies the RequestStart entity attribute.</summary>
    public const string RequestStartField = "RequestStart";
    /// <summary>Identifies the RequestEnd entity attribute.</summary>
    public const string RequestEndField = "RequestEnd";
    /// <summary>Identifies the Outcome entity attribute.</summary>
    public const string OutcomeField = "Outcome";
    /// <summary>Identifies the RequestId entity attribute.</summary>
    public const string RequestIdField = "RequestId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("IntegrationLog")]
    private readonly EntityCollection<IntegrationException> _integrationExceptions = new EntityCollection<IntegrationException>();
    [ReverseAssociation("IntegrationLog")]
    private readonly EntityCollection<IntegrationRequestResponse> _integrationRequestResponses = new EntityCollection<IntegrationRequestResponse>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<IntegrationException> IntegrationExceptions
    {
      get { return Get(_integrationExceptions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<IntegrationRequestResponse> IntegrationRequestResponses
    {
      get { return Get(_integrationRequestResponses); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.IntegrationPoint IntegrationPoint
    {
      get { return Get(ref _integrationPoint, "IntegrationPoint"); }
      set { Set(ref _integrationPoint, value, "IntegrationPoint"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long RequestededBy
    {
      get { return Get(ref _requestededBy, "RequestededBy"); }
      set { Set(ref _requestededBy, value, "RequestededBy"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime RequestedOn
    {
      get { return Get(ref _requestedOn, "RequestedOn"); }
      set { Set(ref _requestedOn, value, "RequestedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime RequestStart
    {
      get { return Get(ref _requestStart, "RequestStart"); }
      set { Set(ref _requestStart, value, "RequestStart"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime RequestEnd
    {
      get { return Get(ref _requestEnd, "RequestEnd"); }
      set { Set(ref _requestEnd, value, "RequestEnd"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.IntegrationOutcome Outcome
    {
      get { return Get(ref _outcome, "Outcome"); }
      set { Set(ref _outcome, value, "Outcome"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.Guid> RequestId
    {
      get { return Get(ref _requestId, "RequestId"); }
      set { Set(ref _requestId, value, "RequestId"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }
    }

    /// <summary>Gets the time the entity was last updated</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime UpdatedOn
    {
      get { return _updatedOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Core.IntegrationException", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class IntegrationException : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 1000)]
    private string _message;
    [ValidatePresence]
    private string _stackTrace;
    private long _integrationLogId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Message entity attribute.</summary>
    public const string MessageField = "Message";
    /// <summary>Identifies the StackTrace entity attribute.</summary>
    public const string StackTraceField = "StackTrace";
    /// <summary>Identifies the IntegrationLogId entity attribute.</summary>
    public const string IntegrationLogIdField = "IntegrationLogId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("IntegrationExceptions")]
    private readonly EntityHolder<IntegrationLog> _integrationLog = new EntityHolder<IntegrationLog>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public IntegrationLog IntegrationLog
    {
      get { return Get(_integrationLog); }
      set { Set(_integrationLog, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Message
    {
      get { return Get(ref _message, "Message"); }
      set { Set(ref _message, value, "Message"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string StackTrace
    {
      get { return Get(ref _stackTrace, "StackTrace"); }
      set { Set(ref _stackTrace, value, "StackTrace"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="IntegrationLog" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long IntegrationLogId
    {
      get { return Get(ref _integrationLogId, "IntegrationLogId"); }
      set { Set(ref _integrationLogId, value, "IntegrationLogId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Core.IntegrationRequestResponse", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class IntegrationRequestResponse : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 1000)]
    private string _url;
    private System.DateTime _requestSentOn;
    private System.Nullable<System.DateTime> _responseReceivedOn;
    private System.Nullable<int> _statusCode;
    [ValidateLength(0, 250)]
    private string _statusDescription;
    private long _integrationLogId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Url entity attribute.</summary>
    public const string UrlField = "Url";
    /// <summary>Identifies the RequestSentOn entity attribute.</summary>
    public const string RequestSentOnField = "RequestSentOn";
    /// <summary>Identifies the ResponseReceivedOn entity attribute.</summary>
    public const string ResponseReceivedOnField = "ResponseReceivedOn";
    /// <summary>Identifies the StatusCode entity attribute.</summary>
    public const string StatusCodeField = "StatusCode";
    /// <summary>Identifies the StatusDescription entity attribute.</summary>
    public const string StatusDescriptionField = "StatusDescription";
    /// <summary>Identifies the IntegrationLogId entity attribute.</summary>
    public const string IntegrationLogIdField = "IntegrationLogId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("IntegrationRequestResponse")]
    private readonly EntityCollection<IntegrationRequestResponseMessages> _integrationRequestResponseMessages = new EntityCollection<IntegrationRequestResponseMessages>();
    [ReverseAssociation("IntegrationRequestResponses")]
    private readonly EntityHolder<IntegrationLog> _integrationLog = new EntityHolder<IntegrationLog>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<IntegrationRequestResponseMessages> IntegrationRequestResponseMessages
    {
      get { return Get(_integrationRequestResponseMessages); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public IntegrationLog IntegrationLog
    {
      get { return Get(_integrationLog); }
      set { Set(_integrationLog, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Url
    {
      get { return Get(ref _url, "Url"); }
      set { Set(ref _url, value, "Url"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime RequestSentOn
    {
      get { return Get(ref _requestSentOn, "RequestSentOn"); }
      set { Set(ref _requestSentOn, value, "RequestSentOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ResponseReceivedOn
    {
      get { return Get(ref _responseReceivedOn, "ResponseReceivedOn"); }
      set { Set(ref _responseReceivedOn, value, "ResponseReceivedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> StatusCode
    {
      get { return Get(ref _statusCode, "StatusCode"); }
      set { Set(ref _statusCode, value, "StatusCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string StatusDescription
    {
      get { return Get(ref _statusDescription, "StatusDescription"); }
      set { Set(ref _statusDescription, value, "StatusDescription"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="IntegrationLog" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long IntegrationLogId
    {
      get { return Get(ref _integrationLogId, "IntegrationLogId"); }
      set { Set(ref _integrationLogId, value, "IntegrationLogId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Core.IntegrationRequestResponseMessages", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class IntegrationRequestResponseMessages : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    private string _requestMessage;
    private string _responseMessage;
    private long _integrationRequestResponseId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the RequestMessage entity attribute.</summary>
    public const string RequestMessageField = "RequestMessage";
    /// <summary>Identifies the ResponseMessage entity attribute.</summary>
    public const string ResponseMessageField = "ResponseMessage";
    /// <summary>Identifies the IntegrationRequestResponseId entity attribute.</summary>
    public const string IntegrationRequestResponseIdField = "IntegrationRequestResponseId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("IntegrationRequestResponseMessages")]
    private readonly EntityHolder<IntegrationRequestResponse> _integrationRequestResponse = new EntityHolder<IntegrationRequestResponse>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public IntegrationRequestResponse IntegrationRequestResponse
    {
      get { return Get(_integrationRequestResponse); }
      set { Set(_integrationRequestResponse, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string RequestMessage
    {
      get { return Get(ref _requestMessage, "RequestMessage"); }
      set { Set(ref _requestMessage, value, "RequestMessage"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ResponseMessage
    {
      get { return Get(ref _responseMessage, "ResponseMessage"); }
      set { Set(ref _responseMessage, value, "ResponseMessage"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="IntegrationRequestResponse" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long IntegrationRequestResponseId
    {
      get { return Get(ref _integrationRequestResponseId, "IntegrationRequestResponseId"); }
      set { Set(ref _integrationRequestResponseId, value, "IntegrationRequestResponseId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Core.IntegrationImportRecord", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class IntegrationImportRecord : Entity<long>
  {
    #region Fields
  
    private Focus.Core.IntegrationImportRecordType _recordType;
    [ValidatePresence]
    [ValidateLength(0, 255)]
    private string _externalId;
    private Focus.Core.IntegrationImportRecordStatus _status;
    private System.Nullable<System.DateTime> _importStartTime;
    private System.Nullable<System.DateTime> _importEndTime;
    [ValidateLength(0, 2000)]
    private string _details;
    private System.Nullable<System.Guid> _requestId;
    private System.Nullable<long> _focusId;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the RecordType entity attribute.</summary>
    public const string RecordTypeField = "RecordType";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";
    /// <summary>Identifies the Status entity attribute.</summary>
    public const string StatusField = "Status";
    /// <summary>Identifies the ImportStartTime entity attribute.</summary>
    public const string ImportStartTimeField = "ImportStartTime";
    /// <summary>Identifies the ImportEndTime entity attribute.</summary>
    public const string ImportEndTimeField = "ImportEndTime";
    /// <summary>Identifies the Details entity attribute.</summary>
    public const string DetailsField = "Details";
    /// <summary>Identifies the RequestId entity attribute.</summary>
    public const string RequestIdField = "RequestId";
    /// <summary>Identifies the FocusId entity attribute.</summary>
    public const string FocusIdField = "FocusId";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.IntegrationImportRecordType RecordType
    {
      get { return Get(ref _recordType, "RecordType"); }
      set { Set(ref _recordType, value, "RecordType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.IntegrationImportRecordStatus Status
    {
      get { return Get(ref _status, "Status"); }
      set { Set(ref _status, value, "Status"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ImportStartTime
    {
      get { return Get(ref _importStartTime, "ImportStartTime"); }
      set { Set(ref _importStartTime, value, "ImportStartTime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ImportEndTime
    {
      get { return Get(ref _importEndTime, "ImportEndTime"); }
      set { Set(ref _importEndTime, value, "ImportEndTime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Details
    {
      get { return Get(ref _details, "Details"); }
      set { Set(ref _details, value, "Details"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.Guid> RequestId
    {
      get { return Get(ref _requestId, "RequestId"); }
      set { Set(ref _requestId, value, "RequestId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> FocusId
    {
      get { return Get(ref _focusId, "FocusId"); }
      set { Set(ref _focusId, value, "FocusId"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Core.BackdatedActionEvents")]
  public partial class BackdatedActionEvents : Entity<long>
  {
    #region Fields
  
    private long _actionId;
    private System.Guid _sessionId;
    private System.Guid _requestId;
    private long _userId;
    private System.DateTime _actionedOn;
    private System.Nullable<long> _entityId;
    private System.Nullable<long> _entityIdAdditional01;
    private System.Nullable<long> _entityIdAdditional02;
    private string _additionalDetails;
    private System.DateTime _createdOn;
    private long _actionTypeId;
    private System.Nullable<long> _entityTypeId;
    private System.DateTime _actionBackdatedOn;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ActionId entity attribute.</summary>
    public const string ActionIdField = "ActionId";
    /// <summary>Identifies the SessionId entity attribute.</summary>
    public const string SessionIdField = "SessionId";
    /// <summary>Identifies the RequestId entity attribute.</summary>
    public const string RequestIdField = "RequestId";
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the ActionedOn entity attribute.</summary>
    public const string ActionedOnField = "ActionedOn";
    /// <summary>Identifies the EntityId entity attribute.</summary>
    public const string EntityIdField = "EntityId";
    /// <summary>Identifies the EntityIdAdditional01 entity attribute.</summary>
    public const string EntityIdAdditional01Field = "EntityIdAdditional01";
    /// <summary>Identifies the EntityIdAdditional02 entity attribute.</summary>
    public const string EntityIdAdditional02Field = "EntityIdAdditional02";
    /// <summary>Identifies the AdditionalDetails entity attribute.</summary>
    public const string AdditionalDetailsField = "AdditionalDetails";
    /// <summary>Identifies the CreatedOn entity attribute.</summary>
    public const string CreatedOnField = "CreatedOn";
    /// <summary>Identifies the ActionTypeId entity attribute.</summary>
    public const string ActionTypeIdField = "ActionTypeId";
    /// <summary>Identifies the EntityTypeId entity attribute.</summary>
    public const string EntityTypeIdField = "EntityTypeId";
    /// <summary>Identifies the ActionBackdatedOn entity attribute.</summary>
    public const string ActionBackdatedOnField = "ActionBackdatedOn";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public long ActionId
    {
      get { return Get(ref _actionId, "ActionId"); }
      set { Set(ref _actionId, value, "ActionId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Guid SessionId
    {
      get { return Get(ref _sessionId, "SessionId"); }
      set { Set(ref _sessionId, value, "SessionId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Guid RequestId
    {
      get { return Get(ref _requestId, "RequestId"); }
      set { Set(ref _requestId, value, "RequestId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ActionedOn
    {
      get { return Get(ref _actionedOn, "ActionedOn"); }
      set { Set(ref _actionedOn, value, "ActionedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EntityId
    {
      get { return Get(ref _entityId, "EntityId"); }
      set { Set(ref _entityId, value, "EntityId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EntityIdAdditional01
    {
      get { return Get(ref _entityIdAdditional01, "EntityIdAdditional01"); }
      set { Set(ref _entityIdAdditional01, value, "EntityIdAdditional01"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EntityIdAdditional02
    {
      get { return Get(ref _entityIdAdditional02, "EntityIdAdditional02"); }
      set { Set(ref _entityIdAdditional02, value, "EntityIdAdditional02"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AdditionalDetails
    {
      get { return Get(ref _additionalDetails, "AdditionalDetails"); }
      set { Set(ref _additionalDetails, value, "AdditionalDetails"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return Get(ref _createdOn, "CreatedOn"); }
      set { Set(ref _createdOn, value, "CreatedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long ActionTypeId
    {
      get { return Get(ref _actionTypeId, "ActionTypeId"); }
      set { Set(ref _actionTypeId, value, "ActionTypeId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EntityTypeId
    {
      get { return Get(ref _entityTypeId, "EntityTypeId"); }
      set { Set(ref _entityTypeId, value, "EntityTypeId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ActionBackdatedOn
    {
      get { return Get(ref _actionBackdatedOn, "ActionBackdatedOn"); }
      set { Set(ref _actionBackdatedOn, value, "ActionBackdatedOn"); }
    }

    #endregion
  }
	


  /// <summary>
  /// Provides a strong-typed unit of work for working with the DataCore model.
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  public partial class DataCoreUnitOfWork : Mindscape.LightSpeed.UnitOfWork
  {

    public System.Linq.IQueryable<SingleSignOn> SingleSignOns
    {
      get { return this.Query<SingleSignOn>(); }
    }
    
    public System.Linq.IQueryable<ActionType> ActionTypes
    {
      get { return this.Query<ActionType>(); }
    }
    
    public System.Linq.IQueryable<EntityType> EntityTypes
    {
      get { return this.Query<EntityType>(); }
    }
    
    public System.Linq.IQueryable<ActionEvent> ActionEvents
    {
      get { return this.Query<ActionEvent>(); }
    }
    
    public System.Linq.IQueryable<StatusLog> StatusLogs
    {
      get { return this.Query<StatusLog>(); }
    }
    
    public System.Linq.IQueryable<Session> Sessions
    {
      get { return this.Query<Session>(); }
    }
    
    public System.Linq.IQueryable<SessionState> SessionStates
    {
      get { return this.Query<SessionState>(); }
    }
    
    public System.Linq.IQueryable<IntegrationLog> IntegrationLogs
    {
      get { return this.Query<IntegrationLog>(); }
    }
    
    public System.Linq.IQueryable<IntegrationException> IntegrationExceptions
    {
      get { return this.Query<IntegrationException>(); }
    }
    
    public System.Linq.IQueryable<IntegrationRequestResponse> IntegrationRequestResponses
    {
      get { return this.Query<IntegrationRequestResponse>(); }
    }
    
    public System.Linq.IQueryable<IntegrationRequestResponseMessages> IntegrationRequestResponseMessages
    {
      get { return this.Query<IntegrationRequestResponseMessages>(); }
    }
    
    public System.Linq.IQueryable<IntegrationImportRecord> IntegrationImportRecords
    {
      get { return this.Query<IntegrationImportRecord>(); }
    }
    
    public System.Linq.IQueryable<BackdatedActionEvents> BackdatedActionEvents
    {
      get { return this.Query<BackdatedActionEvents>(); }
    }
    
  }
}

#endregion


#region Data Transfer Objects - Classes

/*
namespace Focus.Core.DataTransferObjects.FocusCore.Entities
{
  using System;
  using System.Runtime.Serialization;

    [Serializable]
    [DataContract(Name="SingleSignOn", Namespace = Constants.DataContractNamespace)]
    public class SingleSignOnDto
    {
      [DataMember]
      public System.Guid? Id { get; set; }
      [DataMember]
      public DateTime CreatedOn { get; set; }
      [DataMember]
      public long UserId { get; set; }
      [DataMember]
      public long ActionerId { get; set; }
      [DataMember]
      public string ExternalAdminId { get; set; }
      [DataMember]
      public string ExternalOfficeId { get; set; }
      [DataMember]
      public string ExternalPassword { get; set; }
      [DataMember]
      public string ExternalUsername { get; set; }
    }

    [Serializable]
    [DataContract(Name="ActionType", Namespace = Constants.DataContractNamespace)]
    public class ActionTypeDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public bool ReportOn { get; set; }
      [DataMember]
      public bool? AssistAction { get; set; }
    }

    [Serializable]
    [DataContract(Name="EntityType", Namespace = Constants.DataContractNamespace)]
    public class EntityTypeDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
    }

    [Serializable]
    [DataContract(Name="ActionEvent", Namespace = Constants.DataContractNamespace)]
    public class ActionEventDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public Guid SessionId { get; set; }
      [DataMember]
      public Guid RequestId { get; set; }
      [DataMember]
      public long UserId { get; set; }
      [DataMember]
      public DateTime ActionedOn { get; set; }
      [DataMember]
      public long? EntityId { get; set; }
      [DataMember]
      public long? EntityIdAdditional01 { get; set; }
      [DataMember]
      public long? EntityIdAdditional02 { get; set; }
      [DataMember]
      public string AdditionalDetails { get; set; }
      [DataMember]
      public DateTime CreatedOn { get; set; }
      [DataMember]
      public ActionEventStatus ActivityStatusId { get; set; }
      [DataMember]
      public DateTime? ActivityDeletedOn { get; set; }
      [DataMember]
      public long? ActivityUpdatedBy { get; set; }
      [DataMember]
      public long ActionTypeId { get; set; }
      [DataMember]
      public long? EntityTypeId { get; set; }
    }

    [Serializable]
    [DataContract(Name="StatusLog", Namespace = Constants.DataContractNamespace)]
    public class StatusLogDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long EntityId { get; set; }
      [DataMember]
      public long? OriginalStatus { get; set; }
      [DataMember]
      public long NewStatus { get; set; }
      [DataMember]
      public long UserId { get; set; }
      [DataMember]
      public DateTime ActionedOn { get; set; }
      [DataMember]
      public long EntityTypeId { get; set; }
    }

    [Serializable]
    [DataContract(Name="Session", Namespace = Constants.DataContractNamespace)]
    public class SessionDto
    {
      [DataMember]
      public System.Guid? Id { get; set; }
      [DataMember]
      public long UserId { get; set; }
      [DataMember]
      public DateTime LastRequestOn { get; set; }
    }

    [Serializable]
    [DataContract(Name="SessionState", Namespace = Constants.DataContractNamespace)]
    public class SessionStateDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public DateTime? DeletedOn { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public byte[] Value { get; set; }
      [DataMember]
      public string TypeOf { get; set; }
      [DataMember]
      public long Size { get; set; }
      [DataMember]
      public Guid SessionId { get; set; }
    }

    [Serializable]
    [DataContract(Name="IntegrationLog", Namespace = Constants.DataContractNamespace)]
    public class IntegrationLogDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public DateTime CreatedOn { get; set; }
      [DataMember]
      public DateTime UpdatedOn { get; set; }
      [DataMember]
      public IntegrationPoint IntegrationPoint { get; set; }
      [DataMember]
      public long RequestededBy { get; set; }
      [DataMember]
      public DateTime RequestedOn { get; set; }
      [DataMember]
      public DateTime RequestStart { get; set; }
      [DataMember]
      public DateTime RequestEnd { get; set; }
      [DataMember]
      public IntegrationOutcome Outcome { get; set; }
      [DataMember]
      public Guid? RequestId { get; set; }
    }

    [Serializable]
    [DataContract(Name="IntegrationException", Namespace = Constants.DataContractNamespace)]
    public class IntegrationExceptionDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Message { get; set; }
      [DataMember]
      public string StackTrace { get; set; }
      [DataMember]
      public long IntegrationLogId { get; set; }
    }

    [Serializable]
    [DataContract(Name="IntegrationRequestResponse", Namespace = Constants.DataContractNamespace)]
    public class IntegrationRequestResponseDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Url { get; set; }
      [DataMember]
      public DateTime RequestSentOn { get; set; }
      [DataMember]
      public DateTime? ResponseReceivedOn { get; set; }
      [DataMember]
      public int? StatusCode { get; set; }
      [DataMember]
      public string StatusDescription { get; set; }
      [DataMember]
      public long IntegrationLogId { get; set; }
    }

    [Serializable]
    [DataContract(Name="IntegrationRequestResponseMessages", Namespace = Constants.DataContractNamespace)]
    public class IntegrationRequestResponseMessagesDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string RequestMessage { get; set; }
      [DataMember]
      public string ResponseMessage { get; set; }
      [DataMember]
      public long IntegrationRequestResponseId { get; set; }
    }

    [Serializable]
    [DataContract(Name="IntegrationImportRecord", Namespace = Constants.DataContractNamespace)]
    public class IntegrationImportRecordDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public DateTime CreatedOn { get; set; }
      [DataMember]
      public IntegrationImportRecordType RecordType { get; set; }
      [DataMember]
      public string ExternalId { get; set; }
      [DataMember]
      public IntegrationImportRecordStatus Status { get; set; }
      [DataMember]
      public DateTime? ImportStartTime { get; set; }
      [DataMember]
      public DateTime? ImportEndTime { get; set; }
      [DataMember]
      public string Details { get; set; }
      [DataMember]
      public Guid? RequestId { get; set; }
      [DataMember]
      public long? FocusId { get; set; }
    }

    [Serializable]
    [DataContract(Name="BackdatedActionEvents", Namespace = Constants.DataContractNamespace)]
    public class BackdatedActionEventsDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long ActionId { get; set; }
      [DataMember]
      public Guid SessionId { get; set; }
      [DataMember]
      public Guid RequestId { get; set; }
      [DataMember]
      public long UserId { get; set; }
      [DataMember]
      public DateTime ActionedOn { get; set; }
      [DataMember]
      public long? EntityId { get; set; }
      [DataMember]
      public long? EntityIdAdditional01 { get; set; }
      [DataMember]
      public long? EntityIdAdditional02 { get; set; }
      [DataMember]
      public string AdditionalDetails { get; set; }
      [DataMember]
      public DateTime CreatedOn { get; set; }
      [DataMember]
      public long ActionTypeId { get; set; }
      [DataMember]
      public long? EntityTypeId { get; set; }
      [DataMember]
      public DateTime ActionBackdatedOn { get; set; }
    }


}
*/

#endregion

#region Data Transfer Objects - Mappers

/*
namespace Focus.Services.DtoMappers
{
    using Focus.Core.DataTransferObjects.FocusCore.Entities;
    using Focus.Data.Core.Entities;

    public static class FocusCore.EntitiesMappers
    {

      #region SingleSignOnDto Mappers

      public static SingleSignOnDto AsDto(this SingleSignOn entity)
      {
        var dto = new SingleSignOnDto
        {
				  CreatedOn =  entity.CreatedOn,
          UserId = entity.UserId,
          ActionerId = entity.ActionerId,
          ExternalAdminId = entity.ExternalAdminId,
          ExternalOfficeId = entity.ExternalOfficeId,
          ExternalPassword = entity.ExternalPassword,
          ExternalUsername = entity.ExternalUsername,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static SingleSignOn CopyTo(this SingleSignOnDto dto, SingleSignOn entity)
      {
        entity.UserId = dto.UserId;
        entity.ActionerId = dto.ActionerId;
        entity.ExternalAdminId = dto.ExternalAdminId;
        entity.ExternalOfficeId = dto.ExternalOfficeId;
        entity.ExternalPassword = dto.ExternalPassword;
        entity.ExternalUsername = dto.ExternalUsername;
        return entity;
      }

      public static SingleSignOn CopyTo(this SingleSignOnDto dto)
      {
        var entity = new SingleSignOn();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region ActionTypeDto Mappers

      public static ActionTypeDto AsDto(this ActionType entity)
      {
        var dto = new ActionTypeDto
        {
          Name = entity.Name,
          ReportOn = entity.ReportOn,
          AssistAction = entity.AssistAction,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static ActionType CopyTo(this ActionTypeDto dto, ActionType entity)
      {
        entity.Name = dto.Name;
        entity.ReportOn = dto.ReportOn;
        entity.AssistAction = dto.AssistAction;
        return entity;
      }

      public static ActionType CopyTo(this ActionTypeDto dto)
      {
        var entity = new ActionType();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region EntityTypeDto Mappers

      public static EntityTypeDto AsDto(this EntityType entity)
      {
        var dto = new EntityTypeDto
        {
          Name = entity.Name,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static EntityType CopyTo(this EntityTypeDto dto, EntityType entity)
      {
        entity.Name = dto.Name;
        return entity;
      }

      public static EntityType CopyTo(this EntityTypeDto dto)
      {
        var entity = new EntityType();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region ActionEventDto Mappers

      public static ActionEventDto AsDto(this ActionEvent entity)
      {
        var dto = new ActionEventDto
        {
          SessionId = entity.SessionId,
          RequestId = entity.RequestId,
          UserId = entity.UserId,
          ActionedOn = entity.ActionedOn,
          EntityId = entity.EntityId,
          EntityIdAdditional01 = entity.EntityIdAdditional01,
          EntityIdAdditional02 = entity.EntityIdAdditional02,
          AdditionalDetails = entity.AdditionalDetails,
          CreatedOn = entity.CreatedOn,
          ActivityStatusId = entity.ActivityStatusId,
          ActivityDeletedOn = entity.ActivityDeletedOn,
          ActivityUpdatedBy = entity.ActivityUpdatedBy,
          ActionTypeId = entity.ActionTypeId,
          EntityTypeId = entity.EntityTypeId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static ActionEvent CopyTo(this ActionEventDto dto, ActionEvent entity)
      {
        entity.SessionId = dto.SessionId;
        entity.RequestId = dto.RequestId;
        entity.UserId = dto.UserId;
        entity.ActionedOn = dto.ActionedOn;
        entity.EntityId = dto.EntityId;
        entity.EntityIdAdditional01 = dto.EntityIdAdditional01;
        entity.EntityIdAdditional02 = dto.EntityIdAdditional02;
        entity.AdditionalDetails = dto.AdditionalDetails;
        entity.CreatedOn = dto.CreatedOn;
        entity.ActivityStatusId = dto.ActivityStatusId;
        entity.ActivityDeletedOn = dto.ActivityDeletedOn;
        entity.ActivityUpdatedBy = dto.ActivityUpdatedBy;
        entity.ActionTypeId = dto.ActionTypeId;
        entity.EntityTypeId = dto.EntityTypeId;
        return entity;
      }

      public static ActionEvent CopyTo(this ActionEventDto dto)
      {
        var entity = new ActionEvent();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region StatusLogDto Mappers

      public static StatusLogDto AsDto(this StatusLog entity)
      {
        var dto = new StatusLogDto
        {
          EntityId = entity.EntityId,
          OriginalStatus = entity.OriginalStatus,
          NewStatus = entity.NewStatus,
          UserId = entity.UserId,
          ActionedOn = entity.ActionedOn,
          EntityTypeId = entity.EntityTypeId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static StatusLog CopyTo(this StatusLogDto dto, StatusLog entity)
      {
        entity.EntityId = dto.EntityId;
        entity.OriginalStatus = dto.OriginalStatus;
        entity.NewStatus = dto.NewStatus;
        entity.UserId = dto.UserId;
        entity.ActionedOn = dto.ActionedOn;
        entity.EntityTypeId = dto.EntityTypeId;
        return entity;
      }

      public static StatusLog CopyTo(this StatusLogDto dto)
      {
        var entity = new StatusLog();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region SessionDto Mappers

      public static SessionDto AsDto(this Session entity)
      {
        var dto = new SessionDto
        {
          UserId = entity.UserId,
          LastRequestOn = entity.LastRequestOn,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static Session CopyTo(this SessionDto dto, Session entity)
      {
        entity.UserId = dto.UserId;
        entity.LastRequestOn = dto.LastRequestOn;
        return entity;
      }

      public static Session CopyTo(this SessionDto dto)
      {
        var entity = new Session();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region SessionStateDto Mappers

      public static SessionStateDto AsDto(this SessionState entity)
      {
        var dto = new SessionStateDto
        {
				  DeletedOn =  entity.DeletedOn,
          Key = entity.Key,
          Value = entity.Value,
          TypeOf = entity.TypeOf,
          Size = entity.Size,
          SessionId = entity.SessionId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static SessionState CopyTo(this SessionStateDto dto, SessionState entity)
      {
        entity.Key = dto.Key;
        entity.Value = dto.Value;
        entity.TypeOf = dto.TypeOf;
        entity.Size = dto.Size;
        entity.SessionId = dto.SessionId;
        return entity;
      }

      public static SessionState CopyTo(this SessionStateDto dto)
      {
        var entity = new SessionState();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region IntegrationLogDto Mappers

      public static IntegrationLogDto AsDto(this IntegrationLog entity)
      {
        var dto = new IntegrationLogDto
        {
				  CreatedOn =  entity.CreatedOn,
				  UpdatedOn =  entity.UpdatedOn,
          IntegrationPoint = entity.IntegrationPoint,
          RequestededBy = entity.RequestededBy,
          RequestedOn = entity.RequestedOn,
          RequestStart = entity.RequestStart,
          RequestEnd = entity.RequestEnd,
          Outcome = entity.Outcome,
          RequestId = entity.RequestId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static IntegrationLog CopyTo(this IntegrationLogDto dto, IntegrationLog entity)
      {
        entity.IntegrationPoint = dto.IntegrationPoint;
        entity.RequestededBy = dto.RequestededBy;
        entity.RequestedOn = dto.RequestedOn;
        entity.RequestStart = dto.RequestStart;
        entity.RequestEnd = dto.RequestEnd;
        entity.Outcome = dto.Outcome;
        entity.RequestId = dto.RequestId;
        return entity;
      }

      public static IntegrationLog CopyTo(this IntegrationLogDto dto)
      {
        var entity = new IntegrationLog();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region IntegrationExceptionDto Mappers

      public static IntegrationExceptionDto AsDto(this IntegrationException entity)
      {
        var dto = new IntegrationExceptionDto
        {
          Message = entity.Message,
          StackTrace = entity.StackTrace,
          IntegrationLogId = entity.IntegrationLogId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static IntegrationException CopyTo(this IntegrationExceptionDto dto, IntegrationException entity)
      {
        entity.Message = dto.Message;
        entity.StackTrace = dto.StackTrace;
        entity.IntegrationLogId = dto.IntegrationLogId;
        return entity;
      }

      public static IntegrationException CopyTo(this IntegrationExceptionDto dto)
      {
        var entity = new IntegrationException();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region IntegrationRequestResponseDto Mappers

      public static IntegrationRequestResponseDto AsDto(this IntegrationRequestResponse entity)
      {
        var dto = new IntegrationRequestResponseDto
        {
          Url = entity.Url,
          RequestSentOn = entity.RequestSentOn,
          ResponseReceivedOn = entity.ResponseReceivedOn,
          StatusCode = entity.StatusCode,
          StatusDescription = entity.StatusDescription,
          IntegrationLogId = entity.IntegrationLogId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static IntegrationRequestResponse CopyTo(this IntegrationRequestResponseDto dto, IntegrationRequestResponse entity)
      {
        entity.Url = dto.Url;
        entity.RequestSentOn = dto.RequestSentOn;
        entity.ResponseReceivedOn = dto.ResponseReceivedOn;
        entity.StatusCode = dto.StatusCode;
        entity.StatusDescription = dto.StatusDescription;
        entity.IntegrationLogId = dto.IntegrationLogId;
        return entity;
      }

      public static IntegrationRequestResponse CopyTo(this IntegrationRequestResponseDto dto)
      {
        var entity = new IntegrationRequestResponse();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region IntegrationRequestResponseMessagesDto Mappers

      public static IntegrationRequestResponseMessagesDto AsDto(this IntegrationRequestResponseMessages entity)
      {
        var dto = new IntegrationRequestResponseMessagesDto
        {
          RequestMessage = entity.RequestMessage,
          ResponseMessage = entity.ResponseMessage,
          IntegrationRequestResponseId = entity.IntegrationRequestResponseId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static IntegrationRequestResponseMessages CopyTo(this IntegrationRequestResponseMessagesDto dto, IntegrationRequestResponseMessages entity)
      {
        entity.RequestMessage = dto.RequestMessage;
        entity.ResponseMessage = dto.ResponseMessage;
        entity.IntegrationRequestResponseId = dto.IntegrationRequestResponseId;
        return entity;
      }

      public static IntegrationRequestResponseMessages CopyTo(this IntegrationRequestResponseMessagesDto dto)
      {
        var entity = new IntegrationRequestResponseMessages();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region IntegrationImportRecordDto Mappers

      public static IntegrationImportRecordDto AsDto(this IntegrationImportRecord entity)
      {
        var dto = new IntegrationImportRecordDto
        {
				  CreatedOn =  entity.CreatedOn,
          RecordType = entity.RecordType,
          ExternalId = entity.ExternalId,
          Status = entity.Status,
          ImportStartTime = entity.ImportStartTime,
          ImportEndTime = entity.ImportEndTime,
          Details = entity.Details,
          RequestId = entity.RequestId,
          FocusId = entity.FocusId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static IntegrationImportRecord CopyTo(this IntegrationImportRecordDto dto, IntegrationImportRecord entity)
      {
        entity.RecordType = dto.RecordType;
        entity.ExternalId = dto.ExternalId;
        entity.Status = dto.Status;
        entity.ImportStartTime = dto.ImportStartTime;
        entity.ImportEndTime = dto.ImportEndTime;
        entity.Details = dto.Details;
        entity.RequestId = dto.RequestId;
        entity.FocusId = dto.FocusId;
        return entity;
      }

      public static IntegrationImportRecord CopyTo(this IntegrationImportRecordDto dto)
      {
        var entity = new IntegrationImportRecord();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region BackdatedActionEventsDto Mappers

      public static BackdatedActionEventsDto AsDto(this BackdatedActionEvents entity)
      {
        var dto = new BackdatedActionEventsDto
        {
          ActionId = entity.ActionId,
          SessionId = entity.SessionId,
          RequestId = entity.RequestId,
          UserId = entity.UserId,
          ActionedOn = entity.ActionedOn,
          EntityId = entity.EntityId,
          EntityIdAdditional01 = entity.EntityIdAdditional01,
          EntityIdAdditional02 = entity.EntityIdAdditional02,
          AdditionalDetails = entity.AdditionalDetails,
          CreatedOn = entity.CreatedOn,
          ActionTypeId = entity.ActionTypeId,
          EntityTypeId = entity.EntityTypeId,
          ActionBackdatedOn = entity.ActionBackdatedOn,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static BackdatedActionEvents CopyTo(this BackdatedActionEventsDto dto, BackdatedActionEvents entity)
      {
        entity.ActionId = dto.ActionId;
        entity.SessionId = dto.SessionId;
        entity.RequestId = dto.RequestId;
        entity.UserId = dto.UserId;
        entity.ActionedOn = dto.ActionedOn;
        entity.EntityId = dto.EntityId;
        entity.EntityIdAdditional01 = dto.EntityIdAdditional01;
        entity.EntityIdAdditional02 = dto.EntityIdAdditional02;
        entity.AdditionalDetails = dto.AdditionalDetails;
        entity.CreatedOn = dto.CreatedOn;
        entity.ActionTypeId = dto.ActionTypeId;
        entity.EntityTypeId = dto.EntityTypeId;
        entity.ActionBackdatedOn = dto.ActionBackdatedOn;
        return entity;
      }

      public static BackdatedActionEvents CopyTo(this BackdatedActionEventsDto dto)
      {
        var entity = new BackdatedActionEvents();
        return dto.CopyTo(entity);
      }	

      #endregion

    }
}
*/

#endregion
