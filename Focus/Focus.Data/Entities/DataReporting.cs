using System;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Validation;
using Mindscape.LightSpeed.Linq;

#region Entities

namespace Focus.Data.Reporting.Entities
{
  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Report.JobSeeker", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeeker : Entity<long>
  {
    #region Fields
  
    private System.Nullable<long> _careerJobSeekerId;
    [ValidateLength(0, 100)]
    private string _firstName;
    [ValidateLength(0, 100)]
    private string _lastName;
    [ValidateLength(0, 400)]
    private string _emailAddress;
    private System.Nullable<System.DateTime> _accountCreationDate;
    [ValidateLength(0, 400)]
    private string _lastJobTitle;
    [ValidateLength(0, 400)]
    private string _lastEmployer;
    private string _lastJobDescription;
    [ValueField]
    private System.Nullable<Focus.Core.ClaimantStatus> _claimantStatus;
    [ValueField]
    private System.Nullable<Focus.Core.VeteranDefinition> _veteranDefinition;
    [ValueField]
    private System.Nullable<Focus.Core.VeteranType> _veteranType;
    [ValueField]
    private System.Nullable<Focus.Core.VeteranEmploymentStatus> _veteranEmploymentStatus;
    [ValueField]
    private System.Nullable<Focus.Core.VeteranTransactionType> _veteranTransactionType;
    [ValueField]
    private System.Nullable<Focus.Core.VeteranMilitaryDischarge> _veteranMilitaryDischarge;
    [ValueField]
    private System.Nullable<Focus.Core.VeteranBranchOfService> _veteranBranchOfService;
    [ValueField]
    private System.Nullable<Focus.Core.EducationLevel> _educationLevel;
    [ValidateLength(0, 50)]
    private string _city;
    [ValidateLength(0, 50)]
    private string _clientStatus;
    private int _weeksSinceAccountCreated;
    [ValidateLength(0, 202)]
    private string _fullName;
    private System.Nullable<System.DateTime> _reportStartDate;
    private System.Nullable<int> _daysSinceRegistration;
    private System.Nullable<int> _resumeWordCount;
    private System.Nullable<int> _resumeSkillCount;
    private System.Nullable<System.DateTime> _lastLoginDate;
    [ValidateLength(0, 50)]
    private string _zipCode;
    [ValidateLength(0, 200)]
    private string _addressLine1;
    [ValidateLength(0, 5)]
    private string _stateKey;
    private long _candidateId;
    [ValidateLength(0, 20)]
    private string _clientId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the CareerJobSeekerId entity attribute.</summary>
    public const string CareerJobSeekerIdField = "CareerJobSeekerId";
    /// <summary>Identifies the FirstName entity attribute.</summary>
    public const string FirstNameField = "FirstName";
    /// <summary>Identifies the LastName entity attribute.</summary>
    public const string LastNameField = "LastName";
    /// <summary>Identifies the EmailAddress entity attribute.</summary>
    public const string EmailAddressField = "EmailAddress";
    /// <summary>Identifies the AccountCreationDate entity attribute.</summary>
    public const string AccountCreationDateField = "AccountCreationDate";
    /// <summary>Identifies the LastJobTitle entity attribute.</summary>
    public const string LastJobTitleField = "LastJobTitle";
    /// <summary>Identifies the LastEmployer entity attribute.</summary>
    public const string LastEmployerField = "LastEmployer";
    /// <summary>Identifies the LastJobDescription entity attribute.</summary>
    public const string LastJobDescriptionField = "LastJobDescription";
    /// <summary>Identifies the ClaimantStatus entity attribute.</summary>
    public const string ClaimantStatusField = "ClaimantStatus";
    /// <summary>Identifies the VeteranDefinition entity attribute.</summary>
    public const string VeteranDefinitionField = "VeteranDefinition";
    /// <summary>Identifies the VeteranType entity attribute.</summary>
    public const string VeteranTypeField = "VeteranType";
    /// <summary>Identifies the VeteranEmploymentStatus entity attribute.</summary>
    public const string VeteranEmploymentStatusField = "VeteranEmploymentStatus";
    /// <summary>Identifies the VeteranTransactionType entity attribute.</summary>
    public const string VeteranTransactionTypeField = "VeteranTransactionType";
    /// <summary>Identifies the VeteranMilitaryDischarge entity attribute.</summary>
    public const string VeteranMilitaryDischargeField = "VeteranMilitaryDischarge";
    /// <summary>Identifies the VeteranBranchOfService entity attribute.</summary>
    public const string VeteranBranchOfServiceField = "VeteranBranchOfService";
    /// <summary>Identifies the EducationLevel entity attribute.</summary>
    public const string EducationLevelField = "EducationLevel";
    /// <summary>Identifies the City entity attribute.</summary>
    public const string CityField = "City";
    /// <summary>Identifies the ClientStatus entity attribute.</summary>
    public const string ClientStatusField = "ClientStatus";
    /// <summary>Identifies the WeeksSinceAccountCreated entity attribute.</summary>
    public const string WeeksSinceAccountCreatedField = "WeeksSinceAccountCreated";
    /// <summary>Identifies the FullName entity attribute.</summary>
    public const string FullNameField = "FullName";
    /// <summary>Identifies the ReportStartDate entity attribute.</summary>
    public const string ReportStartDateField = "ReportStartDate";
    /// <summary>Identifies the DaysSinceRegistration entity attribute.</summary>
    public const string DaysSinceRegistrationField = "DaysSinceRegistration";
    /// <summary>Identifies the ResumeWordCount entity attribute.</summary>
    public const string ResumeWordCountField = "ResumeWordCount";
    /// <summary>Identifies the ResumeSkillCount entity attribute.</summary>
    public const string ResumeSkillCountField = "ResumeSkillCount";
    /// <summary>Identifies the LastLoginDate entity attribute.</summary>
    public const string LastLoginDateField = "LastLoginDate";
    /// <summary>Identifies the ZipCode entity attribute.</summary>
    public const string ZipCodeField = "ZipCode";
    /// <summary>Identifies the AddressLine1 entity attribute.</summary>
    public const string AddressLine1Field = "AddressLine1";
    /// <summary>Identifies the StateKey entity attribute.</summary>
    public const string StateKeyField = "StateKey";
    /// <summary>Identifies the CandidateId entity attribute.</summary>
    public const string CandidateIdField = "CandidateId";
    /// <summary>Identifies the ClientId entity attribute.</summary>
    public const string ClientIdField = "ClientId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSeeker")]
    private readonly EntityCollection<JobSeekerSkill> _jobSeekerSkills = new EntityCollection<JobSeekerSkill>();
    [ReverseAssociation("JobSeeker")]
    private readonly EntityCollection<JobSeekerServicesReceived> _jobSeekerServicesReceiveds = new EntityCollection<JobSeekerServicesReceived>();
    [ReverseAssociation("JobSeeker")]
    private readonly EntityCollection<JobSeekerCertificationLicense> _jobSeekerCertificationLicenses = new EntityCollection<JobSeekerCertificationLicense>();
    [ReverseAssociation("JobSeeker")]
    private readonly EntityCollection<JobSeekerApplication> _jobSeekerApplications = new EntityCollection<JobSeekerApplication>();
    [ReverseAssociation("JobSeeker")]
    private readonly EntityCollection<JobSeekerNote> _jobSeekerNotes = new EntityCollection<JobSeekerNote>();
    [ReverseAssociation("JobSeeker")]
    private readonly EntityCollection<JobSeekerCount> _jobSeekerCounts = new EntityCollection<JobSeekerCount>();
    [ReverseAssociation("JobSeeker")]
    private readonly EntityCollection<JobSeekerActivity> _jobSeekerActivities = new EntityCollection<JobSeekerActivity>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSeekerSkill> JobSeekerSkills
    {
      get { return Get(_jobSeekerSkills); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSeekerServicesReceived> JobSeekerServicesReceiveds
    {
      get { return Get(_jobSeekerServicesReceiveds); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSeekerCertificationLicense> JobSeekerCertificationLicenses
    {
      get { return Get(_jobSeekerCertificationLicenses); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSeekerApplication> JobSeekerApplications
    {
      get { return Get(_jobSeekerApplications); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSeekerNote> JobSeekerNotes
    {
      get { return Get(_jobSeekerNotes); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSeekerCount> JobSeekerCounts
    {
      get { return Get(_jobSeekerCounts); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSeekerActivity> JobSeekerActivities
    {
      get { return Get(_jobSeekerActivities); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> CareerJobSeekerId
    {
      get { return Get(ref _careerJobSeekerId, "CareerJobSeekerId"); }
      set { Set(ref _careerJobSeekerId, value, "CareerJobSeekerId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string FirstName
    {
      get { return Get(ref _firstName, "FirstName"); }
      set { Set(ref _firstName, value, "FirstName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string LastName
    {
      get { return Get(ref _lastName, "LastName"); }
      set { Set(ref _lastName, value, "LastName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string EmailAddress
    {
      get { return Get(ref _emailAddress, "EmailAddress"); }
      set { Set(ref _emailAddress, value, "EmailAddress"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> AccountCreationDate
    {
      get { return Get(ref _accountCreationDate, "AccountCreationDate"); }
      set { Set(ref _accountCreationDate, value, "AccountCreationDate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string LastJobTitle
    {
      get { return Get(ref _lastJobTitle, "LastJobTitle"); }
      set { Set(ref _lastJobTitle, value, "LastJobTitle"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string LastEmployer
    {
      get { return Get(ref _lastEmployer, "LastEmployer"); }
      set { Set(ref _lastEmployer, value, "LastEmployer"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string LastJobDescription
    {
      get { return Get(ref _lastJobDescription, "LastJobDescription"); }
      set { Set(ref _lastJobDescription, value, "LastJobDescription"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ClaimantStatus> ClaimantStatus
    {
      get { return Get(ref _claimantStatus, "ClaimantStatus"); }
      set { Set(ref _claimantStatus, value, "ClaimantStatus"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.VeteranDefinition> VeteranDefinition
    {
      get { return Get(ref _veteranDefinition, "VeteranDefinition"); }
      set { Set(ref _veteranDefinition, value, "VeteranDefinition"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.VeteranType> VeteranType
    {
      get { return Get(ref _veteranType, "VeteranType"); }
      set { Set(ref _veteranType, value, "VeteranType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.VeteranEmploymentStatus> VeteranEmploymentStatus
    {
      get { return Get(ref _veteranEmploymentStatus, "VeteranEmploymentStatus"); }
      set { Set(ref _veteranEmploymentStatus, value, "VeteranEmploymentStatus"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.VeteranTransactionType> VeteranTransactionType
    {
      get { return Get(ref _veteranTransactionType, "VeteranTransactionType"); }
      set { Set(ref _veteranTransactionType, value, "VeteranTransactionType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.VeteranMilitaryDischarge> VeteranMilitaryDischarge
    {
      get { return Get(ref _veteranMilitaryDischarge, "VeteranMilitaryDischarge"); }
      set { Set(ref _veteranMilitaryDischarge, value, "VeteranMilitaryDischarge"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.VeteranBranchOfService> VeteranBranchOfService
    {
      get { return Get(ref _veteranBranchOfService, "VeteranBranchOfService"); }
      set { Set(ref _veteranBranchOfService, value, "VeteranBranchOfService"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.EducationLevel> EducationLevel
    {
      get { return Get(ref _educationLevel, "EducationLevel"); }
      set { Set(ref _educationLevel, value, "EducationLevel"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string City
    {
      get { return Get(ref _city, "City"); }
      set { Set(ref _city, value, "City"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ClientStatus
    {
      get { return Get(ref _clientStatus, "ClientStatus"); }
      set { Set(ref _clientStatus, value, "ClientStatus"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int WeeksSinceAccountCreated
    {
      get { return Get(ref _weeksSinceAccountCreated, "WeeksSinceAccountCreated"); }
      set { Set(ref _weeksSinceAccountCreated, value, "WeeksSinceAccountCreated"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string FullName
    {
      get { return Get(ref _fullName, "FullName"); }
      set { Set(ref _fullName, value, "FullName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ReportStartDate
    {
      get { return Get(ref _reportStartDate, "ReportStartDate"); }
      set { Set(ref _reportStartDate, value, "ReportStartDate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> DaysSinceRegistration
    {
      get { return Get(ref _daysSinceRegistration, "DaysSinceRegistration"); }
      set { Set(ref _daysSinceRegistration, value, "DaysSinceRegistration"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> ResumeWordCount
    {
      get { return Get(ref _resumeWordCount, "ResumeWordCount"); }
      set { Set(ref _resumeWordCount, value, "ResumeWordCount"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> ResumeSkillCount
    {
      get { return Get(ref _resumeSkillCount, "ResumeSkillCount"); }
      set { Set(ref _resumeSkillCount, value, "ResumeSkillCount"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> LastLoginDate
    {
      get { return Get(ref _lastLoginDate, "LastLoginDate"); }
      set { Set(ref _lastLoginDate, value, "LastLoginDate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ZipCode
    {
      get { return Get(ref _zipCode, "ZipCode"); }
      set { Set(ref _zipCode, value, "ZipCode"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string AddressLine1
    {
      get { return Get(ref _addressLine1, "AddressLine1"); }
      set { Set(ref _addressLine1, value, "AddressLine1"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string StateKey
    {
      get { return Get(ref _stateKey, "StateKey"); }
      set { Set(ref _stateKey, value, "StateKey"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long CandidateId
    {
      get { return Get(ref _candidateId, "CandidateId"); }
      set { Set(ref _candidateId, value, "CandidateId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ClientId
    {
      get { return Get(ref _clientId, "ClientId"); }
      set { Set(ref _clientId, value, "ClientId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Report.JobSeekerSkill", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeekerSkill : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    private string _skill;
    private long _jobSeekerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Skill entity attribute.</summary>
    public const string SkillField = "Skill";
    /// <summary>Identifies the JobSeekerId entity attribute.</summary>
    public const string JobSeekerIdField = "JobSeekerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSeekerSkills")]
    private readonly EntityHolder<JobSeeker> _jobSeeker = new EntityHolder<JobSeeker>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public JobSeeker JobSeeker
    {
      get { return Get(_jobSeeker); }
      set { Set(_jobSeeker, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Skill
    {
      get { return Get(ref _skill, "Skill"); }
      set { Set(ref _skill, value, "Skill"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobSeeker" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobSeekerId
    {
      get { return Get(ref _jobSeekerId, "JobSeekerId"); }
      set { Set(ref _jobSeekerId, value, "JobSeekerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Report.JobSeekerServicesReceived", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeekerServicesReceived : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 100)]
    private string _serviceReceived;
    private System.Nullable<long> _jobSeekerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ServiceReceived entity attribute.</summary>
    public const string ServiceReceivedField = "ServiceReceived";
    /// <summary>Identifies the JobSeekerId entity attribute.</summary>
    public const string JobSeekerIdField = "JobSeekerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSeekerServicesReceiveds")]
    private readonly EntityHolder<JobSeeker> _jobSeeker = new EntityHolder<JobSeeker>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public JobSeeker JobSeeker
    {
      get { return Get(_jobSeeker); }
      set { Set(_jobSeeker, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ServiceReceived
    {
      get { return Get(ref _serviceReceived, "ServiceReceived"); }
      set { Set(ref _serviceReceived, value, "ServiceReceived"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobSeeker" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> JobSeekerId
    {
      get { return Get(ref _jobSeekerId, "JobSeekerId"); }
      set { Set(ref _jobSeekerId, value, "JobSeekerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Report.JobSeekerCertificationLicense", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeekerCertificationLicense : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 150)]
    private string _certificationLicense;
    private System.Nullable<long> _jobSeekerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the CertificationLicense entity attribute.</summary>
    public const string CertificationLicenseField = "CertificationLicense";
    /// <summary>Identifies the JobSeekerId entity attribute.</summary>
    public const string JobSeekerIdField = "JobSeekerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSeekerCertificationLicenses")]
    private readonly EntityHolder<JobSeeker> _jobSeeker = new EntityHolder<JobSeeker>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public JobSeeker JobSeeker
    {
      get { return Get(_jobSeeker); }
      set { Set(_jobSeeker, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string CertificationLicense
    {
      get { return Get(ref _certificationLicense, "CertificationLicense"); }
      set { Set(ref _certificationLicense, value, "CertificationLicense"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobSeeker" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> JobSeekerId
    {
      get { return Get(ref _jobSeekerId, "JobSeekerId"); }
      set { Set(ref _jobSeekerId, value, "JobSeekerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Report.Activity", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class Activity : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 50)]
    private string _county;
    [ValidateLength(0, 50)]
    private string _state;
    private System.Nullable<System.DateTime> _lastLogin;
    [ValidateLength(0, 50)]
    private string _office;
    [ValidateLength(0, 50)]
    private string _wibLocation;
    [ValidateUnique]
    private System.Nullable<long> _userId;
    [ValidateLength(0, 50)]
    private string _firstName;
    [ValidateLength(0, 50)]
    private string _lastName;
    [ValidateLength(0, 102)]
    private string _fullName;
    [ValidateLength(0, 200)]
    private string _location;
    [ValidateLength(0, 50)]
    private string _townCity;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the County entity attribute.</summary>
    public const string CountyField = "County";
    /// <summary>Identifies the State entity attribute.</summary>
    public const string StateField = "State";
    /// <summary>Identifies the LastLogin entity attribute.</summary>
    public const string LastLoginField = "LastLogin";
    /// <summary>Identifies the Office entity attribute.</summary>
    public const string OfficeField = "Office";
    /// <summary>Identifies the WibLocation entity attribute.</summary>
    public const string WibLocationField = "WibLocation";
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the FirstName entity attribute.</summary>
    public const string FirstNameField = "FirstName";
    /// <summary>Identifies the LastName entity attribute.</summary>
    public const string LastNameField = "LastName";
    /// <summary>Identifies the FullName entity attribute.</summary>
    public const string FullNameField = "FullName";
    /// <summary>Identifies the Location entity attribute.</summary>
    public const string LocationField = "Location";
    /// <summary>Identifies the TownCity entity attribute.</summary>
    public const string TownCityField = "TownCity";


    #endregion
    
    #region Relationships

    [ReverseAssociation("User")]
    private readonly EntityCollection<SystemUsage> _systemUsages = new EntityCollection<SystemUsage>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<SystemUsage> SystemUsages
    {
      get { return Get(_systemUsages); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string County
    {
      get { return Get(ref _county, "County"); }
      set { Set(ref _county, value, "County"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string State
    {
      get { return Get(ref _state, "State"); }
      set { Set(ref _state, value, "State"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> LastLogin
    {
      get { return Get(ref _lastLogin, "LastLogin"); }
      set { Set(ref _lastLogin, value, "LastLogin"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Office
    {
      get { return Get(ref _office, "Office"); }
      set { Set(ref _office, value, "Office"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string WibLocation
    {
      get { return Get(ref _wibLocation, "WibLocation"); }
      set { Set(ref _wibLocation, value, "WibLocation"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string FirstName
    {
      get { return Get(ref _firstName, "FirstName"); }
      set { Set(ref _firstName, value, "FirstName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string LastName
    {
      get { return Get(ref _lastName, "LastName"); }
      set { Set(ref _lastName, value, "LastName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string FullName
    {
      get { return Get(ref _fullName, "FullName"); }
      set { Set(ref _fullName, value, "FullName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Location
    {
      get { return Get(ref _location, "Location"); }
      set { Set(ref _location, value, "Location"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string TownCity
    {
      get { return Get(ref _townCity, "TownCity"); }
      set { Set(ref _townCity, value, "TownCity"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Report.Employer", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class Employer : Entity<long>
  {
    #region Fields
  
    private System.Nullable<long> _employerId;
    [ValidateLength(0, 400)]
    private string _employerName;
    [ValidateLength(0, 50)]
    private string _city;
    [ValidateLength(0, 50)]
    private string _state;
    private System.Nullable<int> _listings;
    private string _specialConditions;
    private System.Nullable<System.DateTime> _accountCreationDate;
    [ValidateLength(0, 50)]
    private string _county;
    [ValidateLength(0, 50)]
    private string _office;
    [ValidateLength(0, 50)]
    private string _wibLocation;
    [ValidateLength(0, 20)]
    private string _zipCode;
    [ValidateLength(0, 50)]
    private string _logitude;
    [ValidateLength(0, 50)]
    private string _latitude;
    [ValidateLength(0, 200)]
    private string _targetSector;
    [ValidateLength(0, 20)]
    private string _federalEmployerIdentificationNumber;
    [ValidateLength(0, 50)]
    private string _contactFirstName;
    [ValidateLength(0, 50)]
    private string _contactLastName;
    [ValidateLength(0, 102)]
    private string _contactFullName;
    private System.Nullable<System.DateTime> _reportStartDate;
    [ValidateLength(0, 200)]
    private string _addressLine1;
    [ValidateLength(0, 5)]
    private string _stateKey;
    private long _employeeId;
    private bool _requestedScreeningAssistance;
    private bool _supportsVeterans;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the EmployerName entity attribute.</summary>
    public const string EmployerNameField = "EmployerName";
    /// <summary>Identifies the City entity attribute.</summary>
    public const string CityField = "City";
    /// <summary>Identifies the State entity attribute.</summary>
    public const string StateField = "State";
    /// <summary>Identifies the Listings entity attribute.</summary>
    public const string ListingsField = "Listings";
    /// <summary>Identifies the SpecialConditions entity attribute.</summary>
    public const string SpecialConditionsField = "SpecialConditions";
    /// <summary>Identifies the AccountCreationDate entity attribute.</summary>
    public const string AccountCreationDateField = "AccountCreationDate";
    /// <summary>Identifies the County entity attribute.</summary>
    public const string CountyField = "County";
    /// <summary>Identifies the Office entity attribute.</summary>
    public const string OfficeField = "Office";
    /// <summary>Identifies the WibLocation entity attribute.</summary>
    public const string WibLocationField = "WibLocation";
    /// <summary>Identifies the ZipCode entity attribute.</summary>
    public const string ZipCodeField = "ZipCode";
    /// <summary>Identifies the Logitude entity attribute.</summary>
    public const string LogitudeField = "Logitude";
    /// <summary>Identifies the Latitude entity attribute.</summary>
    public const string LatitudeField = "Latitude";
    /// <summary>Identifies the TargetSector entity attribute.</summary>
    public const string TargetSectorField = "TargetSector";
    /// <summary>Identifies the FederalEmployerIdentificationNumber entity attribute.</summary>
    public const string FederalEmployerIdentificationNumberField = "FederalEmployerIdentificationNumber";
    /// <summary>Identifies the ContactFirstName entity attribute.</summary>
    public const string ContactFirstNameField = "ContactFirstName";
    /// <summary>Identifies the ContactLastName entity attribute.</summary>
    public const string ContactLastNameField = "ContactLastName";
    /// <summary>Identifies the ContactFullName entity attribute.</summary>
    public const string ContactFullNameField = "ContactFullName";
    /// <summary>Identifies the ReportStartDate entity attribute.</summary>
    public const string ReportStartDateField = "ReportStartDate";
    /// <summary>Identifies the AddressLine1 entity attribute.</summary>
    public const string AddressLine1Field = "AddressLine1";
    /// <summary>Identifies the StateKey entity attribute.</summary>
    public const string StateKeyField = "StateKey";
    /// <summary>Identifies the EmployeeId entity attribute.</summary>
    public const string EmployeeIdField = "EmployeeId";
    /// <summary>Identifies the RequestedScreeningAssistance entity attribute.</summary>
    public const string RequestedScreeningAssistanceField = "RequestedScreeningAssistance";
    /// <summary>Identifies the SupportsVeterans entity attribute.</summary>
    public const string SupportsVeteransField = "SupportsVeterans";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Employer")]
    private readonly EntityCollection<EmployerCount> _employerCounts = new EntityCollection<EmployerCount>();
    [ReverseAssociation("Employer")]
    private readonly EntityCollection<EmployerActivity> _employerActivities = new EntityCollection<EmployerActivity>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<EmployerCount> EmployerCounts
    {
      get { return Get(_employerCounts); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<EmployerActivity> EmployerActivities
    {
      get { return Get(_employerActivities); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerName
    {
      get { return Get(ref _employerName, "EmployerName"); }
      set { Set(ref _employerName, value, "EmployerName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string City
    {
      get { return Get(ref _city, "City"); }
      set { Set(ref _city, value, "City"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string State
    {
      get { return Get(ref _state, "State"); }
      set { Set(ref _state, value, "State"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> Listings
    {
      get { return Get(ref _listings, "Listings"); }
      set { Set(ref _listings, value, "Listings"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SpecialConditions
    {
      get { return Get(ref _specialConditions, "SpecialConditions"); }
      set { Set(ref _specialConditions, value, "SpecialConditions"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> AccountCreationDate
    {
      get { return Get(ref _accountCreationDate, "AccountCreationDate"); }
      set { Set(ref _accountCreationDate, value, "AccountCreationDate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string County
    {
      get { return Get(ref _county, "County"); }
      set { Set(ref _county, value, "County"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Office
    {
      get { return Get(ref _office, "Office"); }
      set { Set(ref _office, value, "Office"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string WibLocation
    {
      get { return Get(ref _wibLocation, "WibLocation"); }
      set { Set(ref _wibLocation, value, "WibLocation"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ZipCode
    {
      get { return Get(ref _zipCode, "ZipCode"); }
      set { Set(ref _zipCode, value, "ZipCode"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Logitude
    {
      get { return Get(ref _logitude, "Logitude"); }
      set { Set(ref _logitude, value, "Logitude"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Latitude
    {
      get { return Get(ref _latitude, "Latitude"); }
      set { Set(ref _latitude, value, "Latitude"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string TargetSector
    {
      get { return Get(ref _targetSector, "TargetSector"); }
      set { Set(ref _targetSector, value, "TargetSector"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string FederalEmployerIdentificationNumber
    {
      get { return Get(ref _federalEmployerIdentificationNumber, "FederalEmployerIdentificationNumber"); }
      set { Set(ref _federalEmployerIdentificationNumber, value, "FederalEmployerIdentificationNumber"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ContactFirstName
    {
      get { return Get(ref _contactFirstName, "ContactFirstName"); }
      set { Set(ref _contactFirstName, value, "ContactFirstName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ContactLastName
    {
      get { return Get(ref _contactLastName, "ContactLastName"); }
      set { Set(ref _contactLastName, value, "ContactLastName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ContactFullName
    {
      get { return Get(ref _contactFullName, "ContactFullName"); }
      set { Set(ref _contactFullName, value, "ContactFullName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ReportStartDate
    {
      get { return Get(ref _reportStartDate, "ReportStartDate"); }
      set { Set(ref _reportStartDate, value, "ReportStartDate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string AddressLine1
    {
      get { return Get(ref _addressLine1, "AddressLine1"); }
      set { Set(ref _addressLine1, value, "AddressLine1"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string StateKey
    {
      get { return Get(ref _stateKey, "StateKey"); }
      set { Set(ref _stateKey, value, "StateKey"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployeeId
    {
      get { return Get(ref _employeeId, "EmployeeId"); }
      set { Set(ref _employeeId, value, "EmployeeId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool RequestedScreeningAssistance
    {
      get { return Get(ref _requestedScreeningAssistance, "RequestedScreeningAssistance"); }
      set { Set(ref _requestedScreeningAssistance, value, "RequestedScreeningAssistance"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool SupportsVeterans
    {
      get { return Get(ref _supportsVeterans, "SupportsVeterans"); }
      set { Set(ref _supportsVeterans, value, "SupportsVeterans"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Report.JobOrder", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobOrder : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 400)]
    private string _jobTitle;
    [ValidateLength(0, 50)]
    private string _city;
    [ValidateLength(0, 50)]
    private string _state;
    private System.Nullable<System.DateTime> _postingDate;
    private System.Nullable<System.DateTime> _closingDate;
    private System.Nullable<int> _openings;
    private string _specialConditions;
    [ValidateLength(0, 50)]
    private string _county;
    [ValidateLength(0, 50)]
    private string _office;
    [ValidateLength(0, 50)]
    private string _wibLocation;
    [ValidateLength(0, 50)]
    private string _zipCode;
    [ValidateLength(0, 50)]
    private string _longitude;
    [ValidateLength(0, 50)]
    private string _latitude;
    private System.Nullable<decimal> _minSalary;
    private System.Nullable<decimal> _maxSalary;
    private System.Nullable<int> _salaryFrequency;
    private System.Nullable<int> _jobStatus;
    [ValueField]
    private System.Nullable<Focus.Core.EducationLevel> _educationLevel;
    private System.Nullable<int> _jobFlag;
    private System.Nullable<int> _screeningAssistanceRequired;
    [ValueField]
    private System.Nullable<Focus.Core.Compliance> _compliance;
    private System.Nullable<bool> _suppressJobOrder;
    private System.Nullable<bool> _spidered;
    [ValidateLength(0, 50)]
    private string _onetOccupationGroup;
    [ValidateLength(0, 50)]
    private string _onetDetailedOccupation;
    [ValidateLength(0, 50)]
    private string _onetIndustryGroup;
    [ValidateLength(0, 50)]
    private string _onetDetailedIndustry;
    private System.Nullable<long> _externalEmployerId;
    private System.Nullable<long> _externalEmployeeId;
    [ValidateLength(0, 400)]
    private string _employerName;
    private System.Nullable<long> _referenceId;
    [ValidateLength(0, 400)]
    private string _jobDescription;
    [ValidateLength(0, 50)]
    private string _wotcInterest;
    private System.Nullable<long> _employerId;
    [ValidateLength(0, 150)]
    private string _externalJobId;
    private System.Nullable<System.DateTime> _reportStartDate;
    private System.Nullable<long> _daysToExpiry;
    private System.Nullable<bool> _isSelfService;
    private System.Nullable<bool> _isStaffAssisted;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobTitle entity attribute.</summary>
    public const string JobTitleField = "JobTitle";
    /// <summary>Identifies the City entity attribute.</summary>
    public const string CityField = "City";
    /// <summary>Identifies the State entity attribute.</summary>
    public const string StateField = "State";
    /// <summary>Identifies the PostingDate entity attribute.</summary>
    public const string PostingDateField = "PostingDate";
    /// <summary>Identifies the ClosingDate entity attribute.</summary>
    public const string ClosingDateField = "ClosingDate";
    /// <summary>Identifies the Openings entity attribute.</summary>
    public const string OpeningsField = "Openings";
    /// <summary>Identifies the SpecialConditions entity attribute.</summary>
    public const string SpecialConditionsField = "SpecialConditions";
    /// <summary>Identifies the County entity attribute.</summary>
    public const string CountyField = "County";
    /// <summary>Identifies the Office entity attribute.</summary>
    public const string OfficeField = "Office";
    /// <summary>Identifies the WibLocation entity attribute.</summary>
    public const string WibLocationField = "WibLocation";
    /// <summary>Identifies the ZipCode entity attribute.</summary>
    public const string ZipCodeField = "ZipCode";
    /// <summary>Identifies the Longitude entity attribute.</summary>
    public const string LongitudeField = "Longitude";
    /// <summary>Identifies the Latitude entity attribute.</summary>
    public const string LatitudeField = "Latitude";
    /// <summary>Identifies the MinSalary entity attribute.</summary>
    public const string MinSalaryField = "MinSalary";
    /// <summary>Identifies the MaxSalary entity attribute.</summary>
    public const string MaxSalaryField = "MaxSalary";
    /// <summary>Identifies the SalaryFrequency entity attribute.</summary>
    public const string SalaryFrequencyField = "SalaryFrequency";
    /// <summary>Identifies the JobStatus entity attribute.</summary>
    public const string JobStatusField = "JobStatus";
    /// <summary>Identifies the EducationLevel entity attribute.</summary>
    public const string EducationLevelField = "EducationLevel";
    /// <summary>Identifies the JobFlag entity attribute.</summary>
    public const string JobFlagField = "JobFlag";
    /// <summary>Identifies the ScreeningAssistanceRequired entity attribute.</summary>
    public const string ScreeningAssistanceRequiredField = "ScreeningAssistanceRequired";
    /// <summary>Identifies the Compliance entity attribute.</summary>
    public const string ComplianceField = "Compliance";
    /// <summary>Identifies the SuppressJobOrder entity attribute.</summary>
    public const string SuppressJobOrderField = "SuppressJobOrder";
    /// <summary>Identifies the Spidered entity attribute.</summary>
    public const string SpideredField = "Spidered";
    /// <summary>Identifies the OnetOccupationGroup entity attribute.</summary>
    public const string OnetOccupationGroupField = "OnetOccupationGroup";
    /// <summary>Identifies the OnetDetailedOccupation entity attribute.</summary>
    public const string OnetDetailedOccupationField = "OnetDetailedOccupation";
    /// <summary>Identifies the OnetIndustryGroup entity attribute.</summary>
    public const string OnetIndustryGroupField = "OnetIndustryGroup";
    /// <summary>Identifies the OnetDetailedIndustry entity attribute.</summary>
    public const string OnetDetailedIndustryField = "OnetDetailedIndustry";
    /// <summary>Identifies the ExternalEmployerId entity attribute.</summary>
    public const string ExternalEmployerIdField = "ExternalEmployerId";
    /// <summary>Identifies the ExternalEmployeeId entity attribute.</summary>
    public const string ExternalEmployeeIdField = "ExternalEmployeeId";
    /// <summary>Identifies the EmployerName entity attribute.</summary>
    public const string EmployerNameField = "EmployerName";
    /// <summary>Identifies the ReferenceId entity attribute.</summary>
    public const string ReferenceIdField = "ReferenceId";
    /// <summary>Identifies the JobDescription entity attribute.</summary>
    public const string JobDescriptionField = "JobDescription";
    /// <summary>Identifies the WotcInterest entity attribute.</summary>
    public const string WotcInterestField = "WotcInterest";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the ExternalJobId entity attribute.</summary>
    public const string ExternalJobIdField = "ExternalJobId";
    /// <summary>Identifies the ReportStartDate entity attribute.</summary>
    public const string ReportStartDateField = "ReportStartDate";
    /// <summary>Identifies the DaysToExpiry entity attribute.</summary>
    public const string DaysToExpiryField = "DaysToExpiry";
    /// <summary>Identifies the IsSelfService entity attribute.</summary>
    public const string IsSelfServiceField = "IsSelfService";
    /// <summary>Identifies the IsStaffAssisted entity attribute.</summary>
    public const string IsStaffAssistedField = "IsStaffAssisted";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobOrder")]
    private readonly EntityCollection<JobSeekerApplication> _jobSeekerApplications = new EntityCollection<JobSeekerApplication>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSeekerApplication> JobSeekerApplications
    {
      get { return Get(_jobSeekerApplications); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string JobTitle
    {
      get { return Get(ref _jobTitle, "JobTitle"); }
      set { Set(ref _jobTitle, value, "JobTitle"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string City
    {
      get { return Get(ref _city, "City"); }
      set { Set(ref _city, value, "City"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string State
    {
      get { return Get(ref _state, "State"); }
      set { Set(ref _state, value, "State"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> PostingDate
    {
      get { return Get(ref _postingDate, "PostingDate"); }
      set { Set(ref _postingDate, value, "PostingDate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ClosingDate
    {
      get { return Get(ref _closingDate, "ClosingDate"); }
      set { Set(ref _closingDate, value, "ClosingDate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> Openings
    {
      get { return Get(ref _openings, "Openings"); }
      set { Set(ref _openings, value, "Openings"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SpecialConditions
    {
      get { return Get(ref _specialConditions, "SpecialConditions"); }
      set { Set(ref _specialConditions, value, "SpecialConditions"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string County
    {
      get { return Get(ref _county, "County"); }
      set { Set(ref _county, value, "County"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Office
    {
      get { return Get(ref _office, "Office"); }
      set { Set(ref _office, value, "Office"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string WibLocation
    {
      get { return Get(ref _wibLocation, "WibLocation"); }
      set { Set(ref _wibLocation, value, "WibLocation"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ZipCode
    {
      get { return Get(ref _zipCode, "ZipCode"); }
      set { Set(ref _zipCode, value, "ZipCode"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Longitude
    {
      get { return Get(ref _longitude, "Longitude"); }
      set { Set(ref _longitude, value, "Longitude"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Latitude
    {
      get { return Get(ref _latitude, "Latitude"); }
      set { Set(ref _latitude, value, "Latitude"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> MinSalary
    {
      get { return Get(ref _minSalary, "MinSalary"); }
      set { Set(ref _minSalary, value, "MinSalary"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> MaxSalary
    {
      get { return Get(ref _maxSalary, "MaxSalary"); }
      set { Set(ref _maxSalary, value, "MaxSalary"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> SalaryFrequency
    {
      get { return Get(ref _salaryFrequency, "SalaryFrequency"); }
      set { Set(ref _salaryFrequency, value, "SalaryFrequency"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> JobStatus
    {
      get { return Get(ref _jobStatus, "JobStatus"); }
      set { Set(ref _jobStatus, value, "JobStatus"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.EducationLevel> EducationLevel
    {
      get { return Get(ref _educationLevel, "EducationLevel"); }
      set { Set(ref _educationLevel, value, "EducationLevel"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> JobFlag
    {
      get { return Get(ref _jobFlag, "JobFlag"); }
      set { Set(ref _jobFlag, value, "JobFlag"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> ScreeningAssistanceRequired
    {
      get { return Get(ref _screeningAssistanceRequired, "ScreeningAssistanceRequired"); }
      set { Set(ref _screeningAssistanceRequired, value, "ScreeningAssistanceRequired"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.Compliance> Compliance
    {
      get { return Get(ref _compliance, "Compliance"); }
      set { Set(ref _compliance, value, "Compliance"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> SuppressJobOrder
    {
      get { return Get(ref _suppressJobOrder, "SuppressJobOrder"); }
      set { Set(ref _suppressJobOrder, value, "SuppressJobOrder"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> Spidered
    {
      get { return Get(ref _spidered, "Spidered"); }
      set { Set(ref _spidered, value, "Spidered"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetOccupationGroup
    {
      get { return Get(ref _onetOccupationGroup, "OnetOccupationGroup"); }
      set { Set(ref _onetOccupationGroup, value, "OnetOccupationGroup"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetDetailedOccupation
    {
      get { return Get(ref _onetDetailedOccupation, "OnetDetailedOccupation"); }
      set { Set(ref _onetDetailedOccupation, value, "OnetDetailedOccupation"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetIndustryGroup
    {
      get { return Get(ref _onetIndustryGroup, "OnetIndustryGroup"); }
      set { Set(ref _onetIndustryGroup, value, "OnetIndustryGroup"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetDetailedIndustry
    {
      get { return Get(ref _onetDetailedIndustry, "OnetDetailedIndustry"); }
      set { Set(ref _onetDetailedIndustry, value, "OnetDetailedIndustry"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> ExternalEmployerId
    {
      get { return Get(ref _externalEmployerId, "ExternalEmployerId"); }
      set { Set(ref _externalEmployerId, value, "ExternalEmployerId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> ExternalEmployeeId
    {
      get { return Get(ref _externalEmployeeId, "ExternalEmployeeId"); }
      set { Set(ref _externalEmployeeId, value, "ExternalEmployeeId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerName
    {
      get { return Get(ref _employerName, "EmployerName"); }
      set { Set(ref _employerName, value, "EmployerName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> ReferenceId
    {
      get { return Get(ref _referenceId, "ReferenceId"); }
      set { Set(ref _referenceId, value, "ReferenceId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string JobDescription
    {
      get { return Get(ref _jobDescription, "JobDescription"); }
      set { Set(ref _jobDescription, value, "JobDescription"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string WotcInterest
    {
      get { return Get(ref _wotcInterest, "WotcInterest"); }
      set { Set(ref _wotcInterest, value, "WotcInterest"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalJobId
    {
      get { return Get(ref _externalJobId, "ExternalJobId"); }
      set { Set(ref _externalJobId, value, "ExternalJobId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ReportStartDate
    {
      get { return Get(ref _reportStartDate, "ReportStartDate"); }
      set { Set(ref _reportStartDate, value, "ReportStartDate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> DaysToExpiry
    {
      get { return Get(ref _daysToExpiry, "DaysToExpiry"); }
      set { Set(ref _daysToExpiry, value, "DaysToExpiry"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> IsSelfService
    {
      get { return Get(ref _isSelfService, "IsSelfService"); }
      set { Set(ref _isSelfService, value, "IsSelfService"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> IsStaffAssisted
    {
      get { return Get(ref _isStaffAssisted, "IsStaffAssisted"); }
      set { Set(ref _isStaffAssisted, value, "IsStaffAssisted"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Report.SystemUsage", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class SystemUsage : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 50)]
    private string _action;
    private System.Nullable<System.DateTime> _actionDateTime;
    private System.Nullable<long> _userId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Action entity attribute.</summary>
    public const string ActionField = "Action";
    /// <summary>Identifies the ActionDateTime entity attribute.</summary>
    public const string ActionDateTimeField = "ActionDateTime";
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("SystemUsages")]
    private readonly EntityHolder<Activity> _user = new EntityHolder<Activity>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Activity User
    {
      get { return Get(_user); }
      set { Set(_user, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Action
    {
      get { return Get(ref _action, "Action"); }
      set { Set(ref _action, value, "Action"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ActionDateTime
    {
      get { return Get(ref _actionDateTime, "ActionDateTime"); }
      set { Set(ref _actionDateTime, value, "ActionDateTime"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="User" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Report.JobSeekerApplication", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeekerApplication : Entity<long>
  {
    #region Fields
  
    [ValueField]
    private Focus.Core.ApplicationStatus _applicationResult;
    private System.Nullable<long> _employerReferenceId;
    private System.Nullable<long> _jobSeekerId;
    private System.Nullable<long> _jobOrderId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ApplicationResult entity attribute.</summary>
    public const string ApplicationResultField = "ApplicationResult";
    /// <summary>Identifies the EmployerReferenceId entity attribute.</summary>
    public const string EmployerReferenceIdField = "EmployerReferenceId";
    /// <summary>Identifies the JobSeekerId entity attribute.</summary>
    public const string JobSeekerIdField = "JobSeekerId";
    /// <summary>Identifies the JobOrderId entity attribute.</summary>
    public const string JobOrderIdField = "JobOrderId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSeekerApplications")]
    private readonly EntityHolder<JobSeeker> _jobSeeker = new EntityHolder<JobSeeker>();
    [ReverseAssociation("JobSeekerApplications")]
    private readonly EntityHolder<JobOrder> _jobOrder = new EntityHolder<JobOrder>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public JobSeeker JobSeeker
    {
      get { return Get(_jobSeeker); }
      set { Set(_jobSeeker, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public JobOrder JobOrder
    {
      get { return Get(_jobOrder); }
      set { Set(_jobOrder, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ApplicationStatus ApplicationResult
    {
      get { return Get(ref _applicationResult, "ApplicationResult"); }
      set { Set(ref _applicationResult, value, "ApplicationResult"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EmployerReferenceId
    {
      get { return Get(ref _employerReferenceId, "EmployerReferenceId"); }
      set { Set(ref _employerReferenceId, value, "EmployerReferenceId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobSeeker" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> JobSeekerId
    {
      get { return Get(ref _jobSeekerId, "JobSeekerId"); }
      set { Set(ref _jobSeekerId, value, "JobSeekerId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobOrder" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> JobOrderId
    {
      get { return Get(ref _jobOrderId, "JobOrderId"); }
      set { Set(ref _jobOrderId, value, "JobOrderId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Report.JobSeekerNote", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeekerNote : Entity<long>
  {
    #region Fields
  
    private string _note;
    private long _noteId;
    private long _referenceId;
    private long _jobSeekerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Note entity attribute.</summary>
    public const string NoteField = "Note";
    /// <summary>Identifies the NoteId entity attribute.</summary>
    public const string NoteIdField = "NoteId";
    /// <summary>Identifies the ReferenceId entity attribute.</summary>
    public const string ReferenceIdField = "ReferenceId";
    /// <summary>Identifies the JobSeekerId entity attribute.</summary>
    public const string JobSeekerIdField = "JobSeekerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSeekerNotes")]
    private readonly EntityHolder<JobSeeker> _jobSeeker = new EntityHolder<JobSeeker>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public JobSeeker JobSeeker
    {
      get { return Get(_jobSeeker); }
      set { Set(_jobSeeker, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Note
    {
      get { return Get(ref _note, "Note"); }
      set { Set(ref _note, value, "Note"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long NoteId
    {
      get { return Get(ref _noteId, "NoteId"); }
      set { Set(ref _noteId, value, "NoteId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long ReferenceId
    {
      get { return Get(ref _referenceId, "ReferenceId"); }
      set { Set(ref _referenceId, value, "ReferenceId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobSeeker" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobSeekerId
    {
      get { return Get(ref _jobSeekerId, "JobSeekerId"); }
      set { Set(ref _jobSeekerId, value, "JobSeekerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Report.SavedReport")]
  public partial class SavedReport : Entity<long>
  {
    #region Fields
  
    [ValueField]
    private Focus.Core.ReportType _reportType;
    private string _criteria;
    private System.Nullable<long> _userId;
    [ValidateLength(0, 200)]
    private string _name;
    private string _displayType;
    private System.DateTime _reportDate;
    private bool _displayOnDashboard;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ReportType entity attribute.</summary>
    public const string ReportTypeField = "ReportType";
    /// <summary>Identifies the Criteria entity attribute.</summary>
    public const string CriteriaField = "Criteria";
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the DisplayType entity attribute.</summary>
    public const string DisplayTypeField = "DisplayType";
    /// <summary>Identifies the ReportDate entity attribute.</summary>
    public const string ReportDateField = "ReportDate";
    /// <summary>Identifies the DisplayOnDashboard entity attribute.</summary>
    public const string DisplayOnDashboardField = "DisplayOnDashboard";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ReportType ReportType
    {
      get { return Get(ref _reportType, "ReportType"); }
      set { Set(ref _reportType, value, "ReportType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Criteria
    {
      get { return Get(ref _criteria, "Criteria"); }
      set { Set(ref _criteria, value, "Criteria"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string DisplayType
    {
      get { return Get(ref _displayType, "DisplayType"); }
      set { Set(ref _displayType, value, "DisplayType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ReportDate
    {
      get { return Get(ref _reportDate, "ReportDate"); }
      set { Set(ref _reportDate, value, "ReportDate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool DisplayOnDashboard
    {
      get { return Get(ref _displayOnDashboard, "DisplayOnDashboard"); }
      set { Set(ref _displayOnDashboard, value, "DisplayOnDashboard"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Report.JobSeekerCount", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeekerCount : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 100)]
    private string _name;
    private int _count;
    private System.DateTime _countDate;
    private long _jobSeekerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the Count entity attribute.</summary>
    public const string CountField = "Count";
    /// <summary>Identifies the CountDate entity attribute.</summary>
    public const string CountDateField = "CountDate";
    /// <summary>Identifies the JobSeekerId entity attribute.</summary>
    public const string JobSeekerIdField = "JobSeekerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSeekerCounts")]
    private readonly EntityHolder<JobSeeker> _jobSeeker = new EntityHolder<JobSeeker>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public JobSeeker JobSeeker
    {
      get { return Get(_jobSeeker); }
      set { Set(_jobSeeker, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Count
    {
      get { return Get(ref _count, "Count"); }
      set { Set(ref _count, value, "Count"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CountDate
    {
      get { return Get(ref _countDate, "CountDate"); }
      set { Set(ref _countDate, value, "CountDate"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobSeeker" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobSeekerId
    {
      get { return Get(ref _jobSeekerId, "JobSeekerId"); }
      set { Set(ref _jobSeekerId, value, "JobSeekerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Report.EmployerCount", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class EmployerCount : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 100)]
    private string _name;
    private int _count;
    private System.DateTime _countDate;
    private long _employerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the Count entity attribute.</summary>
    public const string CountField = "Count";
    /// <summary>Identifies the CountDate entity attribute.</summary>
    public const string CountDateField = "CountDate";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("EmployerCounts")]
    private readonly EntityHolder<Employer> _employer = new EntityHolder<Employer>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Employer Employer
    {
      get { return Get(_employer); }
      set { Set(_employer, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public int Count
    {
      get { return Get(ref _count, "Count"); }
      set { Set(ref _count, value, "Count"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CountDate
    {
      get { return Get(ref _countDate, "CountDate"); }
      set { Set(ref _countDate, value, "CountDate"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Employer" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Report.ReportDate", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class ReportDate : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 100)]
    private System.DateTime _executionDate;
    private System.DateTime _reportStartDate;
    private System.DateTime _reportEndDate;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ExecutionDate entity attribute.</summary>
    public const string ExecutionDateField = "ExecutionDate";
    /// <summary>Identifies the ReportStartDate entity attribute.</summary>
    public const string ReportStartDateField = "ReportStartDate";
    /// <summary>Identifies the ReportEndDate entity attribute.</summary>
    public const string ReportEndDateField = "ReportEndDate";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ExecutionDate
    {
      get { return Get(ref _executionDate, "ExecutionDate"); }
      set { Set(ref _executionDate, value, "ExecutionDate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ReportStartDate
    {
      get { return Get(ref _reportStartDate, "ReportStartDate"); }
      set { Set(ref _reportStartDate, value, "ReportStartDate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ReportEndDate
    {
      get { return Get(ref _reportEndDate, "ReportEndDate"); }
      set { Set(ref _reportEndDate, value, "ReportEndDate"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Report.JobSeekerActivity", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class JobSeekerActivity : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 102)]
    private string _staffName;
    private System.DateTime _activityDate;
    [ValidateLength(0, 200)]
    private string _activity;
    private System.Nullable<long> _userId;
    private bool _assistAction;
    [ValidateLength(0, 202)]
    private string _jobSeekerFullName;
    private long _jobSeekerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the StaffName entity attribute.</summary>
    public const string StaffNameField = "StaffName";
    /// <summary>Identifies the ActivityDate entity attribute.</summary>
    public const string ActivityDateField = "ActivityDate";
    /// <summary>Identifies the Activity entity attribute.</summary>
    public const string ActivityField = "Activity";
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the AssistAction entity attribute.</summary>
    public const string AssistActionField = "AssistAction";
    /// <summary>Identifies the JobSeekerFullName entity attribute.</summary>
    public const string JobSeekerFullNameField = "JobSeekerFullName";
    /// <summary>Identifies the JobSeekerId entity attribute.</summary>
    public const string JobSeekerIdField = "JobSeekerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSeekerActivities")]
    private readonly EntityHolder<JobSeeker> _jobSeeker = new EntityHolder<JobSeeker>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public JobSeeker JobSeeker
    {
      get { return Get(_jobSeeker); }
      set { Set(_jobSeeker, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string StaffName
    {
      get { return Get(ref _staffName, "StaffName"); }
      set { Set(ref _staffName, value, "StaffName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ActivityDate
    {
      get { return Get(ref _activityDate, "ActivityDate"); }
      set { Set(ref _activityDate, value, "ActivityDate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Activity
    {
      get { return Get(ref _activity, "Activity"); }
      set { Set(ref _activity, value, "Activity"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool AssistAction
    {
      get { return Get(ref _assistAction, "AssistAction"); }
      set { Set(ref _assistAction, value, "AssistAction"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string JobSeekerFullName
    {
      get { return Get(ref _jobSeekerFullName, "JobSeekerFullName"); }
      set { Set(ref _jobSeekerFullName, value, "JobSeekerFullName"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobSeeker" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobSeekerId
    {
      get { return Get(ref _jobSeekerId, "JobSeekerId"); }
      set { Set(ref _jobSeekerId, value, "JobSeekerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Report.EmployerActivity", IdentityMethod=IdentityMethod.KeyTable)]
  public partial class EmployerActivity : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 202)]
    private string _staffName;
    private System.DateTime _activityDate;
    [ValidateLength(0, 200)]
    private string _activity;
    private System.Nullable<long> _userId;
    private bool _assistAction;
    [ValidateLength(0, 400)]
    private string _employerName;
    private long _employerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the StaffName entity attribute.</summary>
    public const string StaffNameField = "StaffName";
    /// <summary>Identifies the ActivityDate entity attribute.</summary>
    public const string ActivityDateField = "ActivityDate";
    /// <summary>Identifies the Activity entity attribute.</summary>
    public const string ActivityField = "Activity";
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the AssistAction entity attribute.</summary>
    public const string AssistActionField = "AssistAction";
    /// <summary>Identifies the EmployerName entity attribute.</summary>
    public const string EmployerNameField = "EmployerName";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("EmployerActivities")]
    private readonly EntityHolder<Employer> _employer = new EntityHolder<Employer>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Employer Employer
    {
      get { return Get(_employer); }
      set { Set(_employer, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string StaffName
    {
      get { return Get(ref _staffName, "StaffName"); }
      set { Set(ref _staffName, value, "StaffName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ActivityDate
    {
      get { return Get(ref _activityDate, "ActivityDate"); }
      set { Set(ref _activityDate, value, "ActivityDate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Activity
    {
      get { return Get(ref _activity, "Activity"); }
      set { Set(ref _activity, value, "Activity"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool AssistAction
    {
      get { return Get(ref _assistAction, "AssistAction"); }
      set { Set(ref _assistAction, value, "AssistAction"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerName
    {
      get { return Get(ref _employerName, "EmployerName"); }
      set { Set(ref _employerName, value, "EmployerName"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Employer" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Data.Report.LookUpItem")]
  public partial class LookUpItem : Entity<long>
  {
    #region Fields
  
    [ValueField]
    private Focus.Core.ReportingLookUp _lookUpType;
    [ValidateLength(0, 150)]
    private string _lookUpValue;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the LookUpType entity attribute.</summary>
    public const string LookUpTypeField = "LookUpType";
    /// <summary>Identifies the LookUpValue entity attribute.</summary>
    public const string LookUpValueField = "LookUpValue";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ReportingLookUp LookUpType
    {
      get { return Get(ref _lookUpType, "LookUpType"); }
      set { Set(ref _lookUpType, value, "LookUpType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string LookUpValue
    {
      get { return Get(ref _lookUpValue, "LookUpValue"); }
      set { Set(ref _lookUpValue, value, "LookUpValue"); }
    }

    #endregion
  }
	


  /// <summary>
  /// Provides a strong-typed unit of work for working with the DataReporting model.
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  public partial class DataReportingUnitOfWork : Mindscape.LightSpeed.UnitOfWork
  {

    public System.Linq.IQueryable<JobSeeker> JobSeekers
    {
      get { return this.Query<JobSeeker>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerSkill> JobSeekerSkills
    {
      get { return this.Query<JobSeekerSkill>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerServicesReceived> JobSeekerServicesReceiveds
    {
      get { return this.Query<JobSeekerServicesReceived>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerCertificationLicense> JobSeekerCertificationLicenses
    {
      get { return this.Query<JobSeekerCertificationLicense>(); }
    }
    
    public System.Linq.IQueryable<Activity> Activities
    {
      get { return this.Query<Activity>(); }
    }
    
    public System.Linq.IQueryable<Employer> Employers
    {
      get { return this.Query<Employer>(); }
    }
    
    public System.Linq.IQueryable<JobOrder> JobOrders
    {
      get { return this.Query<JobOrder>(); }
    }
    
    public System.Linq.IQueryable<SystemUsage> SystemUsages
    {
      get { return this.Query<SystemUsage>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerApplication> JobSeekerApplications
    {
      get { return this.Query<JobSeekerApplication>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerNote> JobSeekerNotes
    {
      get { return this.Query<JobSeekerNote>(); }
    }
    
    public System.Linq.IQueryable<SavedReport> SavedReports
    {
      get { return this.Query<SavedReport>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerCount> JobSeekerCounts
    {
      get { return this.Query<JobSeekerCount>(); }
    }
    
    public System.Linq.IQueryable<EmployerCount> EmployerCounts
    {
      get { return this.Query<EmployerCount>(); }
    }
    
    public System.Linq.IQueryable<ReportDate> ReportDates
    {
      get { return this.Query<ReportDate>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerActivity> JobSeekerActivities
    {
      get { return this.Query<JobSeekerActivity>(); }
    }
    
    public System.Linq.IQueryable<EmployerActivity> EmployerActivities
    {
      get { return this.Query<EmployerActivity>(); }
    }
    
    public System.Linq.IQueryable<LookUpItem> LookUpItems
    {
      get { return this.Query<LookUpItem>(); }
    }
    
  }
}

#endregion


#region Data Transfer Objects - Classes

/*
namespace Focus.Core.DataTransferObjects.FocusReporting.Entities
{
  using System;
  using System.Runtime.Serialization;

    [Serializable]
    [DataContract(Name="JobSeeker", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long? CareerJobSeekerId { get; set; }
      [DataMember]
      public string FirstName { get; set; }
      [DataMember]
      public string LastName { get; set; }
      [DataMember]
      public string EmailAddress { get; set; }
      [DataMember]
      public DateTime? AccountCreationDate { get; set; }
      [DataMember]
      public string LastJobTitle { get; set; }
      [DataMember]
      public string LastEmployer { get; set; }
      [DataMember]
      public string LastJobDescription { get; set; }
      [DataMember]
      public ClaimantStatus? ClaimantStatus { get; set; }
      [DataMember]
      public VeteranDefinition? VeteranDefinition { get; set; }
      [DataMember]
      public VeteranType? VeteranType { get; set; }
      [DataMember]
      public VeteranEmploymentStatus? VeteranEmploymentStatus { get; set; }
      [DataMember]
      public VeteranTransactionType? VeteranTransactionType { get; set; }
      [DataMember]
      public VeteranMilitaryDischarge? VeteranMilitaryDischarge { get; set; }
      [DataMember]
      public VeteranBranchOfService? VeteranBranchOfService { get; set; }
      [DataMember]
      public EducationLevel? EducationLevel { get; set; }
      [DataMember]
      public string City { get; set; }
      [DataMember]
      public string ClientStatus { get; set; }
      [DataMember]
      public int WeeksSinceAccountCreated { get; set; }
      [DataMember]
      public string FullName { get; set; }
      [DataMember]
      public DateTime? ReportStartDate { get; set; }
      [DataMember]
      public int? DaysSinceRegistration { get; set; }
      [DataMember]
      public int? ResumeWordCount { get; set; }
      [DataMember]
      public int? ResumeSkillCount { get; set; }
      [DataMember]
      public DateTime? LastLoginDate { get; set; }
      [DataMember]
      public string ZipCode { get; set; }
      [DataMember]
      public string AddressLine1 { get; set; }
      [DataMember]
      public string StateKey { get; set; }
      [DataMember]
      public long CandidateId { get; set; }
      [DataMember]
      public string ClientId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeekerSkill", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerSkillDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Skill { get; set; }
      [DataMember]
      public long JobSeekerId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeekerServicesReceived", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerServicesReceivedDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string ServiceReceived { get; set; }
      [DataMember]
      public long? JobSeekerId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeekerCertificationLicense", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerCertificationLicenseDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string CertificationLicense { get; set; }
      [DataMember]
      public long? JobSeekerId { get; set; }
    }

    [Serializable]
    [DataContract(Name="Activity", Namespace = Constants.DataContractNamespace)]
    public class ActivityDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string County { get; set; }
      [DataMember]
      public string State { get; set; }
      [DataMember]
      public DateTime? LastLogin { get; set; }
      [DataMember]
      public string Office { get; set; }
      [DataMember]
      public string WibLocation { get; set; }
      [DataMember]
      public long? UserId { get; set; }
      [DataMember]
      public string FirstName { get; set; }
      [DataMember]
      public string LastName { get; set; }
      [DataMember]
      public string FullName { get; set; }
      [DataMember]
      public string Location { get; set; }
      [DataMember]
      public string TownCity { get; set; }
    }

    [Serializable]
    [DataContract(Name="Employer", Namespace = Constants.DataContractNamespace)]
    public class EmployerDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public long? EmployerId { get; set; }
      [DataMember]
      public string EmployerName { get; set; }
      [DataMember]
      public string City { get; set; }
      [DataMember]
      public string State { get; set; }
      [DataMember]
      public int? Listings { get; set; }
      [DataMember]
      public string SpecialConditions { get; set; }
      [DataMember]
      public DateTime? AccountCreationDate { get; set; }
      [DataMember]
      public string County { get; set; }
      [DataMember]
      public string Office { get; set; }
      [DataMember]
      public string WibLocation { get; set; }
      [DataMember]
      public string ZipCode { get; set; }
      [DataMember]
      public string Logitude { get; set; }
      [DataMember]
      public string Latitude { get; set; }
      [DataMember]
      public string TargetSector { get; set; }
      [DataMember]
      public string FederalEmployerIdentificationNumber { get; set; }
      [DataMember]
      public string ContactFirstName { get; set; }
      [DataMember]
      public string ContactLastName { get; set; }
      [DataMember]
      public string ContactFullName { get; set; }
      [DataMember]
      public DateTime? ReportStartDate { get; set; }
      [DataMember]
      public string AddressLine1 { get; set; }
      [DataMember]
      public string StateKey { get; set; }
      [DataMember]
      public long EmployeeId { get; set; }
      [DataMember]
      public bool RequestedScreeningAssistance { get; set; }
      [DataMember]
      public bool SupportsVeterans { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobOrder", Namespace = Constants.DataContractNamespace)]
    public class JobOrderDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string JobTitle { get; set; }
      [DataMember]
      public string City { get; set; }
      [DataMember]
      public string State { get; set; }
      [DataMember]
      public DateTime? PostingDate { get; set; }
      [DataMember]
      public DateTime? ClosingDate { get; set; }
      [DataMember]
      public int? Openings { get; set; }
      [DataMember]
      public string SpecialConditions { get; set; }
      [DataMember]
      public string County { get; set; }
      [DataMember]
      public string Office { get; set; }
      [DataMember]
      public string WibLocation { get; set; }
      [DataMember]
      public string ZipCode { get; set; }
      [DataMember]
      public string Longitude { get; set; }
      [DataMember]
      public string Latitude { get; set; }
      [DataMember]
      public decimal? MinSalary { get; set; }
      [DataMember]
      public decimal? MaxSalary { get; set; }
      [DataMember]
      public int? SalaryFrequency { get; set; }
      [DataMember]
      public int? JobStatus { get; set; }
      [DataMember]
      public EducationLevel? EducationLevel { get; set; }
      [DataMember]
      public int? JobFlag { get; set; }
      [DataMember]
      public int? ScreeningAssistanceRequired { get; set; }
      [DataMember]
      public Compliance? Compliance { get; set; }
      [DataMember]
      public bool? SuppressJobOrder { get; set; }
      [DataMember]
      public bool? Spidered { get; set; }
      [DataMember]
      public string OnetOccupationGroup { get; set; }
      [DataMember]
      public string OnetDetailedOccupation { get; set; }
      [DataMember]
      public string OnetIndustryGroup { get; set; }
      [DataMember]
      public string OnetDetailedIndustry { get; set; }
      [DataMember]
      public long? ExternalEmployerId { get; set; }
      [DataMember]
      public long? ExternalEmployeeId { get; set; }
      [DataMember]
      public string EmployerName { get; set; }
      [DataMember]
      public long? ReferenceId { get; set; }
      [DataMember]
      public string JobDescription { get; set; }
      [DataMember]
      public string WotcInterest { get; set; }
      [DataMember]
      public long? EmployerId { get; set; }
      [DataMember]
      public string ExternalJobId { get; set; }
      [DataMember]
      public DateTime? ReportStartDate { get; set; }
      [DataMember]
      public long? DaysToExpiry { get; set; }
      [DataMember]
      public bool? IsSelfService { get; set; }
      [DataMember]
      public bool? IsStaffAssisted { get; set; }
    }

    [Serializable]
    [DataContract(Name="SystemUsage", Namespace = Constants.DataContractNamespace)]
    public class SystemUsageDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Action { get; set; }
      [DataMember]
      public DateTime? ActionDateTime { get; set; }
      [DataMember]
      public long? UserId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeekerApplication", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerApplicationDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public ApplicationStatus ApplicationResult { get; set; }
      [DataMember]
      public long? EmployerReferenceId { get; set; }
      [DataMember]
      public long? JobSeekerId { get; set; }
      [DataMember]
      public long? JobOrderId { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeekerNote", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerNoteDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Note { get; set; }
      [DataMember]
      public long NoteId { get; set; }
      [DataMember]
      public long ReferenceId { get; set; }
      [DataMember]
      public long JobSeekerId { get; set; }
    }

    [Serializable]
    [DataContract(Name="SavedReport", Namespace = Constants.DataContractNamespace)]
    public class SavedReportDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public ReportType ReportType { get; set; }
      [DataMember]
      public string Criteria { get; set; }
      [DataMember]
      public long? UserId { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public string DisplayType { get; set; }
      [DataMember]
      public DateTime ReportDate { get; set; }
      [DataMember]
      public bool DisplayOnDashboard { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeekerCount", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerCountDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public int Count { get; set; }
      [DataMember]
      public DateTime CountDate { get; set; }
      [DataMember]
      public long JobSeekerId { get; set; }
    }

    [Serializable]
    [DataContract(Name="EmployerCount", Namespace = Constants.DataContractNamespace)]
    public class EmployerCountDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public int Count { get; set; }
      [DataMember]
      public DateTime CountDate { get; set; }
      [DataMember]
      public long EmployerId { get; set; }
    }

    [Serializable]
    [DataContract(Name="ReportDate", Namespace = Constants.DataContractNamespace)]
    public class ReportDateDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public DateTime ExecutionDate { get; set; }
      [DataMember]
      public DateTime ReportStartDate { get; set; }
      [DataMember]
      public DateTime ReportEndDate { get; set; }
    }

    [Serializable]
    [DataContract(Name="JobSeekerActivity", Namespace = Constants.DataContractNamespace)]
    public class JobSeekerActivityDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string StaffName { get; set; }
      [DataMember]
      public DateTime ActivityDate { get; set; }
      [DataMember]
      public string Activity { get; set; }
      [DataMember]
      public long? UserId { get; set; }
      [DataMember]
      public bool AssistAction { get; set; }
      [DataMember]
      public string JobSeekerFullName { get; set; }
      [DataMember]
      public long JobSeekerId { get; set; }
    }

    [Serializable]
    [DataContract(Name="EmployerActivity", Namespace = Constants.DataContractNamespace)]
    public class EmployerActivityDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string StaffName { get; set; }
      [DataMember]
      public DateTime ActivityDate { get; set; }
      [DataMember]
      public string Activity { get; set; }
      [DataMember]
      public long? UserId { get; set; }
      [DataMember]
      public bool AssistAction { get; set; }
      [DataMember]
      public string EmployerName { get; set; }
      [DataMember]
      public long EmployerId { get; set; }
    }

    [Serializable]
    [DataContract(Name="LookUpItem", Namespace = Constants.DataContractNamespace)]
    public class LookUpItemDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public ReportingLookUp LookUpType { get; set; }
      [DataMember]
      public string LookUpValue { get; set; }
    }


}
*/

#endregion

#region Data Transfer Objects - Mappers

/*
namespace Focus.Services.DtoMappers
{
    using Focus.Core.DataTransferObjects.FocusReporting.Entities;
    using Focus.Data.Reporting.Entities;

    public static class FocusReporting.EntitiesMappers
    {

      #region JobSeekerDto Mappers

      public static JobSeekerDto AsDto(this JobSeeker entity)
      {
        var dto = new JobSeekerDto
        {
          CareerJobSeekerId = entity.CareerJobSeekerId,
          FirstName = entity.FirstName,
          LastName = entity.LastName,
          EmailAddress = entity.EmailAddress,
          AccountCreationDate = entity.AccountCreationDate,
          LastJobTitle = entity.LastJobTitle,
          LastEmployer = entity.LastEmployer,
          LastJobDescription = entity.LastJobDescription,
          ClaimantStatus = entity.ClaimantStatus,
          VeteranDefinition = entity.VeteranDefinition,
          VeteranType = entity.VeteranType,
          VeteranEmploymentStatus = entity.VeteranEmploymentStatus,
          VeteranTransactionType = entity.VeteranTransactionType,
          VeteranMilitaryDischarge = entity.VeteranMilitaryDischarge,
          VeteranBranchOfService = entity.VeteranBranchOfService,
          EducationLevel = entity.EducationLevel,
          City = entity.City,
          ClientStatus = entity.ClientStatus,
          WeeksSinceAccountCreated = entity.WeeksSinceAccountCreated,
          FullName = entity.FullName,
          ReportStartDate = entity.ReportStartDate,
          DaysSinceRegistration = entity.DaysSinceRegistration,
          ResumeWordCount = entity.ResumeWordCount,
          ResumeSkillCount = entity.ResumeSkillCount,
          LastLoginDate = entity.LastLoginDate,
          ZipCode = entity.ZipCode,
          AddressLine1 = entity.AddressLine1,
          StateKey = entity.StateKey,
          CandidateId = entity.CandidateId,
          ClientId = entity.ClientId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeeker CopyTo(this JobSeekerDto dto, JobSeeker entity)
      {
        entity.CareerJobSeekerId = dto.CareerJobSeekerId;
        entity.FirstName = dto.FirstName;
        entity.LastName = dto.LastName;
        entity.EmailAddress = dto.EmailAddress;
        entity.AccountCreationDate = dto.AccountCreationDate;
        entity.LastJobTitle = dto.LastJobTitle;
        entity.LastEmployer = dto.LastEmployer;
        entity.LastJobDescription = dto.LastJobDescription;
        entity.ClaimantStatus = dto.ClaimantStatus;
        entity.VeteranDefinition = dto.VeteranDefinition;
        entity.VeteranType = dto.VeteranType;
        entity.VeteranEmploymentStatus = dto.VeteranEmploymentStatus;
        entity.VeteranTransactionType = dto.VeteranTransactionType;
        entity.VeteranMilitaryDischarge = dto.VeteranMilitaryDischarge;
        entity.VeteranBranchOfService = dto.VeteranBranchOfService;
        entity.EducationLevel = dto.EducationLevel;
        entity.City = dto.City;
        entity.ClientStatus = dto.ClientStatus;
        entity.WeeksSinceAccountCreated = dto.WeeksSinceAccountCreated;
        entity.FullName = dto.FullName;
        entity.ReportStartDate = dto.ReportStartDate;
        entity.DaysSinceRegistration = dto.DaysSinceRegistration;
        entity.ResumeWordCount = dto.ResumeWordCount;
        entity.ResumeSkillCount = dto.ResumeSkillCount;
        entity.LastLoginDate = dto.LastLoginDate;
        entity.ZipCode = dto.ZipCode;
        entity.AddressLine1 = dto.AddressLine1;
        entity.StateKey = dto.StateKey;
        entity.CandidateId = dto.CandidateId;
        entity.ClientId = dto.ClientId;
        return entity;
      }

      public static JobSeeker CopyTo(this JobSeekerDto dto)
      {
        var entity = new JobSeeker();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeekerSkillDto Mappers

      public static JobSeekerSkillDto AsDto(this JobSeekerSkill entity)
      {
        var dto = new JobSeekerSkillDto
        {
          Skill = entity.Skill,
          JobSeekerId = entity.JobSeekerId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeekerSkill CopyTo(this JobSeekerSkillDto dto, JobSeekerSkill entity)
      {
        entity.Skill = dto.Skill;
        entity.JobSeekerId = dto.JobSeekerId;
        return entity;
      }

      public static JobSeekerSkill CopyTo(this JobSeekerSkillDto dto)
      {
        var entity = new JobSeekerSkill();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeekerServicesReceivedDto Mappers

      public static JobSeekerServicesReceivedDto AsDto(this JobSeekerServicesReceived entity)
      {
        var dto = new JobSeekerServicesReceivedDto
        {
          ServiceReceived = entity.ServiceReceived,
          JobSeekerId = entity.JobSeekerId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeekerServicesReceived CopyTo(this JobSeekerServicesReceivedDto dto, JobSeekerServicesReceived entity)
      {
        entity.ServiceReceived = dto.ServiceReceived;
        entity.JobSeekerId = dto.JobSeekerId;
        return entity;
      }

      public static JobSeekerServicesReceived CopyTo(this JobSeekerServicesReceivedDto dto)
      {
        var entity = new JobSeekerServicesReceived();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeekerCertificationLicenseDto Mappers

      public static JobSeekerCertificationLicenseDto AsDto(this JobSeekerCertificationLicense entity)
      {
        var dto = new JobSeekerCertificationLicenseDto
        {
          CertificationLicense = entity.CertificationLicense,
          JobSeekerId = entity.JobSeekerId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeekerCertificationLicense CopyTo(this JobSeekerCertificationLicenseDto dto, JobSeekerCertificationLicense entity)
      {
        entity.CertificationLicense = dto.CertificationLicense;
        entity.JobSeekerId = dto.JobSeekerId;
        return entity;
      }

      public static JobSeekerCertificationLicense CopyTo(this JobSeekerCertificationLicenseDto dto)
      {
        var entity = new JobSeekerCertificationLicense();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region ActivityDto Mappers

      public static ActivityDto AsDto(this Activity entity)
      {
        var dto = new ActivityDto
        {
          County = entity.County,
          State = entity.State,
          LastLogin = entity.LastLogin,
          Office = entity.Office,
          WibLocation = entity.WibLocation,
          UserId = entity.UserId,
          FirstName = entity.FirstName,
          LastName = entity.LastName,
          FullName = entity.FullName,
          Location = entity.Location,
          TownCity = entity.TownCity,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static Activity CopyTo(this ActivityDto dto, Activity entity)
      {
        entity.County = dto.County;
        entity.State = dto.State;
        entity.LastLogin = dto.LastLogin;
        entity.Office = dto.Office;
        entity.WibLocation = dto.WibLocation;
        entity.UserId = dto.UserId;
        entity.FirstName = dto.FirstName;
        entity.LastName = dto.LastName;
        entity.FullName = dto.FullName;
        entity.Location = dto.Location;
        entity.TownCity = dto.TownCity;
        return entity;
      }

      public static Activity CopyTo(this ActivityDto dto)
      {
        var entity = new Activity();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region EmployerDto Mappers

      public static EmployerDto AsDto(this Employer entity)
      {
        var dto = new EmployerDto
        {
          EmployerId = entity.EmployerId,
          EmployerName = entity.EmployerName,
          City = entity.City,
          State = entity.State,
          Listings = entity.Listings,
          SpecialConditions = entity.SpecialConditions,
          AccountCreationDate = entity.AccountCreationDate,
          County = entity.County,
          Office = entity.Office,
          WibLocation = entity.WibLocation,
          ZipCode = entity.ZipCode,
          Logitude = entity.Logitude,
          Latitude = entity.Latitude,
          TargetSector = entity.TargetSector,
          FederalEmployerIdentificationNumber = entity.FederalEmployerIdentificationNumber,
          ContactFirstName = entity.ContactFirstName,
          ContactLastName = entity.ContactLastName,
          ContactFullName = entity.ContactFullName,
          ReportStartDate = entity.ReportStartDate,
          AddressLine1 = entity.AddressLine1,
          StateKey = entity.StateKey,
          EmployeeId = entity.EmployeeId,
          RequestedScreeningAssistance = entity.RequestedScreeningAssistance,
          SupportsVeterans = entity.SupportsVeterans,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static Employer CopyTo(this EmployerDto dto, Employer entity)
      {
        entity.EmployerId = dto.EmployerId;
        entity.EmployerName = dto.EmployerName;
        entity.City = dto.City;
        entity.State = dto.State;
        entity.Listings = dto.Listings;
        entity.SpecialConditions = dto.SpecialConditions;
        entity.AccountCreationDate = dto.AccountCreationDate;
        entity.County = dto.County;
        entity.Office = dto.Office;
        entity.WibLocation = dto.WibLocation;
        entity.ZipCode = dto.ZipCode;
        entity.Logitude = dto.Logitude;
        entity.Latitude = dto.Latitude;
        entity.TargetSector = dto.TargetSector;
        entity.FederalEmployerIdentificationNumber = dto.FederalEmployerIdentificationNumber;
        entity.ContactFirstName = dto.ContactFirstName;
        entity.ContactLastName = dto.ContactLastName;
        entity.ContactFullName = dto.ContactFullName;
        entity.ReportStartDate = dto.ReportStartDate;
        entity.AddressLine1 = dto.AddressLine1;
        entity.StateKey = dto.StateKey;
        entity.EmployeeId = dto.EmployeeId;
        entity.RequestedScreeningAssistance = dto.RequestedScreeningAssistance;
        entity.SupportsVeterans = dto.SupportsVeterans;
        return entity;
      }

      public static Employer CopyTo(this EmployerDto dto)
      {
        var entity = new Employer();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobOrderDto Mappers

      public static JobOrderDto AsDto(this JobOrder entity)
      {
        var dto = new JobOrderDto
        {
          JobTitle = entity.JobTitle,
          City = entity.City,
          State = entity.State,
          PostingDate = entity.PostingDate,
          ClosingDate = entity.ClosingDate,
          Openings = entity.Openings,
          SpecialConditions = entity.SpecialConditions,
          County = entity.County,
          Office = entity.Office,
          WibLocation = entity.WibLocation,
          ZipCode = entity.ZipCode,
          Longitude = entity.Longitude,
          Latitude = entity.Latitude,
          MinSalary = entity.MinSalary,
          MaxSalary = entity.MaxSalary,
          SalaryFrequency = entity.SalaryFrequency,
          JobStatus = entity.JobStatus,
          EducationLevel = entity.EducationLevel,
          JobFlag = entity.JobFlag,
          ScreeningAssistanceRequired = entity.ScreeningAssistanceRequired,
          Compliance = entity.Compliance,
          SuppressJobOrder = entity.SuppressJobOrder,
          Spidered = entity.Spidered,
          OnetOccupationGroup = entity.OnetOccupationGroup,
          OnetDetailedOccupation = entity.OnetDetailedOccupation,
          OnetIndustryGroup = entity.OnetIndustryGroup,
          OnetDetailedIndustry = entity.OnetDetailedIndustry,
          ExternalEmployerId = entity.ExternalEmployerId,
          ExternalEmployeeId = entity.ExternalEmployeeId,
          EmployerName = entity.EmployerName,
          ReferenceId = entity.ReferenceId,
          JobDescription = entity.JobDescription,
          WotcInterest = entity.WotcInterest,
          EmployerId = entity.EmployerId,
          ExternalJobId = entity.ExternalJobId,
          ReportStartDate = entity.ReportStartDate,
          DaysToExpiry = entity.DaysToExpiry,
          IsSelfService = entity.IsSelfService,
          IsStaffAssisted = entity.IsStaffAssisted,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobOrder CopyTo(this JobOrderDto dto, JobOrder entity)
      {
        entity.JobTitle = dto.JobTitle;
        entity.City = dto.City;
        entity.State = dto.State;
        entity.PostingDate = dto.PostingDate;
        entity.ClosingDate = dto.ClosingDate;
        entity.Openings = dto.Openings;
        entity.SpecialConditions = dto.SpecialConditions;
        entity.County = dto.County;
        entity.Office = dto.Office;
        entity.WibLocation = dto.WibLocation;
        entity.ZipCode = dto.ZipCode;
        entity.Longitude = dto.Longitude;
        entity.Latitude = dto.Latitude;
        entity.MinSalary = dto.MinSalary;
        entity.MaxSalary = dto.MaxSalary;
        entity.SalaryFrequency = dto.SalaryFrequency;
        entity.JobStatus = dto.JobStatus;
        entity.EducationLevel = dto.EducationLevel;
        entity.JobFlag = dto.JobFlag;
        entity.ScreeningAssistanceRequired = dto.ScreeningAssistanceRequired;
        entity.Compliance = dto.Compliance;
        entity.SuppressJobOrder = dto.SuppressJobOrder;
        entity.Spidered = dto.Spidered;
        entity.OnetOccupationGroup = dto.OnetOccupationGroup;
        entity.OnetDetailedOccupation = dto.OnetDetailedOccupation;
        entity.OnetIndustryGroup = dto.OnetIndustryGroup;
        entity.OnetDetailedIndustry = dto.OnetDetailedIndustry;
        entity.ExternalEmployerId = dto.ExternalEmployerId;
        entity.ExternalEmployeeId = dto.ExternalEmployeeId;
        entity.EmployerName = dto.EmployerName;
        entity.ReferenceId = dto.ReferenceId;
        entity.JobDescription = dto.JobDescription;
        entity.WotcInterest = dto.WotcInterest;
        entity.EmployerId = dto.EmployerId;
        entity.ExternalJobId = dto.ExternalJobId;
        entity.ReportStartDate = dto.ReportStartDate;
        entity.DaysToExpiry = dto.DaysToExpiry;
        entity.IsSelfService = dto.IsSelfService;
        entity.IsStaffAssisted = dto.IsStaffAssisted;
        return entity;
      }

      public static JobOrder CopyTo(this JobOrderDto dto)
      {
        var entity = new JobOrder();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region SystemUsageDto Mappers

      public static SystemUsageDto AsDto(this SystemUsage entity)
      {
        var dto = new SystemUsageDto
        {
          Action = entity.Action,
          ActionDateTime = entity.ActionDateTime,
          UserId = entity.UserId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static SystemUsage CopyTo(this SystemUsageDto dto, SystemUsage entity)
      {
        entity.Action = dto.Action;
        entity.ActionDateTime = dto.ActionDateTime;
        entity.UserId = dto.UserId;
        return entity;
      }

      public static SystemUsage CopyTo(this SystemUsageDto dto)
      {
        var entity = new SystemUsage();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeekerApplicationDto Mappers

      public static JobSeekerApplicationDto AsDto(this JobSeekerApplication entity)
      {
        var dto = new JobSeekerApplicationDto
        {
          ApplicationResult = entity.ApplicationResult,
          EmployerReferenceId = entity.EmployerReferenceId,
          JobSeekerId = entity.JobSeekerId,
          JobOrderId = entity.JobOrderId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeekerApplication CopyTo(this JobSeekerApplicationDto dto, JobSeekerApplication entity)
      {
        entity.ApplicationResult = dto.ApplicationResult;
        entity.EmployerReferenceId = dto.EmployerReferenceId;
        entity.JobSeekerId = dto.JobSeekerId;
        entity.JobOrderId = dto.JobOrderId;
        return entity;
      }

      public static JobSeekerApplication CopyTo(this JobSeekerApplicationDto dto)
      {
        var entity = new JobSeekerApplication();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeekerNoteDto Mappers

      public static JobSeekerNoteDto AsDto(this JobSeekerNote entity)
      {
        var dto = new JobSeekerNoteDto
        {
          Note = entity.Note,
          NoteId = entity.NoteId,
          ReferenceId = entity.ReferenceId,
          JobSeekerId = entity.JobSeekerId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeekerNote CopyTo(this JobSeekerNoteDto dto, JobSeekerNote entity)
      {
        entity.Note = dto.Note;
        entity.NoteId = dto.NoteId;
        entity.ReferenceId = dto.ReferenceId;
        entity.JobSeekerId = dto.JobSeekerId;
        return entity;
      }

      public static JobSeekerNote CopyTo(this JobSeekerNoteDto dto)
      {
        var entity = new JobSeekerNote();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region SavedReportDto Mappers

      public static SavedReportDto AsDto(this SavedReport entity)
      {
        var dto = new SavedReportDto
        {
          ReportType = entity.ReportType,
          Criteria = entity.Criteria,
          UserId = entity.UserId,
          Name = entity.Name,
          DisplayType = entity.DisplayType,
          ReportDate = entity.ReportDate,
          DisplayOnDashboard = entity.DisplayOnDashboard,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static SavedReport CopyTo(this SavedReportDto dto, SavedReport entity)
      {
        entity.ReportType = dto.ReportType;
        entity.Criteria = dto.Criteria;
        entity.UserId = dto.UserId;
        entity.Name = dto.Name;
        entity.DisplayType = dto.DisplayType;
        entity.ReportDate = dto.ReportDate;
        entity.DisplayOnDashboard = dto.DisplayOnDashboard;
        return entity;
      }

      public static SavedReport CopyTo(this SavedReportDto dto)
      {
        var entity = new SavedReport();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeekerCountDto Mappers

      public static JobSeekerCountDto AsDto(this JobSeekerCount entity)
      {
        var dto = new JobSeekerCountDto
        {
          Name = entity.Name,
          Count = entity.Count,
          CountDate = entity.CountDate,
          JobSeekerId = entity.JobSeekerId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeekerCount CopyTo(this JobSeekerCountDto dto, JobSeekerCount entity)
      {
        entity.Name = dto.Name;
        entity.Count = dto.Count;
        entity.CountDate = dto.CountDate;
        entity.JobSeekerId = dto.JobSeekerId;
        return entity;
      }

      public static JobSeekerCount CopyTo(this JobSeekerCountDto dto)
      {
        var entity = new JobSeekerCount();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region EmployerCountDto Mappers

      public static EmployerCountDto AsDto(this EmployerCount entity)
      {
        var dto = new EmployerCountDto
        {
          Name = entity.Name,
          Count = entity.Count,
          CountDate = entity.CountDate,
          EmployerId = entity.EmployerId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static EmployerCount CopyTo(this EmployerCountDto dto, EmployerCount entity)
      {
        entity.Name = dto.Name;
        entity.Count = dto.Count;
        entity.CountDate = dto.CountDate;
        entity.EmployerId = dto.EmployerId;
        return entity;
      }

      public static EmployerCount CopyTo(this EmployerCountDto dto)
      {
        var entity = new EmployerCount();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region ReportDateDto Mappers

      public static ReportDateDto AsDto(this ReportDate entity)
      {
        var dto = new ReportDateDto
        {
          ExecutionDate = entity.ExecutionDate,
          ReportStartDate = entity.ReportStartDate,
          ReportEndDate = entity.ReportEndDate,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static ReportDate CopyTo(this ReportDateDto dto, ReportDate entity)
      {
        entity.ExecutionDate = dto.ExecutionDate;
        entity.ReportStartDate = dto.ReportStartDate;
        entity.ReportEndDate = dto.ReportEndDate;
        return entity;
      }

      public static ReportDate CopyTo(this ReportDateDto dto)
      {
        var entity = new ReportDate();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region JobSeekerActivityDto Mappers

      public static JobSeekerActivityDto AsDto(this JobSeekerActivity entity)
      {
        var dto = new JobSeekerActivityDto
        {
          StaffName = entity.StaffName,
          ActivityDate = entity.ActivityDate,
          Activity = entity.Activity,
          UserId = entity.UserId,
          AssistAction = entity.AssistAction,
          JobSeekerFullName = entity.JobSeekerFullName,
          JobSeekerId = entity.JobSeekerId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static JobSeekerActivity CopyTo(this JobSeekerActivityDto dto, JobSeekerActivity entity)
      {
        entity.StaffName = dto.StaffName;
        entity.ActivityDate = dto.ActivityDate;
        entity.Activity = dto.Activity;
        entity.UserId = dto.UserId;
        entity.AssistAction = dto.AssistAction;
        entity.JobSeekerFullName = dto.JobSeekerFullName;
        entity.JobSeekerId = dto.JobSeekerId;
        return entity;
      }

      public static JobSeekerActivity CopyTo(this JobSeekerActivityDto dto)
      {
        var entity = new JobSeekerActivity();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region EmployerActivityDto Mappers

      public static EmployerActivityDto AsDto(this EmployerActivity entity)
      {
        var dto = new EmployerActivityDto
        {
          StaffName = entity.StaffName,
          ActivityDate = entity.ActivityDate,
          Activity = entity.Activity,
          UserId = entity.UserId,
          AssistAction = entity.AssistAction,
          EmployerName = entity.EmployerName,
          EmployerId = entity.EmployerId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static EmployerActivity CopyTo(this EmployerActivityDto dto, EmployerActivity entity)
      {
        entity.StaffName = dto.StaffName;
        entity.ActivityDate = dto.ActivityDate;
        entity.Activity = dto.Activity;
        entity.UserId = dto.UserId;
        entity.AssistAction = dto.AssistAction;
        entity.EmployerName = dto.EmployerName;
        entity.EmployerId = dto.EmployerId;
        return entity;
      }

      public static EmployerActivity CopyTo(this EmployerActivityDto dto)
      {
        var entity = new EmployerActivity();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region LookUpItemDto Mappers

      public static LookUpItemDto AsDto(this LookUpItem entity)
      {
        var dto = new LookUpItemDto
        {
          LookUpType = entity.LookUpType,
          LookUpValue = entity.LookUpValue,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static LookUpItem CopyTo(this LookUpItemDto dto, LookUpItem entity)
      {
        entity.LookUpType = dto.LookUpType;
        entity.LookUpValue = dto.LookUpValue;
        return entity;
      }

      public static LookUpItem CopyTo(this LookUpItemDto dto)
      {
        var entity = new LookUpItem();
        return dto.CopyTo(entity);
      }	

      #endregion

    }
}
*/

#endregion
