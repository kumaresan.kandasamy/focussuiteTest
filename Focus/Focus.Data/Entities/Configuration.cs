using System;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Validation;
using Mindscape.LightSpeed.Linq;

#region Entities

namespace Focus.Data.Configuration.Entities
{
  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.CodeGroup", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class CodeGroup : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    [ValidateUnique]
    private string _key;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";


    #endregion
    
    #region Relationships

    [ReverseAssociation("CodeGroup")]
    private readonly EntityCollection<CodeGroupItem> _codeGroupItems = new EntityCollection<CodeGroupItem>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<CodeGroupItem> CodeGroupItems
    {
      get { return Get(_codeGroupItems); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.CodeGroupItem", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class CodeGroupItem : Entity<long>
  {
    #region Fields
  
    private int _displayOrder;
    private long _codeGroupId;
    private long _codeItemId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the DisplayOrder entity attribute.</summary>
    public const string DisplayOrderField = "DisplayOrder";
    /// <summary>Identifies the CodeGroupId entity attribute.</summary>
    public const string CodeGroupIdField = "CodeGroupId";
    /// <summary>Identifies the CodeItemId entity attribute.</summary>
    public const string CodeItemIdField = "CodeItemId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("CodeGroupItems")]
    private readonly EntityHolder<CodeGroup> _codeGroup = new EntityHolder<CodeGroup>();
    [ReverseAssociation("CodeGroupItems")]
    private readonly EntityHolder<CodeItem> _codeItem = new EntityHolder<CodeItem>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public CodeGroup CodeGroup
    {
      get { return Get(_codeGroup); }
      set { Set(_codeGroup, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public CodeItem CodeItem
    {
      get { return Get(_codeItem); }
      set { Set(_codeItem, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public int DisplayOrder
    {
      get { return Get(ref _displayOrder, "DisplayOrder"); }
      set { Set(ref _displayOrder, value, "DisplayOrder"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="CodeGroup" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long CodeGroupId
    {
      get { return Get(ref _codeGroupId, "CodeGroupId"); }
      set { Set(ref _codeGroupId, value, "CodeGroupId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="CodeItem" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long CodeItemId
    {
      get { return Get(ref _codeItemId, "CodeItemId"); }
      set { Set(ref _codeItemId, value, "CodeItemId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.CodeItem", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class CodeItem : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateUnique]
    [ValidateLength(0, 200)]
    private string _key;
    private bool _isSystem;
    [ValidateLength(0, 200)]
    private string _parentKey;
    [ValidateLength(0, 36)]
    private string _externalId;
    private System.Nullable<int> _customFilterId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the IsSystem entity attribute.</summary>
    public const string IsSystemField = "IsSystem";
    /// <summary>Identifies the ParentKey entity attribute.</summary>
    public const string ParentKeyField = "ParentKey";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";
    /// <summary>Identifies the CustomFilterId entity attribute.</summary>
    public const string CustomFilterIdField = "CustomFilterId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("CodeItem")]
    private readonly EntityCollection<CodeGroupItem> _codeGroupItems = new EntityCollection<CodeGroupItem>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<CodeGroupItem> CodeGroupItems
    {
      get { return Get(_codeGroupItems); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsSystem
    {
      get { return Get(ref _isSystem, "IsSystem"); }
      set { Set(ref _isSystem, value, "IsSystem"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ParentKey
    {
      get { return Get(ref _parentKey, "ParentKey"); }
      set { Set(ref _parentKey, value, "ParentKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> CustomFilterId
    {
      get { return Get(ref _customFilterId, "CustomFilterId"); }
      set { Set(ref _customFilterId, value, "CustomFilterId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.ConfigurationItem", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class ConfigurationItem : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _key;
    [ValidatePresence]
    [ValidateLength(0, 2000)]
    private string _value;
    private bool _internalOnly;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the Value entity attribute.</summary>
    public const string ValueField = "Value";
    /// <summary>Identifies the InternalOnly entity attribute.</summary>
    public const string InternalOnlyField = "InternalOnly";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Value
    {
      get { return Get(ref _value, "Value"); }
      set { Set(ref _value, value, "Value"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool InternalOnly
    {
      get { return Get(ref _internalOnly, "InternalOnly"); }
      set { Set(ref _internalOnly, value, "InternalOnly"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.Localisation", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class Localisation : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 5)]
    private string _culture;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Culture entity attribute.</summary>
    public const string CultureField = "Culture";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Localisation")]
    private readonly EntityCollection<LocalisationItem> _localisationItems = new EntityCollection<LocalisationItem>();
    [ReverseAssociation("Localisation")]
    private readonly EntityCollection<CertificateLicenceLocalisationItem> _certificateLicences = new EntityCollection<CertificateLicenceLocalisationItem>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<LocalisationItem> LocalisationItems
    {
      get { return Get(_localisationItems); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<CertificateLicenceLocalisationItem> CertificateLicences
    {
      get { return Get(_certificateLicences); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Culture
    {
      get { return Get(ref _culture, "Culture"); }
      set { Set(ref _culture, value, "Culture"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.LocalisationItem", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class LocalisationItem : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 50)]
    private string _contextKey;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _key;
    [ValidatePresence]
    private string _value;
    private bool _localised;
    private long _localisationId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ContextKey entity attribute.</summary>
    public const string ContextKeyField = "ContextKey";
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the Value entity attribute.</summary>
    public const string ValueField = "Value";
    /// <summary>Identifies the Localised entity attribute.</summary>
    public const string LocalisedField = "Localised";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("LocalisationItems")]
    private readonly EntityHolder<Localisation> _localisation = new EntityHolder<Localisation>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Localisation Localisation
    {
      get { return Get(_localisation); }
      set { Set(_localisation, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string ContextKey
    {
      get { return Get(ref _contextKey, "ContextKey"); }
      set { Set(ref _contextKey, value, "ContextKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Value
    {
      get { return Get(ref _value, "Value"); }
      set { Set(ref _value, value, "Value"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool Localised
    {
      get { return Get(ref _localised, "Localised"); }
      set { Set(ref _localised, value, "Localised"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Localisation" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.EmailTemplate", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class EmailTemplate : Entity<long>
  {
    #region Fields
  
    private Focus.Core.EmailTemplateTypes _emailTemplateType;
    [ValidateLength(0, 1000)]
    private string _subject;
    private string _body;
    [ValidateLength(0, 100)]
    private string _salutation;
    [ValidateLength(0, 100)]
    private string _recipient;
    private Focus.Core.SenderEmailTypes _senderEmailType;
    [ValidateLength(0, 200)]
    private string _clientSpecificEmailAddress;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the EmailTemplateType entity attribute.</summary>
    public const string EmailTemplateTypeField = "EmailTemplateType";
    /// <summary>Identifies the Subject entity attribute.</summary>
    public const string SubjectField = "Subject";
    /// <summary>Identifies the Body entity attribute.</summary>
    public const string BodyField = "Body";
    /// <summary>Identifies the Salutation entity attribute.</summary>
    public const string SalutationField = "Salutation";
    /// <summary>Identifies the Recipient entity attribute.</summary>
    public const string RecipientField = "Recipient";
    /// <summary>Identifies the SenderEmailType entity attribute.</summary>
    public const string SenderEmailTypeField = "SenderEmailType";
    /// <summary>Identifies the ClientSpecificEmailAddress entity attribute.</summary>
    public const string ClientSpecificEmailAddressField = "ClientSpecificEmailAddress";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.EmailTemplateTypes EmailTemplateType
    {
      get { return Get(ref _emailTemplateType, "EmailTemplateType"); }
      set { Set(ref _emailTemplateType, value, "EmailTemplateType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Subject
    {
      get { return Get(ref _subject, "Subject"); }
      set { Set(ref _subject, value, "Subject"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Body
    {
      get { return Get(ref _body, "Body"); }
      set { Set(ref _body, value, "Body"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Salutation
    {
      get { return Get(ref _salutation, "Salutation"); }
      set { Set(ref _salutation, value, "Salutation"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Recipient
    {
      get { return Get(ref _recipient, "Recipient"); }
      set { Set(ref _recipient, value, "Recipient"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.SenderEmailTypes SenderEmailType
    {
      get { return Get(ref _senderEmailType, "SenderEmailType"); }
      set { Set(ref _senderEmailType, value, "SenderEmailType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ClientSpecificEmailAddress
    {
      get { return Get(ref _clientSpecificEmailAddress, "ClientSpecificEmailAddress"); }
      set { Set(ref _clientSpecificEmailAddress, value, "ClientSpecificEmailAddress"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.CertificateLicence", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class CertificateLicence : Entity<long>
  {
    #region Fields
  
    private string _key;
    private Focus.Core.CertifcateLicenseTypes _certificateLicenseType;
    [ValidateLength(0, 40)]
    private string _onetParentCode;
    private string _isCertificate;
    private string _isLicence;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the CertificateLicenseType entity attribute.</summary>
    public const string CertificateLicenseTypeField = "CertificateLicenseType";
    /// <summary>Identifies the OnetParentCode entity attribute.</summary>
    public const string OnetParentCodeField = "OnetParentCode";
    /// <summary>Identifies the IsCertificate entity attribute.</summary>
    public const string IsCertificateField = "IsCertificate";
    /// <summary>Identifies the IsLicence entity attribute.</summary>
    public const string IsLicenceField = "IsLicence";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.CertifcateLicenseTypes CertificateLicenseType
    {
      get { return Get(ref _certificateLicenseType, "CertificateLicenseType"); }
      set { Set(ref _certificateLicenseType, value, "CertificateLicenseType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetParentCode
    {
      get { return Get(ref _onetParentCode, "OnetParentCode"); }
      set { Set(ref _onetParentCode, value, "OnetParentCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string IsCertificate
    {
      get { return Get(ref _isCertificate, "IsCertificate"); }
      set { Set(ref _isCertificate, value, "IsCertificate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string IsLicence
    {
      get { return Get(ref _isLicence, "IsLicence"); }
      set { Set(ref _isLicence, value, "IsLicence"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.CertificateLicenceLocalisationItem", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class CertificateLicenceLocalisationItem : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _key;
    [ValidateLength(0, 500)]
    private string _value;
    private long _localisationId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the Value entity attribute.</summary>
    public const string ValueField = "Value";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("CertificateLicences")]
    private readonly EntityHolder<Localisation> _localisation = new EntityHolder<Localisation>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Localisation Localisation
    {
      get { return Get(_localisation); }
      set { Set(_localisation, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Value
    {
      get { return Get(ref _value, "Value"); }
      set { Set(ref _value, value, "Value"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Localisation" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.ApplicationImage", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class ApplicationImage : Entity<long>
  {
    #region Fields
  
    private Focus.Core.ApplicationImageTypes _type;
    private byte[] _image;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Type entity attribute.</summary>
    public const string TypeField = "Type";
    /// <summary>Identifies the Image entity attribute.</summary>
    public const string ImageField = "Image";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ApplicationImageTypes Type
    {
      get { return Get(ref _type, "Type"); }
      set { Set(ref _type, value, "Type"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public byte[] Image
    {
      get { return Get(ref _image, "Image"); }
      set { Set(ref _image, value, "Image"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.LookupItemsView")]
  public partial class LookupItemsView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _key;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _lookupType;
    [ValidatePresence]
    private string _value;
    private int _displayOrder;
    private long _parentId;
    [ValidateLength(0, 200)]
    private string _parentKey;
    [ValidatePresence]
    [ValidateLength(0, 5)]
    private string _culture;
    [ValidateLength(0, 36)]
    private string _externalId;
    private System.Nullable<int> _customFilterId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the LookupType entity attribute.</summary>
    public const string LookupTypeField = "LookupType";
    /// <summary>Identifies the Value entity attribute.</summary>
    public const string ValueField = "Value";
    /// <summary>Identifies the DisplayOrder entity attribute.</summary>
    public const string DisplayOrderField = "DisplayOrder";
    /// <summary>Identifies the ParentId entity attribute.</summary>
    public const string ParentIdField = "ParentId";
    /// <summary>Identifies the ParentKey entity attribute.</summary>
    public const string ParentKeyField = "ParentKey";
    /// <summary>Identifies the Culture entity attribute.</summary>
    public const string CultureField = "Culture";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";
    /// <summary>Identifies the CustomFilterId entity attribute.</summary>
    public const string CustomFilterIdField = "CustomFilterId";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LookupType
    {
      get { return Get(ref _lookupType, "LookupType"); }
      set { Set(ref _lookupType, value, "LookupType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Value
    {
      get { return Get(ref _value, "Value"); }
      set { Set(ref _value, value, "Value"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int DisplayOrder
    {
      get { return Get(ref _displayOrder, "DisplayOrder"); }
      set { Set(ref _displayOrder, value, "DisplayOrder"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long ParentId
    {
      get { return Get(ref _parentId, "ParentId"); }
      set { Set(ref _parentId, value, "ParentId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ParentKey
    {
      get { return Get(ref _parentKey, "ParentKey"); }
      set { Set(ref _parentKey, value, "ParentKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Culture
    {
      get { return Get(ref _culture, "Culture"); }
      set { Set(ref _culture, value, "Culture"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> CustomFilterId
    {
      get { return Get(ref _customFilterId, "CustomFilterId"); }
      set { Set(ref _customFilterId, value, "CustomFilterId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.CensorshipRule", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class CensorshipRule : Entity<long>
  {
    #region Fields
  
    private Focus.Core.CensorshipType _type;
    private Focus.Core.CensorshipLevel _level;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _phrase;
    private string _whiteList;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Type entity attribute.</summary>
    public const string TypeField = "Type";
    /// <summary>Identifies the Level entity attribute.</summary>
    public const string LevelField = "Level";
    /// <summary>Identifies the Phrase entity attribute.</summary>
    public const string PhraseField = "Phrase";
    /// <summary>Identifies the WhiteList entity attribute.</summary>
    public const string WhiteListField = "WhiteList";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.CensorshipType Type
    {
      get { return Get(ref _type, "Type"); }
      set { Set(ref _type, value, "Type"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.CensorshipLevel Level
    {
      get { return Get(ref _level, "Level"); }
      set { Set(ref _level, value, "Level"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Phrase
    {
      get { return Get(ref _phrase, "Phrase"); }
      set { Set(ref _phrase, value, "Phrase"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string WhiteList
    {
      get { return Get(ref _whiteList, "WhiteList"); }
      set { Set(ref _whiteList, value, "WhiteList"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.ActivityCategory", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class ActivityCategory : Entity<long>
  {
    #region Fields
  
    private string _localisationKey;
    private string _name;
    private bool _visible;
    private bool _administrable;
    private Focus.Core.ActivityType _activityType;
    private bool _editable;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the LocalisationKey entity attribute.</summary>
    public const string LocalisationKeyField = "LocalisationKey";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the Visible entity attribute.</summary>
    public const string VisibleField = "Visible";
    /// <summary>Identifies the Administrable entity attribute.</summary>
    public const string AdministrableField = "Administrable";
    /// <summary>Identifies the ActivityType entity attribute.</summary>
    public const string ActivityTypeField = "ActivityType";
    /// <summary>Identifies the Editable entity attribute.</summary>
    public const string EditableField = "Editable";


    #endregion
    
    #region Relationships

    [ReverseAssociation("ActivityCategory")]
    private readonly EntityCollection<Activity> _activities = new EntityCollection<Activity>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Activity> Activities
    {
      get { return Get(_activities); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string LocalisationKey
    {
      get { return Get(ref _localisationKey, "LocalisationKey"); }
      set { Set(ref _localisationKey, value, "LocalisationKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool Visible
    {
      get { return Get(ref _visible, "Visible"); }
      set { Set(ref _visible, value, "Visible"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool Administrable
    {
      get { return Get(ref _administrable, "Administrable"); }
      set { Set(ref _administrable, value, "Administrable"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ActivityType ActivityType
    {
      get { return Get(ref _activityType, "ActivityType"); }
      set { Set(ref _activityType, value, "ActivityType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool Editable
    {
      get { return Get(ref _editable, "Editable"); }
      set { Set(ref _editable, value, "Editable"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.Activity", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class Activity : Entity<long>
  {
    #region Fields
  
    private string _localisationKey;
    private string _name;
    private bool _enableDisable;
    private bool _administrable;
    private int _sortOrder;
    private bool _used;
    private long _externalId;
    private long _activityCategoryId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the LocalisationKey entity attribute.</summary>
    public const string LocalisationKeyField = "LocalisationKey";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the EnableDisable entity attribute.</summary>
    public const string EnableDisableField = "EnableDisable";
    /// <summary>Identifies the Administrable entity attribute.</summary>
    public const string AdministrableField = "Administrable";
    /// <summary>Identifies the SortOrder entity attribute.</summary>
    public const string SortOrderField = "SortOrder";
    /// <summary>Identifies the Used entity attribute.</summary>
    public const string UsedField = "Used";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";
    /// <summary>Identifies the ActivityCategoryId entity attribute.</summary>
    public const string ActivityCategoryIdField = "ActivityCategoryId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Activities")]
    private readonly EntityHolder<ActivityCategory> _activityCategory = new EntityHolder<ActivityCategory>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public ActivityCategory ActivityCategory
    {
      get { return Get(_activityCategory); }
      set { Set(_activityCategory, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string LocalisationKey
    {
      get { return Get(ref _localisationKey, "LocalisationKey"); }
      set { Set(ref _localisationKey, value, "LocalisationKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool EnableDisable
    {
      get { return Get(ref _enableDisable, "EnableDisable"); }
      set { Set(ref _enableDisable, value, "EnableDisable"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool Administrable
    {
      get { return Get(ref _administrable, "Administrable"); }
      set { Set(ref _administrable, value, "Administrable"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SortOrder
    {
      get { return Get(ref _sortOrder, "SortOrder"); }
      set { Set(ref _sortOrder, value, "SortOrder"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool Used
    {
      get { return Get(ref _used, "Used"); }
      set { Set(ref _used, value, "Used"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="ActivityCategory" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long ActivityCategoryId
    {
      get { return Get(ref _activityCategoryId, "ActivityCategoryId"); }
      set { Set(ref _activityCategoryId, value, "ActivityCategoryId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.ActivityView")]
  public partial class ActivityView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    private string _categoryLocalisationKey;
    [ValidatePresence]
    private string _categoryName;
    private bool _categoryVisible;
    private bool _categoryAdministrable;
    private Focus.Core.ActivityType _activityType;
    [ValidatePresence]
    private string _activityLocalisationKey;
    [ValidatePresence]
    private string _activityName;
    private bool _activityVisible;
    private bool _activityAdministrable;
    private int _sortOrder;
    private long _activityCategoryId;
    private long _activityExternalId;
    private bool _editable;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the CategoryLocalisationKey entity attribute.</summary>
    public const string CategoryLocalisationKeyField = "CategoryLocalisationKey";
    /// <summary>Identifies the CategoryName entity attribute.</summary>
    public const string CategoryNameField = "CategoryName";
    /// <summary>Identifies the CategoryVisible entity attribute.</summary>
    public const string CategoryVisibleField = "CategoryVisible";
    /// <summary>Identifies the CategoryAdministrable entity attribute.</summary>
    public const string CategoryAdministrableField = "CategoryAdministrable";
    /// <summary>Identifies the ActivityType entity attribute.</summary>
    public const string ActivityTypeField = "ActivityType";
    /// <summary>Identifies the ActivityLocalisationKey entity attribute.</summary>
    public const string ActivityLocalisationKeyField = "ActivityLocalisationKey";
    /// <summary>Identifies the ActivityName entity attribute.</summary>
    public const string ActivityNameField = "ActivityName";
    /// <summary>Identifies the ActivityVisible entity attribute.</summary>
    public const string ActivityVisibleField = "ActivityVisible";
    /// <summary>Identifies the ActivityAdministrable entity attribute.</summary>
    public const string ActivityAdministrableField = "ActivityAdministrable";
    /// <summary>Identifies the SortOrder entity attribute.</summary>
    public const string SortOrderField = "SortOrder";
    /// <summary>Identifies the ActivityCategoryId entity attribute.</summary>
    public const string ActivityCategoryIdField = "ActivityCategoryId";
    /// <summary>Identifies the ActivityExternalId entity attribute.</summary>
    public const string ActivityExternalIdField = "ActivityExternalId";
    /// <summary>Identifies the Editable entity attribute.</summary>
    public const string EditableField = "Editable";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string CategoryLocalisationKey
    {
      get { return Get(ref _categoryLocalisationKey, "CategoryLocalisationKey"); }
      set { Set(ref _categoryLocalisationKey, value, "CategoryLocalisationKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CategoryName
    {
      get { return Get(ref _categoryName, "CategoryName"); }
      set { Set(ref _categoryName, value, "CategoryName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool CategoryVisible
    {
      get { return Get(ref _categoryVisible, "CategoryVisible"); }
      set { Set(ref _categoryVisible, value, "CategoryVisible"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool CategoryAdministrable
    {
      get { return Get(ref _categoryAdministrable, "CategoryAdministrable"); }
      set { Set(ref _categoryAdministrable, value, "CategoryAdministrable"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ActivityType ActivityType
    {
      get { return Get(ref _activityType, "ActivityType"); }
      set { Set(ref _activityType, value, "ActivityType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ActivityLocalisationKey
    {
      get { return Get(ref _activityLocalisationKey, "ActivityLocalisationKey"); }
      set { Set(ref _activityLocalisationKey, value, "ActivityLocalisationKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ActivityName
    {
      get { return Get(ref _activityName, "ActivityName"); }
      set { Set(ref _activityName, value, "ActivityName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool ActivityVisible
    {
      get { return Get(ref _activityVisible, "ActivityVisible"); }
      set { Set(ref _activityVisible, value, "ActivityVisible"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool ActivityAdministrable
    {
      get { return Get(ref _activityAdministrable, "ActivityAdministrable"); }
      set { Set(ref _activityAdministrable, value, "ActivityAdministrable"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SortOrder
    {
      get { return Get(ref _sortOrder, "SortOrder"); }
      set { Set(ref _sortOrder, value, "SortOrder"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long ActivityCategoryId
    {
      get { return Get(ref _activityCategoryId, "ActivityCategoryId"); }
      set { Set(ref _activityCategoryId, value, "ActivityCategoryId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long ActivityExternalId
    {
      get { return Get(ref _activityExternalId, "ActivityExternalId"); }
      set { Set(ref _activityExternalId, value, "ActivityExternalId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool Editable
    {
      get { return Get(ref _editable, "Editable"); }
      set { Set(ref _editable, value, "Editable"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.ExternalLookUpItem", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class ExternalLookUpItem : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 50)]
    private string _internalId;
    [ValidateLength(0, 150)]
    private string _externalId;
    private Focus.Core.ExternalLookUpType _externalLookUpType;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the InternalId entity attribute.</summary>
    public const string InternalIdField = "InternalId";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";
    /// <summary>Identifies the ExternalLookUpType entity attribute.</summary>
    public const string ExternalLookUpTypeField = "ExternalLookUpType";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string InternalId
    {
      get { return Get(ref _internalId, "InternalId"); }
      set { Set(ref _internalId, value, "InternalId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ExternalLookUpType ExternalLookUpType
    {
      get { return Get(ref _externalLookUpType, "ExternalLookUpType"); }
      set { Set(ref _externalLookUpType, value, "ExternalLookUpType"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.DefaultLocalisationItem", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class DefaultLocalisationItem : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    [ValidatePresence]
    private string _key;
    private string _defaultValue;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the DefaultValue entity attribute.</summary>
    public const string DefaultValueField = "DefaultValue";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string DefaultValue
    {
      get { return Get(ref _defaultValue, "DefaultValue"); }
      set { Set(ref _defaultValue, value, "DefaultValue"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.LaborInsightMapping")]
  public partial class LaborInsightMapping : Entity<System.Guid>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _laborInsightId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _laborInsightValue;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _codeGroupKey;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _codeItemKey;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the LaborInsightId entity attribute.</summary>
    public const string LaborInsightIdField = "LaborInsightId";
    /// <summary>Identifies the LaborInsightValue entity attribute.</summary>
    public const string LaborInsightValueField = "LaborInsightValue";
    /// <summary>Identifies the CodeGroupKey entity attribute.</summary>
    public const string CodeGroupKeyField = "CodeGroupKey";
    /// <summary>Identifies the CodeItemKey entity attribute.</summary>
    public const string CodeItemKeyField = "CodeItemKey";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string LaborInsightId
    {
      get { return Get(ref _laborInsightId, "LaborInsightId"); }
      set { Set(ref _laborInsightId, value, "LaborInsightId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LaborInsightValue
    {
      get { return Get(ref _laborInsightValue, "LaborInsightValue"); }
      set { Set(ref _laborInsightValue, value, "LaborInsightValue"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CodeGroupKey
    {
      get { return Get(ref _codeGroupKey, "CodeGroupKey"); }
      set { Set(ref _codeGroupKey, value, "CodeGroupKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CodeItemKey
    {
      get { return Get(ref _codeItemKey, "CodeItemKey"); }
      set { Set(ref _codeItemKey, value, "CodeItemKey"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.LaborInsightMappingView")]
  public partial class LaborInsightMappingView : Entity<System.Guid>
  {
    #region Fields
  
    private long _codeGroupItemId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _codeGroupKey;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _laborInsightId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _laborInsightValue;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the CodeGroupItemId entity attribute.</summary>
    public const string CodeGroupItemIdField = "CodeGroupItemId";
    /// <summary>Identifies the CodeGroupKey entity attribute.</summary>
    public const string CodeGroupKeyField = "CodeGroupKey";
    /// <summary>Identifies the LaborInsightId entity attribute.</summary>
    public const string LaborInsightIdField = "LaborInsightId";
    /// <summary>Identifies the LaborInsightValue entity attribute.</summary>
    public const string LaborInsightValueField = "LaborInsightValue";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public long CodeGroupItemId
    {
      get { return Get(ref _codeGroupItemId, "CodeGroupItemId"); }
      set { Set(ref _codeGroupItemId, value, "CodeGroupItemId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CodeGroupKey
    {
      get { return Get(ref _codeGroupKey, "CodeGroupKey"); }
      set { Set(ref _codeGroupKey, value, "CodeGroupKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LaborInsightId
    {
      get { return Get(ref _laborInsightId, "LaborInsightId"); }
      set { Set(ref _laborInsightId, value, "LaborInsightId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LaborInsightValue
    {
      get { return Get(ref _laborInsightValue, "LaborInsightValue"); }
      set { Set(ref _laborInsightValue, value, "LaborInsightValue"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Config.StarRatingMinimumScore", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class StarRatingMinimumScore : Entity<long>
  {
    #region Fields
  
    private int _minimumScore;
    private int _maximumScore;
    private int _starRating;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the MinimumScore entity attribute.</summary>
    public const string MinimumScoreField = "MinimumScore";
    /// <summary>Identifies the MaximumScore entity attribute.</summary>
    public const string MaximumScoreField = "MaximumScore";
    /// <summary>Identifies the StarRating entity attribute.</summary>
    public const string StarRatingField = "StarRating";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public int MinimumScore
    {
      get { return Get(ref _minimumScore, "MinimumScore"); }
      set { Set(ref _minimumScore, value, "MinimumScore"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int MaximumScore
    {
      get { return Get(ref _maximumScore, "MaximumScore"); }
      set { Set(ref _maximumScore, value, "MaximumScore"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int StarRating
    {
      get { return Get(ref _starRating, "StarRating"); }
      set { Set(ref _starRating, value, "StarRating"); }
    }

    #endregion
  }
	


  /// <summary>
  /// Provides a strong-typed unit of work for working with the Configuration model.
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  public partial class ConfigurationUnitOfWork : Mindscape.LightSpeed.UnitOfWork
  {

    public System.Linq.IQueryable<CodeGroup> CodeGroups
    {
      get { return this.Query<CodeGroup>(); }
    }
    
    public System.Linq.IQueryable<CodeGroupItem> CodeGroupItems
    {
      get { return this.Query<CodeGroupItem>(); }
    }
    
    public System.Linq.IQueryable<CodeItem> CodeItems
    {
      get { return this.Query<CodeItem>(); }
    }
    
    public System.Linq.IQueryable<ConfigurationItem> ConfigurationItems
    {
      get { return this.Query<ConfigurationItem>(); }
    }
    
    public System.Linq.IQueryable<Localisation> Localisations
    {
      get { return this.Query<Localisation>(); }
    }
    
    public System.Linq.IQueryable<LocalisationItem> LocalisationItems
    {
      get { return this.Query<LocalisationItem>(); }
    }
    
    public System.Linq.IQueryable<EmailTemplate> EmailTemplates
    {
      get { return this.Query<EmailTemplate>(); }
    }
    
    public System.Linq.IQueryable<CertificateLicence> CertificateLicences
    {
      get { return this.Query<CertificateLicence>(); }
    }
    
    public System.Linq.IQueryable<CertificateLicenceLocalisationItem> CertificateLicenceLocalisationItems
    {
      get { return this.Query<CertificateLicenceLocalisationItem>(); }
    }
    
    public System.Linq.IQueryable<ApplicationImage> ApplicationImages
    {
      get { return this.Query<ApplicationImage>(); }
    }
    
    public System.Linq.IQueryable<LookupItemsView> LookupItemsViews
    {
      get { return this.Query<LookupItemsView>(); }
    }
    
    public System.Linq.IQueryable<CensorshipRule> CensorshipRules
    {
      get { return this.Query<CensorshipRule>(); }
    }
    
    public System.Linq.IQueryable<ActivityCategory> ActivityCategories
    {
      get { return this.Query<ActivityCategory>(); }
    }
    
    public System.Linq.IQueryable<Activity> Activities
    {
      get { return this.Query<Activity>(); }
    }
    
    public System.Linq.IQueryable<ActivityView> ActivityViews
    {
      get { return this.Query<ActivityView>(); }
    }
    
    public System.Linq.IQueryable<ExternalLookUpItem> ExternalLookUpItems
    {
      get { return this.Query<ExternalLookUpItem>(); }
    }
    
    public System.Linq.IQueryable<DefaultLocalisationItem> DefaultLocalisationItems
    {
      get { return this.Query<DefaultLocalisationItem>(); }
    }
    
    public System.Linq.IQueryable<LaborInsightMapping> LaborInsightMappings
    {
      get { return this.Query<LaborInsightMapping>(); }
    }
    
    public System.Linq.IQueryable<LaborInsightMappingView> LaborInsightMappingViews
    {
      get { return this.Query<LaborInsightMappingView>(); }
    }
    
    public System.Linq.IQueryable<StarRatingMinimumScore> StarRatingMinimumScores
    {
      get { return this.Query<StarRatingMinimumScore>(); }
    }
    
  }
}

#endregion


#region Data Transfer Objects - Classes

/*
namespace Focus.Core.DataTransferObjects.FocusConfiguration.Entities
{
  using System;
  using System.Runtime.Serialization;

    [Serializable]
    [DataContract(Name="CodeGroup", Namespace = Constants.DataContractNamespace)]
    public class CodeGroupDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Key { get; set; }
    }

    [Serializable]
    [DataContract(Name="CodeGroupItem", Namespace = Constants.DataContractNamespace)]
    public class CodeGroupItemDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public int DisplayOrder { get; set; }
      [DataMember]
      public long CodeGroupId { get; set; }
      [DataMember]
      public long CodeItemId { get; set; }
    }

    [Serializable]
    [DataContract(Name="CodeItem", Namespace = Constants.DataContractNamespace)]
    public class CodeItemDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public bool IsSystem { get; set; }
      [DataMember]
      public string ParentKey { get; set; }
      [DataMember]
      public string ExternalId { get; set; }
      [DataMember]
      public int? CustomFilterId { get; set; }
    }

    [Serializable]
    [DataContract(Name="ConfigurationItem", Namespace = Constants.DataContractNamespace)]
    public class ConfigurationItemDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public string Value { get; set; }
      [DataMember]
      public bool InternalOnly { get; set; }
    }

    [Serializable]
    [DataContract(Name="Localisation", Namespace = Constants.DataContractNamespace)]
    public class LocalisationDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Culture { get; set; }
    }

    [Serializable]
    [DataContract(Name="LocalisationItem", Namespace = Constants.DataContractNamespace)]
    public class LocalisationItemDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string ContextKey { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public string Value { get; set; }
      [DataMember]
      public bool Localised { get; set; }
      [DataMember]
      public long LocalisationId { get; set; }
    }

    [Serializable]
    [DataContract(Name="EmailTemplate", Namespace = Constants.DataContractNamespace)]
    public class EmailTemplateDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public EmailTemplateTypes EmailTemplateType { get; set; }
      [DataMember]
      public string Subject { get; set; }
      [DataMember]
      public string Body { get; set; }
      [DataMember]
      public string Salutation { get; set; }
      [DataMember]
      public string Recipient { get; set; }
      [DataMember]
      public SenderEmailTypes SenderEmailType { get; set; }
      [DataMember]
      public string ClientSpecificEmailAddress { get; set; }
    }

    [Serializable]
    [DataContract(Name="CertificateLicence", Namespace = Constants.DataContractNamespace)]
    public class CertificateLicenceDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public CertifcateLicenseTypes CertificateLicenseType { get; set; }
      [DataMember]
      public string OnetParentCode { get; set; }
      [DataMember]
      public string IsCertificate { get; set; }
      [DataMember]
      public string IsLicence { get; set; }
    }

    [Serializable]
    [DataContract(Name="CertificateLicenceLocalisationItem", Namespace = Constants.DataContractNamespace)]
    public class CertificateLicenceLocalisationItemDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public string Value { get; set; }
      [DataMember]
      public long LocalisationId { get; set; }
    }

    [Serializable]
    [DataContract(Name="ApplicationImage", Namespace = Constants.DataContractNamespace)]
    public class ApplicationImageDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public ApplicationImageTypes Type { get; set; }
      [DataMember]
      public byte[] Image { get; set; }
    }

    [Serializable]
    [DataContract(Name="LookupItemsView", Namespace = Constants.DataContractNamespace)]
    public class LookupItemsViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public string LookupType { get; set; }
      [DataMember]
      public string Value { get; set; }
      [DataMember]
      public int DisplayOrder { get; set; }
      [DataMember]
      public long ParentId { get; set; }
      [DataMember]
      public string ParentKey { get; set; }
      [DataMember]
      public string Culture { get; set; }
      [DataMember]
      public string ExternalId { get; set; }
      [DataMember]
      public int? CustomFilterId { get; set; }
    }

    [Serializable]
    [DataContract(Name="CensorshipRule", Namespace = Constants.DataContractNamespace)]
    public class CensorshipRuleDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public CensorshipType Type { get; set; }
      [DataMember]
      public CensorshipLevel Level { get; set; }
      [DataMember]
      public string Phrase { get; set; }
      [DataMember]
      public string WhiteList { get; set; }
    }

    [Serializable]
    [DataContract(Name="ActivityCategory", Namespace = Constants.DataContractNamespace)]
    public class ActivityCategoryDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string LocalisationKey { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public bool Visible { get; set; }
      [DataMember]
      public bool Administrable { get; set; }
      [DataMember]
      public ActivityType ActivityType { get; set; }
      [DataMember]
      public bool Editable { get; set; }
    }

    [Serializable]
    [DataContract(Name="Activity", Namespace = Constants.DataContractNamespace)]
    public class ActivityDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string LocalisationKey { get; set; }
      [DataMember]
      public string Name { get; set; }
      [DataMember]
      public bool EnableDisable { get; set; }
      [DataMember]
      public bool Administrable { get; set; }
      [DataMember]
      public int SortOrder { get; set; }
      [DataMember]
      public bool Used { get; set; }
      [DataMember]
      public long ExternalId { get; set; }
      [DataMember]
      public long ActivityCategoryId { get; set; }
    }

    [Serializable]
    [DataContract(Name="ActivityView", Namespace = Constants.DataContractNamespace)]
    public class ActivityViewDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string CategoryLocalisationKey { get; set; }
      [DataMember]
      public string CategoryName { get; set; }
      [DataMember]
      public bool CategoryVisible { get; set; }
      [DataMember]
      public bool CategoryAdministrable { get; set; }
      [DataMember]
      public ActivityType ActivityType { get; set; }
      [DataMember]
      public string ActivityLocalisationKey { get; set; }
      [DataMember]
      public string ActivityName { get; set; }
      [DataMember]
      public bool ActivityVisible { get; set; }
      [DataMember]
      public bool ActivityAdministrable { get; set; }
      [DataMember]
      public int SortOrder { get; set; }
      [DataMember]
      public long ActivityCategoryId { get; set; }
      [DataMember]
      public long ActivityExternalId { get; set; }
      [DataMember]
      public bool Editable { get; set; }
    }

    [Serializable]
    [DataContract(Name="ExternalLookUpItem", Namespace = Constants.DataContractNamespace)]
    public class ExternalLookUpItemDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public string InternalId { get; set; }
      [DataMember]
      public string ExternalId { get; set; }
      [DataMember]
      public ExternalLookUpType ExternalLookUpType { get; set; }
    }

    [Serializable]
    [DataContract(Name="DefaultLocalisationItem", Namespace = Constants.DataContractNamespace)]
    public class DefaultLocalisationItemDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public DateTime CreatedOn { get; set; }
      [DataMember]
      public string Key { get; set; }
      [DataMember]
      public string DefaultValue { get; set; }
    }

    [Serializable]
    [DataContract(Name="LaborInsightMapping", Namespace = Constants.DataContractNamespace)]
    public class LaborInsightMappingDto
    {
      [DataMember]
      public System.Guid? Id { get; set; }
      [DataMember]
      public string LaborInsightId { get; set; }
      [DataMember]
      public string LaborInsightValue { get; set; }
      [DataMember]
      public string CodeGroupKey { get; set; }
      [DataMember]
      public string CodeItemKey { get; set; }
    }

    [Serializable]
    [DataContract(Name="LaborInsightMappingView", Namespace = Constants.DataContractNamespace)]
    public class LaborInsightMappingViewDto
    {
      [DataMember]
      public System.Guid? Id { get; set; }
      [DataMember]
      public long CodeGroupItemId { get; set; }
      [DataMember]
      public string CodeGroupKey { get; set; }
      [DataMember]
      public string LaborInsightId { get; set; }
      [DataMember]
      public string LaborInsightValue { get; set; }
    }

    [Serializable]
    [DataContract(Name="StarRatingMinimumScore", Namespace = Constants.DataContractNamespace)]
    public class StarRatingMinimumScoreDto
    {
      [DataMember]
      public long? Id { get; set; }
      [DataMember]
      public int MinimumScore { get; set; }
      [DataMember]
      public int MaximumScore { get; set; }
      [DataMember]
      public int StarRating { get; set; }
    }


}
*/

#endregion

#region Data Transfer Objects - Mappers

/*
namespace Focus.Services.DtoMappers
{
    using Focus.Core.DataTransferObjects.FocusConfiguration.Entities;
    using Focus.Data.Configuration.Entities;

    public static class FocusConfiguration.EntitiesMappers
    {

      #region CodeGroupDto Mappers

      public static CodeGroupDto AsDto(this CodeGroup entity)
      {
        var dto = new CodeGroupDto
        {
          Key = entity.Key,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static CodeGroup CopyTo(this CodeGroupDto dto, CodeGroup entity)
      {
        entity.Key = dto.Key;
        return entity;
      }

      public static CodeGroup CopyTo(this CodeGroupDto dto)
      {
        var entity = new CodeGroup();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region CodeGroupItemDto Mappers

      public static CodeGroupItemDto AsDto(this CodeGroupItem entity)
      {
        var dto = new CodeGroupItemDto
        {
          DisplayOrder = entity.DisplayOrder,
          CodeGroupId = entity.CodeGroupId,
          CodeItemId = entity.CodeItemId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static CodeGroupItem CopyTo(this CodeGroupItemDto dto, CodeGroupItem entity)
      {
        entity.DisplayOrder = dto.DisplayOrder;
        entity.CodeGroupId = dto.CodeGroupId;
        entity.CodeItemId = dto.CodeItemId;
        return entity;
      }

      public static CodeGroupItem CopyTo(this CodeGroupItemDto dto)
      {
        var entity = new CodeGroupItem();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region CodeItemDto Mappers

      public static CodeItemDto AsDto(this CodeItem entity)
      {
        var dto = new CodeItemDto
        {
          Key = entity.Key,
          IsSystem = entity.IsSystem,
          ParentKey = entity.ParentKey,
          ExternalId = entity.ExternalId,
          CustomFilterId = entity.CustomFilterId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static CodeItem CopyTo(this CodeItemDto dto, CodeItem entity)
      {
        entity.Key = dto.Key;
        entity.IsSystem = dto.IsSystem;
        entity.ParentKey = dto.ParentKey;
        entity.ExternalId = dto.ExternalId;
        entity.CustomFilterId = dto.CustomFilterId;
        return entity;
      }

      public static CodeItem CopyTo(this CodeItemDto dto)
      {
        var entity = new CodeItem();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region ConfigurationItemDto Mappers

      public static ConfigurationItemDto AsDto(this ConfigurationItem entity)
      {
        var dto = new ConfigurationItemDto
        {
          Key = entity.Key,
          Value = entity.Value,
          InternalOnly = entity.InternalOnly,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static ConfigurationItem CopyTo(this ConfigurationItemDto dto, ConfigurationItem entity)
      {
        entity.Key = dto.Key;
        entity.Value = dto.Value;
        entity.InternalOnly = dto.InternalOnly;
        return entity;
      }

      public static ConfigurationItem CopyTo(this ConfigurationItemDto dto)
      {
        var entity = new ConfigurationItem();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region LocalisationDto Mappers

      public static LocalisationDto AsDto(this Localisation entity)
      {
        var dto = new LocalisationDto
        {
          Culture = entity.Culture,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static Localisation CopyTo(this LocalisationDto dto, Localisation entity)
      {
        entity.Culture = dto.Culture;
        return entity;
      }

      public static Localisation CopyTo(this LocalisationDto dto)
      {
        var entity = new Localisation();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region LocalisationItemDto Mappers

      public static LocalisationItemDto AsDto(this LocalisationItem entity)
      {
        var dto = new LocalisationItemDto
        {
          ContextKey = entity.ContextKey,
          Key = entity.Key,
          Value = entity.Value,
          Localised = entity.Localised,
          LocalisationId = entity.LocalisationId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static LocalisationItem CopyTo(this LocalisationItemDto dto, LocalisationItem entity)
      {
        entity.ContextKey = dto.ContextKey;
        entity.Key = dto.Key;
        entity.Value = dto.Value;
        entity.Localised = dto.Localised;
        entity.LocalisationId = dto.LocalisationId;
        return entity;
      }

      public static LocalisationItem CopyTo(this LocalisationItemDto dto)
      {
        var entity = new LocalisationItem();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region EmailTemplateDto Mappers

      public static EmailTemplateDto AsDto(this EmailTemplate entity)
      {
        var dto = new EmailTemplateDto
        {
          EmailTemplateType = entity.EmailTemplateType,
          Subject = entity.Subject,
          Body = entity.Body,
          Salutation = entity.Salutation,
          Recipient = entity.Recipient,
          SenderEmailType = entity.SenderEmailType,
          ClientSpecificEmailAddress = entity.ClientSpecificEmailAddress,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static EmailTemplate CopyTo(this EmailTemplateDto dto, EmailTemplate entity)
      {
        entity.EmailTemplateType = dto.EmailTemplateType;
        entity.Subject = dto.Subject;
        entity.Body = dto.Body;
        entity.Salutation = dto.Salutation;
        entity.Recipient = dto.Recipient;
        entity.SenderEmailType = dto.SenderEmailType;
        entity.ClientSpecificEmailAddress = dto.ClientSpecificEmailAddress;
        return entity;
      }

      public static EmailTemplate CopyTo(this EmailTemplateDto dto)
      {
        var entity = new EmailTemplate();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region CertificateLicenceDto Mappers

      public static CertificateLicenceDto AsDto(this CertificateLicence entity)
      {
        var dto = new CertificateLicenceDto
        {
          Key = entity.Key,
          CertificateLicenseType = entity.CertificateLicenseType,
          OnetParentCode = entity.OnetParentCode,
          IsCertificate = entity.IsCertificate,
          IsLicence = entity.IsLicence,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static CertificateLicence CopyTo(this CertificateLicenceDto dto, CertificateLicence entity)
      {
        entity.Key = dto.Key;
        entity.CertificateLicenseType = dto.CertificateLicenseType;
        entity.OnetParentCode = dto.OnetParentCode;
        entity.IsCertificate = dto.IsCertificate;
        entity.IsLicence = dto.IsLicence;
        return entity;
      }

      public static CertificateLicence CopyTo(this CertificateLicenceDto dto)
      {
        var entity = new CertificateLicence();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region CertificateLicenceLocalisationItemDto Mappers

      public static CertificateLicenceLocalisationItemDto AsDto(this CertificateLicenceLocalisationItem entity)
      {
        var dto = new CertificateLicenceLocalisationItemDto
        {
          Key = entity.Key,
          Value = entity.Value,
          LocalisationId = entity.LocalisationId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static CertificateLicenceLocalisationItem CopyTo(this CertificateLicenceLocalisationItemDto dto, CertificateLicenceLocalisationItem entity)
      {
        entity.Key = dto.Key;
        entity.Value = dto.Value;
        entity.LocalisationId = dto.LocalisationId;
        return entity;
      }

      public static CertificateLicenceLocalisationItem CopyTo(this CertificateLicenceLocalisationItemDto dto)
      {
        var entity = new CertificateLicenceLocalisationItem();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region ApplicationImageDto Mappers

      public static ApplicationImageDto AsDto(this ApplicationImage entity)
      {
        var dto = new ApplicationImageDto
        {
          Type = entity.Type,
          Image = entity.Image,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static ApplicationImage CopyTo(this ApplicationImageDto dto, ApplicationImage entity)
      {
        entity.Type = dto.Type;
        entity.Image = dto.Image;
        return entity;
      }

      public static ApplicationImage CopyTo(this ApplicationImageDto dto)
      {
        var entity = new ApplicationImage();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region LookupItemsViewDto Mappers

      public static LookupItemsViewDto AsDto(this LookupItemsView entity)
      {
        var dto = new LookupItemsViewDto
        {
          Key = entity.Key,
          LookupType = entity.LookupType,
          Value = entity.Value,
          DisplayOrder = entity.DisplayOrder,
          ParentId = entity.ParentId,
          ParentKey = entity.ParentKey,
          Culture = entity.Culture,
          ExternalId = entity.ExternalId,
          CustomFilterId = entity.CustomFilterId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static LookupItemsView CopyTo(this LookupItemsViewDto dto, LookupItemsView entity)
      {
        entity.Key = dto.Key;
        entity.LookupType = dto.LookupType;
        entity.Value = dto.Value;
        entity.DisplayOrder = dto.DisplayOrder;
        entity.ParentId = dto.ParentId;
        entity.ParentKey = dto.ParentKey;
        entity.Culture = dto.Culture;
        entity.ExternalId = dto.ExternalId;
        entity.CustomFilterId = dto.CustomFilterId;
        return entity;
      }

      public static LookupItemsView CopyTo(this LookupItemsViewDto dto)
      {
        var entity = new LookupItemsView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region CensorshipRuleDto Mappers

      public static CensorshipRuleDto AsDto(this CensorshipRule entity)
      {
        var dto = new CensorshipRuleDto
        {
          Type = entity.Type,
          Level = entity.Level,
          Phrase = entity.Phrase,
          WhiteList = entity.WhiteList,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static CensorshipRule CopyTo(this CensorshipRuleDto dto, CensorshipRule entity)
      {
        entity.Type = dto.Type;
        entity.Level = dto.Level;
        entity.Phrase = dto.Phrase;
        entity.WhiteList = dto.WhiteList;
        return entity;
      }

      public static CensorshipRule CopyTo(this CensorshipRuleDto dto)
      {
        var entity = new CensorshipRule();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region ActivityCategoryDto Mappers

      public static ActivityCategoryDto AsDto(this ActivityCategory entity)
      {
        var dto = new ActivityCategoryDto
        {
          LocalisationKey = entity.LocalisationKey,
          Name = entity.Name,
          Visible = entity.Visible,
          Administrable = entity.Administrable,
          ActivityType = entity.ActivityType,
          Editable = entity.Editable,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static ActivityCategory CopyTo(this ActivityCategoryDto dto, ActivityCategory entity)
      {
        entity.LocalisationKey = dto.LocalisationKey;
        entity.Name = dto.Name;
        entity.Visible = dto.Visible;
        entity.Administrable = dto.Administrable;
        entity.ActivityType = dto.ActivityType;
        entity.Editable = dto.Editable;
        return entity;
      }

      public static ActivityCategory CopyTo(this ActivityCategoryDto dto)
      {
        var entity = new ActivityCategory();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region ActivityDto Mappers

      public static ActivityDto AsDto(this Activity entity)
      {
        var dto = new ActivityDto
        {
          LocalisationKey = entity.LocalisationKey,
          Name = entity.Name,
          EnableDisable = entity.EnableDisable,
          Administrable = entity.Administrable,
          SortOrder = entity.SortOrder,
          Used = entity.Used,
          ExternalId = entity.ExternalId,
          ActivityCategoryId = entity.ActivityCategoryId,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static Activity CopyTo(this ActivityDto dto, Activity entity)
      {
        entity.LocalisationKey = dto.LocalisationKey;
        entity.Name = dto.Name;
        entity.EnableDisable = dto.EnableDisable;
        entity.Administrable = dto.Administrable;
        entity.SortOrder = dto.SortOrder;
        entity.Used = dto.Used;
        entity.ExternalId = dto.ExternalId;
        entity.ActivityCategoryId = dto.ActivityCategoryId;
        return entity;
      }

      public static Activity CopyTo(this ActivityDto dto)
      {
        var entity = new Activity();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region ActivityViewDto Mappers

      public static ActivityViewDto AsDto(this ActivityView entity)
      {
        var dto = new ActivityViewDto
        {
          CategoryLocalisationKey = entity.CategoryLocalisationKey,
          CategoryName = entity.CategoryName,
          CategoryVisible = entity.CategoryVisible,
          CategoryAdministrable = entity.CategoryAdministrable,
          ActivityType = entity.ActivityType,
          ActivityLocalisationKey = entity.ActivityLocalisationKey,
          ActivityName = entity.ActivityName,
          ActivityVisible = entity.ActivityVisible,
          ActivityAdministrable = entity.ActivityAdministrable,
          SortOrder = entity.SortOrder,
          ActivityCategoryId = entity.ActivityCategoryId,
          ActivityExternalId = entity.ActivityExternalId,
          Editable = entity.Editable,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static ActivityView CopyTo(this ActivityViewDto dto, ActivityView entity)
      {
        entity.CategoryLocalisationKey = dto.CategoryLocalisationKey;
        entity.CategoryName = dto.CategoryName;
        entity.CategoryVisible = dto.CategoryVisible;
        entity.CategoryAdministrable = dto.CategoryAdministrable;
        entity.ActivityType = dto.ActivityType;
        entity.ActivityLocalisationKey = dto.ActivityLocalisationKey;
        entity.ActivityName = dto.ActivityName;
        entity.ActivityVisible = dto.ActivityVisible;
        entity.ActivityAdministrable = dto.ActivityAdministrable;
        entity.SortOrder = dto.SortOrder;
        entity.ActivityCategoryId = dto.ActivityCategoryId;
        entity.ActivityExternalId = dto.ActivityExternalId;
        entity.Editable = dto.Editable;
        return entity;
      }

      public static ActivityView CopyTo(this ActivityViewDto dto)
      {
        var entity = new ActivityView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region ExternalLookUpItemDto Mappers

      public static ExternalLookUpItemDto AsDto(this ExternalLookUpItem entity)
      {
        var dto = new ExternalLookUpItemDto
        {
          InternalId = entity.InternalId,
          ExternalId = entity.ExternalId,
          ExternalLookUpType = entity.ExternalLookUpType,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static ExternalLookUpItem CopyTo(this ExternalLookUpItemDto dto, ExternalLookUpItem entity)
      {
        entity.InternalId = dto.InternalId;
        entity.ExternalId = dto.ExternalId;
        entity.ExternalLookUpType = dto.ExternalLookUpType;
        return entity;
      }

      public static ExternalLookUpItem CopyTo(this ExternalLookUpItemDto dto)
      {
        var entity = new ExternalLookUpItem();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region DefaultLocalisationItemDto Mappers

      public static DefaultLocalisationItemDto AsDto(this DefaultLocalisationItem entity)
      {
        var dto = new DefaultLocalisationItemDto
        {
				  CreatedOn =  entity.CreatedOn,
          Key = entity.Key,
          DefaultValue = entity.DefaultValue,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static DefaultLocalisationItem CopyTo(this DefaultLocalisationItemDto dto, DefaultLocalisationItem entity)
      {
        entity.Key = dto.Key;
        entity.DefaultValue = dto.DefaultValue;
        return entity;
      }

      public static DefaultLocalisationItem CopyTo(this DefaultLocalisationItemDto dto)
      {
        var entity = new DefaultLocalisationItem();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region LaborInsightMappingDto Mappers

      public static LaborInsightMappingDto AsDto(this LaborInsightMapping entity)
      {
        var dto = new LaborInsightMappingDto
        {
          LaborInsightId = entity.LaborInsightId,
          LaborInsightValue = entity.LaborInsightValue,
          CodeGroupKey = entity.CodeGroupKey,
          CodeItemKey = entity.CodeItemKey,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static LaborInsightMapping CopyTo(this LaborInsightMappingDto dto, LaborInsightMapping entity)
      {
        entity.LaborInsightId = dto.LaborInsightId;
        entity.LaborInsightValue = dto.LaborInsightValue;
        entity.CodeGroupKey = dto.CodeGroupKey;
        entity.CodeItemKey = dto.CodeItemKey;
        return entity;
      }

      public static LaborInsightMapping CopyTo(this LaborInsightMappingDto dto)
      {
        var entity = new LaborInsightMapping();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region LaborInsightMappingViewDto Mappers

      public static LaborInsightMappingViewDto AsDto(this LaborInsightMappingView entity)
      {
        var dto = new LaborInsightMappingViewDto
        {
          CodeGroupItemId = entity.CodeGroupItemId,
          CodeGroupKey = entity.CodeGroupKey,
          LaborInsightId = entity.LaborInsightId,
          LaborInsightValue = entity.LaborInsightValue,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static LaborInsightMappingView CopyTo(this LaborInsightMappingViewDto dto, LaborInsightMappingView entity)
      {
        entity.CodeGroupItemId = dto.CodeGroupItemId;
        entity.CodeGroupKey = dto.CodeGroupKey;
        entity.LaborInsightId = dto.LaborInsightId;
        entity.LaborInsightValue = dto.LaborInsightValue;
        return entity;
      }

      public static LaborInsightMappingView CopyTo(this LaborInsightMappingViewDto dto)
      {
        var entity = new LaborInsightMappingView();
        return dto.CopyTo(entity);
      }	

      #endregion

      #region StarRatingMinimumScoreDto Mappers

      public static StarRatingMinimumScoreDto AsDto(this StarRatingMinimumScore entity)
      {
        var dto = new StarRatingMinimumScoreDto
        {
          MinimumScore = entity.MinimumScore,
          MaximumScore = entity.MaximumScore,
          StarRating = entity.StarRating,
				  Id = entity.Id
        };
        return dto;
      }
      
      public static StarRatingMinimumScore CopyTo(this StarRatingMinimumScoreDto dto, StarRatingMinimumScore entity)
      {
        entity.MinimumScore = dto.MinimumScore;
        entity.MaximumScore = dto.MaximumScore;
        entity.StarRating = dto.StarRating;
        return entity;
      }

      public static StarRatingMinimumScore CopyTo(this StarRatingMinimumScoreDto dto)
      {
        var entity = new StarRatingMinimumScore();
        return dto.CopyTo(entity);
      }	

      #endregion

    }
}
*/

#endregion
