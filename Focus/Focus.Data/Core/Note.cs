﻿namespace Focus.Data.Core.Entities
{
	partial class Note
	{ 
		public Note Clone()
		{
			return new Note
			       	{
			       		Body = Body,
			       		Title = Title
			       	};
		}
	}
}
