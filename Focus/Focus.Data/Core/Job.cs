﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core;
using Framework.Core;

#endregion


namespace Focus.Data.Core.Entities
{
	partial class Job
	{
		public Job Clone()
		{
			var clone = new Job
			            	{ 
											JobTitle = JobTitle,
											ClosingOn = ClosingOn,
											ApprovalStatus = ApprovalStatus,
											MinSalary = MinSalary,
											MaxSalary = MaxSalary,
											HoursPerWeek = HoursPerWeek,
											MinimumHoursPerWeek = MinimumHoursPerWeek,
											OverTimeRequired = OverTimeRequired,
											NumberOfOpenings = NumberOfOpenings,
                      HideOpeningsOnPosting = HideOpeningsOnPosting,
											//PostedOn = PostedOn,
											JobStatus = JobStatus,
											Description = Description, 
											BusinessUnitLogo = BusinessUnitLogo,
											Employer = Employer, 
											BusinessUnit = BusinessUnit,
											CreatedBy = CreatedBy, 
											UpdatedBy = CreatedBy, 
											BusinessUnitDescription = BusinessUnitDescription,
											EmployerDescriptionPostingPosition = EmployerDescriptionPostingPosition,
                      IsConfidential = IsConfidential,
											//PostedBy = PostedBy,
											//Posting = Posting, 
											WizardPath = WizardPath,
											WizardStep = WizardStep,
											HeldBy = HeldBy, 
											HeldOn = HeldOn,
											ClosedBy = ClosedBy,
											ClosedOn = ClosedOn, 
											JobLocationType = JobLocationType,
											OnetId = OnetId, 
											SalaryFrequencyId = SalaryFrequencyId,
											FederalContractor = FederalContractor,
											CourtOrderedAffirmativeAction = CourtOrderedAffirmativeAction,
											ForeignLabourCertification = ForeignLabourCertification,
                      ForeignLabourCertificationH2A = ForeignLabourCertificationH2A,
                      ForeignLabourCertificationH2B = ForeignLabourCertificationH2B,
                      ForeignLabourCertificationOther = ForeignLabourCertificationOther,
											FederalContractorExpiresOn = FederalContractorExpiresOn, 
											WorkOpportunitiesTaxCreditHires = WorkOpportunitiesTaxCreditHires,
											PostingFlags = PostingFlags, 
											IsCommissionBased = IsCommissionBased,
                      IsSalaryAndCommissionBased = IsSalaryAndCommissionBased,
											NormalWorkDays = NormalWorkDays,
											WorkDaysVary = WorkDaysVary, 
											NormalWorkShiftsId = NormalWorkShiftsId, 
											LeaveBenefits = LeaveBenefits,
											RetirementBenefits = RetirementBenefits, 
											InsuranceBenefits = InsuranceBenefits, 
											MiscellaneousBenefits = MiscellaneousBenefits, 
											OtherBenefitsDetails = OtherBenefitsDetails, 
											InterviewContactPreferences = InterviewContactPreferences,
											InterviewEmailAddress = InterviewEmailAddress, 
											InterviewApplicationUrl = InterviewApplicationUrl, 
											InterviewMailAddress = InterviewMailAddress, 
											InterviewFaxNumber = InterviewFaxNumber, 
											InterviewPhoneNumber = InterviewPhoneNumber, 
											InterviewDirectApplicationDetails = InterviewDirectApplicationDetails, 
											InterviewOtherInstructions = InterviewOtherInstructions, 
											ScreeningPreferences = ScreeningPreferences,
											MinimumEducationLevel = MinimumEducationLevel, 
											MinimumEducationLevelRequired = MinimumEducationLevelRequired, 
											MinimumAge = MinimumAge, 
											MinimumAgeReason = MinimumAgeReason, 
											MinimumAgeRequired = MinimumAgeRequired,
                      MinimumAgeReasonValue = MinimumAgeReasonValue,
                      MinimumCollegeYears = MinimumCollegeYears,
                      StudentEnrolled = StudentEnrolled,
											LicencesRequired = LicencesRequired, 
											CertificationRequired = CertificationRequired,
											LanguagesRequired = LanguagesRequired, 
											Tasks = Tasks, 
											YellowProfanityWords = YellowProfanityWords,
											RedProfanityWords = RedProfanityWords,
											EmploymentStatusId = EmploymentStatusId,
											JobTypeId = JobTypeId,
											JobStatusId = JobStatusId,
											EmployeeId = EmployeeId,
											ApprovedOn = ApprovedOn,
											ApprovedBy = ApprovedBy,
											MinimumExperience = MinimumExperience,
                      MinimumExperienceMonths = MinimumExperienceMonths,
											MinimumExperienceRequired = MinimumExperienceRequired,
											DrivingLicenceClassId = DrivingLicenceClassId,
											DrivingLicenceRequired = DrivingLicenceRequired,
											HideSalaryOnPosting = HideSalaryOnPosting,
											HideEducationOnPosting = HideEducationOnPosting,
											HideExperienceOnPosting = HideExperienceOnPosting,
											HideMinimumAgeOnPosting = HideMinimumAgeOnPosting,
											HideDriversLicenceOnPosting = HideDriversLicenceOnPosting,
											HideLicencesOnPosting = HideLicencesOnPosting,
											HideCertificationsOnPosting = HideCertificationsOnPosting,
											HideLanguagesOnPosting = HideLanguagesOnPosting,
											HideSpecialRequirementsOnPosting = HideSpecialRequirementsOnPosting,
											HideCareerReadinessOnPosting = HideCareerReadinessOnPosting,
                      JobType = JobType,
                      AssignedToId = AssignedToId,
											Location = Location,
											CareerReadinessLevel = CareerReadinessLevel,
											CareerReadinessLevelRequired = CareerReadinessLevelRequired,
											SuitableForHomeWorker = SuitableForHomeWorker,
											PreScreeningServiceRequest = PreScreeningServiceRequest,
											DisplayBenefits = DisplayBenefits,
											CriminalBackgroundExclusionRequired = CriminalBackgroundExclusionRequired,
											HasCheckedCriminalRecordExclusion = HasCheckedCriminalRecordExclusion,
											CreditCheckRequired = CreditCheckRequired,
											ExtendVeteranPriority = ExtendVeteranPriority,
											VeteranPriorityEndDate = ExtendVeteranPriority.GetValueOrDefault(ExtendVeteranPriorityOfServiceTypes.None) == ExtendVeteranPriorityOfServiceTypes.None
																								 ? null
																								 : VeteranPriorityEndDate
                     };

			if(JobAddresses.IsNotNullOrEmpty())
				foreach (var jobAddress in JobAddresses) clone.JobAddresses.Add(jobAddress.Clone());
			
			if (PostingSurveys.IsNotNullOrEmpty())
			    foreach (var postingSurvey in PostingSurveys) clone.PostingSurveys.Add(postingSurvey.Clone());

			if (Notes.IsNotNullOrEmpty())
				foreach (var note in Notes) clone.Notes.Add(note.Clone());

			if (JobSkills.IsNotNullOrEmpty())
				foreach (var jobSkill in JobSkills) clone.JobSkills.Add(jobSkill.Clone());
			
			if (JobSpecialRequirements.IsNotNullOrEmpty())
				foreach (var jobSpecialRequirements in JobSpecialRequirements) clone.JobSpecialRequirements.Add(jobSpecialRequirements.Clone());

			if(JobLocations.IsNotNullOrEmpty())
				foreach (var jobLocation in JobLocations) clone.JobLocations.Add(jobLocation.Clone());

      if (JobLanguages.IsNotNullOrEmpty())
        foreach (var jobLanguage in JobLanguages) clone.JobLanguages.Add(jobLanguage.Clone());

      if (JobCertificates.IsNotNullOrEmpty())
        foreach (var jobCertificate in JobCertificates) clone.JobCertificates.Add(jobCertificate.Clone());

      if (JobLicences.IsNotNullOrEmpty())
        foreach (var jobLicence in JobLicences) clone.JobLicences.Add(jobLicence.Clone());

      if (JobProgramOfStudies.IsNotNullOrEmpty())
        foreach (var jobProgramOfStudy in JobProgramOfStudies) clone.JobProgramOfStudies.Add(jobProgramOfStudy.Clone());

			if(JobDrivingLicenceEndorsements.IsNotNullOrEmpty())
				foreach(var jobDrivingLicenceEndorsement in JobDrivingLicenceEndorsements) clone.JobDrivingLicenceEndorsements.Add((jobDrivingLicenceEndorsement.Clone()));

			return clone;
		}
	}
}
