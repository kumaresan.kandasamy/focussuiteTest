using System;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Validation;
using Mindscape.LightSpeed.Linq;

#region Entities

namespace Focus.Data.Core
{
  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class ActionEvent : Entity<long>
  {
    #region Fields
  
    private System.Guid _sessionId;
    private System.Guid _requestId;
    private long _userId;
    private System.DateTime _actionedOn;
    private System.Nullable<long> _entityId;
    private System.Nullable<long> _entityIdAdditional01;
    private System.Nullable<long> _entityIdAdditional02;
    private string _additionalDetails;
    private System.Nullable<long> _entityTypeId;
    private long _actionTypeId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SessionId entity attribute.</summary>
    public const string SessionIdField = "SessionId";
    /// <summary>Identifies the RequestId entity attribute.</summary>
    public const string RequestIdField = "RequestId";
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the ActionedOn entity attribute.</summary>
    public const string ActionedOnField = "ActionedOn";
    /// <summary>Identifies the EntityId entity attribute.</summary>
    public const string EntityIdField = "EntityId";
    /// <summary>Identifies the EntityIdAdditional01 entity attribute.</summary>
    public const string EntityIdAdditional01Field = "EntityIdAdditional01";
    /// <summary>Identifies the EntityIdAdditional02 entity attribute.</summary>
    public const string EntityIdAdditional02Field = "EntityIdAdditional02";
    /// <summary>Identifies the AdditionalDetails entity attribute.</summary>
    public const string AdditionalDetailsField = "AdditionalDetails";
    /// <summary>Identifies the EntityTypeId entity attribute.</summary>
    public const string EntityTypeIdField = "EntityTypeId";
    /// <summary>Identifies the ActionTypeId entity attribute.</summary>
    public const string ActionTypeIdField = "ActionTypeId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("ActionEvents")]
    private readonly EntityHolder<EntityType> _entityType = new EntityHolder<EntityType>();
    [ReverseAssociation("ActionEvents")]
    private readonly EntityHolder<ActionType> _actionType = new EntityHolder<ActionType>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityType EntityType
    {
      get { return Get(_entityType); }
      set { Set(_entityType, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ActionType ActionType
    {
      get { return Get(_actionType); }
      set { Set(_actionType, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public System.Guid SessionId
    {
      get { return Get(ref _sessionId, "SessionId"); }
      set { Set(ref _sessionId, value, "SessionId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Guid RequestId
    {
      get { return Get(ref _requestId, "RequestId"); }
      set { Set(ref _requestId, value, "RequestId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ActionedOn
    {
      get { return Get(ref _actionedOn, "ActionedOn"); }
      set { Set(ref _actionedOn, value, "ActionedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EntityId
    {
      get { return Get(ref _entityId, "EntityId"); }
      set { Set(ref _entityId, value, "EntityId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EntityIdAdditional01
    {
      get { return Get(ref _entityIdAdditional01, "EntityIdAdditional01"); }
      set { Set(ref _entityIdAdditional01, value, "EntityIdAdditional01"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EntityIdAdditional02
    {
      get { return Get(ref _entityIdAdditional02, "EntityIdAdditional02"); }
      set { Set(ref _entityIdAdditional02, value, "EntityIdAdditional02"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AdditionalDetails
    {
      get { return Get(ref _additionalDetails, "AdditionalDetails"); }
      set { Set(ref _additionalDetails, value, "AdditionalDetails"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="EntityType" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EntityTypeId
    {
      get { return Get(ref _entityTypeId, "EntityTypeId"); }
      set { Set(ref _entityTypeId, value, "EntityTypeId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="ActionType" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long ActionTypeId
    {
      get { return Get(ref _actionTypeId, "ActionTypeId"); }
      set { Set(ref _actionTypeId, value, "ActionTypeId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class User : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateUnique]
    [ValidateLength(0, 100)]
    private string _userName;
    [ValidatePresence]
    [ValidateLength(0, 86)]
    private string _passwordHash;
    [ValidateLength(0, 8)]
    private string _passwordSalt;
    private bool _enabled;
    [ValidateLength(0, 86)]
    private string _validationKey;
    private System.Nullable<Focus.Core.UserTypes> _userType;
    private System.Nullable<System.DateTime> _loggedInOn;
    private System.Nullable<System.DateTime> _lastLoggedInOn;
    [ValidateLength(0, 255)]
    private string _externalId;
    private bool _isClientAuthenticated;
    [ValidateLength(0, 50)]
    private string _screenName;
    [ValidateLength(0, 100)]
    private string _securityQuestion;
    [ValidateLength(0, 86)]
    private string _securityAnswerHash;
    private long _personId;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    private readonly System.DateTime _updatedOn;
    private readonly System.Nullable<System.DateTime> _deletedOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the UserName entity attribute.</summary>
    public const string UserNameField = "UserName";
    /// <summary>Identifies the PasswordHash entity attribute.</summary>
    public const string PasswordHashField = "PasswordHash";
    /// <summary>Identifies the PasswordSalt entity attribute.</summary>
    public const string PasswordSaltField = "PasswordSalt";
    /// <summary>Identifies the Enabled entity attribute.</summary>
    public const string EnabledField = "Enabled";
    /// <summary>Identifies the ValidationKey entity attribute.</summary>
    public const string ValidationKeyField = "ValidationKey";
    /// <summary>Identifies the UserType entity attribute.</summary>
    public const string UserTypeField = "UserType";
    /// <summary>Identifies the LoggedInOn entity attribute.</summary>
    public const string LoggedInOnField = "LoggedInOn";
    /// <summary>Identifies the LastLoggedInOn entity attribute.</summary>
    public const string LastLoggedInOnField = "LastLoggedInOn";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";
    /// <summary>Identifies the IsClientAuthenticated entity attribute.</summary>
    public const string IsClientAuthenticatedField = "IsClientAuthenticated";
    /// <summary>Identifies the ScreenName entity attribute.</summary>
    public const string ScreenNameField = "ScreenName";
    /// <summary>Identifies the SecurityQuestion entity attribute.</summary>
    public const string SecurityQuestionField = "SecurityQuestion";
    /// <summary>Identifies the SecurityAnswerHash entity attribute.</summary>
    public const string SecurityAnswerHashField = "SecurityAnswerHash";
    /// <summary>Identifies the PersonId entity attribute.</summary>
    public const string PersonIdField = "PersonId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("User")]
    private readonly EntityCollection<DismissedMessage> _dismissedMessages = new EntityCollection<DismissedMessage>();
    [ReverseAssociation("User")]
    private readonly EntityHolder<Person> _person = new EntityHolder<Person>();
    [ReverseAssociation("User")]
    private readonly EntityCollection<UserRole> _userRoles = new EntityCollection<UserRole>();

    private ThroughAssociation<UserRole, Role> _roles;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<DismissedMessage> DismissedMessages
    {
      get { return Get(_dismissedMessages); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Person Person
    {
      get { return Get(_person); }
      set { Set(_person, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<UserRole> UserRoles
    {
      get { return Get(_userRoles); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<UserRole, Role> Roles
    {
      get
      {
        if (_roles == null)
        {
          _roles = new ThroughAssociation<UserRole, Role>(_userRoles);
        }
        return Get(_roles);
      }
    }
    

    [System.Diagnostics.DebuggerNonUserCode]
    public string UserName
    {
      get { return Get(ref _userName, "UserName"); }
      set { Set(ref _userName, value, "UserName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PasswordHash
    {
      get { return Get(ref _passwordHash, "PasswordHash"); }
      set { Set(ref _passwordHash, value, "PasswordHash"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PasswordSalt
    {
      get { return Get(ref _passwordSalt, "PasswordSalt"); }
      set { Set(ref _passwordSalt, value, "PasswordSalt"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool Enabled
    {
      get { return Get(ref _enabled, "Enabled"); }
      set { Set(ref _enabled, value, "Enabled"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ValidationKey
    {
      get { return Get(ref _validationKey, "ValidationKey"); }
      set { Set(ref _validationKey, value, "ValidationKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.UserTypes> UserType
    {
      get { return Get(ref _userType, "UserType"); }
      set { Set(ref _userType, value, "UserType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> LoggedInOn
    {
      get { return Get(ref _loggedInOn, "LoggedInOn"); }
      set { Set(ref _loggedInOn, value, "LoggedInOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> LastLoggedInOn
    {
      get { return Get(ref _lastLoggedInOn, "LastLoggedInOn"); }
      set { Set(ref _lastLoggedInOn, value, "LastLoggedInOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsClientAuthenticated
    {
      get { return Get(ref _isClientAuthenticated, "IsClientAuthenticated"); }
      set { Set(ref _isClientAuthenticated, value, "IsClientAuthenticated"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ScreenName
    {
      get { return Get(ref _screenName, "ScreenName"); }
      set { Set(ref _screenName, value, "ScreenName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string SecurityQuestion
    {
      get { return Get(ref _securityQuestion, "SecurityQuestion"); }
      set { Set(ref _securityQuestion, value, "SecurityQuestion"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string SecurityAnswerHash
    {
      get { return Get(ref _securityAnswerHash, "SecurityAnswerHash"); }
      set { Set(ref _securityAnswerHash, value, "SecurityAnswerHash"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Person" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long PersonId
    {
      get { return Get(ref _personId, "PersonId"); }
      set { Set(ref _personId, value, "PersonId"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }
    }

    /// <summary>Gets the time the entity was last updated</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime UpdatedOn
    {
      get { return _updatedOn; }
    }

    /// <summary>Gets the time the entity was soft-deleted</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> DeletedOn
    {
      get { return _deletedOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class BusinessUnit : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _name;
    private bool _isPrimary;
    private long _employerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the IsPrimary entity attribute.</summary>
    public const string IsPrimaryField = "IsPrimary";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("BusinessUnit")]
    private readonly EntityCollection<Job> _jobs = new EntityCollection<Job>();
    [ReverseAssociation("BusinessUnits")]
    private readonly EntityHolder<Employer> _employer = new EntityHolder<Employer>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Job> Jobs
    {
      get { return Get(_jobs); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Employer Employer
    {
      get { return Get(_employer); }
      set { Set(_employer, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsPrimary
    {
      get { return Get(ref _isPrimary, "IsPrimary"); }
      set { Set(ref _isPrimary, value, "IsPrimary"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Employer" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class CodeGroup : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    [ValidateUnique]
    private string _key;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";


    #endregion
    
    #region Relationships

    [ReverseAssociation("CodeGroup")]
    private readonly EntityCollection<CodeGroupItem> _codeGroupItems = new EntityCollection<CodeGroupItem>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<CodeGroupItem> CodeGroupItems
    {
      get { return Get(_codeGroupItems); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class CodeGroupItem : Entity<long>
  {
    #region Fields
  
    private int _displayOrder;
    private long _codeGroupId;
    private long _codeItemId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the DisplayOrder entity attribute.</summary>
    public const string DisplayOrderField = "DisplayOrder";
    /// <summary>Identifies the CodeGroupId entity attribute.</summary>
    public const string CodeGroupIdField = "CodeGroupId";
    /// <summary>Identifies the CodeItemId entity attribute.</summary>
    public const string CodeItemIdField = "CodeItemId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("CodeGroupItems")]
    private readonly EntityHolder<CodeGroup> _codeGroup = new EntityHolder<CodeGroup>();
    [ReverseAssociation("CodeGroupItems")]
    private readonly EntityHolder<CodeItem> _codeItem = new EntityHolder<CodeItem>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public CodeGroup CodeGroup
    {
      get { return Get(_codeGroup); }
      set { Set(_codeGroup, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public CodeItem CodeItem
    {
      get { return Get(_codeItem); }
      set { Set(_codeItem, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public int DisplayOrder
    {
      get { return Get(ref _displayOrder, "DisplayOrder"); }
      set { Set(ref _displayOrder, value, "DisplayOrder"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="CodeGroup" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long CodeGroupId
    {
      get { return Get(ref _codeGroupId, "CodeGroupId"); }
      set { Set(ref _codeGroupId, value, "CodeGroupId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="CodeItem" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long CodeItemId
    {
      get { return Get(ref _codeItemId, "CodeItemId"); }
      set { Set(ref _codeItemId, value, "CodeItemId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class CodeItem : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateUnique]
    [ValidateLength(0, 200)]
    private string _key;
    private bool _isSystem;
    [ValidateLength(0, 200)]
    private string _parentKey;
    [ValidateLength(0, 36)]
    private string _externalId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the IsSystem entity attribute.</summary>
    public const string IsSystemField = "IsSystem";
    /// <summary>Identifies the ParentKey entity attribute.</summary>
    public const string ParentKeyField = "ParentKey";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("CodeItem")]
    private readonly EntityCollection<CodeGroupItem> _codeGroupItems = new EntityCollection<CodeGroupItem>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<CodeGroupItem> CodeGroupItems
    {
      get { return Get(_codeGroupItems); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsSystem
    {
      get { return Get(ref _isSystem, "IsSystem"); }
      set { Set(ref _isSystem, value, "IsSystem"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ParentKey
    {
      get { return Get(ref _parentKey, "ParentKey"); }
      set { Set(ref _parentKey, value, "ParentKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class ConfigurationItem : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _key;
    [ValidatePresence]
    [ValidateLength(0, 500)]
    private string _value;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the Value entity attribute.</summary>
    public const string ValueField = "Value";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Value
    {
      get { return Get(ref _value, "Value"); }
      set { Set(ref _value, value, "Value"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class Employee : Entity<long>
  {
    #region Fields
  
    private Focus.Core.ApprovalStatuses _approvalStatus;
    private long _employerId;
    private long _personId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ApprovalStatus entity attribute.</summary>
    public const string ApprovalStatusField = "ApprovalStatus";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the PersonId entity attribute.</summary>
    public const string PersonIdField = "PersonId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Employee")]
    private readonly EntityCollection<Job> _jobs = new EntityCollection<Job>();
    [ReverseAssociation("Employees")]
    private readonly EntityHolder<Employer> _employer = new EntityHolder<Employer>();
    [ReverseAssociation("Employee")]
    private readonly EntityHolder<Person> _person = new EntityHolder<Person>();
    [ReverseAssociation("Employee")]
    private readonly EntityCollection<EmployeeCandidate> _employeeCandidates = new EntityCollection<EmployeeCandidate>();
    [ReverseAssociation("Employee")]
    private readonly EntityCollection<SavedSearchEmployee> _savedSearchEmployees = new EntityCollection<SavedSearchEmployee>();

    private ThroughAssociation<EmployeeCandidate, Candidate> _candidates;
    private ThroughAssociation<SavedSearchEmployee, SavedSearch> _savedSearches;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Job> Jobs
    {
      get { return Get(_jobs); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Employer Employer
    {
      get { return Get(_employer); }
      set { Set(_employer, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Person Person
    {
      get { return Get(_person); }
      set { Set(_person, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<EmployeeCandidate> EmployeeCandidates
    {
      get { return Get(_employeeCandidates); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<SavedSearchEmployee> SavedSearchEmployees
    {
      get { return Get(_savedSearchEmployees); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<EmployeeCandidate, Candidate> Candidates
    {
      get
      {
        if (_candidates == null)
        {
          _candidates = new ThroughAssociation<EmployeeCandidate, Candidate>(_employeeCandidates);
        }
        return Get(_candidates);
      }
    }
    
    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<SavedSearchEmployee, SavedSearch> SavedSearches
    {
      get
      {
        if (_savedSearches == null)
        {
          _savedSearches = new ThroughAssociation<SavedSearchEmployee, SavedSearch>(_savedSearchEmployees);
        }
        return Get(_savedSearches);
      }
    }
    

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ApprovalStatuses ApprovalStatus
    {
      get { return Get(ref _approvalStatus, "ApprovalStatus"); }
      set { Set(ref _approvalStatus, value, "ApprovalStatus"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Employer" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Person" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long PersonId
    {
      get { return Get(ref _personId, "PersonId"); }
      set { Set(ref _personId, value, "PersonId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class Employer : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _name;
    private System.Nullable<System.DateTime> _commencedOn;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _federalEmployerIdentificationNumber;
    private bool _isValidFederalEmployerIdentificationNumber;
    [ValidateLength(0, 11)]
    private string _stateEmployerIdentificationNumber;
    private Focus.Core.ApprovalStatuses _approvalStatus;
    [ValidateUri]
    [ValidateLength(0, 1000)]
    private string _url;
    private System.Nullable<System.DateTime> _expiredOn;
    private long _ownershipTypeId;
    [ValidateLength(0, 200)]
    private string _industrialClassification;
    private bool _termsAccepted;
    [ValidateLength(0, 20)]
    private string _primaryPhone;
    [ValidateLength(0, 20)]
    private string _primaryPhoneExtension;
    [ValidateLength(0, 20)]
    private string _primaryPhoneType;
    [ValidateLength(0, 20)]
    private string _alternatePhone1;
    [ValidateLength(0, 20)]
    private string _alternatePhone1Type;
    [ValidateLength(0, 20)]
    private string _alternatePhone2;
    [ValidateLength(0, 20)]
    private string _alternatePhone2Type;
    private bool _isRegistrationComplete;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    private readonly System.DateTime _updatedOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the CommencedOn entity attribute.</summary>
    public const string CommencedOnField = "CommencedOn";
    /// <summary>Identifies the FederalEmployerIdentificationNumber entity attribute.</summary>
    public const string FederalEmployerIdentificationNumberField = "FederalEmployerIdentificationNumber";
    /// <summary>Identifies the IsValidFederalEmployerIdentificationNumber entity attribute.</summary>
    public const string IsValidFederalEmployerIdentificationNumberField = "IsValidFederalEmployerIdentificationNumber";
    /// <summary>Identifies the StateEmployerIdentificationNumber entity attribute.</summary>
    public const string StateEmployerIdentificationNumberField = "StateEmployerIdentificationNumber";
    /// <summary>Identifies the ApprovalStatus entity attribute.</summary>
    public const string ApprovalStatusField = "ApprovalStatus";
    /// <summary>Identifies the Url entity attribute.</summary>
    public const string UrlField = "Url";
    /// <summary>Identifies the ExpiredOn entity attribute.</summary>
    public const string ExpiredOnField = "ExpiredOn";
    /// <summary>Identifies the OwnershipTypeId entity attribute.</summary>
    public const string OwnershipTypeIdField = "OwnershipTypeId";
    /// <summary>Identifies the IndustrialClassification entity attribute.</summary>
    public const string IndustrialClassificationField = "IndustrialClassification";
    /// <summary>Identifies the TermsAccepted entity attribute.</summary>
    public const string TermsAcceptedField = "TermsAccepted";
    /// <summary>Identifies the PrimaryPhone entity attribute.</summary>
    public const string PrimaryPhoneField = "PrimaryPhone";
    /// <summary>Identifies the PrimaryPhoneExtension entity attribute.</summary>
    public const string PrimaryPhoneExtensionField = "PrimaryPhoneExtension";
    /// <summary>Identifies the PrimaryPhoneType entity attribute.</summary>
    public const string PrimaryPhoneTypeField = "PrimaryPhoneType";
    /// <summary>Identifies the AlternatePhone1 entity attribute.</summary>
    public const string AlternatePhone1Field = "AlternatePhone1";
    /// <summary>Identifies the AlternatePhone1Type entity attribute.</summary>
    public const string AlternatePhone1TypeField = "AlternatePhone1Type";
    /// <summary>Identifies the AlternatePhone2 entity attribute.</summary>
    public const string AlternatePhone2Field = "AlternatePhone2";
    /// <summary>Identifies the AlternatePhone2Type entity attribute.</summary>
    public const string AlternatePhone2TypeField = "AlternatePhone2Type";
    /// <summary>Identifies the IsRegistrationComplete entity attribute.</summary>
    public const string IsRegistrationCompleteField = "IsRegistrationComplete";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Employer")]
    private readonly EntityCollection<EmployerDescription> _employerDescriptions = new EntityCollection<EmployerDescription>();
    [ReverseAssociation("Employer")]
    private readonly EntityCollection<EmployerLogo> _employerLogos = new EntityCollection<EmployerLogo>();
    [ReverseAssociation("Employer")]
    private readonly EntityCollection<Job> _jobs = new EntityCollection<Job>();
    [ReverseAssociation("Employer")]
    private readonly EntityCollection<EmployerAddress> _employerAddresses = new EntityCollection<EmployerAddress>();
    [ReverseAssociation("Employer")]
    private readonly EntityCollection<Employee> _employees = new EntityCollection<Employee>();
    [ReverseAssociation("Employer")]
    private readonly EntityCollection<BusinessUnit> _businessUnits = new EntityCollection<BusinessUnit>();
    [ReverseAssociation("Employer")]
    private readonly EntityCollection<CandidateNote> _candidateNotes = new EntityCollection<CandidateNote>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<EmployerDescription> EmployerDescriptions
    {
      get { return Get(_employerDescriptions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<EmployerLogo> EmployerLogos
    {
      get { return Get(_employerLogos); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Job> Jobs
    {
      get { return Get(_jobs); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<EmployerAddress> EmployerAddresses
    {
      get { return Get(_employerAddresses); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Employee> Employees
    {
      get { return Get(_employees); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<BusinessUnit> BusinessUnits
    {
      get { return Get(_businessUnits); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<CandidateNote> CandidateNotes
    {
      get { return Get(_candidateNotes); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> CommencedOn
    {
      get { return Get(ref _commencedOn, "CommencedOn"); }
      set { Set(ref _commencedOn, value, "CommencedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string FederalEmployerIdentificationNumber
    {
      get { return Get(ref _federalEmployerIdentificationNumber, "FederalEmployerIdentificationNumber"); }
      set { Set(ref _federalEmployerIdentificationNumber, value, "FederalEmployerIdentificationNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsValidFederalEmployerIdentificationNumber
    {
      get { return Get(ref _isValidFederalEmployerIdentificationNumber, "IsValidFederalEmployerIdentificationNumber"); }
      set { Set(ref _isValidFederalEmployerIdentificationNumber, value, "IsValidFederalEmployerIdentificationNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string StateEmployerIdentificationNumber
    {
      get { return Get(ref _stateEmployerIdentificationNumber, "StateEmployerIdentificationNumber"); }
      set { Set(ref _stateEmployerIdentificationNumber, value, "StateEmployerIdentificationNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ApprovalStatuses ApprovalStatus
    {
      get { return Get(ref _approvalStatus, "ApprovalStatus"); }
      set { Set(ref _approvalStatus, value, "ApprovalStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Url
    {
      get { return Get(ref _url, "Url"); }
      set { Set(ref _url, value, "Url"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ExpiredOn
    {
      get { return Get(ref _expiredOn, "ExpiredOn"); }
      set { Set(ref _expiredOn, value, "ExpiredOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long OwnershipTypeId
    {
      get { return Get(ref _ownershipTypeId, "OwnershipTypeId"); }
      set { Set(ref _ownershipTypeId, value, "OwnershipTypeId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string IndustrialClassification
    {
      get { return Get(ref _industrialClassification, "IndustrialClassification"); }
      set { Set(ref _industrialClassification, value, "IndustrialClassification"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool TermsAccepted
    {
      get { return Get(ref _termsAccepted, "TermsAccepted"); }
      set { Set(ref _termsAccepted, value, "TermsAccepted"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PrimaryPhone
    {
      get { return Get(ref _primaryPhone, "PrimaryPhone"); }
      set { Set(ref _primaryPhone, value, "PrimaryPhone"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PrimaryPhoneExtension
    {
      get { return Get(ref _primaryPhoneExtension, "PrimaryPhoneExtension"); }
      set { Set(ref _primaryPhoneExtension, value, "PrimaryPhoneExtension"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PrimaryPhoneType
    {
      get { return Get(ref _primaryPhoneType, "PrimaryPhoneType"); }
      set { Set(ref _primaryPhoneType, value, "PrimaryPhoneType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AlternatePhone1
    {
      get { return Get(ref _alternatePhone1, "AlternatePhone1"); }
      set { Set(ref _alternatePhone1, value, "AlternatePhone1"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AlternatePhone1Type
    {
      get { return Get(ref _alternatePhone1Type, "AlternatePhone1Type"); }
      set { Set(ref _alternatePhone1Type, value, "AlternatePhone1Type"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AlternatePhone2
    {
      get { return Get(ref _alternatePhone2, "AlternatePhone2"); }
      set { Set(ref _alternatePhone2, value, "AlternatePhone2"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AlternatePhone2Type
    {
      get { return Get(ref _alternatePhone2Type, "AlternatePhone2Type"); }
      set { Set(ref _alternatePhone2Type, value, "AlternatePhone2Type"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsRegistrationComplete
    {
      get { return Get(ref _isRegistrationComplete, "IsRegistrationComplete"); }
      set { Set(ref _isRegistrationComplete, value, "IsRegistrationComplete"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }
    }

    /// <summary>Gets the time the entity was last updated</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime UpdatedOn
    {
      get { return _updatedOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class EmployerDescription : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _title;
    [ValidatePresence]
    [ValidateLength(0, 2000)]
    private string _description;
    private bool _isPrimary;
    private long _employerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Title entity attribute.</summary>
    public const string TitleField = "Title";
    /// <summary>Identifies the Description entity attribute.</summary>
    public const string DescriptionField = "Description";
    /// <summary>Identifies the IsPrimary entity attribute.</summary>
    public const string IsPrimaryField = "IsPrimary";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("EmployerDescription")]
    private readonly EntityCollection<Job> _jobs = new EntityCollection<Job>();
    [ReverseAssociation("EmployerDescriptions")]
    private readonly EntityHolder<Employer> _employer = new EntityHolder<Employer>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Job> Jobs
    {
      get { return Get(_jobs); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Employer Employer
    {
      get { return Get(_employer); }
      set { Set(_employer, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Title
    {
      get { return Get(ref _title, "Title"); }
      set { Set(ref _title, value, "Title"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Description
    {
      get { return Get(ref _description, "Description"); }
      set { Set(ref _description, value, "Description"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsPrimary
    {
      get { return Get(ref _isPrimary, "IsPrimary"); }
      set { Set(ref _isPrimary, value, "IsPrimary"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Employer" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class EmployerLogo : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _name;
    [ValidatePresence]
    private byte[] _logo;
    private long _employerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the Logo entity attribute.</summary>
    public const string LogoField = "Logo";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("EmployerLogo")]
    private readonly EntityCollection<Job> _jobs = new EntityCollection<Job>();
    [ReverseAssociation("EmployerLogos")]
    private readonly EntityHolder<Employer> _employer = new EntityHolder<Employer>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Job> Jobs
    {
      get { return Get(_jobs); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Employer Employer
    {
      get { return Get(_employer); }
      set { Set(_employer, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public byte[] Logo
    {
      get { return Get(ref _logo, "Logo"); }
      set { Set(ref _logo, value, "Logo"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Employer" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class EntityType : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _name;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";


    #endregion
    
    #region Relationships

    [ReverseAssociation("EntityType")]
    private readonly EntityCollection<ActionEvent> _actionEvents = new EntityCollection<ActionEvent>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<ActionEvent> ActionEvents
    {
      get { return Get(_actionEvents); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class Job : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _jobTitle;
    private long _createdBy;
    private long _updatedBy;
    private Focus.Core.ApprovalStatuses _approvalStatus;
    private Focus.Core.JobStatuses _jobStatus;
    private System.Nullable<decimal> _minSalary;
    private System.Nullable<decimal> _maxSalary;
    private System.Nullable<long> _salaryFrequencyId;
    private System.Nullable<decimal> _hoursPerWeek;
    private System.Nullable<bool> _overTimeRequired;
    private System.Nullable<System.DateTime> _closingOn;
    private System.Nullable<int> _numberOfOpenings;
    private System.Nullable<System.DateTime> _postedOn;
    private System.Nullable<long> _postedBy;
    private string _description;
    private string _posting;
    private Focus.Core.EmployerDescriptionPostingPositions _employerDescriptionPostingPosition;
    private int _wizardStep;
    private int _wizardPath;
    private System.Nullable<System.DateTime> _heldOn;
    private System.Nullable<long> _heldBy;
    private System.Nullable<System.DateTime> _closedOn;
    private System.Nullable<long> _closedBy;
    private System.Nullable<Focus.Core.JobLocationTypes> _jobLocationType;
    private System.Nullable<bool> _federalContractor;
    private System.Nullable<bool> _foreignLabourCertification;
    private System.Nullable<bool> _courtOrderedAffirmativeAction;
    private System.Nullable<System.DateTime> _federalContractorExpiresOn;
    private System.Nullable<Focus.Core.WorkOpportunitiesTaxCreditCategories> _workOpportunitiesTaxCreditHires;
    private System.Nullable<Focus.Core.JobPostingFlags> _postingFlags;
    private System.Nullable<bool> _isCommissionBased;
    private System.Nullable<Focus.Core.Weekdays> _normalWorkDays;
    private System.Nullable<bool> _workDaysVary;
    private System.Nullable<long> _normalWorkShiftsId;
    private System.Nullable<Focus.Core.LeaveBenefits> _leaveBenefits;
    private System.Nullable<Focus.Core.RetirementBenefits> _retirementBenefits;
    private System.Nullable<Focus.Core.InsuranceBenefits> _insuranceBenefits;
    private System.Nullable<Focus.Core.MiscellaneousBenefits> _miscellaneousBenefits;
    private string _otherBenefitsDetails;
    private System.Nullable<Focus.Core.ContactMethods> _interviewContactPreferences;
    private string _interviewEmailAddress;
    private string _interviewApplicationUrl;
    private string _interviewMailAddress;
    private string _interviewFaxNumber;
    private string _interviewPhoneNumber;
    private string _interviewDirectApplicationDetails;
    private string _interviewOtherInstructions;
    private System.Nullable<Focus.Core.ScreeningPreferences> _screeningPreferences;
    private System.Nullable<Focus.Core.EducationLevels> _minimumEducationLevel;
    private System.Nullable<bool> _minimumEducationLevelRequired;
    private System.Nullable<int> _minimumAge;
    private string _minimumAgeReason;
    private System.Nullable<bool> _minimumAgeRequired;
    private System.Nullable<bool> _licencesRequired;
    private System.Nullable<bool> _certificationRequired;
    private System.Nullable<bool> _languagesRequired;
    private string _tasks;
    private string _redProfanityWords;
    private string _yellowProfanityWords;
    private System.Nullable<long> _employmentStatusId;
    private System.Nullable<long> _jobTypeId;
    private System.Nullable<long> _jobStatusId;
    private System.Nullable<System.DateTime> _approvedOn;
    private System.Nullable<long> _approvedBy;
    [ValidateLength(0, 36)]
    private string _externalId;
    private System.Nullable<System.DateTime> _awaitingApprovalOn;
    private System.Nullable<int> _minimumExperience;
    private System.Nullable<bool> _minimumExperienceRequired;
    private System.Nullable<long> _drivingLicenceClassId;
    private System.Nullable<Focus.Core.DrivingLicenceEndorsements> _drivingLicenceEndorsements;
    private System.Nullable<bool> _drivingLicenceRequired;
    private System.Nullable<bool> _hideSalaryOnPosting;
    private System.Nullable<long> _businessUnitId;
    private System.Nullable<long> _employeeId;
    private long _employerId;
    private System.Nullable<long> _employerDescriptionId;
    private System.Nullable<long> _employerLogoId;
    private System.Nullable<long> _onetId;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly int _lockVersion;
    private readonly System.DateTime _createdOn;
    private readonly System.DateTime _updatedOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobTitle entity attribute.</summary>
    public const string JobTitleField = "JobTitle";
    /// <summary>Identifies the CreatedBy entity attribute.</summary>
    public const string CreatedByField = "CreatedBy";
    /// <summary>Identifies the UpdatedBy entity attribute.</summary>
    public const string UpdatedByField = "UpdatedBy";
    /// <summary>Identifies the ApprovalStatus entity attribute.</summary>
    public const string ApprovalStatusField = "ApprovalStatus";
    /// <summary>Identifies the JobStatus entity attribute.</summary>
    public const string JobStatusField = "JobStatus";
    /// <summary>Identifies the MinSalary entity attribute.</summary>
    public const string MinSalaryField = "MinSalary";
    /// <summary>Identifies the MaxSalary entity attribute.</summary>
    public const string MaxSalaryField = "MaxSalary";
    /// <summary>Identifies the SalaryFrequencyId entity attribute.</summary>
    public const string SalaryFrequencyIdField = "SalaryFrequencyId";
    /// <summary>Identifies the HoursPerWeek entity attribute.</summary>
    public const string HoursPerWeekField = "HoursPerWeek";
    /// <summary>Identifies the OverTimeRequired entity attribute.</summary>
    public const string OverTimeRequiredField = "OverTimeRequired";
    /// <summary>Identifies the ClosingOn entity attribute.</summary>
    public const string ClosingOnField = "ClosingOn";
    /// <summary>Identifies the NumberOfOpenings entity attribute.</summary>
    public const string NumberOfOpeningsField = "NumberOfOpenings";
    /// <summary>Identifies the PostedOn entity attribute.</summary>
    public const string PostedOnField = "PostedOn";
    /// <summary>Identifies the PostedBy entity attribute.</summary>
    public const string PostedByField = "PostedBy";
    /// <summary>Identifies the Description entity attribute.</summary>
    public const string DescriptionField = "Description";
    /// <summary>Identifies the Posting entity attribute.</summary>
    public const string PostingField = "Posting";
    /// <summary>Identifies the EmployerDescriptionPostingPosition entity attribute.</summary>
    public const string EmployerDescriptionPostingPositionField = "EmployerDescriptionPostingPosition";
    /// <summary>Identifies the WizardStep entity attribute.</summary>
    public const string WizardStepField = "WizardStep";
    /// <summary>Identifies the WizardPath entity attribute.</summary>
    public const string WizardPathField = "WizardPath";
    /// <summary>Identifies the HeldOn entity attribute.</summary>
    public const string HeldOnField = "HeldOn";
    /// <summary>Identifies the HeldBy entity attribute.</summary>
    public const string HeldByField = "HeldBy";
    /// <summary>Identifies the ClosedOn entity attribute.</summary>
    public const string ClosedOnField = "ClosedOn";
    /// <summary>Identifies the ClosedBy entity attribute.</summary>
    public const string ClosedByField = "ClosedBy";
    /// <summary>Identifies the JobLocationType entity attribute.</summary>
    public const string JobLocationTypeField = "JobLocationType";
    /// <summary>Identifies the FederalContractor entity attribute.</summary>
    public const string FederalContractorField = "FederalContractor";
    /// <summary>Identifies the ForeignLabourCertification entity attribute.</summary>
    public const string ForeignLabourCertificationField = "ForeignLabourCertification";
    /// <summary>Identifies the CourtOrderedAffirmativeAction entity attribute.</summary>
    public const string CourtOrderedAffirmativeActionField = "CourtOrderedAffirmativeAction";
    /// <summary>Identifies the FederalContractorExpiresOn entity attribute.</summary>
    public const string FederalContractorExpiresOnField = "FederalContractorExpiresOn";
    /// <summary>Identifies the WorkOpportunitiesTaxCreditHires entity attribute.</summary>
    public const string WorkOpportunitiesTaxCreditHiresField = "WorkOpportunitiesTaxCreditHires";
    /// <summary>Identifies the PostingFlags entity attribute.</summary>
    public const string PostingFlagsField = "PostingFlags";
    /// <summary>Identifies the IsCommissionBased entity attribute.</summary>
    public const string IsCommissionBasedField = "IsCommissionBased";
    /// <summary>Identifies the NormalWorkDays entity attribute.</summary>
    public const string NormalWorkDaysField = "NormalWorkDays";
    /// <summary>Identifies the WorkDaysVary entity attribute.</summary>
    public const string WorkDaysVaryField = "WorkDaysVary";
    /// <summary>Identifies the NormalWorkShiftsId entity attribute.</summary>
    public const string NormalWorkShiftsIdField = "NormalWorkShiftsId";
    /// <summary>Identifies the LeaveBenefits entity attribute.</summary>
    public const string LeaveBenefitsField = "LeaveBenefits";
    /// <summary>Identifies the RetirementBenefits entity attribute.</summary>
    public const string RetirementBenefitsField = "RetirementBenefits";
    /// <summary>Identifies the InsuranceBenefits entity attribute.</summary>
    public const string InsuranceBenefitsField = "InsuranceBenefits";
    /// <summary>Identifies the MiscellaneousBenefits entity attribute.</summary>
    public const string MiscellaneousBenefitsField = "MiscellaneousBenefits";
    /// <summary>Identifies the OtherBenefitsDetails entity attribute.</summary>
    public const string OtherBenefitsDetailsField = "OtherBenefitsDetails";
    /// <summary>Identifies the InterviewContactPreferences entity attribute.</summary>
    public const string InterviewContactPreferencesField = "InterviewContactPreferences";
    /// <summary>Identifies the InterviewEmailAddress entity attribute.</summary>
    public const string InterviewEmailAddressField = "InterviewEmailAddress";
    /// <summary>Identifies the InterviewApplicationUrl entity attribute.</summary>
    public const string InterviewApplicationUrlField = "InterviewApplicationUrl";
    /// <summary>Identifies the InterviewMailAddress entity attribute.</summary>
    public const string InterviewMailAddressField = "InterviewMailAddress";
    /// <summary>Identifies the InterviewFaxNumber entity attribute.</summary>
    public const string InterviewFaxNumberField = "InterviewFaxNumber";
    /// <summary>Identifies the InterviewPhoneNumber entity attribute.</summary>
    public const string InterviewPhoneNumberField = "InterviewPhoneNumber";
    /// <summary>Identifies the InterviewDirectApplicationDetails entity attribute.</summary>
    public const string InterviewDirectApplicationDetailsField = "InterviewDirectApplicationDetails";
    /// <summary>Identifies the InterviewOtherInstructions entity attribute.</summary>
    public const string InterviewOtherInstructionsField = "InterviewOtherInstructions";
    /// <summary>Identifies the ScreeningPreferences entity attribute.</summary>
    public const string ScreeningPreferencesField = "ScreeningPreferences";
    /// <summary>Identifies the MinimumEducationLevel entity attribute.</summary>
    public const string MinimumEducationLevelField = "MinimumEducationLevel";
    /// <summary>Identifies the MinimumEducationLevelRequired entity attribute.</summary>
    public const string MinimumEducationLevelRequiredField = "MinimumEducationLevelRequired";
    /// <summary>Identifies the MinimumAge entity attribute.</summary>
    public const string MinimumAgeField = "MinimumAge";
    /// <summary>Identifies the MinimumAgeReason entity attribute.</summary>
    public const string MinimumAgeReasonField = "MinimumAgeReason";
    /// <summary>Identifies the MinimumAgeRequired entity attribute.</summary>
    public const string MinimumAgeRequiredField = "MinimumAgeRequired";
    /// <summary>Identifies the LicencesRequired entity attribute.</summary>
    public const string LicencesRequiredField = "LicencesRequired";
    /// <summary>Identifies the CertificationRequired entity attribute.</summary>
    public const string CertificationRequiredField = "CertificationRequired";
    /// <summary>Identifies the LanguagesRequired entity attribute.</summary>
    public const string LanguagesRequiredField = "LanguagesRequired";
    /// <summary>Identifies the Tasks entity attribute.</summary>
    public const string TasksField = "Tasks";
    /// <summary>Identifies the RedProfanityWords entity attribute.</summary>
    public const string RedProfanityWordsField = "RedProfanityWords";
    /// <summary>Identifies the YellowProfanityWords entity attribute.</summary>
    public const string YellowProfanityWordsField = "YellowProfanityWords";
    /// <summary>Identifies the EmploymentStatusId entity attribute.</summary>
    public const string EmploymentStatusIdField = "EmploymentStatusId";
    /// <summary>Identifies the JobTypeId entity attribute.</summary>
    public const string JobTypeIdField = "JobTypeId";
    /// <summary>Identifies the JobStatusId entity attribute.</summary>
    public const string JobStatusIdField = "JobStatusId";
    /// <summary>Identifies the ApprovedOn entity attribute.</summary>
    public const string ApprovedOnField = "ApprovedOn";
    /// <summary>Identifies the ApprovedBy entity attribute.</summary>
    public const string ApprovedByField = "ApprovedBy";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";
    /// <summary>Identifies the AwaitingApprovalOn entity attribute.</summary>
    public const string AwaitingApprovalOnField = "AwaitingApprovalOn";
    /// <summary>Identifies the MinimumExperience entity attribute.</summary>
    public const string MinimumExperienceField = "MinimumExperience";
    /// <summary>Identifies the MinimumExperienceRequired entity attribute.</summary>
    public const string MinimumExperienceRequiredField = "MinimumExperienceRequired";
    /// <summary>Identifies the DrivingLicenceClassId entity attribute.</summary>
    public const string DrivingLicenceClassIdField = "DrivingLicenceClassId";
    /// <summary>Identifies the DrivingLicenceEndorsements entity attribute.</summary>
    public const string DrivingLicenceEndorsementsField = "DrivingLicenceEndorsements";
    /// <summary>Identifies the DrivingLicenceRequired entity attribute.</summary>
    public const string DrivingLicenceRequiredField = "DrivingLicenceRequired";
    /// <summary>Identifies the HideSalaryOnPosting entity attribute.</summary>
    public const string HideSalaryOnPostingField = "HideSalaryOnPosting";
    /// <summary>Identifies the BusinessUnitId entity attribute.</summary>
    public const string BusinessUnitIdField = "BusinessUnitId";
    /// <summary>Identifies the EmployeeId entity attribute.</summary>
    public const string EmployeeIdField = "EmployeeId";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the EmployerDescriptionId entity attribute.</summary>
    public const string EmployerDescriptionIdField = "EmployerDescriptionId";
    /// <summary>Identifies the EmployerLogoId entity attribute.</summary>
    public const string EmployerLogoIdField = "EmployerLogoId";
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Job")]
    private readonly EntityCollection<CandidateApplication> _candidateApplications = new EntityCollection<CandidateApplication>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobSkill> _jobSkills = new EntityCollection<JobSkill>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobSpecialRequirement> _jobSpecialRequirements = new EntityCollection<JobSpecialRequirement>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobAddress> _jobAddresses = new EntityCollection<JobAddress>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<PostingSurvey> _postingSurveys = new EntityCollection<PostingSurvey>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobLocation> _jobLocations = new EntityCollection<JobLocation>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobCertificate> _jobCertificates = new EntityCollection<JobCertificate>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobLanguage> _jobLanguages = new EntityCollection<JobLanguage>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobLicence> _jobLicences = new EntityCollection<JobLicence>();
    [ReverseAssociation("Jobs")]
    private readonly EntityHolder<BusinessUnit> _businessUnit = new EntityHolder<BusinessUnit>();
    [ReverseAssociation("Jobs")]
    private readonly EntityHolder<Employee> _employee = new EntityHolder<Employee>();
    [ReverseAssociation("Jobs")]
    private readonly EntityHolder<Employer> _employer = new EntityHolder<Employer>();
    [ReverseAssociation("Jobs")]
    private readonly EntityHolder<EmployerDescription> _employerDescription = new EntityHolder<EmployerDescription>();
    [ReverseAssociation("Jobs")]
    private readonly EntityHolder<EmployerLogo> _employerLogo = new EntityHolder<EmployerLogo>();
    [ReverseAssociation("Jobs")]
    private readonly EntityHolder<Onet> _onet = new EntityHolder<Onet>();
    [ReverseAssociation("Job")]
    private readonly EntityCollection<JobNote> _jobNotes = new EntityCollection<JobNote>();

    private ThroughAssociation<JobNote, Note> _notes;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<CandidateApplication> CandidateApplications
    {
      get { return Get(_candidateApplications); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSkill> JobSkills
    {
      get { return Get(_jobSkills); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobSpecialRequirement> JobSpecialRequirements
    {
      get { return Get(_jobSpecialRequirements); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobAddress> JobAddresses
    {
      get { return Get(_jobAddresses); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<PostingSurvey> PostingSurveys
    {
      get { return Get(_postingSurveys); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobLocation> JobLocations
    {
      get { return Get(_jobLocations); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobCertificate> JobCertificates
    {
      get { return Get(_jobCertificates); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobLanguage> JobLanguages
    {
      get { return Get(_jobLanguages); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobLicence> JobLicences
    {
      get { return Get(_jobLicences); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public BusinessUnit BusinessUnit
    {
      get { return Get(_businessUnit); }
      set { Set(_businessUnit, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Employee Employee
    {
      get { return Get(_employee); }
      set { Set(_employee, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Employer Employer
    {
      get { return Get(_employer); }
      set { Set(_employer, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EmployerDescription EmployerDescription
    {
      get { return Get(_employerDescription); }
      set { Set(_employerDescription, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EmployerLogo EmployerLogo
    {
      get { return Get(_employerLogo); }
      set { Set(_employerLogo, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Onet Onet
    {
      get { return Get(_onet); }
      set { Set(_onet, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobNote> JobNotes
    {
      get { return Get(_jobNotes); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<JobNote, Note> Notes
    {
      get
      {
        if (_notes == null)
        {
          _notes = new ThroughAssociation<JobNote, Note>(_jobNotes);
        }
        return Get(_notes);
      }
    }
    

    [System.Diagnostics.DebuggerNonUserCode]
    public string JobTitle
    {
      get { return Get(ref _jobTitle, "JobTitle"); }
      set { Set(ref _jobTitle, value, "JobTitle"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long CreatedBy
    {
      get { return Get(ref _createdBy, "CreatedBy"); }
      set { Set(ref _createdBy, value, "CreatedBy"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long UpdatedBy
    {
      get { return Get(ref _updatedBy, "UpdatedBy"); }
      set { Set(ref _updatedBy, value, "UpdatedBy"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ApprovalStatuses ApprovalStatus
    {
      get { return Get(ref _approvalStatus, "ApprovalStatus"); }
      set { Set(ref _approvalStatus, value, "ApprovalStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.JobStatuses JobStatus
    {
      get { return Get(ref _jobStatus, "JobStatus"); }
      set { Set(ref _jobStatus, value, "JobStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> MinSalary
    {
      get { return Get(ref _minSalary, "MinSalary"); }
      set { Set(ref _minSalary, value, "MinSalary"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> MaxSalary
    {
      get { return Get(ref _maxSalary, "MaxSalary"); }
      set { Set(ref _maxSalary, value, "MaxSalary"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> SalaryFrequencyId
    {
      get { return Get(ref _salaryFrequencyId, "SalaryFrequencyId"); }
      set { Set(ref _salaryFrequencyId, value, "SalaryFrequencyId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> HoursPerWeek
    {
      get { return Get(ref _hoursPerWeek, "HoursPerWeek"); }
      set { Set(ref _hoursPerWeek, value, "HoursPerWeek"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> OverTimeRequired
    {
      get { return Get(ref _overTimeRequired, "OverTimeRequired"); }
      set { Set(ref _overTimeRequired, value, "OverTimeRequired"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ClosingOn
    {
      get { return Get(ref _closingOn, "ClosingOn"); }
      set { Set(ref _closingOn, value, "ClosingOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> NumberOfOpenings
    {
      get { return Get(ref _numberOfOpenings, "NumberOfOpenings"); }
      set { Set(ref _numberOfOpenings, value, "NumberOfOpenings"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> PostedOn
    {
      get { return Get(ref _postedOn, "PostedOn"); }
      set { Set(ref _postedOn, value, "PostedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> PostedBy
    {
      get { return Get(ref _postedBy, "PostedBy"); }
      set { Set(ref _postedBy, value, "PostedBy"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Description
    {
      get { return Get(ref _description, "Description"); }
      set { Set(ref _description, value, "Description"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Posting
    {
      get { return Get(ref _posting, "Posting"); }
      set { Set(ref _posting, value, "Posting"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.EmployerDescriptionPostingPositions EmployerDescriptionPostingPosition
    {
      get { return Get(ref _employerDescriptionPostingPosition, "EmployerDescriptionPostingPosition"); }
      set { Set(ref _employerDescriptionPostingPosition, value, "EmployerDescriptionPostingPosition"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int WizardStep
    {
      get { return Get(ref _wizardStep, "WizardStep"); }
      set { Set(ref _wizardStep, value, "WizardStep"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int WizardPath
    {
      get { return Get(ref _wizardPath, "WizardPath"); }
      set { Set(ref _wizardPath, value, "WizardPath"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> HeldOn
    {
      get { return Get(ref _heldOn, "HeldOn"); }
      set { Set(ref _heldOn, value, "HeldOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> HeldBy
    {
      get { return Get(ref _heldBy, "HeldBy"); }
      set { Set(ref _heldBy, value, "HeldBy"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ClosedOn
    {
      get { return Get(ref _closedOn, "ClosedOn"); }
      set { Set(ref _closedOn, value, "ClosedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> ClosedBy
    {
      get { return Get(ref _closedBy, "ClosedBy"); }
      set { Set(ref _closedBy, value, "ClosedBy"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.JobLocationTypes> JobLocationType
    {
      get { return Get(ref _jobLocationType, "JobLocationType"); }
      set { Set(ref _jobLocationType, value, "JobLocationType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> FederalContractor
    {
      get { return Get(ref _federalContractor, "FederalContractor"); }
      set { Set(ref _federalContractor, value, "FederalContractor"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> ForeignLabourCertification
    {
      get { return Get(ref _foreignLabourCertification, "ForeignLabourCertification"); }
      set { Set(ref _foreignLabourCertification, value, "ForeignLabourCertification"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> CourtOrderedAffirmativeAction
    {
      get { return Get(ref _courtOrderedAffirmativeAction, "CourtOrderedAffirmativeAction"); }
      set { Set(ref _courtOrderedAffirmativeAction, value, "CourtOrderedAffirmativeAction"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> FederalContractorExpiresOn
    {
      get { return Get(ref _federalContractorExpiresOn, "FederalContractorExpiresOn"); }
      set { Set(ref _federalContractorExpiresOn, value, "FederalContractorExpiresOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.WorkOpportunitiesTaxCreditCategories> WorkOpportunitiesTaxCreditHires
    {
      get { return Get(ref _workOpportunitiesTaxCreditHires, "WorkOpportunitiesTaxCreditHires"); }
      set { Set(ref _workOpportunitiesTaxCreditHires, value, "WorkOpportunitiesTaxCreditHires"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.JobPostingFlags> PostingFlags
    {
      get { return Get(ref _postingFlags, "PostingFlags"); }
      set { Set(ref _postingFlags, value, "PostingFlags"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> IsCommissionBased
    {
      get { return Get(ref _isCommissionBased, "IsCommissionBased"); }
      set { Set(ref _isCommissionBased, value, "IsCommissionBased"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.Weekdays> NormalWorkDays
    {
      get { return Get(ref _normalWorkDays, "NormalWorkDays"); }
      set { Set(ref _normalWorkDays, value, "NormalWorkDays"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> WorkDaysVary
    {
      get { return Get(ref _workDaysVary, "WorkDaysVary"); }
      set { Set(ref _workDaysVary, value, "WorkDaysVary"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> NormalWorkShiftsId
    {
      get { return Get(ref _normalWorkShiftsId, "NormalWorkShiftsId"); }
      set { Set(ref _normalWorkShiftsId, value, "NormalWorkShiftsId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.LeaveBenefits> LeaveBenefits
    {
      get { return Get(ref _leaveBenefits, "LeaveBenefits"); }
      set { Set(ref _leaveBenefits, value, "LeaveBenefits"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.RetirementBenefits> RetirementBenefits
    {
      get { return Get(ref _retirementBenefits, "RetirementBenefits"); }
      set { Set(ref _retirementBenefits, value, "RetirementBenefits"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.InsuranceBenefits> InsuranceBenefits
    {
      get { return Get(ref _insuranceBenefits, "InsuranceBenefits"); }
      set { Set(ref _insuranceBenefits, value, "InsuranceBenefits"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.MiscellaneousBenefits> MiscellaneousBenefits
    {
      get { return Get(ref _miscellaneousBenefits, "MiscellaneousBenefits"); }
      set { Set(ref _miscellaneousBenefits, value, "MiscellaneousBenefits"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string OtherBenefitsDetails
    {
      get { return Get(ref _otherBenefitsDetails, "OtherBenefitsDetails"); }
      set { Set(ref _otherBenefitsDetails, value, "OtherBenefitsDetails"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ContactMethods> InterviewContactPreferences
    {
      get { return Get(ref _interviewContactPreferences, "InterviewContactPreferences"); }
      set { Set(ref _interviewContactPreferences, value, "InterviewContactPreferences"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string InterviewEmailAddress
    {
      get { return Get(ref _interviewEmailAddress, "InterviewEmailAddress"); }
      set { Set(ref _interviewEmailAddress, value, "InterviewEmailAddress"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string InterviewApplicationUrl
    {
      get { return Get(ref _interviewApplicationUrl, "InterviewApplicationUrl"); }
      set { Set(ref _interviewApplicationUrl, value, "InterviewApplicationUrl"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string InterviewMailAddress
    {
      get { return Get(ref _interviewMailAddress, "InterviewMailAddress"); }
      set { Set(ref _interviewMailAddress, value, "InterviewMailAddress"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string InterviewFaxNumber
    {
      get { return Get(ref _interviewFaxNumber, "InterviewFaxNumber"); }
      set { Set(ref _interviewFaxNumber, value, "InterviewFaxNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string InterviewPhoneNumber
    {
      get { return Get(ref _interviewPhoneNumber, "InterviewPhoneNumber"); }
      set { Set(ref _interviewPhoneNumber, value, "InterviewPhoneNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string InterviewDirectApplicationDetails
    {
      get { return Get(ref _interviewDirectApplicationDetails, "InterviewDirectApplicationDetails"); }
      set { Set(ref _interviewDirectApplicationDetails, value, "InterviewDirectApplicationDetails"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string InterviewOtherInstructions
    {
      get { return Get(ref _interviewOtherInstructions, "InterviewOtherInstructions"); }
      set { Set(ref _interviewOtherInstructions, value, "InterviewOtherInstructions"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ScreeningPreferences> ScreeningPreferences
    {
      get { return Get(ref _screeningPreferences, "ScreeningPreferences"); }
      set { Set(ref _screeningPreferences, value, "ScreeningPreferences"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.EducationLevels> MinimumEducationLevel
    {
      get { return Get(ref _minimumEducationLevel, "MinimumEducationLevel"); }
      set { Set(ref _minimumEducationLevel, value, "MinimumEducationLevel"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> MinimumEducationLevelRequired
    {
      get { return Get(ref _minimumEducationLevelRequired, "MinimumEducationLevelRequired"); }
      set { Set(ref _minimumEducationLevelRequired, value, "MinimumEducationLevelRequired"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> MinimumAge
    {
      get { return Get(ref _minimumAge, "MinimumAge"); }
      set { Set(ref _minimumAge, value, "MinimumAge"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string MinimumAgeReason
    {
      get { return Get(ref _minimumAgeReason, "MinimumAgeReason"); }
      set { Set(ref _minimumAgeReason, value, "MinimumAgeReason"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> MinimumAgeRequired
    {
      get { return Get(ref _minimumAgeRequired, "MinimumAgeRequired"); }
      set { Set(ref _minimumAgeRequired, value, "MinimumAgeRequired"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> LicencesRequired
    {
      get { return Get(ref _licencesRequired, "LicencesRequired"); }
      set { Set(ref _licencesRequired, value, "LicencesRequired"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> CertificationRequired
    {
      get { return Get(ref _certificationRequired, "CertificationRequired"); }
      set { Set(ref _certificationRequired, value, "CertificationRequired"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> LanguagesRequired
    {
      get { return Get(ref _languagesRequired, "LanguagesRequired"); }
      set { Set(ref _languagesRequired, value, "LanguagesRequired"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Tasks
    {
      get { return Get(ref _tasks, "Tasks"); }
      set { Set(ref _tasks, value, "Tasks"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string RedProfanityWords
    {
      get { return Get(ref _redProfanityWords, "RedProfanityWords"); }
      set { Set(ref _redProfanityWords, value, "RedProfanityWords"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string YellowProfanityWords
    {
      get { return Get(ref _yellowProfanityWords, "YellowProfanityWords"); }
      set { Set(ref _yellowProfanityWords, value, "YellowProfanityWords"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EmploymentStatusId
    {
      get { return Get(ref _employmentStatusId, "EmploymentStatusId"); }
      set { Set(ref _employmentStatusId, value, "EmploymentStatusId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> JobTypeId
    {
      get { return Get(ref _jobTypeId, "JobTypeId"); }
      set { Set(ref _jobTypeId, value, "JobTypeId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> JobStatusId
    {
      get { return Get(ref _jobStatusId, "JobStatusId"); }
      set { Set(ref _jobStatusId, value, "JobStatusId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ApprovedOn
    {
      get { return Get(ref _approvedOn, "ApprovedOn"); }
      set { Set(ref _approvedOn, value, "ApprovedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> ApprovedBy
    {
      get { return Get(ref _approvedBy, "ApprovedBy"); }
      set { Set(ref _approvedBy, value, "ApprovedBy"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> AwaitingApprovalOn
    {
      get { return Get(ref _awaitingApprovalOn, "AwaitingApprovalOn"); }
      set { Set(ref _awaitingApprovalOn, value, "AwaitingApprovalOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> MinimumExperience
    {
      get { return Get(ref _minimumExperience, "MinimumExperience"); }
      set { Set(ref _minimumExperience, value, "MinimumExperience"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> MinimumExperienceRequired
    {
      get { return Get(ref _minimumExperienceRequired, "MinimumExperienceRequired"); }
      set { Set(ref _minimumExperienceRequired, value, "MinimumExperienceRequired"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> DrivingLicenceClassId
    {
      get { return Get(ref _drivingLicenceClassId, "DrivingLicenceClassId"); }
      set { Set(ref _drivingLicenceClassId, value, "DrivingLicenceClassId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.DrivingLicenceEndorsements> DrivingLicenceEndorsements
    {
      get { return Get(ref _drivingLicenceEndorsements, "DrivingLicenceEndorsements"); }
      set { Set(ref _drivingLicenceEndorsements, value, "DrivingLicenceEndorsements"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> DrivingLicenceRequired
    {
      get { return Get(ref _drivingLicenceRequired, "DrivingLicenceRequired"); }
      set { Set(ref _drivingLicenceRequired, value, "DrivingLicenceRequired"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> HideSalaryOnPosting
    {
      get { return Get(ref _hideSalaryOnPosting, "HideSalaryOnPosting"); }
      set { Set(ref _hideSalaryOnPosting, value, "HideSalaryOnPosting"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="BusinessUnit" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> BusinessUnitId
    {
      get { return Get(ref _businessUnitId, "BusinessUnitId"); }
      set { Set(ref _businessUnitId, value, "BusinessUnitId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Employee" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EmployeeId
    {
      get { return Get(ref _employeeId, "EmployeeId"); }
      set { Set(ref _employeeId, value, "EmployeeId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Employer" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="EmployerDescription" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EmployerDescriptionId
    {
      get { return Get(ref _employerDescriptionId, "EmployerDescriptionId"); }
      set { Set(ref _employerDescriptionId, value, "EmployerDescriptionId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="EmployerLogo" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EmployerLogoId
    {
      get { return Get(ref _employerLogoId, "EmployerLogoId"); }
      set { Set(ref _employerLogoId, value, "EmployerLogoId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Onet" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }
    /// <summary>Gets the row version number for concurrency checking</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int LockVersion
    {
      get { return _lockVersion; }
    }

    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }
    }

    /// <summary>Gets the time the entity was last updated</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime UpdatedOn
    {
      get { return _updatedOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class CandidateApplication : Entity<long>
  {
    #region Fields
  
    private Focus.Core.ApprovalStatuses _approvalStatus;
    [ValidateLength(0, 255)]
    private string _approvalRequiredReason;
    private Focus.Core.ApplicationStatusTypes _applicationStatus;
    private int _applicationScore;
    private long _jobId;
    private long _candidateId;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    private readonly System.DateTime _updatedOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ApprovalStatus entity attribute.</summary>
    public const string ApprovalStatusField = "ApprovalStatus";
    /// <summary>Identifies the ApprovalRequiredReason entity attribute.</summary>
    public const string ApprovalRequiredReasonField = "ApprovalRequiredReason";
    /// <summary>Identifies the ApplicationStatus entity attribute.</summary>
    public const string ApplicationStatusField = "ApplicationStatus";
    /// <summary>Identifies the ApplicationScore entity attribute.</summary>
    public const string ApplicationScoreField = "ApplicationScore";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the CandidateId entity attribute.</summary>
    public const string CandidateIdField = "CandidateId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("CandidateApplications")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();
    [ReverseAssociation("CandidateApplications")]
    private readonly EntityHolder<Candidate> _candidate = new EntityHolder<Candidate>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Candidate Candidate
    {
      get { return Get(_candidate); }
      set { Set(_candidate, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ApprovalStatuses ApprovalStatus
    {
      get { return Get(ref _approvalStatus, "ApprovalStatus"); }
      set { Set(ref _approvalStatus, value, "ApprovalStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ApprovalRequiredReason
    {
      get { return Get(ref _approvalRequiredReason, "ApprovalRequiredReason"); }
      set { Set(ref _approvalRequiredReason, value, "ApprovalRequiredReason"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ApplicationStatusTypes ApplicationStatus
    {
      get { return Get(ref _applicationStatus, "ApplicationStatus"); }
      set { Set(ref _applicationStatus, value, "ApplicationStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int ApplicationScore
    {
      get { return Get(ref _applicationScore, "ApplicationScore"); }
      set { Set(ref _applicationScore, value, "ApplicationScore"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Candidate" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long CandidateId
    {
      get { return Get(ref _candidateId, "CandidateId"); }
      set { Set(ref _candidateId, value, "CandidateId"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }
    }

    /// <summary>Gets the time the entity was last updated</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime UpdatedOn
    {
      get { return _updatedOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class JobSkill : Entity<long>
  {
    #region Fields
  
    private long _skillId;
    private long _jobId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SkillId entity attribute.</summary>
    public const string SkillIdField = "SkillId";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSkills")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public long SkillId
    {
      get { return Get(ref _skillId, "SkillId"); }
      set { Set(ref _skillId, value, "SkillId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class JobSpecialRequirement : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 400)]
    private string _requirement;
    private long _jobId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Requirement entity attribute.</summary>
    public const string RequirementField = "Requirement";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobSpecialRequirements")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Requirement
    {
      get { return Get(ref _requirement, "Requirement"); }
      set { Set(ref _requirement, value, "Requirement"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class Localisation : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 5)]
    private string _culture;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Culture entity attribute.</summary>
    public const string CultureField = "Culture";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Localisation")]
    private readonly EntityCollection<LocalisationItem> _localisationItems = new EntityCollection<LocalisationItem>();
    [ReverseAssociation("Localisation")]
    private readonly EntityCollection<OnetLocalisationItem> _onetLocalisationItems = new EntityCollection<OnetLocalisationItem>();
    [ReverseAssociation("Localisation")]
    private readonly EntityCollection<CertificateLicenceLocalisationItem> _certificateLicences = new EntityCollection<CertificateLicenceLocalisationItem>();
    [ReverseAssociation("Localisation")]
    private readonly EntityCollection<JobTaskLocalisationItem> _jobTaskLocalisationItems = new EntityCollection<JobTaskLocalisationItem>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<LocalisationItem> LocalisationItems
    {
      get { return Get(_localisationItems); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<OnetLocalisationItem> OnetLocalisationItems
    {
      get { return Get(_onetLocalisationItems); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<CertificateLicenceLocalisationItem> CertificateLicences
    {
      get { return Get(_certificateLicences); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobTaskLocalisationItem> JobTaskLocalisationItems
    {
      get { return Get(_jobTaskLocalisationItems); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Culture
    {
      get { return Get(ref _culture, "Culture"); }
      set { Set(ref _culture, value, "Culture"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class LocalisationItem : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 50)]
    private string _contextKey;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _key;
    [ValidatePresence]
    private string _value;
    private bool _localised;
    private long _localisationId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ContextKey entity attribute.</summary>
    public const string ContextKeyField = "ContextKey";
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the Value entity attribute.</summary>
    public const string ValueField = "Value";
    /// <summary>Identifies the Localised entity attribute.</summary>
    public const string LocalisedField = "Localised";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("LocalisationItems")]
    private readonly EntityHolder<Localisation> _localisation = new EntityHolder<Localisation>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Localisation Localisation
    {
      get { return Get(_localisation); }
      set { Set(_localisation, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string ContextKey
    {
      get { return Get(ref _contextKey, "ContextKey"); }
      set { Set(ref _contextKey, value, "ContextKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Value
    {
      get { return Get(ref _value, "Value"); }
      set { Set(ref _value, value, "Value"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool Localised
    {
      get { return Get(ref _localised, "Localised"); }
      set { Set(ref _localised, value, "Localised"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Localisation" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class Note : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _title;
    [ValidatePresence]
    [ValidateLength(0, 1000)]
    private string _body;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Title entity attribute.</summary>
    public const string TitleField = "Title";
    /// <summary>Identifies the Body entity attribute.</summary>
    public const string BodyField = "Body";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Note")]
    private readonly EntityCollection<JobNote> _jobNotes = new EntityCollection<JobNote>();

    private ThroughAssociation<JobNote, Job> _jobs;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobNote> JobNotes
    {
      get { return Get(_jobNotes); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<JobNote, Job> Jobs
    {
      get
      {
        if (_jobs == null)
        {
          _jobs = new ThroughAssociation<JobNote, Job>(_jobNotes);
        }
        return Get(_jobs);
      }
    }
    

    [System.Diagnostics.DebuggerNonUserCode]
    public string Title
    {
      get { return Get(ref _title, "Title"); }
      set { Set(ref _title, value, "Title"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Body
    {
      get { return Get(ref _body, "Body"); }
      set { Set(ref _body, value, "Body"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class Person : Entity<long>
  {
    #region Fields
  
    private long _titleId;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _firstName;
    [ValidateLength(0, 5)]
    private string _middleInitial;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _lastName;
    private System.Nullable<System.DateTime> _dateOfBirth;
    [ValidateLength(0, 11)]
    private string _socialSecurityNumber;
    [ValidateLength(0, 200)]
    private string _jobTitle;
    [ValidateLength(0, 200)]
    private string _emailAddress;
    [ValidateLength(0, 200)]
    private string _office;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the TitleId entity attribute.</summary>
    public const string TitleIdField = "TitleId";
    /// <summary>Identifies the FirstName entity attribute.</summary>
    public const string FirstNameField = "FirstName";
    /// <summary>Identifies the MiddleInitial entity attribute.</summary>
    public const string MiddleInitialField = "MiddleInitial";
    /// <summary>Identifies the LastName entity attribute.</summary>
    public const string LastNameField = "LastName";
    /// <summary>Identifies the DateOfBirth entity attribute.</summary>
    public const string DateOfBirthField = "DateOfBirth";
    /// <summary>Identifies the SocialSecurityNumber entity attribute.</summary>
    public const string SocialSecurityNumberField = "SocialSecurityNumber";
    /// <summary>Identifies the JobTitle entity attribute.</summary>
    public const string JobTitleField = "JobTitle";
    /// <summary>Identifies the EmailAddress entity attribute.</summary>
    public const string EmailAddressField = "EmailAddress";
    /// <summary>Identifies the Office entity attribute.</summary>
    public const string OfficeField = "Office";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Person")]
    private readonly EntityCollection<PersonAddress> _personAddresses = new EntityCollection<PersonAddress>();
    [ReverseAssociation("Person")]
    private readonly EntityCollection<PhoneNumber> _phoneNumbers = new EntityCollection<PhoneNumber>();
    [ReverseAssociation("Person")]
    private readonly EntityCollection<PersonList> _personLists = new EntityCollection<PersonList>();
    [ReverseAssociation("Person")]
    private readonly EntityCollection<PersonResume> _personResumes = new EntityCollection<PersonResume>();
    [ReverseAssociation("Person")]
    private readonly EntityHolder<User> _user = new EntityHolder<User>();
    [ReverseAssociation("Person")]
    private readonly EntityHolder<Employee> _employee = new EntityHolder<Employee>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<PersonAddress> PersonAddresses
    {
      get { return Get(_personAddresses); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<PhoneNumber> PhoneNumbers
    {
      get { return Get(_phoneNumbers); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<PersonList> PersonLists
    {
      get { return Get(_personLists); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<PersonResume> PersonResumes
    {
      get { return Get(_personResumes); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public User User
    {
      get { return Get(_user); }
      set { Set(_user, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Employee Employee
    {
      get { return Get(_employee); }
      set { Set(_employee, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public long TitleId
    {
      get { return Get(ref _titleId, "TitleId"); }
      set { Set(ref _titleId, value, "TitleId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string FirstName
    {
      get { return Get(ref _firstName, "FirstName"); }
      set { Set(ref _firstName, value, "FirstName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string MiddleInitial
    {
      get { return Get(ref _middleInitial, "MiddleInitial"); }
      set { Set(ref _middleInitial, value, "MiddleInitial"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LastName
    {
      get { return Get(ref _lastName, "LastName"); }
      set { Set(ref _lastName, value, "LastName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> DateOfBirth
    {
      get { return Get(ref _dateOfBirth, "DateOfBirth"); }
      set { Set(ref _dateOfBirth, value, "DateOfBirth"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string SocialSecurityNumber
    {
      get { return Get(ref _socialSecurityNumber, "SocialSecurityNumber"); }
      set { Set(ref _socialSecurityNumber, value, "SocialSecurityNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string JobTitle
    {
      get { return Get(ref _jobTitle, "JobTitle"); }
      set { Set(ref _jobTitle, value, "JobTitle"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmailAddress
    {
      get { return Get(ref _emailAddress, "EmailAddress"); }
      set { Set(ref _emailAddress, value, "EmailAddress"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Office
    {
      get { return Get(ref _office, "Office"); }
      set { Set(ref _office, value, "Office"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class PostingSurvey : Entity<long>
  {
    #region Fields
  
    private Focus.Core.SurveyTypes _surveyType;
    private long _satisfactionLevel;
    private bool _didInterview;
    private bool _didHire;
    private System.Nullable<bool> _didOffer;
    private long _jobId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SurveyType entity attribute.</summary>
    public const string SurveyTypeField = "SurveyType";
    /// <summary>Identifies the SatisfactionLevel entity attribute.</summary>
    public const string SatisfactionLevelField = "SatisfactionLevel";
    /// <summary>Identifies the DidInterview entity attribute.</summary>
    public const string DidInterviewField = "DidInterview";
    /// <summary>Identifies the DidHire entity attribute.</summary>
    public const string DidHireField = "DidHire";
    /// <summary>Identifies the DidOffer entity attribute.</summary>
    public const string DidOfferField = "DidOffer";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("PostingSurveys")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.SurveyTypes SurveyType
    {
      get { return Get(ref _surveyType, "SurveyType"); }
      set { Set(ref _surveyType, value, "SurveyType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long SatisfactionLevel
    {
      get { return Get(ref _satisfactionLevel, "SatisfactionLevel"); }
      set { Set(ref _satisfactionLevel, value, "SatisfactionLevel"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool DidInterview
    {
      get { return Get(ref _didInterview, "DidInterview"); }
      set { Set(ref _didInterview, value, "DidInterview"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool DidHire
    {
      get { return Get(ref _didHire, "DidHire"); }
      set { Set(ref _didHire, value, "DidHire"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> DidOffer
    {
      get { return Get(ref _didOffer, "DidOffer"); }
      set { Set(ref _didOffer, value, "DidOffer"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class SavedSearch : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _name;
    [ValidatePresence]
    private string _searchCriteria;
    private bool _alertEmailRequired;
    private Focus.Core.EmailAlertFrequencies _alertEmailFrequency;
    private Focus.Core.EmailFormats _alertEmailFormat;
    [ValidateLength(0, 200)]
    private string _alertEmailAddress;
    private System.Nullable<System.DateTime> _alertEmailScheduledOn;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the SearchCriteria entity attribute.</summary>
    public const string SearchCriteriaField = "SearchCriteria";
    /// <summary>Identifies the AlertEmailRequired entity attribute.</summary>
    public const string AlertEmailRequiredField = "AlertEmailRequired";
    /// <summary>Identifies the AlertEmailFrequency entity attribute.</summary>
    public const string AlertEmailFrequencyField = "AlertEmailFrequency";
    /// <summary>Identifies the AlertEmailFormat entity attribute.</summary>
    public const string AlertEmailFormatField = "AlertEmailFormat";
    /// <summary>Identifies the AlertEmailAddress entity attribute.</summary>
    public const string AlertEmailAddressField = "AlertEmailAddress";
    /// <summary>Identifies the AlertEmailScheduledOn entity attribute.</summary>
    public const string AlertEmailScheduledOnField = "AlertEmailScheduledOn";


    #endregion
    
    #region Relationships

    [ReverseAssociation("SavedSearch")]
    private readonly EntityCollection<SavedSearchEmployee> _savedSearchEmployees = new EntityCollection<SavedSearchEmployee>();

    private ThroughAssociation<SavedSearchEmployee, Employee> _employees;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<SavedSearchEmployee> SavedSearchEmployees
    {
      get { return Get(_savedSearchEmployees); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<SavedSearchEmployee, Employee> Employees
    {
      get
      {
        if (_employees == null)
        {
          _employees = new ThroughAssociation<SavedSearchEmployee, Employee>(_savedSearchEmployees);
        }
        return Get(_employees);
      }
    }
    

    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string SearchCriteria
    {
      get { return Get(ref _searchCriteria, "SearchCriteria"); }
      set { Set(ref _searchCriteria, value, "SearchCriteria"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool AlertEmailRequired
    {
      get { return Get(ref _alertEmailRequired, "AlertEmailRequired"); }
      set { Set(ref _alertEmailRequired, value, "AlertEmailRequired"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.EmailAlertFrequencies AlertEmailFrequency
    {
      get { return Get(ref _alertEmailFrequency, "AlertEmailFrequency"); }
      set { Set(ref _alertEmailFrequency, value, "AlertEmailFrequency"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.EmailFormats AlertEmailFormat
    {
      get { return Get(ref _alertEmailFormat, "AlertEmailFormat"); }
      set { Set(ref _alertEmailFormat, value, "AlertEmailFormat"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AlertEmailAddress
    {
      get { return Get(ref _alertEmailAddress, "AlertEmailAddress"); }
      set { Set(ref _alertEmailAddress, value, "AlertEmailAddress"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> AlertEmailScheduledOn
    {
      get { return Get(ref _alertEmailScheduledOn, "AlertEmailScheduledOn"); }
      set { Set(ref _alertEmailScheduledOn, value, "AlertEmailScheduledOn"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class EmployerAddress : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _line1;
    [ValidateLength(0, 200)]
    private string _line2;
    [ValidateLength(0, 200)]
    private string _line3;
    [ValidateLength(0, 100)]
    private string _townCity;
    private System.Nullable<long> _countyId;
    [ValidateLength(0, 20)]
    private string _postcodeZip;
    private long _stateId;
    private long _countryId;
    private bool _isPrimary;
    private bool _publicTransitAccessible;
    private long _employerId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Line1 entity attribute.</summary>
    public const string Line1Field = "Line1";
    /// <summary>Identifies the Line2 entity attribute.</summary>
    public const string Line2Field = "Line2";
    /// <summary>Identifies the Line3 entity attribute.</summary>
    public const string Line3Field = "Line3";
    /// <summary>Identifies the TownCity entity attribute.</summary>
    public const string TownCityField = "TownCity";
    /// <summary>Identifies the CountyId entity attribute.</summary>
    public const string CountyIdField = "CountyId";
    /// <summary>Identifies the PostcodeZip entity attribute.</summary>
    public const string PostcodeZipField = "PostcodeZip";
    /// <summary>Identifies the StateId entity attribute.</summary>
    public const string StateIdField = "StateId";
    /// <summary>Identifies the CountryId entity attribute.</summary>
    public const string CountryIdField = "CountryId";
    /// <summary>Identifies the IsPrimary entity attribute.</summary>
    public const string IsPrimaryField = "IsPrimary";
    /// <summary>Identifies the PublicTransitAccessible entity attribute.</summary>
    public const string PublicTransitAccessibleField = "PublicTransitAccessible";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("EmployerAddresses")]
    private readonly EntityHolder<Employer> _employer = new EntityHolder<Employer>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Employer Employer
    {
      get { return Get(_employer); }
      set { Set(_employer, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Line1
    {
      get { return Get(ref _line1, "Line1"); }
      set { Set(ref _line1, value, "Line1"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Line2
    {
      get { return Get(ref _line2, "Line2"); }
      set { Set(ref _line2, value, "Line2"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Line3
    {
      get { return Get(ref _line3, "Line3"); }
      set { Set(ref _line3, value, "Line3"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string TownCity
    {
      get { return Get(ref _townCity, "TownCity"); }
      set { Set(ref _townCity, value, "TownCity"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> CountyId
    {
      get { return Get(ref _countyId, "CountyId"); }
      set { Set(ref _countyId, value, "CountyId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PostcodeZip
    {
      get { return Get(ref _postcodeZip, "PostcodeZip"); }
      set { Set(ref _postcodeZip, value, "PostcodeZip"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long StateId
    {
      get { return Get(ref _stateId, "StateId"); }
      set { Set(ref _stateId, value, "StateId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long CountryId
    {
      get { return Get(ref _countryId, "CountryId"); }
      set { Set(ref _countryId, value, "CountryId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsPrimary
    {
      get { return Get(ref _isPrimary, "IsPrimary"); }
      set { Set(ref _isPrimary, value, "IsPrimary"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool PublicTransitAccessible
    {
      get { return Get(ref _publicTransitAccessible, "PublicTransitAccessible"); }
      set { Set(ref _publicTransitAccessible, value, "PublicTransitAccessible"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Employer" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class JobAddress : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _line1;
    [ValidateLength(0, 200)]
    private string _line2;
    [ValidateLength(0, 200)]
    private string _line3;
    [ValidateLength(0, 100)]
    private string _townCity;
    private System.Nullable<long> _countyId;
    [ValidateLength(0, 20)]
    private string _postcodeZip;
    private long _stateId;
    private long _countryId;
    private bool _isPrimary;
    private long _jobId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Line1 entity attribute.</summary>
    public const string Line1Field = "Line1";
    /// <summary>Identifies the Line2 entity attribute.</summary>
    public const string Line2Field = "Line2";
    /// <summary>Identifies the Line3 entity attribute.</summary>
    public const string Line3Field = "Line3";
    /// <summary>Identifies the TownCity entity attribute.</summary>
    public const string TownCityField = "TownCity";
    /// <summary>Identifies the CountyId entity attribute.</summary>
    public const string CountyIdField = "CountyId";
    /// <summary>Identifies the PostcodeZip entity attribute.</summary>
    public const string PostcodeZipField = "PostcodeZip";
    /// <summary>Identifies the StateId entity attribute.</summary>
    public const string StateIdField = "StateId";
    /// <summary>Identifies the CountryId entity attribute.</summary>
    public const string CountryIdField = "CountryId";
    /// <summary>Identifies the IsPrimary entity attribute.</summary>
    public const string IsPrimaryField = "IsPrimary";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobAddresses")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Line1
    {
      get { return Get(ref _line1, "Line1"); }
      set { Set(ref _line1, value, "Line1"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Line2
    {
      get { return Get(ref _line2, "Line2"); }
      set { Set(ref _line2, value, "Line2"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Line3
    {
      get { return Get(ref _line3, "Line3"); }
      set { Set(ref _line3, value, "Line3"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string TownCity
    {
      get { return Get(ref _townCity, "TownCity"); }
      set { Set(ref _townCity, value, "TownCity"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> CountyId
    {
      get { return Get(ref _countyId, "CountyId"); }
      set { Set(ref _countyId, value, "CountyId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PostcodeZip
    {
      get { return Get(ref _postcodeZip, "PostcodeZip"); }
      set { Set(ref _postcodeZip, value, "PostcodeZip"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long StateId
    {
      get { return Get(ref _stateId, "StateId"); }
      set { Set(ref _stateId, value, "StateId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long CountryId
    {
      get { return Get(ref _countryId, "CountryId"); }
      set { Set(ref _countryId, value, "CountryId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsPrimary
    {
      get { return Get(ref _isPrimary, "IsPrimary"); }
      set { Set(ref _isPrimary, value, "IsPrimary"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class Role : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _key;
    [ValidateLength(0, 200)]
    private string _value;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the Value entity attribute.</summary>
    public const string ValueField = "Value";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Role")]
    private readonly EntityCollection<UserRole> _userRoles = new EntityCollection<UserRole>();

    private ThroughAssociation<UserRole, User> _users;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<UserRole> UserRoles
    {
      get { return Get(_userRoles); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<UserRole, User> Users
    {
      get
      {
        if (_users == null)
        {
          _users = new ThroughAssociation<UserRole, User>(_userRoles);
        }
        return Get(_users);
      }
    }
    

    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Value
    {
      get { return Get(ref _value, "Value"); }
      set { Set(ref _value, value, "Value"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class PersonAddress : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _line1;
    [ValidateLength(0, 200)]
    private string _line2;
    [ValidateLength(0, 200)]
    private string _line3;
    [ValidateLength(0, 100)]
    private string _townCity;
    private System.Nullable<long> _countyId;
    [ValidateLength(0, 20)]
    private string _postcodeZip;
    private long _stateId;
    private long _countryId;
    private bool _isPrimary;
    private long _personId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Line1 entity attribute.</summary>
    public const string Line1Field = "Line1";
    /// <summary>Identifies the Line2 entity attribute.</summary>
    public const string Line2Field = "Line2";
    /// <summary>Identifies the Line3 entity attribute.</summary>
    public const string Line3Field = "Line3";
    /// <summary>Identifies the TownCity entity attribute.</summary>
    public const string TownCityField = "TownCity";
    /// <summary>Identifies the CountyId entity attribute.</summary>
    public const string CountyIdField = "CountyId";
    /// <summary>Identifies the PostcodeZip entity attribute.</summary>
    public const string PostcodeZipField = "PostcodeZip";
    /// <summary>Identifies the StateId entity attribute.</summary>
    public const string StateIdField = "StateId";
    /// <summary>Identifies the CountryId entity attribute.</summary>
    public const string CountryIdField = "CountryId";
    /// <summary>Identifies the IsPrimary entity attribute.</summary>
    public const string IsPrimaryField = "IsPrimary";
    /// <summary>Identifies the PersonId entity attribute.</summary>
    public const string PersonIdField = "PersonId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("PersonAddresses")]
    private readonly EntityHolder<Person> _person = new EntityHolder<Person>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Person Person
    {
      get { return Get(_person); }
      set { Set(_person, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Line1
    {
      get { return Get(ref _line1, "Line1"); }
      set { Set(ref _line1, value, "Line1"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Line2
    {
      get { return Get(ref _line2, "Line2"); }
      set { Set(ref _line2, value, "Line2"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Line3
    {
      get { return Get(ref _line3, "Line3"); }
      set { Set(ref _line3, value, "Line3"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string TownCity
    {
      get { return Get(ref _townCity, "TownCity"); }
      set { Set(ref _townCity, value, "TownCity"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> CountyId
    {
      get { return Get(ref _countyId, "CountyId"); }
      set { Set(ref _countyId, value, "CountyId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PostcodeZip
    {
      get { return Get(ref _postcodeZip, "PostcodeZip"); }
      set { Set(ref _postcodeZip, value, "PostcodeZip"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long StateId
    {
      get { return Get(ref _stateId, "StateId"); }
      set { Set(ref _stateId, value, "StateId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long CountryId
    {
      get { return Get(ref _countryId, "CountryId"); }
      set { Set(ref _countryId, value, "CountryId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsPrimary
    {
      get { return Get(ref _isPrimary, "IsPrimary"); }
      set { Set(ref _isPrimary, value, "IsPrimary"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Person" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long PersonId
    {
      get { return Get(ref _personId, "PersonId"); }
      set { Set(ref _personId, value, "PersonId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class ActionType : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    [ValidatePresence]
    private string _name;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";


    #endregion
    
    #region Relationships

    [ReverseAssociation("ActionType")]
    private readonly EntityCollection<ActionEvent> _actionEvents = new EntityCollection<ActionEvent>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<ActionEvent> ActionEvents
    {
      get { return Get(_actionEvents); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class Onet : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 10)]
    private string _onetCode;
    [ValidateLength(0, 200)]
    private string _key;
    private long _jobFamilyId;
    private int _jobZone;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the OnetCode entity attribute.</summary>
    public const string OnetCodeField = "OnetCode";
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the JobFamilyId entity attribute.</summary>
    public const string JobFamilyIdField = "JobFamilyId";
    /// <summary>Identifies the JobZone entity attribute.</summary>
    public const string JobZoneField = "JobZone";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Onet")]
    private readonly EntityCollection<Job> _jobs = new EntityCollection<Job>();
    [ReverseAssociation("Onet")]
    private readonly EntityCollection<OnetTask> _onetTasks = new EntityCollection<OnetTask>();
    [ReverseAssociation("Onet")]
    private readonly EntityCollection<JobTask> _jobTasks = new EntityCollection<JobTask>();
    [ReverseAssociation("Onet")]
    private readonly EntityCollection<OnetWord> _onetWords = new EntityCollection<OnetWord>();
    [ReverseAssociation("Onet")]
    private readonly EntityCollection<OnetPhrase> _onetPhrases = new EntityCollection<OnetPhrase>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Job> Jobs
    {
      get { return Get(_jobs); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<OnetTask> OnetTasks
    {
      get { return Get(_onetTasks); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobTask> JobTasks
    {
      get { return Get(_jobTasks); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<OnetWord> OnetWords
    {
      get { return Get(_onetWords); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<OnetPhrase> OnetPhrases
    {
      get { return Get(_onetPhrases); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetCode
    {
      get { return Get(ref _onetCode, "OnetCode"); }
      set { Set(ref _onetCode, value, "OnetCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long JobFamilyId
    {
      get { return Get(ref _jobFamilyId, "JobFamilyId"); }
      set { Set(ref _jobFamilyId, value, "JobFamilyId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int JobZone
    {
      get { return Get(ref _jobZone, "JobZone"); }
      set { Set(ref _jobZone, value, "JobZone"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class EmailTemplate : Entity<long>
  {
    #region Fields
  
    private Focus.Core.EmailTemplateTypes _emailTemplateType;
    [ValidateLength(0, 1000)]
    private string _subject;
    private string _body;
    [ValidateLength(0, 100)]
    private string _salutation;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the EmailTemplateType entity attribute.</summary>
    public const string EmailTemplateTypeField = "EmailTemplateType";
    /// <summary>Identifies the Subject entity attribute.</summary>
    public const string SubjectField = "Subject";
    /// <summary>Identifies the Body entity attribute.</summary>
    public const string BodyField = "Body";
    /// <summary>Identifies the Salutation entity attribute.</summary>
    public const string SalutationField = "Salutation";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.EmailTemplateTypes EmailTemplateType
    {
      get { return Get(ref _emailTemplateType, "EmailTemplateType"); }
      set { Set(ref _emailTemplateType, value, "EmailTemplateType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Subject
    {
      get { return Get(ref _subject, "Subject"); }
      set { Set(ref _subject, value, "Subject"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Body
    {
      get { return Get(ref _body, "Body"); }
      set { Set(ref _body, value, "Body"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Salutation
    {
      get { return Get(ref _salutation, "Salutation"); }
      set { Set(ref _salutation, value, "Salutation"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class JobLocation : Entity<long>
  {
    #region Fields
  
    private string _location;
    private bool _isPublicTransitAccessible;
    private long _jobId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Location entity attribute.</summary>
    public const string LocationField = "Location";
    /// <summary>Identifies the IsPublicTransitAccessible entity attribute.</summary>
    public const string IsPublicTransitAccessibleField = "IsPublicTransitAccessible";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobLocations")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Location
    {
      get { return Get(ref _location, "Location"); }
      set { Set(ref _location, value, "Location"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsPublicTransitAccessible
    {
      get { return Get(ref _isPublicTransitAccessible, "IsPublicTransitAccessible"); }
      set { Set(ref _isPublicTransitAccessible, value, "IsPublicTransitAccessible"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class OnetTask : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _key;
    private long _onetId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("OnetTasks")]
    private readonly EntityHolder<Onet> _onet = new EntityHolder<Onet>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Onet Onet
    {
      get { return Get(_onet); }
      set { Set(_onet, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Onet" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class CertificateLicence : Entity<long>
  {
    #region Fields
  
    private string _key;
    private Focus.Core.CertifcateLicenseTypes _certificateLicenseType;
    [ValidateLength(0, 40)]
    private string _onetParentCode;
    private string _isCertificate;
    private string _isLicence;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the CertificateLicenseType entity attribute.</summary>
    public const string CertificateLicenseTypeField = "CertificateLicenseType";
    /// <summary>Identifies the OnetParentCode entity attribute.</summary>
    public const string OnetParentCodeField = "OnetParentCode";
    /// <summary>Identifies the IsCertificate entity attribute.</summary>
    public const string IsCertificateField = "IsCertificate";
    /// <summary>Identifies the IsLicence entity attribute.</summary>
    public const string IsLicenceField = "IsLicence";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.CertifcateLicenseTypes CertificateLicenseType
    {
      get { return Get(ref _certificateLicenseType, "CertificateLicenseType"); }
      set { Set(ref _certificateLicenseType, value, "CertificateLicenseType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetParentCode
    {
      get { return Get(ref _onetParentCode, "OnetParentCode"); }
      set { Set(ref _onetParentCode, value, "OnetParentCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string IsCertificate
    {
      get { return Get(ref _isCertificate, "IsCertificate"); }
      set { Set(ref _isCertificate, value, "IsCertificate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string IsLicence
    {
      get { return Get(ref _isLicence, "IsLicence"); }
      set { Set(ref _isLicence, value, "IsLicence"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class JobTask : Entity<long>
  {
    #region Fields
  
    private Focus.Core.JobTaskTypes _jobTaskType;
    [ValidateLength(0, 200)]
    private string _key;
    private long _onetId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobTaskType entity attribute.</summary>
    public const string JobTaskTypeField = "JobTaskType";
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobTask")]
    private readonly EntityCollection<JobTaskMultiOption> _jobTaskMultiOptions = new EntityCollection<JobTaskMultiOption>();
    [ReverseAssociation("JobTasks")]
    private readonly EntityHolder<Onet> _onet = new EntityHolder<Onet>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobTaskMultiOption> JobTaskMultiOptions
    {
      get { return Get(_jobTaskMultiOptions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Onet Onet
    {
      get { return Get(_onet); }
      set { Set(_onet, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.JobTaskTypes JobTaskType
    {
      get { return Get(ref _jobTaskType, "JobTaskType"); }
      set { Set(ref _jobTaskType, value, "JobTaskType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Onet" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class JobTaskMultiOption : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _key;
    private long _jobTaskId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the JobTaskId entity attribute.</summary>
    public const string JobTaskIdField = "JobTaskId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobTaskMultiOptions")]
    private readonly EntityHolder<JobTask> _jobTask = new EntityHolder<JobTask>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public JobTask JobTask
    {
      get { return Get(_jobTask); }
      set { Set(_jobTask, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="JobTask" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobTaskId
    {
      get { return Get(ref _jobTaskId, "JobTaskId"); }
      set { Set(ref _jobTaskId, value, "JobTaskId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class JobLicence : Entity<long>
  {
    #region Fields
  
    private string _licence;
    private long _jobId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Licence entity attribute.</summary>
    public const string LicenceField = "Licence";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobLicences")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Licence
    {
      get { return Get(ref _licence, "Licence"); }
      set { Set(ref _licence, value, "Licence"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class JobCertificate : Entity<long>
  {
    #region Fields
  
    private string _certificate;
    private long _jobId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Certificate entity attribute.</summary>
    public const string CertificateField = "Certificate";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobCertificates")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Certificate
    {
      get { return Get(ref _certificate, "Certificate"); }
      set { Set(ref _certificate, value, "Certificate"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class JobLanguage : Entity<long>
  {
    #region Fields
  
    private string _language;
    private long _jobId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Language entity attribute.</summary>
    public const string LanguageField = "Language";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobLanguages")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Language
    {
      get { return Get(ref _language, "Language"); }
      set { Set(ref _language, value, "Language"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Job" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class Profanity : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _phrase;
    private Focus.Core.ProfanityLevelTypes _profanityLevel;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Phrase entity attribute.</summary>
    public const string PhraseField = "Phrase";
    /// <summary>Identifies the ProfanityLevel entity attribute.</summary>
    public const string ProfanityLevelField = "ProfanityLevel";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Phrase
    {
      get { return Get(ref _phrase, "Phrase"); }
      set { Set(ref _phrase, value, "Phrase"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ProfanityLevelTypes ProfanityLevel
    {
      get { return Get(ref _profanityLevel, "ProfanityLevel"); }
      set { Set(ref _profanityLevel, value, "ProfanityLevel"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class OnetLocalisationItem : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _key;
    private string _primaryValue;
    private string _secondaryValue;
    private long _localisationId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the PrimaryValue entity attribute.</summary>
    public const string PrimaryValueField = "PrimaryValue";
    /// <summary>Identifies the SecondaryValue entity attribute.</summary>
    public const string SecondaryValueField = "SecondaryValue";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("OnetLocalisationItems")]
    private readonly EntityHolder<Localisation> _localisation = new EntityHolder<Localisation>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Localisation Localisation
    {
      get { return Get(_localisation); }
      set { Set(_localisation, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PrimaryValue
    {
      get { return Get(ref _primaryValue, "PrimaryValue"); }
      set { Set(ref _primaryValue, value, "PrimaryValue"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string SecondaryValue
    {
      get { return Get(ref _secondaryValue, "SecondaryValue"); }
      set { Set(ref _secondaryValue, value, "SecondaryValue"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Localisation" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class JobTaskLocalisationItem : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _key;
    private string _primaryValue;
    private string _secondaryValue;
    private long _localisationId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the PrimaryValue entity attribute.</summary>
    public const string PrimaryValueField = "PrimaryValue";
    /// <summary>Identifies the SecondaryValue entity attribute.</summary>
    public const string SecondaryValueField = "SecondaryValue";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobTaskLocalisationItems")]
    private readonly EntityHolder<Localisation> _localisation = new EntityHolder<Localisation>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Localisation Localisation
    {
      get { return Get(_localisation); }
      set { Set(_localisation, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PrimaryValue
    {
      get { return Get(ref _primaryValue, "PrimaryValue"); }
      set { Set(ref _primaryValue, value, "PrimaryValue"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string SecondaryValue
    {
      get { return Get(ref _secondaryValue, "SecondaryValue"); }
      set { Set(ref _secondaryValue, value, "SecondaryValue"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Localisation" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class CertificateLicenceLocalisationItem : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _key;
    [ValidateLength(0, 500)]
    private string _value;
    private long _localisationId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the Value entity attribute.</summary>
    public const string ValueField = "Value";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("CertificateLicences")]
    private readonly EntityHolder<Localisation> _localisation = new EntityHolder<Localisation>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Localisation Localisation
    {
      get { return Get(_localisation); }
      set { Set(_localisation, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Value
    {
      get { return Get(ref _value, "Value"); }
      set { Set(ref _value, value, "Value"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Localisation" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class OnetWord : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _key;
    [ValidateLength(0, 30)]
    private string _word;
    [ValidateLength(0, 30)]
    private string _stem;
    private System.Nullable<int> _frequency;
    [ValidateLength(0, 10)]
    private string _onetSoc;
    private long _onetId;
    private long _onetRingId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the Word entity attribute.</summary>
    public const string WordField = "Word";
    /// <summary>Identifies the Stem entity attribute.</summary>
    public const string StemField = "Stem";
    /// <summary>Identifies the Frequency entity attribute.</summary>
    public const string FrequencyField = "Frequency";
    /// <summary>Identifies the OnetSoc entity attribute.</summary>
    public const string OnetSocField = "OnetSoc";
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";
    /// <summary>Identifies the OnetRingId entity attribute.</summary>
    public const string OnetRingIdField = "OnetRingId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("OnetWords")]
    private readonly EntityHolder<Onet> _onet = new EntityHolder<Onet>();
    [ReverseAssociation("OnetWords")]
    private readonly EntityHolder<OnetRing> _onetRing = new EntityHolder<OnetRing>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Onet Onet
    {
      get { return Get(_onet); }
      set { Set(_onet, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public OnetRing OnetRing
    {
      get { return Get(_onetRing); }
      set { Set(_onetRing, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Word
    {
      get { return Get(ref _word, "Word"); }
      set { Set(ref _word, value, "Word"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Stem
    {
      get { return Get(ref _stem, "Stem"); }
      set { Set(ref _stem, value, "Stem"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> Frequency
    {
      get { return Get(ref _frequency, "Frequency"); }
      set { Set(ref _frequency, value, "Frequency"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetSoc
    {
      get { return Get(ref _onetSoc, "OnetSoc"); }
      set { Set(ref _onetSoc, value, "OnetSoc"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Onet" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="OnetRing" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetRingId
    {
      get { return Get(ref _onetRingId, "OnetRingId"); }
      set { Set(ref _onetRingId, value, "OnetRingId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class OnetRing : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 30)]
    private string _name;
    private int _weighting;
    private int _ringMax;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the Weighting entity attribute.</summary>
    public const string WeightingField = "Weighting";
    /// <summary>Identifies the RingMax entity attribute.</summary>
    public const string RingMaxField = "RingMax";


    #endregion
    
    #region Relationships

    [ReverseAssociation("OnetRing")]
    private readonly EntityCollection<OnetWord> _onetWords = new EntityCollection<OnetWord>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<OnetWord> OnetWords
    {
      get { return Get(_onetWords); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int Weighting
    {
      get { return Get(ref _weighting, "Weighting"); }
      set { Set(ref _weighting, value, "Weighting"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int RingMax
    {
      get { return Get(ref _ringMax, "RingMax"); }
      set { Set(ref _ringMax, value, "RingMax"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class OnetPhrase : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 500)]
    private string _phrase;
    private System.Nullable<int> _equivalent;
    [ValidateLength(0, 10)]
    private string _onetSoc;
    private long _onetId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Phrase entity attribute.</summary>
    public const string PhraseField = "Phrase";
    /// <summary>Identifies the Equivalent entity attribute.</summary>
    public const string EquivalentField = "Equivalent";
    /// <summary>Identifies the OnetSoc entity attribute.</summary>
    public const string OnetSocField = "OnetSoc";
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("OnetPhrases")]
    private readonly EntityHolder<Onet> _onet = new EntityHolder<Onet>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Onet Onet
    {
      get { return Get(_onet); }
      set { Set(_onet, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Phrase
    {
      get { return Get(ref _phrase, "Phrase"); }
      set { Set(ref _phrase, value, "Phrase"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> Equivalent
    {
      get { return Get(ref _equivalent, "Equivalent"); }
      set { Set(ref _equivalent, value, "Equivalent"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetSoc
    {
      get { return Get(ref _onetSoc, "OnetSoc"); }
      set { Set(ref _onetSoc, value, "OnetSoc"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Onet" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class PhoneNumber : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 30)]
    private string _number;
    private Focus.Core.PhoneTypes _phoneType;
    private bool _isPrimary;
    [ValidateLength(0, 10)]
    private string _extension;
    private long _personId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Number entity attribute.</summary>
    public const string NumberField = "Number";
    /// <summary>Identifies the PhoneType entity attribute.</summary>
    public const string PhoneTypeField = "PhoneType";
    /// <summary>Identifies the IsPrimary entity attribute.</summary>
    public const string IsPrimaryField = "IsPrimary";
    /// <summary>Identifies the Extension entity attribute.</summary>
    public const string ExtensionField = "Extension";
    /// <summary>Identifies the PersonId entity attribute.</summary>
    public const string PersonIdField = "PersonId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("PhoneNumbers")]
    private readonly EntityHolder<Person> _person = new EntityHolder<Person>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Person Person
    {
      get { return Get(_person); }
      set { Set(_person, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Number
    {
      get { return Get(ref _number, "Number"); }
      set { Set(ref _number, value, "Number"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.PhoneTypes PhoneType
    {
      get { return Get(ref _phoneType, "PhoneType"); }
      set { Set(ref _phoneType, value, "PhoneType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsPrimary
    {
      get { return Get(ref _isPrimary, "IsPrimary"); }
      set { Set(ref _isPrimary, value, "IsPrimary"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Extension
    {
      get { return Get(ref _extension, "Extension"); }
      set { Set(ref _extension, value, "Extension"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Person" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long PersonId
    {
      get { return Get(ref _personId, "PersonId"); }
      set { Set(ref _personId, value, "PersonId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class JobView : Entity<long>
  {
    #region Fields
  
    private long _employerId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _jobTitle;
    private Focus.Core.JobStatuses _jobStatus;
    private int _applicationCount;
    private System.Nullable<System.DateTime> _postedOn;
    private System.Nullable<System.DateTime> _closingOn;
    private System.Nullable<System.DateTime> _heldOn;
    private System.Nullable<System.DateTime> _closedOn;
    [ValidateLength(0, 200)]
    private string _businessUnitName;
    private System.Nullable<long> _employeeId;
    private Focus.Core.ApprovalStatuses _approvalStatus;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _updatedOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the JobTitle entity attribute.</summary>
    public const string JobTitleField = "JobTitle";
    /// <summary>Identifies the JobStatus entity attribute.</summary>
    public const string JobStatusField = "JobStatus";
    /// <summary>Identifies the ApplicationCount entity attribute.</summary>
    public const string ApplicationCountField = "ApplicationCount";
    /// <summary>Identifies the PostedOn entity attribute.</summary>
    public const string PostedOnField = "PostedOn";
    /// <summary>Identifies the ClosingOn entity attribute.</summary>
    public const string ClosingOnField = "ClosingOn";
    /// <summary>Identifies the HeldOn entity attribute.</summary>
    public const string HeldOnField = "HeldOn";
    /// <summary>Identifies the ClosedOn entity attribute.</summary>
    public const string ClosedOnField = "ClosedOn";
    /// <summary>Identifies the BusinessUnitName entity attribute.</summary>
    public const string BusinessUnitNameField = "BusinessUnitName";
    /// <summary>Identifies the EmployeeId entity attribute.</summary>
    public const string EmployeeIdField = "EmployeeId";
    /// <summary>Identifies the ApprovalStatus entity attribute.</summary>
    public const string ApprovalStatusField = "ApprovalStatus";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string JobTitle
    {
      get { return Get(ref _jobTitle, "JobTitle"); }
      set { Set(ref _jobTitle, value, "JobTitle"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.JobStatuses JobStatus
    {
      get { return Get(ref _jobStatus, "JobStatus"); }
      set { Set(ref _jobStatus, value, "JobStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int ApplicationCount
    {
      get { return Get(ref _applicationCount, "ApplicationCount"); }
      set { Set(ref _applicationCount, value, "ApplicationCount"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> PostedOn
    {
      get { return Get(ref _postedOn, "PostedOn"); }
      set { Set(ref _postedOn, value, "PostedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ClosingOn
    {
      get { return Get(ref _closingOn, "ClosingOn"); }
      set { Set(ref _closingOn, value, "ClosingOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> HeldOn
    {
      get { return Get(ref _heldOn, "HeldOn"); }
      set { Set(ref _heldOn, value, "HeldOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ClosedOn
    {
      get { return Get(ref _closedOn, "ClosedOn"); }
      set { Set(ref _closedOn, value, "ClosedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string BusinessUnitName
    {
      get { return Get(ref _businessUnitName, "BusinessUnitName"); }
      set { Set(ref _businessUnitName, value, "BusinessUnitName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EmployeeId
    {
      get { return Get(ref _employeeId, "EmployeeId"); }
      set { Set(ref _employeeId, value, "EmployeeId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ApprovalStatuses ApprovalStatus
    {
      get { return Get(ref _approvalStatus, "ApprovalStatus"); }
      set { Set(ref _approvalStatus, value, "ApprovalStatus"); }
    }
    /// <summary>Gets the time the entity was last updated</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime UpdatedOn
    {
      get { return _updatedOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class Session : Entity<System.Guid>
  {
    #region Fields
  
    private long _userId;
    private System.DateTime _lastRequestOn;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the LastRequestOn entity attribute.</summary>
    public const string LastRequestOnField = "LastRequestOn";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Session")]
    private readonly EntityCollection<SessionState> _sessionStates = new EntityCollection<SessionState>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<SessionState> SessionStates
    {
      get { return Get(_sessionStates); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public long UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime LastRequestOn
    {
      get { return Get(ref _lastRequestOn, "LastRequestOn"); }
      set { Set(ref _lastRequestOn, value, "LastRequestOn"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class SessionState : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 100)]
    [ValidatePresence]
    private string _key;
    private byte[] _value;
    [ValidateLength(0, 200)]
    private string _typeOf;
    private long _size;
    private System.Guid _sessionId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the Value entity attribute.</summary>
    public const string ValueField = "Value";
    /// <summary>Identifies the TypeOf entity attribute.</summary>
    public const string TypeOfField = "TypeOf";
    /// <summary>Identifies the Size entity attribute.</summary>
    public const string SizeField = "Size";
    /// <summary>Identifies the SessionId entity attribute.</summary>
    public const string SessionIdField = "SessionId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("SessionStates")]
    private readonly EntityHolder<Session> _session = new EntityHolder<Session>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Session Session
    {
      get { return Get(_session); }
      set { Set(_session, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public byte[] Value
    {
      get { return Get(ref _value, "Value"); }
      set { Set(ref _value, value, "Value"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string TypeOf
    {
      get { return Get(ref _typeOf, "TypeOf"); }
      set { Set(ref _typeOf, value, "TypeOf"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long Size
    {
      get { return Get(ref _size, "Size"); }
      set { Set(ref _size, value, "Size"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Session" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Guid SessionId
    {
      get { return Get(ref _sessionId, "SessionId"); }
      set { Set(ref _sessionId, value, "SessionId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class Message : Entity<long>
  {
    #region Fields
  
    private bool _isSystemAlert;
    private System.Nullable<long> _employerId;
    private System.DateTime _expiresOn;
    private System.Nullable<Focus.Core.MessageAudiences> _audience;
    private System.Nullable<Focus.Core.MessageTypes> _messageType;
    private System.Nullable<long> _entityId;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the IsSystemAlert entity attribute.</summary>
    public const string IsSystemAlertField = "IsSystemAlert";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the ExpiresOn entity attribute.</summary>
    public const string ExpiresOnField = "ExpiresOn";
    /// <summary>Identifies the Audience entity attribute.</summary>
    public const string AudienceField = "Audience";
    /// <summary>Identifies the MessageType entity attribute.</summary>
    public const string MessageTypeField = "MessageType";
    /// <summary>Identifies the EntityId entity attribute.</summary>
    public const string EntityIdField = "EntityId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Message")]
    private readonly EntityCollection<DismissedMessage> _dismissedMessages = new EntityCollection<DismissedMessage>();
    [ReverseAssociation("Message")]
    private readonly EntityCollection<MessageText> _messageTexts = new EntityCollection<MessageText>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<DismissedMessage> DismissedMessages
    {
      get { return Get(_dismissedMessages); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<MessageText> MessageTexts
    {
      get { return Get(_messageTexts); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsSystemAlert
    {
      get { return Get(ref _isSystemAlert, "IsSystemAlert"); }
      set { Set(ref _isSystemAlert, value, "IsSystemAlert"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ExpiresOn
    {
      get { return Get(ref _expiresOn, "ExpiresOn"); }
      set { Set(ref _expiresOn, value, "ExpiresOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.MessageAudiences> Audience
    {
      get { return Get(ref _audience, "Audience"); }
      set { Set(ref _audience, value, "Audience"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.MessageTypes> MessageType
    {
      get { return Get(ref _messageType, "MessageType"); }
      set { Set(ref _messageType, value, "MessageType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EntityId
    {
      get { return Get(ref _entityId, "EntityId"); }
      set { Set(ref _entityId, value, "EntityId"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class DismissedMessage : Entity<long>
  {
    #region Fields
  
    private long _userId;
    private long _messageId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the MessageId entity attribute.</summary>
    public const string MessageIdField = "MessageId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("DismissedMessages")]
    private readonly EntityHolder<User> _user = new EntityHolder<User>();
    [ReverseAssociation("DismissedMessages")]
    private readonly EntityHolder<Message> _message = new EntityHolder<Message>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public User User
    {
      get { return Get(_user); }
      set { Set(_user, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Message Message
    {
      get { return Get(_message); }
      set { Set(_message, value); }
    }


    /// <summary>Gets or sets the ID for the <see cref="User" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Message" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long MessageId
    {
      get { return Get(ref _messageId, "MessageId"); }
      set { Set(ref _messageId, value, "MessageId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class MessageText : Entity<long>
  {
    #region Fields
  
    private string _text;
    private long _localisationId;
    private long _messageId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Text entity attribute.</summary>
    public const string TextField = "Text";
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";
    /// <summary>Identifies the MessageId entity attribute.</summary>
    public const string MessageIdField = "MessageId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("MessageTexts")]
    private readonly EntityHolder<Message> _message = new EntityHolder<Message>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Message Message
    {
      get { return Get(_message); }
      set { Set(_message, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Text
    {
      get { return Get(ref _text, "Text"); }
      set { Set(ref _text, value, "Text"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Message" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long MessageId
    {
      get { return Get(ref _messageId, "MessageId"); }
      set { Set(ref _messageId, value, "MessageId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class Candidate : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _userName;
    [ValidateLength(0, 50)]
    private string _firstName;
    [ValidateLength(0, 50)]
    private string _lastName;
    [ValidateLength(0, 30)]
    private string _phoneNumber;
    [ValidateLength(0, 200)]
    private string _emailAddress;
    [ValidateLength(0, 36)]
    private string _externalId;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    private readonly System.DateTime _updatedOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the UserName entity attribute.</summary>
    public const string UserNameField = "UserName";
    /// <summary>Identifies the FirstName entity attribute.</summary>
    public const string FirstNameField = "FirstName";
    /// <summary>Identifies the LastName entity attribute.</summary>
    public const string LastNameField = "LastName";
    /// <summary>Identifies the PhoneNumber entity attribute.</summary>
    public const string PhoneNumberField = "PhoneNumber";
    /// <summary>Identifies the EmailAddress entity attribute.</summary>
    public const string EmailAddressField = "EmailAddress";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Candidate")]
    private readonly EntityCollection<CandidateApplication> _candidateApplications = new EntityCollection<CandidateApplication>();
    [ReverseAssociation("Candidate")]
    private readonly EntityCollection<CandidateNote> _candidateNotes = new EntityCollection<CandidateNote>();
    [ReverseAssociation("Candidate")]
    private readonly EntityCollection<EmployeeCandidate> _employeeCandidates = new EntityCollection<EmployeeCandidate>();
    [ReverseAssociation("Candidate")]
    private readonly EntityCollection<PersonListCandidate> _personListCandidates = new EntityCollection<PersonListCandidate>();

    private ThroughAssociation<EmployeeCandidate, Employee> _employees;
    private ThroughAssociation<PersonListCandidate, PersonList> _personLists;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<CandidateApplication> CandidateApplications
    {
      get { return Get(_candidateApplications); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<CandidateNote> CandidateNotes
    {
      get { return Get(_candidateNotes); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<EmployeeCandidate> EmployeeCandidates
    {
      get { return Get(_employeeCandidates); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<PersonListCandidate> PersonListCandidates
    {
      get { return Get(_personListCandidates); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<EmployeeCandidate, Employee> Employees
    {
      get
      {
        if (_employees == null)
        {
          _employees = new ThroughAssociation<EmployeeCandidate, Employee>(_employeeCandidates);
        }
        return Get(_employees);
      }
    }
    
    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<PersonListCandidate, PersonList> PersonLists
    {
      get
      {
        if (_personLists == null)
        {
          _personLists = new ThroughAssociation<PersonListCandidate, PersonList>(_personListCandidates);
        }
        return Get(_personLists);
      }
    }
    

    [System.Diagnostics.DebuggerNonUserCode]
    public string UserName
    {
      get { return Get(ref _userName, "UserName"); }
      set { Set(ref _userName, value, "UserName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string FirstName
    {
      get { return Get(ref _firstName, "FirstName"); }
      set { Set(ref _firstName, value, "FirstName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LastName
    {
      get { return Get(ref _lastName, "LastName"); }
      set { Set(ref _lastName, value, "LastName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PhoneNumber
    {
      get { return Get(ref _phoneNumber, "PhoneNumber"); }
      set { Set(ref _phoneNumber, value, "PhoneNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmailAddress
    {
      get { return Get(ref _emailAddress, "EmailAddress"); }
      set { Set(ref _emailAddress, value, "EmailAddress"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }
    }

    /// <summary>Gets the time the entity was last updated</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime UpdatedOn
    {
      get { return _updatedOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class EmployeeSearchView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _firstName;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _lastName;
    private long _employerId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _employerName;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _industrialClassification;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _federalEmployerIdentificationNumber;
    private System.Nullable<Focus.Core.WorkOpportunitiesTaxCreditCategories> _workOpportunitiesTaxCreditHires;
    private System.Nullable<Focus.Core.JobPostingFlags> _postingFlags;
    private System.Nullable<Focus.Core.ScreeningPreferences> _screeningPreferences;
    [ValidateLength(0, 200)]
    private string _jobTitle;
    private System.Nullable<System.DateTime> _employerCreatedOn;
    [ValidateEmailAddress]
    [ValidateLength(0, 200)]
    private string _emailAddress;
    [ValidateLength(0, 30)]
    private string _phoneNumber;
    private bool _accountEnabled;
    private Focus.Core.ApprovalStatuses _employerApprovalStatus;
    private Focus.Core.ApprovalStatuses _employeeApprovalStatus;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the FirstName entity attribute.</summary>
    public const string FirstNameField = "FirstName";
    /// <summary>Identifies the LastName entity attribute.</summary>
    public const string LastNameField = "LastName";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the EmployerName entity attribute.</summary>
    public const string EmployerNameField = "EmployerName";
    /// <summary>Identifies the IndustrialClassification entity attribute.</summary>
    public const string IndustrialClassificationField = "IndustrialClassification";
    /// <summary>Identifies the FederalEmployerIdentificationNumber entity attribute.</summary>
    public const string FederalEmployerIdentificationNumberField = "FederalEmployerIdentificationNumber";
    /// <summary>Identifies the WorkOpportunitiesTaxCreditHires entity attribute.</summary>
    public const string WorkOpportunitiesTaxCreditHiresField = "WorkOpportunitiesTaxCreditHires";
    /// <summary>Identifies the PostingFlags entity attribute.</summary>
    public const string PostingFlagsField = "PostingFlags";
    /// <summary>Identifies the ScreeningPreferences entity attribute.</summary>
    public const string ScreeningPreferencesField = "ScreeningPreferences";
    /// <summary>Identifies the JobTitle entity attribute.</summary>
    public const string JobTitleField = "JobTitle";
    /// <summary>Identifies the EmployerCreatedOn entity attribute.</summary>
    public const string EmployerCreatedOnField = "EmployerCreatedOn";
    /// <summary>Identifies the EmailAddress entity attribute.</summary>
    public const string EmailAddressField = "EmailAddress";
    /// <summary>Identifies the PhoneNumber entity attribute.</summary>
    public const string PhoneNumberField = "PhoneNumber";
    /// <summary>Identifies the AccountEnabled entity attribute.</summary>
    public const string AccountEnabledField = "AccountEnabled";
    /// <summary>Identifies the EmployerApprovalStatus entity attribute.</summary>
    public const string EmployerApprovalStatusField = "EmployerApprovalStatus";
    /// <summary>Identifies the EmployeeApprovalStatus entity attribute.</summary>
    public const string EmployeeApprovalStatusField = "EmployeeApprovalStatus";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string FirstName
    {
      get { return Get(ref _firstName, "FirstName"); }
      set { Set(ref _firstName, value, "FirstName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LastName
    {
      get { return Get(ref _lastName, "LastName"); }
      set { Set(ref _lastName, value, "LastName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerName
    {
      get { return Get(ref _employerName, "EmployerName"); }
      set { Set(ref _employerName, value, "EmployerName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string IndustrialClassification
    {
      get { return Get(ref _industrialClassification, "IndustrialClassification"); }
      set { Set(ref _industrialClassification, value, "IndustrialClassification"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string FederalEmployerIdentificationNumber
    {
      get { return Get(ref _federalEmployerIdentificationNumber, "FederalEmployerIdentificationNumber"); }
      set { Set(ref _federalEmployerIdentificationNumber, value, "FederalEmployerIdentificationNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.WorkOpportunitiesTaxCreditCategories> WorkOpportunitiesTaxCreditHires
    {
      get { return Get(ref _workOpportunitiesTaxCreditHires, "WorkOpportunitiesTaxCreditHires"); }
      set { Set(ref _workOpportunitiesTaxCreditHires, value, "WorkOpportunitiesTaxCreditHires"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.JobPostingFlags> PostingFlags
    {
      get { return Get(ref _postingFlags, "PostingFlags"); }
      set { Set(ref _postingFlags, value, "PostingFlags"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Core.ScreeningPreferences> ScreeningPreferences
    {
      get { return Get(ref _screeningPreferences, "ScreeningPreferences"); }
      set { Set(ref _screeningPreferences, value, "ScreeningPreferences"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string JobTitle
    {
      get { return Get(ref _jobTitle, "JobTitle"); }
      set { Set(ref _jobTitle, value, "JobTitle"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> EmployerCreatedOn
    {
      get { return Get(ref _employerCreatedOn, "EmployerCreatedOn"); }
      set { Set(ref _employerCreatedOn, value, "EmployerCreatedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmailAddress
    {
      get { return Get(ref _emailAddress, "EmailAddress"); }
      set { Set(ref _emailAddress, value, "EmailAddress"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PhoneNumber
    {
      get { return Get(ref _phoneNumber, "PhoneNumber"); }
      set { Set(ref _phoneNumber, value, "PhoneNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool AccountEnabled
    {
      get { return Get(ref _accountEnabled, "AccountEnabled"); }
      set { Set(ref _accountEnabled, value, "AccountEnabled"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ApprovalStatuses EmployerApprovalStatus
    {
      get { return Get(ref _employerApprovalStatus, "EmployerApprovalStatus"); }
      set { Set(ref _employerApprovalStatus, value, "EmployerApprovalStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ApprovalStatuses EmployeeApprovalStatus
    {
      get { return Get(ref _employeeApprovalStatus, "EmployeeApprovalStatus"); }
      set { Set(ref _employeeApprovalStatus, value, "EmployeeApprovalStatus"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class SavedMessage : Entity<long>
  {
    #region Fields
  
    private Focus.Core.MessageAudiences _audience;
    [ValidateLength(0, 100)]
    private string _name;
    private long _userId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Audience entity attribute.</summary>
    public const string AudienceField = "Audience";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("SavedMessage")]
    private readonly EntityCollection<SavedMessageText> _savedMessageTexts = new EntityCollection<SavedMessageText>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<SavedMessageText> SavedMessageTexts
    {
      get { return Get(_savedMessageTexts); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.MessageAudiences Audience
    {
      get { return Get(ref _audience, "Audience"); }
      set { Set(ref _audience, value, "Audience"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class SavedMessageText : Entity<long>
  {
    #region Fields
  
    private long _localisationId;
    [ValidatePresence]
    private string _text;
    private long _savedMessageId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the LocalisationId entity attribute.</summary>
    public const string LocalisationIdField = "LocalisationId";
    /// <summary>Identifies the Text entity attribute.</summary>
    public const string TextField = "Text";
    /// <summary>Identifies the SavedMessageId entity attribute.</summary>
    public const string SavedMessageIdField = "SavedMessageId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("SavedMessageTexts")]
    private readonly EntityHolder<SavedMessage> _savedMessage = new EntityHolder<SavedMessage>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public SavedMessage SavedMessage
    {
      get { return Get(_savedMessage); }
      set { Set(_savedMessage, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public long LocalisationId
    {
      get { return Get(ref _localisationId, "LocalisationId"); }
      set { Set(ref _localisationId, value, "LocalisationId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Text
    {
      get { return Get(ref _text, "Text"); }
      set { Set(ref _text, value, "Text"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="SavedMessage" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long SavedMessageId
    {
      get { return Get(ref _savedMessageId, "SavedMessageId"); }
      set { Set(ref _savedMessageId, value, "SavedMessageId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class JobSeekerReferralView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 101)]
    private string _name;
    private System.DateTime _referralDate;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _jobTitle;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _employerName;
    private System.Nullable<int> _timeInQueue;
    private long _employerId;
    private string _posting;
    private long _candidateId;
    private long _jobId;
    [ValidateLength(0, 36)]
    private string _candidateExternalId;
    [ValidateLength(0, 255)]
    private string _approvalRequiredReason;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the ReferralDate entity attribute.</summary>
    public const string ReferralDateField = "ReferralDate";
    /// <summary>Identifies the JobTitle entity attribute.</summary>
    public const string JobTitleField = "JobTitle";
    /// <summary>Identifies the EmployerName entity attribute.</summary>
    public const string EmployerNameField = "EmployerName";
    /// <summary>Identifies the TimeInQueue entity attribute.</summary>
    public const string TimeInQueueField = "TimeInQueue";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the Posting entity attribute.</summary>
    public const string PostingField = "Posting";
    /// <summary>Identifies the CandidateId entity attribute.</summary>
    public const string CandidateIdField = "CandidateId";
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the CandidateExternalId entity attribute.</summary>
    public const string CandidateExternalIdField = "CandidateExternalId";
    /// <summary>Identifies the ApprovalRequiredReason entity attribute.</summary>
    public const string ApprovalRequiredReasonField = "ApprovalRequiredReason";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ReferralDate
    {
      get { return Get(ref _referralDate, "ReferralDate"); }
      set { Set(ref _referralDate, value, "ReferralDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string JobTitle
    {
      get { return Get(ref _jobTitle, "JobTitle"); }
      set { Set(ref _jobTitle, value, "JobTitle"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerName
    {
      get { return Get(ref _employerName, "EmployerName"); }
      set { Set(ref _employerName, value, "EmployerName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> TimeInQueue
    {
      get { return Get(ref _timeInQueue, "TimeInQueue"); }
      set { Set(ref _timeInQueue, value, "TimeInQueue"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Posting
    {
      get { return Get(ref _posting, "Posting"); }
      set { Set(ref _posting, value, "Posting"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long CandidateId
    {
      get { return Get(ref _candidateId, "CandidateId"); }
      set { Set(ref _candidateId, value, "CandidateId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CandidateExternalId
    {
      get { return Get(ref _candidateExternalId, "CandidateExternalId"); }
      set { Set(ref _candidateExternalId, value, "CandidateExternalId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ApprovalRequiredReason
    {
      get { return Get(ref _approvalRequiredReason, "ApprovalRequiredReason"); }
      set { Set(ref _approvalRequiredReason, value, "ApprovalRequiredReason"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class EmployerAccountReferralView : Entity<long>
  {
    #region Fields
  
    private long _employerId;
    private long _employeeId;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _employeeFirstName;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _employeeLastName;
    private System.DateTime _accountCreationDate;
    private System.Nullable<int> _timeInQueue;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _employerName;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _employerIndustrialClassification;
    [ValidatePresence]
    [ValidateLength(0, 20)]
    private string _employerPhoneNumber;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _employerFederalEmployerIdentificationNumber;
    [ValidateLength(0, 11)]
    private string _employerStateEmployerIdentificationNumber;
    [ValidateLength(0, 20)]
    private string _employerFaxNumber;
    [ValidateLength(0, 1000)]
    private string _employerUrl;
    [ValidateLength(0, 200)]
    private string _employeeAddressLine1;
    [ValidateLength(0, 100)]
    private string _employeeAddressTownCity;
    private System.Nullable<long> _employeeAddressStateId;
    [ValidateLength(0, 20)]
    private string _employeeAddressPostcodeZip;
    [ValidateLength(0, 200)]
    private string _employeeEmailAddress;
    [ValidateLength(0, 30)]
    private string _employeePhoneNumber;
    [ValidateLength(0, 30)]
    private string _employeeFaxNumber;
    [ValidateLength(0, 200)]
    private string _employerAddressLine1;
    [ValidateLength(0, 100)]
    private string _employerAddressTownCity;
    private System.Nullable<long> _employerAddressStateId;
    [ValidateLength(0, 20)]
    private string _employerAddressPostcodeZip;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the EmployeeId entity attribute.</summary>
    public const string EmployeeIdField = "EmployeeId";
    /// <summary>Identifies the EmployeeFirstName entity attribute.</summary>
    public const string EmployeeFirstNameField = "EmployeeFirstName";
    /// <summary>Identifies the EmployeeLastName entity attribute.</summary>
    public const string EmployeeLastNameField = "EmployeeLastName";
    /// <summary>Identifies the AccountCreationDate entity attribute.</summary>
    public const string AccountCreationDateField = "AccountCreationDate";
    /// <summary>Identifies the TimeInQueue entity attribute.</summary>
    public const string TimeInQueueField = "TimeInQueue";
    /// <summary>Identifies the EmployerName entity attribute.</summary>
    public const string EmployerNameField = "EmployerName";
    /// <summary>Identifies the EmployerIndustrialClassification entity attribute.</summary>
    public const string EmployerIndustrialClassificationField = "EmployerIndustrialClassification";
    /// <summary>Identifies the EmployerPhoneNumber entity attribute.</summary>
    public const string EmployerPhoneNumberField = "EmployerPhoneNumber";
    /// <summary>Identifies the EmployerFederalEmployerIdentificationNumber entity attribute.</summary>
    public const string EmployerFederalEmployerIdentificationNumberField = "EmployerFederalEmployerIdentificationNumber";
    /// <summary>Identifies the EmployerStateEmployerIdentificationNumber entity attribute.</summary>
    public const string EmployerStateEmployerIdentificationNumberField = "EmployerStateEmployerIdentificationNumber";
    /// <summary>Identifies the EmployerFaxNumber entity attribute.</summary>
    public const string EmployerFaxNumberField = "EmployerFaxNumber";
    /// <summary>Identifies the EmployerUrl entity attribute.</summary>
    public const string EmployerUrlField = "EmployerUrl";
    /// <summary>Identifies the EmployeeAddressLine1 entity attribute.</summary>
    public const string EmployeeAddressLine1Field = "EmployeeAddressLine1";
    /// <summary>Identifies the EmployeeAddressTownCity entity attribute.</summary>
    public const string EmployeeAddressTownCityField = "EmployeeAddressTownCity";
    /// <summary>Identifies the EmployeeAddressStateId entity attribute.</summary>
    public const string EmployeeAddressStateIdField = "EmployeeAddressStateId";
    /// <summary>Identifies the EmployeeAddressPostcodeZip entity attribute.</summary>
    public const string EmployeeAddressPostcodeZipField = "EmployeeAddressPostcodeZip";
    /// <summary>Identifies the EmployeeEmailAddress entity attribute.</summary>
    public const string EmployeeEmailAddressField = "EmployeeEmailAddress";
    /// <summary>Identifies the EmployeePhoneNumber entity attribute.</summary>
    public const string EmployeePhoneNumberField = "EmployeePhoneNumber";
    /// <summary>Identifies the EmployeeFaxNumber entity attribute.</summary>
    public const string EmployeeFaxNumberField = "EmployeeFaxNumber";
    /// <summary>Identifies the EmployerAddressLine1 entity attribute.</summary>
    public const string EmployerAddressLine1Field = "EmployerAddressLine1";
    /// <summary>Identifies the EmployerAddressTownCity entity attribute.</summary>
    public const string EmployerAddressTownCityField = "EmployerAddressTownCity";
    /// <summary>Identifies the EmployerAddressStateId entity attribute.</summary>
    public const string EmployerAddressStateIdField = "EmployerAddressStateId";
    /// <summary>Identifies the EmployerAddressPostcodeZip entity attribute.</summary>
    public const string EmployerAddressPostcodeZipField = "EmployerAddressPostcodeZip";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployeeId
    {
      get { return Get(ref _employeeId, "EmployeeId"); }
      set { Set(ref _employeeId, value, "EmployeeId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployeeFirstName
    {
      get { return Get(ref _employeeFirstName, "EmployeeFirstName"); }
      set { Set(ref _employeeFirstName, value, "EmployeeFirstName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployeeLastName
    {
      get { return Get(ref _employeeLastName, "EmployeeLastName"); }
      set { Set(ref _employeeLastName, value, "EmployeeLastName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime AccountCreationDate
    {
      get { return Get(ref _accountCreationDate, "AccountCreationDate"); }
      set { Set(ref _accountCreationDate, value, "AccountCreationDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> TimeInQueue
    {
      get { return Get(ref _timeInQueue, "TimeInQueue"); }
      set { Set(ref _timeInQueue, value, "TimeInQueue"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerName
    {
      get { return Get(ref _employerName, "EmployerName"); }
      set { Set(ref _employerName, value, "EmployerName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerIndustrialClassification
    {
      get { return Get(ref _employerIndustrialClassification, "EmployerIndustrialClassification"); }
      set { Set(ref _employerIndustrialClassification, value, "EmployerIndustrialClassification"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerPhoneNumber
    {
      get { return Get(ref _employerPhoneNumber, "EmployerPhoneNumber"); }
      set { Set(ref _employerPhoneNumber, value, "EmployerPhoneNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerFederalEmployerIdentificationNumber
    {
      get { return Get(ref _employerFederalEmployerIdentificationNumber, "EmployerFederalEmployerIdentificationNumber"); }
      set { Set(ref _employerFederalEmployerIdentificationNumber, value, "EmployerFederalEmployerIdentificationNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerStateEmployerIdentificationNumber
    {
      get { return Get(ref _employerStateEmployerIdentificationNumber, "EmployerStateEmployerIdentificationNumber"); }
      set { Set(ref _employerStateEmployerIdentificationNumber, value, "EmployerStateEmployerIdentificationNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerFaxNumber
    {
      get { return Get(ref _employerFaxNumber, "EmployerFaxNumber"); }
      set { Set(ref _employerFaxNumber, value, "EmployerFaxNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerUrl
    {
      get { return Get(ref _employerUrl, "EmployerUrl"); }
      set { Set(ref _employerUrl, value, "EmployerUrl"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployeeAddressLine1
    {
      get { return Get(ref _employeeAddressLine1, "EmployeeAddressLine1"); }
      set { Set(ref _employeeAddressLine1, value, "EmployeeAddressLine1"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployeeAddressTownCity
    {
      get { return Get(ref _employeeAddressTownCity, "EmployeeAddressTownCity"); }
      set { Set(ref _employeeAddressTownCity, value, "EmployeeAddressTownCity"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EmployeeAddressStateId
    {
      get { return Get(ref _employeeAddressStateId, "EmployeeAddressStateId"); }
      set { Set(ref _employeeAddressStateId, value, "EmployeeAddressStateId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployeeAddressPostcodeZip
    {
      get { return Get(ref _employeeAddressPostcodeZip, "EmployeeAddressPostcodeZip"); }
      set { Set(ref _employeeAddressPostcodeZip, value, "EmployeeAddressPostcodeZip"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployeeEmailAddress
    {
      get { return Get(ref _employeeEmailAddress, "EmployeeEmailAddress"); }
      set { Set(ref _employeeEmailAddress, value, "EmployeeEmailAddress"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployeePhoneNumber
    {
      get { return Get(ref _employeePhoneNumber, "EmployeePhoneNumber"); }
      set { Set(ref _employeePhoneNumber, value, "EmployeePhoneNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployeeFaxNumber
    {
      get { return Get(ref _employeeFaxNumber, "EmployeeFaxNumber"); }
      set { Set(ref _employeeFaxNumber, value, "EmployeeFaxNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerAddressLine1
    {
      get { return Get(ref _employerAddressLine1, "EmployerAddressLine1"); }
      set { Set(ref _employerAddressLine1, value, "EmployerAddressLine1"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerAddressTownCity
    {
      get { return Get(ref _employerAddressTownCity, "EmployerAddressTownCity"); }
      set { Set(ref _employerAddressTownCity, value, "EmployerAddressTownCity"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EmployerAddressStateId
    {
      get { return Get(ref _employerAddressStateId, "EmployerAddressStateId"); }
      set { Set(ref _employerAddressStateId, value, "EmployerAddressStateId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerAddressPostcodeZip
    {
      get { return Get(ref _employerAddressPostcodeZip, "EmployerAddressPostcodeZip"); }
      set { Set(ref _employerAddressPostcodeZip, value, "EmployerAddressPostcodeZip"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class JobPostingReferralView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _jobTitle;
    private long _employerId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _employerName;
    private System.Nullable<int> _timeInQueue;
    [ValidateLength(0, 50)]
    private string _employeeFirstName;
    [ValidateLength(0, 50)]
    private string _employeeLastName;
    private System.Nullable<long> _employeeId;
    private System.Nullable<System.DateTime> _awaitingApprovalOn;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobTitle entity attribute.</summary>
    public const string JobTitleField = "JobTitle";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the EmployerName entity attribute.</summary>
    public const string EmployerNameField = "EmployerName";
    /// <summary>Identifies the TimeInQueue entity attribute.</summary>
    public const string TimeInQueueField = "TimeInQueue";
    /// <summary>Identifies the EmployeeFirstName entity attribute.</summary>
    public const string EmployeeFirstNameField = "EmployeeFirstName";
    /// <summary>Identifies the EmployeeLastName entity attribute.</summary>
    public const string EmployeeLastNameField = "EmployeeLastName";
    /// <summary>Identifies the EmployeeId entity attribute.</summary>
    public const string EmployeeIdField = "EmployeeId";
    /// <summary>Identifies the AwaitingApprovalOn entity attribute.</summary>
    public const string AwaitingApprovalOnField = "AwaitingApprovalOn";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string JobTitle
    {
      get { return Get(ref _jobTitle, "JobTitle"); }
      set { Set(ref _jobTitle, value, "JobTitle"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployerName
    {
      get { return Get(ref _employerName, "EmployerName"); }
      set { Set(ref _employerName, value, "EmployerName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> TimeInQueue
    {
      get { return Get(ref _timeInQueue, "TimeInQueue"); }
      set { Set(ref _timeInQueue, value, "TimeInQueue"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployeeFirstName
    {
      get { return Get(ref _employeeFirstName, "EmployeeFirstName"); }
      set { Set(ref _employeeFirstName, value, "EmployeeFirstName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmployeeLastName
    {
      get { return Get(ref _employeeLastName, "EmployeeLastName"); }
      set { Set(ref _employeeLastName, value, "EmployeeLastName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EmployeeId
    {
      get { return Get(ref _employeeId, "EmployeeId"); }
      set { Set(ref _employeeId, value, "EmployeeId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> AwaitingApprovalOn
    {
      get { return Get(ref _awaitingApprovalOn, "AwaitingApprovalOn"); }
      set { Set(ref _awaitingApprovalOn, value, "AwaitingApprovalOn"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class PersonList : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 100)]
    private string _name;
    private Focus.Core.ListTypes _listType;
    private long _personId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the ListType entity attribute.</summary>
    public const string ListTypeField = "ListType";
    /// <summary>Identifies the PersonId entity attribute.</summary>
    public const string PersonIdField = "PersonId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("PersonLists")]
    private readonly EntityHolder<Person> _person = new EntityHolder<Person>();
    [ReverseAssociation("PersonList")]
    private readonly EntityCollection<PersonListCandidate> _personListCandidates = new EntityCollection<PersonListCandidate>();

    private ThroughAssociation<PersonListCandidate, Candidate> _candidates;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Person Person
    {
      get { return Get(_person); }
      set { Set(_person, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<PersonListCandidate> PersonListCandidates
    {
      get { return Get(_personListCandidates); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<PersonListCandidate, Candidate> Candidates
    {
      get
      {
        if (_candidates == null)
        {
          _candidates = new ThroughAssociation<PersonListCandidate, Candidate>(_personListCandidates);
        }
        return Get(_candidates);
      }
    }
    

    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ListTypes ListType
    {
      get { return Get(ref _listType, "ListType"); }
      set { Set(ref _listType, value, "ListType"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Person" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long PersonId
    {
      get { return Get(ref _personId, "PersonId"); }
      set { Set(ref _personId, value, "PersonId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class ApplicationImage : Entity<long>
  {
    #region Fields
  
    private Focus.Core.ApplicationImageTypes _type;
    private byte[] _image;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Type entity attribute.</summary>
    public const string TypeField = "Type";
    /// <summary>Identifies the Image entity attribute.</summary>
    public const string ImageField = "Image";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ApplicationImageTypes Type
    {
      get { return Get(ref _type, "Type"); }
      set { Set(ref _type, value, "Type"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public byte[] Image
    {
      get { return Get(ref _image, "Image"); }
      set { Set(ref _image, value, "Image"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class StaffSearchView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _firstName;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _lastName;
    private bool _enabled;
    [ValidateEmailAddress]
    [ValidateLength(0, 200)]
    private string _emailAddress;
    [ValidateLength(0, 200)]
    private string _office;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the FirstName entity attribute.</summary>
    public const string FirstNameField = "FirstName";
    /// <summary>Identifies the LastName entity attribute.</summary>
    public const string LastNameField = "LastName";
    /// <summary>Identifies the Enabled entity attribute.</summary>
    public const string EnabledField = "Enabled";
    /// <summary>Identifies the EmailAddress entity attribute.</summary>
    public const string EmailAddressField = "EmailAddress";
    /// <summary>Identifies the Office entity attribute.</summary>
    public const string OfficeField = "Office";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string FirstName
    {
      get { return Get(ref _firstName, "FirstName"); }
      set { Set(ref _firstName, value, "FirstName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LastName
    {
      get { return Get(ref _lastName, "LastName"); }
      set { Set(ref _lastName, value, "LastName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool Enabled
    {
      get { return Get(ref _enabled, "Enabled"); }
      set { Set(ref _enabled, value, "Enabled"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmailAddress
    {
      get { return Get(ref _emailAddress, "EmailAddress"); }
      set { Set(ref _emailAddress, value, "EmailAddress"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Office
    {
      get { return Get(ref _office, "Office"); }
      set { Set(ref _office, value, "Office"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table(IdentityMethod=IdentityMethod.Guid)]
  public partial class SingleSignOn : Entity<System.Guid>
  {
    #region Fields
  
    private long _userId;
    private long _actionerId;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the ActionerId entity attribute.</summary>
    public const string ActionerIdField = "ActionerId";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public long UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long ActionerId
    {
      get { return Get(ref _actionerId, "ActionerId"); }
      set { Set(ref _actionerId, value, "ActionerId"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class JobTaskView : Entity<long>
  {
    #region Fields
  
    private Focus.Core.JobTaskTypes _jobTaskType;
    private long _onetId;
    [ValidatePresence]
    [ValidateLength(0, 5)]
    private string _culture;
    [ValidatePresence]
    private string _prompt;
    private string _response;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobTaskType entity attribute.</summary>
    public const string JobTaskTypeField = "JobTaskType";
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";
    /// <summary>Identifies the Culture entity attribute.</summary>
    public const string CultureField = "Culture";
    /// <summary>Identifies the Prompt entity attribute.</summary>
    public const string PromptField = "Prompt";
    /// <summary>Identifies the Response entity attribute.</summary>
    public const string ResponseField = "Response";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.JobTaskTypes JobTaskType
    {
      get { return Get(ref _jobTaskType, "JobTaskType"); }
      set { Set(ref _jobTaskType, value, "JobTaskType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Culture
    {
      get { return Get(ref _culture, "Culture"); }
      set { Set(ref _culture, value, "Culture"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Prompt
    {
      get { return Get(ref _prompt, "Prompt"); }
      set { Set(ref _prompt, value, "Prompt"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Response
    {
      get { return Get(ref _response, "Response"); }
      set { Set(ref _response, value, "Response"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class JobTaskMultiOptionView : Entity<long>
  {
    #region Fields
  
    private long _onetId;
    [ValidatePresence]
    [ValidateLength(0, 5)]
    private string _culture;
    [ValidatePresence]
    private string _prompt;
    private long _jobTaskId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";
    /// <summary>Identifies the Culture entity attribute.</summary>
    public const string CultureField = "Culture";
    /// <summary>Identifies the Prompt entity attribute.</summary>
    public const string PromptField = "Prompt";
    /// <summary>Identifies the JobTaskId entity attribute.</summary>
    public const string JobTaskIdField = "JobTaskId";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Culture
    {
      get { return Get(ref _culture, "Culture"); }
      set { Set(ref _culture, value, "Culture"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Prompt
    {
      get { return Get(ref _prompt, "Prompt"); }
      set { Set(ref _prompt, value, "Prompt"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long JobTaskId
    {
      get { return Get(ref _jobTaskId, "JobTaskId"); }
      set { Set(ref _jobTaskId, value, "JobTaskId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class OnetView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _key;
    [ValidatePresence]
    private string _occupation;
    private long _jobFamilyId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _jobFamilyKey;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _onetCode;
    [ValidatePresence]
    [ValidateLength(0, 5)]
    private string _culture;
    private string _description;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the Occupation entity attribute.</summary>
    public const string OccupationField = "Occupation";
    /// <summary>Identifies the JobFamilyId entity attribute.</summary>
    public const string JobFamilyIdField = "JobFamilyId";
    /// <summary>Identifies the JobFamilyKey entity attribute.</summary>
    public const string JobFamilyKeyField = "JobFamilyKey";
    /// <summary>Identifies the OnetCode entity attribute.</summary>
    public const string OnetCodeField = "OnetCode";
    /// <summary>Identifies the Culture entity attribute.</summary>
    public const string CultureField = "Culture";
    /// <summary>Identifies the Description entity attribute.</summary>
    public const string DescriptionField = "Description";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Occupation
    {
      get { return Get(ref _occupation, "Occupation"); }
      set { Set(ref _occupation, value, "Occupation"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long JobFamilyId
    {
      get { return Get(ref _jobFamilyId, "JobFamilyId"); }
      set { Set(ref _jobFamilyId, value, "JobFamilyId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string JobFamilyKey
    {
      get { return Get(ref _jobFamilyKey, "JobFamilyKey"); }
      set { Set(ref _jobFamilyKey, value, "JobFamilyKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetCode
    {
      get { return Get(ref _onetCode, "OnetCode"); }
      set { Set(ref _onetCode, value, "OnetCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Culture
    {
      get { return Get(ref _culture, "Culture"); }
      set { Set(ref _culture, value, "Culture"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Description
    {
      get { return Get(ref _description, "Description"); }
      set { Set(ref _description, value, "Description"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class CandidateNote : Entity<long>
  {
    #region Fields
  
    private string _note;
    private long _employerId;
    private long _candidateId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Note entity attribute.</summary>
    public const string NoteField = "Note";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the CandidateId entity attribute.</summary>
    public const string CandidateIdField = "CandidateId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("CandidateNotes")]
    private readonly EntityHolder<Employer> _employer = new EntityHolder<Employer>();
    [ReverseAssociation("CandidateNotes")]
    private readonly EntityHolder<Candidate> _candidate = new EntityHolder<Candidate>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Employer Employer
    {
      get { return Get(_employer); }
      set { Set(_employer, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Candidate Candidate
    {
      get { return Get(_candidate); }
      set { Set(_candidate, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Note
    {
      get { return Get(ref _note, "Note"); }
      set { Set(ref _note, value, "Note"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Employer" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Candidate" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long CandidateId
    {
      get { return Get(ref _candidateId, "CandidateId"); }
      set { Set(ref _candidateId, value, "CandidateId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table(IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class NAICS : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 6)]
    private string _code;
    [ValidateLength(0, 200)]
    private string _name;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Code entity attribute.</summary>
    public const string CodeField = "Code";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Code
    {
      get { return Get(ref _code, "Code"); }
      set { Set(ref _code, value, "Code"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class ApplicationView : Entity<long>
  {
    #region Fields
  
    private long _jobId;
    private long _employerId;
    private long _candidateId;
    private Focus.Core.ApplicationStatusTypes _candidateApplicationStatus;
    private int _candidateApplicationScore;
    [ValidateLength(0, 36)]
    private string _candidateExternalId;
    private System.DateTime _candidateApplicationReceivedOn;
    private Focus.Core.ApprovalStatuses _candidateApplicationApprovalStatus;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _candidateFirstName;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _candidateLastName;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the CandidateId entity attribute.</summary>
    public const string CandidateIdField = "CandidateId";
    /// <summary>Identifies the CandidateApplicationStatus entity attribute.</summary>
    public const string CandidateApplicationStatusField = "CandidateApplicationStatus";
    /// <summary>Identifies the CandidateApplicationScore entity attribute.</summary>
    public const string CandidateApplicationScoreField = "CandidateApplicationScore";
    /// <summary>Identifies the CandidateExternalId entity attribute.</summary>
    public const string CandidateExternalIdField = "CandidateExternalId";
    /// <summary>Identifies the CandidateApplicationReceivedOn entity attribute.</summary>
    public const string CandidateApplicationReceivedOnField = "CandidateApplicationReceivedOn";
    /// <summary>Identifies the CandidateApplicationApprovalStatus entity attribute.</summary>
    public const string CandidateApplicationApprovalStatusField = "CandidateApplicationApprovalStatus";
    /// <summary>Identifies the CandidateFirstName entity attribute.</summary>
    public const string CandidateFirstNameField = "CandidateFirstName";
    /// <summary>Identifies the CandidateLastName entity attribute.</summary>
    public const string CandidateLastNameField = "CandidateLastName";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long CandidateId
    {
      get { return Get(ref _candidateId, "CandidateId"); }
      set { Set(ref _candidateId, value, "CandidateId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ApplicationStatusTypes CandidateApplicationStatus
    {
      get { return Get(ref _candidateApplicationStatus, "CandidateApplicationStatus"); }
      set { Set(ref _candidateApplicationStatus, value, "CandidateApplicationStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int CandidateApplicationScore
    {
      get { return Get(ref _candidateApplicationScore, "CandidateApplicationScore"); }
      set { Set(ref _candidateApplicationScore, value, "CandidateApplicationScore"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CandidateExternalId
    {
      get { return Get(ref _candidateExternalId, "CandidateExternalId"); }
      set { Set(ref _candidateExternalId, value, "CandidateExternalId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CandidateApplicationReceivedOn
    {
      get { return Get(ref _candidateApplicationReceivedOn, "CandidateApplicationReceivedOn"); }
      set { Set(ref _candidateApplicationReceivedOn, value, "CandidateApplicationReceivedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ApprovalStatuses CandidateApplicationApprovalStatus
    {
      get { return Get(ref _candidateApplicationApprovalStatus, "CandidateApplicationApprovalStatus"); }
      set { Set(ref _candidateApplicationApprovalStatus, value, "CandidateApplicationApprovalStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CandidateFirstName
    {
      get { return Get(ref _candidateFirstName, "CandidateFirstName"); }
      set { Set(ref _candidateFirstName, value, "CandidateFirstName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CandidateLastName
    {
      get { return Get(ref _candidateLastName, "CandidateLastName"); }
      set { Set(ref _candidateLastName, value, "CandidateLastName"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class LookupItemsView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _key;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _lookupType;
    [ValidatePresence]
    private string _value;
    private int _displayOrder;
    private long _parentId;
    [ValidateLength(0, 200)]
    private string _parentKey;
    [ValidatePresence]
    [ValidateLength(0, 5)]
    private string _culture;
    [ValidateLength(0, 36)]
    private string _externalId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Key entity attribute.</summary>
    public const string KeyField = "Key";
    /// <summary>Identifies the LookupType entity attribute.</summary>
    public const string LookupTypeField = "LookupType";
    /// <summary>Identifies the Value entity attribute.</summary>
    public const string ValueField = "Value";
    /// <summary>Identifies the DisplayOrder entity attribute.</summary>
    public const string DisplayOrderField = "DisplayOrder";
    /// <summary>Identifies the ParentId entity attribute.</summary>
    public const string ParentIdField = "ParentId";
    /// <summary>Identifies the ParentKey entity attribute.</summary>
    public const string ParentKeyField = "ParentKey";
    /// <summary>Identifies the Culture entity attribute.</summary>
    public const string CultureField = "Culture";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Key
    {
      get { return Get(ref _key, "Key"); }
      set { Set(ref _key, value, "Key"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LookupType
    {
      get { return Get(ref _lookupType, "LookupType"); }
      set { Set(ref _lookupType, value, "LookupType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Value
    {
      get { return Get(ref _value, "Value"); }
      set { Set(ref _value, value, "Value"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int DisplayOrder
    {
      get { return Get(ref _displayOrder, "DisplayOrder"); }
      set { Set(ref _displayOrder, value, "DisplayOrder"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long ParentId
    {
      get { return Get(ref _parentId, "ParentId"); }
      set { Set(ref _parentId, value, "ParentId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ParentKey
    {
      get { return Get(ref _parentKey, "ParentKey"); }
      set { Set(ref _parentKey, value, "ParentKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Culture
    {
      get { return Get(ref _culture, "Culture"); }
      set { Set(ref _culture, value, "Culture"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class CandidateNoteView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    private string _note;
    private long _employerId;
    private long _candidateId;
    [ValidateLength(0, 36)]
    private string _candidateExternalId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Note entity attribute.</summary>
    public const string NoteField = "Note";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the CandidateId entity attribute.</summary>
    public const string CandidateIdField = "CandidateId";
    /// <summary>Identifies the CandidateExternalId entity attribute.</summary>
    public const string CandidateExternalIdField = "CandidateExternalId";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Note
    {
      get { return Get(ref _note, "Note"); }
      set { Set(ref _note, value, "Note"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long CandidateId
    {
      get { return Get(ref _candidateId, "CandidateId"); }
      set { Set(ref _candidateId, value, "CandidateId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CandidateExternalId
    {
      get { return Get(ref _candidateExternalId, "CandidateExternalId"); }
      set { Set(ref _candidateExternalId, value, "CandidateExternalId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class PersonListCandidateView : Entity<long>
  {
    #region Fields
  
    private Focus.Core.ListTypes _listType;
    private long _personId;
    private long _candidateId;
    [ValidateLength(0, 36)]
    private string _candidateExternalId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ListType entity attribute.</summary>
    public const string ListTypeField = "ListType";
    /// <summary>Identifies the PersonId entity attribute.</summary>
    public const string PersonIdField = "PersonId";
    /// <summary>Identifies the CandidateId entity attribute.</summary>
    public const string CandidateIdField = "CandidateId";
    /// <summary>Identifies the CandidateExternalId entity attribute.</summary>
    public const string CandidateExternalIdField = "CandidateExternalId";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.ListTypes ListType
    {
      get { return Get(ref _listType, "ListType"); }
      set { Set(ref _listType, value, "ListType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long PersonId
    {
      get { return Get(ref _personId, "PersonId"); }
      set { Set(ref _personId, value, "PersonId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long CandidateId
    {
      get { return Get(ref _candidateId, "CandidateId"); }
      set { Set(ref _candidateId, value, "CandidateId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CandidateExternalId
    {
      get { return Get(ref _candidateExternalId, "CandidateExternalId"); }
      set { Set(ref _candidateExternalId, value, "CandidateExternalId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class JobTitle : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 150)]
    private string _value;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Value entity attribute.</summary>
    public const string ValueField = "Value";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Value
    {
      get { return Get(ref _value, "Value"); }
      set { Set(ref _value, value, "Value"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class OnetTaskView : Entity<long>
  {
    #region Fields
  
    private long _onetId;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _taskKey;
    [ValidatePresence]
    private string _task;
    [ValidatePresence]
    [ValidateLength(0, 5)]
    private string _culture;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";
    /// <summary>Identifies the TaskKey entity attribute.</summary>
    public const string TaskKeyField = "TaskKey";
    /// <summary>Identifies the Task entity attribute.</summary>
    public const string TaskField = "Task";
    /// <summary>Identifies the Culture entity attribute.</summary>
    public const string CultureField = "Culture";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string TaskKey
    {
      get { return Get(ref _taskKey, "TaskKey"); }
      set { Set(ref _taskKey, value, "TaskKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Task
    {
      get { return Get(ref _task, "Task"); }
      set { Set(ref _task, value, "Task"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Culture
    {
      get { return Get(ref _culture, "Culture"); }
      set { Set(ref _culture, value, "Culture"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class PostalCode : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 8)]
    private string _code;
    private double _longitude;
    private double _latitude;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _countryKey;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _stateKey;
    [ValidateLength(0, 200)]
    private string _countyKey;
    [ValidateLength(0, 50)]
    private string _cityName;
    [ValidateLength(0, 200)]
    private string _officeKey;
    [ValidateLength(0, 200)]
    private string _wibLocationKey;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Code entity attribute.</summary>
    public const string CodeField = "Code";
    /// <summary>Identifies the Longitude entity attribute.</summary>
    public const string LongitudeField = "Longitude";
    /// <summary>Identifies the Latitude entity attribute.</summary>
    public const string LatitudeField = "Latitude";
    /// <summary>Identifies the CountryKey entity attribute.</summary>
    public const string CountryKeyField = "CountryKey";
    /// <summary>Identifies the StateKey entity attribute.</summary>
    public const string StateKeyField = "StateKey";
    /// <summary>Identifies the CountyKey entity attribute.</summary>
    public const string CountyKeyField = "CountyKey";
    /// <summary>Identifies the CityName entity attribute.</summary>
    public const string CityNameField = "CityName";
    /// <summary>Identifies the OfficeKey entity attribute.</summary>
    public const string OfficeKeyField = "OfficeKey";
    /// <summary>Identifies the WibLocationKey entity attribute.</summary>
    public const string WibLocationKeyField = "WibLocationKey";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Code
    {
      get { return Get(ref _code, "Code"); }
      set { Set(ref _code, value, "Code"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public double Longitude
    {
      get { return Get(ref _longitude, "Longitude"); }
      set { Set(ref _longitude, value, "Longitude"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public double Latitude
    {
      get { return Get(ref _latitude, "Latitude"); }
      set { Set(ref _latitude, value, "Latitude"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CountryKey
    {
      get { return Get(ref _countryKey, "CountryKey"); }
      set { Set(ref _countryKey, value, "CountryKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string StateKey
    {
      get { return Get(ref _stateKey, "StateKey"); }
      set { Set(ref _stateKey, value, "StateKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CountyKey
    {
      get { return Get(ref _countyKey, "CountyKey"); }
      set { Set(ref _countyKey, value, "CountyKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CityName
    {
      get { return Get(ref _cityName, "CityName"); }
      set { Set(ref _cityName, value, "CityName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string OfficeKey
    {
      get { return Get(ref _officeKey, "OfficeKey"); }
      set { Set(ref _officeKey, value, "OfficeKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string WibLocationKey
    {
      get { return Get(ref _wibLocationKey, "WibLocationKey"); }
      set { Set(ref _wibLocationKey, value, "WibLocationKey"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class PostalCodeView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 8)]
    private string _code;
    private double _longitude;
    private double _latitude;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _countryKey;
    [ValidatePresence]
    private string _countryName;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _stateKey;
    [ValidatePresence]
    private string _stateName;
    [ValidatePresence]
    [ValidateLength(0, 5)]
    private string _culture;
    [ValidateLength(0, 200)]
    private string _countyKey;
    private string _countyName;
    [ValidateLength(0, 50)]
    private string _cityName;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Code entity attribute.</summary>
    public const string CodeField = "Code";
    /// <summary>Identifies the Longitude entity attribute.</summary>
    public const string LongitudeField = "Longitude";
    /// <summary>Identifies the Latitude entity attribute.</summary>
    public const string LatitudeField = "Latitude";
    /// <summary>Identifies the CountryKey entity attribute.</summary>
    public const string CountryKeyField = "CountryKey";
    /// <summary>Identifies the CountryName entity attribute.</summary>
    public const string CountryNameField = "CountryName";
    /// <summary>Identifies the StateKey entity attribute.</summary>
    public const string StateKeyField = "StateKey";
    /// <summary>Identifies the StateName entity attribute.</summary>
    public const string StateNameField = "StateName";
    /// <summary>Identifies the Culture entity attribute.</summary>
    public const string CultureField = "Culture";
    /// <summary>Identifies the CountyKey entity attribute.</summary>
    public const string CountyKeyField = "CountyKey";
    /// <summary>Identifies the CountyName entity attribute.</summary>
    public const string CountyNameField = "CountyName";
    /// <summary>Identifies the CityName entity attribute.</summary>
    public const string CityNameField = "CityName";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Code
    {
      get { return Get(ref _code, "Code"); }
      set { Set(ref _code, value, "Code"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public double Longitude
    {
      get { return Get(ref _longitude, "Longitude"); }
      set { Set(ref _longitude, value, "Longitude"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public double Latitude
    {
      get { return Get(ref _latitude, "Latitude"); }
      set { Set(ref _latitude, value, "Latitude"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CountryKey
    {
      get { return Get(ref _countryKey, "CountryKey"); }
      set { Set(ref _countryKey, value, "CountryKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CountryName
    {
      get { return Get(ref _countryName, "CountryName"); }
      set { Set(ref _countryName, value, "CountryName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string StateKey
    {
      get { return Get(ref _stateKey, "StateKey"); }
      set { Set(ref _stateKey, value, "StateKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string StateName
    {
      get { return Get(ref _stateName, "StateName"); }
      set { Set(ref _stateName, value, "StateName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Culture
    {
      get { return Get(ref _culture, "Culture"); }
      set { Set(ref _culture, value, "Culture"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CountyKey
    {
      get { return Get(ref _countyKey, "CountyKey"); }
      set { Set(ref _countyKey, value, "CountyKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CountyName
    {
      get { return Get(ref _countyName, "CountyName"); }
      set { Set(ref _countyName, value, "CountyName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CityName
    {
      get { return Get(ref _cityName, "CityName"); }
      set { Set(ref _cityName, value, "CityName"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class ExpiringJobView : Entity<long>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _jobTitle;
    private long _employerId;
    private System.Nullable<long> _employeeId;
    private System.Nullable<System.DateTime> _closingOn;
    private System.Nullable<int> _daysToClosing;
    private System.Nullable<System.DateTime> _postedOn;
    private long _userId;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _firstName;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobTitle entity attribute.</summary>
    public const string JobTitleField = "JobTitle";
    /// <summary>Identifies the EmployerId entity attribute.</summary>
    public const string EmployerIdField = "EmployerId";
    /// <summary>Identifies the EmployeeId entity attribute.</summary>
    public const string EmployeeIdField = "EmployeeId";
    /// <summary>Identifies the ClosingOn entity attribute.</summary>
    public const string ClosingOnField = "ClosingOn";
    /// <summary>Identifies the DaysToClosing entity attribute.</summary>
    public const string DaysToClosingField = "DaysToClosing";
    /// <summary>Identifies the PostedOn entity attribute.</summary>
    public const string PostedOnField = "PostedOn";
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the FirstName entity attribute.</summary>
    public const string FirstNameField = "FirstName";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string JobTitle
    {
      get { return Get(ref _jobTitle, "JobTitle"); }
      set { Set(ref _jobTitle, value, "JobTitle"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployerId
    {
      get { return Get(ref _employerId, "EmployerId"); }
      set { Set(ref _employerId, value, "EmployerId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EmployeeId
    {
      get { return Get(ref _employeeId, "EmployeeId"); }
      set { Set(ref _employeeId, value, "EmployeeId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ClosingOn
    {
      get { return Get(ref _closingOn, "ClosingOn"); }
      set { Set(ref _closingOn, value, "ClosingOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> DaysToClosing
    {
      get { return Get(ref _daysToClosing, "DaysToClosing"); }
      set { Set(ref _daysToClosing, value, "DaysToClosing"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> PostedOn
    {
      get { return Get(ref _postedOn, "PostedOn"); }
      set { Set(ref _postedOn, value, "PostedOn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string FirstName
    {
      get { return Get(ref _firstName, "FirstName"); }
      set { Set(ref _firstName, value, "FirstName"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class OnetWordJobTaskView : Entity<long>
  {
    #region Fields
  
    private long _onetId;
    private long _onetRingId;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _onetSoc;
    [ValidatePresence]
    [ValidateLength(0, 30)]
    private string _stem;
    [ValidatePresence]
    [ValidateLength(0, 30)]
    private string _word;
    private long _jobFamilyId;
    private int _jobZone;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _onetKey;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _onetCode;
    private System.Nullable<int> _frequency;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";
    /// <summary>Identifies the OnetRingId entity attribute.</summary>
    public const string OnetRingIdField = "OnetRingId";
    /// <summary>Identifies the OnetSoc entity attribute.</summary>
    public const string OnetSocField = "OnetSoc";
    /// <summary>Identifies the Stem entity attribute.</summary>
    public const string StemField = "Stem";
    /// <summary>Identifies the Word entity attribute.</summary>
    public const string WordField = "Word";
    /// <summary>Identifies the JobFamilyId entity attribute.</summary>
    public const string JobFamilyIdField = "JobFamilyId";
    /// <summary>Identifies the JobZone entity attribute.</summary>
    public const string JobZoneField = "JobZone";
    /// <summary>Identifies the OnetKey entity attribute.</summary>
    public const string OnetKeyField = "OnetKey";
    /// <summary>Identifies the OnetCode entity attribute.</summary>
    public const string OnetCodeField = "OnetCode";
    /// <summary>Identifies the Frequency entity attribute.</summary>
    public const string FrequencyField = "Frequency";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetRingId
    {
      get { return Get(ref _onetRingId, "OnetRingId"); }
      set { Set(ref _onetRingId, value, "OnetRingId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetSoc
    {
      get { return Get(ref _onetSoc, "OnetSoc"); }
      set { Set(ref _onetSoc, value, "OnetSoc"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Stem
    {
      get { return Get(ref _stem, "Stem"); }
      set { Set(ref _stem, value, "Stem"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Word
    {
      get { return Get(ref _word, "Word"); }
      set { Set(ref _word, value, "Word"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long JobFamilyId
    {
      get { return Get(ref _jobFamilyId, "JobFamilyId"); }
      set { Set(ref _jobFamilyId, value, "JobFamilyId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int JobZone
    {
      get { return Get(ref _jobZone, "JobZone"); }
      set { Set(ref _jobZone, value, "JobZone"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetKey
    {
      get { return Get(ref _onetKey, "OnetKey"); }
      set { Set(ref _onetKey, value, "OnetKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetCode
    {
      get { return Get(ref _onetCode, "OnetCode"); }
      set { Set(ref _onetCode, value, "OnetCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> Frequency
    {
      get { return Get(ref _frequency, "Frequency"); }
      set { Set(ref _frequency, value, "Frequency"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class OnetWordView : Entity<long>
  {
    #region Fields
  
    private long _onetId;
    private long _onetRingId;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _onetSoc;
    [ValidatePresence]
    [ValidateLength(0, 30)]
    private string _stem;
    [ValidatePresence]
    [ValidateLength(0, 30)]
    private string _word;
    private long _jobFamilyId;
    private int _jobZone;
    [ValidatePresence]
    [ValidateLength(0, 200)]
    private string _onetKey;
    [ValidatePresence]
    [ValidateLength(0, 10)]
    private string _onetCode;
    private System.Nullable<int> _frequency;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the OnetId entity attribute.</summary>
    public const string OnetIdField = "OnetId";
    /// <summary>Identifies the OnetRingId entity attribute.</summary>
    public const string OnetRingIdField = "OnetRingId";
    /// <summary>Identifies the OnetSoc entity attribute.</summary>
    public const string OnetSocField = "OnetSoc";
    /// <summary>Identifies the Stem entity attribute.</summary>
    public const string StemField = "Stem";
    /// <summary>Identifies the Word entity attribute.</summary>
    public const string WordField = "Word";
    /// <summary>Identifies the JobFamilyId entity attribute.</summary>
    public const string JobFamilyIdField = "JobFamilyId";
    /// <summary>Identifies the JobZone entity attribute.</summary>
    public const string JobZoneField = "JobZone";
    /// <summary>Identifies the OnetKey entity attribute.</summary>
    public const string OnetKeyField = "OnetKey";
    /// <summary>Identifies the OnetCode entity attribute.</summary>
    public const string OnetCodeField = "OnetCode";
    /// <summary>Identifies the Frequency entity attribute.</summary>
    public const string FrequencyField = "Frequency";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetId
    {
      get { return Get(ref _onetId, "OnetId"); }
      set { Set(ref _onetId, value, "OnetId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long OnetRingId
    {
      get { return Get(ref _onetRingId, "OnetRingId"); }
      set { Set(ref _onetRingId, value, "OnetRingId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetSoc
    {
      get { return Get(ref _onetSoc, "OnetSoc"); }
      set { Set(ref _onetSoc, value, "OnetSoc"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Stem
    {
      get { return Get(ref _stem, "Stem"); }
      set { Set(ref _stem, value, "Stem"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Word
    {
      get { return Get(ref _word, "Word"); }
      set { Set(ref _word, value, "Word"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long JobFamilyId
    {
      get { return Get(ref _jobFamilyId, "JobFamilyId"); }
      set { Set(ref _jobFamilyId, value, "JobFamilyId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int JobZone
    {
      get { return Get(ref _jobZone, "JobZone"); }
      set { Set(ref _jobZone, value, "JobZone"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetKey
    {
      get { return Get(ref _onetKey, "OnetKey"); }
      set { Set(ref _onetKey, value, "OnetKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string OnetCode
    {
      get { return Get(ref _onetCode, "OnetCode"); }
      set { Set(ref _onetCode, value, "OnetCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> Frequency
    {
      get { return Get(ref _frequency, "Frequency"); }
      set { Set(ref _frequency, value, "Frequency"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class Bookmark : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _name;
    private Focus.Core.BookmarkTypes _type;
    private string _criteria;
    private System.Nullable<long> _userId;
    private Focus.Core.UserTypes _userType;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the Type entity attribute.</summary>
    public const string TypeField = "Type";
    /// <summary>Identifies the Criteria entity attribute.</summary>
    public const string CriteriaField = "Criteria";
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the UserType entity attribute.</summary>
    public const string UserTypeField = "UserType";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.BookmarkTypes Type
    {
      get { return Get(ref _type, "Type"); }
      set { Set(ref _type, value, "Type"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Criteria
    {
      get { return Get(ref _criteria, "Criteria"); }
      set { Set(ref _criteria, value, "Criteria"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Core.UserTypes UserType
    {
      get { return Get(ref _userType, "UserType"); }
      set { Set(ref _userType, value, "UserType"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class PersonResume : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 100)]
    private string _fileName;
    [ValidateLength(0, 255)]
    private string _contentType;
    private byte[] _resume;
    [ValidateLength(0, 10)]
    private string _primaryOnet;
    [ValidateLength(0, 10)]
    private string _primaryROnet;
    private long _personId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the FileName entity attribute.</summary>
    public const string FileNameField = "FileName";
    /// <summary>Identifies the ContentType entity attribute.</summary>
    public const string ContentTypeField = "ContentType";
    /// <summary>Identifies the Resume entity attribute.</summary>
    public const string ResumeField = "Resume";
    /// <summary>Identifies the PrimaryOnet entity attribute.</summary>
    public const string PrimaryOnetField = "PrimaryOnet";
    /// <summary>Identifies the PrimaryROnet entity attribute.</summary>
    public const string PrimaryROnetField = "PrimaryROnet";
    /// <summary>Identifies the PersonId entity attribute.</summary>
    public const string PersonIdField = "PersonId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("PersonResumes")]
    private readonly EntityHolder<Person> _person = new EntityHolder<Person>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Person Person
    {
      get { return Get(_person); }
      set { Set(_person, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string FileName
    {
      get { return Get(ref _fileName, "FileName"); }
      set { Set(ref _fileName, value, "FileName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ContentType
    {
      get { return Get(ref _contentType, "ContentType"); }
      set { Set(ref _contentType, value, "ContentType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public byte[] Resume
    {
      get { return Get(ref _resume, "Resume"); }
      set { Set(ref _resume, value, "Resume"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PrimaryOnet
    {
      get { return Get(ref _primaryOnet, "PrimaryOnet"); }
      set { Set(ref _primaryOnet, value, "PrimaryOnet"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PrimaryROnet
    {
      get { return Get(ref _primaryROnet, "PrimaryROnet"); }
      set { Set(ref _primaryROnet, value, "PrimaryROnet"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Person" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public long PersonId
    {
      get { return Get(ref _personId, "PersonId"); }
      set { Set(ref _personId, value, "PersonId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class UserRole : Entity<long>
  {
    #region Fields
  
    private long _userId;
    private long _roleId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the RoleId entity attribute.</summary>
    public const string RoleIdField = "RoleId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("UserRoles")]
    private readonly EntityHolder<User> _user = new EntityHolder<User>();
    [ReverseAssociation("UserRoles")]
    private readonly EntityHolder<Role> _role = new EntityHolder<Role>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public User User
    {
      get { return Get(_user); }
      set { Set(_user, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Role Role
    {
      get { return Get(_role); }
      set { Set(_role, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public long UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long RoleId
    {
      get { return Get(ref _roleId, "RoleId"); }
      set { Set(ref _roleId, value, "RoleId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class EmployeeCandidate : Entity<long>
  {
    #region Fields
  
    private long _employeeId;
    private long _candidateId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the EmployeeId entity attribute.</summary>
    public const string EmployeeIdField = "EmployeeId";
    /// <summary>Identifies the CandidateId entity attribute.</summary>
    public const string CandidateIdField = "CandidateId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("EmployeeCandidates")]
    private readonly EntityHolder<Employee> _employee = new EntityHolder<Employee>();
    [ReverseAssociation("EmployeeCandidates")]
    private readonly EntityHolder<Candidate> _candidate = new EntityHolder<Candidate>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Employee Employee
    {
      get { return Get(_employee); }
      set { Set(_employee, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Candidate Candidate
    {
      get { return Get(_candidate); }
      set { Set(_candidate, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployeeId
    {
      get { return Get(ref _employeeId, "EmployeeId"); }
      set { Set(ref _employeeId, value, "EmployeeId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long CandidateId
    {
      get { return Get(ref _candidateId, "CandidateId"); }
      set { Set(ref _candidateId, value, "CandidateId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class JobNote : Entity<long>
  {
    #region Fields
  
    private long _jobId;
    private long _noteId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the JobId entity attribute.</summary>
    public const string JobIdField = "JobId";
    /// <summary>Identifies the NoteId entity attribute.</summary>
    public const string NoteIdField = "NoteId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobNotes")]
    private readonly EntityHolder<Job> _job = new EntityHolder<Job>();
    [ReverseAssociation("JobNotes")]
    private readonly EntityHolder<Note> _note = new EntityHolder<Note>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Job Job
    {
      get { return Get(_job); }
      set { Set(_job, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Note Note
    {
      get { return Get(_note); }
      set { Set(_note, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public long JobId
    {
      get { return Get(ref _jobId, "JobId"); }
      set { Set(ref _jobId, value, "JobId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long NoteId
    {
      get { return Get(ref _noteId, "NoteId"); }
      set { Set(ref _noteId, value, "NoteId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class SavedSearchEmployee : Entity<long>
  {
    #region Fields
  
    private long _employeeId;
    private long _savedSearchId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the EmployeeId entity attribute.</summary>
    public const string EmployeeIdField = "EmployeeId";
    /// <summary>Identifies the SavedSearchId entity attribute.</summary>
    public const string SavedSearchIdField = "SavedSearchId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("SavedSearchEmployees")]
    private readonly EntityHolder<Employee> _employee = new EntityHolder<Employee>();
    [ReverseAssociation("SavedSearchEmployees")]
    private readonly EntityHolder<SavedSearch> _savedSearch = new EntityHolder<SavedSearch>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Employee Employee
    {
      get { return Get(_employee); }
      set { Set(_employee, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public SavedSearch SavedSearch
    {
      get { return Get(_savedSearch); }
      set { Set(_savedSearch, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public long EmployeeId
    {
      get { return Get(ref _employeeId, "EmployeeId"); }
      set { Set(ref _employeeId, value, "EmployeeId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long SavedSearchId
    {
      get { return Get(ref _savedSearchId, "SavedSearchId"); }
      set { Set(ref _savedSearchId, value, "SavedSearchId"); }
    }

    #endregion
  }
	

  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class PersonListCandidate : Entity<long>
  {
    #region Fields
  
    private long _candidateId;
    private long _personListId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the CandidateId entity attribute.</summary>
    public const string CandidateIdField = "CandidateId";
    /// <summary>Identifies the PersonListId entity attribute.</summary>
    public const string PersonListIdField = "PersonListId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("PersonListCandidates")]
    private readonly EntityHolder<Candidate> _candidate = new EntityHolder<Candidate>();
    [ReverseAssociation("PersonListCandidates")]
    private readonly EntityHolder<PersonList> _personList = new EntityHolder<PersonList>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Candidate Candidate
    {
      get { return Get(_candidate); }
      set { Set(_candidate, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public PersonList PersonList
    {
      get { return Get(_personList); }
      set { Set(_personList, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public long CandidateId
    {
      get { return Get(ref _candidateId, "CandidateId"); }
      set { Set(ref _candidateId, value, "CandidateId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public long PersonListId
    {
      get { return Get(ref _personListId, "PersonListId"); }
      set { Set(ref _personListId, value, "PersonListId"); }
    }

    #endregion
  }
	


  /// <summary>
  /// Provides a strong-typed unit of work for working with the Focus model.
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  public partial class FocusUnitOfWork : Mindscape.LightSpeed.UnitOfWork
  {

    public System.Linq.IQueryable<ActionEvent> ActionEvents
    {
      get { return this.Query<ActionEvent>(); }
    }
    
    public System.Linq.IQueryable<User> Users
    {
      get { return this.Query<User>(); }
    }
    
    public System.Linq.IQueryable<BusinessUnit> BusinessUnits
    {
      get { return this.Query<BusinessUnit>(); }
    }
    
    public System.Linq.IQueryable<CodeGroup> CodeGroups
    {
      get { return this.Query<CodeGroup>(); }
    }
    
    public System.Linq.IQueryable<CodeGroupItem> CodeGroupItems
    {
      get { return this.Query<CodeGroupItem>(); }
    }
    
    public System.Linq.IQueryable<CodeItem> CodeItems
    {
      get { return this.Query<CodeItem>(); }
    }
    
    public System.Linq.IQueryable<ConfigurationItem> ConfigurationItems
    {
      get { return this.Query<ConfigurationItem>(); }
    }
    
    public System.Linq.IQueryable<Employee> Employees
    {
      get { return this.Query<Employee>(); }
    }
    
    public System.Linq.IQueryable<Employer> Employers
    {
      get { return this.Query<Employer>(); }
    }
    
    public System.Linq.IQueryable<EmployerDescription> EmployerDescriptions
    {
      get { return this.Query<EmployerDescription>(); }
    }
    
    public System.Linq.IQueryable<EmployerLogo> EmployerLogos
    {
      get { return this.Query<EmployerLogo>(); }
    }
    
    public System.Linq.IQueryable<EntityType> EntityTypes
    {
      get { return this.Query<EntityType>(); }
    }
    
    public System.Linq.IQueryable<Job> Jobs
    {
      get { return this.Query<Job>(); }
    }
    
    public System.Linq.IQueryable<CandidateApplication> CandidateApplications
    {
      get { return this.Query<CandidateApplication>(); }
    }
    
    public System.Linq.IQueryable<JobSkill> JobSkills
    {
      get { return this.Query<JobSkill>(); }
    }
    
    public System.Linq.IQueryable<JobSpecialRequirement> JobSpecialRequirements
    {
      get { return this.Query<JobSpecialRequirement>(); }
    }
    
    public System.Linq.IQueryable<Localisation> Localisations
    {
      get { return this.Query<Localisation>(); }
    }
    
    public System.Linq.IQueryable<LocalisationItem> LocalisationItems
    {
      get { return this.Query<LocalisationItem>(); }
    }
    
    public System.Linq.IQueryable<Note> Notes
    {
      get { return this.Query<Note>(); }
    }
    
    public System.Linq.IQueryable<Person> Persons
    {
      get { return this.Query<Person>(); }
    }
    
    public System.Linq.IQueryable<PostingSurvey> PostingSurveys
    {
      get { return this.Query<PostingSurvey>(); }
    }
    
    public System.Linq.IQueryable<SavedSearch> SavedSearches
    {
      get { return this.Query<SavedSearch>(); }
    }
    
    public System.Linq.IQueryable<EmployerAddress> EmployerAddresses
    {
      get { return this.Query<EmployerAddress>(); }
    }
    
    public System.Linq.IQueryable<JobAddress> JobAddresses
    {
      get { return this.Query<JobAddress>(); }
    }
    
    public System.Linq.IQueryable<Role> Roles
    {
      get { return this.Query<Role>(); }
    }
    
    public System.Linq.IQueryable<PersonAddress> PersonAddresses
    {
      get { return this.Query<PersonAddress>(); }
    }
    
    public System.Linq.IQueryable<ActionType> ActionTypes
    {
      get { return this.Query<ActionType>(); }
    }
    
    public System.Linq.IQueryable<Onet> Onets
    {
      get { return this.Query<Onet>(); }
    }
    
    public System.Linq.IQueryable<EmailTemplate> EmailTemplates
    {
      get { return this.Query<EmailTemplate>(); }
    }
    
    public System.Linq.IQueryable<JobLocation> JobLocations
    {
      get { return this.Query<JobLocation>(); }
    }
    
    public System.Linq.IQueryable<OnetTask> OnetTasks
    {
      get { return this.Query<OnetTask>(); }
    }
    
    public System.Linq.IQueryable<CertificateLicence> CertificateLicences
    {
      get { return this.Query<CertificateLicence>(); }
    }
    
    public System.Linq.IQueryable<JobTask> JobTasks
    {
      get { return this.Query<JobTask>(); }
    }
    
    public System.Linq.IQueryable<JobTaskMultiOption> JobTaskMultiOptions
    {
      get { return this.Query<JobTaskMultiOption>(); }
    }
    
    public System.Linq.IQueryable<JobLicence> JobLicences
    {
      get { return this.Query<JobLicence>(); }
    }
    
    public System.Linq.IQueryable<JobCertificate> JobCertificates
    {
      get { return this.Query<JobCertificate>(); }
    }
    
    public System.Linq.IQueryable<JobLanguage> JobLanguages
    {
      get { return this.Query<JobLanguage>(); }
    }
    
    public System.Linq.IQueryable<Profanity> Profanities
    {
      get { return this.Query<Profanity>(); }
    }
    
    public System.Linq.IQueryable<OnetLocalisationItem> OnetLocalisationItems
    {
      get { return this.Query<OnetLocalisationItem>(); }
    }
    
    public System.Linq.IQueryable<JobTaskLocalisationItem> JobTaskLocalisationItems
    {
      get { return this.Query<JobTaskLocalisationItem>(); }
    }
    
    public System.Linq.IQueryable<CertificateLicenceLocalisationItem> CertificateLicenceLocalisationItems
    {
      get { return this.Query<CertificateLicenceLocalisationItem>(); }
    }
    
    public System.Linq.IQueryable<OnetWord> OnetWords
    {
      get { return this.Query<OnetWord>(); }
    }
    
    public System.Linq.IQueryable<OnetRing> OnetRings
    {
      get { return this.Query<OnetRing>(); }
    }
    
    public System.Linq.IQueryable<OnetPhrase> OnetPhrases
    {
      get { return this.Query<OnetPhrase>(); }
    }
    
    public System.Linq.IQueryable<PhoneNumber> PhoneNumbers
    {
      get { return this.Query<PhoneNumber>(); }
    }
    
    public System.Linq.IQueryable<JobView> JobViews
    {
      get { return this.Query<JobView>(); }
    }
    
    public System.Linq.IQueryable<Session> Sessions
    {
      get { return this.Query<Session>(); }
    }
    
    public System.Linq.IQueryable<SessionState> SessionStates
    {
      get { return this.Query<SessionState>(); }
    }
    
    public System.Linq.IQueryable<Message> Messages
    {
      get { return this.Query<Message>(); }
    }
    
    public System.Linq.IQueryable<DismissedMessage> DismissedMessages
    {
      get { return this.Query<DismissedMessage>(); }
    }
    
    public System.Linq.IQueryable<MessageText> MessageTexts
    {
      get { return this.Query<MessageText>(); }
    }
    
    public System.Linq.IQueryable<Candidate> Candidates
    {
      get { return this.Query<Candidate>(); }
    }
    
    public System.Linq.IQueryable<EmployeeSearchView> EmployeeSearchViews
    {
      get { return this.Query<EmployeeSearchView>(); }
    }
    
    public System.Linq.IQueryable<SavedMessage> SavedMessages
    {
      get { return this.Query<SavedMessage>(); }
    }
    
    public System.Linq.IQueryable<SavedMessageText> SavedMessageTexts
    {
      get { return this.Query<SavedMessageText>(); }
    }
    
    public System.Linq.IQueryable<JobSeekerReferralView> JobSeekerReferralViews
    {
      get { return this.Query<JobSeekerReferralView>(); }
    }
    
    public System.Linq.IQueryable<EmployerAccountReferralView> EmployerAccountReferralViews
    {
      get { return this.Query<EmployerAccountReferralView>(); }
    }
    
    public System.Linq.IQueryable<JobPostingReferralView> JobPostingReferralViews
    {
      get { return this.Query<JobPostingReferralView>(); }
    }
    
    public System.Linq.IQueryable<PersonList> PersonLists
    {
      get { return this.Query<PersonList>(); }
    }
    
    public System.Linq.IQueryable<ApplicationImage> ApplicationImages
    {
      get { return this.Query<ApplicationImage>(); }
    }
    
    public System.Linq.IQueryable<StaffSearchView> StaffSearchViews
    {
      get { return this.Query<StaffSearchView>(); }
    }
    
    public System.Linq.IQueryable<SingleSignOn> SingleSignOns
    {
      get { return this.Query<SingleSignOn>(); }
    }
    
    public System.Linq.IQueryable<JobTaskView> JobTaskViews
    {
      get { return this.Query<JobTaskView>(); }
    }
    
    public System.Linq.IQueryable<JobTaskMultiOptionView> JobTaskMultiOptionViews
    {
      get { return this.Query<JobTaskMultiOptionView>(); }
    }
    
    public System.Linq.IQueryable<OnetView> OnetViews
    {
      get { return this.Query<OnetView>(); }
    }
    
    public System.Linq.IQueryable<CandidateNote> CandidateNotes
    {
      get { return this.Query<CandidateNote>(); }
    }
    
    public System.Linq.IQueryable<NAICS> NAICs
    {
      get { return this.Query<NAICS>(); }
    }
    
    public System.Linq.IQueryable<ApplicationView> ApplicationViews
    {
      get { return this.Query<ApplicationView>(); }
    }
    
    public System.Linq.IQueryable<LookupItemsView> LookupItemsViews
    {
      get { return this.Query<LookupItemsView>(); }
    }
    
    public System.Linq.IQueryable<CandidateNoteView> CandidateNoteViews
    {
      get { return this.Query<CandidateNoteView>(); }
    }
    
    public System.Linq.IQueryable<PersonListCandidateView> PersonListCandidateViews
    {
      get { return this.Query<PersonListCandidateView>(); }
    }
    
    public System.Linq.IQueryable<JobTitle> JobTitles
    {
      get { return this.Query<JobTitle>(); }
    }
    
    public System.Linq.IQueryable<OnetTaskView> OnetTaskViews
    {
      get { return this.Query<OnetTaskView>(); }
    }
    
    public System.Linq.IQueryable<PostalCode> PostalCodes
    {
      get { return this.Query<PostalCode>(); }
    }
    
    public System.Linq.IQueryable<PostalCodeView> PostalCodeViews
    {
      get { return this.Query<PostalCodeView>(); }
    }
    
    public System.Linq.IQueryable<ExpiringJobView> ExpiringJobViews
    {
      get { return this.Query<ExpiringJobView>(); }
    }
    
    public System.Linq.IQueryable<OnetWordJobTaskView> OnetWordJobTaskViews
    {
      get { return this.Query<OnetWordJobTaskView>(); }
    }
    
    public System.Linq.IQueryable<OnetWordView> OnetWordViews
    {
      get { return this.Query<OnetWordView>(); }
    }
    
    public System.Linq.IQueryable<Bookmark> Bookmarks
    {
      get { return this.Query<Bookmark>(); }
    }
    
    public System.Linq.IQueryable<PersonResume> PersonResumes
    {
      get { return this.Query<PersonResume>(); }
    }
    
    public System.Linq.IQueryable<UserRole> UserRoles
    {
      get { return this.Query<UserRole>(); }
    }
    
    public System.Linq.IQueryable<EmployeeCandidate> EmployeeCandidates
    {
      get { return this.Query<EmployeeCandidate>(); }
    }
    
    public System.Linq.IQueryable<JobNote> JobNotes
    {
      get { return this.Query<JobNote>(); }
    }
    
    public System.Linq.IQueryable<SavedSearchEmployee> SavedSearchEmployees
    {
      get { return this.Query<SavedSearchEmployee>(); }
    }
    
    public System.Linq.IQueryable<PersonListCandidate> PersonListCandidates
    {
      get { return this.Query<PersonListCandidate>(); }
    }
    
  }
}

#endregion

#region Data Transfer Objects

namespace Focus.Data.Core.DataTransferObjects
{
	using Focus.Data.Core;

    [Serializable]
    public partial class ActionEventDto
    {
      public System.Nullable<long> Id { get; set; }
      public System.Guid SessionId { get; set; }
      public System.Guid RequestId { get; set; }
      public long UserId { get; set; }
      public System.DateTime ActionedOn { get; set; }
      public System.Nullable<long> EntityId { get; set; }
      public System.Nullable<long> EntityIdAdditional01 { get; set; }
      public System.Nullable<long> EntityIdAdditional02 { get; set; }
      public string AdditionalDetails { get; set; }
      public System.Nullable<long> EntityTypeId { get; set; }
      public long ActionTypeId { get; set; }
    }

    [Serializable]
    public partial class UserDto
    {
      public System.Nullable<long> Id { get; set; }
      public System.DateTime CreatedOn { get; set; }
      public System.DateTime UpdatedOn { get; set; }
      public System.Nullable<System.DateTime> DeletedOn { get; set; }
      public string UserName { get; set; }
      public string PasswordHash { get; set; }
      public string PasswordSalt { get; set; }
      public bool Enabled { get; set; }
      public string ValidationKey { get; set; }
      public System.Nullable<Focus.Core.UserTypes> UserType { get; set; }
      public System.Nullable<System.DateTime> LoggedInOn { get; set; }
      public System.Nullable<System.DateTime> LastLoggedInOn { get; set; }
      public string ExternalId { get; set; }
      public bool IsClientAuthenticated { get; set; }
      public string ScreenName { get; set; }
      public string SecurityQuestion { get; set; }
      public string SecurityAnswerHash { get; set; }
      public long PersonId { get; set; }
    }

    [Serializable]
    public partial class BusinessUnitDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Name { get; set; }
      public bool IsPrimary { get; set; }
      public long EmployerId { get; set; }
    }

    [Serializable]
    public partial class CodeGroupDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Key { get; set; }
    }

    [Serializable]
    public partial class CodeGroupItemDto
    {
      public System.Nullable<long> Id { get; set; }
      public int DisplayOrder { get; set; }
      public long CodeGroupId { get; set; }
      public long CodeItemId { get; set; }
    }

    [Serializable]
    public partial class CodeItemDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Key { get; set; }
      public bool IsSystem { get; set; }
      public string ParentKey { get; set; }
      public string ExternalId { get; set; }
    }

    [Serializable]
    public partial class ConfigurationItemDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Key { get; set; }
      public string Value { get; set; }
    }

    [Serializable]
    public partial class EmployeeDto
    {
      public System.Nullable<long> Id { get; set; }
      public Focus.Core.ApprovalStatuses ApprovalStatus { get; set; }
      public long EmployerId { get; set; }
      public long PersonId { get; set; }
    }

    [Serializable]
    public partial class EmployerDto
    {
      public System.Nullable<long> Id { get; set; }
      public System.DateTime CreatedOn { get; set; }
      public System.DateTime UpdatedOn { get; set; }
      public string Name { get; set; }
      public System.Nullable<System.DateTime> CommencedOn { get; set; }
      public string FederalEmployerIdentificationNumber { get; set; }
      public bool IsValidFederalEmployerIdentificationNumber { get; set; }
      public string StateEmployerIdentificationNumber { get; set; }
      public Focus.Core.ApprovalStatuses ApprovalStatus { get; set; }
      public string Url { get; set; }
      public System.Nullable<System.DateTime> ExpiredOn { get; set; }
      public long OwnershipTypeId { get; set; }
      public string IndustrialClassification { get; set; }
      public bool TermsAccepted { get; set; }
      public string PrimaryPhone { get; set; }
      public string PrimaryPhoneExtension { get; set; }
      public string PrimaryPhoneType { get; set; }
      public string AlternatePhone1 { get; set; }
      public string AlternatePhone1Type { get; set; }
      public string AlternatePhone2 { get; set; }
      public string AlternatePhone2Type { get; set; }
      public bool IsRegistrationComplete { get; set; }
    }

    [Serializable]
    public partial class EmployerDescriptionDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Title { get; set; }
      public string Description { get; set; }
      public bool IsPrimary { get; set; }
      public long EmployerId { get; set; }
    }

    [Serializable]
    public partial class EmployerLogoDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Name { get; set; }
      public byte[] Logo { get; set; }
      public long EmployerId { get; set; }
    }

    [Serializable]
    public partial class EntityTypeDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Name { get; set; }
    }

    [Serializable]
    public partial class JobDto
    {
      public System.Nullable<long> Id { get; set; }
      public int LockVersion { get; set; }
      public System.DateTime CreatedOn { get; set; }
      public System.DateTime UpdatedOn { get; set; }
      public string JobTitle { get; set; }
      public long CreatedBy { get; set; }
      public long UpdatedBy { get; set; }
      public Focus.Core.ApprovalStatuses ApprovalStatus { get; set; }
      public Focus.Core.JobStatuses JobStatus { get; set; }
      public System.Nullable<decimal> MinSalary { get; set; }
      public System.Nullable<decimal> MaxSalary { get; set; }
      public System.Nullable<long> SalaryFrequencyId { get; set; }
      public System.Nullable<decimal> HoursPerWeek { get; set; }
      public System.Nullable<bool> OverTimeRequired { get; set; }
      public System.Nullable<System.DateTime> ClosingOn { get; set; }
      public System.Nullable<int> NumberOfOpenings { get; set; }
      public System.Nullable<System.DateTime> PostedOn { get; set; }
      public System.Nullable<long> PostedBy { get; set; }
      public string Description { get; set; }
      public string Posting { get; set; }
      public Focus.Core.EmployerDescriptionPostingPositions EmployerDescriptionPostingPosition { get; set; }
      public int WizardStep { get; set; }
      public int WizardPath { get; set; }
      public System.Nullable<System.DateTime> HeldOn { get; set; }
      public System.Nullable<long> HeldBy { get; set; }
      public System.Nullable<System.DateTime> ClosedOn { get; set; }
      public System.Nullable<long> ClosedBy { get; set; }
      public System.Nullable<Focus.Core.JobLocationTypes> JobLocationType { get; set; }
      public System.Nullable<bool> FederalContractor { get; set; }
      public System.Nullable<bool> ForeignLabourCertification { get; set; }
      public System.Nullable<bool> CourtOrderedAffirmativeAction { get; set; }
      public System.Nullable<System.DateTime> FederalContractorExpiresOn { get; set; }
      public System.Nullable<Focus.Core.WorkOpportunitiesTaxCreditCategories> WorkOpportunitiesTaxCreditHires { get; set; }
      public System.Nullable<Focus.Core.JobPostingFlags> PostingFlags { get; set; }
      public System.Nullable<bool> IsCommissionBased { get; set; }
      public System.Nullable<Focus.Core.Weekdays> NormalWorkDays { get; set; }
      public System.Nullable<bool> WorkDaysVary { get; set; }
      public System.Nullable<long> NormalWorkShiftsId { get; set; }
      public System.Nullable<Focus.Core.LeaveBenefits> LeaveBenefits { get; set; }
      public System.Nullable<Focus.Core.RetirementBenefits> RetirementBenefits { get; set; }
      public System.Nullable<Focus.Core.InsuranceBenefits> InsuranceBenefits { get; set; }
      public System.Nullable<Focus.Core.MiscellaneousBenefits> MiscellaneousBenefits { get; set; }
      public string OtherBenefitsDetails { get; set; }
      public System.Nullable<Focus.Core.ContactMethods> InterviewContactPreferences { get; set; }
      public string InterviewEmailAddress { get; set; }
      public string InterviewApplicationUrl { get; set; }
      public string InterviewMailAddress { get; set; }
      public string InterviewFaxNumber { get; set; }
      public string InterviewPhoneNumber { get; set; }
      public string InterviewDirectApplicationDetails { get; set; }
      public string InterviewOtherInstructions { get; set; }
      public System.Nullable<Focus.Core.ScreeningPreferences> ScreeningPreferences { get; set; }
      public System.Nullable<Focus.Core.EducationLevels> MinimumEducationLevel { get; set; }
      public System.Nullable<bool> MinimumEducationLevelRequired { get; set; }
      public System.Nullable<int> MinimumAge { get; set; }
      public string MinimumAgeReason { get; set; }
      public System.Nullable<bool> MinimumAgeRequired { get; set; }
      public System.Nullable<bool> LicencesRequired { get; set; }
      public System.Nullable<bool> CertificationRequired { get; set; }
      public System.Nullable<bool> LanguagesRequired { get; set; }
      public string Tasks { get; set; }
      public string RedProfanityWords { get; set; }
      public string YellowProfanityWords { get; set; }
      public System.Nullable<long> EmploymentStatusId { get; set; }
      public System.Nullable<long> JobTypeId { get; set; }
      public System.Nullable<long> JobStatusId { get; set; }
      public System.Nullable<System.DateTime> ApprovedOn { get; set; }
      public System.Nullable<long> ApprovedBy { get; set; }
      public string ExternalId { get; set; }
      public System.Nullable<System.DateTime> AwaitingApprovalOn { get; set; }
      public System.Nullable<int> MinimumExperience { get; set; }
      public System.Nullable<bool> MinimumExperienceRequired { get; set; }
      public System.Nullable<long> DrivingLicenceClassId { get; set; }
      public System.Nullable<Focus.Core.DrivingLicenceEndorsements> DrivingLicenceEndorsements { get; set; }
      public System.Nullable<bool> DrivingLicenceRequired { get; set; }
      public System.Nullable<bool> HideSalaryOnPosting { get; set; }
      public System.Nullable<long> BusinessUnitId { get; set; }
      public System.Nullable<long> EmployeeId { get; set; }
      public long EmployerId { get; set; }
      public System.Nullable<long> EmployerDescriptionId { get; set; }
      public System.Nullable<long> EmployerLogoId { get; set; }
      public System.Nullable<long> OnetId { get; set; }
    }

    [Serializable]
    public partial class CandidateApplicationDto
    {
      public System.Nullable<long> Id { get; set; }
      public System.DateTime CreatedOn { get; set; }
      public System.DateTime UpdatedOn { get; set; }
      public Focus.Core.ApprovalStatuses ApprovalStatus { get; set; }
      public string ApprovalRequiredReason { get; set; }
      public Focus.Core.ApplicationStatusTypes ApplicationStatus { get; set; }
      public int ApplicationScore { get; set; }
      public long JobId { get; set; }
      public long CandidateId { get; set; }
    }

    [Serializable]
    public partial class JobSkillDto
    {
      public System.Nullable<long> Id { get; set; }
      public long SkillId { get; set; }
      public long JobId { get; set; }
    }

    [Serializable]
    public partial class JobSpecialRequirementDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Requirement { get; set; }
      public long JobId { get; set; }
    }

    [Serializable]
    public partial class LocalisationDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Culture { get; set; }
    }

    [Serializable]
    public partial class LocalisationItemDto
    {
      public System.Nullable<long> Id { get; set; }
      public string ContextKey { get; set; }
      public string Key { get; set; }
      public string Value { get; set; }
      public bool Localised { get; set; }
      public long LocalisationId { get; set; }
    }

    [Serializable]
    public partial class NoteDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Title { get; set; }
      public string Body { get; set; }
    }

    [Serializable]
    public partial class PersonDto
    {
      public System.Nullable<long> Id { get; set; }
      public long TitleId { get; set; }
      public string FirstName { get; set; }
      public string MiddleInitial { get; set; }
      public string LastName { get; set; }
      public System.Nullable<System.DateTime> DateOfBirth { get; set; }
      public string SocialSecurityNumber { get; set; }
      public string JobTitle { get; set; }
      public string EmailAddress { get; set; }
      public string Office { get; set; }
    }

    [Serializable]
    public partial class PostingSurveyDto
    {
      public System.Nullable<long> Id { get; set; }
      public Focus.Core.SurveyTypes SurveyType { get; set; }
      public long SatisfactionLevel { get; set; }
      public bool DidInterview { get; set; }
      public bool DidHire { get; set; }
      public System.Nullable<bool> DidOffer { get; set; }
      public long JobId { get; set; }
    }

    [Serializable]
    public partial class SavedSearchDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Name { get; set; }
      public string SearchCriteria { get; set; }
      public bool AlertEmailRequired { get; set; }
      public Focus.Core.EmailAlertFrequencies AlertEmailFrequency { get; set; }
      public Focus.Core.EmailFormats AlertEmailFormat { get; set; }
      public string AlertEmailAddress { get; set; }
      public System.Nullable<System.DateTime> AlertEmailScheduledOn { get; set; }
    }

    [Serializable]
    public partial class EmployerAddressDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Line1 { get; set; }
      public string Line2 { get; set; }
      public string Line3 { get; set; }
      public string TownCity { get; set; }
      public System.Nullable<long> CountyId { get; set; }
      public string PostcodeZip { get; set; }
      public long StateId { get; set; }
      public long CountryId { get; set; }
      public bool IsPrimary { get; set; }
      public bool PublicTransitAccessible { get; set; }
      public long EmployerId { get; set; }
    }

    [Serializable]
    public partial class JobAddressDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Line1 { get; set; }
      public string Line2 { get; set; }
      public string Line3 { get; set; }
      public string TownCity { get; set; }
      public System.Nullable<long> CountyId { get; set; }
      public string PostcodeZip { get; set; }
      public long StateId { get; set; }
      public long CountryId { get; set; }
      public bool IsPrimary { get; set; }
      public long JobId { get; set; }
    }

    [Serializable]
    public partial class RoleDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Key { get; set; }
      public string Value { get; set; }
    }

    [Serializable]
    public partial class PersonAddressDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Line1 { get; set; }
      public string Line2 { get; set; }
      public string Line3 { get; set; }
      public string TownCity { get; set; }
      public System.Nullable<long> CountyId { get; set; }
      public string PostcodeZip { get; set; }
      public long StateId { get; set; }
      public long CountryId { get; set; }
      public bool IsPrimary { get; set; }
      public long PersonId { get; set; }
    }

    [Serializable]
    public partial class ActionTypeDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Name { get; set; }
    }

    [Serializable]
    public partial class OnetDto
    {
      public System.Nullable<long> Id { get; set; }
      public string OnetCode { get; set; }
      public string Key { get; set; }
      public long JobFamilyId { get; set; }
      public int JobZone { get; set; }
    }

    [Serializable]
    public partial class EmailTemplateDto
    {
      public System.Nullable<long> Id { get; set; }
      public Focus.Core.EmailTemplateTypes EmailTemplateType { get; set; }
      public string Subject { get; set; }
      public string Body { get; set; }
      public string Salutation { get; set; }
    }

    [Serializable]
    public partial class JobLocationDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Location { get; set; }
      public bool IsPublicTransitAccessible { get; set; }
      public long JobId { get; set; }
    }

    [Serializable]
    public partial class OnetTaskDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Key { get; set; }
      public long OnetId { get; set; }
    }

    [Serializable]
    public partial class CertificateLicenceDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Key { get; set; }
      public Focus.Core.CertifcateLicenseTypes CertificateLicenseType { get; set; }
      public string OnetParentCode { get; set; }
      public string IsCertificate { get; set; }
      public string IsLicence { get; set; }
    }

    [Serializable]
    public partial class JobTaskDto
    {
      public System.Nullable<long> Id { get; set; }
      public Focus.Core.JobTaskTypes JobTaskType { get; set; }
      public string Key { get; set; }
      public long OnetId { get; set; }
    }

    [Serializable]
    public partial class JobTaskMultiOptionDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Key { get; set; }
      public long JobTaskId { get; set; }
    }

    [Serializable]
    public partial class JobLicenceDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Licence { get; set; }
      public long JobId { get; set; }
    }

    [Serializable]
    public partial class JobCertificateDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Certificate { get; set; }
      public long JobId { get; set; }
    }

    [Serializable]
    public partial class JobLanguageDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Language { get; set; }
      public long JobId { get; set; }
    }

    [Serializable]
    public partial class ProfanityDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Phrase { get; set; }
      public Focus.Core.ProfanityLevelTypes ProfanityLevel { get; set; }
    }

    [Serializable]
    public partial class OnetLocalisationItemDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Key { get; set; }
      public string PrimaryValue { get; set; }
      public string SecondaryValue { get; set; }
      public long LocalisationId { get; set; }
    }

    [Serializable]
    public partial class JobTaskLocalisationItemDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Key { get; set; }
      public string PrimaryValue { get; set; }
      public string SecondaryValue { get; set; }
      public long LocalisationId { get; set; }
    }

    [Serializable]
    public partial class CertificateLicenceLocalisationItemDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Key { get; set; }
      public string Value { get; set; }
      public long LocalisationId { get; set; }
    }

    [Serializable]
    public partial class OnetWordDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Key { get; set; }
      public string Word { get; set; }
      public string Stem { get; set; }
      public System.Nullable<int> Frequency { get; set; }
      public string OnetSoc { get; set; }
      public long OnetId { get; set; }
      public long OnetRingId { get; set; }
    }

    [Serializable]
    public partial class OnetRingDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Name { get; set; }
      public int Weighting { get; set; }
      public int RingMax { get; set; }
    }

    [Serializable]
    public partial class OnetPhraseDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Phrase { get; set; }
      public System.Nullable<int> Equivalent { get; set; }
      public string OnetSoc { get; set; }
      public long OnetId { get; set; }
    }

    [Serializable]
    public partial class PhoneNumberDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Number { get; set; }
      public Focus.Core.PhoneTypes PhoneType { get; set; }
      public bool IsPrimary { get; set; }
      public string Extension { get; set; }
      public long PersonId { get; set; }
    }

    [Serializable]
    public partial class JobViewDto
    {
      public System.Nullable<long> Id { get; set; }
      public System.DateTime UpdatedOn { get; set; }
      public long EmployerId { get; set; }
      public string JobTitle { get; set; }
      public Focus.Core.JobStatuses JobStatus { get; set; }
      public int ApplicationCount { get; set; }
      public System.Nullable<System.DateTime> PostedOn { get; set; }
      public System.Nullable<System.DateTime> ClosingOn { get; set; }
      public System.Nullable<System.DateTime> HeldOn { get; set; }
      public System.Nullable<System.DateTime> ClosedOn { get; set; }
      public string BusinessUnitName { get; set; }
      public System.Nullable<long> EmployeeId { get; set; }
      public Focus.Core.ApprovalStatuses ApprovalStatus { get; set; }
    }

    [Serializable]
    public partial class SessionDto
    {
      public System.Nullable<System.Guid> Id { get; set; }
      public long UserId { get; set; }
      public System.DateTime LastRequestOn { get; set; }
    }

    [Serializable]
    public partial class SessionStateDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Key { get; set; }
      public byte[] Value { get; set; }
      public string TypeOf { get; set; }
      public long Size { get; set; }
      public System.Guid SessionId { get; set; }
    }

    [Serializable]
    public partial class MessageDto
    {
      public System.Nullable<long> Id { get; set; }
      public System.DateTime CreatedOn { get; set; }
      public bool IsSystemAlert { get; set; }
      public System.Nullable<long> EmployerId { get; set; }
      public System.DateTime ExpiresOn { get; set; }
      public System.Nullable<Focus.Core.MessageAudiences> Audience { get; set; }
      public System.Nullable<Focus.Core.MessageTypes> MessageType { get; set; }
      public System.Nullable<long> EntityId { get; set; }
    }

    [Serializable]
    public partial class DismissedMessageDto
    {
      public System.Nullable<long> Id { get; set; }
      public long UserId { get; set; }
      public long MessageId { get; set; }
    }

    [Serializable]
    public partial class MessageTextDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Text { get; set; }
      public long LocalisationId { get; set; }
      public long MessageId { get; set; }
    }

    [Serializable]
    public partial class CandidateDto
    {
      public System.Nullable<long> Id { get; set; }
      public System.DateTime CreatedOn { get; set; }
      public System.DateTime UpdatedOn { get; set; }
      public string UserName { get; set; }
      public string FirstName { get; set; }
      public string LastName { get; set; }
      public string PhoneNumber { get; set; }
      public string EmailAddress { get; set; }
      public string ExternalId { get; set; }
    }

    [Serializable]
    public partial class EmployeeSearchViewDto
    {
      public System.Nullable<long> Id { get; set; }
      public string FirstName { get; set; }
      public string LastName { get; set; }
      public long EmployerId { get; set; }
      public string EmployerName { get; set; }
      public string IndustrialClassification { get; set; }
      public string FederalEmployerIdentificationNumber { get; set; }
      public System.Nullable<Focus.Core.WorkOpportunitiesTaxCreditCategories> WorkOpportunitiesTaxCreditHires { get; set; }
      public System.Nullable<Focus.Core.JobPostingFlags> PostingFlags { get; set; }
      public System.Nullable<Focus.Core.ScreeningPreferences> ScreeningPreferences { get; set; }
      public string JobTitle { get; set; }
      public System.Nullable<System.DateTime> EmployerCreatedOn { get; set; }
      public string EmailAddress { get; set; }
      public string PhoneNumber { get; set; }
      public bool AccountEnabled { get; set; }
      public Focus.Core.ApprovalStatuses EmployerApprovalStatus { get; set; }
      public Focus.Core.ApprovalStatuses EmployeeApprovalStatus { get; set; }
    }

    [Serializable]
    public partial class SavedMessageDto
    {
      public System.Nullable<long> Id { get; set; }
      public Focus.Core.MessageAudiences Audience { get; set; }
      public string Name { get; set; }
      public long UserId { get; set; }
    }

    [Serializable]
    public partial class SavedMessageTextDto
    {
      public System.Nullable<long> Id { get; set; }
      public long LocalisationId { get; set; }
      public string Text { get; set; }
      public long SavedMessageId { get; set; }
    }

    [Serializable]
    public partial class JobSeekerReferralViewDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Name { get; set; }
      public System.DateTime ReferralDate { get; set; }
      public string JobTitle { get; set; }
      public string EmployerName { get; set; }
      public System.Nullable<int> TimeInQueue { get; set; }
      public long EmployerId { get; set; }
      public string Posting { get; set; }
      public long CandidateId { get; set; }
      public long JobId { get; set; }
      public string CandidateExternalId { get; set; }
      public string ApprovalRequiredReason { get; set; }
    }

    [Serializable]
    public partial class EmployerAccountReferralViewDto
    {
      public System.Nullable<long> Id { get; set; }
      public long EmployerId { get; set; }
      public long EmployeeId { get; set; }
      public string EmployeeFirstName { get; set; }
      public string EmployeeLastName { get; set; }
      public System.DateTime AccountCreationDate { get; set; }
      public System.Nullable<int> TimeInQueue { get; set; }
      public string EmployerName { get; set; }
      public string EmployerIndustrialClassification { get; set; }
      public string EmployerPhoneNumber { get; set; }
      public string EmployerFederalEmployerIdentificationNumber { get; set; }
      public string EmployerStateEmployerIdentificationNumber { get; set; }
      public string EmployerFaxNumber { get; set; }
      public string EmployerUrl { get; set; }
      public string EmployeeAddressLine1 { get; set; }
      public string EmployeeAddressTownCity { get; set; }
      public System.Nullable<long> EmployeeAddressStateId { get; set; }
      public string EmployeeAddressPostcodeZip { get; set; }
      public string EmployeeEmailAddress { get; set; }
      public string EmployeePhoneNumber { get; set; }
      public string EmployeeFaxNumber { get; set; }
      public string EmployerAddressLine1 { get; set; }
      public string EmployerAddressTownCity { get; set; }
      public System.Nullable<long> EmployerAddressStateId { get; set; }
      public string EmployerAddressPostcodeZip { get; set; }
    }

    [Serializable]
    public partial class JobPostingReferralViewDto
    {
      public System.Nullable<long> Id { get; set; }
      public string JobTitle { get; set; }
      public long EmployerId { get; set; }
      public string EmployerName { get; set; }
      public System.Nullable<int> TimeInQueue { get; set; }
      public string EmployeeFirstName { get; set; }
      public string EmployeeLastName { get; set; }
      public System.Nullable<long> EmployeeId { get; set; }
      public System.Nullable<System.DateTime> AwaitingApprovalOn { get; set; }
    }

    [Serializable]
    public partial class PersonListDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Name { get; set; }
      public Focus.Core.ListTypes ListType { get; set; }
      public long PersonId { get; set; }
    }

    [Serializable]
    public partial class ApplicationImageDto
    {
      public System.Nullable<long> Id { get; set; }
      public Focus.Core.ApplicationImageTypes Type { get; set; }
      public byte[] Image { get; set; }
    }

    [Serializable]
    public partial class StaffSearchViewDto
    {
      public System.Nullable<long> Id { get; set; }
      public string FirstName { get; set; }
      public string LastName { get; set; }
      public bool Enabled { get; set; }
      public string EmailAddress { get; set; }
      public string Office { get; set; }
    }

    [Serializable]
    public partial class SingleSignOnDto
    {
      public System.Nullable<System.Guid> Id { get; set; }
      public System.DateTime CreatedOn { get; set; }
      public long UserId { get; set; }
      public long ActionerId { get; set; }
    }

    [Serializable]
    public partial class JobTaskViewDto
    {
      public System.Nullable<long> Id { get; set; }
      public Focus.Core.JobTaskTypes JobTaskType { get; set; }
      public long OnetId { get; set; }
      public string Culture { get; set; }
      public string Prompt { get; set; }
      public string Response { get; set; }
    }

    [Serializable]
    public partial class JobTaskMultiOptionViewDto
    {
      public System.Nullable<long> Id { get; set; }
      public long OnetId { get; set; }
      public string Culture { get; set; }
      public string Prompt { get; set; }
      public long JobTaskId { get; set; }
    }

    [Serializable]
    public partial class OnetViewDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Key { get; set; }
      public string Occupation { get; set; }
      public long JobFamilyId { get; set; }
      public string JobFamilyKey { get; set; }
      public string OnetCode { get; set; }
      public string Culture { get; set; }
      public string Description { get; set; }
    }

    [Serializable]
    public partial class CandidateNoteDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Note { get; set; }
      public long EmployerId { get; set; }
      public long CandidateId { get; set; }
    }

    [Serializable]
    public partial class NAICSDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Code { get; set; }
      public string Name { get; set; }
    }

    [Serializable]
    public partial class ApplicationViewDto
    {
      public System.Nullable<long> Id { get; set; }
      public long JobId { get; set; }
      public long EmployerId { get; set; }
      public long CandidateId { get; set; }
      public Focus.Core.ApplicationStatusTypes CandidateApplicationStatus { get; set; }
      public int CandidateApplicationScore { get; set; }
      public string CandidateExternalId { get; set; }
      public System.DateTime CandidateApplicationReceivedOn { get; set; }
      public Focus.Core.ApprovalStatuses CandidateApplicationApprovalStatus { get; set; }
      public string CandidateFirstName { get; set; }
      public string CandidateLastName { get; set; }
    }

    [Serializable]
    public partial class LookupItemsViewDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Key { get; set; }
      public string LookupType { get; set; }
      public string Value { get; set; }
      public int DisplayOrder { get; set; }
      public long ParentId { get; set; }
      public string ParentKey { get; set; }
      public string Culture { get; set; }
      public string ExternalId { get; set; }
    }

    [Serializable]
    public partial class CandidateNoteViewDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Note { get; set; }
      public long EmployerId { get; set; }
      public long CandidateId { get; set; }
      public string CandidateExternalId { get; set; }
    }

    [Serializable]
    public partial class PersonListCandidateViewDto
    {
      public System.Nullable<long> Id { get; set; }
      public Focus.Core.ListTypes ListType { get; set; }
      public long PersonId { get; set; }
      public long CandidateId { get; set; }
      public string CandidateExternalId { get; set; }
    }

    [Serializable]
    public partial class JobTitleDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Value { get; set; }
    }

    [Serializable]
    public partial class OnetTaskViewDto
    {
      public System.Nullable<long> Id { get; set; }
      public long OnetId { get; set; }
      public string TaskKey { get; set; }
      public string Task { get; set; }
      public string Culture { get; set; }
    }

    [Serializable]
    public partial class PostalCodeDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Code { get; set; }
      public double Longitude { get; set; }
      public double Latitude { get; set; }
      public string CountryKey { get; set; }
      public string StateKey { get; set; }
      public string CountyKey { get; set; }
      public string CityName { get; set; }
      public string OfficeKey { get; set; }
      public string WibLocationKey { get; set; }
    }

    [Serializable]
    public partial class PostalCodeViewDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Code { get; set; }
      public double Longitude { get; set; }
      public double Latitude { get; set; }
      public string CountryKey { get; set; }
      public string CountryName { get; set; }
      public string StateKey { get; set; }
      public string StateName { get; set; }
      public string Culture { get; set; }
      public string CountyKey { get; set; }
      public string CountyName { get; set; }
      public string CityName { get; set; }
    }

    [Serializable]
    public partial class ExpiringJobViewDto
    {
      public System.Nullable<long> Id { get; set; }
      public string JobTitle { get; set; }
      public long EmployerId { get; set; }
      public System.Nullable<long> EmployeeId { get; set; }
      public System.Nullable<System.DateTime> ClosingOn { get; set; }
      public System.Nullable<int> DaysToClosing { get; set; }
      public System.Nullable<System.DateTime> PostedOn { get; set; }
      public long UserId { get; set; }
      public string FirstName { get; set; }
    }

    [Serializable]
    public partial class OnetWordJobTaskViewDto
    {
      public System.Nullable<long> Id { get; set; }
      public long OnetId { get; set; }
      public long OnetRingId { get; set; }
      public string OnetSoc { get; set; }
      public string Stem { get; set; }
      public string Word { get; set; }
      public long JobFamilyId { get; set; }
      public int JobZone { get; set; }
      public string OnetKey { get; set; }
      public string OnetCode { get; set; }
      public System.Nullable<int> Frequency { get; set; }
    }

    [Serializable]
    public partial class OnetWordViewDto
    {
      public System.Nullable<long> Id { get; set; }
      public long OnetId { get; set; }
      public long OnetRingId { get; set; }
      public string OnetSoc { get; set; }
      public string Stem { get; set; }
      public string Word { get; set; }
      public long JobFamilyId { get; set; }
      public int JobZone { get; set; }
      public string OnetKey { get; set; }
      public string OnetCode { get; set; }
      public System.Nullable<int> Frequency { get; set; }
    }

    [Serializable]
    public partial class BookmarkDto
    {
      public System.Nullable<long> Id { get; set; }
      public string Name { get; set; }
      public Focus.Core.BookmarkTypes Type { get; set; }
      public string Criteria { get; set; }
      public System.Nullable<long> UserId { get; set; }
      public Focus.Core.UserTypes UserType { get; set; }
    }

    [Serializable]
    public partial class PersonResumeDto
    {
      public System.Nullable<long> Id { get; set; }
      public string FileName { get; set; }
      public string ContentType { get; set; }
      public byte[] Resume { get; set; }
      public string PrimaryOnet { get; set; }
      public string PrimaryROnet { get; set; }
      public long PersonId { get; set; }
    }

    [Serializable]
    public partial class UserRoleDto
    {
      public System.Nullable<long> Id { get; set; }
      public long UserId { get; set; }
      public long RoleId { get; set; }
    }

    [Serializable]
    public partial class EmployeeCandidateDto
    {
      public System.Nullable<long> Id { get; set; }
      public long EmployeeId { get; set; }
      public long CandidateId { get; set; }
    }

    [Serializable]
    public partial class JobNoteDto
    {
      public System.Nullable<long> Id { get; set; }
      public long JobId { get; set; }
      public long NoteId { get; set; }
    }

    [Serializable]
    public partial class SavedSearchEmployeeDto
    {
      public System.Nullable<long> Id { get; set; }
      public long EmployeeId { get; set; }
      public long SavedSearchId { get; set; }
    }

    [Serializable]
    public partial class PersonListCandidateDto
    {
      public System.Nullable<long> Id { get; set; }
      public long CandidateId { get; set; }
      public long PersonListId { get; set; }
    }


    public static partial class DtoExtensions
    {
      public static ActionEventDto AsDto(this ActionEvent entity)
      {
        var dto = new ActionEventDto();
				dto.Id = entity.Id;
        dto.SessionId = entity.SessionId;
        dto.RequestId = entity.RequestId;
        dto.UserId = entity.UserId;
        dto.ActionedOn = entity.ActionedOn;
        dto.EntityId = entity.EntityId;
        dto.EntityIdAdditional01 = entity.EntityIdAdditional01;
        dto.EntityIdAdditional02 = entity.EntityIdAdditional02;
        dto.AdditionalDetails = entity.AdditionalDetails;
        dto.EntityTypeId = entity.EntityTypeId;
        dto.ActionTypeId = entity.ActionTypeId;
        return dto;
      }
      
      public static ActionEvent CopyTo(this ActionEventDto dto, ActionEvent entity)
      {
        entity.SessionId = dto.SessionId;
        entity.RequestId = dto.RequestId;
        entity.UserId = dto.UserId;
        entity.ActionedOn = dto.ActionedOn;
        entity.EntityId = dto.EntityId;
        entity.EntityIdAdditional01 = dto.EntityIdAdditional01;
        entity.EntityIdAdditional02 = dto.EntityIdAdditional02;
        entity.AdditionalDetails = dto.AdditionalDetails;
        entity.EntityTypeId = dto.EntityTypeId;
        entity.ActionTypeId = dto.ActionTypeId;
        return entity;
      }

      public static ActionEvent CopyTo(this ActionEventDto dto)
      {
        var entity = new ActionEvent();
        return dto.CopyTo(entity);
      }	

      public static UserDto AsDto(this User entity)
      {
        var dto = new UserDto();
				dto.Id = entity.Id;
				dto.CreatedOn =  entity.CreatedOn;
				dto.UpdatedOn =  entity.UpdatedOn;
				dto.DeletedOn =  entity.DeletedOn;
        dto.UserName = entity.UserName;
        dto.PasswordHash = entity.PasswordHash;
        dto.PasswordSalt = entity.PasswordSalt;
        dto.Enabled = entity.Enabled;
        dto.ValidationKey = entity.ValidationKey;
        dto.UserType = entity.UserType;
        dto.LoggedInOn = entity.LoggedInOn;
        dto.LastLoggedInOn = entity.LastLoggedInOn;
        dto.ExternalId = entity.ExternalId;
        dto.IsClientAuthenticated = entity.IsClientAuthenticated;
        dto.ScreenName = entity.ScreenName;
        dto.SecurityQuestion = entity.SecurityQuestion;
        dto.SecurityAnswerHash = entity.SecurityAnswerHash;
        dto.PersonId = entity.PersonId;
        return dto;
      }
      
      public static User CopyTo(this UserDto dto, User entity)
      {
        entity.UserName = dto.UserName;
        entity.PasswordHash = dto.PasswordHash;
        entity.PasswordSalt = dto.PasswordSalt;
        entity.Enabled = dto.Enabled;
        entity.ValidationKey = dto.ValidationKey;
        entity.UserType = dto.UserType;
        entity.LoggedInOn = dto.LoggedInOn;
        entity.LastLoggedInOn = dto.LastLoggedInOn;
        entity.ExternalId = dto.ExternalId;
        entity.IsClientAuthenticated = dto.IsClientAuthenticated;
        entity.ScreenName = dto.ScreenName;
        entity.SecurityQuestion = dto.SecurityQuestion;
        entity.SecurityAnswerHash = dto.SecurityAnswerHash;
        entity.PersonId = dto.PersonId;
        return entity;
      }

      public static User CopyTo(this UserDto dto)
      {
        var entity = new User();
        return dto.CopyTo(entity);
      }	

      public static BusinessUnitDto AsDto(this BusinessUnit entity)
      {
        var dto = new BusinessUnitDto();
				dto.Id = entity.Id;
        dto.Name = entity.Name;
        dto.IsPrimary = entity.IsPrimary;
        dto.EmployerId = entity.EmployerId;
        return dto;
      }
      
      public static BusinessUnit CopyTo(this BusinessUnitDto dto, BusinessUnit entity)
      {
        entity.Name = dto.Name;
        entity.IsPrimary = dto.IsPrimary;
        entity.EmployerId = dto.EmployerId;
        return entity;
      }

      public static BusinessUnit CopyTo(this BusinessUnitDto dto)
      {
        var entity = new BusinessUnit();
        return dto.CopyTo(entity);
      }	

      public static CodeGroupDto AsDto(this CodeGroup entity)
      {
        var dto = new CodeGroupDto();
				dto.Id = entity.Id;
        dto.Key = entity.Key;
        return dto;
      }
      
      public static CodeGroup CopyTo(this CodeGroupDto dto, CodeGroup entity)
      {
        entity.Key = dto.Key;
        return entity;
      }

      public static CodeGroup CopyTo(this CodeGroupDto dto)
      {
        var entity = new CodeGroup();
        return dto.CopyTo(entity);
      }	

      public static CodeGroupItemDto AsDto(this CodeGroupItem entity)
      {
        var dto = new CodeGroupItemDto();
				dto.Id = entity.Id;
        dto.DisplayOrder = entity.DisplayOrder;
        dto.CodeGroupId = entity.CodeGroupId;
        dto.CodeItemId = entity.CodeItemId;
        return dto;
      }
      
      public static CodeGroupItem CopyTo(this CodeGroupItemDto dto, CodeGroupItem entity)
      {
        entity.DisplayOrder = dto.DisplayOrder;
        entity.CodeGroupId = dto.CodeGroupId;
        entity.CodeItemId = dto.CodeItemId;
        return entity;
      }

      public static CodeGroupItem CopyTo(this CodeGroupItemDto dto)
      {
        var entity = new CodeGroupItem();
        return dto.CopyTo(entity);
      }	

      public static CodeItemDto AsDto(this CodeItem entity)
      {
        var dto = new CodeItemDto();
				dto.Id = entity.Id;
        dto.Key = entity.Key;
        dto.IsSystem = entity.IsSystem;
        dto.ParentKey = entity.ParentKey;
        dto.ExternalId = entity.ExternalId;
        return dto;
      }
      
      public static CodeItem CopyTo(this CodeItemDto dto, CodeItem entity)
      {
        entity.Key = dto.Key;
        entity.IsSystem = dto.IsSystem;
        entity.ParentKey = dto.ParentKey;
        entity.ExternalId = dto.ExternalId;
        return entity;
      }

      public static CodeItem CopyTo(this CodeItemDto dto)
      {
        var entity = new CodeItem();
        return dto.CopyTo(entity);
      }	

      public static ConfigurationItemDto AsDto(this ConfigurationItem entity)
      {
        var dto = new ConfigurationItemDto();
				dto.Id = entity.Id;
        dto.Key = entity.Key;
        dto.Value = entity.Value;
        return dto;
      }
      
      public static ConfigurationItem CopyTo(this ConfigurationItemDto dto, ConfigurationItem entity)
      {
        entity.Key = dto.Key;
        entity.Value = dto.Value;
        return entity;
      }

      public static ConfigurationItem CopyTo(this ConfigurationItemDto dto)
      {
        var entity = new ConfigurationItem();
        return dto.CopyTo(entity);
      }	

      public static EmployeeDto AsDto(this Employee entity)
      {
        var dto = new EmployeeDto();
				dto.Id = entity.Id;
        dto.ApprovalStatus = entity.ApprovalStatus;
        dto.EmployerId = entity.EmployerId;
        dto.PersonId = entity.PersonId;
        return dto;
      }
      
      public static Employee CopyTo(this EmployeeDto dto, Employee entity)
      {
        entity.ApprovalStatus = dto.ApprovalStatus;
        entity.EmployerId = dto.EmployerId;
        entity.PersonId = dto.PersonId;
        return entity;
      }

      public static Employee CopyTo(this EmployeeDto dto)
      {
        var entity = new Employee();
        return dto.CopyTo(entity);
      }	

      public static EmployerDto AsDto(this Employer entity)
      {
        var dto = new EmployerDto();
				dto.Id = entity.Id;
				dto.CreatedOn =  entity.CreatedOn;
				dto.UpdatedOn =  entity.UpdatedOn;
        dto.Name = entity.Name;
        dto.CommencedOn = entity.CommencedOn;
        dto.FederalEmployerIdentificationNumber = entity.FederalEmployerIdentificationNumber;
        dto.IsValidFederalEmployerIdentificationNumber = entity.IsValidFederalEmployerIdentificationNumber;
        dto.StateEmployerIdentificationNumber = entity.StateEmployerIdentificationNumber;
        dto.ApprovalStatus = entity.ApprovalStatus;
        dto.Url = entity.Url;
        dto.ExpiredOn = entity.ExpiredOn;
        dto.OwnershipTypeId = entity.OwnershipTypeId;
        dto.IndustrialClassification = entity.IndustrialClassification;
        dto.TermsAccepted = entity.TermsAccepted;
        dto.PrimaryPhone = entity.PrimaryPhone;
        dto.PrimaryPhoneExtension = entity.PrimaryPhoneExtension;
        dto.PrimaryPhoneType = entity.PrimaryPhoneType;
        dto.AlternatePhone1 = entity.AlternatePhone1;
        dto.AlternatePhone1Type = entity.AlternatePhone1Type;
        dto.AlternatePhone2 = entity.AlternatePhone2;
        dto.AlternatePhone2Type = entity.AlternatePhone2Type;
        dto.IsRegistrationComplete = entity.IsRegistrationComplete;
        return dto;
      }
      
      public static Employer CopyTo(this EmployerDto dto, Employer entity)
      {
        entity.Name = dto.Name;
        entity.CommencedOn = dto.CommencedOn;
        entity.FederalEmployerIdentificationNumber = dto.FederalEmployerIdentificationNumber;
        entity.IsValidFederalEmployerIdentificationNumber = dto.IsValidFederalEmployerIdentificationNumber;
        entity.StateEmployerIdentificationNumber = dto.StateEmployerIdentificationNumber;
        entity.ApprovalStatus = dto.ApprovalStatus;
        entity.Url = dto.Url;
        entity.ExpiredOn = dto.ExpiredOn;
        entity.OwnershipTypeId = dto.OwnershipTypeId;
        entity.IndustrialClassification = dto.IndustrialClassification;
        entity.TermsAccepted = dto.TermsAccepted;
        entity.PrimaryPhone = dto.PrimaryPhone;
        entity.PrimaryPhoneExtension = dto.PrimaryPhoneExtension;
        entity.PrimaryPhoneType = dto.PrimaryPhoneType;
        entity.AlternatePhone1 = dto.AlternatePhone1;
        entity.AlternatePhone1Type = dto.AlternatePhone1Type;
        entity.AlternatePhone2 = dto.AlternatePhone2;
        entity.AlternatePhone2Type = dto.AlternatePhone2Type;
        entity.IsRegistrationComplete = dto.IsRegistrationComplete;
        return entity;
      }

      public static Employer CopyTo(this EmployerDto dto)
      {
        var entity = new Employer();
        return dto.CopyTo(entity);
      }	

      public static EmployerDescriptionDto AsDto(this EmployerDescription entity)
      {
        var dto = new EmployerDescriptionDto();
				dto.Id = entity.Id;
        dto.Title = entity.Title;
        dto.Description = entity.Description;
        dto.IsPrimary = entity.IsPrimary;
        dto.EmployerId = entity.EmployerId;
        return dto;
      }
      
      public static EmployerDescription CopyTo(this EmployerDescriptionDto dto, EmployerDescription entity)
      {
        entity.Title = dto.Title;
        entity.Description = dto.Description;
        entity.IsPrimary = dto.IsPrimary;
        entity.EmployerId = dto.EmployerId;
        return entity;
      }

      public static EmployerDescription CopyTo(this EmployerDescriptionDto dto)
      {
        var entity = new EmployerDescription();
        return dto.CopyTo(entity);
      }	

      public static EmployerLogoDto AsDto(this EmployerLogo entity)
      {
        var dto = new EmployerLogoDto();
				dto.Id = entity.Id;
        dto.Name = entity.Name;
        dto.Logo = entity.Logo;
        dto.EmployerId = entity.EmployerId;
        return dto;
      }
      
      public static EmployerLogo CopyTo(this EmployerLogoDto dto, EmployerLogo entity)
      {
        entity.Name = dto.Name;
        entity.Logo = dto.Logo;
        entity.EmployerId = dto.EmployerId;
        return entity;
      }

      public static EmployerLogo CopyTo(this EmployerLogoDto dto)
      {
        var entity = new EmployerLogo();
        return dto.CopyTo(entity);
      }	

      public static EntityTypeDto AsDto(this EntityType entity)
      {
        var dto = new EntityTypeDto();
				dto.Id = entity.Id;
        dto.Name = entity.Name;
        return dto;
      }
      
      public static EntityType CopyTo(this EntityTypeDto dto, EntityType entity)
      {
        entity.Name = dto.Name;
        return entity;
      }

      public static EntityType CopyTo(this EntityTypeDto dto)
      {
        var entity = new EntityType();
        return dto.CopyTo(entity);
      }	

      public static JobDto AsDto(this Job entity)
      {
        var dto = new JobDto();
				dto.Id = entity.Id;
				dto.LockVersion =  entity.LockVersion;
				dto.CreatedOn =  entity.CreatedOn;
				dto.UpdatedOn =  entity.UpdatedOn;
        dto.JobTitle = entity.JobTitle;
        dto.CreatedBy = entity.CreatedBy;
        dto.UpdatedBy = entity.UpdatedBy;
        dto.ApprovalStatus = entity.ApprovalStatus;
        dto.JobStatus = entity.JobStatus;
        dto.MinSalary = entity.MinSalary;
        dto.MaxSalary = entity.MaxSalary;
        dto.SalaryFrequencyId = entity.SalaryFrequencyId;
        dto.HoursPerWeek = entity.HoursPerWeek;
        dto.OverTimeRequired = entity.OverTimeRequired;
        dto.ClosingOn = entity.ClosingOn;
        dto.NumberOfOpenings = entity.NumberOfOpenings;
        dto.PostedOn = entity.PostedOn;
        dto.PostedBy = entity.PostedBy;
        dto.Description = entity.Description;
        dto.Posting = entity.Posting;
        dto.EmployerDescriptionPostingPosition = entity.EmployerDescriptionPostingPosition;
        dto.WizardStep = entity.WizardStep;
        dto.WizardPath = entity.WizardPath;
        dto.HeldOn = entity.HeldOn;
        dto.HeldBy = entity.HeldBy;
        dto.ClosedOn = entity.ClosedOn;
        dto.ClosedBy = entity.ClosedBy;
        dto.JobLocationType = entity.JobLocationType;
        dto.FederalContractor = entity.FederalContractor;
        dto.ForeignLabourCertification = entity.ForeignLabourCertification;
        dto.CourtOrderedAffirmativeAction = entity.CourtOrderedAffirmativeAction;
        dto.FederalContractorExpiresOn = entity.FederalContractorExpiresOn;
        dto.WorkOpportunitiesTaxCreditHires = entity.WorkOpportunitiesTaxCreditHires;
        dto.PostingFlags = entity.PostingFlags;
        dto.IsCommissionBased = entity.IsCommissionBased;
        dto.NormalWorkDays = entity.NormalWorkDays;
        dto.WorkDaysVary = entity.WorkDaysVary;
        dto.NormalWorkShiftsId = entity.NormalWorkShiftsId;
        dto.LeaveBenefits = entity.LeaveBenefits;
        dto.RetirementBenefits = entity.RetirementBenefits;
        dto.InsuranceBenefits = entity.InsuranceBenefits;
        dto.MiscellaneousBenefits = entity.MiscellaneousBenefits;
        dto.OtherBenefitsDetails = entity.OtherBenefitsDetails;
        dto.InterviewContactPreferences = entity.InterviewContactPreferences;
        dto.InterviewEmailAddress = entity.InterviewEmailAddress;
        dto.InterviewApplicationUrl = entity.InterviewApplicationUrl;
        dto.InterviewMailAddress = entity.InterviewMailAddress;
        dto.InterviewFaxNumber = entity.InterviewFaxNumber;
        dto.InterviewPhoneNumber = entity.InterviewPhoneNumber;
        dto.InterviewDirectApplicationDetails = entity.InterviewDirectApplicationDetails;
        dto.InterviewOtherInstructions = entity.InterviewOtherInstructions;
        dto.ScreeningPreferences = entity.ScreeningPreferences;
        dto.MinimumEducationLevel = entity.MinimumEducationLevel;
        dto.MinimumEducationLevelRequired = entity.MinimumEducationLevelRequired;
        dto.MinimumAge = entity.MinimumAge;
        dto.MinimumAgeReason = entity.MinimumAgeReason;
        dto.MinimumAgeRequired = entity.MinimumAgeRequired;
        dto.LicencesRequired = entity.LicencesRequired;
        dto.CertificationRequired = entity.CertificationRequired;
        dto.LanguagesRequired = entity.LanguagesRequired;
        dto.Tasks = entity.Tasks;
        dto.RedProfanityWords = entity.RedProfanityWords;
        dto.YellowProfanityWords = entity.YellowProfanityWords;
        dto.EmploymentStatusId = entity.EmploymentStatusId;
        dto.JobTypeId = entity.JobTypeId;
        dto.JobStatusId = entity.JobStatusId;
        dto.ApprovedOn = entity.ApprovedOn;
        dto.ApprovedBy = entity.ApprovedBy;
        dto.ExternalId = entity.ExternalId;
        dto.AwaitingApprovalOn = entity.AwaitingApprovalOn;
        dto.MinimumExperience = entity.MinimumExperience;
        dto.MinimumExperienceRequired = entity.MinimumExperienceRequired;
        dto.DrivingLicenceClassId = entity.DrivingLicenceClassId;
        dto.DrivingLicenceEndorsements = entity.DrivingLicenceEndorsements;
        dto.DrivingLicenceRequired = entity.DrivingLicenceRequired;
        dto.HideSalaryOnPosting = entity.HideSalaryOnPosting;
        dto.BusinessUnitId = entity.BusinessUnitId;
        dto.EmployeeId = entity.EmployeeId;
        dto.EmployerId = entity.EmployerId;
        dto.EmployerDescriptionId = entity.EmployerDescriptionId;
        dto.EmployerLogoId = entity.EmployerLogoId;
        dto.OnetId = entity.OnetId;
        return dto;
      }
      
      public static Job CopyTo(this JobDto dto, Job entity)
      {
        entity.JobTitle = dto.JobTitle;
        entity.CreatedBy = dto.CreatedBy;
        entity.UpdatedBy = dto.UpdatedBy;
        entity.ApprovalStatus = dto.ApprovalStatus;
        entity.JobStatus = dto.JobStatus;
        entity.MinSalary = dto.MinSalary;
        entity.MaxSalary = dto.MaxSalary;
        entity.SalaryFrequencyId = dto.SalaryFrequencyId;
        entity.HoursPerWeek = dto.HoursPerWeek;
        entity.OverTimeRequired = dto.OverTimeRequired;
        entity.ClosingOn = dto.ClosingOn;
        entity.NumberOfOpenings = dto.NumberOfOpenings;
        entity.PostedOn = dto.PostedOn;
        entity.PostedBy = dto.PostedBy;
        entity.Description = dto.Description;
        entity.Posting = dto.Posting;
        entity.EmployerDescriptionPostingPosition = dto.EmployerDescriptionPostingPosition;
        entity.WizardStep = dto.WizardStep;
        entity.WizardPath = dto.WizardPath;
        entity.HeldOn = dto.HeldOn;
        entity.HeldBy = dto.HeldBy;
        entity.ClosedOn = dto.ClosedOn;
        entity.ClosedBy = dto.ClosedBy;
        entity.JobLocationType = dto.JobLocationType;
        entity.FederalContractor = dto.FederalContractor;
        entity.ForeignLabourCertification = dto.ForeignLabourCertification;
        entity.CourtOrderedAffirmativeAction = dto.CourtOrderedAffirmativeAction;
        entity.FederalContractorExpiresOn = dto.FederalContractorExpiresOn;
        entity.WorkOpportunitiesTaxCreditHires = dto.WorkOpportunitiesTaxCreditHires;
        entity.PostingFlags = dto.PostingFlags;
        entity.IsCommissionBased = dto.IsCommissionBased;
        entity.NormalWorkDays = dto.NormalWorkDays;
        entity.WorkDaysVary = dto.WorkDaysVary;
        entity.NormalWorkShiftsId = dto.NormalWorkShiftsId;
        entity.LeaveBenefits = dto.LeaveBenefits;
        entity.RetirementBenefits = dto.RetirementBenefits;
        entity.InsuranceBenefits = dto.InsuranceBenefits;
        entity.MiscellaneousBenefits = dto.MiscellaneousBenefits;
        entity.OtherBenefitsDetails = dto.OtherBenefitsDetails;
        entity.InterviewContactPreferences = dto.InterviewContactPreferences;
        entity.InterviewEmailAddress = dto.InterviewEmailAddress;
        entity.InterviewApplicationUrl = dto.InterviewApplicationUrl;
        entity.InterviewMailAddress = dto.InterviewMailAddress;
        entity.InterviewFaxNumber = dto.InterviewFaxNumber;
        entity.InterviewPhoneNumber = dto.InterviewPhoneNumber;
        entity.InterviewDirectApplicationDetails = dto.InterviewDirectApplicationDetails;
        entity.InterviewOtherInstructions = dto.InterviewOtherInstructions;
        entity.ScreeningPreferences = dto.ScreeningPreferences;
        entity.MinimumEducationLevel = dto.MinimumEducationLevel;
        entity.MinimumEducationLevelRequired = dto.MinimumEducationLevelRequired;
        entity.MinimumAge = dto.MinimumAge;
        entity.MinimumAgeReason = dto.MinimumAgeReason;
        entity.MinimumAgeRequired = dto.MinimumAgeRequired;
        entity.LicencesRequired = dto.LicencesRequired;
        entity.CertificationRequired = dto.CertificationRequired;
        entity.LanguagesRequired = dto.LanguagesRequired;
        entity.Tasks = dto.Tasks;
        entity.RedProfanityWords = dto.RedProfanityWords;
        entity.YellowProfanityWords = dto.YellowProfanityWords;
        entity.EmploymentStatusId = dto.EmploymentStatusId;
        entity.JobTypeId = dto.JobTypeId;
        entity.JobStatusId = dto.JobStatusId;
        entity.ApprovedOn = dto.ApprovedOn;
        entity.ApprovedBy = dto.ApprovedBy;
        entity.ExternalId = dto.ExternalId;
        entity.AwaitingApprovalOn = dto.AwaitingApprovalOn;
        entity.MinimumExperience = dto.MinimumExperience;
        entity.MinimumExperienceRequired = dto.MinimumExperienceRequired;
        entity.DrivingLicenceClassId = dto.DrivingLicenceClassId;
        entity.DrivingLicenceEndorsements = dto.DrivingLicenceEndorsements;
        entity.DrivingLicenceRequired = dto.DrivingLicenceRequired;
        entity.HideSalaryOnPosting = dto.HideSalaryOnPosting;
        entity.BusinessUnitId = dto.BusinessUnitId;
        entity.EmployeeId = dto.EmployeeId;
        entity.EmployerId = dto.EmployerId;
        entity.EmployerDescriptionId = dto.EmployerDescriptionId;
        entity.EmployerLogoId = dto.EmployerLogoId;
        entity.OnetId = dto.OnetId;
        return entity;
      }

      public static Job CopyTo(this JobDto dto)
      {
        var entity = new Job();
        return dto.CopyTo(entity);
      }	

      public static CandidateApplicationDto AsDto(this CandidateApplication entity)
      {
        var dto = new CandidateApplicationDto();
				dto.Id = entity.Id;
				dto.CreatedOn =  entity.CreatedOn;
				dto.UpdatedOn =  entity.UpdatedOn;
        dto.ApprovalStatus = entity.ApprovalStatus;
        dto.ApprovalRequiredReason = entity.ApprovalRequiredReason;
        dto.ApplicationStatus = entity.ApplicationStatus;
        dto.ApplicationScore = entity.ApplicationScore;
        dto.JobId = entity.JobId;
        dto.CandidateId = entity.CandidateId;
        return dto;
      }
      
      public static CandidateApplication CopyTo(this CandidateApplicationDto dto, CandidateApplication entity)
      {
        entity.ApprovalStatus = dto.ApprovalStatus;
        entity.ApprovalRequiredReason = dto.ApprovalRequiredReason;
        entity.ApplicationStatus = dto.ApplicationStatus;
        entity.ApplicationScore = dto.ApplicationScore;
        entity.JobId = dto.JobId;
        entity.CandidateId = dto.CandidateId;
        return entity;
      }

      public static CandidateApplication CopyTo(this CandidateApplicationDto dto)
      {
        var entity = new CandidateApplication();
        return dto.CopyTo(entity);
      }	

      public static JobSkillDto AsDto(this JobSkill entity)
      {
        var dto = new JobSkillDto();
				dto.Id = entity.Id;
        dto.SkillId = entity.SkillId;
        dto.JobId = entity.JobId;
        return dto;
      }
      
      public static JobSkill CopyTo(this JobSkillDto dto, JobSkill entity)
      {
        entity.SkillId = dto.SkillId;
        entity.JobId = dto.JobId;
        return entity;
      }

      public static JobSkill CopyTo(this JobSkillDto dto)
      {
        var entity = new JobSkill();
        return dto.CopyTo(entity);
      }	

      public static JobSpecialRequirementDto AsDto(this JobSpecialRequirement entity)
      {
        var dto = new JobSpecialRequirementDto();
				dto.Id = entity.Id;
        dto.Requirement = entity.Requirement;
        dto.JobId = entity.JobId;
        return dto;
      }
      
      public static JobSpecialRequirement CopyTo(this JobSpecialRequirementDto dto, JobSpecialRequirement entity)
      {
        entity.Requirement = dto.Requirement;
        entity.JobId = dto.JobId;
        return entity;
      }

      public static JobSpecialRequirement CopyTo(this JobSpecialRequirementDto dto)
      {
        var entity = new JobSpecialRequirement();
        return dto.CopyTo(entity);
      }	

      public static LocalisationDto AsDto(this Localisation entity)
      {
        var dto = new LocalisationDto();
				dto.Id = entity.Id;
        dto.Culture = entity.Culture;
        return dto;
      }
      
      public static Localisation CopyTo(this LocalisationDto dto, Localisation entity)
      {
        entity.Culture = dto.Culture;
        return entity;
      }

      public static Localisation CopyTo(this LocalisationDto dto)
      {
        var entity = new Localisation();
        return dto.CopyTo(entity);
      }	

      public static LocalisationItemDto AsDto(this LocalisationItem entity)
      {
        var dto = new LocalisationItemDto();
				dto.Id = entity.Id;
        dto.ContextKey = entity.ContextKey;
        dto.Key = entity.Key;
        dto.Value = entity.Value;
        dto.Localised = entity.Localised;
        dto.LocalisationId = entity.LocalisationId;
        return dto;
      }
      
      public static LocalisationItem CopyTo(this LocalisationItemDto dto, LocalisationItem entity)
      {
        entity.ContextKey = dto.ContextKey;
        entity.Key = dto.Key;
        entity.Value = dto.Value;
        entity.Localised = dto.Localised;
        entity.LocalisationId = dto.LocalisationId;
        return entity;
      }

      public static LocalisationItem CopyTo(this LocalisationItemDto dto)
      {
        var entity = new LocalisationItem();
        return dto.CopyTo(entity);
      }	

      public static NoteDto AsDto(this Note entity)
      {
        var dto = new NoteDto();
				dto.Id = entity.Id;
        dto.Title = entity.Title;
        dto.Body = entity.Body;
        return dto;
      }
      
      public static Note CopyTo(this NoteDto dto, Note entity)
      {
        entity.Title = dto.Title;
        entity.Body = dto.Body;
        return entity;
      }

      public static Note CopyTo(this NoteDto dto)
      {
        var entity = new Note();
        return dto.CopyTo(entity);
      }	

      public static PersonDto AsDto(this Person entity)
      {
        var dto = new PersonDto();
				dto.Id = entity.Id;
        dto.TitleId = entity.TitleId;
        dto.FirstName = entity.FirstName;
        dto.MiddleInitial = entity.MiddleInitial;
        dto.LastName = entity.LastName;
        dto.DateOfBirth = entity.DateOfBirth;
        dto.SocialSecurityNumber = entity.SocialSecurityNumber;
        dto.JobTitle = entity.JobTitle;
        dto.EmailAddress = entity.EmailAddress;
        dto.Office = entity.Office;
        return dto;
      }
      
      public static Person CopyTo(this PersonDto dto, Person entity)
      {
        entity.TitleId = dto.TitleId;
        entity.FirstName = dto.FirstName;
        entity.MiddleInitial = dto.MiddleInitial;
        entity.LastName = dto.LastName;
        entity.DateOfBirth = dto.DateOfBirth;
        entity.SocialSecurityNumber = dto.SocialSecurityNumber;
        entity.JobTitle = dto.JobTitle;
        entity.EmailAddress = dto.EmailAddress;
        entity.Office = dto.Office;
        return entity;
      }

      public static Person CopyTo(this PersonDto dto)
      {
        var entity = new Person();
        return dto.CopyTo(entity);
      }	

      public static PostingSurveyDto AsDto(this PostingSurvey entity)
      {
        var dto = new PostingSurveyDto();
				dto.Id = entity.Id;
        dto.SurveyType = entity.SurveyType;
        dto.SatisfactionLevel = entity.SatisfactionLevel;
        dto.DidInterview = entity.DidInterview;
        dto.DidHire = entity.DidHire;
        dto.DidOffer = entity.DidOffer;
        dto.JobId = entity.JobId;
        return dto;
      }
      
      public static PostingSurvey CopyTo(this PostingSurveyDto dto, PostingSurvey entity)
      {
        entity.SurveyType = dto.SurveyType;
        entity.SatisfactionLevel = dto.SatisfactionLevel;
        entity.DidInterview = dto.DidInterview;
        entity.DidHire = dto.DidHire;
        entity.DidOffer = dto.DidOffer;
        entity.JobId = dto.JobId;
        return entity;
      }

      public static PostingSurvey CopyTo(this PostingSurveyDto dto)
      {
        var entity = new PostingSurvey();
        return dto.CopyTo(entity);
      }	

      public static SavedSearchDto AsDto(this SavedSearch entity)
      {
        var dto = new SavedSearchDto();
				dto.Id = entity.Id;
        dto.Name = entity.Name;
        dto.SearchCriteria = entity.SearchCriteria;
        dto.AlertEmailRequired = entity.AlertEmailRequired;
        dto.AlertEmailFrequency = entity.AlertEmailFrequency;
        dto.AlertEmailFormat = entity.AlertEmailFormat;
        dto.AlertEmailAddress = entity.AlertEmailAddress;
        dto.AlertEmailScheduledOn = entity.AlertEmailScheduledOn;
        return dto;
      }
      
      public static SavedSearch CopyTo(this SavedSearchDto dto, SavedSearch entity)
      {
        entity.Name = dto.Name;
        entity.SearchCriteria = dto.SearchCriteria;
        entity.AlertEmailRequired = dto.AlertEmailRequired;
        entity.AlertEmailFrequency = dto.AlertEmailFrequency;
        entity.AlertEmailFormat = dto.AlertEmailFormat;
        entity.AlertEmailAddress = dto.AlertEmailAddress;
        entity.AlertEmailScheduledOn = dto.AlertEmailScheduledOn;
        return entity;
      }

      public static SavedSearch CopyTo(this SavedSearchDto dto)
      {
        var entity = new SavedSearch();
        return dto.CopyTo(entity);
      }	

      public static EmployerAddressDto AsDto(this EmployerAddress entity)
      {
        var dto = new EmployerAddressDto();
				dto.Id = entity.Id;
        dto.Line1 = entity.Line1;
        dto.Line2 = entity.Line2;
        dto.Line3 = entity.Line3;
        dto.TownCity = entity.TownCity;
        dto.CountyId = entity.CountyId;
        dto.PostcodeZip = entity.PostcodeZip;
        dto.StateId = entity.StateId;
        dto.CountryId = entity.CountryId;
        dto.IsPrimary = entity.IsPrimary;
        dto.PublicTransitAccessible = entity.PublicTransitAccessible;
        dto.EmployerId = entity.EmployerId;
        return dto;
      }
      
      public static EmployerAddress CopyTo(this EmployerAddressDto dto, EmployerAddress entity)
      {
        entity.Line1 = dto.Line1;
        entity.Line2 = dto.Line2;
        entity.Line3 = dto.Line3;
        entity.TownCity = dto.TownCity;
        entity.CountyId = dto.CountyId;
        entity.PostcodeZip = dto.PostcodeZip;
        entity.StateId = dto.StateId;
        entity.CountryId = dto.CountryId;
        entity.IsPrimary = dto.IsPrimary;
        entity.PublicTransitAccessible = dto.PublicTransitAccessible;
        entity.EmployerId = dto.EmployerId;
        return entity;
      }

      public static EmployerAddress CopyTo(this EmployerAddressDto dto)
      {
        var entity = new EmployerAddress();
        return dto.CopyTo(entity);
      }	

      public static JobAddressDto AsDto(this JobAddress entity)
      {
        var dto = new JobAddressDto();
				dto.Id = entity.Id;
        dto.Line1 = entity.Line1;
        dto.Line2 = entity.Line2;
        dto.Line3 = entity.Line3;
        dto.TownCity = entity.TownCity;
        dto.CountyId = entity.CountyId;
        dto.PostcodeZip = entity.PostcodeZip;
        dto.StateId = entity.StateId;
        dto.CountryId = entity.CountryId;
        dto.IsPrimary = entity.IsPrimary;
        dto.JobId = entity.JobId;
        return dto;
      }
      
      public static JobAddress CopyTo(this JobAddressDto dto, JobAddress entity)
      {
        entity.Line1 = dto.Line1;
        entity.Line2 = dto.Line2;
        entity.Line3 = dto.Line3;
        entity.TownCity = dto.TownCity;
        entity.CountyId = dto.CountyId;
        entity.PostcodeZip = dto.PostcodeZip;
        entity.StateId = dto.StateId;
        entity.CountryId = dto.CountryId;
        entity.IsPrimary = dto.IsPrimary;
        entity.JobId = dto.JobId;
        return entity;
      }

      public static JobAddress CopyTo(this JobAddressDto dto)
      {
        var entity = new JobAddress();
        return dto.CopyTo(entity);
      }	

      public static RoleDto AsDto(this Role entity)
      {
        var dto = new RoleDto();
				dto.Id = entity.Id;
        dto.Key = entity.Key;
        dto.Value = entity.Value;
        return dto;
      }
      
      public static Role CopyTo(this RoleDto dto, Role entity)
      {
        entity.Key = dto.Key;
        entity.Value = dto.Value;
        return entity;
      }

      public static Role CopyTo(this RoleDto dto)
      {
        var entity = new Role();
        return dto.CopyTo(entity);
      }	

      public static PersonAddressDto AsDto(this PersonAddress entity)
      {
        var dto = new PersonAddressDto();
				dto.Id = entity.Id;
        dto.Line1 = entity.Line1;
        dto.Line2 = entity.Line2;
        dto.Line3 = entity.Line3;
        dto.TownCity = entity.TownCity;
        dto.CountyId = entity.CountyId;
        dto.PostcodeZip = entity.PostcodeZip;
        dto.StateId = entity.StateId;
        dto.CountryId = entity.CountryId;
        dto.IsPrimary = entity.IsPrimary;
        dto.PersonId = entity.PersonId;
        return dto;
      }
      
      public static PersonAddress CopyTo(this PersonAddressDto dto, PersonAddress entity)
      {
        entity.Line1 = dto.Line1;
        entity.Line2 = dto.Line2;
        entity.Line3 = dto.Line3;
        entity.TownCity = dto.TownCity;
        entity.CountyId = dto.CountyId;
        entity.PostcodeZip = dto.PostcodeZip;
        entity.StateId = dto.StateId;
        entity.CountryId = dto.CountryId;
        entity.IsPrimary = dto.IsPrimary;
        entity.PersonId = dto.PersonId;
        return entity;
      }

      public static PersonAddress CopyTo(this PersonAddressDto dto)
      {
        var entity = new PersonAddress();
        return dto.CopyTo(entity);
      }	

      public static ActionTypeDto AsDto(this ActionType entity)
      {
        var dto = new ActionTypeDto();
				dto.Id = entity.Id;
        dto.Name = entity.Name;
        return dto;
      }
      
      public static ActionType CopyTo(this ActionTypeDto dto, ActionType entity)
      {
        entity.Name = dto.Name;
        return entity;
      }

      public static ActionType CopyTo(this ActionTypeDto dto)
      {
        var entity = new ActionType();
        return dto.CopyTo(entity);
      }	

      public static OnetDto AsDto(this Onet entity)
      {
        var dto = new OnetDto();
				dto.Id = entity.Id;
        dto.OnetCode = entity.OnetCode;
        dto.Key = entity.Key;
        dto.JobFamilyId = entity.JobFamilyId;
        dto.JobZone = entity.JobZone;
        return dto;
      }
      
      public static Onet CopyTo(this OnetDto dto, Onet entity)
      {
        entity.OnetCode = dto.OnetCode;
        entity.Key = dto.Key;
        entity.JobFamilyId = dto.JobFamilyId;
        entity.JobZone = dto.JobZone;
        return entity;
      }

      public static Onet CopyTo(this OnetDto dto)
      {
        var entity = new Onet();
        return dto.CopyTo(entity);
      }	

      public static EmailTemplateDto AsDto(this EmailTemplate entity)
      {
        var dto = new EmailTemplateDto();
				dto.Id = entity.Id;
        dto.EmailTemplateType = entity.EmailTemplateType;
        dto.Subject = entity.Subject;
        dto.Body = entity.Body;
        dto.Salutation = entity.Salutation;
        return dto;
      }
      
      public static EmailTemplate CopyTo(this EmailTemplateDto dto, EmailTemplate entity)
      {
        entity.EmailTemplateType = dto.EmailTemplateType;
        entity.Subject = dto.Subject;
        entity.Body = dto.Body;
        entity.Salutation = dto.Salutation;
        return entity;
      }

      public static EmailTemplate CopyTo(this EmailTemplateDto dto)
      {
        var entity = new EmailTemplate();
        return dto.CopyTo(entity);
      }	

      public static JobLocationDto AsDto(this JobLocation entity)
      {
        var dto = new JobLocationDto();
				dto.Id = entity.Id;
        dto.Location = entity.Location;
        dto.IsPublicTransitAccessible = entity.IsPublicTransitAccessible;
        dto.JobId = entity.JobId;
        return dto;
      }
      
      public static JobLocation CopyTo(this JobLocationDto dto, JobLocation entity)
      {
        entity.Location = dto.Location;
        entity.IsPublicTransitAccessible = dto.IsPublicTransitAccessible;
        entity.JobId = dto.JobId;
        return entity;
      }

      public static JobLocation CopyTo(this JobLocationDto dto)
      {
        var entity = new JobLocation();
        return dto.CopyTo(entity);
      }	

      public static OnetTaskDto AsDto(this OnetTask entity)
      {
        var dto = new OnetTaskDto();
				dto.Id = entity.Id;
        dto.Key = entity.Key;
        dto.OnetId = entity.OnetId;
        return dto;
      }
      
      public static OnetTask CopyTo(this OnetTaskDto dto, OnetTask entity)
      {
        entity.Key = dto.Key;
        entity.OnetId = dto.OnetId;
        return entity;
      }

      public static OnetTask CopyTo(this OnetTaskDto dto)
      {
        var entity = new OnetTask();
        return dto.CopyTo(entity);
      }	

      public static CertificateLicenceDto AsDto(this CertificateLicence entity)
      {
        var dto = new CertificateLicenceDto();
				dto.Id = entity.Id;
        dto.Key = entity.Key;
        dto.CertificateLicenseType = entity.CertificateLicenseType;
        dto.OnetParentCode = entity.OnetParentCode;
        dto.IsCertificate = entity.IsCertificate;
        dto.IsLicence = entity.IsLicence;
        return dto;
      }
      
      public static CertificateLicence CopyTo(this CertificateLicenceDto dto, CertificateLicence entity)
      {
        entity.Key = dto.Key;
        entity.CertificateLicenseType = dto.CertificateLicenseType;
        entity.OnetParentCode = dto.OnetParentCode;
        entity.IsCertificate = dto.IsCertificate;
        entity.IsLicence = dto.IsLicence;
        return entity;
      }

      public static CertificateLicence CopyTo(this CertificateLicenceDto dto)
      {
        var entity = new CertificateLicence();
        return dto.CopyTo(entity);
      }	

      public static JobTaskDto AsDto(this JobTask entity)
      {
        var dto = new JobTaskDto();
				dto.Id = entity.Id;
        dto.JobTaskType = entity.JobTaskType;
        dto.Key = entity.Key;
        dto.OnetId = entity.OnetId;
        return dto;
      }
      
      public static JobTask CopyTo(this JobTaskDto dto, JobTask entity)
      {
        entity.JobTaskType = dto.JobTaskType;
        entity.Key = dto.Key;
        entity.OnetId = dto.OnetId;
        return entity;
      }

      public static JobTask CopyTo(this JobTaskDto dto)
      {
        var entity = new JobTask();
        return dto.CopyTo(entity);
      }	

      public static JobTaskMultiOptionDto AsDto(this JobTaskMultiOption entity)
      {
        var dto = new JobTaskMultiOptionDto();
				dto.Id = entity.Id;
        dto.Key = entity.Key;
        dto.JobTaskId = entity.JobTaskId;
        return dto;
      }
      
      public static JobTaskMultiOption CopyTo(this JobTaskMultiOptionDto dto, JobTaskMultiOption entity)
      {
        entity.Key = dto.Key;
        entity.JobTaskId = dto.JobTaskId;
        return entity;
      }

      public static JobTaskMultiOption CopyTo(this JobTaskMultiOptionDto dto)
      {
        var entity = new JobTaskMultiOption();
        return dto.CopyTo(entity);
      }	

      public static JobLicenceDto AsDto(this JobLicence entity)
      {
        var dto = new JobLicenceDto();
				dto.Id = entity.Id;
        dto.Licence = entity.Licence;
        dto.JobId = entity.JobId;
        return dto;
      }
      
      public static JobLicence CopyTo(this JobLicenceDto dto, JobLicence entity)
      {
        entity.Licence = dto.Licence;
        entity.JobId = dto.JobId;
        return entity;
      }

      public static JobLicence CopyTo(this JobLicenceDto dto)
      {
        var entity = new JobLicence();
        return dto.CopyTo(entity);
      }	

      public static JobCertificateDto AsDto(this JobCertificate entity)
      {
        var dto = new JobCertificateDto();
				dto.Id = entity.Id;
        dto.Certificate = entity.Certificate;
        dto.JobId = entity.JobId;
        return dto;
      }
      
      public static JobCertificate CopyTo(this JobCertificateDto dto, JobCertificate entity)
      {
        entity.Certificate = dto.Certificate;
        entity.JobId = dto.JobId;
        return entity;
      }

      public static JobCertificate CopyTo(this JobCertificateDto dto)
      {
        var entity = new JobCertificate();
        return dto.CopyTo(entity);
      }	

      public static JobLanguageDto AsDto(this JobLanguage entity)
      {
        var dto = new JobLanguageDto();
				dto.Id = entity.Id;
        dto.Language = entity.Language;
        dto.JobId = entity.JobId;
        return dto;
      }
      
      public static JobLanguage CopyTo(this JobLanguageDto dto, JobLanguage entity)
      {
        entity.Language = dto.Language;
        entity.JobId = dto.JobId;
        return entity;
      }

      public static JobLanguage CopyTo(this JobLanguageDto dto)
      {
        var entity = new JobLanguage();
        return dto.CopyTo(entity);
      }	

      public static ProfanityDto AsDto(this Profanity entity)
      {
        var dto = new ProfanityDto();
				dto.Id = entity.Id;
        dto.Phrase = entity.Phrase;
        dto.ProfanityLevel = entity.ProfanityLevel;
        return dto;
      }
      
      public static Profanity CopyTo(this ProfanityDto dto, Profanity entity)
      {
        entity.Phrase = dto.Phrase;
        entity.ProfanityLevel = dto.ProfanityLevel;
        return entity;
      }

      public static Profanity CopyTo(this ProfanityDto dto)
      {
        var entity = new Profanity();
        return dto.CopyTo(entity);
      }	

      public static OnetLocalisationItemDto AsDto(this OnetLocalisationItem entity)
      {
        var dto = new OnetLocalisationItemDto();
				dto.Id = entity.Id;
        dto.Key = entity.Key;
        dto.PrimaryValue = entity.PrimaryValue;
        dto.SecondaryValue = entity.SecondaryValue;
        dto.LocalisationId = entity.LocalisationId;
        return dto;
      }
      
      public static OnetLocalisationItem CopyTo(this OnetLocalisationItemDto dto, OnetLocalisationItem entity)
      {
        entity.Key = dto.Key;
        entity.PrimaryValue = dto.PrimaryValue;
        entity.SecondaryValue = dto.SecondaryValue;
        entity.LocalisationId = dto.LocalisationId;
        return entity;
      }

      public static OnetLocalisationItem CopyTo(this OnetLocalisationItemDto dto)
      {
        var entity = new OnetLocalisationItem();
        return dto.CopyTo(entity);
      }	

      public static JobTaskLocalisationItemDto AsDto(this JobTaskLocalisationItem entity)
      {
        var dto = new JobTaskLocalisationItemDto();
				dto.Id = entity.Id;
        dto.Key = entity.Key;
        dto.PrimaryValue = entity.PrimaryValue;
        dto.SecondaryValue = entity.SecondaryValue;
        dto.LocalisationId = entity.LocalisationId;
        return dto;
      }
      
      public static JobTaskLocalisationItem CopyTo(this JobTaskLocalisationItemDto dto, JobTaskLocalisationItem entity)
      {
        entity.Key = dto.Key;
        entity.PrimaryValue = dto.PrimaryValue;
        entity.SecondaryValue = dto.SecondaryValue;
        entity.LocalisationId = dto.LocalisationId;
        return entity;
      }

      public static JobTaskLocalisationItem CopyTo(this JobTaskLocalisationItemDto dto)
      {
        var entity = new JobTaskLocalisationItem();
        return dto.CopyTo(entity);
      }	

      public static CertificateLicenceLocalisationItemDto AsDto(this CertificateLicenceLocalisationItem entity)
      {
        var dto = new CertificateLicenceLocalisationItemDto();
				dto.Id = entity.Id;
        dto.Key = entity.Key;
        dto.Value = entity.Value;
        dto.LocalisationId = entity.LocalisationId;
        return dto;
      }
      
      public static CertificateLicenceLocalisationItem CopyTo(this CertificateLicenceLocalisationItemDto dto, CertificateLicenceLocalisationItem entity)
      {
        entity.Key = dto.Key;
        entity.Value = dto.Value;
        entity.LocalisationId = dto.LocalisationId;
        return entity;
      }

      public static CertificateLicenceLocalisationItem CopyTo(this CertificateLicenceLocalisationItemDto dto)
      {
        var entity = new CertificateLicenceLocalisationItem();
        return dto.CopyTo(entity);
      }	

      public static OnetWordDto AsDto(this OnetWord entity)
      {
        var dto = new OnetWordDto();
				dto.Id = entity.Id;
        dto.Key = entity.Key;
        dto.Word = entity.Word;
        dto.Stem = entity.Stem;
        dto.Frequency = entity.Frequency;
        dto.OnetSoc = entity.OnetSoc;
        dto.OnetId = entity.OnetId;
        dto.OnetRingId = entity.OnetRingId;
        return dto;
      }
      
      public static OnetWord CopyTo(this OnetWordDto dto, OnetWord entity)
      {
        entity.Key = dto.Key;
        entity.Word = dto.Word;
        entity.Stem = dto.Stem;
        entity.Frequency = dto.Frequency;
        entity.OnetSoc = dto.OnetSoc;
        entity.OnetId = dto.OnetId;
        entity.OnetRingId = dto.OnetRingId;
        return entity;
      }

      public static OnetWord CopyTo(this OnetWordDto dto)
      {
        var entity = new OnetWord();
        return dto.CopyTo(entity);
      }	

      public static OnetRingDto AsDto(this OnetRing entity)
      {
        var dto = new OnetRingDto();
				dto.Id = entity.Id;
        dto.Name = entity.Name;
        dto.Weighting = entity.Weighting;
        dto.RingMax = entity.RingMax;
        return dto;
      }
      
      public static OnetRing CopyTo(this OnetRingDto dto, OnetRing entity)
      {
        entity.Name = dto.Name;
        entity.Weighting = dto.Weighting;
        entity.RingMax = dto.RingMax;
        return entity;
      }

      public static OnetRing CopyTo(this OnetRingDto dto)
      {
        var entity = new OnetRing();
        return dto.CopyTo(entity);
      }	

      public static OnetPhraseDto AsDto(this OnetPhrase entity)
      {
        var dto = new OnetPhraseDto();
				dto.Id = entity.Id;
        dto.Phrase = entity.Phrase;
        dto.Equivalent = entity.Equivalent;
        dto.OnetSoc = entity.OnetSoc;
        dto.OnetId = entity.OnetId;
        return dto;
      }
      
      public static OnetPhrase CopyTo(this OnetPhraseDto dto, OnetPhrase entity)
      {
        entity.Phrase = dto.Phrase;
        entity.Equivalent = dto.Equivalent;
        entity.OnetSoc = dto.OnetSoc;
        entity.OnetId = dto.OnetId;
        return entity;
      }

      public static OnetPhrase CopyTo(this OnetPhraseDto dto)
      {
        var entity = new OnetPhrase();
        return dto.CopyTo(entity);
      }	

      public static PhoneNumberDto AsDto(this PhoneNumber entity)
      {
        var dto = new PhoneNumberDto();
				dto.Id = entity.Id;
        dto.Number = entity.Number;
        dto.PhoneType = entity.PhoneType;
        dto.IsPrimary = entity.IsPrimary;
        dto.Extension = entity.Extension;
        dto.PersonId = entity.PersonId;
        return dto;
      }
      
      public static PhoneNumber CopyTo(this PhoneNumberDto dto, PhoneNumber entity)
      {
        entity.Number = dto.Number;
        entity.PhoneType = dto.PhoneType;
        entity.IsPrimary = dto.IsPrimary;
        entity.Extension = dto.Extension;
        entity.PersonId = dto.PersonId;
        return entity;
      }

      public static PhoneNumber CopyTo(this PhoneNumberDto dto)
      {
        var entity = new PhoneNumber();
        return dto.CopyTo(entity);
      }	

      public static JobViewDto AsDto(this JobView entity)
      {
        var dto = new JobViewDto();
				dto.Id = entity.Id;
				dto.UpdatedOn =  entity.UpdatedOn;
        dto.EmployerId = entity.EmployerId;
        dto.JobTitle = entity.JobTitle;
        dto.JobStatus = entity.JobStatus;
        dto.ApplicationCount = entity.ApplicationCount;
        dto.PostedOn = entity.PostedOn;
        dto.ClosingOn = entity.ClosingOn;
        dto.HeldOn = entity.HeldOn;
        dto.ClosedOn = entity.ClosedOn;
        dto.BusinessUnitName = entity.BusinessUnitName;
        dto.EmployeeId = entity.EmployeeId;
        dto.ApprovalStatus = entity.ApprovalStatus;
        return dto;
      }
      
      public static JobView CopyTo(this JobViewDto dto, JobView entity)
      {
        entity.EmployerId = dto.EmployerId;
        entity.JobTitle = dto.JobTitle;
        entity.JobStatus = dto.JobStatus;
        entity.ApplicationCount = dto.ApplicationCount;
        entity.PostedOn = dto.PostedOn;
        entity.ClosingOn = dto.ClosingOn;
        entity.HeldOn = dto.HeldOn;
        entity.ClosedOn = dto.ClosedOn;
        entity.BusinessUnitName = dto.BusinessUnitName;
        entity.EmployeeId = dto.EmployeeId;
        entity.ApprovalStatus = dto.ApprovalStatus;
        return entity;
      }

      public static JobView CopyTo(this JobViewDto dto)
      {
        var entity = new JobView();
        return dto.CopyTo(entity);
      }	

      public static SessionDto AsDto(this Session entity)
      {
        var dto = new SessionDto();
				dto.Id = entity.Id;
        dto.UserId = entity.UserId;
        dto.LastRequestOn = entity.LastRequestOn;
        return dto;
      }
      
      public static Session CopyTo(this SessionDto dto, Session entity)
      {
        entity.UserId = dto.UserId;
        entity.LastRequestOn = dto.LastRequestOn;
        return entity;
      }

      public static Session CopyTo(this SessionDto dto)
      {
        var entity = new Session();
        return dto.CopyTo(entity);
      }	

      public static SessionStateDto AsDto(this SessionState entity)
      {
        var dto = new SessionStateDto();
				dto.Id = entity.Id;
        dto.Key = entity.Key;
        dto.Value = entity.Value;
        dto.TypeOf = entity.TypeOf;
        dto.Size = entity.Size;
        dto.SessionId = entity.SessionId;
        return dto;
      }
      
      public static SessionState CopyTo(this SessionStateDto dto, SessionState entity)
      {
        entity.Key = dto.Key;
        entity.Value = dto.Value;
        entity.TypeOf = dto.TypeOf;
        entity.Size = dto.Size;
        entity.SessionId = dto.SessionId;
        return entity;
      }

      public static SessionState CopyTo(this SessionStateDto dto)
      {
        var entity = new SessionState();
        return dto.CopyTo(entity);
      }	

      public static MessageDto AsDto(this Message entity)
      {
        var dto = new MessageDto();
				dto.Id = entity.Id;
				dto.CreatedOn =  entity.CreatedOn;
        dto.IsSystemAlert = entity.IsSystemAlert;
        dto.EmployerId = entity.EmployerId;
        dto.ExpiresOn = entity.ExpiresOn;
        dto.Audience = entity.Audience;
        dto.MessageType = entity.MessageType;
        dto.EntityId = entity.EntityId;
        return dto;
      }
      
      public static Message CopyTo(this MessageDto dto, Message entity)
      {
        entity.IsSystemAlert = dto.IsSystemAlert;
        entity.EmployerId = dto.EmployerId;
        entity.ExpiresOn = dto.ExpiresOn;
        entity.Audience = dto.Audience;
        entity.MessageType = dto.MessageType;
        entity.EntityId = dto.EntityId;
        return entity;
      }

      public static Message CopyTo(this MessageDto dto)
      {
        var entity = new Message();
        return dto.CopyTo(entity);
      }	

      public static DismissedMessageDto AsDto(this DismissedMessage entity)
      {
        var dto = new DismissedMessageDto();
				dto.Id = entity.Id;
        dto.UserId = entity.UserId;
        dto.MessageId = entity.MessageId;
        return dto;
      }
      
      public static DismissedMessage CopyTo(this DismissedMessageDto dto, DismissedMessage entity)
      {
        entity.UserId = dto.UserId;
        entity.MessageId = dto.MessageId;
        return entity;
      }

      public static DismissedMessage CopyTo(this DismissedMessageDto dto)
      {
        var entity = new DismissedMessage();
        return dto.CopyTo(entity);
      }	

      public static MessageTextDto AsDto(this MessageText entity)
      {
        var dto = new MessageTextDto();
				dto.Id = entity.Id;
        dto.Text = entity.Text;
        dto.LocalisationId = entity.LocalisationId;
        dto.MessageId = entity.MessageId;
        return dto;
      }
      
      public static MessageText CopyTo(this MessageTextDto dto, MessageText entity)
      {
        entity.Text = dto.Text;
        entity.LocalisationId = dto.LocalisationId;
        entity.MessageId = dto.MessageId;
        return entity;
      }

      public static MessageText CopyTo(this MessageTextDto dto)
      {
        var entity = new MessageText();
        return dto.CopyTo(entity);
      }	

      public static CandidateDto AsDto(this Candidate entity)
      {
        var dto = new CandidateDto();
				dto.Id = entity.Id;
				dto.CreatedOn =  entity.CreatedOn;
				dto.UpdatedOn =  entity.UpdatedOn;
        dto.UserName = entity.UserName;
        dto.FirstName = entity.FirstName;
        dto.LastName = entity.LastName;
        dto.PhoneNumber = entity.PhoneNumber;
        dto.EmailAddress = entity.EmailAddress;
        dto.ExternalId = entity.ExternalId;
        return dto;
      }
      
      public static Candidate CopyTo(this CandidateDto dto, Candidate entity)
      {
        entity.UserName = dto.UserName;
        entity.FirstName = dto.FirstName;
        entity.LastName = dto.LastName;
        entity.PhoneNumber = dto.PhoneNumber;
        entity.EmailAddress = dto.EmailAddress;
        entity.ExternalId = dto.ExternalId;
        return entity;
      }

      public static Candidate CopyTo(this CandidateDto dto)
      {
        var entity = new Candidate();
        return dto.CopyTo(entity);
      }	

      public static EmployeeSearchViewDto AsDto(this EmployeeSearchView entity)
      {
        var dto = new EmployeeSearchViewDto();
				dto.Id = entity.Id;
        dto.FirstName = entity.FirstName;
        dto.LastName = entity.LastName;
        dto.EmployerId = entity.EmployerId;
        dto.EmployerName = entity.EmployerName;
        dto.IndustrialClassification = entity.IndustrialClassification;
        dto.FederalEmployerIdentificationNumber = entity.FederalEmployerIdentificationNumber;
        dto.WorkOpportunitiesTaxCreditHires = entity.WorkOpportunitiesTaxCreditHires;
        dto.PostingFlags = entity.PostingFlags;
        dto.ScreeningPreferences = entity.ScreeningPreferences;
        dto.JobTitle = entity.JobTitle;
        dto.EmployerCreatedOn = entity.EmployerCreatedOn;
        dto.EmailAddress = entity.EmailAddress;
        dto.PhoneNumber = entity.PhoneNumber;
        dto.AccountEnabled = entity.AccountEnabled;
        dto.EmployerApprovalStatus = entity.EmployerApprovalStatus;
        dto.EmployeeApprovalStatus = entity.EmployeeApprovalStatus;
        return dto;
      }
      
      public static EmployeeSearchView CopyTo(this EmployeeSearchViewDto dto, EmployeeSearchView entity)
      {
        entity.FirstName = dto.FirstName;
        entity.LastName = dto.LastName;
        entity.EmployerId = dto.EmployerId;
        entity.EmployerName = dto.EmployerName;
        entity.IndustrialClassification = dto.IndustrialClassification;
        entity.FederalEmployerIdentificationNumber = dto.FederalEmployerIdentificationNumber;
        entity.WorkOpportunitiesTaxCreditHires = dto.WorkOpportunitiesTaxCreditHires;
        entity.PostingFlags = dto.PostingFlags;
        entity.ScreeningPreferences = dto.ScreeningPreferences;
        entity.JobTitle = dto.JobTitle;
        entity.EmployerCreatedOn = dto.EmployerCreatedOn;
        entity.EmailAddress = dto.EmailAddress;
        entity.PhoneNumber = dto.PhoneNumber;
        entity.AccountEnabled = dto.AccountEnabled;
        entity.EmployerApprovalStatus = dto.EmployerApprovalStatus;
        entity.EmployeeApprovalStatus = dto.EmployeeApprovalStatus;
        return entity;
      }

      public static EmployeeSearchView CopyTo(this EmployeeSearchViewDto dto)
      {
        var entity = new EmployeeSearchView();
        return dto.CopyTo(entity);
      }	

      public static SavedMessageDto AsDto(this SavedMessage entity)
      {
        var dto = new SavedMessageDto();
				dto.Id = entity.Id;
        dto.Audience = entity.Audience;
        dto.Name = entity.Name;
        dto.UserId = entity.UserId;
        return dto;
      }
      
      public static SavedMessage CopyTo(this SavedMessageDto dto, SavedMessage entity)
      {
        entity.Audience = dto.Audience;
        entity.Name = dto.Name;
        entity.UserId = dto.UserId;
        return entity;
      }

      public static SavedMessage CopyTo(this SavedMessageDto dto)
      {
        var entity = new SavedMessage();
        return dto.CopyTo(entity);
      }	

      public static SavedMessageTextDto AsDto(this SavedMessageText entity)
      {
        var dto = new SavedMessageTextDto();
				dto.Id = entity.Id;
        dto.LocalisationId = entity.LocalisationId;
        dto.Text = entity.Text;
        dto.SavedMessageId = entity.SavedMessageId;
        return dto;
      }
      
      public static SavedMessageText CopyTo(this SavedMessageTextDto dto, SavedMessageText entity)
      {
        entity.LocalisationId = dto.LocalisationId;
        entity.Text = dto.Text;
        entity.SavedMessageId = dto.SavedMessageId;
        return entity;
      }

      public static SavedMessageText CopyTo(this SavedMessageTextDto dto)
      {
        var entity = new SavedMessageText();
        return dto.CopyTo(entity);
      }	

      public static JobSeekerReferralViewDto AsDto(this JobSeekerReferralView entity)
      {
        var dto = new JobSeekerReferralViewDto();
				dto.Id = entity.Id;
        dto.Name = entity.Name;
        dto.ReferralDate = entity.ReferralDate;
        dto.JobTitle = entity.JobTitle;
        dto.EmployerName = entity.EmployerName;
        dto.TimeInQueue = entity.TimeInQueue;
        dto.EmployerId = entity.EmployerId;
        dto.Posting = entity.Posting;
        dto.CandidateId = entity.CandidateId;
        dto.JobId = entity.JobId;
        dto.CandidateExternalId = entity.CandidateExternalId;
        dto.ApprovalRequiredReason = entity.ApprovalRequiredReason;
        return dto;
      }
      
      public static JobSeekerReferralView CopyTo(this JobSeekerReferralViewDto dto, JobSeekerReferralView entity)
      {
        entity.Name = dto.Name;
        entity.ReferralDate = dto.ReferralDate;
        entity.JobTitle = dto.JobTitle;
        entity.EmployerName = dto.EmployerName;
        entity.TimeInQueue = dto.TimeInQueue;
        entity.EmployerId = dto.EmployerId;
        entity.Posting = dto.Posting;
        entity.CandidateId = dto.CandidateId;
        entity.JobId = dto.JobId;
        entity.CandidateExternalId = dto.CandidateExternalId;
        entity.ApprovalRequiredReason = dto.ApprovalRequiredReason;
        return entity;
      }

      public static JobSeekerReferralView CopyTo(this JobSeekerReferralViewDto dto)
      {
        var entity = new JobSeekerReferralView();
        return dto.CopyTo(entity);
      }	

      public static EmployerAccountReferralViewDto AsDto(this EmployerAccountReferralView entity)
      {
        var dto = new EmployerAccountReferralViewDto();
				dto.Id = entity.Id;
        dto.EmployerId = entity.EmployerId;
        dto.EmployeeId = entity.EmployeeId;
        dto.EmployeeFirstName = entity.EmployeeFirstName;
        dto.EmployeeLastName = entity.EmployeeLastName;
        dto.AccountCreationDate = entity.AccountCreationDate;
        dto.TimeInQueue = entity.TimeInQueue;
        dto.EmployerName = entity.EmployerName;
        dto.EmployerIndustrialClassification = entity.EmployerIndustrialClassification;
        dto.EmployerPhoneNumber = entity.EmployerPhoneNumber;
        dto.EmployerFederalEmployerIdentificationNumber = entity.EmployerFederalEmployerIdentificationNumber;
        dto.EmployerStateEmployerIdentificationNumber = entity.EmployerStateEmployerIdentificationNumber;
        dto.EmployerFaxNumber = entity.EmployerFaxNumber;
        dto.EmployerUrl = entity.EmployerUrl;
        dto.EmployeeAddressLine1 = entity.EmployeeAddressLine1;
        dto.EmployeeAddressTownCity = entity.EmployeeAddressTownCity;
        dto.EmployeeAddressStateId = entity.EmployeeAddressStateId;
        dto.EmployeeAddressPostcodeZip = entity.EmployeeAddressPostcodeZip;
        dto.EmployeeEmailAddress = entity.EmployeeEmailAddress;
        dto.EmployeePhoneNumber = entity.EmployeePhoneNumber;
        dto.EmployeeFaxNumber = entity.EmployeeFaxNumber;
        dto.EmployerAddressLine1 = entity.EmployerAddressLine1;
        dto.EmployerAddressTownCity = entity.EmployerAddressTownCity;
        dto.EmployerAddressStateId = entity.EmployerAddressStateId;
        dto.EmployerAddressPostcodeZip = entity.EmployerAddressPostcodeZip;
        return dto;
      }
      
      public static EmployerAccountReferralView CopyTo(this EmployerAccountReferralViewDto dto, EmployerAccountReferralView entity)
      {
        entity.EmployerId = dto.EmployerId;
        entity.EmployeeId = dto.EmployeeId;
        entity.EmployeeFirstName = dto.EmployeeFirstName;
        entity.EmployeeLastName = dto.EmployeeLastName;
        entity.AccountCreationDate = dto.AccountCreationDate;
        entity.TimeInQueue = dto.TimeInQueue;
        entity.EmployerName = dto.EmployerName;
        entity.EmployerIndustrialClassification = dto.EmployerIndustrialClassification;
        entity.EmployerPhoneNumber = dto.EmployerPhoneNumber;
        entity.EmployerFederalEmployerIdentificationNumber = dto.EmployerFederalEmployerIdentificationNumber;
        entity.EmployerStateEmployerIdentificationNumber = dto.EmployerStateEmployerIdentificationNumber;
        entity.EmployerFaxNumber = dto.EmployerFaxNumber;
        entity.EmployerUrl = dto.EmployerUrl;
        entity.EmployeeAddressLine1 = dto.EmployeeAddressLine1;
        entity.EmployeeAddressTownCity = dto.EmployeeAddressTownCity;
        entity.EmployeeAddressStateId = dto.EmployeeAddressStateId;
        entity.EmployeeAddressPostcodeZip = dto.EmployeeAddressPostcodeZip;
        entity.EmployeeEmailAddress = dto.EmployeeEmailAddress;
        entity.EmployeePhoneNumber = dto.EmployeePhoneNumber;
        entity.EmployeeFaxNumber = dto.EmployeeFaxNumber;
        entity.EmployerAddressLine1 = dto.EmployerAddressLine1;
        entity.EmployerAddressTownCity = dto.EmployerAddressTownCity;
        entity.EmployerAddressStateId = dto.EmployerAddressStateId;
        entity.EmployerAddressPostcodeZip = dto.EmployerAddressPostcodeZip;
        return entity;
      }

      public static EmployerAccountReferralView CopyTo(this EmployerAccountReferralViewDto dto)
      {
        var entity = new EmployerAccountReferralView();
        return dto.CopyTo(entity);
      }	

      public static JobPostingReferralViewDto AsDto(this JobPostingReferralView entity)
      {
        var dto = new JobPostingReferralViewDto();
				dto.Id = entity.Id;
        dto.JobTitle = entity.JobTitle;
        dto.EmployerId = entity.EmployerId;
        dto.EmployerName = entity.EmployerName;
        dto.TimeInQueue = entity.TimeInQueue;
        dto.EmployeeFirstName = entity.EmployeeFirstName;
        dto.EmployeeLastName = entity.EmployeeLastName;
        dto.EmployeeId = entity.EmployeeId;
        dto.AwaitingApprovalOn = entity.AwaitingApprovalOn;
        return dto;
      }
      
      public static JobPostingReferralView CopyTo(this JobPostingReferralViewDto dto, JobPostingReferralView entity)
      {
        entity.JobTitle = dto.JobTitle;
        entity.EmployerId = dto.EmployerId;
        entity.EmployerName = dto.EmployerName;
        entity.TimeInQueue = dto.TimeInQueue;
        entity.EmployeeFirstName = dto.EmployeeFirstName;
        entity.EmployeeLastName = dto.EmployeeLastName;
        entity.EmployeeId = dto.EmployeeId;
        entity.AwaitingApprovalOn = dto.AwaitingApprovalOn;
        return entity;
      }

      public static JobPostingReferralView CopyTo(this JobPostingReferralViewDto dto)
      {
        var entity = new JobPostingReferralView();
        return dto.CopyTo(entity);
      }	

      public static PersonListDto AsDto(this PersonList entity)
      {
        var dto = new PersonListDto();
				dto.Id = entity.Id;
        dto.Name = entity.Name;
        dto.ListType = entity.ListType;
        dto.PersonId = entity.PersonId;
        return dto;
      }
      
      public static PersonList CopyTo(this PersonListDto dto, PersonList entity)
      {
        entity.Name = dto.Name;
        entity.ListType = dto.ListType;
        entity.PersonId = dto.PersonId;
        return entity;
      }

      public static PersonList CopyTo(this PersonListDto dto)
      {
        var entity = new PersonList();
        return dto.CopyTo(entity);
      }	

      public static ApplicationImageDto AsDto(this ApplicationImage entity)
      {
        var dto = new ApplicationImageDto();
				dto.Id = entity.Id;
        dto.Type = entity.Type;
        dto.Image = entity.Image;
        return dto;
      }
      
      public static ApplicationImage CopyTo(this ApplicationImageDto dto, ApplicationImage entity)
      {
        entity.Type = dto.Type;
        entity.Image = dto.Image;
        return entity;
      }

      public static ApplicationImage CopyTo(this ApplicationImageDto dto)
      {
        var entity = new ApplicationImage();
        return dto.CopyTo(entity);
      }	

      public static StaffSearchViewDto AsDto(this StaffSearchView entity)
      {
        var dto = new StaffSearchViewDto();
				dto.Id = entity.Id;
        dto.FirstName = entity.FirstName;
        dto.LastName = entity.LastName;
        dto.Enabled = entity.Enabled;
        dto.EmailAddress = entity.EmailAddress;
        dto.Office = entity.Office;
        return dto;
      }
      
      public static StaffSearchView CopyTo(this StaffSearchViewDto dto, StaffSearchView entity)
      {
        entity.FirstName = dto.FirstName;
        entity.LastName = dto.LastName;
        entity.Enabled = dto.Enabled;
        entity.EmailAddress = dto.EmailAddress;
        entity.Office = dto.Office;
        return entity;
      }

      public static StaffSearchView CopyTo(this StaffSearchViewDto dto)
      {
        var entity = new StaffSearchView();
        return dto.CopyTo(entity);
      }	

      public static SingleSignOnDto AsDto(this SingleSignOn entity)
      {
        var dto = new SingleSignOnDto();
				dto.Id = entity.Id;
				dto.CreatedOn =  entity.CreatedOn;
        dto.UserId = entity.UserId;
        dto.ActionerId = entity.ActionerId;
        return dto;
      }
      
      public static SingleSignOn CopyTo(this SingleSignOnDto dto, SingleSignOn entity)
      {
        entity.UserId = dto.UserId;
        entity.ActionerId = dto.ActionerId;
        return entity;
      }

      public static SingleSignOn CopyTo(this SingleSignOnDto dto)
      {
        var entity = new SingleSignOn();
        return dto.CopyTo(entity);
      }	

      public static JobTaskViewDto AsDto(this JobTaskView entity)
      {
        var dto = new JobTaskViewDto();
				dto.Id = entity.Id;
        dto.JobTaskType = entity.JobTaskType;
        dto.OnetId = entity.OnetId;
        dto.Culture = entity.Culture;
        dto.Prompt = entity.Prompt;
        dto.Response = entity.Response;
        return dto;
      }
      
      public static JobTaskView CopyTo(this JobTaskViewDto dto, JobTaskView entity)
      {
        entity.JobTaskType = dto.JobTaskType;
        entity.OnetId = dto.OnetId;
        entity.Culture = dto.Culture;
        entity.Prompt = dto.Prompt;
        entity.Response = dto.Response;
        return entity;
      }

      public static JobTaskView CopyTo(this JobTaskViewDto dto)
      {
        var entity = new JobTaskView();
        return dto.CopyTo(entity);
      }	

      public static JobTaskMultiOptionViewDto AsDto(this JobTaskMultiOptionView entity)
      {
        var dto = new JobTaskMultiOptionViewDto();
				dto.Id = entity.Id;
        dto.OnetId = entity.OnetId;
        dto.Culture = entity.Culture;
        dto.Prompt = entity.Prompt;
        dto.JobTaskId = entity.JobTaskId;
        return dto;
      }
      
      public static JobTaskMultiOptionView CopyTo(this JobTaskMultiOptionViewDto dto, JobTaskMultiOptionView entity)
      {
        entity.OnetId = dto.OnetId;
        entity.Culture = dto.Culture;
        entity.Prompt = dto.Prompt;
        entity.JobTaskId = dto.JobTaskId;
        return entity;
      }

      public static JobTaskMultiOptionView CopyTo(this JobTaskMultiOptionViewDto dto)
      {
        var entity = new JobTaskMultiOptionView();
        return dto.CopyTo(entity);
      }	

      public static OnetViewDto AsDto(this OnetView entity)
      {
        var dto = new OnetViewDto();
				dto.Id = entity.Id;
        dto.Key = entity.Key;
        dto.Occupation = entity.Occupation;
        dto.JobFamilyId = entity.JobFamilyId;
        dto.JobFamilyKey = entity.JobFamilyKey;
        dto.OnetCode = entity.OnetCode;
        dto.Culture = entity.Culture;
        dto.Description = entity.Description;
        return dto;
      }
      
      public static OnetView CopyTo(this OnetViewDto dto, OnetView entity)
      {
        entity.Key = dto.Key;
        entity.Occupation = dto.Occupation;
        entity.JobFamilyId = dto.JobFamilyId;
        entity.JobFamilyKey = dto.JobFamilyKey;
        entity.OnetCode = dto.OnetCode;
        entity.Culture = dto.Culture;
        entity.Description = dto.Description;
        return entity;
      }

      public static OnetView CopyTo(this OnetViewDto dto)
      {
        var entity = new OnetView();
        return dto.CopyTo(entity);
      }	

      public static CandidateNoteDto AsDto(this CandidateNote entity)
      {
        var dto = new CandidateNoteDto();
				dto.Id = entity.Id;
        dto.Note = entity.Note;
        dto.EmployerId = entity.EmployerId;
        dto.CandidateId = entity.CandidateId;
        return dto;
      }
      
      public static CandidateNote CopyTo(this CandidateNoteDto dto, CandidateNote entity)
      {
        entity.Note = dto.Note;
        entity.EmployerId = dto.EmployerId;
        entity.CandidateId = dto.CandidateId;
        return entity;
      }

      public static CandidateNote CopyTo(this CandidateNoteDto dto)
      {
        var entity = new CandidateNote();
        return dto.CopyTo(entity);
      }	

      public static NAICSDto AsDto(this NAICS entity)
      {
        var dto = new NAICSDto();
				dto.Id = entity.Id;
        dto.Code = entity.Code;
        dto.Name = entity.Name;
        return dto;
      }
      
      public static NAICS CopyTo(this NAICSDto dto, NAICS entity)
      {
        entity.Code = dto.Code;
        entity.Name = dto.Name;
        return entity;
      }

      public static NAICS CopyTo(this NAICSDto dto)
      {
        var entity = new NAICS();
        return dto.CopyTo(entity);
      }	

      public static ApplicationViewDto AsDto(this ApplicationView entity)
      {
        var dto = new ApplicationViewDto();
				dto.Id = entity.Id;
        dto.JobId = entity.JobId;
        dto.EmployerId = entity.EmployerId;
        dto.CandidateId = entity.CandidateId;
        dto.CandidateApplicationStatus = entity.CandidateApplicationStatus;
        dto.CandidateApplicationScore = entity.CandidateApplicationScore;
        dto.CandidateExternalId = entity.CandidateExternalId;
        dto.CandidateApplicationReceivedOn = entity.CandidateApplicationReceivedOn;
        dto.CandidateApplicationApprovalStatus = entity.CandidateApplicationApprovalStatus;
        dto.CandidateFirstName = entity.CandidateFirstName;
        dto.CandidateLastName = entity.CandidateLastName;
        return dto;
      }
      
      public static ApplicationView CopyTo(this ApplicationViewDto dto, ApplicationView entity)
      {
        entity.JobId = dto.JobId;
        entity.EmployerId = dto.EmployerId;
        entity.CandidateId = dto.CandidateId;
        entity.CandidateApplicationStatus = dto.CandidateApplicationStatus;
        entity.CandidateApplicationScore = dto.CandidateApplicationScore;
        entity.CandidateExternalId = dto.CandidateExternalId;
        entity.CandidateApplicationReceivedOn = dto.CandidateApplicationReceivedOn;
        entity.CandidateApplicationApprovalStatus = dto.CandidateApplicationApprovalStatus;
        entity.CandidateFirstName = dto.CandidateFirstName;
        entity.CandidateLastName = dto.CandidateLastName;
        return entity;
      }

      public static ApplicationView CopyTo(this ApplicationViewDto dto)
      {
        var entity = new ApplicationView();
        return dto.CopyTo(entity);
      }	

      public static LookupItemsViewDto AsDto(this LookupItemsView entity)
      {
        var dto = new LookupItemsViewDto();
				dto.Id = entity.Id;
        dto.Key = entity.Key;
        dto.LookupType = entity.LookupType;
        dto.Value = entity.Value;
        dto.DisplayOrder = entity.DisplayOrder;
        dto.ParentId = entity.ParentId;
        dto.ParentKey = entity.ParentKey;
        dto.Culture = entity.Culture;
        dto.ExternalId = entity.ExternalId;
        return dto;
      }
      
      public static LookupItemsView CopyTo(this LookupItemsViewDto dto, LookupItemsView entity)
      {
        entity.Key = dto.Key;
        entity.LookupType = dto.LookupType;
        entity.Value = dto.Value;
        entity.DisplayOrder = dto.DisplayOrder;
        entity.ParentId = dto.ParentId;
        entity.ParentKey = dto.ParentKey;
        entity.Culture = dto.Culture;
        entity.ExternalId = dto.ExternalId;
        return entity;
      }

      public static LookupItemsView CopyTo(this LookupItemsViewDto dto)
      {
        var entity = new LookupItemsView();
        return dto.CopyTo(entity);
      }	

      public static CandidateNoteViewDto AsDto(this CandidateNoteView entity)
      {
        var dto = new CandidateNoteViewDto();
				dto.Id = entity.Id;
        dto.Note = entity.Note;
        dto.EmployerId = entity.EmployerId;
        dto.CandidateId = entity.CandidateId;
        dto.CandidateExternalId = entity.CandidateExternalId;
        return dto;
      }
      
      public static CandidateNoteView CopyTo(this CandidateNoteViewDto dto, CandidateNoteView entity)
      {
        entity.Note = dto.Note;
        entity.EmployerId = dto.EmployerId;
        entity.CandidateId = dto.CandidateId;
        entity.CandidateExternalId = dto.CandidateExternalId;
        return entity;
      }

      public static CandidateNoteView CopyTo(this CandidateNoteViewDto dto)
      {
        var entity = new CandidateNoteView();
        return dto.CopyTo(entity);
      }	

      public static PersonListCandidateViewDto AsDto(this PersonListCandidateView entity)
      {
        var dto = new PersonListCandidateViewDto();
				dto.Id = entity.Id;
        dto.ListType = entity.ListType;
        dto.PersonId = entity.PersonId;
        dto.CandidateId = entity.CandidateId;
        dto.CandidateExternalId = entity.CandidateExternalId;
        return dto;
      }
      
      public static PersonListCandidateView CopyTo(this PersonListCandidateViewDto dto, PersonListCandidateView entity)
      {
        entity.ListType = dto.ListType;
        entity.PersonId = dto.PersonId;
        entity.CandidateId = dto.CandidateId;
        entity.CandidateExternalId = dto.CandidateExternalId;
        return entity;
      }

      public static PersonListCandidateView CopyTo(this PersonListCandidateViewDto dto)
      {
        var entity = new PersonListCandidateView();
        return dto.CopyTo(entity);
      }	

      public static JobTitleDto AsDto(this JobTitle entity)
      {
        var dto = new JobTitleDto();
				dto.Id = entity.Id;
        dto.Value = entity.Value;
        return dto;
      }
      
      public static JobTitle CopyTo(this JobTitleDto dto, JobTitle entity)
      {
        entity.Value = dto.Value;
        return entity;
      }

      public static JobTitle CopyTo(this JobTitleDto dto)
      {
        var entity = new JobTitle();
        return dto.CopyTo(entity);
      }	

      public static OnetTaskViewDto AsDto(this OnetTaskView entity)
      {
        var dto = new OnetTaskViewDto();
				dto.Id = entity.Id;
        dto.OnetId = entity.OnetId;
        dto.TaskKey = entity.TaskKey;
        dto.Task = entity.Task;
        dto.Culture = entity.Culture;
        return dto;
      }
      
      public static OnetTaskView CopyTo(this OnetTaskViewDto dto, OnetTaskView entity)
      {
        entity.OnetId = dto.OnetId;
        entity.TaskKey = dto.TaskKey;
        entity.Task = dto.Task;
        entity.Culture = dto.Culture;
        return entity;
      }

      public static OnetTaskView CopyTo(this OnetTaskViewDto dto)
      {
        var entity = new OnetTaskView();
        return dto.CopyTo(entity);
      }	

      public static PostalCodeDto AsDto(this PostalCode entity)
      {
        var dto = new PostalCodeDto();
				dto.Id = entity.Id;
        dto.Code = entity.Code;
        dto.Longitude = entity.Longitude;
        dto.Latitude = entity.Latitude;
        dto.CountryKey = entity.CountryKey;
        dto.StateKey = entity.StateKey;
        dto.CountyKey = entity.CountyKey;
        dto.CityName = entity.CityName;
        dto.OfficeKey = entity.OfficeKey;
        dto.WibLocationKey = entity.WibLocationKey;
        return dto;
      }
      
      public static PostalCode CopyTo(this PostalCodeDto dto, PostalCode entity)
      {
        entity.Code = dto.Code;
        entity.Longitude = dto.Longitude;
        entity.Latitude = dto.Latitude;
        entity.CountryKey = dto.CountryKey;
        entity.StateKey = dto.StateKey;
        entity.CountyKey = dto.CountyKey;
        entity.CityName = dto.CityName;
        entity.OfficeKey = dto.OfficeKey;
        entity.WibLocationKey = dto.WibLocationKey;
        return entity;
      }

      public static PostalCode CopyTo(this PostalCodeDto dto)
      {
        var entity = new PostalCode();
        return dto.CopyTo(entity);
      }	

      public static PostalCodeViewDto AsDto(this PostalCodeView entity)
      {
        var dto = new PostalCodeViewDto();
				dto.Id = entity.Id;
        dto.Code = entity.Code;
        dto.Longitude = entity.Longitude;
        dto.Latitude = entity.Latitude;
        dto.CountryKey = entity.CountryKey;
        dto.CountryName = entity.CountryName;
        dto.StateKey = entity.StateKey;
        dto.StateName = entity.StateName;
        dto.Culture = entity.Culture;
        dto.CountyKey = entity.CountyKey;
        dto.CountyName = entity.CountyName;
        dto.CityName = entity.CityName;
        return dto;
      }
      
      public static PostalCodeView CopyTo(this PostalCodeViewDto dto, PostalCodeView entity)
      {
        entity.Code = dto.Code;
        entity.Longitude = dto.Longitude;
        entity.Latitude = dto.Latitude;
        entity.CountryKey = dto.CountryKey;
        entity.CountryName = dto.CountryName;
        entity.StateKey = dto.StateKey;
        entity.StateName = dto.StateName;
        entity.Culture = dto.Culture;
        entity.CountyKey = dto.CountyKey;
        entity.CountyName = dto.CountyName;
        entity.CityName = dto.CityName;
        return entity;
      }

      public static PostalCodeView CopyTo(this PostalCodeViewDto dto)
      {
        var entity = new PostalCodeView();
        return dto.CopyTo(entity);
      }	

      public static ExpiringJobViewDto AsDto(this ExpiringJobView entity)
      {
        var dto = new ExpiringJobViewDto();
				dto.Id = entity.Id;
        dto.JobTitle = entity.JobTitle;
        dto.EmployerId = entity.EmployerId;
        dto.EmployeeId = entity.EmployeeId;
        dto.ClosingOn = entity.ClosingOn;
        dto.DaysToClosing = entity.DaysToClosing;
        dto.PostedOn = entity.PostedOn;
        dto.UserId = entity.UserId;
        dto.FirstName = entity.FirstName;
        return dto;
      }
      
      public static ExpiringJobView CopyTo(this ExpiringJobViewDto dto, ExpiringJobView entity)
      {
        entity.JobTitle = dto.JobTitle;
        entity.EmployerId = dto.EmployerId;
        entity.EmployeeId = dto.EmployeeId;
        entity.ClosingOn = dto.ClosingOn;
        entity.DaysToClosing = dto.DaysToClosing;
        entity.PostedOn = dto.PostedOn;
        entity.UserId = dto.UserId;
        entity.FirstName = dto.FirstName;
        return entity;
      }

      public static ExpiringJobView CopyTo(this ExpiringJobViewDto dto)
      {
        var entity = new ExpiringJobView();
        return dto.CopyTo(entity);
      }	

      public static OnetWordJobTaskViewDto AsDto(this OnetWordJobTaskView entity)
      {
        var dto = new OnetWordJobTaskViewDto();
				dto.Id = entity.Id;
        dto.OnetId = entity.OnetId;
        dto.OnetRingId = entity.OnetRingId;
        dto.OnetSoc = entity.OnetSoc;
        dto.Stem = entity.Stem;
        dto.Word = entity.Word;
        dto.JobFamilyId = entity.JobFamilyId;
        dto.JobZone = entity.JobZone;
        dto.OnetKey = entity.OnetKey;
        dto.OnetCode = entity.OnetCode;
        dto.Frequency = entity.Frequency;
        return dto;
      }
      
      public static OnetWordJobTaskView CopyTo(this OnetWordJobTaskViewDto dto, OnetWordJobTaskView entity)
      {
        entity.OnetId = dto.OnetId;
        entity.OnetRingId = dto.OnetRingId;
        entity.OnetSoc = dto.OnetSoc;
        entity.Stem = dto.Stem;
        entity.Word = dto.Word;
        entity.JobFamilyId = dto.JobFamilyId;
        entity.JobZone = dto.JobZone;
        entity.OnetKey = dto.OnetKey;
        entity.OnetCode = dto.OnetCode;
        entity.Frequency = dto.Frequency;
        return entity;
      }

      public static OnetWordJobTaskView CopyTo(this OnetWordJobTaskViewDto dto)
      {
        var entity = new OnetWordJobTaskView();
        return dto.CopyTo(entity);
      }	

      public static OnetWordViewDto AsDto(this OnetWordView entity)
      {
        var dto = new OnetWordViewDto();
				dto.Id = entity.Id;
        dto.OnetId = entity.OnetId;
        dto.OnetRingId = entity.OnetRingId;
        dto.OnetSoc = entity.OnetSoc;
        dto.Stem = entity.Stem;
        dto.Word = entity.Word;
        dto.JobFamilyId = entity.JobFamilyId;
        dto.JobZone = entity.JobZone;
        dto.OnetKey = entity.OnetKey;
        dto.OnetCode = entity.OnetCode;
        dto.Frequency = entity.Frequency;
        return dto;
      }
      
      public static OnetWordView CopyTo(this OnetWordViewDto dto, OnetWordView entity)
      {
        entity.OnetId = dto.OnetId;
        entity.OnetRingId = dto.OnetRingId;
        entity.OnetSoc = dto.OnetSoc;
        entity.Stem = dto.Stem;
        entity.Word = dto.Word;
        entity.JobFamilyId = dto.JobFamilyId;
        entity.JobZone = dto.JobZone;
        entity.OnetKey = dto.OnetKey;
        entity.OnetCode = dto.OnetCode;
        entity.Frequency = dto.Frequency;
        return entity;
      }

      public static OnetWordView CopyTo(this OnetWordViewDto dto)
      {
        var entity = new OnetWordView();
        return dto.CopyTo(entity);
      }	

      public static BookmarkDto AsDto(this Bookmark entity)
      {
        var dto = new BookmarkDto();
				dto.Id = entity.Id;
        dto.Name = entity.Name;
        dto.Type = entity.Type;
        dto.Criteria = entity.Criteria;
        dto.UserId = entity.UserId;
        dto.UserType = entity.UserType;
        return dto;
      }
      
      public static Bookmark CopyTo(this BookmarkDto dto, Bookmark entity)
      {
        entity.Name = dto.Name;
        entity.Type = dto.Type;
        entity.Criteria = dto.Criteria;
        entity.UserId = dto.UserId;
        entity.UserType = dto.UserType;
        return entity;
      }

      public static Bookmark CopyTo(this BookmarkDto dto)
      {
        var entity = new Bookmark();
        return dto.CopyTo(entity);
      }	

      public static PersonResumeDto AsDto(this PersonResume entity)
      {
        var dto = new PersonResumeDto();
				dto.Id = entity.Id;
        dto.FileName = entity.FileName;
        dto.ContentType = entity.ContentType;
        dto.Resume = entity.Resume;
        dto.PrimaryOnet = entity.PrimaryOnet;
        dto.PrimaryROnet = entity.PrimaryROnet;
        dto.PersonId = entity.PersonId;
        return dto;
      }
      
      public static PersonResume CopyTo(this PersonResumeDto dto, PersonResume entity)
      {
        entity.FileName = dto.FileName;
        entity.ContentType = dto.ContentType;
        entity.Resume = dto.Resume;
        entity.PrimaryOnet = dto.PrimaryOnet;
        entity.PrimaryROnet = dto.PrimaryROnet;
        entity.PersonId = dto.PersonId;
        return entity;
      }

      public static PersonResume CopyTo(this PersonResumeDto dto)
      {
        var entity = new PersonResume();
        return dto.CopyTo(entity);
      }	

      public static UserRoleDto AsDto(this UserRole entity)
      {
        var dto = new UserRoleDto();
				dto.Id = entity.Id;
        dto.UserId = entity.UserId;
        dto.RoleId = entity.RoleId;
        return dto;
      }
      
      public static UserRole CopyTo(this UserRoleDto dto, UserRole entity)
      {
        entity.UserId = dto.UserId;
        entity.RoleId = dto.RoleId;
        return entity;
      }

      public static UserRole CopyTo(this UserRoleDto dto)
      {
        var entity = new UserRole();
        return dto.CopyTo(entity);
      }	

      public static EmployeeCandidateDto AsDto(this EmployeeCandidate entity)
      {
        var dto = new EmployeeCandidateDto();
				dto.Id = entity.Id;
        dto.EmployeeId = entity.EmployeeId;
        dto.CandidateId = entity.CandidateId;
        return dto;
      }
      
      public static EmployeeCandidate CopyTo(this EmployeeCandidateDto dto, EmployeeCandidate entity)
      {
        entity.EmployeeId = dto.EmployeeId;
        entity.CandidateId = dto.CandidateId;
        return entity;
      }

      public static EmployeeCandidate CopyTo(this EmployeeCandidateDto dto)
      {
        var entity = new EmployeeCandidate();
        return dto.CopyTo(entity);
      }	

      public static JobNoteDto AsDto(this JobNote entity)
      {
        var dto = new JobNoteDto();
				dto.Id = entity.Id;
        dto.JobId = entity.JobId;
        dto.NoteId = entity.NoteId;
        return dto;
      }
      
      public static JobNote CopyTo(this JobNoteDto dto, JobNote entity)
      {
        entity.JobId = dto.JobId;
        entity.NoteId = dto.NoteId;
        return entity;
      }

      public static JobNote CopyTo(this JobNoteDto dto)
      {
        var entity = new JobNote();
        return dto.CopyTo(entity);
      }	

      public static SavedSearchEmployeeDto AsDto(this SavedSearchEmployee entity)
      {
        var dto = new SavedSearchEmployeeDto();
				dto.Id = entity.Id;
        dto.EmployeeId = entity.EmployeeId;
        dto.SavedSearchId = entity.SavedSearchId;
        return dto;
      }
      
      public static SavedSearchEmployee CopyTo(this SavedSearchEmployeeDto dto, SavedSearchEmployee entity)
      {
        entity.EmployeeId = dto.EmployeeId;
        entity.SavedSearchId = dto.SavedSearchId;
        return entity;
      }

      public static SavedSearchEmployee CopyTo(this SavedSearchEmployeeDto dto)
      {
        var entity = new SavedSearchEmployee();
        return dto.CopyTo(entity);
      }	

      public static PersonListCandidateDto AsDto(this PersonListCandidate entity)
      {
        var dto = new PersonListCandidateDto();
				dto.Id = entity.Id;
        dto.CandidateId = entity.CandidateId;
        dto.PersonListId = entity.PersonListId;
        return dto;
      }
      
      public static PersonListCandidate CopyTo(this PersonListCandidateDto dto, PersonListCandidate entity)
      {
        entity.CandidateId = dto.CandidateId;
        entity.PersonListId = dto.PersonListId;
        return entity;
      }

      public static PersonListCandidate CopyTo(this PersonListCandidateDto dto)
      {
        var entity = new PersonListCandidate();
        return dto.CopyTo(entity);
      }	


    }
}

#endregion
