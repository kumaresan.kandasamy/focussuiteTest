﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core;
using Framework.Core;

#endregion

namespace Focus.Data.Core.Entities
{
  public partial class Person : IReportable
  {
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> DateOfBirth
    {
      get
      {
        return Get(ref _dateOfBirth, "DateOfBirth");
      }
      set
      {
        Set(ref _dateOfBirth, value, "DateOfBirth");
        Age = value.HasValue ? value.Value.GetAge() : (short?) null;
      }
    }

    public ReportEntityType ReportEntityType { get { return ReportEntityType.Person; } }
    public long ReportEntityId { get { return Id; } }
    public long? ReportAdditionalEntityId { get { return null; } }
    public bool IsChild { get { return false; } }
  }
}