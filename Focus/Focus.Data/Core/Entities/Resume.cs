﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core;

#endregion

namespace Focus.Data.Core.Entities
{
  public partial class Resume : IReportable
  {
    public ReportEntityType ReportEntityType { get { return ReportEntityType.JobSeeker; } }
    public long ReportEntityId { get { return PersonId; } }
    public bool IsChild { get { return true; } }
    public long? ReportAdditionalEntityId { get { return null; } }
  }
}