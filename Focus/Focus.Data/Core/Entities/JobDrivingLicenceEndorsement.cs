﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives



#endregion

namespace Focus.Data.Core.Entities
{
	public partial class JobDrivingLicenceEndorsement
	{
		public JobDrivingLicenceEndorsement Clone()
		{
			return new JobDrivingLicenceEndorsement
			{
				DrivingLicenceEndorsementId = DrivingLicenceEndorsementId,
			};
		}
	}
}
