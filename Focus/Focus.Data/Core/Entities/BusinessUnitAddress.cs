﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core;

#endregion

namespace Focus.Data.Core.Entities
{
  public partial class BusinessUnitAddress : IReportable
  {
    public ReportEntityType ReportEntityType { get { return ReportEntityType.Employer; } }
    public long ReportEntityId { get { return BusinessUnitId; } }
    public long? ReportAdditionalEntityId { get { return null; } }
    public bool IsChild { get { return true; } }
  }
}