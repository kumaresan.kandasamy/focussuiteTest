﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Focus.Core;

namespace Focus.Data.Core.Entities
{
    public partial class User : IReportable
    {
        public ReportEntityType ReportEntityType { get { return ReportEntityType.Person; } }
        public long ReportEntityId { get { return PersonId; } }
        public bool IsChild { get { return true; } }
        public long? ReportAdditionalEntityId { get { return null; } }
    }
}
