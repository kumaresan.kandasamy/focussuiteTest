﻿namespace Focus.Data.Core.Entities
{
	partial class PostingSurvey
	{
		public PostingSurvey Clone()
		{
			return new PostingSurvey
			{
				DidHire = DidHire,
				DidInterview = DidInterview,
				SatisfactionLevel = SatisfactionLevel,
				SurveyType = SurveyType
			};
		}

	}
}
