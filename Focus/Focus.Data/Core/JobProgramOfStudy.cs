﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

namespace Focus.Data.Core.Entities
{
  public partial class JobProgramOfStudy
  {
    public JobProgramOfStudy Clone()
    {
      return new JobProgramOfStudy
      {
        ProgramOfStudy = ProgramOfStudy,
				DegreeEducationLevelId = DegreeEducationLevelId,
      };
    }
  }
}

