﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

namespace Focus.Data.Core.Entities
{
	partial class JobAddress
	{
		public JobAddress Clone()
		{
			return new JobAddress
										{
											Line1 = Line1,
											Line2 = Line2,
											Line3 = Line3,
											TownCity = TownCity,
											PostcodeZip = PostcodeZip,
											StateId = StateId,
											IsPrimary = IsPrimary,
											CountyId = CountyId,
											CountryId = CountryId,
										};
		}
	}
}
