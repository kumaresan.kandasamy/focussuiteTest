﻿#macro( GetFieldType $field )
#set( $fieldType = $Translator.TranslateType($field.DataType, $field.AllowNulls) )
#if ( $fieldType.contains("System.Nullable<") )
  #set( $fieldType = $fieldType.replace("System.Nullable<", "") )
  #set( $fieldType = $fieldType.replace(">", "?") )
#end
#set( $fieldType = $fieldType.replace("System.", "") )
#set( $fieldType = $fieldType.replace("Focus.Core.", "") )
$fieldType#end

#region Data Transfer Objects - Classes

/*
namespace Focus.Core.DataTransferObjects.${Context.namespace.replace(".Data.", "")}
{
  using System;
  using System.Runtime.Serialization;

#foreach ($entity in $Entities)
    [Serializable]
    [DataContract(Name="${entity.Name}", Namespace = Constants.DataContractNamespace)]
    public class ${entity.Name}Dto
    {
      [DataMember]
#if ($Translator.TranslateIdentityType($entity) == "string")
      public string Id { get; set; }
#else
      public $Translator.TranslateIdentityType($entity)? Id { get; set; }
#end
#if ($entity.ReadOnlyFields.Count() > 0)
#foreach ($field in $entity.ReadOnlyFields)
#set( $fieldType = "#GetFieldType( $field )" )
      [DataMember]
      public ${fieldType} $field.Name { get; set; }
#end
#end
#foreach ($field in $entity.Fields)
#set( $fieldType = "#GetFieldType( $field )" )
      [DataMember]
      public ${fieldType} $field.Name { get; set; }
#end
#foreach ($field in $entity.ValueObjectMembers)
      [DataMember]
      public ${field.DataType.Name}Dto $field.Name { get; set; }
#end
    }

#foreach ($valueType in $entity.GetCompositeIdentityType())
    public partial class ${valueType.Name}Dto
    {
#foreach ($field in $valueType.Properties)
#set( $fieldType = "#GetFieldType( $field )" )
      [DataMember]
      public ${fieldType} $field.Name { get; set; }
#end
    }
#end
#end

#foreach ($valueType in $ValueObjectTypes)
    public partial class ${valueType.Name}Dto
    {
#foreach ($field in $valueType.Properties)
#set( $fieldType = "#GetFieldType( $field )" )
      [DataMember]
      public ${fieldType} $field.Name { get; set; }
#end
    }
#end   
}
*/

#endregion

#region Data Transfer Objects - Mappers

/*
namespace Focus.Services.DtoMappers
{
    using Focus.Core.DataTransferObjects.${Context.namespace.replace(".Data.", "")};
    using ${Context.namespace};

    public static class ${Context.namespace.replace(".Data.", "")}Mappers
    {
#foreach ($entity in $Entities)     

      #region ${entity.Name}Dto Mappers

      $Translator.TranslateTypeVisibility($entity.TypeAttributes) static ${entity.Name}Dto AsDto(#if ($Context.IncludeLinq)
this #end
$Translator.QualifiedTypeName($entity) entity)
      {
        var dto = new ${entity.Name}Dto
        {
#foreach ($field in $entity.ReadOnlyFields)
				  ${field.Name} =  entity.${field.Name},
#end
#foreach ($field in $entity.Fields)
          ${field.Name} = entity.${field.Name},
#end
#foreach ($field in $entity.ValueObjectMembers)
          ${field.Name} = AsDto(entity.${field.Name}),
#end
				  Id = entity.Id
        };
        return dto;
      }
      
      $Translator.TranslateTypeVisibility($entity.TypeAttributes) static $Translator.QualifiedTypeName($entity) CopyTo(#if ($Context.IncludeLinq)
this #end
${entity.Name}Dto dto, $Translator.QualifiedTypeName($entity) entity)
      {
#foreach ($field in $entity.Fields)
        entity.${field.Name} = dto.${field.Name};
#end
#foreach ($field in $entity.ValueObjectMembers)
        entity.${field.Name} = AsValue(dto.${field.Name});
#end
        return entity;
      }

      $Translator.TranslateTypeVisibility($entity.TypeAttributes) static $Translator.QualifiedTypeName($entity) CopyTo(#if ($Context.IncludeLinq)
this #end
${entity.Name}Dto dto)
      {
        var entity = new $Translator.QualifiedTypeName($entity)();
        return dto.CopyTo(entity);
      }	

#foreach ($type in $entity.GetCompositeIdentityType())
      $Translator.TranslateTypeVisibility($type.TypeAttributes) static ${type.Name}Dto AsDto(#if ($Context.IncludeLinq)
this #end
$Translator.QualifiedTypeName($type) value)
      {
#if (!$type.EmitAsValueType)
        if (value == null)
        {
          return null;
        }
#end
        return new ${type.Name}Dto
        {
#foreach ($field in $type.Properties)
          ${field.Name} = value.${field.Name},
#end
        };
      }
      
      $Translator.TranslateTypeVisibility($type.TypeAttributes) static $Translator.QualifiedTypeName($type) AsValue(#if ($Context.IncludeLinq)
this #end
${type.Name}Dto source)
      {
        return new $Translator.QualifiedTypeName($type)(
#foreach ($field in $type.Properties)
#each
          source.${field.Name} #between
,
#end

        );
      }
#end
      #endregion
#end

#foreach ($type in $ValueObjectTypes)
      $Translator.TranslateTypeVisibility($type.TypeAttributes) static ${type.Name}Dto AsDto(#if ($Context.IncludeLinq)
this #end
$Translator.QualifiedTypeName($type) value)
      {
#if (!$type.EmitAsValueType)
        if (value == null)
        {
          return null;
        }
#end
        return new ${type.Name}Dto
        {
#foreach ($field in $type.Properties)
          ${field.Name} = value.${field.Name},
#end
        };
      }
      
      $Translator.TranslateTypeVisibility($type.TypeAttributes) static $Translator.QualifiedTypeName($type) AsValue(#if ($Context.IncludeLinq)
this #end
${type.Name}Dto source)
      {
        return new $Translator.QualifiedTypeName($type)(
#foreach ($field in $type.Properties)
#each
          source.${field.Name} #between
,
#end

        );
      }
#end
    }
}
*/

#endregion
