﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core.DataTransferObjects.FocusCore;
using Mindscape.LightSpeed;

using Focus.Data.Configuration.Entities;

#endregion

namespace Focus.Data.Repositories
{
	using Contracts;

	public class ConfigurationRepository : RepositoryBase<ConfigurationUnitOfWork>, IConfigurationRepository
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ConfigurationRepository" /> class.
		/// </summary>
		/// <param name="unitOfWorkScope">The unit of work scope.</param>
		public ConfigurationRepository(UnitOfWorkScopeBase<ConfigurationUnitOfWork> unitOfWorkScope) : base(unitOfWorkScope)
		{ }

		#region Entity Queryable Helpers

		/// <summary>
		/// Gets the application images.
		/// </summary>
		/// <value>The application images.</value>
		public IQueryable<ApplicationImage> ApplicationImages
		{
			get { return Query<ApplicationImage>(); }
		}

		/// <summary>
		/// Gets the certificate licence localisation items.
		/// </summary>
		/// <value>The certificate licence localisation items.</value>
		public IQueryable<CertificateLicenceLocalisationItem> CertificateLicenceLocalisationItems
		{
			get { return Query<CertificateLicenceLocalisationItem>(); }
		}

		/// <summary>
		/// Gets the certificate and licenses.
		/// </summary>
		/// <value>The certificate licenses.</value>
		public IQueryable<CertificateLicence> CertificateLicenses
		{
			get { return Query<CertificateLicence>(); }
		}

		/// <summary>
		/// Gets the code group items.
		/// </summary>
		/// <value>The code group items.</value>
		public IQueryable<CodeGroupItem> CodeGroupItems
		{
			get { return Query<CodeGroupItem>(); }
		}

		/// <summary>
		/// Gets the code groups.
		/// </summary>
		/// <value>The code groups.</value>
		public IQueryable<CodeGroup> CodeGroups
		{
			get { return Query<CodeGroup>(); }
		}

		/// <summary>
		/// Gets the code items.
		/// </summary>
		/// <value>The code items.</value>
		public IQueryable<CodeItem> CodeItems
		{
			get { return Query<CodeItem>(); }
		}

		/// <summary>
		/// Gets the configuration items.
		/// </summary>
		/// <value>The configuration items.</value>
		public IQueryable<ConfigurationItem> ConfigurationItems
		{
			get { return Query<ConfigurationItem>(); }
		}

		/// <summary>
		/// Gets the email templates.
		/// </summary>
		/// <value>The email templates.</value>
		public IQueryable<EmailTemplate> EmailTemplates
		{
			get { return Query<EmailTemplate>(); }
		}

		/// <summary>
		/// Gets the localisations.
		/// </summary>
		/// <value>The localisations.</value>
		public IQueryable<Localisation> Localisations
		{
			get { return Query<Localisation>(); }
		}

		/// <summary>
		/// Gets the localisation items.
		/// </summary>
		/// <value>The localisation items.</value>
		public IQueryable<LocalisationItem> LocalisationItems
		{
			get { return Query<LocalisationItem>(); }
		}

		/// <summary>
		/// Gets the default localisation items.
		/// </summary>
		/// <value>The default localisation items.</value>
		public IQueryable<DefaultLocalisationItem> DefaultLocalisationItems
		{
			get { return Query<DefaultLocalisationItem>(); }
		}

		/// <summary>
		/// Gets the external look up items.
		/// </summary>
		/// <value>
		/// The external look up items.
		/// </value>
	  public IQueryable<ExternalLookUpItem> ExternalLookUpItems
	  {
	    get { return Query<ExternalLookUpItem>(); }
	  }

		/// <summary>
		/// Gets the labor insight mappings.
		/// </summary>
		/// <value>
		/// The labor insight mappings.
		/// </value>
		public IQueryable<LaborInsightMapping> LaborInsightMappings
		{
			get { return Query<LaborInsightMapping>(); }
			
		}
		/// <summary>
		/// Gets the labor insight mappings view.
		/// </summary>
		/// <value>
		/// The labor insight mappings view.
		/// </value>
		public IQueryable<LaborInsightMappingView> LaborInsightMappingsView
		{
			get { return Query<LaborInsightMappingView>(); }
		}

		#endregion

		#region View Queryable Helpers

		/// <summary>
		/// Gets the lookup items views.
		/// </summary>
		/// <value>The lookup items views.</value>
		public IQueryable<LookupItemsView> LookupItemsViews
		{
			get { return Query<LookupItemsView>(); }
		}

		#endregion
	}
}
