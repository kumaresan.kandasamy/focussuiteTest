﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Focus.Data.Report.Entities;

using Framework.Core;

using Mindscape.LightSpeed;

#endregion

namespace Focus.Data.Repositories
{
  using Contracts;

  public class ReportRepository : RepositoryBase<ReportUnitOfWork>, IReportRepository
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="ReportRepository" /> class.
    /// </summary>
    /// <param name="unitOfWorkScope">The unit of work scope.</param>
    public ReportRepository(UnitOfWorkScopeBase<ReportUnitOfWork> unitOfWorkScope) : base(unitOfWorkScope)
    { }

    #region Entity Queryable Helpers

    /// <summary>
    /// Gets the employers.
    /// </summary>
    /// <value>The employers.</value>
    public IQueryable<Employer> Employers
    {
      get { return Query<Employer>(); }
    }

    /// <summary>
    /// Gets the employers.
    /// </summary>
    /// <value>The employers.</value>
    public IQueryable<EmployerView> EmployerViews
    {
      get { return Query<EmployerView>(); }
    }

    /// <summary>
    /// Gets the jobs.
    /// </summary>
    /// <value>The jobs.</value>
    public IQueryable<JobOrder> JobOrders
    {
      get { return Query<JobOrder>(); }
    }

    /// <summary>
    /// Gets the jobs.
    /// </summary>
    /// <value>The jobs.</value>
    public IQueryable<JobOrderView> JobOrderViews
    {
      get { return Query<JobOrderView>(); }
    }

    /// <summary>
    /// Gets the staff members.
    /// </summary>
    /// <value>The staff members.</value>
    public IQueryable<StaffMember> StaffMembers
    {
      get { return Query<StaffMember>(); }
    }

    /// <summary>
    /// Gets the job seekers.
    /// </summary>
    /// <value>The job seekers.</value>
    public IQueryable<JobSeeker> JobSeekers
    {
      get { return Query<JobSeeker>(); }
    }

		/// <summary>
		/// Gets the job seeker job histories.
		/// </summary>
		/// <value> The job seeker job histories. </value>
		public IQueryable<JobSeekerJobHistory> JobSeekerJobHistories
		{
			get { return Query<JobSeekerJobHistory>(); }
		}

    /// <summary>
    /// Gets the job seeker actions.
    /// </summary>
    /// <value>The job seeker actions.</value>
    public IQueryable<JobSeekerAction> JobSeekerActions
    {
      get { return Query<JobSeekerAction>(); }
    }

    /// <summary>
    /// Gets the activity assignments.
    /// </summary>
    /// <value>The activity assignments.</value>
    public IQueryable<JobSeekerActivityAssignment> JobSeekerActivityAssignments
    {
      get { return Query<JobSeekerActivityAssignment>(); }
    }

    /// <summary>
    /// Gets the saved reports.
    /// </summary>
    /// <value>The saved reports.</value>
    public IQueryable<SavedReport> SavedReports
    {
      get { return Query<SavedReport>(); }
    }

		/// <summary>
		/// Gets the statistics.
		/// </summary>
		/// <value> The statistics. </value>
		public IQueryable<Statistic> Statistics
		{
			get { return Query<Statistic>(); }
		}

		/// <summary>
		/// Gets the job seeker non purged.
		/// </summary>
		/// <value>
		/// The job seeker non purged.
		/// </value>
		public IQueryable<JobSeeker_NonPurged> JobSeekerNonPurged
		{
			get { return Query<JobSeeker_NonPurged>(); }
		}

		/// <summary>
		/// Gets the job seeker military history non purged.
		/// </summary>
		/// <value>
		/// The job seeker military history non purged.
		/// </value>
		public IQueryable<JobSeekerMilitaryHistory_NonPurged> JobSeekerMilitaryHistoryNonPurged
		{
			get { return Query<JobSeekerMilitaryHistory_NonPurged>(); }
		}

    #endregion

    #region Stored Procedure Queryable Helpers

    /// <summary>
    /// Gets the action totals for job seekers
    /// </summary>
    /// <param name="fromDate">The first date of actions</param>
    /// <param name="toDate">The last date of actions</param>
    /// <param name="jobSeekerIds">An optional list of job seeker Ids</param>
    /// <returns>A list of job seeker action totals</returns>
    public IQueryable<JobSeekerAction> JobSeekerActionTotals(DateTime fromDate, DateTime toDate, List<long> jobSeekerIds = null)
    {
      if (jobSeekerIds.IsNullOrEmpty())
        return UnitOfWorkScope.Current.JobSeekerActionTotals(fromDate, toDate).AsQueryable();

      var jobSeekerXml = string.Join("", jobSeekerIds.Select(id => string.Concat("<id>", id, "</id>")));
      return UnitOfWorkScope.Current.JobSeekerActionTotalsForIds(fromDate, toDate, jobSeekerXml).AsQueryable();
    }

    /// <summary>
    /// Gets the activity totals for job seekers
    /// </summary>
    /// <param name="fromDate">The first date of actions</param>
    /// <param name="toDate">The last date of actions</param>
    /// <param name="categoryKey">The required activity category</param>
    /// <param name="activityKey">An optional activity</param>
    /// <param name="jobSeekerIds">An optional list of job seeker Ids</param>
    /// <returns>A list of job seeker activity totals</returns>
    public IQueryable<JobSeekerActivityAssignmentsTotal> JobSeekerActivityTotals(DateTime fromDate, DateTime toDate, string categoryKey, string activityKey, List<long> jobSeekerIds = null)
    {
      if (jobSeekerIds.IsNullOrEmpty())
        return UnitOfWorkScope.Current.JobSeekerActivityTotals(fromDate, toDate, categoryKey, activityKey).AsQueryable();

      var jobSeekerXml = string.Join("", jobSeekerIds.Select(id => string.Concat("<id>", id, "</id>")));
      return UnitOfWorkScope.Current.JobSeekerActivityTotalsForIds(fromDate, toDate, categoryKey, activityKey, jobSeekerXml).AsQueryable();
    }

    /// <summary>
    /// Gets the action totals for job orders
    /// </summary>
    /// <param name="fromDate">The first date of actions</param>
    /// <param name="toDate">The last date of actions</param>
    /// <param name="jobOrderIds">An optional list of job order Ids</param>
    /// <returns>A list of job order action totals</returns>
    public IQueryable<JobOrderAction> JobOrderActionTotals(DateTime fromDate, DateTime toDate, List<long> jobOrderIds = null)
    {
      if (jobOrderIds.IsNullOrEmpty())
        return UnitOfWorkScope.Current.JobOrderActionTotals(fromDate, toDate).AsQueryable();

      var jobOrderXml = string.Join("", jobOrderIds.Select(id => string.Concat("<id>", id, "</id>")));
      return UnitOfWorkScope.Current.JobOrderActionTotalsForIds(fromDate, toDate, jobOrderXml).AsQueryable();
    }

    /// <summary>
    /// Gets the action totals for employers
    /// </summary>
    /// <param name="fromDate">The first date of actions</param>
    /// <param name="toDate">The last date of actions</param>
    /// <param name="employerIds">An optional list of employer Ids</param>
    /// <returns>A list of employer action totals</returns>
    public IQueryable<EmployerAction> EmployerActionTotals(DateTime fromDate, DateTime toDate, List<long> employerIds = null)
    {
      if (employerIds.IsNullOrEmpty())
        return UnitOfWorkScope.Current.EmployerActionTotals(fromDate, toDate).AsQueryable();

      var jobOrderXml = string.Join("", employerIds.Select(id => string.Concat("<id>", id, "</id>")));
      return UnitOfWorkScope.Current.EmployerActionTotalsForIds(fromDate, toDate, jobOrderXml).AsQueryable();
    }

    /// <summary>
    /// Gets the job seeker ids matching the keyword filter
    /// </summary>
    /// <param name="keywords">The keywords to check</param>
    /// <param name="typesToCheck">The types to check</param>
    /// <param name="searchAll">Whether to search all keywords or any</param>
    /// <returns>A list of job seeker ids matching the filter</returns>
    public IQueryable<KeywordFilterView> JobSeekerKeywordFilter(List<string> keywords, List<int> typesToCheck, bool searchAll)
    {
      var keywordXml = string.Join("", keywords.Select(keyword => string.Concat("<keyword>", HttpUtility.HtmlEncode(keyword), "</keyword>")));
      var typesXml = string.Join("", typesToCheck.Select(context => string.Concat("<type>", context, "</type>")));

      return UnitOfWorkScope.Current.JobSeekerKeywordFilter(keywordXml, typesXml, searchAll).AsQueryable();
    }

    /// <summary>
    /// Gets the job order ids matching the keyword filter
    /// </summary>
    /// <param name="keywords">The keywords to check</param>
    /// <param name="checkTitle">Whether to check the job title</param>
    /// <param name="checkDescription">Whether to check the job description</param>
    /// <param name="checkEmployer">Whether to check the employer</param>
    /// <param name="searchAll">Whether to search all keywords or any</param>
    /// <returns>A list of job order ids matching the filter</returns>
    public IQueryable<KeywordFilterView> JobOrderKeywordFilter(List<string> keywords, bool checkTitle, bool checkDescription, bool checkEmployer, bool searchAll)
    {
      var keywordXml = string.Join("", keywords.Select(keyword => string.Concat("<keyword>", HttpUtility.HtmlEncode(keyword), "</keyword>")));
      return UnitOfWorkScope.Current.JobOrderKeywordFilter(keywordXml, checkTitle, checkDescription, checkEmployer, searchAll).AsQueryable();
    }

    /// <summary>
    /// Gets the employer ids matching the keyword filter
    /// </summary>
    /// <param name="keywords">The keywords to check</param>
    /// <param name="checkName">Whether to check the employer name</param>
    /// <param name="checkJobTitle">Whether to check the job title</param>
    /// <param name="checkJobDescription">Whether to check the job description</param>
    /// <param name="searchAll">Whether to search all keywords or any</param>
    /// <returns>A list of employer ids matching the filter</returns>
    public IQueryable<KeywordFilterView> EmployerKeywordFilter(List<string> keywords, bool checkName, bool checkJobTitle, bool checkJobDescription, bool searchAll)
    {
      var keywordXml = string.Join("", keywords.Select(keyword => string.Concat("<keyword>", HttpUtility.HtmlEncode(keyword), "</keyword>")));
      return UnitOfWorkScope.Current.EmployerKeywordFilter(keywordXml, checkName, checkJobTitle, checkJobDescription, searchAll).AsQueryable();
    }

		/// <summary>
		/// Jobs the seeker office name update.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		public void JobSeekerOfficeNameUpdate(long officeId)
		{
			UnitOfWorkScope.Current.JobSeekerOfficeNameUpdate(officeId);
		}

		/// <summary>
		/// Employers the office name update.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		public void EmployerOfficeNameUpdate(long officeId)
		{
			UnitOfWorkScope.Current.EmployerOfficeNameUpdate(officeId);
		}

		/// <summary>
		/// Jobs the order office name update.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		public void JobOrderOfficeNameUpdate(long officeId)
		{
			UnitOfWorkScope.Current.JobOrderOfficeNameUpdate(officeId);
		}

		/// <summary>
		/// Staffs the member office name update.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		public void StaffMemberOfficeNameUpdate(long officeId)
		{
			UnitOfWorkScope.Current.StaffMemberOfficeNameUpdate(officeId);
		}

    #endregion

	}
}
