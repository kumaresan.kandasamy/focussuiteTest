﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Data;

using Mindscape.LightSpeed;

using Focus.Data.Reporting.Entities;

#endregion

namespace Focus.Data.Repositories
{
	using Contracts;

	public class ReportingRepository : RepositoryBase<DataReportingUnitOfWork>, IReportingRepository
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ReportingRepository" /> class.
		/// </summary>
		/// <param name="unitOfWorkScope">The unit of work scope.</param>
		public ReportingRepository(UnitOfWorkScopeBase<DataReportingUnitOfWork> unitOfWorkScope) : base(unitOfWorkScope)
		{ }
	}
}
