﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Data;
using System.Linq;
using Focus.Data.Library.Entities;

#endregion

namespace Focus.Data.Repositories.Contracts
{
	public interface ILibraryRepository : IRepository
	{
		#region Core methods

		/// <summary>
		/// Executes the SQL.
		/// </summary>
		/// <param name="sql">The SQL.</param>
		/// <returns></returns>
		IDataReader ExecuteSql(string sql);

		#endregion
		
		#region Entity Queryable Helpers

		/// <summary>
		/// Gets the job tasks.
		/// </summary>
		/// <value>The job tasks.</value>
		IQueryable<JobTask> JobTasks { get; }

		/// <summary>
		/// Gets the job task localisation items.
		/// </summary>
		/// <value>The job task localisation items.</value>
		IQueryable<JobTaskLocalisationItem> JobTaskLocalisationItems { get; }

		/// <summary>
		/// Gets the job task multi options.
		/// </summary>
		/// <value>The job task multi options.</value>
		IQueryable<JobTaskMultiOption> JobTaskMultiOptions { get; }
		
		/// <summary>
		/// Gets the localisations.
		/// </summary>
		/// <value>The localisations.</value>
		IQueryable<Localisation> Localisations { get; }

		/// <summary>
		/// Gets the localisation items.
		/// </summary>
		/// <value>The localisation items.</value>
		IQueryable<LocalisationItem> LocalisationItems { get; }

		/// <summary>
		/// Gets the military occupations.
		/// </summary>
		/// <value>The military occupations.</value>
		IQueryable<MilitaryOccupation> MilitaryOccupations { get; }

		/// <summary>
		/// Gets the military occupation groups.
		/// </summary>
		/// <value>The military occupation groups.</value>
		IQueryable<MilitaryOccupationGroup> MilitaryOccupationGroups { get; }

		/// <summary>
		/// Gets the notes.
		/// </summary>
		/// <value>The notes.</value>
		IQueryable<NAICS> NAICS { get; }

		/// <summary>
		/// Gets the onet commodities.
		/// </summary>
		/// <value>
		/// The onet commodities.
		/// </value>
		IQueryable<OnetCommodity> OnetCommodities { get; }


		/// <summary>
		/// Gets the onets.
		/// </summary>
		/// <value>The onets.</value>
		IQueryable<Onet> Onets { get; }

    /// <summary>
    /// Gets the OnetROneta.
    /// </summary>
    /// <value>The OnetROnet.</value>
    IQueryable<OnetROnet> OnetROnet { get; }


		/// <summary>
		/// Gets the onet localisation items.
		/// </summary>
		/// <value>The onet localisation items.</value>
		IQueryable<OnetLocalisationItem> OnetLocalisationItems { get; }

		/// <summary>
		/// Gets the onet phrases.
		/// </summary>
		/// <value>The onet phrases.</value>
		IQueryable<OnetPhrase> OnetPhrases { get; }

		/// <summary>
		/// Gets the onet rings.
		/// </summary>
		/// <value>The onet rings.</value>
		IQueryable<OnetRing> OnetRings { get; }

		/// <summary>
		/// Gets the onet tasks.
		/// </summary>
		/// <value>The onet tasks.</value>
		IQueryable<OnetTask> OnetTasks { get; }

		/// <summary>
		/// Gets the onet words.
		/// </summary>
		/// <value>The onet words.</value>
		IQueryable<OnetWord> OnetWords { get; }


		/// <summary>
		/// Gets the resume skills.
		/// </summary>
		/// <value>The resume skills.</value>
		IQueryable<ResumeSkill> ResumeSkills { get; }

    /// <summary>
    /// Gets the ronets.
    /// </summary>
    /// <value>The ronets.</value>
    IQueryable<ROnet> ROnets { get; }

		/// <summary>
		/// Gets the ronet onets.
		/// </summary>
		/// <value>The ronet onets.</value>
		IQueryable<ROnetOnet> ROnetOnets { get; } 
			
		/// <summary>
		/// Gets the skills.
		/// </summary>
		/// <value>The skills.</value>
		IQueryable<Skill> Skills { get; }

		/// <summary>
		/// Gets the standard industrial classifications.
		/// </summary>
		/// <value>The standard industrial classifications.</value>
		IQueryable<StandardIndustrialClassification> StandardIndustrialClassifications { get; }

    /// <summary>
    /// Gets the education internship statements.
    /// </summary>
    /// <value>The education internship statements.</value>
    IQueryable<EducationInternshipStatement> EducationInternshipStatements { get; }

    /// <summary>
    /// Gets the education internship statements.
    /// </summary>
    /// <value>The education internship statements.</value>
    IQueryable<EducationInternshipSkill> EducationInternshipSkills { get; }

    /// <summary>
    /// Gets the education internship statements.
    /// </summary>
    /// <value>The education internship statements.</value>
    IQueryable<EducationInternshipCategory> EducationInternshipCategories { get; }

    /// <summary>
    /// Gets the onetSOCs.
    /// </summary>
    /// <value>The onetSOCs.</value>
    IQueryable<OnetSOC> OnetSOCs { get; }

    /// <summary>
    /// Gets the SOCs.
    /// </summary>
    /// <value>The SOCs.</value>
    IQueryable<SOC> SOCs { get; }

		/// <summary>
		/// Gets the jobs.
		/// </summary>
		/// <value>The jobs.</value>
		IQueryable<Job> Jobs { get; }

		/// <summary>
		/// Gets the career areas.
		/// </summary>
		/// <value>
		/// The career areas.
		/// </value>
		IQueryable<CareerArea> CareerAreas { get; }

			#endregion

		#region View Queryable Helpers

		/// <summary>
		/// Gets the military occupation job title views.
		/// </summary>
		/// <value>The military occupation job title views.</value>
		IQueryable<MilitaryOccupationJobTitleView> MilitaryOccupationJobTitleViews { get; }

		/// <summary>
		/// Gets the onet word views.
		/// </summary>
		/// <value>The onet word views.</value>
		IQueryable<OnetWordView> OnetWordViews { get; }
		
		/// <summary>
		/// Gets the onet word job task views.
		/// </summary>
		/// <value>The onet word job task views.</value>
		IQueryable<OnetWordJobTaskView> OnetWordJobTaskViews { get; }

    /// <summary>
    /// Gets the job degree education level views.
    /// </summary>
    /// <value>The job degree education level views.</value>
    IQueryable<JobDegreeEducationLevelView> JobDegreeEducationLevelViews { get; }

		/// <summary>
		/// Gets the onet17 onet12 mapping views.
		/// </summary>
		/// <value>The onet17 onet12 mapping views.</value>
		IQueryable<Onet17Onet12MappingView> Onet17Onet12MappingViews { get; }

	  IQueryable<OnetToROnetConversionView> OnetToROnetConversionViews { get; }
	
		#endregion
	}
}
