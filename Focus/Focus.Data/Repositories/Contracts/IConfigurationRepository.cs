﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Data.Configuration.Entities;

#endregion


namespace Focus.Data.Repositories.Contracts
{
	public interface IConfigurationRepository : IRepository
	{
		#region Entity Queryable Helpers

		/// <summary>
		/// Gets the application images.
		/// </summary>
		/// <value>The application images.</value>
		IQueryable<ApplicationImage> ApplicationImages { get; }

		/// <summary>
		/// Gets the certificate licence localisation items.
		/// </summary>
		/// <value>The certificate licence localisation items.</value>
		IQueryable<CertificateLicenceLocalisationItem> CertificateLicenceLocalisationItems { get; }
		
		/// <summary>
		/// Gets the certificate and licenses.
		/// </summary>
		/// <value>The certificate licenses.</value>
		IQueryable<CertificateLicence> CertificateLicenses { get; }

		/// <summary>
		/// Gets the code group items.
		/// </summary>
		/// <value>The code group items.</value>
		IQueryable<CodeGroupItem> CodeGroupItems { get; }
		
		/// <summary>
		/// Gets the code groups.
		/// </summary>
		/// <value>The code groups.</value>
		IQueryable<CodeGroup> CodeGroups { get; }

		/// <summary>
		/// Gets the code items.
		/// </summary>
		/// <value>The code items.</value>
		IQueryable<CodeItem> CodeItems { get; }

		/// <summary>
		/// Gets the configuration items.
		/// </summary>
		/// <value>The configuration items.</value>
		IQueryable<ConfigurationItem> ConfigurationItems { get; }

		/// <summary>
		/// Gets the email templates.
		/// </summary>
		/// <value>The email templates.</value>
		IQueryable<EmailTemplate> EmailTemplates { get; }

		/// <summary>
		/// Gets the localisations.
		/// </summary>
		/// <value>The localisations.</value>
		IQueryable<Localisation> Localisations { get; }

		/// <summary>
		/// Gets the localisation items.
		/// </summary>
		/// <value>The localisation items.</value>
		IQueryable<LocalisationItem> LocalisationItems { get; }

		/// <summary>
		/// Gets the default localisation items.
		/// </summary>
		/// <value>The default localisation items.</value>
		IQueryable<DefaultLocalisationItem> DefaultLocalisationItems { get; }

		/// <summary>
		/// Gets the external look up items.
		/// </summary>
		/// <value>
		/// The external look up items.
		/// </value>
		IQueryable<ExternalLookUpItem> ExternalLookUpItems { get; }

		/// <summary>
		/// Gets the labor insight mappings
		/// </summary>
		/// <value>
		/// The labor insight mappings.
		/// </value>
		IQueryable<LaborInsightMapping> LaborInsightMappings { get; }

		/// <summary>
		/// Gets the labor insight mapping view.
		/// </summary>
		/// <value>
		/// The labor insight mapping view.
		/// </value>
		IQueryable<LaborInsightMappingView> LaborInsightMappingsView { get; }
		
		#endregion

		#region View Queryable Helpers

		/// <summary>
		/// Gets the lookup items views.
		/// </summary>
		/// <value>The lookup items views.</value>
		IQueryable<LookupItemsView> LookupItemsViews { get; }

		#endregion
	}
}
