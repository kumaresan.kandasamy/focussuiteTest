﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Data.Migration.Entities;

#endregion

namespace Focus.Data.Repositories.Contracts
{
	public interface IMigrationRepository : IRepository
	{
		#region Entity Queryable Helpers

		/// <summary>
		/// Gets the employer requests.
		/// </summary>
		/// <value>The employer requests.</value>
		IQueryable<EmployerRequest> EmployerRequests { get; }

		/// <summary>
		/// Gets the job order requests.
		/// </summary>
		/// <value>The job order requests.</value>
		IQueryable<JobOrderRequest> JobOrderRequests { get; }

		/// <summary>
		/// Gets the job seeker requests.
		/// </summary>
		/// <value>The job seeker requests.</value>
		IQueryable<JobSeekerRequest> JobSeekerRequests { get; }

		/// <summary>
		/// Gets the user requests.
		/// </summary>
		/// <value>The user requests.</value>
		IQueryable<UserRequest> UserRequests { get; }

    /// <summary>
    /// Gets the batch records
    /// </summary>
    /// <value>The batch records.</value>
    IQueryable<BatchRecord> BatchRecords { get; }

		#endregion
	}
}
