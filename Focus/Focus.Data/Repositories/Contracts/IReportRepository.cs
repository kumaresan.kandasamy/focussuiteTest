﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Data.Report.Entities;

#endregion

namespace Focus.Data.Repositories.Contracts
{
  public interface IReportRepository : IRepository
  {
    #region Entity Queryable Helpers

    /// <summary>
    /// Gets the employers.
    /// </summary>
    /// <value>The employers.</value>
    IQueryable<Employer> Employers { get; }

    /// <summary>
    /// Gets the employers.
    /// </summary>
    /// <value>The employers.</value>
    IQueryable<EmployerView> EmployerViews { get; }

    /// <summary>
    /// Gets the jobs.
    /// </summary>
    /// <value>The jobs.</value>
    IQueryable<JobOrder> JobOrders { get; }

    /// <summary>
    /// Gets the jobs.
    /// </summary>
    /// <value>The jobs.</value>
    IQueryable<JobOrderView> JobOrderViews { get; }

    /// <summary>
    /// Gets the staff members.
    /// </summary>
    /// <value>The staff members.</value>
    IQueryable<StaffMember> StaffMembers { get; }

    /// <summary>
    /// Gets the job seekers.
    /// </summary>
    /// <value>The job seekers.</value>
    IQueryable<JobSeeker> JobSeekers { get; }

		/// <summary>
		/// Gets the job seeker job histories.
		/// </summary>
		/// <value> The job seeker job histories. </value>
		IQueryable<JobSeekerJobHistory> JobSeekerJobHistories { get; }

    /// <summary>
    /// Gets the actions.
    /// </summary>
    /// <value>The actions.</value>
    IQueryable<JobSeekerAction> JobSeekerActions { get; }

    /// <summary>
    /// Gets the activity assignments.
    /// </summary>
    /// <value>The activity assignments.</value>
    IQueryable<JobSeekerActivityAssignment> JobSeekerActivityAssignments { get; }

    /// <summary>
    /// Gets the saved reports.
    /// </summary>
    /// <value>The saved reports.</value>
    IQueryable<SavedReport> SavedReports { get; }

		/// <summary>
		/// Gets the statistics.
		/// </summary>
		/// <value> The statistics. </value>
		IQueryable<Statistic> Statistics { get; }

		/// <summary>
		/// Gets the job seeker non purged.
		/// </summary>
		/// <value>
		/// The job seeker non purged.
		/// </value>
		IQueryable<JobSeeker_NonPurged> JobSeekerNonPurged { get; }

		/// <summary>
		/// Gets the job seeker military history non purged.
		/// </summary>
		/// <value>
		/// The job seeker military history non purged.
		/// </value>
		IQueryable<JobSeekerMilitaryHistory_NonPurged> JobSeekerMilitaryHistoryNonPurged { get; }

			#endregion

		#region Stored Procedure Queryable Helpers
		
		/// <summary>
    /// Gets the action totals for job seekers
    /// </summary>
    /// <param name="fromDate">The first date of actions</param>
    /// <param name="toDate">The last date of actions</param>
    /// <param name="jobSeekerIds">An optional list of job seeker Ids</param>
    /// <returns>A list of job seeker action totals</returns>
    IQueryable<JobSeekerAction> JobSeekerActionTotals(DateTime fromDate, DateTime toDate, List<long> jobSeekerIds = null);

    /// <summary>
    /// Gets the activity totals for job seekers
    /// </summary>
    /// <param name="fromDate">The first date of actions</param>
    /// <param name="toDate">The last date of actions</param>
    /// <param name="categoryKey">The required activity category</param>
    /// <param name="activityKey">An optional activity</param>
    /// <param name="jobSeekerIds">An optional list of job seeker Ids</param>
    /// <returns>A list of job seeker activity totals</returns>
    IQueryable<JobSeekerActivityAssignmentsTotal> JobSeekerActivityTotals(DateTime fromDate, DateTime toDate, string categoryKey, string activityKey, List<long> jobSeekerIds = null);

    /// <summary>
    /// Gets the action totals for job orders
    /// </summary>
    /// <param name="fromDate">The first date of actions</param>
    /// <param name="toDate">The last date of actions</param>
    /// <param name="jobOrderIds">An optional list of job order Ids</param>
    /// <returns>A list of job order action totals</returns>
    IQueryable<JobOrderAction> JobOrderActionTotals(DateTime fromDate, DateTime toDate, List<long> jobOrderIds = null);

    /// <summary>
    /// Gets the action totals for employers
    /// </summary>
    /// <param name="fromDate">The first date of actions</param>
    /// <param name="toDate">The last date of actions</param>
    /// <param name="employerIds">An optional list of employer Ids</param>
    /// <returns>A list of employer action totals</returns>
    IQueryable<EmployerAction> EmployerActionTotals(DateTime fromDate, DateTime toDate, List<long> employerIds = null);

    /// <summary>
    /// Gets the job seeker ids matching the keyword filter
    /// </summary>
    /// <param name="keywords">The keywords to check</param>
    /// <param name="typesToCheck">The types to check</param>
    /// <param name="searchAll">Whether to search all keywords or any</param>
    /// <returns>A list of job seeker ids matching the filter</returns>
    IQueryable<KeywordFilterView> JobSeekerKeywordFilter(List<string> keywords, List<int> typesToCheck, bool searchAll);

    /// <summary>
    /// Gets the job order ids matching the keyword filter
    /// </summary>
    /// <param name="keywords">The keywords to check</param>
    /// <param name="checkTitle">Whether to check the job title</param>
    /// <param name="checkDescription">Whether to check the job description</param>
    /// <param name="checkEmployer">Whether to check the employer</param>
    /// <param name="searchAll">Whether to search all keywords or any</param>
    /// <returns>A list of job order ids matching the filter</returns>
    IQueryable<KeywordFilterView> JobOrderKeywordFilter(List<string> keywords, bool checkTitle, bool checkDescription, bool checkEmployer, bool searchAll);

    /// <summary>
    /// Gets the employer ids matching the keyword filter
    /// </summary>
    /// <param name="keywords">The keywords to check</param>
    /// <param name="checkName">Whether to check the employer name</param>
    /// <param name="checkJobTitle">Whether to check the job title</param>
    /// <param name="checkJobDescription">Whether to check the job description</param>
    /// <param name="searchAll">Whether to search all keywords or any</param>
    /// <returns>A list of employer ids matching the filter</returns>
    IQueryable<KeywordFilterView> EmployerKeywordFilter(List<string> keywords, bool checkName, bool checkJobTitle, bool checkJobDescription, bool searchAll);

		/// <summary>
		/// Jobs the seeker office name update.
		/// </summary>
		/// <param name="officeId">The office id.</param>
	  void JobSeekerOfficeNameUpdate(long officeId);

		/// <summary>
		/// Employers the office name update.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		void EmployerOfficeNameUpdate(long officeId);

		/// <summary>
		/// Jobs the order office name update.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		void JobOrderOfficeNameUpdate(long officeId);

		/// <summary>
		/// Staffs the member office name update.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		void StaffMemberOfficeNameUpdate(long officeId);

	  #endregion
  }
}
