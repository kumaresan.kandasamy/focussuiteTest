﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

using Focus.Core;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;

#endregion

namespace Focus.Data.Repositories.Contracts
{
	public interface IRepository
	{
		/// <summary>
		/// Adds the specified entity to the repository.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="entity">The entity.</param>
		void Add<T>(T entity) where T : Entity;

		/// <summary>
		/// Removes the specified entity from the repository.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="entity">The entity.</param>
		void Remove<T>(T entity) where T : Entity;

		/// <summary>
		/// Removes the entity / entities from the repository from the specified where expression.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="selector">The selector.</param>
		void Remove<T>(Expression<Func<T, bool>> selector) where T : Entity;

		/// <summary>
		/// Removes the entity / entities from the repository using the specified query.
		/// </summary>
		/// <param name="query">The query.</param>
		void Remove(Query query);

		/// <summary>
		/// Updates the entity / entities in the repository using the specified query.
		/// </summary>
		/// <param name="query">The query.</param>
		/// <param name="attributes">The attributes.</param>
		void Update(Query query, object attributes);

		/// <summary>
		/// Counts all the entities in the repository.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		long Count<T>() where T : Entity;

		/// <summary>
		/// Counts the entities in the repository for the specified where expression.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="selector">The selector.</param>
		/// <returns></returns>
		long Count<T>(Expression<Func<T, bool>> selector) where T : Entity;

		/// <summary>
		/// Checks if the entities exists in the repository for the specified where expression.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="selector">The selector.</param>
		/// <returns></returns>
		bool Exists<T>(Expression<Func<T, bool>> selector) where T : Entity;

		/// <summary>
		/// Finds the entity by the id.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		T FindById<T>(object id) where T : Entity;

		/// <summary>
		/// Finds a list of all entities in the repository.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		IList<T> Find<T>() where T : Entity;

		/// <summary>
		/// Finds a list of entities in the repository using the given the specified where expression.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="selector">The selector.</param>
		/// <returns></returns>
		IList<T> Find<T>(Expression<Func<T, bool>> selector) where T : Entity;

		/// <summary>
		/// Queries this repository using a Linq query.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		IQueryable<T> Query<T>() where T : Entity;

		/// <summary>
		/// Finds and pages a list of entities in the repository using the given the specified where expression.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="query">The query.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <returns></returns>
		PagedList<T> FindPaged<T>(IQueryable<T> query, int pageIndex, int pageSize) where T : Entity;

		/// <summary>
		/// Saves the changes to the repository.
		/// </summary>
		/// <param name="reset">if set to <c>true</c> then the repository is reset.</param>
		void SaveChanges(bool reset = false);

		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="sql">The SQL.</param>
		/// <returns></returns>
		int ExecuteNonQuery(string sql);

    /// <summary>
    /// Executes the query and returns the result
    /// </summary>
    /// <param name="sql">The SQL.</param>
    /// <returns></returns>
    T ExecuteScalar<T>(string sql);

    /// <summary>
    /// Executes the SQL to return a record set
    /// </summary>
    /// <param name="sql">The SQL.</param>
    /// <returns>A data reader</returns>
    IDataReader ExecuteDataReader(string sql);

    /// <summary>
    /// Event handler for when state changes for an entity in the repository
    /// </summary>
    event EventHandler<RepositoryEntityStateChangedEventArgs> EntityStateChanged;

		/// <summary>
		/// Sets the CreatedOn field
		/// </summary>
		/// <param name="entity">Target entity</param>
		/// <param name="id">Id of row to update</param>
		/// <param name="createdOn">New value for CreatedOn field</param>
		void SetCreatedOn(Entity entity, long id, DateTime createdOn);

		/// <summary>
		/// Sets the DeletedOn field
		/// </summary>
		/// <param name="entity">Target entity</param>
		/// <param name="id">Id of row to update</param>
		/// <param name="deletedOn">New value for DeletedOn field</param>
		void SetDeletedOn( Entity entity, long id, DateTime deletedOn );

		/// <summary>
		/// Sets the UpdatedOn field
		/// </summary>
		/// <param name="entity">Target entity</param>
		/// <param name="id">Id of row to update</param>
		/// <param name="updatedOn">New value for UpdatedOn field</param>
		void SetUpdatedOn( Entity entity, long id, DateTime updatedOn );

		/// <summary>
		/// Sets the ActionedOn field
		/// </summary>
		/// <param name="entity">Target entity</param>
		/// <param name="id">Id of row to update</param>
		/// <param name="actionedOn">New value for ActionedOn field</param>
		void SetActionedOn(Entity entity, long id, DateTime actionedOn);
	}
}
