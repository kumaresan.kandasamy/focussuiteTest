﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Data.Core.Entities;

#endregion

namespace Focus.Data.Repositories.Contracts
{
	public interface ICoreRepository : IRepository
	{
		#region Entity Queryable Helpers

		/// <summary>
		/// Gets the action events.
		/// </summary>
		/// <value>The action events.</value>
		IQueryable<ActionEvent> ActionEvents { get; }

        IQueryable<BackdatedActionEvents> BackdatedActionEvent { get; }

		/// <summary>
		/// Gets the action types.
		/// </summary>
		/// <value>The action types.</value>
		IQueryable<ActionType> ActionTypes { get; }

		/// <summary>
		/// Gets the applications.
		/// </summary>
		/// <value>The applications.</value>
		IQueryable<Application> Applications { get; }

		/// <summary>
		/// Gets the business units.
		/// </summary>
		/// <value>The business units.</value>
		IQueryable<BusinessUnit> BusinessUnits { get; }

		/// <summary>
		/// Gets the business unit addresses.
		/// </summary>
		/// <value>The business unit addresses.</value>
		IQueryable<BusinessUnitAddress> BusinessUnitAddresses { get; }

		/// <summary>
		/// Gets the business unit descriptions.
		/// </summary>
		/// <value>The business unit descriptions.</value>
		IQueryable<BusinessUnitDescription> BusinessUnitDescriptions { get; }

		/// <summary>
		/// Gets the business unit logos.
		/// </summary>
		/// <value>The business unit logos.</value>
		IQueryable<BusinessUnitLogo> BusinessUnitLogos { get; }

		/// <summary>
		/// Gets the dismissed messages.
		/// </summary>
		/// <value>The dismissed messages.</value>
		IQueryable<DismissedMessage> DismissedMessages { get; }

		/// <summary>
		/// Gets the employees.
		/// </summary>
		/// <value>The employees.</value>
		IQueryable<Employee> Employees { get; }

		/// <summary>
		/// Gets the employee business units.
		/// </summary>
		/// <value>The employee business units.</value>
		IQueryable<EmployeeBusinessUnit> EmployeeBusinessUnits { get; }

		/// <summary>
		/// Gets the business unit activity.
		/// </summary>
		/// <value>The business unit activity.</value>
		IQueryable<BusinessUnitActivityView> BusinessUnitActivity { get; }

		/// <summary>
		/// Gets the business unit placements.
		/// </summary>
		/// <value>The business unit placements.</value>
		IQueryable<BusinessUnitPlacementsView> BusinessUnitPlacements { get; }

		/// <summary>
		/// Gets the open position matched.
		/// </summary>
		/// <value>The open position matched.</value>
		IQueryable<OpenPositionMatchesView> OpenPositionMatchViews { get; }

		/// <summary>
		/// Gets the employers.
		/// </summary>
		/// <value>The employers.</value>
		IQueryable<Employer> Employers { get; }

		/// <summary>
		/// Gets the employer addresses.
		/// </summary>
		/// <value>The employer addresses.</value>
		IQueryable<EmployerAddress> EmployerAddresses { get; }

		/// <summary>
		/// Gets the entity types.
		/// </summary>
		/// <value>The entity types.</value>
		IQueryable<EntityType> EntityTypes { get; }

		/// <summary>
		/// Gets the excluded lens postings.
		/// </summary>
		/// <value> The excluded lens postings. </value>
		IQueryable<ExcludedLensPosting> ExcludedLensPostings { get; }

    /// <summary>
    /// Gets the issues.
    /// </summary>
    /// <value>The issues.</value>
    IQueryable<Issues> Issues { get; }

		/// <summary>
		/// Gets the jobs.
		/// </summary>
		/// <value>The jobs.</value>
		IQueryable<Job> Jobs { get; set; }

		/// <summary>
		/// Gets the jobs.
		/// </summary>
		/// <value>The jobs.</value>
		IQueryable<JobPrevData> JobPrevDatas { get; set; }

		/// <summary>
		/// Gets or sets the job certificates.
		/// </summary>
		/// <value>The job certificates.</value>
		IQueryable<JobCertificate> JobCertificates { get; }

		/// <summary>
		/// Gets the job addresses.
		/// </summary>
		/// <value>The job addresses.</value>
		IQueryable<JobAddress> JobAddresses { get; }

		/// <summary>
		/// Gets the job driving licence endorsements.
		/// </summary>
		/// <value>
		/// The job driving licence endorsements.
		/// </value>
		IQueryable<JobDrivingLicenceEndorsement> JobDrivingLicenceEndorsements { get; }
		
		/// <summary>
		/// Gets the job languages.
		/// </summary>
		/// <value>The job languages.</value>
		IQueryable<JobLanguage> JobLanguages { get; }

		/// <summary>
		/// Gets the job licenses.
		/// </summary>
		/// <value>The job licenses.</value>
		IQueryable<JobLicence> JobLicences { get; }

		/// <summary>
		/// Gets the job locations.
		/// </summary>
		/// <value>The job locations.</value>
		IQueryable<JobLocation> JobLocations { get; }

		/// <summary>
		/// Gets the job programs of study.
		/// </summary>
		/// <value>The job programs of study.</value>
		IQueryable<JobProgramOfStudy> JobProgramsOfStudy { get; }
			
		/// <summary>
		/// Gets the job notes.
		/// </summary>
		/// <value>The job notes.</value>
		IQueryable<JobNote> JobNotes { get; }

		/// <summary>
		/// Gets the job skills.
		/// </summary>
		/// <value>The job skills.</value>
		IQueryable<JobSkill> JobSkills { get; }

    /// <summary>
    /// Gets the job education internship skills.
    /// </summary>
    /// <value>The job education internship skills.</value>
    IQueryable<JobEducationInternshipSkill> JobEducationInternshipSkills { get; }

		/// <summary>
		/// Gets the job special requirements.
		/// </summary>
		/// <value>The job special requirements.</value>
		IQueryable<JobSpecialRequirement> JobSpecialRequirements { get; }

		/// <summary>
		/// Gets the note reminders.
		/// </summary>
		/// <value>The note reminders.</value>
		IQueryable<NoteReminder> NoteReminders { get; }

		/// <summary>
		/// Gets the messages.
		/// </summary>
		/// <value>The messages.</value>
		IQueryable<Message> Messages { get; }

		/// <summary>
		/// Gets the message texts.
		/// </summary>
		/// <value>The message texts.</value>
		IQueryable<MessageText> MessageTexts { get; }

		/// <summary>
		/// Gets the notes.
		/// </summary>
		/// <value>The notes.</value>
		IQueryable<Note> Notes { get; }

		/// <summary>
		/// Gets the persons.
		/// </summary>
		/// <value>The persons.</value>
		IQueryable<Person> Persons { get; }

		/// <summary>
		/// Gets the person notes.
		/// </summary>
		/// <value>The person notes.</value>
		IQueryable<PersonNote> PersonNotes { get; }

		/// <summary>
		/// Gets the phone numbers.
		/// </summary>
		/// <value>The phone numbers.</value>
		IQueryable<PhoneNumber> PhoneNumbers { get; }

		/// <summary>
		/// Gets the person addresses.
		/// </summary>
		/// <value>The person addresses.</value>
		IQueryable<PersonAddress> PersonAddresses { get; }

		/// <summary>
		/// Gets the person lists.
		/// </summary>
		/// <value>The person lists.</value>
		IQueryable<PersonList> PersonLists { get; }

		/// <summary>
		/// Gets the person list people.
		/// </summary>
		/// <value>The person list people.</value>
		IQueryable<PersonListPerson> PersonListPeople { get; }

        /// <summary>
        /// Gets the person mobile devices.
        /// </summary>
        /// <value>
        /// The person mobile devices.
        /// </value>
        IQueryable<PersonMobileDevice> PersonMobileDevices { get; }

		/// <summary>
		/// Gets the person posting matches.
		/// </summary>
		/// <value>The person posting matches.</value>
		IQueryable<PersonPostingMatch> PersonPostingMatches { get; }

		/// <summary>
		/// Gets the person posting matches to ignore.
		/// </summary>
		/// <value>The person posting matches to ignore.</value>
		IQueryable<PersonPostingMatchToIgnore> PersonPostingMatchesToIgnore { get; }

		/// <summary>
		/// Gets the postings.
		/// </summary>
		/// <value>The postings.</value>
		IQueryable<Posting> Postings { get; }

		/// <summary>
		/// Gets the posting surveys.
		/// </summary>
		/// <value>The posting surveys.</value>
		IQueryable<PostingSurvey> PostingSurveys { get; }

		/// <summary>
		/// Gets the open position match to ignore.
		/// </summary>
		/// <value>The open position match to ignore.</value>
		IQueryable<RecentlyPlaced> RecentlyPlaced { get; }

		/// <summary>
		/// Gets the open position match to ignore.
		/// </summary>
		/// <value>The open position match to ignore.</value>
		IQueryable<RecentlyPlacedMatch> RecentlyPlacedMatches { get; }

		/// <summary>
		/// Gets the resumes.
		/// </summary>
		/// <value>
		/// The resumes.
		/// </value>
		IQueryable<Resume> Resumes { get; }

		/// <summary>
		/// Gets the resume documents.
		/// </summary>
		/// <value>
		/// The resume documents.
		/// </value>
		IQueryable<ResumeDocument> ResumeDocuments { get; }

		/// <summary>
		/// Gets the resume jobs.
		/// </summary>
		/// <value>The resume jobs.</value>
		IQueryable<ResumeJob> ResumeJobs { get; }

    /// <summary>
		/// Gets the roles.
		/// </summary>
		/// <value>The roles.</value>
		IQueryable<Role> Roles { get; }

		/// <summary>
		/// Gets the saved messages.
		/// </summary>
		/// <value>The saved messages.</value>
		IQueryable<SavedMessage> SavedMessages { get; }

		/// <summary>
		/// Gets the saved message texts.
		/// </summary>
		/// <value>The saved message texts.</value>
		IQueryable<SavedMessageText> SavedMessageTexts { get; }

		/// <summary>
		/// Gets the saved searches.
		/// </summary>
		/// <value>The saved searches.</value>
		IQueryable<SavedSearch> SavedSearches { get; }

		/// <summary>
		/// Gets the saved search employees.
		/// </summary>
		/// <value>The saved search employees.</value>
		IQueryable<SavedSearchUser> SavedSearchUsers { get; }

		/// <summary>
		/// Gets the single sign ons.
		/// </summary>
		/// <value>The single sign ons.</value>
		IQueryable<SingleSignOn> SingleSignOns { get; }

		/// <summary>
		/// Gets the status logs.
		/// </summary>
		/// <value>The status logs.</value>
		IQueryable<StatusLog> StatusLogs { get; }

		/// <summary>
		/// Gets the users.
		/// </summary>
		/// <value>The users.</value>
		IQueryable<User> Users { get; }

		/// <summary>
		/// Gets the user security questions.
		/// </summary>
		/// <value>The user security questions.</value>
		IQueryable<UserSecurityQuestion> UserSecurityQuestions { get; }

		/// <summary>
		/// Gets the user roles.
		/// </summary>
		/// <value>The user roles.</value>
		IQueryable<UserRole> UserRoles { get; }

		/// <summary>
		/// Gets the viewed postings.
		/// </summary>
		/// <value>
		/// The viewed postings.
		/// </value>
		IQueryable<ViewedPosting> ViewedPostings { get; }

		/// <summary>
		/// Gets the invite to apply.
		/// </summary>
		/// <value>The invite to apply.</value>
		IQueryable<InviteToApply> InviteToApply { get; }

		/// <summary>
		/// Gets the registration pins.
		/// </summary>
		/// <value>
		/// The registration pins.
		/// </value>
		IQueryable<RegistrationPin> RegistrationPins { get; }

		/// <summary>
		/// Gets the office.
		/// </summary>
		/// <value> The office. </value>
		IQueryable<Office> Offices { get; }

		/// <summary>
		/// Gets the person postcode mappers.
		/// </summary>
		/// <value>
		/// The person postcode mappers.
		/// </value>
		IQueryable<PersonPostcodeMapper> PersonPostcodeMappers { get; }

		/// <summary>
		/// Gets the person office mappers.
		/// </summary>
		/// <value>
		/// The person office mappers.
		/// </value>
		IQueryable<PersonOfficeMapper> PersonOfficeMappers { get; }

		/// <summary>
		/// Gets the employer office mappers.
		/// </summary>
		/// <value>
		/// The employer office mappers.
		/// </value>
		IQueryable<EmployerOfficeMapper> EmployerOfficeMappers { get; }

		/// <summary>
		/// Gets the job office mappers.
		/// </summary>
		/// <value>
		/// The job office mappers.
		/// </value>
		IQueryable<JobOfficeMapper> JobOfficeMappers { get; }

    /// <summary>
    /// Gets the job issues.
    /// </summary>
    /// <value>
    /// The job issues.
    /// </value>
    IQueryable<JobIssues> JobIssues { get; }

    /// <summary>
    /// Gets the candidate search histories.
    /// </summary>
    /// <value>
    /// The candidate search histories.
    /// </value>
	  IQueryable<CandidateSearchHistory> CandidateSearchHistories { get; }

    /// <summary>
    /// Gets the candidate search results.
    /// </summary>
    /// <value>
    /// The candidate search results.
    /// </value>
	  IQueryable<CandidateSearchResult> CandidateSearchResults { get; }

		/// <summary>
		/// Gets the job seeker surveys.
		/// </summary>
		/// <value>
		/// The job seeker surveys.
		/// </value>
		IQueryable<JobSeekerSurvey> JobSeekerSurveys { get; }

		/// <summary>
		/// Gets the persons current offices.
		/// </summary>
		/// <value>
		/// The persons current offices.
		/// </value>
		IQueryable<PersonsCurrentOffice> PersonsCurrentOffices { get; }

    /// <summary>
    /// Gets the resume templates.
    /// </summary>
    /// <value>
    /// The resume templates.
    /// </value>
	  IQueryable<ResumeTemplate> ResumeTemplates { get; }

		/// <summary>
		/// Gets the user action type activities.
		/// </summary>
		/// <value>
		/// The user action type activities.
		/// </value>
		IQueryable<UserActionTypeActivity> UserActionTypeActivities { get; }

		/// <summary>
		/// Gets the encryptions.
		/// </summary>
		/// <value>
		/// The encryptions.
		/// </value>
		IQueryable<Encryption> Encryptions { get; }
		/// <summary>
		/// Gets the push notification.
		/// </summary>
		/// <value>
		/// The push notifications.
		/// </value>
		IQueryable<PushNotification> PushNotifications { get; }
		
		/// <summary>
		/// Gets the employer referral status reasons.
		/// </summary>
		/// <value>
		/// The employer referral status reasons.
		/// </value>
		IQueryable<EmployerReferralStatusReason> EmployerReferralStatusReasons { get; }

		/// <summary>
		/// Gets the business unit referral status reasons.
		/// </summary>
		/// <value>
		/// The business unit referral status reasons.
		/// </value>
		IQueryable<BusinessUnitReferralStatusReason> BusinessUnitReferralStatusReasons { get; }

		/// <summary>
		/// Gets the application referral status reasons.
		/// </summary>
		/// <value>
		/// The application referral status reasons.
		/// </value>
		IQueryable<ApplicationReferralStatusReason> ApplicationReferralStatusReasons { get; }

		/// <summary>
		/// Gets the employee referral status reasons.
		/// </summary>
		/// <value>
		/// The employee referral status reasons.
		/// </value>
		IQueryable<EmployeeReferralStatusReason> EmployeeReferralStatusReasons { get; }

		/// <summary>
		/// Gets the documents.
		/// </summary>
		/// <value>
		/// The documents.
		/// </value>
		IQueryable<Document> Documents { get; }

		/// <summary>
		/// Gets the referrals.
		/// </summary>
		/// <value>The referrals.</value>
		IQueryable<Referral> Referrals { get; }

        /// <summary>
        /// Gets the activity update setting.
        /// </summary>
        /// <value>The setting.</value>
        IQueryable<PersonActivityUpdateSettings> PersonActivityUpdateSetting { get; }

		#endregion

		#region View Queryable Helpers

		/// <summary>
		/// Gets the application views.
		/// </summary>
		/// <value>The application views.</value>
		IQueryable<ApplicationView> ApplicationViews { get; }

		/// <summary>
		/// Gets the application basic views.
		/// </summary>
		/// <value>The application basic views.</value>
		IQueryable<ApplicationBasicView> ApplicationBasicViews { get; }

		/// <summary>
		/// Gets the job views.
		/// </summary>
		/// <value>The job view</value>
		IQueryable<JobView> JobViews { get; }

		/// <summary>
		/// Gets the job Ids View.
		/// </summary>
		/// <value>The job Id view</value>
		IQueryable<JobIdsView> JobIdsViews { get; }

		/// <summary>
		/// Gets the employee views.
		/// </summary>
		/// <value>The employee views.</value>
		IQueryable<EmployeeSearchView> EmployeeSearchViews { get; }

		/// <summary>
		/// Gets the job seeker referral views.
		/// </summary>
		/// <value>The job seeker referral views.</value>
		IQueryable<JobSeekerReferralView> JobSeekerReferralViews { get; }

		/// <summary>
		/// Gets the employer account referral views.
		/// </summary>
		/// <value>The employer account referral views.</value>
		IQueryable<EmployerAccountReferralView> EmployerAccountReferralViews { get; }

		/// <summary>
		/// Gets the job posting referral views.
		/// </summary>
		/// <value>The job posting referral views.</value>
		IQueryable<JobPostingReferralView> JobPostingReferralViews { get; }

		/// <summary>
		/// Gets the staff search views.
		/// </summary>
		/// <value>The staff search views.</value>
		IQueryable<StaffSearchView> StaffSearchViews { get; }

		/// <summary>
		/// Gets the candidate note views.
		/// </summary>
		/// <value>The candidate note views.</value>
		IQueryable<CandidateNoteView> CandidateNoteViews { get; }

		/// <summary>
		/// Gets the person list candidate views. 
		/// </summary>
		/// <value>The person list candidate views.</value>
		IQueryable<PersonListCandidateView> PersonListCandidateViews { get; }

		/// <summary>
		/// Gets the expiring job views.
		/// </summary>
		/// <value>The expiring job views.</value>
		IQueryable<ExpiringJobView> ExpiringJobViews { get; }

		/// <summary>
		/// Gets the note reminder view.
		/// </summary>
		/// <value>The note reminder view.</value>
		IQueryable<NoteReminderView> NoteReminderViews { get; }

		/// <summary>
		/// Gets the application status log views.
		/// </summary>
		/// <value>The application status log views.</value>
		IQueryable<ApplicationStatusLogView> ApplicationStatusLogViews { get; }

		/// <summary>
		/// Gets the active search alert views.
		/// </summary>
		/// <value>The active search alert views.</value>
		IQueryable<ActiveSearchAlertView> ActiveSearchAlertViews { get; }

		/// <summary>
		/// Gets the hiring manager activity view.
		/// </summary>
		/// <value>The hiring manager activity view.</value>
		IQueryable<HiringManagerActivityView> HiringManagerActivityView { get; }

		/// <summary>
		/// Gets the assist user activity views.
		/// </summary>
		/// <value>The assist user activity views.</value>
		IQueryable<AssistUserActivityView> AssistUserActivityViews { get; }

		/// <summary>
		/// Gets the assist user employers assisted views.
		/// </summary>
		/// <value>The assist user employers assisted views.</value>
		IQueryable<AssistUserEmployersAssistedView> AssistUserEmployersAssistedViews { get; }

		/// <summary>
		/// Gets the assist user employers assisted views.
		/// </summary>
		/// <value>The assist user employers assisted views.</value>
		IQueryable<AssistUserJobSeekersAssistedView> AssistUserJobSeekersAssistedViews { get; }

		/// <summary>
		/// Gets the user alert settings.
		/// </summary>
		/// <value>The user alert settings.</value>
		IQueryable<UserAlert> UserAlerts { get; }

		/// <summary>
		/// Gets the active user alert settings, including information on Users.
		/// </summary>
		/// <value>The active user alert views.</value>
		IQueryable<ActiveUserAlertView> ActiveUserAlertViews { get; }

		/// <summary>
		/// Gets the candidate view.
		/// </summary>
		/// <value>The candidate view.</value>
		IQueryable<CandidateView> CandidateViews { get; }

		/// <summary>
		/// Gets the recently placed views.
		/// </summary>
		/// <value>The recently placed views.</value>
		IQueryable<RecentlyPlacedMatchesToIgnore> RecentlyPlacedMatchesToIgnore { get; }

		/// <summary>
		/// Gets the job seeker activity action views.
		/// </summary>
		/// <value>The job seeker activity action views.</value>
		IQueryable<JobSeekerActivityActionView> JobSeekerActivityActionViews { get; }

		/// <summary>
		/// Gets the candidate click how to apply view.
		/// </summary>
		/// <value>The candidate click how to apply view.</value>
		IQueryable<CandidateClickHowToApplyView> CandidateClickHowToApplyView { get; }

		/// <summary>
		/// Gets the candidate did not click how to apply view.
		/// </summary>
		/// <value>The candidate did not click how to apply view.</value>
		IQueryable<CandidateDidNotClickHowToApplyView> CandidateDidNotClickHowToApplyView { get; }

		/// <summary>
		/// Gets the viewed resume views.
		/// </summary>
		/// <value>The viewed resume views.</value>
		IQueryable<ViewedResumeView> ViewedResumeViews { get; }

    /// <summary>
    /// Gets the employer search views.
    /// </summary>
    /// <value>
    /// The employer search views.
    /// </value>
    IQueryable<BusinessUnitSearchView> BusinessUnitSearchViews { get; }

    /// <summary>
    /// Gets the job seeker profile view.
    /// </summary>
    /// <value>The job seeker profile view.</value>
    IQueryable<JobSeekerProfileView> JobSeekerProfileViews { get; }

		/// <summary>
		/// Gets the recently placed person match views.
		/// </summary>
		/// <value>The recently placed person match views.</value>
		IQueryable<RecentlyPlacedPersonMatchView> RecentlyPlacedPersonMatchViews { get; }

		/// <summary>
		/// Gets the employer placements views.
		/// </summary>
		/// <value>The employer placements views.</value>
		IQueryable<EmployerPlacementsView> EmployerPlacementsViews { get; }

		/// <summary>
		/// Gets the new applicant referral views.
		/// </summary>
		/// <value> The new applicant referral views. </value>
		IQueryable<NewApplicantReferralView> NewApplicantReferralViews { get; }

    /// <summary> Gets the job issues views. </summary>
    /// <value> The job issues views. </value>
	  IQueryable<JobIssuesView> JobIssuesViews { get; }

		/// <summary> Gets the referral views. </summary>
		/// <value> The referral views. </value>
		IQueryable<ReferralView> ReferralViews { get; }

    /// <summary> Gets the campuses. </summary>
    /// <value> The campuses. </value>
    IQueryable<Campus> Campuses { get; }

		/// <summary>
		/// Gets the job seeker referral all status views.
		/// </summary>
		/// <value>The job seeker referral all status views.</value>
		IQueryable<JobSeekerReferralAllStatusView> JobSeekerReferralAllStatusViews { get; }

		/// <summary>
		/// Gets the invite to apply basic views.
		/// </summary>
		/// <value>The invite to apply basic views.</value>
		IQueryable<InviteToApplyBasicView> InviteToApplyBasicViews { get; }

    /// <summary>
    /// Gets the upload files.
    /// </summary>
    /// <value>
    /// The upload files.
    /// </value>
    IQueryable<UploadFile> UploadFiles { get; }

    /// <summary>
    /// Gets the integration import records.
    /// </summary>
    /// <value>
    /// The integration import records.
    /// </value>
    IQueryable<IntegrationImportRecord> IntegrationImportRecords { get; }

		/// <summary>
		/// Gets the employer account activity views.
		/// </summary>
		/// <value> The employer account activity views. </value>
		IQueryable<EmployerAccountActivityView> EmployerAccountActivityViews { get; }


        /// <summary>
        /// The jobs sent in alerts.
        /// </summary>
        /// <value>The jobs sent in alerts for job seeker's saved searches.</value>
        IQueryable<JobsSentInAlerts> JobsSentInAlerts { get; }

	  #endregion

		#region Stored Procedure Queryable Helpers

		/// <summary>
		/// Gets the recently placed person matches for a user ordered by business unit name asc.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <param name="businessUnitName">Name of the business unit.</param>
		/// <param name="filterByOffices">if set to <c>true</c> [filter by offices].</param>
		/// <param name="officeId">The office id.</param>
		/// <param name="matchCount">The match count.</param>
		/// <param name="pageNumber">The page number.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <param name="rowCount">The row count.</param>
		/// <returns></returns>
		IQueryable<RecentlyPlacedMatchView> GetRecentlyPlacedPersonMatchesOrderByBusinessUnitNameAsc(long userId, long? businessUnitId, string businessUnitName, bool filterByOffices, long? officeId, int matchCount, 
																																																	int pageNumber, int pageSize, ref int rowCount);

		/// <summary>
		/// Gets the recently placed person matches for a user ordered by business unit name desc.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <param name="businessUnitName">Name of the business unit.</param>
		/// <param name="filterByOffices">if set to <c>true</c> [filter by offices].</param>
		/// <param name="officeId">The office id.</param>
		/// <param name="matchCount">The match count.</param>
		/// <param name="pageNumber">The page number.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <param name="rowCount">The row count.</param>
		/// <returns></returns>
		IQueryable<RecentlyPlacedMatchView> GetRecentlyPlacedPersonMatchesOrderByBusinessUnitNameDesc(long userId, long? businessUnitId, string businessUnitName, bool filterByOffices, long? officeId, int matchCount, 
																																																		int pageNumber, int pageSize, ref int rowCount);

		/// <summary>
		/// Gets the recently placed person matches for a user ordered by placed person name asc.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <param name="businessUnitName">Name of the business unit.</param>
		/// <param name="filterByOffices">if set to <c>true</c> [filter by offices].</param>
		/// <param name="officeId">The office id.</param>
		/// <param name="matchCount">The match count.</param>
		/// <param name="pageNumber">The page number.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <param name="rowCount">The row count.</param>
		/// <returns></returns>
		IQueryable<RecentlyPlacedMatchView> GetRecentlyPlacedPersonMatchesOrderByPlacedPersonNameAsc(long userId, long? businessUnitId, string businessUnitName, bool filterByOffices, long? officeId, int matchCount, 
																																																	int pageNumber, int pageSize, ref int rowCount);

		/// <summary>
		/// Gets the recently placed person matches for a user ordered by placed person name desc.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <param name="businessUnitName">Name of the business unit.</param>
		/// <param name="filterByOffices">if set to <c>true</c> [filter by offices].</param>
		/// <param name="officeId">The office id.</param>
		/// <param name="matchCount">The match count.</param>
		/// <param name="pageNumber">The page number.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <param name="rowCount">The row count.</param>
		/// <returns></returns>
		IQueryable<RecentlyPlacedMatchView> GetRecentlyPlacedPersonMatchesOrderByPlacedPersonNameDesc(long userId, long? businessUnitId, string businessUnitName, bool filterByOffices, long? officeId, int matchCount, 
																																																		int pageNumber, int pageSize, ref int rowCount);

		/// <summary>
		/// Gets the posting's person matches for a user.
		/// </summary>
		/// <param name="postingId">The posting id.</param>
		/// <param name="userId">The user id.</param>
		/// <param name="pageNumber">The page number.</param>
		/// <param name="pageSize">The page size.</param>
		/// <param name="rowCount">The row count.</param>
		/// <returns></returns>
		IQueryable<GetPostingPersonMatchesResult> GetPostingPersonMatches(long postingId, long userId, int pageNumber, int pageSize, ref int rowCount);

		/// <summary>
		/// Jobs the seeker survey totals.
		/// </summary>
		/// <param name="jobSeekerId">The job seeker id.</param>
		/// <returns></returns>
		IQueryable<JobSeekerSurveyTotalsView> JobSeekerSurveyTotals(long jobSeekerId);

		/// <summary>
		/// Gets the spidered employers with good matches order by posting count desc.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="employerName">Name of the employer.</param>
		/// <param name="minimumNumberOfPostings">The minimum number of postings.</param>
		/// <param name="minimumPostingDate">The minimum posting date.</param>
		/// <param name="pageNumber">The page number.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <param name="rowCount">The row count.</param>
		/// <returns></returns>
		IQueryable<GetSpideredEmployersWithGoodMatchesResult> GetSpideredEmployersWithGoodMatchesOrderByPostingCountDesc(long userId, string employerName, int minimumNumberOfPostings, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount);

		/// <summary>
		/// Gets the spidered employers with good matches order by employer name asc.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="employerName">Name of the employer.</param>
		/// <param name="minimumNumberOfPostings">The minimum number of postings.</param>
		/// <param name="minimumPostingDate">The minimum posting date.</param>
		/// <param name="pageNumber">The page number.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <param name="rowCount">The row count.</param>
		/// <returns></returns>
		IQueryable<GetSpideredEmployersWithGoodMatchesResult> GetSpideredEmployersWithGoodMatchesOrderByEmployerNameAsc(long userId, string employerName, int minimumNumberOfPostings, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount);

		/// <summary>
		/// Gets the spidered employers with good matches order by employer name desc.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="employerName">Name of the employer.</param>
		/// <param name="minimumNumberOfPostings">The minimum number of postings.</param>
		/// <param name="minimumPostingDate">The minimum posting date.</param>
		/// <param name="pageNumber">The page number.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <param name="rowCount">The row count.</param>
		/// <returns></returns>
		IQueryable<GetSpideredEmployersWithGoodMatchesResult> GetSpideredEmployersWithGoodMatchesOrderByEmployerNameDesc(long userId, string employerName, int minimumNumberOfPostings, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount);

		/// <summary>
		/// Gets the spidered employers with good matches order by job title asc.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="employerName">Name of the employer.</param>
		/// <param name="minimumNumberOfPostings">The minimum number of postings.</param>
		/// <param name="minimumPostingDate">The minimum posting date.</param>
		/// <param name="pageNumber">The page number.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <param name="rowCount">The row count.</param>
		/// <returns></returns>
		IQueryable<GetSpideredEmployersWithGoodMatchesResult> GetSpideredEmployersWithGoodMatchesOrderByJobTitleAsc(long userId, string employerName, int minimumNumberOfPostings, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount);

		/// <summary>
		/// Gets the spidered employers with good matches order by job title desc.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="employerName">Name of the employer.</param>
		/// <param name="minimumNumberOfPostings">The minimum number of postings.</param>
		/// <param name="minimumPostingDate">The minimum posting date.</param>
		/// <param name="pageNumber">The page number.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <param name="rowCount">The row count.</param>
		/// <returns></returns>
		IQueryable<GetSpideredEmployersWithGoodMatchesResult> GetSpideredEmployersWithGoodMatchesOrderByJobTitleDesc(long userId, string employerName, int minimumNumberOfPostings, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount);

		/// <summary>
		/// Gets the spidered employers with good matches order by posting date asc.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="employerName">Name of the employer.</param>
		/// <param name="minimumNumberOfPostings">The minimum number of postings.</param>
		/// <param name="minimumPostingDate">The minimum posting date.</param>
		/// <param name="pageNumber">The page number.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <param name="rowCount">The row count.</param>
		/// <returns></returns>
		IQueryable<GetSpideredEmployersWithGoodMatchesResult> GetSpideredEmployersWithGoodMatchesOrderByPostingDateAsc(long userId, string employerName, int minimumNumberOfPostings, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount);

		/// <summary>
		/// Gets the spidered employers with good matches order by posting date desc.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="employerName">Name of the employer.</param>
		/// <param name="minimumNumberOfPostings">The minimum number of postings.</param>
		/// <param name="minimumPostingDate">The minimum posting date.</param>
		/// <param name="pageNumber">The page number.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <param name="rowCount">The row count.</param>
		/// <returns></returns>
		IQueryable<GetSpideredEmployersWithGoodMatchesResult> GetSpideredEmployersWithGoodMatchesOrderByPostingDateDesc(long userId, string employerName, int minimumNumberOfPostings, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount);

		/// <summary>
		/// Gets the spidered postings with good matches order by posting date asc.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="employerName">Name of the employer.</param>
		/// <param name="minimumPostingDate">The minimum posting date.</param>
		/// <param name="pageNumber">The page number.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <param name="rowCount">The row count.</param>
		/// <returns></returns>
		IQueryable<GetSpideredPostingsWithGoodMatchesResult> GetSpideredPostingsWithGoodMatchesOrderByPostingDateAsc(long userId, string employerName, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount);

		/// <summary>
		/// Gets the spidered postings with good matches order by posting date desc.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="employerName">Name of the employer.</param>
		/// <param name="minimumPostingDate">The minimum posting date.</param>
		/// <param name="pageNumber">The page number.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <param name="rowCount">The row count.</param>
		/// <returns></returns>
		IQueryable<GetSpideredPostingsWithGoodMatchesResult> GetSpideredPostingsWithGoodMatchesOrderByPostingDateDesc(long userId, string employerName, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount);

		/// <summary>
		/// Gets the spidered postings with good matches order by job title asc.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="employerName">Name of the employer.</param>
		/// <param name="minimumPostingDate">The minimum posting date.</param>
		/// <param name="pageNumber">The page number.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <param name="rowCount">The row count.</param>
		/// <returns></returns>
		IQueryable<GetSpideredPostingsWithGoodMatchesResult> GetSpideredPostingsWithGoodMatchesOrderByJobTitleAsc(long userId, string employerName, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount);

		/// <summary>
		/// Gets the spidered postings with good matches order by job title desc.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="employerName">Name of the employer.</param>
		/// <param name="minimumPostingDate">The minimum posting date.</param>
		/// <param name="pageNumber">The page number.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <param name="rowCount">The row count.</param>
		/// <returns></returns>
		IQueryable<GetSpideredPostingsWithGoodMatchesResult> GetSpideredPostingsWithGoodMatchesOrderByJobTitleDesc(long userId, string employerName, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount);

	  /// <summary>
	  /// Students the activity report.
	  /// </summary>
	  /// <param name="startDate">The start date.</param>
	  /// <param name="endDate">The end date.</param>
	  /// <returns></returns>
	  IQueryable<StudentActivityReportView> StudentActivityReport(DateTime startDate, DateTime endDate);

      void UpdateSession(Guid sessionId, long userId, DateTime lastRequeston, bool checkUser);

	    #endregion

	}
}
