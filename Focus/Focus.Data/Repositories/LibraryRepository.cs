﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Data;
using System.Linq;

using Mindscape.LightSpeed;

using Focus.Data.Library.Entities;

#endregion

namespace Focus.Data.Repositories
{
	using Contracts;

	public class LibraryRepository : RepositoryBase<LibraryUnitOfWork>, ILibraryRepository
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="LibraryRepository" /> class.
		/// </summary>
		/// <param name="unitOfWorkScope">The unit of work scope.</param>
		public LibraryRepository(UnitOfWorkScopeBase<LibraryUnitOfWork> unitOfWorkScope) : base(unitOfWorkScope)
		{ }

		/// <summary>
		/// Executes the SQL.
		/// </summary>
		/// <param name="sql">The SQL.</param>
		/// <returns></returns>
		public IDataReader ExecuteSql(string sql)
		{
			using (var cmd = UnitOfWorkScope.Current.Context.DataProviderObjectFactory.CreateCommand())
			{
				cmd.CommandText = sql;
				cmd.CommandType = CommandType.Text;

				return UnitOfWorkScope.Current.PrepareCommand(cmd).ExecuteReader();
			}
		}
		
		#region Entity Queryable Helpers

		/// <summary>
		/// Gets the job tasks.
		/// </summary>
		/// <value>The job tasks.</value>
		public IQueryable<JobTask> JobTasks
		{
			get { return Query<JobTask>(); }
		}

		/// <summary>
		/// Gets the job task localisation items.
		/// </summary>
		/// <value>The job task localisation items.</value>
		public IQueryable<JobTaskLocalisationItem> JobTaskLocalisationItems
		{
			get { return Query<JobTaskLocalisationItem>(); }
		}

		/// <summary>
		/// Gets the job task multi options.
		/// </summary>
		/// <value>The job task multi options.</value>
		public IQueryable<JobTaskMultiOption> JobTaskMultiOptions
		{
			get { return Query<JobTaskMultiOption>(); }
		}

		/// <summary>
		/// Gets the localisations.
		/// </summary>
		/// <value>The localisations.</value>
		public IQueryable<Localisation> Localisations
		{
			get { return Query<Localisation>(); }
		}

		/// <summary>
		/// Gets the localisation items.
		/// </summary>
		/// <value>The localisation items.</value>
		public IQueryable<LocalisationItem> LocalisationItems
		{
			get { return Query<LocalisationItem>(); }
		}

		/// <summary>
		/// Gets the military occupations.
		/// </summary>
		/// <value>The military occupations.</value>
		public IQueryable<MilitaryOccupation> MilitaryOccupations
		{
			get { return Query<MilitaryOccupation>(); }
		}

		/// <summary>
		/// Gets the military occupation groups.
		/// </summary>
		/// <value>The military occupation groups.</value>
		public IQueryable<MilitaryOccupationGroup> MilitaryOccupationGroups
		{
			get { return Query<MilitaryOccupationGroup>(); }
		}

		/// <summary>
		/// Gets the notes.
		/// </summary>
		/// <value>The notes.</value>
		public IQueryable<NAICS> NAICS
		{
			get { return Query<NAICS>(); }
		}

		/// <summary>
		/// Gets the onet commodities.
		/// </summary>
		/// <value>The onet commodities.</value>
		public IQueryable<OnetCommodity> OnetCommodities
		{
			get { return Query<OnetCommodity>(); }
		}

		/// <summary>
		/// Gets the onets.
		/// </summary>
		/// <value>The onets.</value>
		public IQueryable<Onet> Onets
		{
			get { return Query<Onet>(); }
		}

    /// <summary>
    /// Gets the OnetsROnets.
    /// </summary>
    /// <value>The OnetsROnets.</value>
    public IQueryable<OnetROnet> OnetROnet
    {
      get { return Query<OnetROnet>(); }
    }

		/// <summary>
		/// Gets the onet localisation items.
		/// </summary>
		/// <value>The onet localisation items.</value>
		public IQueryable<OnetLocalisationItem> OnetLocalisationItems
		{
			get { return Query<OnetLocalisationItem>(); }
		}

		/// <summary>
		/// Gets the onet phrases.
		/// </summary>
		/// <value>The onet phrases.</value>
		public IQueryable<OnetPhrase> OnetPhrases
		{
			get { return Query<OnetPhrase>(); }
		}

		/// <summary>
		/// Gets the onet rings.
		/// </summary>
		/// <value>The onet rings.</value>
		public IQueryable<OnetRing> OnetRings
		{
			get { return Query<OnetRing>(); }
		}

		/// <summary>
		/// Gets the onet tasks.
		/// </summary>
		/// <value>The onet tasks.</value>
		public IQueryable<OnetTask> OnetTasks
		{
			get { return Query<OnetTask>(); }
		}

		/// <summary>
		/// Gets the onet words.
		/// </summary>
		/// <value>The onet words.</value>
		public IQueryable<OnetWord> OnetWords
		{
			get { return Query<OnetWord>(); }
		}

		/// <summary>
		/// Gets the resume skills.
		/// </summary>
		/// <value>The resume skills.</value>
		public IQueryable<ResumeSkill> ResumeSkills
		{
			get { return Query<ResumeSkill>(); }
		}

    /// <summary>
    /// Gets the ronets.
    /// </summary>
    /// <value>The ronets.</value>
    public IQueryable<ROnet> ROnets
    {
      get { return Query<ROnet>(); }
    }

		/// <summary>
		/// Gets the ronet onets.
		/// </summary>
		/// <value>The ronet onets.</value>
		public IQueryable<ROnetOnet> ROnetOnets
		{
			get { return Query<ROnetOnet>(); }
		}

		/// <summary>
		/// Gets the skills.
		/// </summary>
		/// <value>The skills.</value>
		public IQueryable<Skill> Skills
		{
			get { return Query<Skill>(); }
		}

		/// <summary>
		/// Gets the standard industrial classifications.
		/// </summary>
		/// <value>The standard industrial classifications.</value>
		public IQueryable<StandardIndustrialClassification> StandardIndustrialClassifications
		{
			get { return Query<StandardIndustrialClassification>(); }
		}

    /// <summary>
    /// Gets the education intership statements.
    /// </summary>
    /// <value>The education intership statements.</value>
    public IQueryable<EducationInternshipStatement> EducationInternshipStatements
    {
      get { return Query<EducationInternshipStatement>(); }
    }

    /// <summary>
    /// Gets the education intership categories.
    /// </summary>
    /// <value>The education intership categories.</value>
    public IQueryable<EducationInternshipCategory> EducationInternshipCategories
    {
      get { return Query<EducationInternshipCategory>(); }
    }

    /// <summary>
    /// Gets the education intership statements.
    /// </summary>
    /// <value>The education intership statements.</value>
    public IQueryable<EducationInternshipSkill> EducationInternshipSkills
    {
      get { return Query<EducationInternshipSkill>(); }
    }

    /// <summary>
    /// Gets the onetSOCs.
    /// </summary>
    /// <value>The onetSOCs.</value>
    public IQueryable<OnetSOC> OnetSOCs
    {
      get { return Query<OnetSOC>(); }
    }
    /// <summary>
    /// Gets the SOCs.
    /// </summary>
    /// <value>The SOCs.</value>
    public IQueryable<SOC> SOCs
    {
      get { return Query<SOC>(); }
    }

		/// <summary>
		/// Gets the jobs.
		/// </summary>
		/// <value>The jobs.</value>
		public IQueryable<Job> Jobs
		{
			get { return Query<Job>(); }
		}

		/// <summary>
		/// Gets the career areas.
		/// </summary>
		/// <value> The career areas. </value>
		public IQueryable<CareerArea> CareerAreas
		{
			get { return Query<CareerArea>(); }
		}

		#endregion

		#region View Queryable Helpers

		/// <summary>
		/// Gets the military occupation job title views.
		/// </summary>
		/// <value>The military occupation job title views.</value>
		public IQueryable<MilitaryOccupationJobTitleView> MilitaryOccupationJobTitleViews
		{
			get { return Query<MilitaryOccupationJobTitleView>(); }
		}

		/// <summary>
		/// Gets the onet word views.
		/// </summary>
		/// <value>The onet word views.</value>
		public IQueryable<OnetWordView> OnetWordViews
		{
			get { return Query<OnetWordView>(); }
		}

		/// <summary>
		/// Gets the onet word job task views.
		/// </summary>
		/// <value>The onet word job task views.</value>
		public IQueryable<OnetWordJobTaskView> OnetWordJobTaskViews
		{
			get { return Query<OnetWordJobTaskView>(); }
		}

    /// <summary>
    /// Gets the onet word job task views.
    /// </summary>
    /// <value>The onet word job task views.</value>
    public IQueryable<JobDegreeEducationLevelView> JobDegreeEducationLevelViews
    {
      get { return Query<JobDegreeEducationLevelView>(); }
    }

		public IQueryable<Onet17Onet12MappingView> Onet17Onet12MappingViews
		{
			get { return Query<Onet17Onet12MappingView>(); }
		}

    /// <summary>
    /// Gets the onet to r onet conversion views.
    /// </summary>
    /// <value>
    /// The onet to r onet conversion views.
    /// </value>
	  public IQueryable<OnetToROnetConversionView> OnetToROnetConversionViews
	  {
      get { return Query<OnetToROnetConversionView>(); }
	  }

	  #endregion
	}
}

