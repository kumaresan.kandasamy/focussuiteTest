﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

using Focus.Core;
using Focus.Data.Core.Entities;

using Framework.Core;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Linq;
using Mindscape.LightSpeed.Querying;

#endregion

namespace Focus.Data.Repositories
{
	using Contracts;

	public abstract class RepositoryBase<TUnitOfWork> : IRepository where TUnitOfWork : class, IUnitOfWork, new()
	{
		private readonly UnitOfWorkScopeBase<TUnitOfWork> _unitOfWorkScopeBase;

    public event EventHandler<RepositoryEntityStateChangedEventArgs> EntityStateChanged;

		/// <summary>
		/// Initializes a new instance of the <see cref="RepositoryBase{TUnitOfWork}" /> class.
		/// </summary>
		/// <param name="unitOfWorkScope">The unit of work scope.</param>
		protected RepositoryBase(UnitOfWorkScopeBase<TUnitOfWork> unitOfWorkScope)
		{
			_unitOfWorkScopeBase = unitOfWorkScope;
		}

		/// <summary>
		/// Gets the unit of work scope.
		/// </summary>
		protected UnitOfWorkScopeBase<TUnitOfWork> UnitOfWorkScope
		{
			get { return _unitOfWorkScopeBase; }
		}

		/// <summary>
		/// Adds the specified entity to the repository.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="entity">The entity.</param>
		public void Add<T>(T entity) where T : Entity
		{
			UnitOfWorkScope.Current.Add(entity);
		}

		/// <summary>
		/// Removes the specified entity from the repository.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="entity">The entity.</param>
		public void Remove<T>(T entity) where T : Entity
		{
			UnitOfWorkScope.Current.Remove(entity);
		}

		/// <summary>
		/// Removes the entity / entities from the repository from the specified where expression.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="selector">The selector.</param>
		public void Remove<T>(Expression<Func<T, bool>> selector) where T : Entity
		{
			foreach (var entity in UnitOfWorkScope.Current.Query<T>().Where(selector))
				UnitOfWorkScope.Current.Remove(entity);
		}

		/// <summary>
		/// Removes the entity / entities from the repository using the specified query.
		/// </summary>
		/// <param name="query">The query.</param>
		public void Remove(Query query)
		{
			UnitOfWorkScope.Current.Remove(query);
		}

		/// <summary>
		/// Updates the entity / entities in the repository using the specified query.
		/// </summary>
		/// <param name="query">The query.</param>
		/// <param name="attributes">The attributes.</param>
		public void Update(Query query, object attributes)
		{
			UnitOfWorkScope.Current.Update(query, attributes);
		}

		/// <summary>
		/// Counts all the entities in the repository.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public long Count<T>() where T : Entity
		{
			return UnitOfWorkScope.Current.Count<T>();
		}

		/// <summary>
		/// Counts the entities in the repository for the specified where expression.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="selector">The selector.</param>
		/// <returns></returns>
		public long Count<T>(Expression<Func<T, bool>> selector) where T : Entity
		{
			return UnitOfWorkScope.Current.Query<T>().Count(selector);
		}

		/// <summary>
		/// Checks if the entities exists in the repository for the specified where expression.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="selector">The selector.</param>
		/// <returns></returns>
		public bool Exists<T>(Expression<Func<T, bool>> selector) where T : Entity
		{
			return Count(selector) > 0;
		}

		/// <summary>
		/// Finds the entity by the id.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		public T FindById<T>(object id) where T : Entity
		{
			return UnitOfWorkScope.Current.FindById<T>(id);
		}

		/// <summary>
		/// Finds a list of all entities in the repository.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public IList<T> Find<T>() where T : Entity
		{
			return UnitOfWorkScope.Current.Find<T>();
		}

		/// <summary>
		/// Finds a list of entities in the repository using the given the specified where expression.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="selector">The selector.</param>
		/// <returns></returns>
		public IList<T> Find<T>(Expression<Func<T, bool>> selector) where T : Entity
		{
			return UnitOfWorkScope.Current.Query<T>().Where(selector).ToList();
		}

		/// <summary>
		/// Queries this repository using a Linq query.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public IQueryable<T> Query<T>() where T : Entity
		{
			return UnitOfWorkScope.Current.Query<T>();
		}

		/// <summary>
		/// Finds and pages a list of entities in the repository IQueriable.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="query">The query.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <returns></returns>
		public PagedList<T> FindPaged<T>(IQueryable<T> query, int pageIndex, int pageSize) where T : Entity
		{
			return new PagedList<T>(query, pageIndex, pageSize);
		}

		/// <summary>
		/// Saves the changes to the repository.
		/// </summary>
		/// <param name="reset">if set to <c>true</c> then the repository is reset.</param>
		public void SaveChanges(bool reset = false)
		{
      List<RepositoryEntityStateChangedEventArgs> entityArgs = null;

      foreach (var entity in UnitOfWorkScope.Current)
      {
        if (entity.EntityState != EntityState.Default && entity.IsValid)
        {
          var reportableEntity = entity as IReportable;
          if (reportableEntity.IsNotNull() && reportableEntity.ReportEntityType != ReportEntityType.None)
          {
            if (entityArgs.IsNull())
              entityArgs = new List<RepositoryEntityStateChangedEventArgs>();

            entityArgs.Add(new RepositoryEntityStateChangedEventArgs(reportableEntity, entity.EntityState == EntityState.Deleted));
          }
        }
      } 
      UnitOfWorkScope.Current.SaveChanges(reset);

		  if (entityArgs.IsNotNullOrEmpty())
		  {
		    foreach (var entityArg in entityArgs)
		    {
          OnEntityStateChanged(entityArg);
		    }
		  }
		}

    public void OnEntityStateChanged(RepositoryEntityStateChangedEventArgs e)
    {
      if (EntityStateChanged != null)
        EntityStateChanged(this, e);
    }

		/// <summary>
		/// Executes the SQL.
		/// </summary>
		/// <param name="sql">The SQL.</param>
		/// <returns></returns>
		public int ExecuteNonQuery(string sql)
		{
			using (var cmd = UnitOfWorkScope.Current.Context.DataProviderObjectFactory.CreateCommand())
			{
				cmd.CommandText = sql;
				cmd.CommandType = CommandType.Text;

				return UnitOfWorkScope.Current.PrepareCommand(cmd).ExecuteNonQuery();
			}
		}

    /// <summary>
    /// Executes the query and returns the int result
    /// </summary>
    /// <param name="sql">The SQL.</param>
    /// <returns></returns>
    public T ExecuteScalar<T>(string sql)
    {
      using (var cmd = UnitOfWorkScope.Current.Context.DataProviderObjectFactory.CreateCommand())
      {
        cmd.CommandText = sql;
        cmd.CommandType = CommandType.Text;

        return (T)UnitOfWorkScope.Current.PrepareCommand(cmd).ExecuteScalar();
      }
    }

    /// <summary>
    /// Executes the SQL to return a record set
    /// </summary>
    /// <param name="sql">The SQL.</param>
    /// <returns>A data reader</returns>
    public IDataReader ExecuteDataReader(string sql)
    {
      using (var cmd = UnitOfWorkScope.Current.Context.DataProviderObjectFactory.CreateCommand())
      {
        cmd.CommandText = sql;
        cmd.CommandType = CommandType.Text;

        return UnitOfWorkScope.Current.PrepareCommand(cmd).ExecuteReader();
      }
    }

		/// <summary>
		/// Sets the field value
		/// </summary>
		/// <param name="entity">Target entity</param>
		/// <param name="id">Id of row to update</param>
		/// <param name="fieldName">Name of field to update</param>
		/// <param name="createdOn">New value for CreatedOn field</param>
		private void SetFieldValue( Entity entity, long id, string fieldName, DateTime createdOn )
		{
			var changes = new Dictionary<string, object>
		  {
			  { fieldName, createdOn }
		  };
			var updateQuery = new Query( entity.GetType(), Entity.Attribute( "Id" ) == id );
			Update( updateQuery, changes );
		}

		/// <summary>
		/// Sets the CreatedOn field
		/// </summary>
		/// <param name="entity">Target entity</param>
		/// <param name="id">Id of row to update</param>
		/// <param name="createdOn">New value for CreatedOn field</param>
		public void SetCreatedOn( Entity entity, long id, DateTime createdOn )
		{
			SetFieldValue( entity, id, "CreatedOn", createdOn );
		}

		/// <summary>
		/// Sets the UpdatedOn field
		/// </summary>
		/// <param name="entity">Target entity</param>
		/// <param name="id">Id of row to update</param>
		/// <param name="updatedOn">New value for UpdatedOn field</param>
		public void SetUpdatedOn( Entity entity, long id, DateTime updatedOn )
		{
			SetFieldValue( entity, id, "UpdatedOn", updatedOn );
		}

		/// <summary>
		/// Sets the DeletedOn field
		/// </summary>
		/// <param name="entity">Target entity</param>
		/// <param name="id">Id of row to update</param>
		/// <param name="deletedOn">New value for DeletedOn field</param>
		public void SetDeletedOn( Entity entity, long id, DateTime deletedOn )
		{
			SetFieldValue( entity, id, "DeletedOn", deletedOn );
		}

		/// <summary>
		/// Sets the ActionedOn field
		/// </summary>
		/// <param name="entity">Target entity</param>
		/// <param name="id">Id of row to update</param>
		/// <param name="actionedOn">New value for CreatedOn field</param>
		public void SetActionedOn( Entity entity, long id, DateTime actionedOn )
		{
			SetFieldValue( entity, id, "ActionedOn", actionedOn );
		}
	}

  public class RepositoryEntityStateChangedEventArgs : EventArgs
  {
    public long EntityId { get; set; }
    public long? AdditionalEntityId { get; set; }
    public ReportEntityType EntityType { get; set; }
    public bool EntityDeletion { get; set; }

    public RepositoryEntityStateChangedEventArgs()
    {

    }

    public RepositoryEntityStateChangedEventArgs(IReportable entity)
    {
      EntityId = entity.ReportEntityId;
      AdditionalEntityId = entity.ReportAdditionalEntityId;
      EntityType = entity.ReportEntityType;
      EntityDeletion = false;
    }

    public RepositoryEntityStateChangedEventArgs(IReportable entity, bool isDeletion)
    {
      EntityId = entity.ReportEntityId;
      AdditionalEntityId = entity.ReportAdditionalEntityId;
      EntityType = entity.ReportEntityType;
      EntityDeletion = isDeletion && !entity.IsChild;
    }
  }
}