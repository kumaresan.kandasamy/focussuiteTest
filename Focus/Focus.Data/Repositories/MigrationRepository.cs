﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;

using Mindscape.LightSpeed;

using Focus.Data.Migration.Entities;

#endregion

namespace Focus.Data.Repositories
{
	using Contracts;

	public class MigrationRepository : RepositoryBase<DataMigrationUnitOfWork>, IMigrationRepository
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="MigrationRepository" /> class.
		/// </summary>
		/// <param name="unitOfWorkScope">The unit of work scope.</param>
		public MigrationRepository(UnitOfWorkScopeBase<DataMigrationUnitOfWork> unitOfWorkScope) : base(unitOfWorkScope)
		{ }

		#region Entity Queryable Helpers

		/// <summary>
		/// Gets the Employer Requests.
		/// </summary>
		/// <value>The Employer Request.</value>
		public IQueryable<EmployerRequest> EmployerRequests
		{
			get { return Query<EmployerRequest>(); }
		}

		/// <summary>
		/// Gets the JobOrder Requests.
		/// </summary>
		/// <value>The JobOrder Request.</value>
		public IQueryable<JobOrderRequest> JobOrderRequests
		{
			get { return Query<JobOrderRequest>(); }
		}

		/// <summary>
		/// Gets the JobOrder Requests.
		/// </summary>
		/// <value>The JobOrder Request.</value>
		public IQueryable<JobSeekerRequest> JobSeekerRequests
		{
			get { return Query<JobSeekerRequest>(); }
		}

		/// <summary>
		/// Gets the User Requests.
		/// </summary>
		/// <value>The Assist User Request.</value>
		public IQueryable<UserRequest> UserRequests
		{
			get { return Query<UserRequest>(); }
		}

    /// <summary>
    /// Gets the batch records
    /// </summary>
    /// <value>The batch records.</value>
    public IQueryable<BatchRecord> BatchRecords
    {
      get { return Query<BatchRecord>(); }
    }

		#endregion
	}
}
