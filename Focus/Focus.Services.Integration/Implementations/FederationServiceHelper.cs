﻿using System;
using System.IdentityModel.Tokens;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Microsoft.IdentityModel.Protocols.WSTrust;
using Microsoft.IdentityModel.Protocols.WSTrust.Bindings;

namespace Focus.Services.Integration.Implementations
{
	public class FederatedServiceHelper<T> : IDisposable
	{
		private EndpointAddress RelyingPartyEndpoint { get; set; }

		private WSTrustChannelFactory TokenChannelFactory { get; set; }
		private ChannelFactory<T> ServiceChannelFactory { get; set; }

		//private T _channel;

		public FederatedServiceHelper
			(
				string sercviceEndpoint,
				WSFederationHttpSecurityMode securityMode,
				string relyingParty = null,
				string tokenServiceEndpoint = null,
				string username = null,
				string password = null
			)
		{
			Binding serviceBinding = null;

			switch (securityMode)
			{
				case WSFederationHttpSecurityMode.TransportWithMessageCredential:
					var federationServiceBinding = new WS2007FederationHttpBinding(securityMode);
					federationServiceBinding.Security.Message.IssuedKeyType = SecurityKeyType.BearerKey;
					federationServiceBinding.Security.Message.EstablishSecurityContext = false;
          federationServiceBinding.MaxReceivedMessageSize = 5242880;
					if (relyingParty != null) federationServiceBinding.Security.Message.IssuerAddress = new EndpointAddress(relyingParty);
					serviceBinding = federationServiceBinding;


					if (relyingParty != null) RelyingPartyEndpoint = new EndpointAddress(relyingParty);

					var tokenBinding = new UserNameWSTrustBinding(SecurityMode.TransportWithMessageCredential);
					if (tokenServiceEndpoint != null)
					{
						var tokenEndpoint = new EndpointAddress(tokenServiceEndpoint);

						TokenChannelFactory = new WSTrustChannelFactory(tokenBinding, tokenEndpoint);
						TokenChannelFactory.Credentials.UserName.UserName = username;
						TokenChannelFactory.Credentials.UserName.Password = password;
					}
					break;
			}

			if (serviceBinding == null || String.IsNullOrWhiteSpace(sercviceEndpoint)) return;
			ServiceChannelFactory = new ChannelFactory<T>(serviceBinding, new EndpointAddress(sercviceEndpoint));
			ServiceChannelFactory.ConfigureChannelFactory();
		}

		private SecurityToken GetToken()
		{
			var securityTokenRequest = new RequestSecurityToken
			{
				RequestType = WSTrust13Constants.RequestTypes.Issue,
				AppliesTo = RelyingPartyEndpoint,
				KeyType = WSTrust13Constants.KeyTypes.Bearer
			};

			var channel = TokenChannelFactory.CreateChannel();

			return channel.Issue(securityTokenRequest);
		}

		public T Channel
		{
			get
			{
				return ServiceChannelFactory.CreateChannelWithIssuedToken(GetToken());
			}
		}

		public void Dispose()
		{
			if (TokenChannelFactory != null)
			{
				try
				{
					TokenChannelFactory.Close();
				}
				catch
				{
					if (TokenChannelFactory != null)
					{
						TokenChannelFactory.Abort();
					}
				}
			}

			if (ServiceChannelFactory != null)
			{
				try
				{
					ServiceChannelFactory.Close();
				}
				catch
				{
					if (ServiceChannelFactory != null)
					{
						ServiceChannelFactory.Abort();
					}
				}
			}
		}
	}
}
