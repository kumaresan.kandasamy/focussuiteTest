﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Text.RegularExpressions;

using Focus.Core;
using Focus.Core.Models.Integration;

using Framework.Core;

#endregion

namespace Focus.Services.Integration.Implementations.Aptimus
{
  public static class IntegrationExtensions
  {

    public static string ToHiringManagerAddress(this AddressModel address)
    {
      if (address.IsNull()) return string.Empty;

      var addressString = address.AddressLine1;
      if (address.AddressLine2.IsNotNullOrEmpty()) addressString += ", " + address.AddressLine2;
      if (address.AddressLine3.IsNotNullOrEmpty()) addressString += ", " + address.AddressLine3;
      if (address.City.IsNotNullOrEmpty()) addressString += ", " + address.City;
      if (address.State.IsNotNullOrEmpty()) addressString += ", " + address.State;
      if (address.PostCode.IsNotNullOrEmpty()) addressString += ", " + address.PostCode;
      if (address.Country.IsNotNullOrEmpty()) addressString += ". " + address.Country;

      return addressString;
    }

    public static string ToAptimusJobStatus(this JobStatuses status, bool liveInstance)
    {
      if (!liveInstance) return "EXPIRED";

      return status == JobStatuses.Active ? "ACTIVE" : "EXPIRED";
    }

    public static bool IsValidZipCode(this string zip)
    {
      var regX = new Regex(@"^\d{5}(?:[-\s]\d{4})?$");
      return regX.IsMatch(zip);
    }
  }
}
