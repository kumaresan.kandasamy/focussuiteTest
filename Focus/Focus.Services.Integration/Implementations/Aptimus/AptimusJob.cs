﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

#endregion

namespace Focus.Services.Integration.Implementations.Aptimus
{
  [DataContract(Name = "", Namespace = "")]
  public class AptimusJob
  {
    [IgnoreDataMember]
    public const string DateFormat = "yyyy-MM-ddTHH:mm:ss.ffff";

    [DataMember(Name = "providerName", Order = 1)]
    public string ProviderName { get { return "BURNING_GLASS"; } }

    /// <summary>
    /// Gets or sets the internal job ID.
    /// </summary>
    [DataMember(Name = "providerSourceId", Order = 2, EmitDefaultValue = false)]
    public string InternalId { get; set; }

    [DataMember(Name = "title", Order = 3)]
    public string Title { get; set; }

    [DataMember(Name = "providerCompanyName", Order = 23)]
    public string BusinessUnit { get; set; }

    [DataMember(Name = "description", Order = 4)]
    public string Description { get; set; }

    [DataMember(Name = "status", Order = 5)]
    public string Status { get; set; }

    [DataMember(Name = "publicIndustry", Order = 6, EmitDefaultValue = false)]
    public bool? PublicIndustry { get { return false; } } // Based on NAICS but NAICs not used in EDU

    [DataMember(Name = "jobCount", Order = 7, EmitDefaultValue = true)]
    public int Openings { get; set; }

    [DataMember(Name = "applyType", Order = 8, EmitDefaultValue = false)]
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
    public string HowToApply { get; set; }

    [DataMember(Name = "salary", Order = 9, EmitDefaultValue = false)]
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
    public Salary Salary { get; set; }

    [DataMember(Name = "contact", Order = 10, EmitDefaultValue = false)]
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
    public Contact Contact { get; set; }

    [DataMember(Name = "location", Order = 11, EmitDefaultValue = false)]
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
    public Location Location { get; set; }

    [IgnoreDataMember]
    public DateTime? PostingDate { get; set; }

    [DataMember(Name = "postingDate", Order = 12, EmitDefaultValue = false)]
    public string PostingDateAlias { get { return PostingDate != null ? PostingDate.Value.ToString(DateFormat) : null; } }

    [DataMember(Name = "postingType", Order = 13)]
    public string PostingType { get { return "Company-Recruiter"; } }

    [IgnoreDataMember]
    public DateTime OpeningDate { get; set; }

    [DataMember(Name = "dateOpening", Order = 14)]
    public string OpeningDateAlias { get { return OpeningDate.ToString(DateFormat); } }

    [IgnoreDataMember]
    public DateTime ClosingDate { get; set; }

    [DataMember(Name = "dateClosing", Order = 15)]
    public string ClosingDateAlias { get { return ClosingDate.ToString(DateFormat); } }

    [IgnoreDataMember]
    public DateTime UpdateDate { get; set; }

    [DataMember(Name = "dateUpdate", Order = 16)]
    public string UpdateDateAlias { get { return UpdateDate.ToString(DateFormat); } }

    [IgnoreDataMember]
    public int Age { get { return 0; } }

    [DataMember(Name = "jobCode", Order = 18, EmitDefaultValue = false)]
    public string BGTOcc { get; set; } // Could contain Onet if ROnet not found

    [DataMember(Name = "jobCodeType", Order = 19, EmitDefaultValue = false)]
    public string CodeType { get; set; }

    [DataMember(Name = "experience", Order = 20, EmitDefaultValue = false)]
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
    public Experience Experience { get; set; }

    [DataMember(Name = "jobAttributes", Order = 20)]
    public List<Attributes> Attributes { get; set; }

    [DataMember(Name = "skills", Order = 21, EmitDefaultValue = false)]
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
    public List<Skills> Skills { get; set; }

    [DataMember(Name = "qualifications", Order = 22, EmitDefaultValue = false)]
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
    public List<Certifications> Qualifications { get; set; }
  }

  [DataContract(Name = "salary", Namespace = "")]
  public class Salary
  {
    [DataMember(Name = "minimum", Order = 1)]
    public decimal Minimum { get; set; }

    [DataMember(Name = "maximum", Order = 2)]
    public decimal Maximum { get; set; }

    [DataMember(Name = "currency", Order = 3)]
    public string Currency { get { return "USD"; } }

    [DataMember(Name = "frequency", Order = 4)]
    public string Frequency { get; set; }
  }

  [DataContract(Name = "contact", Namespace = "")]
  public class Contact
  {
    [DataMember(Name = "firstName", Order = 1)]
    public string FirstName { get; set; }

    [DataMember(Name = "lastName", Order = 2)]
    public string Surname { get; set; }

    [DataMember(Name = "email", Order = 3)]
    public string Email { get; set; }

    [DataMember(Name = "address", Order = 4)]
    public string Address { get; set; }

    [DataMember(Name = "phone", Order = 5)]
    public string Phone { get; set; }

    [DataMember(Name = "phoneExt", Order = 6, EmitDefaultValue = false)]
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
    public string Extension { get; set; }
  }

  [DataContract(Name = "contact", Namespace = "")]
  public class Location
  {
    [DataMember(Name = "address1", Order = 1)]
    public string Address1 { get; set; }

    [DataMember(Name = "address2", Order = 2)]
    public string Address2 { get; set; }

    [DataMember(Name = "city", Order = 3)]
    public string City { get; set; }

    [DataMember(Name = "state", Order = 4)]
    public string State { get; set; }

    [DataMember(Name = "country", Order = 5)]
    public string Country { get; set; }

    [DataMember(Name = "postal", Order = 6)]
    public string PostCode { get; set; }

    [DataMember(Name = "latitude", Order = 7, EmitDefaultValue = false)]
    public double? Latitude { get; set; }

    [DataMember(Name = "longitude", Order = 8, EmitDefaultValue = false)]
    public double? Longitude { get; set; }

  }

  [DataContract(Name = "experience", Namespace = "")]
  public class Experience
  {
    [DataMember(Name = "max", Order = 1)]
    public int Max { get; set; }

    [DataMember(Name = "maxInterval", Order = 2)]
    public string Interval { get; set; }
  }

  [DataContract(Name = "jobAttribute", Namespace = "")]
  public class Attributes
  {
    [DataMember(Name = "attributeName", Order = 1)]
    public string AttributeName { get; set; }

    [DataMember(Name = "attributeValue", Order = 2)]
    public string AttributeValue { get; set; }
  }

  [DataContract(Name = "", Namespace = "")]
  public class Skills
  {
    [DataMember(Name = "skillName", Order = 1)]
    public string Name { get; set; }

    [DataMember(Name = "assignmentType", Order = 2)]
    public string Type { get { return "canonSkillsFromBg"; } }
  }

  [DataContract(Name = "", Namespace = "")]
  public class Certifications
  {
    [DataMember(Name = "type", Order = 1)]
    public string Type { get { return "Certifications"; } }

    [DataMember(Name = "name", Order = 2)]
    public string Name { get; set; }

    [DataMember(Name = "requiredType", Order = 3)]
    public string Required { get { return "optional"; } }
  }



}
