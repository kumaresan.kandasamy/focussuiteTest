﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Services.Integration.Implementations.Aptimus
{
  [Serializable]
  public class ClientSettings
  {
    [DataMember]
    public bool LiveInstance { get; set; }

    [DataMember]
    public string BaseUrl { get; set; }

    [DataMember]
    public string JobUrlSuffix { get; set; }

    [DataMember]
    public string Username { get; set; }

    [DataMember]
    public string Password { get; set; }
  }
}
