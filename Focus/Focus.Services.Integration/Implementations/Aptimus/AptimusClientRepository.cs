﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

using Newtonsoft.Json;

using Framework.Core;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.IntegrationMessages;
using Focus.Core.Models.Integration;

#endregion

namespace Focus.Services.Integration.Implementations.Aptimus
{
  public class AptimusClientRepository : ClientRepository, IIntegrationRepository
  {
    private readonly ClientSettings _clientSettings;

    public AptimusClientRepository(string settings, List<ExternalLookUpItemDto> externalLookUpItems)
    {
      _clientSettings = (ClientSettings)JsonConvert.DeserializeObject(settings, typeof(ClientSettings));
    }

		protected override IntegrationPoint[] GetImplementedIntegrationPoints()
		{
			return new[] { IntegrationPoint.SaveJob };
		}
		
    #region Implementation of IClientRepository

		public LogActionResponse LogAction(LogActionRequest request)
		{
			return new LogActionResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

    public GetJobSeekerResponse GetJobSeeker(GetJobSeekerRequest request)
    {
      return new GetJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public GetJobSeekerResponse GetJobSeekerBySocialSecurityNumber(GetJobSeekerRequest request)
    {
      return new GetJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AuthenticateJobSeekerResponse AuthenticateJobSeeker(AuthenticateJobSeekerRequest request)
    {
      return new AuthenticateJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveJobSeekerResponse SaveJobSeeker(SaveJobSeekerRequest request)
    {
      return new SaveJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public JobSeekerLoginResponse JobSeekerLogin(JobSeekerLoginRequest request)
    {
      return new JobSeekerLoginResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveJobMatchesResponse SaveJobMatches(SaveJobMatchesRequest request)
    {
      return new SaveJobMatchesResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AssignJobSeekerActivityResponse AssignJobSeekerActivity(AssignJobSeekerActivityRequest request)
    {
      return new AssignJobSeekerActivityResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AssignAdminToJobSeekerResponse AssignAdminToJobSeeker(AssignAdminToJobSeekerRequest request)
    {
      return new AssignAdminToJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public IsUIClaimantResponse IsUIClaimant(IsUIClaimantRequest request)
    {
      return new IsUIClaimantResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public ChangeUserPasswordResponse ChangeUserPassword(ChangeUserPasswordRequest request)
    {
      return new ChangeUserPasswordResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveJobResponse SaveJob(SaveJobRequest request)
    {
      var requestResponse = new RequestResponse();
      var integrationResponse = new SaveJobResponse(request);
      try
      {
        var job = JsonConvert.SerializeObject(BuildAptimusJob(request.Job));

        requestResponse.Url = BuildApiPath(_clientSettings.JobUrlSuffix);
        requestResponse.OutgoingRequest = job;
        requestResponse.StartTime = DateTime.Now;

        requestResponse = SendWebRequest(job, requestResponse.Url, request.Job.ExternalId.IsNotNullOrEmpty() ? "PUT" : "POST");

        integrationResponse.ExternalJobId = requestResponse.IncomingResponse;
      }
      catch (Exception e)
      {
        integrationResponse.Outcome = IntegrationOutcome.Failure;
        integrationResponse.Exception = e;
        requestResponse.EndTime = DateTime.Now;
      }
      finally
      {
        integrationResponse.IntegrationBreadcrumbs = new List<RequestResponse> { requestResponse };
      }
      return integrationResponse;
    }

    public GetJobResponse GetJob(GetJobRequest request)
    {
      return new GetJobResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public UpdateJobStatusResponse UpdateJobStatus(UpdateJobStatusRequest request)
    {
      return new UpdateJobStatusResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public ReferJobSeekerResponse ReferJobSeeker(ReferJobSeekerRequest request)
    {
      return new ReferJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SetApplicationStatusResponse SetApplicationStatus(SetApplicationStatusRequest request)
    {
      return new SetApplicationStatusResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public GetEmployerResponse GetEmployer(GetEmployerRequest request)
    {
      return new GetEmployerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveEmployerResponse SaveEmployer(SaveEmployerRequest request)
    {
      return new SaveEmployerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AssignAdminToEmployerResponse AssignAdminToEmployer(AssignAdminToEmployerRequest request)
    {
      return new AssignAdminToEmployerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AssignEmployerActivityResponse AssignEmployerActivity(AssignEmployerActivityRequest request)
    {
      return new AssignEmployerActivityResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveEmployeeResponse SaveEmployee(SaveEmployeeRequest request)
    {
      return new SaveEmployeeResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AuthenticateStaffResponse AuthenticateStaffUser(AuthenticateStaffRequest request)
    {
      return new AuthenticateStaffResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }
    
    public UpdateEmployeeStatusResponse UpdateEmployeeStatus(UpdateEmployeeStatusRequest request)
    {
      return new UpdateEmployeeStatusResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public DisableJobSeekersReportResponse DisableJobSeekersReport(DisableJobSeekersReportRequest request)
    {
      return new DisableJobSeekersReportResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    #endregion

		#region Helper Methods

		/// <summary>
    /// Builds the aptimus job.
    /// </summary>
    /// <remarks>public so function can be accessed by unit tests</remarks>
    /// <param name="job">The job.</param>
    private AptimusJob BuildAptimusJob(JobModel job)
    {
      var aptimusJob = new AptimusJob
      {
        InternalId = job.LensId,
        Title = job.Title,
        Description = job.Description.IsNotNullOrEmpty() ? job.Description : " ", BusinessUnit = job.BusinessUnitName,
        Status = job.JobStatus.ToAptimusJobStatus(_clientSettings.LiveInstance),
        Openings = job.Openings,
        PostingDate = job.PostingDate,
        ClosingDate = job.ClosingDate.Value,
        OpeningDate = job.CreatedDate,
        UpdateDate = job.UpdateDate,
        BGTOcc = job.BGTOcc ?? job.OnetCode, CodeType = job.BGTOcc.IsNotNull() ? "RONET" : "ONET",
        HowToApply = CollectInterviewPrefs(job.InterviewContactPreferences),
        Contact = new Contact
        {
          FirstName = job.HiringManager.FirstName,
          Surname = job.HiringManager.Surname,
          Email = job.HiringManager.Email,
          Address = job.HiringManager.Address.ToHiringManagerAddress(),
          Phone = job.HiringManager.PhoneNumber,
          Extension = job.HiringManager.Extension
        }
      };

      var address = job.Addresses.FirstOrDefault(x => !x.FromJobAddress) ?? job.Addresses.FirstOrDefault(x => x.FromJobAddress);

      if (address.IsNotNull())
      {
        aptimusJob.Location = new Location
        {
                                  Address1 = address.AddressLine1,
                                  Address2 = address.AddressLine2,
                                  City = address.City,
                                  PostCode = address.PostCode,
                                  State = address.State,
                                  Country = address.Country.IsNotNullOrEmpty()? address.Country : address.PostCode.IsValidZipCode()? "USA" : null,
                                  Longitude = address.Longitude.IsNotNull()? address.Longitude * -1 : address.Longitude,
                                  Latitude = address.Latitude
        };
      }

      if (job.Certificates.IsNotNullOrEmpty())
      {
        aptimusJob.Qualifications = new List<Certifications>();
        foreach (var certificate in job.Certificates)
        {
          aptimusJob.Qualifications.Add(new Certifications { Name = certificate });
        }

      }

      if (job.MinSalary.HasValue)
      {
        aptimusJob.Salary = new Salary
        {
          Minimum = job.MinSalary.Value,
          Maximum = job.MaxSalary.Value,
          Frequency = job.SalaryUnit
        };
      }

      aptimusJob.Attributes = new List<Attributes>
                                {
                                  new Attributes
                                    {
                                      AttributeName = "FOCUS_TALENT",
                                      AttributeValue = "Y"
                                    }
                                };

      if (job.OnetCode.IsNotNullOrEmpty())
        aptimusJob.Attributes.Add(new Attributes { AttributeName = "ONET", AttributeValue = job.OnetCode });

      return aptimusJob;
    }

		/// <summary>
		/// Builds the API path.
		/// </summary>
		/// <param name="parameters">The parameters.</param>
		/// <returns></returns>
		private string BuildApiPath(string parameters)
		{
			return _clientSettings.BaseUrl + parameters;
		}
		
    /// <summary>
    /// Sends the web request.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    public RequestResponse SendWebRequest(string request, string uri, string method)
    {
      var response = new RequestResponse();

      // clean up accented chars
      //var encoder = new UTF8Encoding();
      var content = new ASCIIEncoding().GetBytes(request);
      var contentLength = content.Length;

      response.OutgoingRequest = request;

      // Make WebRequest
      var webRequest = (HttpWebRequest)WebRequest.Create(uri);
      webRequest.Method = method;
      webRequest.ContentType = "application/json";
      webRequest.Accept = "application/json";
      webRequest.ContentLength = contentLength;
      //webRequest.Credentials = new NetworkCredential{UserName = _clientSettings.Username, Password = _clientSettings.Password};
      webRequest.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(_clientSettings.Username + ":" + _clientSettings.Password)));

      var dataStream = webRequest.GetRequestStream();
      dataStream.Write(content, 0, contentLength);
      dataStream.Close();

      // Get Reponse
      response.StartTime = DateTime.Now;
      var webResponse = (HttpWebResponse)webRequest.GetResponse();

      response.EndTime = DateTime.Now;
			response.StatusCode = webResponse.StatusCode;
			response.StatusDescription = webResponse.StatusDescription;
      response.Url = webRequest.RequestUri.AbsoluteUri;

      if (webResponse != null)
      {
        dataStream = webResponse.GetResponseStream();
        if (dataStream != null)
        {
          var reader = new StreamReader(dataStream);
          response.IncomingResponse = reader.ReadToEnd();
          reader.Close();
        }
      }

      return response;
    }

    private string CollectInterviewPrefs(ContactMethods methods)
    {
      if (methods.IsNull()) return null;

      var contactMethods = new List<string>();

      if ((methods & ContactMethods.Email) == ContactMethods.Email)
        contactMethods.Add(ContactMethods.Email.ToString());

      if ((methods & ContactMethods.Fax) == ContactMethods.Fax)
        contactMethods.Add(ContactMethods.Fax.ToString());

      if ((methods & ContactMethods.FocusCareer) == ContactMethods.FocusCareer)
        contactMethods.Add(ContactMethods.FocusCareer.ToString());

      if ((methods & ContactMethods.FocusTalent) == ContactMethods.FocusTalent)
        contactMethods.Add(ContactMethods.FocusTalent.ToString());

      if ((methods & ContactMethods.InPerson) == ContactMethods.InPerson)
        contactMethods.Add(ContactMethods.InPerson.ToString());

      if ((methods & ContactMethods.Mail) == ContactMethods.Mail)
        contactMethods.Add(ContactMethods.Mail.ToString());

      if ((methods & ContactMethods.Online) == ContactMethods.Online)
        contactMethods.Add(ContactMethods.Online.ToString());

      if ((methods & ContactMethods.Telephone) == ContactMethods.Telephone)
        contactMethods.Add(ContactMethods.Telephone.ToString());

      return contactMethods.IsNotNullOrEmpty() ? string.Join("|", contactMethods) : null;
    }

		#endregion
	}
}
