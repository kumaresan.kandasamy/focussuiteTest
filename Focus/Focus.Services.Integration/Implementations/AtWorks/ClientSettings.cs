﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace Focus.Services.Integration.Implementations.AtWorks
{
  [Serializable]
  public class ClientSettings
  {
    public string Url { get; set; }
    public string Customer { get; set; }
    public string AuthCode { get; set; }
  }
}
