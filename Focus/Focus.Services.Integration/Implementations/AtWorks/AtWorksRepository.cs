﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

using Focus.Core;
using Focus.Core.IntegrationMessages;
using Focus.Core.Models.Integration;
using Framework.Core;

using Newtonsoft.Json;

#endregion

namespace Focus.Services.Integration.Implementations.AtWorks
{
  public class AtWorksRepository : ClientRepository, IIntegrationRepository
  {
    private readonly ClientSettings _clientSettings;

    public AtWorksRepository(string settings)
    {
      _clientSettings = (ClientSettings) JsonConvert.DeserializeObject(settings, typeof (ClientSettings));
    }

		protected override IntegrationPoint[] GetImplementedIntegrationPoints()
		{
			return new[] { IntegrationPoint.SaveJobSeeker, IntegrationPoint.AssignJobSeekerActivity, IntegrationPoint.ReferJobSeeker };
		}

    #region Implementation of IClientRepository

		public LogActionResponse LogAction(LogActionRequest request)
		{
			return new LogActionResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

    public GetJobSeekerResponse GetJobSeeker(GetJobSeekerRequest request)
    {
      return new GetJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public GetJobSeekerResponse GetJobSeekerBySocialSecurityNumber(GetJobSeekerRequest request)
    {
      return new GetJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AuthenticateJobSeekerResponse AuthenticateJobSeeker(AuthenticateJobSeekerRequest request)
    {
      return new AuthenticateJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveJobSeekerResponse SaveJobSeeker(SaveJobSeekerRequest request)
    {
      var response = new SaveJobSeekerResponse(request);
      try
      {
        var requestResponse = SendWebRequest(BuildAtWorksReqest(request.PersonId.ToString(), request.JobSeeker.ResumeXml));
        response.IntegrationBreadcrumbs.Add(requestResponse);
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public JobSeekerLoginResponse JobSeekerLogin(JobSeekerLoginRequest request)
    {
      return new JobSeekerLoginResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveJobMatchesResponse SaveJobMatches(SaveJobMatchesRequest request)
    {
      return new SaveJobMatchesResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AssignJobSeekerActivityResponse AssignJobSeekerActivity(AssignJobSeekerActivityRequest request)
    {
      var response = new AssignJobSeekerActivityResponse(request);
      try
      {
        var requestResponse = SendWebRequest(BuildAtWorksReqest(request.PersonId.ToString(), "<activities>" + request.ActivityId + "</activities>"));
        response.IntegrationBreadcrumbs.Add(requestResponse);
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public AssignAdminToJobSeekerResponse AssignAdminToJobSeeker(AssignAdminToJobSeekerRequest request)
    {
      return new AssignAdminToJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public IsUIClaimantResponse IsUIClaimant(IsUIClaimantRequest request)
    {
      return new IsUIClaimantResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public ChangeUserPasswordResponse ChangeUserPassword(ChangeUserPasswordRequest request)
    {
      return new ChangeUserPasswordResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveJobResponse SaveJob(SaveJobRequest request)
    {
      return new SaveJobResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public GetJobResponse GetJob(GetJobRequest request)
    {
      return new GetJobResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public ReferJobSeekerResponse ReferJobSeeker(ReferJobSeekerRequest request)
    {
      var response = new ReferJobSeekerResponse(request);
      try
      {

        var referralRequest = string.Format(@"<referral>
<seekerid>{0}</seekerid>
<job>
<id>{1}</id>
<customerjobid>{2}</customerjobid>
<title>{3}</title>
<employer>{4}</employer>
<location>{5}</location><url></url>
</job>
</referral>", request.PersonId, request.JobId, request.ExternalJobId, request.Job.Title, request.Job.BusinessUnitName, BuildAddressString(request.Job.Addresses));
        var requestResponse = SendWebRequest(BuildAtWorksReqest(request.PersonId.ToString(), referralRequest));
        response.IntegrationBreadcrumbs.Add(requestResponse);
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public SetApplicationStatusResponse SetApplicationStatus(SetApplicationStatusRequest request)
    {
      return new SetApplicationStatusResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public GetEmployerResponse GetEmployer(GetEmployerRequest request)
    {
      return new GetEmployerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveEmployerResponse SaveEmployer(SaveEmployerRequest request)
    {
      return new SaveEmployerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AssignAdminToEmployerResponse AssignAdminToEmployer(AssignAdminToEmployerRequest request)
    {
      return new AssignAdminToEmployerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AssignEmployerActivityResponse AssignEmployerActivity(AssignEmployerActivityRequest request)
    {
      return new AssignEmployerActivityResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveEmployeeResponse SaveEmployee(SaveEmployeeRequest request)
    {
      return new SaveEmployeeResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AuthenticateStaffResponse AuthenticateStaffUser(AuthenticateStaffRequest request)
    {
      return new AuthenticateStaffResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public UpdateEmployeeStatusResponse UpdateEmployeeStatus(UpdateEmployeeStatusRequest request)
    {
      return new UpdateEmployeeStatusResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public DisableJobSeekersReportResponse DisableJobSeekersReport(DisableJobSeekersReportRequest request)
    {
      return new DisableJobSeekersReportResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    #endregion

		#region Helper Methods

		/// <summary>
    /// Sends the web request.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    private RequestResponse SendWebRequest(string request)
    {
      var response = new RequestResponse();

      try
      {
        var encoder = new UTF8Encoding();
        var content = encoder.GetBytes(request);
        var contentLength = content.Length;

        response.OutgoingRequest = request;

        // Make WebRequest
        var webRequest = WebRequest.Create(_clientSettings.Url);
        webRequest.Method = "POST";
        webRequest.ContentType = "text/xml;charset=utf-8";
        webRequest.ContentLength = contentLength;

        var dataStream = webRequest.GetRequestStream();
        dataStream.Write(content, 0, contentLength);
        dataStream.Close();

        // Get Reponse
        response.StartTime = DateTime.Now;

				var webResponse = (HttpWebResponse)webRequest.GetResponse();

				response.EndTime = DateTime.Now;
				response.StatusCode = webResponse.StatusCode;
				response.StatusDescription = webResponse.StatusDescription;
				response.Url = webRequest.RequestUri.AbsoluteUri;

        if (webResponse != null)
        {
          dataStream = webResponse.GetResponseStream();
          if (dataStream != null)
          {
            var reader = new StreamReader(dataStream);
            response.IncomingResponse = reader.ReadToEnd();
            reader.Close();
          }
        }
      }
			catch (WebException ex)
			{
				var httpWebResponse = ex.Response as HttpWebResponse;
				if (httpWebResponse.IsNotNull())
				{
					response.StatusCode = httpWebResponse.StatusCode;
					response.StatusDescription = httpWebResponse.StatusDescription;
				}
				else
				{
					response.StatusCode = null;
					response.StatusDescription = ex.Status.ToString();
				}

				throw new Exception("WebException while sending request to @Works", ex);
			}
      catch (Exception ex)
      {
        throw new Exception("Failure while sending request to @Works", ex);
      }

      return response;
    }

    private string BuildAtWorksReqest(string Id, string request)
    {
      return string.Format(BaseRequest, _clientSettings.Customer, _clientSettings.AuthCode, Id, request);
    }

    private string BuildAddressString(List<AddressModel> addresses)
    {
      if (addresses.IsNullOrEmpty()) return string.Empty;

      var address = addresses.First();
      var addressString = string.Empty;

      if (address.AddressLine1.IsNotNullOrEmpty()) addressString += address.AddressLine1 + ", ";
      if (address.AddressLine2.IsNotNullOrEmpty()) addressString += address.AddressLine2 + ", ";
      if (address.AddressLine3.IsNotNullOrEmpty()) addressString += address.AddressLine3 + ", ";
      if (address.City.IsNotNullOrEmpty()) addressString += address.City + ", ";
      if (address.State.IsNotNullOrEmpty()) addressString += address.State + ", ";
      if (address.PostCode.IsNotNullOrEmpty()) addressString += address.PostCode + ", ";
      if (address.Country.IsNotNullOrEmpty()) addressString += address.Country + ", ";

      return addressString.EndsWith(", ")? addressString.Substring(0, addressString.Length -1) : addressString;
    }

    private const string BaseRequest = @"<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
  <soap:Body>
    <FullXML xmlns='http://tempuri.org/'>
      <strCustomer>{0}</strCustomer>
      <strAuthCode>{1}</strAuthCode>
      <lngPK>{2}</lngPK>
      <strXML><![CDATA[{3}]]></strXML>
     </FullXML>
  </soap:Body>
</soap:Envelope>";

		#endregion
	}
}
