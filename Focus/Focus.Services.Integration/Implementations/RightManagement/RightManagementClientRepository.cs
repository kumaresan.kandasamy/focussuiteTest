﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Focus.Core.Models.Career;
using Focus.Services.Integration.Helpers;
using Newtonsoft.Json;

using Framework.Core;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.IntegrationMessages;
using Focus.Core.Models.Integration;

#endregion

namespace Focus.Services.Integration.Implementations.RightManagement
{
  public class RightManagementClientRepository : ClientRepository, IIntegrationRepository
  {
    private readonly ClientSettings _clientSettings;
	  private readonly List<ExternalLookUpItemDto> _externalLookUpItems;

		public RightManagementClientRepository(string settings, List<ExternalLookUpItemDto> externalLookUpItems)
    {
      _clientSettings = (ClientSettings)JsonConvert.DeserializeObject(settings, typeof(ClientSettings));
			_externalLookUpItems = externalLookUpItems;
    }

		protected override IntegrationPoint[] GetImplementedIntegrationPoints()
		{
			return new[] { IntegrationPoint.LogAction, IntegrationPoint.GetJobSeeker };
		}

    #region Implementation of IClientRepository

		public LogActionResponse LogAction(LogActionRequest request)
		{
			var response = new LogActionResponse(request);

			try
			{
				var activitiesRequest = String.Format(@"<ActivitiesPushInput>
	<SourceSystem>{0}</SourceSystem>
	<UserId>{1}</UserId>
	<ActionedOn>{2}</ActionedOn>
	<ActionType>{3}</ActionType>
</ActivitiesPushInput>", _clientSettings.SourceSystem, request.ActionerExternalId, request.ActionedOn.ToString("yyyy-MM-dd HH:mm:ss"), request.ActionType);

				var webResponse = SendWebRequest("BG_Activities", activitiesRequest);
				if (webResponse.StatusCode != HttpStatusCode.OK)
					throw new InvalidOperationException(String.Format("BG_Activities returned status {0} - {1}", webResponse.StatusCode, webResponse.StatusDescription));
			}
			catch (Exception e)
			{
				response.Outcome = IntegrationOutcome.Failure;
				response.Exception = e;
			}

			return response;
		}

    public GetJobSeekerResponse GetJobSeeker(GetJobSeekerRequest request)
    {
			var response = new GetJobSeekerResponse(request);

			// Right Management don't want us to update
			// the data if this isn't a new job seeker request
	    if (!request.IsNew)
		    return response;

			try
			{
				if (request.JobSeeker.IsNull() || request.JobSeeker.ExternalId.IsNullOrEmpty())
					throw new ArgumentNullException("Jobseeker External Id required");

				var getCandidateRequest = String.Format(@"<GetCandidateProfileInput>
	<SourceSystem>{0}</SourceSystem>
	<UserId>{1}</UserId>
</GetCandidateProfileInput>", _clientSettings.SourceSystem, request.JobSeeker.ExternalId);

				var webResponse = SendWebRequest("REv_GetCandidate", getCandidateRequest);
				if (webResponse.StatusCode != HttpStatusCode.OK)
					throw new InvalidOperationException(String.Format("REv_GetCandidate returned status {0} - {1}", webResponse.StatusCode, webResponse.StatusDescription));

				#region Process the data
				
				var jobSeekerXml = webResponse.IncomingResponse;

				if (jobSeekerXml.IsNotNullOrEmpty())
				{
					var responseXml = XDocument.Parse(jobSeekerXml);
					response.JobSeeker = request.JobSeeker;
					
					var seekerElement = responseXml.Descendants("GetCandidateProfileOutput").FirstOrDefault();
					if (seekerElement.IsNotNull())
					{
						response.JobSeeker.LastName = XmlHelpers.GetXmlElementStringValue(seekerElement, "LastName");
						response.JobSeeker.FirstName = XmlHelpers.GetXmlElementStringValue(seekerElement, "FirstName");
						response.JobSeeker.EmailAddress = XmlHelpers.GetXmlElementStringValue(seekerElement, "Email");

						response.JobSeeker.Resume.ResumeContent.SeekerContactDetails.PhoneNumber = new List<Phone>();

						var workPhone = XmlHelpers.GetXmlElementStringValue(seekerElement, "WorkPhone");
						if (workPhone.IsNotNullOrEmpty())
						{
							workPhone = workPhone.Length <= _clientSettings.PhoneNumberLength ? workPhone : workPhone.Substring(0, _clientSettings.PhoneNumberLength);
							if (workPhone.All(Char.IsDigit))
							{
								response.JobSeeker.Resume.ResumeContent.SeekerContactDetails.PhoneNumber.Add(new Phone { PhoneNumber = workPhone, PhoneType = PhoneType.Work });
								response.JobSeeker.PrimaryPhone = workPhone;
							}
						}

						var mobilePhone = XmlHelpers.GetXmlElementStringValue(seekerElement, "MobilePhone");
						if (mobilePhone.IsNotNullOrEmpty())
						{
							mobilePhone = mobilePhone.Length <= _clientSettings.PhoneNumberLength ? mobilePhone : mobilePhone.Substring(0, _clientSettings.PhoneNumberLength);
							if (mobilePhone.All(Char.IsDigit))
							{
								response.JobSeeker.Resume.ResumeContent.SeekerContactDetails.PhoneNumber.Add(new Phone { PhoneNumber = mobilePhone, PhoneType = PhoneType.Cell });
								if (response.JobSeeker.PrimaryPhone.IsNullOrEmpty())
									response.JobSeeker.PrimaryPhone = mobilePhone;
							}
						}

						var homePhone = XmlHelpers.GetXmlElementStringValue(seekerElement, "HomePhone");
						if (homePhone.IsNotNullOrEmpty())
						{
							homePhone = homePhone.Length <= _clientSettings.PhoneNumberLength ? homePhone : homePhone.Substring(0, _clientSettings.PhoneNumberLength);
							if (homePhone.All(Char.IsDigit))
							{
								response.JobSeeker.Resume.ResumeContent.SeekerContactDetails.PhoneNumber.Add(new Phone { PhoneNumber = homePhone, PhoneType = PhoneType.Home });
								if (response.JobSeeker.PrimaryPhone.IsNullOrEmpty())
									response.JobSeeker.PrimaryPhone = homePhone;
							}
						}

						var resumeAddress = (response.JobSeeker.Resume.ResumeContent.SeekerContactDetails.PostalAddress = new Address());
						
						response.JobSeeker.AddressLine1 = resumeAddress.Street1 = XmlHelpers.GetXmlElementStringValue(seekerElement, "AddressLine1");
						response.JobSeeker.AddressLine2 = resumeAddress.Street2 = XmlHelpers.GetXmlElementStringValue(seekerElement, "AddressLine2");

						response.JobSeeker.AddressTownCity = resumeAddress.City = XmlHelpers.GetXmlElementStringValue(seekerElement, "City");
						response.JobSeeker.AddressPostcodeZip = resumeAddress.Zip = XmlHelpers.GetXmlElementStringValue(seekerElement, "Zip");
						response.JobSeeker.AddressStateId = resumeAddress.StateId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.State, XmlHelpers.GetXmlElementStringValue(seekerElement, "State")));
						response.JobSeeker.AddressCountryId = resumeAddress.CountryId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.Country, XmlHelpers.GetXmlElementStringValue(seekerElement, "Country")));
					}
				}

				#endregion
			}
			catch (Exception e)
			{
				response.Outcome = IntegrationOutcome.Failure;
				response.Exception = e;
			}

			return response;
    }

    public GetJobSeekerResponse GetJobSeekerBySocialSecurityNumber(GetJobSeekerRequest request)
    {
      return new GetJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AuthenticateJobSeekerResponse AuthenticateJobSeeker(AuthenticateJobSeekerRequest request)
    {
      return new AuthenticateJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveJobSeekerResponse SaveJobSeeker(SaveJobSeekerRequest request)
    {
      return new SaveJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public JobSeekerLoginResponse JobSeekerLogin(JobSeekerLoginRequest request)
    {
      return new JobSeekerLoginResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveJobMatchesResponse SaveJobMatches(SaveJobMatchesRequest request)
    {
      return new SaveJobMatchesResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AssignJobSeekerActivityResponse AssignJobSeekerActivity(AssignJobSeekerActivityRequest request)
    {
      return new AssignJobSeekerActivityResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AssignAdminToJobSeekerResponse AssignAdminToJobSeeker(AssignAdminToJobSeekerRequest request)
    {
      return new AssignAdminToJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public IsUIClaimantResponse IsUIClaimant(IsUIClaimantRequest request)
    {
      return new IsUIClaimantResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public ChangeUserPasswordResponse ChangeUserPassword(ChangeUserPasswordRequest request)
    {
      return new ChangeUserPasswordResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveJobResponse SaveJob(SaveJobRequest request)
    {
			return new SaveJobResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public GetJobResponse GetJob(GetJobRequest request)
    {
      return new GetJobResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public UpdateJobStatusResponse UpdateJobStatus(UpdateJobStatusRequest request)
    {
      return new UpdateJobStatusResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public ReferJobSeekerResponse ReferJobSeeker(ReferJobSeekerRequest request)
    {
      return new ReferJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SetApplicationStatusResponse SetApplicationStatus(SetApplicationStatusRequest request)
    {
      return new SetApplicationStatusResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public GetEmployerResponse GetEmployer(GetEmployerRequest request)
    {
      return new GetEmployerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveEmployerResponse SaveEmployer(SaveEmployerRequest request)
    {
      return new SaveEmployerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AssignAdminToEmployerResponse AssignAdminToEmployer(AssignAdminToEmployerRequest request)
    {
      return new AssignAdminToEmployerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AssignEmployerActivityResponse AssignEmployerActivity(AssignEmployerActivityRequest request)
    {
      return new AssignEmployerActivityResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveEmployeeResponse SaveEmployee(SaveEmployeeRequest request)
    {
      return new SaveEmployeeResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AuthenticateStaffResponse AuthenticateStaffUser(AuthenticateStaffRequest request)
    {
      return new AuthenticateStaffResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public UpdateEmployeeStatusResponse UpdateEmployeeStatus(UpdateEmployeeStatusRequest request)
    {
      return new UpdateEmployeeStatusResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public DisableJobSeekersReportResponse DisableJobSeekersReport(DisableJobSeekersReportRequest request)
    {
      return new DisableJobSeekersReportResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    #endregion

		#region Helper Methods

		/// <summary>
		/// Sends the web request.
		/// </summary>
		/// <param name="method">The method.</param>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    public RequestResponse SendWebRequest(string method, string request)
    {
      var response = new RequestResponse();

			try
			{
				// Clean up accented chars
				var content = new ASCIIEncoding().GetBytes(request);
				var contentLength = content.Length;

				response.OutgoingRequest = request;

				// Make WebRequest
				var url = _clientSettings.Url + (_clientSettings.Url.EndsWith("/") ? "" : "/") + method;
				var webRequest = (HttpWebRequest)WebRequest.Create(url);
				webRequest.AllowAutoRedirect = false;
				webRequest.Method = "POST";
				webRequest.ContentType = "text/xml";
				webRequest.Accept = "text/xml";
				webRequest.ContentLength = contentLength;

				using (var dataStream = webRequest.GetRequestStream())
				{
					dataStream.Write(content, 0, contentLength);
					dataStream.Close();
				}

				// Get Reponse
				response.StartTime = DateTime.Now;
				var webResponse = (HttpWebResponse)webRequest.GetResponse();

				response.EndTime = DateTime.Now;
				response.StatusCode = webResponse.StatusCode;
				response.StatusDescription = webResponse.StatusDescription;
				response.Url = webRequest.RequestUri.AbsoluteUri;

				if (webResponse != null)
				{
					response.StatusCode = webResponse.StatusCode;
					response.StatusDescription = webResponse.StatusDescription;

					using (var dataStream = webResponse.GetResponseStream())
					{
						if (dataStream != null)
						{
							var reader = new StreamReader(dataStream);
							response.IncomingResponse = reader.ReadToEnd();
							reader.Close();
						}
					}
				}
			}
			catch (WebException ex)
			{
				var webResponse = ex.Response as HttpWebResponse;
				if (webResponse.IsNotNull())
				{
					response.StatusCode = webResponse.StatusCode;
					response.StatusDescription = webResponse.StatusDescription;
				}
				else
				{
					response.StatusCode = null;
					response.StatusDescription = ex.Status.ToString();
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Failure while sending request to Right Management", ex);
			}

      return response;
    }

		/// <summary>
		/// Converts to internall identifier.
		/// </summary>
		/// <param name="lookUpType">Type of the look up.</param>
		/// <param name="externalValue">The external value.</param>
		/// <returns></returns>
		private string ConvertToInternallId(ExternalLookUpType lookUpType, string externalValue)
		{
			var returnVal = _externalLookUpItems.SingleOrDefault(x => x.ExternalId == externalValue && x.ExternalLookUpType == lookUpType);
			if (returnVal.IsNotNull())
				return returnVal.InternalId;

			return null;
		}

		#endregion
	}
}
