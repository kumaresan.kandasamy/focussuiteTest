﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Xml.Linq;
using System.Xml.Schema;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.IntegrationMessages;
using Focus.Core.Models.Career;
using Focus.Core.Models.Integration;
using Focus.Services.Integration.Helpers;
using Framework.Core;
using Newtonsoft.Json;

#endregion

namespace Focus.Services.Integration.Implementations.Wisconsin
{
  public class WisconsinClientRepository : ClientRepository, IIntegrationRepository
  {

    private readonly ClientSettings _clientSettings;
	  private readonly List<ExternalLookUpItemDto> _externalLookUpItems;

    public WisconsinClientRepository(string settings, List<ExternalLookUpItemDto> externalLookUpItems)
    {
      _clientSettings = (ClientSettings)JsonConvert.DeserializeObject(settings, typeof(ClientSettings));
			_externalLookUpItems = externalLookUpItems;
    }

		protected override IntegrationPoint[] GetImplementedIntegrationPoints()
		{
			return new[]
			{			
				IntegrationPoint.AssignJobSeekerActivity, IntegrationPoint.GetEmployer, 
				IntegrationPoint.GetJob, IntegrationPoint.UpdateEmployeeStatus, IntegrationPoint.GetJobSeeker,
			  IntegrationPoint.JobSeekerLogin, IntegrationPoint.SaveJobSeeker
			};
		}

    #region Implementation of IClientRepository

    public GetJobSeekerResponse GetJobSeeker(GetJobSeekerRequest request){
      var response = new GetJobSeekerResponse(request);

      try
      {

        if ((request.JobSeeker.IsNull() || request.JobSeeker.ExternalId.IsNullOrEmpty()))
          throw new ArgumentNullException("Jobseeker External Id or job seeker XML required");


        if (request.ForImport)
        {

          string jobSeekerXml = null;
          if (request.JobSeeker.ExternalId.IsNotNullOrEmpty())
          {
            var jobSeekerBreadcrumb = BeginWebRequest("Getjobseeker", request.JobSeeker.ExternalId);
            jobSeekerXml = GetJobseeker(request.JobSeeker.ExternalId);
            EndWebRequest(jobSeekerBreadcrumb, jobSeekerXml);
            response.IntegrationBreadcrumbs.Add(jobSeekerBreadcrumb);
          }

          if (jobSeekerXml.IsNullOrEmpty()) throw new InvalidDataException("No response received");

          var responseXml = XDocument.Parse(jobSeekerXml);

          response.JobSeeker = new JobSeekerModel();

          var jobSeekerElement = responseXml.Descendants("JOB_SEEKER").FirstOrDefault();

          if (jobSeekerElement.IsNull()) throw new XmlSchemaValidationException("JOB_SEEKER element not present when expected");

          response.JobSeeker.LastName = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Last_Name");
          response.JobSeeker.FirstName = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "First_Name");
          response.JobSeeker.MiddleInitial = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Middle_Initial");
          response.JobSeeker.EmailAddress = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Email_Address");
          response.JobSeeker.ExternalId = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Job_Seeker_ID");

          response.JobSeeker.DateOfBirth = XmlHelpers.GetXmlElementDateTimeValue(jobSeekerElement, "Birth_Date");

          response.JobSeeker.PrimaryPhone = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Telephone_Area_Code") + XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Telephone_Number");

          var postCodeExtension = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Residence_Zip_Extension_Code");
          response.JobSeeker.AddressPostcodeZip = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Residence_Zip_Code") + (postCodeExtension.IsNotNullOrEmpty() ? "-" + postCodeExtension : string.Empty);
          response.JobSeeker.AddressLine1 = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Residence_Line_One_Address");
          response.JobSeeker.AddressTownCity = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Residence_City_Name");
          response.JobSeeker.AddressStateId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.State, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Residence_State_Code")));
          int countyId;
          if (int.TryParse(ConvertToInternallId(ExternalLookUpType.County, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "County_Code")), out countyId))
            response.JobSeeker.AddressCountyId = countyId;

          if (responseXml.Descendants("STATUS").Any())
          {
            var statusElement = responseXml.Descendants("STATUS").First();
            response.JobSeeker.UiClaimant = XmlHelpers.GetXmlElementBoolValue(statusElement, "UI_Claimant");
          }

          if (responseXml.Descendants("CUSTOMER_RESUME").Any())
          {
            response.JobSeeker.Resumes = new List<ResumeImportModel>();

            foreach (var resumeXml in responseXml.Descendants("CUSTOMER_RESUME"))
            {
              // Build basics for each resume
              var resume = new ResumeModel
                             {
                               ResumeMetaInfo = new ResumeEnvelope
                                                  {
                                                    CompletionStatus = ResumeCompletionStatuses.Completed, // Must set to complete to get into Lens, even if data isn't complete
                                                    ResumeCreationMethod = ResumeCreationMethod.NotSpecified,
                                                    ResumeStatus = ResumeStatuses.Active,
                                                    ResumeName = "Resume on " + XmlHelpers.GetXmlElementDateTimeValue(resumeXml, "Create_Timestamp").ToString("M/d/yyyy h:mm tt")
                                                  },
                               ResumeContent = new ResumeBody
                                                 {
                                                   ExperienceInfo = new ExperienceInfo
                                                                      {
                                                                        Jobs = new List<Job>()
                                                                      },
                                                   Profile = new UserProfile
                                                               {
                                                                 UserGivenName = new Name
                                                                                   {
                                                                                     FirstName = XmlHelpers.GetXmlElementStringValue(resumeXml, "First_Name"),
                                                                                     LastName = XmlHelpers.GetXmlElementStringValue(resumeXml, "Last_Name"),
                                                                                     MiddleName = XmlHelpers.GetXmlElementStringValue(resumeXml, "Middle_Name")
                                                                                   },
                                                                 EmailAddress = XmlHelpers.GetXmlElementStringValue(resumeXml, "Email_Address"),
                                                                 PostalAddress = new Address
                                                                                   {
                                                                                     Zip = XmlHelpers.GetXmlElementStringValue(resumeXml, "Zip_Code"),
                                                                                     Street1 = XmlHelpers.GetXmlElementStringValue(resumeXml, "Line_One_Address"),
                                                                                     Street2 = XmlHelpers.GetXmlElementStringValue(resumeXml, "Line_Two_Address"),
                                                                                     City = XmlHelpers.GetXmlElementStringValue(resumeXml, "City_Name"),
                                                                                     StateId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.State, XmlHelpers.GetXmlElementStringValue(resumeXml, "State_Code"))),
                                                                                     CountryId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.Country, "US"))
                                                                                   },
                                                                 PrimaryPhone = new Phone
                                                                                  {
                                                                                    PhoneNumber = XmlHelpers.GetXmlElementStringValue(resumeXml, "Telephone_Area_Code") + XmlHelpers.GetXmlElementStringValue(resumeXml, "Telephone_Number")
                                                                                  }
                                                               }

                                                 }
                             };


              // Iterate through work history
              if (resumeXml.Descendants("RESUME_WORK_HISTORY").Any())
              {
                foreach (var workHistoryXml in resumeXml.Descendants("RESUME_WORK_HISTORY"))
                {
                  var job = new Job
                              {
                                JobId = Guid.NewGuid(),
                                Title = XmlHelpers.GetXmlElementStringValue(workHistoryXml, "Position_Name"),
                                Employer = XmlHelpers.GetXmlElementStringValue(workHistoryXml, "Employer_Name"),
                                Description = XmlHelpers.GetXmlElementStringValue(workHistoryXml, "Responsibilities_Text"),
                                JobAddress = new Address
                                               {
                                                 City = XmlHelpers.GetXmlElementStringValue(workHistoryXml, "City_Name"),
                                                 StateId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.State, XmlHelpers.GetXmlElementStringValue(workHistoryXml, "State_Code"))),
                                                 CountryId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.Country, "US")),
                                                 CountryName = "US"
                                               }
                              };

                  DateTime jobStartDate;
                  if (DateTime.TryParse(XmlHelpers.GetXmlElementIntValue(workHistoryXml, "Start_Month") + "/1/" + XmlHelpers.GetXmlElementIntValue(workHistoryXml, "Start_Year"), CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.None, out jobStartDate))
                    job.JobStartDate = jobStartDate;
                  DateTime jobEndDate;
                  if (DateTime.TryParse(XmlHelpers.GetXmlElementIntValue(workHistoryXml, "End_Month") + "/1/" + XmlHelpers.GetXmlElementIntValue(workHistoryXml, "End_Year"), CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.None, out jobEndDate))
                    job.JobEndDate = jobEndDate;


                  if (job.JobStartDate.IsNotNull() && job.JobEndDate.IsNull())
                    job.IsCurrentJob = true;

                  resume.ResumeContent.ExperienceInfo.Jobs.Add(job);

                }
              }

              // Contact information
              resume.ResumeContent.SeekerContactDetails = new Contact
                                                            {
                                                              SeekerName = new Name
                                                                             {
                                                                               FirstName = XmlHelpers.GetXmlElementStringValue(resumeXml, "First_Name"),
                                                                               LastName = XmlHelpers.GetXmlElementStringValue(resumeXml, "Last_Name"),
                                                                               MiddleName = XmlHelpers.GetXmlElementStringValue(resumeXml, "Middle_Name")
                                                                             },
                                                              PostalAddress = new Address
                                                                                {
                                                                                  Zip = XmlHelpers.GetXmlElementStringValue(resumeXml, "Zip_Code"),
                                                                                  Street1 = XmlHelpers.GetXmlElementStringValue(resumeXml, "Line_One_Address"),
                                                                                  Street2 = XmlHelpers.GetXmlElementStringValue(resumeXml, "Line_Two_Address"),
                                                                                  City = XmlHelpers.GetXmlElementStringValue(resumeXml, "City_Name"),
                                                                                  StateId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.State, XmlHelpers.GetXmlElementStringValue(resumeXml, "State_Code"))),
                                                                                  CountryId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.Country, "US"))
                                                                                },
                                                              PhoneNumber = new List<Phone>
                                                                              {
                                                                                new Phone
                                                                                  {
                                                                                    PhoneNumber = XmlHelpers.GetXmlElementStringValue(resumeXml, "Telephone_Area_Code") + XmlHelpers.GetXmlElementStringValue(resumeXml, "Telephone_Number")
                                                                                  }
                                                                              },
                                                              EmailAddress = XmlHelpers.GetXmlElementStringValue(resumeXml, "Email_Address"),
                                                              WebSite = XmlHelpers.GetXmlElementStringValue(resumeXml, "RESUME_WEBSITE_URL_ADDRESS")
                                                            };
              PhoneTypes phoneType;
              if (Enum.TryParse(ConvertToInternallId(ExternalLookUpType.PhoneType, XmlHelpers.GetXmlAttributeStringValue(resumeXml, "Telephone_Type_Code")), true, out phoneType))
              {
                resume.ResumeContent.SeekerContactDetails.PhoneNumber[0].PhoneType = (PhoneType?) phoneType;
              }

              if (XmlHelpers.GetXmlElementStringValue(resumeXml, "Alternate_Telephone_Number").IsNotNullOrEmpty())
              {
                resume.ResumeContent.SeekerContactDetails.PhoneNumber.Add(new Phone
                                                                            {
                                                                              PhoneNumber = XmlHelpers.GetXmlElementStringValue(resumeXml, "Alternate_Telephone_Area_Code") + XmlHelpers.GetXmlElementStringValue(resumeXml, "Alternate_Telephone_Number")
                                                                            });

                if (Enum.TryParse(ConvertToInternallId(ExternalLookUpType.PhoneType, XmlHelpers.GetXmlAttributeStringValue(resumeXml, "Alternate_Telephone_Type_Code")), true, out phoneType))
                {
                  resume.ResumeContent.SeekerContactDetails.PhoneNumber[1].PhoneType = (PhoneType?) phoneType;
                }
              }

              // Optional contact info
              if (XmlHelpers.GetXmlElementStringValue(resumeXml, "Zip_Extension_Code").IsNotNullOrEmpty())
                resume.ResumeContent.SeekerContactDetails.PostalAddress.Zip += "-" + XmlHelpers.GetXmlElementStringValue(resumeXml, "Zip_Extension_Code");

              // Education
              resume.ResumeContent.EducationInfo = new EducationInfo();
              var enrollmentStatus = ConvertToInternallId(ExternalLookUpType.SchoolStatus, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Education_Status_Code"));
              if (enrollmentStatus.IsNotNullOrEmpty())
                resume.ResumeContent.EducationInfo.SchoolStatus = (SchoolStatus?) Enum.Parse(typeof (SchoolStatus), enrollmentStatus, true);
              var educationLevel = ConvertToInternallId(ExternalLookUpType.EducationLevel, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "School_Grade_Code"));
              if (educationLevel.IsNotNullOrEmpty())
                resume.ResumeContent.EducationInfo.EducationLevel = (EducationLevel?) Enum.Parse(typeof (EducationLevel), educationLevel, true);

              // Degrees/Schools
              if (resumeXml.Descendants("CUSTOMER_EDUCATION_HISTORY").Any())
              {
                resume.ResumeContent.EducationInfo.Schools = new List<School>();
                foreach (var educationXml in resumeXml.Descendants("CUSTOMER_EDUCATION_HISTORY"))
                {
                  var school = new School
                                 {
                                   Institution = XmlHelpers.GetXmlElementStringValue(educationXml, "Institution_Name"),
                                   Degree = XmlHelpers.GetXmlElementStringValue(educationXml, "Education_Details_Text"),
                                   Courses = XmlHelpers.GetXmlElementStringValue(educationXml, "Course_Of_Study_Text")
                                 };
                  var degreeEndMonth = XmlHelpers.GetXmlElementStringValue(educationXml, "End_Month");
                  var degreeEndYear = XmlHelpers.GetXmlElementStringValue(educationXml, "End_Year");
                  int endMonth, endYear;

                  if (int.TryParse(degreeEndMonth, out endMonth) && int.TryParse(degreeEndYear, out endYear))
                  {
                    if (endYear > DateTime.UtcNow.Year || (endYear == DateTime.UtcNow.Year && endMonth > DateTime.UtcNow.Month))
                      // Assume currently enrolled
                      school.ExpectedCompletionDate = new DateTime(endYear, endMonth, 1);
                    else
                      school.CompletionDate = new DateTime(endYear, endMonth, 1);
                  }

                  resume.ResumeContent.EducationInfo.Schools.Add(school);
                }
              }
              // Driver's licence
              resume.ResumeContent.Profile.License = new LicenseInfo
                                                       {
                                                         HasDriverLicense = Convert.ToBoolean(ConvertToInternallId(ExternalLookUpType.Boolean, XmlHelpers.GetXmlElementStringValue(resumeXml, "Valid_Drivers_License_Flag")))
                                                       };
              if (resume.ResumeContent.Profile.License.HasDriverLicense.GetValueOrDefault())
              {
                // Default to regular
                resume.ResumeContent.Profile.License.DrivingLicenceClassId = Convert.ToInt32(ConvertToInternallId(ExternalLookUpType.DrivingLicenceClass, "Regular"));
                resume.ResumeContent.Profile.License.DriverStateId = Convert.ToInt32(ConvertToInternallId(ExternalLookUpType.State, "55")); // 55 is WI's identifier for WI
              }
              // Set driver's licence info to be hidden by default
              resume.Special = new ResumeSpecialInfo
                                 {
                                   HideInfo = new HideResumeInfo
                                                {
                                                  HideDriverLicense = true
                                                }
                                 };
              // Summary
              resume.ResumeContent.SummaryInfo = new SummaryInfo
                                                   {
                                                     IncludeSummary = true,
                                                     Summary = XmlHelpers.GetXmlElementStringValue(resumeXml, "Job_Titles_Text"),
                                                     UseAutomatedSummary = false
                                                   };
              // No addins except references
              if (resumeXml.Descendants("CUSTOMER_REFERENCES").Any())
              {
                var references = string.Empty;
                var first = true;
                foreach (var referenceXml in resumeXml.Descendants("CUSTOMER_REFERENCES"))
                {
                  if (!first && references.IsNotNullOrEmpty())
                    references += Environment.NewLine;
                  // Formatting approved by WI
                  if (XmlHelpers.GetXmlElementStringValue(referenceXml, "Reference_Name").IsNotNullOrEmpty())
                    references += XmlHelpers.GetXmlElementStringValue(referenceXml, "Reference_Name") + Environment.NewLine;
                  if (XmlHelpers.GetXmlElementStringValue(referenceXml, "Reference_Job_Title").IsNotNullOrEmpty())
                    references += XmlHelpers.GetXmlElementStringValue(referenceXml, "Reference_Job_Title") + Environment.NewLine;
                  if (XmlHelpers.GetXmlElementStringValue(referenceXml, "Reference_Employer_Name").IsNotNullOrEmpty())
                    references += XmlHelpers.GetXmlElementStringValue(referenceXml, "Reference_Employer_Name") + Environment.NewLine;
                  if (XmlHelpers.GetXmlElementStringValue(referenceXml, "Telephone_Number").IsNotNullOrEmpty())
                  {
                    references += "Tel: ";
                    if (XmlHelpers.GetXmlElementStringValue(referenceXml, "Telephone_Area_Code").IsNotNullOrEmpty())
                      references += "(" + XmlHelpers.GetXmlElementStringValue(referenceXml, "Telephone_Area_Code") + ") ";

                    references += XmlHelpers.GetXmlElementStringValue(referenceXml, "Telephone_Number");

                    if (XmlHelpers.GetXmlElementStringValue(referenceXml, "Telephone_Extension_Number").IsNotNullOrEmpty())
                      references += "(Ext: " + XmlHelpers.GetXmlElementStringValue(referenceXml, "Telephone_Extension_Number") + ")";

                    references += Environment.NewLine;
                  }
                  if (XmlHelpers.GetXmlElementStringValue(referenceXml, "Email_Address").IsNotNullOrEmpty())
                  {
                    references += "Email: " + XmlHelpers.GetXmlElementStringValue(referenceXml, "Email_Address") + Environment.NewLine;
                  }
                  first = false;
                }
                resume.ResumeContent.References = references;
                resume.ResumeContent.AdditionalResumeSections = new List<AdditionalResumeSection>();
                resume.ResumeContent.AdditionalResumeSections.Add(new AdditionalResumeSection
                                                                    {
                                                                      SectionContent = references,
                                                                      SectionType = ResumeSectionType.References,
                                                                      UseSectionInResume = true
                                                                    });
              }

              // Profile
              resume.ResumeContent.Profile.DOB = XmlHelpers.GetXmlElementDateTimeValue(jobSeekerElement, "Birth_Date");

              EmploymentStatus employmentStatus;
              if (Enum.TryParse(ConvertToInternallId(ExternalLookUpType.EmploymentStatus, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Current_Employment_Status_Code")), out employmentStatus))
                resume.ResumeContent.ExperienceInfo.EmploymentStatus = employmentStatus;

              Genders gender;
              if (Enum.TryParse(ConvertToInternallId(ExternalLookUpType.Gender, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Gender_Code")), out gender))
                resume.ResumeContent.Profile.Sex = gender;
              resume.ResumeContent.Profile.EthnicHeritage = new EthnicHeritage();
              var ethnicity = ConvertToInternallId(ExternalLookUpType.Ethnicity, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Ethnic_Hispanic_Flag"));
              resume.ResumeContent.Profile.EthnicHeritage.EthnicHeritageId = ethnicity.IsNullOrEmpty() ? (long?) null : Convert.ToInt64(ethnicity);

              resume.ResumeContent.Profile.EthnicHeritage.RaceIds = new List<long?>();
              resume.ResumeContent.Profile.EthnicHeritage.RaceIds.Add(ConvertRaceToId(jobSeekerElement, "Race_American_Indian_Or_Alaskan_Native_Flag"));
              resume.ResumeContent.Profile.EthnicHeritage.RaceIds.Add(ConvertRaceToId(jobSeekerElement, "Race_Black_Or_African_American_Flag"));
              resume.ResumeContent.Profile.EthnicHeritage.RaceIds.Add(ConvertRaceToId(jobSeekerElement, "Race_White_Flag"));
              resume.ResumeContent.Profile.EthnicHeritage.RaceIds.Add(ConvertRaceToId(jobSeekerElement, "Race_Asian_Flag"));
              resume.ResumeContent.Profile.EthnicHeritage.RaceIds.Add(ConvertRaceToId(jobSeekerElement, "Race_Hawaiian_Native_Or_Other_Pacific_Islander_Flag"));
              resume.ResumeContent.Profile.EthnicHeritage.RaceIds.RemoveAll(x => x.IsNull());
              if (!resume.ResumeContent.Profile.EthnicHeritage.RaceIds.Any() && Convert.ToBoolean(ConvertToInternallId(ExternalLookUpType.Boolean, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Race_Other"))))
              {
                resume.ResumeContent.Profile.EthnicHeritage.RaceIds.Add(ConvertRaceToId(jobSeekerElement, "Race_Other")); // Race_Other maps to Not disclosed if no other race options are chosen
              }

              // Default to true for now (WI don't currently record this data but may do in the future)
              resume.ResumeContent.Profile.IsUSCitizen = true; //Convert.ToBoolean(ConvertToInternallId(ExternalLookUpType.Boolean, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "US_Citizen_Flag")));
              if (!resume.ResumeContent.Profile.IsUSCitizen.GetValueOrDefault())
              {
                resume.ResumeContent.Profile.AlienId = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Alien_Registration_Number");
                resume.ResumeContent.Profile.IsPermanentResident = Convert.ToBoolean(ConvertToInternallId(ExternalLookUpType.Boolean, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Permanent_Resident_Refuge_Flag")));
                resume.ResumeContent.Profile.AlienExpires = XmlHelpers.GetXmlElementNullableDateTimeValue(jobSeekerElement, "Alien_Expiration_date");
              }

              DisabilityStatus disabilityStatus;
              if (Enum.TryParse(ConvertToInternallId(ExternalLookUpType.DisabilityStatus, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Disability_Code")), true, out disabilityStatus))
              {
                resume.ResumeContent.Profile.DisabilityStatus = disabilityStatus;

                if (disabilityStatus == DisabilityStatus.Disabled)
                {
                    resume.ResumeContent.Profile.DisabilityCategoryIds = new List<long?>();
                    resume.ResumeContent.Profile.DisabilityCategoryIds.Add(ConvertDisabilityToId(jobSeekerElement, "Chronic_health_Flag"));
                    resume.ResumeContent.Profile.DisabilityCategoryIds.Add(ConvertDisabilityToId(jobSeekerElement, "Cognitive_Intellectual_Flag"));
                    resume.ResumeContent.Profile.DisabilityCategoryIds.Add(ConvertDisabilityToId(jobSeekerElement, "Hearing_related_Flag"));
                    resume.ResumeContent.Profile.DisabilityCategoryIds.Add(ConvertDisabilityToId(jobSeekerElement, "Vision_related_Flag"));
                    resume.ResumeContent.Profile.DisabilityCategoryIds.Add(ConvertDisabilityToId(jobSeekerElement, "Learning_disability_Flag"));
                    resume.ResumeContent.Profile.DisabilityCategoryIds.Add(ConvertDisabilityToId(jobSeekerElement, "Psychiatric_disability_Flag"));
                    resume.ResumeContent.Profile.DisabilityCategoryIds.Add(ConvertDisabilityToId(jobSeekerElement, "Mbility_impairment_Flag"));
                    resume.ResumeContent.Profile.DisabilityCategoryIds.RemoveAll(x => x.IsNull());
                    if (!resume.ResumeContent.Profile.DisabilityCategoryIds.Any() && Convert.ToBoolean(ConvertToInternallId(ExternalLookUpType.Boolean, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Not disclosed"))))
                        resume.ResumeContent.Profile.DisabilityCategoryIds.Add(ConvertDisabilityToId(jobSeekerElement, "Disability_Other_Flag"));
                }
              }

              // Military service
              resume.ResumeContent.Profile.Veteran = new VeteranInfo
                                                       {
                                                         IsVeteran = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Veteran_status_code").IsIn("2", "3")
                                                       };
              // No other military data is to be mapped at this point in time

              // Preferences
              resume.Special.Preferences = new Preferences
                                             {
                                               IsResumeSearchable = !Convert.ToBoolean(ConvertToInternallId(ExternalLookUpType.Boolean, XmlHelpers.GetXmlElementStringValue(resumeXml, "Exclude_From_Search_Flag"))),
                                               IsContactInfoVisible = (XmlHelpers.GetXmlElementStringValue(resumeXml, "RESUME_DISPLAY_PREFERENCES_CODE") != "P")
                                             };


              // Expected salary
              if (resumeXml.Descendants("Desired_Salary_Amount").Any())
              {
                resume.Special.Preferences.Salary = XmlHelpers.GetXmlElementFloatValue(resumeXml, "Desired_Salary_Amount");
                if (resume.Special.Preferences.Salary.GetValueOrDefault() != 0)
                  resume.Special.Preferences.SalaryFrequencyId = Convert.ToInt32(ConvertToInternallId(ExternalLookUpType.SalaryUnit, XmlHelpers.GetXmlElementStringValue(resumeXml, "Pay_Type_Code")));
                else
                  resume.Special.Preferences.Salary = null;
              }

              resume.Special.Preferences.ShiftDetail = new Shift
                                                         {
                                                           WorkOverTime = false,
                                                           ShiftTypeIds = new List<long>()
                                                         };
              resume.Special.Preferences.WillingToRelocate = Convert.ToBoolean(ConvertToInternallId(ExternalLookUpType.Boolean, XmlHelpers.GetXmlElementStringValue(resumeXml, "Willing_To_Relocate_Flag")));

              // Shift type
              if (resumeXml.Descendants("RESUME_WORK_SHIFT").Any())
              {
                foreach (var workShiftXml in resumeXml.Descendants("RESUME_WORK_SHIFT"))
                {
                  var shiftId = ConvertToInternallId(ExternalLookUpType.Shift, XmlHelpers.GetXmlElementStringValue(workShiftXml, "Job_Shift_Code"));
                  if (shiftId.IsNotNullOrEmpty() && resume.Special.Preferences.ShiftDetail.ShiftTypeIds.All(x => x != Convert.ToInt32(shiftId)))
                    resume.Special.Preferences.ShiftDetail.ShiftTypeIds.Add(Convert.ToInt32(shiftId));
                }
              }

              // Default work week to Any (See Linda Williamson's mail on 15th December 2014)
              resume.Special.Preferences.ShiftDetail.WorkWeekId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.WorkWeek, "ANY"));

              // Duration maps from many values to a single Focus value
              if (resumeXml.Descendants("RESUME_WORK_WEEK").Any())
              {
                // Add WI values in order of precedence and loop through to find most suitable match
                var durationValues = new[]
                                       {
                                         "Full-time", "Part-time", "Full-time temporary", "Part-time temporary", "On Call", "On Call Temporary", "Project/Contract", "Internship", "Apprenticeship"
                                       };
                var found = false;
                for (var i = 0; i < durationValues.Count() && !found; i++)
                {
                  foreach (var durationValue in resumeXml.Descendants("RESUME_WORK_WEEK").Where(durationValue => XmlHelpers.GetXmlElementStringValue(durationValue, "Workweek_Type").Equals(durationValues[i].ToLower(), StringComparison.OrdinalIgnoreCase)))
                  {
                    resume.Special.Preferences.ShiftDetail.WorkDurationId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.WorkType, durationValues[i]));
                    found = true;
                  }
                }
              }

              // Default search to 25 miles of resume address
              resume.Special.Preferences.ZipPreference = new List<DesiredZip>
                                                           {
                                                             new DesiredZip
                                                               {
                                                                 Zip = resume.ResumeContent.SeekerContactDetails.PostalAddress.Zip,
                                                                 RadiusId = Convert.ToInt32(ConvertToInternallId(ExternalLookUpType.Radius, "Radius.TwentyFiveMiles"))
                                                               }
                                                           };
              resume.Special.Preferences.SearchInMyState = false;
              //resume.Special.Preferences.HomeBasedJobPostings = false;
              var resumeModel = new ResumeImportModel
                                  {
                                    Resume = resume,
                                    LastUpdated = XmlHelpers.GetXmlElementDateTimeValue(resumeXml, "Update_Timestamp"),
                                    ExternalId = XmlHelpers.GetXmlElementStringValue(resumeXml, "CUSTOMER_RESUME_ID")
                                  };

              // If this is an uploaded resume then we need to pull in the 
              if (XmlHelpers.GetXmlElementStringValue(resumeXml, "RESUME_DISPLAY_PREFERENCES_CODE") == "U")
              {
                var resumeBreadcrumb = BeginWebRequest("GetResume", resumeModel.ExternalId);
                var resumeData = GetResume(resumeModel.ExternalId);
                EndWebRequest(resumeBreadcrumb, resumeData.ResumeName);
                response.IntegrationBreadcrumbs.Add(resumeBreadcrumb);

                if (resumeData.FileStream.Length <= (5*1024*1024))
                {
                  // Calculate MIME type from Focus acceptable file extensions
                  switch (resumeData.FileExtension.ToLower())
                  {
                    case ".pdf":
                      resumeModel.ContentType = "application/pdf";
                      break;
                    case ".rtf":
                      resumeModel.ContentType = "text/rtf";
                      break;
                    case ".doc":
                      resumeModel.ContentType = "application/msword";
                      break;
                    case ".docx":
                      resumeModel.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                      break;
                  }
                  // Only pass on the resume if it is a valid file type.
                  if (resumeModel.ContentType.IsNotNullOrEmpty())
                  {
                    resume.ResumeMetaInfo.ResumeCreationMethod = ResumeCreationMethod.Upload;
                    resume.Special.PreservedFormatId = 0;
                    resume.Special.PreservedOrder = "Original";
                    resumeModel.DocumentBytes = resumeData.FileStream.ToArray();
                    resumeModel.FileName = resumeData.ResumeName;
                  }
                }
              }

              response.JobSeeker.Resumes.Add(resumeModel);
            }
          }
          if (response.JobSeeker.Resumes.IsNotNullOrEmpty())
          {
            var defaultResume = response.JobSeeker.Resumes.OrderByDescending(x => x.LastUpdated).First();
            defaultResume.IsDefault = true;
            if (defaultResume.Resume.ResumeContent.IsNotNull() && defaultResume.Resume.ResumeContent.EducationInfo.IsNotNull())
              response.JobSeeker.EnrollmentStatus = defaultResume.Resume.ResumeContent.EducationInfo.SchoolStatus;
          }
        }
        else
        {
          response.JobSeeker = new JobSeekerModel();
          bool uiClaimant;
          response.JobSeeker.Resume = GetResumeTemplate(request.JobSeeker.ExternalId, response.IntegrationBreadcrumbs, out uiClaimant);
          response.JobSeeker.UiClaimant = uiClaimant;
        }

      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public GetJobSeekerResponse GetJobSeekerBySocialSecurityNumber(GetJobSeekerRequest request)
    {
      return new GetJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public GetEmployerResponse GetEmployer(GetEmployerRequest request)
    {
      var response = new GetEmployerResponse(request);

      try
      {
        if (!request.ForImport)
          return response;

        if ((request.EmployerId.IsNullOrEmpty()))
          throw new ArgumentNullException("Employer External Id required");

        string employerXml = null;
        if (request.EmployerId.IsNotNullOrEmpty())
        {
          var employerBreadcrumb = BeginWebRequest("GetErProfile", request.EmployerId);
	        employerXml = GetErProfile(request.EmployerId);
          EndWebRequest(employerBreadcrumb, employerXml);
          response.IntegrationBreadcrumbs.Add(employerBreadcrumb);
        }

        if (employerXml.IsNullOrEmpty()) throw new InvalidDataException("No profile xml received"); // Employer data called profile in JCW

        var responseXml = XDocument.Parse(employerXml);
        response.Employer = new EmployerModel{ BusinessUnits = new List<BusinessUnitModel>()};

        var profileElement = responseXml.Descendants("EmployerProfile").FirstOrDefault();
        response.Employer.FederalEmployerIdentificationNumber = XmlHelpers.GetXmlElementStringValue(profileElement, "FeinCode");
        // Insert "-" into FEIN to ensure that the formatting is consistent in Focus
        if (response.Employer.FederalEmployerIdentificationNumber.IsNotNullOrEmpty() && !response.Employer.FederalEmployerIdentificationNumber.Contains("-"))
          response.Employer.FederalEmployerIdentificationNumber = response.Employer.FederalEmployerIdentificationNumber.Insert(2, "-");
        response.Employer.Name = XmlHelpers.GetXmlElementStringValue(profileElement, "WorksiteTradeName");
        response.Employer.ExternalId = request.EmployerId;
        response.Employer.Owner = Convert.ToInt32(ConvertToInternallId(ExternalLookUpType.EmployerOwner, XmlHelpers.GetXmlElementStringValue(profileElement, "OwnershipCode")));

        var employerBuBreadcrumb = BeginWebRequest("GetErSites", request.EmployerId);
        var businessUnitsXml = XDocument.Parse(GetErSites(request.EmployerId));
        EndWebRequest(employerBuBreadcrumb, businessUnitsXml.ToString());
        response.IntegrationBreadcrumbs.Add(employerBuBreadcrumb);

        foreach (var site in businessUnitsXml.Descendants("SITE"))
        {
          var businessUnitBreadcrumb = BeginWebRequest("GetSite", XmlHelpers.GetXmlElementStringValue(site, "SITE_LOC_NUM"));
          var businessUnitXml = GetSite(XmlHelpers.GetXmlElementStringValue(site, "SITE_LOC_NUM"));
          EndWebRequest(businessUnitBreadcrumb, businessUnitXml);
          response.IntegrationBreadcrumbs.Add(businessUnitBreadcrumb);


          var buResponseXml = XDocument.Parse(businessUnitXml);

          var businessUnitElement = buResponseXml.Descendants("SiteLocation").FirstOrDefault();
          var siteElement = buResponseXml.Descendants("SiteGeneralInfo").FirstOrDefault();
          var businessUnit = new BusinessUnitModel
                               {
                                 BusinessUnit = new EmployerModel
                                                  {
                                                    ExternalId = XmlHelpers.GetXmlElementStringValue(businessUnitElement, "SiteID"),
                                                    Name = XmlHelpers.GetXmlElementStringValue(businessUnitElement, "WorksiteTradeName"),
                                                    Address = new AddressModel
                                                                {
                                                                  AddressLine1 = XmlHelpers.GetXmlElementStringValue(businessUnitElement, "MailingAddress1"),
                                                                  AddressLine2 = XmlHelpers.GetXmlElementStringValue(businessUnitElement, "MailingAddress2"),
                                                                  City = XmlHelpers.GetXmlElementStringValue(businessUnitElement, "MailingCity"),
                                                                  StateId = Convert.ToInt32(ConvertToInternallId(ExternalLookUpType.State, XmlHelpers.GetXmlElementStringValue(businessUnitElement, "MailingState"))),
                                                                  PostCode = XmlHelpers.GetXmlElementStringValue(businessUnitElement, "MailingZip"),
                                                                  CountryId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.Country, "US"))
                                                                },
                                                    PublicTransportAccessible = false,
                                                    Url = XmlHelpers.GetXmlElementStringValue(businessUnitElement, "WebSiteAddress"),
                                                    Phone = XmlHelpers.GetXmlElementStringValue(businessUnitElement, "WorksitePhoneAreaCode") + XmlHelpers.GetXmlElementStringValue(businessUnitElement, "WorksitePhoneNumber"),
                                                    PhoneExt = string.Empty,
                                                    PhoneType = PhoneTypes.Phone,
                                                    AltPhone = XmlHelpers.GetXmlElementStringValue(businessUnitElement, "FaxAreaCode") + XmlHelpers.GetXmlElementStringValue(businessUnitElement, "FaxNumber"),
                                                    AltPhoneType = PhoneTypes.Fax,
                                                    Naics = XmlHelpers.GetXmlElementStringValue(siteElement, "NAICSCode"),
                                                    BusinessDescription = XmlHelpers.GetXmlElementStringValue(siteElement, "ProductServiceDesc"),
                                                    Owner = response.Employer.Owner
                                                  },
                                 ExternalCreatedDate = XmlHelpers.GetXmlElementDateTimeValue(businessUnitElement, "CreatedDate")
                               };
          int countyId;
          if (int.TryParse(ConvertToInternallId(ExternalLookUpType.County,  XmlHelpers.GetXmlElementStringValue(businessUnitElement, "CountyCode")), out countyId))
            businessUnit.BusinessUnit.Address.CountyId = countyId;

          if (XmlHelpers.GetXmlElementStringValue(siteElement, "NumberEmployees").IsNotNullOrEmpty())
            businessUnit.BusinessUnit.NoOfEmployees = Convert.ToInt32(XmlHelpers.GetXmlElementStringValue(siteElement, "NumberEmployees"));

          if (businessUnit.BusinessUnit.Url.IsNotNullOrEmpty() && !businessUnit.BusinessUnit.Url.ToLower().StartsWith("http")) // Focus requires http prefix to urls in this field
            businessUnit.BusinessUnit.Url = "http://" + businessUnit.BusinessUnit.Url;


          var accountType = ConvertToInternallId(ExternalLookUpType.AccountType, XmlHelpers.GetXmlElementStringValue(siteElement, "StaffingAgencyFlag"));
          if (accountType.IsNotNullOrEmpty())
            businessUnit.BusinessUnit.AccountType = Convert.ToInt32(accountType);

          response.Employer.BusinessUnits.Add(businessUnit);
        }
        // Set the first business unit created as the default for the employer
        response.Employer.BusinessUnits.OrderByDescending(x => x.ExternalCreatedDate).First().IsDefault = true;

        // TODO: Use primary business unit to populate missing employer data
        var primaryBU = response.Employer.BusinessUnits.First(x => x.IsDefault);
        response.Employer.Phone = primaryBU.BusinessUnit.Phone;
        response.Employer.PhoneType = primaryBU.BusinessUnit.PhoneType;
        response.Employer.AltPhone = primaryBU.BusinessUnit.AltPhone;
        response.Employer.AltPhoneType = primaryBU.BusinessUnit.AltPhoneType;
        response.Employer.Address = primaryBU.BusinessUnit.Address;
        response.Employer.Naics = primaryBU.BusinessUnit.Naics;

        var employeesBreadcrumb = BeginWebRequest("GetEmployerContacts", response.Employer.ExternalId);

        var employeesXml = XDocument.Parse(GetEmployerContacts(response.Employer.ExternalId));

        EndWebRequest(employeesBreadcrumb, employeesXml.ToString());
        response.IntegrationBreadcrumbs.Add(employeesBreadcrumb);


        foreach (var employeeContactElement in employeesXml.Descendants("CONTACT"))
        {
          var employeeBreadcrumb = BeginWebRequest("GetEmployerContact", XmlHelpers.GetXmlElementStringValue(employeeContactElement, "ER_CNTC_NUM"));
          var employeeXml = GetEmployerContact(XmlHelpers.GetXmlElementStringValue(employeeContactElement, "ER_CNTC_NUM"));
          EndWebRequest(employeeBreadcrumb, employeeXml);
          response.IntegrationBreadcrumbs.Add(employeeBreadcrumb);

          var empResponseXml = XDocument.Parse(employeeXml);
          var employeeElement = empResponseXml.Descendants("EmployerContactInfo").FirstOrDefault();
          var employee = new EmployeeModel();
          employee.Email = XmlHelpers.GetXmlElementStringValue(employeeElement, "EmailAddress");

          // Only continue with this employee if they have a valid email address and have accessed their account in the last 26 months
          if (employee.Email.IsValidEmail() && (XmlHelpers.GetXmlElementNullableDateTimeValue(employeeElement, "InternetAccountAccessTimestamp").GetValueOrDefault() > DateTime.Now.AddMonths(-26)))
          {

            employee.FirstName = XmlHelpers.GetXmlElementStringValue(employeeElement, "FirstName");
            employee.LastName = XmlHelpers.GetXmlElementStringValue(employeeElement, "LastName");
            employee.MigrationId = XmlHelpers.GetXmlElementStringValue(employeeElement, "ContactID");
            employee.ExternalId = XmlHelpers.GetXmlElementStringValue(employeeElement, "NameID");
            employee.Address = new AddressModel
                                 {
                                   AddressLine1 = XmlHelpers.GetXmlElementStringValue(employeeElement, "LocationAddress1"),
                                   AddressLine2 = XmlHelpers.GetXmlElementStringValue(employeeElement, "LocationAddress2"),
                                   City = XmlHelpers.GetXmlElementStringValue(employeeElement, "LocationCity"),
                                   StateId = Convert.ToInt32(ConvertToInternallId(ExternalLookUpType.State, XmlHelpers.GetXmlElementStringValue(employeeElement, "LocationState"))),
                                   PostCode = XmlHelpers.GetXmlElementStringValue(employeeElement, "LocationZip"),
                                   CountryId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.Country, "US"))
                                 };
            employee.Phone = primaryBU.BusinessUnit.Phone;
            employee.PhoneExt = string.Empty;
            employee.AltPhone = primaryBU.BusinessUnit.AltPhone;


            response.Employer.Employees.Add(employee);
          }
        }
        // No employee is a valid rule in WI but we need a dummy on
        if (response.Employer.Employees.IsNullOrEmpty())
        {
          var employee = new EmployeeModel
                           {
                             Email = DateTime.Now.Ticks.ToString() + "@dummyemail.comx",
                             FirstName = "Fake",
                             LastName = "Employee",
                             ExternalId = "FakeEmployee" + response.Employer.ExternalId,
                             Address = response.Employer.Address,
                             Phone = primaryBU.BusinessUnit.Phone,
                             PhoneExt = string.Empty,
                             AltPhone = primaryBU.BusinessUnit.AltPhone
                           };
          response.Employer.Employees.Add(employee);
        }

        // Retrieve jobs that need to be imported
        response.JobsToImport = new List<Tuple<string, ApprovalStatuses>>();

        var employerJobsBreadcrumb = BeginWebRequest("GetEmpJobOrds", response.Employer.ExternalId);
        var employerJobsXml = GetEmpJobOrds(response.Employer.ExternalId);
        EndWebRequest(employerJobsBreadcrumb, employerJobsXml);
        response.IntegrationBreadcrumbs.Add(employerJobsBreadcrumb);


        var empJobsResponseXml = XDocument.Parse(employerJobsXml);
        // Awaiting approval
        foreach (var employerJobElement in empJobsResponseXml.Descendants("AWAITING_APPROVAL_JOB_ORDERS"))
        {
          response.JobsToImport.Add(new Tuple<string, ApprovalStatuses>(XmlHelpers.GetXmlElementStringValue(employerJobElement, "JobOrderNumber"), ApprovalStatuses.WaitingApproval));
        }
        // Active jobs
        foreach (var employerJobElement in empJobsResponseXml.Descendants("ON_JOBNET_JOB_ORDERS"))
        {
          response.JobsToImport.Add(new Tuple<string, ApprovalStatuses>(XmlHelpers.GetXmlElementStringValue(employerJobElement, "JobOrderNumber"), ApprovalStatuses.Approved));
        }


      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public GetJobResponse GetJob(GetJobRequest request)
    {
      var response = new GetJobResponse(request);

      try
      {
        if ((request.JobId.IsNullOrEmpty()))
          throw new ArgumentNullException("Job External Id required");

        string jobXml = null;
        if (request.JobId.IsNotNullOrEmpty())
        {
          var jobBreadcrumb = BeginWebRequest("GetJobOrdData", request.JobId);
          jobXml = GetJobOrdData(request.JobId);
          EndWebRequest(jobBreadcrumb, jobXml);
          response.IntegrationBreadcrumbs.Add(jobBreadcrumb);
        }
        else
          jobXml = ExampleJobXml;

        if (jobXml.IsNullOrEmpty()) throw new InvalidDataException("No response received");

        var responseXml = XDocument.Parse(jobXml);
        response.Job = new JobModel();

        var jobElement = responseXml.Descendants("Job_Order").FirstOrDefault();

        // Basics
        response.Job.ExternalId = XmlHelpers.GetXmlElementStringValue(jobElement, "JobOrderNumber");
        response.Job.ExternalBusinessUnitId = XmlHelpers.GetXmlElementStringValue(jobElement, "SiteLocationNumber");
        response.Job.ExternalEmployerId = XmlHelpers.GetXmlElementStringValue(jobElement, "EmployerProfileNumber");

        // Title & Company
        response.Job.Title = XmlHelpers.GetXmlElementStringValue(jobElement, "JobTitle");
        response.Job.OnetCode = XmlHelpers.GetXmlElementStringValue(jobElement, "OccupationalInformationNetworkCode");
        response.Job.ConfidentialEmployer = !XmlHelpers.GetXmlElementBoolValue(jobElement, "EmployerIdentificationDisplayedFlag");
        var siteLocationElement = responseXml.Descendants("SiteLocation").FirstOrDefault();
        if (siteLocationElement.IsNotNull())
          response.Job.BusinessUnitDescription = XmlHelpers.GetXmlElementStringValue(siteLocationElement, "SiteLocationDescription");

        response.Job.Description = XmlHelpers.GetXmlElementStringValue(jobElement, "DutiesResponsibilitiesText");


        EducationLevels educationLevel;

        if (Enum.TryParse(ConvertToInternallId(ExternalLookUpType.JobEducationLevel, XmlHelpers.GetXmlElementStringValue(jobElement, "EducationLevelCode")), out educationLevel))
          response.Job.EducationLevel = educationLevel;
        else response.Job.EducationLevel = EducationLevels.None;

        int minimumAge;
        if (int.TryParse(XmlHelpers.GetXmlElementStringValue(jobElement, "MinimumAge"), out minimumAge))
        {
          response.Job.MinimumAge = minimumAge;
          response.Job.MinimumAgeReason = "Previously approved"; // Text provided by WI
        }
        // Details
        response.Job.Addresses = new List<AddressModel>
                                   {
                                     new AddressModel
                                       {
                                         AddressLine1 = XmlHelpers.GetXmlElementStringValue(jobElement, "SiteLocationLine1Address"),
                                         City = XmlHelpers.GetXmlElementStringValue(jobElement, "SiteLocationCityName"),
                                         StateId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.State, XmlHelpers.GetXmlElementStringValue(jobElement, "SiteLocationStateCode"))),
                                         PostCode = XmlHelpers.GetXmlElementStringValue(jobElement, "SiteLocationZipCode") + XmlHelpers.GetXmlElementStringValue(jobElement, "SiteLocationZipExtensionCode")
                                       }
                                   };

        int countyId;
        if (int.TryParse(XmlHelpers.GetXmlElementStringValue(jobElement, "CountyCode"), out countyId))
          response.Job.Addresses[0].CountyId = countyId;
        response.Job.PublicTransportAccessible = XmlHelpers.GetXmlElementBoolValue(jobElement, "PublicTransportationFlag");
        response.Job.FederalContractor = false;
        response.Job.CourtOrderedAffirmativeAction = false;
        response.Job.ForeignLabourCertificationH2A = false;
        response.Job.ForeignLabourCertificationH2B = false;
        response.Job.ForeignLabourCertificationOther = false;
        response.Job.WorkOpportunitiesTaxCreditHires = WorkOpportunitiesTaxCreditCategories.None;

        // Salary and benefits
        response.Job.MinSalary = XmlHelpers.GetXmlElementDecimalValue(jobElement, "MinimumPayAmount");
        response.Job.MaxSalary = XmlHelpers.GetXmlElementDecimalValue(jobElement, "MaximumPayAmount");
        if (response.Job.MinSalary > response.Job.MaxSalary)
          response.Job.MaxSalary = response.Job.MinSalary;
        if (response.Job.MinSalary.GetValueOrDefault() > 0 && response.Job.MaxSalary.GetValueOrDefault() > 0)
          response.Job.SalaryUnitId = Convert.ToInt32(ConvertToInternallId(ExternalLookUpType.SalaryUnit, XmlHelpers.GetXmlElementStringValue(jobElement, "MinimumPayTimeUnitType")));

        response.Job.Weekdays = DaysOfWeek.None;
        foreach (var workdayElement in responseXml.Descendants("Job_Order_Work_Day"))
        {
          switch (XmlHelpers.GetXmlElementStringValue(workdayElement, "WorkdayType"))
          {
            case "WEEKENDS ONLY":
              response.Job.Weekdays = response.Job.Weekdays | DaysOfWeek.Weekends;
              break;
            case "MONDAY":
              response.Job.Weekdays = response.Job.Weekdays | DaysOfWeek.Monday;
              break;
            case "TUESDAY":
              response.Job.Weekdays = response.Job.Weekdays | DaysOfWeek.Tuesday;
              break;
            case "WEDNESDAY":
              response.Job.Weekdays = response.Job.Weekdays | DaysOfWeek.Wednesday;
              break;
            case "THURSDAY":
              response.Job.Weekdays = response.Job.Weekdays | DaysOfWeek.Thursday;
              break;
            case "FRIDAY":
              response.Job.Weekdays = response.Job.Weekdays | DaysOfWeek.Friday;
              break;
            case "SATURDAY":
              response.Job.Weekdays = response.Job.Weekdays | DaysOfWeek.Saturday;
              break;
            case "SUNDAY":
              response.Job.Weekdays = response.Job.Weekdays | DaysOfWeek.Sunday;
              break;
            case "MONDAY-FRIDAY":
              response.Job.Weekdays = response.Job.Weekdays | DaysOfWeek.Weekdays;
              break;
            case "OTHER":
              response.Job.Weekdays = response.Job.Weekdays | DaysOfWeek.None;
              break;
          }
        }
        // TODO: Normal work shifts
        var shifts = responseXml.Descendants("Job_Order_Work_Shift").FirstOrDefault();
        if (shifts.IsNotNull())
        {
          long shiftId;
          if (long.TryParse(ConvertToInternallId(ExternalLookUpType.Shift, XmlHelpers.GetXmlElementStringValue(shifts, "JobShiftCode")), out shiftId))
            response.Job.ShiftId = shiftId;
        }

        response.Job.EmploymentStatusId = Convert.ToInt32(ConvertToInternallId(ExternalLookUpType.JobEmploymentStatus, XmlHelpers.GetXmlElementStringValue(jobElement, "WorkweekType")));
        response.Job.MinimumHoursPerWeek = XmlHelpers.GetXmlElementIntValue(jobElement, "Min_Work_Hrs_Qty");
        response.Job.MaximumHoursPerWeek = XmlHelpers.GetXmlElementIntValue(jobElement, "Max_Work_Hrs_Qty");
        if (response.Job.MinimumHoursPerWeek.IsNotNull() && (response.Job.MaximumHoursPerWeek.IsNullOrZero() || (response.Job.MaximumHoursPerWeek.GetValueOrDefault() < response.Job.MinimumHoursPerWeek.Value)))
          response.Job.MaximumHoursPerWeek = response.Job.MinimumHoursPerWeek;
        if ((response.Job.MaximumHoursPerWeek.IsNotNull() && (response.Job.MaximumHoursPerWeek.IsNull() || (response.Job.MaximumHoursPerWeek.GetValueOrDefault() < response.Job.MinimumHoursPerWeek.Value))))
          response.Job.MinimumHoursPerWeek = response.Job.MaximumHoursPerWeek;

        if ((response.Job.MinimumHoursPerWeek.IsNotNull() && response.Job.MaximumHoursPerWeek.IsNotNull()) && response.Job.MaximumHoursPerWeek == response.Job.MinimumHoursPerWeek)
          response.Job.MinimumHoursPerWeek = null;

        response.Job.JobTypeId = Convert.ToInt32(ConvertToInternallId(ExternalLookUpType.JobType, XmlHelpers.GetXmlElementStringValue(jobElement, "WorkweekType")));
        

        // Benefits
        response.Job.LeaveBenefits = LeaveBenefits.None;
        response.Job.RetirementBenefits = RetirementBenefits.None;
        response.Job.InsuranceBenefits = InsuranceBenefits.None;
        response.Job.MiscellaneousBenefits = MiscellaneousBenefits.None;

        foreach (var benefit in responseXml.Descendants("Job_Order_Benefit"))
        {
          var benefitNumber = XmlHelpers.GetXmlElementIntValue(benefit, "BenefitNumber");
          switch (benefitNumber)
          {
            case 15:
              response.Job.LeaveBenefits = response.Job.LeaveBenefits | LeaveBenefits.PaidHolidays;
              break;
            case 20:
              response.Job.LeaveBenefits = response.Job.LeaveBenefits | LeaveBenefits.Sick;
              break;
            case 25:
            case 17:
              response.Job.LeaveBenefits = response.Job.LeaveBenefits | LeaveBenefits.Vacation;
              break;
            case 33:
              response.Job.RetirementBenefits = response.Job.RetirementBenefits | RetirementBenefits.Four03BPlan;
              break;
            case 1:
              response.Job.RetirementBenefits = response.Job.RetirementBenefits | RetirementBenefits.Four01K;
              break;
            case 18:
              response.Job.RetirementBenefits = response.Job.RetirementBenefits | RetirementBenefits.ProfitSharing;
              break;
            case 5:
              response.Job.RetirementBenefits = response.Job.RetirementBenefits | RetirementBenefits.DeferredCompensation;
              break;
            case 7:
              response.Job.InsuranceBenefits = response.Job.InsuranceBenefits | InsuranceBenefits.Dental;
              break;
            case 9:
              response.Job.InsuranceBenefits = response.Job.InsuranceBenefits | InsuranceBenefits.Disability;
              break;
            case 32:
              response.Job.InsuranceBenefits = response.Job.InsuranceBenefits | InsuranceBenefits.DomesticPartnerCoverage;
              break;
            case 12:
              response.Job.InsuranceBenefits = response.Job.InsuranceBenefits | InsuranceBenefits.Health;
              break;
            case 10:
              response.Job.InsuranceBenefits = response.Job.InsuranceBenefits | InsuranceBenefits.HealthSavings;
              break;
            case 14:
              response.Job.InsuranceBenefits = response.Job.InsuranceBenefits | InsuranceBenefits.Life;
              break;
            case 31:
              response.Job.InsuranceBenefits = response.Job.InsuranceBenefits | InsuranceBenefits.Vision;
              break;
            case 2:
            case 3:
            case 4:
              response.Job.MiscellaneousBenefits = response.Job.MiscellaneousBenefits | MiscellaneousBenefits.ChildCare;
              break;
            case 24:
              response.Job.MiscellaneousBenefits = response.Job.MiscellaneousBenefits | MiscellaneousBenefits.TuitionAssistance;
              break;
            case 28:
              response.Job.MiscellaneousBenefits = response.Job.MiscellaneousBenefits | MiscellaneousBenefits.ClothingAllowance;
              break;
            case 99:
              response.Job.MiscellaneousBenefits = response.Job.MiscellaneousBenefits | MiscellaneousBenefits.Other;
              response.Job.OtherBenefitsDetails = XmlHelpers.GetXmlElementStringValue(benefit, "OtherBenefitsText");
              break;
          }
        }
        if (response.Job.OtherBenefitsDetails.IsNull()) response.Job.OtherBenefitsDetails = string.Empty;


        foreach (var contactMethod in responseXml.Descendants("Job_Order_application_Method"))
        {
          var method = XmlHelpers.GetXmlElementIntValue(contactMethod, "ApplicationMethodCode");

          switch (method)
          {
            case 3:
              response.Job.InterviewContactPreferences = response.Job.InterviewContactPreferences | ContactMethods.Email;
              response.Job.ContactEmail = XmlHelpers.GetXmlElementStringValue(contactMethod, "ApplicationInstructionText");
              break;
            case 4:
              response.Job.InterviewContactPreferences = response.Job.InterviewContactPreferences | ContactMethods.Online;
              response.Job.ContactUrl = XmlHelpers.GetXmlElementStringValue(contactMethod, "ApplicationInstructionText");
              break;
            case 2:
              response.Job.InterviewContactPreferences = response.Job.InterviewContactPreferences | ContactMethods.Mail;
              response.Job.ContactAddress = XmlHelpers.GetXmlElementStringValue(contactMethod, "ApplicationInstructionText");
              break;
            case 5:
              response.Job.InterviewContactPreferences = response.Job.InterviewContactPreferences | ContactMethods.Fax;
              response.Job.ContactFax = XmlHelpers.GetXmlElementStringValue(contactMethod, "ApplicationInstructionText");
              break;
            case 7:
              response.Job.InterviewContactPreferences = response.Job.InterviewContactPreferences | ContactMethods.Telephone;
              response.Job.ContactPhone = XmlHelpers.GetXmlElementStringValue(contactMethod, "ApplicationInstructionText");
              break;
            case 1:
            case 99:
              response.Job.InterviewContactPreferences = response.Job.InterviewContactPreferences | ContactMethods.InPerson;
              response.Job.ContactInPerson = XmlHelpers.GetXmlElementStringValue(contactMethod, "ApplicationInstructionText");
              break;
          }
        }
        if (response.Job.InterviewContactPreferences == ContactMethods.None)
          response.Job.InterviewContactPreferences = ContactMethods.FocusCareer;
        else if (response.Job.ContactUrl.IsNotNullOrEmpty() && !response.Job.ContactUrl.ToLower().StartsWith("http")) // Focus requires http prefix to urls in this field
          response.Job.ContactUrl = "http://" + response.Job.ContactUrl;


        response.Job.ClosingDate = XmlHelpers.GetXmlElementNullableDateTimeValue(jobElement, "JobOrderRemovalDate");
        response.Job.Openings = XmlHelpers.GetXmlElementIntValue(jobElement, "JobOpeningsQuantity");

        if (response.Job.ClosingDate < DateTime.Now)
          response.Job.JobStatus = JobStatuses.Closed;


      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

		public AssignJobSeekerActivityResponse AssignJobSeekerActivity(AssignJobSeekerActivityRequest request)
		{
			// Check to see if we have an id for the user, otherwise don't send the message
			if (request.JobSeekerId.IsNullOrEmpty()) return new AssignJobSeekerActivityResponse(request){Outcome = IntegrationOutcome.Failure};

			var response = new AssignJobSeekerActivityResponse(request);
			string selfServiceType;

			switch (request.ActivityActionType)
			{
				case ActionTypes.CompleteResume:
				case ActionTypes.EditDefaultCompletedResume:
					selfServiceType = "130";
					break;

				case ActionTypes.RunManualJobSearch:
					selfServiceType = "131";
					break;

				case ActionTypes.ViewCareerExplorationTools:
					selfServiceType = "132";
					break;

				default:
					return new AssignJobSeekerActivityResponse(request) {Outcome = IntegrationOutcome.NotImplemented};
			}

			response.Outcome = AddSelfService(request.JobSeekerId, selfServiceType)
					                  ? IntegrationOutcome.Success
					                  : IntegrationOutcome.Failure;

			return response;
		}

    public SaveJobSeekerResponse SaveJobSeeker(SaveJobSeekerRequest request)
    {
      var response = new SaveJobSeekerResponse(request)
                       {
                         ExternalJobSeekerId = request.JobSeeker.ExternalId
                       };

      try
      {
        var Race_American_Indian_Or_Alaskan_Native_Flag = "2";
        var Race_Asian_Flag = "2";
        var Race_Black_Or_African_American_Flag = "2";
        var Race_Hawaiian_Native_Or_Other_Pacific_Islander_Flag = "2";
        var Race_White_Flag = "2";
        var Race_Other_Flag = "2";
        var Ethnic_Hispanic_Flag = "2";

        if (request.JobSeeker.DefaultResumeCompletionStatus.GetValueOrDefault(ResumeCompletionStatuses.None) == ResumeCompletionStatuses.Completed)
        {
          if (request.JobSeeker.Resume.ResumeContent.Profile.IsNotNull() && request.JobSeeker.Resume.ResumeContent.Profile.EthnicHeritage.IsNotNull())
          {
            if (request.JobSeeker.Resume.ResumeContent.Profile.EthnicHeritage.RaceIds.IsNotNullOrEmpty())
            {
              foreach (var externalRaceId in request.JobSeeker.Resume.ResumeContent.Profile.EthnicHeritage.RaceIds.Select(raceId => ConvertToExternallId(ExternalLookUpType.Race, raceId.ToString())).Where(externalRaceId => externalRaceId.IsNotNull()))
              {
                switch (externalRaceId)
                {
                  case "Race_American_Indian_Or_Alaskan_Native_Flag":
                    Race_American_Indian_Or_Alaskan_Native_Flag = "1";
                    break;
                  case "Race_Asian_Flag":
                    Race_Asian_Flag = "1";
                    break;
                  case "Race_Black_Or_African_American_Flag":
                    Race_Black_Or_African_American_Flag = "1";
                    break;
                  case "Race_Hawaiian_Native_Or_Other_Pacific_Islander_Flag":
                    Race_Hawaiian_Native_Or_Other_Pacific_Islander_Flag = "1";
                    break;
                  case "Race_White_Flag":
                    Race_White_Flag = "1";
                    break;
                  case "Race_Other_Flag":
                    Race_Other_Flag = "1";
                    break;
                }
              }

              Ethnic_Hispanic_Flag = ConvertToExternallId(ExternalLookUpType.Ethnicity, request.JobSeeker.Resume.ResumeContent.Profile.EthnicHeritage.EthnicHeritageId.GetValueOrDefault().ToString()) ?? "2";

            }

            var Education_Status_Code = ConvertToExternallId(ExternalLookUpType.SchoolStatus, request.JobSeeker.Resume.ResumeContent.EducationInfo.SchoolStatus.ToString());
            var School_Grade_Code = ConvertToExternallId(ExternalLookUpType.EducationLevel, request.JobSeeker.Resume.ResumeContent.EducationInfo.EducationLevel.ToString());
            var Current_Employment_Status_Code = ConvertToExternallId(ExternalLookUpType.EmploymentStatus, request.JobSeeker.Resume.ResumeContent.ExperienceInfo.EmploymentStatus.ToString());

            var jobSeekerBreadcrumb = BeginWebRequest("UpdateContactInformation", request.JobSeeker.ExternalId, Ethnic_Hispanic_Flag, Race_American_Indian_Or_Alaskan_Native_Flag, Race_Asian_Flag, Race_Black_Or_African_American_Flag, Race_Hawaiian_Native_Or_Other_Pacific_Islander_Flag, Race_White_Flag, Race_Other_Flag, Education_Status_Code, School_Grade_Code, Current_Employment_Status_Code);
            var success = UpdateContactInformation(request.JobSeeker.ExternalId, Ethnic_Hispanic_Flag, Race_American_Indian_Or_Alaskan_Native_Flag, Race_Asian_Flag, Race_Black_Or_African_American_Flag, Race_Hawaiian_Native_Or_Other_Pacific_Islander_Flag, Race_White_Flag, Race_Other_Flag, Education_Status_Code, School_Grade_Code, Current_Employment_Status_Code);
            EndWebRequest(jobSeekerBreadcrumb, success.ToString());
            response.IntegrationBreadcrumbs.Add(jobSeekerBreadcrumb);

            response.Outcome = success ? IntegrationOutcome.Success : IntegrationOutcome.Failure;
          }
        }

        // Send resume stats regardless of whether we have a complete resume
        var resumeCompletionBreadcrumb = BeginWebRequest("CustomerResumeStatus", request.JobSeeker.ExternalId, request.JobSeeker.Stats.ActiveResumes.ToString(), request.JobSeeker.Stats.SearchableResumes.ToString());
        var resumeCompletionResult = CustomerResumeStatus(request.JobSeeker.ExternalId, request.JobSeeker.Stats.ActiveResumes.ToString(), request.JobSeeker.Stats.SearchableResumes.ToString());
        EndWebRequest(resumeCompletionBreadcrumb, resumeCompletionResult.ToString());
        response.IntegrationBreadcrumbs.Add(resumeCompletionBreadcrumb);

      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }
      return response;
    }

    public JobSeekerLoginResponse JobSeekerLogin(JobSeekerLoginRequest request)
    {
      var response = new JobSeekerLoginResponse(request);
      try
      {
        bool uiClaimant;
        response.Resume = GetResumeTemplate(request.JobSeeker.ExternalId, response.IntegrationBreadcrumbs, out uiClaimant);
        response.UiClaimant = uiClaimant;
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }
      return response;
    }

    public ResumeModel GetResumeTemplate(string jobSeekerId, List<RequestResponse> breadcrumbs, out bool uiClamaint)
     {
       var jobSeekerBreadcrumb = BeginWebRequest("Getjobseeker", jobSeekerId);
       var jobSeekerXml = GetJobseeker(jobSeekerId);
       EndWebRequest(jobSeekerBreadcrumb, jobSeekerXml);
       breadcrumbs.Add(jobSeekerBreadcrumb);

       var responseXml = XDocument.Parse(jobSeekerXml);

       var jobSeekerElement = responseXml.Descendants("JOB_SEEKER").FirstOrDefault();

       uiClamaint = false;
       if (responseXml.Descendants("STATUS").Any())
       {
         var statusElement = responseXml.Descendants("STATUS").First();
         uiClamaint = XmlHelpers.GetXmlElementBoolValue(statusElement, "UI_Claimant");
       }


       var resume = new ResumeModel
       {
         ResumeMetaInfo = new ResumeEnvelope
         {
           CompletionStatus = ResumeCompletionStatuses.None, // Must set to complete to get into Lens, even if data isn't complete
           ResumeCreationMethod = ResumeCreationMethod.NotSpecified,
           ResumeStatus = ResumeStatuses.Active,
           ResumeName = "Resume template on " + DateTime.Now.ToString("M/d/yyyy h:mm tt")
         },
         ResumeContent = new ResumeBody
         {
           ExperienceInfo = new ExperienceInfo
           {
             Jobs = new List<Job>()
           },
           Profile = new UserProfile
           {
             UserGivenName = new Name
             {
               FirstName = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "First_Name"),
               LastName = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Last_Name"),
               MiddleName = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Middle_Initial")
             },
             EmailAddress = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Email_Address"),
             PostalAddress = new Address
             {
               Zip = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Residence_Zip_Code"),
               Street1 = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Residence_Line_One_Address"),
               Street2 = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Residence_Line_Two_Address"),
               City = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Residence_City_Name"),
               StateId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.State, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Residence_State_Code"))),
               CountryId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.Country, "US"))
             },
             PrimaryPhone = new Phone
             {
               PhoneNumber = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Telephone_Area_Code") + XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Telephone_Number")
             }
           },
           SeekerContactDetails = new Contact
           {
             SeekerName = new Name
             {
               FirstName = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "First_Name"),
               LastName = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Last_Name"),
               MiddleName = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Middle_Initial")
             },
             PostalAddress = new Address
             {
               Zip = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Residence_Zip_Code"),
               Street1 = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Residence_Line_One_Address"),
               Street2 = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Residence_Line_Two_Address"),
               City = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Residence_City_Name"),
               StateId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.State, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Residence_State_Code"))),
               CountryId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.Country, "US"))
             },
             EmailAddress = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Email_Address"),
             PhoneNumber = new List<Phone> { new Phone { PhoneNumber = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Telephone_Area_Code") + XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Telephone_Number"), PhoneType = PhoneType.Cell } }
           }

         },
         Special = new ResumeSpecialInfo()
       };
       int countyId;
       if (int.TryParse(ConvertToInternallId(ExternalLookUpType.County, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "County_Code")), out countyId))
       {
         resume.ResumeContent.Profile.PostalAddress.CountyId = countyId;
         resume.ResumeContent.SeekerContactDetails.PostalAddress.CountyId = countyId;
       }
       var postCodeExtension = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Residence_Zip_Extension_Code");
       if (postCodeExtension.IsNotNullOrEmpty())
       {
         resume.ResumeContent.Profile.PostalAddress.Zip += "-" + postCodeExtension;
         resume.ResumeContent.SeekerContactDetails.PostalAddress.Zip += "-" + postCodeExtension;
       }


       // Education
       resume.ResumeContent.EducationInfo = new EducationInfo();
       var enrollmentStatus = ConvertToInternallId(ExternalLookUpType.SchoolStatus, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Education_Status_Code"));
       if (enrollmentStatus.IsNotNullOrEmpty())
         resume.ResumeContent.EducationInfo.SchoolStatus = (SchoolStatus?)Enum.Parse(typeof(SchoolStatus), enrollmentStatus, true);
       var educationLevel = ConvertToInternallId(ExternalLookUpType.EducationLevel, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "School_Grade_Code"));
       if (educationLevel.IsNotNullOrEmpty())
         resume.ResumeContent.EducationInfo.EducationLevel = (EducationLevel?)Enum.Parse(typeof(EducationLevel), educationLevel, true);

       // Profile
       resume.ResumeContent.Profile.DOB = XmlHelpers.GetXmlElementDateTimeValue(jobSeekerElement, "Birth_Date");

       EmploymentStatus employmentStatus;
       if (Enum.TryParse(ConvertToInternallId(ExternalLookUpType.EmploymentStatus, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Current_Employment_Status_Code")), out employmentStatus))
         resume.ResumeContent.ExperienceInfo.EmploymentStatus = employmentStatus;

       Genders gender;
       if (Enum.TryParse(ConvertToInternallId(ExternalLookUpType.Gender, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Gender_Code")), out gender))
         resume.ResumeContent.Profile.Sex = gender;
       resume.ResumeContent.Profile.EthnicHeritage = new EthnicHeritage();
       var ethnicity = ConvertToInternallId(ExternalLookUpType.Ethnicity, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Ethnic_Hispanic_Flag"));
       resume.ResumeContent.Profile.EthnicHeritage.EthnicHeritageId = ethnicity.IsNullOrEmpty() ? (long?)null : Convert.ToInt64(ethnicity);

       resume.ResumeContent.Profile.EthnicHeritage.RaceIds = new List<long?>();
       resume.ResumeContent.Profile.EthnicHeritage.RaceIds.Add(ConvertRaceToId(jobSeekerElement, "Race_American_Indian_Or_Alaskan_Native_Flag"));
       resume.ResumeContent.Profile.EthnicHeritage.RaceIds.Add(ConvertRaceToId(jobSeekerElement, "Race_Black_Or_African_American_Flag"));
       resume.ResumeContent.Profile.EthnicHeritage.RaceIds.Add(ConvertRaceToId(jobSeekerElement, "Race_White_Flag"));
       resume.ResumeContent.Profile.EthnicHeritage.RaceIds.Add(ConvertRaceToId(jobSeekerElement, "Race_Asian_Flag"));
       resume.ResumeContent.Profile.EthnicHeritage.RaceIds.Add(ConvertRaceToId(jobSeekerElement, "Race_Hawaiian_Native_Or_Other_Pacific_Islander_Flag"));
       resume.ResumeContent.Profile.EthnicHeritage.RaceIds.RemoveAll(x => x.IsNull());
       if (!resume.ResumeContent.Profile.EthnicHeritage.RaceIds.Any() && Convert.ToBoolean(ConvertToInternallId(ExternalLookUpType.Boolean, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Race_Other"))))
       {
         resume.ResumeContent.Profile.EthnicHeritage.RaceIds.Add(ConvertRaceToId(jobSeekerElement, "Race_Other")); // Race_Other maps to Not disclosed if no other race options are chosen
       }

       var uSCitizenFlag = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "US_Citizen_Flag");

      // If value not supplied by WI default to true! (See email from Nicole on 23/04/2015
      resume.ResumeContent.Profile.IsUSCitizen = uSCitizenFlag.IsNullOrEmpty() || Convert.ToBoolean(ConvertToInternallId(ExternalLookUpType.Boolean, uSCitizenFlag));

       if (!resume.ResumeContent.Profile.IsUSCitizen.GetValueOrDefault())
       {
         resume.ResumeContent.Profile.AlienId = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Alien_Registration_Number");
         resume.ResumeContent.Profile.IsPermanentResident = Convert.ToBoolean(ConvertToInternallId(ExternalLookUpType.Boolean, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Permanent_Resident_Refuge_Flag")));
         resume.ResumeContent.Profile.AlienExpires = XmlHelpers.GetXmlElementNullableDateTimeValue(jobSeekerElement, "Alien_Expiration_date");
       }

       DisabilityStatus disabilityStatus;
       if (Enum.TryParse(ConvertToInternallId(ExternalLookUpType.DisabilityStatus, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Disability_Code")), true, out disabilityStatus))
       {
         resume.ResumeContent.Profile.DisabilityStatus = disabilityStatus;

         if (disabilityStatus == DisabilityStatus.Disabled)
         {
             resume.ResumeContent.Profile.DisabilityCategoryIds = new List<long?>();
             resume.ResumeContent.Profile.DisabilityCategoryIds.Add(ConvertDisabilityToId(jobSeekerElement, "Chronic_health_Flag"));
             resume.ResumeContent.Profile.DisabilityCategoryIds.Add(ConvertDisabilityToId(jobSeekerElement, "Cognitive_Intellectual_Flag"));
             resume.ResumeContent.Profile.DisabilityCategoryIds.Add(ConvertDisabilityToId(jobSeekerElement, "Hearing_related_Flag"));
             resume.ResumeContent.Profile.DisabilityCategoryIds.Add(ConvertDisabilityToId(jobSeekerElement, "Vision_related_Flag"));
             resume.ResumeContent.Profile.DisabilityCategoryIds.Add(ConvertDisabilityToId(jobSeekerElement, "Learning_disability_Flag"));
             resume.ResumeContent.Profile.DisabilityCategoryIds.Add(ConvertDisabilityToId(jobSeekerElement, "Psychiatric_disability_Flag"));
             resume.ResumeContent.Profile.DisabilityCategoryIds.Add(ConvertDisabilityToId(jobSeekerElement, "Mobility_impairment_Flag"));
             resume.ResumeContent.Profile.DisabilityCategoryIds.RemoveAll(x => x.IsNull());
             if (!resume.ResumeContent.Profile.DisabilityCategoryIds.Any() && Convert.ToBoolean(ConvertToInternallId(ExternalLookUpType.Boolean, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Not disclosed"))))
                 resume.ResumeContent.Profile.DisabilityCategoryIds.Add(ConvertDisabilityToId(jobSeekerElement, "Disability_Other_Flag"));
           //var disabilityCategory = ConvertToInternallId(ExternalLookUpType.DisabilityCategory, XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Disability_category_Code"));
           //if (disabilityCategory.IsNotNullOrEmpty())
           //  resume.ResumeContent.Profile.DisabilityCategoryId = Convert.ToInt32(disabilityCategory);
         }
       }

       // Military service
       resume.ResumeContent.Profile.Veteran = new VeteranInfo { IsVeteran = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "Veteran_status_code").IsIn("2", "3") };
       // No other military data is to be mapped at this point in time

       return resume;
     }

    public UpdateEmployeeStatusResponse UpdateEmployeeStatus(UpdateEmployeeStatusRequest request)
    {
      var response = new UpdateEmployeeStatusResponse(request);

      try
      {
        RequestResponse breadcrumb;
        bool result;
        if (request.Blocked)
        {
          breadcrumb = BeginWebRequest("DeactivateEmployer", request.ExternalEmployeeUserId, request.ExternalStaffUserId);
          result = DeactivateEmployer(request.ExternalEmployeeUserId, request.ExternalStaffUserId);
        }
        else
        {
          breadcrumb = BeginWebRequest("ActivateEmployer", request.ExternalEmployeeUserId, request.ExternalStaffUserId);
          result = ActivateEmployer(request.ExternalEmployeeUserId, request.ExternalStaffUserId.IsNotNullOrEmpty()? request.ExternalStaffUserId : "AutoApproved");
        }
        EndWebRequest(breadcrumb, result.ToString());
        response.IntegrationBreadcrumbs.Add(breadcrumb);
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }
      return response;
    }

    #region Not implemented

    public LogActionResponse LogAction(LogActionRequest request)
    {
      return new LogActionResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AuthenticateJobSeekerResponse AuthenticateJobSeeker(AuthenticateJobSeekerRequest request)
    {
      return new AuthenticateJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveJobMatchesResponse SaveJobMatches(SaveJobMatchesRequest request)
    {
      return new SaveJobMatchesResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AssignAdminToJobSeekerResponse AssignAdminToJobSeeker(AssignAdminToJobSeekerRequest request)
    {
      return new AssignAdminToJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public IsUIClaimantResponse IsUIClaimant(IsUIClaimantRequest request)
    {
      return new IsUIClaimantResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public ChangeUserPasswordResponse ChangeUserPassword(ChangeUserPasswordRequest request)
    {
      return new ChangeUserPasswordResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveJobResponse SaveJob(SaveJobRequest request)
    {
      return new SaveJobResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public ReferJobSeekerResponse ReferJobSeeker(ReferJobSeekerRequest request)
    {
      return new ReferJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SetApplicationStatusResponse SetApplicationStatus(SetApplicationStatusRequest request)
    {
      return new SetApplicationStatusResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveEmployerResponse SaveEmployer(SaveEmployerRequest request)
    {
      return new SaveEmployerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AssignAdminToEmployerResponse AssignAdminToEmployer(AssignAdminToEmployerRequest request)
    {
      return new AssignAdminToEmployerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AssignEmployerActivityResponse AssignEmployerActivity(AssignEmployerActivityRequest request)
    {
      return new AssignEmployerActivityResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveEmployeeResponse SaveEmployee(SaveEmployeeRequest request)
    {
      return new SaveEmployeeResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AuthenticateStaffResponse AuthenticateStaffUser(AuthenticateStaffRequest request)
    {
      return new AuthenticateStaffResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public DisableJobSeekersReportResponse DisableJobSeekersReport(DisableJobSeekersReportRequest request)
    {
      return new DisableJobSeekersReportResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    #endregion

    #endregion

    #region Private methods
    
    /// <summary>
    /// Converts the race to identifier.
    /// </summary>
    /// <param name="jobSeekerElement">The job seeker element.</param>
    /// <param name="elementName">Name of the element.</param>
    /// <returns></returns>
    private long? ConvertRaceToId(XElement jobSeekerElement, string elementName)
    {
      if (XmlHelpers.GetXmlElementStringValue(jobSeekerElement, elementName) == "1")
      {
        long raceId;
        if (long.TryParse(ConvertToInternallId(ExternalLookUpType.Race, elementName), out raceId))
        {
          return raceId;
        }
      }
      return null;
    }

    /// <summary>
    /// Converts the disability category to identifier.
    /// </summary>
    /// <param name="jobSeekerElement">The job seeker element.</param>
    /// <param name="elementName">Name of the element.</param>
    /// <returns></returns>
    private long ConvertDisabilityToId(XElement jobSeekerElement, string elementName)
    {
        if (XmlHelpers.GetXmlElementStringValue(jobSeekerElement, elementName) == "1")
        {
            long disabilityId;
            if (long.TryParse(ConvertToInternallId(ExternalLookUpType.DisabilityCategory, elementName), out disabilityId))
            {
                return disabilityId;
            }
        }
        return default(long);
    }

    /// <summary>
    /// Converts to internal identifier.
    /// </summary>
    /// <param name="lookUpType">Type of the look up.</param>
    /// <param name="externalValue">The external value.</param>
    /// <returns></returns>
    private string ConvertToInternallId(ExternalLookUpType lookUpType, string externalValue)
    {
      var returnVal = _externalLookUpItems.OrderBy(x => x.Id).FirstOrDefault(x => x.ExternalId == externalValue && x.ExternalLookUpType == lookUpType);
      if (returnVal.IsNotNull())
        return returnVal.InternalId;

      return null;
    }

    /// <summary>
    /// Converts to external identifier.
    /// </summary>
    /// <param name="lookUpType">Type of the look up.</param>
    /// <param name="externalValue">The external value.</param>
    /// <returns></returns>
    private string ConvertToExternallId(ExternalLookUpType lookUpType, string internalValue)
    {
      var returnVal = _externalLookUpItems.OrderBy(x => x.Id).FirstOrDefault(x => x.InternalId == internalValue && x.ExternalLookUpType == lookUpType);
      if (returnVal.IsNotNull())
        return returnVal.ExternalId;

      return null;
    }

    private List<long?> ConvertRaceIds (XElement jobSeekerElement)
    {
      var raceIds = new List<long?>
                      {
                        ConvertRaceToId(jobSeekerElement, "Race_American_Indian_Or_Alaskan_Native_Flag"),
                        ConvertRaceToId(jobSeekerElement, "Race_Black_Or_African_American_Flag"),
                        ConvertRaceToId(jobSeekerElement, "Race_White_Flag"),
                        ConvertRaceToId(jobSeekerElement, "Race_Asian_Flag"),
                        ConvertRaceToId(jobSeekerElement, "Race_Hawaiian_Native_Or_Other_Pacific_Islander_Flag"),
                        ConvertRaceToId(jobSeekerElement, "Race_Other_Flag")
                      };
      raceIds.RemoveAll(x => x.IsNull());

      return raceIds;
    }

    private List<long> ConvertDisabilityCategoryIds(XElement jobseekerElement)
    {
        var disabilityIds = new List<long>
                             {
                                 ConvertDisabilityToId(jobseekerElement,"Cognitive_Intellectual_Flag" ),
                                 ConvertDisabilityToId(jobseekerElement,"Hearing_related_Flag" ),
                                 ConvertDisabilityToId(jobseekerElement,"Vision_related_Flag" ),
                                 ConvertDisabilityToId(jobseekerElement,"Learning_disability_Flag" ),
                                 ConvertDisabilityToId(jobseekerElement,"Psychiatric_disability_Flag" ),
                                 ConvertDisabilityToId(jobseekerElement,"Mobility_impairment_Flag" ),
                                 ConvertDisabilityToId(jobseekerElement,"Disability_Other_Flag")
                             };
        disabilityIds.RemoveAll(x => x.IsNull());

        return disabilityIds;
    }

    private RequestResponse BeginWebRequest(string method, params string[] args)
    {
      return new RequestResponse { StartTime = DateTime.Now, OutgoingRequest = String.Join(",", args), Url = method };
    }

    private void EndWebRequest(RequestResponse breadcrumb, string result)
    {
      breadcrumb.EndTime = DateTime.Now;
      breadcrumb.IncomingResponse = result;
    }

    #region Wisconsin focus service calls

    /// <summary>
    /// Gets the jobseeker from client.
    /// </summary>
    /// <param name="jobseekerId">The jobseeker identifier.</param>
    /// <returns></returns>
    private string GetJobseeker(string jobseekerId)
    {
      using (var federatedServiceHelper = new FederatedServiceHelper<IWisconsinClient>
        (
          _clientSettings.FocusServiceEndpoint,
          _clientSettings.ServiceSecurityMode,
          _clientSettings.RelyingParty,
          _clientSettings.TokenServiceEndpoint,
          _clientSettings.Username,
          _clientSettings.Password
        ))
      {
        return federatedServiceHelper.Channel.Getjobseeker(jobseekerId);
      }
    }

		/// <summary>
		/// Adds the self service.
		/// </summary>
		/// <param name="jobseekerID">The jobseeker identifier.</param>
		/// <param name="selfServiceType">Type of the self service.</param>
		/// <returns></returns>
		private bool AddSelfService(string jobseekerID, string selfServiceType)
		{
			using (var federatedServiceHelper = new FederatedServiceHelper<IWisconsinClient>
				(
					_clientSettings.FocusServiceEndpoint,
					_clientSettings.ServiceSecurityMode,
					_clientSettings.RelyingParty,
					_clientSettings.TokenServiceEndpoint,
					_clientSettings.Username,
					_clientSettings.Password
				))
			{
				return federatedServiceHelper.Channel.AddSelfService(jobseekerID, selfServiceType);
			}
		}

		/// <summary>
		/// Maintains the resume completion flag.
		/// </summary>
		/// <param name="actionerId">The actioner identifier.</param>
		/// <param name="personId">The person identifier.</param>
		/// <param name="wiCustKey">The wi customer key.</param>
		/// <returns></returns>
		private string MaintainResumeCompletionFlag(string actionerId, string personId, string wiCustKey)
		{
			using (var federatedServiceHelper = new FederatedServiceHelper<IWisconsinClient>
				(
					_clientSettings.FocusServiceEndpoint,
					_clientSettings.ServiceSecurityMode,
					_clientSettings.RelyingParty,
					_clientSettings.TokenServiceEndpoint,
					_clientSettings.Username,
					_clientSettings.Password
				))
			{
				return federatedServiceHelper.Channel.MaintainResumeCompletionFlag(actionerId, personId, wiCustKey);
			}
		}

		/// <summary>
		/// Updates the contact information.
		/// </summary>
		/// <param name="jobSeekerId">The job seeker identifier.</param>
		/// <param name="ethnicHispanicFlag">The ethnic hispanic flag.</param>
		/// <param name="raceAmericanIndianOrAlaskanNativeFlag">The race american indian or alaskan native flag.</param>
		/// <param name="raceAsianFlag">The race asian flag.</param>
		/// <param name="raceBlackOrAfricanAmericanFlag">The race black or african american flag.</param>
		/// <param name="raceHawaiianNativeOrOtherPacificIslanderFlag">The race hawaiian native or other pacific islander flag.</param>
		/// <param name="raceWhiteFlag">The race white flag.</param>
		/// <param name="raceOtherFlag">The race other flag.</param>
		/// <param name="educationStatusCode">The education status code.</param>
		/// <param name="schoolGradeCode">The school grade code.</param>
		/// <param name="currentEmploymentStatusCode">The current employment status code.</param>
		/// <returns></returns>
		private bool UpdateContactInformation(string jobSeekerId, string ethnicHispanicFlag, string raceAmericanIndianOrAlaskanNativeFlag, string raceAsianFlag, string raceBlackOrAfricanAmericanFlag, string raceHawaiianNativeOrOtherPacificIslanderFlag, string raceWhiteFlag, string raceOtherFlag, string educationStatusCode, string schoolGradeCode, string currentEmploymentStatusCode)
			{
			using (var federatedServiceHelper = new FederatedServiceHelper<IWisconsinClient>
				(
					_clientSettings.FocusServiceEndpoint,
					_clientSettings.ServiceSecurityMode,
					_clientSettings.RelyingParty,
					_clientSettings.TokenServiceEndpoint,
					_clientSettings.Username,
					_clientSettings.Password
				))
			{
				return federatedServiceHelper.Channel.UpdateContactInformation(jobSeekerId, ethnicHispanicFlag, raceAmericanIndianOrAlaskanNativeFlag, raceAsianFlag, raceBlackOrAfricanAmericanFlag, raceHawaiianNativeOrOtherPacificIslanderFlag, raceWhiteFlag, raceOtherFlag, educationStatusCode, schoolGradeCode, currentEmploymentStatusCode);
			}
		}

    private bool DeactivateEmployer(string employerID, string staffID)
    {
      using (var federatedServiceHelper = new FederatedServiceHelper<IWisconsinClient>
        (
          _clientSettings.FocusServiceEndpoint,
          _clientSettings.ServiceSecurityMode,
          _clientSettings.RelyingParty,
          _clientSettings.TokenServiceEndpoint,
          _clientSettings.Username,
          _clientSettings.Password
        ))
      {
        return federatedServiceHelper.Channel.DeactivateEmployer(employerID, staffID);
      }
    }

    private bool ActivateEmployer(string employerID, string staffID)
    {
      using (var federatedServiceHelper = new FederatedServiceHelper<IWisconsinClient>
        (
          _clientSettings.FocusServiceEndpoint,
          _clientSettings.ServiceSecurityMode,
          _clientSettings.RelyingParty,
          _clientSettings.TokenServiceEndpoint,
          _clientSettings.Username,
          _clientSettings.Password
        ))
      {
        return federatedServiceHelper.Channel.ActivateEmployer(employerID, staffID);
      }
    }

    /// <summary>
    /// Send the job seeker resume stats
    /// </summary>
    /// <param name="jobSeekerId">The job seeker identifier.</param>
    /// <param name="completedResumeCount">The completed resume count.</param>
    /// <param name="searchableResumeCount">The searchable resume count.</param>
    /// <returns></returns>
    private bool CustomerResumeStatus(string jobSeekerId, string completedResumeCount, string searchableResumeCount)
    {
      using (var federatedServiceHelper = new FederatedServiceHelper<IWisconsinClient>
        (
          _clientSettings.FocusServiceEndpoint,
          _clientSettings.ServiceSecurityMode,
          _clientSettings.RelyingParty,
          _clientSettings.TokenServiceEndpoint,
          _clientSettings.Username,
          _clientSettings.Password
        ))
      {
        return federatedServiceHelper.Channel.CustomerResumeStatus(jobSeekerId, completedResumeCount, searchableResumeCount);
      }
    }

		#endregion

		#region Wisconsin conversion service calls

		/// <summary>
		/// Gets the job from client.
		/// </summary>
		/// <param name="jobId">The job identifier.</param>
		/// <returns></returns>
		private string GetJobOrdData(string jobId)
		{
			using (var federatedServiceHelper = new FederatedServiceHelper<IWisconsinClient>
				(
					_clientSettings.ConversionServiceEndpoint,
					_clientSettings.ServiceSecurityMode,
					_clientSettings.RelyingParty,
					_clientSettings.TokenServiceEndpoint,
					_clientSettings.Username,
					_clientSettings.Password
				))
			{
				return federatedServiceHelper.Channel.GetJobOrdData(jobId);
			}
		}

		/// <summary>
		/// Gets the emp job ords.
		/// </summary>
		/// <param name="ErProfileNo">The er profile no.</param>
		/// <returns></returns>
		private string GetEmpJobOrds(string ErProfileNo)
		{
			using (var federatedServiceHelper = new FederatedServiceHelper<IWisconsinClient>
				(
					_clientSettings.ConversionServiceEndpoint,
					_clientSettings.ServiceSecurityMode,
					_clientSettings.RelyingParty,
					_clientSettings.TokenServiceEndpoint,
					_clientSettings.Username,
					_clientSettings.Password
				))
			{
				return federatedServiceHelper.Channel.GetEmpJobOrds(ErProfileNo);
			}
		}

		/// <summary>
		/// Gets the er profile.
		/// </summary>
		/// <param name="ErProfileNo">The er profile no.</param>
		/// <returns></returns>
		private string GetErProfile(string ErProfileNo)
		{
			using (var federatedServiceHelper = new FederatedServiceHelper<IWisconsinClient>
				(
					_clientSettings.ConversionServiceEndpoint,
					_clientSettings.ServiceSecurityMode,
					_clientSettings.RelyingParty,
					_clientSettings.TokenServiceEndpoint,
					_clientSettings.Username,
					_clientSettings.Password
				))
			{
				return federatedServiceHelper.Channel.GetErProfile(ErProfileNo);
			}
		}

		/// <summary>
		/// Gets the er sites.
		/// </summary>
		/// <param name="ErProfileNo">The er profile no.</param>
		/// <returns></returns>
		private string GetErSites(string ErProfileNo)
		{
			using (var federatedServiceHelper = new FederatedServiceHelper<IWisconsinClient>
				(
					_clientSettings.ConversionServiceEndpoint,
					_clientSettings.ServiceSecurityMode,
					_clientSettings.RelyingParty,
					_clientSettings.TokenServiceEndpoint,
					_clientSettings.Username,
					_clientSettings.Password
				))
			{
				return federatedServiceHelper.Channel.GetErSites(ErProfileNo);
			}
		}

		/// <summary>
		/// Gets the site.
		/// </summary>
		/// <param name="SiteLocNo">The site loc no.</param>
		/// <returns></returns>
		private string GetSite(string SiteLocNo)
		{
			using (var federatedServiceHelper = new FederatedServiceHelper<IWisconsinClient>
				(
					_clientSettings.ConversionServiceEndpoint,
					_clientSettings.ServiceSecurityMode,
					_clientSettings.RelyingParty,
					_clientSettings.TokenServiceEndpoint,
					_clientSettings.Username,
					_clientSettings.Password
				))
			{
				return federatedServiceHelper.Channel.GetSite(SiteLocNo);
			}
		}

		/// <summary>
		/// Gets the employer contact.
		/// </summary>
		/// <param name="ContactNo">The contact no.</param>
		/// <returns></returns>
		private string GetEmployerContact(string ContactNo)
		{
			using (var federatedServiceHelper = new FederatedServiceHelper<IWisconsinClient>
				(
					_clientSettings.ConversionServiceEndpoint,
					_clientSettings.ServiceSecurityMode,
					_clientSettings.RelyingParty,
					_clientSettings.TokenServiceEndpoint,
					_clientSettings.Username,
					_clientSettings.Password
				))
			{
				return federatedServiceHelper.Channel.GetEmployerContact(ContactNo);
			}
		}

		/// <summary>
		/// Gets the employer contacts.
		/// </summary>
		/// <param name="ErProfileNo">The er profile no.</param>
		/// <returns></returns>
		private string GetEmployerContacts(string ErProfileNo)
		{
			using (var federatedServiceHelper = new FederatedServiceHelper<IWisconsinClient>
				(
					_clientSettings.ConversionServiceEndpoint,
					_clientSettings.ServiceSecurityMode,
					_clientSettings.RelyingParty,
					_clientSettings.TokenServiceEndpoint,
					_clientSettings.Username,
					_clientSettings.Password
				))
			{
				return federatedServiceHelper.Channel.GetEmployerContacts(ErProfileNo);
			}
		}

		/// <summary>
		/// Gets the employer.
		/// </summary>
		/// <param name="ErProfileNo">The er profile no.</param>
		/// <returns></returns>
		private string GetEmployer(string ErProfileNo)
		{
			using (var federatedServiceHelper = new FederatedServiceHelper<IWisconsinClient>
				(
					_clientSettings.ConversionServiceEndpoint,
					_clientSettings.ServiceSecurityMode,
					_clientSettings.RelyingParty,
					_clientSettings.TokenServiceEndpoint,
					_clientSettings.Username,
					_clientSettings.Password
				))
			{
				return federatedServiceHelper.Channel.GetEmployer(ErProfileNo);
			}
		}

		/// <summary>
		/// Gets the resume.
		/// </summary>
		/// <param name="resumeID">The resume identifier.</param>
		/// <returns></returns>
		private UploadedResume GetResume(string resumeID)
		{
			using (var federatedServiceHelper = new FederatedServiceHelper<IWisconsinClient>
				(
					_clientSettings.ConversionServiceEndpoint,
					_clientSettings.ServiceSecurityMode,
					_clientSettings.RelyingParty,
					_clientSettings.TokenServiceEndpoint,
					_clientSettings.Username,
					_clientSettings.Password
				))
			{
				return federatedServiceHelper.Channel.GetResume(resumeID);
			}
		}

		#endregion

		#endregion


		#region ExampleXml

		#region Example employer and artifact xml

		private const string ExampleEmployerXml = @"<EmployerDataset>
	<EmployerProfile>
		<EmployerID>362490</EmployerID>
		<LegalName>ICE CREAM INC 1</LegalName>
		<TradeName>ICE CREAM INC 1</TradeName>
		<OwnershipCode>5</OwnershipCode>
		<SizeCode>A </SizeCode>
		<SizeCodeReferenceYear>04 </SizeCodeReferenceYear>
		<SizeCodeReferenceQuarter>4</SizeCodeReferenceQuarter>
		<FeinCode>569874326</FeinCode>
		<UIAccountRootNumber>9999999999</UIAccountRootNumber>
		<WebAddress />
		<MultipleLocationsFlag>N</MultipleLocationsFlag>
		<EmployerDescription>make lots of yummy stuff..</EmployerDescription>
		<ProfitNonProfitGovernmentIndicator>P</ProfitNonProfitGovernmentIndicator>
		<CreatedDate>7/18/2006 4:20:59 PM</CreatedDate>
		<CreatedBy>EXPOE202</CreatedBy>
		<UpdatedDate>6/12/2013 7:42:59 AM</UpdatedDate>
		<UpdatedBy>DWD\POTOCSU</UpdatedBy>
		<DeletedBy />
		<DeletedDate />
		<WorksiteTradeName>ICE CREAM INC.</WorksiteTradeName>
		<FederalContractorFlag>Y</FederalContractorFlag>
	</EmployerProfile>
</EmployerDataset>
";

    private const string ExampleEmployerBuXml = @"<ErSites>
	<SITE>
		<SITE_LOC_NUM>295643</SITE_LOC_NUM>
	</SITE>
	<SITE>
		<SITE_LOC_NUM>294595</SITE_LOC_NUM>
	</SITE>
	<SITE>
		<SITE_LOC_NUM>295139</SITE_LOC_NUM>
	</SITE>
	<SITE>
		<SITE_LOC_NUM>295382</SITE_LOC_NUM>
	</SITE>
	<SITE>
		<SITE_LOC_NUM>295342</SITE_LOC_NUM>
	</SITE>
	<SITE>
		<SITE_LOC_NUM>295362</SITE_LOC_NUM>
	</SITE>
	<SITE>
		<SITE_LOC_NUM>295138</SITE_LOC_NUM>
	</SITE>
	<SITE>
		<SITE_LOC_NUM>295242</SITE_LOC_NUM>
	</SITE>
	<SITE>
		<SITE_LOC_NUM>295363</SITE_LOC_NUM>
	</SITE>
	<SITE>
		<SITE_LOC_NUM>295462</SITE_LOC_NUM>
	</SITE>
	<SITE>
		<SITE_LOC_NUM>295283</SITE_LOC_NUM>
	</SITE>
	<SITE>
		<SITE_LOC_NUM>295263</SITE_LOC_NUM>
	</SITE>
	<SITE>
		<SITE_LOC_NUM>295343</SITE_LOC_NUM>
	</SITE>
	<SITE>
		<SITE_LOC_NUM>295262</SITE_LOC_NUM>
	</SITE>
</ErSites>
";
    private const string ExampleBusinessUnitXml = @"<SiteDataset>
	<SiteLocation>
		<SiteID>295290</SiteID>
		<EmployerID>362490</EmployerID>
		<UIAccountRootNumber>9999999999</UIAccountRootNumber>
		<UIAccountSuffixNumber />
		<TradeName>ICE CREAM INC</TradeName>
		<LocationAddress1>A1</LocationAddress1>
		<LocationAddress2>B2</LocationAddress2>
		<LocationCity>C3</LocationCity>
		<LocationState>IL</LocationState>
		<LocationZip>12346</LocationZip>
		<LocationZipExtension>9875</LocationZipExtension>
		<MailingAddress1>1234 ELM AVE</MailingAddress1>
		<MailingAddress2>SUITE A</MailingAddress2>
		<MailingCity>CURTISS</MailingCity>
		<MailingState>WI</MailingState>
		<MailingZip>54422</MailingZip>
		<MailingZipExtension />
		<WebSiteAddress />
		<PhoneAreaCode>112</PhoneAreaCode>
		<PhoneNumber>2233334</PhoneNumber>
		<FaxAreaCode>445</FaxAreaCode>
		<FaxNumber>5566667</FaxNumber>
		<TownshipCode />
		<CountyCode>119</CountyCode>
		<MSACode />
		<WDACode>007</WDACode>
		<MailToCorporateHeadquartersFlag />
		<CorporateHeadquartersFlag>N</CorporateHeadquartersFlag>
		<InformationReleaseFlag />
		<CreatedDate>5/13/2011 8:27:03 AM</CreatedDate>
		<CreatedBy>kit1225</CreatedBy>
		<UpdatedDate>10/9/2014 10:43:00 AM</UpdatedDate>
		<UpdatedBy>DWD\HELLEPA</UpdatedBy>
		<InactiveSiteLocationFlag>2</InactiveSiteLocationFlag>
		<LiabilityEndDate />
		<StopMailFlag>Y</StopMailFlag>
		<InactiveSiteLocationDate>10/9/2014 10:43:00 AM</InactiveSiteLocationDate>
		<DeletedBy />
		<DeletedDate />
		<WorksiteTradeName>STRAWS AND SPOONS</WorksiteTradeName>
		<WorksitePhoneAreaCode>715</WorksitePhoneAreaCode>
		<WorksitePhoneNumber>6781111</WorksitePhoneNumber>
		<WorksiteFaxAreaCode />
		<WorksiteFaxNumber />
		<WorksiteStopMailFlag>Y</WorksiteStopMailFlag>
		<SiteLocationDescription>cccxc</SiteLocationDescription>
	</SiteLocation>
	<SiteGeneralInfo>
		<SiteID>295290</SiteID>
		<SizeCode />
		<SizeCodeReferenceQuarter />
		<SizeCodeReferenceYear />
		<FunctionCode>0</FunctionCode>
		<SICCode />
		<NAICSCode>325211</NAICSCode>
		<SalesVolume />
		<ProductServiceDesc>make the straws and spoons for the shakes and malts</ProductServiceDesc>
		<OperationHoursText />
		<SalesMarketingJobsPercent>50 </SalesMarketingJobsPercent>
		<ServiceJobsPercent />
		<SkilledTradeJobsPercent />
		<ProductionJobsPercent />
		<OtherJobsPercent />
		<FulltimeJobsPercent />
		<ParttimeJobsPercent />
		<TempJobsPercent />
		<ProfessionalJobsPercent>50 </ProfessionalJobsPercent>
		<ClericalJobsPercent>50 </ClericalJobsPercent>
		<NumberEmployees />
		<NumberEmployeesUpdatedDate />
		<AffirmativeActionFlag />
		<GovernmentContractsFlag>Y</GovernmentContractsFlag>
		<CreatedDate>6/20/2012 1:21:11 PM</CreatedDate>
		<CreatedBy>DWD\POTOCSU</CreatedBy>
		<UpdatedDate />
		<UpdatedBy />
		<StaffingAgencyFlag>Y</StaffingAgencyFlag>
		<CommunityDevelopmentZone>Y</CommunityDevelopmentZone>
		<EmpowermentZone>Y</EmpowermentZone>
		<NCRCLetterOnFileFlag>Y</NCRCLetterOnFileFlag>
		<NCRCChampionFlag>Y</NCRCChampionFlag>
	</SiteGeneralInfo>
	<SiteBenefitInfo>
		<SiteID>295282</SiteID>
		<OtherBenefitDescription>this is other benefits text</OtherBenefitDescription>
		<BenefitComments>comments for benefits</BenefitComments>
	</SiteBenefitInfo>
	<SiteBenefit>
		<SiteID>295282</SiteID>
		<BenefitNumber>2</BenefitNumber>
	</SiteBenefit>
	<SiteBenefit>
		<SiteID>295282</SiteID>
		<BenefitNumber>3</BenefitNumber>
	</SiteBenefit>
	<SiteBenefit>
		<SiteID>295282</SiteID>
		<BenefitNumber>4</BenefitNumber>
	</SiteBenefit>
	<SiteBenefit>
		<SiteID>295282</SiteID>
		<BenefitNumber>7</BenefitNumber>
	</SiteBenefit>
	<SiteBenefit>
		<SiteID>295282</SiteID>
		<BenefitNumber>12</BenefitNumber>
	</SiteBenefit>
	<SiteBenefit>
		<SiteID>295282</SiteID>
		<BenefitNumber>14</BenefitNumber>
	</SiteBenefit>
	<SiteBenefit>
		<SiteID>295282</SiteID>
		<BenefitNumber>16</BenefitNumber>
	</SiteBenefit>
	<SiteBenefit>
		<SiteID>295282</SiteID>
		<BenefitNumber>17</BenefitNumber>
	</SiteBenefit>
	<SiteBenefit>
		<SiteID>295282</SiteID>
		<BenefitNumber>18</BenefitNumber>
	</SiteBenefit>
	<SiteBenefit>
		<SiteID>295282</SiteID>
		<BenefitNumber>20</BenefitNumber>
	</SiteBenefit>
	<SiteBenefit>
		<SiteID>295282</SiteID>
		<BenefitNumber>21</BenefitNumber>
	</SiteBenefit>
	<SiteBenefit>
		<SiteID>295282</SiteID>
		<BenefitNumber>22</BenefitNumber>
	</SiteBenefit>
	<SiteBenefit>
		<SiteID>295282</SiteID>
		<BenefitNumber>23</BenefitNumber>
	</SiteBenefit>
	<SiteBenefit>
		<SiteID>295282</SiteID>
		<BenefitNumber>24</BenefitNumber>
	</SiteBenefit>
	<SiteBenefit>
		<SiteID>295282</SiteID>
		<BenefitNumber>25</BenefitNumber>
	</SiteBenefit>
	<SiteBenefit>
		<SiteID>295282</SiteID>
		<BenefitNumber>34</BenefitNumber>
	</SiteBenefit>
	<SiteBenefit>
		<SiteID>295282</SiteID>
		<BenefitNumber>99</BenefitNumber>
	</SiteBenefit>
	<SiteApplicationInstructionMethods>
		<SiteID>295282</SiteID>
		<APP_MTHD_CD>01</APP_MTHD_CD>
		<APP_ISTR_TXT>1234 main st Waunakee, wi</APP_ISTR_TXT>
	</SiteApplicationInstructionMethods>
	<SiteApplicationInstructionMethods>
		<SiteID>295282</SiteID>
		<APP_MTHD_CD>04</APP_MTHD_CD>
		<APP_ISTR_TXT>www.applyhere.com</APP_ISTR_TXT>
	</SiteApplicationInstructionMethods>
	<SiteApplicationInstructionMethods>
		<SiteID>295282</SiteID>
		<APP_MTHD_CD>99</APP_MTHD_CD>
		<APP_ISTR_TXT>call between 9 and 9:15</APP_ISTR_TXT>
	</SiteApplicationInstructionMethods>
</SiteDataset>";

    private const string ExampleEmployerEmployeeXml = @"<EMPLOYER_CONTACTS>
	<CONTACT>
		<ER_CNTC_NUM>70536</ER_CNTC_NUM>
	</CONTACT>
	<CONTACT>
		<ER_CNTC_NUM>71419</ER_CNTC_NUM>
	</CONTACT>
	<CONTACT>
		<ER_CNTC_NUM>71239</ER_CNTC_NUM>
	</CONTACT>
	<CONTACT>
		<ER_CNTC_NUM>71439</ER_CNTC_NUM>
	</CONTACT>
	<CONTACT>
		<ER_CNTC_NUM>71422</ER_CNTC_NUM>
	</CONTACT>
	<CONTACT>
		<ER_CNTC_NUM>71421</ER_CNTC_NUM>
	</CONTACT>
	<CONTACT>
		<ER_CNTC_NUM>71459</ER_CNTC_NUM>
	</CONTACT>
</EMPLOYER_CONTACTS>";

    private const string ExampleEmployeeXml = @"<ContactDataset>
	<EmployerContactInfo>
		<ContactID>70590</ContactID>
		<RoleNumber>17</RoleNumber>
		<LastName>CANNY 1</LastName>
		<FirstName>KIT</FirstName>
		<MiddleInitial>K</MiddleInitial>
		<Salutation>MISS</Salutation>
		<LocationAddress1>1234 GOLF COURSE RD</LocationAddress1>
		<LocationAddress2>SUITE 123</LocationAddress2>
		<LocationCity>SHERWOOD</LocationCity>
		<LocationState>WI</LocationState>
		<LocationZip>53703</LocationZip>
		<LocationZipExtension />
		<PhoneAreaCode>608</PhoneAreaCode>
		<PhoneNumber>2611234</PhoneNumber>
		<PhoneExtension>12</PhoneExtension>
		<FaxAreaCode>608</FaxAreaCode>
		<FaxNumber>1231234</FaxNumber>
		<FaxExtension>12345</FaxExtension>
		<EmailAddress>sue.potocnik@dwd.wisconsin.govx</EmailAddress>
		<ResponsibilityAreaText>This is what I want to Say</ResponsibilityAreaText>
		<Comments>No Comment at this time</Comments>
		<CreatedDate>1/22/2009 8:02:51 AM</CreatedDate>
		<CreatedBy>DWD\POTOCSU</CreatedBy>
		<UpdatedDate>8/20/2014 7:29:21 AM</UpdatedDate>
		<UpdatedBy>EMPLOYER</UpdatedBy>
		<DeletedDate />
		<DeletedBy />
		<JobOrderContactFlag>Y</JobOrderContactFlag>
		<InternetAcctInactiveTimestamp>8/19/2014 12:00:00 AM</InternetAcctInactiveTimestamp>
		<InternetAccountAccessTimestamp>10/8/2013 9:58:29 AM</InternetAccountAccessTimestamp>
		<InternetAccountCreationTimestamp>1/22/2009 8:02:51 AM</InternetAccountCreationTimestamp>
		<InternetAccountCreatedByNumber>8354</InternetAccountCreatedByNumber>
		<SiteID />
		<JobCenterNumber>549</JobCenterNumber>
		<FIPSStateCountySurrogateKey>61</FIPSStateCountySurrogateKey>
		<CellPhoneAreaCode>608</CellPhoneAreaCode>
		<CellPhoneNumber>5551234</CellPhoneNumber>
		<PreferredMethodOfContactCode>01</PreferredMethodOfContactCode>
		<MayWeContactYouFlag>Y</MayWeContactYouFlag>
		<ReceiveJobOrderExpirationNoticeFlag>N</ReceiveJobOrderExpirationNoticeFlag>
		<ReceiveJobOrderApprovalNoticeFlag>N</ReceiveJobOrderApprovalNoticeFlag>
	</EmployerContactInfo>
	<SiteContact>
		<ContactID>70536</ContactID>
		<SiteID>294595</SiteID>
	</SiteContact>
	<SiteContact>
		<ContactID>70536</ContactID>
		<SiteID>295242</SiteID>
	</SiteContact>
	<SiteContact>
		<ContactID>70536</ContactID>
		<SiteID>295263</SiteID>
	</SiteContact>
	<SiteContact>
		<ContactID>70536</ContactID>
		<SiteID>295362</SiteID>
	</SiteContact>
</ContactDataset>";

#endregion

    #region Example JobXml

    private const string ExampleJobXml = @"<JobOrderDataset>
  <Job_Order>
    <JobOrderNumber>000054597</JobOrderNumber>
    <EmployerProfileNumber>191b4fe4-ba64-425d-9dec-51cfa388eaeb</EmployerProfileNumber>
    <OrderTakerJobCenterStaffNumber>10766</OrderTakerJobCenterStaffNumber>
    <JobOrderStatusDescription>STAFF INCOMPLETE</JobOrderStatusDescription>
    <EmployerLegalName>VJC, INC</EmployerLegalName>
    <EmployerTradeName>VJC TEST EMPLOYER</EmployerTradeName>
    <SiteLocationNumber>295290</SiteLocationNumber>
    <SiteTradeName>VJC TEST EMPLOYER</SiteTradeName>
    <SiteLocationLine1Address>AAAAA</SiteLocationLine1Address>
    <SiteLocationLine2Address />
    <SiteLocationCityName>MADISON</SiteLocationCityName>
    <SiteLocationStateCode>WI</SiteLocationStateCode>
    <SiteLocationZipCode>53713</SiteLocationZipCode>
    <SiteLocationZipExtensionCode />
    <SiteLocationCountyCode>025</SiteLocationCountyCode>
    <SiteLocationTelephoneAreaCode>123</SiteLocationTelephoneAreaCode>
    <EmployerContactNumber>70717</EmployerContactNumber>
    <SiteLocationTelephoneNumber>4567890</SiteLocationTelephoneNumber>
    <WebSiteAddress />
    <JobCenterNumber>32</JobCenterNumber>
    <JobTitle>Missing Title</JobTitle>
    <UnemploymentInsuranceAccountRootNumber />
    <UnemploymentInsuranceAccountSuffixNumber />
    <OrderCategoryCode>A</OrderCategoryCode>
    <OwnershipCode />
    <VeteransReleaseFlag>0</VeteransReleaseFlag>
    <NationalPostingFlag />
    <JobOpeningsQuantity>100</JobOpeningsQuantity>
    <StaffReferralCount />
    <SelfReferralCount />
    <OrderSourceType>Employer</OrderSourceType>
    <EmployerIdentificationDisplayedFlag>Y</EmployerIdentificationDisplayedFlag>
    <EmployerContactIdentificationDisplayedFlag />
    <WorkweekType>Part-Time Temporary</WorkweekType>
    <ApprenticeshipPositionFlag>N</ApprenticeshipPositionFlag>
    <EmploymentAgencyFlag>N</EmploymentAgencyFlag>
    <ValidityGeneralizationJobFamilyType />
    <Min_Work_Hrs_Qty>1</Min_Work_Hrs_Qty>
    <Max_Work_Hrs_Qty />
    <MinimumPayAmount>10</MinimumPayAmount>
    <MinimumPayTimeUnitType>Per Hour</MinimumPayTimeUnitType>
    <MaximumPayAmount />
    <MaximumPayTimeUnitType />
    <WorkSiteLocationText />
    <DutiesResponsibilitiesText>no</DutiesResponsibilitiesText>
    <EducationLevelCode />
    <EducationLevelPriorityType>None</EducationLevelPriorityType>
    <EducationTrainingLevelText />
    <LicenseCertificationText />
    <QualificationsText />
    <ExperienceText />
    <DriversLicenseFlag>N</DriversLicenseFlag>
    <VehiclePriorityType>None</VehiclePriorityType>
    <PublicTransportationFlag>N</PublicTransportationFlag>
    <PublicTransportationText />
    <MinimumAge />
    <MaximumAge />
    <AgePriorityType>None</AgePriorityType>
    <UpdateTimestamp />
    <UpdateByNumber>10766</UpdateByNumber>
    <JobDurationText>1-3 Days</JobDurationText>
    <TestingType />
    <AffirmativeActionFlag />
    <FederalContractorFlag>N</FederalContractorFlag>
    <JobOrderDate>8/25/2014 12:00:00 AM</JobOrderDate>
    <JobOrderRemovalDate>9/24/2014 12:00:00 AM</JobOrderRemovalDate>
    <RemovedDate />
    <LastEmployerContactDate />
    <VeteransReleaseSetDate>5/30/2012 2:23:33 PM</VeteransReleaseSetDate>
    <TemporaryJobEndDate />
    <BenefitsCommentText />
    <StaffCommentText />
    <EmployerCommentText />
    <EmployerAddedSiteFlag />
    <EmployerUpdatedSiteFlag />
    <NorthAmericanIndustryClassificationSystemCode>111150</NorthAmericanIndustryClassificationSystemCode>
    <NationalPostingDate />
    <OrderResponsibilityJobCenterStaffNumber>10766</OrderResponsibilityJobCenterStaffNumber>
    <SetVeteranReleaseJobCenterStaffNumber />
    <InProgressJobCenterStaffNumber />
    <CreateTimestamp>8/25/2014 12:38:02 PM</CreateTimestamp>
    <UpdateCode />
    <OccupationalInformationNetworkCode>47-5049.00</OccupationalInformationNetworkCode>
    <CreateCode>S</CreateCode>
    <AutomatedMatchingSystemJobOrderNumber />
    <RemoveCode>B</RemoveCode>
    <DictionaryOfOccupationTitlesCode />
    <AdditionalCompensationText />
    <MandatoryStatementMilitaryRecruitmentFlag>N</MandatoryStatementMilitaryRecruitmentFlag>
    <MandatoryStatementLaborDisputeFlag>N</MandatoryStatementLaborDisputeFlag>
    <MandatoryStatementCommissionPieceworkJobFlag>N</MandatoryStatementCommissionPieceworkJobFlag>
    <MandatoryStatementTippedOccupationsFlag>N</MandatoryStatementTippedOccupationsFlag>
    <MandatoryStatementPrivateEmploymentAgencyFlag />
    <MandatoryStatementPrivateEmploymentAgencyCode />
    <FederalEmployerIdentificationNumber />
    <Adtl_Work_Hrs_Txt />
    <MileageFlag>N</MileageFlag>
    <MandatoryStatementNativeAmericanTribalBusinessFlag>N</MandatoryStatementNativeAmericanTribalBusinessFlag>
    <JobOrderExpirationNoticeFlag>Y</JobOrderExpirationNoticeFlag>
    <ExperienceAndQualificationsText />
    <AgriculturalClearanceFlag>N</AgriculturalClearanceFlag>
    <MandatoryStatementPreemploymentDrugScreeningFlag>N</MandatoryStatementPreemploymentDrugScreeningFlag>
    <MandatoryStatementBackgroundCheckFlag>N</MandatoryStatementBackgroundCheckFlag>
    <WorkSiteLine1Address>AAAAA</WorkSiteLine1Address>
    <WorkSiteLine2Address />
    <WorkSiteCityName>MADISON</WorkSiteCityName>
    <WorkSiteStateCode>55</WorkSiteStateCode>
    <WorkSiteZipCode>53713</WorkSiteZipCode>
    <HasPhysicalWorkSiteFlag>Y</HasPhysicalWorkSiteFlag>
    <TemporaryJobStartDate />
    <AmericanRecoveryAndReinvestmentActFlag />
    <NationalCareerReadinessCertificatePreferenceCode />
    <NationalCareerReadinessCertificateCertificationLevelCode />
    <OverrideJobOrderRemovalDateFlag>N</OverrideJobOrderRemovalDateFlag>
    <SpecialVeteransPositionFlag>N</SpecialVeteransPositionFlag>
    <WisconsinHousingAndEconomicDevelopmentAuthorityApplicationNumber />
  </Job_Order>
  <SiteLocation>
    <SiteID>294795</SiteID>
    <EmployerID>422303</EmployerID>
    <TradeName>VJC TEST EMPLOYER</TradeName>
    <LocationAddress1>T7</LocationAddress1>
    <LocationAddress2>T8</LocationAddress2>
    <LocationCity>TE6</LocationCity>
    <LocationState>MN</LocationState>
    <LocationZip>54211</LocationZip>
    <LocationZipExtension>8413</LocationZipExtension>
    <MailingAddress1>AAAAA</MailingAddress1>
    <MailingAddress2 />
    <MailingCity>MADISON</MailingCity>
    <MailingState>WI</MailingState>
    <MailingZip>53713</MailingZip>
    <MailingZipExtension />
    <WebSiteAddress />
    <PhoneAreaCode>123</PhoneAreaCode>
    <PhoneNumber>4567890</PhoneNumber>
    <FaxAreaCode>222</FaxAreaCode>
    <FaxNumber>3335555</FaxNumber>
    <TownshipCode />
    <CountyCode>025</CountyCode>
    <MSACode>4720</MSACode>
    <WDACode>010</WDACode>
    <MailToCorporateHeadquartersFlag />
    <CorporateHeadquartersFlag>N</CorporateHeadquartersFlag>
    <InformationReleaseFlag />
    <CreatedDate>6/1/2009 2:43:24 PM</CreatedDate>
    <CreatedBy>vjcemployer</CreatedBy>
    <UpdatedDate>8/28/2012 12:10:16 PM</UpdatedDate>
    <UpdatedBy>DWD\LAMBEJO</UpdatedBy>
    <InactiveSiteLocationFlag>1</InactiveSiteLocationFlag>
    <LiabilityEndDate />
    <StopMailFlag>N</StopMailFlag>
    <InactiveSiteLocationDate />
    <DeletedBy />
    <DeletedDate />
    <WorksiteTradeName>VJC TEST EMPLOYER</WorksiteTradeName>
    <WorksitePhoneAreaCode>123</WorksitePhoneAreaCode>
    <WorksitePhoneNumber>4567890</WorksitePhoneNumber>
    <WorksiteFaxAreaCode />
    <WorksiteFaxNumber />
    <WorksiteStopMailFlag>N</WorksiteStopMailFlag>
    <SiteLocationDescription>This is VJC TEST EMPLOYER</SiteLocationDescription>
  </SiteLocation>
  <Job_Order_application_Method>
    <JobOrderApplicationMethodSurrogateKey>97949</JobOrderApplicationMethodSurrogateKey>
    <JobOrderNumber>000054596</JobOrderNumber>
    <ApplicationMethodType>E-Mail a Résumé</ApplicationMethodType>
    <ApplicationMethodCode>03</ApplicationMethodCode>
    <ApplicationInstructionText>test@testing.com</ApplicationInstructionText>
    <CreateUserName>DWD\GOLKOVI</CreateUserName>
    <UpdateTimestamp />
    <CreateTimestamp>8/25/2014 12:38:02 PM</CreateTimestamp>
    <UpdateUserName />
  </Job_Order_application_Method>
  <Job_Order_application_Method>
    <JobOrderApplicationMethodSurrogateKey>97950</JobOrderApplicationMethodSurrogateKey>
    <JobOrderNumber>000054596</JobOrderNumber>
    <ApplicationMethodType>E-Mail a Work Application</ApplicationMethodType>
    <ApplicationMethodCode>08</ApplicationMethodCode>
    <ApplicationInstructionText>newtest@newtest.com</ApplicationInstructionText>
    <CreateUserName>DWD\GOLKOVI</CreateUserName>
    <UpdateTimestamp />
    <CreateTimestamp>8/25/2014 12:38:02 PM</CreateTimestamp>
    <UpdateUserName />
  </Job_Order_application_Method>
  <Job_Order_Benefit>
    <JobOrderNumber>000054596</JobOrderNumber>
    <BenefitNumber>27</BenefitNumber>
    <OtherBenefitsText />
  </Job_Order_Benefit>
  <Job_Order_Event>
    <JobOrderEventSurrogateKey>78691</JobOrderEventSurrogateKey>
    <EventTypeCode>086</EventTypeCode>
    <JobOrderNumber>000054596</JobOrderNumber>
    <CreateTimestamp>8/25/2014 12:38:02 PM</CreateTimestamp>
    <CreatedByNumber>10766</CreatedByNumber>
    <CreateCode>S</CreateCode>
    <EventText />
    <JobCenterNumber>32</JobCenterNumber>
  </Job_Order_Event>
  <Job_Order_Work_Shift>
    <JobOrderNumber>000054596</JobOrderNumber>
    <JobShiftCode>01</JobShiftCode>
    <OtherShiftText />
  </Job_Order_Work_Shift>
  <Job_Order_Work_Site>
    <WorkSiteSurrogateKey>15</WorkSiteSurrogateKey>
    <JobOrderNumber>000054596</JobOrderNumber>
  </Job_Order_Work_Site>
  <Job_Order_Work_Day>
    <JobOrderWorkdaySurrogateKey>253188</JobOrderWorkdaySurrogateKey>
    <JobOrderNumber>000054596</JobOrderNumber>
    <WorkdayType>MONDAY-FRIDAY</WorkdayType>
    <OtherWorkdayText />
  </Job_Order_Work_Day>
</JobOrderDataset>";

    #endregion
    #endregion


  }
}
