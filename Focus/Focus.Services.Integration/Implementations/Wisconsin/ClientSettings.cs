﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;

#endregion

namespace Focus.Services.Integration.Implementations.Wisconsin
{
  public class ClientSettings
  {
		/// <summary>
		/// Gets or sets the service security mode.
		/// </summary>
		/// <value>
		/// The service security mode.
		/// </value>
		public WSFederationHttpSecurityMode ServiceSecurityMode { get; set; }

		/// <summary>
		/// Gets or sets the focus service endpoint.
		/// </summary>
		/// <value>
		/// The focus service endpoint.
		/// </value>
		public string FocusServiceEndpoint { get; set; }

		/// <summary>
		/// Gets or sets the conversion service endpoint.
		/// </summary>
		/// <value>
		/// The conversion service endpoint.
		/// </value>
		public string ConversionServiceEndpoint { get; set; }

		/// <summary>
		/// Gets or sets the token service endpoint.
		/// </summary>
		/// <value>
		/// The token service endpoint.
		/// </value>
		public string TokenServiceEndpoint { get; set; }

		/// <summary>
		/// Gets or sets the relying party.
		/// </summary>
		/// <value>
		/// The relying party.
		/// </value>
		public string RelyingParty { get; set; }

		/// <summary>
		/// Gets or sets the username.
		/// </summary>
		/// <value>
		/// The username.
		/// </value>
		public string Username { get; set; }

		/// <summary>
		/// Gets or sets the password.
		/// </summary>
		/// <value>
		/// The password.
		/// </value>
		public string Password { get; set; }
  }
}
