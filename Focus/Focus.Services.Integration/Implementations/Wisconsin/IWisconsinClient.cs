﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;

#endregion

namespace Focus.Services.Integration.Implementations.Wisconsin
{
	[ServiceContractAttribute]
	public interface IWisconsinClient
	{
		#region Focus client

		[OperationContract(Action = "http://tempuri.org/IFocus/Getjobseeker", ReplyAction = "http://tempuri.org/IFocus/GetjobseekerResponse")]
		string Getjobseeker(string jobseekerID);

		[OperationContract(Action = "http://tempuri.org/IFocus/AddSelfService", ReplyAction = "http://tempuri.org/IFocus/AddSelfServiceResponse")]
    bool AddSelfService(string jobseekerID, string selfServiceType);

    [OperationContract(Action = "http://tempuri.org/IFocus/MaintainResumeCompletionFlag", ReplyAction = "http://tempuri.org/IFocus/MaintainResumeCompletionFlagResponse")]
    string MaintainResumeCompletionFlag(string actionerId, string personId, string wiCustKey);

		[OperationContract(Action = "http://tempuri.org/IFocus/UpdateContactInformation", ReplyAction = "http://tempuri.org/IFocus/UpdateContactInformationResponse")]
		bool UpdateContactInformation(string JobSeekerID, string Ethnic_Hispanic_Flag, string Race_American_Indian_Or_Alaskan_Native_Flag, string Race_Asian_Flag, string Race_Black_Or_African_American_Flag, string Race_Hawaiian_Native_Or_Other_Pacific_Islander_Flag, string Race_White_Flag, string Race_Other_Flag, string Education_Status_Code, string School_Grade_Code, string Current_Employment_Status_Code);

    [OperationContract(Action = "http://tempuri.org/IFocus/DeactivateEmployer", ReplyAction = "http://tempuri.org/IFocus/DeactivateEmployerResponse")]
    bool DeactivateEmployer(string employerID, string staffID);

    [OperationContract(Action = "http://tempuri.org/IFocus/ActivateEmployer", ReplyAction = "http://tempuri.org/IFocus/ActivateEmployerResponse")]
    bool ActivateEmployer(string employerID, string staffID);

    [OperationContract(Action = "http://tempuri.org/IFocus/CustomerResumeStatus", ReplyAction = "http://tempuri.org/IFocus/CustomerResumeStatusResponse")]
    bool CustomerResumeStatus(string jobSeekerId, string completedResumeCount, string searchableResumeCount);

		#endregion

		#region Conversion client

		[OperationContract(Action = "http://tempuri.org/IConversion/GetJobOrdData", ReplyAction = "http://tempuri.org/IConversion/GetJobOrdDataResponse")]
		string GetJobOrdData(string OrderNo);

		[OperationContract(Action = "http://tempuri.org/IConversion/GetEmpJobOrds", ReplyAction = "http://tempuri.org/IConversion/GetEmpJobOrdsResponse")]
		string GetEmpJobOrds(string ErProfileNo);

		[OperationContract(Action = "http://tempuri.org/IConversion/GetErProfile", ReplyAction = "http://tempuri.org/IConversion/GetErProfileResponse")]
		string GetErProfile(string ErProfileNo);

		[OperationContract(Action = "http://tempuri.org/IConversion/GetErSites", ReplyAction = "http://tempuri.org/IConversion/GetErSitesResponse")]
		string GetErSites(string ErProfileNo);

		[OperationContract(Action = "http://tempuri.org/IConversion/GetSite", ReplyAction = "http://tempuri.org/IConversion/GetSiteResponse")]
		string GetSite(string SiteLocNo);

		[OperationContract(Action = "http://tempuri.org/IConversion/GetEmployerContact", ReplyAction = "http://tempuri.org/IConversion/GetEmployerContactResponse")]
		string GetEmployerContact(string ContactNo);

		[OperationContract(Action = "http://tempuri.org/IConversion/GetEmployerContacts", ReplyAction = "http://tempuri.org/IConversion/GetEmployerContactsResponse")]
		string GetEmployerContacts(string ErProfileNo);

		[OperationContract(Action = "http://tempuri.org/IConversion/GetEmployer", ReplyAction = "http://tempuri.org/IConversion/GetEmployerResponse")]
		string GetEmployer(string ErProfileNo);

		[OperationContract(Action = "http://tempuri.org/IConversion/GetResume", ReplyAction = "http://tempuri.org/IConversion/GetResumeResponse")]
		UploadedResume GetResume(string resumeID);

		#endregion
	}

	[DataContract(Name = "UploadedResume", Namespace = "http://schemas.datacontract.org/2004/07/DWD.DET.VJC.Web")]
	[Serializable]
	public class UploadedResume : object, IExtensibleDataObject, INotifyPropertyChanged
	{

		[NonSerialized]
		private ExtensionDataObject extensionDataField;

		[OptionalField]
		private int CustResumeKeyField;

		[OptionalField]
		private string FileExtensionField;

		[OptionalField]
		private System.IO.MemoryStream FileStreamField;

		[OptionalField]
		private string ResumeNameField;

		[Browsable(false)]
		public ExtensionDataObject ExtensionData
		{
			get
			{
				return extensionDataField;
			}
			set
			{
				extensionDataField = value;
			}
		}

		[DataMember]
		public int CustResumeKey
		{
			get
			{
				return CustResumeKeyField;
			}
			set
			{
				if ((CustResumeKeyField.Equals(value) != true))
				{
					CustResumeKeyField = value;
					RaisePropertyChanged("CustResumeKey");
				}
			}
		}

		[DataMember]
		public string FileExtension
		{
			get
			{
				return FileExtensionField;
			}
			set
			{
				if ((ReferenceEquals(FileExtensionField, value) != true))
				{
					FileExtensionField = value;
					RaisePropertyChanged("FileExtension");
				}
			}
		}

		[DataMember]
		public MemoryStream FileStream
		{
			get
			{
				return FileStreamField;
			}
			set
			{
				if ((ReferenceEquals(FileStreamField, value) != true))
				{
					FileStreamField = value;
					RaisePropertyChanged("FileStream");
				}
			}
		}

		[DataMember]
		public string ResumeName
		{
			get
			{
				return ResumeNameField;
			}
			set
			{
				if ((ReferenceEquals(ResumeNameField, value) != true))
				{
					ResumeNameField = value;
					RaisePropertyChanged("ResumeName");
				}
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged(string propertyName)
		{
			PropertyChangedEventHandler propertyChanged = PropertyChanged;
			if ((propertyChanged != null))
			{
				propertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
    
}
