﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

namespace Focus.Services.Integration.Implementations.EKOS
{
  public class ClientSettings : AOSOS.ClientSettings
  {
    public string EKOSUrl { get; set; }
    public string EKOSUserName { get; set; }
    public string EKOSPassword { get; set; }
    public string EKOSNamespaceUri { get; set; }
    public string DefaultNaics { get; set; }
    public string SelfServiceActivityId { get; set; }
    public string AssistedServiceActivityId { get; set; }
    public string KewesEncryptionKey { get; set; }
		public string EkosServiceUserName { get; set; }
		public string EkosServicePassword { get; set; }
		public List<ActionTypeRestriction> ActionTypeRestrictions { get; set; }
  }

	[Serializable]
	public class ActionTypeRestriction
	{
		public string ExternalActivityId { get; set; }
		public int RestrictedToInstancePerHour { get; set; }
	}
}
