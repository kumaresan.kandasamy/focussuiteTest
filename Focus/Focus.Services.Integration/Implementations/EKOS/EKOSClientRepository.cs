﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.IntegrationMessages;
using Focus.Core.Models.Integration;
using Focus.Services.Integration.Helpers;
using Focus.Services.Integration.Implementations.AOSOS;
using Focus.Services.Integration.Implementations.EKOS.CommandBuilders;
using Focus.Services.Integration.Implementations.EKOS.ResponseTranslators;

using Framework.Core;

using Newtonsoft.Json;

#endregion

namespace Focus.Services.Integration.Implementations.EKOS
{
  public class EKOSClientRepository : AOSOSClientRepository
  {
    private readonly ClientSettings _clientSettings;
    private readonly EmployerCommandBuilder _employerCommandBuilder;
    private readonly JobSeekerCommandBuilder _jobSeekerCommandBuilder;
    private readonly JobCommandBuilder _jobCommandBuilder;
    private readonly StaffCommandBuilder _staffCommandBuilder;
    private readonly Translator _translator;
    private readonly List<ExternalLookUpItemDto> _externalLookUpItems;
    public static ConcurrentDictionary<string, StaffModel> StaffModelCache;

		protected override IntegrationPoint[] GetImplementedIntegrationPoints()
		{
			return new[]
			{
				IntegrationPoint.AuthenticateJobSeeker, IntegrationPoint.AssignEmployerActivity, IntegrationPoint.AssignAdminToEmployer,
				IntegrationPoint.AssignAdminToJobOrder, IntegrationPoint.AssignAdminToJobSeeker, IntegrationPoint.AssignJobSeekerActivity, 
				IntegrationPoint.SaveJobMatches, IntegrationPoint.SetApplicationStatus,
				IntegrationPoint.ChangePassword, IntegrationPoint.DisableJobSeekersReport, IntegrationPoint.GetEmployer, 
				IntegrationPoint.IsUIClaimant, IntegrationPoint.LogAction, IntegrationPoint.GetJob, 
				IntegrationPoint.ReferJobSeeker, IntegrationPoint.UpdateEmployeeStatus, IntegrationPoint.UpdateJobStatus, 
				IntegrationPoint.GetJobSeeker, IntegrationPoint.JobSeekerLogin, IntegrationPoint.SaveEmployee, 
				IntegrationPoint.SaveEmployer, IntegrationPoint.SaveJob,
				IntegrationPoint.SaveJobSeeker, IntegrationPoint.GetStaff
			};
		}

    /// <summary>
    /// Refreshes the staff model in the cache for the ExternalUsername specified in the request.
    /// </summary>
    /// <param name="request">The request.</param>
    private bool RefreshStaffModel(IIntegrationRequest request)
    {
      if (request.IsNull())
        return true;

			return RefreshStaffModel(request.ExternalUsername);
    }

		/// <summary>
		/// Refreshes the staff model in the cache for the ExternalUsername
		/// </summary>
		/// <param name="externalUsername">The external username.</param>
		private bool RefreshStaffModel(string externalUsername)
		{
			var success = true;

			if (StaffModelCache == null) StaffModelCache = new ConcurrentDictionary<string, StaffModel>();
			if (externalUsername.IsNullOrEmpty())
				return true;

			StaffModel staffUserInfo = null;
			try
			{
				var requestResponseGet = SendWebRequest(_staffCommandBuilder.GetStaffUser(externalUsername), "GetStaffProfile");
				staffUserInfo = _translator.XmlToStaffModel(requestResponseGet.IncomingResponse);
			}
			catch
			{
				success = false;
			}

			if (staffUserInfo.IsNotNull())
			{
				StaffModelCache.AddOrUpdate(externalUsername, staffUserInfo, (s, model) => staffUserInfo);
			}

			return success;
		}

    /// <summary>
    /// Sets the request's external password and admin id by checking the <see cref="StaffModelCache"/> first then AOSOS.
    /// If this connot find a password for the request.ExternalUsername IT DOES NOT CHANGE THE PASSWORD.
    /// </summary>
    /// <param name="request">The request.</param>
    private void SetRequestPassword(IIntegrationRequest request)
    {
      if (request.IsNull() || request.ExternalUsername.IsNull())
        return;

	    var success = true;
			if (StaffModelCache.IsNull() || !StaffModelCache.ContainsKey(request.ExternalUsername))
        success = RefreshStaffModel(request);

	    if (success)
	    {
		    if (StaffModelCache.IsNotNullOrEmpty() && StaffModelCache.ContainsKey(request.ExternalUsername) &&
		        StaffModelCache[request.ExternalUsername].IsNotNull())
		    {
			    request.ExternalPassword = StaffModelCache[request.ExternalUsername].Password;
			    request.ExternalAdminId = StaffModelCache[request.ExternalUsername].ExternalId;
		    }
	    }
	    else
	    {
				// In the case of failure, it needs to fall back to using default credentials
				if (request.ExternalUsername.StartsWith("selfreg", StringComparison.OrdinalIgnoreCase) || request.ExternalPassword.IsNullOrEmpty())
				{
					request.ExternalAdminId = null;
					request.ExternalOfficeId = null;
					request.ExternalPassword = null;
					request.ExternalUsername = null;
				}
	    }
    }

		public override string GetExternalAdminId(string externalUsername)
		{
			if (externalUsername.IsNullOrEmpty())
				return null;

			if (StaffModelCache.IsNull() || !StaffModelCache.ContainsKey(externalUsername))
				RefreshStaffModel(externalUsername);

			if (StaffModelCache.IsNotNullOrEmpty() && StaffModelCache.ContainsKey(externalUsername) && StaffModelCache[externalUsername].IsNotNull())
			{
				return StaffModelCache[externalUsername].ExternalId;
			}

			return null;
		}

    public EKOSClientRepository(string settings, List<ExternalLookUpItemDto> externalLookUpItems)
      : base(settings, externalLookUpItems)
    {
      _clientSettings = (ClientSettings)JsonConvert.DeserializeObject(settings, typeof(ClientSettings));
      _employerCommandBuilder = new EmployerCommandBuilder(_clientSettings, externalLookUpItems);
      _jobSeekerCommandBuilder = new JobSeekerCommandBuilder(_clientSettings, externalLookUpItems);
      _jobCommandBuilder = new JobCommandBuilder(_clientSettings, externalLookUpItems);
      _staffCommandBuilder = new StaffCommandBuilder(_clientSettings, externalLookUpItems);
      _externalLookUpItems = externalLookUpItems;
      _translator = new Translator(_clientSettings);
    }

    public override SaveJobSeekerResponse SaveJobSeeker(SaveJobSeekerRequest request)
    {
      var response = new SaveJobSeekerResponse(request);
      try
      {
        SetRequestPassword(request);
        response = base.SaveJobSeeker(request);
        if (response.Outcome != IntegrationOutcome.Success && response.Outcome != IntegrationOutcome.Ignored)
        {
          RefreshStaffModel(request);
          SetRequestPassword(request);
          response = base.SaveJobSeeker(request);
        }
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

		public override SaveJobMatchesResponse SaveJobMatches(SaveJobMatchesRequest request)
    {

      var response = new SaveJobMatchesResponse(request);
      try
      {
				UpdateSelfServiceDetails(request, request.ExternalJobSeekerId);

        SetRequestPassword(request);
        response = base.SaveJobMatches(request);
        if (response.Outcome != IntegrationOutcome.Success)
        {
          RefreshStaffModel(request);
          SetRequestPassword(request);
          response = base.SaveJobMatches(request);
        }
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }
      return response;
    }


    #region Job

		/// <summary>
		/// Gets the job version.
		/// </summary>
		/// <param name="jobId">The job identifier.</param>
		/// <param name="breadcrumbs">The breadcrumbs.</param>
		/// <returns>The job version</returns>
		protected override string GetJobVersion(string jobId, List<RequestResponse> breadcrumbs)
		{
			var requestResponse = SendWebRequest(_jobCommandBuilder.GetJobInformation(jobId), "GetJobInformation");
			breadcrumbs.Add(requestResponse);

			var errors = _translator.FindErrors(requestResponse.IncomingResponse);
			if (errors.IsNotNullOrEmpty()) throw new Exception(errors);

			var jobVersion = _translator.XmlToJobVersion(requestResponse.IncomingResponse);
			if (jobVersion.IsNull())
				throw new Exception("GetJobVersion response Xml could not be transformed to a Job Version.");

			return jobVersion;
		}

    /// <summary>
    /// Gets the job referral information.
    /// </summary>
    /// <param name="jobId">The job identifier.</param>
    /// <param name="breadcrumbs">The breadcrumbs.</param>
    /// <returns>A tuple indicating the current number of referrals and the maximum allowed</returns>
    protected override Tuple<int, int> GetJobReferralInfo(string jobId, List<RequestResponse> breadcrumbs)
    {
	    try
	    {
				var requestResponse = SendWebRequest(_jobCommandBuilder.GetJobInformation(jobId), "GetJobInformation");
				breadcrumbs.Add(requestResponse);

				var errors = _translator.FindErrors(requestResponse.IncomingResponse);
				if (errors.IsNotNullOrEmpty()) throw new Exception(errors);

				return _translator.XmlToReferralCounts(requestResponse.IncomingResponse);
			}
	    catch
	    {
				
	    }

	    return base.GetJobReferralInfo(jobId, breadcrumbs);
    }

    #endregion

    #region Job Seeker

		/// <summary>
		/// Gets the job seeker version from the EKOS web service
		/// </summary>
		/// <param name="jobSeekerId">The external ID of the job seeker</param>
		/// <param name="breadcrumbs">The breadcrumb trail</param>
		/// <returns>The job seeker version</returns>
		protected override string GetJobSeekerVersion(string jobSeekerId, List<RequestResponse> breadcrumbs)
		{
			if (jobSeekerId.IsNullOrEmpty()) throw new Exception("GetJobSeekerVersion requires a jobSeekerId.");
			var requestResponse = SendWebRequest(_jobSeekerCommandBuilder.GetJobSeekerVersion(jobSeekerId), "GetSeekerVersion");
			breadcrumbs.Add(requestResponse);

			var errors = _translator.FindErrors(requestResponse.IncomingResponse);
			if (errors.IsNotNullOrEmpty()) throw new Exception(errors);

			var jobSeekerVersion = _translator.XmlToJobSeekerVersion(requestResponse.IncomingResponse);
			if (jobSeekerVersion.IsNull())
				throw new Exception("GetJobSeekerVersion response Xml could not be transformed to a JobSeeker Version.");

			return jobSeekerVersion;
		}

		/// <summary>
    /// Authenticates the job seeker.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    public override AuthenticateJobSeekerResponse AuthenticateJobSeeker(AuthenticateJobSeekerRequest request)
    {
      var response = new AuthenticateJobSeekerResponse(request);

      try
      {
        if (request.AuthenticationDetails.IsNullOrEmpty() || _clientSettings.KewesEncryptionKey.IsNullOrEmpty())
          return base.AuthenticateJobSeeker(request);

        var encryptedUserName = "";
        var encryptedPassword = "";

        if (request.AuthenticationDetails.ContainsKey("uid"))
          encryptedUserName = request.AuthenticationDetails["uid"];
        else if (request.AuthenticationDetails.ContainsKey("string1"))
          encryptedUserName = request.AuthenticationDetails["string1"];

        if (request.AuthenticationDetails.ContainsKey("pwd"))
          encryptedPassword = request.AuthenticationDetails["pwd"];
        else if (request.AuthenticationDetails.ContainsKey("string2"))
          encryptedPassword = request.AuthenticationDetails["string2"];

        var encryptionKey = _clientSettings.KewesEncryptionKey;

        request.UserName = Decrypt(encryptedUserName, ref encryptionKey);
        request.Password = Decrypt(encryptedPassword, ref encryptionKey);

        return base.AuthenticateJobSeeker(request);
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }
      return response;
    }

    /// <summary>
    /// Gets whether the job seeker is a UI Claimant
    /// </summary>
    /// <param name="request">Details of the job seeker to check</param>
    /// <returns>Whether they are a claimant or not</returns>
    public override IsUIClaimantResponse IsUIClaimant(IsUIClaimantRequest request)
    {
      var response = new IsUIClaimantResponse(request);
      try
      {
        string command;
        if (request.SocialSecurityNumber.IsNotNullOrEmpty())
          command = _jobSeekerCommandBuilder.GetClaimantStatus(request.SocialSecurityNumber);
        else
          throw new Exception("Only SSN is supported");

        var requestResponse = SendWebRequest(command, "GetClaimantStatus");
        response.IntegrationBreadcrumbs.Add(requestResponse);
        var errors = _translator.FindErrors(requestResponse.IncomingResponse);
        if (errors.IsNotNullOrEmpty())
          throw new Exception(errors);

        response.IsUIClaimant = (_translator.XmlToClaimantStatus(requestResponse.IncomingResponse).IsIn("2", "5"));
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public override ReferJobSeekerResponse ReferJobSeeker(ReferJobSeekerRequest request)
    {
      var response = new ReferJobSeekerResponse(request);
      try
      {
        // Get office self reg password
        SetRequestPassword(request);

				if (request.Job.IsNotNull() && request.Job.Id > 0 && request.Job.ExternalId.IsNullOrEmpty())
				{
					if (request.JobSeekerId.IsNullOrEmpty())
					{
						throw new Exception("Unable to refer job seeker as neither job nor job seeker has an external id");	
					}
					throw new Exception("Unable to refer job seeker as associated job has no external id");	
				}

				if (request.JobSeekerId.IsNullOrEmpty())
				{
					throw new Exception("Unable to refer job seeker as the job seeker has no external id");
				}

        // Talent job so go through AOSOS implementation
				if (request.JobId != 0 || request.ExternalJobId.IsNotNullOrEmpty())
					return base.ReferJobSeeker(request);

	      // Spidered job but already added to Ekos so go through AOSOS implementation
        if (request.Job.IsNotNull() && request.Job.ExternalId.IsNotNullOrEmpty())
        {
          request.ExternalJobId = request.Job.ExternalId;
          return base.ReferJobSeeker(request);
        }

        // New job (presumably spidered but not already added to EKOS).
        // 1. See if employer exists
        var naics = request.Job.BusinessUnitNaics;
        if (naics.IsNullOrEmpty() || !Regex.IsMatch(naics, @"^\d*$"))
          naics = _clientSettings.DefaultNaics;

        var fein = "9999999" + naics.Substring(0, 2);
        var employer = base.GetEmployerByFein(fein, response.IntegrationBreadcrumbs);
        if (employer.IsNull())
        {
          ExternalLookUpItemDto externalLookUpItemDto = _externalLookUpItems.FirstOrDefault(x => x.ExternalLookUpType == ExternalLookUpType.BaseNaics && x.InternalId == naics.Substring(0, 2));
          string name = externalLookUpItemDto.IsNull() ? "UNKNOWN NAICS" : externalLookUpItemDto.ExternalId;
          var employerRequest = CreateSaveEmployerRequest(request, naics, fein, name);
          var employerResponse = base.SaveEmployer(employerRequest);
          response.IntegrationBreadcrumbs.AddRange(employerResponse.IntegrationBreadcrumbs);
          if (employerResponse.Outcome == IntegrationOutcome.Failure)
            throw employerResponse.Exception;

          if (request.Job.IsNotNull() && employerResponse.IsNotNull() && employerResponse.Employees.IsNotNullOrEmpty())
            request.Job.HiringManager = new HiringManager { ExternalId = employerResponse.Employees[0].Item2 };
        }
        else
        {
          // We also need to make sure we have employees, if there isn't one then add one
          if (employer.Employees.IsNotNullOrEmpty())
          {
            request.Job.HiringManager = new HiringManager { ExternalId = employer.Employees[0].ExternalId };
          }
          else
          {
            var employeeRequest = CreateSaveEmployeeRequest(request, employer);
            var employeeResponse = base.SaveEmployee(employeeRequest);
            response.IntegrationBreadcrumbs.AddRange(employeeResponse.IntegrationBreadcrumbs);
            if (employeeResponse.Outcome == IntegrationOutcome.Failure)
              throw employeeResponse.Exception;

            request.Job.HiringManager.ExternalId = employeeResponse.ExternalEmployeeId;
          }

          if (!employer.IsActive)
          {
            employer.Status = EmployerStatusTypes.Active;
            var employerRequest = new SaveEmployerRequest
            {
              Employer = employer
            };
            var employerResponse = base.SaveEmployer(employerRequest);
            response.IntegrationBreadcrumbs.AddRange(employerResponse.IntegrationBreadcrumbs);
            if (employerResponse.Outcome == IntegrationOutcome.Failure)
              throw employerResponse.Exception;

            if (request.Job.IsNotNull() && employerResponse.IsNotNull() && employerResponse.Employees.IsNotNullOrEmpty())
              request.Job.HiringManager = new HiringManager { ExternalId = employerResponse.Employees[0].Item2 };
          }
        }

        // Add job
        var jobRequest = new SaveJobRequest { Job = request.Job };
        var jobResponse = base.SaveJob(jobRequest);
        response.IntegrationBreadcrumbs.AddRange(jobResponse.IntegrationBreadcrumbs);
        if (jobResponse.Outcome == IntegrationOutcome.Failure)
          throw jobResponse.Exception;

        // Remember externalId to update posting
        request.ExternalJobId = response.ExternalJobId = jobResponse.ExternalJobId;
        // Add referral
        var referralResponse = base.ReferJobSeeker(request);
        response.IntegrationBreadcrumbs.AddRange(referralResponse.IntegrationBreadcrumbs);
        if (referralResponse.Outcome == IntegrationOutcome.Failure)
          throw referralResponse.Exception;
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

		/// <summary>
		/// Look up the job seeker's external ID based on their SSN
		/// </summary>
		/// <param name="ssn">The job seeker's SSN</param>
		/// <param name="breadcrumbs">A list of all requests and responses sent to EKOS</param>
		/// <returns>The job seeker id</returns>
		protected override string GetJobSeekerId(string ssn, List<RequestResponse> breadcrumbs)
		{
			if (ssn.IsNullOrEmpty())
			{
				throw new Exception("GetJobSeekerId requires an SSN.");
			}

			var requestResponse = SendWebRequest(_jobSeekerCommandBuilder.GetJobSeekerId(ssn), "GetSeekerId");
			breadcrumbs.Add(requestResponse);

			var errors = _translator.FindErrors(requestResponse.IncomingResponse);
			if (errors.IsNotNullOrEmpty())
			{
				throw new Exception(errors);
			}

			return _translator.XmlToJobSeekerId(requestResponse.IncomingResponse);
		}

		#endregion

		#region Employer

		/// <summary>
		/// Gets the employer version from the EKOS web service
		/// </summary>
		/// <param name="employerId">The external ID of the employer</param>
		/// <param name="breadcrumbs">The breadcrumb trail</param>
		/// <returns>The job seeker version</returns>
		protected override string GetEmployerVersion(string employerId, List<RequestResponse> breadcrumbs)
		{
			if (employerId.IsNullOrEmpty()) throw new Exception("GetEmployerVersion requires a employer.");
			var requestResponse = SendWebRequest(_employerCommandBuilder.GetEmployerVersion(employerId), "GetEmployerVersion");
			breadcrumbs.Add(requestResponse);

			var errors = _translator.FindErrors(requestResponse.IncomingResponse);
			if (errors.IsNotNullOrEmpty()) throw new Exception(errors);

			var employerVersion = _translator.XmlToEmployerVersion(requestResponse.IncomingResponse);
			if (employerVersion.IsNull())
				throw new Exception("GetEmployerVersion response Xml could not be transformed to an Employer Version.");

			return employerVersion;
		}

		private static SaveEmployeeRequest CreateSaveEmployeeRequest(ReferJobSeekerRequest request, EmployerModel employer)
    {
      var employeeRequest = new SaveEmployeeRequest
      {
        Employee = new EmployeeModel
        {
          FirstName = "FIRSTNAME",
          LastName = "LASTNAME",
          Phone = "5666666666",
          Fax = "5666666666",
          Email = "testemployer@demo.com",
          ExternalEmployerId = employer.ExternalId,
          Address = new AddressModel
          {
            AddressLine1 = "Test Address",
            City = request.Job.Addresses[0].City,
            State = request.Job.Addresses[0].State,
            PostCode = request.Job.Addresses[0].PostCode,
            CountyId = request.Job.Addresses[0].CountyId,
            StateId = request.Job.Addresses[0].StateId,
            CountryId = request.Job.Addresses[0].CountryId
          }
        }
      };
      return employeeRequest;
    }

    private SaveEmployerRequest CreateSaveEmployerRequest(ReferJobSeekerRequest request, string naics, string fein, string name)
    {
      var employerRequest = new SaveEmployerRequest
      {
        Employer = new EmployerModel
        {
          Name = name,
          FederalEmployerIdentificationNumber = fein,
          Naics = naics.Substring(0, 2),
          Id = request.PostingId,
          Status = EmployerStatusTypes.Active,
          Address = new AddressModel
          {
            AddressLine1 = "Test Address",
            City = request.Job.Addresses[0].City,
            State = request.Job.Addresses[0].State,
            PostCode = request.Job.Addresses[0].PostCode,
            CountyId = request.Job.Addresses[0].CountyId,
            StateId = request.Job.Addresses[0].StateId,
            CountryId = request.Job.Addresses[0].CountryId
          },
          Employees = new List<EmployeeModel>
	        {
	          new EmployeeModel
	          {
	            FirstName = "FIRSTNAME",
	            LastName = "LASTNAME",
	            Phone = "5666666666",
	            Fax = "5666666666",
	            Email = "testemployer@demo.com",
	            Address = new AddressModel
	            {
	              AddressLine1 = "Test Address",
	              City = request.Job.Addresses[0].City,
	              State = request.Job.Addresses[0].State,
	              PostCode = request.Job.Addresses[0].PostCode,
	              CountyId = request.Job.Addresses[0].CountyId,
	              StateId = request.Job.Addresses[0].StateId,
	              CountryId = request.Job.Addresses[0].CountryId
	            }
	          }
	        }
        }
      };
      return employerRequest;
    }
		
		public override SaveJobResponse SaveJob(SaveJobRequest request)
    {
      var response = new SaveJobResponse(request);
      try
      {
        SetRequestPassword(request);
        response = base.SaveJob(request);
        if (response.Outcome != IntegrationOutcome.Success)
        {
          RefreshStaffModel(request);
          SetRequestPassword(request);
          response = base.SaveJob(request);
        }
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public override SetApplicationStatusResponse SetApplicationStatus(SetApplicationStatusRequest request)
    {
      var response = new SetApplicationStatusResponse(request);
      try
      {
        SetRequestPassword(request);
        response = base.SetApplicationStatus(request);
        if (response.Outcome.IsNotIn(IntegrationOutcome.Success, IntegrationOutcome.Ignored))
        {
          RefreshStaffModel(request);
          SetRequestPassword(request);
          response = base.SetApplicationStatus(request);
        }
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }


      return response;
    }

    public override SaveEmployerResponse SaveEmployer(SaveEmployerRequest request)
    {
      var response = new SaveEmployerResponse(request);
      try
      {
	      var employer = request.Employer;
	      if (request.ExternalOfficeId.IsNullOrEmpty())
	      {
					request.ExternalOfficeId = ConvertFirstToExternalId(ExternalLookUpType.IntegrationOfficeIdPerZip, employer.Address.PostCode);

					Func<EmployeeModel, bool> predicate = employee => employee.Address.IsNotNull() && employee.Address.PostCode.IsNotNullOrEmpty();
		      if (request.Employer.Employees.Any(predicate))
		      {
						request.ExternalOfficeId = ConvertFirstToExternalId(ExternalLookUpType.IntegrationOfficeIdPerZip, employer.Employees.First(predicate).Address.PostCode);
		      }
	      }

	      var isSelfRef = false;
				if (request.ExternalAdminId.IsNullOrEmpty() && request.ExternalOfficeId.IsNotNullOrEmpty())
				{
					isSelfRef = true;
					request.ExternalAdminId = GetExternalAdminId(string.Format("selfreg{0}", request.ExternalOfficeId).ToLowerInvariant());
				}

				if (request.ExternalUsername.IsNullOrEmpty() && isSelfRef)
				{
					request.ExternalUsername = string.Format("selfreg{0}", request.ExternalOfficeId).ToLowerInvariant();
					request.ExternalPassword = "selfreg";
				}

        SetRequestPassword(request);
        response = base.SaveEmployer(request);
        if (response.Outcome != IntegrationOutcome.Success)
        {
          RefreshStaffModel(request);
          SetRequestPassword(request);
          response = base.SaveEmployer(request);
        }
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

		internal string ConvertFirstToExternalId(ExternalLookUpType lookUpType, string internalValue)
		{
			var returnVal = _externalLookUpItems.FirstOrDefault(x => x.InternalId == internalValue && x.ExternalLookUpType == lookUpType);
			if (returnVal.IsNotNull())
				return returnVal.ExternalId;

			return null;
		}

    public override AssignAdminToEmployerResponse AssignAdminToEmployer(AssignAdminToEmployerRequest request)
    {
      var response = new AssignAdminToEmployerResponse(request);
      try
      {
        SetRequestPassword(request);
        response = base.AssignAdminToEmployer(request);
        if (response.Outcome != IntegrationOutcome.Success)
        {
          RefreshStaffModel(request);
          SetRequestPassword(request);
          response = base.AssignAdminToEmployer(request);
        }
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

		public override AssignAdminToJobOrderResponse AssignAdminToJobOrder(AssignAdminToJobOrderRequest request)
		{
			var response = new AssignAdminToJobOrderResponse(request);
			try
			{
				SetRequestPassword(request);
				response = base.AssignAdminToJobOrder(request);
				if (response.Outcome != IntegrationOutcome.Success)
				{
					RefreshStaffModel(request);
					SetRequestPassword(request);
					response = base.AssignAdminToJobOrder(request);
				}
			}
			catch (Exception e)
			{
				response.Outcome = IntegrationOutcome.Failure;
				response.Exception = e;
			}

			return response;
		}

    public override AssignEmployerActivityResponse AssignEmployerActivity(AssignEmployerActivityRequest request)
    {
      var response = new AssignEmployerActivityResponse(request);
      try
      {
				if (request.ActivityId.IsNullOrEmpty() || request.ActivityId == "0" || request.EmployerId.IsNullOrEmpty())
				{
					response.Outcome = IntegrationOutcome.Ignored;
					return response;
				}

        SetRequestPassword(request);
        response = base.AssignEmployerActivity(request);
        if (response.Outcome != IntegrationOutcome.Success)
        {
          RefreshStaffModel(request);
          SetRequestPassword(request);
          response = base.AssignEmployerActivity(request);
        }
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public override SaveEmployeeResponse SaveEmployee(SaveEmployeeRequest request)
    {
      var response = new SaveEmployeeResponse(request);
      try
      {
        SetRequestPassword(request);
        response = base.SaveEmployee(request);
        if (response.Outcome != IntegrationOutcome.Success)
        {
          RefreshStaffModel(request);
          SetRequestPassword(request);
          response = base.SaveEmployee(request);
        }
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    /// <summary>
    /// Gets the job seeker's version
    /// </summary>
    /// <param name="actionType">Type of the action.</param>
    /// <param name="selfServicedActivity">Is this a self-serviced activity</param>
    /// <returns></returns>
    public override string GetActivityId(ActionTypes actionType, bool selfServicedActivity)
    {
      var selfService = _externalLookUpItems.FirstOrDefault(x => x.InternalId == actionType.ToString() && x.ExternalLookUpType == ExternalLookUpType.ActionType);
      if (selfService.IsNull())
        return null;

      if (selfService.ExternalId == _clientSettings.SelfServiceActivityId && !selfServicedActivity)
        return _clientSettings.AssistedServiceActivityId; // Convert to staff assisted activity
    
      return selfService.ExternalId;
    }

		public override bool IsActivityStaffAssisted(ActionTypes actionType)
		{
			return GetActivityId(actionType, false) == _clientSettings.AssistedServiceActivityId;
		}

		public override int GetActivityRestriction(AssignJobSeekerActivityRequest request)
		{
			if (!request.IsManual && _clientSettings.ActionTypeRestrictions.IsNotNullOrEmpty() &&
					_clientSettings.ActionTypeRestrictions.Any(atr => atr.ExternalActivityId == request.ActivityId))
			{
				var activityRestriction =
					_clientSettings.ActionTypeRestrictions.First(atr => atr.ExternalActivityId == request.ActivityId)
						.RestrictedToInstancePerHour;

				return activityRestriction;
			}

			return 0;
		}

    public override AssignJobSeekerActivityResponse AssignJobSeekerActivity(AssignJobSeekerActivityRequest request)
    {
      var response = new AssignJobSeekerActivityResponse(request);
      try
      {
	      if (request.ActivityId.IsNullOrEmpty() || request.ActivityId == "0" || request.JobSeekerId.IsNullOrEmpty())
	      {
					response.Outcome = IntegrationOutcome.Ignored;
		      return response;
	      }

	      if (request.SelfServicedActivity)
        {
	        UpdateSelfServiceDetails(request, request.JobSeekerId);
        }

        SetRequestPassword(request);
        response = base.AssignJobSeekerActivity(request);
        if (response.Outcome != IntegrationOutcome.Success)
        {
          RefreshStaffModel(request);
          SetRequestPassword(request);
          response = base.AssignJobSeekerActivity(request);
        }
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      response.Request = request;
      return response;
    }

    public override AssignAdminToJobSeekerResponse AssignAdminToJobSeeker(AssignAdminToJobSeekerRequest request)
    {
      var response = new AssignAdminToJobSeekerResponse(request);
      try
      {
        SetRequestPassword(request);
        response = base.AssignAdminToJobSeeker(request);
        if (response.Outcome != IntegrationOutcome.Success)
        {
          RefreshStaffModel(request);
          SetRequestPassword(request);
          response = base.AssignAdminToJobSeeker(request);
        }
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    #endregion

    #region Staff

    /// <summary>
    /// Gets the authenticated user from EKOS Web Service
    /// </summary>
    /// <param name="userName">The user name</param>
    /// <param name="password">The user's password</param>
    /// <param name="breadcrumbs">The breadcrumb trail</param>
    /// <returns>The authentication model (null if not authenticated)</returns>
    protected override StaffModel GetAuthenticatedUser(string userName, string password, List<RequestResponse> breadcrumbs)
    {
      // EKOS Web Service for KY supports fully scoped data in the 
      // resultset so we can get it all directly without making a second call to AOSOS

      // Authorise User
      var requestResponse = SendWebRequest(_staffCommandBuilder.GetStaffUser(userName), "GetStaffProfile");
      breadcrumbs.Add(requestResponse);

      var errors = _translator.FindErrors(requestResponse.IncomingResponse);
      if (errors.IsNotNullOrEmpty())
        throw new Exception(errors);

      var staffModel = _translator.XmlToStaffModel(requestResponse.IncomingResponse);
      if (staffModel.IsNull() || staffModel.Password != password)
        return null;

      if (staffModel.AuthModel.IsNull())
        staffModel.AuthModel = new AuthenticationModel()
        {
          ExternalAdminId = staffModel.ExternalId,
          ExternalOfficeId = staffModel.AuthModel.ExternalOfficeId
        };

      return staffModel;
    }


    #endregion

    #region Shared Methods

		/// <summary>
		/// Updates external details for self-service requests
		/// </summary>
		/// <param name="request">The request to update</param>
		/// <param name="jobSeekerId">The job seeker Id</param>
		private void UpdateSelfServiceDetails(IIntegrationRequest request, string jobSeekerId)
		{
			JobSeekerModel ekosJobSeekerModel = GetJobSeekerById(jobSeekerId, new List<RequestResponse>());
			string externalOfficeId = ekosJobSeekerModel.OfficeId;
			ExternalLookUpItemDto externalOfficeLookup =
				_externalLookUpItems.FirstOrDefault(
					x =>
						x.InternalId == ekosJobSeekerModel.AddressPostcodeZip &&
						x.ExternalLookUpType == ExternalLookUpType.IntegrationOfficeIdPerZip);
			if (externalOfficeLookup != null && !string.IsNullOrWhiteSpace(externalOfficeLookup.ExternalId))
			{
				externalOfficeId = externalOfficeLookup.ExternalId;
			}

			request.ExternalUsername = string.Format("selfreg{0}", externalOfficeId).ToLowerInvariant();
			request.ExternalOfficeId = externalOfficeId;
			request.ExternalAdminId = ekosJobSeekerModel.AdminId.IsNotNullOrEmpty() ? ekosJobSeekerModel.AdminId : _clientSettings.DefaultAdminId;
		}

    /// <summary>
    /// Sends the web request.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <param name="action">The soap action.</param>
    /// <returns></returns>
    /// <exception cref="System.Exception">Failure while sending request to EKOS</exception>
		private RequestResponse SendWebRequest(string request, string action)
		{
			var response = new RequestResponse {StartTime = DateTime.Now};

	    long webRequestElapsedMilliseconds = -1;
			var methodLog = new StringBuilder();

			try
			{
				methodLog.AppendFormat("{0} : Encoding request content", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"));

				var encoder = new UTF8Encoding();
				var content = encoder.GetBytes(request);
				var contentLength = content.Length;

				if (ServicePointManager.DefaultConnectionLimit < 200)
					ServicePointManager.DefaultConnectionLimit = 200;

				response.OutgoingRequest = request;

				methodLog.AppendFormat("{0} : Creating web request : {1}: {2}", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"),
					_clientSettings.EKOSUrl, _clientSettings.EKOSNamespaceUri + action);

				// Make WebRequest
				var webRequest = WebRequest.Create(_clientSettings.EKOSUrl);
				response.Url = webRequest.RequestUri.AbsoluteUri;
				webRequest.Method = "POST";
				webRequest.ContentType = "text/xml;charset=utf-8";
				webRequest.ContentLength = contentLength;
				webRequest.Headers.Add("SOAPAction", _clientSettings.EKOSNamespaceUri + action);

				methodLog.AppendFormat("{0} : Setting timeout to 200000ms", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"));

				webRequest.Timeout = 200000;

				using (var inputStream = webRequest.GetRequestStream())
				{
					inputStream.Write(content, 0, contentLength);
					inputStream.Close();
				}

				var stopwatch = new Stopwatch();
				try
				{
					stopwatch.Start();

					methodLog.AppendFormat("{0} : Sending request", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"));

					using (var webResponse = (HttpWebResponse)webRequest.GetResponse())
					{
						response.EndTime = DateTime.Now;
						response.StatusCode = webResponse.StatusCode;
						response.StatusDescription = webResponse.StatusDescription;

						methodLog.AppendFormat("{0} : Getting request content", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"));

						using (var outputStream = webResponse.GetResponseStream())
						{
							if (outputStream != null)
							{
								using (var reader = new StreamReader(outputStream))
								{
									response.IncomingResponse = reader.ReadToEnd();
									reader.Close();
								}
							}
						}

						webResponse.Close();
					}

					methodLog.AppendFormat("{0} : Request complete", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"));

					stopwatch.Stop();
					webRequestElapsedMilliseconds = stopwatch.ElapsedMilliseconds;
				}
				catch (Exception)
				{
					methodLog.AppendFormat("{0} : Request failed", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"));

					stopwatch.Stop();
					webRequestElapsedMilliseconds = stopwatch.ElapsedMilliseconds;

					throw;
				}
			}
			catch (WebException ex)
			{
				var httpWebResponse = ex.Response as HttpWebResponse;
				if (httpWebResponse.IsNotNull())
				{
					response.StatusCode = httpWebResponse.StatusCode;
					response.StatusDescription = httpWebResponse.StatusDescription;
				}
				else
				{
					response.StatusCode = null;
					response.StatusDescription = ex.Status.ToString();
				}

				throw new IntegrationRequestException(
					string.Format("EKOS: Type: {0}. Message: {1}. Status: {2}, Inner Exception: {3}. Request Time: {4}, Log: {5}",
						"WebException", ex.Message, ex.Status, ex.InnerException.IsNull() ? string.Empty : ex.InnerException.Message,
						webRequestElapsedMilliseconds, methodLog), response);
			}
			catch (Exception ex)
			{
				throw new IntegrationRequestException(
					string.Format("EKOS: Type: {0}. Message: {1}. Inner Exception: {2}. Request Time: {3}, Log: {4}",
						"Exception", ex.Message, ex.InnerException.IsNull() ? string.Empty : ex.InnerException.Message,
						webRequestElapsedMilliseconds, methodLog), response);
			}

			return response;
		}

    #endregion

    #region Encryption (Not to be used elsewhere!)

    private const int mEncryptionBoxLen = 255;
    private static int[] mEncryptionBox = new int[mEncryptionBoxLen + 1];

    public static string Decrypt(String inEncryptedText, ref String inEncryptionKey)
    {
      ConvertKeyToAscii(inEncryptionKey);

      string cryptedText = "";
      //remove the last pipe from the encrypted text.
      if ((inEncryptedText.Substring(inEncryptedText.Length - 1, 1).Equals("|")))
        inEncryptedText = inEncryptedText.Substring(0, inEncryptedText.Length - 1);

      string[] input = inEncryptedText.Split('|');
      int[] output = new int[input.Length];
      byte[] encryptionBox = new byte[256];

      for (int x = 0; x <= encryptionBox.Length - 1; x++)
        encryptionBox[x] = Convert.ToByte(mEncryptionBox[x]);

      int cipherLen = input.Length + 1;

      int offset = 0;

      int i = 0;
      int j = 0;
      for (offset = 0; offset <= input.Length - 1; offset++)
      {
        i = (i + 1) % mEncryptionBoxLen;
        j = (j + encryptionBox[i]) % mEncryptionBoxLen;
        int temp = encryptionBox[i];
        encryptionBox[i] = encryptionBox[j];
        encryptionBox[j] = Convert.ToByte(temp);
        int a = Convert.ToInt32(input[offset]);
        int temp2 = encryptionBox[i];
        int temp3 = encryptionBox[j];
        int temp4 = (temp2 + temp3) % mEncryptionBoxLen;
        int b = encryptionBox[temp4];
        output[offset] = (a ^ b);
      }

      for (int x = 0; x <= output.Length - 1; x++)
        cryptedText = cryptedText + (char)(output[x]);
      return cryptedText.Trim();
    }

    private static void ConvertKeyToAscii(string inEncryptionKey)
    {
      int[] AsciiChars = new int[inEncryptionKey.Length + 1];
      long index = 0;
      for (int i = 0; i <= inEncryptionKey.Length - 1; i++)
        AsciiChars[i] = (int)(inEncryptionKey[i]);

      long keyLength = inEncryptionKey.Length;

      for (int count = 0; count <= mEncryptionBoxLen - 1; count++)
        mEncryptionBox[count] = count;


      for (int count = 0; count <= mEncryptionBoxLen - 1; count++)
      {
        index = (index + mEncryptionBox[count] + AsciiChars[count % keyLength]) % mEncryptionBoxLen;
        int temp = mEncryptionBox[count];
        mEncryptionBox[count] = mEncryptionBox[index];
        mEncryptionBox[index] = temp;
      }
    }

    #endregion
  }
}
