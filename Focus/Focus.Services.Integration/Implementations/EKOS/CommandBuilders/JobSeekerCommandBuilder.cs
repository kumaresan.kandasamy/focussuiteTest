﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;

using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Services.Integration.Implementations.EKOS.CommandBuilders
{
  public class JobSeekerCommandBuilder : CommandBuilder
  {
    public JobSeekerCommandBuilder(ClientSettings clientSettings, List<ExternalLookUpItemDto> externalLookUpItems) : base(clientSettings, externalLookUpItems)
    {
    }

    /// <summary>
    /// Gets the job seeker's claimant status
    /// </summary>
    /// <param name="socialSecurityNumber">The job seeker's social security number</param>
    /// <returns></returns>
    public string GetClaimantStatus(string socialSecurityNumber)
    {
      var request = string.Format(@"<GetClaimantStatus xmlns='{0}'>
          <SSN>{1}</SSN>
        </GetClaimantStatus>",
          EKOSSettings.EKOSNamespaceUri,
          socialSecurityNumber);

      return GetBaseRequest(request);
    }

    /// <summary>
    /// Gets the job seeker version.
    /// </summary>
    /// <param name="jobSeekerName">The job seeker's name.</param>
    /// <returns></returns>
    public string GetSeekerByUsername(string jobSeekerName)
    {
      var request = string.Format(@"<ValidateUser xmlns='{0}'>
          <UserName>{1}</UserName>
        </ValidateUser>",
          EKOSSettings.EKOSNamespaceUri,
          jobSeekerName);

      return GetBaseRequest(request);
    }

    /// <summary>
    /// Gets the job seeker version.
    /// </summary>
    /// <param name="jobSeekerId">The (external) job seeker id.</param>
    /// <returns></returns>
    public string GetJobSeekerVersion(string jobSeekerId)
    {
      var request = string.Format(@"<GetSeekerVersion xmlns='{0}'>
          <SeekerID>{1}</SeekerID>
        </GetSeekerVersion>",
          EKOSSettings.EKOSNamespaceUri,
          jobSeekerId);

      return GetBaseRequest(request);
    }

		/// <summary>
		/// Gets the job seeker id by SSN.
		/// </summary>
		/// <param name="ssn">The job seeker's ssn</param>
		/// <returns>The (external) job seeker id.</returns>
		public string GetJobSeekerId(string ssn)
		{
			var request = string.Format(@"<GetSeekerId xmlns='{0}'>
          <SSN>{1}</SSN>
        </GetSeekerId>",
					EKOSSettings.EKOSNamespaceUri,
					ssn);

			return GetBaseRequest(request);
		}
  }
}
