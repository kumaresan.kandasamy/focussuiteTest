﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;

using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Services.Integration.Implementations.EKOS.CommandBuilders
{
  public class JobCommandBuilder : CommandBuilder
  {
    public JobCommandBuilder(ClientSettings clientSettings, List<ExternalLookUpItemDto> externalLookUpItems) : base(clientSettings, externalLookUpItems)
    {
    }

    /// <summary>
    /// Gets the job id
    /// </summary>
    /// <param name="jobId">The external job id</param>
    /// <returns></returns>
    public string GetJobInformation(string jobId)
    {
      var request = string.Format(@"<GetJobInformation xmlns='{0}'>
          <JobID>{1}</JobID>
        </GetJobInformation>",
          EKOSSettings.EKOSNamespaceUri,
          jobId);

      return GetBaseRequest(request);
    }
  }
}
