﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives


using System.Collections.Generic;

using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Services.Integration.Implementations.EKOS.CommandBuilders
{
  public class CommandBuilder
  {
    protected ClientSettings EKOSSettings { get; set; }

    internal readonly List<ExternalLookUpItemDto> ExternalLookUpItems;

    protected CommandBuilder(ClientSettings clientSettings, List<ExternalLookUpItemDto> externalLookUpItems)
    {
      EKOSSettings = clientSettings;
      ExternalLookUpItems = externalLookUpItems;
    }

    /// <summary>
    /// Formats a request into a full SOAP request
    /// </summary>
    /// <param name="request">The body of the request</param>
    /// <returns>The full SOAP XML</returns>
    internal string GetBaseRequest(string request)
    {
      return string.Format(@"<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
  <soap:Header>
    <AuthHeader xmlns='{0}'>
      <UserName>{1}</UserName>
      <Password>{2}</Password>
    </AuthHeader>
  </soap:Header>
  <soap:Body>
    {3}
  </soap:Body>
</soap:Envelope>",
                 EKOSSettings.EKOSNamespaceUri,
                 EKOSSettings.EkosServiceUserName,
                 EKOSSettings.EkosServicePassword,
                 request);
    }
  }
}
