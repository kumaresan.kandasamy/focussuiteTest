﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;

using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Services.Integration.Implementations.EKOS.CommandBuilders
{
  public class StaffCommandBuilder : CommandBuilder
  {
    public StaffCommandBuilder(ClientSettings clientSettings, List<ExternalLookUpItemDto> externalLookUpItems) : base(clientSettings, externalLookUpItems)
    {
    }

    /// <summary>
    /// Gets the staff member's profile
    /// </summary>
    /// <param name="userName">The staff member's user name</param>
    /// <returns></returns>
    public string GetStaffUser(string userName)
    {
      var request = string.Format(@"<GetStaffProfile xmlns='{0}'>
          <StaffUserName>{1}</StaffUserName>
        </GetStaffProfile>",
          EKOSSettings.EKOSNamespaceUri,
          userName);

      return GetBaseRequest(request);
    }

  }
}
