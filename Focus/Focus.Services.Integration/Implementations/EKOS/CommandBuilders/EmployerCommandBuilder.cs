﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;

using Focus.Core.DataTransferObjects.FocusCore;

using Framework.Core;

#endregion

namespace Focus.Services.Integration.Implementations.EKOS.CommandBuilders
{
  public class EmployerCommandBuilder : CommandBuilder
  {
    public EmployerCommandBuilder(ClientSettings clientSettings, List<ExternalLookUpItemDto> externalLookUpItems) : base(clientSettings, externalLookUpItems)
    {
    }

    /// <summary>
    /// Gets the employer by fein.
    /// </summary>
    /// <param name="federalEmployerIdentificationNumber">The federal employer identification number.</param>
    /// <returns></returns>
    public string GetEmployerByFein(string federalEmployerIdentificationNumber)
    {
      var request = string.Format(@"<ValidateFEIN xmlns='{0}'>
          <EmployerFEIN>{1}</EmployerFEIN>
        </ValidateFEIN>",
          EKOSSettings.EKOSNamespaceUri,
          federalEmployerIdentificationNumber.Replace("-", "").HtmlEncode());

      return GetBaseRequest(request);
    }

    /// <summary>
    /// Gets the employer version.
    /// </summary>
    /// <param name="employerId">The (external) employer id.</param>
    /// <returns></returns>
    public string GetEmployerVersion(string employerId)
    {
      var request = string.Format(@"<GetEmployerVersion xmlns='{0}'>
          <EmployerID>{1}</EmployerID>
        </GetEmployerVersion>",
          EKOSSettings.EKOSNamespaceUri,
          employerId);

      return GetBaseRequest(request);
    }
  }
}
