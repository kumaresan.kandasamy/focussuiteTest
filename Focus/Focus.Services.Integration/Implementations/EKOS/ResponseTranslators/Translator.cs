﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

using Focus.Core.Models.Integration;
using Focus.Services.Integration.Helpers;
using Framework.Core;

#endregion

namespace Focus.Services.Integration.Implementations.EKOS.ResponseTranslators
{
  public class Translator
  {
    private readonly ClientSettings _clientSettings;

    public Translator(ClientSettings clientSettings)
    {
      _clientSettings = clientSettings;
    }

    #region Employer

    /// <summary>
    /// XML to employer, includes exployers employees
    /// </summary>
    /// <param name="employerXml">The employer XML.</param>
    /// <returns>The Employer Model</returns>
    public EmployerModel XmlToEmployer(string employerXml)
    {
      if (employerXml.IsNotNullOrEmpty())
      {
        var responseXml = XDocument.Parse(employerXml);

        EmployerModel employer = null;

        var employerElement = responseXml.Descendants("Table1").FirstOrDefault();

        if (employerElement.IsNotNull())
        {
          var isFEINExists = XmlHelpers.GetXmlElementStringValue(employerElement, "IsFEINExists").IsIn("Y", "1");
          if (isFEINExists)
          {
            employer = new EmployerModel
            {
              Name = XmlHelpers.GetXmlElementStringValue(employerElement, "CompanyName"),
              Phone = XmlHelpers.GetXmlElementStringValue(employerElement, "ContactPhone"),
              Url = XmlHelpers.GetXmlElementStringValue(employerElement, "CompanyURL"),
              MoreDetailsRequired = true
            };

            var addressLine = XmlHelpers.GetXmlElementStringValue(employerElement, "CompanyAddress");
            if (addressLine.IsNotNullOrEmpty())
              employer.Address = ConvertStringToAddress(addressLine);
          }

        }

        return employer;
      }

      return null;
    }

    /// <summary>
    /// XML to employer version
    /// </summary>
    /// <param name="versionXml">The XML response containing the version number.</param>
    /// <returns>The version number of the employer</returns>
    public string XmlToEmployerVersion(string versionXml)
    {
      return XmlToVersion(versionXml, "GetEmployerVersionResult");
    }

    #endregion

    #region Job Seeker

    /// <summary>
    /// XML to claimant status
    /// </summary>
    /// <param name="claimantStatusXml">The SOAP response XML.</param>
    /// <returns>The claimant status of the job seeker</returns>
    public string XmlToClaimantStatus(string claimantStatusXml)
    {
      var claimantStatus = "";
      if (claimantStatusXml.IsNotNullOrEmpty())
      {
        var responseXml = XDocument.Parse(claimantStatusXml);

        var claimantStatusElement = responseXml.Descendants(XName.Get("GetClaimantStatusResult", _clientSettings.EKOSNamespaceUri)).FirstOrDefault();

        if (claimantStatusElement.IsNotNull())
          claimantStatus = claimantStatusElement.Value;
      }

      return claimantStatus;
    }


		/// <summary>
		/// XML to job version
		/// </summary>
		/// <param name="jobXml">The SOAP response XML.</param>
		/// <returns>The job seeker version</returns>
		public string XmlToJobVersion(string jobXml)
		{
			if (jobXml.IsNotNullOrEmpty())
			{
				var responseXml = XDocument.Parse(jobXml);
				var referralsTableElement = responseXml.Descendants("Table").FirstOrDefault();

				if (referralsTableElement.IsNotNull())
				{
					return XmlHelpers.GetXmlElementStringValue(referralsTableElement, "VERSION");
				}
			}

			return string.Empty;
		}

    /// <summary>
    /// XML to referrals counts
    /// </summary>
    /// <param name="referralsXml">The SOAP response XML.</param>
    /// <returns>A tuple containing current referrals, and maximum allowed referrals</returns>
    public Tuple<int, int> XmlToReferralCounts(string referralsXml)
    {
      if (referralsXml.IsNotNullOrEmpty())
      {
        var responseXml = XDocument.Parse(referralsXml);
        var referralsTableElement = responseXml.Descendants("Table").FirstOrDefault();

        if (referralsTableElement.IsNotNull())
        {
          var totalReferralCount = XmlHelpers.GetXmlElementIntValue(referralsTableElement, "REFERRALPROVIDEDCOUN");
          var maxReferralCount = XmlHelpers.GetXmlElementIntValue(referralsTableElement, "REFERRALDESIREDCOUNT");

          return new Tuple<int, int>(totalReferralCount, maxReferralCount);
        }
      }

      return new Tuple<int, int>(0, 0);
    }
    
    /// <summary>
    /// XML to job seeker version
    /// </summary>
    /// <param name="versionXml">The XML response containing the version number.</param>
    /// <returns>The version number of the job seeker</returns>
    public string XmlToJobSeekerVersion(string versionXml)
    {
      return XmlToVersion(versionXml, "GetSeekerVersionResult");
    }

		/// <summary>
		/// XML to job seeker id
		/// </summary>
		/// <param name="responseXml">The XML response containing the external id.</param>
		/// <returns>The external id of the job seeker</returns>
		public string XmlToJobSeekerId(string responseXml)
		{
			return XmlToVersion(responseXml, "GetSeekerIdResult");
		}

    /// <summary>
    /// XML to job seeker model
    /// </summary>
    /// <param name="jobSeekerXml">The XML response containing the job seeker details.</param>
    /// <returns>The Job Seeker Model</returns>
    public JobSeekerModel XmlToJobSeekerUser(string jobSeekerXml)
    {
      if (jobSeekerXml.IsNotNullOrEmpty())
      {
        var responseXml = XDocument.Parse(jobSeekerXml);

        JobSeekerModel jobSeeker = null;

        var jobSeekerElement = responseXml.Descendants("Table").FirstOrDefault();
        if (jobSeekerElement.IsNotNull())
        {
          jobSeeker = new JobSeekerModel
          {
            ExternalId = XmlHelpers.GetXmlElementStringValue(jobSeekerElement, "SEEKER_ID")
          };
        }

        return jobSeeker;
      }

      return null;
    }

    #endregion

    #region Staff

    /// <summary>
    /// XML to staff authentication model
    /// </summary>
    /// <param name="staffProfileXml">The XML response containing the staff profile details.</param>
    /// <returns>The Job Seeker Model</returns>
    public StaffModel XmlToStaffModel(string staffProfileXml)
    {
      if (staffProfileXml.IsNotNullOrEmpty())
      {
        var responseXml = XDocument.Parse(staffProfileXml);

        StaffModel staffModel = null;

        var staffElement = responseXml.Descendants("Table").FirstOrDefault();
        if (staffElement.IsNotNull())
        {
          // Dataset that is contained in the table is as follows
          //
          // <ADMIN_ID>99999</ADMIN_ID>
          // <ADMINSTATUSFLAG>Active_Admin</ADMINSTATUSFLAG>
          // <LASTNAME>what is a tester</LASTNAME>
          // <FIRSTNAME>tester</FIRSTNAME>
          // <MIDDLEINITIAL>M</MIDDLEINITIAL>
          // <PASSWORD>secret</PASSWORD>
          // <USERNAME>tester1</USERNAME>
          // <EMAIL>tester@test.com</EMAIL>
          // <OFFICE_ID>KY999999</OFFICE_ID>
          // <SECURITYGROUP>41696</SECURITYGROUP>
          // <TITLE></TITLE>
          // <PHONEPRI>1 555 555 555</PHONEPRI>

          staffModel = new StaffModel
          {
            ExternalId = XmlHelpers.GetXmlElementStringValue(staffElement, "ADMIN_ID"),
            Password = XmlHelpers.GetXmlElementStringValue(staffElement, "PASSWORD"),
            Version = XmlHelpers.GetXmlElementStringValue(staffElement, "VERSION"),
            Email = XmlHelpers.GetXmlElementStringValue(staffElement, "EMAIL"),
            FirstName = XmlHelpers.GetXmlElementStringValue(staffElement, "FIRSTNAME"),
            MiddleInitial = XmlHelpers.GetXmlElementStringValue(staffElement, "MIDDLEINITIAL"),
            Surname = XmlHelpers.GetXmlElementStringValue(staffElement, "LASTNAME"),
            JobTitle = XmlHelpers.GetXmlElementStringValue(staffElement, "TITLE"),
            PhoneNumber = XmlHelpers.GetXmlElementStringValue(staffElement, "PHONEPRI"),
            AuthModel = new AuthenticationModel
            {
              ExternalAdminId = XmlHelpers.GetXmlElementStringValue(staffElement, "ADMIN_ID"),
              ExternalOfficeId = XmlHelpers.GetXmlElementStringValue(staffElement, "OFFICE_ID"),
            }
          };
        }

        return staffModel;
      }

      return null;
    }


    #endregion

    #region Helpers

    /// <summary>
    /// Finds the errors.
    /// </summary>
    /// <param name="response">The response.</param>
    /// <returns>An error message</returns>
    public string FindErrors(string response)
    {
      if (response.IsNullOrEmpty()) return "";

      var responseXml = XDocument.Parse(response);
      var errorList = responseXml.Descendants("faultstring").Select(element => element.Value).ToList();

      if (errorList.Count > 0)
      {
        var errors = new StringBuilder("EKOS Error(s): ");
        foreach (var error in errorList)
        {
          errors.Append(error);
          errors.Append("\n");
        }
        return errors.ToString();
      }

      return "";
    }

    #endregion

    #region Private

    /// <summary>
    /// Converts a string contain an address (on one line) to an AddressModel
    /// </summary>
    /// <param name="address">The address of the form "line, (line,) city-zip</param>
    /// <returns>The adddress model</returns>
    private static AddressModel ConvertStringToAddress(string address)
    {
      var addressArray = address.Split(',');

      return new AddressModel { AddressLine1 = addressArray[1], City = addressArray[2] };

      //var matches = Regex.Matches(address, @"(.*?,)+(.*?-\d*)?");

      //var addressModel = new AddressModel
      //{
      //  AddressLine1 = matches[0].Groups[1].Captures[0].Value.TrimEnd(new [] { ' ', ',' })
      //};

      //if (matches[0].Groups.Count > 2)
      //{
      //  var cityZip = matches[0].Groups[2].Value;

      //  var splitCityZip = cityZip.Split(new[] {'-'});

      //  addressModel.City = splitCityZip[0];
      //  addressModel.PostCode = splitCityZip[1];
      //}
    }

    /// <summary>
    /// XML to version number
    /// </summary>
    /// <param name="versionXml">The XML response containing the version number.</param>
    /// <param name="elementName">Name of the element.</param>
    /// <returns>The version number</returns>
    private string XmlToVersion(string versionXml, string elementName)
    {
      if (versionXml.IsNotNullOrEmpty())
      {
        var responseXml = XDocument.Parse(versionXml);

        var versionElement = responseXml.Descendants(XName.Get(elementName, _clientSettings.EKOSNamespaceUri)).FirstOrDefault();
        if (versionElement.IsNotNull())
          return versionElement.Value;
      }

      return null;
    }

    #endregion
  }
}
