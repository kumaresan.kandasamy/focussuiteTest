﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using Focus.Core;
using Focus.Core.IntegrationMessages;
using Focus.Core.Models.Integration;
using Framework.Core;
using Framework.DataAccess;

#endregion

namespace Focus.Services.Integration.Implementations.Mock
{
  public class MockClientRepository : ClientRepository, IIntegrationRepository
  {

		protected override IntegrationPoint[] GetImplementedIntegrationPoints()
		{
			return new[]
			{
				IntegrationPoint.AuthenticateJobSeeker, IntegrationPoint.AssignAdminToJobOrder,IntegrationPoint.GetEmployer, 
				IntegrationPoint.IsUIClaimant, IntegrationPoint.UpdateJobStatus, IntegrationPoint.GetJobSeeker, 			
			};
		}

    #region Implementation of IClientRepository

		public LogActionResponse LogAction(LogActionRequest request)
		{
			throw new NotImplementedException();
		}

    public GetJobSeekerResponse GetJobSeeker(GetJobSeekerRequest request)
    {
			JobSeekerModel jobSeeker;

			if (request.JobSeeker.SocialSecurityNumber.IsNotNullOrEmpty() && request.JobSeeker.SocialSecurityNumber == "987654321")
			{
				jobSeeker = new JobSeekerModel
				{
					EmailAddress = "test@mock.com",
					ExternalId = request.JobSeeker.ExternalId ?? "mock1",
					ExternalUsername = "Mock",
					ExternalPassword = "Password1",
					FirstName = "Mock",
					LastName = "Mockson",
					SocialSecurityNumber = request.JobSeeker.SocialSecurityNumber ?? "123456789",
					AddressLine1 = "Line1",
					AddressTownCity = "Bluegrove",
					AddressPostcodeZip = "76352",
					PrimaryPhone = "",
					AddressStateId = 12896,
					DateOfBirth = new DateTime(1980, 01, 01)
				};
			}
			else
			{
				jobSeeker = new JobSeekerModel
				{
					EmailAddress = "test@mock.com",
					ExternalId = request.JobSeeker.ExternalId ?? "mock1",
					ExternalUsername = "Mock",
					ExternalPassword = "Password1",
					FirstName = "Mock",
					LastName = "Mockson",
					SocialSecurityNumber = request.JobSeeker.SocialSecurityNumber ?? "123456789",
					AddressLine1 = "Line1",
					AddressTownCity = "Bluegrove",
					AddressPostcodeZip = "76352",
					PrimaryPhone = "1111111111",
					AddressStateId = 12896,
					DateOfBirth = new DateTime(1980, 01, 01)
				};
			}
      
      // This mocks the external system returning no job seeker (indicated when SSN or External Id contain nothing but Zs or 0s)
      if ((request.JobSeeker.SocialSecurityNumber.IsNotNullOrEmpty() && request.JobSeeker.SocialSecurityNumber.Replace("0", "").Replace("Z", "").Length == 0)
      || (request.JobSeeker.ExternalId.IsNotNullOrEmpty() && request.JobSeeker.ExternalId.Replace("0", "").Replace("Z", "").Length == 0))
        jobSeeker = null;

      return new GetJobSeekerResponse(request) { JobSeeker = jobSeeker };
    }

    public GetJobSeekerResponse GetJobSeekerBySocialSecurityNumber(GetJobSeekerRequest request)
    {
      return new GetJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AuthenticateJobSeekerResponse AuthenticateJobSeeker(AuthenticateJobSeekerRequest request)
    {
      var mockId = request.AuthenticationDetails.IsNotNull() && request.AuthenticationDetails.ContainsKey("mockid")
        ? request.AuthenticationDetails["mockid"]
        : null;

      var jobSeeker = new JobSeekerModel
      {
        EmailAddress = "test@mock.com",
        ExternalId = mockId ?? "mock1",
        ExternalUsername = "Mock",
        ExternalPassword = "Password1",
        FirstName = "Mock",
        LastName = "Mockson",
        SocialSecurityNumber = "123456789",
        AddressLine1 = "Line1",
        AddressTownCity = "Bluegrove",
        AddressStateId = 12896,
        AddressPostcodeZip = "76352",
        PrimaryPhone = "1111111111",
        DateOfBirth = new DateTime(1980, 01, 01)
      };

      if (mockId == "NOUSER")
        jobSeeker = null;

      return new AuthenticateJobSeekerResponse(request) { JobSeeker = jobSeeker };
    }

    public SaveJobSeekerResponse SaveJobSeeker(SaveJobSeekerRequest request)
    {
      throw new NotImplementedException();
    }

    public JobSeekerLoginResponse JobSeekerLogin(JobSeekerLoginRequest request)
    {
      throw new NotImplementedException();
    }

    public SaveJobMatchesResponse SaveJobMatches(SaveJobMatchesRequest request)
    {
      throw new NotImplementedException();
    }

    public AssignJobSeekerActivityResponse AssignJobSeekerActivity(AssignJobSeekerActivityRequest request)
    {
      throw new NotImplementedException();
    }

    public AssignAdminToJobSeekerResponse AssignAdminToJobSeeker(AssignAdminToJobSeekerRequest request)
    {
      throw new NotImplementedException();
    }

    public IsUIClaimantResponse IsUIClaimant(IsUIClaimantRequest request)
    {
      if (request.ExternalJobSeekerId.IsNotNull())
      {
        return new IsUIClaimantResponse(request)
        {
          Outcome = IntegrationOutcome.Success,
          IsUIClaimant = (request.ExternalJobSeekerId.AsInt() % 2 == 0)
        };
      }

      return new IsUIClaimantResponse(request)
      {
        Outcome = IntegrationOutcome.Failure,
        IsUIClaimant = false
      };
    }

    public ChangeUserPasswordResponse ChangeUserPassword(ChangeUserPasswordRequest request)
    {
      throw new NotImplementedException();
    }

    public SaveJobResponse SaveJob(SaveJobRequest request)
    {
      throw new NotImplementedException();
    }

    public GetJobResponse GetJob(GetJobRequest request)
    {
      throw new NotImplementedException();
    }

    public ReferJobSeekerResponse ReferJobSeeker(ReferJobSeekerRequest request)
    {
      throw new NotImplementedException();
    }

    public SetApplicationStatusResponse SetApplicationStatus(SetApplicationStatusRequest request)
    {
      throw new NotImplementedException();
    }

    public GetEmployerResponse GetEmployer(GetEmployerRequest request)
    {
      var employer = new EmployerModel
                       {
                         FederalEmployerIdentificationNumber = request.FEIN,
                         ExternalId = "000",
                         Status = EmployerStatusTypes.Active,
                         Name = "Mock Company Name",
                         LegalName = "mock Legal Name",
                         IsValidFederalEmployerIdentificationNumber = true,
                         StateEmployerIdentificationNumber = "888",
                         IsRegistrationComplete = false,
                         ExternalOfficeId = "222",
                         Phone = "5555555555",
                         AltPhone = "5555",
                         Fax = "5",
                         Url = "http://dvkdj.comomo.comx",
                         Naics = "1111",
                         BusinessDescription = "Mock company business description",
                         PublicTransportAccessible = true,
                         Address = new AddressModel
                                     {
                                       AddressLine1 = "123 Mock Street",
                                       AddressLine2 = "The Mock Area",
                                       AddressLine3 = "Mock Address Live 3",
                                       PostCode = "11147",
                                       ExternalStateId = "NY",
                                       ExternalCountryId = "US",
                                       City = "Mock city"
                                     }
                       };

      employer.Employees.Add(new EmployeeModel
                               {
                                 FirstName = "Cliff",
                                 LastName = "Hilltington",
                                 ExternalEmployerId = "001",
                                 Phone = "5555555555555",
                                 AltPhone = "5555",
                                 JobTitle = "Test data enterer",
                                 Email = "cliff@mocktestemail.comx",
                                 Address = new AddressModel
                                             {
                                               AddressLine1 = "123 EmployeeMock Street",
                                               AddressLine2 = "The EmployeeMock Area",
                                               AddressLine3 = "EmployeeMock Address Line 3",
                                               PostCode = "02672",
                                               ExternalStateId = "MA",
                                               ExternalCountyId = "25001",
                                               ExternalCountryId = "US",
                                               City = "EmployeeMock city"
                                             }
                               });

      employer.Employees.Add(new EmployeeModel
                               {
                                 FirstName = "Rory",
                                 LastName = "Maplear",
                                 ExternalEmployerId = "002",
                                 Phone = "5555555555556",
                                 AltPhone = "5556",
                                 JobTitle = "Test data enterer 2",
                                 Email = "rory@mocktestemail.comx",
                                 Address = new AddressModel
                                             {
                                               AddressLine1 = "12 EmployeeMock Street",
                                               AddressLine2 = "EmployeeMock Area",
                                               AddressLine3 = "EmployeeMock Address 3",
                                               PostCode = "15040",
                                               ExternalStateId = "PA",
                                               ExternalCountryId = "US",
                                               City = "EmployeeMock city"
                                             }
                               });


      return new GetEmployerResponse(request) { Employer = employer };
    }

    public SaveEmployerResponse SaveEmployer(SaveEmployerRequest request)
    {
      throw new NotImplementedException();
    }

    public AssignAdminToEmployerResponse AssignAdminToEmployer(AssignAdminToEmployerRequest request)
    {
      throw new NotImplementedException();
    }

    public AssignEmployerActivityResponse AssignEmployerActivity(AssignEmployerActivityRequest request)
    {
      throw new NotImplementedException();
    }

    public SaveEmployeeResponse SaveEmployee(SaveEmployeeRequest request)
    {
      throw new NotImplementedException();
    }

    public UpdateEmployeeStatusResponse UpdateEmployeeStatus(UpdateEmployeeStatusRequest request)
    {
      throw new NotImplementedException();
    }

    public AuthenticateStaffResponse AuthenticateStaffUser(AuthenticateStaffRequest request)
    {
      throw new NotImplementedException();
    }

    public DisableJobSeekersReportResponse DisableJobSeekersReport(DisableJobSeekersReportRequest request)
    {
      return new DisableJobSeekersReportResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    #endregion
  }
}
