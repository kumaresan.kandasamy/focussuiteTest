﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Core.IntegrationMessages;
using Framework.Core;

#endregion

namespace Focus.Services.Integration.Implementations
{
  public abstract class ClientRepository
  {
		public bool IsImplemented(IntegrationPoint integrationPoint)
		{
			var implementedIntegrationPoints = GetImplementedIntegrationPoints();

			return implementedIntegrationPoints.IsNotNullOrEmpty() && GetImplementedIntegrationPoints().Any(x => x == integrationPoint);
		}

	  protected abstract IntegrationPoint[] GetImplementedIntegrationPoints();

		public AssignAdminToJobOrderResponse AssignAdminToJobOrder(AssignAdminToJobOrderRequest request)
		{
			return new AssignAdminToJobOrderResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

    public virtual string GetActivityId(ActionTypes actionType, bool selfServicedActivity)
    {
      return null;
    }

		public virtual bool IsActivityStaffAssisted(ActionTypes actionType)
		{
			return false;
		}

		public virtual int GetActivityRestriction(AssignJobSeekerActivityRequest request)
		{
			return 0;
		}

		/// <summary>
		/// Whether the External Office Id should be updated
		/// </summary>
		/// <param name="request">The current request</param>
		/// <returns>A boolean indicating if the office id should be updated</returns>
		public virtual bool OfficeRequiresUpdate(IIntegrationRequest request)
		{
			return false;
		}

		public virtual IntegrationAuthentication AuthenticationSupported(FocusModules module)
		{
			return IntegrationAuthentication.None;
		}
  }
}
