﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using Focus.Core;
using Focus.Core.IntegrationMessages;
using Framework.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

#endregion

namespace Focus.Services.Integration.Implementations.Georgia
{
	public class GeorgiaClientRepository : ClientRepository, IIntegrationRepository
  {
    private readonly ClientSettings _clientSettings;

		public GeorgiaClientRepository(string settings)
		{
			_clientSettings = (ClientSettings)JsonConvert.DeserializeObject(settings, typeof(ClientSettings));
		}

		protected override IntegrationPoint[] GetImplementedIntegrationPoints()
		{
			return new[] {IntegrationPoint.LogAction, IntegrationPoint.DisableJobSeekersReport};
		}

		#region Implementation of IClientRepository

		public LogActionResponse LogAction(LogActionRequest request)
		{
			var response = new LogActionResponse(request);

			try
			{
				if (request.ActionData.IsNullOrEmpty())
				{
					response.Outcome = IntegrationOutcome.Success;
					return response;
				}

				var endpoint = _clientSettings.CharacteristicsEndpoint;
				if (request.ActionType.IsIn(ActionTypes.PostJobToLens, ActionTypes.UnregisterJobFromLens, ActionTypes.UpdateJobAssignedOffice))
				{
					endpoint = _clientSettings.ActiveJobsEndpoint;
				}

				var webResponse = SendWebRequest(endpoint, request.ActionData);
				response.IntegrationBreadcrumbs.Add(webResponse);

				if (webResponse.StatusCode != HttpStatusCode.OK)
					throw new InvalidOperationException(string.Format("BG_Activities returned status {0} - {1}", webResponse.StatusCode, webResponse.StatusDescription));
			}
			catch (Exception e)
			{
				response.Outcome = IntegrationOutcome.Failure;
				response.Exception = e;
			}
			return response;
		}

		public GetJobSeekerResponse GetJobSeeker(GetJobSeekerRequest request)
		{
			return new GetJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public GetJobSeekerResponse GetJobSeekerBySocialSecurityNumber(GetJobSeekerRequest request)
		{
			return new GetJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public AuthenticateJobSeekerResponse AuthenticateJobSeeker(AuthenticateJobSeekerRequest request)
		{
			return new AuthenticateJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public SaveJobSeekerResponse SaveJobSeeker(SaveJobSeekerRequest request)
		{
			return new SaveJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public SaveJobMatchesResponse SaveJobMatches(SaveJobMatchesRequest request)
		{
			return new SaveJobMatchesResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public AssignJobSeekerActivityResponse AssignJobSeekerActivity(AssignJobSeekerActivityRequest request)
		{
			return new AssignJobSeekerActivityResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public AssignAdminToJobSeekerResponse AssignAdminToJobSeeker(AssignAdminToJobSeekerRequest request)
		{
			return new AssignAdminToJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public IsUIClaimantResponse IsUIClaimant(IsUIClaimantRequest request)
		{
			return new IsUIClaimantResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public JobSeekerLoginResponse JobSeekerLogin(JobSeekerLoginRequest request)
		{
			return new JobSeekerLoginResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public ChangeUserPasswordResponse ChangeUserPassword(ChangeUserPasswordRequest request)
		{
			return new ChangeUserPasswordResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public SaveJobResponse SaveJob(SaveJobRequest request)
		{
			return new SaveJobResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public GetJobResponse GetJob(GetJobRequest request)
		{
			return new GetJobResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public ReferJobSeekerResponse ReferJobSeeker(ReferJobSeekerRequest request)
		{
			return new ReferJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public SetApplicationStatusResponse SetApplicationStatus(SetApplicationStatusRequest request)
		{
			return new SetApplicationStatusResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public GetEmployerResponse GetEmployer(GetEmployerRequest request)
		{
			return new GetEmployerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public SaveEmployerResponse SaveEmployer(SaveEmployerRequest request)
		{
			return new SaveEmployerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public AssignAdminToEmployerResponse AssignAdminToEmployer(AssignAdminToEmployerRequest request)
		{
			return new AssignAdminToEmployerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public AssignEmployerActivityResponse AssignEmployerActivity(AssignEmployerActivityRequest request)
		{
			return new AssignEmployerActivityResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public SaveEmployeeResponse SaveEmployee(SaveEmployeeRequest request)
		{
			return new SaveEmployeeResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public UpdateEmployeeStatusResponse UpdateEmployeeStatus(UpdateEmployeeStatusRequest request)
		{
			return new UpdateEmployeeStatusResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

		public AuthenticateStaffResponse AuthenticateStaffUser(AuthenticateStaffRequest request)
		{
			return new AuthenticateStaffResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

	  public DisableJobSeekersReportResponse DisableJobSeekersReport(DisableJobSeekersReportRequest request)
	  {          
      var response = new DisableJobSeekersReportResponse(request);

      try
      {
        if (request.JobSeekerIdList.IsNullOrEmpty())
        {
          response.Outcome = IntegrationOutcome.Success;
          return response;
        }

        var json = JsonConvert.SerializeObject(request.JobSeekerIdList, Formatting.Indented, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.None });
        var endpoint = _clientSettings.DisableJobSeekersReportEndpoint;

        var webResponse = SendWebRequest(endpoint, json);
				response.IntegrationBreadcrumbs.Add(webResponse);

        if (webResponse.StatusCode != HttpStatusCode.OK)
          throw new InvalidOperationException(string.Format("BG_Activities returned status {0} - {1}", webResponse.StatusCode, webResponse.StatusDescription));
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }
      return response;
	  }
    
	  #endregion

		#region Shared Methods

	  private RequestResponse SendWebRequest(string endpoint, string actionData)
	  {
		  var response = new RequestResponse();
			long webRequestElapsedMilliseconds = -1;
			var methodLog = new StringBuilder();

		  try
		  {
				methodLog.AppendFormat("{0} : Cleaning up request content", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"));

			  var encoder = new UTF8Encoding();
				var content = encoder.GetBytes(actionData);
			  var contentLength = content.Length;

				if (ServicePointManager.DefaultConnectionLimit < _clientSettings.ConnectionLimit)
					ServicePointManager.DefaultConnectionLimit = _clientSettings.ConnectionLimit;

				response.OutgoingRequest = actionData;
			  response.Url = endpoint;

				methodLog.AppendFormat("{0} : Creating web request : {1}", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"), endpoint);
                methodLog.AppendFormat("{0} : web request : {1}", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"), actionData);

			  var webRequest = (HttpWebRequest) WebRequest.Create(endpoint);
			  webRequest.Method = "POST";
			  webRequest.ContentType = "application/json";
			  webRequest.ContentLength = contentLength;

				methodLog.AppendFormat("{0} : Setting timeout to {1}ms", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"), _clientSettings.WebRequestTimeout);

				webRequest.Timeout = _clientSettings.WebRequestTimeout;

				methodLog.AppendFormat("{0} : Setting request content", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"));

				using (var inputStream = webRequest.GetRequestStream())
				{
					inputStream.Write(content, 0, contentLength);
					inputStream.Close();
				}

				var stopwatch = new Stopwatch();

			  try
			  {
					stopwatch.Start();

					methodLog.AppendFormat("{0} : Sending request", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"));

					response.StartTime = DateTime.Now;

				  using (var webResponse = (HttpWebResponse)webRequest.GetResponse())
				  {
						response.EndTime = DateTime.Now;
						response.StatusCode = webResponse.StatusCode;
						response.StatusDescription = webResponse.StatusDescription;

						methodLog.AppendFormat("{0} : Getting request content", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"));

						using (var outputStream = webResponse.GetResponseStream())
						{
							if (outputStream != null)
							{
								using (var reader = new StreamReader(outputStream))
								{
									response.IncomingResponse = reader.ReadToEnd();
									reader.Close();
								}
							}
						}

						webResponse.Close();
				  }


			  }
			  catch (Exception)
			  {
					methodLog.AppendFormat("{0} : Request failed", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"));

					stopwatch.Stop();
					webRequestElapsedMilliseconds = stopwatch.ElapsedMilliseconds;

					throw;
			  }
		  }
		  catch (WebException ex)
		  {
				var httpWebResponse = ex.Response as HttpWebResponse;
				if (httpWebResponse.IsNotNull())
				{
					response.StatusCode = httpWebResponse.StatusCode;
					response.StatusDescription = httpWebResponse.StatusDescription;
				}
				else
				{
					response.StatusCode = null;
					response.StatusDescription = ex.Status.ToString();
				}

				throw new Exception(
					string.Format("Georgia: Type: {0}. Message: {1}. Status: {2}, Inner Exception: {3}. Request Time: {4}, Log: {5}",
						"WebException", ex.Message, ex.Status, ex.InnerException.IsNull() ? string.Empty : ex.InnerException.Message,
						webRequestElapsedMilliseconds, methodLog));
		  }
		  catch (Exception ex)
		  {
				throw new Exception(
					string.Format("Georgia: Type: {0}. Message: {1}. Inner Exception: {2}. Request Time: {3}, Log: {4}",
						"WebException", ex.Message, ex.InnerException.IsNull() ? string.Empty : ex.InnerException.Message,
						webRequestElapsedMilliseconds, methodLog));
		  }

		  return response;
	  }

	  #endregion
	}
}
