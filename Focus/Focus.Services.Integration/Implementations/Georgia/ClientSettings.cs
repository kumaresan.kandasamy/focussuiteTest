﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

#endregion

namespace Focus.Services.Integration.Implementations.Georgia
{
  [Serializable]
  public class ClientSettings
  {
		[DataMember]
		public string ActiveJobsEndpoint { get; set; }

		[DataMember]
		public string CharacteristicsEndpoint { get; set; }

    [DataMember]
		public string DisableJobSeekersReportEndpoint { get; set; }

		[DataMember]
		public int WebRequestTimeout { get; set; }

		[DataMember]
		public int ConnectionLimit { get; set; }

	  [OnDeserializing()]
	  internal void OnDeserializingMethod(StreamingContext context)
	  {
			WebRequestTimeout = 200000;
		  ConnectionLimit = 200;
	  }
  }
}
