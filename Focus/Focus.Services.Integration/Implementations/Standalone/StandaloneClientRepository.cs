﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core;
using Focus.Core.IntegrationMessages;

#endregion

namespace Focus.Services.Integration.Implementations.Standalone
{
  public class StandaloneClientRepository : ClientRepository, IIntegrationRepository
  {
		protected override IntegrationPoint[] GetImplementedIntegrationPoints()
		{
			return new IntegrationPoint[0];
		}

    #region Implementation of IClientRepository

		public LogActionResponse LogAction(LogActionRequest request)
		{
			return new LogActionResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
		}

    public GetJobSeekerResponse GetJobSeeker(GetJobSeekerRequest request)
    {
      return new GetJobSeekerResponse(request) {Outcome = IntegrationOutcome.NotImplemented};
    }

    public GetJobSeekerResponse GetJobSeekerBySocialSecurityNumber(GetJobSeekerRequest request)
    {
      return new GetJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AuthenticateJobSeekerResponse AuthenticateJobSeeker(AuthenticateJobSeekerRequest request)
    {
      return new AuthenticateJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveJobSeekerResponse SaveJobSeeker(SaveJobSeekerRequest request)
    {
      return new SaveJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public JobSeekerLoginResponse JobSeekerLogin(JobSeekerLoginRequest request)
    {
      return new JobSeekerLoginResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveJobMatchesResponse SaveJobMatches(SaveJobMatchesRequest request)
    {
      return new SaveJobMatchesResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AssignJobSeekerActivityResponse AssignJobSeekerActivity(AssignJobSeekerActivityRequest request)
    {
      return new AssignJobSeekerActivityResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AssignAdminToJobSeekerResponse AssignAdminToJobSeeker(AssignAdminToJobSeekerRequest request)
    {
      return new AssignAdminToJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public IsUIClaimantResponse IsUIClaimant(IsUIClaimantRequest request)
    {
      return new IsUIClaimantResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public ChangeUserPasswordResponse ChangeUserPassword(ChangeUserPasswordRequest request)
    {
      return new ChangeUserPasswordResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveJobResponse SaveJob(SaveJobRequest request)
    {
      return new SaveJobResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public GetJobResponse GetJob(GetJobRequest request)
    {
      return new GetJobResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public ReferJobSeekerResponse ReferJobSeeker(ReferJobSeekerRequest request)
    {
      return new ReferJobSeekerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SetApplicationStatusResponse SetApplicationStatus(SetApplicationStatusRequest request)
    {
      return new SetApplicationStatusResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public GetEmployerResponse GetEmployer(GetEmployerRequest request)
    {
      return new GetEmployerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveEmployerResponse SaveEmployer(SaveEmployerRequest request)
    {
      return new SaveEmployerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AssignAdminToEmployerResponse AssignAdminToEmployer(AssignAdminToEmployerRequest request)
    {
      return new AssignAdminToEmployerResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AssignEmployerActivityResponse AssignEmployerActivity(AssignEmployerActivityRequest request)
    {
      return new AssignEmployerActivityResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public SaveEmployeeResponse SaveEmployee(SaveEmployeeRequest request)
    {
      return new SaveEmployeeResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public UpdateEmployeeStatusResponse UpdateEmployeeStatus(UpdateEmployeeStatusRequest request)
    {
      return new UpdateEmployeeStatusResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public AuthenticateStaffResponse AuthenticateStaffUser(AuthenticateStaffRequest request)
    {
      return new AuthenticateStaffResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public DisableJobSeekersReportResponse DisableJobSeekersReport(DisableJobSeekersReportRequest request)
    {
      return new DisableJobSeekersReportResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    #endregion
  }
}
