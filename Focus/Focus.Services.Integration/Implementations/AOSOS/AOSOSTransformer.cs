﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Core.Models.Integration;
using Framework.Core;
using EducationLevel = Focus.Core.EducationLevel;

#endregion

namespace Focus.Services.Integration.Implementations.AOSOS
{
  public static class AOSOSTransformer
  {

    


    

    //#region Job Transformations

    


    ///// <summary>
    ///// Remaps the education level from ASOS to Focus
    ///// </summary>
    ///// <param name="AOSOSEducationLevel">The AOSOS education level.</param>
    ///// <returns></returns>
    //private static EducationLevels RemapEducationLevel (int AOSOSEducationLevel)
    //{
    //  switch (AOSOSEducationLevel)
    //  {
    //    case 1:
    //      return EducationLevels.None;
    //    case 2:
    //      return EducationLevels.HighSchoolDiploma;
    //    case 5:
    //      return EducationLevels.AssociatesDegree;
    //    case 6:
    //      return EducationLevels.BachelorsDegree;
    //    case 7:
    //      return EducationLevels.GraduateDegree;
    //    case 8:
    //      return EducationLevels.DoctoralDegree;
    //    default:
    //      return EducationLevels.None;
    //  }
    //}

    //public static string MapJobEducationLevel(EducationLevels? level)
    //{
    //  var returnValue = string.Empty;

    //  switch (level)
    //  {
    //    case EducationLevels.None:
    //      returnValue = "1";
    //      break;
    //    case EducationLevels.HighSchoolDiploma:
    //      returnValue = "2";
    //      break;
    //    //case EducationLevels.None:
    //    //  returnValue = "3";
    //    //  break;
    //    //case EducationLevels.None:
    //    //  returnValue = "4";
    //    //  break;
    //    case EducationLevels.AssociatesDegree:
    //      returnValue = "5";
    //      break;
    //    case EducationLevels.BachelorsDegree:
    //      returnValue = "6";
    //      break;
    //    case EducationLevels.GraduateDegree:
    //      returnValue = "7";
    //      break;
    //    case EducationLevels.DoctoralDegree:
    //      returnValue = "8";
    //      break;
    //  }

    //  return returnValue;
    //}



    //public static string MapResumeEducationLevel(Core.Models.Career.EducationLevel? level)
    //{
    //  var returnValue = string.Empty;
    //  return returnValue;
    //}


    //public static string MapGender(Genders? gender)
    //{
    //  switch (gender)
    //  {
    //    case Genders.Male:
    //      return "2";
    //      break;
    //    case Genders.Female:
    //      return "1";
    //      break;
    //    case Genders.NotDisclosed:
    //      return "3";
    //      break;
    //    default:
    //      return string.Empty;
    //      break;
    //  }
    //}


    ///// <summary>
    ///// Maps the job status.
    ///// </summary>
    ///// <param name="jobStatus">The job status.</param>
    ///// <returns></returns>
    //public static string MapJobStatus(JobStatuses jobStatus)
    //{

    //  // TODO: Map correctly
    //  var retVal = "";
    //  switch (jobStatus)
    //  {
    //    case JobStatuses.Active:
    //      retVal = "1";// "1"Open
    //      break;

    //    case JobStatuses.AwaitingEmployerApproval:
    //      retVal = "2";// "2"Pending
    //      break;

    //    //case JobStatusTypes.Referred:
    //    //  retVal = "3";// "3"Referred
    //    //  break;

    //    case JobStatuses.OnHold:
    //      retVal = "5";// "5"Suspend
    //      break;

    //    //case JobStatusTypes.Deleted: // ????
    //    //  retVal = "6";// // "6"Archived
    //    //  break;

    //    case JobStatuses.Closed:
    //      retVal = "7";// "7"Closed
    //      break;

    //    //case JobStatusTypes.Filled:
    //    //  retVal = "8";// "8"Filled
    //    //  break;
    //  }

    //  return retVal;
    //}

    


    

    //#endregion


    

    //#region Helpers

   

    

    

    

    

    

    //#endregion
  }
}
