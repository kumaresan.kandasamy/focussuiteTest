﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;

using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Services.Integration.Implementations.AOSOS.CommandBuilders
{
  public class UserCommandBuilder : CommandBuilder
  {
    public UserCommandBuilder(ClientSettings clientSettings, List<ExternalLookUpItemDto> externalLookUpItems) : base(clientSettings, externalLookUpItems)
    {
    }

    #region User Requests

    /// <summary>
    /// Authenticates the user.
    /// </summary>
    /// <param name="userName">Name of the user.</param>
    /// <param name="password">The password.</param>
    /// <returns></returns>
    public string AuthenticateUser(string userName, string password)
    {
      var request = string.Format("<?xml version='1.0' ?><osos><loginRequest security_username='{0}' security_password='{1}' method='auth'/></osos>", userName, password);
      return request;
    }

    /// <summary>
    /// Changes the user password.
    /// </summary>
    /// <param name="userName">Name of the user.</param>
    /// <param name="password">The password.</param>
    /// <param name="newPassword">The new password.</param>
    /// <returns></returns>
    public string ChangeUserPassword(string userName, string password, string newPassword)
    {
      var request = string.Format("<?xml version='1.0' ?><osos><loginRequest security_username='{0}' security_password='{1}' old_password='{1}' new_password='{2}' confirm_password='{2}' method='change_password'/></osos>", userName, password, newPassword);
      return request;
    }

    /// <summary>
    /// Gets the user information.
    /// </summary>
    /// <param name="userName">Name of the user.</param>
    /// <param name="password">The password.</param>
    /// <param name="currentOfficeId">The current office id.</param>
    /// <param name="ctAggrVersion">The ct aggr version.</param>
    /// <param name="adminId">The admin id.</param>
    /// <returns></returns>
    public string GetUserInformation(string userName, string password, string currentOfficeId, string ctAggrVersion, string adminId)
    {
      var request = string.Format("<?xml version='1.0' ?><osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}'><administrators><administrator method='select' expand='*' id='{4}'></administrator></administrators></request></osos>", userName, password, currentOfficeId, ctAggrVersion, adminId);
      return request;
    }

    #endregion
  }
}
