﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Text.RegularExpressions;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;
using Focus.Core.Models.Integration;

using Framework.Core;
using Focus.Core.Messages.OccupationService;
using Focus.Core.Criteria.Onet;
using Framework.Logging;
using Framework.ServiceLocation;

#endregion

namespace Focus.Services.Integration.Implementations.AOSOS.CommandBuilders
{
  public class JobSeekerCommandBuilder : CommandBuilder
  {
    public JobSeekerCommandBuilder(ClientSettings clientSettings, List<ExternalLookUpItemDto> externalLookUpItems)
      : base(clientSettings, externalLookUpItems)
    {
    }

    /// <summary>
    /// Gets the seeker user name by social security number.
    /// </summary>
    /// <param name="socialSecurityNumber">The social security number.</param>
    /// <returns></returns>
    public string GetSeekerBySocialSecurityNumber(string socialSecurityNumber)
    {
      //ssn,vet_pref,last_name,first_name,mi,objective,seeker_status_cd
      var request = string.Format("<seeker_quicksearch method='query' returns='username,seeker_status_cd'><ssn_1>{0}</ssn_1></seeker_quicksearch>", socialSecurityNumber.HtmlEncode());
      return GetBaseRequest(request);
    }

    /// <summary>
    /// Gets the seeker by id.
    /// </summary>
    /// <param name="seekerId">The seeker id.</param>
    /// <returns></returns>
    public string GetSeekerById(string seekerId)
    {
      var request = string.Format("<seekers><seeker method='select' expand='ssn,full_ssn,desired_countries,desired_zips,ethnic_heritages,schools,desired_titles,title_skills,vet_pref,certifications,desired_states,previous_jobs' id='{0}'></seeker></seekers>", seekerId.HtmlEncode());
      return GetBaseRequest(request);
    }

    /// <summary>
    /// Checks if JobSeeker exists.
    /// </summary>
    /// <param name="userName">Name of the user.</param>
    /// <returns></returns>
    public string GetSeekerByUsername(string userName)
    {
      var request = string.Format(
        @"<seeker_quicksearch method='query' returns='username,seeker_status_cd,first_name,last_name,ssn,full_ssn'><username_1>{0}</username_1></seeker_quicksearch>",
        userName.HtmlEncode());
      return GetBaseRequest(request);
    }

		/// <summary>
		/// Gets the XML to look up the job seeker's external ID based on their personal details
		/// </summary>
		/// <param name="firstName">The job seeker's fist name</param>
		/// <param name="lastName">The job seeker's last name</param>
		/// <param name="dateOfBirth">The job seeker's date of birth</param>
		/// <returns>The xml to query the job seeker</returns>
		public string GetSeekerByDetails(string firstName, string lastName, DateTime dateOfBirth)
		{
			var request = string.Format("<seeker_search method='query' count='25' start='0'><last_name>{0}</last_name><first_name>{1}</first_name><dob>{2}</dob></seeker_search>", 
				lastName.HtmlEncode(),
				firstName.HtmlEncode(),
				dateOfBirth.ToString("MMddyyyy"));

			return GetBaseRequest(request);
		}

    /// <summary>
    /// Adds the job seeker.
    /// </summary>
    /// <param name="model">The job seeker model.</param>
    /// <param name="includeSelectiveService">Whether to include the selective service when adding the job seeker.</param>
    /// <returns></returns>
    public string AddJobSeeker(JobSeekerModel model, bool includeSelectiveService, string authUserName, string authPassword, string authOfficeId, string authAdminId)
    {
			if (authOfficeId.IsNullOrEmpty() || authUserName.StartsWith("selfreg", StringComparison.OrdinalIgnoreCase))
	    {
		    var externalOffice = ConvertFirstToExternalId(ExternalLookUpType.IntegrationOfficeIdPerZip, model.AddressPostcodeZip);
		    if (externalOffice.IsNotNullOrEmpty())
		    {
			    model.OfficeId = externalOffice;
			    model.AdminId = string.Format("selfreg{0}", externalOffice.ToLower());
		    }
	    }
	    else
	    {
				model.OfficeId = authOfficeId;
				model.AdminId = authAdminId;
			}

	    var jobSeekerXml = JobSeekerToXml(model, includeSelectiveService, true, null);
      var request = string.Format(
        @"<seekers>
            <seeker method='insert' version='1' uuid='{0}' ct_aggr_version='{1}'>
              <office_id>{2}</office_id>
              <admin_id>{3}</admin_id>
              <username>{4}</username>
              <password>{5}</password>
              <origination_method>{6}</origination_method>
              <origin>3</origin>
              <seeker_status_cd>1</seeker_status_cd>
              <job_seeker_flag>1</job_seeker_flag>
              {7}
            </seeker>
            </seekers>",
        Guid.NewGuid().ToString("N"),
        CtAggrVersion,
				model.OfficeId.IsNullOrEmpty() ? DefaultOfficeId : model.OfficeId,
				authAdminId.IsNullOrEmpty() ? DefaultAdminId : authAdminId,
        model.Username,
        DateTime.Now.Ticks,
        OriginationMethod,
        jobSeekerXml);
      return GetBaseRequestIncludingOfficeIdAndAdminId(request, authUserName, authPassword, authOfficeId, authAdminId);
    }

    protected string LookupExternalOfficeId(long officeId)
    {
      //var office = Repositories.Core.
      return DefaultOfficeId;
    }


    /// <summary>
    /// Saves the job seeker.
    /// </summary>
    /// <param name="model">The job seeker model.</param>
    /// <param name="version">The version.</param>
    /// <returns></returns>
    public string SaveJobSeeker(JobSeekerModel model, string version, bool includeSelectiveService, JobSeekerModel ekosJobSeeker, string authUserName, string authPassword, string authOfficeId, string authAdminId)
    {
      var jobSeekerXml = JobSeekerToXml(model, includeSelectiveService, false, ekosJobSeeker);
      var request =
        string.Format(
          "<seekers><seeker method='update' id='{0}' version='{1}' uuid='{2}' current_office_id='{3}'>{4}</seeker></seekers>",
          model.ExternalId, version, Guid.NewGuid(), model.OfficeId, jobSeekerXml);
      return GetBaseRequestIncludingOfficeIdAndAdminId(request, authUserName, authPassword, authOfficeId, authAdminId);
    }

    /// <summary>
    /// Sets the job seeker status
    /// </summary>
    /// <param name="model">The job seeker model</param>
    /// <param name="active">Whether they are active or not</param>
    /// <param name="version">The job seeker version number</param>
    /// <returns></returns>
    public string SetJobSeekerStatus(JobSeekerModel model, bool active, string version, string authUserName, string authPassword, string authOfficeId, string authAdminId)
    {
			return SetJobSeekerStatus(model.ExternalId, model.OfficeId, active, version, authUserName, authPassword, authOfficeId, authAdminId);
    }

		/// <summary>
		/// Sets the job seeker status
		/// </summary>
		/// <param name="jobSeekerId">The job seeker external id</param>
		/// <param name="jobSeekerOfficeId">The job seeker external id</param>
		/// <param name="active">Whether they are active or not</param>
		/// <param name="version">The job seeker version number</param>
		/// <returns></returns>
		public string SetJobSeekerStatus(string jobSeekerId, string jobSeekerOfficeId, bool active, string version, string authUserName, string authPassword, string authOfficeId, string authAdminId)
		{
			var jobSeekerFlag = active ? "1" : "0";
			var jobSeekerXml = string.Format("<job_seeker_flag>{0}</job_seeker_flag>", jobSeekerFlag);
			var seekerStatus = active ? "1" : "3";
			jobSeekerXml += string.Format("<seeker_status_cd>{0}</seeker_status_cd>", seekerStatus);
			var request =
				string.Format(
					"<seekers><seeker method='update' id='{0}' version='{1}' uuid='{2}' current_office_id='{3}'>{4}</seeker></seekers>",
					jobSeekerId,
					version,
					Guid.NewGuid(),
					jobSeekerOfficeId,
					jobSeekerXml);
			return GetBaseRequestIncludingOfficeIdAndAdminId(request, authUserName, authPassword, authOfficeId, authAdminId);
		}

    /// <summary>
    /// Saves the job matches.
    /// </summary>
    /// <param name="jobSeekerId">The job seeker id.</param>
    /// <param name="jobIds">The job ids.</param>
    /// <returns></returns>
    public string SaveJobMatches(string jobSeekerId, IEnumerable<string> jobIds, string authUserName, string authPassword, string authOfficeId, string authAdminId)
    {
      var sb = new StringBuilder();
      foreach (var jobId in jobIds)
      {
        sb.Append(string.Format("<job id='{0}'/>", jobId.HtmlEncode()));
      }

      var request = string.Format("<seeker_match id='{0}' method='match'><jobs>{1}</jobs></seeker_match>", jobSeekerId.HtmlEncode(), sb);

      return GetBaseRequestIncludingOfficeIdAndAdminId(request, authUserName, authPassword, authOfficeId, authAdminId);
    }

    /// <summary>
    /// Assigns the admin to the job seeker.
    /// </summary>
    /// <param name="jobSeekerId">The job seeker identifier.</param>
    /// <param name="adminId">The admin identifier.</param>
    /// <param name="version">The version.</param>
    /// <param name="uuId">The uu id.</param>
    /// <returns></returns>
    public string AssignAdmin(string jobSeekerId, string adminId, string version, string authUserName, string authPassword, string authOfficeId, string authAdminId, Guid? uuId = null)
    {
      var request = string.Format("<seekers><seeker method='update' id='{0}' version='{1}' uuid='{2}'><admin_id>{3}</admin_id></seeker></seekers>", jobSeekerId.HtmlEncode(), version, uuId ?? Guid.NewGuid(), adminId.HtmlEncode());

      return GetBaseRequestIncludingOfficeIdAndAdminId(request, authUserName, authPassword, authOfficeId, authAdminId);
    }

    /// <summary>
    /// Saves the activity.
    /// </summary>
    /// <param name="jobSeekerId">The job seeker identifier.</param>
    /// <param name="version">The version.</param>
    /// <param name="activityId">The activity identifier.</param>
    /// <param name="adminId">The admin identifier.</param>
    /// <param name="officeId">The office identifier.</param>
    /// <param name="uuid">The UUID.</param>
    /// <param name="requestDate">The request date.</param>
    /// <returns></returns>
    public string SaveActivity(string jobSeekerId, string version, string activityId, string adminId, string officeId, string authUserName, string authPassword, string authOfficeId, string authAdminId, Guid? uuid = null, DateTime? requestDate = null)
    {
      requestDate = requestDate ?? DateTime.Now;
      uuid = uuid ?? Guid.NewGuid();

      var request =
        string.Format(
          @"<seekers><seeker method='update' id='{0}' version='{1}' uuid='{2}'><activities><activity uuid='{3}' method='insert'><ctime>{4}</ctime><seeker_service_type_cd>{5}</seeker_service_type_cd>{6}</activity></activities></seeker></seekers>",
          jobSeekerId.HtmlEncode(), version.HtmlEncode(), uuid, uuid, requestDate.Value.ToString("MMddyyyy"), activityId.HtmlEncode(),
          GetActivityAdminAndOfficeCommand(adminId, officeId ?? DefaultOfficeId));

      return GetBaseRequestIncludingOfficeIdAndAdminId(request, authUserName, authPassword, authOfficeId, authAdminId);
    }

    #region XML builder

    /// <summary>
    /// Converts a job seeker model to XML for AOSOS
    /// </summary>
    /// <param name="seeker">The job seeker model</param>
    /// <param name="includeSelectiveService">if set to <c>true</c> [include selective service].</param>
    /// <param name="insert">If <c>true</c> includes mode='insert' attributes otherwise doesn't.</param>
    /// <param name="ekosJobSeeker">The job seeker as currently stored in EKOS.</param>
    /// <returns>
    /// The XML required by AOSOS
    /// </returns>
    internal string JobSeekerToXml(JobSeekerModel seeker, bool includeSelectiveService, bool insert,
      JobSeekerModel ekosJobSeeker)
    {
      var xml = new StringBuilder();
      if (seeker.IsNotNull())
        ProcessUsername(ekosJobSeeker, seeker, xml);

      if (seeker.IsNotNull() && seeker.Resume.IsNotNull() && seeker.Resume.ResumeContent.IsNotNull())
      {
        var profile = seeker.Resume.ResumeContent.Profile;
        UserProfile ekosProfile = null;
        if (ekosJobSeeker.IsNotNull() && ekosJobSeeker.Resume.IsNotNull() &&
            ekosJobSeeker.Resume.ResumeContent.IsNotNull())
        {
          ekosProfile = ekosJobSeeker.Resume.ResumeContent.Profile;
        }

        AddressModel postalAddress = null;
        if (profile.IsNotNull())
        {
          ProcessSocialSecurityNumber(seeker, ekosJobSeeker, xml);
          ProcessUserGivenName(ekosProfile, profile, xml);
          ProcessStandardProfileProperties(ekosProfile, profile, xml);
          ProcessDisability(ekosProfile, profile, xml);
          if (includeSelectiveService && ekosProfile.IsNull() ||
              (ekosProfile.IsNotNull() &&
               profile.RegisteredWithSelectiveService != ekosProfile.RegisteredWithSelectiveService))
            xml.Append(BoolToXml("selective_service_flag", profile.RegisteredWithSelectiveService, string.Empty));

          if (seeker.Resume.ResumeContent.SeekerContactDetails.IsNotNull() &&
              seeker.Resume.ResumeContent.SeekerContactDetails.PostalAddress.IsNotNull())
          {
            postalAddress = seeker.Resume.ResumeContent.SeekerContactDetails.PostalAddress.ToIntegrationAddress();
          }
          else if (profile.PostalAddress.IsNotNull())
          {
            postalAddress = profile.PostalAddress.ToIntegrationAddress();
          }

          AddressModel ekosAddress = new AddressModel();
          if (ekosJobSeeker.IsNotNull() &&
              ekosJobSeeker.Resume.IsNotNull() &&
              ekosJobSeeker.Resume.ResumeContent.IsNotNull() &&
              ekosJobSeeker.Resume.ResumeContent.SeekerContactDetails.IsNotNull() &&
              ekosJobSeeker.Resume.ResumeContent.SeekerContactDetails.PostalAddress.IsNotNull())
            ekosAddress = ekosJobSeeker.Resume.ResumeContent.SeekerContactDetails.PostalAddress.ToIntegrationAddress();
          AddressModelPartialComparer addressModelPartialComparer = new AddressModelPartialComparer();
          if (addressModelPartialComparer.Compare(postalAddress, ekosAddress) != 0)
            xml.Append(AddressToXml(postalAddress));
          ProcessPrimaryPhone(profile, ekosProfile, xml);
          ProcessUSCitizen(profile, ekosProfile, xml);
          ProcessVeteran(profile, ekosProfile, xml);
          ProcessDrivingLicense(profile, ekosProfile, xml);
          ProcessEthnicHeritage(ekosProfile, profile, xml);
          string profileObjective = "N/A";
          if (seeker.Resume.ResumeContent.AdditionalResumeSections.IsNotNullOrEmpty())
          {
            var objective =
              seeker.Resume.ResumeContent.AdditionalResumeSections.SingleOrDefault(
                x => x.SectionType == ResumeSectionType.Objective);
            if (objective.IsNotNull())
              profileObjective = objective.SectionContent.ToString();
          }

          string ekosObjective = string.Empty;
          if (ekosJobSeeker.IsNotNull() &&
              ekosJobSeeker.Resume.IsNotNull() &&
              ekosJobSeeker.Resume.ResumeContent.IsNotNull() &&
              ekosJobSeeker.Resume.ResumeContent.AdditionalResumeSections.IsNotNullOrEmpty())
          {
            var ekosProfileObjective =
              ekosJobSeeker.Resume.ResumeContent.AdditionalResumeSections.SingleOrDefault(
                x => x.SectionType == ResumeSectionType.Objective);
            if (ekosProfileObjective.IsNotNull())
              ekosObjective = ekosProfileObjective.SectionContent.ToString();
          }

          if (profileObjective != ekosObjective)
          {
            xml.Append(StringToXml("objective", profileObjective));
          }

          var education = seeker.Resume.ResumeContent.EducationInfo;
          ProcessEducation(ekosJobSeeker, education, xml);
          ProcessSchools(ekosJobSeeker, xml, education);
          //xml.Append(StringToXml("origination_method", AOSOSSettings.OriginationMethod));
          //xml.Append(StringToXml("seeker_status_cd", seeker.ExternalStatusId));
          //xml.Append(StringToXml("username", seeker.Username));
          //xml.Append(StringToXml("password", DateTime.Now.Ticks.ToString()));

          var skills = seeker.Resume.ResumeContent.Skills;
          ProcessSkills(ekosJobSeeker, skills, xml);
          ProcessCertification(ekosJobSeeker, skills, xml);
          var experience = seeker.Resume.ResumeContent.ExperienceInfo;
          var ekosExperience = new ExperienceInfo();
          if (ekosJobSeeker.IsNotNull() &&
              ekosJobSeeker.Resume.IsNotNull() &&
              ekosJobSeeker.Resume.ResumeContent.IsNotNull() &&
              ekosJobSeeker.Resume.ResumeContent.ExperienceInfo.IsNotNull())
            ekosExperience = ekosJobSeeker.Resume.ResumeContent.ExperienceInfo;
          if (experience.IsNotNull() && experience.EmploymentStatus.IsNotNull() && (ekosExperience.EmploymentStatus.IsNull() || experience.EmploymentStatus != ekosExperience.EmploymentStatus))
          {
            xml.Append(StringToXml("employment_status_cd",
              ConvertToExternallId(ExternalLookUpType.EmploymentStatus, experience.EmploymentStatus.ToString())));
          }

          var preferences = new Preferences();
          if (seeker.Resume.Special.IsNotNull() && seeker.Resume.Special.Preferences.IsNotNull())
            preferences = seeker.Resume.Special.Preferences;
          ProcessWorkPreferences(seeker, ekosJobSeeker, xml, preferences);
          ProcessLocationPreferences(ekosJobSeeker, preferences, xml, postalAddress);
          ProcessDesiredTitles(ekosJobSeeker, xml, experience);
        }
      }

      var retVal = xml.ToString();
      return retVal;
    }

    private void ProcessDesiredTitles(JobSeekerModel ekosJobSeeker, StringBuilder xml, ExperienceInfo experience)
    {
      List<DesiredTitle> ekosDesiredTitles = new List<DesiredTitle>();
      if (ekosJobSeeker.IsNotNull() &&
          ekosJobSeeker.Resume.IsNotNull() &&
          ekosJobSeeker.Resume.Special.IsNotNull() &&
          ekosJobSeeker.Resume.Special.Preferences.IsNotNull() &&
          ekosJobSeeker.Resume.Special.Preferences.TitlePreference.IsNotNullOrEmpty())
      {
        ekosDesiredTitles = ekosJobSeeker.Resume.Special.Preferences.TitlePreference;
      }

      var desiredTitles = new List<DesiredTitle>();
      if (experience.IsNotNull() && experience.Jobs.IsNotNullOrEmpty())
      {
        desiredTitles = experience.Jobs
          .Where(job => job.OnetCode.IsNotNullOrEmpty())
          .GroupBy(job => job.OnetCode)
          .Select(group =>
          new DesiredTitle()
          {
            Onet = (group.Key.IndexOf("|", StringComparison.Ordinal) > 0 ? group.Key.SubstringBefore("|") : group.Key).Replace("-", "").Replace(".", ""),
            ExperienceInMonths = group.Sum(g => Math.Abs(((g.JobEndDate ?? DateTime.Now).Month - g.JobStartDate.GetValueOrDefault().Month) + (12 * ((g.JobEndDate ?? DateTime.Now).Year - g.JobStartDate.GetValueOrDefault().Year))))
          }).ToList();
        desiredTitles = desiredTitles.Where(desiredTitle => desiredTitle.Onet.IsNotNullOrEmpty()).ToList();
      }
      else
      {
	      desiredTitles.Add(new DesiredTitle
		    {
			    ExperienceInMonths = 0,
					Onet = AOSOSSettings.DefaultOnet3
		    });
      }

      DesiredTitlePartialEqualityComparer comparer = new DesiredTitlePartialEqualityComparer();
      List<DesiredTitle> ekosTitlesToDelete = ekosDesiredTitles.Except(desiredTitles, comparer).ToList();
      List<DesiredTitle> titlesToInsert = desiredTitles.Except(ekosDesiredTitles, comparer).ToList();
      DesiredTitleFullEqualityComparer fullComparer = new DesiredTitleFullEqualityComparer();
      List<DesiredTitle> titlesToUpdate = ekosDesiredTitles.Intersect(desiredTitles, comparer).Except(desiredTitles, fullComparer).ToList();

			// Ensure we don't delete items we are actually updating
			ekosTitlesToDelete.RemoveAll(x => titlesToUpdate.Select(y => y.ExternalId).Contains(x.ExternalId));

      if (ekosTitlesToDelete.Any() || titlesToInsert.Any() || titlesToUpdate.Any())
      {
        xml.Append("<desired_titles>");
        if (ekosTitlesToDelete.Any())
        {
          foreach (var title in ekosTitlesToDelete)
          {
            xml.AppendFormat("<desired_title method='delete' id='{0}'></desired_title>", title.ExternalId);
          }
        }

        if (titlesToInsert.Any())
        {
          foreach (var jobOnet in titlesToInsert)
          {
            xml.Append("<desired_title method='insert'>");
            xml.Append(StringToXml("onet3", jobOnet.Onet));
            xml.Append(IntToXml("experience", Math.Min(jobOnet.ExperienceInMonths, 720)));
            xml.Append("</desired_title>");
          }
        }

        if (titlesToUpdate.Any())
        {
          foreach (var jobOnet in titlesToUpdate)
          {
            DesiredTitle matchingDesiredTitle = desiredTitles.FirstOrDefault(desiredTitle => desiredTitle.Onet == jobOnet.Onet);
            xml.Append(string.Format("<desired_title method='update' id='{0}'>", jobOnet.ExternalId));
            xml.Append(StringToXml("onet3", jobOnet.Onet));
            xml.Append(IntToXml("experience", matchingDesiredTitle == null ? 0 : Math.Min(matchingDesiredTitle.ExperienceInMonths, 720)));
            xml.Append("</desired_title>");
          }
        }

        xml.Append("</desired_titles>");
      }

      // if there wasn't any desired title from ekos and there aren't any in the resume then insert a default one.
      if (!ekosDesiredTitles.Any() && !desiredTitles.Any())
      {
        xml.Append("<desired_titles>");
        xml.Append("<desired_title method='insert'>");
        xml.Append(StringToXml("onet3", AOSOSSettings.DefaultOnet3));
        xml.Append(IntToXml("experience", 0));
        xml.Append("</desired_title>");
        xml.Append("</desired_titles>");
      }
    }

    private void ProcessCertification(JobSeekerModel ekosJobSeeker, SkillInfo skills, StringBuilder xml)
    {
      List<Certification> certifications = new List<Certification>();
      List<Certification> ekosCertifications = new List<Certification>();
      if (ekosJobSeeker.IsNotNull() &&
          ekosJobSeeker.Resume.IsNotNull() &&
          ekosJobSeeker.Resume.ResumeContent.IsNotNull() &&
          ekosJobSeeker.Resume.ResumeContent.Skills.IsNotNull() &&
          ekosJobSeeker.Resume.ResumeContent.Skills.Certifications.IsNotNullOrEmpty())
      {
        ekosCertifications = ekosJobSeeker.Resume.ResumeContent.Skills.Certifications;
      }

      if (skills.Certifications.IsNotNullOrEmpty())
      {
        certifications = skills.Certifications;
      }

      var certificationPartialComparer = new CertificationPartialEqualityComparer();
      IEnumerable<Certification> certificationsToDelete = ekosCertifications.Except(certifications,
        certificationPartialComparer);
      IEnumerable<Certification> certificationsToInsert = certifications.Except(ekosCertifications,
        certificationPartialComparer);
      if (certificationsToDelete.Any() || certificationsToInsert.Any())
      {
        xml.Append("<certifications>");
        foreach (var ekosCert in certificationsToDelete)
        {
          xml.AppendFormat("<certification method='delete' id='{0}'></certification>", ekosCert.ExternalId);
        }

        foreach (var certificate in certificationsToInsert)
        {
          xml.AppendFormat("<certification method='insert'>");
          xml.Append(StringToXml("organization_name", certificate.OrganizationName.Trim(39, false)));
          xml.Append(StringToXml("certificate", certificate.Certificate.Trim(59, false)));
          xml.Append(DateTimeToXml("completion_date", certificate.CompletionDate));
          xml.Append(AddressToXml(certificate.CertificationAddress.ToIntegrationAddress(), false, false, false, false, false));
          xml.Append("</certification>");
        }

        xml.Append("</certifications>");
      }
    }

    private void ProcessEducation(JobSeekerModel ekosJobSeeker, EducationInfo education, StringBuilder xml)
    {
      EducationInfo ekosEduction = null;
      if (ekosJobSeeker.IsNotNull() &&
          ekosJobSeeker.Resume.IsNotNull() &&
          ekosJobSeeker.Resume.ResumeContent.IsNotNull() &&
          ekosJobSeeker.Resume.ResumeContent.EducationInfo.IsNotNull())
        ekosEduction = ekosJobSeeker.Resume.ResumeContent.EducationInfo;
      if (education.IsNotNull())
      {
        if (ekosEduction.IsNull() ||
            (ekosEduction.IsNotNull() && education.EducationLevel != ekosEduction.EducationLevel))
          xml.Append(StringToXml("norm_edu_level_cd",
            ConvertToExternallId(ExternalLookUpType.EducationLevel, education.EducationLevel.ToString())));
        if (ekosEduction.IsNull() ||
            (ekosEduction.IsNotNull() && education.SchoolStatus != ekosEduction.SchoolStatus))
          xml.Append(StringToXml("school_status_cd",
            ConvertToExternallId(ExternalLookUpType.SchoolStatus, education.SchoolStatus.ToString())));
      }
    }

    private void ProcessSchools(JobSeekerModel ekosJobSeeker, StringBuilder xml, EducationInfo education)
    {
      var schools = new List<School>();
      var ekosSchools = new List<School>();
      if (ekosJobSeeker.IsNotNull() && ekosJobSeeker.Resume.IsNotNull() &&
          ekosJobSeeker.Resume.ResumeContent.IsNotNull() &&
          ekosJobSeeker.Resume.ResumeContent.EducationInfo.IsNotNull() &&
          ekosJobSeeker.Resume.ResumeContent.EducationInfo.Schools.IsNotNullOrEmpty())
      {
        ekosSchools = ekosJobSeeker.Resume.ResumeContent.EducationInfo.Schools;
      }

      if (education.IsNotNull() && education.Schools.IsNotNullOrEmpty())
      {
        schools = education.Schools;

				foreach (var school in schools.Where(school => school.Major.IsNotNullOrEmpty() && school.Major.Length > 40))
				{
					school.Major = school.Major.Substring(0, 40);
				}
      }

      IEqualityComparer<School> schoolComparer = new SchoolPartialEqualityComparer();
      List<School> schoolsToDelete = ekosSchools.Except(schools, schoolComparer).ToList();
      List<School> schoolsToInsert = schools.Except(ekosSchools, schoolComparer).ToList();
      if (schoolsToDelete.Any() || schoolsToInsert.Any())
      {
        xml.Append("<schools>");
        foreach (var ekosSchool in schoolsToDelete)
        {
          xml.AppendFormat("<school id='{0}' method='delete'></school>", ekosSchool.ExternalId);
        }

        foreach (var school in schoolsToInsert)
        {
          xml.AppendFormat("<school method='insert'>");
          xml.Append(StringToXml("major", school.Major.IsNullOrEmpty() ? "None reported" : school.Major));
          xml.Append(StringToXml("degree", school.Degree.Trim(59, false)));
          xml.Append(StringToXml("school_name", school.Institution.Trim(40, false)));
          xml.Append(AddressToXml(school.SchoolAddress.ToIntegrationAddress(), false, true, false, false, false));
          xml.Append(DateTimeToXml("completion_date", school.CompletionDate));
          if (school.GradePointAverage.Value.HasValue)
            xml.Append(StringToXml("gpa", school.GradePointAverage.Value.Value.ToString("0.00")));
          xml.Append("</school>");
        }

        xml.Append("</schools>");
      }
    }

    private void ProcessSkills(JobSeekerModel ekosJobSeeker, SkillInfo skills, StringBuilder xml)
    {
      var skillsString = skills.Skills.IsNotNullOrEmpty()
        ? string.Join(", ", skills.Skills.Where(s => s.IsNotNullOrEmpty())).Trim(4000, false).StripDisallowedCharacters()
        : "";

	    if (skillsString.IsNullOrEmpty())
		    skillsString = "None reported";

      // if the ekosJobSeeker is null then this is an insert so we must send a skills node. Setting ekosSkillString to string.Empty
      // ensures this as it will be different to either "None reported" or the combined skills of the new job seeker.
      var ekosSkillsString = ekosJobSeeker.IsNull() ? string.Empty : "None reported";
      if (ekosJobSeeker.IsNotNull() &&
          ekosJobSeeker.Resume.IsNotNull() &&
          ekosJobSeeker.Resume.ResumeContent.IsNotNull() &&
          ekosJobSeeker.Resume.ResumeContent.Skills.IsNotNull() &&
          ekosJobSeeker.Resume.ResumeContent.Skills.Skills.IsNotNullOrEmpty())
        ekosSkillsString =
          string.Join(", ", ekosJobSeeker.Resume.ResumeContent.Skills.Skills.Select(skill => skill.Trim())).Trim(4000, false).StripDisallowedCharacters();
      if (skillsString != ekosSkillsString)
        xml.Append(StringToXml("skills", skillsString));
    }

    private void ProcessLocationPreferences(JobSeekerModel ekosJobSeeker, Preferences preferences, StringBuilder xml,
      AddressModel postalAddress)
    {
      List<DesiredZip> preferenceZips = new List<DesiredZip>();
      if (preferences.IsNotNull() && preferences.ZipPreference.IsNotNullOrEmpty())
      {
        preferenceZips = preferences.ZipPreference;
      }

      List<DesiredZip> ekosZips = new List<DesiredZip>();
      if (ekosJobSeeker.IsNotNull() &&
          ekosJobSeeker.Resume.IsNotNull() &&
          ekosJobSeeker.Resume.Special.IsNotNull() &&
          ekosJobSeeker.Resume.Special.Preferences.IsNotNull() &&
          ekosJobSeeker.Resume.Special.Preferences.ZipPreference.IsNotNullOrEmpty())
      {
        ekosZips = ekosJobSeeker.Resume.Special.Preferences.ZipPreference;
      }

      DesiredZipPartialEqualityComparer partialComparer = new DesiredZipPartialEqualityComparer();
      DesiredZipFullEqualityComparer fullComparer = new DesiredZipFullEqualityComparer();
      IEnumerable<DesiredZip> zipsToDelete = ekosZips.Except(preferenceZips, fullComparer);
      IEnumerable<DesiredZip> zipsToInsert = preferenceZips.Except(ekosZips, fullComparer);
      if (zipsToDelete.Any() || zipsToInsert.Any())
      {
        xml.Append("<desired_zips>");
        foreach (var zipPreference in zipsToDelete)
        {
          xml.AppendFormat("<desired_zip method='delete' zip='{0}'></desired_zip>", zipPreference.Zip);
        }

        foreach (var zips in zipsToInsert)
        {
          xml.Append(string.Format("<desired_zip zip='{0}' method='insert'><radius>{1}</radius></desired_zip>",
            zips.Zip.Trim(5, false), zips.Radius));
        }

        xml.Append("</desired_zips>");
      }

      List<string> preferredStates = new List<string>();
      if (preferences.IsNotNull())
      {
        if (preferences.MSAPreference.IsNotNullOrEmpty())
        {
          preferredStates.AddRange(preferences.MSAPreference.Select(state => state.StateCode).ToList());
        }

        if (preferences.StatePreference.IsNotNullOrEmpty())
        {
          preferredStates.AddRange(preferences.StatePreference.Select(state => state.StateCode));
        }
      }

      preferredStates = preferredStates.Distinct().ToList();
      List<string> ekosPreferredStates = new List<string>();
      if (ekosJobSeeker.IsNotNull() &&
          ekosJobSeeker.Resume.IsNotNull() &&
          ekosJobSeeker.Resume.Special.IsNotNull() &&
          ekosJobSeeker.Resume.Special.Preferences.IsNotNull())
      {
        if (ekosJobSeeker.Resume.Special.Preferences.MSAPreference.IsNotNullOrEmpty())
        {
          ekosPreferredStates.AddRange(
            ekosJobSeeker.Resume.Special.Preferences.MSAPreference.Select(state => state.StateCode));
        }

        if (ekosJobSeeker.Resume.Special.Preferences.StatePreference.IsNotNullOrEmpty())
        {
          ekosPreferredStates.AddRange(
            ekosJobSeeker.Resume.Special.Preferences.StatePreference.Select(state => state.StateCode));
        }
      }

      string homeAddressState = postalAddress.ExternalCountryId == "US"
        ? ConvertToExternallId(ExternalLookUpType.State, postalAddress.StateId.ToString(CultureInfo.InvariantCulture))
        : "ZZ";
      if (!preferredStates.Any() && !preferenceZips.Any()) preferredStates.Add(homeAddressState);
      List<string> statesToDelete = ekosPreferredStates.Except(preferredStates).ToList();
      List<string> statesToInsert = preferredStates.Except(ekosPreferredStates).ToList();
      if (statesToDelete.Any() || statesToInsert.Any() || (!preferenceZips.Any() && !preferredStates.Any()))
      {
        xml.Append("<desired_states>");
        foreach (var stateCode in statesToDelete)
        {
          xml.AppendFormat("<desired_state method='delete' state='{0}'></desired_state>", stateCode);
        }

        foreach (var stateCode in statesToInsert)
        {
          xml.AppendFormat("<desired_state method='insert' state='{0}'></desired_state>", stateCode);
        }

        xml.Append("</desired_states>");
      }
    }

    private void ProcessWorkPreferences(JobSeekerModel seeker, JobSeekerModel ekosJobSeeker, StringBuilder xml, Preferences preferences)
    {
      if (seeker.Resume.Special.IsNotNull() && seeker.Resume.Special.Preferences.IsNotNull())
      {
	      Preferences ekosPreferences = null;

        if (ekosJobSeeker.IsNotNull() && ekosJobSeeker.Resume.IsNotNull() && ekosJobSeeker.Resume.Special.IsNotNull() &&
            ekosJobSeeker.Resume.Special.Preferences.IsNotNull())
          ekosPreferences = ekosJobSeeker.Resume.Special.Preferences;
        if (ekosPreferences.IsNull() || preferences.Salary != ekosPreferences.Salary)
          xml.Append(DecimalToXml("sal", (decimal?)preferences.Salary));
        if (ekosPreferences.IsNull() || preferences.SalaryFrequencyId != ekosPreferences.SalaryFrequencyId)
          xml.Append(StringToXml("salary_unit_cd",
            ConvertToExternallId(ExternalLookUpType.SalaryUnit, preferences.SalaryFrequencyId.ToString())));

				var focusPreferences = seeker.Resume.Special.Preferences;
				var focusShift = focusPreferences.ShiftDetail;

        Shift ekosShift = null;
        if (ekosPreferences != null)
          ekosShift = ekosPreferences.ShiftDetail;

				if (focusShift.IsNotNull())
        {
        //  if (ekosShift.IsNull() || shift.WorkWeekId != ekosShift.WorkWeekId)
        //    xml.Append(StringToXml("work_week",
        //      ConvertToExternallId(ExternalLookUpType.WorkWeek, shift.WorkWeekId.ToString())));

        //  if (shift.WorkDurationId.HasValue)
        //  {
        //    // there is a many-to-one mapping for WorkType i.e. many internal map to one external. Only send update is the external changes.
        //    var shiftWorkDurationExternalId = ConvertToExternallId(ExternalLookUpType.WorkType, shift.WorkDurationId.ToString());
        //    var ekosWorkDurationExternalId = string.Empty;
        //    if (ekosShift.IsNotNull() && ekosShift.WorkDurationId.HasValue)
        //      ekosWorkDurationExternalId = ConvertToExternallId(ExternalLookUpType.WorkType, ekosShift.WorkDurationId.ToString());
        //    if (ekosShift.IsNull() || shiftWorkDurationExternalId != ekosWorkDurationExternalId)
        //      xml.Append(StringToXml("work_type", shiftWorkDurationExternalId));
        //  }

					var ekosShiftTypeIds = ekosShift.IsNotNull() && ekosShift.ShiftTypeIds.IsNotNullOrEmpty()
																	? ekosShift.ShiftTypeIds
																	: new List<long>();

					var focusShiftTypeIds = focusShift.ShiftTypeIds ?? new List<long>();

					foreach (var shiftTypeId in focusShiftTypeIds.Except(ekosShiftTypeIds))
					{
						var shiftType = ConvertToExternallId(ExternalLookUpType.ShiftType, shiftTypeId);
						if (shiftType.IsNotNullOrEmpty())
							xml.Append(string.Format("<{0}>1</{0}>", shiftType));
					}

					foreach (var shiftTypeId in ekosShiftTypeIds.Except(focusShiftTypeIds))
					{
						var shiftType = ConvertToExternallId(ExternalLookUpType.ShiftType, shiftTypeId);
						if (shiftType.IsNotNullOrEmpty())
							xml.Append(string.Format("<{0}>0</{0}>", shiftType));
					}
        }
      }
    }

    private void ProcessPreviousJobs(JobSeekerModel ekosJobSeeker, StringBuilder xml, ExperienceInfo experience)
    {
      /*
       * TODO:	This method currently deletes all the previous jobs recieved from EKOS then inserts all the previous jobs from the resume.
       *				This should work out if there are recieved jobs to actually delete or any resume jobs to actually insert
       *				And for those jobs that already exist in both recieved and resume check for differences and only update those differences.
       *				As the KY integration with EKOS doesn't send previous jobs at all (i.e. addPreviousJobsToXml is false) then this
       *				hasn't been implemented.
      */
      xml.Append("<previous_jobs>");
      if (ekosJobSeeker.IsNotNull() && ekosJobSeeker.Resume.IsNotNull() &&
          ekosJobSeeker.Resume.ResumeContent.IsNotNull() &&
          ekosJobSeeker.Resume.ResumeContent.ExperienceInfo.IsNotNull() &&
          ekosJobSeeker.Resume.ResumeContent.ExperienceInfo.Jobs.IsNotNullOrEmpty())
      {
        foreach (var ekosJob in ekosJobSeeker.Resume.ResumeContent.ExperienceInfo.Jobs)
        {
          xml.AppendFormat("<previous_job method='delete' id='{0}'></previous_job>", ekosJob.ExternalId);
        }
      }

      if (experience.IsNotNull() && experience.Jobs.IsNotNullOrEmpty())
      {
        foreach (var job in experience.Jobs.OrderByDescending(x => x.JobStartDate))
        {
          xml.AppendFormat("<previous_job method='insert'>");
          xml.Append(StringToXml("employer", job.Employer));
          xml.Append(StringToXml("title", job.Title));
          xml.Append(DateTimeToXml("start_date", job.JobStartDate));
          if (job.JobEndDate.IsNotNull())
            xml.Append(DateTimeToXml("end_date", job.JobEndDate));
          xml.Append(AddressToXml(job.JobAddress.ToIntegrationAddress(), false, includeZip: false));
          xml.Append(StringToXml("duties", job.Description));
          xml.Append(DecimalToXml("sal", (decimal)job.Salary.GetValueOrDefault(0.01f)));
          xml.Append(StringToXml("salary_unit_cd", ConvertToExternallId(ExternalLookUpType.SalaryUnit, job.SalaryFrequencyId.ToString()) ?? "6"));
          xml.Append("</previous_job>");
        }
      }

      xml.Append("</previous_jobs>");
    }

    private void ProcessDrivingLicense(UserProfile profile, UserProfile ekosProfile, StringBuilder xml)
    {
      LicenseInfo ekosLicenseInfo = null;
      if (ekosProfile.IsNotNull() && ekosProfile.License.IsNotNull()) ekosLicenseInfo = ekosProfile.License;
      IComparer<LicenseInfo> comparer = new LicenceInfoPartialComparer();
      if (comparer.Compare(profile.License, ekosLicenseInfo) != 0)
      {
        var licence = profile.License;
        if (licence.HasDriverLicense.GetValueOrDefault())
        {
          xml.Append(BoolToXml("driver_flag", licence.HasDriverLicense));
          xml.Append(StringToXml("driver_state",
            ConvertToExternallId(ExternalLookUpType.State, licence.DriverStateId.ToString())));
          string externalId = ConvertToExternallId(ExternalLookUpType.DrivingLicenceClass,
            licence.DrivingLicenceClassId.ToString());
          if (externalId.IsNotNullOrEmpty())
          {
            xml.Append(StringToXml("driver_class", externalId));
            List<long> ekosEndorsementIds = new List<long>();
            List<long> endorsementIds = new List<long>();
            if (ekosProfile.IsNotNull() &&
                ekosProfile.License.IsNotNull() &&
                ekosProfile.License.DrivingLicenceEndorsementIds.IsNotNullOrEmpty())
              ekosEndorsementIds = ekosProfile.License.DrivingLicenceEndorsementIds;
            if (licence.DrivingLicenceEndorsementIds.IsNotNullOrEmpty())
            {
              endorsementIds = licence.DrivingLicenceEndorsementIds;
            }

            foreach (var endorsement in ekosEndorsementIds.Except(endorsementIds))
            {
              externalId = ConvertToExternallId(ExternalLookUpType.DrivingLicenceEndorsements, endorsement);
              if (externalId.IsNotNullOrEmpty())
                xml.Append(string.Format("<{0}>0</{0}>", externalId));
            }

            foreach (var endorsement in licence.DrivingLicenceEndorsementIds)
            {
              externalId = ConvertToExternallId(ExternalLookUpType.DrivingLicenceEndorsements, endorsement);
              if (externalId.IsNotNullOrEmpty())
                xml.Append(string.Format("<{0}>-1</{0}>", externalId));
            }
          }
        }
        else
        {
          xml.Append("<driver_flag>0</driver_flag>");
          xml.Append("<driver_class></driver_class>");
          xml.Append("<driver_state></driver_state>");
          var lookups = GetAllLookUpsInCollection(ExternalLookUpType.DrivingLicenceEndorsements);
          lookups.ForEach(drv =>
          {
            var externalId = drv.ExternalId;
            if (externalId.IsNotNullOrEmpty())
            {
              xml.Append(string.Format("<{0}>0</{0}>", externalId));
            }
          });
        }
      }
    }

    private void ProcessVeteran(UserProfile profile, UserProfile ekosProfile, StringBuilder xml)
    {
      if (profile.Veteran.IsNotNull() && profile.Veteran.IsHomeless.HasValue && profile.Veteran.IsHomeless.Value)
      {
        if (ekosProfile.IsNull() || (ekosProfile.IsNotNull() && ekosProfile.Veteran.IsNull()) ||
            (ekosProfile.Veteran.IsNotNull() && profile.Veteran.IsHomeless != ekosProfile.Veteran.IsHomeless))
          xml.Append("<hous_sit_type_cd>1</hous_sit_type_cd>");
      }
      else
      {
        if (ekosProfile.IsNull() ||
          (ekosProfile.IsNotNull() && ekosProfile.Veteran.IsNull()) ||
          (ekosProfile.Veteran.IsNotNull() && profile.Veteran.IsHomeless.GetValueOrDefault(false) != ekosProfile.Veteran.IsHomeless))
          xml.Append("<hous_sit_type_cd></hous_sit_type_cd>");
      }

      if (profile.Veteran.IsNotNull() && profile.Veteran.IsVeteran.GetValueOrDefault(false))
      {
        if (ekosProfile.IsNull() || (ekosProfile.IsNotNull() && ekosProfile.Veteran.IsNull()) ||
            (ekosProfile.Veteran.IsNotNull() && new VeteranInfoPartialComparer().Compare(profile.Veteran, ekosProfile.Veteran) != 0))
        {
          var veteran = profile.Veteran;
	        var sendVeteran = true;
					if (veteran.History[0].VeteranEra == VeteranEra.TransitioningServiceMember)
					{
						if (veteran.History[0].VeteranEndDate <= DateTime.Now.Date)
							sendVeteran = false;
					}

					if (sendVeteran)
					{
						xml.Append(BoolToXml("vet_flag", veteran.IsVeteran, trueValue: "1"));
						if (veteran.History[0].VeteranEra.IsIn(VeteranEra.OtherVet, VeteranEra.Vietnam, VeteranEra.TransitioningServiceMember))
						{
							xml.Append(DateTimeToXml("vet_start_date", veteran.History[0].VeteranStartDate));
							xml.Append(DateTimeToXml("vet_end_date", veteran.History[0].VeteranEndDate));
						}

						xml.Append(StringToXml("vet_era", ConvertToExternallId(ExternalLookUpType.VeteranEra, veteran.History[0].VeteranEra.ToString())));

						xml.Append(StringToXml("vet_disability_status",
							ConvertToExternallId(ExternalLookUpType.VeteranDisabilityStatus, veteran.History[0].VeteranDisabilityStatus.ToString())));
						string vetTsmTypeCd = ConvertToExternallId(ExternalLookUpType.VeteranTransitionType, veteran.History[0].TransitionType.ToString());
						if (vetTsmTypeCd.IsNotNullOrEmpty())
							xml.Append(StringToXml("vet_tsm_type_cd", vetTsmTypeCd));
					  else
							xml.Append(StringToXml("vet_tsm_type_cd", string.Empty));
						if ((bool)veteran.IsVeteran)
							xml.Append(BoolToXml("campaign_vet_flag", veteran.History[0].IsCampaignVeteran));
					}
        }
      }
      else
      {
        if (ekosProfile.IsNull() || (ekosProfile.IsNotNull() && ekosProfile.Veteran.IsNull()) ||
            (ekosProfile.Veteran.IsNotNull() && new VeteranInfoPartialComparer().Compare(profile.Veteran, ekosProfile.Veteran) != 0))
          xml.Append("<vet_flag>0</vet_flag>");
      }

      if (profile.Veteran.IsNotNull() && 
         (bool) profile.Veteran.IsVeteran &&
					profile.Veteran.History.IsNotNullOrEmpty() && 
          profile.Veteran.History[0].MilitaryBranchOfServiceId.HasValue &&
					(ekosProfile.IsNull() || ekosProfile.Veteran.IsNull() || ekosProfile.Veteran.History.IsNullOrEmpty() ||
           profile.Veteran.History[0].MilitaryBranchOfServiceId != ekosProfile.Veteran.History[0].MilitaryBranchOfServiceId))
      {
        var branchCd = ConvertToExternallId(ExternalLookUpType.MilitaryBranchOfService, profile.Veteran.History[0].MilitaryBranchOfServiceId.ToString());
        if (branchCd.IsNotNullOrEmpty())
        {
          xml.Append(StringToXml("branch_cd", branchCd));
        }
      }
    }

    private void ProcessUSCitizen(UserProfile profile, UserProfile ekosProfile, StringBuilder xml)
    {
      if (profile.IsUSCitizen.IsNotNull() &&
          (ekosProfile.IsNull() || (ekosProfile.IsNotNull() &&
            (profile.IsUSCitizen != ekosProfile.IsUSCitizen || profile.AlienExpires != ekosProfile.AlienExpires || profile.AlienId != ekosProfile.AlienId))))
      {
        xml.Append(BoolToXml("citizen_flag", profile.IsUSCitizen));
        if (!profile.IsUSCitizen.Value)
        {
          xml.Append(StringToXml("alien_id", profile.AlienId));
          if (profile.IsPermanentResident.GetValueOrDefault(false))
          {
            xml.Append(BoolToXml("alien_perm_flag", true));
            xml.Append(StringToXml("alien_expires", string.Empty));
          }
          else
          {
            xml.Append(BoolToXml("alien_perm_flag", false));
            xml.Append(DateTimeToXml("alien_expires", profile.AlienExpires));
          }
        }
      }
    }

    private void ProcessDisability(UserProfile ekosProfile, UserProfile profile, StringBuilder xml)
    {
      if (ekosProfile.IsNull() || (ekosProfile.IsNotNull() && profile.DisabilityStatus != ekosProfile.DisabilityStatus))
        xml.Append(StringToXml("disability_status",
          ConvertToExternallId(ExternalLookUpType.DisabilityStatus, profile.DisabilityStatus.ToString())));
      if (profile.DisabilityStatus == DisabilityStatus.Disabled && ekosProfile.IsNull() ||
          (ekosProfile.IsNotNull() && profile.DisabilityCategoryIds != ekosProfile.DisabilityCategoryIds))
        xml.Append(StringToXml("disability_category_cd",
          ConvertToExternallId(ExternalLookUpType.DisabilityCategory, profile.DisabilityCategoryIds.ToString())));
    }

    private void ProcessStandardProfileProperties(UserProfile ekosProfile, UserProfile profile, StringBuilder xml)
    {
      if (ekosProfile.IsNull() || (ekosProfile.IsNotNull() && profile.DOB != ekosProfile.DOB))
        xml.Append(DateTimeToXml("dob", profile.DOB));
      if (ekosProfile.IsNull() || (ekosProfile.IsNotNull() && profile.Sex != ekosProfile.Sex))
        xml.Append(StringToXml("sex", ConvertToExternallId(ExternalLookUpType.Gender, profile.Sex.ToString())));
      if (ekosProfile.IsNull() ||
          (ekosProfile.IsNotNull() &&
           (profile.EmailAddress.IsNull() ? string.Empty : profile.EmailAddress) != ekosProfile.EmailAddress))
        xml.Append(StringToXml("email", profile.EmailAddress));
      if (ekosProfile.IsNull() || (ekosProfile.IsNotNull() && profile.IsMigrant != ekosProfile.IsMigrant))
        xml.Append(BoolToXml("migrant_flag", profile.IsMigrant ?? false));
    }

    private void ProcessPrimaryPhone(UserProfile profile, UserProfile ekosProfile, StringBuilder xml)
    {
      if (profile.PrimaryPhone.IsNotNull())
      {
        IComparer<Phone> phoneComparer = new PhonePartialComparer();
        if (ekosProfile.IsNull() || ekosProfile.PrimaryPhone.IsNull() ||
            phoneComparer.Compare(profile.PrimaryPhone, ekosProfile.PrimaryPhone) != 0)
        {
          if (profile.PrimaryPhone.PhoneType == PhoneType.Fax)
          {
            xml.Append(PhoneNumberToXml("fax_pri", profile.PrimaryPhone.PhoneNumber));
            xml.Append(PhoneNumberToXml("phone_pri", string.Empty));
            xml.Append(BoolToXml("phone_pri_flag", false));
            xml.Append(BoolToXml("fax_flag", true));
          }
          else
          {
            xml.Append(PhoneNumberToXml("phone_pri", profile.PrimaryPhone.PhoneNumber));
            xml.Append(PhoneNumberToXml("fax_pri", string.Empty));
            xml.Append(BoolToXml("phone_pri_flag", true));
            xml.Append(BoolToXml("fax_flag", false));
          }
        }
      }
      else
      {
        xml.Append(PhoneNumberToXml("phone_pri", string.Empty));
        xml.Append(PhoneNumberToXml("fax_pri", string.Empty));
        xml.Append(BoolToXml("phone_pri_flag", false));
        xml.Append(BoolToXml("fax_flag", false));
      }
    }

    private void ProcessUsername(JobSeekerModel ekosModel, JobSeekerModel model, StringBuilder xml)
    {
      if (ekosModel.IsNull() ||
          (ekosModel.IsNotNull() && model.Username != ekosModel.Username))
      {
        xml.Append(StringToXml("username", model.Username));
      }
    }

    private void ProcessUserGivenName(UserProfile ekosProfile, UserProfile profile, StringBuilder xml)
    {
      if (ekosProfile.IsNull() ||
          (ekosProfile.IsNotNull() && profile.UserGivenName.FirstName != ekosProfile.UserGivenName.FirstName))
        xml.Append(StringToXml("first_name", profile.UserGivenName.FirstName.Trim(20, false)));
      if (ekosProfile.IsNull() ||
          (ekosProfile.IsNotNull() && profile.UserGivenName.LastName != ekosProfile.UserGivenName.LastName))
        xml.Append(StringToXml("last_name", profile.UserGivenName.LastName.Trim(20, false)));
      string profileMiddleName = profile.UserGivenName.MiddleName;
      if (profileMiddleName.IsNull()) profileMiddleName = string.Empty;
      string ekosMiddleName = ekosProfile.IsNull() ? string.Empty : ekosProfile.UserGivenName.IsNull() ? string.Empty : ekosProfile.UserGivenName.MiddleName;
      if (ekosMiddleName.IsNull()) ekosMiddleName = string.Empty;
      if (profileMiddleName != ekosMiddleName)
        xml.Append(StringToXml("mi",
          profile.UserGivenName.MiddleName.IsNotNullOrEmpty()
            ? profile.UserGivenName.MiddleName.Substring(0, 1)
            : string.Empty));
    }

    private void ProcessSocialSecurityNumber(JobSeekerModel seeker, JobSeekerModel ekosJobSeeker, StringBuilder xml)
    {
      if (ekosJobSeeker.IsNull() ||
          (ekosJobSeeker.IsNotNull() &&
          (seeker.SocialSecurityNumber.IsNull() ? string.Empty : seeker.SocialSecurityNumber) != ekosJobSeeker.SocialSecurityNumber))
        xml.Append(seeker.SocialSecurityNumber.IsNullOrEmpty()
          ? BoolToXml("ssn_not_provided_flag", true)
          : StringToXml("ssn", seeker.SocialSecurityNumber));
    }

    private void ProcessEthnicHeritage(UserProfile ekosProfile, UserProfile profile, StringBuilder xml)
    {
      StringBuilder ethnicHeritageXml = new StringBuilder();
      // ethic heritage race ids in ekosProfile are internal ids.
      EthnicHeritage ekosEthnicHeritage = new EthnicHeritage();
      EthnicHeritage profileEthnicHeritage = new EthnicHeritage();
      if (ekosProfile.IsNotNull() && ekosProfile.EthnicHeritage.IsNotNull())
      {
        ekosEthnicHeritage = ekosProfile.EthnicHeritage;
      }

      if (profile.EthnicHeritage.IsNotNull())
      {
        profileEthnicHeritage = profile.EthnicHeritage;
      }

      List<long?> raceIdsToDelete = new List<long?>();
      if (ekosEthnicHeritage.RaceIds.IsNotNullOrEmpty())
      {
        raceIdsToDelete = ekosEthnicHeritage.RaceIds;
        if (profileEthnicHeritage.RaceIds.IsNotNullOrEmpty())
          raceIdsToDelete = raceIdsToDelete.Except(profileEthnicHeritage.RaceIds).ToList();
      }

      List<long?> raceIdsToInsert = new List<long?>();
      if (profileEthnicHeritage.RaceIds.IsNotNullOrEmpty())
      {
        raceIdsToInsert = profileEthnicHeritage.RaceIds;
        if (ekosEthnicHeritage.RaceIds.IsNotNullOrEmpty())
          raceIdsToInsert = raceIdsToInsert.Except(ekosEthnicHeritage.RaceIds).ToList();
      }

      raceIdsToDelete.ForEach(race =>
      {
        string externalLookupId = ConvertToExternallId(ExternalLookUpType.Race, race.ToString());
        if (externalLookupId.IsNotNullOrEmpty())
          ethnicHeritageXml.AppendFormat("<ethnic_heritage ethnic_id='{0}' method='delete'></ethnic_heritage>", externalLookupId);
      });

      raceIdsToInsert.ForEach(race =>
      {
        string externalLookupId = ConvertToExternallId(ExternalLookUpType.Race, race.ToString());
        if (externalLookupId.IsNotNullOrEmpty() && externalLookupId != "-1")
          ethnicHeritageXml.AppendFormat("<ethnic_heritage ethnic_id='{0}' method='insert'><selection_flag>1</selection_flag></ethnic_heritage>", externalLookupId);
      });

      if (ekosEthnicHeritage.EthnicHeritageId.IsNotNullOrZero() &&
        (profileEthnicHeritage.EthnicHeritageId.IsNull() || profileEthnicHeritage.EthnicHeritageId.GetValueOrDefault() == 1001303))
      {
        ethnicHeritageXml.Append("<ethnic_heritage ethnic_id='3' method='delete'></ethnic_heritage>");
      }
      else if (ekosEthnicHeritage.EthnicHeritageId.IsNotNullOrZero() &&
        ekosEthnicHeritage.EthnicHeritageId != profileEthnicHeritage.EthnicHeritageId)
      {
        ethnicHeritageXml.AppendFormat("<ethnic_heritage ethnic_id='3' method='update'><selection_flag>{0}</selection_flag></ethnic_heritage>",
          ConvertToExternallId(ExternalLookUpType.Ethnicity, profileEthnicHeritage.EthnicHeritageId.ToString()));
      }
      else if (profileEthnicHeritage.EthnicHeritageId.IsNotNullOrZero() &&
        profileEthnicHeritage.EthnicHeritageId.GetValueOrDefault() != 1001303 &&
        ekosEthnicHeritage.EthnicHeritageId != profileEthnicHeritage.EthnicHeritageId)
      {
        ethnicHeritageXml.AppendFormat("<ethnic_heritage ethnic_id='3' method='insert'><selection_flag>{0}</selection_flag></ethnic_heritage>",
          ConvertToExternallId(ExternalLookUpType.Ethnicity, profileEthnicHeritage.EthnicHeritageId.ToString()));
      }

      if (ethnicHeritageXml.Length > 0)
      {
        xml.Append("<ethnic_heritages>");
        xml.Append(ethnicHeritageXml);
        xml.Append("</ethnic_heritages>");
      }
    }

    #endregion
  }

  internal class LicenceInfoPartialComparer : IComparer<LicenseInfo>
  {
    public int Compare(LicenseInfo x, LicenseInfo y)
    {
      if (x.IsNull() && y.IsNull()) return 0;
      if (x.IsNull() && y.IsNotNull()) return -1;
      if (x.IsNotNull() && y.IsNull()) return 1;
      List<long> xDrivingLicenceEndorsementIds = x.DrivingLicenceEndorsementIds;
      List<long> yDrivingLicenceEndorsementIds = y.DrivingLicenceEndorsementIds;
      if (xDrivingLicenceEndorsementIds.IsNull()) xDrivingLicenceEndorsementIds = new List<long>();
      if (yDrivingLicenceEndorsementIds.IsNull()) yDrivingLicenceEndorsementIds = new List<long>();
      if (x.DriverClassText == y.DriverClassText &&
          x.DriverStateId.GetValueOrDefault() == y.DriverStateId.GetValueOrDefault() &&
          x.DrivingLicenceClassId.GetValueOrDefault() == y.DrivingLicenceClassId.GetValueOrDefault() &&
          xDrivingLicenceEndorsementIds.SequenceEqual(yDrivingLicenceEndorsementIds) &&
          x.HasDriverLicense == y.HasDriverLicense) return 0;
      return -1;
    }
  }

  internal class PhonePartialComparer : IComparer<Phone>
  {
    public int Compare(Phone x, Phone y)
    {
      if (x.IsNull() && y.IsNull()) return 0;
      if (x.IsNull() && y.IsNotNull()) return -1;
      if (x.IsNotNull() && y.IsNull()) return 1;
      if (x.PhoneType == y.PhoneType &&
          x.PhoneNumber == y.PhoneNumber &&
          x.Provider == y.Provider &&
          x.Extention == y.Extention) return 0;
      return -1;
    }
  }

  /// <summary>
  /// A comparer for the <see cref="VeteranInfo"/> type. Makes a comparison based on the properties that are passed to AOSOS.
  /// </summary>
  internal class VeteranInfoPartialComparer : IComparer<VeteranInfo>
  {
    /// <summary>
    /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
    /// </summary>
    /// <param name="x">The first object to compare.</param>
    /// <param name="y">The second object to compare.</param>
    /// <returns>
    /// A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />, as shown in the following table.Value Meaning Less than zero<paramref name="x" /> is less than <paramref name="y" />.Zero<paramref name="x" /> equals <paramref name="y" />.Greater than zero<paramref name="x" /> is greater than <paramref name="y" />.
    /// </returns>
    public int Compare(VeteranInfo x, VeteranInfo y)
    {
      if (x.IsNull() && y.IsNull()) return 0;
      if (x.IsNull() && y.IsNotNull()) return -1;
      if (x.IsNotNull() && y.IsNull()) return 1;
      // if neither are veterans then the rest just doesn't matter
      if ((bool)!x.IsVeteran && (bool)!y.IsVeteran) return 0;
      if (x.IsVeteran == y.IsVeteran &&
          x.History[0].VeteranStartDate == y.History[0].VeteranStartDate &&
          x.History[0].VeteranEndDate == y.History[0].VeteranEndDate &&
          x.History[0].VeteranEra == y.History[0].VeteranEra &&
          x.History[0].VeteranDisabilityStatus == y.History[0].VeteranDisabilityStatus &&
          x.History[0].TransitionType == y.History[0].TransitionType &&
          x.History[0].IsCampaignVeteran == y.History[0].IsCampaignVeteran &&
          x.IsHomeless == y.IsHomeless &&
          x.History[0].MilitaryBranchOfServiceId == y.History[0].MilitaryBranchOfServiceId) return 0;
      return -1;
    }
  }

  internal class DesiredTitlePartialEqualityComparer : IEqualityComparer<DesiredTitle>
  {
    public bool Equals(DesiredTitle x, DesiredTitle y)
    {
      if (x.IsNull() && y.IsNull()) return true;
      if (x.IsNotNull() && y.IsNull()) return false;
      if (x.IsNull() && y.IsNotNull()) return false;
      return x.Onet == y.Onet;
    }

    public int GetHashCode(DesiredTitle obj)
    {
      int hash = 23;
      if (obj.Onet.IsNotNullOrEmpty()) hash = hash * 13 + obj.Onet.GetHashCode();
      return hash;
    }
  }

  internal class DesiredZipPartialEqualityComparer : IEqualityComparer<DesiredZip>
  {
    public bool Equals(DesiredZip x, DesiredZip y)
    {
      if (x.IsNull() && y.IsNull()) return true;
      if (x.IsNotNull() && y.IsNull()) return false;
      if (x.IsNull() && y.IsNotNull()) return false;
      return x.Zip == y.Zip;
    }

    public int GetHashCode(DesiredZip obj)
    {
      int hash = 23;
      if (obj.Zip.IsNotNullOrEmpty()) hash = hash * 13 + obj.Zip.GetHashCode();
      return hash;
    }
  }

  internal class DesiredZipFullEqualityComparer : IEqualityComparer<DesiredZip>
  {
    public bool Equals(DesiredZip x, DesiredZip y)
    {
      if (x.IsNull() && y.IsNull()) return true;
      if (x.IsNotNull() && y.IsNull()) return false;
      if (x.IsNull() && y.IsNotNull()) return false;
      return x.Zip == y.Zip && x.Radius == y.Radius;
    }

    public int GetHashCode(DesiredZip obj)
    {
      int hash = 23;
      if (obj.Zip.IsNotNullOrEmpty()) hash = hash * 13 + obj.Zip.GetHashCode();
      hash = hash * 13 + obj.Radius.GetValueOrDefault(0).GetHashCode();
      return hash;
    }
  }

  internal class DesiredTitleFullEqualityComparer : IEqualityComparer<DesiredTitle>
  {
    public bool Equals(DesiredTitle x, DesiredTitle y)
    {
      if (x.IsNull() && y.IsNull()) return true;
      if (x.IsNotNull() && y.IsNull()) return false;
      if (x.IsNull() && y.IsNotNull()) return false;
      return x.Onet == y.Onet && x.ExperienceInMonths == y.ExperienceInMonths;
    }

    public int GetHashCode(DesiredTitle obj)
    {
      int hash = 23;
      if (obj.Onet.IsNotNullOrEmpty()) hash = hash * 13 + obj.Onet.GetHashCode();
      hash = hash * 13 + obj.ExperienceInMonths.GetHashCode();
      return hash;
    }
  }

  internal class CertificationPartialEqualityComparer : IEqualityComparer<Certification>
  {
    public bool Equals(Certification x, Certification y)
    {
      AddressModelPartialComparer addressModelPartialComparer = new AddressModelPartialComparer();
      if (x.IsNull() && y.IsNull()) return true;
      if (x.IsNotNull() && y.IsNull()) return false;
      if (x.IsNull() && y.IsNotNull()) return false;
      return x.OrganizationName == y.OrganizationName &&
             x.Certificate == y.Certificate &&
             x.CompletionDate == y.CompletionDate &&
             addressModelPartialComparer.Compare(x.CertificationAddress.ToIntegrationAddress(false, false, false, false, false), y.CertificationAddress.ToIntegrationAddress(false, false, false, false, false)) == 0;
    }

    public int GetHashCode(Certification obj)
    {
      AddressModelPartialComparer addressModelConComparer = new AddressModelPartialComparer();
      unchecked
      {
        int hash = 19; // this must be a prime number as must the multiplier below.
        if (obj.OrganizationName.IsNotNull())
          hash = hash * 13 + obj.OrganizationName.GetHashCode();
        if (obj.Certificate.IsNotNull())
          hash = hash * 13 + obj.Certificate.GetHashCode();
        if (obj.CompletionDate.IsNotNull())
          hash = hash * 13 + obj.CompletionDate.GetHashCode();
        if (obj.CertificationAddress.IsNotNull())
          hash = hash * 13 + addressModelConComparer.GetHashCode(obj.CertificationAddress.ToIntegrationAddress(false, false, false, false, false));
        return hash;
      }
    }
  }

  internal class SchoolPartialEqualityComparer : IEqualityComparer<School>
  {
    public bool Equals(School x, School y)
    {
      if (x.IsNull() && y.IsNull()) return true;
      if (x.IsNotNull() && y.IsNull()) return false;
      if (x.IsNull() && y.IsNotNull()) return false;
      return x.Major == y.Major &&
             x.Degree == y.Degree &&
             x.Institution == y.Institution &&
             x.CompletionDate == y.CompletionDate &&
             x.GradePointAverage == y.GradePointAverage &&
             new AddressPartialComparer().Compare(x.SchoolAddress, y.SchoolAddress) == 0;
    }

    public int GetHashCode(School obj)
    {
      AddressPartialComparer addressComparer = new AddressPartialComparer();
      unchecked
      {
        int hash = 31; // this should be a prime number, as should the multiplier below.
        if (obj.Major.IsNotNull())
          hash = hash * 17 + obj.Major.GetHashCode();
        if (obj.Degree.IsNotNull())
          hash = hash * 17 + obj.Degree.GetHashCode();
        if (obj.Institution.IsNotNull())
          hash = hash * 17 + obj.Institution.GetHashCode();
        if (obj.CompletionDate.IsNotNull())
          hash = hash * 17 + obj.CompletionDate.GetHashCode();
        if (obj.GradePointAverage.IsNotNull())
          hash = hash * 17 + obj.GradePointAverage.GetHashCode();
        if (obj.SchoolAddress.IsNotNull())
          hash = hash * 17 + addressComparer.GetHashCode(obj.SchoolAddress);
        return hash;
      }
    }
  }

  internal class AddressPartialComparer : IComparer<Address>
  {
    public int Compare(Address x, Address y)
    {
      if (x.IsNull() && y.IsNull()) return 0;
      if (x.IsNotNull() && y.IsNull()) return 1;
      if (x.IsNull() && y.IsNotNull()) return -1;
      if (x.Street1 == y.Street1 &&
          x.Street2 == y.Street2 &&
          x.City == y.City &&
          x.CountyId == y.CountyId &&
          x.CountryId == y.CountryId &&
          x.StateId == y.StateId &&
          x.Zip == y.Zip)
        return 0;
      return -1;
    }

    public int GetHashCode(Address obj)
    {
      unchecked
      {
        int hash = 23;
        if (obj.Street1.IsNotNullOrEmpty())
          hash = hash * 23 + obj.Street1.GetHashCode();
        if (obj.Street2.IsNotNullOrEmpty())
          hash = hash * 23 + obj.Street2.GetHashCode();
        if (obj.City.IsNotNullOrEmpty())
          hash = hash * 23 + obj.City.GetHashCode();
        if (obj.CountyId.HasValue)
          hash = hash * 23 + obj.CountyId.GetHashCode();
        if (obj.CountryId.HasValue)
          hash = hash * 23 + obj.CountryId.GetHashCode();
        if (obj.StateId.HasValue)
          hash = hash * 23 + obj.StateId.GetHashCode();
        if (obj.Zip.IsNotNullOrEmpty())
          hash = hash * 23 + obj.Zip.GetHashCode();
        return hash;
      }
    }
  }

  internal class AddressModelPartialComparer : IComparer<AddressModel>
  {
    public int Compare(AddressModel x, AddressModel y)
    {
      if (x.IsNull() && y.IsNull()) return 0;
      if (x.IsNotNull() && y.IsNull()) return 1;
      if (x.IsNull() && y.IsNotNull()) return -1;
      if (x.AddressLine1 == y.AddressLine1 &&
          x.AddressLine2 == y.AddressLine2 &&
          x.AddressLine3 == y.AddressLine3 &&
          x.City == y.City &&
          x.CountyId == y.CountyId &&
          x.CountryId == y.CountryId &&
          x.StateId == y.StateId &&
          x.PostCode == y.PostCode)
        return 0;
      return -1;
    }

    public int GetHashCode(AddressModel obj)
    {
      unchecked
      {
        int hash = 23;
        if (obj.AddressLine1.IsNotNullOrEmpty())
          hash = hash * 23 + obj.AddressLine1.GetHashCode();
        if (obj.AddressLine2.IsNotNullOrEmpty())
          hash = hash * 23 + obj.AddressLine2.GetHashCode();
        if (obj.AddressLine3.IsNotNullOrEmpty())
          hash = hash * 23 + obj.AddressLine3.GetHashCode();
        if (obj.City.IsNotNullOrEmpty())
          hash = hash * 23 + obj.City.GetHashCode();
        hash = hash * 23 + obj.CountyId.GetHashCode();
        hash = hash * 23 + obj.CountryId.GetHashCode();
        hash = hash * 23 + obj.StateId.GetHashCode();
        if (obj.PostCode.IsNotNullOrEmpty())
          hash = hash * 23 + obj.PostCode.GetHashCode();
        return hash;
      }
    }
  }
}
