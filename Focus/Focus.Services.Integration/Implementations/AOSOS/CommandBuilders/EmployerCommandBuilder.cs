﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Integration;

using Framework.Core;

#endregion

namespace Focus.Services.Integration.Implementations.AOSOS.CommandBuilders
{
  public class EmployerCommandBuilder : CommandBuilder
  {
		private AOSOSClientRepository _clientRepository;

		public EmployerCommandBuilder(ClientSettings clientSettings, List<ExternalLookUpItemDto> externalLookUpItems, AOSOSClientRepository clientRepository)
      : base(clientSettings, externalLookUpItems)
    {
			_clientRepository = clientRepository;
    }

    #region Employer Requests

    /// <summary>
    /// Assigns the admin to the employer.
    /// </summary>
    /// <param name="employerId">The employer identifier.</param>
    /// <param name="adminId">The admin identifier.</param>
    /// <param name="version">The version.</param>
    /// <returns></returns>
    public string AssignAdminToEmployer(string employerId, string adminId, string version, string externalUsername, string externalPassword, string externalOfficeId, string externalAdminId, Guid? uuId = null)
    {
      var request = string.Format("<employers><employer method='update' id='{0}' version='{1}' uuid='{2}'><admin_id>{3}</admin_id></employer></employers>", employerId.HtmlEncode(), version, uuId ?? Guid.NewGuid(), adminId.HtmlEncode());
      return GetBaseRequestIncludingOfficeIdAndAdminId(request, externalUsername, externalPassword, externalOfficeId, externalAdminId);
    }

    /// <summary>
    /// Saves the employer.
    /// </summary>
    /// <param name="employer">The employer.</param>
    /// <param name="version">The employer version.</param>
    /// <param name="settings">The client settings.</param>
    /// <returns></returns>
    public string SaveEmployer(EmployerModel employer, string version, string externalUsername, string externalPassword, string externalOfficeId, string externalAdminId)
    {
			var employerXml = EmployerToXml(employer, AOSOSSettings, externalOfficeId, externalAdminId, externalUsername);
      if (employerXml.IsNotNullOrEmpty())
      {
        var request = employer.ExternalId.IsNotNullOrEmpty()
                        ? string.Format("<employers><employer id='{0}' method='update' version='{1}' uuid='{2}'>{3}</employer></employers>", employer.ExternalId, version, employer.TrackingId, employerXml)
                        : string.Format("<employers><employer method='insert' uuid='{0}' ct_aggr_version='{1}'>{2}</employer></employers>", employer.TrackingId, CtAggrVersion, employerXml);
        return GetBaseRequestIncludingOfficeIdAndAdminId(request, externalUsername, externalPassword, externalOfficeId, externalAdminId);
      }

      return "";
    }

    /// <summary>
    /// Saves the employee.
    /// </summary>
    /// <param name="employee">The employee.</param>
    /// <param name="version">The version.</param>
    /// <returns></returns>
    public string SaveEmployee(EmployeeModel employee, string version, string externalUsername, string externalPassword, string externalOfficeId, string externalAdminId)
    {
      var employeeXml = EmployeeToXml(employee, AOSOSSettings, externalAdminId);
      if (employeeXml.IsNullOrEmpty())
        return string.Empty;

      var request = employee.ExternalId.IsNotNullOrEmpty()
                        ? string.Format("<employers><employer id='{0}' method='update' version='{1}' uuid='{2}'><contacts><contact id='{3}' uuid='{5}' method='update'>{4}</contact></contacts></employer></employers>", employee.ExternalEmployerId, version, Guid.NewGuid(), employee.ExternalId, employeeXml, employee.TrackingId)
                        : string.Format("<employers><employer id='{0}' method='update' version='{1}' uuid='{2}'><contacts><contact uuid='{4}' method='insert'>{3}</contact></contacts></employer></employers>", employee.ExternalEmployerId, version, Guid.NewGuid(), employeeXml, employee.TrackingId);
      return GetBaseRequestIncludingOfficeIdAndAdminId(request, externalUsername, externalPassword, externalOfficeId, externalAdminId);
    }

    /// <summary>
    /// Deletes the employer.
    /// </summary>
    /// <param name="employerId">The employer id.</param>
    /// <param name="trackingId">The tracking id.</param>
    /// <returns></returns>
    public string DeleteEmployer(string employerId, string trackingId)
    {
      throw new NotImplementedException();
      /*var request = string.Format("<employers><employer method='delete' id='{0}' uuid='{1}' ct_aggr_version='{2}'/></employers>", employerId, trackingId, CtAggrVersion);
      return GetBaseRequest(request);*/
    }

    /// <summary>
    /// Gets the employer by id.
    /// </summary>
    /// <param name="employerId">The employer id.</param>
    /// <returns></returns>
    public string GetEmployerById(string employerId)
    {
      var request = string.Format("<employers><employer id='{0}' method='select' expand='*'/></employers>", employerId.HtmlEncode());

      return GetBaseRequest(request);
    }

    /// <summary>
    /// Gets the employer by fein.
    /// </summary>
    /// <param name="federalEmployerIdentificationNumber">The federal employer identification number.</param>
    /// <returns></returns>
    public string GetEmployerByFein(string federalEmployerIdentificationNumber, bool includeEmployees = false)
    {
      var request = string.Format("<employer_search method='query' expand='legal_name,company,admin_id,office_id,sein,address.*,phone_pri,phone_sec,fax_pri,email,url_pri,url_sec,naics,sein,fein,business_desc,owner{1}'><fein>{0}</fein></employer_search>", federalEmployerIdentificationNumber.Replace("-", "").HtmlEncode(), includeEmployees ? ",contacts" : "");

      return GetBaseRequest(request);
    }

    /// <summary>
    /// Gets the name of the employer by legal.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <returns></returns>
    public string GetEmployerByLegalName(string name)
    {
      var request = string.Format("<employer_search method='query' expand='legal_name,company,admin_id,office_id,sein,address.*,phone_pri,phone_sec,fax_pri,email,url_pri,url_sec,naics,sein,fein,business_desc,owner'><legal_name>{0}</legal_name></employer_search>", name.HtmlEncode());

      return GetBaseRequest(request);
    }

    /// <summary>
    /// Gets the name of the employer by company.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <returns></returns>
    public string GetEmployerByCompanyName(string name)
    {
      var request = string.Format("<employer_search method='query' expand='legal_name,company,admin_id,office_id,sein,address.*,phone_pri,phone_sec,fax_pri,email,url_pri,url_sec,naics,sein,fein,business_desc,owner'><company>{0}</company></employer_search>", name.HtmlEncode());

      return GetBaseRequest(request);
    }


    /// <summary>
    /// Saves the activity.
    /// </summary>
    /// <param name="employerId">The employer identifier.</param>
    /// <param name="employeeId">The employee identifier.</param>
    /// <param name="activityId">The activity identifier.</param>
    /// <param name="adminId">The admin identifier.</param>
    /// <param name="officeId">The office identifier.</param>
    /// <param name="version">The version.</param>
    /// <param name="uuid">The UUID (Only used in unit tests)</param>
    /// <param name="requestDate">The request date.(Only used in unit tests)</param>
    /// <returns></returns>
    public string SaveActivity(string employerId, string employeeId, string activityId, string adminId, string officeId, string version, string externalUsername, string externalPassword, string externalOfficeId, string externalAdminId, Guid? uuid = null, DateTime? requestDate = null)
    {
      requestDate = requestDate ?? DateTime.Now;
      uuid = uuid ?? Guid.NewGuid();
      var request =
        string.Format(
          "<employers><employer method='update' id='{0}' version='{1}' uuid='{2}'><activities><activity uuid='{2}' method='insert'><contact_id>{3}</contact_id><employer_service_type_cd>{4}</employer_service_type_cd>{5}<ctime>{6}</ctime></activity></activities></employer></employers>",
          employerId.HtmlEncode(), version.HtmlEncode(), uuid, employeeId.HtmlEncode(), activityId.HtmlEncode(), GetActivityAdminAndOfficeCommand(adminId, officeId), requestDate.Value.ToString("MMddyyyy"));
      return GetBaseRequestIncludingOfficeIdAndAdminId(request, externalUsername, externalPassword, externalOfficeId, externalAdminId);
    }

    #endregion

    #region XML builders

    /// <summary>
    /// Employee to XML.
    /// </summary>
    /// <param name="employee">The employee.</param>
    /// <param name="clientSettings">The client settings.</param>
    /// <param name="externalAdminId">The external admin identifier.</param>
    /// <returns></returns>
    internal string EmployeeToXml(EmployeeModel employee, ClientSettings clientSettings, string externalAdminId = null)
    {
      var xml = new StringBuilder();
      if (employee != null)
      {
        xml.Append(StringToXml("salutation_cd", ConvertToExternallId(ExternalLookUpType.Salutation, employee.SalutationId.ToString())));
        xml.Append(StringToXml("first_name", employee.FirstName.Trim(20, false)));
        xml.Append(StringToXml("last_name", employee.LastName.Trim(20, false)));
        xml.Append(PhoneNumberToXml("phone_pri", employee.Phone));
        xml.Append(StringToXml("phone_pri_ext", employee.PhoneExt));
        xml.Append(PhoneNumberToXml("phone_sec", employee.AltPhone));
        xml.Append(StringToXml("phone_sec_ext", employee.AltPhoneExt));
        xml.Append(PhoneNumberToXml("fax_pri", employee.Fax));
        if (employee.Email.Length < 80)
          xml.Append(StringToXml("email", employee.Email));
				
				if (externalAdminId.IsNullOrEmpty() && _clientRepository.IsNotNull())
				{
					var externalOffice = ConvertFirstToExternalId(ExternalLookUpType.IntegrationOfficeIdPerZip, employee.Address.PostCode);
					if (externalOffice.IsNotNullOrEmpty())
					{
						externalAdminId = _clientRepository.GetExternalAdminId(string.Format("selfreg{0}", externalOffice).ToLowerInvariant());
					}
				}

        if (employee.ExternalAdminId.IsNullOrEmpty())
        {
          xml.Append(externalAdminId.IsNullOrEmpty()
                       ? StringToXml("admin_id", clientSettings.DefaultAdminId)
                       : StringToXml("admin_id", externalAdminId));
        }
        else
        {
          xml.Append(StringToXml("admin_id", employee.ExternalAdminId));
        }

        if (employee.ExternalEmployerId.IsNotNullOrEmpty())
          xml.Append(StringToXml("employer_id", employee.ExternalEmployerId));

        if (employee.JobTitle.IsNotNullOrEmpty())
          xml.Append(StringToXml("title", employee.JobTitle.Trim(39, false)));

        xml.Append(AddressToXml(employee.Address, false));
      }

      return xml.ToString();
    }

		/// <summary>
		/// Employer to XML.
		/// </summary>
		/// <param name="employer">The employer.</param>
		/// <param name="clientSettings">The client settings.</param>
		/// <param name="externalOffice">The external office.</param>
		/// <param name="externalAdminId">The external admin identifier.</param>
		/// <param name="externalUsername">The external username.</param>
		/// <returns></returns>
		internal string EmployerToXml(EmployerModel employer, ClientSettings clientSettings, string externalOffice = null, string externalAdminId = null, string externalUsername = null)
    {
      var xml = new StringBuilder();
      if (employer != null)
      {
        xml.Append(StringToXml("employer_status_cd", "1"));
        xml.Append(StringToXml("company", employer.Name.Trim(79, false)));
        if (employer.FederalEmployerIdentificationNumber.IsNotNullOrEmpty())
          xml.Append(StringToXml("fein", employer.FederalEmployerIdentificationNumber.Replace("-", "")));

        if (employer.StateEmployerIdentificationNumber.IsNotNullOrEmpty())
          xml.Append(StringToXml("sein", employer.StateEmployerIdentificationNumber.Replace("-", "")));

        if (employer.Naics.Contains(" "))
          xml.Append(StringToXml("naics", employer.Naics.Split(Convert.ToChar(" "))[0]));
        else
          xml.Append(StringToXml("naics", employer.Naics));

        xml.Append(StringToXml("legal_name", employer.LegalName.Trim(79, false)));
        xml.Append(StringToXml("owner", ConvertToExternallId(ExternalLookUpType.EmployerOwner, employer.Owner.ToString())));
        xml.Append(StringToXml("business_desc", employer.BusinessDescription));
        xml.Append(PhoneNumberToXml("phone_pri", employer.Phone));
        xml.Append(StringToXml("phone_pri_ext", employer.PhoneExt));
        xml.Append(PhoneNumberToXml("phone_sec", employer.AltPhone));
        xml.Append(StringToXml("phone_sec_ext", employer.AltPhoneExt));
        xml.Append(PhoneNumberToXml("fax_pri", employer.Fax));
        xml.Append(PhoneNumberToXml("email", employer.Email));
        xml.Append(StringToXml("url_pri", employer.Url).Replace(@"http:\\\\", @"http:\\"));
        xml.Append(BoolToXml("public_transport_flag", (bool?)employer.PublicTransportAccessible));

	      if (externalOffice.IsNullOrEmpty())
	      {
		      externalOffice = ConvertFirstToExternalId(ExternalLookUpType.IntegrationOfficeIdPerZip, employer.Address.PostCode);

		      // switch to the zip of the first employee (if there are any with a zip that is).
		      Func<EmployeeModel, bool> predicate =
			      employee => employee.Address.IsNotNull() && employee.Address.PostCode.IsNotNullOrEmpty();

		      if (employer.Employees.Any(predicate))
		      {
			      externalOffice = ConvertFirstToExternalId(ExternalLookUpType.IntegrationOfficeIdPerZip, employer.Employees.First(predicate).Address.PostCode);
		      }
	      }

	      if (externalAdminId.IsNullOrEmpty() && externalOffice.IsNotNullOrEmpty() && _clientRepository.IsNotNull())
				{
					externalAdminId = _clientRepository.GetExternalAdminId(string.Format("selfreg{0}", externalOffice).ToLowerInvariant());
				}

        if (employer.ExternalId.IsNullOrEmpty())
        {
          xml.Append(externalAdminId.IsNullOrEmpty()
                       ? StringToXml("admin_id", clientSettings.DefaultAdminId) 
                       : StringToXml("admin_id", externalAdminId));

          xml.Append(externalOffice.IsNullOrEmpty()
                       ? StringToXml("office_id", clientSettings.DefaultOfficeId)
                       : StringToXml("office_id", externalOffice));
        }
				else if (externalUsername.IsNotNull() && externalUsername.StartsWith("selfreg", StringComparison.OrdinalIgnoreCase))
        {
          // update
	        var existingAdmin = employer.ExternalAdminId;
	        if (existingAdmin.IsNullOrEmpty())
						existingAdmin = externalAdminId.IsNullOrEmpty() ? "" : externalAdminId;

          xml.Append(StringToXml("admin_id", existingAdmin));

	        var existingOffice = employer.ExternalOfficeId;
					if (existingOffice.IsNullOrEmpty())
		        existingOffice = externalOffice.IsNullOrEmpty() ? clientSettings.DefaultOfficeId : externalOffice;

					xml.Append(StringToXml("office_id", existingOffice));
        }

        if (employer.ExternalId.IsNullOrEmpty())
          xml.Append(StringToXml("origination_method", "8"));

        var employerStatus = 0;
        switch (employer.Status)
        {
          case EmployerStatusTypes.Active:
            employerStatus = 1;
            break;
          case EmployerStatusTypes.InActive:
            employerStatus = 3;
            break;
          case EmployerStatusTypes.Delete:
            employerStatus = 4;
            break;
          case EmployerStatusTypes.Archive:
            employerStatus = 5;
            break;
        }

        if (employerStatus > 0)
          xml.Append(IntToXml("employer_status_cd", employerStatus));

        if (employer.Employees.IsNotNullOrEmpty())
        {
          xml.Append("<contacts>");
          foreach (var contact in employer.Employees)
          {
            if (contact.ExternalId.IsNullOrEmpty())
              xml.AppendFormat("<contact uuid='{0}' method='insert'>", contact.TrackingId);
            else
              xml.AppendFormat("<contact uuid='{0}' method='update' id='{1}' >", contact.TrackingId, contact.ExternalId);

            xml.Append(EmployeeToXml(contact, clientSettings));
            xml.Append("</contact>");
          }

          xml.Append("</contacts>");
        }

        if (employer.Activities.IsNotNullOrEmpty())
        {
          xml.Append("<activities>");
          foreach (var activity in employer.Activities)
          {
            xml.AppendFormat("<activity uuid='{0}' method='insert'>", activity.TrackingId);
            xml.Append(EmployerActivityToXml(activity));
            xml.Append("</activity>");
          }

          xml.Append("</activities>");
        }

        xml.Append(AddressToXml(employer.Address));
      }

      var retVal = xml.ToString();
      return retVal;
    }

    #endregion



  }
}
