﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Integration;

using Framework.Core;

#endregion

namespace Focus.Services.Integration.Implementations.AOSOS.CommandBuilders
{
  public class JobCommandBuilder : CommandBuilder
  {
		private AOSOSClientRepository _clientRepository;

    public JobCommandBuilder(ClientSettings clientSettings, List<ExternalLookUpItemDto> externalLookUpItems, AOSOSClientRepository clientRepository)
      : base(clientSettings, externalLookUpItems)
    {
	    _clientRepository = clientRepository;
    }

    #region Job Requests

    /// <summary>
    /// Gets the job by id.
    /// </summary>
    /// <param name="jobId">The job id.</param>
    /// <returns></returns>
    public string GetJobById(string jobId)
    {
      const string command = "<jobs><job id='{0}' method='select' expand='*'/></jobs>";
      var request = string.Format(command, jobId);

      return GetBaseRequest(request);
    }

    /// <summary>
    /// Saves the job.
    /// </summary>
    /// <param name="job">The job.</param>
    /// <param name="ekosJob">The job from EKOS</param>
    /// <param name="version">The version.</param>
    /// <param name="uuId">The uuId.(Passed in during unit testing)</param>
    /// <returns></returns>
    public string SaveJob(JobModel job, JobModel ekosJob, string version, string externalUsername, string externalPassword, string externalOfficeId, string externalAdminId, Guid? uuId = null)
    {
      var jobXml = JobToXml(job, ekosJob, externalOfficeId, externalAdminId, externalUsername);
      if (jobXml.IsNotNullOrEmpty())
      {
        var request = job.ExternalId.IsNotNullOrEmpty()
                        ? string.Format("<jobs><job id='{0}' method='update' version='{1}' uuid='{2}' ct_aggr_version='{3}'>{4}</job></jobs>", job.ExternalId, version, uuId ?? Guid.NewGuid(), AOSOSSettings.CtAggrVersion, jobXml)
                        : string.Format("<jobs><job method='insert' uuid='{0}' ct_aggr_version='{1}'>{2}</job></jobs>", uuId ?? Guid.NewGuid(), AOSOSSettings.CtAggrVersion, jobXml);
        return GetBaseRequestIncludingOfficeIdAndAdminId(request, externalUsername, externalPassword, externalOfficeId, externalAdminId);
      }

      return string.Empty;
    }

    /// <summary>
    /// Gets the job referral information.
    /// </summary>
    /// <param name="jobId">The job identifier.</param>
    /// <returns></returns>
    public string GetJobReferralInfo(string jobId)
    {
      var request =
        string.Format(
          "<jobs><job method='select' return='job_status_cd, referral_desired_ct, referrals' id='{0}'></job></jobs>",
          jobId);
      return GetBaseRequest(request);
    }

    /// <summary>
    /// Refers the job seeker.
    /// </summary>
    /// <param name="jobId">The job identifier.</param>
    /// <param name="jobSeekerId">The job seeker identifier.</param>
    /// <returns></returns>
    public string ReferJobSeeker(string jobId, string jobSeekerId, string authUserName, string authPassword, string authOfficeId, string authAdminId)
    {
      var request =
        string.Format("<seeker_match id='{0}' method='refer'><jobs><job id='{1}'/></jobs></seeker_match>", jobSeekerId, jobId);
      return GetBaseRequestIncludingOfficeIdAndAdminId(request, authUserName, authPassword, authOfficeId, authAdminId);
    }


    /// <summary>
    /// Sets the application status.
    /// </summary>
    /// <param name="jobSeekerId">The job seeker identifier.</param>
    /// <param name="seekerVersion">The job seeker version.</param>
    /// <param name="jobId">The job identifier.</param>
    /// <param name="jobVersion">The job version.</param>
    /// <param name="applicationId">The application identifier.</param>
    /// <param name="statusId">The application status.</param>
    /// <returns></returns>
    public string SetApplicationStatus(string jobSeekerId, string seekerVersion, string jobId, string jobVersion, string applicationId, string statusId, string authUserName, string authPassword, string authOfficeId, string authAdminId)
    {
      seekerVersion = seekerVersion ?? string.Empty;
      jobVersion = jobVersion ?? string.Empty;
      applicationId = applicationId ?? string.Empty;

      var request = string.Format(
          @"<jobs>
              <job method='update' id='{3}' version='{4}' uuid='{5}'>
                <referrals>
                  <referral method='update' id='{6}' uuid='{7}'>
                    <result>{8}</result>
                    {9}
                  </referral>
                </referrals>
              </job>
            </jobs>",
          jobSeekerId.HtmlEncode(),
          seekerVersion.HtmlEncode(),
          Guid.NewGuid(),
          jobId.HtmlEncode(),
          jobVersion.HtmlEncode(),
          Guid.NewGuid(),
          applicationId.HtmlEncode(),
          Guid.NewGuid(),
          statusId.HtmlEncode(),
          statusId == "1" ? "<sal>0.01</sal><sal_unit>6</sal_unit>" : string.Empty);
      return GetBaseRequestIncludingOfficeIdAndAdminId(request, authUserName, authPassword, authOfficeId, authAdminId);
    }

    /// <summary>
    /// Gets the seekers that match a list of jobs
    /// </summary>
    /// <param name="jobIds">The list of jobs</param>
    /// <param name="authUserName"></param>
    /// <param name="authPassword"></param>
    /// <param name="authOfficeId"></param>
    /// <param name="authAdminId"></param>
    /// <returns>Xml returned from EKOS with the matching seekers</returns>
    public string GetJobMatches(IEnumerable<string> jobIds, string authUserName, string authPassword, string authOfficeId, string authAdminId)
    {
      var sb = new StringBuilder("<jobs>");
      foreach (var jobId in jobIds)
      {
        sb.Append(string.Format("<job id='{0}' method='select' expand='matches'/>", jobId.HtmlEncode()));
      }
      sb.Append("</jobs>");

      return GetBaseRequestIncludingOfficeIdAndAdminId(sb.ToString(), authUserName, authPassword, authOfficeId, authAdminId);
    }

		/// <summary>
		/// Assigns the admin to the job
		/// </summary>
		/// <param name="jobId">The job identifier.</param>
		/// <param name="adminId">The admin identifier.</param>
		/// <param name="version">The version.</param>
		/// <returns></returns>
		public string AssignAdminToJobOrder(string jobId, string adminId, string version, string externalUsername, string externalPassword, string externalOfficeId, string externalAdminId, Guid? uuId = null)
		{
			var request = string.Format("<jobs><job method='update' id='{0}' version='{1}' uuid='{2}'><admin_id>{3}</admin_id></job></jobs>", jobId.HtmlEncode(), version, uuId ?? Guid.NewGuid(), adminId.HtmlEncode());
			return GetBaseRequestIncludingOfficeIdAndAdminId(request, externalUsername, externalPassword, externalOfficeId, externalAdminId);
		}

    #endregion

    #region Transformers

    /// <summary>
    /// Jobs to XML.
    /// </summary>
    /// <param name="job">The job.</param>
    /// <param name="ekosJob">The job from EKOS</param>
    /// <param name="externalOffice">The external office.</param>
    /// <param name="externalAdminId">The external admin identifier.</param>
    /// <param name="externalUsername">The external username.</param>
    /// <returns></returns>
    private string JobToXml(JobModel job, JobModel ekosJob, string externalOffice = null, string externalAdminId = null, string externalUsername = null)
    {
      var xml = new StringBuilder();

      if (job != null)
      {
        xml.Append(StringToXml("job_status_cd", ConvertToExternallId(ExternalLookUpType.JobStatus, job.JobStatus.ToString())));
	      if (job.HiringManager.IsNotNull() && job.HiringManager.ExternalId.IsNotNull())
	      {
		      xml.Append(StringToXml("employer_contact_id", job.HiringManager.ExternalId));
	      }
	      else
	      {
					if (job.ExternalId.IsNullOrEmpty())
  					throw new Exception("Hiring Manager not specified for job");
	      }

	      if (job.Onet17Code.IsNotNull())
          xml.Append(StringToXml("onet3", job.Onet17Code.Replace("-", "").Replace(".", "")));
        else
          xml.Append(StringToXml("onet3", job.OnetCode.Replace("-", "").Replace(".", "")));
        xml.Append(StringToXml("title", job.Title.Trim(120, false)));

        if ((ekosJob.IsNull()) || (ekosJob.IsNotNull() && (ekosJob.ClosingDate != job.ClosingDate)))
          xml.Append(DateTimeToXml("last_open_date", job.ClosingDate.HasValue ? job.ClosingDate : DateTime.Now.AddMonths(1).Date));

        //xml.Append(StringToXml("duration", job.HoursPerWeek.GetValueOrDefault() >= 30 ? "3" : "6")); // Perm if hours per week is > 30
	      var duration = ConvertToExternallId(ExternalLookUpType.Duration, job.DurationId.ToString());
	      if (duration.IsNullOrEmpty())
		      duration = "3";
				xml.Append(StringToXml("duration", duration));

        xml.Append(IntToXml("position_ct", job.Openings));
        xml.Append(DecimalToXml("origination_id", job.Id));

        if (job.MaxSalary.HasValue || job.MinSalary.HasValue)
        {
          var salaryUnit = job.SalaryUnitId.IsNotNullOrZero()
            ? ConvertToExternallId(ExternalLookUpType.SalaryUnit, job.SalaryUnitId.GetValueOrDefault())
            : job.SalaryUnit;
          
          if (job.MinSalary.HasValue)
          {
            var adjustedMinSalary = AdjustSalary(job.MinSalary.Value, salaryUnit);
            xml.Append(DecimalToXml("sal_min", adjustedMinSalary));
          }
          
          if (job.MaxSalary.HasValue)
          {
            var adjustedMaxSalary = AdjustSalary(job.MaxSalary.Value, salaryUnit);
            xml.Append(DecimalToXml("sal_max", adjustedMaxSalary));
          }

          xml.Append(StringToXml("sal_unit", salaryUnit));
        }
        if (job.Shift.IsNotNullOrEmpty()) xml.Append(StringToXml("shift", job.Shift));
        xml.Append(BoolToXml("work_vary_flag", job.WorkDaysVary));
        xml.Append(BoolToXml("work_mon_flag", ((job.Weekdays & DaysOfWeek.Monday) == DaysOfWeek.Monday)));
        xml.Append(BoolToXml("work_tue_flag", ((job.Weekdays & DaysOfWeek.Tuesday) == DaysOfWeek.Tuesday)));
        xml.Append(BoolToXml("work_wed_flag", ((job.Weekdays & DaysOfWeek.Wednesday) == DaysOfWeek.Wednesday)));
        xml.Append(BoolToXml("work_thu_flag", ((job.Weekdays & DaysOfWeek.Thursday) == DaysOfWeek.Thursday)));
        xml.Append(BoolToXml("work_fri_flag", ((job.Weekdays & DaysOfWeek.Friday) == DaysOfWeek.Friday)));
        xml.Append(BoolToXml("work_sat_flag", ((job.Weekdays & DaysOfWeek.Saturday) == DaysOfWeek.Saturday)));
        xml.Append(BoolToXml("work_sun_flag", ((job.Weekdays & DaysOfWeek.Sunday) == DaysOfWeek.Sunday)));
        xml.Append(BoolToXml("public_transport_flag", job.PublicTransportAccessible));
        if (job.CourtOrderedAffirmativeAction.HasValue && job.CourtOrderedAffirmativeAction.Value)
          xml.Append(BoolToXml("affirmative_action_flag", true));
        AddressModel jobAddressModel = new AddressModel();
        if (job.Addresses.IsNotNullOrEmpty())
        {
          jobAddressModel = job.Addresses.FirstOrDefault(address => !address.FromJobAddress) ?? job.Addresses[0];
        }

        xml.Append(AddressToXml(jobAddressModel));
        if (job.MinimumExperienceMonths.IsNotNull() || job.MinimumExperienceYears.IsNotNull())
          xml.Append(IntToXml("experience", ((job.MinimumExperienceYears.GetValueOrDefault() * 12) + job.MinimumExperienceMonths.GetValueOrDefault())));
        xml.Append(StringToXml("edu", MapEduLevel(job.EducationLevel)));
        if (job.Licences.IsNotNullOrEmpty())
          xml.Append(StringToXml("license", string.Join(", ", job.Licences).TruncateString(59)));

        if (job.DrivingLicenceClassId.IsNotNull())
        {
          xml.Append(StringToXml("driver_class", ConvertToExternallId(ExternalLookUpType.DrivingLicenceClass, job.DrivingLicenceClassId.ToString())));
          if (job.DrivingLicenceEndorsements.IsNullOrEmpty()) job.DrivingLicenceEndorsements = new List<long>();
          var endorsements = GetAllLookUpsInCollection(ExternalLookUpType.DrivingLicenceEndorsements);
          foreach (var endorsement in endorsements)
          {
            xml.Append(BoolToXml(endorsement.ExternalId, job.DrivingLicenceEndorsements.Any(x => x.ToString() == endorsement.InternalId)));
          }
        }

        xml.Append(StringToXml("description", job.Description.Trim(4000, false))); // Maximum of 4000 characters
        xml.Append("<benefits>");
        if (job.ExternalId.IsNotNullOrEmpty()) // Insert benefits on insert into AOSOS then delete and insert
        {
          foreach (var benefit in ExternalLookUpItems.Where(x => x.ExternalLookUpType == ExternalLookUpType.InsuranceBenefits || x.ExternalLookUpType == ExternalLookUpType.LeaveBenefits || x.ExternalLookUpType == ExternalLookUpType.RetirementsBenefits || x.ExternalLookUpType == ExternalLookUpType.MiscellaneousBenefits))
            xml.Append(BenefitsToXml(benefit.ExternalId, "delete", null));
        }

        var workBenefit = ExternalLookUpItems.SingleOrDefault(x => x.ExternalLookUpType == ExternalLookUpType.InsuranceBenefits && x.InternalId == InsuranceBenefits.Health.ToString());
        if (workBenefit.IsNotNull())
          xml.Append(BenefitsToXml(workBenefit.ExternalId, "insert", ((job.InsuranceBenefits & InsuranceBenefits.Health) == InsuranceBenefits.Health)));

        workBenefit = ExternalLookUpItems.SingleOrDefault(x => x.ExternalLookUpType == ExternalLookUpType.InsuranceBenefits && x.InternalId == InsuranceBenefits.Dental.ToString());
        if (workBenefit.IsNotNull())
          xml.Append(BenefitsToXml(workBenefit.ExternalId, "insert", ((job.InsuranceBenefits & InsuranceBenefits.Dental) == InsuranceBenefits.Dental)));

        workBenefit = ExternalLookUpItems.SingleOrDefault(x => x.ExternalLookUpType == ExternalLookUpType.LeaveBenefits && x.InternalId == LeaveBenefits.PaidHolidays.ToString());
        if (workBenefit.IsNotNull())
          xml.Append(BenefitsToXml(workBenefit.ExternalId, "insert", ((job.LeaveBenefits & LeaveBenefits.PaidHolidays) == LeaveBenefits.PaidHolidays)));

        workBenefit = ExternalLookUpItems.SingleOrDefault(x => x.ExternalLookUpType == ExternalLookUpType.LeaveBenefits && x.InternalId == LeaveBenefits.Sick.ToString());
        if (workBenefit.IsNotNull())
          xml.Append(BenefitsToXml(workBenefit.ExternalId, "insert", ((job.LeaveBenefits & LeaveBenefits.Sick) == LeaveBenefits.Sick)));

        workBenefit = ExternalLookUpItems.SingleOrDefault(x => x.ExternalLookUpType == ExternalLookUpType.LeaveBenefits && x.InternalId == LeaveBenefits.Vacation.ToString());
        if (workBenefit.IsNotNull())
          xml.Append(BenefitsToXml(workBenefit.ExternalId, "insert", ((job.LeaveBenefits & LeaveBenefits.Vacation) == LeaveBenefits.Vacation)));

        workBenefit = ExternalLookUpItems.SingleOrDefault(x => x.ExternalLookUpType == ExternalLookUpType.RetirementsBenefits && x.InternalId == RetirementBenefits.PensionPlan.ToString());
        if (workBenefit.IsNotNull())
          xml.Append(BenefitsToXml(workBenefit.ExternalId, "insert", ((job.RetirementBenefits & RetirementBenefits.PensionPlan) == RetirementBenefits.PensionPlan)));

        workBenefit = ExternalLookUpItems.SingleOrDefault(x => x.ExternalLookUpType == ExternalLookUpType.MiscellaneousBenefits && x.InternalId == MiscellaneousBenefits.ClothingAllowance.ToString());
        if (workBenefit.IsNotNull())
          xml.Append(BenefitsToXml(workBenefit.ExternalId, "insert", ((job.MiscellaneousBenefits & MiscellaneousBenefits.ClothingAllowance) == MiscellaneousBenefits.ClothingAllowance)));

        workBenefit = ExternalLookUpItems.SingleOrDefault(x => x.ExternalLookUpType == ExternalLookUpType.MiscellaneousBenefits && x.InternalId == MiscellaneousBenefits.ChildCare.ToString());
        if (workBenefit.IsNotNull())
          xml.Append(BenefitsToXml(workBenefit.ExternalId, "insert", ((job.MiscellaneousBenefits & MiscellaneousBenefits.ChildCare) == MiscellaneousBenefits.ChildCare)));

        xml.Append("</benefits>");

				// See FVN-3577 - Due to EKOS wanting the Employer to have a URL when URL is selected as a contact method (a requirement that does not exist in Focus), 
				//                don't send URL as a contact method, but send Email. Contact method is not actually used in EKOS by staff.
				// Also see FVN-3590
	      var contactByEmail = ((job.InterviewContactPreferences & ContactMethods.Email) == ContactMethods.Email)
															|| ((job.InterviewContactPreferences & ContactMethods.Online) == ContactMethods.Online)
															|| ((job.InterviewContactPreferences & ContactMethods.Fax) == ContactMethods.Fax)
															|| ((job.InterviewContactPreferences & ContactMethods.Telephone) == ContactMethods.Telephone)
															|| (job.InterviewContactPreferences == ContactMethods.FocusCareer)
															|| (job.InterviewContactPreferences == ContactMethods.FocusCareer)
															|| (job.InterviewContactPreferences == ContactMethods.FocusTalent);

        xml.Append(BoolToXml("contact_email_flag", contactByEmail));
				xml.Append(BoolToXml("contact_fax_flag", false));
				xml.Append(BoolToXml("contact_phone_flag", false));
				xml.Append(BoolToXml("contact_postal_flag", ((job.InterviewContactPreferences & ContactMethods.Mail) == ContactMethods.Mail)));
        xml.Append(BoolToXml("contact_send_direct_flag", ((job.InterviewContactPreferences & ContactMethods.InPerson) == ContactMethods.InPerson)));
				xml.Append(BoolToXml("contact_url_flag", false));
				xml.Append(StringToXml("url_pri", ""));

	      var originId = job.OriginId.GetValueOrDefault().IsIn(7, 8, 9) ? AOSOSSettings.OriginationMethod : "999";
        xml.Append(StringToXml("origination_method", originId));

				externalOffice = externalUsername.IsNotNullOrEmpty() && externalUsername.StartsWith("selfreg", StringComparison.OrdinalIgnoreCase) 
					? jobAddressModel.OfficeExternalId 
					: externalOffice;
        xml.Append(externalOffice.IsNullOrEmpty()
                     ? StringToXml("office_id", job.ExternalOfficeId.IsNullOrEmpty() ? AOSOSSettings.DefaultOfficeId : job.ExternalOfficeId)
                     : StringToXml("office_id", externalOffice));

        xml.Append(job.HoursPerWeek.GetValueOrDefault() > 0
          ? IntToXml("hours_per_wk", Math.Min((int) job.HoursPerWeek.Value, 99))
          : StringToXml("hours_per_wk", string.Empty));

        externalAdminId = externalUsername.IsNotNullOrEmpty() && externalUsername.StartsWith("selfreg", StringComparison.OrdinalIgnoreCase) 
					? job.ExternalAdminId
					: externalAdminId;
				if (externalAdminId.IsNullOrEmpty() && externalOffice.IsNotNullOrEmpty() && _clientRepository.IsNotNull())
				{
					externalAdminId = _clientRepository.GetExternalAdminId(string.Format("selfreg{0}", externalOffice).ToLowerInvariant());
				}
        xml.Append(StringToXml("admin_id", externalAdminId.IsNotNullOrEmpty() ? externalAdminId : AOSOSSettings.DefaultAdminId));

      }

      return xml.ToString();
    }

    /// <summary>
    /// Adjust the salary so that is in the range allowed by EKOS
    /// </summary>
    /// <param name="salary">The salary to adjust</param>
    /// <param name="salaryUnit">The salary unit ("1" = hourly, "2" = daily, "3" = weekly, "4" = monthly, "5" = annual)</param>
    /// <returns>An adjust salary that is within the range</returns>
    /// <remarks>
    /// EKOS adjusts all salaries to an annual amount, which must be in the range 10192.00 to 350000.00
    /// Hourly amount adjustment: HOURLY RATE * 8 (hours per day) * 5 (working days per week) * 52 (working weeks)
    /// Daily amount adjustment: DAILY RATE * 5 * 52
    /// Weekly amount adjustment: WEEKLY RATE * 52
    /// Monthly amount adjustment: MONTHLY RATE * 12
    /// </remarks>
    private static decimal AdjustSalary(decimal salary, string salaryUnit)
    {
      var minSalary = 4.90M;
      var maxSalary = 168.26M;

      switch (salaryUnit)
      {
        case "2":
          minSalary = 39.20M;
          maxSalary = 1346.15M;
          break;
        case "3":
          minSalary = 196.00M;
          maxSalary = 6730.76M;
          break;
        case "4":
          minSalary = 849.34M;
          maxSalary = 29166.66M;
          break;
        case "5":
          minSalary = 10192.00M;
          maxSalary = 350000.00M;
          break;
      }

      if (salary < minSalary)
      {
        salary = minSalary;
      }
      else if (salary > maxSalary)
      {
        salary = maxSalary;
      }

      return salary;
    }

    private string MapEduLevel(EducationLevels? level)
    {
      if (level.IsNull()) return "1";
      if (((EducationLevels)level & EducationLevels.DoctorateDegree) == EducationLevels.DoctorateDegree) return "8";
      if (((EducationLevels)level & EducationLevels.GraduateDegree) == EducationLevels.GraduateDegree) return "7";
      if (((EducationLevels)level & EducationLevels.BachelorsDegree) == EducationLevels.BachelorsDegree) return "6";
      if (((EducationLevels)level & EducationLevels.AssociatesDegree) == EducationLevels.AssociatesDegree) return "5";
      if (((EducationLevels)level & EducationLevels.HighSchoolDiplomaOrEquivalent) == EducationLevels.HighSchoolDiplomaOrEquivalent) return "2";
      return "1";
    }

    #endregion
  }
}
