﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;

using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Services.Integration.Implementations.AOSOS.CommandBuilders
{
  public class StaffCommandBuilder : CommandBuilder
  {
    public StaffCommandBuilder(ClientSettings clientSettings, List<ExternalLookUpItemDto> externalLookUpItems) : base(clientSettings, externalLookUpItems)
    {
    }

    /// <summary>
    /// Gets the staff.
    /// </summary>
    /// <param name="userName">The username.</param>
    /// <param name="password">The password.</param>
    /// <returns></returns>
    public string AuthenticateStaffUser(string userName, string password)
    {
      return string.Format("<?xml version='1.0' ?><osos><loginRequest security_username='{0}' security_password='{1}' method='auth'/></osos>", userName, password);
    }

    public string GetStaffUser(string userName, string password, string officeId, string adminId)
    {
      var xml = string.Format("<administrators><administrator id='{0}' method='select' expand='*'/></administrators>", adminId);
      return GetBaseRequest(xml, userName, password, officeId, adminId);
    }
  }
}
