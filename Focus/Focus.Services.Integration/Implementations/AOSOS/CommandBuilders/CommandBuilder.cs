﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Integration;

using Framework.Core;

#endregion

namespace Focus.Services.Integration.Implementations.AOSOS.CommandBuilders
{
  public abstract class CommandBuilder
  {

    private string SecurityName { get { return AOSOSSettings.DefaultSecurityName; } }
    private string Password { get { return AOSOSSettings.DefaultPassword; } }
    public string DefaultOfficeId { get { return AOSOSSettings.DefaultOfficeId; } }
    public string DefaultAdminId { get { return AOSOSSettings.DefaultAdminId; } }
    public string CtAggrVersion { get { return AOSOSSettings.CtAggrVersion; } }
    public string OriginationMethod { get { return AOSOSSettings.OriginationMethod; } }
    public ClientSettings AOSOSSettings { get; set; }
    internal readonly List<ExternalLookUpItemDto> ExternalLookUpItems;

    protected CommandBuilder(ClientSettings clientSettings, List<ExternalLookUpItemDto> externalLookUpItems)
    {
      AOSOSSettings = clientSettings;
      ExternalLookUpItems = externalLookUpItems;
    }

    internal string GetBaseRequest(string request, string officeId = null)
    {
	    if (officeId.IsNullOrEmpty()) officeId = DefaultOfficeId;
      return GetBaseRequestIncludingOfficeIdAndAdminId(request, SecurityName, Password, officeId, DefaultAdminId);
    }

    /// <summary>
    /// Gets the base request.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <param name="userName">The user name</param>
    /// <param name="password">The password</param>
    /// <param name="officeId">The office identifier.</param>
    /// <param name="adminId">The admin identifier.</param>
    /// <returns></returns>
    internal string GetBaseRequest(string request, string userName, string password, string officeId = null, string adminId = null)
    {
      return GetBaseRequestIncludingOfficeIdAndAdminId(request, userName, password, officeId ?? DefaultOfficeId, adminId ?? DefaultAdminId);
    }

    internal string GetBaseRequestIncludingOfficeIdAndAdminId(string request, string userName, string password, string officeId, string adminId)
    {
	    if (userName.IsNullOrEmpty()) userName = SecurityName;
	    if (password.IsNullOrEmpty()) password = Password;
	    if (officeId.IsNullOrEmpty()) officeId = DefaultOfficeId;
	    if (adminId.IsNullOrEmpty()) adminId = DefaultAdminId;
      var containerRequest = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{5}'>{4}</request></osos>", userName, password, officeId, CtAggrVersion, request, adminId);// do we need : encoding='{0}'
      return containerRequest;
    }

    internal string ConvertToExternallId(ExternalLookUpType lookUpType, long internalValue)
    {
      return ConvertToExternallId(lookUpType, internalValue.ToString(CultureInfo.InvariantCulture));
    }

    internal string ConvertToExternallId(ExternalLookUpType lookUpType, int internalValue)
    {
      return ConvertToExternallId(lookUpType, internalValue.ToString(CultureInfo.InvariantCulture));
    }

    internal string ConvertToExternallId(ExternalLookUpType lookUpType, string internalValue)
    {
      var returnVal = ExternalLookUpItems.SingleOrDefault(x => x.InternalId == internalValue && x.ExternalLookUpType == lookUpType);

      if (returnVal.IsNotNull())
        return returnVal.ExternalId;

      return null;
    }

    internal string ConvertFirstToExternalId(ExternalLookUpType lookUpType, string internalValue)
    {
      var returnVal = ExternalLookUpItems.FirstOrDefault(x => x.InternalId == internalValue && x.ExternalLookUpType == lookUpType);
      if (returnVal.IsNotNull())
        return returnVal.ExternalId;

      return null;
    }

    internal List<ExternalLookUpItemDto> GetAllLookUpsInCollection(ExternalLookUpType lookUpType)
    {
      return ExternalLookUpItems.Where(x => x.ExternalLookUpType == lookUpType).ToList();
    }

    /// <summary>
    /// Gets the activity admin and office command. (Shared by the job and employer command builders)
    /// </summary>
    /// <param name="adminId">The admin identifier.</param>
    /// <param name="officeId">The office identifier.</param>
    /// <returns></returns>
    internal string GetActivityAdminAndOfficeCommand(string adminId, string officeId)
    {
      var command = string.Empty;
      // Logic copied from v1
      if (adminId.IsNotNullOrEmpty())
      {
        command += string.Format("<admin_id>{0}</admin_id>", adminId.HtmlEncode());
        if (officeId.IsNotNullOrEmpty())
          command += string.Format("<office_id>{0}</office_id>", officeId.HtmlEncode());
      }
      return command;

    }

    #region XML builders

    /// <summary>
    /// Activity to XML.
    /// </summary>
    /// <param name="activity">The activity.</param>
    /// <param name="entityName">Name of the entity.</param>
    /// <returns></returns>
    internal string ActivityToXml(ActivityModel activity, string entityName)
    {
      var xml = new StringBuilder();

      if (activity != null)
      {
        xml.Append(StringToXml("ctime", activity.ActivityDate.ToString("MMddyyyy")));
        xml.Append(StringToXml("admin_id", activity.ExternalAdminId));
        if (entityName == "employer")
        {
          xml.Append(StringToXml("contact_id", activity.ExternalEmployeeId));
          xml.Append(StringToXml("comments", activity.Comments));
        }
        xml.Append(StringToXml(entityName + "_service_type_cd", activity.ExternalActivityType));
      }
      return xml.ToString();
    }

    /// <summary>
    /// Address to XML.
    /// </summary>
    /// <param name="address">The address.</param>
    /// <param name="includeCounty">Whether to include the county element.</param>
    /// <param name="includeCity">Whether to include the city element.</param>
    /// <param name="includeZip">Whether to include the zip element.</param>
    /// <param name="includeAddressLines">Whether to "addr_1" and "addr_2" elements.</param>
    /// <param name="includeRootElement">Whether to include the root "address" element.</param>
    /// <returns></returns>
    internal string AddressToXml(AddressModel address, bool includeCounty = true, bool includeCity = true, bool includeZip = true, bool includeAddressLines = true, bool includeRootElement = true)
    {
      var xml = new StringBuilder();

      if (address != null)
      {
        address.ExternalCountryId = ConvertToExternallId(ExternalLookUpType.Country, address.CountryId.ToString(CultureInfo.InvariantCulture));

        if (includeRootElement)
          xml.Append("<address>");

        if (includeAddressLines)
        {
          xml.Append(StringToXml("addr_1", address.AddressLine1.Trim(79, false)));
          if (address.AddressLine2.IsNotNullOrEmpty())
            xml.Append(StringToXml("addr_2", address.AddressLine2.Trim(79, false)));
        }

        if (includeCity)
          xml.Append(StringToXml("city", address.City.Trim(40, false))); // Max 40 chars for city name

        xml.Append(StringToXml("state", address.ExternalCountryId == "US" 
                                          ? ConvertToExternallId(ExternalLookUpType.State, address.StateId.ToString(CultureInfo.InvariantCulture)) 
                                          : "ZZ")); // Set to ZZ if address not in the US
        
        if (address.ExternalCountryId == "US" && includeCounty)
          xml.Append(StringToXml("county", ConvertToExternallId(ExternalLookUpType.County, address.CountyId.ToString(CultureInfo.InvariantCulture))));
        
        if(address.ExternalCountryId.IsNotNullOrEmpty())
          xml.Append(StringToXml("country", address.ExternalCountryId));

	      if (includeZip)
	      {
		      string sanitizedZip = Regex.Replace(address.PostCode, "[^0-9]", string.Empty);
		      sanitizedZip = sanitizedZip.Substring(0, Math.Min(5, sanitizedZip.Length));
		      xml.Append(StringToXml("zip", sanitizedZip)); 
	      }

	      if (includeRootElement)
          xml.Append("</address>");
      }
      return xml.ToString();
    }

    internal string BenefitsToXml(string benefitId, string action, bool? insertAction)
    {
      return string.Format("<benefit method=\"{0}\" benefit_id=\"{1}\"><select_flag>{2}</select_flag></benefit>", action, benefitId, insertAction.IsNull() ? "" : (insertAction.Value ? "-1" : "0"));
    }

    /// <summary>
    /// Bool to XML.
    /// </summary>
    /// <param name="tagName">Name of the tag.</param>
    /// <param name="value">The value.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <param name="trueValue">The value to use as "true" (defaults to "-1").</param>
    /// <returns></returns>
    internal string BoolToXml(string tagName, bool? value, string defaultValue = "", string trueValue = "-1")
    {
      string mappedValue;

      if (value.HasValue)
        mappedValue = value.Value ? trueValue : "0"; // Use -1 for true in all boolean messages
      else
        mappedValue = defaultValue;

      return (mappedValue.IsNotNullOrEmpty() ? string.Format("<{0}>{1}</{0}>", tagName, mappedValue) : defaultValue);
    }

    /// <summary>
    /// DateTime to XML.
    /// </summary>
    /// <param name="tagName">Name of the tag.</param>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    internal string DateTimeToXml(string tagName, DateTime? value)
    {
      return DateTimeToXml(tagName, value, "");
    }

    /// <summary>
    /// DateTime to XML.
    /// </summary>
    /// <param name="tagName">Name of the tag.</param>
    /// <param name="value">The value.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <returns></returns>
    internal string DateTimeToXml(string tagName, DateTime? value, string defaultValue)
    {
      var mappedValue = value.HasValue ? value.Value.ToString("MMddyyyy") : defaultValue;

      return (mappedValue.IsNotNullOrEmpty() ? string.Format("<{0}>{1}</{0}>", tagName, mappedValue) : defaultValue);
    }

    /// <summary>
    /// Decimal to XML.
    /// </summary>
    /// <param name="tagName">Name of the tag.</param>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    internal string DecimalToXml(string tagName, decimal? value)
    {
      return DecimalToXml(tagName, value, "");
    }

    /// <summary>
    /// Decimal to XML.
    /// </summary>
    /// <param name="tagName">Name of the tag.</param>
    /// <param name="value">The value.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <returns></returns>
    internal string DecimalToXml(string tagName, decimal? value, string defaultValue)
    {
      var mappedValue = value.IsNotNull() ? value.ToString() : defaultValue;

      return (mappedValue.IsNotNullOrEmpty() ? string.Format("<{0}>{1}</{0}>", tagName.HtmlEncode(), mappedValue) : defaultValue);
    }

    /// <summary>
    /// Employer activity to XML.
    /// </summary>
    /// <param name="activity">The activity.</param>
    /// <returns></returns>
    internal string EmployerActivityToXml(ActivityModel activity)
    {
      return ActivityToXml(activity, "employer");
    }

    /// <summary>
    /// Int to XML.
    /// </summary>
    /// <param name="tagName">Name of the tag.</param>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    internal string IntToXml(string tagName, int? value)
    {
      return IntToXml(tagName, value, "");
    }

    /// <summary>
    /// Int to XML.
    /// </summary>
    /// <param name="tagName">Name of the tag.</param>
    /// <param name="value">The value.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <returns></returns>
    internal string IntToXml(string tagName, int? value, string defaultValue)
    {
      var mappedValue = value.IsNotNull() ? value.ToString() : defaultValue;

      return (mappedValue.IsNotNullOrEmpty() ? string.Format("<{0}>{1}</{0}>", tagName.HtmlEncode(), mappedValue) : defaultValue);
    }

    /// <summary>
    /// Converts the phone number to XML.
    /// </summary>
    /// <param name="tagName">Name of the tag.</param>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    internal string PhoneNumberToXml(string tagName, string value)
    {
      if (value.IsNull()) value = string.Empty;
      return StringToXml(tagName, value.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").HtmlEncode(), "");
    }

    /// <summary>
    /// String to XML.
    /// </summary>
    /// <param name="tagName">Name of the tag.</param>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    internal string StringToXml(string tagName, string value)
    {
      return StringToXml(tagName, value, "");
    }

    /// <summary>
    /// Strings to XML.
    /// </summary>
    /// <param name="tagName">Name of the tag.</param>
    /// <param name="value">The value.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <returns></returns>
    internal string StringToXml(string tagName, string value, string defaultValue)
    {
      var mappedValue = value ?? defaultValue;
      
      return string.Format("<{0}>{1}</{0}>", tagName, mappedValue.StripDisallowedCharacters().HtmlEncode());
    }

    #endregion
  }

	/// <summary>
	/// Extension methods.
	/// </summary>
	internal static class Extensions
	{
		/// <summary>
		/// Strips the disallowed characters.
		/// </summary>
		/// <param name="original">The original.</param>
		/// <returns>Replaces accented characters with their non-accented equivalents then removes all non-printable characters.</returns>
		public static string StripDisallowedCharacters(this string original)
		{
			original = original.ReplaceAccentedCharacters();
			return Regex.Replace(original, @"[^\u0020-\u007E\u00A3]", string.Empty);
		}
	}
}
