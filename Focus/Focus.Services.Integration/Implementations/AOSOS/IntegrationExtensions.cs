﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Focus.Core.Models.Career;
using Focus.Core.Models.Integration;

namespace Focus.Services.Integration.Implementations.AOSOS
{
  public static class IntegrationExtensions
  {
    public static AddressModel ToIntegrationAddress(this Address address, bool includeAddressLine1 = true, bool includeAddressLine2 = true, bool includeCity = true, bool includeCounty = true, bool includeZip = true)
    {
      var intAddress = new AddressModel
               {
                 AddressLine1 = includeAddressLine1 ? address.Street1 : null,
                 AddressLine2 = includeAddressLine2 ? address.Street2 : null,
                 City = includeCity ? address.City : null,
                 CountyId = includeCounty ? address.CountyId.GetValueOrDefault() : 0,
                 StateId = address.StateId.GetValueOrDefault(),
                 CountryId = address.CountryId.GetValueOrDefault(),
                 PostCode = includeZip ? address.Zip : null
               };
      if (string.IsNullOrEmpty(intAddress.AddressLine1)) intAddress.AddressLine1 = "Unknown";
      return intAddress;
    }
  }
}
