﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;
using Focus.Core.Models.Integration;
using Focus.Services.Integration.Helpers;
using Framework.Core;

#endregion

namespace Focus.Services.Integration.Implementations.AOSOS.ResponseTranslators
{
	public class Translator
	{
		private readonly List<ExternalLookUpItemDto> _externalLookUpItems;

		public Translator(List<ExternalLookUpItemDto> externalLookUpItems)
		{
			_externalLookUpItems = externalLookUpItems;
		}


		#region  General response mapping

		/// <summary>
		/// Processes the mappings.
		/// </summary>
		/// <param name="response">The response.</param>
		/// <returns></returns>
		public List<ResponseMapping> ProcessMappings(string response)
		{
			var responseMappingValues = new List<ResponseMapping>();
			XDocument responseXml;
			try
			{
				responseXml = XDocument.Parse(response);
			}
			catch (Exception)
			{
				return responseMappingValues;
			}

			responseMappingValues.AddRange(from mapping in responseXml.Descendants("mapping")
																		 let version = XmlHelpers.GetXmlAttributeStringValue(mapping, "version")
																		 select new ResponseMapping
																							{
																								Version = version,
																								ExternalId = XmlHelpers.GetXmlAttributeStringValue(mapping, "id"),
																								TrackingId = XmlHelpers.GetXmlAttributeStringValue(mapping, "uuid"),
																								AssociatedExternalID = XmlHelpers.GetXmlAttributeStringValue(mapping, "jd02_id")
																							});

			return responseMappingValues;
		}



		#endregion

		#region Employer/Employee translation

		public EmployeeModel XmlToEmployee(string employerXml, string emailAddress)
		{
			var employer = XmlToEmployer(employerXml);

			if (employer.IsNull() || employer.Employees.IsNullOrEmpty()) return null;

			return employer.Employees.SingleOrDefault(x => x.Email == emailAddress);
		}

		/// <summary>
		/// XML to employer, includes exployers employees
		/// </summary>
		/// <param name="employerXml">The employer XML.</param>
		/// <returns></returns>
		public EmployerModel XmlToEmployer(string employerXml)
		{
			if (employerXml.IsNotNullOrEmpty())
			{
				var responseXml = XDocument.Parse(employerXml);
				EmployerModel employer = null;

				foreach (var employerElement in responseXml.Descendants("employer"))
				{
					employer = new EmployerModel();
				  employer.IsActive = (XmlHelpers.GetXmlElementStringValue(employerElement, "employer_status_cd") == "1");
					employer.ExternalId = XmlHelpers.GetXmlAttributeStringValue(employerElement, "id");
					employer.Name = XmlHelpers.GetXmlElementStringValue(employerElement, "company");
					employer.LegalName = XmlHelpers.GetXmlElementStringValue(employerElement, "legal_name");

					// Contact details
					employer.Phone = XmlHelpers.GetXmlElementPhoneNumberValue(employerElement, "phone_pri");
					employer.PhoneExt = XmlHelpers.GetXmlElementStringValue(employerElement, "phone_pri_ext");
					employer.Fax = XmlHelpers.GetXmlElementPhoneNumberValue(employerElement, "fax_pri");
					employer.AltPhone = XmlHelpers.GetXmlElementPhoneNumberValue(employerElement, "phone_sec");
					employer.AltPhoneExt = XmlHelpers.GetXmlElementStringValue(employerElement, "phone_sec_ext");
					employer.Url = XmlHelpers.GetXmlElementStringValue(employerElement, "url_pri");

					employer.ExternalAdminId = XmlHelpers.GetXmlAttributeStringValue(employerElement, "admin_id");

					var address = new AddressModel();

					var addressElement = employerElement.Elements("address").SingleOrDefault();
					if (addressElement.IsNotNull())
					{
						address.AddressLine1 = XmlHelpers.GetXmlElementStringValue(addressElement, "addr_1");
						address.AddressLine2 = XmlHelpers.GetXmlElementStringValue(addressElement, "addr_2");

						address.AddressLine3 = XmlHelpers.GetXmlElementStringValue(addressElement, "addr_3"); // DB field available, but nothing in the front end!

						address.City = XmlHelpers.GetXmlElementStringValue(addressElement, "city");
						address.PostCode = XmlHelpers.GetXmlElementStringValue(addressElement, "zip");
						address.ExternalStateId = XmlHelpers.GetXmlElementStringValue(addressElement, "state");
						address.StateId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.State, XmlHelpers.GetXmlElementStringValue(addressElement, "state")));
						address.ExternalCountyId = XmlHelpers.GetXmlElementStringValue(addressElement, "county");
						address.CountyId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.County, XmlHelpers.GetXmlElementStringValue(addressElement, "county")));
						address.ExternalCountryId = XmlHelpers.GetXmlElementStringValue(addressElement, "country");
						address.CountryId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.Country, XmlHelpers.GetXmlElementStringValue(addressElement, "country")));

						employer.Address = address;

						employer.PublicTransportAccessible = XmlHelpers.GetXmlElementBoolValue(employerElement, "public_transport_flag");
					}



					//if (addresses.IsNotNull())
					//{
					//  var addressElement = addresses.Descendants("address").SingleOrDefault();
					//  if (addressElement.IsNotNull())
					//  {
					//    address.AddressLine1 = XmlHelpers.GetXmlElementStringValue(addressElement, "addr_1");
					//    address.AddressLine2 = XmlHelpers.GetXmlElementStringValue(addressElement, "addr_2");

					//    address.AddressLine3 = XmlHelpers.GetXmlElementStringValue(addressElement, "addr_3"); // DB field available, but nothing in the front end!

					//    address.City = XmlHelpers.GetXmlElementStringValue(addressElement, "city");
					//    address.PostCode = XmlHelpers.GetXmlElementStringValue(employerElement, "zip");
					//    address.ExternalStateId = XmlHelpers.GetXmlElementStringValue(addressElement, "state");
					//    address.ExternalCountyId = XmlHelpers.GetXmlElementStringValue(addressElement, "county");
					//    address.ExternalCountryId = XmlHelpers.GetXmlElementStringValue(addressElement, "country");

					//    employer.Address = address;

					//    employer.PublicTransportAccessible = XmlHelpers.GetXmlElementBoolValue(employerElement, "public_transport_flag");
					//  }
					//}

					foreach (var employeeElement in employerElement.Descendants(("contact")))
					{
						var employee = new EmployeeModel();

						employee.ExternalId = XmlHelpers.GetXmlAttributeStringValue(employeeElement, "id");
						employee.FirstName = XmlHelpers.GetXmlElementStringValue(employeeElement, "first_name");
						employee.LastName = XmlHelpers.GetXmlElementStringValue(employeeElement, "last_name");
						employee.JobTitle = XmlHelpers.GetXmlElementStringValue(employeeElement, "title");

						// Contact details
						employee.Email = XmlHelpers.GetXmlElementPhoneNumberValue(employeeElement, "email");
						employee.Phone = XmlHelpers.GetXmlElementPhoneNumberValue(employeeElement, "phone_pri");
						employee.PhoneExt = XmlHelpers.GetXmlElementStringValue(employeeElement, "phone_pri_ext");
						employee.Fax = XmlHelpers.GetXmlElementPhoneNumberValue(employeeElement, "fax_pri");
						employee.AltPhone = XmlHelpers.GetXmlElementPhoneNumberValue(employeeElement, "phone_sec");
						employee.AltPhoneExt = XmlHelpers.GetXmlElementStringValue(employeeElement, "phone_sec_ext");

						var employeeAddressElement = employeeElement.Descendants("address").SingleOrDefault();
						if (employeeAddressElement.IsNotNull())
							employee.Address = new AddressModel
							{
								AddressLine1 = XmlHelpers.GetXmlElementStringValue(employeeAddressElement, "addr_1"),
								AddressLine2 = XmlHelpers.GetXmlElementStringValue(employeeAddressElement, "addr_2"),
								AddressLine3 = XmlHelpers.GetXmlElementStringValue(employeeAddressElement, "addr_3"),
								City = XmlHelpers.GetXmlElementStringValue(employeeAddressElement, "city"),
                PostCode = XmlHelpers.GetXmlElementStringValue(employeeAddressElement, "zip"),
                ExternalStateId = XmlHelpers.GetXmlElementStringValue(employeeAddressElement, "state"),
                ExternalCountyId = XmlHelpers.GetXmlElementStringValue(employeeAddressElement, "county"),
                ExternalCountryId = XmlHelpers.GetXmlElementStringValue(employeeAddressElement, "country"),
                StateId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.State, XmlHelpers.GetXmlElementStringValue(employeeAddressElement, "state"))),
                CountyId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.County, XmlHelpers.GetXmlElementStringValue(employeeAddressElement, "county"))),
                CountryId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.Country, XmlHelpers.GetXmlElementStringValue(employeeAddressElement, "country")))
							};

						employer.Employees.Add(employee);
					}

					employer.FederalEmployerIdentificationNumber = XmlHelpers.GetXmlElementStringValue(employerElement, "fein");
					if (employer.FederalEmployerIdentificationNumber.IsNotNullOrEmpty() && employer.FederalEmployerIdentificationNumber.Length >= 2)
					{
						employer.FederalEmployerIdentificationNumber = employer.FederalEmployerIdentificationNumber.Insert(2, "-"); // Format FEIN
						employer.IsValidFederalEmployerIdentificationNumber = true;
					}

					employer.StateEmployerIdentificationNumber = XmlHelpers.GetXmlElementStringValue(employerElement, "sein");
					if (employer.StateEmployerIdentificationNumber.IsNotNullOrEmpty() && employer.StateEmployerIdentificationNumber.Length >= 8)
						employer.StateEmployerIdentificationNumber = employer.StateEmployerIdentificationNumber.Insert(8, "-"); // Format SEIN

					employer.Owner = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.EmployerOwner, XmlHelpers.GetXmlElementStringValue(employerElement, "owner")));
					employer.Naics = XmlHelpers.GetXmlElementStringValue(employerElement, "naics");
					employer.ExternalOfficeId = XmlHelpers.GetXmlElementStringValue(employerElement, "office_id");
					employer.BusinessDescription = XmlHelpers.GetXmlElementStringValue(employerElement, "business_desc");

					employer.Version = XmlHelpers.GetXmlAttributeStringValue(employerElement, "version");

					var employerStatus = XmlHelpers.GetXmlElementIntValue(employerElement, "employer_status_cd");

					if (employerStatus.IsNotNull())
					{
						switch (employerStatus)
						{
							case 1:
								employer.Status = EmployerStatusTypes.Active;
								break;

							case 3:
								employer.Status = EmployerStatusTypes.InActive;
								break;

							case 4:
								employer.Status = EmployerStatusTypes.Delete;
								break;

							case 5:
								employer.Status = EmployerStatusTypes.Archive;
								break;

							default:
								employer.Status = EmployerStatusTypes.NotSet;
								break;
						}
					}

				}
				return employer;
			}

			return null;
		}

		#endregion

		#region Job seeker translation

		/// <summary>
		/// Gets JobSeeker from user details XML.
		/// </summary>
		/// <param name="jobSeekerUserDetailsXml">The job seeker user details XML.</param>
		/// <returns></returns>
		public JobSeekerModel XmlToJobSeekerUser(string jobSeekerUserDetailsXml)
		{
			var responseXml = XDocument.Parse(jobSeekerUserDetailsXml);
			JobSeekerModel jobSeekerUser = null;

			foreach (var seekerElement in responseXml.Descendants("seeker"))
			{
				jobSeekerUser = new JobSeekerModel
				{
					ExternalId = XmlHelpers.GetXmlAttributeStringValue(seekerElement, "id"),
					Username = XmlHelpers.GetXmlElementStringValue(seekerElement, "username"),
					FirstName = XmlHelpers.GetXmlElementStringValue(seekerElement, "first_name"),
					LastName = XmlHelpers.GetXmlElementStringValue(seekerElement, "last_name"),
					SocialSecurityNumber = XmlHelpers.GetXmlElementStringValue(seekerElement, "ssn"),
					Password = XmlHelpers.GetXmlElementStringValue(seekerElement, "password")
				};
			}

			return jobSeekerUser;
		}

		/// <summary>
		/// Gets JobSeeker from XML.
		/// </summary>
		/// <param name="jobSeekerXml">The job seeker XML.</param>
		/// <returns></returns>
		public JobSeekerModel XmlToJobSeeker(string jobSeekerXml)
		{
			if (jobSeekerXml.IsNotNullOrEmpty())
			{
				var responseXml = XDocument.Parse(jobSeekerXml);
				JobSeekerModel jobSeeker = null;

				var seekerElement = responseXml.Descendants("seeker").FirstOrDefault();
				if (seekerElement.IsNotNull())
				{
					jobSeeker = XmlElementToJobSeeker(seekerElement, true);
				}

				return jobSeeker;
			}

			return null;
		}

		/// <summary>
		/// Converts an XML element to a job seeker model
		/// </summary>
		/// <param name="seekerElement">The Xml element</param>
		/// <param name="isFullXml">Whether the XML holds the full job seeker details</param>
		/// <returns>The job seeker model</returns>
		private JobSeekerModel XmlElementToJobSeeker(XElement seekerElement, bool isFullXml)
		{
			string ssn = XmlHelpers.GetXmlElementStringValue(seekerElement, "ssn");
			string fullSsn = XmlHelpers.GetXmlElementStringValue(seekerElement, "full_ssn");
			if (!string.IsNullOrWhiteSpace(fullSsn))
				ssn = fullSsn;
			var jobSeeker = new JobSeekerModel
			{
				ExternalId = XmlHelpers.GetXmlAttributeStringValue(seekerElement, "id"),
				Version = XmlHelpers.GetXmlAttributeStringValue(seekerElement, "version"),
				LastName = XmlHelpers.GetXmlElementStringValue(seekerElement, "last_name").Trim(),
				FirstName = XmlHelpers.GetXmlElementStringValue(seekerElement, "first_name").Trim(),
				MiddleInitial = XmlHelpers.GetXmlElementStringValue(seekerElement, "mi"),
				Password = XmlHelpers.GetXmlElementStringValue(seekerElement, "password"),
				UiClaimant = XmlHelpers.GetXmlElementStringValue(seekerElement, "ui_claimant_status").IsIn("2", "5"),
				OfficeId = XmlHelpers.GetXmlElementStringValue(seekerElement, "office_id"),
				AdminId = XmlHelpers.GetXmlElementStringValue(seekerElement, "admin_id"),
				Username = XmlHelpers.GetXmlElementStringValue(seekerElement, "username"),
				ExternalUsername = XmlHelpers.GetXmlElementStringValue(seekerElement, "username"),
				ExternalPassword = XmlHelpers.GetXmlElementStringValue(seekerElement, "password"),
				EmailAddress = XmlHelpers.GetXmlElementStringValue(seekerElement, "email"),
				SocialSecurityNumber = ssn,
				DateOfBirth = XmlHelpers.GetXmlElementNullableDateTimeValue(seekerElement, "dob"),
				PrimaryPhone = XmlHelpers.GetXmlElementStringValue(seekerElement, "phone_pri"),
				Resume = isFullXml ? XmlToResume(seekerElement) : null,
				JobSeekerFlag = XmlHelpers.GetXmlElementBoolValue(seekerElement, "job_seeker_flag"),
				SeekerStatusCd = XmlHelpers.GetXmlElementIntValue(seekerElement, "seeker_status_cd")
			};

			var addressElement = seekerElement.Element("address");
			if (addressElement.IsNull() && !isFullXml)
			{
				addressElement = seekerElement;
			}
			if (addressElement.IsNotNull())
			{
				jobSeeker.AddressLine1 = XmlHelpers.GetXmlElementStringValue(addressElement, "addr_1");
				jobSeeker.AddressTownCity = XmlHelpers.GetXmlElementStringValue(addressElement, "city");
				jobSeeker.AddressPostcodeZip = XmlHelpers.GetXmlElementStringValue(addressElement, "zip");
				jobSeeker.AddressCountyId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.County, XmlHelpers.GetXmlElementStringValue(addressElement, "county")));
				jobSeeker.AddressStateId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.State, XmlHelpers.GetXmlElementStringValue(addressElement, "state")));
				jobSeeker.AddressCountryId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.Country, XmlHelpers.GetXmlElementStringValue(addressElement, "country")));
			}

			return jobSeeker;
		}

		/// <summary>
		/// Gets JobSeekers from the XML.
		/// </summary>
		/// <param name="jobSeekerXml">The job seeker XML.</param>
		/// <returns>A list of job seeker models</returns>
		public List<JobSeekerModel> XmlToJobSeekers(string jobSeekerXml)
		{
			var jobSeekers = new List<JobSeekerModel>();

			if (jobSeekerXml.IsNotNullOrEmpty())
			{
				var responseXml = XDocument.Parse(jobSeekerXml);
				jobSeekers.AddRange(responseXml.Descendants("seeker").Select(js => XmlElementToJobSeeker(js, false)));
			}

			return jobSeekers;
		}

		/// <summary>
		/// Extracts the ResumeModel from the Job Seeker's XML
		/// </summary>
		/// <param name="seekerElement">The job seeker XML element</param>
		/// <returns>The ResumeModel</returns>
		private ResumeModel XmlToResume(XElement seekerElement)
		{
			// Get Employment History
			List<Job> jobs = null;
			DateTime? experienceStateDate = null;
			DateTime? experienceEndDate = null;

			var previousJobsElement = seekerElement.Descendants("previous_jobs").FirstOrDefault();
			if (previousJobsElement.IsNotNull())
			{
				var previousJobElements = previousJobsElement.Elements("previous_job").ToList();
				if (previousJobElements.Any())
				{
					jobs = new List<Job>();

					previousJobElements.ForEach(previousJobElement =>
					{
						var job = new Job
						{
							Employer = XmlHelpers.GetXmlElementStringValue(previousJobElement, "employer"),
							Title = XmlHelpers.GetXmlElementStringValue(previousJobElement, "title"),
							Description = XmlHelpers.GetXmlElementStringValue(previousJobElement, "duties"),
							Salary = XmlHelpers.GetXmlElementFloatValue(previousJobElement, "sal"),
							SalaryFrequencyId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.SalaryUnit, XmlHelpers.GetXmlElementStringValue(previousJobElement, "salary_unit_cd"))),
							ExternalId = XmlHelpers.GetXmlAttributeStringValue(previousJobElement, "id")
						};

						var jobAddressElement = previousJobElement.Element("address");
						if (jobAddressElement.IsNotNull())
						{
							job.JobAddress = new Address
							{
								Street1 = XmlHelpers.GetXmlElementStringValue(jobAddressElement, "addr_1"),
								Street2 = XmlHelpers.GetXmlElementStringValue(jobAddressElement, "addr_2"),
								City = XmlHelpers.GetXmlElementStringValue(jobAddressElement, "city"),
								CountyId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.County, XmlHelpers.GetXmlElementStringValue(jobAddressElement, "county"))),
								StateId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.State, XmlHelpers.GetXmlElementStringValue(jobAddressElement, "state"))),
								CountryId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.Country, XmlHelpers.GetXmlElementStringValue(jobAddressElement, "country"))),
								Zip = XmlHelpers.GetXmlElementStringValue(jobAddressElement, "zip")
							};
						}

						var dateRangeElement = previousJobElement.Element("daterange");
						if (dateRangeElement.IsNotNull())
						{
							job.JobStartDate = XmlHelpers.GetXmlElementNullableDateTimeValue(dateRangeElement, "start");
							job.JobEndDate = XmlHelpers.GetXmlElementNullableDateTimeValue(dateRangeElement, "end");
						}
						else
						{
							job.JobStartDate = XmlHelpers.GetXmlElementNullableDateTimeValue(previousJobElement, "start_date");
							job.JobEndDate = XmlHelpers.GetXmlElementNullableDateTimeValue(previousJobElement, "end_date");
						}

						if (job.JobStartDate.HasValue && (!experienceStateDate.HasValue || job.JobStartDate < experienceStateDate.Value))
							experienceStateDate = job.JobStartDate;

						if (job.JobEndDate.HasValue && (!experienceEndDate.HasValue || job.JobEndDate > experienceEndDate.Value))
							experienceEndDate = job.JobEndDate;

						jobs.Add(job);
					});
				}
			}

			// Get Experience
			var experience = new ExperienceInfo
			{
				Jobs = jobs,
				ExperienceStartDate = experienceStateDate,
				ExperienceEndDate = experienceEndDate,
				EmploymentStatus = ConvertToEnum<EmploymentStatus>(ExternalLookUpType.EmploymentStatus, XmlHelpers.GetXmlElementStringValue(seekerElement, "employment_status_cd"))
			};

			// Get Address Details
			Address address = null;

			var addressElement = seekerElement.Element("address");
			if (addressElement.IsNotNull())
			{
				address = new Address
				{
					Street1 = XmlHelpers.GetXmlElementStringValue(addressElement, "addr_1"),
					Street2 = XmlHelpers.GetXmlElementStringValue(addressElement, "addr_2"),
					City = XmlHelpers.GetXmlElementStringValue(addressElement, "city"),
					Zip = XmlHelpers.GetXmlElementStringValue(addressElement, "zip"),
					CountyId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.County, XmlHelpers.GetXmlElementStringValue(addressElement, "county"))),
					StateId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.State, XmlHelpers.GetXmlElementStringValue(addressElement, "state"))),
					CountryId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.Country, XmlHelpers.GetXmlElementStringValue(addressElement, "country")))
					//StateName = ddlState.SelectedItem.Text,
					//CountyName = App.Settings.CountyEnabled ? ddlCounty.SelectedItem.Text : string.Empty,
					//CountryName = ddlCountry.SelectedItem.Text,
				};
			}

			// Get Phone Numbers
			List<Phone> phoneNumbers = null;

			var primaryPhone = XmlHelpers.GetXmlElementStringValue(seekerElement, "phone_pri");
			var altPhone = XmlHelpers.GetXmlElementStringValue(seekerElement, "phone_sec");
			var fax = XmlHelpers.GetXmlElementStringValue(seekerElement, "fax");

			if (primaryPhone.IsNotNullOrEmpty() || altPhone.IsNotNullOrEmpty() || fax.IsNotNullOrEmpty())
			{
				phoneNumbers = new List<Phone>();

				if (primaryPhone.IsNotNullOrEmpty())
				{
					phoneNumbers.Add(new Phone
					{
						PhoneNumber = primaryPhone,
						PhoneType = PhoneType.Home
					});
				}

				if (altPhone.IsNotNullOrEmpty())
				{
					phoneNumbers.Add(new Phone
					{
						PhoneNumber = altPhone,
						PhoneType = PhoneType.Work
					});
				}

				if (fax.IsNotNullOrEmpty())
				{
					phoneNumbers.Add(new Phone
					{
						PhoneNumber = fax,
						PhoneType = PhoneType.Fax
					});
				}
			}

			// Get Contact Details
			var contact = new Contact
			{
				SeekerName = new Name
				{
					FirstName = XmlHelpers.GetXmlElementStringValue(seekerElement, "first_name"),
					MiddleName = XmlHelpers.GetXmlElementStringValue(seekerElement, "mi"),
					LastName = XmlHelpers.GetXmlElementStringValue(seekerElement, "last_name")
				},
				EmailAddress = XmlHelpers.GetXmlElementStringValue(seekerElement, "email"),
				PostalAddress = address,
				PhoneNumber = phoneNumbers
			};

			// Get Education Info
			var educationInfo = new EducationInfo
			{
				SchoolStatus = ConvertToEnum<SchoolStatus>(ExternalLookUpType.SchoolStatus, XmlHelpers.GetXmlElementStringValue(seekerElement, "school_status_cd")),
				EducationLevel = ConvertToFirstEnum<EducationLevel>(ExternalLookUpType.EducationLevel, XmlHelpers.GetXmlElementStringValue(seekerElement, "norm_edu_level_cd"))
			};

			var schoolsElement = seekerElement.Element("schools");
			if (schoolsElement.IsNotNull())
			{
				var schoolElements = schoolsElement.Elements("school").ToList();
				if (schoolElements.Any())
				{
					educationInfo.Schools = new List<School>();
					var schoolIndex = 0;
					schoolElements.ForEach(schoolElement =>
					{
						var date = XmlHelpers.GetXmlElementDateTimeValue(schoolElement, "completion_date");

						var school = new School
						{
							SchoolId = schoolIndex,
							CompletionDate = (date <= DateTime.Now.Date) ? date : (DateTime?)null,
							ExpectedCompletionDate = (date > DateTime.Now.Date) ? date : (DateTime?)null,
							Degree = XmlHelpers.GetXmlElementStringValue(schoolElement, "degree"),
							Institution = XmlHelpers.GetXmlElementStringValue(schoolElement, "school_name"),
							Major = XmlHelpers.GetXmlElementStringValue(schoolElement, "major"),
							Courses = null, //GetXmlElementStringValue(schoolElement, "course"),
							Honors = XmlHelpers.GetXmlElementStringValue(schoolElement, "honors"),
							GradePointAverage = null,
							Activities = null,
							SchoolAddress = new Address
							{
								City = XmlHelpers.GetXmlElementStringValue(schoolElement, "city"),
								StateId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.State, XmlHelpers.GetXmlElementStringValue(schoolElement, "state"))),
								CountryId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.Country, XmlHelpers.GetXmlElementStringValue(schoolElement, "country")))
								//StateName = string.IsNullOrEmpty(i.StateName) ? null : i.StateName,
								//CountryName = string.IsNullOrEmpty(i.CountryName) ? null : i.CountryName
							},
							ExternalId = XmlHelpers.GetXmlAttributeStringValue(schoolElement, "id")
						};
						educationInfo.Schools.Add(school);
						schoolIndex++;
					});
				}
			}

			// Get Driving Licence
			List<long> endorsements = null;
			var endorsementElements = seekerElement.Elements().Where(e => e.Name.LocalName.StartsWith("drv_") && e.Value.IsIn("1", "-1")).ToList();

			if (endorsementElements.Any())
			{
				endorsements = new List<long>();
				endorsementElements.ForEach(endorsementElement =>
				{
					var endorsementId = ConvertToInternallId(ExternalLookUpType.DrivingLicenceEndorsements, endorsementElement.Name.LocalName);
					if (endorsementId.IsNotNull())
						endorsements.Add(Convert.ToInt64(endorsementId));
				});
			}

			var license = new LicenseInfo
			{
				HasDriverLicense = XmlHelpers.GetXmlElementStringValue(seekerElement, "driver_flag").IsIn("1", "-1"),
				DriverStateId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.State, XmlHelpers.GetXmlElementStringValue(seekerElement, "driver_state"))),
				DrivingLicenceClassId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.DrivingLicenceClass, XmlHelpers.GetXmlElementStringValue(seekerElement, "driver_class"))),
				DrivingLicenceEndorsementIds = endorsements
				//DriverStateName = ddlDriverLicense.SelectedValue.AsBoolean() ? ddlState.SelectedItem.Text : null,
				//DriverClassText = ddlDriverLicense.SelectedValue.AsBoolean() ? ddlLicenseType.SelectedItem.Text : null
			};

			// Get ethnicity
			EthnicHeritage ethnicity = null;
			var ethnicHeritagesElement = seekerElement.Element("ethnic_heritages");
			if (ethnicHeritagesElement.IsNotNull())
			{
				var ethnicHeritageElements = ethnicHeritagesElement.Elements("ethnic_heritage").ToList();
				if (ethnicHeritageElements.IsNotNullOrEmpty())
				{
					var raceIds = ethnicHeritageElements
						.Select(ele => ConvertToInternallId(ExternalLookUpType.Race, XmlHelpers.GetXmlAttributeStringValue(ele, "ethnic_id")))
						.Where(ele => ele.IsNotNull())
						.ToList();
					ethnicity = new EthnicHeritage
					{
						RaceIds = raceIds.Select(r => (long?)Convert.ToInt64(r)).ToList()
					};
					var possibleEthnicHeritageEthnicityElement =
						ethnicHeritageElements.FirstOrDefault(
							ele => XmlHelpers.GetXmlAttributeStringValue(ele, "ethnic_id") == "3");
					if (possibleEthnicHeritageEthnicityElement.IsNotNull() && possibleEthnicHeritageEthnicityElement.Elements("selection_flag").Any())
					{
						long ethnicHeritageId = 0;
						if (long.TryParse(ConvertToInternallId(ExternalLookUpType.Ethnicity,
							XmlHelpers.GetXmlElementStringValue(possibleEthnicHeritageEthnicityElement, "selection_flag")),
							out ethnicHeritageId))
						{
							ethnicity.EthnicHeritageId = ethnicHeritageId;
						}
					}
				}
			}

			// Get Veteran Info
			VeteranInfo veteran = null;
			var veteranFlagElement = seekerElement.Element("vet_flag");
			if (veteranFlagElement.IsNotNull() && veteranFlagElement.Value.IsIn("1", "-1"))
			{
				long? branchOfServiceId = null;
				long tmp = 0;
				if (long.TryParse(
					ConvertToInternallId(ExternalLookUpType.MilitaryBranchOfService,
						XmlHelpers.GetXmlElementStringValue(seekerElement, "branch_cd")), out tmp))
					branchOfServiceId = tmp;
				veteran = new VeteranInfo
				{
					IsVeteran = true,
          IsHomeless = XmlHelpers.GetXmlElementBoolValue(seekerElement, "hous_sit_type_cd"),
          History = new List<VeteranHistory>
          {
            new VeteranHistory
            {
					    VeteranEra = ConvertToEnum<VeteranEra>(ExternalLookUpType.VeteranEra, XmlHelpers.GetXmlElementStringValue(seekerElement, "vet_era")),
					    VeteranDisabilityStatus = ConvertToEnum<VeteranDisabilityStatus>(ExternalLookUpType.VeteranDisabilityStatus, XmlHelpers.GetXmlElementStringValue(seekerElement, "vet_disability_status")),
					    TransitionType = ConvertToEnum<VeteranTransitionType>(ExternalLookUpType.VeteranTransitionType, XmlHelpers.GetXmlElementStringValue(seekerElement, "vet_tsm_type_cd")),
              VeteranStartDate = XmlHelpers.GetXmlElementDateTimeValue(seekerElement, "vet_start_date"),
              VeteranEndDate = XmlHelpers.GetXmlElementDateTimeValue(seekerElement, "vet_end_date"),
              IsCampaignVeteran = XmlHelpers.GetXmlElementBoolValue(seekerElement, "campaign_vet_flag"),
              MilitaryBranchOfServiceId = branchOfServiceId
            }
          }
				};
			}
			else if (veteranFlagElement.IsNotNull() && veteranFlagElement.Value == "0")
			{
				veteran = new VeteranInfo()
				{
					IsVeteran = false,
					IsHomeless = false
				};
			}

			// Get User Profile
			var userInfo = new UserProfile
			{
				DOB = XmlHelpers.GetXmlElementDateTimeValue(seekerElement, "dob"),
				Sex = ConvertToEnum<Genders>(ExternalLookUpType.Gender, XmlHelpers.GetXmlElementStringValue(seekerElement, "sex")),
				EthnicHeritage = ethnicity,
				DisabilityStatus = ConvertToEnum<DisabilityStatus>(ExternalLookUpType.DisabilityStatus, XmlHelpers.GetXmlElementStringValue(seekerElement, "disability_status")),
				DisabilityCategoryIds = null,
				License = license,
				Veteran = veteran,
				IsUSCitizen = XmlHelpers.GetXmlElementBoolValue(seekerElement, "citizen_flag"),
				AlienId = XmlHelpers.GetXmlElementStringValue(seekerElement, "alien_id"),
				IsPermanentResident = XmlHelpers.GetXmlElementBoolValue(seekerElement, "alien_perm_flag"),
				AlienExpires = XmlHelpers.GetXmlElementDateTimeValue(seekerElement, "AlienExpires"),
				UserGivenName = new Name()
				{
					FirstName = XmlHelpers.GetXmlElementStringValue(seekerElement, "first_name"),
					LastName = XmlHelpers.GetXmlElementStringValue(seekerElement, "last_name"),
					MiddleName = XmlHelpers.GetXmlElementStringValue(seekerElement, "mi")
				},
				EmailAddress = XmlHelpers.GetXmlElementStringValue(seekerElement, "email")
			};

			// Get Skills
			var skillInfo = new SkillInfo
			{
				Skills = XmlHelpers.GetXmlElementStringValue(seekerElement, "skills").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList()
			};

			var langsElement = seekerElement.Element("languages");
			if (langsElement.IsNotNull())
			{
				var langElements = langsElement.Elements("language").ToList();
				if (langElements.Any())
				{
					skillInfo.LanguageProficiencies = new List<LanguageProficiency>();

					langElements.ForEach(langElement =>
					{
						var language = new LanguageProficiency
						{
							Language = XmlHelpers.GetXmlElementStringValue(langElement, "language"),
							Proficiency = Convert.ToInt64(XmlHelpers.GetXmlAttributeStringValue(langElement, "proficiency"))
						};

						skillInfo.LanguageProficiencies.Add(language);
					});
				}
			}

			var certsElement = seekerElement.Element("certifications");
			if (certsElement.IsNotNull())
			{
				var certElements = certsElement.Elements("certification").ToList();
				if (certElements.Any())
				{
					skillInfo.Certifications = new List<Certification>();
					certElements.ForEach(certElement =>
					{
						// so far we only get country and state information from EKOS. Street1, street2, city, county and, zip? Who knows?
						var certificate = new Certification
						{
							Certificate = XmlHelpers.GetXmlElementStringValue(certElement, "certificate"),
							CertificationAddress = new Address
							{
								Street1 = XmlHelpers.GetXmlElementStringValue(certElement, "street1"),
								Street2 = XmlHelpers.GetXmlElementStringValue(certElement, "street2"),
								City = XmlHelpers.GetXmlElementStringValue(certElement, "city"),
								CountyId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.County, XmlHelpers.GetXmlElementStringValue(certElement, "county"))),
								StateId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.State, XmlHelpers.GetXmlElementStringValue(certElement, "state"))),
								CountryId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.Country, XmlHelpers.GetXmlElementStringValue(certElement, "country"))),
								Zip = XmlHelpers.GetXmlElementStringValue(certElement, "zip")
							},
							CompletionDate = XmlHelpers.GetXmlElementDateTimeValue(certElement, "completion_date"),
							OrganizationName = XmlHelpers.GetXmlElementStringValue(certElement, "organization_name"),
							ExternalId = XmlHelpers.GetXmlAttributeStringValue(certElement, "id")
						};

						skillInfo.Certifications.Add(certificate);
					});
				}
			}

			// Get states to search
			List<long> shiftTypeIds = null;
			var shiftTypeElements = seekerElement.Elements().Where(e => e.Name.LocalName.StartsWith("shift_") && e.Value.IsIn("1", "-1")).ToList();

			if (shiftTypeElements.Any())
			{
				shiftTypeIds = new List<long>();
				shiftTypeElements.ForEach(shiftElement =>
				{
					var shiftTypeId = ConvertToInternallId(ExternalLookUpType.ShiftType, shiftElement.Name.LocalName);
					if (shiftTypeId.IsNotNull())
						shiftTypeIds.Add(Convert.ToInt64(shiftTypeId));
				});
			}

			// Get zips to search
			List<DesiredZip> desiredZips = null;
			var desiredZipsElement = seekerElement.Element("desired_zips");
			if (desiredZipsElement.IsNotNull())
			{
				var desiredZipElements = desiredZipsElement.Elements("desired_zip").ToList();
				if (desiredZipElements.IsNotNullOrEmpty())
				{
					desiredZips = desiredZipElements.Select(desiredZipElement => new DesiredZip
					{
						Zip = desiredZipElement.Attribute("zip").Value,
						Radius = XmlHelpers.GetXmlElementIntValue(desiredZipElement, "radius"),
						ExternalId = desiredZipsElement.Attribute("id").Value
					}).ToList();
				}
			}

			// Get states to search
			List<DesiredMSA> desiredStates = null;
			var desiredStatesElement = seekerElement.Element("desired_states");
			if (desiredStatesElement.IsNotNull())
			{
				var desiredStateElements = desiredStatesElement.Elements("desired_state").ToList();
				if (desiredStateElements.IsNotNullOrEmpty())
					desiredStates = desiredStateElements.Select(desiredStateElement => new DesiredMSA
					{
						StateCode = XmlHelpers.GetXmlAttributeStringValue(desiredStateElement, "state")
					}).ToList();
			}

			List<DesiredTitle> desiredTitles = null;
			var desiredTitlesElement = seekerElement.Element("desired_titles");
			if (desiredTitlesElement.IsNotNull())
			{
				var desiredTitleElements = desiredTitlesElement.Elements("desired_title").ToList();
				if (desiredTitleElements.IsNotNullOrEmpty())
				{
					desiredTitles = desiredTitleElements.Select(element => new DesiredTitle
					{
						ExternalId = XmlHelpers.GetXmlAttributeStringValue(element, "id"),
						Onet = XmlHelpers.GetXmlElementStringValue(element, "onet3"),
            ExperienceInMonths = XmlHelpers.GetXmlElementIntValue(element, "experience")
					}).ToList();
				}
			}

			// Get Preferences
			var preferences = new Preferences
			{
				IsResumeSearchable = null,
				Salary = XmlHelpers.GetXmlElementFloatValue(seekerElement, "sal"),
				SalaryFrequencyId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.SalaryUnit, XmlHelpers.GetXmlElementStringValue(seekerElement, "salary_unit_cd"))),
				ShiftDetail = new Shift
				{
					WorkOverTime = null,
					WorkWeekId = Convert.ToInt64(ConvertToInternallId(ExternalLookUpType.WorkWeek, XmlHelpers.GetXmlElementStringValue(seekerElement, "work_week"))),
					WorkDurationId = Convert.ToInt64(ConvertToFirstInternalId(ExternalLookUpType.WorkType, XmlHelpers.GetXmlElementStringValue(seekerElement, "work_type"))),
					ShiftTypeIds = shiftTypeIds
				},
				WillingToRelocate = null,
				SearchInMyState = null,
				ZipPreference = desiredZips,
				MSAPreference = desiredStates,
				PostingsInterestedIn = JobTypes.All,
				TitlePreference = desiredTitles
			};

			// Get Additional Sections
			List<AdditionalResumeSection> sections = null;
			var objectiveElement = seekerElement.Element("objective");
			if (objectiveElement.IsNotNull())
			{
				sections = new List<AdditionalResumeSection>
        {
          new AdditionalResumeSection
          {
            SectionType = ResumeSectionType.Objective,
            SectionContent = objectiveElement.Value
          }
        };
			}

			if (contact.PhoneNumber.IsNull()) contact.PhoneNumber = new List<Phone>();
			if (userInfo.PrimaryPhone.IsNull() && contact.PhoneNumber.Any())
				userInfo.PrimaryPhone =
					contact.PhoneNumber.FirstOrDefault(
						phone => phone.PhoneType == PhoneType.Home && phone.PhoneNumber.IsNotNullOrEmpty()) ??
					contact.PhoneNumber.FirstOrDefault(
						phone => phone.PhoneType == PhoneType.Cell && phone.PhoneNumber.IsNotNullOrEmpty()) ??
					contact.PhoneNumber.FirstOrDefault(
						phone => phone.PhoneType == PhoneType.Work && phone.PhoneNumber.IsNotNullOrEmpty());

			// Build ResumeModel
			var resume = new ResumeModel
			{
				ResumeMetaInfo = new ResumeEnvelope
				{
					CompletionStatus = 0
				},
				ResumeContent = new ResumeBody
				{
					SeekerContactDetails = contact,
					EducationInfo = educationInfo,
					Profile = userInfo,
					Skills = skillInfo,
					ExperienceInfo = experience,
					AdditionalResumeSections = sections
				},
				Special = new ResumeSpecialInfo
				{
					Preferences = preferences,
				}
			};

			return resume;
		}

		#endregion

		#region Job translation

		/// <summary>
		/// XMLs to referral counts.
		/// </summary>
		/// <param name="jobXml">The job XML.</param>
		/// <returns></returns>
		public Tuple<int, int> XmlToReferralCounts(string jobXml)
		{
			var job = XDocument.Parse(jobXml);
			var referralInfo = job.Descendants("job").SingleOrDefault();
			var maxReferralCount = XmlHelpers.GetXmlElementIntValue(referralInfo, "referral_desired_ct");
			var totalReferralCount = XmlHelpers.GetXmlElementIntValue(referralInfo, "referral_provided_ct");

			return new Tuple<int, int>(totalReferralCount, maxReferralCount);
		}

		/// <summary>
		/// XMLs to job.
		/// </summary>
		/// <param name="jobXml">The job XML.</param>
		/// <returns></returns>
		public JobModel XmlToJob(string jobXml)
		{
			if (jobXml.IsNotNullOrEmpty())
			{
				var responseXml = XDocument.Parse(jobXml);
				JobModel job = null;

				foreach (var jobElement in responseXml.Descendants("job"))
				{
					job = new JobModel
					{
						Title = XmlHelpers.GetXmlElementStringValue(jobElement, "title"),
						MaxSalary = XmlHelpers.GetXmlElementDecimalValue(jobElement, "sal_max"),
						MinSalary = XmlHelpers.GetXmlElementDecimalValue(jobElement, "sal_min"),
						ExternalOfficeId = XmlHelpers.GetXmlElementStringValue(jobElement, "office_id")
					};

					var onet = XmlHelpers.GetXmlElementStringValue(jobElement, "onet3");
					if (onet.IsNotNullOrEmpty())
					{
						if (onet.Length > 2) onet = onet.Insert(2, "-");

						if (onet.Length > 7) onet = onet.Insert(7, ".");
						job.OnetCode = onet;
					}

					job.SalaryUnit = XmlHelpers.GetXmlElementStringValue(jobElement, "sal_unit");
					job.Shift = XmlHelpers.GetXmlElementStringValue(jobElement, "shift");
					job.ClosingDate = XmlHelpers.GetXmlElementDateTimeValue(jobElement, "last_open_date");
					// job.JobStatus = JobStatus.Active;
					// job.Benefits = new List<Benefit>();
					job.HoursPerWeek = XmlHelpers.GetXmlElementDecimalValue(jobElement, "hours_per_wk");
					EducationLevels edLevel;
					if (Enum.TryParse(ConvertToInternallId(ExternalLookUpType.EducationLevel, XmlHelpers.GetXmlElementStringValue(jobElement, "edu")), out edLevel))
						job.EducationLevel = edLevel;

					job.Description = XmlHelpers.GetXmlElementStringValue(jobElement, "description");
					//job.ContactPhone = true;
					//job.Category = "";

					var address = new AddressModel();
					foreach (var addressElement in jobElement.Descendants("address"))
					{
						address.AddressLine1 = XmlHelpers.GetXmlElementStringValue(addressElement, "addr_1");
						address.AddressLine2 = XmlHelpers.GetXmlElementStringValue(addressElement, "addr_2");
						//address.AddressLine3 = XmlHelpers.GetXmlElementStringValue(addressElement, "....").Value;
						address.City = XmlHelpers.GetXmlElementStringValue(addressElement, "city");
						address.PostCode = XmlHelpers.GetXmlElementStringValue(addressElement, "zip");
						address.ExternalStateId = XmlHelpers.GetXmlElementStringValue(addressElement, "state");
					}

					job.Version = XmlHelpers.GetXmlAttributeStringValue(jobElement, "version");

					var referralsElement = jobElement.Element("referrals");
					if (referralsElement.IsNotNull() && referralsElement.HasElements)
					{
						job.Referrals = new List<JobReferral>();

						foreach (var referralElement in referralsElement.Elements("referral"))
						{
							job.Referrals.Add(new JobReferral
							{
								JobSeekerExternalId = XmlHelpers.GetXmlElementStringValue(referralElement, "seeker_id"),
								ApplicationExternalId = XmlHelpers.GetXmlAttributeStringValue(referralElement, "id")
							});
						}
					}
				}
				return job;
			}

			return null;
		}

    /// <summary>
    /// Extracts a lookup of job seekers for a list of jobs
    /// </summary>
    /// <param name="jobsXml"></param>
    /// <returns></returns>
    public ILookup<string, string> XmlToJobMatches(string jobsXml)
    {
      var responseXml = XDocument.Parse(jobsXml);

      var matchesElements = responseXml.XPathSelectElements("osos/response/jobs/job/matches/match");
      return matchesElements.ToLookup(match => match.Element("job_id").Value, match => match.Element("seeker_id").Value);
    }

		#endregion

		#region Staff
		public StaffModel XmlToStaffModel(string staffXml)
		{
			StaffModel staff = null;
			if (staffXml.IsNotNullOrEmpty())
			{
				var responseXml = XDocument.Parse(staffXml);
				staff = new StaffModel();

				var staffElement = responseXml.Descendants("administrator").SingleOrDefault();
				staff.Version = XmlHelpers.GetXmlAttributeStringValue(staffElement, "version");
				staff.ExternalId = XmlHelpers.GetXmlAttributeStringValue(staffElement, "id");
				staff.FirstName = XmlHelpers.GetXmlElementStringValue(staffElement, "first_name");
				staff.Surname = XmlHelpers.GetXmlElementStringValue(staffElement, "last_name");
				staff.JobTitle = XmlHelpers.GetXmlElementStringValue(staffElement, "title");
				staff.PhoneNumber = XmlHelpers.GetXmlElementStringValue(staffElement, "phone_pri");
				staff.Email = XmlHelpers.GetXmlElementStringValue(staffElement, "email");
			}
			return staff;
		}

		/// <summary>
		/// XMLs to authentication model.
		/// </summary>
		/// <param name="authXml">The authentication XML.</param>
		/// <returns></returns>
		public AuthenticationModel XmlToAuthModel(string authXml)
		{
			AuthenticationModel auth = null;

			if (authXml.IsNotNullOrEmpty())
			{
				var responseXml = XDocument.Parse(authXml);
				var authElement = responseXml.Descendants("loginResponse").SingleOrDefault();
				auth = new AuthenticationModel { ExternalAdminId = XmlHelpers.GetXmlElementStringValue(authElement, "admin_id"), ExternalOfficeId = XmlHelpers.GetXmlElementStringValue(authElement, "office_id") };
			}

			return auth;
		}
		#endregion

		#region Helpers

		/// <summary>
		/// Finds the errors.
		/// </summary>
		/// <param name="response">The response.</param>
		/// <returns></returns>
		public string FindErrors(string response)
		{
			if (response.IsNullOrEmpty()) return "";
			var responseXml = XDocument.Parse(response);
			var errorList = responseXml.Descendants("error").Select(element => XmlHelpers.GetXmlElementStringValue(element, "message")).ToList();

			if (errorList.Count > 0)
			{
				var errors = new StringBuilder("AOSOS Error(s): ");
				foreach (var error in errorList)
				{
					errors.Append(error);
					errors.Append("\n");
				}
				return errors.ToString();
			}

			return "";
		}





		#endregion

		public string ConvertToInternallId(ExternalLookUpType lookUpType, string externalValue)
		{
			var returnVal = _externalLookUpItems.SingleOrDefault(x => x.ExternalId == externalValue && x.ExternalLookUpType == lookUpType);
			if (returnVal.IsNotNull())
				return returnVal.InternalId;

			return null;
		}

		public string ConvertToFirstInternalId(ExternalLookUpType lookUpType, string externalValue)
		{
			var returnVal = _externalLookUpItems.FirstOrDefault(x => x.ExternalId == externalValue && x.ExternalLookUpType == lookUpType);
			return returnVal.IsNotNull() ? returnVal.InternalId : null;
		}

		/// <summary>
		/// Converts an external value to an internal enumeration value
		/// </summary>
		/// <typeparam name="T">The type of enumeration required</typeparam>
		/// <param name="lookUpType">The lookup type</param>
		/// <param name="externalValue">The external value</param>
		/// <returns>The enumerated value</returns>
		private T? ConvertToEnum<T>(ExternalLookUpType lookUpType, string externalValue) where T : struct
		{
			// For enumerations, it is assume the lookup holds the enumeration text
			var internalId = ConvertToInternallId(lookUpType, externalValue);
			T result;
			if (Enum.TryParse(internalId, true, out result))
				return result;

			return null;
		}

    /// <summary>
    /// Converts an external value to an internal enumeration value
    /// </summary>
    /// <typeparam name="T">The type of enumeration required</typeparam>
    /// <param name="lookUpType">The lookup type</param>
    /// <param name="externalValue">The external value</param>
    /// <returns>The enumerated value</returns>
	  private T? ConvertToFirstEnum<T>(ExternalLookUpType lookUpType, string externalValue) where T : struct
	  {
	    var internalId = ConvertToFirstInternalId(lookUpType, externalValue);
	    T result;
	    if (Enum.TryParse(internalId, true, out result))
	      return result;

	    return null;
	  }
	}
}
