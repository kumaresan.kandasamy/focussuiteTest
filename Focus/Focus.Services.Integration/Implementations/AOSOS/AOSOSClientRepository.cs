﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Linq;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.IntegrationMessages;
using Focus.Core.Models.Integration;
using Focus.Services.Integration.Implementations.AOSOS.CommandBuilders;
using Focus.Services.Integration.Implementations.AOSOS.ResponseTranslators;

using Framework.Core;
using Framework.Exceptions;
using Newtonsoft.Json;

#endregion

namespace Focus.Services.Integration.Implementations.AOSOS
{
  public class AOSOSClientRepository : ClientRepository, IIntegrationRepository
  {
    private readonly ClientSettings _clientSettings;
    private readonly JobSeekerCommandBuilder _jobSeekerCommandBuilder;
    private readonly UserCommandBuilder _userCommandBuilder;
    private readonly JobCommandBuilder _jobCommandBuilder;
    private readonly EmployerCommandBuilder _employerCommandBuilder;
    private readonly StaffCommandBuilder _staffCommandBuilder;
    private readonly Translator _translator;

    #region Constructor

    /// <summary>
    /// Initializes a new instance of the <see cref="AOSOSClientRepository" /> class.
    /// </summary>
    /// <param name="settings">The settings.</param>
    /// <param name="externalLookUpItems">The external look up items.</param>
    /// <param name="includeSelectiveService">if set to <c>true</c> [include selective service].</param>
    public AOSOSClientRepository(string settings, List<ExternalLookUpItemDto> externalLookUpItems)
    {
      _clientSettings = (ClientSettings)JsonConvert.DeserializeObject(settings, typeof(ClientSettings));
      _jobSeekerCommandBuilder = new JobSeekerCommandBuilder(_clientSettings, externalLookUpItems);
      _userCommandBuilder = new UserCommandBuilder(_clientSettings, externalLookUpItems);
      _jobCommandBuilder = new JobCommandBuilder(_clientSettings, externalLookUpItems, this);
      _employerCommandBuilder = new EmployerCommandBuilder(_clientSettings, externalLookUpItems, this);
      _staffCommandBuilder = new StaffCommandBuilder(_clientSettings, externalLookUpItems);
      _translator = new Translator(externalLookUpItems);
    }

    #endregion

		protected override IntegrationPoint[] GetImplementedIntegrationPoints()
		{
			return new[]
			{
				IntegrationPoint.GetJobSeeker, IntegrationPoint.AuthenticateJobSeeker, IntegrationPoint.SaveJobSeeker,
			  IntegrationPoint.JobSeekerLogin, IntegrationPoint.AssignJobSeekerActivity, IntegrationPoint.SaveJobMatches, 
				IntegrationPoint.AssignAdminToJobSeeker, IntegrationPoint.IsUIClaimant, IntegrationPoint.ChangePassword,
				IntegrationPoint.SaveJob, IntegrationPoint.GetJob, IntegrationPoint.ReferJobSeeker, 
				IntegrationPoint.SetApplicationStatus, IntegrationPoint.GetEmployer, IntegrationPoint.SaveEmployer, 
				IntegrationPoint.AssignAdminToEmployer, IntegrationPoint.AssignAdminToJobOrder, IntegrationPoint.AssignEmployerActivity,
 				IntegrationPoint.SaveEmployee, IntegrationPoint.GetStaff
			};
		}

		#region Implementation of IClientProvider

		public LogActionResponse LogAction(LogActionRequest request)
    {
      return new LogActionResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    public UpdateEmployeeStatusResponse UpdateEmployeeStatus(UpdateEmployeeStatusRequest request)
    {
      return new UpdateEmployeeStatusResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    /// <summary>
    /// Gets the job seeker buy either id, SSN or user name
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns>The job seeker details</returns>
    public GetJobSeekerResponse GetJobSeeker(GetJobSeekerRequest request)
    {
      var response = new GetJobSeekerResponse(request);
      try
      {
        if (request.JobSeeker.ExternalId.IsNotNullOrEmpty())
          response.JobSeeker = GetJobSeekerById(request.JobSeeker.ExternalId.ToUpper(), response.IntegrationBreadcrumbs);
        else if (request.JobSeeker.SocialSecurityNumber.IsNotNullOrEmpty())
          response.JobSeeker = GetJobSeekerBySocialSecurityNumber(request.JobSeeker.SocialSecurityNumber,
            response.IntegrationBreadcrumbs);
        else
          response.JobSeeker = GetJobSeekerByUsername(request.JobSeeker.Username, response.IntegrationBreadcrumbs);
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public GetJobSeekerResponse GetJobSeekerBySocialSecurityNumber(GetJobSeekerRequest request)
    {
      var response = new GetJobSeekerResponse(request);
      if (request.JobSeeker.IsNull() || request.JobSeeker.SocialSecurityNumber.IsNullOrEmpty())
      {
        response.Outcome = IntegrationOutcome.Failure;
        return response;
      }

      try
      {
        response.JobSeeker = GetJobSeekerBySocialSecurityNumber(request.JobSeeker.SocialSecurityNumber, response.IntegrationBreadcrumbs);
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    /// <summary>
    /// Authenticates the job seeker.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    public virtual AuthenticateJobSeekerResponse AuthenticateJobSeeker(AuthenticateJobSeekerRequest request)
    {
      var response = new AuthenticateJobSeekerResponse(request);
      try
      {
        // First see if there is a job seeker with this username (AOSOS is CASE SENSITIVE on usernames)
        var seeker = GetJobSeekerByUsername(request.UserName, response.IntegrationBreadcrumbs);
        // Nothing found? Tell the authentication service so
        if (seeker.IsNull())
        {
          response.AuthenticationResult = JobSeekerAuthenticationResult.UserNotFound;
          response.Outcome = IntegrationOutcome.Failure;
          return response;
        }

        // If passwords don't match then tell the authentication service
        if (seeker.Password != request.Password)
        {
          response.AuthenticationResult = JobSeekerAuthenticationResult.PasswordMismatch;
          response.Outcome = IntegrationOutcome.Failure;
          return response;
        }

        // Return job seeker
        response.JobSeeker = seeker;
        response.Outcome = IntegrationOutcome.Success;
        response.AuthenticationResult = JobSeekerAuthenticationResult.UserFound;
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public virtual SaveJobSeekerResponse SaveJobSeeker(SaveJobSeekerRequest request)
    {
      var response = new SaveJobSeekerResponse(request);
      try
      {
        var version = string.Empty;
        JobSeekerModel ekosJobSeeker = null;

	      if (request.JobSeeker.ExternalId.IsNull())
	      {
		      string externalId = null;
		      if (request.JobSeeker.SocialSecurityNumber.IsNotNullOrEmpty())
		      {
			      externalId = GetJobSeekerId(request.JobSeeker.SocialSecurityNumber, response.IntegrationBreadcrumbs);
		      }
					else if (request.JobSeeker.DateOfBirth.HasValue)
		      {
						externalId = GetJobSeekerId(request.JobSeeker.FirstName, request.JobSeeker.LastName, request.JobSeeker.DateOfBirth.Value, request.JobSeeker.AddressPostcodeZip, response.IntegrationBreadcrumbs);
					}
					
					if (externalId.IsNotNullOrEmpty())
					{
						request.JobSeeker.ExternalId = externalId;
					}
				}

				if (request.JobSeeker.ExternalId.IsNotNull())
        {
          ekosJobSeeker = GetJobSeekerById(request.JobSeeker.ExternalId, response.IntegrationBreadcrumbs);
          version = ekosJobSeeker.Version;
        }

        RequestResponse requestResponse = null;
        string errors = null;
        if (ekosJobSeeker.IsNotNull() && !ekosJobSeeker.JobSeekerFlag)
        {
          requestResponse =
            SendWebRequest(_jobSeekerCommandBuilder.SetJobSeekerStatus(ekosJobSeeker, true, version, request.ExternalUsername,
              request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId));
          response.IntegrationBreadcrumbs.Add(requestResponse);
          errors = _translator.FindErrors(requestResponse.IncomingResponse);
          if (errors.IsNotNullOrEmpty())
            throw new Exception(errors);
          ekosJobSeeker = GetJobSeekerById(request.JobSeeker.ExternalId, response.IntegrationBreadcrumbs);
          version = ekosJobSeeker.Version;
        }

        string requestText;
        if (request.JobSeeker.ExternalId.IsNull() ||
                         (request.JobSeeker.ExternalId.IsNotNull() && version.IsNullOrEmpty()))
        {
					if (request.JobSeeker.Resume.IsNull() || request.JobSeeker.DefaultResumeCompletionStatus != ResumeCompletionStatuses.Completed)
					{
						response.Outcome = IntegrationOutcome.Ignored;
						return response;
					}

          requestText = _jobSeekerCommandBuilder.AddJobSeeker(request.JobSeeker, request.IncludeSelectiveService,
            request.ExternalUsername, request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId);
        }
        else
        {
          requestText = _jobSeekerCommandBuilder.SaveJobSeeker(request.JobSeeker, "<<<VERSION>>>", request.IncludeSelectiveService, ekosJobSeeker,
              request.ExternalUsername, request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId);

					version = GetJobSeekerVersion(request.JobSeeker.ExternalId, response.IntegrationBreadcrumbs);
					requestText = requestText.Replace("<<<VERSION>>>", version);
        }

				requestResponse = SendWebRequest(requestText);
        response.IntegrationBreadcrumbs.Add(requestResponse);
        errors = _translator.FindErrors(requestResponse.IncomingResponse);
        if (errors.IsNotNullOrEmpty())
          throw new Exception(errors);

        var results = _translator.ProcessMappings(requestResponse.IncomingResponse);
        response.ExternalJobSeekerId = results[0].ExternalId;
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    /// <summary>
    /// Sets the job seeker status
    /// </summary>
    /// <param name="request">The request containing the job seeker details</param>
    /// <returns></returns>
    public JobSeekerLoginResponse JobSeekerLogin(JobSeekerLoginRequest request)
    {
      var response = new JobSeekerLoginResponse(request);
      try
      {
        if (request.JobSeeker.ExternalId.IsNotNull())
        {
					var version = GetJobSeekerVersion(request.JobSeeker.ExternalId, response.IntegrationBreadcrumbs);
          var requestResponse =
            SendWebRequest(_jobSeekerCommandBuilder.SetJobSeekerStatus(request.JobSeeker, request.Active, version, request.ExternalUsername, request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId));
          response.IntegrationBreadcrumbs.Add(requestResponse);
          var errors = _translator.FindErrors(requestResponse.IncomingResponse);
          if (errors.IsNotNullOrEmpty()) throw new Exception(errors);
        }
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public virtual SaveJobMatchesResponse SaveJobMatches(SaveJobMatchesRequest request)
    {
      var response = new SaveJobMatchesResponse(request);
      try
      {
        var currentMatches = GetJobMatchesForJobSeeker(request.ExternalJobSeekerId, request.Jobs, request.ExternalUsername, request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId, response.IntegrationBreadcrumbs);
        var newJobs = request.Jobs.Where(j => currentMatches.All(cm => cm != j)).ToList();
        if (newJobs.Any())
        {
          var requestResponse = SendWebRequest(_jobSeekerCommandBuilder.SaveJobMatches(request.ExternalJobSeekerId, newJobs, request.ExternalUsername, request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId));
          response.IntegrationBreadcrumbs.Add(requestResponse);
          var errors = _translator.FindErrors(requestResponse.IncomingResponse);
          if (errors.IsNotNullOrEmpty())
            throw new Exception(errors);
        }
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public virtual AssignJobSeekerActivityResponse AssignJobSeekerActivity(AssignJobSeekerActivityRequest request)
    {
      var response = new AssignJobSeekerActivityResponse(request);
      try
      {
        RequestResponse requestResponse;
        string errors;
        var version = GetJobSeekerVersion(request.JobSeekerId, response.IntegrationBreadcrumbs);
        if (request.SelfServicedActivity)
        {
          // ensure that for self serviced activities that the job seeker is active and actively seeking for jobs
          JobSeekerModel jobSeekerModel = GetJobSeekerById(request.JobSeekerId, response.IntegrationBreadcrumbs);
          if (!jobSeekerModel.JobSeekerFlag || jobSeekerModel.SeekerStatusCd != 1)
          {
            requestResponse =
              SendWebRequest(_jobSeekerCommandBuilder.SetJobSeekerStatus(jobSeekerModel, true, version,
                request.ExternalUsername, request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId));
            errors = _translator.FindErrors(requestResponse.IncomingResponse);
            if (errors.IsNotNullOrEmpty())
              throw new Exception(errors);
            // don't forget to get the new version number after updating the job seeker.
            version = GetJobSeekerVersion(request.JobSeekerId, response.IntegrationBreadcrumbs);
          }
        }

				request.AdminId = request.ExternalAdminId;
				request.OfficeId = request.ExternalOfficeId;

        requestResponse =
          SendWebRequest(_jobSeekerCommandBuilder.SaveActivity(request.JobSeekerId, version, request.ActivityId,
            request.AdminId, request.OfficeId, request.ExternalUsername, request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId, requestDate: request.RequestMade));
        response.IntegrationBreadcrumbs.Add(requestResponse);
        errors = _translator.FindErrors(requestResponse.IncomingResponse);
        if (errors.IsNotNullOrEmpty())
          throw new Exception(errors);
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public virtual AssignAdminToJobSeekerResponse AssignAdminToJobSeeker(AssignAdminToJobSeekerRequest request)
    {
      var response = new AssignAdminToJobSeekerResponse(request);
      try
      {
        var version = GetJobSeekerVersion(request.JobSeekerId, response.IntegrationBreadcrumbs);
        var requestResponse =
          SendWebRequest(_jobSeekerCommandBuilder.AssignAdmin(request.JobSeekerId, request.AdminId, version, request.ExternalUsername, request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId));
        response.IntegrationBreadcrumbs.Add(requestResponse);
        var errors = _translator.FindErrors(requestResponse.IncomingResponse);
        if (errors.IsNotNullOrEmpty())
          throw new Exception(errors);
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public virtual IsUIClaimantResponse IsUIClaimant(IsUIClaimantRequest request)
    {
      var response = new IsUIClaimantResponse(request);
      try
      {
        JobSeekerModel jobSeeker;
        if (request.ExternalJobSeekerId.IsNotNullOrEmpty())
          jobSeeker = GetJobSeekerById(request.ExternalJobSeekerId, response.IntegrationBreadcrumbs);
        else if (request.SocialSecurityNumber.IsNotNullOrEmpty())
          jobSeeker = GetJobSeekerBySocialSecurityNumber(request.SocialSecurityNumber, response.IntegrationBreadcrumbs);
        else
          throw new Exception("Only External ID or SSN is supported");

        response.IsUIClaimant = jobSeeker.UiClaimant;
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

		/// <summary>
		/// Gets the job version.
		/// </summary>
		/// <param name="jobId">The job identifier.</param>
		/// <param name="breadcrumbs">The breadcrumbs.</param>
		/// <returns>The job version</returns>
		protected virtual string GetJobVersion(string jobId, List<RequestResponse> breadcrumbs)
		{
			if (jobId.IsNullOrEmpty())
				throw new Exception("GetJobVersion requires a jobId.");

			var requestResponse = SendWebRequest(_jobCommandBuilder.GetJobById(jobId));
			breadcrumbs.Add(requestResponse);

			var errors = _translator.FindErrors(requestResponse.IncomingResponse);
			if (errors.IsNotNullOrEmpty()) 
				throw new Exception(errors);

			var job = _translator.XmlToJob(requestResponse.IncomingResponse);
			if (job.IsNull())
				throw new Exception("GetJobVersion response Xml could not be transformed to a Job.");

			return job.Version;
		}

    public ChangeUserPasswordResponse ChangeUserPassword(ChangeUserPasswordRequest request)
    {
      var response = new ChangeUserPasswordResponse(request);
      try
      {
        var requestResponse =
          SendWebRequest(_userCommandBuilder.ChangeUserPassword(request.UserName, request.OldPassword, request.NewPassword));
        response.IntegrationBreadcrumbs.Add(requestResponse);
        var errors = _translator.FindErrors(requestResponse.IncomingResponse);
        if (errors.IsNotNullOrEmpty())
          throw new Exception(errors);
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public virtual SaveJobResponse SaveJob(SaveJobRequest request)
    {
      var response = new SaveJobResponse(request);
      
      try
      {
        var version = string.Empty;
        JobModel ekosJob = null;

        if (request.Job.ExternalId.IsNotNullOrEmpty())
        {
          version = GetJobVersion(request.Job.ExternalId, response.IntegrationBreadcrumbs);
          ekosJob = GetJobById(request.Job.ExternalId, response.IntegrationBreadcrumbs);
        }

        var requestResponse = SendWebRequest(_jobCommandBuilder.SaveJob(request.Job, ekosJob, version, request.ExternalUsername, request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId));
        response.IntegrationBreadcrumbs.Add(requestResponse);
        var errors = _translator.FindErrors(requestResponse.IncomingResponse);
        if (errors.IsNotNullOrEmpty())
          throw new Exception(errors);

        var results = _translator.ProcessMappings(requestResponse.IncomingResponse);
        response.ExternalJobId = results[0].ExternalId;
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public GetJobResponse GetJob(GetJobRequest request)
    {
      var response = new GetJobResponse(request);
      try
      {
        response.Job = GetJobById(request.JobId, response.IntegrationBreadcrumbs);
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public virtual ReferJobSeekerResponse ReferJobSeeker(ReferJobSeekerRequest request)
    {
      var response = new ReferJobSeekerResponse(request);
      try
      {
        if (request.JobId == 0 && request.ExternalJobId.IsNullOrEmpty())
          return response;
        if (request.JobSeekerId.IsNullOrEmpty())
          return response;
        var referralCount = GetJobReferralInfo(request.ExternalJobId, response.IntegrationBreadcrumbs);
				if ((referralCount.Item2 > 0 && referralCount.Item1 >= referralCount.Item2) || referralCount.Item1 >= 200)
        {
          response.Outcome = IntegrationOutcome.Success;
          response.ReferralLimitExceeded = true;
          return response;
        }

        ReferSeeker(request.ExternalJobId, request.JobSeekerId, response.IntegrationBreadcrumbs, request.ExternalUsername, request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId);
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    /// <summary>
    /// Sets the application status
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    public virtual SetApplicationStatusResponse SetApplicationStatus(SetApplicationStatusRequest request)
    {
      var response = new SetApplicationStatusResponse(request);
      try
      {
        var job = GetJobById(request.JobId, response.IntegrationBreadcrumbs);
        var jobSeeker = GetJobSeekerById(request.JobSeekerId, response.IntegrationBreadcrumbs);

				var status = _jobCommandBuilder.ConvertToExternallId(ExternalLookUpType.ApplicationStatus,
          request.ApplicationStatus.ToString());
				if (status.IsNullOrEmpty())
				{
					response.Outcome = IntegrationOutcome.Ignored;
					return response;
				}

        var applicationId = job.Referrals.IsNotNullOrEmpty()
          ? job.Referrals.Where(r => r.JobSeekerExternalId == jobSeeker.ExternalId)
            .Select(r => r.ApplicationExternalId)
            .FirstOrDefault()
          : String.Empty;
        var errors = string.Empty;
        string version = jobSeeker.Version;
        RequestResponse requestResponse;
        if (!jobSeeker.JobSeekerFlag || jobSeeker.SeekerStatusCd != 1)
        {
          requestResponse =
            SendWebRequest(_jobSeekerCommandBuilder.SetJobSeekerStatus(jobSeeker, true, version,
              request.ExternalUsername, request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId));
          errors = _translator.FindErrors(requestResponse.IncomingResponse);
          if (errors.IsNotNullOrEmpty())
            throw new Exception(errors);
          // don't forget to get the new version number after updating the job seeker.
          version = GetJobSeekerVersion(request.JobSeekerId, response.IntegrationBreadcrumbs);
        }

        requestResponse =
          SendWebRequest(applicationId.IsNotNullOrEmpty() ? _jobCommandBuilder.SetApplicationStatus(request.JobSeekerId, version, request.JobId,
          job.Version, applicationId, status, request.ExternalUsername, request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId) :
          _jobCommandBuilder.ReferJobSeeker(request.JobId, request.JobSeekerId, request.ExternalUsername, request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId));
        response.IntegrationBreadcrumbs.Add(requestResponse);
        errors = _translator.FindErrors(requestResponse.IncomingResponse);
        if (errors.IsNotNullOrEmpty())
          throw new Exception(errors);
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public virtual GetEmployerResponse GetEmployer(GetEmployerRequest request)
    {
      var response = new GetEmployerResponse(request);
      try
      {
        var command = string.Empty;
        if (request.EmployerId.IsNotNullOrEmpty())
          command = _employerCommandBuilder.GetEmployerById(request.EmployerId);
        else if (request.FEIN.IsNotNullOrEmpty())
          command = _employerCommandBuilder.GetEmployerByFein(request.FEIN);
        else if (request.CompanyName.IsNotNullOrEmpty())
          command = _employerCommandBuilder.GetEmployerByCompanyName(request.CompanyName);
        else if (request.LegalName.IsNotNullOrEmpty())
          command = _employerCommandBuilder.GetEmployerByLegalName(request.LegalName);

        var requestResponse = SendWebRequest(command);
        response.IntegrationBreadcrumbs.Add(requestResponse);
        var errors = _translator.FindErrors(requestResponse.IncomingResponse);
        if (errors.IsNotNullOrEmpty())
          throw new Exception(errors);

        response.Employer = _translator.XmlToEmployer(requestResponse.IncomingResponse);
        response.Version = response.Employer.IsNotNull() ? response.Employer.Version : null;
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public virtual SaveEmployerResponse SaveEmployer(SaveEmployerRequest request)
    {
      var response = new SaveEmployerResponse(request);
      try
      {
        var version = request.Employer.ExternalId.IsNotNullOrEmpty()
          ? GetEmployerVersion(request.Employer.ExternalId, response.IntegrationBreadcrumbs)
          : string.Empty;
        if (version.IsNullOrEmpty())
        {
          // Check to see if it exists by FEIN
          var employer = GetEmployerByFein(request.Employer.FederalEmployerIdentificationNumber,
            response.IntegrationBreadcrumbs);
          if (employer.IsNotNull())
          {
            request.Employer.ExternalId = employer.ExternalId;
            version = request.Employer.Version = employer.Version;
          }
        }

        var requestResponse = SendWebRequest(_employerCommandBuilder.SaveEmployer(request.Employer, version, request.ExternalUsername, request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId));
        response.IntegrationBreadcrumbs.Add(requestResponse);
        var errors = _translator.FindErrors(requestResponse.IncomingResponse);
        if (errors.IsNotNullOrEmpty())
          throw new Exception(errors);

        var results = _translator.ProcessMappings(requestResponse.IncomingResponse);
        var mapping = results.FirstOrDefault(x => x.TrackingId == request.Employer.TrackingId);
        if (mapping.IsNotNull())
          response.ExternalEmployerId = mapping.ExternalId;

        response.Employees = new List<Tuple<long, string>>();
        foreach (var employee in request.Employer.Employees.Where(x => x.ExternalId.IsNullOrEmpty()))
        {
          var employeeMapping = results.FirstOrDefault(x => x.TrackingId == employee.TrackingId);
          if (employeeMapping.IsNotNull())
            response.Employees.Add(new Tuple<long, string>(employee.Id, employeeMapping.ExternalId));
        }
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    /// <summary>
    /// Send admin assignment data to AOSOS
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    /// <exception cref="System.Exception"></exception>
    public virtual AssignAdminToEmployerResponse AssignAdminToEmployer(AssignAdminToEmployerRequest request)
    {
      var response = new AssignAdminToEmployerResponse(request);
      try
      {
        var version = GetEmployerVersion(request.EmployerId, response.IntegrationBreadcrumbs);
        var requestResponse =
          SendWebRequest(_employerCommandBuilder.AssignAdminToEmployer(request.EmployerId, request.AdminId, version, request.ExternalUsername, request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId));
        response.IntegrationBreadcrumbs.Add(requestResponse);
        var errors = _translator.FindErrors(requestResponse.IncomingResponse);
        if (errors.IsNotNullOrEmpty())
          throw new Exception(errors);
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

		/// <summary>
		/// Send admin assignment data to AOSOS
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception"></exception>
		public virtual AssignAdminToJobOrderResponse AssignAdminToJobOrder(AssignAdminToJobOrderRequest request)
		{
			var response = new AssignAdminToJobOrderResponse(request);
			try
			{
				var version = string.Empty;
				if (request.JobExternalId.IsNotNullOrEmpty())
					version = GetJobById(request.JobExternalId, response.IntegrationBreadcrumbs).Version;

				var requestResponse =
					SendWebRequest(_jobCommandBuilder.AssignAdminToJobOrder(request.JobExternalId, request.AdminId, version, request.ExternalUsername, request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId));
				response.IntegrationBreadcrumbs.Add(requestResponse);
				var errors = _translator.FindErrors(requestResponse.IncomingResponse);
				if (errors.IsNotNullOrEmpty())
					throw new Exception(errors);
			}
			catch (Exception e)
			{
				response.Outcome = IntegrationOutcome.Failure;
				response.Exception = e;
			}

			return response;
		}

    public virtual AssignEmployerActivityResponse AssignEmployerActivity(AssignEmployerActivityRequest request)
    {
      var response = new AssignEmployerActivityResponse(request);
      try
      {
        var version = GetEmployerVersion(request.EmployerId, response.IntegrationBreadcrumbs);
        var requestResponse =
          SendWebRequest(_employerCommandBuilder.SaveActivity(request.EmployerId, request.EmployeeId, request.ActivityId,
            request.AdminId, request.OfficeId, version, request.ExternalUsername, request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId));
        response.IntegrationBreadcrumbs.Add(requestResponse);
        var errors = _translator.FindErrors(requestResponse.IncomingResponse);
        if (errors.IsNotNullOrEmpty())
          throw new Exception(errors);
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    public virtual SaveEmployeeResponse SaveEmployee(SaveEmployeeRequest request)
    {
      var response = new SaveEmployeeResponse(request);
      try
      {
        Guid employeeTrackingId = Guid.NewGuid();
        if (request.Employee.TrackingId.IsNullOrEmpty())
          request.Employee.TrackingId = employeeTrackingId.ToString();

        var version = GetEmployerVersion(request.Employee.ExternalEmployerId, response.IntegrationBreadcrumbs);
        var requestResponse = SendWebRequest(_employerCommandBuilder.SaveEmployee(request.Employee, version, request.ExternalUsername, request.ExternalPassword, request.ExternalOfficeId, request.ExternalAdminId));
        response.IntegrationBreadcrumbs.Add(requestResponse);
        if (request.Employee.ExternalId.IsNullOrEmpty())
        {
          var results = _translator.ProcessMappings(requestResponse.IncomingResponse);
          var employeeMapping = results.FirstOrDefault(x => x.TrackingId == request.Employee.TrackingId);
          if (employeeMapping.IsNotNull())
            response.ExternalEmployeeId = employeeMapping.ExternalId;
        }
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.Exception = e;
      }

      return response;
    }

    /// <summary>
    /// Authenticates the staff user.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    public AuthenticateStaffResponse AuthenticateStaffUser(AuthenticateStaffRequest request)
    {
      var response = new AuthenticateStaffResponse(request);
      try
      {
        // Authorise User
        response.StaffUserInfo = GetAuthenticatedUser(request.Username, request.Password, response.IntegrationBreadcrumbs);
      }
      catch (Exception e)
      {
        response.Outcome = IntegrationOutcome.Failure;
        response.StaffUserInfo = null;
        response.Exception = e;
      }

      return response;
    }

    public DisableJobSeekersReportResponse DisableJobSeekersReport(DisableJobSeekersReportRequest request)
    {
      return new DisableJobSeekersReportResponse(request) { Outcome = IntegrationOutcome.NotImplemented };
    }

    #endregion

    #region JobSeeker requests

    /// <summary>
    /// Gets the job seeker's version
    /// </summary>
    /// <param name="jobSeekerId">The job seeker id.</param>
    /// <param name="breadcrumbs">The breadcrumbs.</param>
    /// <returns></returns>
    /// <exception cref="System.Exception">
    /// GetJobSeekerVersion requires a jobSeekerId.
    /// or
    /// An error was returned from AOSOS.
    /// or
    /// GetSeekerById response Xml could not be transformed to a JobSeeker.
    /// </exception>
    protected virtual string GetJobSeekerVersion(string jobSeekerId, List<RequestResponse> breadcrumbs)
    {
      if (jobSeekerId.IsNullOrEmpty()) throw new Exception("GetJobSeekerVersion requires a jobSeekerId.");
      var requestResponse = SendWebRequest(_jobSeekerCommandBuilder.GetSeekerById(jobSeekerId));
      breadcrumbs.Add(requestResponse);

      var errors = _translator.FindErrors(requestResponse.IncomingResponse);
      if (errors.IsNotNullOrEmpty()) throw new Exception(errors);

      var jobSeeker = _translator.XmlToJobSeeker(requestResponse.IncomingResponse);
      if (jobSeeker.IsNull())
        throw new Exception("GetSeekerById response Xml could not be transformed to a JobSeeker.");

      return jobSeeker.Version;
    }

    /// <summary>
    /// Gets a JobSeeker by the user name.
    /// </summary>
    /// <param name="username">The user name.</param>
    /// <param name="breadcrumbs">The breadcrumbs.</param>
    /// <returns></returns>
    /// <exception cref="System.Exception">
    /// GetSeekerBySocialSecurityNumber response Xml could not be transformed to a JobSeeker.
    /// </exception>
    protected virtual JobSeekerModel GetJobSeekerByUsername(string username, List<RequestResponse> breadcrumbs)
    {
      var requestResponse = SendWebRequest(_jobSeekerCommandBuilder.GetSeekerByUsername(username));

      breadcrumbs.Add(requestResponse);

      var errors = _translator.FindErrors(requestResponse.IncomingResponse);
      if (errors.IsNotNullOrEmpty()) throw new Exception(errors);

      var jobSeeker = _translator.XmlToJobSeekerUser(requestResponse.IncomingResponse);

      if (jobSeeker.IsNull())
        throw new Exception("GetJobSeekerByUsername response Xml could not be transformed to a JobSeeker.");

      jobSeeker = GetJobSeekerById(jobSeeker.ExternalId, breadcrumbs);

      return jobSeeker;
    }


    /// <summary>
    /// Gets a JobSeeker by the user SSN.
    /// </summary>
    /// <param name="socialSecurityNumber">The social security number.</param>
    /// <param name="breadcrumbs">The breadcrumbs.</param>
    /// <returns></returns>
    /// <exception cref="System.Exception">
    /// GetSeekerBySocialSecurityNumber response Xml could not be transformed to a JobSeeker.
    /// </exception>
    protected JobSeekerModel GetJobSeekerBySocialSecurityNumber(string socialSecurityNumber,
      List<RequestResponse> breadcrumbs)
    {
      var requestResponse = SendWebRequest(_jobSeekerCommandBuilder.GetSeekerBySocialSecurityNumber(socialSecurityNumber));
      breadcrumbs.Add(requestResponse);
      var errors = _translator.FindErrors(requestResponse.IncomingResponse);
      if (errors.IsNotNullOrEmpty())
        throw new Exception(errors);

      var jobSeeker = _translator.XmlToJobSeekerUser(requestResponse.IncomingResponse);
      if (jobSeeker.IsNull())
        throw new Exception("GetSeekerBySocialSecurityNumber response Xml could not be transformed to a JobSeeker.");

      jobSeeker = GetJobSeekerById(jobSeeker.ExternalId, breadcrumbs);
      return jobSeeker;
    }

    /// <summary>
    /// Gets the seeker by id.
    /// </summary>
    /// <param name="jobSeekerId">The job seeker id.</param>
    /// <param name="breadcrumbs">The breadcrumbs.</param>
    /// <returns></returns>
    protected JobSeekerModel GetJobSeekerById(string jobSeekerId, List<RequestResponse> breadcrumbs)
    {
      var requestResponse = SendWebRequest(_jobSeekerCommandBuilder.GetSeekerById(jobSeekerId));
      breadcrumbs.Add(requestResponse);

      var errors = _translator.FindErrors(requestResponse.IncomingResponse);
      if (errors.IsNotNullOrEmpty()) throw new Exception(errors);

      var jobSeeker = _translator.XmlToJobSeeker(requestResponse.IncomingResponse);
      if (jobSeeker.IsNull()) throw new Exception("GetSeekerById response Xml could not be transformed to a JobSeeker.");

      return jobSeeker;
    }

		/// <summary>
		/// Look up the job seeker's external ID based on their personal details
		/// </summary>
		/// <param name="firstName">The job seeker's fist name</param>
		/// <param name="lastName">The job seeker's last name</param>
		/// <param name="dateOfBirth">The job seeker's date of birth</param>
		/// <param name="zip">The job seeker's zip</param>
		/// <param name="breadcrumbs">A list of all requests and responses sent to EKOS</param>
		/// <returns>The job seeker id</returns>
		protected virtual string GetJobSeekerId(string firstName, string lastName, DateTime dateOfBirth, string zip, List<RequestResponse> breadcrumbs)
		{
			firstName = firstName.Trim();
			lastName = lastName.Trim();

			// Search for matching job seekers by name and date of birth
			var requestResponse = SendWebRequest(_jobSeekerCommandBuilder.GetSeekerByDetails(firstName, lastName, dateOfBirth));
			breadcrumbs.Add(requestResponse);

			var errors = _translator.FindErrors(requestResponse.IncomingResponse);
			if (errors.IsNotNullOrEmpty())
			{
				throw new Exception(errors);
			}

			var jobSeekers = _translator.XmlToJobSeekers(requestResponse.IncomingResponse);
			if (jobSeekers.IsNullOrEmpty())
			{
				return string.Empty;
			}

			// Do extra filtering on zips to be sure (Also filter on name as EKOS query does partial name checks)
			var jobSeekerMatches = jobSeekers.Where(js => js.FirstName.Equals(firstName, StringComparison.OrdinalIgnoreCase) && js.LastName.Equals(lastName, StringComparison.OrdinalIgnoreCase) && js.AddressPostcodeZip == zip).ToList();

			// If more than one match, don't return anything
			return jobSeekerMatches.Count == 1
							 ? jobSeekerMatches.First().ExternalId
							 : string.Empty;
		}

		/// <summary>
		/// Look up the job seeker's external ID based on their SSN
		/// </summary>
		/// <param name="ssn">The job seeker's SSN</param>
		/// <param name="breadcrumbs">A list of all requests and responses sent to EKOS</param>
		/// <returns>The job seeker id</returns>
		protected virtual string GetJobSeekerId(string ssn, List<RequestResponse> breadcrumbs)
		{
			return GetJobSeekerBySocialSecurityNumber(ssn, breadcrumbs).ExternalId;
		}

    #endregion

    #region User requests

    /// <summary>
    /// Gets the staff model from AOSOS
    /// </summary>
    /// <param name="userName">The user name</param>
    /// <param name="password">The user's password</param>
    /// <param name="breadcrumbs">The breadcrumb trail</param>
    /// <returns>The authentication model (null if not authenticated)</returns>
    protected virtual StaffModel GetAuthenticatedUser(string userName, string password, List<RequestResponse> breadcrumbs)
    {
      // Authorise User
      var requestResponse = SendWebRequest(_staffCommandBuilder.AuthenticateStaffUser(userName, password));
      breadcrumbs.Add(requestResponse);

      var errors = _translator.FindErrors(requestResponse.IncomingResponse);
      if (errors.IsNotNullOrEmpty())
        throw new Exception(errors);

      var authenticationModel = _translator.XmlToAuthModel(requestResponse.IncomingResponse);

      requestResponse = SendWebRequest(_staffCommandBuilder.GetStaffUser(userName, password, authenticationModel.ExternalOfficeId, authenticationModel.ExternalAdminId));
      breadcrumbs.Add(requestResponse);

      errors = _translator.FindErrors(requestResponse.IncomingResponse);
      if (errors.IsNotNullOrEmpty())
        throw new Exception(errors);

      var staffModel = _translator.XmlToStaffModel(requestResponse.IncomingResponse);

      if (staffModel.AuthModel.IsNull())
        staffModel.AuthModel = new AuthenticationModel()
        {
          ExternalAdminId = authenticationModel.ExternalAdminId,
          ExternalOfficeId = authenticationModel.ExternalOfficeId
        };

      return staffModel;
    }


    #endregion

    #region Employer

    /// <summary>
    /// Gets the version number of the employer
    /// </summary>
    /// <param name="employerId"></param>
    /// <param name="breadcrumbs"></param>
    /// <returns></returns>
    protected virtual string GetEmployerVersion(string employerId, List<RequestResponse> breadcrumbs)
    {
      var requestResponse = SendWebRequest(_employerCommandBuilder.GetEmployerById(employerId));

      breadcrumbs.Add(requestResponse);

      var errors = _translator.FindErrors(requestResponse.IncomingResponse);
      if (errors.IsNotNullOrEmpty()) throw new Exception(errors);

      var employer = _translator.XmlToEmployer(requestResponse.IncomingResponse);

      if (employer.IsNull())
        throw new Exception("GetEmployerById response Xml could not be transformed to a Employer.");

      return employer.IsNull() ? string.Empty : employer.Version;
    }

    protected EmployerModel GetEmployerByFein(string fein, List<RequestResponse> breadcrumbs)
    {
      var command = _employerCommandBuilder.GetEmployerByFein(fein, true);

      var requestResponse = SendWebRequest(command);

      breadcrumbs.Add(requestResponse);

      var errors = _translator.FindErrors(requestResponse.IncomingResponse);
      if (errors.IsNotNullOrEmpty()) throw new Exception(errors);

      return _translator.XmlToEmployer(requestResponse.IncomingResponse);
    }

    #endregion

    #region Job requests

    protected virtual Tuple<int, int> GetJobReferralInfo(string jobId, List<RequestResponse> breadcrumbs)
    {
      var requestResponse = SendWebRequest(_jobCommandBuilder.GetJobReferralInfo(jobId));
      breadcrumbs.Add(requestResponse);

      var errors = _translator.FindErrors(requestResponse.IncomingResponse);
      if (errors.IsNotNullOrEmpty()) throw new Exception(errors);

      return _translator.XmlToReferralCounts(requestResponse.IncomingResponse);
    }

    private void ReferSeeker(string jobId, string jobSeekerId, List<RequestResponse> breadcrumbs, string authUserName, string authPassword, string authOfficeId, string authAdminId)
    {
      if (jobSeekerId.IsNullOrEmpty()) return;
			var jobSeekerVersion = GetJobSeekerVersion(jobSeekerId, breadcrumbs);

			var requestResponse = SendWebRequest(_jobSeekerCommandBuilder.SetJobSeekerStatus(jobSeekerId, "", true, jobSeekerVersion, authUserName, authPassword, authOfficeId, authAdminId));
			var errors = _translator.FindErrors(requestResponse.IncomingResponse);
			if (errors.IsNotNullOrEmpty())
				throw new Exception(errors);

      requestResponse = SendWebRequest(_jobCommandBuilder.ReferJobSeeker(jobId, jobSeekerId, authUserName, authPassword, authOfficeId, authAdminId));
      breadcrumbs.Add(requestResponse);

      errors = _translator.FindErrors(requestResponse.IncomingResponse);
      if (errors.IsNotNullOrEmpty()) throw new Exception(errors);
    }


    private JobModel GetJobById(string jobId, List<RequestResponse> breadcrumbs)
    {
      var requestResponse = SendWebRequest(_jobCommandBuilder.GetJobById(jobId));

      breadcrumbs.Add(requestResponse);

      var errors = _translator.FindErrors(requestResponse.IncomingResponse);
      if (errors.IsNotNullOrEmpty()) throw new Exception(errors);

      var job = _translator.XmlToJob(requestResponse.IncomingResponse);

      if (job.IsNull())
        throw new Exception("GetJobById response Xml could not be transformed to a Job.");

      return job;
    }

    /// <summary>
    /// Returns the jobs, from a given list, which are already matched to a job seeker
    /// </summary>
    /// <param name="jobSeekerId">The job seeker to match</param>
    /// <param name="jobIds">A list of jobs to check</param>
    /// <param name="authUserName"></param>
    /// <param name="authPassword"></param>
    /// <param name="authOfficeId"></param>
    /// <param name="authAdminId"></param>
    /// <param name="breadcrumbs"></param>
    /// <returns>A list of jobs to which the job seeker is already matched</returns>
    protected List<string> GetJobMatchesForJobSeeker(string jobSeekerId, List<string> jobIds, string authUserName, string authPassword, string authOfficeId, string authAdminId, List<RequestResponse> breadcrumbs)
    {
      var requestResponse = SendWebRequest(_jobCommandBuilder.GetJobMatches(jobIds, authUserName, authPassword, authOfficeId, authAdminId));
      breadcrumbs.Add(requestResponse);

      var errors = _translator.FindErrors(requestResponse.IncomingResponse);
      if (errors.IsNotNullOrEmpty())
        throw new Exception(errors);

      var allMatches = _translator.XmlToJobMatches(requestResponse.IncomingResponse);

      return jobIds.Where(id => allMatches[id].Contains(jobSeekerId)).ToList();
    }

    #endregion

		#region Other requests

		/// <summary>
		/// Whether the External Office Id should be updated
		/// </summary>
		/// <param name="request">The current request</param>
		/// <returns>A boolean indicating if the office id should be updated</returns>
		public override bool OfficeRequiresUpdate(IIntegrationRequest request)
		{
			return true;
		}

		#endregion

    #region Helper methods

		public override IntegrationAuthentication AuthenticationSupported(FocusModules module)
		{
			switch (module)
			{
				case FocusModules.Assist:
					return IntegrationAuthentication.Full;
				case FocusModules.CareerExplorer:
					return IntegrationAuthentication.ReturnDetails;
				default:
					return IntegrationAuthentication.None;
			}
		}

		public virtual string GetExternalAdminId(string externalUsername)
		{
			return null;
		}

    /// <summary>
    /// Sends the web request.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
		protected RequestResponse SendWebRequest(string request)
		{
			var response = new RequestResponse {StartTime = DateTime.Now};

	    long webRequestElapsedMilliseconds = -1;
			var methodLog = new StringBuilder();

			try
			{
				methodLog.AppendFormat("{0} : Cleaning up request content", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"));

				// clean up accented chars
				var cleanedRequest = request.ReplaceAccentedCharacters();
				var encoder = new UTF8Encoding();
				var content = encoder.GetBytes(cleanedRequest);
				var contentLength = content.Length;

				if (ServicePointManager.DefaultConnectionLimit < 200)
					ServicePointManager.DefaultConnectionLimit = 200;

				response.OutgoingRequest = cleanedRequest;
				if (_clientSettings.AcceptAllCertificates)
					ServicePointManager.ServerCertificateValidationCallback = AcceptAllCertifications;

				methodLog.AppendFormat("{0} : Creating web request : {1}", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"),
					_clientSettings.Url);

				var webRequest = WebRequest.Create(_clientSettings.Url);
				response.Url = webRequest.RequestUri.AbsoluteUri;
				webRequest.Method = "POST";
				webRequest.ContentType = "text/xml";
				webRequest.ContentLength = contentLength;

				methodLog.AppendFormat("{0} : Setting timeout to 200000ms", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"));

				webRequest.Timeout = 200000;

				methodLog.AppendFormat("{0} : Setting request content", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"));

				using (var inputStream = webRequest.GetRequestStream())
				{
					inputStream.Write(content, 0, contentLength);
					inputStream.Close();
				}

				var stopwatch = new Stopwatch();
				try
				{
					stopwatch.Start();

					methodLog.AppendFormat("{0} : Sending request", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"));

					using (var webResponse = (HttpWebResponse)webRequest.GetResponse())
					{
						response.EndTime = DateTime.Now;
						response.StatusCode = webResponse.StatusCode;
						response.StatusDescription = webResponse.StatusDescription;

						methodLog.AppendFormat("{0} : Getting request content", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"));

						using (var outputStream = webResponse.GetResponseStream())
						{
							if (outputStream != null)
							{
								using (var reader = new StreamReader(outputStream))
								{
									response.IncomingResponse = reader.ReadToEnd();
									reader.Close();
								}
							}
						}

						webResponse.Close();
					}

					methodLog.AppendFormat("{0} : Request complete", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"));

					stopwatch.Stop();
					webRequestElapsedMilliseconds = stopwatch.ElapsedMilliseconds;
				}
				catch (Exception)
				{
					methodLog.AppendFormat("{0} : Request failed", DateTime.Now.ToString("yyyyMMdd HH:mm:ss.fff"));

					stopwatch.Stop();
					webRequestElapsedMilliseconds = stopwatch.ElapsedMilliseconds;

					throw;
				}
			}
			catch (WebException ex)
			{
				var httpWebResponse = ex.Response as HttpWebResponse;
				if (httpWebResponse.IsNotNull())
				{
					response.StatusCode = httpWebResponse.StatusCode;
					response.StatusDescription = httpWebResponse.StatusDescription;
				}
				else
				{
					response.StatusCode = null;
					response.StatusDescription = ex.Status.ToString();
				}

				throw new IntegrationRequestException(
					string.Format("AOSOS: Type: {0}. Message: {1}. Status: {2}, Inner Exception: {3}. Request Time: {4}, Log: {5}",
						"WebException", ex.Message, ex.Status, ex.InnerException.IsNull() ? string.Empty : ex.InnerException.Message,
						webRequestElapsedMilliseconds, methodLog), response);
			}
			catch (Exception ex)
			{
				throw new IntegrationRequestException(
					string.Format("AOSOS: Type: {0}. Message: {1}. Inner Exception: {2}. Request Time: {3}, Log: {4}",
						"Exception", ex.Message, ex.InnerException.IsNull() ? string.Empty : ex.InnerException.Message,
						webRequestElapsedMilliseconds, methodLog), response);
			}

			return response;
		}

    public bool AcceptAllCertifications(object sender,
      System.Security.Cryptography.X509Certificates.X509Certificate certification,
      System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
    {
      return true;
    }


		
	}

  internal static class Extensions
  {

    // Taken from prototype for speed look for a better solution
    /// <summary>
    /// Replaces the accented characters.
    /// </summary>
    /// <param name="text">The text.</param>
    /// <returns></returns>
    public static string ReplaceAccentedCharacters(this string text)
    {
      var returnText = "";
      try
      {
        if (String.IsNullOrEmpty(text))
          return text;

        var replacementMapping = new[,] {
                        {'À', 'A'}, {'Ð', 'D'}, {'à', 'a'}, {'ð', 'o'}, {'À', 'A'}, {'Á', 'A'}, {'Â', 'A'}, 
                        {'Ã', 'A'}, {'Ä', 'A'}, {'Å', 'A'}, {'Æ', 'A'}, {'Ç', 'C'}, {'È', 'E'}, {'É', 'E'}, 
                        {'Ê', 'E'}, {'Ë', 'E'}, {'Ì', 'I'}, {'Í', 'I'}, {'Î', 'I'}, {'Ï', 'I'}, {'Ñ', 'N'}, 
                        {'Ò', 'O'}, {'Ó', 'O'}, {'Ô', 'O'}, {'Õ', 'O'}, {'Ö', 'O'}, {'×', 'x'}, {'ý', 'y'}, 
                        {'Ù', 'U'}, {'Ú', 'U'}, {'Û', 'U'}, {'Ü', 'U'}, {'Ý', 'Y'}, {'à', 'a'}, {'á', 'a'}, 
                        {'â', 'a'}, {'ã', 'a'}, {'ä', 'a'}, {'å', 'a'}, {'æ', 'a'}, {'ç', 'c'}, {'è', 'e'}, 
                        {'é', 'e'}, {'ê', 'e'}, {'ë', 'e'}, {'ì', 'i'}, {'í', 'i'}, {'î', 'i'}, {'ï', 'i'}, 
                        {'ð', 'o'}, {'ñ', 'n'}, {'ò', 'o'}, {'ó', 'o'}, {'ô', 'o'}, {'õ', 'o'}, {'ö', 'o'}, 
                        {'÷', '/'}, {'ø', '0'}, {'ù', 'u'}, {'ú', 'u'}, {'û', 'u'}, {'ü', 'u'}, {'ý', 'y'}, 
                        {'þ', 'p'}, {'ÿ', 'y'}, {'¡', '!'}, {'¿', '?'}, {'À', 'A'}, {'Á', 'A'}, {'Â', 'A'}, 
                        {'Ã', 'A'}, {'Ä', 'A'}, {'Å', 'A'}, {'Æ', 'A'}, {'Ç', 'C'}, {'È', 'E'}, {'É', 'E'}, 
                        {'Ê', 'E'}, {'Ë', 'E'}, {'Ì', 'I'}, {'Í', 'I'}, {'Î', 'I'}, {'Ï', 'I'}, {'Ð', 'D'}, 
                        {'Ñ', 'N'}, {'Ò', 'O'}, {'Ó', 'O'}, {'Ô', 'O'}, {'Õ', 'O'}, {'Ö', 'O'}, {'×', 'x'}, 
                        {'Ø', '0'}, {'Ù', 'U'}, {'Ú', 'U'}, {'Û', 'U'}, {'Ü', 'U'}, {'Ý', 'Y'}, {'Þ', 'p'}, 
                        {'ß', 'B'}, {'à', 'a'}, {'á', 'a'}, {'â', 'a'}, {'ã', 'a'}, {'ä', 'a'}, {'å', 'a'}, 
                        {'æ', 'a'}, {'ç', 'c'}, {'è', 'e'}, {'é', 'e'}, {'ê', 'e'}, {'ë', 'e'}, {'ì', 'i'}, 
                        {'í', 'i'}, {'î', 'i'}, {'ï', 'i'}, {'ð', 'o'}, {'ñ', 'n'}, {'ò', 'o'}, {'ó', 'o'}, 
                        {'ô', 'o'}, {'õ', 'o'}, {'ö', 'o'}, {'ù', 'u'}, {'ú', 'u'}, {'û', 'u'}, {'ÿ', 'y'},
                        {'ü', 'u'}, {'þ', 'p'}
                    };

        for (var i = 0; i < replacementMapping.GetLength(0); i++)
          text = text.Replace(replacementMapping[i, 0], replacementMapping[i, 1]);


        returnText = System.Text.RegularExpressions.Regex.Replace(text, @"[^\x20-\x7E]", "");
      }
      catch
      {
        returnText = "";
      }

      return returnText;
    }
    #endregion
  }
}
