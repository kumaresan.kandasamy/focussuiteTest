﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.IntegrationMessages;
using Focus.Services.Integration.Implementations.AOSOS;
using Focus.Services.Integration.Implementations.Aptimus;
using Focus.Services.Integration.Implementations.AtWorks;
using Focus.Services.Integration.Implementations.EKOS;
using Focus.Services.Integration.Implementations.Georgia;
using Focus.Services.Integration.Implementations.Mock;
using Focus.Services.Integration.Implementations.RightManagement;
using Focus.Services.Integration.Implementations.Standalone;
using Focus.Services.Integration.Implementations.Wisconsin;

#endregion

namespace Focus.Services.Integration
{
  public class IntegrationRepositoryFactory
  {
    public readonly IIntegrationRepository Provider;

    /// <summary>
    /// Initializes a new instance of the <see cref="IntegrationRepositoryFactory" /> class.
    /// </summary>
    /// <param name="provider">The provider.</param>
    /// <param name="integrationSettings">The integration settings.</param>
    /// <param name="externalLookUpItems">The external look up items.</param>
    /// <param name="useSelectiveService">if set to <c>true</c> [use selective service].</param>
    /// <exception cref="System.ArgumentException">providerName
    /// or
    /// Outbound Integration - Unable to load the defined Client Provider:  + provider</exception>
    public IntegrationRepositoryFactory(IntegrationClient provider, string integrationSettings, List<ExternalLookUpItemDto> externalLookUpItems)
    {
      try
      {
        switch (provider)
        {
          case IntegrationClient.Aptimus:
            Provider = new AptimusClientRepository(integrationSettings, externalLookUpItems);
            break;

          case IntegrationClient.AOSOS:
            Provider = new AOSOSClientRepository(integrationSettings, externalLookUpItems);
            break;

          case IntegrationClient.EKOS:
            Provider = new EKOSClientRepository(integrationSettings, externalLookUpItems);
            break;

          case IntegrationClient.Standalone:
            Provider = new StandaloneClientRepository();
            break;

          case IntegrationClient.Mock:
            Provider = new MockClientRepository();
            break;

          case IntegrationClient.AtWorks:
            Provider = new AtWorksRepository(integrationSettings);
            break;

          case IntegrationClient.RightManagement:
            Provider = new RightManagementClientRepository(integrationSettings, externalLookUpItems);
            break;

          case IntegrationClient.Wisconsin:
            Provider = new WisconsinClientRepository(integrationSettings, externalLookUpItems);
            break;

					case IntegrationClient.Georgia:
						Provider = new GeorgiaClientRepository(integrationSettings);
						break;


          default:
            throw new ArgumentException(string.Format("Client provider named '{0}' is unknown", provider), "providerName");
        }
      }
      catch (Exception ex)
      {
        throw new ArgumentException("Outbound Integration - Unable to load the defined Client Provider: " + provider, ex);
      }
    }
  }
}
