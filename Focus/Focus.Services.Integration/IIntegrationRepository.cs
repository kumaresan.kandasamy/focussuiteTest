﻿#region Copyright © 2000 - 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core;
using Focus.Core.IntegrationMessages;

#endregion

namespace Focus.Services.Integration
{
  public interface IIntegrationRepository
  {
	  bool IsImplemented(IntegrationPoint integrationPoint);

		#region System

	  LogActionResponse LogAction(LogActionRequest request);

		#endregion

		#region Job seeker

		GetJobSeekerResponse GetJobSeeker(GetJobSeekerRequest request);
    GetJobSeekerResponse GetJobSeekerBySocialSecurityNumber(GetJobSeekerRequest request);
    AuthenticateJobSeekerResponse AuthenticateJobSeeker(AuthenticateJobSeekerRequest request);
    SaveJobSeekerResponse SaveJobSeeker(SaveJobSeekerRequest request);
    SaveJobMatchesResponse SaveJobMatches(SaveJobMatchesRequest request);
    AssignJobSeekerActivityResponse AssignJobSeekerActivity(AssignJobSeekerActivityRequest request);
    AssignAdminToJobSeekerResponse AssignAdminToJobSeeker(AssignAdminToJobSeekerRequest request);
    IsUIClaimantResponse IsUIClaimant(IsUIClaimantRequest request);
    JobSeekerLoginResponse JobSeekerLogin(JobSeekerLoginRequest request);

    #endregion

    #region User

    ChangeUserPasswordResponse ChangeUserPassword(ChangeUserPasswordRequest request);

    #endregion

    #region Job

    SaveJobResponse SaveJob(SaveJobRequest request);

    GetJobResponse GetJob(GetJobRequest request);

    ReferJobSeekerResponse ReferJobSeeker(ReferJobSeekerRequest request);

    SetApplicationStatusResponse SetApplicationStatus(SetApplicationStatusRequest request);

		AssignAdminToJobOrderResponse AssignAdminToJobOrder(AssignAdminToJobOrderRequest request);

    #endregion

    #region Employer

    GetEmployerResponse GetEmployer(GetEmployerRequest request);

    SaveEmployerResponse SaveEmployer(SaveEmployerRequest request);

    AssignAdminToEmployerResponse AssignAdminToEmployer(AssignAdminToEmployerRequest request);

    AssignEmployerActivityResponse AssignEmployerActivity(AssignEmployerActivityRequest request);

    #region Employee

    SaveEmployeeResponse SaveEmployee(SaveEmployeeRequest request);

    UpdateEmployeeStatusResponse UpdateEmployeeStatus(UpdateEmployeeStatusRequest request);

    #endregion

    #endregion

    #region Staff

    AuthenticateStaffResponse AuthenticateStaffUser(AuthenticateStaffRequest request);

    #endregion

    #region Report

    DisableJobSeekersReportResponse DisableJobSeekersReport(DisableJobSeekersReportRequest request);

    #endregion

    #region Helper Methods

    bool OfficeRequiresUpdate(IIntegrationRequest request);
    string GetActivityId(ActionTypes actionType, bool selfServicedActivity);
	  bool IsActivityStaffAssisted(ActionTypes actionType);
		int GetActivityRestriction(AssignJobSeekerActivityRequest request);
	  IntegrationAuthentication AuthenticationSupported(FocusModules module);

	  #endregion
	}
}
