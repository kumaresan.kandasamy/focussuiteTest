﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Globalization;
using System.Xml.Linq;

using Framework.Core;

#endregion

namespace Focus.Services.Integration.Helpers
{
  public static class XmlHelpers
  {
    /// <summary>
    /// Gets the XML element bool value.
    /// </summary>
    /// <param name="element">The element.</param>
    /// <param name="tag">The tag.</param>
    /// <returns></returns>
    public static bool GetXmlElementBoolValue(XElement element, string tag)
    {
      var value = GetXmlElementStringValue(element, tag);

      return (value.ToLower().IsIn("1", "-1", "true", "y"));
    }

    /// <summary>
    /// Gets the XML element date time value.
    /// </summary>
    /// <param name="element">The element.</param>
    /// <param name="tag">The tag.</param>
    /// <returns></returns>
    public static DateTime GetXmlElementDateTimeValue(XElement element, string tag)
    {
      var returnValue = GetXmlElementNullableDateTimeValue(element, tag);

      return returnValue.GetValueOrDefault();
    }

    /// <summary>
    /// Gets the XML element as a nullable date time value.
    /// </summary>
    /// <param name="element">The element.</param>
    /// <param name="tag">The tag.</param>
    /// <returns></returns>
    public static DateTime? GetXmlElementNullableDateTimeValue(XElement element, string tag)
    {
      var value = GetXmlElementStringValue(element, tag);

      // Expected format is actually MMddyyy, which TryParse doesn't accept, so format it to MM/dd/yyyy
      if (value.IsNotNullOrEmpty() && value.Length.IsIn(6, 8) && !value.Contains("/"))
        value = string.Format("{0}/{1}/{2}", value.Substring(0, 2), value.Substring(2, 2), value.Substring(4, 2 + value.Length - 6));

      DateTime returnValue;
      if (!DateTime.TryParse(value, new CultureInfo("en-US"), DateTimeStyles.None, out returnValue))
        return null;

      return returnValue;
    }

    /// <summary>
    /// Gets the XML element decimal value.
    /// </summary>
    /// <param name="element">The element.</param>
    /// <param name="tag">The tag.</param>
    /// <returns></returns>
    public static decimal GetXmlElementDecimalValue(XElement element, string tag)
    {
      var value = GetXmlElementStringValue(element, tag);

      decimal returnValue;
      decimal.TryParse(value, out returnValue);
      return returnValue;
    }

    /// <summary>
    /// Gets the XML element decimal value.
    /// </summary>
    /// <param name="element">The element.</param>
    /// <param name="tag">The tag.</param>
    /// <returns></returns>
    public static float GetXmlElementFloatValue(XElement element, string tag)
    {
      var value = GetXmlElementStringValue(element, tag);

      float returnValue;
      float.TryParse(value, out returnValue);
      return returnValue;
    }

    /// <summary>
    /// Gets the XML element string value.
    /// </summary>
    /// <param name="element">The element.</param>
    /// <param name="tag">The tag.</param>
    /// <param name="namespaceUri">The namespace URI.</param>
    /// <returns></returns>
    public static string GetXmlElementStringValue(XElement element, string tag, string namespaceUri = null)
    {
      var xName = namespaceUri.IsNull() ? XName.Get(tag) : XName.Get(tag, namespaceUri);

      return (string)element.Element(xName) ?? "";
    }

    /// <summary>
    /// Gets the XML element phone number value.
    /// </summary>
    /// <param name="element">The element.</param>
    /// <param name="tag">The tag.</param>
    /// <returns></returns>
    public static string GetXmlElementPhoneNumberValue(XElement element, string tag)
    {
      var value = (string)element.Element(tag) ?? "";

      if (value.Length == 10)
      {
        var tmp = "(" + value.Insert(3, ") ");
        value = tmp.Insert(10, "-");
      }
      return value;
    }


    /// <summary>
    /// Gets the XML attribute string value.
    /// </summary>
    /// <param name="element">The element.</param>
    /// <param name="tag">The tag.</param>
    /// <returns></returns>
    public static string GetXmlAttributeStringValue(XElement element, string tag)
    {
      return (string)element.Attribute(tag) ?? "";
    }

    /// <summary>
    /// Gets the XML element int value.
    /// </summary>
    /// <param name="element">The element.</param>
    /// <param name="tag">The tag.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <param name="namespaceUri">The namespace URI.</param>
    /// <returns></returns>
    public static int GetXmlElementIntValue(XElement element, string tag, int defaultValue = 0, string namespaceUri = null)
    {
      var stringValue = GetXmlElementStringValue(element, tag, namespaceUri);
      int returnValue;
      return !int.TryParse(stringValue, out returnValue) ? defaultValue : returnValue;
    }
  }
}
