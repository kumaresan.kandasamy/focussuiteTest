﻿DECLARE @Items TABLE
(
	[Key] NVARCHAR(400),
	[Value] NVARCHAR(MAX)
)

DECLARE @LocalisationId bigint

SELECT @LocalisationId = [Id] FROM [Config.Localisation] WHERE Culture = '**-**'

INSERT INTO @Items ([Key], [Value])
VALUES('Focus.CareerExplorer.WebExplorer.Controls.JobModal.ClientDegreeDisclaimer.Text', '<br /><div>While widely available, not all programs are available in all locations or in both online and on-campus formats. Please check with a University Enrollment Representative.</div>')

INSERT INTO @Items ([Key], [Value])
VALUES('Focus.CareerExplorer.WebExplorer.Controls.JobPrint.ClientDegreeDisclaimer.Text', '<br /><div>While widely available, not all programs are available in all locations or in both online and on-campus formats. Please check with a University Enrollment Representative.</div>')

INSERT INTO @Items ([Key], [Value])
VALUES('Focus.CareerExplorer.WebAuth.Controls.LogInOrRegister.lightboxOptions2.Label', 'Explore options without registering')
INSERT INTO @Items ([Key], [Value])
VALUES('Focus.CareerExplorer.WebAuth.Controls.LogInOrRegister.LoginNote.Label', '<h3>Welcome to Phoenix Career Explorer!</h3>
				Finding the right career and understanding the educational requirements you’ll need to meet employer demand, 
now and in the future, need not be a frustrating experience. Take the Phoenix Career Explorer journey and learn how interesting real-time exploration can be! Whether you''re uncertain about your career direction, 
considering a career change, or know exactly where you want to go and be in the years ahead, Phoenix Career Explorer can meet your information needs with real-time data about careers, education, and employers in your area – or anywhere you might want to go. 
Thanks for signing in. Visit Phoenix Career Explorer often, share information with your friends and colleagues ... and enjoy the ride to your future!
				<br/>
				<br/>
				<h3>What you can do on Phoenix Career Explorer:</h3>
				
						<ul>
					
						<li>Research careers, employers and programs of study</li>
					
						<li>Explore career and education options</li>
					
						</ul>')
INSERT INTO @Items ([Key], [Value])
VALUES('Focus.CareerExplorer.WebAuth.Controls.LogInOrRegister.LoginRegisterHeader3.Label', 'You may research careers, employers, and programs of study without registering, but you will not be able to permanently store a resume to see personalized career options.')


DELETE LI FROM @Items TI
INNER JOIN [Config.LocalisationItem] LI ON LI.[Key] = TI.[Key]

BEGIN TRANSACTION

INSERT INTO [Config.LocalisationItem] ([Key], [Value], LocalisationId, ContextKey, Localised)
SELECT [Key], [Value], @LocalisationId, '', 0
FROM @Items

COMMIT TRANSACTION