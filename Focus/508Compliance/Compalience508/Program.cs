﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using HtmlAgilityPack;
using log4net;
using log4net.Config;

// ReSharper disable once CheckNamespace
namespace Compliance508
{
    public static class HtmlParser
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static HtmlDocument _htmlDocument;
        private static string _fileName;
        private static int _lineNumber;
        public static List<string> Titles = new List<string>();
        public static void Main()
        {
            XmlConfigurator.Configure();
            int numberOfFile = 0;
            foreach (var file in Directory.EnumerateFiles(@"D:\projects\FocusWF\FocusSuite", "*.*", SearchOption.AllDirectories)
                .Where(s => s.EndsWith(".aspx") || s.EndsWith(".ascx") || s.EndsWith(".cs")))
            {
                numberOfFile++;
                _fileName = file;
                ReadFile(_fileName);

            }
            var allFiles = Directory.EnumerateFiles(@"D:\projects\FocusWF\FocusSuite", "*.*",
                SearchOption.AllDirectories)
                .Where(s => s.EndsWith(".aspx") || s.EndsWith(".ascx"));
            foreach (var file in allFiles)
            {
                numberOfFile++;
                _fileName = file;
                FindTitle(_fileName);
                //FindInput(_fileName);
            }
            Log.Info(string.Format("Total files checked: {0}", numberOfFile));
        }

        private static void FindInput(string fileName)
        {
            var fileContents = File.ReadAllText(fileName);
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(fileContents);
            //foreach (HtmlNode input in doc.DocumentNode.SelectNodes("//input"))
            //{
                
            //}

        }
        
        private static void FindTitle(string fileName)
        {
            //Document title must not be blank
            var fileContents = File.ReadAllText(fileName);
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(fileContents);
            var head = htmlDoc.DocumentNode.SelectNodes("//head");
            if (head == null) return;
            var title = head.Descendants("title").FirstOrDefault();

            if (title == null || title.InnerText == "")
                Log.Info(string.Format("File: {0}, line: {1} has no title tag in head", _fileName, _lineNumber));
            else if (Titles.Contains(title.InnerText.Trim()))
                Log.Info(string.Format("File: {0}, line: {1} has same title tag", _fileName, _lineNumber));
            else 
                Titles.Add(title.InnerText.Trim());
        }

        public static bool ReadFile(string fileName)
        {
            //the path of the file
            var inFile = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            var reader = new StreamReader(inFile);
            _lineNumber = 0;
            try
            {
                var record = reader.ReadLine();
                while (record != null)
                {
                    _lineNumber++;
                    Parse(record);
                    record = reader.ReadLine();
                }
            }
            finally
            {
                reader.Close();
                inFile.Close();
            }
            return true;
        }
        public static string Parse(string input)
        {
            _htmlDocument = new HtmlDocument();

            _htmlDocument.LoadHtml(input);
            ParseNode(_htmlDocument.DocumentNode);

            return _htmlDocument.DocumentNode.WriteTo().Trim();
        }
        private static void ParseChildren(HtmlNode parentNode)
        {
            for (int i = parentNode.ChildNodes.Count - 1; i >= 0; i--)
            {
                ParseNode(parentNode.ChildNodes[i]);
            }
        }
        private static void ParseNode(HtmlNode node)
        {
            if (node.NodeType == HtmlNodeType.Element)
            {
                //IMG elements must have an ALT attribute.IMG elements must have an ALT attribute.
                if (node.Name.ToLower() == "img" && (!node.HasAttributes || !node.Attributes.Contains("alt")))
                    Log.Info(string.Format("File: {0}, line: {1} has no alt tag", _fileName, _lineNumber));
                if (node.Name.ToLower() == "asp:image")
                    if (!node.HasAttributes)
                        Log.Info(string.Format("File: {0}, line: {1} has no alt tag", _fileName, _lineNumber));
                    else
                        if (!node.Attributes.Contains("AlternateText") && !node.Attributes.Contains("alt"))
                            Log.Info(string.Format("File: {0}, line: {1} has no alt tag", _fileName, _lineNumber));
            }
            if (node.HasChildNodes)
            {
                ParseChildren(node);
            }
        }
    }
}