DELETE FROM FocusDevLibrary.dbo.[Library.JobDegreeLevel]
INSERT INTO FocusDevLibrary.dbo.[Library.JobDegreeLevel]
(JobId,DegreeLevel)
SELECT DISTINCT
	r.JobId,
	2
FROM
	FocusDevLibraryImport.dbo.JobMinimumEducation rdt
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON rdt.r_Onet = r.R_Onet
WHERE
	rdt.DegreeType = 'Less than a Bachelor''s Degree'

INSERT INTO FocusDevLibrary.dbo.[Library.JobDegreeLevel]
(JobId,DegreeLevel)
SELECT DISTINCT
	r.JobId,
	3
FROM
	FocusDevLibraryImport.dbo.JobMinimumEducation rdt
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON rdt.r_Onet = r.R_Onet
WHERE
	rdt.DegreeType = 'Bachelor''s Degree and Above'
