INSERT INTO FocusDevLibrary.dbo.[Library.InternshipJobTitle]
(InternshipCategoryId,JobTitle,Frequency)
SELECT DISTINCT
	i.Id,
	it.CleanJobTitle,
	it.Frequency
FROM
	FocusDevLibraryImport.dbo.InternshipJobTitle it
	INNER JOIN FocusDevLibraryImport.dbo.Bookmarks_InternshipCategory i ON it.Cluster = i.Name
