INSERT INTO FocusDevLibrary.dbo.[Library.InternshipReport]
(InternshipCategoryId,StateAreaId,NumberAvailable)
SELECT DISTINCT
	i.id,
	g.StateAreaId,
	ir.[Count]
FROM
	FocusDevLibraryImport.dbo.InternshipReport ir
	INNER JOIN FocusDevLibraryImport.dbo.Bookmarks_InternshipCategory i ON ir.InternClusterVarName = i.Name
	INNER JOIN FocusDevLibraryImport.dbo.[Geography] g ON ir.Area = g.Area
WHERE
	ir.AreaType = 'National'
	AND ir.[Count] > 0
	AND g.AreaType = 'National'

INSERT INTO FocusDevLibrary.dbo.[Library.InternshipReport]
(InternshipCategoryId,StateAreaId,NumberAvailable)
SELECT DISTINCT
	i.id,
	g.StateAreaId,
	ir.[Count]
FROM
	FocusDevLibraryImport.dbo.InternshipReport ir
	INNER JOIN FocusDevLibraryImport.dbo.Bookmarks_InternshipCategory i ON ir.InternClusterVarName = i.Name
	INNER JOIN FocusDevLibraryImport.dbo.[Geography] g ON ir.Area = g.Area
WHERE
	ir.AreaType = 'State'
	AND ir.[Count] > 0
	AND g.AreaType = 'State'

INSERT INTO FocusDevLibrary.dbo.[Library.InternshipReport]
(InternshipCategoryId,StateAreaId,NumberAvailable)
SELECT DISTINCT
	i.id,
	g.StateAreaId,
	ir.[Count]
FROM
	FocusDevLibraryImport.dbo.InternshipReport ir
	INNER JOIN FocusDevLibraryImport.dbo.Bookmarks_InternshipCategory i ON ir.InternClusterVarName = i.Name
	INNER JOIN FocusDevLibraryImport.dbo.[Geography] g ON ir.Area = g.Area
WHERE
	ir.AreaType = 'MSA'
	AND ir.[Count] > 0
	AND g.AreaType = 'MSA'
