INSERT INTO FocusDevLibrary.dbo.[Library.JobExperienceLevel]
(JobId,ExperienceLevel,ExperiencePercentile)
SELECT
	r.JobId,
	1,
	ROUND((ISNULL(re.LessThanTwo,0) * 100),0)
FROM
	FocusDevLibraryImport.dbo.JobExperienceLevel re
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON re.r_onet = r.R_Onet

INSERT INTO FocusDevLibrary.dbo.[Library.JobExperienceLevel]
(JobId,ExperienceLevel,ExperiencePercentile)
SELECT
	r.JobId,
	2,
	ROUND((ISNULL(re.TwoToFive,0) * 100),0)
FROM
	FocusDevLibraryImport.dbo.JobExperienceLevel re
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON re.r_onet = r.R_Onet

INSERT INTO FocusDevLibrary.dbo.[Library.JobExperienceLevel]
(JobId,ExperienceLevel,ExperiencePercentile)
SELECT
	r.JobId,
	3,
	ROUND((ISNULL(re.FiveToEight,0) * 100),0)
FROM
	FocusDevLibraryImport.dbo.JobExperienceLevel re
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON re.r_onet = r.R_Onet

INSERT INTO FocusDevLibrary.dbo.[Library.JobExperienceLevel]
(JobId,ExperienceLevel,ExperiencePercentile)
SELECT
	r.JobId,
	4,
	ROUND((ISNULL(re.EightPlus,0) * 100),0)
FROM
	FocusDevLibraryImport.dbo.JobExperienceLevel re
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON re.r_onet = r.R_Onet
