﻿INSERT INTO FocusDevLibrary.dbo.[Library.DegreeAlias]
(DegreeId,EducationLevel,AliasExtended, DegreeEducationLevelId, Alias)
SELECT DISTINCT
	d.DegreeId,
	rd.DegreeLevel,
	CASE
		WHEN rd.DegreeLevel = 13 THEN 'Certificate'
		WHEN rd.DegreeLevel = 14 THEN 'Associate''s Degree'
		WHEN rd.DegreeLevel = 16 THEN 'Bachelor''s Degree'
		WHEN rd.DegreeLevel = 17 THEN 'Post-Baccalaureate Certificate'
		WHEN rd.DegreeLevel = 18 THEN 'Graduate/Professional Degree'
	END + ' - ' + rd.[Lookup],
	d.DegreeEducationLevelId,
	rd.[Lookup]
FROM
	FocusDevLibraryImport.dbo.DegreeAlias rd
	INNER JOIN FocusDevLibraryImport.dbo.JobDegree d ON rd.Rcip = d.RCIP and rd.DegreeLevel = d.DegreeLevel
