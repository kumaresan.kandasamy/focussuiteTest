BEGIN TRANSACTION
DELETE FROM FocusBase.dbo.[Library.JobCertification]
DELETE FROM FocusBase.dbo.[Library.Certification]
DELETE FROM FocusBase.dbo.[Library.DegreeAlias]
DELETE FROM FocusBase.dbo.[Library.DegreeEducationLevelExt]
DELETE FROM FocusBase.dbo.[Library.ProgramAreaDegree]
DELETE FROM FocusBase.dbo.[Library.ProgramArea]
DELETE FROM FocusBase.dbo.[Library.StudyPlaceDegreeEducationLevel]
DELETE FROM FocusBase.dbo.[Library.StudyPlace]

-- Employer
DELETE FROM FocusBase.dbo.[Library.EmployerJob]
DELETE FROM FocusBase.dbo.[Library.EmployerSkill]


-- Skills
DELETE FROM FocusBase.dbo.[Library.SkillCategorySkill]
DELETE FROM FocusBase.dbo.[Library.SkillReport]
DELETE FROM FocusBase.dbo.[Library.SkillCategory]


-- Internships
DELETE FROM FocusBase.dbo.[Library.InternshipEmployer]
DELETE FROM FocusBase.dbo.[Library.InternshipJobTitle]
DELETE FROM FocusBase.dbo.[Library.InternshipSkill]
DELETE FROM FocusBase.dbo.[Library.InternshipReport]
DELETE FROM FocusBase.dbo.[Library.InternshipCategory]


-- Job
DELETE FROM FocusBase.dbo.[Library.JobReport]
DELETE FROM FocusBase.dbo.[Library.JobJobTitle]
DELETE FROM FocusBase.dbo.[Library.JobTitle]
DELETE FROM FocusBase.dbo.[Library.JobRelatedJob]
DELETE FROM FocusBase.dbo.[Library.JobCareerArea]
DELETE FROM FocusBase.dbo.[Library.JobCareerPathFrom]
DELETE FROM FocusBase.dbo.[Library.JobCareerPathTo]
DELETE FROM FocusBase.dbo.[Library.JobDegreeEducationLevelReport]
DELETE FROM FocusBase.dbo.[Library.JobDegreeEducationLevel]
DELETE FROM FocusBase.dbo.[Library.JobDegreeLevel]
DELETE FROM FocusBase.dbo.[Library.JobEducationRequirement]
DELETE FROM FocusBase.dbo.[Library.JobEmployerSkill]
DELETE FROM FocusBase.dbo.[Library.JobExperienceLevel]
DELETE FROM FocusBase.dbo.[Library.JobSkill]
DELETE FROM FocusBase.dbo.[Library.CareerArea]

DELETE FROM FocusBase.dbo.[Library.Skill]
DELETE FROM FocusBase.dbo.[Library.Employer]
DELETE FROM FocusBase.dbo.[Library.DegreeEducationLevel]
DELETE FROM FocusBase.dbo.[Library.Degree]
DELETE FROM FocusBase.dbo.[Library.Job]

-- Geography

DELETE FROM FocusBase.dbo.[Library.StateArea]
DELETE FROM FocusBase.dbo.[Library.State]
DELETE FROM FocusBase.dbo.[Library.Area]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.Area] ON

INSERT INTO [FocusBase].[dbo].[Library.Area]
(
	[ID]
    ,[Name]
    ,[SortOrder]
)
SELECT [Id]
      ,[Name]
      ,[SortOrder]
FROM [FocusDevLibrary].[dbo].[Library.Area]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.Area] OFF
SET IDENTITY_INSERT [FocusBase].[dbo].[Library.State] ON

INSERT INTO [FocusBase].[dbo].[Library.State]
           ([Id]
           ,[CountryId]
           ,[Name]
           ,[Code]
           ,[SortOrder])
SELECT [Id]
      ,[CountryId]
      ,[Name]
      ,[Code]
      ,[SortOrder]
  FROM [FocusDevLibrary].[dbo].[Library.State]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.State] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.StateArea] ON

INSERT INTO [FocusBase].[dbo].[Library.StateArea]
           ([Id]
           ,[AreaCode]
           ,[Display]
           ,[IsDefault]
           ,[StateId]
           ,[AreaId])
SELECT [Id]
      ,[AreaCode]
      ,[Display]
      ,[IsDefault]
      ,[StateId]
      ,[AreaId]
  FROM [FocusDevLibrary].[dbo].[Library.StateArea]
  
SET IDENTITY_INSERT [FocusBase].[dbo].[Library.StateArea] OFF


SET IDENTITY_INSERT [FocusBase].[dbo].[Library.Job] ON

INSERT INTO [FocusBase].[dbo].[Library.Job]
           ([Id]
           ,[Name]
           ,[ROnet]
           ,[LocalisationId]
           ,[DegreeIntroStatement]
           ,[Description]
           ,[StarterJob])
 SELECT [Id]
      ,[Name]
      ,[ROnet]
      ,[LocalisationId]
      ,[DegreeIntroStatement]
      ,[Description]
      ,[StarterJob]
  FROM [FocusDevLibrary].[dbo].[Library.Job]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.Job] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.Degree] ON
INSERT INTO [FocusBase].[dbo].[Library.Degree]
           ([Id]
           ,[Name]
           ,[RcipCode]
           ,[IsDegreeArea]
           ,[IsClientData]
           ,[ClientDataTag]
           ,[Tier])
SELECT [Id]
      ,[Name]
      ,[RcipCode]
      ,[IsDegreeArea]
      ,[IsClientData]
      ,[ClientDataTag]
      ,[Tier]
  FROM [FocusDevLibrary].[dbo].[Library.Degree]


SET IDENTITY_INSERT [FocusBase].[dbo].[Library.Degree] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.DegreeEducationLevel] ON

INSERT INTO [FocusBase].[dbo].[Library.DegreeEducationLevel]
           ([Id]
           ,[EducationLevel]
           ,[DegreesAwarded]
           ,[Name]
           ,[ExcludeFromReport]
           ,[DegreeId])
SELECT [Id]
      ,[EducationLevel]
      ,[DegreesAwarded]
      ,[Name]
      ,[ExcludeFromReport]
      ,[DegreeId]
  FROM [FocusDevLibrary].[dbo].[Library.DegreeEducationLevel]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.DegreeEducationLevel] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.Employer] ON

INSERT INTO [FocusBase].[dbo].[Library.Employer]
           ([Id]
           ,[Name])
SELECT [Id]
      ,[Name]
  FROM [FocusDevLibrary].[dbo].[Library.Employer]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.Employer] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.Skill] ON

INSERT INTO [FocusBase].[dbo].[Library.Skill]
           ([Id]
           ,[SkillType]
           ,[Name]
           ,[LocalisationId])
SELECT [Id]
      ,[SkillType]
      ,[Name]
      ,[LocalisationId]
  FROM [FocusDevLibrary].[dbo].[Library.Skill]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.Skill] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.CareerArea] ON

INSERT INTO [FocusBase].[dbo].[Library.CareerArea]
           ([Id]
           ,[Name]
           ,[LocalisationId])
SELECT [Id]
      ,[Name]
      ,[LocalisationId]
  FROM [FocusDevLibrary].[dbo].[Library.CareerArea]


SET IDENTITY_INSERT [FocusBase].[dbo].[Library.CareerArea] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobSkill] ON

INSERT INTO [FocusBase].[dbo].[Library.JobSkill]
           ([Id]
           ,[JobSkillType]
           ,[DemandPercentile]
           ,[Rank]
           ,[JobId]
           ,[SkillId])
SELECT [Id]
      ,[JobSkillType]
      ,[DemandPercentile]
      ,[Rank]
      ,[JobId]
      ,[SkillId]
  FROM [FocusDevLibrary].[dbo].[Library.JobSkill]
  
SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobSkill] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobExperienceLevel] ON

INSERT INTO [FocusBase].[dbo].[Library.JobExperienceLevel]
           ([Id]
           ,[ExperienceLevel]
           ,[ExperiencePercentile]
           ,[JobId])
SELECT [Id]
      ,[ExperienceLevel]
      ,[ExperiencePercentile]
      ,[JobId]
  FROM [FocusDevLibrary].[dbo].[Library.JobExperienceLevel]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobExperienceLevel] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobEmployerSkill] ON

IF object_id('tempdb..#TempJobEmployerSkill') IS NOT NULL
BEGIN
  DROP TABLE #TempJobEmployerSkill
END
IF object_id('tempdb..#TempCurrentJobEmployerSkill') IS NOT NULL
BEGIN
  DROP TABLE #TempCurrentJobEmployerSkill
END


CREATE TABLE #TempJobEmployerSkill
(
	Id BIGINT
)
CREATE TABLE #TempCurrentJobEmployerSkill
(
	Id BIGINT
)

INSERT INTO #TempJobEmployerSkill (ID)
SELECT [Id]
  FROM [FocusDevLibrary].[dbo].[Library.JobEmployerSkill]
  
WHILE EXISTS (SELECT 1 FROM #TempJobEmployerSkill)
BEGIN

INSERT INTO #TempCurrentJobEmployerSkill (ID)
SELECT top 100000 ID from #TempJobEmployerSkill

INSERT INTO [FocusBase].[dbo].[Library.JobEmployerSkill]
           ([Id]
           ,[SkillCount]
           ,[Rank]
           ,[JobId]
           ,[EmployerId]
           ,[SkillId])
SELECT [Id]
      ,[SkillCount]
      ,[Rank]
      ,[JobId]
      ,[EmployerId]
      ,[SkillId]
 FROM [FocusDevLibrary].[dbo].[Library.JobEmployerSkill]
 WHERE Id IN (SELECT ID FROM #TempCurrentJobEmployerSkill)
 
 DELETE FROM #TempJobEmployerSkill WHERE Id IN (SELECT ID FROM #TempCurrentJobEmployerSkill)
 DELETE FROM #TempCurrentJobEmployerSkill
  
  
END
  
SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobEmployerSkill] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobEducationRequirement] ON

INSERT INTO [FocusBase].[dbo].[Library.JobEducationRequirement]
           ([Id]
           ,[EducationRequirementType]
           ,[RequirementPercentile]
           ,[JobId])
SELECT [Id]
      ,[EducationRequirementType]
      ,[RequirementPercentile]
      ,[JobId]
  FROM [FocusDevLibrary].[dbo].[Library.JobEducationRequirement]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobEducationRequirement] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobDegreeLevel] ON

INSERT INTO [FocusBase].[dbo].[Library.JobDegreeLevel]
           ([Id]
           ,[DegreeLevel]
           ,[JobId])
SELECT [Id]
      ,[DegreeLevel]
      ,[JobId]
  FROM [FocusDevLibrary].[dbo].[Library.JobDegreeLevel]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobDegreeLevel] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobDegreeEducationLevel] ON

INSERT INTO [FocusBase].[dbo].[Library.JobDegreeEducationLevel]
           ([Id]
           ,[ExcludeFromJob]
           ,[Tier]
           ,[JobId]
           ,[DegreeEducationLevelId])
 SELECT [Id]
      ,[ExcludeFromJob]
      ,[Tier]
      ,[JobId]
      ,[DegreeEducationLevelId]
  FROM [FocusDevLibrary].[dbo].[Library.JobDegreeEducationLevel]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobDegreeEducationLevel] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobDegreeEducationLevelReport] ON

INSERT INTO [FocusBase].[dbo].[Library.JobDegreeEducationLevelReport]
           ([Id]
           ,[HiringDemand]
           ,[DegreesAwarded]
           ,[StateAreaId]
           ,[JobDegreeEducationLevelId])
SELECT [Id]
      ,[HiringDemand]
      ,[DegreesAwarded]
      ,[StateAreaId]
      ,[JobDegreeEducationLevelId]
  FROM [FocusDevLibrary].[dbo].[Library.JobDegreeEducationLevelReport]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobDegreeEducationLevelReport] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobCareerPathTo] ON

INSERT INTO [FocusBase].[dbo].[Library.JobCareerPathTo]
           ([Id]
           ,[Rank]
           ,[CurrentJobId]
           ,[Next1JobId]
           ,[Next2JobId])
SELECT [Id]
      ,[Rank]
      ,[CurrentJobId]
      ,[Next1JobId]
      ,[Next2JobId]
  FROM [FocusDevLibrary].[dbo].[Library.JobCareerPathTo]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobCareerPathTo] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobCareerPathFrom] ON

INSERT INTO [FocusBase].[dbo].[Library.JobCareerPathFrom]
           ([Id]
           ,[Rank]
           ,[FromJobId]
           ,[ToJobId])
SELECT [Id]
      ,[Rank]
      ,[FromJobId]
      ,[ToJobId]
  FROM [FocusDevLibrary].[dbo].[Library.JobCareerPathFrom]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobCareerPathFrom] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobCareerArea] ON

INSERT INTO [FocusBase].[dbo].[Library.JobCareerArea]
           ([Id]
           ,[JobId]
           ,[CareerAreaId])
SELECT [Id]
      ,[JobId]
      ,[CareerAreaId]
  FROM [FocusDevLibrary].[dbo].[Library.JobCareerArea]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobCareerArea] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobRelatedJob] ON

INSERT INTO [FocusBase].[dbo].[Library.JobRelatedJob]
           ([Id]
           ,[RelatedJobId]
           ,[Priority]
           ,[JobId])
SELECT [Id]
      ,[RelatedJobId]
      ,[Priority]
      ,[JobId]
  FROM [FocusDevLibrary].[dbo].[Library.JobRelatedJob]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobRelatedJob] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobTitle] ON

INSERT INTO [FocusBase].[dbo].[Library.JobTitle]
           ([Id]
           ,[Name])
SELECT [Id]
      ,[Name]
  FROM [FocusDevLibrary].[dbo].[Library.JobTitle]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobTitle] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobJobTitle] ON

INSERT INTO [FocusBase].[dbo].[Library.JobJobTitle]
           ([Id]
           ,[DemandPercentile]
           ,[JobId]
           ,[JobTitleId])
SELECT [Id]
      ,[DemandPercentile]
      ,[JobId]
      ,[JobTitleId]
  FROM [FocusDevLibrary].[dbo].[Library.JobJobTitle]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobJobTitle] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobReport] ON

INSERT INTO [FocusBase].[dbo].[Library.JobReport]
           ([Id]
           ,[HiringDemand]
           ,[SalaryTrend]
           ,[SalaryTrendPercentile]
           ,[SalaryTrendAverage]
           ,[LocalisationId]
           ,[GrowthTrend]
           ,[GrowthPercentile]
           ,[HiringTrend]
           ,[HiringTrendPercentile]
           ,[SalaryTrendMin]
           ,[SalaryTrendMax]
           ,[SalaryTrendRealtime]
           ,[SalaryTrendRealtimeAverage]
           ,[SalaryTrendRealtimeMin]
           ,[SalaryTrendRealtimeMax]
           ,[JobId]
           ,[StateAreaId]
		   ,[HiringTrendSubLevel])
SELECT [Id]
      ,[HiringDemand]
      ,[SalaryTrend]
      ,[SalaryTrendPercentile]
      ,[SalaryTrendAverage]
      ,[LocalisationId]
      ,[GrowthTrend]
      ,[GrowthPercentile]
      ,[HiringTrend]
      ,[HiringTrendPercentile]
      ,[SalaryTrendMin]
      ,[SalaryTrendMax]
      ,[SalaryTrendRealtime]
      ,[SalaryTrendRealtimeAverage]
      ,[SalaryTrendRealtimeMin]
      ,[SalaryTrendRealtimeMax]
      ,[JobId]
      ,[StateAreaId]
	  ,[HiringTrendSubLevel]
  FROM [FocusDevLibrary].[dbo].[Library.JobReport]
  

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobReport] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.InternshipCategory] ON

INSERT INTO [FocusBase].[dbo].[Library.InternshipCategory]
           ([Id]
           ,[Name]
           ,[LocalisationId]
           ,[LensFilterId])
SELECT [Id]
      ,[Name]
      ,[LocalisationId]
      ,[LensFilterId]
  FROM [FocusDevLibrary].[dbo].[Library.InternshipCategory]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.InternshipCategory] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.InternshipReport] ON

INSERT INTO [FocusBase].[dbo].[Library.InternshipReport]
           ([Id]
           ,[NumberAvailable]
           ,[StateAreaId]
           ,[InternshipCategoryId])
SELECT [Id]
      ,[NumberAvailable]
      ,[StateAreaId]
      ,[InternshipCategoryId]
  FROM [FocusDevLibrary].[dbo].[Library.InternshipReport]


SET IDENTITY_INSERT [FocusBase].[dbo].[Library.InternshipReport] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.InternshipSkill] ON

INSERT INTO [FocusBase].[dbo].[Library.InternshipSkill]
           ([Id]
           ,[Rank]
           ,[SkillId]
           ,[InternshipCategoryId])
SELECT [Id]
      ,[Rank]
      ,[SkillId]
      ,[InternshipCategoryId]
  FROM [FocusDevLibrary].[dbo].[Library.InternshipSkill]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.InternshipSkill] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.InternshipJobTitle] ON

INSERT INTO [FocusBase].[dbo].[Library.InternshipJobTitle]
           ([Id]
           ,[JobTitle]
           ,[Frequency]
           ,[InternshipCategoryId])
SELECT [Id]
      ,[JobTitle]
      ,[Frequency]
      ,[InternshipCategoryId]
  FROM [FocusDevLibrary].[dbo].[Library.InternshipJobTitle]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.InternshipJobTitle] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.InternshipEmployer] ON

INSERT INTO [FocusBase].[dbo].[Library.InternshipEmployer]
           ([Id]
           ,[Frequency]
           ,[EmployerId]
           ,[StateAreaId]
           ,[InternshipCategoryId])
SELECT [Id]
      ,[Frequency]
      ,[EmployerId]
      ,[StateAreaId]
      ,[InternshipCategoryId]
  FROM [FocusDevLibrary].[dbo].[Library.InternshipEmployer]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.InternshipEmployer] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.SkillCategory] ON

INSERT INTO [FocusBase].[dbo].[Library.SkillCategory]
           ([Id]
           ,[Name]
           ,[LocalisationId]
           ,[ParentId])
SELECT [Id]
      ,[Name]
      ,[LocalisationId]
      ,[ParentId]
  FROM [FocusDevLibrary].[dbo].[Library.SkillCategory]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.SkillCategory] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.SkillReport] ON

INSERT INTO [FocusBase].[dbo].[Library.SkillReport]
           ([Id]
           ,[Frequency]
           ,[SkillId]
           ,[StateAreaId])
SELECT [Id]
      ,[Frequency]
      ,[SkillId]
      ,[StateAreaId]
  FROM [FocusDevLibrary].[dbo].[Library.SkillReport]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.SkillReport] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.SkillCategorySkill] ON

INSERT INTO [FocusBase].[dbo].[Library.SkillCategorySkill]
           ([Id]
           ,[SkillCategoryId]
           ,[SkillId])
SELECT [Id]
      ,[SkillCategoryId]
      ,[SkillId]
  FROM [FocusDevLibrary].[dbo].[Library.SkillCategorySkill]
  
SET IDENTITY_INSERT [FocusBase].[dbo].[Library.SkillCategorySkill] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.EmployerSkill] ON

INSERT INTO [FocusBase].[dbo].[Library.EmployerSkill]
           ([Id]
           ,[Postings]
           ,[EmployerId]
           ,[SkillId])
SELECT [Id]
      ,[Postings]
      ,[EmployerId]
      ,[SkillId]
  FROM [FocusDevLibrary].[dbo].[Library.EmployerSkill]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.EmployerSkill] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.EmployerJob] ON

INSERT INTO [FocusBase].[dbo].[Library.EmployerJob]
           ([Id]
           ,[Positions]
           ,[JobId]
           ,[EmployerId]
           ,[StateAreaId])
SELECT [Id]
      ,[Positions]
      ,[JobId]
      ,[EmployerId]
      ,[StateAreaId]
  FROM [FocusDevLibrary].[dbo].[Library.EmployerJob]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.EmployerJob] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.StudyPlace] ON

INSERT INTO [FocusBase].[dbo].[Library.StudyPlace]
           ([Id]
           ,[Name]
           ,[StateAreaId])
SELECT [Id]
      ,[Name]
      ,[StateAreaId]
  FROM [FocusDevLibrary].[dbo].[Library.StudyPlace]


SET IDENTITY_INSERT [FocusBase].[dbo].[Library.StudyPlace] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.StudyPlaceDegreeEducationLevel] ON

INSERT INTO [FocusBase].[dbo].[Library.StudyPlaceDegreeEducationLevel]
           ([Id]
           ,[StudyPlaceId]
           ,[DegreeEducationLevelId])
SELECT [Id]
      ,[StudyPlaceId]
      ,[DegreeEducationLevelId]
  FROM [FocusDevLibrary].[dbo].[Library.StudyPlaceDegreeEducationLevel]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.StudyPlaceDegreeEducationLevel] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.ProgramArea] ON

INSERT INTO [FocusBase].[dbo].[Library.ProgramArea]
           ([Id]
           ,[Name]
           ,[Description]
           ,[IsClientData]
           ,[ClientDataTag])
SELECT [Id]
      ,[Name]
      ,[Description]
      ,[IsClientData]
      ,[ClientDataTag]
  FROM [FocusDevLibrary].[dbo].[Library.ProgramArea]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.ProgramArea] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.ProgramAreaDegree] ON

INSERT INTO [FocusBase].[dbo].[Library.ProgramAreaDegree]
           ([Id]
           ,[DegreeId]
           ,[ProgramAreaId])
SELECT [Id]
      ,[DegreeId]
      ,[ProgramAreaId]
  FROM [FocusDevLibrary].[dbo].[Library.ProgramAreaDegree]


SET IDENTITY_INSERT [FocusBase].[dbo].[Library.ProgramAreaDegree] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.DegreeEducationLevelExt] ON

INSERT INTO [FocusBase].[dbo].[Library.DegreeEducationLevelExt]
           ([Id]
           ,[Name]
           ,[Url]
           ,[Description]
           ,[DegreeEducationLevelId])
SELECT [Id]
      ,[Name]
      ,[Url]
      ,[Description]
      ,[DegreeEducationLevelId]
  FROM [FocusDevLibrary].[dbo].[Library.DegreeEducationLevelExt]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.DegreeEducationLevelExt] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.DegreeAlias] ON

INSERT INTO [FocusBase].[dbo].[Library.DegreeAlias]
           ([Id]
           ,[AliasExtended]
           ,[EducationLevel]
           ,[Alias]
           ,[DegreeId]
           ,[DegreeEducationLevelId])
SELECT [Id]
      ,[AliasExtended]
      ,[EducationLevel]
      ,[Alias]
      ,[DegreeId]
      ,[DegreeEducationLevelId]
  FROM [FocusDevLibrary].[dbo].[Library.DegreeAlias]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.DegreeAlias] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.Certification] ON

INSERT INTO [FocusBase].[dbo].[Library.Certification]
           ([Id]
           ,[Name])
SELECT [Id]
      ,[Name]
  FROM [FocusDevLibrary].[dbo].[Library.Certification]
  
SET IDENTITY_INSERT [FocusBase].[dbo].[Library.Certification] OFF

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobCertification] ON

INSERT INTO [FocusBase].[dbo].[Library.JobCertification]
           ([Id]
           ,[DemandPercentile]
           ,[JobId]
           ,[CertificationId])
SELECT [Id]
      ,[DemandPercentile]
      ,[JobId]
      ,[CertificationId]
  FROM [FocusDevLibrary].[dbo].[Library.JobCertification]

SET IDENTITY_INSERT [FocusBase].[dbo].[Library.JobCertification] OFF
--COMMIT TRANSACTION

--ROLLBACK TRANSACTION
