
-- Update MSA's
UPDATE FocusExplorerDevDest.dbo.Area SET 
PostalCode = gz.Zip
FROM FocusExplorerDevDest.dbo.Area a
INNER JOIN FocusExplorerDevSource.dbo.[Geography] g ON a.Id = g.AreaId
INNER JOIN FocusExplorerDevSource.dbo.[GeographyZip] gz ON g.Area = gz.Area
WHERE AreaId != 418

-- Update States
UPDATE FocusExplorerDevDest.dbo.Area SET 
PostalCode = gz.Zip
FROM FocusExplorerDevDest.dbo.Area a
INNER JOIN FocusExplorerDevSource.dbo.[Geography] g ON a.Id = g.AreaId
INNER JOIN FocusExplorerDevSource.dbo.[GeographyZip] gz ON g.Area = gz.Area
WHERE AreaId = 418
