
-- Delete existing
DELETE FROM FocusDevLibrary.dbo.[Library.StateArea]
DELETE FROM FocusDevLibrary.dbo.[Library.State]
DELETE FROM FocusDevLibrary.dbo.[Library.Area]

DECLARE @CountryId bigint
SET @CountryId = 13370 -- US

-- National - All States / All Areas
SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.State] ON

INSERT INTO FocusDevLibrary.dbo.[Library.State]
(
	ID,
	CountryId,
	Name,
	Code,
	SortOrder
)
SELECT
	bg.StateId,
	@CountryId,
	'All States',
	'',
	0
FROM
	FocusDevLibraryImport.dbo.[bookmarks_geography] bg
INNER JOIN FocusDevLibraryImport.dbo.[geography] g on bg.AreaCode = g.Area
WHERE
	g.AreaType = 'National'

-- States
INSERT INTO FocusDevLibrary.dbo.[Library.State]
(ID, CountryId,Name,Code,SortOrder)
SELECT
	bg.StateId,
	@CountryId,
	g.AreaName,
	g.PrimaryState,
	1
FROM
	FocusDevLibraryImport.dbo.[bookmarks_geography] bg
INNER JOIN FocusDevLibraryImport.dbo.[geography] g on bg.AreaCode = g.Area
WHERE
	g.AreaType = 'State'
	
SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.State] OFF

SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.Area] ON
INSERT INTO FocusDevLibrary.dbo.[Library.Area]
(
	ID,
	Name,
	SortOrder
)
SELECT
	bg.Area_Id,
	'All Areas',
	0
FROM
	FocusDevLibraryImport.dbo.[bookmarks_geography] bg
INNER JOIN FocusDevLibraryImport.dbo.[geography] g on bg.AreaCode = g.Area
WHERE
	AreaType = 'National'
-- Area
INSERT INTO FocusDevLibrary.dbo.[Library.Area]
(
	Id,
	Name,
	SortOrder
)
SELECT DISTINCT
	Area_ID,
	Area,
	1
FROM
	FocusDevLibraryImport.dbo.[bookmarks_geography] bg
WHERE
	bg.Area_id IS NOT NULL
AND bg.AreaCode IN (SELECT Area FROM Geography WHERE AreaType = 'MSA')
SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.Area] OFF

INSERT INTO FocusDevLibrary.dbo.[Library.Area]
(
	Name,
	SortOrder
)
SELECT DISTINCT
	Area,
	1
FROM
	FocusDevLibraryImport.dbo.[bookmarks_geography] bg
WHERE
	bg.Area_id IS NULL
AND bg.Area NOT IN (select [Name] FROM FocusDevLibrary.dbo.[Library.Area])
AND bg.AreaCode IN (SELECT Area FROM Geography WHERE AreaType = 'MSA')

UPDATE
	g
SET
	g.Area_ID = a.Id
FROM
	FocusDevLibraryImport.dbo.[bookmarks_geography] g
INNER JOIN FocusDevLibrary.dbo.[Library.Area] a ON g.Area = a.[Name]
WHERE
	g.Area_Id IS NULL
	
UPDATE
	g
SET
	g.StateId = s.Id
FROM
	FocusDevLibraryImport.dbo.[Geography] g
INNER JOIN FocusDevLibrary.dbo.[Library.State] s
	ON g.PrimaryState = s.Code



UPDATE
	g
SET
	g.StateId = (SELECT ID FROM FocusDevLibrary.dbo.[Library.State] WHERE Name = 'All States')
FROM
	FocusDevLibraryImport.dbo.[Geography] g
WHERE
	g.PrimaryState = 'National'

	
UPDATE
	g
SET
	g.AreaId = a.Id
FROM
	FocusDevLibraryImport.dbo.[Geography] g
INNER JOIN FocusDevLibrary.dbo.[Library.Area] a
	ON g.AreaName = a.Name
WHERE g.AreaType = 'MSA'

UPDATE
	g
SET
	g.AreaId = (SELECT Id FROM FocusDevLibrary.dbo.[Library.Area] WHERE SortOrder = 0)
FROM
	FocusDevLibraryImport.dbo.[Geography] g
WHERE g.AreaType <> 'MSA'

UPDATE
	g
SET
	g.StateAreaId = bg.State_Area_Id
FROM
	FocusDevLibraryImport.dbo.[Geography] g
INNER JOIN FocusDevLibraryImport.dbo.[bookmarks_geography] bg
	ON bg.StateId = g.StateId AND bg.Area_Id = g.AreaId



SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.StateArea] ON
-- State Area
INSERT INTO FocusDevLibrary.dbo.[Library.StateArea]
(
	ID,
	StateId,
	AreaId,
	AreaCode,
	Display
)
SELECT DISTINCT
	g.StateAreaId,
	g.StateId,
	g.AreaId,
	g.Area,
	1
FROM
	FocusDevLibraryImport.dbo.[geography] g
WHERE
	g.StateAreaId IS NOT NULL

SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.StateArea] OFF

INSERT INTO FocusDevLibrary.dbo.[Library.StateArea]
(
	StateId,
	AreaId,
	AreaCode,
	Display
)
SELECT DISTINCT
	g.StateId,
	g.AreaId,
	g.Area,
	1
FROM
	FocusDevLibraryImport.dbo.[geography] g
WHERE
	g.StateAreaId IS NULL

UPDATE
	g
SET g.StateAreaId = s.ID
FROM
	FocusDevLibraryImport.dbo.[Geography] g
INNER JOIN FocusDevLibrary.dbo.[Library.StateArea] s
	ON s.StateId = g.StateId AND s.AreaId = g.AreaId
