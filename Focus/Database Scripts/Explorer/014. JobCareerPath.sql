DELETE FROM FocusDevLibrary.dbo.[Library.JobCareerPathTo]
INSERT INTO FocusDevLibrary.dbo.[Library.JobCareerPathTo]
(CurrentJobId, Next1JobId, Next2JobId, [Rank])
select
	r1.JobId, r2.JobId, r3.JobId, rc.[NewRank]
from
	FocusDevLibraryImport.dbo.JobCareerPathTo rc
inner join FocusDevLibraryImport.dbo.Job r1
	on rc.CurrentROnet = r1.R_Onet
inner join FocusDevLibraryImport.dbo.Job r2
	on rc.Next1Ronet = r2.R_Onet
left join FocusDevLibraryImport.dbo.Job r3
	on rc.Next2Ronet = r3.R_Onet
where
	r1.JobId IS NOT NULL
and r2.JobId IS NOT NULL
and rc.[NewRank] IS NOT NULL


DELETE FROM FocusDevLibrary.dbo.[Library.JobCareerPathFrom]
INSERT INTO FocusDevLibrary.dbo.[Library.JobCareerPathFrom]
(ToJobId, FromJobId, [Rank])
select 
	r2.JobId, r1.JobId, rc.[Rank]
from
	FocusDevLibraryImport.dbo.JobCareerPathFrom rc
inner join FocusDevLibraryImport.dbo.Job r1
	on rc.CurrentROnet = r1.R_Onet
inner join FocusDevLibraryImport.dbo.Job r2
	on rc.Next1Ronet = r2.R_Onet
where
	r1.JobId IS NOT NULL
and r2.JobId IS NOT NULL
and rc.[Rank] IS NOT NULL
