--select * from ronetdegreeextuopx

DELETE FROM FocusExplorerDevSource.dbo.ROnetDegreeExtUOPX
WHERE RCIP IS NULL

INSERT INTO FocusExplorerDevDest.dbo.DegreeEducationLevelExt
(DegreeEducationLevelId,Name,Url,[Description])
SELECT DISTINCT
	--d.Id,
	--d.Name,
	del.Id,
	--del.EducationLevel,
	rde.UOPXProgram,
	CASE WHEN ISNULL(rde.LinktoPDF,'') = '' THEN '#' ELSE rde.LinktoPDF END,
	rde.[Description]
FROM
	FocusExplorerDevSource.dbo.ROnetDegreeExtUOPX rde
	INNER JOIN FocusExplorerDevDest.dbo.Degree d ON rde.Rcip = d.RCIPCode
	INNER JOIN FocusExplorerDevDest.dbo.DegreeEducationLevel del ON d.Id = del.DegreeId AND rde.DegreeLevel = del.EducationLevel
