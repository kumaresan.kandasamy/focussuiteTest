INSERT INTO FocusDevLibrary.dbo.[Library.JobSkill]
(JobId,SkillId,JobSkillType,DemandPercentile,[Rank])
SELECT
	r.JobId,
	s.Id,
	CASE
		WHEN Source = 'Postings' THEN 1
		WHEN Source = 'Resumes' THEN 2
	END,
	ROUND((ISNULL(rs.ValidSkillRatio,0) * 100),4),
	[Rank]
FROM
	FocusDevLibraryImport.dbo.JobSkills rs
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON rs.R_Onet = r.R_Onet
	INNER JOIN FocusDevLibraryImport.dbo.Skill s ON rs.SkillName = s.Name
WHERE
	ISNULL(s.[Action], '') <> 'Delete'
