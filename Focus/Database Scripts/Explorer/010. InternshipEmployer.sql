insert into FocusDevLibrary.dbo.[Library.InternshipEmployer] (InternshipCategoryId, EmployerId, Frequency, StateAreaId)
select distinct ic.Id, e.Id, i.[Count], s.StateAreaId 
from FocusDevLibraryImport.dbo.internshipemployer i 
inner join FocusDevLibraryImport.dbo.Bookmarks_InternshipCategory ic on i.InternEmployerVarName = ic.Name 
inner join FocusDevLibraryImport.dbo.Employer e on i.CanonEmployer = e.CanonEmployer
inner join FocusDevLibraryImport.dbo.Geography s on i.Area = s.Area and s.PrimaryState = ISNULL(i.[State], 'National')
