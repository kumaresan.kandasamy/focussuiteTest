INSERT INTO FocusDevLibrary.dbo.[Library.InternshipSkill]
(InternshipCategoryId,SkillId,[Rank])
SELECT DISTINCT
	i.Id,
	s.Id,
	[Rank]
FROM
	FocusDevLibraryImport.dbo.InternshipSkills isk
	INNER JOIN FocusDevLibraryImport.dbo.Bookmarks_InternshipCategory i ON isk.InternClusterVarName = i.Name
	INNER JOIN FocusDevLibraryImport.dbo.Skill s ON isk.SkillName = s.Name
