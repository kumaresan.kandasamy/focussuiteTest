update
	e
set e.Id = e2.Id
from 
	FocusDevLibraryImport.dbo.Employer e
inner join FocusDevLibraryImport.dbo.Bookmarks_Employer e2 ON e.CanonEmployer = e2.Name
WHERE e2.Id IS NOT NULL


SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.Employer] ON
INSERT INTO FocusDevLibrary.dbo.[Library.Employer]
(ID, Name)
SELECT DISTINCT
	Id,
	CanonEmployer
FROM
	FocusDevLibraryImport.dbo.Employer
WHERE
	Id IS NOT NULL
SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.Employer] OFF

INSERT INTO FocusDevLibrary.dbo.[Library.Employer]
(Name)
SELECT
	CanonEmployer
FROM
	FocusDevLibraryImport.dbo.Employer
WHERE
	Id IS NULL
	
update
	e
set e.Id = e2.Id
from 
	FocusDevLibraryImport.dbo.Employer e
inner join FocusDevLibrary.dbo.[Library.Employer] e2 ON e.CanonEmployer = e2.Name
where e.Id is null
