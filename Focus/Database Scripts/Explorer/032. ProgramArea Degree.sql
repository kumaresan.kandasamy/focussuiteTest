DECLARE @MaxId BIGINT

UPDATE
	pa
SET pa.Id = bpa.Id
FROM
	FocusDevLibraryImport.dbo.ProgramArea pa
INNER JOIN FocusDevLibraryImport.dbo.Bookmarks_ProgramArea bpa
	on pa.Name = bpa.Name
	and pa.IsClientData = bpa.IsClientData
	and pa.ClientDataTag = bpa.ClientDataTag
	


SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.ProgramArea] ON

INSERT INTO FocusDevLibrary.dbo.[Library.ProgramArea]
(Id, Name, IsClientData, ClientDataTag)
SELECT
	ID,
	Name,
	IsClientData,
	ClientDataTag
FROM
	FocusDevLibraryImport.dbo.ProgramArea
WHERE ID IS NOT NULL

SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.ProgramArea] OFF

SELECT @MaxId = MAX(Id) FROM FocusDevLibrary.dbo.[Library.ProgramArea]
DBCC CHECKIDENT ("FocusDevLibrary.dbo.[Library.ProgramArea]", RESEED, @MaxId);

INSERT INTO FocusDevLibrary.dbo.[Library.ProgramArea]
(Name, IsClientData, ClientDataTag)
SELECT
	Name,
	IsClientData,
	ClientDataTag
FROM
	FocusDevLibraryImport.dbo.ProgramArea
WHERE ID IS NULL

UPDATE
	pa
SET pa.Id = bpa.Id
FROM
	FocusDevLibraryImport.dbo.ProgramArea pa
INNER JOIN FocusDevLibrary.dbo.[Library.ProgramArea] bpa
	on pa.Name = bpa.Name
	and pa.IsClientData = bpa.IsClientData
	and ISNULL(pa.ClientDataTag, 'BGT-INTERNAL') = ISNULL(bpa.ClientDataTag, 'BGT-INTERNAL')
	

INSERT INTO FocusDevLibrary.dbo.[Library.ProgramAreaDegree]
(DegreeId, ProgramAreaId)
SELECT DISTINCT
	d.Id,
	pa.Id
FROM
	FocusDevLibraryImport.dbo.[ProgramArea] pa
INNER JOIN FocusDevLibraryImport.dbo.[DegreeProgramArea] dpa
	ON pa.Name = dpa.DegreeArea
	AND pa.IsClientData = dpa.IsClientData
	and ISNULL(pa.ClientDataTag, 'BGT-INTERNAL') = ISNULL(dpa.ClientDataTag, 'BGT-INTERNAL')
INNER JOIN FocusDevLibrary.dbo.[Library.Degree] d
	ON d.RcipCode = dpa.Rcip
	AND pa.IsClientData = d.IsClientData
	and ISNULL(pa.ClientDataTag, 'BGT-INTERNAL') = ISNULL(d.ClientDataTag, 'BGT-INTERNAL')
UNION
SELECT DISTINCT
	d.Id,
	pa.Id
FROM
	FocusDevLibraryImport.dbo.[ProgramArea] pa
INNER JOIN FocusDevLibraryImport.dbo.[DegreeProgramArea] dpa
	ON pa.Name = dpa.DegreeArea2
	AND pa.IsClientData = dpa.IsClientData
	and ISNULL(pa.ClientDataTag, 'BGT-INTERNAL') = ISNULL(dpa.ClientDataTag, 'BGT-INTERNAL')
INNER JOIN FocusDevLibrary.dbo.[Library.Degree] d
	ON d.RcipCode = dpa.Rcip
	AND pa.IsClientData = d.IsClientData
	and ISNULL(pa.ClientDataTag, 'BGT-INTERNAL') = ISNULL(d.ClientDataTag, 'BGT-INTERNAL')
UNION
SELECT DISTINCT
	d.Id,
	pa.Id
FROM
	FocusDevLibraryImport.dbo.[ProgramArea] pa
INNER JOIN FocusDevLibraryImport.dbo.[DegreeProgramArea] dpa
	ON pa.Name = dpa.DegreeArea3
	AND pa.IsClientData = dpa.IsClientData
	and ISNULL(pa.ClientDataTag, 'BGT-INTERNAL') = ISNULL(dpa.ClientDataTag, 'BGT-INTERNAL')
INNER JOIN FocusDevLibrary.dbo.[Library.Degree] d
	ON d.RcipCode = dpa.Rcip
	AND pa.IsClientData = d.IsClientData
	and ISNULL(pa.ClientDataTag, 'BGT-INTERNAL') = ISNULL(d.ClientDataTag, 'BGT-INTERNAL')
