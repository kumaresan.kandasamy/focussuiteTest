UPDATE FocusDevLibraryImport.dbo.CareerAreaJob
SET CareerArea1Id = NULL, CareerArea2Id = NULL, CareerArea3Id = NULL
GO

UPDATE 
	rcac
SET
	rcac.CareerArea1Id = ca.Id
FROM
	FocusDevLibraryImport.dbo.CareerAreaJob rcac
	INNER JOIN FocusDevLibraryImport.dbo.CareerArea ca ON rcac.CareerArea1 = ca.Name

UPDATE 
	rcac
SET
	rcac.CareerArea2Id = ca.Id
FROM
	FocusDevLibraryImport.dbo.CareerAreaJob rcac
	INNER JOIN FocusDevLibraryImport.dbo.CareerArea ca ON rcac.CareerArea2 = ca.Name
WHERE
	rcac.CareerArea2 IS NOT NULL

UPDATE 
	rcac
SET
	rcac.CareerArea3Id = ca.Id
FROM
	FocusDevLibraryImport.dbo.CareerAreaJob rcac
	INNER JOIN FocusDevLibraryImport.dbo.CareerArea ca ON rcac.CareerArea3 = ca.Name
WHERE
	rcac.CareerArea3 IS NOT NULL

INSERT INTO FocusDevLibrary.dbo.[Library.JobCareerArea]
(JobId,CareerAreaId)
SELECT
	r.JobId,
	rcac.CareerArea1Id
FROM
	FocusDevLibraryImport.dbo.CareerAreaJob rcac
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON rcac.onetplus = r.R_Onet
WHERE
	rcac.CareerArea1Id IS NOT NULL
UNION
SELECT
	r.JobId,
	rcac.CareerArea2Id
FROM
	FocusDevLibraryImport.dbo.CareerAreaJob rcac
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON rcac.onetplus = r.R_Onet
WHERE
	rcac.CareerArea2Id IS NOT NULL
UNION
SELECT
	r.JobId,
	rcac.CareerArea3Id
FROM
	FocusDevLibraryImport.dbo.CareerAreaJob rcac
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON rcac.onetplus = r.R_Onet
WHERE
	rcac.CareerArea3Id IS NOT NULL
