﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.Criteria.JobCertificate;
using Focus.Core.Criteria.JobLanguage;
using Focus.Core.Criteria.JobLicence;
using Focus.Core.Criteria.JobSpecialRequirement;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.AccountService;
using Focus.Core.Messages.AnnotationService;
using Focus.Core.Messages.CandidateService;
using Focus.Core.Messages.CoreService;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Messages.JobService;
using Focus.Core.Messages.PostingService;
using Focus.Core.Messages.ResumeService;
using Focus.Core.Messages.SearchService;
using Focus.Core.Models.Career;
using Focus.MigrationServices.Providers.Common;
using Focus.MigrationServices.Providers.Interfaces;
using JobLocationCriteria = Focus.Core.Criteria.JobLocation.JobLocationCriteria;

#endregion

namespace Focus.MigrationServices.Providers.Implementations
{
  public class FakeProvider : MigrationProviderBase, IMigrationProvider, IEmployer, IJobOrder, IJobSeeker, IUser // IImportTalentUserFromClientRequest
  {
    private int _fakeEmployerNumber;
    private readonly int _fakeEmployerCount;

    private int _fakeJobOrderNumber;
    private readonly int _fakeJobOrderCount;

    private int _fakeJobSeekerNumber;
    private readonly int _fakeJobSeekerCount;

    private int _fakeUserNumber;
    private readonly int _fakeUserCount;

    private int _fakeSavedSearchNumber;
    private readonly int _fakeSavedSearchCount;

    private int _fakeViewedPostingNumber;
    private readonly int _fakeViewedPostingCount;

    private bool _fakeJobCertificatesCreated;
    private bool _fakeJobLanguagesCreated;
    private bool _fakeJobLicencesCreated;
    private bool _fakeJobSpecialRequirementsCreated;
    private bool _fakeJobLocationsCreated;

    public FakeProvider()
    {
      var rnd = new Random();

      _fakeEmployerNumber = 1;
      _fakeEmployerCount = rnd.Next(1, 50);

      _fakeJobOrderNumber = 1;
      _fakeJobOrderCount = rnd.Next(1, 100);

      _fakeJobSeekerNumber = 1;
      _fakeJobSeekerCount = rnd.Next(1, 100);

      _fakeSavedSearchNumber = 1;
      _fakeSavedSearchCount = _fakeJobSeekerCount;

      _fakeViewedPostingNumber = 1;
      _fakeViewedPostingCount = _fakeJobSeekerCount;

      _fakeUserNumber = 1;
      _fakeUserCount = rnd.Next(1, 5);
    }

    public void Initialise(RecordType recordType, long lastId)
    {

    }

    #region Employer

    /// <summary>
    /// Gets a random number of employers requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new employer users</returns>
    public List<RegisterTalentUserRequest> MigrateEmployers(MigrationProviderArgs providerArgs)
    {
      var requests = new List<RegisterTalentUserRequest>();

      for (; _fakeEmployerNumber < _fakeEmployerCount; _fakeEmployerNumber++)
      {
        var request = new RegisterTalentUserRequest
        {
          AccountUserName = string.Concat("Fake User ", _fakeEmployerNumber),
          AccountPassword = string.Concat("FakePassword", _fakeEmployerNumber)
        };

        request.EmployeePerson = new PersonDto
        {
          TitleId = GetLookupId(LookupTypes.Titles, "Mr", ""),
          FirstName = "Fake",
          MiddleInitial = "",
          LastName = "Employee",
          JobTitle = "",
          EmailAddress = string.Format("FakeEmail{0}@test.com", _fakeEmployerNumber.ToString(CultureInfo.InvariantCulture))
        };

        request.EmployeeAddress = new PersonAddressDto
        {
          Line1 = "Address Line 1",
          Line2 = "Address Line 2",
          TownCity = "City",
          CountyId = GetLookupId(LookupTypes.Counties, "Texas", ""),
          PostcodeZip = "1234",
          StateId = GetLookupId(LookupTypes.States, "OK", ""),
          CountryId = GetLookupId(LookupTypes.Countries, "United States", ""),
          IsPrimary = true
        };

        request.EmployeePhone = string.Concat("(123) 123-1", _fakeEmployerNumber.ToString(CultureInfo.InvariantCulture).PadLeft(3, '0'));
        request.EmployeePhoneExtension = string.Concat("1", _fakeEmployerNumber.ToString(CultureInfo.InvariantCulture).PadLeft(3, '0'));
        request.EmployeePhoneType = "Phone";
        request.EmployeeAlternatePhone1 = "";
        request.EmployeeAlternatePhone1Type = "Phone";
        request.EmployeeAlternatePhone2 = "";
        request.EmployeeAlternatePhone2Type = "Phone";
        request.EmployeeEmailAddress = string.Format("FakeEmail{0}@test.com", _fakeEmployerNumber.ToString(CultureInfo.InvariantCulture));

        request.Employer = new EmployerDto
        {
          Id = 0,
          Name = string.Concat("Fake Employer ", _fakeEmployerNumber),
          FederalEmployerIdentificationNumber = string.Concat(_fakeEmployerNumber.ToString(CultureInfo.InvariantCulture).PadLeft(2, '0'), "-", _fakeEmployerNumber.ToString(CultureInfo.InvariantCulture).PadLeft(7, '0')),
          StateEmployerIdentificationNumber = string.Concat(_fakeEmployerNumber.ToString(CultureInfo.InvariantCulture).PadLeft(2, '1'), "-", _fakeEmployerNumber.ToString(CultureInfo.InvariantCulture).PadLeft(7, '1')),
          Url = "",
          OwnershipTypeId = GetLookupId(LookupTypes.OwnershipTypes, "FederalGovernment", ""),
          PrimaryPhone = string.Concat("(123) 123-1", _fakeEmployerNumber.ToString(CultureInfo.InvariantCulture).PadLeft(3, '0')),
          PrimaryPhoneExtension = string.Concat("1", _fakeEmployerNumber.ToString(CultureInfo.InvariantCulture).PadLeft(3, '0')),
          PrimaryPhoneType = "Phone",
          AlternatePhone1 = "",
          AlternatePhone1Type = "Phone",
          AlternatePhone2 = "",
          AlternatePhone2Type = "Phone",
          IndustrialClassification = "11 - Agriculture, Forestry, Fishing and Hunting",
          TermsAccepted = true
        };

        request.EmployeeMigrationId = "1234";
        request.EmployerAddress = new EmployerAddressDto
        {
          Id = 0,
          Line1 = "Address Line 1",
          Line2 = "Address Line 2",
          TownCity = "City",
          CountyId = GetLookupId(LookupTypes.Counties, "Texas", ""),
          PostcodeZip = "1234",
          StateId = GetLookupId(LookupTypes.States, "OK", ""),
          CountryId = GetLookupId(LookupTypes.Countries, "United States", ""),
          IsPrimary = true,
          EmployerId = 0,
          PublicTransitAccessible = true
        };

        request.BusinessUnit = new BusinessUnitDto
        {
          Id = 0,
          EmployerId = 0,
          Name = string.Concat("Fake Business Unit ", _fakeEmployerNumber),
          Url = "",
          OwnershipTypeId = GetLookupId(LookupTypes.OwnershipTypes, "FederalGovernment", ""),
          PrimaryPhone = string.Concat("(123) 123-1", _fakeEmployerNumber.ToString(CultureInfo.InvariantCulture).PadLeft(3, '0')),
          PrimaryPhoneExtension = string.Concat("1", _fakeEmployerNumber.ToString(CultureInfo.InvariantCulture).PadLeft(3, '0')),
          PrimaryPhoneType = "Phone",
          AlternatePhone1 = "",
          AlternatePhone1Type = "Phone",
          AlternatePhone2 = "",
          AlternatePhone2Type = "Phone",
          IndustrialClassification = "11 - Agriculture, Forestry, Fishing and Hunting",
        };

        request.BusinessUnitDescription = new BusinessUnitDescriptionDto
        {
          Id = 0,
          Title = "Company description",
          Description = request.Employer.Name
        };

        request.BusinessUnitAddress = new BusinessUnitAddressDto
        {
          Id = 0,
          Line1 = "Address Line 1",
          Line2 = "Address Line 2",
          TownCity = "City",
          CountyId = GetLookupId(LookupTypes.Counties, "Texas", ""),
          PostcodeZip = "1234",
          StateId = GetLookupId(LookupTypes.States, "OK", ""),
          CountryId = GetLookupId(LookupTypes.Countries, "United States", ""),
          IsPrimary = true,
          BusinessUnitId = 0,
          PublicTransitAccessible = true
        };

        requests.Add(request);
      }

      return requests;
    }

    /// <summary>
    /// Gets a list of secondary business unit descriptions requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new business unit descriptions</returns>
    public List<SaveBusinessUnitDescriptionRequest> MigrateBusinessUnitDescriptions(MigrationProviderArgs providerArgs)
    {
      return new List<SaveBusinessUnitDescriptionRequest>();
    }

    /// <summary>
    /// Gets a list of secondary business unit logo requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new business unit logos</returns>
    public List<SaveBusinessUnitLogoRequest> MigrateBusinessUnitLogos(MigrationProviderArgs providerArgs)
    {
      return new List<SaveBusinessUnitLogoRequest>();
    }

    /// <summary>
    /// Gets a list of resume searches requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new resume saved searches</returns>
    public List<SaveCandidateSavedSearchRequest> MigrateResumeSearches(MigrationProviderArgs providerArgs)
    {
      return new List<SaveCandidateSavedSearchRequest>();
    }

    /// <summary>
    /// Gets a list of resume alerts requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new resume alerts searches</returns>
    public List<SaveCandidateSavedSearchRequest> MigrateResumeAlerts(MigrationProviderArgs providerArgs)
    {
      return new List<SaveCandidateSavedSearchRequest>();
    }

    #endregion

    #region Job Order

    /// <summary>
    /// Gets a random list of job order requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job order requests</returns>
    public List<SaveJobRequest> MigrateJobOrders(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobRequest>();

      for (; _fakeJobOrderNumber < _fakeJobOrderCount; _fakeJobOrderNumber++)
      {
        var request = new SaveJobRequest();

        var job = new JobDto
        {
          EmployerId = 1,
          EmployeeId = 1,
          JobStatus = JobStatuses.Draft,
          EmployerDescriptionPostingPosition = EmployerDescriptionPostingPositions.BelowJobPosting,
          OnetId = 112896,
          BusinessUnitId = 1,
          BusinessUnitDescriptionId = 1,
          BusinessUnitLogoId = 0,
          IsConfidential = false,
          Description = "A fake job description",

          MinimumEducationLevel = EducationLevels.None,
          MinimumEducationLevelRequired = false,

          MinimumExperience = 2,
          MinimumExperienceMonths = 6,
          MinimumExperienceRequired = true,

          MinimumAge = 18,
          MinimumAgeReason = "Fake reason",
          MinimumAgeRequired = true,

          DrivingLicenceClassId = null,
          DrivingLicenceRequired = false,

          LicencesRequired = false,
          CertificationRequired = false,
          LanguagesRequired = false,

          JobLocationType = JobLocationTypes.MainSite,

          FederalContractor = false,
          FederalContractorExpiresOn = null,

          ForeignLabourCertification = false,
          ForeignLabourCertificationH2A = false,
          ForeignLabourCertificationH2B = false,
          ForeignLabourCertificationOther = false,

          CourtOrderedAffirmativeAction = false,
          WorkOpportunitiesTaxCreditHires = WorkOpportunitiesTaxCreditCategories.None,
          PostingFlags = JobPostingFlags.ITEmployer,

          MinSalary = 10,
          MaxSalary = 15,
          SalaryFrequencyId = 182530, 
          HideSalaryOnPosting = false,
          IsCommissionBased = false,
					IsSalaryAndCommissionBased = false,

          NormalWorkDays = DaysOfWeek.All,
          WorkDaysVary = true,
          EmploymentStatusId = 1572528, 
          HoursPerWeek = 35,
          NormalWorkShiftsId = 182648,
          OverTimeRequired = false,
          JobTypeId = 1572558,
          JobStatusId = 1572588, 

          LeaveBenefits = LeaveBenefits.None,
          RetirementBenefits = RetirementBenefits.None,
          InsuranceBenefits = InsuranceBenefits.None,
          MiscellaneousBenefits = MiscellaneousBenefits.None,
          OtherBenefitsDetails = "Other benefits",

          ClosingOn = DateTime.Now.Date.AddDays(7),
          NumberOfOpenings = 1,
          InterviewContactPreferences = ContactMethods.None,

          InterviewEmailAddress = string.Format("FakeEmail{0}@test.com", _fakeJobOrderNumber.ToString(CultureInfo.InvariantCulture)),
          InterviewApplicationUrl = "",
          InterviewMailAddress = "Test Mail Address",
          InterviewFaxNumber = "",
          InterviewPhoneNumber = string.Concat("(123) 123-1", _fakeJobOrderNumber.ToString(CultureInfo.InvariantCulture).PadLeft(3, '0')),
          InterviewDirectApplicationDetails = "",
          InterviewOtherInstructions = "",

          ScreeningPreferences = ScreeningPreferences.AllowUnqualifiedApplications,
          ApprovalStatus = ApprovalStatuses.None
        };

        request.Job = job;
	      request.Module = FocusModules.General;
        requests.Add(request);
      }

      return requests;
    }

    /// <summary>
    /// Gets a random list of job certificate requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job certificate requests</returns>
    public List<SaveJobCertificatesRequest> MigrateJobCertificates(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobCertificatesRequest>();

      if (!_fakeJobCertificatesCreated)
      {
        var jobCertificates = new List<JobCertificateDto>();
        var rnd = new Random();
        var certsToCreate = rnd.Next(1, providerArgs.BatchSize);

        for (int certNumber = 1; certNumber <= certsToCreate; certNumber++)
        {
          var jobCertificate = new JobCertificateDto
          {
            Id = 0,
            JobId = 1, 
            Certificate = "Fake Certificate"
          };

          jobCertificates.Add(jobCertificate);
        }

        var request = new SaveJobCertificatesRequest
        {
          JobCertificates = jobCertificates,
          Criteria = new JobCertificateCriteria { JobId = 1 }
        };

        requests.Add(request);
      }

      _fakeJobCertificatesCreated = true;

      return requests;
    }

    /// <summary>
    /// Gets a random list of job language requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job language requests</returns>
    public List<SaveJobLanguagesRequest> MigrateJobLanguages(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobLanguagesRequest>();

      if (!_fakeJobLanguagesCreated)
      {
        var jobLanguages = new List<JobLanguageDto>();
        var rnd = new Random();
        var lanuguagesToCreate = rnd.Next(1, providerArgs.BatchSize);

        for (var languageNumber = 1; languageNumber <= lanuguagesToCreate; languageNumber++)
        {
          var jobLanguage = new JobLanguageDto
          {
            Id = 0,
            JobId = 1,
            Language = "Fake Language"
          };

          jobLanguages.Add(jobLanguage);
        }

        var request = new SaveJobLanguagesRequest
        {
          JobLanguages = jobLanguages,
          Criteria = new JobLanguageCriteria { JobId = 1 }
        };

        requests.Add(request);
      }

      _fakeJobLanguagesCreated = true;

      return requests;
    }

    /// <summary>
    /// Gets a random list of job licence requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job licence requests</returns>
    public List<SaveJobLicencesRequest> MigrateJobLicences(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobLicencesRequest>();

      if (!_fakeJobLicencesCreated)
      {
        var jobLicences = new List<JobLicenceDto>();
        var rnd = new Random();
        var licencesToCreate = rnd.Next(1, providerArgs.BatchSize);

        for (var licenceNumber = 1; licenceNumber <= licencesToCreate; licenceNumber++)
        {
          var jobLicence = new JobLicenceDto
          {
            Id = 0,
            JobId = licenceNumber, 
            Licence = "Fake Licence"
          };

          jobLicences.Add(jobLicence);
        }

        var request = new SaveJobLicencesRequest
        {
          JobLicences = jobLicences,
          Criteria = new JobLicenceCriteria { JobId = 1 }
        };

        requests.Add(request);
      }

      _fakeJobLicencesCreated = true;

      return requests;
    }

    /// <summary>
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job special requirements requests</returns>
    public List<SaveJobSpecialRequirementsRequest> MigrateJobSpecialRequirements(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobSpecialRequirementsRequest>();

      if (!_fakeJobSpecialRequirementsCreated)
      {
        var jobSpecialRequirements = new List<JobSpecialRequirementDto>();
        var rnd = new Random();
        var specialRequirementsToCreate = rnd.Next(1, providerArgs.BatchSize);

        for (var specialRequirementNumber = 1; specialRequirementNumber <= specialRequirementsToCreate; specialRequirementNumber++)
        {
          var jobSpecialRequirement = new JobSpecialRequirementDto
          {
            Id = 0,
            JobId = specialRequirementNumber, 
            Requirement = "Fake SpecialRequirement"
          };

          jobSpecialRequirements.Add(jobSpecialRequirement);
        }

        var request = new SaveJobSpecialRequirementsRequest
        {
          JobSpecialRequirements = jobSpecialRequirements,
          Criteria = new JobSpecialRequirementCriteria { JobId = 1 }
        };

        requests.Add(request);
      }

      _fakeJobSpecialRequirementsCreated = true;

      return requests;
    }

    /// <summary>
    /// Gets a random list of job locations requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job location requests</returns>
    public List<SaveJobLocationsRequest> MigrateJobLocations(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobLocationsRequest>();

      if (!_fakeJobLocationsCreated)
      {
        var jobSpecialRequirements = new List<JobLocationDto>();
        var rnd = new Random();
        var locationsToCreate = rnd.Next(1, providerArgs.BatchSize);

        for (var locationNumber = 1; locationNumber <= locationsToCreate; locationNumber++)
        {
          var jobSpecialRequirement = new JobLocationDto
          {
            Id = 0,
            JobId = locationNumber, 
            IsPublicTransitAccessible = true,
            Location = "Chester, England"
          };

          jobSpecialRequirements.Add(jobSpecialRequirement);
        }

        var request = new SaveJobLocationsRequest
        {
          JobLocations = jobSpecialRequirements,
          Criteria = new JobLocationCriteria { JobId = 1 }
        };

        requests.Add(request);
      }

      _fakeJobLocationsCreated = true;

      return requests;
    }

    /// <summary>
    /// Gets a random list of job address requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>>
    /// <returns>A list of new job location requests</returns>
    public List<SaveJobAddressRequest> MigrateJobAddresses(MigrationProviderArgs providerArgs)
    {
      return new List<SaveJobAddressRequest>();
    }

    public List<SaveJobProgramsOfStudyRequest> MigrateJobProgramsOfStudy(MigrationProviderArgs providerArgs)
    {
      return new List<SaveJobProgramsOfStudyRequest>();
    }

		public List<SaveJobDrivingLicenceEndorsementsRequest> MigrateJobDrivingLicenceEndorsements(MigrationProviderArgs providerArgs)
		{
			return new List<SaveJobDrivingLicenceEndorsementsRequest>();
		}

    #endregion

    #region Job Seeker

    /// <summary>
    /// Gets a list of job seeker requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job seekers requests</returns>
    public List<RegisterCareerUserRequest> MigrateJobSeekers(MigrationProviderArgs providerArgs)
    {
      var requests = new List<RegisterCareerUserRequest>();

      for (; _fakeJobSeekerNumber < _fakeJobSeekerCount; _fakeJobSeekerNumber++)
      {
        var request = new RegisterCareerUserRequest
        {
          AccountUserName = string.Format("FakeEmail{0}@test.com", _fakeJobSeekerNumber.ToString(CultureInfo.InvariantCulture)),
          AccountPassword = "Password1",
          DateOfBirth = new DateTime(1980, 1, 1),
          EmailAddress = string.Format("FakeEmail{0}@test.com", _fakeJobSeekerNumber.ToString(CultureInfo.InvariantCulture)),
          SocialSecurityNumber = _fakeJobSeekerNumber.ToString(CultureInfo.InvariantCulture).PadLeft(8, '1'),
          FirstName = "Fake",
          MiddleInitial = "",
          LastName = "User",
          SecurityQuestion = "Fake Security Question",
          SecurityAnswer = "Fake Security Answer"
        };

        request.JobSeekerMigrationId = request.AccountUserName;

        requests.Add(request);
      }

      return requests;
    }

    /// <summary>
    /// Gets a list of resume requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new resume requests</returns>
    public List<SaveResumeRequest> MigrateResumes(MigrationProviderArgs providerArgs)
    {
      return new List<SaveResumeRequest>();
    }

    /// <summary>
    /// Gets a list of resume document requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new resume document requests</returns>
    public List<SaveResumeDocumentRequest> MigrateResumeDocuments(MigrationProviderArgs providerArgs)
    {
      return new List<SaveResumeDocumentRequest>();
    }

    /// <summary>
    /// Gets a list of saved searches for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of saved searcg requests</returns>
    public List<SaveSearchRequest> MigrateJobSeekerSavedSearches(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveSearchRequest>();

      for (; _fakeSavedSearchNumber < _fakeSavedSearchCount; _fakeSavedSearchNumber++)
      {
        var request = new SaveSearchRequest
        {
          Name = "",
          SearchType = SavedSearchTypes.CareerPostingSearch,
          SearchCriteria = new SearchCriteria(),
          AlertEmailRequired = false,
          AlertEmailFrequency = EmailAlertFrequencies.Weekly,
          AlertEmailFormat = EmailFormats.HTML,
          AlertEmailStatus = AlertStatus.Active,
          AlertEmailAddress = "",
          SavedSearchMigrationId = string.Format("FakeEmail{0}@test.com", _fakeSavedSearchNumber.ToString(CultureInfo.InvariantCulture)),
          JobSeekerMigrationId = string.Format("FakeEmail{0}@test.com", _fakeSavedSearchNumber.ToString(CultureInfo.InvariantCulture))
        };

        requests.Add(request);
      }

      return requests;
    }

    /// <summary>
    /// Gets a list of search alerts for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of search alert requests</returns>
    public List<SaveSearchRequest> MigrateJobSeekerSearchAlerts(MigrationProviderArgs providerArgs)
    {
      return new List<SaveSearchRequest>();
    }

    /// <summary>
    /// Gets a list of saved searches for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of saved search requests</returns>
    public List<AddViewedPostingRequest> MigrateViewedPostings(MigrationProviderArgs providerArgs)
    {
      var requests = new List<AddViewedPostingRequest>();

      for (; _fakeViewedPostingNumber < _fakeViewedPostingCount; _fakeViewedPostingNumber++)
      {
        var request = new AddViewedPostingRequest
        {
          ViewedPosting = new ViewedPostingDto
          {
            UserId = 1,
            LensPostingId = "FakeLensId",
            ViewedOn = DateTime.Now
          },
          ViewedPostingMigrationId = string.Format("FakeEmail{0}@test.com", _fakeViewedPostingNumber.ToString(CultureInfo.InvariantCulture)),
          JobSeekerMigrationId = string.Format("FakeEmail{0}@test.com", _fakeViewedPostingNumber.ToString(CultureInfo.InvariantCulture))
        };

        requests.Add(request);
      }

      return requests;
    }

    /// <summary>
    /// Gets a list of referrals for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of referral requests</returns>
    public List<ReferCandidateRequest> MigrateReferrals(MigrationProviderArgs providerArgs)
    {
      return new List<ReferCandidateRequest>();
    }

    /// <summary>
    /// Gets a list of activities for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of activities</returns>
		public List<AssignActivityOrActionRequest> MigrateJobSeekerActivities(MigrationProviderArgs providerArgs)
    {
      return new List<AssignActivityOrActionRequest>();
    }

    /// <summary>
    /// Gets a list of flags for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of flags</returns>
    public List<ToggleCandidateFlagRequest> MigrateFlaggedJobSeekers(MigrationProviderArgs providerArgs)
    {
      return new List<ToggleCandidateFlagRequest>();
    }

    #endregion

    #region User

    /// <summary>
    /// Gets a random number of user requests to set up
    /// </summary>
    /// <returns>A list of new users</returns>
    public List<CreateAssistUserRequest> MigrateUsers(MigrationProviderArgs providerArgs)
    {
      var requests = new List<CreateAssistUserRequest>();

      for (; _fakeUserNumber < _fakeUserCount; _fakeUserNumber++)
      {
        var model = new CreateAssistUserModel
        {
          AccountUserName = string.Format("fakeuser{0}@ci.bgt.com", _fakeUserNumber.ToString(CultureInfo.InvariantCulture)),
          AccountPassword = "FakePassword1",
          AccountExternalId = string.Concat("External", _fakeUserNumber.ToString(CultureInfo.InvariantCulture)),
          UserPhone = "Undefined",
          UserExternalOffice = "",
          IsClientAuthenticated = true,
          IsEnabled = true,
          UserPerson = new PersonDto
          {
            FirstName = "Fake",
            LastName = "User"
          },
          UserEmailAddress = string.Format("fakeuser{0}@ci.bgt.com", _fakeUserNumber.ToString(CultureInfo.InvariantCulture))
        };
        var request = new CreateAssistUserRequest
        {
          Models = new List<CreateAssistUserModel> { model },
          UserMigrationId = string.Concat("Fake:", _fakeUserNumber.ToString(CultureInfo.InvariantCulture))
        };

        requests.Add(request);
      }

      return requests;
    }

    /// <summary>
    /// Gets a list of notes and reminders for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of notes and reminders requests</returns>
    public List<SaveNoteReminderRequest> MigrateNotesAndReminders(MigrationProviderArgs providerArgs)
    {
      return new List<SaveNoteReminderRequest>();
    }

    /// <summary>
    /// Gets a list of office requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new office requests</returns>
    public List<SaveOfficeRequest> MigrateOffices(MigrationProviderArgs providerArgs)
    {
      return new List<SaveOfficeRequest>();
    }

    /// <summary>
    /// Gets a list of office mapping requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new office mapping requests</returns>
    public List<PersonOfficeMapperRequest> MigrateOfficeMappings(MigrationProviderArgs providerArgs)
    {
      return new List<PersonOfficeMapperRequest>();
    }

    #endregion
  /*
		#region TalentUser
		public List<ImportTalentUserFromClientRequest> MigrateTalentUser(MigrationProviderArgs providerArgs)
		{
			var requests = new List<ImportTalentUserFromClientRequest>();

			for (; _fakeEmployerNumber < _fakeEmployerCount; _fakeEmployerNumber++)
			{
				var request = new ImportTalentUserFromClientRequest
				              	{
				              		UserId = _fakeEmployerNumber.ToString()
				              	};

				requests.Add(request);
			}

			return requests; ;
		}
		#endregion
   */


		public List<EmployerTradeNamesRequest> MigrateEmployerTradeNames(MigrationProviderArgs providerArgs, bool preProcess)
		{
//			throw new NotImplementedException();
			return null;
		}


		public void PreprocessEmployerTradeNameData(MigrationProviderArgs providerArgs, Framework.DataAccess.Database db)
		{
//			throw new NotImplementedException();
		}


		public void PostprocessEmployerTradeNameData()
		{
//			throw new NotImplementedException();
		}


		public List<SaveJobRequest> MigrateSpideredJobs(MigrationProviderArgs providerArgs)
		{
//			throw new NotImplementedException();
			return null;
		}

		public List<SaveJobRequest> MigrateSpideredJobReferrals(MigrationProviderArgs providerArgs)
		{
//			throw new NotImplementedException();
			return null;
		}

		public List<SaveJobRequest> MigrateSavedJobs(MigrationProviderArgs providerArgs)
		{
//			throw new NotImplementedException();
			return null;
		}

		public List<SaveJobRequest> MigrateJobsViewed(MigrationProviderArgs providerArgs)
		{
//			throw new NotImplementedException();
			return null;
		}

		List<SaveSavedJobRequest> IJobOrder.MigrateSavedJobs(MigrationProviderArgs providerArgs)
		{
//			throw new NotImplementedException();
			return null;
		}
		
		List<ReferCandidateRequest> IJobOrder.MigrateSpideredJobReferrals(MigrationProviderArgs providerArgs)
		{
//			throw new NotImplementedException();
			return null;
		}

		List<SaveSpideredJobRequest> IJobOrder.MigrateSpideredJobs(MigrationProviderArgs providerArgs)
		{
//			throw new NotImplementedException();
			return null;
		}
	}
}
