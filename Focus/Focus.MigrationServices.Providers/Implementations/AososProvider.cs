﻿// Proprietary and Confidential
#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml.XPath;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.AccountService;
using Focus.Core.Messages.CoreService;
using Focus.Core.Messages.EmployerService;
using Focus.MigrationServices.Providers.Common;
using Focus.MigrationServices.Providers.Interfaces;

using Framework.Core;

#endregion

namespace Focus.MigrationServices.Providers.Implementations
{
  public class AososProvider : MigrationProviderBase, IMigrationProvider, IUser
  {
    private int _lastUserid;
    private int _lastOfficeId;
    private int _lastMappingId;

    public AososProvider()
    {
      _lastUserid =
        _lastOfficeId =
        _lastMappingId = 0;
    }

    public void Initialise(RecordType recordType, long lastId)
    {
    }

    #region Assist Data

    /// <summary>
    /// Gets the Assist users from an XML file
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new users</returns>
    public List<CreateAssistUserRequest> MigrateUsers(MigrationProviderArgs providerArgs)
    {
      var requests = new List<CreateAssistUserRequest>();

      try
      {
        var file = XDocument.Load(MigrationFileName);
        var root = file.Root;
        if (root.IsNotNull())
        {
          var assistUsers = root.XPathSelectElements("//ADMINISTRATORS/Administrator").Skip(_lastUserid).Take(providerArgs.BatchSize);

          foreach (var assistUser in assistUsers)
          {
            var model = new CreateAssistUserModel
						{
							/*AccountUserName = GetAttributeValue(assistUser, "EMAIL"),*/
							AccountUserName = GetAttributeValue(assistUser, "security_username"), // original code, but sample data has a single email address for all admins, can't have users with same username in v3 */
              AccountPassword = GetAttributeValue(assistUser, "security_password"),
              UserEmailAddress = GetChildElementValue(assistUser, "EMAIL"),
              UserPerson = new PersonDto
              {
                //TitleId = PersonalTitleDropDownList.SelectedValueToLong(),
                FirstName = GetChildElementValue(assistUser, "FIRST_NAME"),
                MiddleInitial = string.Empty,
                LastName = GetChildElementValue(assistUser, "LAST_NAME"),
                JobTitle = GetChildElementValue(assistUser, "TITLE"),
                EmailAddress = GetChildElementValue(assistUser, "EMAIL"),
              },
              UserPhone = FormatPhoneNumber(GetChildElementValue(assistUser, "PHONE_PRI")),
              UserAlternatePhone = FormatPhoneNumber(GetChildElementValue(assistUser, "PHONE_SEC")),
              UserFax = GetChildElementValue(assistUser, "FAX_PRI"),
              IsEnabled = true,
              IsClientAuthenticated = true,
              UserExternalOffice = GetChildElementValue(assistUser, "OFFICE_ID"),
              AccountExternalId = GetAttributeValue(assistUser, "ID")
            };
						
            var request = new CreateAssistUserRequest
            {
              Models = new List<CreateAssistUserModel> { model },
              SendEmail = false,
              GeneratePassword = false,
              UserMigrationId = string.Concat("AOSOS:", GetAttributeValue(assistUser, "ID")),
              UserSecondaryMigrationId = string.Concat("AOSOS:", GetAttributeValue(assistUser, "security_username")),
              ExtraRoles = new List<string>
              {
                Constants.RoleKeys.AssistJobSeekerReferralApprover,
                Constants.RoleKeys.AssistEmployerAccountApprover,
                Constants.RoleKeys.AssistPostingApprover,
                Constants.RoleKeys.AssistJobSeekerReferralApprovalViewer,
                Constants.RoleKeys.AssistEmployerAccountApprovalViewer,
                Constants.RoleKeys.AssistPostingApprovalViewer
              }
            };

            _lastUserid += 1;

            requests.Add(request);
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateUsers SQL Execution exception.", ex);
      }

      return requests;
    }

    private void AssignCorrectlyFormattedEmailAddressToAccountUserNameField(CreateAssistUserModel model)
    {
      if (!Regex.IsMatch(model.UserEmailAddress ?? "", @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"))
        model.UserEmailAddress = string.Format("{0}.{1}@kygovmigrated.com", model.UserPerson.FirstName, model.UserPerson.LastName);

      if (model.AccountUserName.IsNullOrEmpty() && model.UserEmailAddress.IsNotNullOrEmpty())
        model.AccountUserName = model.UserEmailAddress;
    }

    /// <summary>
    /// Gets a list of notes and reminders for job seekers
    /// </summary>
    /// 
    /// <returns>A list of notes and reminders requests</returns>
    public List<SaveNoteReminderRequest> MigrateNotesAndReminders(MigrationProviderArgs providerArgs)
    {
      return new List<SaveNoteReminderRequest>();
    }

    /// <summary>
    /// Gets a list of office requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new office requests</returns>
    public List<SaveOfficeRequest> MigrateOffices(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveOfficeRequest>();

      try
      {
        var officeFile = XDocument.Load(MigrationFileName);

        var zipsFileName = Regex.Replace(MigrationFileName, @"\.xml", "_zips.xml", RegexOptions.IgnoreCase);
        var officeZipsFile = XDocument.Load(zipsFileName);

        if (zipsFileName.Equals(MigrationFileName, StringComparison.OrdinalIgnoreCase))
          throw new Exception("Error location zips file");

        var officeRoot = officeFile.Root;
        var zipsRoot = officeZipsFile.Root;

        if (officeRoot.IsNotNull())
        {
          var defaultStateId = GetLookupId(LookupTypes.States, AppSettings.DefaultStateKey.Replace("State.", "")).GetValueOrDefault(0);

          var previousExternalIds = officeRoot.XPathSelectElements("OFFICE[not(@INACTIVE='true')]").Take(_lastOfficeId).Select(o => GetAttributeValue(o, "OFFICE_ID")).ToList();

          var offices = officeRoot.XPathSelectElements("OFFICE[not(@INACTIVE='true')]").Skip(_lastOfficeId).Take(providerArgs.BatchSize);
          foreach (var office in offices)
          {
            var officeId = GetAttributeValue(office, "OFFICE_ID");
            if (previousExternalIds.Contains(officeId, StringComparer.OrdinalIgnoreCase))
              throw new Exception(string.Format("Duplicate office id ({0}) found", officeId));

            var zips = string.Empty;
            if (zipsRoot.IsNotNull())
            {
              var xPath = string.Format("zip[@office_id='{0}']", officeId);
              zips = string.Join(",", zipsRoot.XPathSelectElements(xPath).Select(z => z.Attribute("code").Value));
            }
            var request = new SaveOfficeRequest
            {
              Office = new OfficeDto
              {
                OfficeName = office.Value,
                ExternalId = officeId,
                StateId = defaultStateId,
                PostcodeZip = string.Empty,
                AssignedPostcodeZip = zips
              },
              OfficeMigrationId = string.Concat("AOSOS:", officeId)
            };

            _lastOfficeId += 1;
            previousExternalIds.Add(officeId);
            requests.Add(request);
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateOffices SQL Execution exception.", ex);
      }

      return requests;
    }

    /// <summary>
    /// Gets a list of office mapping requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new office mapping requests</returns>
    public List<PersonOfficeMapperRequest> MigrateOfficeMappings(MigrationProviderArgs providerArgs)
    {
      var requests = new List<PersonOfficeMapperRequest>();

      try
      {
        var mappingFile = XDocument.Load(MigrationFileName);
        var mappingRoot = mappingFile.Root;
        
        if (mappingRoot.IsNotNull())
        {
          var mappingLookup = mappingRoot.XPathSelectElements("OFFICE[@STATUS='1']").ToLookup(o => o.Attribute("ADMIN_ID").Value, o => o.Attribute("ID").Value);

          var mappings = mappingLookup.Skip(_lastMappingId).Take(providerArgs.BatchSize);
          foreach (var mapping in mappings)
          {
            var request = new PersonOfficeMapperRequest
            {
              PersonId = 0,
              PersonOffices = null,
              UserMigrationId = string.Concat("AOSOS:", mapping.Key),
              OfficeMigrationIds = mapping.Select(office => string.Concat("AOSOS:", office)).ToList()
            };

            _lastMappingId += 1;

            requests.Add(request);
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateOffices SQL Execution exception.", ex);
      }

      return requests;
    }
    
    #endregion

    #region Helpers

    /*
    /// <summary>
    /// Generates a random password of letters and numbers
    /// </summary>
    /// <returns>The password</returns>
    private string GenerateRandomPassword()
    {
      string password;

      do
      {
        password = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20);
      } while (!Regex.IsMatch(password, AppSettings.PasswordRegExPattern));

      return password;
    }
    */

    /// <summary>
    /// Gets the value of a child element for an XElement
    /// </summary>
    /// <param name="parentElement">The parent XElement</param>
    /// <param name="childElementName">The name of the child element</param>
    /// <param name="index">Whether to target a specific index</param>
    /// <returns>
    /// The value of the child element, or an empty string should it not exist
    /// </returns>
    private string GetChildElementValue(XElement parentElement, string childElementName, int index = 0)
    {
      XElement childElement = null;
      if (index == 0)
      {
        childElement = parentElement.Element(childElementName);
      }
      else
      {
        var childElements = parentElement.Elements(childElementName).Take(index + 1).ToList();
        if (childElements.Count() > index)
          childElement = childElements[index];
      }
      return (childElement == null || childElement.Value.Length == 0) ? string.Empty : childElement.Value;
    }

    /// <summary>
    /// Gets the value of an attribute for an XElement
    /// </summary>
    /// <param name="parentElement">The parent XElement</param>
    /// <param name="attributeName">The name of the child element</param>
    /// <param name="defaultValue">The value to return should it not exist</param>
    /// <returns>
    /// The value of the atrribute, or the default value should it not exist
    /// </returns>
    private string GetAttributeValue(XElement parentElement, string attributeName, string defaultValue = "")
    {
      var attribute = parentElement.Attribute(attributeName);

      return attribute.IsNull() ? defaultValue : attribute.Value;
    }

    #endregion
  }
}
