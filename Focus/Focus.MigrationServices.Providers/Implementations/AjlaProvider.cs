﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.AccountService;
using Focus.Core.Messages.JobService;
using Focus.MigrationServices.Providers.Common;
using Focus.MigrationServices.Providers.Interfaces;

using Framework.DataAccess;
using Framework.Core;

#endregion

namespace Focus.MigrationServices.Providers.Implementations
{
  public class AjlaPovider : MigrationProviderBase, IMigrationProvider, IJobOrder //IImportTalentUserFromClientRequest //IImportAssistUserFromClientRequest //ICreateAssistUserRequest //IEmployer
  {
    private readonly string _ajlaConnectionString;
    //private long _lastEmployerId;
		private long _lastJobOrderId;
    private long _lastJbiidForLocation;
    private long _lastJbiidForAddress;
    private long _lastUsiid;

    public AjlaPovider()
    {
      _ajlaConnectionString = MigrationConnectionString;
    }

    public void Initialise(RecordType recordType, long lastId)
    {
      
    }

    #region IEmployer

		///// <summary>
		///// Gets a list of employer requests to migrate
		///// </summary>
		///// <param name="batchSize">The number of employer requests required</param>
		///// <returns>A list of employer requests</returns>
		//public List<RegisterTalentUserRequest> MigrateEmployers(int providerArgs.Batchsize)
		//{
		//  var requests = new List<RegisterTalentUserRequest>();

		//  try
		//  {
		//    using (var db = CreateMsSqlDatabaseAccess(_ajlaConnectionString, false))
		//    {
		//      var dbParams = new[]
		//      {
		//        new DatabaseParam("LastCoiid", _lastCoiid),
		//        new DatabaseParam("BatchSize", providerArgs.Batchsize)
		//      };

		//      var dataReader = db.ExecuteReader("EXEC Migrations_AJLA_EmployerRequests @LastCoiid, @BatchSize", dbParams);

		//      while (dataReader.Read())
		//      {
		//        var ownershipTypeId = LookupOwnershipType(dataReader.GetString(dataReader.GetOrdinal("EmployerOwnershipType")));
		//        var industrialClassification = GetIndustrialClassification(dataReader.GetString(dataReader.GetOrdinal("EmployerIndustrialClassification")));

		//        // Focus requires urls to be valid uris
		//        var url = FormatUrl(dataReader.GetString(dataReader.GetOrdinal("EmployerUrl")));

		//        var request = new RegisterTalentUserRequest
		//        {
		//          AccountUserName = dataReader.GetString(dataReader.GetOrdinal("UserName")),
		//          AccountPassword = dataReader.GetString(dataReader.GetOrdinal("Password")),
		//          EmployeePerson = new PersonDto
		//          {
		//            //TitleId = PersonalTitleDropDownList.SelectedValueToLong(),
		//            FirstName = dataReader.GetString(dataReader.GetOrdinal("EmployeePersonFirstName")),
		//            MiddleInitial = dataReader.GetString(dataReader.GetOrdinal("EmployeePersonMiddleInitial")),
		//            LastName = dataReader.GetString(dataReader.GetOrdinal("EmployeePersonLastName")),
		//            JobTitle = dataReader.GetString(dataReader.GetOrdinal("EmployeePersonJobTitle")),
		//            EmailAddress = dataReader.GetString(dataReader.GetOrdinal("EmployeePersonEmailAddress")),
		//          },
		//          EmployeeAddress = new PersonAddressDto
		//          {
		//            Line1 = dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressLine1")),
		//            Line2 = dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressLine2")),
		//            TownCity = dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressTownCity")),
		//            CountyId = null,
		//            PostcodeZip = dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressPostcodeZip")),
		//            StateId = GetLookupId(LookupTypes.States, dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressState")), "OK"),
		//            CountryId = GetLookupId(LookupTypes.Countries, dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressCountry")), "US"),
		//            IsPrimary = true
		//          },
		//          EmployeePhone = FormatPhoneNumber(dataReader.GetString(dataReader.GetOrdinal("EmployeePhone"))),
		//          EmployeePhoneExtension = dataReader.GetString(dataReader.GetOrdinal("EmployeePhoneExtension")),
		//          EmployeePhoneType = "Phone",
		//          EmployeeAlternatePhone1 = FormatPhoneNumber(dataReader.GetString(dataReader.GetOrdinal("EmployeeAlternatePhone1"))),
		//          EmployeeAlternatePhone1Type = "Fax",
		//          EmployeeAlternatePhone2 = FormatPhoneNumber(dataReader.GetString(dataReader.GetOrdinal("EmployeeAlternatePhone2"))),
		//          EmployeeAlternatePhone2Type = "Phone",
		//          EmployeeEmailAddress = dataReader.GetString(dataReader.GetOrdinal("EmployeePersonEmailAddress")),
		//          Employer = new EmployerDto
		//          {
		//            Id = 0,
		//            Name = dataReader.GetString(dataReader.GetOrdinal("EmployerName")),
		//            FederalEmployerIdentificationNumber = FormatFEIN(dataReader.GetString(dataReader.GetOrdinal("FederalEmployerIdentificationNumber"))),
		//            StateEmployerIdentificationNumber = dataReader.GetString(dataReader.GetOrdinal("StateEmployerIdentificationNumber")),
		//            Url = url,
		//            OwnershipTypeId = ownershipTypeId,
		//            PrimaryPhone = FormatPhoneNumber(dataReader.GetString(dataReader.GetOrdinal("EmployerPrimaryPhone"))),
		//            PrimaryPhoneExtension = dataReader.GetString(dataReader.GetOrdinal("EmployerPrimaryPhoneExtension")),
		//            PrimaryPhoneType = "Phone",
		//            AlternatePhone1 = FormatPhoneNumber(dataReader.GetString(dataReader.GetOrdinal("EmployerAlternatePhone1"))),
		//            AlternatePhone1Type = "Fax",
		//            AlternatePhone2 = "",
		//            AlternatePhone2Type = "Phone",
		//            IndustrialClassification = industrialClassification,
		//            CommencedOn = null,
		//            ApprovalStatus = ApprovalStatuses.Approved,
		//            ExpiredOn = null,
		//            TermsAccepted = true,
		//            IsRegistrationComplete = true,
		//            IsValidFederalEmployerIdentificationNumber = true,
		//            CreatedOn = dataReader.GetDateTime(dataReader.GetOrdinal("EmployerCreatedOn")),
		//            UpdatedOn = dataReader.GetDateTime(dataReader.GetOrdinal("EmployerUpdatedOn"))
		//          },
		//          EmployerExternalId =  dataReader.GetString(dataReader.GetOrdinal("EmployerExternalId")),
		//          EmployeeMigrationId = string.Concat("AJLA:", dataReader.GetString(dataReader.GetOrdinal("EmployeeMigrationId"))),
		//          EmployerAddress = new EmployerAddressDto
		//          {
		//            Line1 = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressLine1")),
		//            Line2 = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressLine2")),
		//            TownCity = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressTownCity")),
		//            CountyId = null,
		//            PostcodeZip = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressPostcodeZip")),
		//            StateId = GetLookupId(LookupTypes.States, dataReader.GetString(dataReader.GetOrdinal("EmployerAddressState")), "OK"),
		//            CountryId = GetLookupId(LookupTypes.Countries, dataReader.GetString(dataReader.GetOrdinal("EmployerAddressCountry")), "US"),
		//            IsPrimary = true,
		//            EmployerId = 0,
		//            PublicTransitAccessible = true
		//          },
		//          BusinessUnit = new BusinessUnitDto
		//          {
		//            Name = dataReader.GetString(dataReader.GetOrdinal("EmployerName")),
		//            Url = url,
		//            OwnershipTypeId = ownershipTypeId,
		//            PrimaryPhone = FormatPhoneNumber(dataReader.GetString(dataReader.GetOrdinal("EmployerPrimaryPhone"))),
		//            PrimaryPhoneExtension = dataReader.GetString(dataReader.GetOrdinal("EmployerPrimaryPhoneExtension")),
		//            PrimaryPhoneType = "Phone",
		//            AlternatePhone1 = FormatPhoneNumber(dataReader.GetString(dataReader.GetOrdinal("EmployeeAlternatePhone1"))),
		//            AlternatePhone1Type = "Fax",
		//            AlternatePhone2 = "",
		//            AlternatePhone2Type = "Phone",
		//            IsPrimary = (dataReader.GetInt32(dataReader.GetOrdinal("BusinessUnitIsPrimary")) == 1),
		//            IndustrialClassification = industrialClassification,
		//          },
		//          BusinessUnitDescription = new BusinessUnitDescriptionDto
		//          {
		//            Title = "Company Description",
		//            Description = dataReader.GetString(dataReader.GetOrdinal("EmployerName"))
		//          },
		//          BusinessUnitAddress = new BusinessUnitAddressDto
		//          {
		//            Line1 = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressLine1")),
		//            Line2 = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressLine2")),
		//            TownCity = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressTownCity")),
		//            CountyId = null,
		//            PostcodeZip = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressPostcodeZip")),
		//            StateId = GetLookupId(LookupTypes.States, dataReader.GetString(dataReader.GetOrdinal("EmployerAddressState")), "OK"),
		//            CountryId = GetLookupId(LookupTypes.Countries, dataReader.GetString(dataReader.GetOrdinal("EmployerAddressCountry")), "US"),
		//            IsPrimary = true,
		//            PublicTransitAccessible = true
		//          }
		//        };

		//        // Focus requires user names to be email addresses
		//        if (!request.AccountUserName.Contains("@"))
		//          request.AccountUserName = string.Concat(request.AccountUserName, "@ajila.com");

		//        if (request.EmployeeEmailAddress.Length == 0)
		//          request.EmployeeEmailAddress = request.AccountUserName;

		//        request.AccountPassword = "Password1";

		//        _lastCoiid = dataReader.GetString(dataReader.GetOrdinal("Coiid"));

		//        requests.Add(request);
		//      }
		//    }
		//  }
		//  catch (Exception ex)
		//  {
		//    throw new Exception("MigrateUsers SQL Execution exception.", ex);
		//  }

		//  return requests;
		//}

		///// <summary>
		///// Gets a list of secondary business unit descriptions requests to set up
		///// </summary>
		///// <param name="batchSize">The number of records required</param>
		///// <returns>A list of new business unit descriptions</returns>
		//public List<SaveBusinessUnitDescriptionRequest> MigrateBusinessUnitDescriptions(int providerArgs.Batchsize)
		//{
		//  return new List<SaveBusinessUnitDescriptionRequest>();
		//}

		///// <summary>
		///// Gets a list of secondary business unit logo requests to set up
		///// </summary>
		///// <param name="batchSize">The number of records required</param>
		///// <returns>A list of new business unit logos</returns>
		//public List<SaveBusinessUnitLogoRequest> MigrateBusinessUnitLogos(int providerArgs.Batchsize)
		//{
		//  return new List<SaveBusinessUnitLogoRequest>();
		//}

    #endregion

    #region ISaveJobRequest

    /// <summary>
    /// Gets a list of job requests to migrate
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of job requests</returns>
    public List<SaveJobRequest> MigrateJobOrders(MigrationProviderArgs providerArgs)
  	{
      var requests = new List<SaveJobRequest>();
			//if (_lastJobOrderId == 0) _lastJobOrderId = 682443;
      try
      {
				using (var db = CreateMsSqlDatabaseAccess(_ajlaConnectionString, false))
        {
          var dbParams = new[]
					{
					  new DatabaseParam("LastJbiid", _lastJobOrderId),
            new DatabaseParam("BatchSize", providerArgs.BatchSize)
					};

          var dataReader = db.ExecuteReader("EXEC Migrations_AJLA_JobOrderRequests @LastJbiid, @BatchSize", dbParams);

          while (dataReader.Read())
          {
          	var job = new JobDto
          	{
          	  EmployerId = 0,
              EmployeeId = 0
          	};

          	job.JobStatus = JobStatuses.Active;
          	job.JobTitle = dataReader.GetString(dataReader.GetOrdinal("JobTitle"));
          	job.ApprovalStatus = ApprovalStatuses.Approved;
          	job.OnetId = GetOnetIdByCode(dataReader.GetString(dataReader.GetOrdinal("OnetCode")));
          	job.BusinessUnitId = 0;
          	job.BusinessUnitDescriptionId = 0;
          	job.EmployerDescriptionPostingPosition = EmployerDescriptionPostingPositions.BelowJobPosting;
          	job.BusinessUnitLogoId = null;
          	job.IsConfidential = false;
						////Description = string.Concat(dataReader.GetString(dataReader.GetOrdinal("Description")), "\n\n", dataReader.GetString(dataReader.GetOrdinal("JobSkills"))),
          	job.Description = dataReader.GetString(dataReader.GetOrdinal("Description"));
          	job.MinimumEducationLevel = LookUpMinimumEducationLevel(dataReader.GetString(dataReader.GetOrdinal("MinimumEducationLevel")));
          	job.MinimumEducationLevelRequired = (dataReader.GetString(dataReader.GetOrdinal("MinimumEducationLevel")) !="0");
						////MinimumExperience = LookUpMinimumExperience(dataReader.GetInt32(dataReader.GetOrdinal("MinimumExperience"))),
          	job.MinimumExperience = dataReader.GetInt32(dataReader.GetOrdinal("MinimumExperience"));
          	job.MinimumExperienceMonths = dataReader.GetInt32(dataReader.GetOrdinal("MinimumExperienceMonths"));
          	job.MinimumExperienceRequired = (dataReader.GetString(dataReader.GetOrdinal("MinimumExperienceRequired")) == "1");
          	job.MinimumAge = null;
          	job.MinimumAgeReason = "";
          	job.MinimumAgeRequired = false;
          	job.DrivingLicenceClassId = null;
          	job.DrivingLicenceRequired = false;
          	job.LicencesRequired = false;
          	job.CertificationRequired = false;
          	job.LanguagesRequired = false;
          	job.JobLocationType = JobLocationTypes.MainSite;
          	job.FederalContractor = false;
          	job.FederalContractorExpiresOn = null;
          	job.ForeignLabourCertificationH2A = false;
          	job.ForeignLabourCertificationH2B = false;
          	job.ForeignLabourCertificationOther = false;
          	job.CourtOrderedAffirmativeAction = false;
          	job.WorkOpportunitiesTaxCreditHires = WorkOpportunitiesTaxCreditCategories.None;
          	job.HiringFromTaxCreditProgramNotificationSent = null;
          	job.PostingFlags = JobPostingFlags.None;
          	job.MinSalary = dataReader.GetDecimal(dataReader.GetOrdinal("MinSalary"));
          	job.MaxSalary = dataReader.GetDecimal(dataReader.GetOrdinal("MaxSalary"));
						job.SalaryFrequencyId = LookupSalaryFrequency(dataReader.GetString(dataReader.GetOrdinal("SalaryFrequency")));// Maps to the CodeItem
          	job.HideSalaryOnPosting = false;
						job.EmploymentStatusId = LookupEmploymentStatus(dataReader.GetString(dataReader.GetOrdinal("EmploymentStatus")));// Maps to the CodeItem
						job.HoursPerWeek = LookupHoursPerWeek(dataReader.GetInt32(dataReader.GetOrdinal("HoursPerWeek")));// Hard Coded Mapping
          	job.OverTimeRequired = false;
          	job.IsCommissionBased = (dataReader.GetString(dataReader.GetOrdinal("IsCommissionBased")) == "1");
						job.IsSalaryAndCommissionBased = (dataReader.GetString(dataReader.GetOrdinal("IsSalaryAndCommissionBased")) == "1");
          	job.NormalWorkDays = DaysOfWeek.None;
          	job.WorkDaysVary = null;
						job.NormalWorkShiftsId = LookupNormalWorkingShifts(dataReader.GetString(dataReader.GetOrdinal("NormalWorkShifts")));// Maps to the CodeItem - check with transformer
						job.JobTypeId = LookupJobType(dataReader.GetString(dataReader.GetOrdinal("JobType")));// Maps to the CodeItem
						job.JobStatusId = LookupJobStatus(dataReader.GetString(dataReader.GetOrdinal("JobStatus")));// Maps to the CodeItem
          	job.LeaveBenefits = LeaveBenefits.None;
          	job.RetirementBenefits = RetirementBenefits.None;
						job.InsuranceBenefits = LookupInsuranceBenefits(dataReader);// Maps with enum
						job.MiscellaneousBenefits = LookupMiscellaneousBenefits(dataReader);// Maps with enum
          	job.OtherBenefitsDetails = dataReader.GetString(dataReader.GetOrdinal("OtherBenefitsDetails"));
          	job.CreatedOn = dataReader.GetDateTime(dataReader.GetOrdinal("CreatedOn"));
						job.CreatedBy = 0; // TODO: May need migration provider to look this up
          	job.UpdatedOn = dataReader.GetDateTime(dataReader.GetOrdinal("UpdatedOn"));
						job.UpdatedBy = 0; // TODO: May need migration provider to look this up
          	job.PostedOn = dataReader.GetDateTime(dataReader.GetOrdinal("PostedOn"));
          	job.PostedBy = null;
          	job.ClosingOn = dataReader.IsDBNull(dataReader.GetOrdinal("ExpiryDate"))
          	                	? dataReader.GetDateTime(dataReader.GetOrdinal("PostedOn")).AddDays(
          	                		int.Parse(dataReader.GetString(dataReader.GetOrdinal("DaysToExpire"))))
          	                	: dataReader.GetDateTime(dataReader.GetOrdinal("ExpiryDate"));
          	job.ClosedOn = dataReader.IsDBNull(dataReader.GetOrdinal("ClosedOn"))
          	               	? (DateTime?) null
          	               	: dataReader.GetDateTime(dataReader.GetOrdinal("ClosedOn"));
          	job.ClosedBy = null;
          	job.ApprovedOn = dataReader.GetDateTime(dataReader.GetOrdinal("ApprovedOn"));
          	job.ApprovedBy = null;
          	job.NumberOfOpenings = dataReader.GetInt32(dataReader.GetOrdinal("NumberOfOpenings"));
								job.InterviewContactPreferences = LookupInterviewContactPreferences(dataReader.GetString(dataReader.GetOrdinal("InterviewContactPreferences")));// Maps with enum
          	job.InterviewEmailAddress = dataReader.GetString(dataReader.GetOrdinal("InterviewEmailAddress"));
          	job.InterviewApplicationUrl = dataReader.GetString(dataReader.GetOrdinal("InterviewApplicationUrl"));
          	job.InterviewMailAddress = string.Format("{0}\n{1}\n{2}\n{3}\n{4}",
          	                                         dataReader.GetString(
          	                                         	dataReader.GetOrdinal("InterviewContactAddress1")),
          	                                         dataReader.GetString(
          	                                         	dataReader.GetOrdinal("InterviewContactAddress2")),
          	                                         dataReader.GetString(dataReader.GetOrdinal("InterviewContactCity")),
          	                                         dataReader.GetString(dataReader.GetOrdinal("InterviewContactState")),
          	                                         // We dont need to map this
          	                                         dataReader.GetString(dataReader.GetOrdinal("InterviewContactZip")));
          	job.InterviewFaxNumber = dataReader.GetString(dataReader.GetOrdinal("InterviewFaxNumber"));
          	job.InterviewPhoneNumber = dataReader.GetString(dataReader.GetOrdinal("InterviewPhoneNumber"));
          	job.InterviewDirectApplicationDetails = "";
          	job.InterviewOtherInstructions = "";
          	job.ScreeningPreferences = ScreeningPreferences.AllowUnqualifiedApplications;
          	job.WizardStep = 7;
          	job.WizardPath = 1;
          	job.ExternalId = dataReader.GetString(dataReader.GetOrdinal("ExternalId"));
          	//job.Posting = "";




            var request = new SaveJobRequest
            {
              Job = job,
							Module = FocusModules.General,
              JobMigrationId = string.Concat("AJLA:", dataReader.GetString(dataReader.GetOrdinal("ExternalId"))),
							EmployeeMigrationId = string.Concat("AJLA:", dataReader.GetString(dataReader.GetOrdinal("EmployeeMigrationId")))
            };


            if (request.Job.ClosingOn < DateTime.Now || request.Job.ClosedOn.HasValue)
            {
              request.Job.JobStatus = JobStatuses.Closed;
              if (!request.Job.ClosedOn.HasValue)
                request.Job.ClosedOn = request.Job.ClosingOn;
            }

            _lastJobOrderId = dataReader.GetInt64(dataReader.GetOrdinal("Jbiid"));
 
            requests.Add(request);
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateJobOrders SQL Execution exception.", ex);
      }

      return requests;
  	}

		private decimal? LookupHoursPerWeek(int value)
		{
			switch (value)
			{
				case 1:
					return 1;
				case 2:
					return 10;
				case 3:
					return 20;
				case 4:
					return 30;
				case 5:
					return 40;
				case 6:
					return 50;
				default:
					return null;
			}
		}

		/// <summary>
    /// Gets a list of job certificate requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job certificate requests</returns>
    public List<SaveJobCertificatesRequest> MigrateJobCertificates(MigrationProviderArgs providerArgs)
    {
      return new List<SaveJobCertificatesRequest>();
    }

    /// <summary>
    /// Gets a list of job certificate requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job certificate requests</returns>
    public List<SaveJobLanguagesRequest> MigrateJobLanguages(MigrationProviderArgs providerArgs)
    {
      return new List<SaveJobLanguagesRequest>();
    }

    /// <summary>
    /// Gets a list of job licence requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job licence requests</returns>
    public List<SaveJobLicencesRequest> MigrateJobLicences(MigrationProviderArgs providerArgs)
    {
      return new List<SaveJobLicencesRequest>();
    }

    /// <summary>
    /// Gets a list of job special requirements requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job special requirements requests</returns>
    public List<SaveJobSpecialRequirementsRequest> MigrateJobSpecialRequirements(MigrationProviderArgs providerArgs)
  	{
  		return new List<SaveJobSpecialRequirementsRequest>();
  	}

    /// <summary>
    /// Gets a list of job locations requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job location requests</returns>
    public List<SaveJobLocationsRequest> MigrateJobLocations(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobLocationsRequest>();
			//if (_lastJbiidForLocation == 0) _lastJbiidForLocation = 682443;
      try
      {
        using (var db = CreateMsSqlDatabaseAccess(_ajlaConnectionString, false))
        {
          var dbParams = new[]
					{
					  new DatabaseParam("LastJbiid", _lastJbiidForLocation),
            new DatabaseParam("BatchSize", providerArgs.BatchSize)
					};

          var dataReader = db.ExecuteReader("EXEC Migrations_AJLA_JobLocationRequests @LastJbiid, @BatchSize", dbParams);

          while (dataReader.Read())
          {
            var location = new JobLocationDto
            {
              JobId = 0, 
              Location = dataReader.GetString(dataReader.GetOrdinal("TownCity")),
              IsPublicTransitAccessible = true
            };

            var zip = dataReader.GetString(dataReader.GetOrdinal("PostcodeZip"));
						var state = GetLookup(LookupTypes.States, dataReader.GetString(dataReader.GetOrdinal("State")));// MAPPED IN THE SOURCE DATA - A HACK
            location.Location = (state != null)
                                  ? string.Format("{0}, {1} ({2})", location.Location, state.Text, zip)
                                  : string.Format("{0}, ({1})", location.Location, zip);

            var request = new SaveJobLocationsRequest
            {
              JobLocations = new List<JobLocationDto> { location },
              JobLocationMigrationId = string.Concat("AJLA:", dataReader.GetString(dataReader.GetOrdinal("JobMigrationId"))),
							JobMigrationId = string.Concat("AJLA:", dataReader.GetString(dataReader.GetOrdinal("JobMigrationId")))
            };

            _lastJbiidForLocation = dataReader.GetInt64(dataReader.GetOrdinal("Jbiid"));

            requests.Add(request);
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateJobLocations SQL Execution exception.", ex);
      }

      return requests;
    }

    /// <summary>
    /// Gets a list of job locations requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job location requests</returns>
    public List<SaveJobAddressRequest> MigrateJobAddresses(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobAddressRequest>();
			//if (_lastJbiidForAddress == 0) _lastJbiidForAddress = 631751;
      try
      {
        using (var db = CreateMsSqlDatabaseAccess(_ajlaConnectionString, false))
        {
          var dbParams = new[]
					{
					  new DatabaseParam("LastJbiid", _lastJbiidForAddress),
            new DatabaseParam("BatchSize", providerArgs.BatchSize)
					};

          var dataReader = db.ExecuteReader("EXEC Migrations_AJLA_JobAddressRequests @LastJbiid, @BatchSize", dbParams);

          while (dataReader.Read())
          {
            var request = new SaveJobAddressRequest
            {
              JobAddress = new JobAddressDto
				      {
                JobId = 0, 
								Line1 = dataReader.GetString(dataReader.GetOrdinal("Line1")),
                Line2 = dataReader.GetString(dataReader.GetOrdinal("Line2")),
								Line3 = "",
                TownCity = dataReader.GetString(dataReader.GetOrdinal("TownCity")),
								CountyId = null,
                PostcodeZip = dataReader.GetString(dataReader.GetOrdinal("PostcodeZip")),
								StateId = dataReader.GetInt64(dataReader.GetOrdinal("State")),// MAPPED IN THE SOURCE DATA - A HACK
								CountryId = dataReader.GetInt64(dataReader.GetOrdinal("Country")),// MAPPED IN THE SOURCE DATA - A HACK
								IsPrimary = true
              },
							JobAddressMigrationId = string.Concat("AJLA:", dataReader.GetString(dataReader.GetOrdinal("JobMigrationId"))),
							JobMigrationId = string.Concat("AJLA:", dataReader.GetString(dataReader.GetOrdinal("JobMigrationId")))
            };

            _lastJbiidForAddress = dataReader.GetInt64(dataReader.GetOrdinal("Jbiid"));

            requests.Add(request);
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateJobAddresses SQL Execution exception.", ex);
      }

      return requests;
    }

    public List<SaveJobProgramsOfStudyRequest> MigrateJobProgramsOfStudy(MigrationProviderArgs providerArgs)
    {
      return new List<SaveJobProgramsOfStudyRequest>();
    }

	  public List<SaveJobDrivingLicenceEndorsementsRequest> MigrateJobDrivingLicenceEndorsements(MigrationProviderArgs providerArgs)
	  {
		  return new List<SaveJobDrivingLicenceEndorsementsRequest>();
	  }

    #endregion

    /// <summary>
    /// Gets a random number of user requests to set up
    /// </summary>
    /// <returns>A list of new users</returns>
    public List<CreateAssistUserRequest> MigrateUsers(MigrationProviderArgs providerArgs)
    {
      var requests = new List<CreateAssistUserRequest>();

      try
      {
        using (var db = CreateMsSqlDatabaseAccess(_ajlaConnectionString, false))
        {
          var dbParams = new[]
					{
					  new DatabaseParam("LastUsiid", _lastUsiid),
            new DatabaseParam("BatchSize", providerArgs.BatchSize)
					};

          var dataReader = db.ExecuteReader("EXEC Migrations_AJLA_AssistUserRequests @LastUsiid, @BatchSize", dbParams);

          while (dataReader.Read())
          {
            var model = new CreateAssistUserModel
            {
              AccountUserName = dataReader.GetString(dataReader.GetOrdinal("UserName")),
              AccountPassword = dataReader.GetString(dataReader.GetOrdinal("Password")),
              UserPerson = new PersonDto
              {
                //TitleId = PersonalTitleDropDownList.SelectedValueToLong(),
                FirstName = dataReader.GetString(dataReader.GetOrdinal("UserPersonFirstName")),
                MiddleInitial = dataReader.GetString(dataReader.GetOrdinal("UserPersonMiddleInitial")),
                LastName = dataReader.GetString(dataReader.GetOrdinal("UserPersonLastName")),
                JobTitle = dataReader.GetString(dataReader.GetOrdinal("UserPersonJobTitle")),
                EmailAddress = dataReader.GetString(dataReader.GetOrdinal("UserPersonEmailAddress")),
              },
              UserAddress = new PersonAddressDto
              {
                Line1 = dataReader.GetString(dataReader.GetOrdinal("UserAddressLine1")),
                Line2 = dataReader.GetString(dataReader.GetOrdinal("UserAddressLine2")),
                TownCity = dataReader.GetString(dataReader.GetOrdinal("UserAddressTownCity")),
                CountyId = null,
                PostcodeZip = dataReader.GetString(dataReader.GetOrdinal("UserAddressPostcodeZip")),
                StateId = GetLookupId(LookupTypes.States, dataReader.GetString(dataReader.GetOrdinal("UserAddressState")), "OK"),
                CountryId = GetLookupId(LookupTypes.Countries, dataReader.GetString(dataReader.GetOrdinal("UserAddressCountry")), "US"),
                IsPrimary = true
              },
              UserPhone = FormatPhoneNumber(dataReader.GetString(dataReader.GetOrdinal("UserPhone"))),
              UserFax = FormatPhoneNumber(dataReader.GetString(dataReader.GetOrdinal("UserFax"))),
              UserAlternatePhone = FormatPhoneNumber(dataReader.GetString(dataReader.GetOrdinal("UserAlternatePhone"))),
              UserEmailAddress = dataReader.GetString(dataReader.GetOrdinal("UserPersonEmailAddress")),
              IsEnabled = true,
              AccountExternalId = dataReader.GetString(dataReader.GetOrdinal("AccountExternalId"))
            };

            // Focus requires user names to be email addresses
            if (!model.AccountUserName.Contains("@"))
              model.AccountUserName = string.Concat(model.AccountUserName, "@ajila.com");

            if (model.UserEmailAddress.Length == 0)
              model.UserEmailAddress = model.AccountUserName;

            model.AccountPassword = "Password1";
            model.UserExternalOffice = "Undefined";

            var request = new CreateAssistUserRequest
            {
              Models = new List<CreateAssistUserModel> { model },
              UserMigrationId = dataReader.GetString(dataReader.GetOrdinal("UserMigrationId"))
            };

            _lastUsiid = dataReader.GetInt64(dataReader.GetOrdinal("Usiid"));

            requests.Add(request);
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateEmployers SQL Execution exception.", ex);
      }

      return requests;
    }

    #region Helpers

    /*
    /// <summary>
    /// Looks up the ownership type.
    /// </summary>
    /// <param name="lookupCode">The ownership type from AJLA.</param>
    /// <returns>The focus ownership type.</returns>
    private long LookupOwnershipType(string lookupCode)
    {
      string key;

      switch (lookupCode)
      {
        case "1":
          key = "FederalGovernment";
          break;

        case "2":
          key = "StateGovernment";
          break;

        case "3":
          key = "LocalGovernment";
          break;

        case "4":
          key = "ForeignInternational";
          break;

        case "5":
          key = "PrivateCorporation";
          break;

        default:
          key = "PrivateCorporation";
          break;
      }

      return GetLookupId(LookupTypes.OwnershipTypes, key, "");
    }
    */

    /// <summary>
    /// Looks up the education level.
    /// </summary>
    /// <param name="lookupCode">The education code type from AJLA.</param>
    /// <returns>The education level.</returns>
    private EducationLevels LookUpMinimumEducationLevel(string lookupCode)
    {
			if (lookupCode.IsNotNullOrEmpty())
			{
				switch (lookupCode)
				{
					case "3":
						return EducationLevels.HighSchoolDiplomaOrEquivalent;
					case "7":
						return EducationLevels.AssociatesDegree;
					case "11":
						return EducationLevels.BachelorsDegree;
					case "13":
						return EducationLevels.GraduateDegree;
					case "17":
						return EducationLevels.DoctorateDegree;
					//case "1": // here for reference - we ignore these values
					//case "15":
					default:
						return EducationLevels.None;
				}
			}
			return EducationLevels.None;
    }

    /*
    /// <summary>
    /// Looks up the minimum expecience level.
    /// </summary>
    /// <param name="lookupCode">The minimum experience code type from AJLA.</param>
    /// <returns>The minimum expecience level.</returns>
    private int LookUpMinimumExperience(string lookupCode)
    {
      switch (lookupCode)
      {
        case "1": // Less than 1 Year
          return 0;

        case "2": // 1-3 Years
          return 1;

        case "3": // 3-5 Years
          return 3;

        case "4": // 5+ Years
          return 5;

        default:
          return 0;
      }
    }
    */

    /// <summary>
    /// Looks up the Salary Frequency.
    /// </summary>
    /// <param name="lookupCode">The lookup code of the Salary Frequency in AJLA.</param>
    /// <returns>The ID of the Salary Frequency in Focus</returns>
    private long? LookupSalaryFrequency(string lookupCode)
    {
      switch (lookupCode)
      {
        case "H":
          return GetLookupId(LookupTypes.Frequencies, "Hourly", "");

        case "A":
          return GetLookupId(LookupTypes.Frequencies, "Yearly", "");

        default:
          return null;
      }
    }

    /// <summary>
    /// Looks up the Employment Status.
    /// </summary>
    /// <param name="lookupCode">The lookup code of the Employment Status in AJLA.</param>
    /// <returns>The ID of the Employment Status in Focus</returns>
    private long? LookupEmploymentStatus(string lookupCode)
    {
      switch (lookupCode)
      {
        case "F":
          return GetLookupId(LookupTypes.EmploymentStatuses, "Fulltime", "");

        case "P":
          return GetLookupId(LookupTypes.EmploymentStatuses, "Parttime", "");

        default:
          return null;
      }
    }

    /// <summary>
    /// Looks up the Normal Working Shifts.
    /// </summary>
    /// <param name="lookupCode">The lookup code of the Normal Working Shifts in AJLA.</param>
    /// <returns>The ID of the Normal Working Shifts in Focus</returns>
    private long? LookupNormalWorkingShifts(string lookupCode)
    {
			if (lookupCode.IsNotNullOrEmpty())
			{
				switch (lookupCode)
				{
					case "DayShift":
						return GetLookupId(LookupTypes.WorkShifts, "FirstDay", "");

					case "EveningShift":
						return GetLookupId(LookupTypes.WorkShifts, "SecondEvening", "");

					case "NightShift":
						return GetLookupId(LookupTypes.WorkShifts, "ThirdNight", "");

					case "RotatingShift":
						return GetLookupId(LookupTypes.WorkShifts, "Rotating", "");

					case "SplitShift":
						return GetLookupId(LookupTypes.WorkShifts, "Split", "");

					default:
						return GetLookupId(LookupTypes.WorkShifts, "Varies", "");
				}
			}
    	return null;
    }

    /// <summary>
    /// Looks up the Job Type.
    /// </summary>
    /// <param name="lookupCode">The lookup code of the Job Type in AJLA.</param>
    /// <returns>The ID of the Job Type in Focus</returns>
    private long? LookupJobType(string lookupCode)
    {
      switch (lookupCode)
      {
        case "P":
          return GetLookupId(LookupTypes.JobTypes, "Permanent", "");

        case "T":
          return GetLookupId(LookupTypes.JobTypes, "Temporary", "");

        default:
          return null;
      }
    }

    /// <summary>
    /// Looks up the Job Status.
    /// </summary>
    /// <param name="lookupCode">The lookup code of the Job Status in AJLA.</param>
    /// <returns>The ID of the Job Status in Focus</returns>
    private long? LookupJobStatus(string lookupCode)
    {
      switch (lookupCode)
      {
        case "H":
          return GetLookupId(LookupTypes.JobStatuses, "Hourly", "");

        default:
          return GetLookupId(LookupTypes.JobStatuses, "Salaried", "");
      }
    }

    /// <summary>
    /// Gets the insursance benefits.
    /// </summary>
    /// <param name="dataReader">A data reader contain AJLA benefit details.</param>
    /// <returns>The Insurance Benefits.</returns>
    private InsuranceBenefits LookupInsuranceBenefits(System.Data.Common.DbDataReader dataReader)
    {
      var benefits = InsuranceBenefits.None;

      if (dataReader.GetString(dataReader.GetOrdinal("Dental")) != "N") benefits |= InsuranceBenefits.Dental;
      if (dataReader.GetString(dataReader.GetOrdinal("Medical")) != "N") benefits |= InsuranceBenefits.Health;
      if (dataReader.GetString(dataReader.GetOrdinal("LifeInsurance")) != "N") benefits |= InsuranceBenefits.Life;
      if (dataReader.GetString(dataReader.GetOrdinal("Disability")) != "N") benefits |= InsuranceBenefits.Disability;

      return benefits;
    }

    /// <summary>
    /// Gets the insursance benefits.
    /// </summary>
    /// <param name="dataReader">A data reader contain AJLA benefit details.</param>
    /// <returns>The Miscellaneous Benefits.</returns>
    private MiscellaneousBenefits LookupMiscellaneousBenefits(System.Data.Common.DbDataReader dataReader)
    {
      var benefits = MiscellaneousBenefits.None;

      if (dataReader.GetString(dataReader.GetOrdinal("ChildCare")) != "N") benefits |= MiscellaneousBenefits.ChildCare;
      if (dataReader.GetString(dataReader.GetOrdinal("Relocation")) != "N") benefits |= MiscellaneousBenefits.Relocation;
      if (dataReader.GetString(dataReader.GetOrdinal("OtherBenefitsDetails")).Length > 0) benefits |= MiscellaneousBenefits.Other;

      return benefits;
    }

    /// <summary>
    /// Looks up the interview contact preferences.
    /// </summary>
    /// <param name="lookupCode">The code to lookup.</param>
    /// <returns>The interview contact preference.</returns>
    private ContactMethods LookupInterviewContactPreferences(string lookupCode)
    {
			ContactMethods flags = 0;
			if(lookupCode.IsNotNullOrEmpty())
			{
				var values = lookupCode.Split(',');
				
				foreach (var value in values)
				{
					switch (value)
					{
						case "e":
							flags = flags | ContactMethods.Email;
							break;
						case "f":
							flags = flags | ContactMethods.Fax;
							break;
						case "m":
							flags = flags | ContactMethods.Mail;
							break;
						case "p":
							flags = flags | ContactMethods.InPerson;
							break;
						case "c":
							flags = flags | ContactMethods.Telephone;
							break;
						default:
							flags = flags | ContactMethods.FocusTalent;
							break;
					} 
				}
			}
			return flags;
    }

    #endregion

    /*
		#region Migrate Talent Users

		public List<ImportTalentUserFromClientRequest> MigrateTalentUser(int providerArgs.Batchsize)
		{
			var requests = new List<ImportTalentUserFromClientRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(_ajlaConnectionString, false))
				{
					var dbParams = new[]
			      {
			        new DatabaseParam("LastUsiid", _lastEmployerId),
			        new DatabaseParam("BatchSize", providerArgs.Batchsize)
			      };

					var dataReader = db.ExecuteReader("EXEC Migrations_AJLA_TalentUserRequests @LastUsiid, @BatchSize", dbParams);

					while (dataReader.Read())
					{
						var usiid = dataReader.GetInt64(dataReader.GetOrdinal("Usiid"));
						var request = new ImportTalentUserFromClientRequest
						{
							UserId = usiid.ToString(),
							MigrationId = string.Concat("AJLA:", usiid)
						};
						
						_lastEmployerId = usiid;

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateTalentUsers SQL Execution exception.", ex);
			}

			return requests;
		}

		#endregion
    */

    /*
		#region Migratte Assist User

		public List<ImportAssistUserFromClientRequest> MigrateAssistUser(int providerArgs.Batchsize)
		{
			var requests = new List<ImportAssistUserFromClientRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(_ajlaConnectionString, false))
				{
					var dbParams = new[]
			      {
			        new DatabaseParam("LastUsiid", _lastEmployerId),
			        new DatabaseParam("BatchSize", providerArgs.Batchsize)
			      };

					var dataReader = db.ExecuteReader("EXEC Migrations_AJLA_AssistUserRequests @LastUsiid, @BatchSize", dbParams);

					while (dataReader.Read())
					{
						var usiid = dataReader.GetInt64(dataReader.GetOrdinal("Usiid"));
						var request = new ImportAssistUserFromClientRequest
						{
							UserId = usiid.ToString()
						};

						_lastEmployerId = usiid;

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateAssistUsers SQL Execution exception.", ex);
			}

			return requests;
		}

		#endregion
    */


		public List<SaveSpideredJobRequest> MigrateSpideredJobs(MigrationProviderArgs providerArgs)
		{
			//throw new NotImplementedException();
			return null;
		}
		
		public List<SaveSavedJobRequest> MigrateSavedJobs(MigrationProviderArgs providerArgs)
		{
			//throw new NotImplementedException();
			return null;
		}
		
		List<Core.Messages.CandidateService.ReferCandidateRequest> IJobOrder.MigrateSpideredJobReferrals(MigrationProviderArgs providerArgs)
		{
			//throw new NotImplementedException();
			return null;
		}
	}
}
