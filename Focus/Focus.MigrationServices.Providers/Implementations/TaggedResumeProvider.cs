﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml.Xsl;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.AccountService;
using Focus.Core.Messages.AnnotationService;
using Focus.Core.Messages.CandidateService;
using Focus.Core.Messages.PostingService;
using Focus.Core.Messages.ResumeService;
using Focus.Core.Models.Career;
using Focus.Data.Library.Entities;
using Focus.MigrationServices.Providers.Common;
using Focus.MigrationServices.Providers.Interfaces;
using Focus.Services.Mappers;
using Framework.Core;

#endregion

namespace Focus.MigrationServices.Providers.Implementations
{
  public class TaggedResumeProvider : MigrationProviderBase, IMigrationProvider, IJobSeeker, IResume
  {
    private readonly Random _randomNumber;

    private string _lastJobSeekerFileName;
    private string _lastResumeFileName;
    private string _lastResumeDocumentFileName;

    private List<ProgramAreaDegreeEducationLevelView> _degrees;
    private List<SchoolStatus> _enrollmentStatuses;
    private List<EmploymentStatus> _employmentStatuses;
    private List<EducationLevel> _educationLevelsHigh;
    private List<EducationLevel> _educationLevelsLow;
    private List<long> _militaryDischargeTypes;
    private List<VeteranDisabilityStatus> _disabilityStatuses;
    private List<long> _branchesOfService;
    private ILookup<long, long> _serviceRanks; 

    private int _lastDegreeIndex;
    private int _lastEnrolmentStatusIndex;
    private int _lastEmploymentStatusIndex;
    private int _lastEducationLevelHighIndex;
    private int _lastEducationLevelLowIndex;
    private int _lastMilitaryDischargeTypeIndex;
    private int _lastDisabilityStatusIndex;
    private int _lastBranchOfServiceIndex;
    private Dictionary<long, int> _lastServiceRankIndexes;

    private int _degreeCount;
    private int _emailCount;

    public TaggedResumeProvider()
    {
      _randomNumber = new Random();
      _lastJobSeekerFileName = _lastResumeFileName = _lastResumeDocumentFileName = string.Empty;
    }

    public void Initialise(RecordType recordType, long lastId)
    {
      _degrees = GetDegrees();
      _degreeCount = _degrees.Count;
      _lastDegreeIndex = -1;

      if (AppSettings.Theme == FocusThemes.Education)
      {
        _enrollmentStatuses = new List<SchoolStatus>
        {
          SchoolStatus.Prospective,
          SchoolStatus.FirstYear,
          SchoolStatus.SophomoreOrAbove,
          SchoolStatus.NonCreditOther,
          SchoolStatus.Alumni
        };
      }
      else
      {
        _enrollmentStatuses = new List<SchoolStatus>
        {
          SchoolStatus.Not_Attending_School_HS_Graduate,
          SchoolStatus.Not_Attending_School_OR_HS_Dropout,
          SchoolStatus.In_School_Post_HS,
          SchoolStatus.In_School_Alternative_School,
          SchoolStatus.In_School_HS_OR_less
        };
      }
      _lastEnrolmentStatusIndex = -1;

      _educationLevelsHigh = new List<EducationLevel>
      {
        EducationLevel.Doctorate_Degree_26,
        EducationLevel.Masters_Degree_25,
        EducationLevel.Bachelors_OR_Equivalent_24,
        EducationLevel.HS_3_Year_Associates_Degree_23,
        EducationLevel.HS_2_Year_Associates_Degree_22,
        EducationLevel.HS_1_Year_Associates_Degree_21,
        EducationLevel.HS_3_Year_Vocational_Degree_20,
        EducationLevel.HS_2_Year_Vocational_Degree_19,
        EducationLevel.HS_1_Year_Vocational_Degree_18,
        EducationLevel.HS_3_Year_College_OR_VOC_Tech_No_Degree_17,
        EducationLevel.HS_2_Year_College_OR_VOC_Tech_No_Degree_16,
        EducationLevel.HS_1_Year_College_OR_VOC_Tech_No_Degree_15,
        EducationLevel.Disabled_w_Cert_IEP_14,
        EducationLevel.GED_88,
        EducationLevel.Grade_12_HS_Graduate_13
      };
      _lastEducationLevelHighIndex = -1;

      _educationLevelsLow = new List<EducationLevel>
      {
        EducationLevel.Grade_12_No_Diploma_12,
        EducationLevel.Grade_11_11,
        EducationLevel.Grade_10_10,
        EducationLevel.Grade_9_09,
        EducationLevel.Grade_8_08,
        EducationLevel.Grade_7_07,
        EducationLevel.Grade_6_06,
        EducationLevel.Grade_5_05,
        EducationLevel.Grade_4_04,
        EducationLevel.Grade_3_03,
        EducationLevel.Grade_2_02,
        EducationLevel.Grade_1_01,
        EducationLevel.No_Grade_00
      };
      _lastEducationLevelLowIndex = -1;

      _employmentStatuses = new List<EmploymentStatus>
      {
        EmploymentStatus.Employed,
        EmploymentStatus.EmployedTerminationReceived,
        EmploymentStatus.UnEmployed
      };
      _lastEmploymentStatusIndex = -1;

      _militaryDischargeTypes = GetMilitaryDischargeTypes();
      _lastMilitaryDischargeTypeIndex = -1;

      _disabilityStatuses = new List<VeteranDisabilityStatus>
      {
        VeteranDisabilityStatus.NotDisabled,
        VeteranDisabilityStatus.Disabled,
        VeteranDisabilityStatus.SpecialDisabled
      };
      _lastDisabilityStatusIndex = -1;

      _branchesOfService = GetBranchOfServiceTypes();
      _lastBranchOfServiceIndex = -1;

      _serviceRanks = GetServiceRanks();
      _lastServiceRankIndexes = _branchesOfService.ToDictionary(branch => branch, branch => -1);
    }

    #region Job Seekers

    /// <summary>
    /// Gets a list of job seeker requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job seekers requests</returns>
    public List<RegisterCareerUserRequest> MigrateJobSeekers(MigrationProviderArgs providerArgs)
    {
      var requests = new List<RegisterCareerUserRequest>();

      try
      {
        var folderInfo = new DirectoryInfo(MigrationFolderName);

        var files = folderInfo.GetFiles("*.tag")
                              .Where(f => string.Compare(f.Name, _lastJobSeekerFileName, StringComparison.OrdinalIgnoreCase) > 0)
                              .Take(providerArgs.BatchSize);

        var defaultContryId = GetLookupId(LookupTypes.Countries, "United States", "");

        foreach (var fileInfo in files)
        {
          var resumeXml = XDocument.Load(fileInfo.FullName);

          _emailCount++;
          if (_emailCount == 1000)
            _emailCount = 1;

          var email = SpoofData
            ? string.Format("Test{0}{1}@burning-glass.com", DateTime.Now.ToString("yyMMddHHmmssfff"), _emailCount.ToString("000"))
            : XPathGetElementValue(resumeXml, "//contact/email");

          var nameElement = resumeXml.XPathSelectElement("//contact/name");

          var request = new RegisterCareerUserRequest
          {
            AccountUserName = email,
            AccountPassword = SpoofData ? "Password1" : GenerateRandomPassword(),
            EmailAddress = email,
            SocialSecurityNumber = _randomNumber.Next(1, 999999999).ToString("000000000"),
            FirstName = nameElement == null ? string.Empty : XPathGetElementValue(nameElement, "givenname"),
            MiddleInitial = string.Empty,
            LastName = nameElement == null ? string.Empty : XPathGetElementValue(nameElement, "surname"),
            SecurityQuestion = "What is your email",
            SecurityAnswer = email,
            JobSeekerMigrationId = fileInfo.Name
          };

          if (string.IsNullOrEmpty(request.FirstName))
            request.FirstName = "Unknown";

          if (string.IsNullOrEmpty(request.LastName))
            request.LastName = "Unknown";

          var addressElement = resumeXml.XPathSelectElement("//contact/address");
          if (addressElement != null)
          {
            var state = XPathGetAttributeValue(addressElement, "state");
            request.PostalAddress = new Address
            {
              Street1 = XPathGetElementValue(addressElement, "street"),
              Street2 = "",
              City = XPathGetElementValue(addressElement, "city"),
              StateId = string.IsNullOrEmpty(state) ? (long?) null : GetLookupId(LookupTypes.States, state, "ZZ"),
              CountryId = defaultContryId,
              CountyId = null,
              Zip = XPathGetElementValue(addressElement, "postalcode")
            };

            if (string.IsNullOrEmpty(request.PostalAddress.Street1))
              request.PostalAddress.Street1 = "Not specified";
          }
          else
          {
            request.PostalAddress = new Address
            {
              Street1 = "Not specified",
              Street2 = "",
              City = "",
              StateId = null,
              CountryId = defaultContryId,
              CountyId = null,
              Zip = ""
            };
          }

          if (AppSettings.Theme == FocusThemes.Education && SpoofData)
          {
            var nextDegree = GetNextDegree();
            if (nextDegree != null)
            {
              request.ProgramOfStudyId = nextDegree.ProgramAreaId;
              request.DegreeId = nextDegree.DegreeEducationLevelId;
            }

            request.EnrollmentStatus = GetNextEnrollmentStatus();
          }

          var ignoreResume = false;
          if (SpoofData)
          {
            if (request.FirstName.Equals(request.FirstName.ToUpper()) && request.LastName.Equals(request.LastName.ToUpper()))
              ignoreResume = true;
          }

          if (!ignoreResume)
            requests.Add(request);

          _lastJobSeekerFileName = fileInfo.Name;
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateJobSeekers exception.", ex);
      }

      return requests;
    }

    /// <summary>
    /// Gets a list of viewed postings for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of saved search requests</returns>
    public List<AddViewedPostingRequest> MigrateViewedPostings(MigrationProviderArgs providerArgs)
    {
      return new List<AddViewedPostingRequest>();
    }

    /// <summary>
    /// Gets a list of saved searches for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of saved search requests</returns>
    public List<SaveSearchRequest> MigrateJobSeekerSavedSearches(MigrationProviderArgs providerArgs)
    {
      return new List<SaveSearchRequest>();
    }

    /// <summary>
    /// Gets a list of search alerts for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of search alert requests</returns>
    public List<SaveSearchRequest> MigrateJobSeekerSearchAlerts(MigrationProviderArgs providerArgs)
    {
      return new List<SaveSearchRequest>();
    }

    /// <summary>
    /// Gets a list of referrals for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of referral requests</returns>
    public List<ReferCandidateRequest> MigrateReferrals(MigrationProviderArgs providerArgs)
    {
      return new List<ReferCandidateRequest>();
    }

    /// <summary>
    /// Gets a list of activities for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of activities</returns>
		public List<AssignActivityOrActionRequest> MigrateJobSeekerActivities(MigrationProviderArgs providerArgs)
    {
      return new List<AssignActivityOrActionRequest>();
    }

    /// <summary>
    /// Gets a list of flags for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of flags</returns>
    public List<ToggleCandidateFlagRequest> MigrateFlaggedJobSeekers(MigrationProviderArgs providerArgs)
    {
      return new List<ToggleCandidateFlagRequest>();
    }

    /// <summary>
    /// Gets a list of resume requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new resume requests</returns>
    public List<SaveResumeRequest> MigrateResumes(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveResumeRequest>();

      var veteransToSpoof = providerArgs.CustomParm.IsNullOrEmpty() ? 3 : int.Parse(providerArgs.CustomParm);

      try
      {
        var folderInfo = new DirectoryInfo(MigrationFolderName);

        var files = folderInfo.GetFiles("*.tag")
                              .Where(f => string.Compare(f.Name, _lastResumeFileName, StringComparison.OrdinalIgnoreCase) > 0)
                              .Take(providerArgs.BatchSize);
        var fileCount = 0;
        var defaultWorkWeekId = GetLookupId(LookupTypes.WorkWeeks, "Any");

        foreach (var fileInfo in files)
        {
          var taggedResume = File.ReadAllText(fileInfo.FullName);
          var resume = taggedResume.ToResumeModel(RuntimeContext, "**-**");

          resume.ResumeMetaInfo.ResumeCreationMethod = ResumeCreationMethod.Upload;

          if (resume.Special == null)
            resume.Special = new ResumeSpecialInfo();

          if (resume.Special.Preferences == null)
            resume.Special.Preferences = new Preferences();

          if (resume.Special.Preferences.ShiftDetail == null)
            resume.Special.Preferences.ShiftDetail = new Shift();

          resume.Special.Preferences.IsResumeSearchable = true;

          if (resume.ResumeContent.IsNull())
            resume.ResumeContent = new ResumeBody();

          if (SpoofData)
          {
            if (AppSettings.Theme == FocusThemes.Education)
            {
              switch (fileCount % 3)
              {
                case 0:
                  resume.Special.Preferences.PostingsInterestedIn = JobTypes.Job;
                  resume.Special.Preferences.ShiftDetail.WorkWeekId = defaultWorkWeekId;
                  break;

                case 1:
                  resume.Special.Preferences.PostingsInterestedIn = JobTypes.InternshipPaid | JobTypes.InternshipPaid;
                  break;

                default:
                  resume.Special.Preferences.PostingsInterestedIn = JobTypes.Job | JobTypes.InternshipPaid | JobTypes.InternshipPaid;
                  resume.Special.Preferences.ShiftDetail.WorkWeekId = defaultWorkWeekId;
                  break;
              }
            }
            else
            {
              resume.Special.Preferences.ShiftDetail.WorkWeekId = defaultWorkWeekId;
            }
          }

          if (resume.ResumeContent.Profile != null)
          {
            if (resume.ResumeContent.Profile.UserGivenName != null)
            {
              var middleName = resume.ResumeContent.Profile.UserGivenName.MiddleName;
              if (middleName != null && middleName.Length > 5)
                resume.ResumeContent.Profile.UserGivenName.MiddleName = middleName.Substring(0, 5);

              var firstName = resume.ResumeContent.Profile.UserGivenName.FirstName;
              if (string.IsNullOrEmpty(firstName))
                resume.ResumeContent.Profile.UserGivenName.FirstName = "Unknown";

              var lastName = resume.ResumeContent.Profile.UserGivenName.LastName;
              if (string.IsNullOrEmpty(lastName))
                resume.ResumeContent.Profile.UserGivenName.LastName = "Unknown";
            }

            if (SpoofData)
            {
              var phone = resume.ResumeContent.Profile.PrimaryPhone;
              if (phone.IsNotNull())
              {
                if (phone.PhoneNumber.IsNotNull() && Regex.Replace(phone.PhoneNumber, @"[^0-9]", string.Empty).Length > 10)
                  phone.PhoneNumber = phone.PhoneNumber.Substring(0, 10);

                phone.PhoneType = PhoneType.Home;
              }
            }
          }

          if (resume.ResumeContent.SeekerContactDetails != null)
          {
            if (resume.ResumeContent.SeekerContactDetails.SeekerName != null)
            {
              var middleName = resume.ResumeContent.SeekerContactDetails.SeekerName.MiddleName;
              if (middleName != null && middleName.Length > 5)
                resume.ResumeContent.SeekerContactDetails.SeekerName.MiddleName = middleName.Substring(0, 5);

              var firstName = resume.ResumeContent.SeekerContactDetails.SeekerName.FirstName;
              if (string.IsNullOrEmpty(firstName))
                resume.ResumeContent.SeekerContactDetails.SeekerName.FirstName = "Unknown";

              var lastName = resume.ResumeContent.SeekerContactDetails.SeekerName.LastName;
              if (string.IsNullOrEmpty(lastName))
                resume.ResumeContent.SeekerContactDetails.SeekerName.LastName = "Unknown";
            }

            if (SpoofData)
            {
              var phones = resume.ResumeContent.SeekerContactDetails.PhoneNumber;
              var phoneIndex = 0;
              if (phones.IsNotNull())
              {
                phones.ForEach(pn =>
                {
                  if (pn.PhoneNumber.IsNotNull() && Regex.Replace(pn.PhoneNumber, @"[^0-9]", string.Empty).Length > 10)
                    pn.PhoneNumber = pn.PhoneNumber.Substring(0, 10);

                  pn.PhoneType = phoneIndex == 0 ? PhoneType.Home : PhoneType.NonUS;
                  phoneIndex++;
                });
              }

              var address = resume.ResumeContent.SeekerContactDetails.PostalAddress;
              if (address.Zip.IsNotNullOrEmpty() && (address.CountyId.IsNull() || address.StateId.IsNull()))
              {
                var postalCode = LibraryRepository.Query<PostalCode>().FirstOrDefault(pc => pc.Code == address.Zip);
                if (postalCode.IsNotNull())
                {
                  if (address.StateId.IsNull() && postalCode.StateKey.IsNotNullOrEmpty())
                  {
                    var stateKey = postalCode.StateKey.Substring(postalCode.StateKey.IndexOf(".", StringComparison.InvariantCulture) + 1).ToLowerInvariant();
                    var stateLookup = GetLookup(LookupTypes.States, stateKey);
                    if (stateLookup.IsNotNull())
                    {
                      address.StateId = stateLookup.Id;
                      address.StateName = stateLookup.Text;
                    }
                  }

                  if (address.CountyId.IsNull() && postalCode.CountyKey.IsNotNullOrEmpty() && address.StateId.IsNotNull())
                  {
                    var countyKey = postalCode.CountyKey.Substring(postalCode.CountyKey.IndexOf(".", StringComparison.InvariantCulture) + 1).ToLowerInvariant();
                    var countyLookup = GetLookup(LookupTypes.Counties, countyKey);
                    if (countyLookup.IsNotNull())
                    {
                      address.CountyId = countyLookup.Id;
                      address.CountyName = countyLookup.Text;
                    }
                    else if (SpoofData && address.StateId.IsNotNull())
                    {
                      var firstCounty = ConfigurationRepository.LookupItemsViews.FirstOrDefault(lookup => lookup.LookupType == "states" && lookup.ParentId == address.StateId);
                      if (firstCounty.IsNotNull())
                      {
                        address.CountyId = firstCounty.Id;
                        address.CountyName = firstCounty.Value;
                      }
                    }
                  }
                }
              }
            }
          }

          if (SpoofData && AppSettings.Theme == FocusThemes.Workforce)
          {
            if (resume.ResumeContent.EducationInfo.IsNull())
              resume.ResumeContent.EducationInfo = new EducationInfo();

            var hasRandomisedSchoolStatus = false;
            if (resume.ResumeContent.EducationInfo.SchoolStatus.IsNull() || resume.ResumeContent.EducationInfo.SchoolStatus == SchoolStatus.NA)
            {
              hasRandomisedSchoolStatus = true;
              resume.ResumeContent.EducationInfo.SchoolStatus = GetNextEnrollmentStatus();
            }

            if (hasRandomisedSchoolStatus || resume.ResumeContent.EducationInfo.EducationLevel.IsNull())
            {
              resume.ResumeContent.EducationInfo.EducationLevel = resume.ResumeContent.EducationInfo.SchoolStatus.IsIn(SchoolStatus.Not_Attending_School_HS_Graduate, SchoolStatus.In_School_Post_HS) 
                ? GetNextEducationLevelHigh() 
                : GetNextEducationLevelLow();
            }

            var schools = resume.ResumeContent.EducationInfo.Schools;
            if (schools.IsNotNullOrEmpty())
            {
              schools.ForEach(school =>
              {
                if (school.Degree.IsNullOrEmpty())
                  school.Degree = "Bachelor of Arts (AB or BA)";

                if (school.Major.IsNullOrEmpty())
                  school.Major = "Psychology";

                if (school.Institution.IsNullOrEmpty())
                  school.Institution = "Yale";

                if (school.CompletionDate.IsNull())
                  school.CompletionDate = new DateTime(2000, 06, 01);
              });
            }

            if (resume.ResumeContent.Profile.IsNull())
              resume.ResumeContent.Profile = new UserProfile();

            if (resume.ResumeContent.Profile.License.IsNull())
              resume.ResumeContent.Profile.License = new LicenseInfo {HasDriverLicense = true};

            var licence = resume.ResumeContent.Profile.License;
            if (licence.HasDriverLicense.GetValueOrDefault(false))
            {
              if (licence.DriverStateId.IsNull())
              {
                if (resume.ResumeContent.SeekerContactDetails.IsNull() || resume.ResumeContent.SeekerContactDetails.PostalAddress.IsNull())
                {
                  licence.DriverStateId = GetLookupId(LookupTypes.States, "TX");
                 licence.DriverStateName = "Texas";
                }
                else
                {
                  licence.DriverStateId = resume.ResumeContent.SeekerContactDetails.PostalAddress.StateId;
                  licence.DriverStateName = resume.ResumeContent.SeekerContactDetails.PostalAddress.StateName;
                }
              }

              if (licence.DrivingLicenceClassId.IsNull())
                licence.DrivingLicenceClassId = GetLookupId(LookupTypes.DrivingLicenceClasses, "ClassD");
            }

            if (resume.ResumeContent.Profile.DOB.IsNull())
              resume.ResumeContent.Profile.DOB = new DateTime(1960, 1, 1);

            if (resume.ResumeContent.ExperienceInfo.IsNull())
              resume.ResumeContent.ExperienceInfo = new ExperienceInfo();

            if (resume.ResumeContent.ExperienceInfo.EmploymentStatus.IsNull() || resume.ResumeContent.ExperienceInfo.EmploymentStatus == EmploymentStatus.None)
              resume.ResumeContent.ExperienceInfo.EmploymentStatus = SpoofData ? GetNextEmploymentStatus() : EmploymentStatus.UnEmployed;
            
            if (resume.ResumeContent.Profile.Sex.IsNull())
              resume.ResumeContent.Profile.Sex = Genders.NotDisclosed;

            if (resume.ResumeContent.Profile.EthnicHeritage.IsNull() || resume.ResumeContent.Profile.EthnicHeritage.EthnicHeritageId.IsNull())
            {
              resume.ResumeContent.Profile.EthnicHeritage = new EthnicHeritage
              {
                EthnicHeritageId = GetLookupId(LookupTypes.EthnicHeritages, "NotDisclosed"),
                RaceIds = new List<long?> { GetLookupId(LookupTypes.Races, "NotDisclosed") }
              };
            }

            if (resume.ResumeContent.Profile.IsUSCitizen.IsNull())
              resume.ResumeContent.Profile.IsUSCitizen = true;

            if (resume.ResumeContent.Profile.DisabilityStatus.IsNull())
              resume.ResumeContent.Profile.DisabilityStatus = DisabilityStatus.NotDisclosed;

            if (resume.Special.Preferences.Salary.IsNull())
              resume.Special.Preferences.Salary = 100000;

            if (resume.Special.Preferences.SalaryFrequencyId.IsNull())
              resume.Special.Preferences.SalaryFrequencyId = GetLookupId(LookupTypes.Frequencies, "Yearly");

            if (resume.Special.Preferences.ShiftDetail.WorkOverTime.IsNull())
              resume.Special.Preferences.ShiftDetail.WorkOverTime = true;

            if (resume.Special.Preferences.WillingToRelocate.IsNull())
              resume.Special.Preferences.WillingToRelocate = true;

            if (resume.Special.Preferences.ShiftDetail.WorkDurationId.IsNull())
              resume.Special.Preferences.ShiftDetail.WorkDurationId = GetLookupId(LookupTypes.Durations, "FullTimeRegular");

            if (resume.Special.Preferences.ShiftDetail.ShiftTypeIds.IsNullOrEmpty())
            {
              resume.Special.Preferences.ShiftDetail.ShiftTypeIds = new List<long>
              {
                GetLookupId(LookupTypes.WorkShifts, "FirstDay").GetValueOrDefault(0),
                GetLookupId(LookupTypes.WorkShifts, "SecondEvening").GetValueOrDefault(0),
                GetLookupId(LookupTypes.WorkShifts, "ThirdNight").GetValueOrDefault(0),
                GetLookupId(LookupTypes.WorkShifts, "Rotating").GetValueOrDefault(0),
                GetLookupId(LookupTypes.WorkShifts, "Split").GetValueOrDefault(0),
                GetLookupId(LookupTypes.WorkShifts, "Varies").GetValueOrDefault(0)
              };
            }

            var veteran = resume.ResumeContent.Profile.Veteran;
            if (veteran.IsNull())
            {
              resume.ResumeContent.Profile.Veteran = new VeteranInfo {IsVeteran = false};
              veteran = resume.ResumeContent.Profile.Veteran;
            }

            if (fileCount % 10 < veteransToSpoof && (bool)!veteran.IsVeteran)
            {
              veteran.IsVeteran = true;

              var branchOfServiceId = GetNextBranchOfServiceTypeId();
              var history = new VeteranHistory
              {
                VeteranEra = VeteranEra.OtherVet,
                VeteranDisabilityStatus = GetNextDisabilityStatus(),
                IsCampaignVeteran = true,
                VeteranStartDate = new DateTime(2000, 1, 1),
                VeteranEndDate = new DateTime(2002, 1, 1),
                MilitaryDischargeId = GetNextMilitaryDischargeTypeId(),
                MilitaryBranchOfServiceId = branchOfServiceId,
                RankId = GetNextServiceRankId(branchOfServiceId)
              };
              veteran.History = new List<VeteranHistory>{ history };
            }
          }

          var request = new SaveResumeRequest
          {
            SeekerResume = resume,
            ResumeMigrationId = fileInfo.Name,
            JobSeekerMigrationId = fileInfo.Name
          };

          requests.Add(request);

          _lastResumeFileName = fileInfo.Name;
          fileCount++;
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateResumes exception.", ex);
      }

      return requests;
    }

    /// <summary>
    /// Gets a list of resume document requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new resume document requests</returns>
    public List<SaveResumeDocumentRequest> MigrateResumeDocuments(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveResumeDocumentRequest>();

      try
      {
        var folderInfo = new DirectoryInfo(MigrationFolderName);

        var files = folderInfo.GetFiles("*.tag")
                              .Where(
                                f =>
                                string.Compare(f.Name, _lastResumeDocumentFileName, StringComparison.OrdinalIgnoreCase) >
                                0)
                              .Take(providerArgs.BatchSize);

        var assembly = Assembly.GetExecutingAssembly();
        var stream = assembly.GetManifestResourceStream("Focus.MigrationServices.Providers.Assets.TaggedResume.xslt");
        if (stream == null)
          throw new Exception("Unable to get TaggedResume xslt");

        var reader = new StreamReader(stream);
        var xmlReader = XmlReader.Create(reader);

        var xslt = new XslCompiledTransform();
        xslt.Load(xmlReader);

        xmlReader.Close();
        reader.Close();
        stream.Close();

        foreach (var fileInfo in files)
        {
          var outputString = new StringWriter();
          var output = new XmlTextWriter(outputString);
          xslt.Transform(fileInfo.FullName, output);
          output.Close();
          outputString.Close();

          var resumeDocument = new ResumeDocumentDto
          {
            FileName = fileInfo.Name.Replace(".tag", ".html"),
            ContentType = "text/html",
            DocumentBytes = outputString.Encoding.GetBytes(outputString.ToString()),
            Html = outputString.ToString()
          };

          var request = new SaveResumeDocumentRequest
          {
            ResumeDocument = resumeDocument,
            ResumeMigrationId = fileInfo.Name,
            JobSeekerMigrationId = fileInfo.Name
          };

          requests.Add(request);

          _lastResumeDocumentFileName = fileInfo.Name;
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateResumeDocuments exception.", ex);
      }

      return requests;
    }

    #endregion

    #region Helpers

    /// <summary>
    /// Generates a random password of letters and numbers
    /// </summary>
    /// <returns>The password</returns>
    private string GenerateRandomPassword()
    {
      string password;

      do
      {
        password = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20);
      } while (!Regex.IsMatch(password, AppSettings.PasswordRegExPattern));

      return password;
    }


    /// <summary>
    /// Gets all degrees from the database (Education only)
    /// </summary>
    /// <returns></returns>
    private List<ProgramAreaDegreeEducationLevelView> GetDegrees()
    {
      var query = LibraryRepository.Query<ProgramAreaDegreeEducationLevelView>();

      query = AppSettings.ExplorerDegreeFilteringType == DegreeFilteringType.Ours 
        ? query.Where(x => !x.IsClientData) 
        : query.Where(x => x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag);

      return query.ToList();
    }

    /// <summary>
    /// Cycles through all possible degrees and returns the next in the list
    /// </summary>
    /// <returns>The next degree in the list</returns>
    private ProgramAreaDegreeEducationLevelView GetNextDegree()
    {
      if (_degreeCount == 0)
        return null;

      _lastDegreeIndex++;
      if (_lastDegreeIndex >= _degreeCount)
        _lastDegreeIndex = 0;

      return _degrees[_lastDegreeIndex];
    }

    /// <summary>
    /// Cycles through all possible enrollment statuses and returns the next in the list
    /// </summary>
    /// <returns>The next  enrollment status in the list</returns>
    private SchoolStatus GetNextEnrollmentStatus()
    {
      _lastEnrolmentStatusIndex++;
      if (_lastEnrolmentStatusIndex >= _enrollmentStatuses.Count)
        _lastEnrolmentStatusIndex = 0;

      return _enrollmentStatuses[_lastEnrolmentStatusIndex];
    }

    /// <summary>
    /// Cycles through all possible education levels (for HS and above) and returns the next in the list
    /// </summary>
    /// <returns>The next education level in the list</returns>
    private EducationLevel GetNextEducationLevelHigh()
    {
      _lastEducationLevelHighIndex++;
      if (_lastEducationLevelHighIndex >= _educationLevelsHigh.Count)
        _lastEducationLevelHighIndex = 0;

      return _educationLevelsHigh[_lastEducationLevelHighIndex];
    }

    /// <summary>
    /// Cycles through all possible education levels (for below HS) and returns the next in the list
    /// </summary>
    /// <returns>The next education level in the list</returns>
    private EducationLevel GetNextEducationLevelLow()
    {
      _lastEducationLevelLowIndex++;
      if (_lastEducationLevelLowIndex >= _educationLevelsLow.Count)
        _lastEducationLevelLowIndex = 0;

      return _educationLevelsLow[_lastEducationLevelLowIndex];
    }

    /// <summary>
    /// Cycles through all possible Employment statuses and returns the next in the list
    /// </summary>
    /// <returns>The next  Employment status in the list</returns>
    private EmploymentStatus GetNextEmploymentStatus()
    {
      _lastEmploymentStatusIndex++;
      if (_lastEmploymentStatusIndex >= _employmentStatuses.Count)
        _lastEmploymentStatusIndex = 0;

      return _employmentStatuses[_lastEmploymentStatusIndex];
    }

    /// <summary>
    /// Gets all possible military discharge types
    /// </summary>
    /// <returns>A list of military discharge reasons</returns>
    private List<long> GetMilitaryDischargeTypes()
    {
     return ConfigurationRepository.LookupItemsViews.Where(lookup => lookup.LookupType == "MilitaryDischargeTypes")
                                                    .Select(lookup => lookup.Id)
                                                    .ToList();
    }

    /// <summary>
    /// Gets the next military discharge type id
    /// </summary>
    /// <returns>The military discharge type id</returns>
    private long GetNextMilitaryDischargeTypeId()
    {
      _lastMilitaryDischargeTypeIndex++;
      if (_lastMilitaryDischargeTypeIndex >= _militaryDischargeTypes.Count)
        _lastMilitaryDischargeTypeIndex = 0;

      return _militaryDischargeTypes[_lastMilitaryDischargeTypeIndex];
    }

    /// <summary>
    /// Cycles through all possible disability statuses and returns the next in the list
    /// </summary>
    /// <returns>The next disability status in the list</returns>
    private VeteranDisabilityStatus GetNextDisabilityStatus()
    {
      _lastDisabilityStatusIndex++;
      if (_lastDisabilityStatusIndex >= _disabilityStatuses.Count)
        _lastDisabilityStatusIndex = 0;

      return _disabilityStatuses[_lastDisabilityStatusIndex];
    }

    /// <summary>
    /// Gets all possible military branch of service
    /// </summary>
    /// <returns>A list of military branches of service</returns>
    private List<long> GetBranchOfServiceTypes()
    {
      return ConfigurationRepository.LookupItemsViews.Where(lookup => lookup.LookupType == "MilitaryBranchesOfService")
                                                     .Select(lookup => lookup.Id)
                                                     .ToList();
    }

    /// <summary>
    /// Gets the next military branch of service
    /// </summary>
    /// <returns>The military branch of service</returns>
    private long GetNextBranchOfServiceTypeId()
    {
      _lastBranchOfServiceIndex++;
      if (_lastBranchOfServiceIndex >= _branchesOfService.Count)
        _lastBranchOfServiceIndex = 0;

      return _branchesOfService[_lastBranchOfServiceIndex];
    }

    /// <summary>
    /// Gets all possible military service ranks
    /// </summary>
    /// <returns>A lookup of military service ranks (lookup by branch id)</returns>
    private ILookup<long, long> GetServiceRanks()
    {
      return ConfigurationRepository.LookupItemsViews.Where(lookup => lookup.LookupType == "MilitaryRanks")
                                    .ToLookup(lookup => lookup.ParentId, lookup => lookup.Id);
    }

    /// <summary>
    /// Gets the next military rank
    /// </summary>
    /// <returns>The military rank</returns>
    private long? GetNextServiceRankId(long? branchOfServiceId)
    {
      if (branchOfServiceId == null)
        return null;

      var lastServiceRankIndex = _lastServiceRankIndexes[branchOfServiceId.Value];
      var serviceRanks = _serviceRanks[branchOfServiceId.Value].ToList();

      lastServiceRankIndex++;
      if (lastServiceRankIndex >= serviceRanks.Count())
        lastServiceRankIndex = 0;

      _lastServiceRankIndexes[branchOfServiceId.Value] = lastServiceRankIndex;

      return serviceRanks[lastServiceRankIndex];
    }

    /// <summary>
    /// Gets the value of an element relative to an Xml element
    /// </summary>
    /// <param name="node">The parent node</param>
    /// <param name="xpath">The xpath for the required node</param>
    /// <returns>The value of the required node</returns>
    private string XPathGetElementValue(XNode node, string xpath)
    {
      var element = node.XPathSelectElement(xpath);
      return element != null ? element.Value : null;
    }

    /// <summary>
    /// Gets the value of an attribute for an Xml element
    /// </summary>
    /// <param name="element">The parent node</param>
    /// <param name="name">The name of the required attribute</param>
    /// <returns>The value of the required attribute</returns>
    private string XPathGetAttributeValue(XElement element, string name)
    {
      var attribute = element.Attribute(name);
      return attribute != null ? attribute.Value : null;
    }

    #endregion
  }
}
