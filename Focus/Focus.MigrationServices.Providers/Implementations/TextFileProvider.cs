﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Messages.AccountService;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Messages.JobService;
using Focus.Core.Messages.SearchService;
using Focus.Data.Library.Entities;
using Focus.MigrationServices.Providers.Common;
using Focus.MigrationServices.Providers.Interfaces;
using Focus.Services.DtoMappers;
using EmployerDto = Focus.Core.DataTransferObjects.FocusCore.EmployerDto;
using JobDto = Focus.Core.DataTransferObjects.FocusCore.JobDto;

using Framework.Core;

#endregion

namespace Focus.MigrationServices.Providers.Implementations
{
  public class TextFileProvider : MigrationProviderBase, IMigrationProvider, IEmployer, IJobOrder
  {
    private readonly Regex _csvRegex;

    private int _lastEmployerLine;
    private int _lastJobLine;
    private int _lastJobProgramOfStudyLine;
    private int _lastJobAddressLine;
    private int _lastJobLocationLine;

    private int _employerCount;

    private List<string> _naicsKeys;
    private Dictionary<string, NAICSDto> _naics;
    private int _naicsCount;
    private int _lastNaicsIndex;

    private List<string> _ownershipTypeKeys;
    private Dictionary<string, long> _ownershipTypeIds;
    private int _lastOwnershipTypeIndex;

    private Dictionary<string, List<ProgramAreaDegreeEducationLevelView>> _degrees;

    public TextFileProvider()
    {
      _csvRegex = new Regex("(?<=^|,)(\"(?:[^\"]|\"\")*\"|[^,]*)");

      _lastEmployerLine = _lastJobLine = _lastJobProgramOfStudyLine = _lastJobAddressLine = _lastJobLocationLine = 0;
    }

    public void Initialise(RecordType recordType, long lastId)
    {
      _naics = LibraryRepository.Query<NAICS>().Select(n => n.AsDto()).ToDictionary(n => n.Code, n => n);
      _naicsKeys = _naics.Keys.ToList();
      _naicsCount = _naics.Count;
      _lastNaicsIndex = -1;

      _degrees = GetDegrees();

      _ownershipTypeIds = new Dictionary<string, long>
      {
        {"federalgovernment", GetLookupId(LookupTypes.OwnershipTypes, "Federal Government").GetValueOrDefault(0) },
        {"foreigninternational", GetLookupId(LookupTypes.OwnershipTypes, "Foreign/International").GetValueOrDefault(0) },
        {"localgovernment", GetLookupId(LookupTypes.OwnershipTypes, "Local Government").GetValueOrDefault(0) },
        {"nonprofit", GetLookupId(LookupTypes.OwnershipTypes, "Non-Profit").GetValueOrDefault(0) },
        {"privatecorporation", GetLookupId(LookupTypes.OwnershipTypes, "Private/Corporation").GetValueOrDefault(0) },
        {"stategovernment", GetLookupId(LookupTypes.OwnershipTypes, "State Government").GetValueOrDefault(0) }
      };
      _ownershipTypeKeys = _ownershipTypeIds.Keys.ToList();
      _lastOwnershipTypeIndex = -1;
    }

    #region Employers

    /// <summary>
    /// Gets a list of employer requests to migrate
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of employer requests</returns>
    public List<RegisterTalentUserRequest> MigrateEmployers(MigrationProviderArgs providerArgs)
    {
      var requests = new List<RegisterTalentUserRequest>();

      var columnProvider = AppSettings.Theme == FocusThemes.Education
                            ? (ITextFileProviderEmployerColumns)new TextFileProviderEmployerEducationColumns()
                            : new TextFileProviderEmployerWorkforceColumns();

      var fileInfo = new FileInfo(MigrationFileName);

      var fileData = fileInfo.OpenText();
      fileData.ReadLine();

      var lineNumber = 0;

      while (lineNumber < _lastEmployerLine)
      {
        fileData.ReadLine();
        lineNumber++;
      }

      string line;
      while ((line = fileData.ReadLine()) != null && lineNumber - _lastEmployerLine < providerArgs.BatchSize)
      {
        lineNumber++;

        _employerCount++;
        if (_employerCount == 1000)
          _employerCount = 1;

        var matches = _csvRegex.Matches(line);

        var fein = GetMatch(matches, columnProvider.EmployerFederalNumberColumn);
        var firstName = GetMatch(matches, columnProvider.EmployerFirstNameColumn);
        var title = GetMatch(matches, columnProvider.EmployerTitleColumn).Replace(".", "");
        var lastName = GetMatch(matches, columnProvider.EmployerLastNameColumn);
        var jobTitle = GetMatch(matches, columnProvider.EmployerJobTitleColumn);
        var companyName = GetMatch(matches, columnProvider.EmployerCompanyColumn);
        var address = GetMatch(matches, columnProvider.EmployerAddressColumn);
        var address2 = GetMatch(matches, columnProvider.EmployerAddress2Column);
        var city = GetMatch(matches, columnProvider.EmployerCityColumn);
        var county = GetMatch(matches, columnProvider.EmployerCountyColumn);
        var state = GetMatch(matches, columnProvider.EmployerStateColumn);
        var zipCode = GetMatch(matches, columnProvider.EmployerZipColumn);
        var country = GetMatch(matches, columnProvider.EmployerCountryColumn);
        var phoneNumber = GetMatch(matches, columnProvider.EmployerPhoneColumn);
        var fax = GetMatch(matches, columnProvider.EmployerFaxColumn);
        var email = GetMatch(matches, columnProvider.EmployerEmailColumn);
        var web = GetMatch(matches, columnProvider.EmployerWebColumn);

        var stateId = GetLookupId(LookupTypes.States, state, "Outside U.S.");
        var countyId = GetLookupId(LookupTypes.Counties, county, "", stateId);
        var countryId = GetLookupId(LookupTypes.Countries, country, "United States");
        var titleId = GetLookupId(LookupTypes.Titles, title, "");

        if (fein.IsNullOrEmpty())
          fein = Guid.NewGuid().ToString();

        string naics;

        var naicsCode = GetMatch(matches, columnProvider.EmployerIndustrialClassificationColumn);
        if (naicsCode.Length == 0)
          naics = SpoofData ? GetNextNaics() : string.Empty;
        else
          naics = string.Concat(_naics[naicsCode].Code, " - ", _naics[naicsCode].Name);

        long ownershipTypeId;

        var ownershipType = GetMatch(matches, columnProvider.EmployerOwnershipTypeColumn);
        if (ownershipType.Length == 0)
          ownershipTypeId = SpoofData ? GetNextOwnershipType() : _ownershipTypeIds["federalgovernment"];
        else
          ownershipTypeId = _ownershipTypeIds[ownershipType.ToLowerInvariant()];

        var request = new RegisterTalentUserRequest
        {
          AccountUserName = email,
          AccountPassword = SpoofData ? "Password1" : GenerateRandomPassword()
        };

        request.EmployeePerson = new PersonDto
        {
          TitleId = titleId,
          FirstName = firstName,
          MiddleInitial = "",
          LastName = lastName,
          JobTitle = jobTitle,
          EmailAddress = email
        };

        request.EmployeeAddress = new PersonAddressDto
        {
          Line1 = address,
          Line2 = address2,
          TownCity = city,
          CountyId = countyId,
          PostcodeZip = zipCode,
          StateId = stateId,
          CountryId = countryId,
          IsPrimary = true
        };

        request.EmployeePhone = phoneNumber;
        request.EmployeePhoneExtension = "";
        request.EmployeePhoneType = "Phone";
        request.EmployeeAlternatePhone1 = fax;
        request.EmployeeAlternatePhone1Type = "Fax";
        request.EmployeeAlternatePhone2 = "";
        request.EmployeeAlternatePhone2Type = "Phone";
        request.EmployeeEmailAddress = email;

        request.Employer = new EmployerDto
        {
          Id = 0,
          Name = companyName,
          FederalEmployerIdentificationNumber = fein,
          StateEmployerIdentificationNumber = "",
          Url = web.IsNullOrEmpty() ? null : web,
          OwnershipTypeId = ownershipTypeId,
          PrimaryPhone = phoneNumber,
          PrimaryPhoneExtension = "",
          PrimaryPhoneType = "Phone",
          AlternatePhone1 = fax,
          AlternatePhone1Type = "Fax",
          AlternatePhone2 = "",
          AlternatePhone2Type = "Phone",
          IndustrialClassification = naics,
          TermsAccepted = true,
          ApprovalStatus = ApprovalStatuses.Approved,
          IsValidFederalEmployerIdentificationNumber = true
        };

        request.EmployeeMigrationId = string.Concat("FEIN:", fein);

        request.EmployerAddress = new EmployerAddressDto
        {
          Id = 0,
          Line1 = address,
          Line2 = address2,
          TownCity = city,
          CountyId = countyId,
          PostcodeZip = zipCode,
          StateId = stateId,
          CountryId = countryId,
          IsPrimary = true,
          EmployerId = 0,
          PublicTransitAccessible = true
        };

        request.BusinessUnit = new BusinessUnitDto
        {
          Id = 0,
          EmployerId = 0,
          Name = companyName,
          Url = web.IsNullOrEmpty() ? null : web,
          OwnershipTypeId = ownershipTypeId,
          PrimaryPhone = phoneNumber,
          PrimaryPhoneExtension = "",
          PrimaryPhoneType = "Phone",
          AlternatePhone1 = fax,
          AlternatePhone1Type = "Fax",
          AlternatePhone2 = "",
          AlternatePhone2Type = "Phone",
          IndustrialClassification = naics,
        };

        request.BusinessUnitDescription = new BusinessUnitDescriptionDto
        {
          Id = 0,
          Title = "Company description",
          Description = request.Employer.Name
        };

        request.BusinessUnitAddress = new BusinessUnitAddressDto
        {
          Id = 0,
          Line1 = address,
          Line2 = "",
          TownCity = city,
          CountyId = countyId,
          PostcodeZip = zipCode,
          StateId = stateId,
          CountryId = countryId,
          IsPrimary = true,
          BusinessUnitId = 0,
          PublicTransitAccessible = true
        };

        requests.Add(request);
      }

      _lastEmployerLine = lineNumber;

      return requests;
    }

    /// <summary>
    /// Gets a list of secondary business unit descriptions requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new business unit descriptions</returns>
    public List<SaveBusinessUnitDescriptionRequest> MigrateBusinessUnitDescriptions(MigrationProviderArgs providerArgs)
    {
      return new List<SaveBusinessUnitDescriptionRequest>();
    }

    /// <summary>
    /// Gets a list of secondary business unit logo requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new business unit logos</returns>
    public List<SaveBusinessUnitLogoRequest> MigrateBusinessUnitLogos(MigrationProviderArgs providerArgs)
    {
      return new List<SaveBusinessUnitLogoRequest>();
    }

    /// <summary>
    /// Gets a list of resume searches requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new resume saved searches</returns>
    public List<SaveCandidateSavedSearchRequest> MigrateResumeSearches(MigrationProviderArgs providerArgs)
    {
      return new List<SaveCandidateSavedSearchRequest>();
    }

    /// <summary>
    /// Gets a list of resume alerts requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new resume alerts searches</returns>
    public List<SaveCandidateSavedSearchRequest> MigrateResumeAlerts(MigrationProviderArgs providerArgs)
    {
      return new List<SaveCandidateSavedSearchRequest>();
    }

    /// <summary>
    /// Gets a list of job requests to migrate
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of employer requests</returns>
    public List<SaveJobRequest> MigrateJobOrders(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobRequest>();

      var columnProvider = AppSettings.Theme == FocusThemes.Education
                      ? (ITextFileProviderJobColumns)new TextFileProviderJobEducationColumns()
                      : new TextFileProviderJobWorkforceColumns();

      var fileInfo = new FileInfo(MigrationFileName);

      var fileData = fileInfo.OpenText();
      fileData.ReadLine();

      var lineNumber = 0;

      while (lineNumber < _lastJobLine)
      {
        fileData.ReadLine();
        lineNumber++;
      }

      string line;
      while ((line = fileData.ReadLine()) != null && lineNumber - _lastJobLine < providerArgs.BatchSize)
      {
        lineNumber++;

        var matches = _csvRegex.Matches(line);

        var fein = GetMatch(matches, columnProvider.JobFederalNumberColumn);
        var jobTypeText = GetMatch(matches, columnProvider.JobJobTypeColumn);
        var onet = GetMatch(matches, columnProvider.JobOnetCodeColumn);
        var jobTitle = GetMatch(matches, columnProvider.JobJobTitleColumn);
        var description = GetMatch(matches, columnProvider.JobDescriptionColumn);
        var closingDate = DateTime.Parse(GetMatch(matches, columnProvider.JobClosingDateColumn));
        var numberOfOpenings = int.Parse(GetMatch(matches, columnProvider.JobNumberOfOpeningsColumn));
        var educationLevelText = GetMatch(matches, columnProvider.JobEducationLevelColumn);
        var foreignLabourText = GetMatch(matches, columnProvider.JobForeignLabourColumn);
        var workOpportunitiesTaxCreditText = GetMatch(matches, columnProvider.JobWOTCInterestColumn);
        var federalContractorText = GetMatch(matches, columnProvider.JobFederalContractorColumn);
        var courtOrderedAffirmativeText = GetMatch(matches, columnProvider.JobCourtOrderedAffirmativeColumn);

        var jobType = JobTypes.Job;
        switch (jobTypeText.ToLower())
        {
          case "internshippaid":
            jobType = JobTypes.InternshipPaid;
            break;

          case "internshipunpaid":
            jobType = JobTypes.InternshipUnpaid;
            break;
        }

        var minimumEducationLevel = EducationLevels.None;
        switch (educationLevelText.ToLower())
        {
          case "highschooldiploma":
            minimumEducationLevel = EducationLevels.HighSchoolDiplomaOrEquivalent;
            break;

          case "associatesdegree":
            minimumEducationLevel = EducationLevels.AssociatesDegree;
            break;

          case "bachelorsdegree":
            minimumEducationLevel = EducationLevels.BachelorsDegree;
            break;

          case "mastersdegree":
            minimumEducationLevel = EducationLevels.GraduateDegree;
            break;

          case "doctoraldegree":
            minimumEducationLevel = EducationLevels.DoctorateDegree;
            break;
        }

        var workOpportunitiesTaxCredit = WorkOpportunitiesTaxCreditCategories.None;
        if (AppSettings.Theme == FocusThemes.Workforce && Enum.IsDefined(typeof (WorkOpportunitiesTaxCreditCategories), workOpportunitiesTaxCreditText))
          workOpportunitiesTaxCredit = (WorkOpportunitiesTaxCreditCategories)Enum.Parse(typeof (WorkOpportunitiesTaxCreditCategories), workOpportunitiesTaxCreditText, true);

        var request = new SaveJobRequest();

        var job = new JobDto
        {
          EmployerId = 0,
          EmployeeId = 0,
          JobType = jobType,
          JobStatus = JobStatuses.Active,
          EmployerDescriptionPostingPosition = EmployerDescriptionPostingPositions.BelowJobPosting,
          OnetId = GetOnetIdByCode(onet),
          BusinessUnitId = 1,
          BusinessUnitDescriptionId = 1,
          BusinessUnitLogoId = null,
          IsConfidential = false,
          JobTitle = jobTitle,
          Description = description,
          ProgramsOfStudyRequired = false,

          MinimumEducationLevel = minimumEducationLevel,
          MinimumEducationLevelRequired = false,

          MinimumExperience = AppSettings.Theme == FocusThemes.Workforce && SpoofData ? 2 : (int?)null,
          MinimumExperienceMonths = AppSettings.Theme == FocusThemes.Workforce && SpoofData ? 6 : (int?)null,
          MinimumExperienceRequired = AppSettings.Theme == FocusThemes.Workforce && SpoofData,

          MinimumAge = null,
          MinimumAgeReason = "",
          MinimumAgeRequired = false,

          DrivingLicenceClassId = null,
          DrivingLicenceRequired = false,

          LicencesRequired = false,
          CertificationRequired = false,
          LanguagesRequired = false,

          JobLocationType = JobLocationTypes.MainSite,

          FederalContractor = federalContractorText.Equals("Y", StringComparison.OrdinalIgnoreCase),
          FederalContractorExpiresOn = null,

          ForeignLabourCertification = null,
          ForeignLabourCertificationH2A = foreignLabourText.Equals("H2A", StringComparison.OrdinalIgnoreCase),
          ForeignLabourCertificationH2B = foreignLabourText.Equals("H2B", StringComparison.OrdinalIgnoreCase),
          ForeignLabourCertificationOther = foreignLabourText.Equals("Other", StringComparison.OrdinalIgnoreCase),

          CourtOrderedAffirmativeAction = courtOrderedAffirmativeText.Equals("Y", StringComparison.OrdinalIgnoreCase),
          WorkOpportunitiesTaxCreditHires = AppSettings.Theme == FocusThemes.Workforce ? workOpportunitiesTaxCredit : (WorkOpportunitiesTaxCreditCategories?)null,
          PostingFlags = AppSettings.Theme == FocusThemes.Workforce && SpoofData ? JobPostingFlags.ITEmployer : (JobPostingFlags?)null,

          MinSalary = SpoofData ? 10 : (decimal?) null,
          MaxSalary = SpoofData ? 15 : (decimal?)null,
          SalaryFrequencyId = SpoofData ? GetLookupId(LookupTypes.Frequencies, "Hourly") : null,
          HideSalaryOnPosting = false,
          IsCommissionBased = false,
					IsSalaryAndCommissionBased = false,

          NormalWorkDays = AppSettings.Theme == FocusThemes.Workforce && SpoofData ? DaysOfWeek.All : (DaysOfWeek?)null,
          WorkDaysVary = AppSettings.Theme == FocusThemes.Workforce && SpoofData ? true : (bool?)null,
          EmploymentStatusId = AppSettings.Theme == FocusThemes.Workforce && SpoofData ? GetLookupId(LookupTypes.EmploymentStatuses, "Fulltime", "") : (long?)null,
          HoursPerWeek = 35,
          NormalWorkShiftsId = AppSettings.Theme == FocusThemes.Workforce && SpoofData ? GetLookupId(LookupTypes.WorkShifts, "Varies", "") : (long?)null,
          OverTimeRequired = false,
          JobTypeId = AppSettings.Theme == FocusThemes.Workforce && SpoofData ? GetLookupId(LookupTypes.JobTypes, "Permanent", "") : (long?)null,
          JobStatusId = AppSettings.Theme == FocusThemes.Workforce && SpoofData ? GetLookupId(LookupTypes.JobStatuses, "Hourly", "") : (long?)null,

          LeaveBenefits = LeaveBenefits.None,
          RetirementBenefits = RetirementBenefits.None,
          InsuranceBenefits = InsuranceBenefits.None,
          MiscellaneousBenefits = MiscellaneousBenefits.None,
          OtherBenefitsDetails = "",

          ClosingOn = closingDate,
          NumberOfOpenings = numberOfOpenings,
          InterviewContactPreferences = ContactMethods.FocusTalent,

          InterviewEmailAddress = "",
          InterviewApplicationUrl = "",
          InterviewMailAddress = "",
          InterviewFaxNumber = "",
          InterviewPhoneNumber = "",
          InterviewDirectApplicationDetails = "",
          InterviewOtherInstructions = "",

          ScreeningPreferences = ScreeningPreferences.AllowUnqualifiedApplications,
          ApprovalStatus = ApprovalStatuses.None,

          WizardPath = 1,
          WizardStep = AppSettings.Theme == FocusThemes.Education ? 5 : 7
        };

        request.Job = job;
	      request.Module = FocusModules.General;
        request.EmployeeMigrationId = string.Concat("FEIN:", fein);
        request.JobMigrationId = string.Concat(fileInfo.LastWriteTime.ToString(CultureInfo.InvariantCulture), " - ", lineNumber.ToString(CultureInfo.InvariantCulture));

        requests.Add(request);
      }

      _lastJobLine = lineNumber;

      return requests;
    }

    public List<SaveJobAddressRequest> MigrateJobAddresses(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobAddressRequest>();

      var fileInfo = new FileInfo(MigrationFileName);

      var fileData = fileInfo.OpenText();
      fileData.ReadLine();

      var lineNumber = 0;

      while (lineNumber < _lastJobAddressLine)
      {
        fileData.ReadLine();
        lineNumber++;
      }

      while (fileData.ReadLine() != null && lineNumber - _lastJobAddressLine < providerArgs.BatchSize)
      {
        lineNumber++;

        var request = new SaveJobAddressRequest
        {
          JobAddress = new JobAddressDto
          {
            Id = -1
          },
          JobMigrationId = string.Concat(fileInfo.LastWriteTime.ToString(CultureInfo.InvariantCulture), " - ", lineNumber.ToString(CultureInfo.InvariantCulture))
        };
        request.JobAddressMigrationId = request.JobMigrationId;

        requests.Add(request);
      }

      _lastJobAddressLine = lineNumber;

      return requests;
    }

    public List<SaveJobCertificatesRequest> MigrateJobCertificates(MigrationProviderArgs providerArgs)
    {
      return new List<SaveJobCertificatesRequest>();
    }

    public List<SaveJobLanguagesRequest> MigrateJobLanguages(MigrationProviderArgs providerArgs)
    {
      return new List<SaveJobLanguagesRequest>();
    }

    public List<SaveJobLicencesRequest> MigrateJobLicences(MigrationProviderArgs providerArgs)
    {
      return new List<SaveJobLicencesRequest>();
    }

    public List<SaveJobLocationsRequest> MigrateJobLocations(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobLocationsRequest>();

      var fileInfo = new FileInfo(MigrationFileName);

      var fileData = fileInfo.OpenText();
      fileData.ReadLine();

      var lineNumber = 0;

      while (lineNumber < _lastJobLocationLine)
      {
        fileData.ReadLine();
        lineNumber++;
      }

      while (fileData.ReadLine() != null && lineNumber - _lastJobLocationLine < providerArgs.BatchSize)
      {
        lineNumber++;

        var request = new SaveJobLocationsRequest
        {
          JobLocations = new List<JobLocationDto>
          {
            new JobLocationDto
            {
              Id = -1,
              IsPublicTransitAccessible = true
            }
          },
          JobMigrationId = string.Concat(fileInfo.LastWriteTime.ToString(CultureInfo.InvariantCulture), " - ", lineNumber.ToString(CultureInfo.InvariantCulture))
        };
        request.JobLocationMigrationId = request.JobMigrationId;

        requests.Add(request);
      }

      _lastJobLocationLine = lineNumber;

      return requests;
    }

    public List<SaveJobSpecialRequirementsRequest> MigrateJobSpecialRequirements(MigrationProviderArgs providerArgs)
    {
      return new List<SaveJobSpecialRequirementsRequest>();
    }

    public List<SaveJobProgramsOfStudyRequest> MigrateJobProgramsOfStudy(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobProgramsOfStudyRequest>();

      var columnProvider = AppSettings.Theme == FocusThemes.Education
                ? (ITextFileProviderJobColumns)new TextFileProviderJobEducationColumns()
                : new TextFileProviderJobWorkforceColumns();

      if (columnProvider.JobDegreeColumn == -1)
        return requests;

      var fileInfo = new FileInfo(MigrationFileName);

      var fileData = fileInfo.OpenText();
      fileData.ReadLine();

      var lineNumber = 0;

      while (lineNumber < _lastJobProgramOfStudyLine)
      {
        fileData.ReadLine();
        lineNumber++;
      }

      string line;
      while ((line = fileData.ReadLine()) != null && lineNumber - _lastJobProgramOfStudyLine < providerArgs.BatchSize)
      {
        lineNumber++;

        var matches = _csvRegex.Matches(line);

        var degreeText = GetMatch(matches, columnProvider.JobDegreeColumn);

        var programOfStudyDto = _degrees.ContainsKey(degreeText)
                                  ? new JobProgramOfStudyDto
                                    {
                                      ProgramOfStudy = degreeText,
																			DegreeEducationLevelId = _degrees[degreeText][0].DegreeEducationLevelId
                                    }
                                  : null;

        var request = new SaveJobProgramsOfStudyRequest
        {
          JobProgramsOfStudy = programOfStudyDto != null 
            ? new List<JobProgramOfStudyDto> { programOfStudyDto }
            : new List<JobProgramOfStudyDto>(),
          JobMigrationId = string.Concat(fileInfo.LastWriteTime.ToString(CultureInfo.InvariantCulture), " - ", lineNumber.ToString(CultureInfo.InvariantCulture))
        };
        request.JobProgramOfStudyMigrationId = request.JobMigrationId;

        requests.Add(request);
      }

      _lastJobProgramOfStudyLine = lineNumber;

      return requests;
    }

		public List<SaveJobDrivingLicenceEndorsementsRequest> MigrateJobDrivingLicenceEndorsements(MigrationProviderArgs providerArgs)
		{
			return new List<SaveJobDrivingLicenceEndorsementsRequest>();
		}

    #endregion

    #region Helper Functions

    /// <summary>
    /// Cycles through all possible NAICS and returns the next in the list
    /// </summary>
    /// <returns>The next NAICS in the list</returns>
    private string GetNextNaics()
    {
      if (_naicsCount == 0)
        return null;

      _lastNaicsIndex++;
      if (_lastNaicsIndex >= _naicsCount)
        _lastNaicsIndex = 0;

      var nextNaics = _naics[_naicsKeys[_lastNaicsIndex]];
      return string.Concat(nextNaics.Code, " - ", nextNaics.Name);
    }

    /// <summary>
    /// Cycles through all possible ownership types and returns the next in the list
    /// </summary>
    /// <returns>The next ownership types in the list</returns>
    private long GetNextOwnershipType()
    {
      _lastOwnershipTypeIndex++;
      if (_lastOwnershipTypeIndex >= _ownershipTypeIds.Count)
        _lastOwnershipTypeIndex = 0;

      return _ownershipTypeIds[_ownershipTypeKeys[_lastOwnershipTypeIndex]];
    }

    /// <summary>
    /// Gets all degrees from the database (Education only)
    /// </summary>
    /// <returns></returns>
    private Dictionary<string, List<ProgramAreaDegreeEducationLevelView>> GetDegrees()
    {
      var query = LibraryRepository.Query<ProgramAreaDegreeEducationLevelView>();

      query = AppSettings.ExplorerDegreeFilteringType == DegreeFilteringType.Ours 
        ? query.Where(x => !x.IsClientData) 
        : query.Where(x => x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag);

      var degrees = query.ToList();
      var lookup = new Dictionary<string, List<ProgramAreaDegreeEducationLevelView>>();
      degrees.ForEach(d =>
      {
        if (!lookup.ContainsKey(d.DegreeEducationLevelName))
          lookup.Add(d.DegreeEducationLevelName, new List<ProgramAreaDegreeEducationLevelView>());
          
        var degreeList = lookup[d.DegreeEducationLevelName];
        degreeList.Add(d);
      });

      return lookup;
    }

    /// <summary>
    /// Gets the relevant match from a regular expression
    /// </summary>
    /// <param name="matches">All the matches</param>
    /// <param name="column">The column to get</param>
    /// <returns>The matching value</returns>
    private string GetMatch(MatchCollection matches, int column)
    {
      if (column == -1)
        return string.Empty;

      var match = matches[column].Value;
      if (match.StartsWith("\"") && match.EndsWith("\""))
        match = match.Substring(1, match.Length - 2);

      return match;
    }

    /// <summary>
    /// Generates a random password of letters and numbers
    /// </summary>
    /// <returns>The password</returns>
    private string GenerateRandomPassword()
    {
      string password;

      do
      {
        password = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20);
      } while (!Regex.IsMatch(password, AppSettings.PasswordRegExPattern));

      return password;
    }

    #endregion


		public List<EmployerTradeNamesRequest> MigrateEmployerTradeNames(MigrationProviderArgs providerArgs, bool preProcess)
		{
			//throw new NotImplementedException();
			return null;
		}


		public void PreprocessEmployerTradeNameData(MigrationProviderArgs providerArgs, Framework.DataAccess.Database db)
		{
			//throw new NotImplementedException();
		}


		public void PostprocessEmployerTradeNameData()
		{
			//throw new NotImplementedException();
		}


		public List<SaveJobRequest> MigrateSpideredJobs(MigrationProviderArgs providerArgs)
		{
			//throw new NotImplementedException();
			return null;
		}

		public List<SaveJobRequest> MigrateSpideredJobReferrals(MigrationProviderArgs providerArgs)
		{
			//throw new NotImplementedException();
			return null;
		}

		public List<SaveJobRequest> MigrateSavedJobs(MigrationProviderArgs providerArgs)
		{
			//throw new NotImplementedException();
			return null;
		}

		public List<SaveJobRequest> MigrateJobsViewed(MigrationProviderArgs providerArgs)
		{
			//throw new NotImplementedException();
			return null;
		}


		List<SaveSpideredJobRequest> IJobOrder.MigrateSpideredJobs(MigrationProviderArgs providerArgs)
		{
			//throw new NotImplementedException();
			return null;
		}

		List<SaveSavedJobRequest> IJobOrder.MigrateSavedJobs(MigrationProviderArgs providerArgs)
		{
			//throw new NotImplementedException();
			return null;
		}
		
		List<Core.Messages.CandidateService.ReferCandidateRequest> IJobOrder.MigrateSpideredJobReferrals(MigrationProviderArgs providerArgs)
		{
			//throw new NotImplementedException();
			return null;
		}
	}

  public interface ITextFileProviderEmployerColumns
  {
    int EmployerFederalNumberColumn { get; }
    int EmployerTitleColumn { get; }
    int EmployerFirstNameColumn { get; }
    int EmployerLastNameColumn { get; }
    int EmployerJobTitleColumn { get; }
    int EmployerCompanyColumn { get; }
    int EmployerAddressColumn { get; }
    int EmployerAddress2Column { get; }
    int EmployerCityColumn { get; }
    int EmployerCountyColumn { get; }
    int EmployerStateColumn { get; }
    int EmployerZipColumn { get; }
    int EmployerCountryColumn { get; }
    int EmployerPhoneColumn { get; }
    int EmployerFaxColumn { get; }
    int EmployerEmailColumn { get; }
    int EmployerWebColumn { get; }
    int EmployerIndustrialClassificationColumn { get; }
    int EmployerOwnershipTypeColumn { get; }
  }

  public interface ITextFileProviderJobColumns
  {
    int JobFederalNumberColumn { get; }
    int JobJobTypeColumn { get; }
    int JobOnetCodeColumn { get; }
    int JobJobTitleColumn { get; }
    int JobDescriptionColumn { get; }
    int JobDegreeColumn { get; }
    int JobEducationLevelColumn { get; }
    int JobClosingDateColumn { get; }
    int JobNumberOfOpeningsColumn { get; }
	  // ReSharper disable once InconsistentNaming
    int JobWOTCInterestColumn { get; }
    int JobForeignLabourColumn { get; }
    int JobFederalContractorColumn { get; }
    int JobCourtOrderedAffirmativeColumn { get; }
  }

  public class TextFileProviderEmployerEducationColumns : ITextFileProviderEmployerColumns
  {
    public int EmployerFederalNumberColumn { get { return 0; } }
    public int EmployerFirstNameColumn { get { return 1; } }
    public int EmployerLastNameColumn { get { return 2; } }
    public int EmployerTitleColumn { get { return 3; } }
    public int EmployerCompanyColumn { get { return 4; } }
    public int EmployerJobTitleColumn { get { return 5; } }
    public int EmployerAddressColumn { get { return 6; } }
    public int EmployerAddress2Column { get { return 7; } }
    public int EmployerCityColumn { get { return 8; } }
    public int EmployerCountyColumn { get { return -1; } }
    public int EmployerStateColumn { get { return 9; } }
    public int EmployerZipColumn { get { return 10; } }
    public int EmployerCountryColumn { get { return 11; } }
    public int EmployerPhoneColumn { get { return 12; } }
    public int EmployerFaxColumn { get { return 13; } }
    public int EmployerEmailColumn { get { return 14; } }
    public int EmployerWebColumn { get { return 15; } }
    public int EmployerIndustrialClassificationColumn { get { return -1; } }
    public int EmployerOwnershipTypeColumn { get { return -1; } }
  }

  public class TextFileProviderEmployerWorkforceColumns : ITextFileProviderEmployerColumns
  {
    public int EmployerFederalNumberColumn { get { return 0; } }
    public int EmployerFirstNameColumn { get { return 1; } }
    public int EmployerLastNameColumn { get { return 2; } }
    public int EmployerTitleColumn { get { return 3; } }
    public int EmployerJobTitleColumn { get { return -1; } }
    public int EmployerCompanyColumn { get { return 4; } }
    public int EmployerAddressColumn { get { return 5; } }
    public int EmployerAddress2Column { get { return -1; } }
    public int EmployerCityColumn { get { return 6; } }
    public int EmployerCountyColumn { get { return 7; } }
    public int EmployerCountryColumn { get { return -1; } }
    public int EmployerStateColumn { get { return 8; } }
    public int EmployerZipColumn { get { return 9; } }
    public int EmployerPhoneColumn { get { return 10; } }
    public int EmployerFaxColumn { get { return 11; } }
    public int EmployerEmailColumn { get { return 12; } }
    public int EmployerWebColumn { get { return 13; } }
    public int EmployerIndustrialClassificationColumn { get { return 14; } }
    public int EmployerOwnershipTypeColumn { get { return 15; } }
  }

  public class TextFileProviderJobEducationColumns : ITextFileProviderJobColumns
  {
    public int JobFederalNumberColumn { get { return 0; } }
    public int JobJobTypeColumn { get { return 1; } }
    public int JobOnetCodeColumn { get { return 2; } }
    public int JobJobTitleColumn { get { return 3; } }
    public int JobDescriptionColumn { get { return 4; } }
    public int JobDegreeColumn { get { return 5; } }
    public int JobEducationLevelColumn { get { return -1; } }
    public int JobClosingDateColumn { get { return 6; } }
    public int JobNumberOfOpeningsColumn { get { return 7; } }
    public int JobWOTCInterestColumn { get { return -1; } }
    public int JobForeignLabourColumn { get { return -1; } }
    public int JobFederalContractorColumn { get { return -1; } }
    public int JobCourtOrderedAffirmativeColumn { get { return -1; } }
  }

  public class TextFileProviderJobWorkforceColumns : ITextFileProviderJobColumns
  {
    public int JobFederalNumberColumn { get { return 0; } }
    public int JobJobTypeColumn { get { return -1; } }
    public int JobOnetCodeColumn { get { return 1; } }
    public int JobJobTitleColumn { get { return 2; } }
    public int JobDescriptionColumn { get { return 3; } }
    public int JobDegreeColumn { get { return -1; } }
    public int JobEducationLevelColumn { get { return 4; } }
    public int JobClosingDateColumn { get { return 5; } }
    public int JobNumberOfOpeningsColumn { get { return 6; } }
    public int JobWOTCInterestColumn { get { return 7; } }
    public int JobForeignLabourColumn { get { return 8; } }
    public int JobFederalContractorColumn { get { return 9; } }
    public int JobCourtOrderedAffirmativeColumn { get { return 10; } }
  }
}
