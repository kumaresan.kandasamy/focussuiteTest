﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Linq;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.AccountService;
using Focus.Core.Messages.AnnotationService;
using Focus.Core.Messages.CandidateService;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Messages.JobService;
using Focus.Core.Messages.PostingService;
using Focus.Core.Messages.ResumeService;
using Focus.Core.Messages.SearchService;
using Focus.MigrationServices.Providers.Common;
using Focus.MigrationServices.Providers.Interfaces;
using Framework.Core;
using Framework.DataAccess;

#endregion

namespace Focus.MigrationServices.Providers.Implementations
{
  public class CareerTalentProvider : MigrationProviderBase, IMigrationProvider, IEmployer, IJobOrder, IJobSeeker
  {
    private long _lastEmployeeId;
    private long _lastBusinessUnitDescriptionId;
    private long _lastBusinessUnitLogoId;
    private long _lastJobId;
    private long _lastJobLocationId;
    private long _lastJobAddressId;
    private long _lastJobCertificateId;
    private long _lastJobLicenceId;
    private long _lastJobLanguageId;
    private long _lastJobSpecialRequirementId;
	  private long _lastJobDrivingLicenceEndorsementId;

    public void Initialise(RecordType recordType, long lastId)
    {
      switch (recordType)
      {
        case RecordType.Employer:
          _lastEmployeeId = lastId;
          break;

        case RecordType.Job:
          _lastJobId = lastId;
          break;
      }
    }

    #region IEmployer

    /// <summary>
    /// Gets a list of employer requests to migrate
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of employer requests</returns>
    public List<RegisterTalentUserRequest> MigrateEmployers(MigrationProviderArgs providerArgs)
    {
      var requests = new List<RegisterTalentUserRequest>();

      try
      {
        using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
        {
          var dbParams = new[]
					{
					  new DatabaseParam("LastId", _lastEmployeeId),
            new DatabaseParam("BatchSize", providerArgs.BatchSize)
					};

          var dataReader = db.ExecuteReader("EXEC Migrations_NewJersey_EmployerRequests @LastId, @BatchSize", dbParams);

          while (dataReader.Read())
          {
            var ownershipTypeId = LookupOwnershipType(dataReader.GetInt32(dataReader.GetOrdinal("EmployerOwnershipType")));
            var industrialClassification = GetIndustrialClassification(dataReader.GetString(dataReader.GetOrdinal("EmployerIndustrialClassification")));
            var employeeStateId = GetLookupId(LookupTypes.States, dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressState")), "NJ");
            var employerStateId = GetLookupId(LookupTypes.States, dataReader.GetString(dataReader.GetOrdinal("EmployerAddressState")), "NJ");
            var employerCountyId = GetLookupId(LookupTypes.Counties, dataReader.GetString(dataReader.GetOrdinal("EmployerAddressCounty")), employerStateId);

            // Focus requires urls to be valid uris
            var url = FormatUrl(dataReader.GetString(dataReader.GetOrdinal("EmployerUrl")));

            var request = new RegisterTalentUserRequest
            {
              AccountUserName = dataReader.GetString(dataReader.GetOrdinal("UserName")),
              AccountPassword = dataReader.GetString(dataReader.GetOrdinal("Password")),
              EmployeePerson = new PersonDto
              {
                TitleId = LookupPersonalTitle(dataReader.GetString(dataReader.GetOrdinal("EmployeePersonTitle"))),
                FirstName = dataReader.GetString(dataReader.GetOrdinal("EmployeePersonFirstName")),
                MiddleInitial = dataReader.GetString(dataReader.GetOrdinal("EmployeePersonMiddleInitial")),
                LastName = dataReader.GetString(dataReader.GetOrdinal("EmployeePersonLastName")),
                JobTitle = dataReader.GetString(dataReader.GetOrdinal("EmployeePersonJobTitle")),
                EmailAddress = dataReader.GetString(dataReader.GetOrdinal("EmployeePersonEmailAddress"))
              },
              EmployeeAddress = new PersonAddressDto
              {
                Line1 = dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressLine1")),
                Line2 = dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressLine2")),
                TownCity = dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressTownCity")),
                PostcodeZip = dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressPostcodeZip")),
                StateId = employeeStateId,
                CountyId = GetLookupId(LookupTypes.Counties, dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressCounty")), "", employeeStateId),
                CountryId = GetLookupId(LookupTypes.Countries, dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressCountry")), "US"),
                IsPrimary = true
              },
              EmployeePhone = dataReader.GetString(dataReader.GetOrdinal("EmployeePrimaryPhone")),
              EmployeePhoneExtension = "",
              EmployeePhoneType = LookupPhoneType(dataReader.GetString(dataReader.GetOrdinal("EmployeePrimaryPhoneType"))),
              EmployeeAlternatePhone1 = dataReader.GetString(dataReader.GetOrdinal("EmployeeAlternatePhone1")),
              EmployeeAlternatePhone1Type = LookupPhoneType(dataReader.GetString(dataReader.GetOrdinal("EmployeeAlternatePhoneType1"))),
              EmployeeAlternatePhone2 = dataReader.GetString(dataReader.GetOrdinal("EmployeeAlternatePhone2")),
              EmployeeAlternatePhone2Type = LookupPhoneType(dataReader.GetString(dataReader.GetOrdinal("EmployeeAlternatePhoneType2"))),
              EmployeeEmailAddress = dataReader.GetString(dataReader.GetOrdinal("EmployeePersonEmailAddress")),
              Employer = new EmployerDto
              {
                Id = 0,
                Name = dataReader.GetString(dataReader.GetOrdinal("EmployerName")),
                FederalEmployerIdentificationNumber = FormatFEIN(dataReader.GetString(dataReader.GetOrdinal("FederalEmployerIdentificationNumber"))),
                StateEmployerIdentificationNumber = dataReader.GetString(dataReader.GetOrdinal("StateEmployerIdentificationNumber")),
                Url = url,
                OwnershipTypeId = ownershipTypeId,
                PrimaryPhone = dataReader.GetString(dataReader.GetOrdinal("EmployerPrimaryPhone")),
                PrimaryPhoneExtension = "",
                PrimaryPhoneType = LookupPhoneType(dataReader.GetString(dataReader.GetOrdinal("EmployerPrimaryPhoneType"))),
                AlternatePhone1 = dataReader.GetString(dataReader.GetOrdinal("EmployerAlternatePhone1")),
                AlternatePhone1Type = LookupPhoneType(dataReader.GetString(dataReader.GetOrdinal("EmployerAlternatePhoneType1"))),
                AlternatePhone2 = dataReader.GetString(dataReader.GetOrdinal("EmployerAlternatePhone2")),
                AlternatePhone2Type = LookupPhoneType(dataReader.GetString(dataReader.GetOrdinal("EmployerAlternatePhoneType2"))),
                IndustrialClassification = industrialClassification,
                CommencedOn = null,
                ApprovalStatus = LookupEmployerStatus(dataReader.GetInt32(dataReader.GetOrdinal("EmployerStatus"))),
                ExpiredOn = null,
                TermsAccepted = true,
                IsRegistrationComplete = true,
                IsValidFederalEmployerIdentificationNumber = true,
                CreatedOn = dataReader.GetDateTime(dataReader.GetOrdinal("EmployerCreatedOn")),
                UpdatedOn = dataReader.GetDateTime(dataReader.GetOrdinal("EmployerUpdatedOn"))
              },
              EmployerExternalId = dataReader.GetInt32(dataReader.GetOrdinal("EmployerExternalId")).ToString(CultureInfo.InvariantCulture),
              EmployeeMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("EmployeeMigrationId")).ToString(CultureInfo.InvariantCulture)),
              EmployerAddress = new EmployerAddressDto
              {
                Line1 = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressLine1")),
                Line2 = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressLine2")),
                TownCity = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressTownCity")),
                PostcodeZip = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressPostcodeZip")),
                StateId = employerStateId,
                CountyId = employerCountyId,
                CountryId = GetLookupId(LookupTypes.Countries, dataReader.GetString(dataReader.GetOrdinal("EmployerAddressCountry")), "US"),
                IsPrimary = true,
                PublicTransitAccessible = true
              },
              BusinessUnit = new BusinessUnitDto
              {
                Id = -1, // -1 is being used to indicate to the Import Service to look up the existing business unit id
                Name = dataReader.GetString(dataReader.GetOrdinal("EmployerName")),
                Url = url,
                OwnershipTypeId = ownershipTypeId,
                PrimaryPhone = dataReader.GetString(dataReader.GetOrdinal("EmployerPrimaryPhone")),
                PrimaryPhoneExtension = "",
                PrimaryPhoneType = "Phone",
                AlternatePhone1 = dataReader.GetString(dataReader.GetOrdinal("EmployeeAlternatePhone1")),
                AlternatePhone1Type = "Phone",
                AlternatePhone2 = dataReader.GetString(dataReader.GetOrdinal("EmployeeAlternatePhone2")),
                AlternatePhone2Type = "Phone",
                IsPrimary = true,
                IndustrialClassification = industrialClassification,
              },
              BusinessUnitDescription = new BusinessUnitDescriptionDto
              {
                Title = dataReader.GetString(dataReader.GetOrdinal("EmployerDescriptionTitle")),
                Description = dataReader.GetString(dataReader.GetOrdinal("EmployerDescription"))
              },
              BusinessUnitAddress = new BusinessUnitAddressDto
              {
                Line1 = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressLine1")),
                Line2 = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressLine2")),
                TownCity = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressTownCity")),
                PostcodeZip = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressPostcodeZip")),
                StateId = employerStateId,
                CountyId = employerCountyId,
                CountryId = GetLookupId(LookupTypes.Countries, dataReader.GetString(dataReader.GetOrdinal("EmployerAddressCountry")), "US"),
                IsPrimary = true,
                PublicTransitAccessible = true
              }
            };

            request.AccountPassword = "Password1";

            // Do some formatting of the phone numbers
            string extension;

            request.EmployeePhone = ExtractPhoneExtension(request.EmployeePhone, out extension);
            request.EmployeePhoneExtension = extension;

            request.Employer.PrimaryPhone = ExtractPhoneExtension(request.Employer.PrimaryPhone, out extension);
            request.Employer.PrimaryPhoneExtension = extension;

            _lastEmployeeId = dataReader.GetInt32(dataReader.GetOrdinal("EmployerRepId"));

            requests.Add(request);
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateEmployers SQL Execution exception.", ex);
      }

      return requests;
    }

    /// <summary>
    /// Gets a list of secondary business unit descriptions requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new business unit descriptions</returns>
    public List<SaveBusinessUnitDescriptionRequest> MigrateBusinessUnitDescriptions(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveBusinessUnitDescriptionRequest>();

      try
      {
        using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
        {
          var dbParams = new[]
					{
					  new DatabaseParam("LastId", _lastBusinessUnitDescriptionId),
            new DatabaseParam("BatchSize", providerArgs.BatchSize)
					};

          var dataReader = db.ExecuteReader("EXEC Migrations_NewJersey_BusinessUnitDescriptionRequests @LastId, @BatchSize", dbParams);

          while (dataReader.Read())
          {
            var request = new SaveBusinessUnitDescriptionRequest
            {
              BusinessUnitDescription = new BusinessUnitDescriptionDto
              {
                Title = dataReader.GetString(dataReader.GetOrdinal("Title")),
                Description = dataReader.GetString(dataReader.GetOrdinal("Description")),
                IsPrimary = dataReader.GetBoolean(dataReader.GetOrdinal("IsPrimary"))
              },
              BusinessUnitDescriptionMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("DescriptionId")).ToString(CultureInfo.InvariantCulture)),
              EmployeeMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("EmployeeMigrationId")).ToString(CultureInfo.InvariantCulture))
            };

            _lastBusinessUnitDescriptionId = dataReader.GetInt32(dataReader.GetOrdinal("DescriptionId"));

            requests.Add(request);
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateEmployers SQL Execution exception.", ex);
      }

      return requests;
    }


    /// <summary>
    /// Gets a list of secondary business unit logo requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new business unit logos</returns>
    public List<SaveBusinessUnitLogoRequest> MigrateBusinessUnitLogos(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveBusinessUnitLogoRequest>();

      try
      {
        using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
        {
          var dbParams = new[]
					{
					  new DatabaseParam("LastId", _lastBusinessUnitLogoId),
            new DatabaseParam("BatchSize", providerArgs.BatchSize)
					};

          var dataReader = db.ExecuteReader("EXEC Migrations_NewJersey_BusinessUnitLogoRequests @LastId, @BatchSize", dbParams);

          while (dataReader.Read())
          {
            var bytes = (byte[]) dataReader["Logo"];

            if (bytes.Length > 13)
            {
              var request = new SaveBusinessUnitLogoRequest
              {
                BusinessUnitLogo = new BusinessUnitLogoDto
                {
                  Name = dataReader.GetString(dataReader.GetOrdinal("Name")),
                  Logo = ConvertImageBytesToGif((byte[]) dataReader["Logo"])
                },
                BusinessUnitLogoMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("LogoId")).ToString(CultureInfo.InvariantCulture)),
                EmployeeMigrationId = string.Concat("NJ:",dataReader.GetInt32(dataReader.GetOrdinal("EmployeeMigrationId")).ToString(CultureInfo.InvariantCulture))
              };

              requests.Add(request);
            }

            _lastBusinessUnitLogoId = dataReader.GetInt32(dataReader.GetOrdinal("LogoId"));
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateEmployers SQL Execution exception.", ex);
      }

      return requests;
    }

    /// <summary>
    /// Gets a list of resume searches requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new resume saved searches</returns>
    public List<SaveCandidateSavedSearchRequest> MigrateResumeSearches(MigrationProviderArgs providerArgs)
    {
      return new List<SaveCandidateSavedSearchRequest>();
    }

    /// <summary>
    /// Gets a list of resume alerts requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new resume alerts searches</returns>
    public List<SaveCandidateSavedSearchRequest> MigrateResumeAlerts(MigrationProviderArgs providerArgs)
    {
      return new List<SaveCandidateSavedSearchRequest>();
    }

    #endregion

    #region ISaveJobRequest

    /// <summary>
    /// Gets a list of job requests to migrate
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of job requests</returns>
    public List<SaveJobRequest> MigrateJobOrders(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobRequest>();

      try
      {
        using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
        {
          var dbParams = new[]
					{
					  new DatabaseParam("LastId", _lastJobId),
            new DatabaseParam("BatchSize", providerArgs.BatchSize)
					};

          var dataReader = db.ExecuteReader("EXEC Migrations_NewJersey_JobOrderRequests @LastId, @BatchSize", dbParams);

          while (dataReader.Read())
          {
            var postingXml = XDocument.Parse(dataReader.GetString(dataReader.GetOrdinal("JobXml")));
            var postingElement = ((postingXml.Root == null) ? new XElement("posting") : postingXml.Root.Element("posting")) ?? new XElement("posting");
            var requiredElement = postingElement.Descendants("required").FirstOrDefault() ?? new XElement("required");
            var benefitsElement = postingElement.Descendants("benefits").FirstOrDefault() ?? new XElement("benefits");
            var miscellaneousElement = benefitsElement.Descendants("miscellaneous").FirstOrDefault() ?? new XElement("miscellaneous");
            var preferencesElement = postingElement.Descendants("preference").FirstOrDefault() ?? new XElement("preference");

            var request = new SaveJobRequest
            {
							Module = FocusModules.General,
							Job = new JobDto
              {
                EmployerId = 0,
                EmployeeId = 0,
                JobStatus = JobStatuses.Active,
                JobTitle = dataReader.GetString(dataReader.GetOrdinal("JobTitle")),
                ApprovalStatus = ApprovalStatuses.Approved,
                OnetId = GetOnetIdByPhrase(GetChildElementValue(postingElement, "generic_title")),
                BusinessUnitId = 0,
                BusinessUnitDescriptionId = 0,
                EmployerDescriptionPostingPosition = EmployerDescriptionPostingPositions.BelowJobPosting,
                BusinessUnitLogoId = null,
                IsConfidential = false,
                Description = dataReader.GetString(dataReader.GetOrdinal("Description")),
                MinimumEducationLevel = EducationLevels.None,
                MinimumEducationLevelRequired = null,
                MinimumExperience = null,
                MinimumExperienceMonths = null,
                MinimumExperienceRequired = null,
                MinimumAge = null,
                MinimumAgeReason = "",
                MinimumAgeRequired = false,
                DrivingLicenceClassId = null,
                DrivingLicenceRequired = false,
                LicencesRequired = GetBooleanValue(requiredElement.Element("licenses"), "license_mandate"),
                CertificationRequired = GetBooleanValue(requiredElement.Element("certifications"), "certification_mandate"),
                LanguagesRequired = GetBooleanValue(requiredElement.Element("languages"), "language_mandate"),
                JobLocationType = (dataReader.GetInt32(dataReader.GetOrdinal("IsMainSite")) == 1 ? JobLocationTypes.MainSite : JobLocationTypes.OtherLocation),
                FederalContractor = GetBooleanValue(requiredElement, "federal_contractor"),
                FederalContractorExpiresOn = null,
                ForeignLabourCertificationH2A = false,
                ForeignLabourCertificationH2B = false,
                ForeignLabourCertificationOther = GetBooleanValue(requiredElement, "foreign_labor_certification"),
                CourtOrderedAffirmativeAction = GetBooleanValue(requiredElement, "Court_ordered_affirmative_action"),
                WorkOpportunitiesTaxCreditHires = LookupTaxCreditCategories(requiredElement),
                HiringFromTaxCreditProgramNotificationSent = null,
                PostingFlags = LookupJobPostingFlags(postingElement.Element("flag")),
                MinSalary = null,
                MaxSalary = null,
                SalaryFrequencyId = null,
                HideSalaryOnPosting = false,
                EmploymentStatusId = LookupJobEmploymentStatus(postingElement.Element("jobduration")),
                HoursPerWeek = null,
                NormalWorkDays = DaysOfWeek.None,
                WorkDaysVary = null,
                OverTimeRequired = GetBooleanValue(benefitsElement, "workovertime"),
                IsCommissionBased = false,
								IsSalaryAndCommissionBased = false,
                NormalWorkShiftsId = LookupNormalWorkShifts(benefitsElement.Element("workshifts")),
                JobTypeId = LookupJobType(postingElement.Element("jobduration")),
                JobStatusId = null,
                LeaveBenefits = LookupLeaverBenefits(benefitsElement.Element("leave")),
                RetirementBenefits = LookupRetirementBenefits(benefitsElement.Element("retirement")),
                InsuranceBenefits = LookupInsuranceBenefits(benefitsElement.Element("insurance")),
                MiscellaneousBenefits = LookupMiscellaneousBenefits(miscellaneousElement),
                OtherBenefitsDetails = GetChildElementValue(miscellaneousElement, "other"),
                CreatedOn = dataReader.GetDateTime(dataReader.GetOrdinal("CreatedOn")),
                CreatedBy = 0,
                UpdatedOn = dataReader.GetDateTime(dataReader.GetOrdinal("UpdatedOn")),
                UpdatedBy = 0,
                PostedOn = dataReader.IsDBNull(dataReader.GetOrdinal("PostedOn")) ? (DateTime?)null : dataReader.GetDateTime(dataReader.GetOrdinal("PostedOn")),
                PostedBy = null,
                ClosingOn = dataReader.IsDBNull(dataReader.GetOrdinal("ClosingOn")) ? (DateTime?)null : dataReader.GetDateTime(dataReader.GetOrdinal("ClosingOn")),
                ClosedOn = dataReader.IsDBNull(dataReader.GetOrdinal("ClosedOn")) ? (DateTime?)null : dataReader.GetDateTime(dataReader.GetOrdinal("ClosedOn")),
                ClosedBy = null,
                ApprovedOn = dataReader.GetDateTime(dataReader.GetOrdinal("ApprovedOn")),
                ApprovedBy = null,
                NumberOfOpenings = dataReader.GetInt32(dataReader.GetOrdinal("NumberOfOpenings")),
                InterviewContactPreferences = ContactMethods.None,
                InterviewEmailAddress = "",
                InterviewApplicationUrl = "",
                InterviewMailAddress = "",
                InterviewFaxNumber = "",
                InterviewPhoneNumber = "",
                InterviewDirectApplicationDetails = "",
                InterviewOtherInstructions = "",
                ScreeningPreferences = ScreeningPreferences.AllowUnqualifiedApplications,
                WizardStep = 7,
                WizardPath = 1,
                ExternalId = dataReader.GetInt32(dataReader.GetOrdinal("ExternalId")).ToString(CultureInfo.InvariantCulture),
                //Posting = ""
              },
              JobMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("JobMigrationId")).ToString(CultureInfo.InvariantCulture)),
              EmployeeMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("EmployeeMigrationId")).ToString(CultureInfo.InvariantCulture))
            };

            // Set some dates to null if year is 1900
            request.Job.ClosedOn = (request.Job.ClosedOn.HasValue && request.Job.ClosedOn.Value.Year == 1900) ? null : request.Job.ClosedOn;
            request.Job.ClosingOn = (request.Job.ClosingOn.HasValue && request.Job.ClosingOn.Value.Year == 1900) ? null : request.Job.ClosingOn;
            request.Job.PostedOn = (request.Job.PostedOn.HasValue && request.Job.PostedOn.Value.Year == 1900) ? null : request.Job.PostedOn;

            // Job Description
            var jobPropertyElement = postingElement.Element("company");
            if (jobPropertyElement != null)
            {
              request.Job.EmployerDescriptionPostingPosition =
                (GetChildElementValue(jobPropertyElement, "description_position") == "1")
                  ? EmployerDescriptionPostingPositions.BelowJobPosting
                  : EmployerDescriptionPostingPositions.AboveJobPosting;

              request.Job.IsConfidential = GetBooleanValue(jobPropertyElement, "confidential");
              request.EmployerMigrationLogoId = LookupLogoId(dataReader.GetInt32(dataReader.GetOrdinal("EmployerId")), GetChildElementValue(jobPropertyElement, "logo"));
            }

            // Education Level
            jobPropertyElement = requiredElement.Element("education");
            if (jobPropertyElement != null)
            {
              request.Job.MinimumEducationLevel = LookUpMinimumEducationLevel(GetChildElementValue(jobPropertyElement, "level"));
              request.Job.MinimumEducationLevelRequired = GetMandatoryValue(jobPropertyElement);
            }

            // Minimum Experience
            jobPropertyElement = requiredElement.Element("experience");
            if (jobPropertyElement != null)
            {
              request.Job.MinimumExperience = GetIntegerValue(jobPropertyElement, "years");
              request.Job.MinimumExperienceMonths = null;
              request.Job.MinimumExperienceRequired = GetMandatoryValue(jobPropertyElement); 
            }

            // Minimum Age
            jobPropertyElement = requiredElement.Element("age");
            if (jobPropertyElement != null)
            {
              request.Job.MinimumAge = GetIntegerValue(jobPropertyElement, "years");
              request.Job.MinimumAgeReason = GetChildElementValue(jobPropertyElement, "reason");
              request.Job.MinimumAgeRequired = GetMandatoryValue(jobPropertyElement); 
            }

            // Driving Licenses
            jobPropertyElement = requiredElement.Descendants("drivers").FirstOrDefault();
            if (jobPropertyElement != null)
            {
              request.Job.DrivingLicenceClassId = GetLookupId(LookupTypes.DrivingLicenceClasses, GetChildElementValue(jobPropertyElement, "class"));
              request.Job.DrivingLicenceRequired = GetMandatoryValue(jobPropertyElement); 
            }

            // Salary
            jobPropertyElement = benefitsElement.Element("salary");
            if (jobPropertyElement != null)
            {
              request.Job.MinSalary = GetDecimalValue(jobPropertyElement, "min");
              request.Job.MaxSalary = GetDecimalValue(jobPropertyElement, "max");
              request.Job.SalaryFrequencyId = LookupSalaryFrequency(jobPropertyElement.Element("duration"));
              request.Job.HideSalaryOnPosting = GetBooleanValue(jobPropertyElement, "hidesalary");
            }

            // Hours and Days
            jobPropertyElement = benefitsElement.Element("workdays");
            if (jobPropertyElement != null)
            {
              request.Job.HoursPerWeek = GetIntegerValue(jobPropertyElement, "hoursperweek");
              request.Job.NormalWorkDays = LookupWeekDays(jobPropertyElement);
              request.Job.WorkDaysVary = GetBooleanValue(jobPropertyElement, "varies");
            }

            //Interview Contact Preferences
            jobPropertyElement = preferencesElement.Element("contact_method");
            if (jobPropertyElement != null)
              request.Job.InterviewContactPreferences = LookupContactPreferences(request.Job, jobPropertyElement);

            //Screening Preferences
            jobPropertyElement = preferencesElement.Element("screening");
            if (jobPropertyElement != null)
              request.Job.ScreeningPreferences = LookupScreeningPreferences(jobPropertyElement);

            if (request.Job.ClosedOn.HasValue && request.Job.ClosedOn < DateTime.Now)
              request.Job.JobStatus = JobStatuses.Closed;

            _lastJobId = dataReader.GetInt32(dataReader.GetOrdinal("PostingId"));

            requests.Add(request);
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateJobOrders SQL Execution exception.", ex);
      }

      return requests;
    }

    /// <summary>
    /// Gets a list of job certificate requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job certificate requests</returns>
    public List<SaveJobCertificatesRequest> MigrateJobCertificates(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobCertificatesRequest>();

      try
      {
        using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
        {
          var dbParams = new[]
					{
					  new DatabaseParam("LastId", _lastJobCertificateId),
            new DatabaseParam("BatchSize", providerArgs.BatchSize)
					};

          var dataReader = db.ExecuteReader("EXEC Migrations_NewJersey_JobOrderRequests @LastId, @BatchSize", dbParams);

          while (dataReader.Read())
          {
            var postingXml = XDocument.Parse(dataReader.GetString(dataReader.GetOrdinal("JobXml")));
            var certificationsElement = postingXml.Descendants("certifications").FirstOrDefault() ?? new XElement("certifications");
            var certificates = new List<JobCertificateDto>();

            foreach (var certificationElement in certificationsElement.Elements("certification"))
            {
              var certName = GetChildElementValue(certificationElement, "name");
              if (certName.Length > 0)
              {
                var certification = new JobCertificateDto
                {
                  JobId = 0,
                  Certificate = certName
                };

                certificates.Add(certification);
              }
            }

            if (certificates.Count > 0)
            {
              var request = new SaveJobCertificatesRequest
              {
                JobCertificates = certificates,
                JobCertificationMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("JobMigrationId")).ToString(CultureInfo.InvariantCulture)),
                JobMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("JobMigrationId")).ToString(CultureInfo.InvariantCulture))
              };

              requests.Add(request);
            }

            _lastJobCertificateId = dataReader.GetInt32(dataReader.GetOrdinal("PostingId"));
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateJobCertificates SQL Execution exception.", ex);
      }

      return requests;
    }

    /// <summary>
    /// Gets a list of job language requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job language requests</returns>
    public List<SaveJobLanguagesRequest> MigrateJobLanguages(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobLanguagesRequest>();

      try
      {
        using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
        {
          var dbParams = new[]
					{
					  new DatabaseParam("LastId", _lastJobLanguageId),
            new DatabaseParam("BatchSize", providerArgs.BatchSize)
					};

          var dataReader = db.ExecuteReader("EXEC Migrations_NewJersey_JobOrderRequests @LastId, @BatchSize", dbParams);

          while (dataReader.Read())
          {
            var postingXml = XDocument.Parse(dataReader.GetString(dataReader.GetOrdinal("JobXml")));
            var languagesElement = postingXml.Descendants("languages").FirstOrDefault() ?? new XElement("languages");
            var languages = new List<JobLanguageDto>();

            foreach (var languageElement in languagesElement.Elements("language"))
            {
              var languageName = GetChildElementValue(languageElement, "name");
              if (languageName.Length > 0)
              {
                var language = new JobLanguageDto
                {
                  JobId = 0,
                  Language = languageName
                };

                languages.Add(language);
              }
            }

            if (languages.Count > 0)
            {
              var request = new SaveJobLanguagesRequest
              {
                JobLanguages = languages,
                JobLanguageMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("JobMigrationId")).ToString(CultureInfo.InvariantCulture)),
                JobMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("JobMigrationId")).ToString(CultureInfo.InvariantCulture))
              };

              requests.Add(request);
            }

            _lastJobLanguageId = dataReader.GetInt32(dataReader.GetOrdinal("PostingId"));
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateJobLanguages SQL Execution exception.", ex);
      }

      return requests;
    }

    /// <summary>
    /// Gets a list of job licence requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job licence requests</returns>
    public List<SaveJobLicencesRequest> MigrateJobLicences(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobLicencesRequest>();

      try
      {
        using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
        {
          var dbParams = new[]
					{
					  new DatabaseParam("LastId", _lastJobLicenceId),
            new DatabaseParam("BatchSize", providerArgs.BatchSize)
					};

          var dataReader = db.ExecuteReader("EXEC Migrations_NewJersey_JobOrderRequests @LastId, @BatchSize", dbParams);

          while (dataReader.Read())
          {
            var postingXml = XDocument.Parse(dataReader.GetString(dataReader.GetOrdinal("JobXml")));
            var licencesElement = postingXml.Descendants("licenses").FirstOrDefault() ?? new XElement("licenses");
            var licences = licencesElement.Elements("license")
                           .Where(l => l.Value.Length > 0)
                           .Select
                           (
                             licenceElement => new JobLicenceDto
                                                   {
                                                     JobId = 0, 
                                                     Licence = licenceElement.Value
                                                   }
                           ).ToList();

            if (licences.Count > 0)
            {
              var request = new SaveJobLicencesRequest
              {
                JobLicences = licences,
                JobLicenceMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("JobMigrationId")).ToString(CultureInfo.InvariantCulture)),
                JobMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("JobMigrationId")).ToString(CultureInfo.InvariantCulture))
              };

              requests.Add(request);
            }

            _lastJobLicenceId = dataReader.GetInt32(dataReader.GetOrdinal("PostingId"));
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateJobLicences SQL Execution exception.", ex);
      }

      return requests;
    }

    /// <summary>
    /// Gets a list of job special requirements requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>m>
    /// <returns>A list of new job special requirements requests</returns>
    public List<SaveJobSpecialRequirementsRequest> MigrateJobSpecialRequirements(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobSpecialRequirementsRequest>();

      try
      {
        using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
        {
          var dbParams = new[]
					{
					  new DatabaseParam("LastId", _lastJobSpecialRequirementId),
            new DatabaseParam("BatchSize", providerArgs.BatchSize)
					};

          var dataReader = db.ExecuteReader("EXEC Migrations_NewJersey_JobOrderRequests @LastId, @BatchSize", dbParams);

          while (dataReader.Read())
          {
            var postingXml = XDocument.Parse(dataReader.GetString(dataReader.GetOrdinal("JobXml")));
            var specialRequirementsElement = postingXml.Descendants("specialrequirements").FirstOrDefault() ?? new XElement("specialrequirements");
            var specialRequirements = specialRequirementsElement.Elements("requirement")
                                      .Where(r => r.Value.Length > 0)
                                      .Select
                                      (
                                        specialRequirementElement => new JobSpecialRequirementDto
                                                                         {
                                                                           JobId = 0, 
                                                                           Requirement = specialRequirementElement.Value
                                                                         }
                                      ).ToList();

            if (specialRequirements.Count > 0)
            {
              var request = new SaveJobSpecialRequirementsRequest
              {
                JobSpecialRequirements = specialRequirements,
                JobSpecialRequirementMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("JobMigrationId")).ToString(CultureInfo.InvariantCulture)),
                JobMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("JobMigrationId")).ToString(CultureInfo.InvariantCulture))
              };

              requests.Add(request);
            }

            _lastJobSpecialRequirementId = dataReader.GetInt32(dataReader.GetOrdinal("PostingId"));
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateJobSpecialRequirements SQL Execution exception.", ex);
      }

      return requests;
    }

    /// <summary>
    /// Gets a list of job locations requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job location requests</returns>
    public List<SaveJobLocationsRequest> MigrateJobLocations(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobLocationsRequest>();

      try
      {
        using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
        {
          var dbParams = new[]
					{
					  new DatabaseParam("LastId", _lastJobLocationId),
            new DatabaseParam("BatchSize", providerArgs.BatchSize)
					};

          var dataReader = db.ExecuteReader("EXEC Migrations_NewJersey_JobOrderRequests @LastId, @BatchSize", dbParams);

          while (dataReader.Read())
          {
            var postingXml = XDocument.Parse(dataReader.GetString(dataReader.GetOrdinal("JobXml")));
            var locationsElement = postingXml.Descendants("multiplelocation").FirstOrDefault() ?? new XElement("multiplelocation");
            var locationElement = locationsElement.Element("location");

            if (locationElement != null)
            {
              var cityname = GetChildElementValue(locationElement, "cityname");
              var zip = GetChildElementValue(locationElement, "zipcode");
              var state = GetLookup(LookupTypes.States, GetChildElementValue(locationElement, "statecode"));

              var location = new JobLocationDto
              {
                JobId = 0,
                IsPublicTransitAccessible = GetBooleanValue(locationElement, "public_transit"),
                Location = (state != null)
                             ? string.Format("{0}, {1} ({2})", cityname, state.Text, zip)
                             : string.Format("{0}, ({1})", cityname, zip)
              };

              var request = new SaveJobLocationsRequest
              {
                JobLocations = new List<JobLocationDto> {location},
                JobLocationMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("JobMigrationId")).ToString(CultureInfo.InvariantCulture)),
                JobMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("JobMigrationId")).ToString(CultureInfo.InvariantCulture))
              };

              requests.Add(request);
            }

            _lastJobLocationId = dataReader.GetInt32(dataReader.GetOrdinal("PostingId"));
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateJobLocations SQL Execution exception.", ex);
      }

      return requests;
    }

    /// <summary>
    /// Gets a list of job address requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new job address requests</returns>
    public List<SaveJobAddressRequest> MigrateJobAddresses(MigrationProviderArgs providerArgs)
    {
      var requests = new List<SaveJobAddressRequest>();

      try
      {
        using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
        {
          var dbParams = new[]
					{
					  new DatabaseParam("LastId", _lastJobAddressId),
            new DatabaseParam("BatchSize", providerArgs.BatchSize)
					};

          var dataReader = db.ExecuteReader("EXEC Migrations_NewJersey_JobOrderRequests @LastId, @BatchSize", dbParams);

          while (dataReader.Read())
          {
            var request = new SaveJobAddressRequest
            {
              JobAddress = new JobAddressDto
              {
                JobId = 0,
                Line1 = dataReader.GetString(dataReader.GetOrdinal("JobAddressLine1")),
                Line2 = "",
                Line3 = "",
                TownCity = dataReader.GetString(dataReader.GetOrdinal("JobAddressTownCity")),
                CountyId = null,
                PostcodeZip = dataReader.GetString(dataReader.GetOrdinal("JobAddressPostcodeZip")),
                StateId = GetLookupId(LookupTypes.States, dataReader.GetString(dataReader.GetOrdinal("JobAddressState")), "NJ"),
                CountryId = GetLookupId(LookupTypes.Countries, "US", ""),
                IsPrimary = true
              },
              JobAddressMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("JobMigrationId")).ToString(CultureInfo.InvariantCulture)),
              JobMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("JobMigrationId")).ToString(CultureInfo.InvariantCulture))
            };

            _lastJobAddressId = dataReader.GetInt32(dataReader.GetOrdinal("PostingId"));

            requests.Add(request);
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("MigrateJobAddresses SQL Execution exception.", ex);
      }

      return requests;
    }

    public List<SaveJobProgramsOfStudyRequest> MigrateJobProgramsOfStudy(MigrationProviderArgs providerArgs)
    {
      return new List<SaveJobProgramsOfStudyRequest>();
    }

		/// <summary>
		/// Migrates the job driving licence endorsements.
		/// </summary>
		/// <param name="providerArgs">The provider arguments.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">MigrateJobLicences SQL Execution exception.</exception>
		public List<SaveJobDrivingLicenceEndorsementsRequest> MigrateJobDrivingLicenceEndorsements(MigrationProviderArgs providerArgs)
		{
			var requests = new List<SaveJobDrivingLicenceEndorsementsRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
					  new DatabaseParam("LastId", _lastJobDrivingLicenceEndorsementId),
            new DatabaseParam("BatchSize", providerArgs.BatchSize)
					};

					var dataReader = db.ExecuteReader("EXEC Migrations_NewJersey_JobOrderRequests @LastId, @BatchSize", dbParams);

					while (dataReader.Read())
					{
						var postingXml = XDocument.Parse(dataReader.GetString(dataReader.GetOrdinal("JobXml")));
						var driversElement = postingXml.Descendants("drivers").FirstOrDefault() ?? new XElement("drivers");
						var endorsements = LookupDrivingLicenceEndorsements(driversElement.Element("endorsements"));
						
						if (endorsements.Count > 0)
						{
							var request = new SaveJobDrivingLicenceEndorsementsRequest
							{
								JobDrivingLicenceEndorsements = endorsements,
								JobDrivingLicenceEndorsementMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("JobMigrationId")).ToString(CultureInfo.InvariantCulture)),
								JobMigrationId = string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("JobMigrationId")).ToString(CultureInfo.InvariantCulture))
							};

							requests.Add(request);
						}

						_lastJobDrivingLicenceEndorsementId = dataReader.GetInt32(dataReader.GetOrdinal("PostingId"));
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateJobLicences SQL Execution exception.", ex);
			}

			return requests;
		}

    #endregion

    #region Job Seeker Requests

    public List<RegisterCareerUserRequest> MigrateJobSeekers(MigrationProviderArgs providerArgs)
  	{
      return new List<RegisterCareerUserRequest>();
  	}

    /// <summary>
    /// Gets a list of viewed postings for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of saved search requests</returns>
    public List<AddViewedPostingRequest> MigrateViewedPostings(MigrationProviderArgs providerArgs)
    {
      return new List<AddViewedPostingRequest>();
    }

    /// <summary>
    /// Gets a list of saved searches for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of saved search requests</returns>
    public List<SaveSearchRequest> MigrateJobSeekerSavedSearches(MigrationProviderArgs providerArgs)
    {
      return new List<SaveSearchRequest>();
    }

    /// <summary>
    /// Gets a list of search alerts for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of search alert requests</returns>
    public List<SaveSearchRequest> MigrateJobSeekerSearchAlerts(MigrationProviderArgs providerArgs)
    {
      return new List<SaveSearchRequest>();
    }

    /// <summary>
    /// Gets a list of referrals for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of referral requests</returns>
    public List<ReferCandidateRequest> MigrateReferrals(MigrationProviderArgs providerArgs)
    {
      return new List<ReferCandidateRequest>();
    }

    /// <summary>
    /// Gets a list of activities for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of activities</returns>
    public List<AssignActivityOrActionRequest> MigrateJobSeekerActivities(MigrationProviderArgs providerArgs)
    {
      return new List<AssignActivityOrActionRequest>();
    }

    /// <summary>
    /// Gets a list of resume requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>>
    /// <returns>A list of new resume requests</returns>
    public List<SaveResumeRequest> MigrateResumes(MigrationProviderArgs providerArgs)
    {
      return new List<SaveResumeRequest>();
    }

    /// <summary>
    /// Gets a list of resume document requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new resume document requests</returns>
    public List<SaveResumeDocumentRequest> MigrateResumeDocuments(MigrationProviderArgs providerArgs)
    {
      return new List<SaveResumeDocumentRequest>();
    }

    /// <summary>
    /// Gets a list of flags for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of flags</returns>
    public List<ToggleCandidateFlagRequest> MigrateFlaggedJobSeekers(MigrationProviderArgs providerArgs)
    {
      return new List<ToggleCandidateFlagRequest>();
    }

    #endregion

    #region Helpers

    /// <summary>
    /// Looks up the Id of the logo in Career Talent 
    /// </summary>
    /// <param name="employerId">The Id of the Employer in Career Talent</param>
    /// <param name="fileName">The file name of the logo in Career Talent</param>
    /// <returns>The logo name</returns>
    private string LookupLogoId(int employerId, string fileName)
    {
      if (fileName.Length == 0)
        return string.Empty;

      using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
      {
        var dbParams = new[]
          {
            new DatabaseParam("EmployerId", employerId), 
            new DatabaseParam("FileName", fileName)
          };

        var dataReader = db.ExecuteReader("SELECT LogoId FROM EmployerLogos WHERE EmployerId = @EmployerId AND [FileName] LIKE '%/' + @FileName", dbParams);
        try
        {
          return dataReader.Read() 
            ? string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("LogoId")).ToString(CultureInfo.InvariantCulture)) 
            : string.Empty;
        }
        finally
        {
          dataReader.Close();
        }
      }
    }

    // TODO: These may not be fixed in Career Talent
    /// <summary>
    /// Looks up the employee title.
    /// </summary>
    /// <param name="lookupCode">The title from Career Talent.</param>
    /// <returns>The focus employee title.</returns>
    private long LookupPersonalTitle(string lookupCode)
    {
      string key;

      switch (lookupCode)
      {
        case "1":
          key = "Mr";
          break;

        case "2":
          key = "Mrs";
          break;

        case "3":
          key = "Miss";
          break;

        case "4":
          key = "Ms";
          break;

        default:
          key = "";
          break;
      }
      return GetLookupId(LookupTypes.Titles, key, "");
    }

    // TODO: These may not be fixed in Career Talent
    /// <summary>
    /// Looks up the ownership type.
    /// </summary>
    /// <param name="lookupCode">The ownership type from Career Talent.</param>
    /// <returns>The focus ownership type.</returns>
    private long LookupOwnershipType(int lookupCode)
    {
      string key;

      switch (lookupCode)
      {
        case 1:
          key = "FederalGovernment";
          break;

        case 2:
          key = "StateGovernment";
          break;

        case 3:
          key = "LocalGovernment";
          break;

        case 4:
          key = "NonProfit";
          break;

        case 5:
          key = "PrivateCorporation";
          break;

        default:
          key = "ForeignInternational";
          break;
      }
      return GetLookupId(LookupTypes.OwnershipTypes, key, "");
    }

    /// <summary>
    /// Looks up the phone type for a given code (M, F or L)
    /// </summary>
    /// <param name="lookupCode">The code to look up</param>
    /// <returns>The name of the phone type</returns>
    private string LookupPhoneType(string lookupCode)
    {
      switch (lookupCode)
      {
        case "M":
          return "Mobile";

        case "F":
          return "Fax";

        default:
          return "Phone";
      }
    }

    /// <summary>
    /// Looks up the employer status
    /// </summary>
    /// <param name="lookupCode">The code to look up</param>
    /// <returns>The name of the phone type</returns>
    private ApprovalStatuses LookupEmployerStatus(int lookupCode)
    {
      switch (lookupCode)
      {
        case 0:
          return ApprovalStatuses.Rejected;

        case -1:
          return ApprovalStatuses.WaitingApproval;

        default:
          return ApprovalStatuses.Approved;
      }
    }

    /// <summary>
    /// Splits a phone number of the format xxxxxxxxxx-xxxx into separate phone number and extension
    /// </summary>
    /// <param name="phoneNumber">The phone number to check</param>
    /// <param name="extension">The extension</param>
    private string ExtractPhoneExtension(string phoneNumber, out string extension)
    {
      var hyphenPos = phoneNumber.IndexOf("-", StringComparison.InvariantCulture);
      if (hyphenPos > 0)
      {
        phoneNumber = FormatPhoneNumber(phoneNumber.Substring(0, hyphenPos));
        extension = phoneNumber.Substring(hyphenPos + 1);
      }
      else
      {
        phoneNumber = FormatPhoneNumber(phoneNumber);
        extension = string.Empty;
      }

      return phoneNumber;
    }

    /// <summary>
    /// Looks up the education level (hard-coded in Career Talent). 
    /// </summary>
    /// <param name="lookupCode">The education code type from Career Talent.</param>
    /// <returns>The education level.</returns>
    private EducationLevels LookUpMinimumEducationLevel(string lookupCode)
    {
      switch (lookupCode)
      {
        case "0": // 0 = None
          return EducationLevels.None;

        case "12": // 12 = High School Diploma/GED
          return EducationLevels.HighSchoolDiplomaOrEquivalent;

        case "13": // 13 = Associates/Some College/Vocational Degree
          return EducationLevels.AssociatesDegree;

        case "16": // 16 = Bachelors Degree
          return EducationLevels.BachelorsDegree;

        case "18": // 18 = Masters Degree
          return EducationLevels.GraduateDegree;

        case "21": // 17 = Doctoral Degree
          return EducationLevels.DoctorateDegree;

        default:
          return EducationLevels.None;
      }
    }

    /// <summary>
    /// Looks up the Driving License Endorsements from the posting XML
    /// </summary>
    /// <param name="endorsementsElement">The XElement holding the endorsements</param>
    /// <returns>The Driving License Endorsements</returns>
    private List<JobDrivingLicenceEndorsementDto> LookupDrivingLicenceEndorsements(XElement endorsementsElement)
    {
      var endorsements = new List<JobDrivingLicenceEndorsementDto>();

	    var passTransportEndorsement = LookupDrivingLicenceEndorsement(endorsementsElement, "pass_transport", "PassTransport");
			if (passTransportEndorsement.IsNotNull()) endorsements.Add(passTransportEndorsement);

			var hazerdousMaterialsEndorsement = LookupDrivingLicenceEndorsement(endorsementsElement, "hazardous_materials", "HazerdousMaterials");
			if (hazerdousMaterialsEndorsement.IsNotNull()) endorsements.Add(hazerdousMaterialsEndorsement);

			var tankVehicleEndorsement = LookupDrivingLicenceEndorsement(endorsementsElement, "tank_vehicle", "TankVehicle");
			if (tankVehicleEndorsement.IsNotNull()) endorsements.Add(tankVehicleEndorsement);

			var tankHazardEndorsement = LookupDrivingLicenceEndorsement(endorsementsElement, "tank_hazard", "TankHazard");
			if (tankHazardEndorsement.IsNotNull()) endorsements.Add(tankHazardEndorsement);

			var airbrakesEndorsement = LookupDrivingLicenceEndorsement(endorsementsElement, "air_brakes", "Airbrakes");
			if (airbrakesEndorsement.IsNotNull()) endorsements.Add(airbrakesEndorsement);

			var schoolBusEndorsement = LookupDrivingLicenceEndorsement(endorsementsElement, "school_bus", "SchoolBus");
			if (schoolBusEndorsement.IsNotNull()) endorsements.Add(schoolBusEndorsement);

			var doublesTriplesEndorsement = LookupDrivingLicenceEndorsement(endorsementsElement, "doubles_triples", "DoublesTriples");
			if (doublesTriplesEndorsement.IsNotNull()) endorsements.Add(doublesTriplesEndorsement);

			var motorcycleEndorsement = LookupDrivingLicenceEndorsement(endorsementsElement, "motor_cycle", "Motorcycle");
			if (motorcycleEndorsement.IsNotNull()) endorsements.Add(motorcycleEndorsement);

			return endorsements;
    }

		/// <summary>
		/// Lookups the driving licence endorsement.
		/// </summary>
		/// <param name="endorsementsElement">The endorsements element.</param>
		/// <param name="xmlElementName">Name of the XML element.</param>
		/// <param name="lookupKey">The lookup key.</param>
		/// <returns></returns>
	  private JobDrivingLicenceEndorsementDto LookupDrivingLicenceEndorsement(XElement endorsementsElement, string xmlElementName, string lookupKey)
	  {
		  if (GetBooleanValue(endorsementsElement, xmlElementName))
		  {
			  var lookupId = GetLookupId(LookupTypes.DrivingLicenceEndorsements, lookupKey);

			  if (lookupId.HasValue)
				  return new JobDrivingLicenceEndorsementDto{ DrivingLicenceEndorsementId = lookupId.Value };
		  }
 
			return null;
	  }

		/// <summary>
    /// Looks up the Tax Credit Categories from the posting XML
    /// </summary>
    /// <param name="creditsElement">The XElement holding the Tax Credit Categories</param>
    /// <returns>The Tax Credit Categories</returns>
    private WorkOpportunitiesTaxCreditCategories LookupTaxCreditCategories(XElement creditsElement)
    {
      var credits = WorkOpportunitiesTaxCreditCategories.None;

      credits |= (GetBooleanValue(creditsElement, "tanf_k_tap_recipients") ? WorkOpportunitiesTaxCreditCategories.TemporaryAssistanceToNeedyFamilyRecipients : WorkOpportunitiesTaxCreditCategories.None);
      credits |= (GetBooleanValue(creditsElement, "veterans") ? WorkOpportunitiesTaxCreditCategories.Veterans : WorkOpportunitiesTaxCreditCategories.None);
      credits |= (GetBooleanValue(creditsElement, "ex_felons") ? WorkOpportunitiesTaxCreditCategories.ExFelons : WorkOpportunitiesTaxCreditCategories.None);
      credits |= (GetBooleanValue(creditsElement, "ez_and_rrc") ? WorkOpportunitiesTaxCreditCategories.DesignatedCommunityResidents : WorkOpportunitiesTaxCreditCategories.None);
      credits |= (GetBooleanValue(creditsElement, "vocational_rehabilitation") ? WorkOpportunitiesTaxCreditCategories.VocationalRehabilitation : WorkOpportunitiesTaxCreditCategories.None);
      credits |= (GetBooleanValue(creditsElement, "summer_youth") ? WorkOpportunitiesTaxCreditCategories.SummerYouth : WorkOpportunitiesTaxCreditCategories.None);
      credits |= (GetBooleanValue(creditsElement, "snap_recipients") ? WorkOpportunitiesTaxCreditCategories.SNAPRecipients : WorkOpportunitiesTaxCreditCategories.None);
      credits |= (GetBooleanValue(creditsElement, "ssi") ? WorkOpportunitiesTaxCreditCategories.SupplementSecurityIncomeRecipients : WorkOpportunitiesTaxCreditCategories.None);
      credits |= (GetBooleanValue(creditsElement, "long_term_family_assistance_recipient") ? WorkOpportunitiesTaxCreditCategories.LongTermFamilyAssistanceRecipient : WorkOpportunitiesTaxCreditCategories.None);

      return credits;
    }

    /// <summary>
    /// Looks up the Job Posting Flags from the posting XML
    /// </summary>
    /// <param name="flagsElement">The XElement holding the Job Posting Flags </param>
    /// <returns>The Job Posting Flags</returns>
    private JobPostingFlags LookupJobPostingFlags(XElement flagsElement)
    {
      var postingFlags = JobPostingFlags.None;

      if (flagsElement != null)
      {
        postingFlags |= (GetBooleanValue(flagsElement, "healthcare_employer") ? JobPostingFlags.HealthcareEmployer : JobPostingFlags.None);
        postingFlags |= (GetBooleanValue(flagsElement, "manufacturing_employer") ? JobPostingFlags.AdvancedManufacturingEmployer : JobPostingFlags.None);
        postingFlags |= (GetBooleanValue(flagsElement, "biotech_employer") ? JobPostingFlags.BiotechEmployer : JobPostingFlags.None);
        postingFlags |= (GetBooleanValue(flagsElement, "financial_employer") ? JobPostingFlags.FinancialServicesEmployer : JobPostingFlags.None);
        postingFlags |= (GetBooleanValue(flagsElement, "technology_employer") ? JobPostingFlags.TechnologyEmployer : JobPostingFlags.None);
        postingFlags |= (GetBooleanValue(flagsElement, "transportation_employer") ? JobPostingFlags.TransportationEmployer : JobPostingFlags.None);
        postingFlags |= (GetBooleanValue(flagsElement, "green_job") ? JobPostingFlags.GreenJob : JobPostingFlags.None);
        postingFlags |= (GetBooleanValue(flagsElement, "it_employer") ? JobPostingFlags.ITEmployer : JobPostingFlags.None);
      }

      return postingFlags;
    }

    /// <summary>
    /// Looks up the salary frequency (hard-coded in Career Talent)
    /// </summary>
    /// <param name="frequencyElement">The element holding the frequency</param>
    /// <returns>The focus ID of the frequency</returns>
    private long? LookupSalaryFrequency(XElement frequencyElement)
    {
      string key = string.Empty;

      if (frequencyElement == null)
        return null;

      switch (frequencyElement.Value)
      {
        case "1":
          key = "Hourly";
          break;

        case "2":
          key = "Daily";
          break;

        case "3":
          key = "Weekly";
          break;

        case "4":
          key = "Monthly";
          break;

        case "5":
          key = "Yearly";
          break;
      }

      return GetLookupId(LookupTypes.Frequencies, key);
    }

    // TODO: These may not be fixed in Career Talent
    /// <summary>
    /// Looks up the employment status 
    /// </summary>
    /// <param name="jobDurationElement">The element holding the status (part of job duration)</param>
    /// <returns>The focus ID of the employment status</returns>
    /// <seealso cref="LookupJobType"/>
    private long? LookupJobEmploymentStatus(XElement jobDurationElement)
    {
      string key;

      if (jobDurationElement == null)
        return null;

      switch (jobDurationElement.Value)
      {
        case "1":
        case "2":
        case "3":
        case "7":
          key = "Fulltime";
          break;

        default:
          key = "Parttime";
          break;
      }

      return GetLookupId(LookupTypes.EmploymentStatuses, key);
    }

    /// <summary>
    /// Looks up the Working Days of the Week from the posting XML
    /// </summary>
    /// <param name="daysElement">The XElement holding the Days of the Week</param>
    /// <returns>The Week Days to work</returns>
    private DaysOfWeek LookupWeekDays(XElement daysElement)
    {
      var days = DaysOfWeek.None;

      if (daysElement != null)
      {
        days |= (GetBooleanValue(daysElement, "monday") ? DaysOfWeek.Monday : DaysOfWeek.None);
        days |= (GetBooleanValue(daysElement, "tuesday") ? DaysOfWeek.Tuesday : DaysOfWeek.None);
        days |= (GetBooleanValue(daysElement, "wednesday") ? DaysOfWeek.Wednesday : DaysOfWeek.None);
        days |= (GetBooleanValue(daysElement, "thursday") ? DaysOfWeek.Thursday : DaysOfWeek.None);
        days |= (GetBooleanValue(daysElement, "friday") ? DaysOfWeek.Friday : DaysOfWeek.None);
        days |= (GetBooleanValue(daysElement, "saturday") ? DaysOfWeek.Saturday : DaysOfWeek.None);
        days |= (GetBooleanValue(daysElement, "sunday") ? DaysOfWeek.Sunday : DaysOfWeek.None);
      }

      return days;
    }

    // TODO: These may not be fixed in Career Talent
    /// <summary>
    /// Looks up the normal work shifts
    /// </summary>
    /// <param name="shiftsElement">The element normal work shifts</param>
    /// <returns>The focus ID of the normal work shifts</returns>
    private long? LookupNormalWorkShifts(XElement shiftsElement)
    {
      string key = string.Empty;

      if (shiftsElement == null)
        return null;

      switch (shiftsElement.Value)
      {
        case "1":
          key = "FirstDay";
          break;

        case "2":
          key = "SecondEvening";
          break;

        case "3":
          key = "ThirdNight";
          break;

        case "4":
          key = "Rotating";
          break;

        case "5":
          key = "Split";
          break;

        case "6":
          key = "Varies";
          break;
      }

      return GetLookupId(LookupTypes.WorkShifts, key);
    }

    // TODO: These may not be fixed in Career Talent
    /// <summary>
    /// Looks up the job type
    /// </summary>
    /// <param name="jobTypeElement">The element holding the job type (part of job duration)</param>
    /// <returns>The focus ID of the job type</returns>
    /// <seealso cref="LookupJobEmploymentStatus"/>
    private long? LookupJobType(XElement jobTypeElement)
    {
      string key;

      if (jobTypeElement == null)
        return null;

      switch (jobTypeElement.Value)
      {
        case "3":
        case "6":
          key = "Permanent";
          break;

        case "2":
        case "5":
          key = "Temporary";
          break;

        default:
          key = "Interim";
          break;
      }

      return GetLookupId(LookupTypes.JobTypes, key);
    }

    /// <summary>
    /// Looks up the Leaver Benenfits from the posting XML
    /// </summary>
    /// <param name="leaverElement">The XElement holding the Leaver Benenfits Flags </param>
    /// <returns>The Leaver Benenfits Flags</returns>
    private LeaveBenefits LookupLeaverBenefits(XElement leaverElement)
    {
      var benefits = LeaveBenefits.None;

      if (leaverElement != null)
      {
        benefits |= (GetBooleanValue(leaverElement, "paid_holidays") ? LeaveBenefits.PaidHolidays : LeaveBenefits.None);
        benefits |= (GetBooleanValue(leaverElement, "sick") ? LeaveBenefits.Sick : LeaveBenefits.None);
        benefits |= (GetBooleanValue(leaverElement, "vacation_paid_timeoff") ? LeaveBenefits.Vacation : LeaveBenefits.None);
        benefits |= (GetBooleanValue(leaverElement, "medical") ? LeaveBenefits.Medical : LeaveBenefits.None);
        benefits |= (GetBooleanValue(leaverElement, "leave_sharing") ? LeaveBenefits.LeaveSharing : LeaveBenefits.None);
      }

      return benefits;
    }

    /// <summary>
    /// Looks up the Retirement Benefits from the posting XML
    /// </summary>
    /// <param name="retirementElement">The XElement holding the Retirement Benefits Flags </param>
    /// <returns>The Retirement Benefits Flags</returns>
    private RetirementBenefits LookupRetirementBenefits(XElement retirementElement)
    {
      var benefits = RetirementBenefits.None;

      if (retirementElement != null)
      {
        benefits |= (GetBooleanValue(retirementElement, "pension_plan") ? RetirementBenefits.PensionPlan : RetirementBenefits.None);
        benefits |= (GetBooleanValue(retirementElement, "k401") ? RetirementBenefits.Four01K : RetirementBenefits.None);
        benefits |= (GetBooleanValue(retirementElement, "profit_sharing") ? RetirementBenefits.ProfitSharing : RetirementBenefits.None);
      }

      return benefits;
    }

    /// <summary>
    /// Looks up the Insurance Benefits from the posting XML
    /// </summary>
    /// <param name="insuranceElement">The XElement holding the Insurance Benefits Flags </param>
    /// <returns>The Insurance Benefits Flags</returns>
    private InsuranceBenefits LookupInsuranceBenefits(XElement insuranceElement)
    {
      var benefits = InsuranceBenefits.None;

      if (insuranceElement != null)
      {
        benefits |= (GetBooleanValue(insuranceElement, "dental") ? InsuranceBenefits.Dental : InsuranceBenefits.None);
        benefits |= (GetBooleanValue(insuranceElement, "health") ? InsuranceBenefits.Health : InsuranceBenefits.None);
        benefits |= (GetBooleanValue(insuranceElement, "life") ? InsuranceBenefits.Life : InsuranceBenefits.None);
        benefits |= (GetBooleanValue(insuranceElement, "disability") ? InsuranceBenefits.Disability : InsuranceBenefits.None);
        benefits |= (GetBooleanValue(insuranceElement, "health_savings") ? InsuranceBenefits.HealthSavings : InsuranceBenefits.None);
        benefits |= (GetBooleanValue(insuranceElement, "vision") ? InsuranceBenefits.Vision : InsuranceBenefits.None);
      }

      return benefits;
    }

    /// <summary>
    /// Looks up the Miscellaneous Benefits from the posting XML
    /// </summary>
    /// <param name="miscellaneousElement">The XElement holding the Miscellaneous Benefits Flags </param>
    /// <returns>The Miscellaneous Benefits Flags</returns>
    private MiscellaneousBenefits LookupMiscellaneousBenefits(XElement miscellaneousElement)
    {
      var benefits = MiscellaneousBenefits.None;

      if (miscellaneousElement != null)
      {
        benefits |= (GetBooleanValue(miscellaneousElement, "benefits_negotiable") ? MiscellaneousBenefits.BenefitsNegotiable : MiscellaneousBenefits.None);
        benefits |= (GetBooleanValue(miscellaneousElement, "tution_assitance") ? MiscellaneousBenefits.TuitionAssistance : MiscellaneousBenefits.None);
        benefits |= (GetBooleanValue(miscellaneousElement, "clothing_allowance") ? MiscellaneousBenefits.ClothingAllowance : MiscellaneousBenefits.None);
        benefits |= (GetBooleanValue(miscellaneousElement, "child_care") ? MiscellaneousBenefits.ChildCare : MiscellaneousBenefits.None);
        benefits |= (GetBooleanValue(miscellaneousElement, "relocation") ? MiscellaneousBenefits.Relocation : MiscellaneousBenefits.None);
        benefits |= (GetChildElementValue(miscellaneousElement, "other").Length > 0 ? MiscellaneousBenefits.Other : MiscellaneousBenefits.None);
      }

      return benefits;
    }

    /// <summary>
    /// Looks up the Contract Preferences from the posting XML. This will also set the associated job fields for each preference.
    /// </summary>
    /// <param name="job">The job to update</param>
    /// <param name="contactElement">The XElement holding the Contract Preferences</param>
    /// <returns>The Contract Preferences</returns>
    private ContactMethods LookupContactPreferences(JobDto job, XElement contactElement)
    {
      var preferences = ContactMethods.None;

      var email = GetChildElementValue(contactElement, "email");
      if (email.Length > 0)
      {
        preferences |= ContactMethods.Email;
        job.InterviewEmailAddress = email;
      }

      var applyonline = GetChildElementValue(contactElement, "applyonline");
      if (applyonline.Length > 0)
      {
        preferences |= ContactMethods.Online;
        job.InterviewApplicationUrl = applyonline;
      }

      var mail = GetChildElementValue(contactElement, "mail");
      if (mail.Length > 0)
      {
        preferences |= ContactMethods.Mail;
        job.InterviewMailAddress = mail;
      }

      var fax = GetChildElementValue(contactElement, "fax");
      if (fax.Length > 0)
      {
        preferences |= ContactMethods.Fax;
        job.InterviewFaxNumber = fax;
      }

      var call = GetChildElementValue(contactElement, "call");
      if (call.Length > 0)
      {
        call = FormatPhoneNumber(call);
        preferences |= ContactMethods.Telephone;

        // ReSharper disable PossibleNullReferenceException
        var ext = contactElement.Element("call").Attribute("extension");
        // ReSharper restore PossibleNullReferenceException
        if (ext != null)
          call = string.Concat(call, " Ext: ", ext.Value);

        job.InterviewPhoneNumber = call;
      }

      var inperson = GetChildElementValue(contactElement, "inperson");
      if (inperson.Length > 0)
      {
        preferences |= ContactMethods.InPerson;
        job.InterviewDirectApplicationDetails = inperson;
      }

      job.InterviewOtherInstructions = GetChildElementValue(contactElement, "other");

      return preferences;
    }

    /// <summary>
    /// Looks up the Screening Preferences from the posting XML
    /// </summary>
    /// <param name="screeningElement">The XElement holding the Screening Preferences</param>
    /// <returns>The Screening Preferences</returns>
    private ScreeningPreferences LookupScreeningPreferences(XElement screeningElement)
    {
      var preferences = ScreeningPreferences.AllowUnqualifiedApplications;

      if (screeningElement != null && GetBooleanValue(screeningElement, "workforce"))
        preferences = GetBooleanValue(screeningElement, "work_force") 
          ? ScreeningPreferences.JobSeekersMustBeScreened 
          : ScreeningPreferences.JobSeekersMustHave5StarMatch;

      return preferences;
    }

    /// <summary>
    /// Gets the value of a child element for an XElement
    /// </summary>
    /// <param name="parentElement">The parent XElement</param>
    /// <param name="childElementName">The name of the child element</param>
    /// <returns>The value of the child element, or an empty string should it not exist</returns>
    private string GetChildElementValue(XElement parentElement, string childElementName)
    {
      var childElement = parentElement.Element(childElementName);
      return (childElement == null || childElement.Value.Length == 0) ? string.Empty : childElement.Value;
    }

    /// <summary>
    /// Gets the integer value of a child element for an XElement
    /// </summary>
    /// <param name="parentElement">The parent XElement</param>
    /// <param name="childElementName">The name of the child element</param>
    /// <returns>The integer value of the child element, or null should it not exist</returns>
    private int? GetIntegerValue(XElement parentElement, string childElementName)
    {
      var childValue = GetChildElementValue(parentElement, childElementName);
      return childValue.Length == 0 ? (int?)null : int.Parse(childValue);
    }

    /// <summary>
    /// Gets the decimal value of a child element for an XElement
    /// </summary>
    /// <param name="parentElement">The parent XElement</param>
    /// <param name="childElementName">The name of the child element</param>
    /// <returns>The decimal value of the child element, or null should it not exist</returns>
    private decimal? GetDecimalValue(XElement parentElement, string childElementName)
    {
      var childValue = GetChildElementValue(parentElement, childElementName);
      return childValue.Length == 0 ? (decimal?)null : decimal.Parse(childValue);
    }

    /// <summary>
    /// Gets whether an element in the posting XML is marked as mandatory
    /// </summary>
    /// <param name="parentElement">The parent XElement</param>
    /// <returns>Whether the element is marked as 'mandate' or not</returns>
    private bool GetMandatoryValue(XElement parentElement)
    {
      return GetBooleanValue(parentElement, "mandate");
    }

    /// <summary>
    /// Gets the boolean value of a child element in the posting XML
    /// </summary>
    /// <param name="parentElement">The parent XElement</param>
    /// <param name="childElementName">The name of the child element holding the boolean value</param>
    /// <returns>The boolean value of the child element</returns>
    private bool GetBooleanValue(XElement parentElement, string childElementName)
    {
      if (parentElement == null)
        return false;

      var childElement = parentElement.Element(childElementName);
      return (childElement != null && (childElement.Value == "1"));
    }

    /// <summary>
    /// Converts the bytes for the image to a GIF image, and get the byte stream for the GIF
    /// </summary>
    /// <param name="bytes">The bytes array for the image currently in the database.</param>
    /// <returns>The byte array for the image converted to a gif</returns>
    private byte[] ConvertImageBytesToGif(byte[] bytes)
    {
      // In career talent, the image is saved as a Base 64 encoding string convert to a byte array
      var newBytes = Convert.FromBase64String(System.Text.Encoding.Default.GetString(bytes));

      using (var memoryStream = new MemoryStream(newBytes))
      {
        using (var img = System.Drawing.Image.FromStream(memoryStream))
        {
          using (var newStream = new MemoryStream())
          {
            img.Save(newStream, System.Drawing.Imaging.ImageFormat.Gif);

            return newStream.ToArray();
          }
        }
      }
    }

    #endregion


		public List<EmployerTradeNamesRequest> MigrateEmployerTradeNames(MigrationProviderArgs providerArgs, bool preProcess)
		{
//			throw new NotImplementedException();
			return null;
		}


		public void PreprocessEmployerTradeNameData(MigrationProviderArgs providerArgs, Database db)
		{
//			throw new NotImplementedException();
		}


		public void PostprocessEmployerTradeNameData()
		{
//			throw new NotImplementedException();
		}


		public List<SaveJobRequest> MigrateSpideredJobs(MigrationProviderArgs providerArgs)
		{
//			throw new NotImplementedException();
			return null;
		}

		public List<SaveJobRequest> MigrateSpideredJobReferrals(MigrationProviderArgs providerArgs)
		{
//			throw new NotImplementedException();
			return null;
		}

		public List<SaveJobRequest> MigrateSavedJobs(MigrationProviderArgs providerArgs)
		{
//			throw new NotImplementedException();
			return null;
		}

		public List<SaveJobRequest> MigrateJobsViewed(MigrationProviderArgs providerArgs)
		{
//			throw new NotImplementedException();
			return null;
		}

		List<SaveSpideredJobRequest> IJobOrder.MigrateSpideredJobs(MigrationProviderArgs providerArgs)
		{
//			throw new NotImplementedException();
			return null;
		}
		
		List<SaveSavedJobRequest> IJobOrder.MigrateSavedJobs(MigrationProviderArgs providerArgs)
		{
//			throw new NotImplementedException();
			return null;
		}

		List<ReferCandidateRequest> IJobOrder.MigrateSpideredJobReferrals(MigrationProviderArgs providerArgs)
		{
//			throw new NotImplementedException();
			return null;
		}
	}
}
