﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml.Xsl;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Messages.AccountService;
using Focus.Core.Messages.AnnotationService;
using Focus.Core.Messages.CandidateService;
using Focus.Core.Messages.CoreService;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Messages.JobService;
using Focus.Core.Messages.PostingService;
using Focus.Core.Messages.ResumeService;
using Focus.Core.Messages.SearchService;
using Focus.Core.Models.Career;
using Focus.Data.Configuration.Entities;
using Focus.Data.Library.Entities;
using Focus.MigrationServices.Providers.Common;
using Focus.MigrationServices.Providers.Interfaces;
using Focus.Services;
using Focus.Services.DtoMappers;
using Focus.Services.Mappers;
using Framework.Core;
using Framework.DataAccess;
using DistanceUnits = Focus.Core.Models.Career.DistanceUnits;
using EmployerDto = Focus.Core.DataTransferObjects.FocusCore.EmployerDto;
using Job = Focus.Data.Library.Entities.Job;
using JobDto = Focus.Core.DataTransferObjects.FocusCore.JobDto;

#endregion

namespace Focus.MigrationServices.Providers.Implementations
{
	public class FocusCareer01Provider : MigrationProviderBase, IMigrationProvider, IJobSeeker, IResume, IEmployer, IJobOrder, IUser
	{
		private long _lastJobSeekerId;
		private long _lastResumeId;
		//private int _lastResumeDocumentId;
		private long _lastSavedSearchId;
		private long _lastSearchAlertId;
		private long _lastViewedPostingId;
		private long _lastNotesAndReminderId;
		private long _lastReferralId;
		private long _lastFlaggedId;
		private long _lastActivityId;

		private long _lastEmployeeId;
		private long _lastBusinessUnitDescriptionId;
		private long _lastEmployerTradeNameId;
		private long _lastSpideredJobId;
		private long _lastSpideredJobReferralId; /* unique identifier for migrating spidered referrals */
		private long _lastSavedJobId;
		//private long _lastSpideredReferralId; /* part of a composite key for saved jobs */
		private long _lastBusinessUnitLogoId;
		private long _lastResumeSearchId;
		private long _lastResumeAlertId;

		private long _lastJobId;
		/*
    private int _lastJobLocationId;
    private int _lastJobAddressId;
    private int _lastJobCertificateId;
    private int _lastJobLicenceId;
    private int _lastJobLanguageId;
    private int _lastJobSpecialRequirementId;
    */
		private long _lastUserid;

		private Dictionary<string, long> _focusActivityLookup;
		private List<MilitaryOccupationJobTitleViewDto> _militaryOccupations;

		private XslCompiledTransform _xslt;

		public FocusCareer01Provider()
		{
			_lastSpideredJobId = 0;
			_lastSpideredJobReferralId = 0;
			_lastSavedJobId = 0;
			_lastEmployeeId =
				_lastBusinessUnitDescriptionId =
					_lastEmployerTradeNameId =
						_lastBusinessUnitLogoId =
							_lastResumeSearchId =
								_lastResumeAlertId = 0;

			_lastJobSeekerId =
				_lastResumeId =
					_lastSavedSearchId =
						_lastSearchAlertId =
							_lastViewedPostingId =
								_lastNotesAndReminderId =
									_lastReferralId =
										_lastFlaggedId =
											_lastActivityId = 0;
		}

		public void Initialise(RecordType recordType, long lastId)
		{
			switch (recordType)
			{
				case RecordType.Employer:
					_lastEmployeeId = lastId;
					break;

				case RecordType.EmployerTradeName:
					_lastEmployerTradeNameId = lastId;
					break;

				case RecordType.SpideredJobs:
					_lastSpideredJobId = lastId;
					break;

				case RecordType.SpideredJobReferrals:
					_lastSpideredJobReferralId = lastId;
					break;

				case RecordType.SavedJob:
					_lastSavedJobId = lastId;
					break;

				case RecordType.Job:
					_lastJobId = lastId;
					break;

				case RecordType.JobSeeker:
					_lastJobSeekerId = lastId;
					break;

				case RecordType.Resume:
					_lastResumeId = lastId;
					break;

				case RecordType.Referral:
					_lastReferralId = lastId;
					break;

				case RecordType.Activity:
					_lastActivityId = lastId;
					break;

				case RecordType.Notes:
					_lastNotesAndReminderId = lastId;
					break;

				case RecordType.SavedSearch:
					_lastSavedSearchId = lastId;
					break;

				case RecordType.SearchAlert:
					_lastSearchAlertId = lastId;
					break;

				case RecordType.ResumeSearch:
					_lastResumeSearchId = lastId;
					break;

				case RecordType.ResumeAlert:
					_lastResumeAlertId = lastId;
					break;

				case RecordType.ViewedPosting:
					_lastViewedPostingId = lastId;
					break;
			}

			var assembly = Assembly.GetExecutingAssembly();
			var stream = assembly.GetManifestResourceStream("Focus.MigrationServices.Providers.Assets.TaggedResume.xslt");
			if (stream == null)
				throw new Exception("Unable to get TaggedResume xslt");

			var reader = new StreamReader(stream);
			var xmlReader = XmlReader.Create(reader);

			_xslt = new XslCompiledTransform();
			_xslt.Load(xmlReader);

			xmlReader.Close();
			reader.Close();
			stream.Close();

			var focusActivitiesByExternalIdTemp = ConfigurationRepository.Query<Activity>()
				.Where(activity => activity.ExternalId > 0).ToList();
			//.ToDictionary(activity => activity.ExternalId.ToString(CultureInfo.InvariantCulture), activity => activity.Id);

			var focusActivitiesByExternalId = new Dictionary<string, long>();
			foreach (var a in focusActivitiesByExternalIdTemp)
			{
				if (!focusActivitiesByExternalId.ContainsKey(a.ExternalId.ToString(CultureInfo.InvariantCulture)))
				{
					focusActivitiesByExternalId.Add(a.ExternalId.ToString(CultureInfo.InvariantCulture), a.Id);
				}
			}


			var xmlPath = string.Concat(Environment.CurrentDirectory, @"\ActivityMapper.xml");
			if (File.Exists(xmlPath))
			{
				var focusActivityMapperXml = XDocument.Load(xmlPath);
				var root = focusActivityMapperXml.Root;
				if (root != null)
				{
					_focusActivityLookup = new Dictionary<string, long>();

					foreach (var activitiesNode in root.Elements("activities"))
					{
						foreach (var activity in activitiesNode.Elements("activity"))
						{
							var sourceId = activity.Attribute("source").Value;
							var externalId = activity.Attribute("target").Value;

							if (focusActivitiesByExternalId.ContainsKey(externalId))
							{
								_focusActivityLookup.Add(sourceId, focusActivitiesByExternalId[externalId]);
							}
						}
					}
				}
			}

			if (_focusActivityLookup.IsNullOrEmpty())
				_focusActivityLookup = focusActivitiesByExternalId;
		}

		#region Job Seeker Requests

		/// <summary>
		/// Gets a list of job seeker requests
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new job seekers requests</returns>
		public List<RegisterCareerUserRequest> MigrateJobSeekers(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			var requests = new List<RegisterCareerUserRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastJobSeekerId),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", providerArgs.CustomParm),
						new DatabaseParam("CutoffDate", providerArgs.CutOffDate)
					};

					var dataReader = db.ExecuteReader(
						"EXEC Migrations_JobSeekerRequests @LastId, @BatchSize, @CustomerId, @CutoffDate", 60, dbParams);

					var userNameOrdinal = dataReader.GetOrdinal("UserName");
					var passwordOrdinal = dataReader.GetOrdinal("Passcode");
					var emailAddressOrdinal = dataReader.GetOrdinal("EmailAddress");
					var ssnOrdinal = dataReader.GetOrdinal("SSN");
					var securityQuestionOrdinal = dataReader.GetOrdinal("SecurityQuestion");
					var securityAnswerOrdinal = dataReader.GetOrdinal("SecurityAnswer");
					var lastLoginOrdinal = dataReader.GetOrdinal("LastLogin");
					var registeredDateOrdinal = dataReader.GetOrdinal("RegisteredDate");

					var taggedResumeOrdinal = dataReader.GetOrdinal("Resume");
					var migrationIdOrdinal = dataReader.GetOrdinal("MigrationId");
					var jobSeekerIdOrdinal = dataReader.GetOrdinal("JobSeekerId");

					var jobSeekerExternalIdOrdinal = dataReader.GetOrdinal("ExternalJobSeekerId");

					while (dataReader.Read())
					{
						var request = new RegisterCareerUserRequest
						{
							JobSeekerMigrationId = string.Concat("FC01:", dataReader.GetString(migrationIdOrdinal))
						};

						try
						{
							var resumeXmlText = dataReader.IsDBNull(taggedResumeOrdinal)
								? string.Empty
								: dataReader.GetString(taggedResumeOrdinal);
							var resumeXml = resumeXmlText.Length == 0
								? new XDocument()
								: XDocument.Parse(dataReader.GetString(taggedResumeOrdinal));
							var rootElement = resumeXml.Root ?? new XElement("ResDoc");

							request.InsertPersonId = dataReader.GetInt32(jobSeekerIdOrdinal);
							request.AccountUserName = dataReader.GetString(userNameOrdinal);
							request.AccountPassword = GenerateRandomPassword();
							request.MigratedPasswordHash = dataReader.GetString(passwordOrdinal);
							//request.MigratedPasswordHash = Password.GetMD5Hash("Password" + request.InsertPersonId.ToString());
							request.EmailAddress = dataReader.GetString(emailAddressOrdinal);
							request.SocialSecurityNumber = dataReader.GetString(ssnOrdinal);
							request.SecurityQuestion = dataReader.GetString(securityQuestionOrdinal);
							request.SecurityAnswer = dataReader.GetString(securityAnswerOrdinal);
							request.FirstName = "Unknown";
							request.LastName = "Unknown";
							request.ProgramOfStudyId = null;
							request.DegreeId = null;
							request.EnrollmentStatus = null;
							request.LastLoggedIn = dataReader.GetDateTime(lastLoginOrdinal);
							request.OverrideCreationDate = dataReader.GetDateTime(registeredDateOrdinal);
							request.ExternalId = dataReader.IsDBNull(jobSeekerExternalIdOrdinal)
								? null
								: dataReader.GetString(jobSeekerExternalIdOrdinal);

							var contactElement = rootElement.XPathSelectElement("resume/contact");
							if (contactElement.IsNotNull())
							{
								var nameElement = contactElement.Element("name");
								if (nameElement.IsNotNull())
								{
									request.FirstName = GetChildElementValue(nameElement, "givenname");
									request.MiddleInitial = GetChildElementValue(nameElement, "givenname", 1);
									request.LastName = GetChildElementValue(nameElement, "surname");
								}

								var addressElement = contactElement.Element("address");
								if (addressElement.IsNotNull())
								{
									request.PostalAddress = new Address
									{
										Street1 = GetChildElementValue(addressElement, "street"),
										Street2 = "",
										City = GetChildElementValue(addressElement, "city"),
										StateId = GetLookupId(LookupTypes.States, GetChildElementValue(addressElement, "state")),
										CountryId = GetLookupId(LookupTypes.Countries, GetChildElementValue(addressElement, "country")),
										CountyId = GetLookupId(LookupTypes.Counties, GetChildElementValue(addressElement, "state_county")),
										Zip = GetChildElementValue(addressElement, "postalcode")
									};
								}

								var phoneElement = contactElement.Element("phone");
								if (phoneElement.IsNotNull() && phoneElement.Value.IsNotNullOrEmpty())
								{
									var phoneType = PhoneType.Work;
									var typeAttribute = phoneElement.Attribute("type");
									if (typeAttribute != null && !Enum.TryParse(typeAttribute.Value, true, out phoneType))
										phoneType = PhoneType.Work;

									request.PrimaryPhone = new Phone
									{
										PhoneNumber = FormatPhoneNumber(phoneElement.Value),
										PhoneType = phoneType
									};
								}
							}

							var personalElement = rootElement.XPathSelectElement("resume/statements/personal");
							if (personalElement.IsNotNull())
								request.DateOfBirth = GetDateValueValue(personalElement, "dob");

							var educationElement = rootElement.XPathSelectElement("resume/education");
							if (educationElement != null)
							{
								var enrolmentText = GetChildElementValue(educationElement, "school_status_cd");
								int enrolment;
								if (int.TryParse(enrolmentText, out enrolment))
								{
									if (enrolment >= 0 && enrolment <= 14)
										request.EnrollmentStatus = (SchoolStatus)enrolment;
								}
								else
								{
									if (Enum.IsDefined(typeof(SchoolStatus), enrolmentText))
										request.EnrollmentStatus = (SchoolStatus)Enum.Parse(typeof(SchoolStatus), enrolmentText);
								}
							}
						}
						catch (Exception ex)
						{
							request.MigrationValidationMessage = ex.Message;
						}

						_lastJobSeekerId = dataReader.GetInt32(jobSeekerIdOrdinal);

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateJobSeekers SQL Execution exception.", ex);
			}

			return requests;

		}

		public string XPathGetValue(XElement parentElement, string xpath)
		{
			if (parentElement.IsNull())
				return null;

			var xObject = ((IEnumerable)parentElement.XPathEvaluate(xpath)).Cast<Object>().FirstOrDefault();

			var targetElement = xObject as XElement;
			if (targetElement != null)
				return targetElement.Value;

			var targetAttribute = xObject as XAttribute;
			return targetAttribute != null ? targetAttribute.Value : null;
			/*
				try
				{
					XElement targetElement = parentElement.XPathSelectElement(xpath);
					return targetElement != null ? targetElement.Value : null;
				}
				catch
				{
					XAttribute targetAttribute = ((IEnumerable) parentElement.XPathEvaluate(xpath)).Cast<XAttribute>().First();
					return targetAttribute != null ? targetAttribute.Value : null;
				}
			*/
		}

		/// <summary>
		/// Gets the lookup id.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="lookupType">Type of the lookup.</param>
		/// <param name="value">The value.</param>
		/// <param name="parentId">The parent id.</param>
		/// <returns></returns>
		internal static long? GetLookupId(IRuntimeContext runtimeContext, LookupTypes lookupType, string value, long? parentId = 0)
		{
			var parent = parentId.IsNull() ? 0 : Convert.ToInt64(parentId);

			var lookup = runtimeContext.Helpers.Lookup.GetLookup(lookupType, parent).SingleOrDefault(x => x.ExternalId == value);
			return lookup.IsNull() ? (long?)null : lookup.Id;
		}

		/// <summary>
		/// Gets a list of resume requests
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new resume requests</returns>
		public List<SaveResumeRequest> MigrateResumes(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			var requests = new List<SaveResumeRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastResumeId),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", providerArgs.CustomParm),
						new DatabaseParam("CutoffDate", providerArgs.CutOffDate)
					};

					var dataReader = db.ExecuteReader("EXEC Migrations_ResumeRequests @LastId, @BatchSize, @CustomerId, @CutoffDate",
						dbParams);

					var taggedResumeOrdinal = dataReader.GetOrdinal("TaggedResume");
					var emailAddressOrdinal = dataReader.GetOrdinal("EmailAddress");
					var resumeDateOrdinal = dataReader.GetOrdinal("ResumeDate");

					var resumeNameOrdinal = dataReader.GetOrdinal("ResumeName");
					var resumeDataOrdinal = dataReader.GetOrdinal("ResumeData");
					var fileExtensionOrdinal = dataReader.GetOrdinal("FileExtension");
					var registeredDateOrdinal = dataReader.GetOrdinal("RegisteredDate");

					var migrationIdOrdinal = dataReader.GetOrdinal("MigrationId");
					var jobSeekerMigrationIdOrdinal = dataReader.GetOrdinal("JobSeekerMigrationId");
					var resumeIdOrdinal = dataReader.GetOrdinal("ResumeId");

					while (dataReader.Read())
					{
						var request = new SaveResumeRequest
						{
							ResumeMigrationId = string.Concat("FC01:", dataReader.GetString(migrationIdOrdinal)),
							OverrideCreationDate = dataReader.GetDateTime(registeredDateOrdinal)
						};

						var resumeId = dataReader.GetInt32(resumeIdOrdinal);

						var taggedResume = dataReader.GetString(taggedResumeOrdinal);

						if (taggedResume.Length > 0)
						{
							try
							{
								XDocument resumeDoc;
								var resume = taggedResume.ToResumeModel(RuntimeContext, "**-**", out resumeDoc);

								var resumeElement = resumeDoc.XPathSelectElement("//resume");

								if (resumeElement.XPathSelectElement("//preferences/desired_states/desired_state").IsNotNull())
								{
									var msaPreference = resumeElement.XPathSelectElement("//preferences/desired_states/desired_state").IsNull()
										? null
										: (from m in resumeElement.XPathSelectElements("//preferences/desired_states/desired_state")
											where string.IsNullOrEmpty(XPathGetValue(m, "//code")) == false
											select new DesiredMSA
											{
												MSAId =
													GetLookupId(RuntimeContext, LookupTypes.StateMetropolitanStatisticalAreas, XPathGetValue(m, "msa"),
														GetLookupId(RuntimeContext, LookupTypes.States, XPathGetValue(m, "code"))) ?? 0,
												StateId = GetLookupId(RuntimeContext, LookupTypes.States, XPathGetValue(m, "code")),
												StateName = XPathGetValue(m, "value"),
												StateCode = XPathGetValue(m, "code"),
												Choice = XPathGetValue(m, "choice").AsNull<int>()
											}).FirstOrDefault<DesiredMSA>();

									resume.Special.Preferences.MSAPreference = new List<DesiredMSA> { msaPreference };
								}

								if (resume.ResumeMetaInfo.ResumeName.IsNullOrEmpty())
									resume.ResumeMetaInfo.ResumeName = string.Concat("Resume on ", dataReader.GetDateTime(resumeDateOrdinal).ToString("MM/dd/yyyy HH:mm"));
								// ReSharper disable once ReturnValueOfPureMethodIsNotUsed
								dataReader.GetDateTime(resumeDateOrdinal).ToString("MM/dd/yyyy HH:mm");

								resume.ResumeMetaInfo.ResumeId = resumeId;
								resume.ResumeMetaInfo.ResumeCreationMethod = ResumeCreationMethod.Upload;

								// Assume resumes are always searchable
								if (resume.Special == null)
									resume.Special = new ResumeSpecialInfo();

								if (resume.Special.Preferences == null)
									resume.Special.Preferences = new Preferences();

								if (resume.Special.Preferences.ShiftDetail == null)
									resume.Special.Preferences.ShiftDetail = new Shift();

								resume.Special.Preferences.IsResumeSearchable = true;

								if (resume.Special.Preferences.ShiftDetail.WorkDurationId.IsNotNull())
									resume.Special.Preferences.ShiftDetail.WorkDurationId =
										LookupDurationId(resume.Special.Preferences.ShiftDetail.WorkDurationId.Value);

								// Set any missing names to "Unknown"
								if (resume.ResumeContent != null && resume.ResumeContent.Profile != null &&
								    resume.ResumeContent.Profile.UserGivenName != null)
								{
									var middleName = resume.ResumeContent.Profile.UserGivenName.MiddleName;
									if (middleName != null && middleName.Length > 5)
										resume.ResumeContent.Profile.UserGivenName.MiddleName = middleName.Substring(0, 5);

									var firstName = resume.ResumeContent.Profile.UserGivenName.FirstName;
									if (string.IsNullOrEmpty(firstName))
										resume.ResumeContent.Profile.UserGivenName.FirstName = "Unknown";

									var lastName = resume.ResumeContent.Profile.UserGivenName.LastName;
									if (string.IsNullOrEmpty(lastName))
										resume.ResumeContent.Profile.UserGivenName.LastName = "Unknown";

									if (resume.ResumeContent.Profile.EmailAddress.IsNullOrEmpty())
										resume.ResumeContent.Profile.EmailAddress = dataReader.GetString(emailAddressOrdinal);
								}

								if (resume.ResumeContent != null && resume.ResumeContent.SeekerContactDetails != null)
								{
									// Set any missing names to "Unknown"
									if (resume.ResumeContent.SeekerContactDetails.SeekerName != null)
									{
										var middleName = resume.ResumeContent.SeekerContactDetails.SeekerName.MiddleName;
										if (middleName != null && middleName.Length > 5)
											resume.ResumeContent.SeekerContactDetails.SeekerName.MiddleName = middleName.Substring(0, 5);

										var firstName = resume.ResumeContent.SeekerContactDetails.SeekerName.FirstName;
										if (string.IsNullOrEmpty(firstName))
											resume.ResumeContent.SeekerContactDetails.SeekerName.FirstName = "Unknown";

										var lastName = resume.ResumeContent.SeekerContactDetails.SeekerName.LastName;
										if (string.IsNullOrEmpty(lastName))
											resume.ResumeContent.SeekerContactDetails.SeekerName.LastName = "Unknown";
									}

									// Correct phone numbers
									var phoneNumbers = resume.ResumeContent.SeekerContactDetails.PhoneNumber;
									if (phoneNumbers.IsNotNullOrEmpty() && phoneNumbers.Any(phone => phone.PhoneType == PhoneType.Home))
									{
										var phoneNodes = resumeDoc.Root.XPathSelectElements("/ResDoc/resume/contact/phone").ToList();

										var index = 0;
										foreach (var phoneNode in phoneNodes)
										{
											if (phoneNode.Attribute("type").IsNull() || phoneNode.Attribute("type").Value.IsNullOrEmpty())
											{
												var phoneType = PhoneType.NonUS;

												var previousNode = phoneNode.PreviousNode;
												if (previousNode.IsNotNull() && previousNode.NodeType == XmlNodeType.Text)
												{
													switch (((XText)previousNode).Value.Trim().Replace(":", "").ToLowerInvariant())
													{
														case "home":
															phoneType = PhoneType.Home;
															break;
														case "work":
															phoneType = PhoneType.Work;
															break;
														case "cell":
															phoneType = PhoneType.Cell;
															break;
														case "fax":
															phoneType = PhoneType.Fax;
															break;
													}
												}

												phoneNumbers[index].PhoneType = phoneType;
											}

											index++;
										}
									}

									if (resume.ResumeContent.SeekerContactDetails.EmailAddress.IsNullOrEmpty())
										resume.ResumeContent.SeekerContactDetails.EmailAddress = dataReader.GetString(emailAddressOrdinal);
								}

								if (resume.ResumeContent.IsNotNull() && resume.ResumeContent.SummaryInfo.IsNotNull())
								{
									var summaryInfo = resume.ResumeContent.SummaryInfo;
									summaryInfo.Summary = summaryInfo.Summary.FixDoubleEncoding();
									summaryInfo.AutomatedSummary = summaryInfo.AutomatedSummary.FixDoubleEncoding();
								}

								request.SeekerResume = resume;
								request.JobSeekerMigrationId = string.Concat("FC01:", dataReader.GetString(jobSeekerMigrationIdOrdinal));

								// Fix some issues with veterans
								var veteran = resume.ResumeContent.Profile.Veteran;
								if (veteran.IsNotNull())
								{
									foreach (var history in veteran.History)
									{
										var mocCode = history.MilitaryOccupationCode;
										var mocTitle = history.MilitaryOccupation;

										// Check if code or title is specified, but not both
										if (mocCode.IsNullOrEmpty() && mocTitle.IsNotNullOrEmpty())
										{
											var code = LookupMilitaryCodeByTitle(mocTitle);
											if (code.IsNotNullOrEmpty())
											{
												history.MilitaryOccupationCode = code;
											}
											else
											{
												// Check if the code has actually been entered as the description
												var title = LookupMilitaryTitleByCode(mocTitle);
												if (title.IsNotNullOrEmpty())
												{
													history.MilitaryOccupationCode = mocTitle;
													history.MilitaryOccupation = title;
												}
											}
										}
										else if (mocCode.IsNotNullOrEmpty() && mocTitle.IsNullOrEmpty())
										{
											history.MilitaryOccupation = LookupMilitaryTitleByCode(mocCode);
										}

										// Check if rank has been specified by a name, not external id
										var branchOfServiceId = history.MilitaryBranchOfServiceId;
										if (branchOfServiceId.IsNotNullOrZero() && history.RankId.IsNullOrZero())
										{
											var rankElement = resumeDoc.XPathSelectElement("/ResDoc/resume/statements/personal/veteran/rank");
											if (rankElement.IsNotNull())
												history.RankId = GetLookupId(LookupTypes.MilitaryRanks, rankElement.Value, branchOfServiceId.Value);
										}
									}
								}

								// Fix to races
								IEnumerable<XElement> races = resumeDoc.XPathSelectElements("/ResDoc/resume/statements/personal/ethnic_heritages/ethnic_heritage[not(ethnic_id = '3')]");
								if (races.IsNullOrEmpty())
								{
									if (resume.ResumeContent.Profile.EthnicHeritage.IsNull())
										resume.ResumeContent.Profile.EthnicHeritage = new EthnicHeritage();

									resume.ResumeContent.Profile.EthnicHeritage.RaceIds = new List<long?> { GetLookup(LookupTypes.Races, "NotDisclosed").Id };
								}

								if (resumeDoc.XPathSelectElements("/ResDoc/resume/statements/personal/ethnic_heritages/ethnic_heritage[(ethnic_id = '7')]").Any())
								{
									resume.ResumeContent.Profile.EthnicHeritage.RaceIds = new List<long?> { GetLookup(LookupTypes.Races, "NotDisclosed").Id };
								}

								// Fix to skills
								var skillsElement = resumeDoc.XPathSelectElement("/ResDoc/resume/skills/skills");
								if (skillsElement.IsNotNull())
								{
									var allSkills = skillsElement.Value.Trim();

									if (allSkills.IsNotNull() && allSkills.EndsWith(","))
										allSkills = allSkills.Substring(0, allSkills.LastIndexOf(",", StringComparison.Ordinal));

									resume.ResumeContent.Skills.Skills = allSkills.Split(',').ToList();
								}

								var documentBytes = dataReader.IsDBNull(resumeDataOrdinal)
									? null
									: (byte[])dataReader["ResumeData"];

								if (documentBytes.IsNotNull())
								{
									var fileExtension = dataReader.GetString(fileExtensionOrdinal);
									var contentType = "application/octet-stream";

									if (fileExtension.IsNullOrEmpty())
									{
										switch (fileExtension.ToLower())
										{
											case ".doc":
												contentType = "application/msword";
												break;
											case ".docx":
												contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
												break;
											case ".pdf":
												contentType = "application/pdf";
												break;
											case ".rtf":
												contentType = "text/rtf";
												break;
											case ".txt":
												contentType = "text/text";
												break;
											case ".htm":
											case ".html":
												contentType = "text/html";
												break;
										}
									}

									var resumeXml = new XmlDocument();
									resumeXml.LoadXml(taggedResume);

									var outputString = new StringWriter();
									var output = new XmlTextWriter(outputString);
									_xslt.Transform(resumeXml.CreateNavigator(), output);
									output.Close();
									outputString.Close();

									request.ResumeDocument = new ResumeDocumentDto
									{
										FileName = dataReader.GetString(resumeNameOrdinal),
										ContentType = contentType,
										DocumentBytes = documentBytes,
										Html = outputString.ToString()
									};
								}
								else
								{
									request.SeekerResume.ResumeMetaInfo.ResumeCreationMethod = ResumeCreationMethod.BuildWizard;
								}
							}
							catch (Exception ex)
							{
								request.MigrationValidationMessage = ex.Message;
							}
						}
						else
						{
							request.MigrationValidationMessage = "Empty Resume";
						}

						_lastResumeId = resumeId;

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateResumes SQL Execution exception.", ex);
			}

			return requests;
		}

		/// <summary>
		/// Gets a list of resume document requests
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new resume document requests</returns>
		public List<SaveResumeDocumentRequest> MigrateResumeDocuments(MigrationProviderArgs providerArgs)
		{
			// Resume Document now part of main resume
			return new List<SaveResumeDocumentRequest>();
		}

		/// <summary>
		/// Gets a list of saved searches for job seekers
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of saved search requests</returns>
		public List<SaveSearchRequest> MigrateJobSeekerSavedSearches(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			var requests = new List<SaveSearchRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastSavedSearchId),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", providerArgs.CustomParm),
						new DatabaseParam("CutoffDate", providerArgs.CutOffDate)
					};

					var dataReader =
						db.ExecuteReader("EXEC Migrations_JobSeekerSavedSearchRequests @LastId, @BatchSize, @CustomerId, @CutoffDate",
							dbParams);

					var nameOrdinal = dataReader.GetOrdinal("Name");
					var createdOnOrdinal = dataReader.GetOrdinal("CreatedOn");
					var criteralXmlOrdinal = dataReader.GetOrdinal("CriteriaXml");
					var migrationIdOrdinal = dataReader.GetOrdinal("MigrationId");
					var jobSeekerMigrationIdOrdinal = dataReader.GetOrdinal("JobSeekerMigrationId");
					var savedSearchIdOrdinal = dataReader.GetOrdinal("SavedSearchId");

					while (dataReader.Read())
					{
						var request = new SaveSearchRequest
						{
							SavedSearchMigrationId = string.Concat("FC01:", dataReader.GetString(migrationIdOrdinal))
						};

						try
						{
							request.Name = dataReader.GetString(nameOrdinal);
							request.SearchType = SavedSearchTypes.CareerPostingSearch;
							request.SearchCriteria = BuildJobSearchSearchCriteria(dataReader.GetString(criteralXmlOrdinal));
							request.AlertEmailRequired = false;
							request.AlertEmailFrequency = EmailAlertFrequencies.Daily;
							request.AlertEmailFormat = EmailFormats.HTML;
							request.AlertEmailStatus = AlertStatus.Active;
							request.AlertEmailAddress = "";
							request.OverrideCreatedOn = dataReader.GetDateTime(createdOnOrdinal);
							request.SavedSearchMigrationId = string.Concat("FC01:", dataReader.GetString(migrationIdOrdinal));
							request.JobSeekerMigrationId = string.Concat("FC01:", dataReader.GetString(jobSeekerMigrationIdOrdinal));
						}
						catch (Exception ex)
						{
							request.MigrationValidationMessage = ex.Message;
						}

						_lastSavedSearchId = dataReader.GetInt32(savedSearchIdOrdinal);

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateSavedSearches SQL Execution exception.", ex);
			}

			return requests;
		}

		/// <summary>
		/// Gets a list of search alerts for job seekers
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of search alert requests</returns>
		public List<SaveSearchRequest> MigrateJobSeekerSearchAlerts(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			var requests = new List<SaveSearchRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastSearchAlertId),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", providerArgs.CustomParm),
						new DatabaseParam("CutoffDate", providerArgs.CutOffDate)
					};

					var dataReader =
						db.ExecuteReader("EXEC Migrations_JobSeekerSearchAlertRequests @LastId, @BatchSize, @CustomerId, @CutoffDate",
							dbParams);

					var nameOrdinal = dataReader.GetOrdinal("Name");
					var createdOnOrdinal = dataReader.GetOrdinal("CreatedOn");
					var updatedOnOrdinal = dataReader.GetOrdinal("UpdatedOn");
					var criteralXmlOrdinal = dataReader.GetOrdinal("CriteriaXml");
					var frequencyOrdinal = dataReader.GetOrdinal("Frequency");
					var emailAddressOrdinal = dataReader.GetOrdinal("EmailAddress");
					var emailFormatOrdinal = dataReader.GetOrdinal("Format");
					var migrationIdOrdinal = dataReader.GetOrdinal("MigrationId");
					var jobSeekerMigrationIdOrdinal = dataReader.GetOrdinal("JobSeekerMigrationId");
					var searchAlertIdOrdinal = dataReader.GetOrdinal("SearchAlertId");

					while (dataReader.Read())
					{
						var request = new SaveSearchRequest
						{
							SavedSearchMigrationId = string.Concat("FC01:", dataReader.GetString(migrationIdOrdinal))
						};

						try
						{
							request.Name = dataReader.GetString(nameOrdinal);
							request.SearchType = SavedSearchTypes.CareerPostingSearch;
							request.SearchCriteria = BuildJobSearchSearchCriteria(dataReader.GetString(criteralXmlOrdinal));
							request.AlertEmailRequired = true;
							request.AlertEmailFrequency = dataReader.GetString(frequencyOrdinal) == "DAILY"
								? EmailAlertFrequencies.Daily
								: EmailAlertFrequencies.Weekly;
							request.AlertEmailFormat = dataReader.GetString(emailFormatOrdinal) == "T"
								? EmailFormats.TextOnly
								: EmailFormats.HTML;
							request.AlertEmailStatus = AlertStatus.Active;
							request.AlertEmailAddress = dataReader.GetString(emailAddressOrdinal);
							request.OverrideCreatedOn = dataReader.GetDateTime(createdOnOrdinal);
							request.OverrideUpdatedOn = dataReader.GetDateTime(updatedOnOrdinal);
							request.JobSeekerMigrationId = string.Concat("FC01:", dataReader.GetString(jobSeekerMigrationIdOrdinal));
						}
						catch (Exception ex)
						{
							request.MigrationValidationMessage = ex.Message;
						}

						_lastSearchAlertId = dataReader.GetInt32(searchAlertIdOrdinal);

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateJobSeekerSearchAlerts SQL Execution exception.", ex);
			}

			return requests;
		}

		/// <summary>
		/// Gets a list of viewed postings for job seekers
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of viewed posting requests</returns>
		public List<AddViewedPostingRequest> MigrateViewedPostings(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			var requests = new List<AddViewedPostingRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastViewedPostingId),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", providerArgs.CustomParm),
						new DatabaseParam("CutoffDate", providerArgs.CutOffDate)
					};

					var dataReader =
						db.ExecuteReader("EXEC Migrations_ViewedPostingRequests @LastId, @BatchSize, @CustomerId, @CutoffDate", 300, dbParams);

					var viewedDateOrdinal = dataReader.GetOrdinal("ViewedDate");
					var lensPostingIdOrdinal = dataReader.GetOrdinal("LensPostingId");
					var migrationIdOrdinal = dataReader.GetOrdinal("MigrationId");
					var jobSeekerMigrationIdOrdinal = dataReader.GetOrdinal("JobSeekerMigrationId");
					var jobsViewedIdOrdinal = dataReader.GetOrdinal("JobsViewedId");

					while (dataReader.Read())
					{
						var request = new AddViewedPostingRequest
						{
							ViewedPostingMigrationId = string.Concat("FC01:", dataReader.GetString(migrationIdOrdinal))
						};

						try
						{
							request.ViewedPosting = new ViewedPostingDto
							{
								UserId = 0, // Will be set by import service
								ViewedOn = dataReader.GetDateTime(viewedDateOrdinal),
								LensPostingId = dataReader.GetString(lensPostingIdOrdinal)
							};
							request.JobSeekerMigrationId = string.Concat("FC01:", dataReader.GetString(jobSeekerMigrationIdOrdinal));
						}
						catch (Exception ex)
						{
							request.MigrationValidationMessage = ex.Message;
						}

						_lastViewedPostingId = dataReader.GetInt32(jobsViewedIdOrdinal);

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateViewedPosting SQL Execution exception.", ex);
			}

			return requests;
		}

		/// <summary>
		/// Gets a list of referrals for job seekers
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of referral requests</returns>
		public List<ReferCandidateRequest> MigrateReferrals(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			var requests = new List<ReferCandidateRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastReferralId),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", providerArgs.CustomParm),
						new DatabaseParam("CutoffDate", providerArgs.CutOffDate)
					};

					var dataReader = db.ExecuteReader("EXEC Migrations_Referrals @LastId, @BatchSize, @CustomerId, @CutoffDate", 300,
						dbParams);

					var referralIdOrdinal = dataReader.GetOrdinal("ReferralId");
					var scoreOrdinal = dataReader.GetOrdinal("Score");
					var staffIdOrdinal = dataReader.GetOrdinal("StaffId");
					var statusOrdinal = dataReader.GetOrdinal("Status");
					var referredDateOrdinal = dataReader.GetOrdinal("ReferredDate");
					var appliedDateOrdinal = dataReader.GetOrdinal("DateApplied");
					var migrationIdOrdinal = dataReader.GetOrdinal("MigrationId");

					var jobSeekerMigrationIdOrdinal = dataReader.GetOrdinal("JobSeekerMigrationId");
					var jobOrderMigrationIdOrdinal = dataReader.GetOrdinal("JobOrderMigrationId");

					while (dataReader.Read())
					{
						var request = new ReferCandidateRequest
						{
							ReferralMigrationId = string.Concat("FC01:", dataReader.GetString(migrationIdOrdinal))
						};

						try
						{
							request.PersonId = 0;
							request.JobId = 0;
							request.ApplicationScore = dataReader.GetInt32(scoreOrdinal);
							request.WaivedRequirements = null;
							request.StaffReferral = !dataReader.IsDBNull(staffIdOrdinal);
							request.DateReferred = dataReader.IsDBNull(referredDateOrdinal)
								? (DateTime?)null
								: dataReader.GetDateTime(referredDateOrdinal);
							request.DateApplied = dataReader.IsDBNull(appliedDateOrdinal)
								? (DateTime?)null
								: dataReader.GetDateTime(appliedDateOrdinal);
							request.InitialApplicationStatus = LookupApplicationStatus(dataReader.GetInt32(statusOrdinal));
							request.PostingMigrationId = string.Concat("FC01:", dataReader.GetString(jobOrderMigrationIdOrdinal));
							request.JobSeekerMigrationId = string.Concat("FC01:", dataReader.GetString(jobSeekerMigrationIdOrdinal));
							request.MigrationStaffId = string.Concat("AOSOS:", dataReader.GetString(staffIdOrdinal));

						}
						catch (Exception ex)
						{
							request.MigrationValidationMessage = ex.Message;
						}

						_lastReferralId = dataReader.GetInt32(referralIdOrdinal);

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateReferrals SQL Execution exception.", ex);
			}

			return requests;
		}

		/// <summary>
		/// Gets a list of activities for job seekers
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of activities</returns>
		public List<AssignActivityOrActionRequest> MigrateJobSeekerActivities(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty() || !providerArgs.CustomParm.Contains("-"))
				throw new Exception("Custom Parameter must be set to customer id and EOS id (separated with a hyphen)");

			var requests = new List<AssignActivityOrActionRequest>();
			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var customParms = providerArgs.CustomParm.Split('-');
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastActivityId),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", customParms[0]),
						new DatabaseParam("EOS", customParms[1]),
						new DatabaseParam("CutoffDate", providerArgs.CutOffDate)
					};

					var dataReader =
						db.ExecuteReader("EXEC Migrations_JobSeekerActivities @LastId, @BatchSize, @CustomerId, @EOS, @CutoffDate",
							dbParams);

					var activityIdOrdinal = dataReader.GetOrdinal("ActivityId");
					var customerActivityIdOrdinal = dataReader.GetOrdinal("CustomerActivityId");
					var activityTimeOrdinal = dataReader.GetOrdinal("ActivityTime");
					//var addedByOrdinal = dataReader.GetOrdinal("AddedBy");
					var staffNameOrdinal = dataReader.GetOrdinal("CustomerRepId");
					var migrationIdOrdinal = dataReader.GetOrdinal("MigrationId");
					var jobSeekerMigrationIdOrdinal = dataReader.GetOrdinal("JobSeekerMigrationId");

					while (dataReader.Read())
					{
						var customerActivityId = dataReader.GetString(customerActivityIdOrdinal);

						var request = new AssignActivityOrActionRequest
						{
							ActivityMigrationId = string.Concat("FC01:", dataReader.GetString(migrationIdOrdinal))
						};

						try
						{
							request.PersonId = 0;
							request.ActivityId = _focusActivityLookup.ContainsKey(customerActivityId)
								? _focusActivityLookup[customerActivityId]
								: -1;
							request.ActionTypeId = GetIdForAction(customerActivityId);
							request.ActivityTime = dataReader.GetDateTime(activityTimeOrdinal);
							request.OverrideCreatedOn = request.ActivityTime;
							request.JobSeekerMigrationId = string.Concat("FC01:", dataReader.GetString(jobSeekerMigrationIdOrdinal));

							request.StaffMigrationId = request.StaffSecondaryMigrationId = string.Concat("AOSOS:", dataReader.GetString(staffNameOrdinal));
						}
						catch (Exception ex)
						{
							request.MigrationValidationMessage = ex.Message;
						}

						_lastActivityId = dataReader.GetInt32(activityIdOrdinal);

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("JobSeekerActivities SQL Execution exception.", ex);
			}
			return requests;
		}

		private long GetIdForAction(string customerActivityId)
		{
			return MapCustomerActivityToActionType(customerActivityId);
		}

		private long MapCustomerActivityToActionType(string customerActivityId)
		{
			ActionTypes actionType;
			switch (customerActivityId)
			{
				case "SMRY01":
					actionType = ActionTypes.LogIn;
					break;
				case "RES01":
					actionType = ActionTypes.CompleteResume;
					break;
				default:
					actionType = ActionTypes.NoAction;
					break;
			}

			return actionType != ActionTypes.NoAction ? (long)actionType : -1;
		}

		/// <summary>
		/// Gets a list of flags for job seekers
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of flags</returns>
		public List<ToggleCandidateFlagRequest> MigrateFlaggedJobSeekers(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			var requests = new List<ToggleCandidateFlagRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastFlaggedId),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", providerArgs.CustomParm),
						new DatabaseParam("CutoffDate", providerArgs.CutOffDate)
					};

					var dataReader = db.ExecuteReader(
						"EXEC Migrations_FlaggedJobSeekers @LastId, @BatchSize, @CustomerId, @CutoffDate", dbParams);

					var flaggedIdOrdinal = dataReader.GetOrdinal("ApplicantId");
					var migrationIdOrdinal = dataReader.GetOrdinal("MigrationId");

					var jobSeekerMigrationIdOrdinal = dataReader.GetOrdinal("JobSeekerMigrationId");
					var employeeMigrationIdOrdinal = dataReader.GetOrdinal("EmployeeeMigrationId");

					while (dataReader.Read())
					{
						var request = new ToggleCandidateFlagRequest
						{
							ToggleFlagMigrationId = string.Concat("FC01:", dataReader.GetString(migrationIdOrdinal))
						};

						try
						{
							request.PersonId = 0;
							request.FlaggingPersonId = 0;
							request.JobSeekerMigrationId = string.Concat("FC01:", dataReader.GetString(jobSeekerMigrationIdOrdinal));
							request.EmployeeMigrationId = string.Concat("FC01:", dataReader.GetString(employeeMigrationIdOrdinal));
						}
						catch (Exception ex)
						{
							request.MigrationValidationMessage = ex.Message;
						}

						_lastFlaggedId = dataReader.GetInt32(flaggedIdOrdinal);

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateFlaggedJobSeekers SQL Execution exception.", ex);
			}

			return requests;
		}

		#endregion

		#region IEmployer

		/// <summary>
		/// Gets a list of employer requests to migrate
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of employer requests</returns>
		public List<RegisterTalentUserRequest> MigrateEmployers(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			var requests = new List<RegisterTalentUserRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastEmployeeId),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", providerArgs.CustomParm),
						new DatabaseParam("CutoffDate", providerArgs.CutOffDate)
					};

					var dataReader = db.ExecuteReader(
						"EXEC Migrations_EmployerRequests @LastId, @BatchSize, @CustomerId, @CutoffDate", dbParams);

					var employerExternalIdOrdinal = dataReader.GetOrdinal("EmployerExternalId");
					var employeeExternalIdOrdinal = dataReader.GetOrdinal("EmployeeExternalId");
					var passwordOrdinal = dataReader.GetOrdinal("Passcode");

					while (dataReader.Read())
					{
						var request = new RegisterTalentUserRequest
						{
							EmployeeMigrationId = string.Concat("FC01:", dataReader.GetString(dataReader.GetOrdinal("MigrationId")))
						};

						try
						{
							var ownershipTypeId = LookupOwnershipType(dataReader.GetInt32(dataReader.GetOrdinal("EmployerOwnershipType")));
							var industrialClassification =
								GetIndustrialClassification(dataReader.GetString(dataReader.GetOrdinal("EmployerIndustrialClassification")));
							var employeeStateId = GetLookupId(LookupTypes.States,
								dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressState")), "NJ");
							var employerStateId = GetLookupId(LookupTypes.States,
								dataReader.GetString(dataReader.GetOrdinal("EmployerAddressState")), "NJ");
							var employerCountyId = GetLookupId(LookupTypes.Counties,
								dataReader.GetString(dataReader.GetOrdinal("EmployerAddressCounty")), employerStateId);

							// Focus requires urls to be valid uris
							var url = FormatUrl(dataReader.GetString(dataReader.GetOrdinal("EmployerUrl")), true);

							request.AccountUserName = dataReader.GetString(dataReader.GetOrdinal("UserName"));
							request.AccountPassword = GenerateRandomPassword();
							request.MigratedPasswordHash = dataReader.GetString(passwordOrdinal);
							//request.MigratedPasswordHash = Password.GetMD5Hash("Password" + dataReader.GetString(dataReader.GetOrdinal("MigrationId")));

							request.InitialApprovalStatus = LookupEmployerStatus(dataReader.GetInt32(dataReader.GetOrdinal("EmployerStatus")));

							request.EmployeePerson = new PersonDto
							{
								TitleId = LookupPersonalTitle(dataReader.GetString(dataReader.GetOrdinal("EmployeePersonTitle"))),
								FirstName = dataReader.GetString(dataReader.GetOrdinal("EmployeePersonFirstName")),
								MiddleInitial = dataReader.GetString(dataReader.GetOrdinal("EmployeePersonMiddleInitial")),
								LastName = dataReader.GetString(dataReader.GetOrdinal("EmployeePersonLastName")),
								JobTitle = dataReader.GetString(dataReader.GetOrdinal("EmployeePersonJobTitle")),
								EmailAddress = dataReader.GetString(dataReader.GetOrdinal("EmployeePersonEmailAddress"))
							};
							request.EmployeeAddress = new PersonAddressDto
							{
								Line1 = dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressLine1")),
								Line2 = dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressLine2")),
								TownCity = dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressTownCity")),
								PostcodeZip = dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressPostcodeZip")),
								StateId = employeeStateId,
								CountyId =
									GetLookupId(LookupTypes.Counties, dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressCounty")), "",
										employeeStateId),
								CountryId =
									GetLookupId(LookupTypes.Countries, dataReader.GetString(dataReader.GetOrdinal("EmployeeAddressCountry")), "US"),
								IsPrimary = true
							};

							request.OverrideUserCreatedOn = dataReader.GetDateTime(dataReader.GetOrdinal("EmployeeCreatedOn"));
							request.OverrideUserUpdatedOn = dataReader.GetDateTime(dataReader.GetOrdinal("EmployeeUpdatedOn"));

							request.OverrideEmployerCreatedOn = dataReader.GetDateTime(dataReader.GetOrdinal("EmployerCreatedOn"));
							request.OverrideEmployerUpdatedOn = dataReader.GetDateTime(dataReader.GetOrdinal("EmployerUpdatedOn"));

							var employeePhone = ExtractPhoneNumber(dataReader.GetString(dataReader.GetOrdinal("EmployeePrimaryPhone")));
							request.EmployeePhone = employeePhone.MainNumber;
							request.EmployeePhoneExtension = employeePhone.Extension;
							request.EmployeePhoneType = LookupPhoneType(dataReader.GetString(dataReader.GetOrdinal("EmployeePrimaryPhoneType")));

							request.EmployeeAlternatePhone1 = dataReader.GetString(dataReader.GetOrdinal("EmployeeAlternatePhone1"));
							request.EmployeeAlternatePhone1Type =
								LookupPhoneType(dataReader.GetString(dataReader.GetOrdinal("EmployeeAlternatePhoneType1")));

							request.EmployeeAlternatePhone2 = dataReader.GetString(dataReader.GetOrdinal("EmployeeAlternatePhone2"));
							request.EmployeeAlternatePhone2Type =
								LookupPhoneType(dataReader.GetString(dataReader.GetOrdinal("EmployeeAlternatePhoneType2")));

							request.EmployeeEmailAddress = dataReader.GetString(dataReader.GetOrdinal("EmployeePersonEmailAddress"));

							var employerPhone = ExtractPhoneNumber(dataReader.GetString(dataReader.GetOrdinal("EmployerPrimaryPhone")));
							request.Employer = new EmployerDto
							{
								Id = 0,
								Name = dataReader.GetString(dataReader.GetOrdinal("EmployerName")),
								FederalEmployerIdentificationNumber =
									FormatFEIN(dataReader.GetString(dataReader.GetOrdinal("FederalEmployerIdentificationNumber"))),
								StateEmployerIdentificationNumber =
									dataReader.GetString(dataReader.GetOrdinal("StateEmployerIdentificationNumber")),
								Url = url,
								OwnershipTypeId = ownershipTypeId,
								PrimaryPhone = employerPhone.MainNumber,
								PrimaryPhoneExtension = employerPhone.Extension,
								PrimaryPhoneType = LookupPhoneType(dataReader.GetString(dataReader.GetOrdinal("EmployerPrimaryPhoneType"))),
								AlternatePhone1 = dataReader.GetString(dataReader.GetOrdinal("EmployerAlternatePhone1")),
								AlternatePhone1Type =
									LookupPhoneType(dataReader.GetString(dataReader.GetOrdinal("EmployerAlternatePhoneType1"))),
								AlternatePhone2 = dataReader.GetString(dataReader.GetOrdinal("EmployerAlternatePhone2")),
								AlternatePhone2Type =
									LookupPhoneType(dataReader.GetString(dataReader.GetOrdinal("EmployerAlternatePhoneType2"))),
								IndustrialClassification = industrialClassification,
								CommencedOn = null,
								ApprovalStatus = request.InitialApprovalStatus.GetValueOrDefault(ApprovalStatuses.None),
								ExpiredOn = null,
								TermsAccepted = true,
								IsRegistrationComplete = true,
								IsValidFederalEmployerIdentificationNumber = true,
								CreatedOn = dataReader.GetDateTime(dataReader.GetOrdinal("EmployerCreatedOn")),
								UpdatedOn = dataReader.GetDateTime(dataReader.GetOrdinal("EmployerUpdatedOn"))
							};
							request.EmployeeExternalId = dataReader.IsDBNull(employeeExternalIdOrdinal)
								? null
								: dataReader.GetString(employeeExternalIdOrdinal);
							request.EmployerExternalId = dataReader.IsDBNull(employerExternalIdOrdinal)
								? null
								: dataReader.GetString(employerExternalIdOrdinal);
							request.EmployerAddress = new EmployerAddressDto
							{
								Line1 = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressLine1")),
								Line2 = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressLine2")),
								TownCity = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressTownCity")),
								PostcodeZip = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressPostcodeZip")),
								StateId = employerStateId,
								CountyId = employerCountyId,
								CountryId =
									GetLookupId(LookupTypes.Countries, dataReader.GetString(dataReader.GetOrdinal("EmployerAddressCountry")), "US"),
								IsPrimary = true,
								PublicTransitAccessible = true
							};
							request.BusinessUnit = new BusinessUnitDto
							{
								Id = -1, // -1 is being used to indicate to the Import Service to look up the existing business unit id
								Name = dataReader.GetString(dataReader.GetOrdinal("EmployerName")),
								Url = url,
								OwnershipTypeId = ownershipTypeId,
								PrimaryPhone = employerPhone.MainNumber,
								PrimaryPhoneExtension = employerPhone.Extension,
								PrimaryPhoneType = LookupPhoneType(dataReader.GetString(dataReader.GetOrdinal("EmployerPrimaryPhoneType"))),
								AlternatePhone1 = dataReader.GetString(dataReader.GetOrdinal("EmployerAlternatePhone1")),
								AlternatePhone1Type = LookupPhoneType(dataReader.GetString(dataReader.GetOrdinal("EmployerAlternatePhoneType1"))),
								AlternatePhone2 = dataReader.GetString(dataReader.GetOrdinal("EmployerAlternatePhone2")),
								AlternatePhone2Type = LookupPhoneType(dataReader.GetString(dataReader.GetOrdinal("EmployerAlternatePhoneType2"))),
								IsPrimary = true,
								IndustrialClassification = industrialClassification,
							};
							request.BusinessUnitDescription = new BusinessUnitDescriptionDto
							{
								Title = dataReader.GetString(dataReader.GetOrdinal("EmployerDescriptionTitle")),
								Description = UnescapeText(dataReader.GetString(dataReader.GetOrdinal("EmployerDescription")))
							};
							request.BusinessUnitAddress = new BusinessUnitAddressDto
							{
								Line1 = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressLine1")),
								Line2 = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressLine2")),
								TownCity = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressTownCity")),
								PostcodeZip = dataReader.GetString(dataReader.GetOrdinal("EmployerAddressPostcodeZip")),
								StateId = employerStateId,
								CountyId = employerCountyId,
								CountryId =
									GetLookupId(LookupTypes.Countries, dataReader.GetString(dataReader.GetOrdinal("EmployerAddressCountry")), "US"),
								IsPrimary = true,
								PublicTransitAccessible = true
							};

							// Default Business Unit Description if empty
							if (request.BusinessUnitDescription.Title.IsNullOrEmpty())
								request.BusinessUnitDescription.Title = "Untitled";

							if (request.EmployeePhone.IsNullOrEmpty())
							{
								request.EmployeePhone = request.Employer.PrimaryPhone;
								request.EmployeePhoneExtension = request.Employer.PrimaryPhoneExtension;
								request.EmployeePhoneType = request.Employer.PrimaryPhoneType;
							}
						}
						catch (Exception ex)
						{
							request.MigrationValidationMessage = ex.Message;
						}

						_lastEmployeeId = dataReader.GetInt32(dataReader.GetOrdinal("EmployerRepId"));

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateEmployers SQL Execution exception.", ex);
			}

			return requests;
		}

		/// <summary>
		/// Gets a list of secondary business unit descriptions requests to set up
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new business unit descriptions</returns>
		public List<SaveBusinessUnitDescriptionRequest> MigrateBusinessUnitDescriptions(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			var requests = new List<SaveBusinessUnitDescriptionRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastBusinessUnitDescriptionId),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", providerArgs.CustomParm)
					};

					var dataReader =
						db.ExecuteReader("EXEC Migrations_BusinessUnitDescriptionRequests @LastId, @BatchSize, @CustomerId", dbParams);

					while (dataReader.Read())
					{
						var request = new SaveBusinessUnitDescriptionRequest
						{
							BusinessUnitDescriptionMigrationId =
								string.Concat("FC01:", dataReader.GetString(dataReader.GetOrdinal("MigrationId")))
						};

						try
						{
							request.BusinessUnitDescription = new BusinessUnitDescriptionDto
							{
								Title = dataReader.GetString(dataReader.GetOrdinal("Title")),
								Description = UnescapeText(dataReader.GetString(dataReader.GetOrdinal("Description"))),
								IsPrimary = dataReader.GetBoolean(dataReader.GetOrdinal("IsPrimary"))
							};
							request.EmployeeMigrationId = string.Concat("FC01:",
								dataReader.GetString(dataReader.GetOrdinal("EmployeeMigrationId")));

							// Default Business Unit Description if empty
							if (request.BusinessUnitDescription.Title.IsNullOrEmpty())
								request.BusinessUnitDescription.Title = "Untitled";
						}
						catch (Exception ex)
						{
							request.MigrationValidationMessage = ex.Message;
						}

						_lastBusinessUnitDescriptionId = dataReader.GetInt32(dataReader.GetOrdinal("DescriptionId"));

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateEmployers SQL Execution exception.", ex);
			}

			return requests;
		}


		/// <summary>
		/// Gets a list of secondary business unit logo requests to set up
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new business unit logos</returns>
		public List<SaveBusinessUnitLogoRequest> MigrateBusinessUnitLogos(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			var requests = new List<SaveBusinessUnitLogoRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastBusinessUnitLogoId),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", providerArgs.CustomParm)
					};

					var dataReader = db.ExecuteReader("EXEC Migrations_BusinessUnitLogoRequests @LastId, @BatchSize, @CustomerId",
						dbParams);

					while (dataReader.Read())
					{
						var request = new SaveBusinessUnitLogoRequest
						{
							BusinessUnitLogoMigrationId = string.Concat("FC01:", dataReader.GetString(dataReader.GetOrdinal("MigrationId")))
						};

						var bytes = Convert.FromBase64String(dataReader.GetString(dataReader.GetOrdinal("Logo")));
						if (bytes.Length > 13)
						{
							try
							{
								request.BusinessUnitLogo = new BusinessUnitLogoDto
								{
									Name = dataReader.GetString(dataReader.GetOrdinal("Name")),
									Logo = ConvertImageBytesToGif(bytes)
								};

								request.EmployeeMigrationId = string.Concat("FC01:",
									dataReader.GetString(dataReader.GetOrdinal("EmployeeMigrationId")));
							}
							catch (Exception ex)
							{
								request.MigrationValidationMessage = ex.Message;
							}
						}
						else
						{
							request.MigrationValidationMessage = "Invalid logo";
						}

						requests.Add(request);

						_lastBusinessUnitLogoId = dataReader.GetInt32(dataReader.GetOrdinal("LogoId"));
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateEmployers SQL Execution exception.", ex);
			}

			return requests;
		}

		/// <summary>
		/// Gets a list of resume searches requests to set up
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new resume saved searches</returns>
		public List<SaveCandidateSavedSearchRequest> MigrateResumeSearches(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			var requests = new List<SaveCandidateSavedSearchRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastResumeSearchId),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", providerArgs.CustomParm),
						new DatabaseParam("CutoffDate", providerArgs.CutOffDate)
					};

					var dataReader =
						db.ExecuteReader("EXEC Migrations_EmployeeResumeSearchRequests @LastId, @BatchSize, @CustomerId, @CutoffDate",
							dbParams);

					var nameOrdinal = dataReader.GetOrdinal("Name");
					var criteralXmlOrdinal = dataReader.GetOrdinal("CriteriaXml");
					var migrationIdOrdinal = dataReader.GetOrdinal("MigrationId");
					var employeeMigrationIdOrdinal = dataReader.GetOrdinal("EmployeeMigrationId");
					var resumeSearchIdOrdinal = dataReader.GetOrdinal("SavedSearchId");

					while (dataReader.Read())
					{
						var request = new SaveCandidateSavedSearchRequest
						{
							SavedSearchMigrationId = string.Concat("FC01:", dataReader.GetString(migrationIdOrdinal))
						};

						try
						{
							request.Name = dataReader.GetString(nameOrdinal);
							request.SearchType = SavedSearchTypes.TalentCandidateSearch;
							request.Criteria = BuildResumeSearchCriteria(dataReader.GetString(criteralXmlOrdinal));
							request.AlertEmailRequired = false;
							request.AlertEmailFrequency = EmailAlertFrequencies.Daily;
							request.AlertEmailFormat = EmailFormats.HTML;
							request.AlertEmailAddress = "";
							request.EmployeeMigrationId = string.Concat("FC01:", dataReader.GetString(employeeMigrationIdOrdinal));
						}
						catch (Exception ex)
						{
							request.MigrationValidationMessage = ex.Message;
						}

						_lastResumeSearchId = dataReader.GetInt32(resumeSearchIdOrdinal);

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateResumeSearches SQL Execution exception.", ex);
			}

			return requests;
		}

		/// <summary>
		/// Gets a list of resume alerts requests to set up
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new resume alerts searches</returns>
		public List<SaveCandidateSavedSearchRequest> MigrateResumeAlerts(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			var requests = new List<SaveCandidateSavedSearchRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastResumeAlertId),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", providerArgs.CustomParm),
						new DatabaseParam("CutoffDate", providerArgs.CutOffDate)
					};

					var dataReader =
						db.ExecuteReader("EXEC Migrations_EmployeeResumeAlertRequests @LastId, @BatchSize, @CustomerId, @CutoffDate",
							dbParams);

					var nameOrdinal = dataReader.GetOrdinal("Name");
					var criteralXmlOrdinal = dataReader.GetOrdinal("CriteriaXml");
					var frequencyOrdinal = dataReader.GetOrdinal("Frequency");
					var emailAddressOrdinal = dataReader.GetOrdinal("EmailAddress");
					var emailFormatOrdinal = dataReader.GetOrdinal("Format");
					var migrationIdOrdinal = dataReader.GetOrdinal("MigrationId");
					var employeeMigrationIdOrdinal = dataReader.GetOrdinal("EmployeeMigrationId");
					var resumeAlertIdOrdinal = dataReader.GetOrdinal("SearchAlertId");

					while (dataReader.Read())
					{
						var request = new SaveCandidateSavedSearchRequest
						{
							SavedSearchMigrationId = string.Concat("FC01:", dataReader.GetString(migrationIdOrdinal))
						};

						try
						{
							request.Name = dataReader.GetString(nameOrdinal);
							request.SearchType = SavedSearchTypes.TalentCandidateSearch;
							request.Criteria = BuildResumeSearchCriteria(dataReader.GetString(criteralXmlOrdinal));
							request.AlertEmailRequired = true;
							request.AlertEmailFrequency = dataReader.GetString(frequencyOrdinal) == "DAILY"
								? EmailAlertFrequencies.Daily
								: EmailAlertFrequencies.Weekly;
							request.AlertEmailFormat = dataReader.GetString(emailFormatOrdinal) == "T"
								? EmailFormats.TextOnly
								: EmailFormats.HTML;
							request.AlertEmailAddress = dataReader.GetString(emailAddressOrdinal);
							request.EmployeeMigrationId = string.Concat("FC01:", dataReader.GetString(employeeMigrationIdOrdinal));
						}
						catch (Exception ex)
						{
							request.MigrationValidationMessage = ex.Message;
						}

						_lastResumeAlertId = dataReader.GetInt32(resumeAlertIdOrdinal);

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateResumeAlerts SQL Execution exception.", ex);
			}

			return requests;
		}

		#endregion

		#region ISaveJobRequest

		/// <summary>
		/// Gets a list of job requests to migrate
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of job requests</returns>
		public List<SaveJobRequest> MigrateJobOrders(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			var requests = new List<SaveJobRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastJobId),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", providerArgs.CustomParm),
						new DatabaseParam("CutoffDate", providerArgs.CutOffDate)
					};

					var dataReader = db.ExecuteReader(
						"EXEC Migrations_JobOrderRequests @LastId, @BatchSize, @CustomerId, @CutoffDate", dbParams);

					var jobXmlOrdinal = dataReader.GetOrdinal("JobXml");
					var jobTitleOrdinal = dataReader.GetOrdinal("JobTitle");
					var statusOrdinal = dataReader.GetOrdinal("Status");
					var descriptionOrdinal = dataReader.GetOrdinal("Description");
					var mainSiteOrdinal = dataReader.GetOrdinal("IsMainSite");
					var createdOnOrdinal = dataReader.GetOrdinal("CreatedOn");
					var updatedOnOrdinal = dataReader.GetOrdinal("UpdatedOn");
					var postedOnOrdinal = dataReader.GetOrdinal("PostedOn");
					var closingOnOrdinal = dataReader.GetOrdinal("ClosingOn");
					var closedOnOrdinal = dataReader.GetOrdinal("ClosedOn");
					var approvedOnOrdinal = dataReader.GetOrdinal("ApprovedOn");
					var numberOfOpeningsOrdinal = dataReader.GetOrdinal("NumberOfOpenings");
					var externalIdOrdinal = dataReader.GetOrdinal("ExternalId");
					var employerStatusOrdinal = dataReader.GetOrdinal("EmployerStatus");
					var lensJobIdOrdinal = dataReader.GetOrdinal("LensJobId");
					var jobMigrationIdOrdinal = dataReader.GetOrdinal("JobMigrationId");
					var employeeMigrationIdOrdinal = dataReader.GetOrdinal("EmployeeMigrationId");
					var employerIdOrdinal = dataReader.GetOrdinal("EmployerId");
					var postingIdOrdinal = dataReader.GetOrdinal("PostingId");

					while (dataReader.Read())
					{
						var request = new SaveJobRequest
						{
							Module = FocusModules.General,
							JobMigrationId = string.Concat("FC01:", dataReader.GetInt32(jobMigrationIdOrdinal).ToString(CultureInfo.InvariantCulture))
						};

						try
						{
							var postingXml = XDocument.Parse(dataReader.GetString(jobXmlOrdinal));
							var postingElement = ((postingXml.Root == null) ? new XElement("posting") : postingXml.Root.Element("posting")) ??
							                     new XElement("posting");
							var requiredElement = postingElement.Descendants("required").FirstOrDefault() ?? new XElement("required");
							var benefitsElement = postingElement.Descendants("benefits").FirstOrDefault() ?? new XElement("benefits");
							var miscellaneousElement = benefitsElement.Descendants("miscellaneous").FirstOrDefault() ??
							                           new XElement("miscellaneous");

							var status = dataReader.GetInt32(statusOrdinal);

							request.LensPostingMigrationId = dataReader.GetString(lensJobIdOrdinal);

							request.Job = new JobDto
							{
								EmployerId = 0,
								EmployeeId = 0,
								JobStatus = MapJobStatus(status, !String.IsNullOrWhiteSpace(request.LensPostingMigrationId)),
								JobTitle = dataReader.GetString(jobTitleOrdinal),
								ApprovalStatus = MapApprovalStatus(status),
								BusinessUnitId = 0,
								BusinessUnitDescriptionId = 0,
								EmployerDescriptionPostingPosition = EmployerDescriptionPostingPositions.BelowJobPosting,
								BusinessUnitLogoId = null,
								IsConfidential = false,
								Description = dataReader.GetString(descriptionOrdinal),
								MinimumEducationLevel = EducationLevels.None,
								MinimumEducationLevelRequired = null,
								MinimumExperience = null,
								MinimumExperienceMonths = null,
								MinimumExperienceRequired = null,
								MinimumAge = null,
								MinimumAgeReason = "",
								MinimumAgeRequired = false,
								DrivingLicenceClassId = null,
								DrivingLicenceRequired = false,
								LicencesRequired = GetBooleanValue(requiredElement.Element("licenses"), "license_mandate"),
								CertificationRequired = GetBooleanValue(requiredElement.Element("certifications"), "certification_mandate"),
								LanguagesRequired = GetBooleanValue(requiredElement.Element("languages"), "language_mandate"),
								JobLocationType =
									(dataReader.GetInt32(mainSiteOrdinal) == 1 ? JobLocationTypes.MainSite : JobLocationTypes.OtherLocation),
								FederalContractor = GetBooleanValue(requiredElement, "federal_contractor"),
								FederalContractorExpiresOn = null,
								ForeignLabourCertificationH2A = false,
								ForeignLabourCertificationH2B = false,
								ForeignLabourCertificationOther = GetBooleanValue(requiredElement, "foreign_labor_certification"),
								CourtOrderedAffirmativeAction = GetBooleanValue(requiredElement, "Court_ordered_affirmative_action"),
								WorkOpportunitiesTaxCreditHires = LookupTaxCreditCategories(requiredElement),
								HiringFromTaxCreditProgramNotificationSent = null,
								PostingFlags = LookupJobPostingFlags(postingElement.Element("flag")),
								MinSalary = null,
								MaxSalary = null,
								SalaryFrequencyId = null,
								HideSalaryOnPosting = false,
								EmploymentStatusId = LookupJobEmploymentStatus(postingElement.Element("jobduration")),
								HoursPerWeek = null,
								NormalWorkDays = DaysOfWeek.None,
								WorkDaysVary = null,
								OverTimeRequired = GetBooleanValue(benefitsElement, "workovertime"),
								IsCommissionBased = false,
								IsSalaryAndCommissionBased = false,
								NormalWorkShiftsId = LookupNormalWorkShifts(benefitsElement.Element("workshifts")),
								JobTypeId = LookupJobType(postingElement.Element("jobduration")),
								JobStatusId = null,
								LeaveBenefits = LookupLeaverBenefits(benefitsElement.Element("leave")),
								RetirementBenefits = LookupRetirementBenefits(benefitsElement.Element("retirement")),
								InsuranceBenefits = LookupInsuranceBenefits(benefitsElement.Element("insurance")),
								MiscellaneousBenefits = LookupMiscellaneousBenefits(miscellaneousElement),
								OtherBenefitsDetails = GetChildElementValue(miscellaneousElement, "other"),
								CreatedOn = dataReader.GetDateTime(createdOnOrdinal),
								CreatedBy = 0,
								UpdatedOn = dataReader.GetDateTime(updatedOnOrdinal),
								UpdatedBy = 0,
								PostedOn = dataReader.IsDBNull(postedOnOrdinal) ? (DateTime?)null : dataReader.GetDateTime(postedOnOrdinal),
								PostedBy = null,
								ClosingOn = dataReader.IsDBNull(closingOnOrdinal) ? (DateTime?)null : dataReader.GetDateTime(closingOnOrdinal),
								ClosedOn = dataReader.IsDBNull(closedOnOrdinal) ? (DateTime?)null : dataReader.GetDateTime(closedOnOrdinal),
								ClosedBy = null,
								ApprovedOn = dataReader.GetDateTime(approvedOnOrdinal),
								ApprovedBy = null,
								NumberOfOpenings = dataReader.GetInt32(numberOfOpeningsOrdinal),
								InterviewContactPreferences = ContactMethods.None,
								InterviewEmailAddress = "",
								InterviewApplicationUrl = "",
								InterviewMailAddress = "",
								InterviewFaxNumber = "",
								InterviewPhoneNumber = "",
								InterviewDirectApplicationDetails = "",
								InterviewOtherInstructions = "",
								ScreeningPreferences = ScreeningPreferences.AllowUnqualifiedApplications,
								WizardStep = 7,
								WizardPath = 1,
								ExternalId = dataReader.IsDBNull(externalIdOrdinal) ? null : dataReader.GetString(externalIdOrdinal),
								PreScreeningServiceRequest = false
								//Posting = ""
							};

							if (request.Job.JobTitle.IsNullOrEmpty())
								request.Job.JobTitle = "(Not specified)";

							request.EmployeeMigrationId = string.Concat("FC01:",
								dataReader.GetInt32(employeeMigrationIdOrdinal).ToString(CultureInfo.InvariantCulture));
							request.GeneratePostingForNewJob = true;

							if (postingXml.Root.IsNotNull())
							{
								var specialElement = postingXml.Root.Element("special");
								if (specialElement.IsNotNull())
								{
									var officeElement = specialElement.Element("office_id");
									if (officeElement.IsNotNull())
										request.OfficeMigrationId = officeElement.Value;
								}
							}

							// Get the OnetId
							long? onetId = null;
							var titleElement = postingElement.Descendants("title").FirstOrDefault();
							if (titleElement.IsNotNull())
							{
								var onetAttr = GetAttributeValue(titleElement, "onet");
								if (onetAttr.Length > 0)
								{
									var onetCode = onetAttr.Split('|');
									onetId = GetOnetIdByCode(onetCode[0]);
								}
							}
							request.Job.OnetId = onetId ?? GetOnetIdByPhrase(GetChildElementValue(postingElement, "generic_title"));

							// Get 'duties' in place of description
							var duties = GetChildElementTextNodeValue(postingElement, "duties");
							if (duties.Length > 0)
								request.Job.Description = duties;

							// Set some dates to null if year is 1900
							request.Job.ClosedOn = (request.Job.ClosedOn.HasValue && request.Job.ClosedOn.Value.Year == 1900)
								? null
								: request.Job.ClosedOn;
							request.Job.ClosingOn = (request.Job.ClosingOn.HasValue && request.Job.ClosingOn.Value.Year == 1900)
								? null
								: request.Job.ClosingOn;
							request.Job.PostedOn = (request.Job.PostedOn.HasValue && request.Job.PostedOn.Value.Year == 1900)
								? null
								: request.Job.PostedOn;

							// Job Description
							var jobPropertyElement = postingElement.Element("company");
							if (jobPropertyElement != null)
							{
								request.Job.EmployerDescriptionPostingPosition =
									(GetChildElementValue(jobPropertyElement, "description_position") == "1")
										? EmployerDescriptionPostingPositions.BelowJobPosting
										: EmployerDescriptionPostingPositions.AboveJobPosting;

								request.Job.IsConfidential = GetBooleanValue(jobPropertyElement, "confidential");
								request.EmployerMigrationLogoId = LookupLogoId(dataReader.GetInt32(employerIdOrdinal),
									GetChildElementValue(jobPropertyElement, "logo"));
							}

							// Education Level
							jobPropertyElement = requiredElement.Element("education");
							if (jobPropertyElement != null)
							{
								request.Job.MinimumEducationLevel =
									LookUpMinimumEducationLevel(GetChildElementValue(jobPropertyElement, "level"));
								request.Job.MinimumEducationLevelRequired = GetMandatoryValue(jobPropertyElement);
							}

							// Minimum Experience
							jobPropertyElement = requiredElement.Element("experience");
							if (jobPropertyElement != null)
							{
								request.Job.MinimumExperience = GetIntegerValue(jobPropertyElement, "years");
								request.Job.MinimumExperienceMonths = GetIntegerValue(jobPropertyElement, "months");
								request.Job.MinimumExperienceRequired = GetMandatoryValue(jobPropertyElement);
							}

							// Minimum Age
							jobPropertyElement = requiredElement.Element("age");
							if (jobPropertyElement != null)
							{
								request.Job.MinimumAge = GetIntegerValue(jobPropertyElement, "years");
								request.Job.MinimumAgeReason = GetChildElementValue(jobPropertyElement, "reason");
								if (request.Job.MinimumAgeReason.IsNotNullOrEmpty())
									request.Job.MinimumAgeReasonValue = 4;
								request.Job.MinimumAgeRequired = GetMandatoryValue(jobPropertyElement);
							}

							// Driving Licenses
							jobPropertyElement = requiredElement.Descendants("drivers").FirstOrDefault();
							if (jobPropertyElement != null)
							{
								request.Job.DrivingLicenceClassId = GetLookupId(LookupTypes.DrivingLicenceClasses,
									GetChildElementValue(jobPropertyElement, "class"));
								request.Job.DrivingLicenceRequired = GetMandatoryValue(jobPropertyElement);
							}

							// Salary
							jobPropertyElement = benefitsElement.Element("salary");
							if (jobPropertyElement != null)
							{
								request.Job.MinSalary = GetDecimalValue(jobPropertyElement, "min", true);
								request.Job.MaxSalary = GetDecimalValue(jobPropertyElement, "max", true);
								request.Job.SalaryFrequencyId = LookupSalaryFrequency(jobPropertyElement.Element("duration"));
								request.Job.HideSalaryOnPosting = GetBooleanValue(jobPropertyElement, "hidesalary");
							}

							// Hours and Days
							jobPropertyElement = benefitsElement.Element("workdays");
							if (jobPropertyElement != null)
							{
								request.Job.HoursPerWeek = GetIntegerValue(jobPropertyElement, "hoursperweek");
								request.Job.NormalWorkDays = LookupWeekDays(jobPropertyElement);
								request.Job.WorkDaysVary = GetBooleanValue(jobPropertyElement, "varies");
							}

							//Interview Contact Preferences
							var preferencesElement = postingElement.Descendants("preference").FirstOrDefault();
							if (preferencesElement.IsNotNull())
							{
								jobPropertyElement = preferencesElement.Element("contact_method");
								if (jobPropertyElement != null)
									request.Job.InterviewContactPreferences = LookupContactPreferences(request.Job, jobPropertyElement);

								//Screening Preferences
								jobPropertyElement = preferencesElement.Element("screening");
								if (jobPropertyElement != null)
									request.Job.ScreeningPreferences = LookupScreeningPreferences(jobPropertyElement);
							}

							var employerStatus = LookupEmployerStatus(dataReader.GetInt32(employerStatusOrdinal));
							if (employerStatus == ApprovalStatuses.WaitingApproval && request.Job.JobStatus != JobStatuses.Draft)
								request.Job.JobStatus = JobStatuses.AwaitingEmployerApproval;

							if (request.Job.JobStatus.IsIn(JobStatuses.Active, JobStatuses.OnHold, JobStatuses.Closed) && request.LensPostingMigrationId.IsNullOrEmpty())
								request.Job.JobStatus = JobStatuses.Draft;

							// Update Job Status to closed
							if (request.Job.JobStatus == JobStatuses.Active)
							{
								if (request.Job.ClosingOn.HasValue && request.Job.ClosingOn < DateTime.Now)
								{
									request.Job.JobStatus = JobStatuses.Closed;
									if (request.Job.ClosedOn == null)
										request.Job.ClosedOn = request.Job.ClosingOn;
								}
							}

							if (request.Job.JobStatus != JobStatuses.Closed)
								request.Job.ClosedOn = null;

							// Job Locations
							var locationsElement = postingXml.Descendants("multiplelocation").FirstOrDefault() ??
							                       new XElement("multiplelocation");
							var locationElement = locationsElement.Element("location");
							if (locationElement != null)
							{
								var cityname = GetChildElementValue(locationElement, "cityname");
								var zip = GetChildElementValue(locationElement, "zipcode");
								var stateCode = GetChildElementValue(locationElement, "statecode");
								var state = GetLookup(LookupTypes.States, stateCode);

								var location = new JobLocationDto
								{
									JobId = 0,
									IsPublicTransitAccessible = GetBooleanValue(locationElement, "public_transit"),
									Location = (state != null)
										? string.Format("{0}, {1} ({2})", cityname, stateCode, zip)
										: string.Format("{0}, ({1})", cityname, zip)
								};

								request.JobLocations = new List<JobLocationDto> { location };
							}
							else
							{
								request.JobLocations = new List<JobLocationDto>();
							}

							// Job Address
							request.JobAddress = new JobAddressDto
							{
								JobId = 0,
								Line1 = dataReader.GetString(dataReader.GetOrdinal("JobAddressLine1")),
								Line2 = "",
								Line3 = "",
								TownCity = dataReader.GetString(dataReader.GetOrdinal("JobAddressTownCity")),
								CountyId = null,
								PostcodeZip = dataReader.GetString(dataReader.GetOrdinal("JobAddressPostcodeZip")),
								StateId = GetLookupId(LookupTypes.States, dataReader.GetString(dataReader.GetOrdinal("JobAddressState")), "NJ"),
								CountryId = GetLookupId(LookupTypes.Countries, "US", ""),
								IsPrimary = true
							};

							// Job Certificates
							var certificationsElement = postingXml.Descendants("certifications").FirstOrDefault();
							if (certificationsElement.IsNotNull())
							{
								request.JobCertificates = (from certificationElement in certificationsElement.Elements("certification")
									select GetChildElementValue(certificationElement, "name")
									into certName
									where certName.Length > 0
									select new JobCertificateDto
									{
										JobId = 0,
										Certificate = certName
									}).ToList();
							}
							else
							{
								request.JobCertificates = new List<JobCertificateDto>();
							}

							// Job Licences
							var licencesElement = postingXml.Descendants("licenses").FirstOrDefault();
							if (licencesElement.IsNotNull())
							{
								request.JobLicences = licencesElement.Elements("license")
									.Where(l => l.Value.Length > 0)
									.Select
									(
										licenceElement => new JobLicenceDto
										{
											JobId = 0,
											Licence = licenceElement.Value
										}
									).ToList();
							}
							else
							{
								request.JobLicences = new List<JobLicenceDto>();
							}

							// Job Languages
							var languagesElement = postingXml.Descendants("languages").FirstOrDefault();
							if (languagesElement.IsNotNull())
							{
								request.JobLanguages = (from languageElement in languagesElement.Elements("language")
									select GetChildElementValue(languageElement, "name")
									into languageName
									where languageName.Length > 0
									select new JobLanguageDto
									{
										JobId = 0,
										Language = languageName
									}).ToList();
							}
							else
							{
								request.JobLanguages = new List<JobLanguageDto>();
							}

							// Job Special Requirements
							var specialRequirementsElement = postingXml.Descendants("specialrequirements").FirstOrDefault();
							if (specialRequirementsElement.IsNotNull())
							{
								request.JobSpecialRequirements = specialRequirementsElement.Elements("requirement")
									.Where(r => r.Value.Length > 0)
									.Select
									(
										specialRequirementElement => new JobSpecialRequirementDto
										{
											JobId = 0,
											Requirement = Regex.Replace(specialRequirementElement.Value, @"\s*Applicant must\s*", "").TruncateString(400)
										}
									).ToList();
							}
							else
							{
								request.JobSpecialRequirements = new List<JobSpecialRequirementDto>();
							}

							// Driving licencse endorsements
							var driversElement = postingXml.Descendants("drivers").FirstOrDefault() ?? new XElement("drivers");
							var endorsements = LookupDrivingLicenceEndorsements(driversElement.Element("endorsements"));

							if (endorsements.Count > 0)
								request.JobDrivingLicenceEndorsements = endorsements;


							// Job Programs of Study
							request.JobProgramsOfStudy = new List<JobProgramOfStudyDto>();
						}
						catch (Exception ex)
						{
							request.MigrationValidationMessage = ex.Message;
						}

						_lastJobId = dataReader.GetInt32(postingIdOrdinal);

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateJobOrders SQL Execution exception.", ex);
			}

			return requests;
		}

		/// <summary>
		/// Gets a list of job certificate requests
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new job certificate requests</returns>
		public List<SaveJobCertificatesRequest> MigrateJobCertificates(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			// Certificates now exported as part of main job
			return new List<SaveJobCertificatesRequest>();
		}

		/// <summary>
		/// Gets a list of job language requests
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new job language requests</returns>
		public List<SaveJobLanguagesRequest> MigrateJobLanguages(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			// Languages now part of main job
			return new List<SaveJobLanguagesRequest>();

		}

		/// <summary>
		/// Gets a list of job licence requests
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new job licence requests</returns>
		public List<SaveJobLicencesRequest> MigrateJobLicences(MigrationProviderArgs providerArgs)
		{
			// Job Licences now migrated as part of main job
			return new List<SaveJobLicencesRequest>();

		}

		/// <summary>
		/// Gets a list of job special requirements requests
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new job special requirements requests</returns>
		public List<SaveJobSpecialRequirementsRequest> MigrateJobSpecialRequirements(MigrationProviderArgs providerArgs)
		{
			// Special Requirements now part of main job
			return new List<SaveJobSpecialRequirementsRequest>();
		}

		/// <summary>
		/// Gets a list of job locations requests
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new job location requests</returns>
		public List<SaveJobLocationsRequest> MigrateJobLocations(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			// Job locations now done as part of main save request
			return new List<SaveJobLocationsRequest>();

		}

		/// <summary>
		/// Gets a list of job address requests
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new job address requests</returns>
		public List<SaveJobAddressRequest> MigrateJobAddresses(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			// Job Address now migrated as part of main job
			return new List<SaveJobAddressRequest>();

		}

		/// <summary>
		/// Gets a list of job program of study requests (Not used in V1)
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new job program of study requests</returns>
		public List<SaveJobProgramsOfStudyRequest> MigrateJobProgramsOfStudy(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			return new List<SaveJobProgramsOfStudyRequest>();
		}

		/// <summary>
		/// Migrates the job driving licence endorsements.
		/// </summary>
		/// <param name="providerArgs">The provider arguments.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">MigrateJobLicences SQL Execution exception.</exception>
		public List<SaveJobDrivingLicenceEndorsementsRequest> MigrateJobDrivingLicenceEndorsements(
			MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			return new List<SaveJobDrivingLicenceEndorsementsRequest>();
		}

		#endregion

		#region Assist Data

		/// <summary>
		/// Gets a random number of user requests to set up
		/// </summary>
		/// <returns>A list of new users</returns>
		public List<CreateAssistUserRequest> MigrateUsers(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			var requests = new List<CreateAssistUserRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastUserid),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", providerArgs.CustomParm)
					};

					var dataReader = db.ExecuteReader("EXEC Migrations_UserRequests @LastId, @BatchSize, @CustomerId", dbParams);

					while (dataReader.Read())
					{
						var request = new CreateAssistUserRequest
						{
							UserMigrationId = string.Concat("FC01:", dataReader.GetString(dataReader.GetOrdinal("MigrationId"))),
							UserSecondaryMigrationId = string.Concat("AOSOS:", dataReader.GetString(dataReader.GetOrdinal("UserName"))),
						};

						try
						{
							var model = new CreateAssistUserModel
							{
								AccountUserName = dataReader.GetString(dataReader.GetOrdinal("UserName")),
								AccountPassword = GenerateRandomPassword(),
								UserPerson = new PersonDto
								{
									//TitleId = PersonalTitleDropDownList.SelectedValueToLong(),
									FirstName = dataReader.GetString(dataReader.GetOrdinal("UserPersonFirstName")),
									MiddleInitial = dataReader.GetString(dataReader.GetOrdinal("UserPersonMiddleInitial")),
									LastName = dataReader.GetString(dataReader.GetOrdinal("UserPersonLastName")),
									JobTitle = dataReader.GetString(dataReader.GetOrdinal("UserPersonJobTitle")),
									EmailAddress = dataReader.GetString(dataReader.GetOrdinal("UserPersonEmailAddress")),
								},
								UserAddress = new PersonAddressDto
								{
									Line1 = dataReader.GetString(dataReader.GetOrdinal("UserAddressLine1")),
									Line2 = dataReader.GetString(dataReader.GetOrdinal("UserAddressLine2")),
									TownCity = dataReader.GetString(dataReader.GetOrdinal("UserAddressTownCity")),
									CountyId = null,
									PostcodeZip = dataReader.GetString(dataReader.GetOrdinal("UserAddressPostcodeZip")),
									StateId =
										GetLookupId(LookupTypes.States, dataReader.GetString(dataReader.GetOrdinal("UserAddressState")), "OK"),
									CountryId =
										GetLookupId(LookupTypes.Countries, dataReader.GetString(dataReader.GetOrdinal("UserAddressCountry")),
											"US"),
									IsPrimary = true
								},
								UserPhone = FormatPhoneNumber(dataReader.GetString(dataReader.GetOrdinal("UserPhone"))),
								UserFax = FormatPhoneNumber(dataReader.GetString(dataReader.GetOrdinal("UserFax"))),
								UserAlternatePhone = FormatPhoneNumber(dataReader.GetString(dataReader.GetOrdinal("UserAlternatePhone"))),
								UserEmailAddress = dataReader.GetString(dataReader.GetOrdinal("UserPersonEmailAddress")),
								UserExternalOffice = dataReader.GetString(dataReader.GetOrdinal("UserOffice")),
								IsEnabled = true,
							};

							if (model.UserPhone == "Unspecified")
								model.UserPhone = "(000) 000-0000";

							AssignCorrectlyFormattedEmailAddressToAccountUserNameField(model);

							request.Models = new List<CreateAssistUserModel> { model };
							request.ExtraRoles = new List<string>
							{
								Constants.RoleKeys.AssistJobSeekerReferralApprover,
								Constants.RoleKeys.AssistEmployerAccountApprover,
								Constants.RoleKeys.AssistPostingApprover,
								Constants.RoleKeys.AssistJobSeekerReferralApprovalViewer,
								Constants.RoleKeys.AssistEmployerAccountApprovalViewer,
								Constants.RoleKeys.AssistPostingApprovalViewer
							};
						}
						catch (Exception ex)
						{
							request.MigrationValidationMessage = ex.Message;
						}

						_lastUserid = dataReader.GetInt32(dataReader.GetOrdinal("StaffId"));

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateUsers SQL Execution exception.", ex);
			}

			return requests;
		}

		public void AssignCorrectlyFormattedEmailAddressToAccountUserNameField(CreateAssistUserModel model)
		{
			// Focus requires user names to be email addresses
			if (!model.AccountUserName.Contains("@") && model.UserEmailAddress != null)
				model.AccountUserName = model.UserEmailAddress;

			if (!Regex.IsMatch(model.UserEmailAddress ?? "", @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"))
				model.UserEmailAddress = string.Format("{0}.{1}@kygovmigrated.com", model.UserPerson.FirstName, model.UserPerson.LastName);

			if (!Regex.IsMatch(model.AccountUserName, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"))
				model.AccountUserName = model.UserEmailAddress;
		}

		/// <summary>
		/// Gets a list of notes and reminders for job seekers
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of notes and reminders requests</returns>
		public List<SaveNoteReminderRequest> MigrateNotesAndReminders(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			var requests = new List<SaveNoteReminderRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastNotesAndReminderId),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", providerArgs.CustomParm),
						new DatabaseParam("CutoffDate", providerArgs.CutOffDate)
					};

					var dataReader = db.ExecuteReader(
						"EXEC Migrations_JobSeekerNoteReminderRequests @LastId, @BatchSize, @CustomerId, @CutOffDate", dbParams);

					var isNoteOrdinal = dataReader.GetOrdinal("IsNote");
					var notesOrdinal = dataReader.GetOrdinal("Notes");
					var reminderViaDashboardOrdinal = dataReader.GetOrdinal("ReminderViaDashboard");
					var reminderViaEmailOrdinal = dataReader.GetOrdinal("ReminderViaEmail");
					var reminderDateOrdinal = dataReader.GetOrdinal("ReminderDate");
					var reminderToJobSeekerOrdinal = dataReader.GetOrdinal("ReminderToJobSeeker");
					var reminderToStaffOrdinal = dataReader.GetOrdinal("ReminderToStaff");
					var createdDateOrdinal = dataReader.GetOrdinal("CreatedDate");
					var migrationIdOrdinal = dataReader.GetOrdinal("MigrationId");
					var jobSeekerMigrationIdOrdinal = dataReader.GetOrdinal("JobSeekerMigrationId");
					var staffMigrationIdOrdinal = dataReader.GetOrdinal("StaffMigrationId");
					var noteReminderIdOrdinal = dataReader.GetOrdinal("SavedNotesId");

					while (dataReader.Read())
					{
						var request = new SaveNoteReminderRequest
						{
							NoteReminderMigrationId = string.Concat("FC01:", dataReader.GetString(migrationIdOrdinal))
						};

						try
						{
							request.NoteReminder = new NoteReminderDto
							{
								Text = dataReader.GetString(notesOrdinal).Trim(),
								EntityId = 0, // Will be populated by import service
								EntityType = EntityTypes.JobSeeker,
								CreatedBy = 0,
								NoteReminderType = dataReader.GetBoolean(isNoteOrdinal) ? NoteReminderTypes.Note : NoteReminderTypes.Reminder
							};

							request.JobSeekerMigrationId = string.Concat("FC01:", dataReader.GetString(jobSeekerMigrationIdOrdinal));
							request.StaffSecondaryMigrationId = string.Concat("AOSOS:", dataReader.GetString(staffMigrationIdOrdinal));
							request.OverrideCreatedOnDate = dataReader.GetDateTime(createdDateOrdinal);

							if (!dataReader.GetBoolean(isNoteOrdinal))
							{
								request.NoteReminder.ReminderVia = null;

								if (dataReader.GetBoolean(reminderViaEmailOrdinal))
									request.NoteReminder.ReminderVia = ReminderMedia.Email;
								else if (dataReader.GetBoolean(reminderViaDashboardOrdinal))
									request.NoteReminder.ReminderVia = ReminderMedia.DashboardMessage;

								request.NoteReminder.ReminderDue = dataReader.GetDateTime(reminderDateOrdinal);
								if (request.NoteReminder.ReminderDue < DateTime.Now.Date)
									request.NoteReminder.ReminderSentOn = request.NoteReminder.ReminderDue;

								request.ReminderRecipients = new List<NoteReminderRecipientDto>();

								if (dataReader.GetBoolean(reminderToJobSeekerOrdinal))
								{
									request.ReminderRecipients.Add(new NoteReminderRecipientDto
									{
										RecipientEntityId = 0,
										RecipientEntityType = EntityTypes.JobSeeker
									});
								}

								if (dataReader.GetBoolean(reminderToStaffOrdinal))
								{
									request.ReminderRecipients.Add(new NoteReminderRecipientDto
									{
										RecipientEntityId = 0,
										RecipientEntityType = EntityTypes.Staff
									});
								}
							}
						}
						catch (Exception ex)
						{
							request.MigrationValidationMessage = ex.Message;
						}

						_lastNotesAndReminderId = dataReader.GetInt32(noteReminderIdOrdinal);

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateNotesAndReminders SQL Execution exception.", ex);
			}

			return requests;
		}

		/// <summary>
		/// Gets a list of office requests to set up
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new office requests</returns>
		public List<SaveOfficeRequest> MigrateOffices(MigrationProviderArgs providerArgs)
		{
			return new List<SaveOfficeRequest>();
		}

		/// <summary>
		/// Gets a list of office mapping requests to set up
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new office mapping requests</returns>
		public List<PersonOfficeMapperRequest> MigrateOfficeMappings(MigrationProviderArgs providerArgs)
		{
			return new List<PersonOfficeMapperRequest>();
		}

		#endregion

		#region Helpers

		/// <summary>
		/// Generates a random password of letters and numbers
		/// </summary>
		/// <returns>The password</returns>
		private string GenerateRandomPassword()
		{
			string password;

			do
			{
				password = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20);
			} while (!Regex.IsMatch(password, AppSettings.PasswordRegExPattern));

			return password;
		}

		/// <summary>
		/// Converts the Focus Career Search Criteria Xml into a SearchCriteria object
		/// </summary>
		/// <param name="criteriaXmlString">The criteria Xml string</param>
		/// <returns>The SearchCriteria object</returns>
		private SearchCriteria BuildJobSearchSearchCriteria(string criteriaXmlString)
		{
			var searchCriteria = new SearchCriteria
			{
				RequiredResultCriteria = new RequiredResultCriteria
				{
					DocumentsToSearch = DocumentType.Posting,
					MinimumStarMatch = StarMatching.None,
					MaximumDocumentCount = AppSettings.MaximumNoDocumentToReturnInSearch
				}
			};

			var criteriaXml = new XmlDocument();
			criteriaXml.LoadXml(criteriaXmlString);

			var rootElement = criteriaXml.DocumentElement;
			if (rootElement == null)
				return searchCriteria;

			// Minimum Star Match
			var docNode = rootElement.SelectSingleNode("doc[@type='resume']");
			if (docNode != null)
			{
				var minScore = int.Parse(rootElement.GetAttribute("min"));

				if (minScore >= 850)
					searchCriteria.RequiredResultCriteria.MinimumStarMatch = StarMatching.FiveStar;
				else if (minScore >= 700)
					searchCriteria.RequiredResultCriteria.MinimumStarMatch = StarMatching.FourStar;
				else if (minScore >= 550)
					searchCriteria.RequiredResultCriteria.MinimumStarMatch = StarMatching.ThreeStar;
				else if (minScore >= 400)
					searchCriteria.RequiredResultCriteria.MinimumStarMatch = StarMatching.TwoStar;
				else if (minScore >= 200)
					searchCriteria.RequiredResultCriteria.MinimumStarMatch = StarMatching.OneStar;
				else
					searchCriteria.RequiredResultCriteria.MinimumStarMatch = StarMatching.ZeroStar;
			}

			var filtersElement = (XmlElement)rootElement.SelectSingleNode("//filters");
			if (filtersElement == null)
				return searchCriteria;

			// Location Search
			var joblocation = new JobLocationCriteria();

			var distanceElement = (XmlElement)filtersElement.SelectSingleNode("distance");
			var stateElement = (XmlElement)filtersElement.SelectSingleNode("statecode");
			if (distanceElement != null)
			{
				joblocation.Radius = new RadiusCriteria
				{
					Distance = long.Parse(distanceElement.GetAttribute("around")),
					DistanceUnits =
						distanceElement.GetAttribute("units") == "miles"
							? DistanceUnits.Miles
							: DistanceUnits.Kilometres,
					PostalCode = distanceElement.GetAttribute("zipcode")
				};
			}
			else if (stateElement != null && stateElement.HasChildNodes)
			{
				var stateLookup = GetLookup(LookupTypes.States, stateElement.FirstChild.Value);
				if (stateLookup != null)
				{
					joblocation.Area = new AreaCriteria
					{
						StateId = stateLookup.Id,
						MsaId = 0
					};

					joblocation.SelectedStateInCriteria = stateLookup.ExternalId;
				}
			}

			searchCriteria.JobLocationCriteria = joblocation;

			// Keyword Search
			var keywordSearch = new StringBuilder();

			var keywordElement = (XmlElement)filtersElement.SelectSingleNode("keyword[@type='all']");
			if (keywordElement != null && keywordElement.HasChildNodes)
				keywordSearch.Append(keywordElement.FirstChild.Value).Append(" ");

			keywordElement = (XmlElement)filtersElement.SelectSingleNode("keyword[@type='exact']");
			if (keywordElement != null && keywordElement.HasChildNodes)
				keywordSearch.Append("\"").Append(keywordElement.FirstChild.Value).Append("\" ");

			keywordElement = (XmlElement)filtersElement.SelectSingleNode("keyword[@type='any']");
			if (keywordElement != null && keywordElement.HasChildNodes)
			{
				keywordSearch.Append(" OR ")
					.Append(string.Join(" OR ",
						keywordElement.FirstChild.Value.Split(' ').Select(k => k.Trim()).Where(k => k.IsNotNullOrEmpty())))
					.Append(" ");
			}

			keywordElement = (XmlElement)filtersElement.SelectSingleNode("keyword[@type='none']");
			if (keywordElement != null && keywordElement.HasChildNodes)
			{
				keywordSearch.Append(" NOT ")
					.Append(string.Join(" NOT ",
						keywordElement.FirstChild.Value.Split(' ').Select(k => k.Trim()).Where(k => k.IsNotNullOrEmpty())));
			}

			if (keywordSearch.Length > 0)
			{
				var keywordSection = PostingKeywordScopes.Anywhere;
				keywordElement = (XmlElement)filtersElement.SelectSingleNode("keyword[@section!='']");
				if (keywordElement != null)
				{
					switch (keywordElement.GetAttribute("section"))
					{
						case "jobtitle":
							keywordSection = PostingKeywordScopes.JobTitle;
							break;
						case "jobemployer":
							keywordSection = PostingKeywordScopes.Employer;
							break;
						case "duties":
							keywordSection = PostingKeywordScopes.JobDescription;
							break;
					}
				}

				searchCriteria.KeywordCriteria = new KeywordCriteria(keywordSearch.ToString().Trim())
				{
					SearchLocation = keywordSection
				};
			}

			// Posting Age
			var jobPostedElement = (XmlElement)filtersElement.SelectSingleNode("jobposted");
			if (jobPostedElement != null)
			{
				var days = jobPostedElement.GetAttribute("days");
				if (days.IsNullOrEmpty() || days.StartsWith("-"))
				{
					var from = jobPostedElement.GetAttribute("from");
					var to = jobPostedElement.GetAttribute("to");
					if (from.IsNotNullOrEmpty() && to.IsNotNullOrEmpty())
					{
						var fromDate = new DateTime(int.Parse(from.Substring(0, 4)), int.Parse(from.Substring(5, 2)),
							int.Parse(from.Substring(8, 2)));
						var toDate = new DateTime(int.Parse(to.Substring(0, 4)), int.Parse(to.Substring(5, 2)),
							int.Parse(to.Substring(8, 2)));

						days = toDate.Subtract(fromDate).TotalDays.ToString(CultureInfo.InvariantCulture);
					}
				}

				if (days.IsNotNullOrEmpty())
				{
					searchCriteria.PostingAgeCriteria = new PostingAgeCriteria
					{
						PostingAgeInDays = int.Parse(days)
					};
				}
			}

			// Education Level
			if (AppSettings.Theme == FocusThemes.Workforce)
			{
				var educationLevel = (XmlElement)filtersElement.SelectSingleNode("education");
				if (educationLevel != null && educationLevel.HasChildNodes)
				{
					searchCriteria.EducationLevelCriteria = new EducationLevelCriteria
					{
						IncludeJobsWithoutEducationRequirement = educationLevel.GetAttribute("include") == "true",
						RequiredEducationIds = new List<long>()
					};

					if (educationLevel.FirstChild.Value.Contains("12,13"))
						searchCriteria.EducationLevelCriteria.RequiredEducationIds.Add(
							GetLookupId(LookupTypes.RequiredEducationLevels, "LessThanBachelorsDegree").GetValueOrDefault(0));

					if (educationLevel.FirstChild.Value.Contains("18,21"))
						searchCriteria.EducationLevelCriteria.RequiredEducationIds.Add(
							GetLookupId(LookupTypes.RequiredEducationLevels, "GraduateDegree").GetValueOrDefault(0));

					if (educationLevel.FirstChild.Value.Contains("16"))
						searchCriteria.EducationLevelCriteria.RequiredEducationIds.Add(
							GetLookupId(LookupTypes.RequiredEducationLevels, "BachelorsDegree").GetValueOrDefault(0));
				}
			}

			// Minimum Salary
			if (AppSettings.Theme == FocusThemes.Workforce)
			{
				var minimumSalaryElement = (XmlElement)filtersElement.SelectSingleNode("salarymin");
				if (minimumSalaryElement != null && minimumSalaryElement.HasChildNodes)
				{
					searchCriteria.SalaryCriteria = new SalaryCriteria
					{
						IncludeJobsWithoutSalaryInformation = minimumSalaryElement.GetAttribute("include") == "true",
						MinimumSalary = minimumSalaryElement.FirstChild.Value.AsFloat()
					};
				}
			}

			// Include Internships
			searchCriteria.InternshipCriteria = new InternshipCriteria
			{
				Internship = FilterTypes.NoFilter
			};
			var intershipsElement = (XmlElement)filtersElement.SelectSingleNode("internships");
			if (intershipsElement != null)
			{
				searchCriteria.InternshipCriteria.Internship = intershipsElement.GetAttribute("include") == "0"
					? FilterTypes.Exclude
					: FilterTypes.ShowOnly;
			}

			// Occupation
			var occupationsElement = (XmlElement)filtersElement.SelectSingleNode("occupation_selected");
			if (occupationsElement != null && occupationsElement.HasChildNodes &&
			    occupationsElement.FirstChild.Value.IsNotNullOrEmpty())
			{
				var onets =
					occupationsElement.FirstChild.Value.Split(',').Select(r => r.Trim()).Where(r => r.IsNotNullOrEmpty()).ToList();

				var ronets = (from onet in LibraryRepository.Onets
					join onetRonet in LibraryRepository.Query<OnetROnet>()
						on onet.Id equals onetRonet.OnetId
					join ronet in LibraryRepository.Query<ROnet>()
						on onetRonet.ROnetId equals ronet.Id
					where onets.Contains(onet.OnetCode)
					select ronet.Code).ToList();

				if (ronets.Any())
				{
					searchCriteria.ROnetCriteria = new ROnetCriteria
					{
						CareerAreaId = null,
						ROnets = ronets
					};

					var careerArea = (from job in LibraryRepository.Query<Job>()
						join jobCareerArea in LibraryRepository.Query<JobCareerArea>()
							on job.Id equals jobCareerArea.JobId
						where job.ROnet == searchCriteria.ROnetCriteria.ROnets[0]
						select jobCareerArea.CareerAreaId).FirstOrDefault();

					if (careerArea != 0)
						searchCriteria.ROnetCriteria.CareerAreaId = careerArea;
				}
			}

			// Industry Criteria
			if (AppSettings.Theme == FocusThemes.Workforce)
			{
				var industryElement = (XmlElement)filtersElement.SelectSingleNode("detailed_industry_selected");
				if (industryElement != null && industryElement.HasChildNodes)
				{
					var naicsList =
						industryElement.FirstChild.Value.Split(',').Select(n => n.Trim()).Where(n => n.IsNotNullOrEmpty()).ToList();
					var naicsLookup = LibraryRepository.Query<NAICS>()
						.Where(n => naicsList.Contains(n.Code))
						.ToDictionary(n => n.Code, n => n);

					if (naicsLookup.Any())
					{
						searchCriteria.IndustryCriteria = new IndustryCriteria
						{
							IndustryId = naicsLookup.Values.First().ParentId,
							IndustryDetailIds = naicsList.Where(naicsLookup.ContainsKey).Select(n => naicsLookup[n].Id).ToList()
						};
					}
				}
			}

			// Emerging Sector
			if (AppSettings.Theme == FocusThemes.Workforce)
			{
				var sectorsElement = (XmlElement)filtersElement.SelectSingleNode("emerging_sectors");
				if (sectorsElement != null && sectorsElement.HasChildNodes)
				{
					var sectorIdArray =
						sectorsElement.FirstChild.Value.Split(',').Select(s => s.Trim()).Where(s => s.IsNotNullOrEmpty()).ToList();
					var validSectors =
						sectorIdArray.Select(s => GetLookup(LookupTypes.EmergingSectors, s)).Where(s => s.IsNotNull()).ToList();

					if (validSectors.Any())
					{
						searchCriteria.JobSectorCriteria = new JobSectorCriteria
						{
							RequiredJobSectorIds = validSectors.Select(s => s.Id).ToList(),
							RequiredJobSectors = string.Join(", ", validSectors.Select(s => s.Text))
						};
					}
				}
			}

			// Physical abilities
			if (AppSettings.Theme == FocusThemes.Workforce)
			{
				var abilitiesElement = (XmlElement)filtersElement.SelectSingleNode("impairment_categories");
				if (abilitiesElement != null && abilitiesElement.HasChildNodes)
				{
					var abilityIdArray =
						abilitiesElement.FirstChild.Value.Split(',').Select(a => a.Trim()).Where(a => a.IsNotNullOrEmpty()).ToList();
					var validAbilities =
						abilityIdArray.Select(s => GetLookup(LookupTypes.PhysicalAbilities, s)).Where(s => s.IsNotNull()).ToList();

					if (validAbilities.Any())
					{
						searchCriteria.PhysicalAbilityCriteria = new PhysicalAbilityCriteria
						{
							PhysicalAbilityIds = validAbilities.Select(s => s.Id).ToList(),
							PhysicalAbilities = string.Join(", ", validAbilities.Select(s => s.Text))
						};
					}
				}
			}

			return searchCriteria;
		}

		/// <summary>
		/// Converts the Focus Career Resume Search Criteria Xml into a SearchCriteria object
		/// </summary>
		/// <param name="criteriaXmlString">The criteria Xml string</param>
		/// <returns>The SearchCriteria object</returns>
		private CandidateSearchCriteria BuildResumeSearchCriteria(string criteriaXmlString)
		{
			var searchCriteria = new CandidateSearchCriteria
			{
				SearchType = CandidateSearchTypes.OpenSearch
			};

			var criteriaXml = new XmlDocument();
			criteriaXml.LoadXml(criteriaXmlString);

			var rootElement = criteriaXml.DocumentElement;
			if (rootElement == null)
				return searchCriteria;

			// Minimum Star Match
			var docNode = rootElement.SelectSingleNode("doc[@type='search']");
			if (docNode != null)
				searchCriteria.MinimumScore = int.Parse(rootElement.GetAttribute("min"));

			var docElement = (XmlElement)rootElement.SelectSingleNode("//doc[@type='posting']");
			if (docElement != null)
			{
				var postingId = docElement.GetAttribute("id");
				if (postingId.IsNotNullOrEmpty())
				{
					searchCriteria.JobDetailsCriteria = new CandidateSearchJobDetailsCriteria
					{
						MigrationPostingId = string.Concat("FC01:", postingId)
					};
				}
			}

			var filtersElement = (XmlElement)rootElement.SelectSingleNode("//filters");
			if (filtersElement == null)
				return searchCriteria;

			var textInfo = (XmlElement)filtersElement.SelectSingleNode("textinfo");
			var displayNode = textInfo.IsNull()
				? null
				: (XmlElement)textInfo.SelectSingleNode("display");

			// Keyword Search
			string keywordSearch = null;

			/*
      var keywordElement = (XmlElement)filtersElement.SelectSingleNode("keyword[@type='all']");
      if (keywordElement != null && keywordElement.HasChildNodes)
        keywordSearch = string.Join(" AND ", keywordElement.InnerText.Split(',', ';', ' ').Select(k => k.Trim()).Where(k => k.IsNotNullOrEmpty()));

      keywordElement = (XmlElement)filtersElement.SelectSingleNode("keyword[@type='exact']");
      if (keywordElement != null && keywordElement.HasChildNodes)
        keywordSearch = string.Concat("(", keywordElement.InnerText, ")");

      keywordElement = (XmlElement)filtersElement.SelectSingleNode("keyword[@type='any']");
      if (keywordElement != null && keywordElement.HasChildNodes)
        keywordSearch = string.Join(" OR ", keywordElement.InnerText.Split(',', ';', ' ').Select(k => k.Trim()).Where(k => k.IsNotNullOrEmpty()));

      keywordElement = (XmlElement)filtersElement.SelectSingleNode("keyword[@type='none']");
      if (keywordElement != null && keywordElement.HasChildNodes)
        keywordSearch = string.Concat("NOT ", string.Join(" NOT ", keywordElement.InnerText.Split(',', ';', ' ').Select(k => k.Trim()).Where(k => k.IsNotNullOrEmpty())));
      */

			var keywordElement = (XmlElement)filtersElement.SelectSingleNode("keyword");
			if (keywordElement.IsNotNull() && keywordElement.HasChildNodes)
				keywordSearch = keywordElement.FirstChild.Value;

			if (keywordSearch.IsNotNullOrEmpty())
			{
				searchCriteria.SearchType = CandidateSearchTypes.ByKeywords;
				searchCriteria.KeywordCriteria = new CandidateSearchKeywordCriteria
				{
					Keywords = keywordSearch.Trim(),
					Context = KeywordContexts.FullResume,
					Scope = KeywordScopes.EntireWorkHistory
				};
			}

			// Location Criteria
			var distanceElement = (XmlElement)filtersElement.SelectSingleNode("distance");
			if (distanceElement != null)
			{
				var jobLocationCriteria = new CandidateSearchLocationCriteria
				{
					Distance = long.Parse(distanceElement.GetAttribute("around")),
					DistanceUnits =
						distanceElement.GetAttribute("units") == "miles" ? Core.DistanceUnits.Miles : Core.DistanceUnits.Kilometres,
					PostalCode = distanceElement.GetAttribute("zipcode")
				};

				if (searchCriteria.AdditionalCriteria.IsNull())
					searchCriteria.AdditionalCriteria = new CandidateSearchAdditionalCriteria();

				searchCriteria.AdditionalCriteria.LocationCriteria = jobLocationCriteria;
			}

			// Education Criteria
			var educationLevelElement = (XmlElement)filtersElement.SelectSingleNode("education");
			if (educationLevelElement != null && educationLevelElement.HasChildNodes)
			{
				var educationLevel = EducationLevels.None;

				var levelArray =
					educationLevelElement.FirstChild.Value.Split(new[] { ',', ':' }, StringSplitOptions.RemoveEmptyEntries)
						.Where(el => !el.Equals("EDUCATION", StringComparison.OrdinalIgnoreCase))
						.ToList();

				levelArray.ForEach(level =>
				{
					switch (level)
					{
						case "12":
							educationLevel = educationLevel | EducationLevels.HighSchoolDiplomaOrEquivalent;
							break;
						case "13":
						case "14":
							educationLevel = educationLevel | EducationLevels.AssociatesDegree;
							break;
						case "16":
							educationLevel = educationLevel | EducationLevels.BachelorsDegree;
							break;
						case "18":
						case "21":
							educationLevel = educationLevel | EducationLevels.GraduateDegree;
							break;
					}
				});

				if (educationLevel != EducationLevels.None)
				{
					if (searchCriteria.AdditionalCriteria.IsNull())
						searchCriteria.AdditionalCriteria = new CandidateSearchAdditionalCriteria();

					searchCriteria.AdditionalCriteria.EducationCriteria = new CandidateSearchEducationCriteria
					{
						EducationLevel = educationLevel
					};
				}
			}

			// Languages
			var languagesNode = (XmlElement)filtersElement.SelectSingleNode("languages");
			if (languagesNode.IsNotNull() && languagesNode.HasChildNodes)
			{
				// No mapping currently exists, so simply get language name from display text
				var languages = ExtractFromDisplayText(displayNode, "Languages");
				if (languages.IsNotNullOrEmpty())
				{
					if (searchCriteria.AdditionalCriteria.IsNull())
						searchCriteria.AdditionalCriteria = new CandidateSearchAdditionalCriteria();

					searchCriteria.AdditionalCriteria.Languages = languages;
				}
			}

			// Veteran status
			var veteranNode = (XmlElement)filtersElement.SelectSingleNode("veteran");
			if (veteranNode != null && veteranNode.HasChildNodes)
			{
				if (searchCriteria.AdditionalCriteria.IsNull())
					searchCriteria.AdditionalCriteria = new CandidateSearchAdditionalCriteria();

				searchCriteria.AdditionalCriteria.StatusCriteria = new CandidateSearchStatusCriteria
				{
					Veteran = (veteranNode.FirstChild.Value == "1")
				};
			}

			// Driving Licence
			var licenceClassNode = (XmlElement)filtersElement.SelectSingleNode("drivers_license/class");
			if (licenceClassNode.IsNotNull() && licenceClassNode.HasChildNodes)
			{
				var lookup = GetLookup(LookupTypes.DrivingLicenceClasses, licenceClassNode.FirstChild.Value);

				if (lookup.IsNotNull())
				{
					if (searchCriteria.AdditionalCriteria.IsNull())
						searchCriteria.AdditionalCriteria = new CandidateSearchAdditionalCriteria();

					searchCriteria.AdditionalCriteria.DrivingLicenceCriteria = new CandidateSearchDrivingLicenceCriteria
					{
						DrivingLicenceClassId = lookup.Id,
						DrivingLicenceEndorsementsIds = new List<long>()
					};
				}
			}

			// Certifications
			var certifications = ExtractFromDisplayText(displayNode, "Certifications");
			if (certifications.IsNotNullOrEmpty())
			{
				if (searchCriteria.AdditionalCriteria.IsNull())
					searchCriteria.AdditionalCriteria = new CandidateSearchAdditionalCriteria();

				searchCriteria.AdditionalCriteria.Certifications = certifications;
			}

			// Availabilty - Overtime
			var overtimeNode = (XmlElement)filtersElement.SelectSingleNode("workovertime");
			if (overtimeNode != null && overtimeNode.HasChildNodes)
			{
				if (searchCriteria.AdditionalCriteria.IsNull())
					searchCriteria.AdditionalCriteria = new CandidateSearchAdditionalCriteria();

				if (searchCriteria.AdditionalCriteria.AvailabilityCriteria.IsNull())
					searchCriteria.AdditionalCriteria.AvailabilityCriteria = new CandidateSearchAvailabilityCriteria();

				searchCriteria.AdditionalCriteria.AvailabilityCriteria.WorkOvertime = (overtimeNode.FirstChild.Value == "1");
			}

			// Availabilty - Work Shifts
			var shiftNode = (XmlElement)filtersElement.SelectSingleNode("shift");
			if (shiftNode != null && shiftNode.HasChildNodes)
			{
				var lookup = GetLookup(LookupTypes.WorkShifts, shiftNode.FirstChild.Value);
				if (lookup.IsNotNull())
				{
					if (searchCriteria.AdditionalCriteria.IsNull())
						searchCriteria.AdditionalCriteria = new CandidateSearchAdditionalCriteria();

					if (searchCriteria.AdditionalCriteria.AvailabilityCriteria.IsNull())
						searchCriteria.AdditionalCriteria.AvailabilityCriteria = new CandidateSearchAvailabilityCriteria();

					searchCriteria.AdditionalCriteria.AvailabilityCriteria.NormalWorkShiftId = lookup.Id;
				}
			}

			// Availabilty - Duration
			// COMMENTED OUT BECAUSE V1 uses Work-Types "Regular", "Regular or temporary" and "Temporary" which do not correspond to V3's Durations
			/*
      var workTypeNode = (XmlElement)filtersElement.SelectSingleNode("worktype");
      if (workTypeNode != null && workTypeNode.HasChildNodes)
      {
        var lookup = GetLookup(LookupTypes.Durations, workTypeNode.FirstChild.Value);
        if (lookup.IsNotNull())
        {
          if (searchCriteria.AdditionalCriteria.IsNull())
            searchCriteria.AdditionalCriteria = new CandidateSearchAdditionalCriteria();

          if (searchCriteria.AdditionalCriteria.AvailabilityCriteria.IsNull())
            searchCriteria.AdditionalCriteria.AvailabilityCriteria = new CandidateSearchAvailabilityCriteria();

          searchCriteria.AdditionalCriteria.AvailabilityCriteria.WorkTypeId = lookup.Id;
        }
      }
      */

			// Availabilty - Work week
			var workWeekNode = (XmlElement)filtersElement.SelectSingleNode("workweek");
			if (workWeekNode != null && workWeekNode.HasChildNodes)
			{
				var lookup = GetLookup(LookupTypes.WorkWeeks, workWeekNode.FirstChild.Value);
				if (lookup.IsNotNull())
				{
					if (searchCriteria.AdditionalCriteria.IsNull())
						searchCriteria.AdditionalCriteria = new CandidateSearchAdditionalCriteria();

					if (searchCriteria.AdditionalCriteria.AvailabilityCriteria.IsNull())
						searchCriteria.AdditionalCriteria.AvailabilityCriteria = new CandidateSearchAvailabilityCriteria();

					searchCriteria.AdditionalCriteria.AvailabilityCriteria.WorkWeekId = lookup.Id;
				}
			}

			return searchCriteria;
		}

		/// <summary>
		/// Extracts a list of options from then display text.
		/// </summary>
		/// <param name="displayNode">The display node.</param>
		/// <param name="textName">Name of the text to extract</param>
		/// <returns>A list of options</returns>
		private List<string> ExtractFromDisplayText(XmlNode displayNode, string textName)
		{
			if (displayNode.IsNotNull())
			{
				var xPath = string.Format("text()[starts-with(., '- {0}')]", textName);

				var displayTextNode = displayNode.SelectSingleNode(xPath);
				if (displayTextNode.IsNotNull())
				{
					var textList =
						displayTextNode.Value.SubstringAfter(":")
							.Trim()
							.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
							.ToList();
					return textList;
				}
			}

			return null;
		}

		/// <summary>
		/// Maps the V1 job status to the V3 job status
		/// </summary>
		/// <param name="status">The V1 status.</param>
		/// <param name="hasLensPostingId"></param>
		/// <returns>The V3 status</returns>
		private JobStatuses MapJobStatus(int status, bool hasLensPostingId)
		{
			var jobStatus = JobStatuses.Draft;

			switch (status)
			{
				case 511: // RED WORD -- approval status needs to be set (handled elsewhere)
				case 767: // REVIEW
					jobStatus = hasLensPostingId ? JobStatuses.OnHold : JobStatuses.Draft;
					break;
				case 1023: // ACTIVE
					jobStatus = JobStatuses.Active;
					break;

				case 1279: // HOLD
					jobStatus = JobStatuses.OnHold;
					break;

				case 1535: // CLOSED
					jobStatus = JobStatuses.Closed;
					break;
			}

			return jobStatus;
		}

		/// <summary>
		/// Maps the V1 job status to a V3 job approval status
		/// </summary>
		/// <param name="status">The V1 status.</param>
		/// <returns>The V3 approval status</returns>
		private ApprovalStatuses MapApprovalStatus(int status)
		{
			var approvalStatus = ApprovalStatuses.Approved;

			switch (status)
			{
				case 511:
					approvalStatus = ApprovalStatuses.Rejected;
					break;
				case 767:
					approvalStatus = ApprovalStatuses.WaitingApproval;
					break;
				case 1279:
					approvalStatus = ApprovalStatuses.OnHold;
					break;
			}

			return approvalStatus;
		}

		/// <summary>
		/// Looks up the employee title.
		/// </summary>
		/// <param name="lookupCode">The title from Career Talent.</param>
		/// <returns>The focus employee title.</returns>
		private long LookupPersonalTitle(string lookupCode)
		{
			string key;

			switch (lookupCode)
			{
				case "1":
					key = "Mr";
					break;

				case "2":
					key = "Mrs";
					break;

				case "3":
					key = "Miss";
					break;

				case "4":
					key = "Ms";
					break;

				case "5":
					key = "Dr";
					break;

				default:
					key = "";
					break;
			}
			return GetLookupId(LookupTypes.Titles, key, "");
		}

		/// <summary>
		/// Looks up the ownership type.
		/// </summary>
		/// <param name="lookupCode">The ownership type from Career Talent.</param>
		/// <returns>The focus ownership type.</returns>
		private long LookupOwnershipType(int lookupCode)
		{
			string key;

			switch (lookupCode)
			{
				case 1:
					key = "FederalGovernment";
					break;

				case 2:
					key = "StateGovernment";
					break;

				case 3:
					key = "LocalGovernment";
					break;

				case 4:
					key = "NonProfit";
					break;

				case 5:
					key = "PrivateCorporation";
					break;

				case 7:
					key = "ForeignInternational";
					break;

				default:
					key = "";
					break;
			}
			return GetLookupId(LookupTypes.OwnershipTypes, key, "");
		}

		/// <summary>
		/// Looks up the phone type for a given code (M, F or L)
		/// </summary>
		/// <param name="lookupCode">The code to look up</param>
		/// <returns>The name of the phone type</returns>
		private string LookupPhoneType(string lookupCode)
		{
			switch (lookupCode)
			{
				case "M":
					return "Mobile";

				case "F":
					return "Fax";

				default:
					return "Phone";
			}
		}

		/// <summary>
		/// Looks up the employer status
		/// </summary>
		/// <param name="lookupCode">The code to look up</param>
		/// <returns>The name of the phone type</returns>
		private ApprovalStatuses LookupEmployerStatus(int lookupCode)
		{
			switch (lookupCode)
			{
				case -1:
				case -2:
					return ApprovalStatuses.WaitingApproval;

				case 0:
					return ApprovalStatuses.Rejected;

				case 1:
					return ApprovalStatuses.Approved;

				default:
					return ApprovalStatuses.Approved;
			}
		}

		/// <summary>
		/// Looks up the Id of the logo in Career Talent 
		/// </summary>
		/// <param name="employerId">The Id of the Employer in Career Talent</param>
		/// <param name="fileName">The file name of the logo in Career Talent</param>
		/// <returns>The logo name</returns>
		private string LookupLogoId(int employerId, string fileName)
		{
			if (fileName.Length == 0)
				return string.Empty;

			using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
			{
				var dbParams = new[]
				{
					new DatabaseParam("EmployerId", employerId),
					new DatabaseParam("FileName", fileName)
				};

				var dataReader =
					db.ExecuteReader(
						"SELECT LogoId FROM EmployerLogos WHERE EmployerId = @EmployerId AND [FileName] LIKE '%/' + @FileName", dbParams);
				try
				{
					return dataReader.Read()
						? string.Concat("NJ:", dataReader.GetInt32(dataReader.GetOrdinal("LogoId")).ToString(CultureInfo.InvariantCulture))
						: string.Empty;
				}
				finally
				{
					dataReader.Close();
				}
			}
		}

		/// <summary>
		/// Looks up the education level (hard-coded in Career Talent). 
		/// </summary>
		/// <param name="lookupCode">The education code type from Career Talent.</param>
		/// <returns>The education level.</returns>
		private EducationLevels LookUpMinimumEducationLevel(string lookupCode)
		{
			switch (lookupCode)
			{
				case "0": // 0 = None
					return EducationLevels.None;

				case "12": // 12 = High School Diploma/GED
					return EducationLevels.HighSchoolDiplomaOrEquivalent;

				case "13": // 13 = Associates/Some College/Vocational Degree
					return EducationLevels.AssociatesDegree;

				case "16": // 16 = Bachelors Degree
					return EducationLevels.BachelorsDegree;

				case "18": // 18 = Masters Degree
					return EducationLevels.GraduateDegree;

				case "21": // 17 = Doctoral Degree
					return EducationLevels.DoctorateDegree;

				default:
					return EducationLevels.None;
			}
		}

		/// <summary>
		/// Looks up the Driving License Endorsements from the posting XML
		/// </summary>
		/// <param name="endorsementsElement">The XElement holding the endorsements</param>
		/// <returns>The Driving License Endorsements</returns>
		private List<JobDrivingLicenceEndorsementDto> LookupDrivingLicenceEndorsements(XElement endorsementsElement)
		{
			var endorsements = new List<JobDrivingLicenceEndorsementDto>();

			var passTransportEndorsement = LookupDrivingLicenceEndorsement(endorsementsElement, "pass_transport", "PassTransport");
			if (passTransportEndorsement.IsNotNull()) endorsements.Add(passTransportEndorsement);

			var hazerdousMaterialsEndorsement = LookupDrivingLicenceEndorsement(endorsementsElement, "hazardous_materials",
				"HazerdousMaterials");
			if (hazerdousMaterialsEndorsement.IsNotNull()) endorsements.Add(hazerdousMaterialsEndorsement);

			var tankVehicleEndorsement = LookupDrivingLicenceEndorsement(endorsementsElement, "tank_vehicle", "TankVehicle");
			if (tankVehicleEndorsement.IsNotNull()) endorsements.Add(tankVehicleEndorsement);

			var tankHazardEndorsement = LookupDrivingLicenceEndorsement(endorsementsElement, "tank_hazard", "TankHazard");
			if (tankHazardEndorsement.IsNotNull()) endorsements.Add(tankHazardEndorsement);

			var airbrakesEndorsement = LookupDrivingLicenceEndorsement(endorsementsElement, "air_brakes", "Airbrakes");
			if (airbrakesEndorsement.IsNotNull()) endorsements.Add(airbrakesEndorsement);

			var schoolBusEndorsement = LookupDrivingLicenceEndorsement(endorsementsElement, "school_bus", "SchoolBus");
			if (schoolBusEndorsement.IsNotNull()) endorsements.Add(schoolBusEndorsement);

			var doublesTriplesEndorsement = LookupDrivingLicenceEndorsement(endorsementsElement, "doubles_triples",
				"DoublesTriples");
			if (doublesTriplesEndorsement.IsNotNull()) endorsements.Add(doublesTriplesEndorsement);

			var motorcycleEndorsement = LookupDrivingLicenceEndorsement(endorsementsElement, "motor_cycle", "Motorcycle");
			if (motorcycleEndorsement.IsNotNull()) endorsements.Add(motorcycleEndorsement);

			return endorsements;
		}

		/// <summary>
		/// Lookups the driving licence endorsement.
		/// </summary>
		/// <param name="endorsementsElement">The endorsements element.</param>
		/// <param name="xmlElementName">Name of the XML element.</param>
		/// <param name="lookupKey">The lookup key.</param>
		/// <returns></returns>
		private JobDrivingLicenceEndorsementDto LookupDrivingLicenceEndorsement(XElement endorsementsElement,
			string xmlElementName, string lookupKey)
		{
			if (GetBooleanValue(endorsementsElement, xmlElementName))
			{
				var lookupId = GetLookupId(LookupTypes.DrivingLicenceEndorsements, lookupKey);

				if (lookupId.HasValue)
					return new JobDrivingLicenceEndorsementDto { DrivingLicenceEndorsementId = lookupId.Value };
			}

			return null;
		}

		/// <summary>
		/// Looks up the Tax Credit Categories from the posting XML
		/// </summary>
		/// <param name="creditsElement">The XElement holding the Tax Credit Categories</param>
		/// <returns>The Tax Credit Categories</returns>
		private WorkOpportunitiesTaxCreditCategories LookupTaxCreditCategories(XElement creditsElement)
		{
			var credits = WorkOpportunitiesTaxCreditCategories.None;

			credits |= (GetBooleanValue(creditsElement, "tanf_k_tap_recipients")
				? WorkOpportunitiesTaxCreditCategories.TemporaryAssistanceToNeedyFamilyRecipients
				: WorkOpportunitiesTaxCreditCategories.None);
			credits |= (GetBooleanValue(creditsElement, "veterans")
				? WorkOpportunitiesTaxCreditCategories.Veterans
				: WorkOpportunitiesTaxCreditCategories.None);
			credits |= (GetBooleanValue(creditsElement, "ex_felons")
				? WorkOpportunitiesTaxCreditCategories.ExFelons
				: WorkOpportunitiesTaxCreditCategories.None);
			credits |= (GetBooleanValue(creditsElement, "ez_and_rrc")
				? WorkOpportunitiesTaxCreditCategories.DesignatedCommunityResidents
				: WorkOpportunitiesTaxCreditCategories.None);
			credits |= (GetBooleanValue(creditsElement, "vocational_rehabilitation")
				? WorkOpportunitiesTaxCreditCategories.VocationalRehabilitation
				: WorkOpportunitiesTaxCreditCategories.None);
			credits |= (GetBooleanValue(creditsElement, "summer_youth")
				? WorkOpportunitiesTaxCreditCategories.SummerYouth
				: WorkOpportunitiesTaxCreditCategories.None);
			credits |= (GetBooleanValue(creditsElement, "snap_recipients")
				? WorkOpportunitiesTaxCreditCategories.SNAPRecipients
				: WorkOpportunitiesTaxCreditCategories.None);
			credits |= (GetBooleanValue(creditsElement, "ssi")
				? WorkOpportunitiesTaxCreditCategories.SupplementSecurityIncomeRecipients
				: WorkOpportunitiesTaxCreditCategories.None);
			credits |= (GetBooleanValue(creditsElement, "long_term_family_assistance_recipient")
				? WorkOpportunitiesTaxCreditCategories.LongTermFamilyAssistanceRecipient
				: WorkOpportunitiesTaxCreditCategories.None);

			return credits;
		}

		/// <summary>
		/// Looks up the Job Posting Flags from the posting XML
		/// </summary>
		/// <param name="flagsElement">The XElement holding the Job Posting Flags </param>
		/// <returns>The Job Posting Flags</returns>
		private JobPostingFlags LookupJobPostingFlags(XElement flagsElement)
		{
			var postingFlags = JobPostingFlags.None;

			if (flagsElement != null)
			{
				postingFlags |= (GetBooleanValue(flagsElement, "healthcare_employer")
					? JobPostingFlags.HealthcareEmployer
					: JobPostingFlags.None);
				postingFlags |= (GetBooleanValue(flagsElement, "manufacturing_employer")
					? JobPostingFlags.AdvancedManufacturingEmployer
					: JobPostingFlags.None);
				postingFlags |= (GetBooleanValue(flagsElement, "biotech_employer")
					? JobPostingFlags.BiotechEmployer
					: JobPostingFlags.None);
				postingFlags |= (GetBooleanValue(flagsElement, "financial_employer")
					? JobPostingFlags.FinancialServicesEmployer
					: JobPostingFlags.None);
				postingFlags |= (GetBooleanValue(flagsElement, "technology_employer")
					? JobPostingFlags.TechnologyEmployer
					: JobPostingFlags.None);
				postingFlags |= (GetBooleanValue(flagsElement, "transportation_employer")
					? JobPostingFlags.TransportationEmployer
					: JobPostingFlags.None);
				postingFlags |= (GetBooleanValue(flagsElement, "green_job") ? JobPostingFlags.GreenJob : JobPostingFlags.None);
				postingFlags |= (GetBooleanValue(flagsElement, "it_employer") ? JobPostingFlags.ITEmployer : JobPostingFlags.None);
			}

			return postingFlags;
		}

		/// <summary>
		/// Looks up the salary frequency (hard-coded in Career Talent)
		/// </summary>
		/// <param name="frequencyElement">The element holding the frequency</param>
		/// <returns>The focus ID of the frequency</returns>
		private long? LookupSalaryFrequency(XElement frequencyElement)
		{
			string key = string.Empty;

			if (frequencyElement == null)
				return null;

			switch (frequencyElement.Value)
			{
				case "1":
					key = "Hourly";
					break;

				case "2":
					key = "Daily";
					break;

				case "3":
					key = "Weekly";
					break;

				case "4":
					key = "Monthly";
					break;

				case "5":
					key = "Yearly";
					break;
			}

			return GetLookupId(LookupTypes.Frequencies, key);
		}

		/// <summary>
		/// Looks up the employment status 
		/// </summary>
		/// <param name="jobDurationElement">The element holding the status (part of job duration)</param>
		/// <returns>The focus ID of the employment status</returns>
		/// <seealso cref="LookupJobType"/>
		private long? LookupJobEmploymentStatus(XElement jobDurationElement)
		{
			string key;

			if (jobDurationElement == null)
				return null;

			switch (jobDurationElement.Value)
			{
				case "1":
				case "2":
				case "3":
				case "7":
					key = "Fulltime";
					break;

				default:
					key = "Parttime";
					break;
			}

			return GetLookupId(LookupTypes.EmploymentStatuses, key);
		}

		/// <summary>
		/// Looks up the Working Days of the Week from the posting XML
		/// </summary>
		/// <param name="daysElement">The XElement holding the Days of the Week</param>
		/// <returns>The Week Days to work</returns>
		private DaysOfWeek LookupWeekDays(XElement daysElement)
		{
			var days = DaysOfWeek.None;

			if (daysElement != null)
			{
				days |= (GetBooleanValue(daysElement, "monday") ? DaysOfWeek.Monday : DaysOfWeek.None);
				days |= (GetBooleanValue(daysElement, "tuesday") ? DaysOfWeek.Tuesday : DaysOfWeek.None);
				days |= (GetBooleanValue(daysElement, "wednesday") ? DaysOfWeek.Wednesday : DaysOfWeek.None);
				days |= (GetBooleanValue(daysElement, "thursday") ? DaysOfWeek.Thursday : DaysOfWeek.None);
				days |= (GetBooleanValue(daysElement, "friday") ? DaysOfWeek.Friday : DaysOfWeek.None);
				days |= (GetBooleanValue(daysElement, "saturday") ? DaysOfWeek.Saturday : DaysOfWeek.None);
				days |= (GetBooleanValue(daysElement, "sunday") ? DaysOfWeek.Sunday : DaysOfWeek.None);
			}

			return days;
		}

		/// <summary>
		/// Looks up the normal work shifts
		/// </summary>
		/// <param name="shiftsElement">The element normal work shifts</param>
		/// <returns>The focus ID of the normal work shifts</returns>
		private long? LookupNormalWorkShifts(XElement shiftsElement)
		{
			string key = string.Empty;

			if (shiftsElement == null)
				return null;

			switch (shiftsElement.Value)
			{
				case "1":
					key = "FirstDay";
					break;

				case "2":
					key = "SecondEvening";
					break;

				case "3":
					key = "ThirdNight";
					break;

				case "4":
					key = "Rotating";
					break;

				case "5":
					key = "Split";
					break;

				case "6":
					key = "Varies";
					break;
			}

			return GetLookupId(LookupTypes.WorkShifts, key);
		}

		/// <summary>
		/// Looks up the job type
		/// </summary>
		/// <param name="jobTypeElement">The element holding the job type (part of job duration)</param>
		/// <returns>The focus ID of the job type</returns>
		/// <seealso cref="LookupJobEmploymentStatus"/>
		private long? LookupJobType(XElement jobTypeElement)
		{
			string key;

			if (jobTypeElement == null)
				return null;

			switch (jobTypeElement.Value)
			{
				case "3":
				case "6":
					key = "Permanent";
					break;

				case "2":
				case "5":
					key = "Temporary";
					break;

				case "7":
				case "8":
					key = "Seasonal";
					break;

				default:
					key = "Interim";
					break;
			}

			return GetLookupId(LookupTypes.JobTypes, key);
		}

		/// <summary>
		/// Looks up the Leaver Benenfits from the posting XML
		/// </summary>
		/// <param name="leaverElement">The XElement holding the Leaver Benenfits Flags </param>
		/// <returns>The Leaver Benenfits Flags</returns>
		private LeaveBenefits LookupLeaverBenefits(XElement leaverElement)
		{
			var benefits = LeaveBenefits.None;

			if (leaverElement != null)
			{
				benefits |= (GetBooleanValue(leaverElement, "paid_holidays") ? LeaveBenefits.PaidHolidays : LeaveBenefits.None);
				benefits |= (GetBooleanValue(leaverElement, "sick") ? LeaveBenefits.Sick : LeaveBenefits.None);
				benefits |= (GetBooleanValue(leaverElement, "vacation_paid_timeoff") ? LeaveBenefits.Vacation : LeaveBenefits.None);
				benefits |= (GetBooleanValue(leaverElement, "medical") ? LeaveBenefits.Medical : LeaveBenefits.None);
				benefits |= (GetBooleanValue(leaverElement, "leave_sharing") ? LeaveBenefits.LeaveSharing : LeaveBenefits.None);
			}

			return benefits;
		}

		/// <summary>
		/// Looks up the Retirement Benefits from the posting XML
		/// </summary>
		/// <param name="retirementElement">The XElement holding the Retirement Benefits Flags </param>
		/// <returns>The Retirement Benefits Flags</returns>
		private RetirementBenefits LookupRetirementBenefits(XElement retirementElement)
		{
			var benefits = RetirementBenefits.None;

			if (retirementElement != null)
			{
				benefits |= (GetBooleanValue(retirementElement, "pension_plan")
					? RetirementBenefits.PensionPlan
					: RetirementBenefits.None);
				benefits |= (GetBooleanValue(retirementElement, "k401") ? RetirementBenefits.Four01K : RetirementBenefits.None);
				benefits |= (GetBooleanValue(retirementElement, "profit_sharing")
					? RetirementBenefits.ProfitSharing
					: RetirementBenefits.None);
			}

			return benefits;
		}

		/// <summary>
		/// Looks up the Insurance Benefits from the posting XML
		/// </summary>
		/// <param name="insuranceElement">The XElement holding the Insurance Benefits Flags </param>
		/// <returns>The Insurance Benefits Flags</returns>
		private InsuranceBenefits LookupInsuranceBenefits(XElement insuranceElement)
		{
			var benefits = InsuranceBenefits.None;

			if (insuranceElement != null)
			{
				benefits |= (GetBooleanValue(insuranceElement, "dental") ? InsuranceBenefits.Dental : InsuranceBenefits.None);
				benefits |= (GetBooleanValue(insuranceElement, "health") ? InsuranceBenefits.Health : InsuranceBenefits.None);
				benefits |= (GetBooleanValue(insuranceElement, "life") ? InsuranceBenefits.Life : InsuranceBenefits.None);
				benefits |= (GetBooleanValue(insuranceElement, "disability") ? InsuranceBenefits.Disability : InsuranceBenefits.None);
				benefits |= (GetBooleanValue(insuranceElement, "health_savings")
					? InsuranceBenefits.HealthSavings
					: InsuranceBenefits.None);
				benefits |= (GetBooleanValue(insuranceElement, "vision") ? InsuranceBenefits.Vision : InsuranceBenefits.None);
			}

			return benefits;
		}

		/// <summary>
		/// Looks up the Miscellaneous Benefits from the posting XML
		/// </summary>
		/// <param name="miscellaneousElement">The XElement holding the Miscellaneous Benefits Flags </param>
		/// <returns>The Miscellaneous Benefits Flags</returns>
		private MiscellaneousBenefits LookupMiscellaneousBenefits(XElement miscellaneousElement)
		{
			var benefits = MiscellaneousBenefits.None;

			if (miscellaneousElement != null)
			{
				benefits |= (GetBooleanValue(miscellaneousElement, "benefits_negotiable")
					? MiscellaneousBenefits.BenefitsNegotiable
					: MiscellaneousBenefits.None);
				benefits |= (GetBooleanValue(miscellaneousElement, "tution_assitance")
					? MiscellaneousBenefits.TuitionAssistance
					: MiscellaneousBenefits.None);
				benefits |= (GetBooleanValue(miscellaneousElement, "clothing_allowance")
					? MiscellaneousBenefits.ClothingAllowance
					: MiscellaneousBenefits.None);
				benefits |= (GetBooleanValue(miscellaneousElement, "child_care")
					? MiscellaneousBenefits.ChildCare
					: MiscellaneousBenefits.None);
				benefits |= (GetBooleanValue(miscellaneousElement, "relocation")
					? MiscellaneousBenefits.Relocation
					: MiscellaneousBenefits.None);
				benefits |= (GetChildElementValue(miscellaneousElement, "other").Length > 0
					? MiscellaneousBenefits.Other
					: MiscellaneousBenefits.None);
			}

			return benefits;
		}

		/// <summary>
		/// Looks up the Contract Preferences from the posting XML. This will also set the associated job fields for each preference.
		/// </summary>
		/// <param name="job">The job to update</param>
		/// <param name="contactElement">The XElement holding the Contract Preferences</param>
		/// <returns>The Contract Preferences</returns>
		private ContactMethods LookupContactPreferences(JobDto job, XElement contactElement)
		{
			var preferences = ContactMethods.None;

			var ftAccount = GetChildElementValue(contactElement, "ft_account");
			if (ftAccount.Length > 0 && ftAccount != "0")
			{
				preferences |= ContactMethods.FocusTalent;
			}

			var email = GetChildElementValue(contactElement, "email");
			if (email.Length > 0)
			{
				preferences |= ContactMethods.Email;
				job.InterviewEmailAddress = email;
			}

			var applyonline = GetChildElementValue(contactElement, "applyonline");
			if (applyonline.Length > 0)
			{
				preferences |= ContactMethods.Online;
				job.InterviewApplicationUrl = FormatUrl(applyonline);
			}

			var mail = GetChildElementValue(contactElement, "mail");
			if (mail.Length > 0)
			{
				preferences |= ContactMethods.Mail;
				job.InterviewMailAddress = mail;
			}

			var fax = GetChildElementValue(contactElement, "fax");
			if (fax.Length > 0)
			{
				preferences |= ContactMethods.Fax;
				job.InterviewFaxNumber = fax;
			}

			var call = GetChildElementValue(contactElement, "call");
			if (call.Length > 0)
			{
				call = FormatPhoneNumber(call);
				preferences |= ContactMethods.Telephone;

				// ReSharper disable PossibleNullReferenceException
				var ext = contactElement.Element("call").Attribute("extension");
				// ReSharper restore PossibleNullReferenceException
				if (ext != null)
					call = string.Concat(call, " Ext: ", ext.Value);

				job.InterviewPhoneNumber = call;
			}

			var inperson = GetChildElementValue(contactElement, "inperson");
			if (inperson.Length > 0)
			{
				preferences |= ContactMethods.InPerson;
				job.InterviewDirectApplicationDetails = inperson;
			}

			job.InterviewOtherInstructions = GetChildElementValue(contactElement, "other");

			return preferences;
		}

		/// <summary>
		/// Looks up the Screening Preferences from the posting XML
		/// </summary>
		/// <param name="screeningElement">The XElement holding the Screening Preferences</param>
		/// <returns>The Screening Preferences</returns>
		private ScreeningPreferences LookupScreeningPreferences(XElement screeningElement)
		{
			var preferences = ScreeningPreferences.AllowUnqualifiedApplications;

			if (screeningElement != null && GetBooleanValue(screeningElement, "workforce"))
				preferences = GetBooleanValue(screeningElement, "workforce")
					? ScreeningPreferences.JobSeekersMustBeScreened
					: ScreeningPreferences.JobSeekersMustHave5StarMatch;

			return preferences;
		}

		/// <summary>
		/// Lookups the application status for the career 01 status
		/// </summary>
		/// <param name="status">The career 01 status.</param>
		/// <returns>The Application Status</returns>
		private ApplicationStatusTypes LookupApplicationStatus(int status)
		{
			switch (status)
			{
				case 0:
					return ApplicationStatusTypes.NotApplicable;
				case 1:
					return ApplicationStatusTypes.NewApplicant;
				case 2:
					return ApplicationStatusTypes.Interviewed;
				case 3:
					return ApplicationStatusTypes.Hired;
				case 4:
					return ApplicationStatusTypes.NotHired;
				case 5:
					return ApplicationStatusTypes.DidNotApply;
				case 6:
					return ApplicationStatusTypes.RefusedOffer;
			}

			return ApplicationStatusTypes.NewApplicant;
		}

		/// <summary>
		/// Gets the value of an attribute for an XElement
		/// </summary>
		/// <param name="parentElement">The parent XElement</param>
		/// <param name="attributeName">The name of the attribute</param>
		/// <returns>
		/// The value of the attribute, or an empty string should it not exist
		/// </returns>
		private string GetAttributeValue(XElement parentElement, string attributeName)
		{
			var attribute = parentElement.Attribute(attributeName);

			return (attribute == null || attribute.Value.Length == 0) ? string.Empty : attribute.Value;
		}

		/// <summary>
		/// Gets the value of a child element for an XElement
		/// </summary>
		/// <param name="parentElement">The parent XElement</param>
		/// <param name="childElementName">The name of the child element</param>
		/// <param name="index">Whether to target a specific index</param>
		/// <returns>
		/// The value of the child element, or an empty string should it not exist
		/// </returns>
		private string GetChildElementValue(XElement parentElement, string childElementName, int index = 0)
		{
			XElement childElement = null;
			if (index == 0)
			{
				childElement = parentElement.Element(childElementName);
			}
			else
			{
				var childElements = parentElement.Elements(childElementName).Take(index + 1).ToList();
				if (childElements.Count() > index)
					childElement = childElements[index];
			}
			return (childElement == null || childElement.Value.Length == 0) ? string.Empty : childElement.Value;
		}

		/// <summary>
		/// Gets the value of a child element's text node for an XElement
		/// </summary>
		/// <param name="parentElement">The parent XElement</param>
		/// <param name="childElementName">The name of the child element</param>
		/// <param name="index">Whether to target a specific index</param>
		/// <returns>
		/// The value of the child element, or an empty string should it not exist
		/// </returns>
		private string GetChildElementTextNodeValue(XElement parentElement, string childElementName, int index = 0)
		{
			XElement childElement = null;
			if (index == 0)
			{
				childElement = parentElement.Element(childElementName);
			}
			else
			{
				var childElements = parentElement.Elements(childElementName).Take(index + 1).ToList();
				if (childElements.Count() > index)
					childElement = childElements[index];
			}

			if (childElement == null)
				return string.Empty;

			var textNodes = childElement.Nodes().OfType<XText>().ToList();
			return textNodes.Any()
				? textNodes.First().Value
				: string.Empty;
		}

		/// <summary>
		/// Gets the integer value of a child element for an XElement
		/// </summary>
		/// <param name="parentElement">The parent XElement</param>
		/// <param name="childElementName">The name of the child element</param>
		/// <returns>The integer value of the child element, or null should it not exist</returns>
		private int? GetIntegerValue(XElement parentElement, string childElementName)
		{
			var childValue = GetChildElementValue(parentElement, childElementName);
			childValue = Regex.Replace(childValue, @"\+", "");

			if (childValue.Length == 0)
				return null;

			int result;
			if (int.TryParse(childValue, out result))
				return result;

			return null;
		}

		/// <summary>
		/// Gets the decimal value of a child element for an XElement
		/// </summary>
		/// <param name="parentElement">The parent XElement</param>
		/// <param name="childElementName">The name of the child element</param>
		/// <param name="fixDuffData">Attempt to fix some invalid data in V1</param>
		/// <returns>
		/// The decimal value of the child element, or null should it not exist
		/// </returns>
		private decimal? GetDecimalValue(XElement parentElement, string childElementName, bool fixDuffData = false)
		{
			var childValue = GetChildElementValue(parentElement, childElementName);
			childValue = Regex.Replace(childValue, @"\$|\£|\€", "");
			if (fixDuffData)
			{
				childValue = childValue.Replace("..", ".");
				if (Regex.IsMatch(childValue, @".+\..+\..+"))
				{
					var lastIndex = childValue.LastIndexOf(".", StringComparison.Ordinal);
					childValue = string.Concat(childValue.Substring(0, lastIndex).Replace(".", ""), childValue.Substring(lastIndex));
				}
			}
			return childValue.Length == 0 ? (decimal?)null : decimal.Parse(childValue);
		}

		/// <summary>
		/// Gets whether an element in the posting XML is marked as mandatory
		/// </summary>
		/// <param name="parentElement">The parent XElement</param>
		/// <returns>Whether the element is marked as 'mandate' or not</returns>
		private bool GetMandatoryValue(XElement parentElement)
		{
			return GetBooleanValue(parentElement, "mandate");
		}

		/// <summary>
		/// Gets the boolean value of a child element in the posting XML
		/// </summary>
		/// <param name="parentElement">The parent XElement</param>
		/// <param name="childElementName">The name of the child element holding the boolean value</param>
		/// <returns>The boolean value of the child element</returns>
		private bool GetBooleanValue(XElement parentElement, string childElementName)
		{
			if (parentElement == null)
				return false;

			var childElement = parentElement.Element(childElementName);
			return (childElement != null && (childElement.Value == "1"));
		}

		/// <summary>
		/// Gets the data value of a child element in the posting XML
		/// </summary>
		/// <param name="parentElement">The parent XElement</param>
		/// <param name="childElementName">The name of the child element holding the boolean value</param>
		/// <returns>The boolean value of the child element</returns>
		private DateTime? GetDateValueValue(XElement parentElement, string childElementName)
		{
			if (parentElement == null)
				return null;

			var childElement = parentElement.Element(childElementName);
			if (childElement == null || childElement.Value.Length == 0)
				return null;

			var dateString = Regex.Replace(childElement.Value, @"\b(\d+)(?:st|nd|rd|th)\b", "$1");

			DateTime date;
			if (!DateTime.TryParse(dateString, new CultureInfo("en-US"), DateTimeStyles.None, out date))
				return null;

			return date;
		}

		/// <summary>
		/// Converts the bytes for the image to a GIF image, and get the byte stream for the GIF
		/// </summary>
		/// <param name="bytes">The bytes array for the image currently in the database.</param>
		/// <returns>The byte array for the image converted to a gif</returns>
		private byte[] ConvertImageBytesToGif(byte[] bytes)
		{
			using (var memoryStream = new MemoryStream(bytes))
			{
				using (var img = Image.FromStream(memoryStream))
				{
					using (var newStream = new MemoryStream())
					{
						img.Save(newStream, ImageFormat.Gif);

						return newStream.ToArray();
					}
				}
			}
		}

		/// <summary>
		/// Performs a simple HTML decoding on text (as some values in the database have escaped characters in, but others don't)
		/// </summary>
		/// <param name="text">The text to unescape</param>
		/// <returns>Adjusted text</returns>
		private string UnescapeText(string text)
		{
			return text.Replace(@"&amp;", @"&").Replace(@"&apos;", @"'");
		}

		/// <summary>
		/// Gets the military occupation code by job title
		/// </summary>
		/// <param name="title">The job title</param>
		/// <returns>The military occupation code</returns>
		private string LookupMilitaryCodeByTitle(string title)
		{
			var jobTitles = GetMilitaryJobTitles();

			return jobTitles.Where(moc => moc.JobTitle.SubstringAfter(" - ").Equals(title, StringComparison.OrdinalIgnoreCase))
				.Select(moc => moc.JobTitle.SubstringBefore(" - "))
				.FirstOrDefault();
		}

		/// <summary>
		/// Looks up the duration (because in V1 duration actually references work type)
		/// </summary>
		/// <param name="durationId">The Id of the duration</param>
		/// <returns>The V3 duration id</returns>
		private long? LookupDurationId(long durationId)
		{
			var duration = GetLookupById(durationId);

			if (duration.IsNull())
				return null;

			switch (duration.ExternalId)
			{
				case "1":
				case "4":
					duration = GetLookup(LookupTypes.Durations, "FullTimeRegular");
					break;
			}

			return duration.Id;
		}

		/// <summary>
		/// Gets the military occupation code by job code
		/// </summary>
		/// <param name="code">The military occupation code</param>
		/// <returns>The job title</returns>
		private string LookupMilitaryTitleByCode(string code)
		{
			var jobTitles = GetMilitaryJobTitles();

			return jobTitles.Where(moc => moc.JobTitle.SubstringBefore(" - ").Equals(code, StringComparison.OrdinalIgnoreCase))
				.Select(moc => moc.JobTitle.SubstringAfter(" - "))
				.FirstOrDefault();
		}

		/// <summary>
		/// Gets the list of military job titles
		/// </summary>
		/// <returns>The list of military job titles</returns>
		private IEnumerable<MilitaryOccupationJobTitleViewDto> GetMilitaryJobTitles()
		{
			if (_militaryOccupations.IsNull())
				_militaryOccupations = LibraryRepository.MilitaryOccupationJobTitleViews.Select(j => j.AsDto()).ToList();
			return _militaryOccupations;
		}

		#endregion

		public void PreprocessEmployerTradeNameData(MigrationProviderArgs providerArgs, Database db)
		{
			var dbParamsPreprocess = new[]
			{
				new DatabaseParam("LastId", _lastEmployerTradeNameId),
				new DatabaseParam("CustomerId", providerArgs.CustomParm),
				new DatabaseParam("CutoffDate", providerArgs.CutOffDate),
			};
			db.ExecuteBulkStatement(
				"EXEC Migrations_StoreEmployerTradeNamesAndAddressByTradeNameId @LastId, @CustomerId, @CutoffDate",
				dbParamsPreprocess);
		}

		public void PostprocessEmployerTradeNameData()
		{
			using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
			{
				db.ExecuteBulkStatement("EXEC Migrations_DeleteEmployerTradeNamesMigrationTable");
			}
		}

		public List<EmployerTradeNamesRequest> MigrateEmployerTradeNames(MigrationProviderArgs providerArgs, bool preProcess)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
			{
				throw new Exception("Custom Parameter must be set to customer id");
			}

			var requests = new List<EmployerTradeNamesRequest>();

			using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
			{
				if (preProcess)
					PreprocessEmployerTradeNameData(providerArgs, db);

				var dbParams = new[]
				{
					new DatabaseParam("LastId", _lastEmployerTradeNameId),
					new DatabaseParam("BatchSize", providerArgs.BatchSize),
					new DatabaseParam("CustomerId", providerArgs.CustomParm),
					new DatabaseParam("CutoffDate", providerArgs.CutOffDate),
				};

				var dataReader =
					db.ExecuteReader("EXEC Migrations_GetPagedEmployerTradeNames @LastId, @BatchSize, @CustomerId, @CutoffDate", 500,
						dbParams);

				while (dataReader.Read())
				{
					var request = new EmployerTradeNamesRequest();

					request.Id = dataReader.GetInt32(dataReader.GetOrdinal("TradeNameId"));
					request.TradeNameId = string.Concat("FC01:", request.Id.ToString());
					request.TradeName = (dataReader["TradeName"] as string);
					request.IsDefault = Int32.Parse(dataReader["IsDefault"].ToString());
					request.EmployerId = Int32.Parse(dataReader["EmployerId"].ToString());
					request.EmployerLegalName = (dataReader["LegalName"] as string);
					request.EmployeeMigrationId = !dataReader["EmployeeId"].IsDBNull() ? dataReader["EmployeeId"].ToString() : null;
					request.Fein = dataReader["Fein"] as string;
					request.Line1 = !dataReader["Street1"].IsDBNull() ? dataReader["Street1"].ToString() : null;
					request.Line2 = !dataReader["Street2"].IsDBNull() ? dataReader["Street2"].ToString() : null;
					request.City = !dataReader["City"].IsDBNull() ? dataReader["City"].ToString() : null;
					request.State = !dataReader["State"].IsDBNull() ? dataReader["State"].ToString() : null;
					request.County = !dataReader["County"].IsDBNull() ? dataReader["County"].ToString() : null;
					request.PostalCode = !dataReader["PostalCode"].IsDBNull() ? dataReader["PostalCode"].ToString() : null;
					request.ExternalId = "FC01:" + request.EmployeeMigrationId;

					requests.Add(request);
				}
			}

			if (requests.Count != 0)
			{
				requests = requests.OrderBy(x => x.Id).ToList();
				requests = requests.Take(providerArgs.BatchSize).ToList();
				_lastEmployerTradeNameId = (int)requests.Last().Id;
			}
			return requests;
		}

		public List<SaveSpideredJobRequest> MigrateSpideredJobs(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			List<SaveSpideredJobRequest> requests = new List<SaveSpideredJobRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastSpideredJobId),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", providerArgs.CustomParm),
						new DatabaseParam("CutoffDate", providerArgs.CutOffDate)
					};

					var dataReader =
						db.ExecuteReader("EXEC Migrations_SpideredJobOrderRequests @LastId, @BatchSize, @CustomerId, @CutoffDate",
							dbParams);

					var focusJobIdOrdinal = dataReader.GetOrdinal("focusjobid");
					var lensPostingIdOrdinal = dataReader.GetOrdinal("LensPostingId");
					var jobTitleOrdinal = dataReader.GetOrdinal("JobTitle");
					var employerNameOrdinal = dataReader.GetOrdinal("EmployerName");
					var dateCreatedOrdinal = dataReader.GetOrdinal("DateCreated");
					var dateUpdatedOrdinal = dataReader.GetOrdinal("DateUpdated");
					var originIdOrdinal = dataReader.GetOrdinal("OriginId");
					var jobStatusOrdinal = dataReader.GetOrdinal("Status");

					while (dataReader.Read())
					{
						var request = new SaveSpideredJobRequest
						{
							JobMigrationId =
								string.Concat("FC01:", dataReader.GetInt32(focusJobIdOrdinal).ToString(CultureInfo.InvariantCulture))
						};

						try
						{
							request.Status = dataReader.GetInt32(jobStatusOrdinal);
							request.LensPostingMigrationId = dataReader.GetString(lensPostingIdOrdinal);
							request.JobTitle = dataReader.GetString(jobTitleOrdinal);
							request.EmployerName = dataReader.GetString(employerNameOrdinal);
							request.JobId = dataReader.GetInt32(focusJobIdOrdinal);
							request.DateCreated = dataReader.GetDateTime(dateCreatedOrdinal);
							request.DateUpdated = dataReader.GetDateTime(dateUpdatedOrdinal);
							request.OriginId = dataReader.GetInt32(originIdOrdinal);
						}
						catch (Exception ex)
						{
							request.MigrationValidationMessage = ex.Message;
						}

						requests.Add(request);

						if (requests.Count != 0)
						{
							requests = requests.OrderBy(x => x.JobId).ToList();
							requests = requests.Take(providerArgs.BatchSize).ToList();
							_lastSpideredJobId = (int)requests.Last().JobId;
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Migrate Spidered Jobs SQL Execution exception.", ex);
			}

			return requests;
		}

		public List<SaveSavedJobRequest> MigrateSavedJobs(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			var requests = new List<SaveSavedJobRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastSavedJobId),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", providerArgs.CustomParm),
						new DatabaseParam("CutoffDate", providerArgs.CutOffDate)
					};

					var dataReader =
						db.ExecuteReader("EXEC Migrations_SavedJobs @LastId, @BatchSize, @CustomerId, @CutoffDate", 300, dbParams);

					var focusJobIdOrdinal = dataReader.GetOrdinal("focusjobid");
					var lensPostingIdOrdinal = dataReader.GetOrdinal("LensPostingId");
					var jobTitleOrdinal = dataReader.GetOrdinal("JobTitle");
					var jobSeekerMigrationIdOrdinal = dataReader.GetOrdinal("JobSeekerMigrationId");
					var savedJobIdOrdinal = dataReader.GetOrdinal("SavedJobId");

					while (dataReader.Read())
					{
						var request = new SaveSavedJobRequest();
						{
							JobMigrationId = string.Concat("FC01:", dataReader.GetInt32(focusJobIdOrdinal).ToString(CultureInfo.InvariantCulture));
						}

						try
						{
							request.SavedJobId = dataReader.GetInt32(savedJobIdOrdinal);
							request.SavedJobMigrationId = string.Concat("FC01:", dataReader.GetInt32(savedJobIdOrdinal).ToString(CultureInfo.InvariantCulture));
							request.JobSeekerMigrationId = string.Concat("FC01:", dataReader.GetString(jobSeekerMigrationIdOrdinal));
							request.LensPostingMigrationId = dataReader.GetString(lensPostingIdOrdinal);
							request.BookmarkName = dataReader.GetString(jobTitleOrdinal); /* the bookmark name will be the job title */
							request.LensPostingMigrationId = dataReader.GetString(lensPostingIdOrdinal);
							request.JobId = dataReader.GetInt32(focusJobIdOrdinal);
						}
						catch (Exception ex)
						{
							request.MigrationValidationMessage = ex.Message;
						}

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateSavedJobs SQL Execution exception.", ex);
			}


			if (requests.Count != 0)
			{
				requests = requests.OrderBy(x => x.SavedJobId).ToList();
				requests = requests.Take(providerArgs.BatchSize).ToList();
				var lastItem = requests.Last();

				_lastSavedJobId = lastItem.SavedJobId;
			}

			return requests;

		}

		public List<ReferCandidateRequest> MigrateSpideredJobReferrals(MigrationProviderArgs providerArgs)
		{
			if (providerArgs.CustomParm.IsNullOrEmpty())
				throw new Exception("Custom Parameter must be set to customer id");

			var requests = new List<ReferCandidateRequest>();

			try
			{
				using (var db = CreateMsSqlDatabaseAccess(MigrationConnectionString, false))
				{
					var dbParams = new[]
					{
						new DatabaseParam("LastId", _lastSpideredJobReferralId),
						new DatabaseParam("BatchSize", providerArgs.BatchSize),
						new DatabaseParam("CustomerId", providerArgs.CustomParm),
						new DatabaseParam("CutoffDate", providerArgs.CutOffDate)
					};

					var dataReader = db.ExecuteReader(
						"EXEC Migrations_SpideredReferrals @LastId, @BatchSize, @CustomerId, @CutoffDate", 300, dbParams);

					var referralIdOrdinal = dataReader.GetOrdinal("ReferralId");
					var lensPostingIdOrdinal = dataReader.GetOrdinal("LensPostingId");
					//var scoreOrdinal = dataReader.GetOrdinal("Score");
					var staffIdOrdinal = dataReader.GetOrdinal("StaffId");
					//var statusOrdinal = dataReader.GetOrdinal("Status");
					var referredDateOrdinal = dataReader.GetOrdinal("ReferredDate");
					//var appliedDateOrdinal = dataReader.GetOrdinal("DateApplied");
					var migrationIdOrdinal = dataReader.GetOrdinal("MigrationId");
					var resumeMigrationIdOrdinal = dataReader.GetOrdinal("MigrationResumeId");

					var jobSeekerMigrationIdOrdinal = dataReader.GetOrdinal("JobSeekerMigrationId");
					var focusJobIdOrdinal = dataReader.GetOrdinal("focusjobid");

					while (dataReader.Read())
					{
						var request = new ReferCandidateRequest
						{
							ReferralMigrationId = string.Concat("FC01:", dataReader.GetString(migrationIdOrdinal))
						};

						try
						{
							request.ReferralId = dataReader.GetInt32(referralIdOrdinal);
							request.PersonId = 0;
							request.JobId = 0;
							//request.ApplicationScore = dataReader.GetInt32(scoreOrdinal);
							request.WaivedRequirements = null;
							request.StaffReferral = !dataReader.IsDBNull(staffIdOrdinal);
							request.DateReferred = dataReader.IsDBNull(referredDateOrdinal)
								? (DateTime?)null
								: dataReader.GetDateTime(referredDateOrdinal);
							//request.DateApplied = dataReader.IsDBNull(appliedDateOrdinal)
							//? (DateTime?) null
							//: dataReader.GetDateTime(appliedDateOrdinal);
							//request.InitialApplicationStatus = LookupApplicationStatus(dataReader.GetInt32(statusOrdinal));
							request.JobSeekerMigrationId = string.Concat("FC01:", dataReader.GetString(jobSeekerMigrationIdOrdinal));
							request.MigrationStaffId = string.Concat("AOSOS:", dataReader.GetString(staffIdOrdinal));
							request.JobMigrationId = string.Concat("FC01:",
								dataReader.GetInt32(focusJobIdOrdinal).ToString(CultureInfo.InvariantCulture));
							request.LensPostingId = dataReader.GetString(lensPostingIdOrdinal);
							request.ResumeMigrationId = dataReader.GetInt32(resumeMigrationIdOrdinal);
						}
						catch (Exception ex)
						{
							request.MigrationValidationMessage = ex.Message;
						}

						_lastSpideredJobReferralId = dataReader.GetInt32(referralIdOrdinal);

						requests.Add(request);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("MigrateReferrals SQL Execution exception.", ex);
			}

			return requests;

		}

		public string JobMigrationId { get; set; }

	}
}
