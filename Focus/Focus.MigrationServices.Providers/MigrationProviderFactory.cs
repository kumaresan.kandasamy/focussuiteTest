﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.MigrationServices.Providers.Implementations;
using Focus.MigrationServices.Providers.Interfaces;

#endregion

namespace Focus.MigrationServices.Providers
{
  public class MigrationProviderFactory
  {
    /// <summary>
    /// Loads the migration provider for employer
    /// </summary>
    /// <returns>The selected provider</returns>
    public static IMigrationProvider LoadMigrationProvider(string providerName)
    {
      try
      {
        switch (providerName.ToLowerInvariant())
        {
          case "fake": 
            return new FakeProvider();

          case "ajila":
          case "ajla":
            return new AjlaPovider();

          case "careertalent":
            return new CareerTalentProvider();

          case "focuscareer":
          case "focuscareer01":
            return new FocusCareer01Provider();

          case "aosos":
            return new AososProvider();

          case "taggedresume":
            return new TaggedResumeProvider();

          case "textfile":
            return new TextFileProvider();

          default:
            throw new Exception(string.Format("Migration provider named '{0}' is unknown", providerName));
        }
      }
      catch (Exception ex)
      {
        throw new ArgumentException(string.Concat("Migration Factory - Unable to load the defined provider: ", providerName), ex);
      }
    }
  }
}
