﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

namespace Focus.MigrationServices.Providers.Common
{
	public enum RecordType
	{
		None = 0,
		Employer = 1,
		AssistUser = 2,
		Job = 3,
		JobSeeker = 4,
		Resume = 5,
		SavedSearch = 6,
		ViewedPosting = 7,
		Notes = 8,
		Referral = 9,
    Activity = 10,
		Flagged = 11,
		Office = 12,
		OfficeMapping = 13,
		SearchAlert = 14,
		ResumeSearch = 15,
		ResumeAlert = 16,
		EmployerTradeName = 17,
		SpideredJobs = 18,
		SpideredJobReferrals = 19,
		SavedJob = 20,
	}
}
