﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;

using Focus.Core.Messages.AccountService;
using Focus.Core.Messages.EmployerService;

#endregion

namespace Focus.MigrationServices.Providers.Interfaces
{
  public interface IRegisterTalentUserRequest
  {
    /// <summary>
    /// Gets a list of employers requests to set up
    /// </summary>
    /// <param name="batchSize">The number of records required</param>
    /// <returns>A list of new employer users</returns>
    List<RegisterTalentUserRequest> MigrateEmployers(int batchSize);

    /// <summary>
    /// Gets a list of secondary business unit descriptions requests to set up
    /// </summary>
    /// <param name="batchSize">The number of records required</param>
    /// <returns>A list of new business unit descriptions</returns>
    List<SaveBusinessUnitDescriptionRequest> MigrateBusinessUnitDescriptions(int batchSize);

    /// <summary>
    /// Gets a list of business unit logo requests to set up
    /// </summary>
    /// <param name="batchSize">The number of records required</param>
    /// <returns>A list of new business unit logos</returns>
    List<SaveBusinessUnitLogoRequest> MigrateBusinessUnitLogos(int batchSize);
  }
}
