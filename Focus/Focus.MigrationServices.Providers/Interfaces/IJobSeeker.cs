﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;

using Focus.Core.Messages.AccountService;
using Focus.Core.Messages.AnnotationService;
using Focus.Core.Messages.CandidateService;
using Focus.Core.Messages.PostingService;

#endregion

namespace Focus.MigrationServices.Providers.Interfaces
{
  public interface IJobSeeker
  {
    /// <summary>
    /// Gets a list of job seeker requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>
    /// A list of new job seekers requests
    /// </returns>
    List<RegisterCareerUserRequest> MigrateJobSeekers(MigrationProviderArgs providerArgs);

    /// <summary>
    /// Gets a list of viewed postings for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of saved search requests</returns>
    List<AddViewedPostingRequest> MigrateViewedPostings(MigrationProviderArgs providerArgs);

    /// <summary>
    /// Gets a list of saved searches for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of saved search requests</returns>
    List<SaveSearchRequest> MigrateJobSeekerSavedSearches(MigrationProviderArgs providerArgs);

    /// <summary>
    /// Gets a list of referrals for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of referral requests</returns>
    List<ReferCandidateRequest> MigrateReferrals(MigrationProviderArgs providerArgs);

    /// <summary>
    /// Gets a list of activities for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of activities</returns>
    List<AssignActivityOrActionRequest> MigrateJobSeekerActivities(MigrationProviderArgs providerArgs);

    /// <summary>
    /// Gets a list of flags for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of flags</returns>
    List<ToggleCandidateFlagRequest> MigrateFlaggedJobSeekers(MigrationProviderArgs providerArgs);

    /// <summary>
    /// Gets a list of search alerts for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of search alert requests</returns>
    List<SaveSearchRequest> MigrateJobSeekerSearchAlerts(MigrationProviderArgs providerArgs);
  }
}
