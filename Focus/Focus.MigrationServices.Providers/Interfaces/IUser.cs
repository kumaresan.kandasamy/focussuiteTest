﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;

using Focus.Core.Messages.AccountService;
using Focus.Core.Messages.CoreService;
using Focus.Core.Messages.EmployerService;

#endregion

namespace Focus.MigrationServices.Providers.Interfaces
{
  public interface IUser
  {
    /// <summary>
    /// Gets a list of user requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>
    /// A list of new user requests
    /// </returns>
    List<CreateAssistUserRequest> MigrateUsers(MigrationProviderArgs providerArgs);

    /// <summary>
    /// Gets a list of office requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new office requests</returns>
    List<SaveOfficeRequest> MigrateOffices(MigrationProviderArgs providerArgs);

    /// <summary>
    /// Gets a list of office mapping requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new office mapping requests</returns>
    List<PersonOfficeMapperRequest> MigrateOfficeMappings(MigrationProviderArgs providerArgs);
      
    /// <summary>
    /// Gets a list of notes and reminders for job seekers
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>
    /// A list of notes and reminders requests
    /// </returns>
    List<SaveNoteReminderRequest> MigrateNotesAndReminders(MigrationProviderArgs providerArgs);
  }
}
