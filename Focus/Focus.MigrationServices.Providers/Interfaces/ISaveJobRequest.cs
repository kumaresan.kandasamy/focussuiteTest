﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;

using Focus.Core.Messages.JobService;

#endregion

namespace Focus.MigrationServices.Providers.Interfaces
{
  public interface ISaveJobRequest
  {
    /// <summary>
    /// Gets a list of job order requests to set up
    /// </summary>
    /// <param name="batchSize">The number of records required</param>
    /// <returns>A list of new job order requests</returns>
    List<SaveJobRequest> MigrateJobOrders(int batchSize);

    /// <summary>
    /// Gets a list of job certificate requests
    /// </summary>
    /// <param name="batchSize">The number of records required</param>
    /// <returns>A list of new job certificate requests</returns>
    List<SaveJobCertificatesRequest> MigrateJobCertificates(int batchSize);

    /// <summary>
    /// Gets a list of job language requests
    /// </summary>
    /// <param name="batchSize">The number of records required</param>
    /// <returns>A list of new job language requests</returns>
    List<SaveJobLanguagesRequest> MigrateJobLanguages(int batchSize);

    /// <summary>
    /// Gets a list of job licence requests
    /// </summary>
    /// <param name="batchSize">The number of records required</param>
    /// <returns>A list of new job licence requests</returns>
    List<SaveJobLicencesRequest> MigrateJobLicences(int batchSize);

    /// <summary>
    /// Gets a list of job special requirements requests
    /// </summary>
    /// <param name="batchSize">The number of records required</param>
    /// <returns>A list of new job special requirements requests</returns>
    List<SaveJobSpecialRequirementsRequest> MigrateJobSpecialRequirements(int batchSize);

    /// <summary>
    /// Gets a list of job locations requests
    /// </summary>
    /// <param name="batchSize">The number of records required</param>
    /// <returns>A list of new job location requests</returns>
    List<SaveJobLocationsRequest> MigrateJobLocations(int batchSize);

    /// <summary>
    /// Gets a list of job address requests
    /// </summary>
    /// <param name="batchSize">The number of records required</param>
    /// <returns>A list of new job address requests</returns>
    List<SaveJobAddressRequest> MigrateJobAddresses(int batchSize);

    /// <summary>
    /// Gets a list of job address requests
    /// </summary>
    /// <param name="batchSize">The number of records required</param>
    /// <returns>A list of new job address requests</returns>
    List<SaveJobProgramsOfStudyRequest> MigrateJobProgramsOfStudy(int batchSize);
  }
}
