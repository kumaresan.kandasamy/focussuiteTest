﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;

using Focus.Core.Messages.AccountService;

#endregion

namespace Focus.MigrationServices.Providers.Interfaces
{
  public interface IRegisterCareerUserRequest
  {
    /// <summary>
    /// Gets a list of job seeker requests
    /// </summary>
    /// <param name="batchSize">The number of records required</param>
    /// <returns>A list of new job seekers requests</returns>
    List<RegisterCareerUserRequest> MigrateJobSeekers(int batchSize);

  }
}
