﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;

using Focus.Core.Messages.AccountService;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Messages.SearchService;
using Framework.DataAccess;

#endregion

namespace Focus.MigrationServices.Providers.Interfaces
{
  public interface IEmployer
  {
    /// <summary>
    /// Gets a list of employers requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new employer users</returns>
    List<RegisterTalentUserRequest> MigrateEmployers(MigrationProviderArgs providerArgs);

    /// <summary>
    /// Gets a list of secondary business unit descriptions requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new business unit descriptions</returns>
    List<SaveBusinessUnitDescriptionRequest> MigrateBusinessUnitDescriptions(MigrationProviderArgs providerArgs);

    /// <summary>
    /// Gets a list of business unit logo requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new business unit logos</returns>
    List<SaveBusinessUnitLogoRequest> MigrateBusinessUnitLogos(MigrationProviderArgs providerArgs);

    /// <summary>
    /// Gets a list of resume searches requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new resume saved searches</returns>
    List<SaveCandidateSavedSearchRequest> MigrateResumeSearches(MigrationProviderArgs providerArgs);

    /// <summary>
    /// Gets a list of resume alerts requests to set up
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new resume alerts searches</returns>
    List<SaveCandidateSavedSearchRequest> MigrateResumeAlerts(MigrationProviderArgs providerArgs);
		
		/// <summary>
		/// Gets a list of employer tradenames requests to set up
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of employer tradenames</returns>
		List<EmployerTradeNamesRequest> MigrateEmployerTradeNames(MigrationProviderArgs providerArgs, bool preProcess);
		
		/// <summary>
		/// Store employer trade names and associated addresses in a temporary table for later migration
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of employer tradenames</returns>
		void PreprocessEmployerTradeNameData(MigrationProviderArgs providerArgs, Database db);

	  /// <summary>
	  /// Tidy up database employertradename artefacts
	  /// </summary>
	  void PostprocessEmployerTradeNameData();
  }
}
