﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core.Settings.Interfaces;
using Focus.Data.Repositories.Contracts;
using Focus.MigrationServices.Providers.Common;
using Focus.Services;

#endregion

namespace Focus.MigrationServices.Providers.Interfaces
{
  public interface IMigrationProvider
  {
    IConfigurationRepository ConfigurationRepository { get; set; }
    ILibraryRepository LibraryRepository { get; set; }
    IRuntimeContext RuntimeContext { get; set; }
    IAppSettings AppSettings { get; set; }
    bool SpoofData { get; set; }

    string MigrationConnectionString { get; set; }
    string MigrationFolderName { get; set; }
    string MigrationFileName { get; set; }

    void Initialise(RecordType recordType, long lastId);
  }
}
