﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core.Messages.CandidateService;
using Focus.Core.Messages.JobService;

#endregion

namespace Focus.MigrationServices.Providers.Interfaces
{
	public interface IJobOrder
	{
		/// <summary>
		/// Gets a list of job order requests to set up
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new job order requests</returns>
		List<SaveJobRequest> MigrateJobOrders(MigrationProviderArgs providerArgs);

		/// <summary>
		/// Gets a list of job certificate requests
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new job certificate requests</returns>
		List<SaveJobCertificatesRequest> MigrateJobCertificates(MigrationProviderArgs providerArgs);

		/// <summary>
		/// Gets a list of job language requests
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new job language requests</returns>
		List<SaveJobLanguagesRequest> MigrateJobLanguages(MigrationProviderArgs providerArgs);

		/// <summary>
		/// Gets a list of job licence requests
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new job licence requests</returns>
		List<SaveJobLicencesRequest> MigrateJobLicences(MigrationProviderArgs providerArgs);

		/// <summary>
		/// Gets a list of job special requirements requests
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new job special requirements requests</returns>
		List<SaveJobSpecialRequirementsRequest> MigrateJobSpecialRequirements(MigrationProviderArgs providerArgs);

		/// <summary>
		/// Gets a list of job locations requests
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new job location requests</returns>
		List<SaveJobLocationsRequest> MigrateJobLocations(MigrationProviderArgs providerArgs);

		/// <summary>
		/// Gets a list of job address requests
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new job address requests</returns>
		List<SaveJobAddressRequest> MigrateJobAddresses(MigrationProviderArgs providerArgs);

		/// <summary>
		/// Gets a list of job address requests
		/// </summary>
		/// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
		/// <returns>A list of new job address requests</returns>
		List<SaveJobProgramsOfStudyRequest> MigrateJobProgramsOfStudy(MigrationProviderArgs providerArgs);

		/// <summary>
		/// Migrates the job driving licence endorsements.
		/// </summary>
		/// <param name="providerArgs">The provider arguments.</param>
		/// <returns></returns>
		List<SaveJobDrivingLicenceEndorsementsRequest> MigrateJobDrivingLicenceEndorsements(MigrationProviderArgs providerArgs);

		/// <summary>
		/// Migrates the spidered job requests.
		/// </summary>
		/// <param name="providerArgs">The provider arguments.</param>
		/// <returns></returns>
		List<SaveSpideredJobRequest> MigrateSpideredJobs(MigrationProviderArgs _providerArgs);

		/// <summary>
		/// Migrates the spidered job referral requests.
		/// </summary>
		/// <param name="providerArgs">The provider arguments.</param>
		/// <returns></returns>
		List<ReferCandidateRequest> MigrateSpideredJobReferrals(MigrationProviderArgs _providerArgs);

		/// <summary>
		/// Migrates the saved jobs requests.
		/// </summary>
		/// <param name="providerArgs">The provider arguments.</param>
		/// <returns></returns>
		List<SaveSavedJobRequest> MigrateSavedJobs(MigrationProviderArgs _providerArgs);
	}
}
