﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;

using Focus.Core.Messages.ResumeService;

#endregion

namespace Focus.MigrationServices.Providers.Interfaces
{
  public interface IResume
  {

    /// <summary>
    /// Gets a list of resume requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new resume requests</returns>
    List<SaveResumeRequest> MigrateResumes(MigrationProviderArgs providerArgs);

    /// <summary>
    /// Gets a list of resume document requests
    /// </summary>
    /// <param name="providerArgs">Structure holding batch size, custom parameter, and last processed id</param>
    /// <returns>A list of new resume document requests</returns>
    List<SaveResumeDocumentRequest> MigrateResumeDocuments(MigrationProviderArgs providerArgs);
  }
}
