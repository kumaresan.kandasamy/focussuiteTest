﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

using Focus.Core;
using Focus.Core.Settings.Interfaces;
using Focus.Core.Views;
using Focus.Data.Repositories.Contracts;
using Focus.Services;

using Framework.Caching;
using Framework.DataAccess;

#endregion


namespace Focus.MigrationServices.Providers
{
  public class MigrationProviderBase
  {
    protected struct PhoneNumberDetails
    {
      public string OriginalNumber;
      public string MainNumber;
      public string Extension;
    }

    private List<LookupItemView> _lookupItems;

    private readonly Dictionary<LookupTypes, Dictionary<string, LookupItemView>> _lookupsByKey;
    private readonly Dictionary<LookupTypes, Dictionary<string, List<LookupItemView>>> _lookupsByText;
    private readonly Dictionary<LookupTypes, Dictionary<string, List<LookupItemView>>> _lookupsByExternalId;

    public string MigrationConnectionString { get; set; }
    public string MigrationFolderName { get; set; }
    public string MigrationFileName { get; set; }

    public IConfigurationRepository ConfigurationRepository { get; set; }
    public ILibraryRepository LibraryRepository { get; set; }
		public ICoreRepository CoreRepository { get; set; }
    public IRuntimeContext RuntimeContext { get; set; }

    public IAppSettings AppSettings { get; set; }

    public bool SpoofData { get; set; }

    /// <summary>
    /// Base constructor for all providers
    /// </summary>
    public MigrationProviderBase()
    {
      MigrationConnectionString = ConfigurationManager.AppSettings.Get("Focus-MigrationSource-ConnStr");
      MigrationFolderName = ConfigurationManager.AppSettings.Get("Focus-MigrationSource-Folder");
      MigrationFileName = ConfigurationManager.AppSettings.Get("Focus-MigrationSource-File");

      _lookupsByKey = new Dictionary<LookupTypes, Dictionary<string, LookupItemView>>();
      _lookupsByText = new Dictionary<LookupTypes, Dictionary<string, List<LookupItemView>>>();
      _lookupsByExternalId = new Dictionary<LookupTypes, Dictionary<string, List<LookupItemView>>>();
    }

    /// <summary>
    /// Creates the object to allow an MS SQL database to be accessed.
    /// </summary>
    /// <param name="connectionString">The connection string to use to connect to the database.</param>
    /// <param name="withTransaction">Whether to begin a SQL transaction.</param>
    /// <returns>The Database connection object.</returns>
    protected Database CreateMsSqlDatabaseAccess(string connectionString, bool withTransaction)
    {
      return new Database(connectionString, DatabaseType.MsSql, withTransaction);
    }

    /// <summary>
    /// Get the Industrial Classification from a code.
    /// </summary>
    /// <param name="code">The NAICS code to look up.</param>
    /// <returns>The full string for the industrial classification.</returns>
    protected string GetIndustrialClassification(string code)
    {
      var naics = LibraryRepository.NAICS.FirstOrDefault(n => n.Code == code);

      return (naics == null) ? code : string.Concat(code, " - ", naics.Name);
    }

    /// <summary>
    /// Get the Occupation (Onet) Id from a code.
    /// </summary>
    /// <param name="code">The Onet code to look up.</param>
    /// <returns>The Id for the Occupation (Onet).</returns>
    protected long? GetOnetIdByCode(string code)
    {
      var onets = Cacher.Get<Dictionary<string, long>>("MigrationProviderBase:Onets");
      if (onets == null)
      {
        onets = LibraryRepository.Onets.ToDictionary(o => o.OnetCode, o => o.Id);
        Cacher.Set("MigrationProviderBase:Onets", onets);
      }

      return onets.ContainsKey(code) ? onets[code] : (long?)null;
    }

    /// <summary>
    /// Get the Occupation (Onet) Id from a phrase.
    /// </summary>
    /// <param name="phrase">The Onet code to look up.</param>
    /// <returns>The Id for the Occupation (Onet).</returns>
    protected long? GetOnetIdByPhrase(string phrase)
    {
      phrase = phrase.ToLower();
      phrase = Regex.Replace(phrase, "-", " ");
      phrase = Regex.Replace(phrase, "[^a-z ]", "");

      var phrases = Cacher.Get<Dictionary<string, long>>("MigrationProviderBase:OnetPhrases");
      if (phrases == null)
      {
        phrases = new Dictionary<string, long>();
        var allphrases = LibraryRepository.OnetPhrases.ToList();
        allphrases.ForEach(p =>
        {
          if (!phrases.ContainsKey(p.Phrase))
            phrases.Add(p.Phrase.ToLower(), p.OnetId);
        });
        Cacher.Set("MigrationProviderBase:OnetPhrases", phrases);
      }
      
      return phrases.ContainsKey(phrase) ? phrases[phrase] : (long?)null;
    }

    /// <summary>
    /// Formats an 9-digit FEIN into xx-xxxxxxx format
    /// </summary>
    /// <param name="numberToFormat">The number to format</param>
    /// <returns>The formatted FEIN</returns>
    protected string FormatFEIN(string numberToFormat)
    {
      return (string.IsNullOrEmpty(numberToFormat) || numberToFormat.Length < 3)
        ? (numberToFormat ?? "")
        : string.Format("{0}-{1}", numberToFormat.Substring(0, 2), numberToFormat.Substring(2));
    }

    /// <summary>
    /// Formats an 10-digit phone number into (xxx) xxx-xxxx format
    /// </summary>
    /// <param name="numberToFormat">The number to format</param>
    /// <returns>The formatted phone number</returns>
    protected string FormatPhoneNumber(string numberToFormat)
    {
      if (numberToFormat == "Unspecified")
        return numberToFormat;

      return (string.IsNullOrEmpty(numberToFormat) || numberToFormat.Length < 7 || numberToFormat.Contains("(") || numberToFormat.Contains("-"))
        ? (numberToFormat ?? "")
        : string.Format("({0}) {1}-{2}", numberToFormat.Substring(0, 3), numberToFormat.Substring(3, 3), numberToFormat.Substring(6));
    }

    /// <summary>
    /// Splits a phone number of the form of either 'xxxxxxxx' or 'xxxxxxxxx-xxxx' into a phone number and extension
    /// </summary>
    /// <param name="phoneNumber">The original phone number</param>
    /// <returns>Details of the number</returns>
    protected PhoneNumberDetails ExtractPhoneNumber(string phoneNumber)
    {
      var details = new PhoneNumberDetails
      {
        OriginalNumber = phoneNumber,
        MainNumber = "",
        Extension = ""
      };

      if (string.IsNullOrEmpty(phoneNumber))
        return details;

      var parts = phoneNumber.Split(new[] {'-'}, StringSplitOptions.RemoveEmptyEntries);

      if (parts.Length == 0)
        return details;

      details.MainNumber = FormatPhoneNumber(parts[0]);
      if (parts.Length > 1)
        details.Extension = parts[1];

      return details;
    }

    /// <summary>
    /// Ensures a URL is prefixed with http://
    /// </summary>
    /// <param name="url">The url to check.</param>
    /// <param name="checkPattern">Whether to validate the URL format</param>
    /// <returns>
    /// The formatted url.
    /// </returns>
    protected string FormatUrl(string url, bool checkPattern = false)
    {
      if (url.Length > 0)
      {
        if (!url.StartsWith("http://") && !url.StartsWith("https://"))
          url = string.Concat("http://", url);

        if (checkPattern && !Regex.IsMatch(url, AppSettings.UrlRegExPattern))
          url = null;
      }
      else
      {
        url = null;
      }

      return url;
    }

    /// <summary>
    /// Gets the lookup record by Id
    /// </summary>
    /// <param name="lookupId">The id of the lookup</param>
    /// <returns>The lookup</returns>
    protected LookupItemView GetLookupById(long lookupId)
    {
      if (_lookupItems == null)
        _lookupItems = GetLookupItems();

      return _lookupItems.FirstOrDefault(lookup => lookup.Id == lookupId);
    }

    /// <summary>
    /// Looks up the focus look up record for a value from the source db.
    /// </summary>
    /// <param name="lookupType">The type of entity to lookup.</param>
    /// <param name="lookupValue">The value from the source db to lookup.</param>
    /// <returns>The look up record from the focus db (null if not found).</returns>
    protected LookupItemView GetLookup(LookupTypes lookupType, string lookupValue)
    {
      return GetLookup(lookupType, lookupValue, 0);
    }

    /// <summary>
    /// Looks up the focus look up record for a value from the source db.
    /// </summary>
    /// <param name="lookupType">The type of entity to lookup.</param>
    /// <param name="lookupValue">The value from the source db to lookup.</param>
    /// <param name="parentId">The ID of the parent record</param>
    /// <returns>The look up record from the focus db (null if not found).</returns>
    protected LookupItemView GetLookup(LookupTypes lookupType, string lookupValue, long parentId)
    {
      var lookupByKey = _lookupsByKey.ContainsKey(lookupType)
                                  ? _lookupsByKey[lookupType]
                                  : new Dictionary<string, LookupItemView>();

      var lookupByText = _lookupsByText.ContainsKey(lookupType)
                                  ? _lookupsByText[lookupType]
                                  : new Dictionary<string, List<LookupItemView>>();

      var lookupByExternalId = _lookupsByExternalId.ContainsKey(lookupType)
                                  ? _lookupsByExternalId[lookupType]
                                  : new Dictionary<string, List<LookupItemView>>();

      if (!_lookupsByExternalId.ContainsKey(lookupType))
      {
        _lookupsByKey.Add(lookupType, lookupByKey);
        _lookupsByText.Add(lookupType, lookupByText);
        _lookupsByExternalId.Add(lookupType, lookupByExternalId);

        PopulateDictionaries(lookupType, lookupByKey, lookupByText, lookupByExternalId);
      }

      lookupValue = lookupValue.ToLowerInvariant();

      if (lookupByKey.ContainsKey(lookupValue))
        return lookupByKey[lookupValue];

      if (lookupByText.ContainsKey(lookupValue))
      {
        var returnValue = lookupByText[lookupValue].FirstOrDefault(lu => lu.ParentId == parentId);
        if (returnValue != null)
          return returnValue;
      }

      return lookupByExternalId.ContainsKey(lookupValue) ? lookupByExternalId[lookupValue].FirstOrDefault(lu => lu.ParentId == parentId || parentId == 0) : null;
    }

    /// <summary>
    /// Looks up the focus id for a value from the source db.
    /// </summary>
    /// <param name="lookupType">The type of entity to lookup.</param>
    /// <param name="lookupValue">The value from the source db to lookup.</param>
    /// <returns>The id from the focus db (null if not found).</returns>
    protected long? GetLookupId(LookupTypes lookupType, string lookupValue)
    {
      return GetLookupId(lookupType, lookupValue, 0);
    }

    /// <summary>
    /// Looks up the focus id for a value from the source db.
    /// </summary>
    /// <param name="lookupType">The type of entity to lookup.</param>
    /// <param name="lookupValue">The value from the source db to lookup.</param>
    /// <param name="parentId">The ID of the parent record</param>
    /// <returns>The id from the focus db (null if not found).</returns>
    protected long? GetLookupId(LookupTypes lookupType, string lookupValue, long parentId)
    {
      var lookup = GetLookup(lookupType, lookupValue, parentId);
      return (lookup == null) ? (long?)null : lookup.Id;
    }

    /// <summary>
    /// Looks up the focus id for a value from the source db.
    /// </summary>
    /// <param name="lookupType">The type of entity to lookup.</param>
    /// <param name="lookupValue">The value from the source db to lookup.</param>
    /// <param name="defaultLookupValue">A value to look up if the source value is not found.</param>
    /// <returns>The id from the focus db (0 if not found).</returns>
    protected long GetLookupId(LookupTypes lookupType, string lookupValue, string defaultLookupValue)
    {
      return GetLookupId(lookupType, lookupValue ,defaultLookupValue,  0);
    }

    /// <summary>
    /// Looks up the focus id for a value from the source db.
    /// </summary>
    /// <param name="lookupType">The type of entity to lookup.</param>
    /// <param name="lookupValue">The value from the source db to lookup.</param>
    /// <param name="defaultLookupValue">A value to look up if the source value is not found.</param>
    /// <param name="parentId">The ID of the parent record</param>
    /// <returns>The id from the focus db (0 if not found).</returns>
    protected long GetLookupId(LookupTypes lookupType, string lookupValue, string defaultLookupValue, long parentId)
    {
      var checkValue = GetLookup(lookupType, lookupValue, parentId);
      if (checkValue != null)
        return checkValue.Id;

      checkValue = GetLookup(lookupType, defaultLookupValue, parentId);
      return (checkValue != null) ? checkValue.Id : 0;
    }
    /// <summary>
    /// Populates the lookup dictionaries for a look up type. There will be one to look up via the External ID, and one to look up by text.
    /// </summary>
    /// <param name="lookupType">The type to look up.</param>
    /// <param name="keyDictionary">The dictionary of keys.</param>
    /// <param name="textDictionary">The dictionary of look up text values.</param>
    /// <param name="externalIdDictionary">The dictionary of external Ids.</param>
    private void PopulateDictionaries(LookupTypes lookupType, Dictionary<string, LookupItemView> keyDictionary, Dictionary<string, List<LookupItemView>> textDictionary, Dictionary<string, List<LookupItemView>> externalIdDictionary)
    {
      if (_lookupItems == null)
        _lookupItems = GetLookupItems();

      foreach (var lookup in _lookupItems.Where(l => l.LookupType == lookupType))
      {
        var key = lookup.Key.Substring(lookup.Key.IndexOf(".", System.StringComparison.InvariantCulture) + 1).ToLowerInvariant();
        if (!keyDictionary.ContainsKey(key))
          keyDictionary.Add(key, lookup);

        var textToLookup = lookup.Text.ToLowerInvariant();
        if (!textDictionary.ContainsKey(textToLookup))
          textDictionary.Add(textToLookup, new List<LookupItemView>());

        textDictionary[textToLookup].Add(lookup);
          
        if (lookup.ExternalId.Length > 0)
        {
          var externalIdToLookup = lookup.ExternalId.ToLowerInvariant();
          if (!externalIdDictionary.ContainsKey(externalIdToLookup))
            externalIdDictionary.Add(externalIdToLookup, new List<LookupItemView>());

          externalIdDictionary[externalIdToLookup].Add(lookup);
        }
      }
    }

    /// <summary>
    /// Gets the lookup items from the repository.
    /// </summary>
    /// <returns>A list of all look-up items.</returns>
    private List<LookupItemView> GetLookupItems()
    {
      var cultureKeys = 
        (
          from lu in ConfigurationRepository.LookupItemsViews
          where lu.Culture == Thread.CurrentThread.CurrentUICulture.ToString().ToUpperInvariant()
          select new LookupItemView(lu.Id, lu.Key, lu.LookupType, lu.Value, lu.DisplayOrder, lu.ParentId, lu.ParentKey, lu.ExternalId, lu.CustomFilterId)
        ).ToList();

      var defaultKeys =
        (
          from lu in ConfigurationRepository.LookupItemsViews
          where lu.Culture == "**-**"
          select new LookupItemView(lu.Id, lu.Key, lu.LookupType, lu.Value, lu.DisplayOrder, lu.ParentId, lu.ParentKey, lu.ExternalId, lu.CustomFilterId)
        ).ToList();

      var removeSet = 
        (
          from rs in defaultKeys
          join ks in cultureKeys on
						new { rs.Key, rs.LookupType }
						equals
						new { ks.Key, ks.LookupType }
					select rs
        ).ToList();

      removeSet.ForEach(x => defaultKeys.Remove(x));
      return cultureKeys.Union(defaultKeys).ToList();
    }
  }

  public struct MigrationProviderArgs
  {
    public int BatchSize;
    public string CustomParm;
    public DateTime? CutOffDate;
  }
}
