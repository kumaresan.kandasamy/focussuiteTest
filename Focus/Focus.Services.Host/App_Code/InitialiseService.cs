﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using Framework.Logging;
using Framework.ServiceLocation;

#endregion

namespace Focus.Services.Host
{
  public class ServiceMessageInspector : IDispatchMessageInspector
  {
    private static ServicesRuntime<StructureMapServiceLocator> _servicesRuntime;

    public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
    {
      if (_servicesRuntime == null)
      {
        _servicesRuntime = new ServicesRuntime<StructureMapServiceLocator>(ServiceRuntimeEnvironment.Wcf, new StructureMapServiceLocator());
        _servicesRuntime.Start();
      }

      _servicesRuntime.BeginRequest();

      return null;
    }

    public void BeforeSendReply(ref Message reply, object correlationState)
    {
      if (_servicesRuntime != null)
      {
        _servicesRuntime.EndRequest();
        Logger.Flush(true);
      }
    }
  }

  [AttributeUsage(AttributeTargets.Class)]
  public class ServiceOutputBehavior : Attribute, IServiceBehavior
  {
    public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
    {
    }

    public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
    {
      foreach (ChannelDispatcherBase t in serviceHostBase.ChannelDispatchers)
      {
        var channelDispatcher = t as ChannelDispatcher;
        if (channelDispatcher != null)
        {
          foreach (var endpointDispatcher in channelDispatcher.Endpoints)
          {
            var inspector = new ServiceMessageInspector();
            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(inspector);
          }
        }
      }
    }

    public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
    {
    }
  }

  public class ServiceOutputBehaviorExtensionElement : BehaviorExtensionElement
  {
    protected override object CreateBehavior()
    {
      return new ServiceOutputBehavior();
    }

    public override Type BehaviorType
    {
      get
      {
        return typeof(ServiceOutputBehavior);
      }
    }
  }


}