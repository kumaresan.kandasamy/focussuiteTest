USE [FocusTestRepReporting]
GO

/****** Object:  StoredProcedure [dbo].[Reporting_JobOrder]    Script Date: 02/25/2013 14:50:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[Reporting_JobOrder]
	@DateFrom	DATETIME,
	@DateTo	DATETIME,
	@County	NVARCHAR(MAX),
	@Office NVARCHAR(MAX),
	@WIB NVARCHAR(MAX),
	@Radius INT,
	@ZIP NVARCHAR(20),
	@OccupationGroup NVARCHAR(MAX),
	@DetailedOccupation NVARCHAR(MAX),
	@SalaryFrequency INT,
	@MinSalary	DECIMAL,
	@MaxSalary	DECIMAL,
	@JobStatus	INT,
	@EducationLevel	INT,
	@SearchTerms NVARCHAR(MAX),
	@SearchType INT,
	@KeyWordsInJobTitle BIT,
	@TargetedSectors NVARCHAR(MAX), -- To do
	@WOTCInterests NVARCHAR(MAX),
	@FEIN NVARCHAR(10),
	@EmployerName NVARCHAR(50),
	@JobTitle NVARCHAR(50),
	@Flags INT,
	@SearchAccountCreationDate BIT,
	@SearchAssistanceRequested BIT,
	@Compliance INT,
	@SuppressJobOrder BIT,
	@SelfService BIT,
	@StaffAssisted BIT,
	@Spidered BIT,
	@PageNumber	INT,
	@PageSize	INT,
	@SortOrder	VARCHAR(20)

AS

CREATE TABLE #JobOrder
(
	Id BIGINT,
	JobTitle NVARCHAR(400), 
	EmployerName NVARCHAR(100), 
	City NVARCHAR(50), 
	PostingDate DATETIME, 
	ClosingDate DATETIME, 
	Openings INT, 
	SpecialConditions NVARCHAR(100),
	FilterCounties INT, 
	FilterOffices INT,
	FilterZips INT,
	FilterWOTCInterest INT,
	FilterONetOccupationGroups INT,
	FilterONetDetailedOccupations INT,
	FilterTargetedSectors INT
)

DECLARE @Counties TABLE ( Id int, Value nvarchar(100))
DECLARE @Offices TABLE ( Id int, Value nvarchar(100))
DECLARE @WIBs TABLE ( Id int, Value nvarchar(100))
DECLARE @ONetOccupationGroups TABLE ( Id int, Value nvarchar(100))
DECLARE @ONetDetailedOccupations TABLE ( Id int, Value nvarchar(100))
DECLARE @TargetedSector TABLE ( Id INT, Value NVARCHAR(100))
DECLARE @WOTCInterest TABLE ( Id INT, Value NVARCHAR(100))
DECLARE @KeywordSearchTerms TABLE (	Id INT, Value NVARCHAR(100), Processed BIT DEFAULT 0)
CREATE TABLE #Zips ( ZipCode VARCHAR(10))

CREATE TABLE #KeyWordJobOrder ( Id	bigint )

INSERT INTO @KeywordSearchTerms ( Id, Value ) SELECT Id, Value FROM dbo.Split(NULLIF(@SearchTerms,''), '|');
INSERT INTO @ONetDetailedOccupations ( Id, Value ) SELECT Id, Value FROM dbo.Split(NULLIF(@DetailedOccupation, ''), '|');
INSERT INTO @ONetOccupationGroups (	Id, Value ) SELECT Id, Value FROM dbo.Split(NULLIF(@OccupationGroup, ''), '|');
INSERT INTO @Counties (	Id, Value ) SELECT Id, Value FROM dbo.Split(NULLIF(@County, ''), '|');
INSERT INTO @Offices ( Id, Value ) SELECT Id, Value FROM dbo.Split(NULLIF(@Office, ''), '|');
INSERT INTO @WIBs (	Id, Value ) SELECT Id, Value FROM dbo.Split(NULLIF(@WIB, ''), '|');
INSERT INTO @TargetedSector ( Id, Value ) SELECT Id, Value FROM	dbo.Split(NULLIF(@TargetedSectors, ''), '|');
INSERT INTO @WOTCInterest ( Id, Value ) SELECT Id, Value FROM dbo.Split(NULLIF(@WOTCInterests, ''), '|');

DECLARE @TrimmedZip NVARCHAR(20)
SET @TrimmedZip = RTRIM(LTRIM(@ZIP))
IF (@TrimmedZip <> '')
	BEGIN
		INSERT INTO #Zips
		SELECT z2.ZipCode
		FROM dbo.ZipCodes z1
		CROSS JOIN dbo.ZipCodes z2
		WHERE z1.zipcode = @TrimmedZip
		AND dbo.CalculateDistance(z1.Longitude, z1.Latitude, z2.Longitude, z2.Latitude) < @Radius
	END
ELSE
	BEGIN
		INSERT INTO #Zips VALUES (NULL) 
	END
	
DECLARE @KeyWordSearchSql NVARCHAR(max)
DECLARE @CurrentKeyWord NVARCHAR(max)
DECLARE @CurrentKeyWordId NVARCHAR(max)
DECLARE @ProcessKeyWords BIT
DECLARE @SearchOperator NVARCHAR(4)
SET @ProcessKeyWords = 0
SET @KeyWordSearchSql= ''

IF @SearchType = 1
	SET @SearchOperator = 'and'
ELSE IF @SearchType = 2 or @KeyWordsInJobTitle = 1
	SET @SearchOperator = 'or'

IF EXISTS (SELECT 1 FROM @KeywordSearchTerms WHERE Value IS NOT NULL)
BEGIN
	SET @ProcessKeyWords = 1
	WHILE EXISTS (SELECT 1 FROM @KeywordSearchTerms where Processed = 0)
	BEGIN
		SELECT TOP 1 @CurrentKeyWordId = Id, @CurrentKeyWord = Value FROM @KeywordSearchTerms where Processed = 0
		
		IF LEN(@KeyWordSearchSql) > 0
			SET @KeyWordSearchSql = @KeyWordSearchSql + ' ' + @SearchOperator + ' '
		ELSE
			SET @KeyWordSearchSql = ' '	
		
		IF @KeyWordsInJobTitle = 1 -- We only need to worry about job title and not the others
			set @KeyWordSearchSql = @KeyWordSearchSql + 'jo.JobTitle like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'''
		ELSE
		BEGIN
			SET @KeyWordSearchSql = @KeyWordSearchSql + '(jo.JobTitle like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'''
			SET @KeyWordSearchSql = @KeyWordSearchSql + ' or jo.EmployerName like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'''
			SET @KeyWordSearchSql = @KeyWordSearchSql + ' or jo.JobDescription like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'')'
		END		
						
		UPDATE @KeywordSearchTerms SET Processed = 1 WHERE @CurrentKeyWordId = Id AND @CurrentKeyWord = Value
	END
	SET @KeyWordSearchSql = 'insert into #KeyWordJobOrder (id) select distinct jo.id from dbo.JobOrder jo where ' + @KeyWordSearchSql
	EXECUTE sp_executesql @KeyWordSearchSql;
END
ELSE
BEGIN
	INSERT INTO #KeyWordJobOrder (Id) Values (NULL)
END;

DECLARE @FilterCounties INT
SELECT TOP 1 @FilterCounties = CASE ISNULL(Value, '') WHEN ''  THEN 1 ELSE 0 END FROM @Counties

DECLARE @FilterOffices INT
SELECT TOP 1 @FilterOffices = CASE ISNULL(Value, '') WHEN ''  THEN 1 ELSE 0 END FROM @Offices

DECLARE @FilterZips INT
SELECT TOP 1 @FilterZips = CASE ISNULL(ZipCode, '') WHEN ''  THEN 1 ELSE 0 END FROM #Zips

DECLARE @FilterWOTCInterest INT
SELECT TOP 1 @FilterWOTCInterest = CASE ISNULL(Value, '') WHEN ''  THEN 1 ELSE 0 END FROM @WOTCInterest

DECLARE @FilterONetOccupationGroups INT
SELECT TOP 1 @FilterONetOccupationGroups = CASE ISNULL(Value, '') WHEN ''  THEN 1 ELSE 0 END FROM @ONetOccupationGroups

DECLARE @FilterONetDetailedOccupations INT
SELECT TOP 1 @FilterONetDetailedOccupations = CASE ISNULL(Value, '') WHEN ''  THEN 1 ELSE 0 END FROM @ONetDetailedOccupations

DECLARE @FilterTargetedSectors INT
SELECT TOP 1 @FilterTargetedSectors = CASE ISNULL(Value, '') WHEN ''  THEN 1 ELSE 0 END FROM @TargetedSector

INSERT INTO #JobOrder
(
	Id,
	JobTitle,
	EmployerName,
	City,
	PostingDate,
	ClosingDate,
	Openings,
	SpecialConditions,
	FilterCounties,
	FilterOffices,
	FilterZips,
	FilterWOTCInterest,
	FilterONetOccupationGroups,
	FilterONetDetailedOccupations,
	FilterTargetedSectors
)
SELECT
	jo.Id,
	jo.JobTitle, 
	jo.EmployerName, 
	jo.City, 
	jo.PostingDate, 
	jo.ClosingDate, 
	jo.Openings, 
	SUBSTRING(jo.SpecialConditions,1,100) AS SpecialConditions,
	@FilterCounties,
	@FilterOffices,
	@FilterZips,
	@FilterWOTCInterest,
	@FilterONetOccupationGroups,
	@FilterONetDetailedOccupations,
	@FilterTargetedSectors
FROM 
	dbo.JobOrder jo
INNER JOIN #KeyWordJobOrder kst
	ON kst.Id = jo.Id OR @ProcessKeyWords = 0
LEFT JOIN dbo.Employer e
	ON jo.EmployerId  = e.Id
WHERE
	(ISNULL(@SalaryFrequency,0) = 0
	OR
		(jo.SalaryFrequency = @SalaryFrequency
		AND	jo.MinSalary < @MaxSalary
		AND jo.MaxSalary > @MinSalary
		)
	)
	AND (jo.JobStatus & @JobStatus <> 0 or @JobStatus = 0)
	AND (jo.EducationLevel & @EducationLevel <> 0 or @EducationLevel = 0)
	AND ISNULL(e.FederalEmployerIdentificationNumber, '') LIKE '%' + ISNULL(@FEIN, '') + '%'
	AND ISNULL(jo.EmployerName, '') LIKE '%' + ISNULL(@EmployerName,'') + '%'
	AND ISNULL(jo.JobTitle, '') LIKE '%' + ISNULL(@JobTitle,'') + '%'
	AND (jo.JobFlag & @Flags <> 0 or @Flags = 0)
	AND ((e.RequestedScreeningAssistance = 1 and @SearchAssistanceRequested = 1) or @SearchAssistanceRequested = 0)
	AND (jo.Compliance & @Compliance <> 0 or @Compliance = 0)
	AND ((jo.SuppressJobOrder = 1 and @SuppressJobOrder = 1) or @SuppressJobOrder = 0)
	AND ((jo.IsSelfService = 1 and @SelfService = 1) or @SelfService = 0)
	AND ((jo.IsStaffAssisted = 1 and @StaffAssisted = 1) or @StaffAssisted = 0)
	AND ((jo.Spidered = 1 and @Spidered = 1) or @Spidered = 0)
	AND (@SearchAccountCreationDate = 0 or (@SearchAccountCreationDate = 1 and e.AccountCreationDate between @DateFrom and @DateTo))

IF(@FilterCounties = 0)
BEGIN 
	UPDATE #JobOrder SET
	FilterCounties = 1
	FROM #JobOrder tjo
	INNER JOIN JobOrder jo ON tjo.Id = jo.Id
	INNER JOIN @Counties c on jo.County LIKE '%' + c.Value + '%'
END

IF (@FilterOffices = 0)
BEGIN 
	UPDATE #JobOrder SET
	FilterOffices = 1
	FROM #JobOrder tjo
	INNER JOIN JobOrder jo ON tjo.Id = jo.Id
	INNER JOIN @Offices o on jo.Office LIKE '%' + o.Value + '%'
END

IF (@FilterZips = 0)
BEGIN 
	UPDATE #JobCounter SET
	FilterZips = 1
	FROM #JobOrder tjo
	INNER JOIN dbo.Employer e ON tjo.EmployerId = e.id
	INNER JOIN #Zips z ON e.ZipCode = z.ZipCode
END

IF (@FilterWOTCInterest = 0)
BEGIN 
	UPDATE #JobOrder SET
	FilterWOTCInterest = 1
	FROM #JobOrder tjo
	INNER JOIN dbo.JobOrder jo ON tjo.Id = jo.Id
	INNER JOIN @WOTCInterest wi	ON jo.WotcInterest LIKE '%' + wi.Value + '%'
END

IF (@FilterONetOccupationGroups = 0)
BEGIN 
	UPDATE #JobOrder SET
	FilterONetOccupationGroups = 1
	FROM #JobOrder tjo
	INNER JOIN dbo.JobOrder jo ON tjo.Id = jo.Id
	INNER JOIN @ONetOccupationGroups oog on jo.OnetOccupationGroup LIKE '%' + oog.Value + '%'
END

IF (@FilterONetDetailedOccupations = 0)
BEGIN 
	UPDATE #JobOrder SET
	FilterONetDetailedOccupations = 1
	FROM #JobOrder tjo
	INNER JOIN dbo.JobOrder jo ON tjo.Id = jo.Id
	INNER JOIN @ONetDetailedOccupations odo ON jo.OnetDetailedOccupation LIKE '%' + odo.Value + '%'
END

IF (@FilterTargetedSectors = 0)
BEGIN 
	UPDATE #JobOrder SET
	FilterTargetedSectors = 1
	FROM #JobOrder tjo
	INNER JOIN dbo.Employer e ON tjo.EmployerId = e.id
	INNER JOIN @TargetedSector ts ON e.TargetSector LIKE '%' + ts.Value + '%' OR ts.Value IS NULL
END;

WITH joborders AS
(
	SELECT
		CASE LOWER(@SortOrder)
			WHEN 'jobtitle_asc' THEN ROW_NUMBER()OVER (ORDER BY jo.JobTitle ASC, jo.Id ASC)
			WHEN 'employer_asc' THEN ROW_NUMBER()OVER (ORDER BY jo.EmployerName ASC, jo.Id ASC)
			WHEN 'city_asc' THEN ROW_NUMBER()OVER (ORDER BY jo.City ASC, jo.Id ASC)
			WHEN 'postingdate_asc' THEN ROW_NUMBER()OVER (ORDER BY jo.PostingDate ASC, jo.Id ASC)
			WHEN 'closingdate_asc' THEN ROW_NUMBER()OVER (ORDER BY jo.ClosingDate ASC, jo.Id ASC)
			WHEN 'openings_asc' THEN ROW_NUMBER()OVER (ORDER BY jo.Openings ASC, jo.Id ASC)
			WHEN 'specialcond_asc' THEN ROW_NUMBER()OVER (ORDER BY jo.SpecialConditions ASC, jo.Id ASC)
			WHEN 'specialcond_desc' THEN ROW_NUMBER()OVER (ORDER BY jo.SpecialConditions DESC, jo.Id ASC)
			WHEN 'openings_desc' THEN ROW_NUMBER()OVER (ORDER BY jo.Openings DESC, jo.Id ASC)
			WHEN 'closingdate_desc' THEN ROW_NUMBER()OVER (ORDER BY jo.ClosingDate DESC, jo.Id ASC)
			WHEN 'postingdate_desc' THEN ROW_NUMBER()OVER (ORDER BY jo.PostingDate DESC, jo.Id ASC)
			WHEN 'city_desc' THEN ROW_NUMBER()OVER (ORDER BY jo.City DESC, jo.Id ASC)
			WHEN 'employer_desc' THEN ROW_NUMBER()OVER (ORDER BY jo.EmployerName DESC, jo.Id ASC)
			WHEN 'jobtitle_desc' THEN ROW_NUMBER()OVER (ORDER BY jo.JobTitle DESC, jo.Id ASC)
			ELSE ROW_NUMBER()OVER (ORDER BY jo.JobTitle ASC, jo.Id ASC)
		END AS pagingId,
		CASE @SortOrder
			WHEN 'jobtitle_asc' THEN ROW_NUMBER()OVER (ORDER BY jo.JobTitle DESC, jo.Id DESC)
			WHEN 'employer_asc' THEN ROW_NUMBER()OVER (ORDER BY jo.EmployerName DESC, jo.Id DESC)
			WHEN 'city_asc' THEN ROW_NUMBER()OVER (ORDER BY jo.City DESC, jo.Id DESC)
			WHEN 'postingdate_asc' THEN ROW_NUMBER()OVER (ORDER BY jo.PostingDate DESC, jo.Id DESC)
			WHEN 'closingdate_asc' THEN ROW_NUMBER()OVER (ORDER BY jo.ClosingDate DESC, jo.Id DESC)
			WHEN 'openings_asc' THEN ROW_NUMBER()OVER (ORDER BY jo.Openings DESC, jo.Id DESC)
			WHEN 'specialcond_asc' THEN ROW_NUMBER()OVER (ORDER BY jo.SpecialConditions DESC, jo.Id DESC)
			WHEN 'specialcond_desc' THEN ROW_NUMBER()OVER (ORDER BY jo.SpecialConditions ASC, jo.Id DESC)
			WHEN 'openings_desc' THEN ROW_NUMBER()OVER (ORDER BY jo.Openings ASC, jo.Id DESC)
			WHEN 'closingdate_desc' THEN ROW_NUMBER()OVER (ORDER BY jo.ClosingDate ASC, jo.Id DESC)
			WHEN 'postingdate_desc' THEN ROW_NUMBER()OVER (ORDER BY jo.PostingDate ASC, jo.Id DESC)
			WHEN 'city_desc' THEN ROW_NUMBER()OVER (ORDER BY jo.City ASC, jo.Id DESC)
			WHEN 'employer_desc' THEN ROW_NUMBER()OVER (ORDER BY jo.EmployerName ASC, jo.Id DESC)
			WHEN 'jobtitle_desc' THEN ROW_NUMBER()OVER (ORDER BY jo.JobTitle ASC, jo.Id DESC)
			ELSE ROW_NUMBER()OVER (ORDER BY jo.JobTitle DESC, jo.Id DESC)
		END AS pagingRevId,
		jo.Id,
		jo.JobTitle, 
		jo.EmployerName, 
		jo.City, 
		jo.PostingDate, 
		jo.ClosingDate, 
		jo.Openings, 
		jo.SpecialConditions
	FROM 
		#JobOrder jo
	WHERE jo.FilterCounties = 1 
		AND	jo.FilterOffices = 1 
		AND jo.FilterZips = 1
		AND jo.FilterWOTCInterest = 1
		AND jo.FilterONetOccupationGroups = 1
		AND jo.FilterONetDetailedOccupations = 1
		AND jo.FilterTargetedSectors = 1
)
SELECT
	jo.Id,
	jo.pagingId,
	jo.pagingRevId,
	jo.JobTitle, 
	jo.EmployerName, 
	jo.City, 
	jo.PostingDate, 
	jo.ClosingDate, 
	jo.Openings, 
	jo.SpecialConditions
FROM
	joborders jo
WHERE
	pagingId between  ((@PageNumber - 1) * @PageSize) + 1 and (@PageNumber * @PageSize)
ORDER BY
	pagingId ASC










GO

