/*
-
- This script should be run on the Focus database to set the default configuration values
-
*/
Print 'Setting configuration values to the default settings'

DECLARE @CodeGroupId bigint
DECLARE @CodeItemId bigint

Print 'Start - Localisation Item - DrivingLicenceClasses.ClassD'

UPDATE [Config.LocalisationItem] SET Value = 'Regular Operator (Class D)' WHERE [Key] = 'DrivingLicenceClasses.ClassD'

Print 'End - Localisation Item - DrivingLicenceClasses.ClassD'

Print 'Start - Code Item - DrivingLicenceClasses.RegularWithProvisions'

SELECT @CodeGroupId = Id FROM [Config.CodeGroup] WHERE [Key] = 'DrivingLicenceClasses'

SELECT @CodeItemId = Id FROM [Config.CodeItem] WHERE [Key] = 'DrivingLicenceClasses.RegularWithProvisions'

IF EXISTS (SELECT 1 FROM [Config.CodeGroupItem] WHERE CodeGroupId = @CodeGroupId AND CodeItemId = @CodeItemId)
BEGIN
	DELETE FROM [Config.CodeGroupItem] WHERE CodeGroupId = @CodeGroupId AND CodeItemId = @CodeItemId
END

Print 'End - Code Item - DrivingLicenceClasses.RegularWithProvisions'

Print 'Start - Code Item - DrivingLicenceClasses.NonCommercialTruck'

SELECT @CodeItemId = Id FROM [Config.CodeItem] WHERE [Key] = 'DrivingLicenceClasses.NonCommercialTruck'

IF EXISTS (SELECT 1 FROM [Config.CodeGroupItem] WHERE CodeGroupId = @CodeGroupId AND CodeItemId = @CodeItemId)
BEGIN
	DELETE FROM [Config.CodeGroupItem] WHERE CodeGroupId = @CodeGroupId AND CodeItemId = @CodeItemId
END

Print 'End - Code Item - DrivingLicenceClasses.NonCommercialTruck'

Print 'Start - Code Item - DrivingLicenceClasses.NonCommercialVehicle'

SELECT @CodeItemId = Id FROM [Config.CodeItem] WHERE [Key] = 'DrivingLicenceClasses.NonCommercialVehicle'

IF EXISTS (SELECT 1 FROM [Config.CodeGroupItem] WHERE CodeGroupId = @CodeGroupId AND CodeItemId = @CodeItemId)
BEGIN
	DELETE FROM [Config.CodeGroupItem] WHERE CodeGroupId = @CodeGroupId AND CodeItemId = @CodeItemId
END

Print 'End - Code Item - DrivingLicenceClasses.NonCommercialVehicle'

Print 'Start - Code Item - DrivingLicenceEndorsements.Limo'

SELECT @CodeGroupId = Id FROM [Config.CodeGroup] WHERE [Key] = 'DrivingLicenceEndorsements'
SELECT @CodeItemId = Id FROM [Config.CodeItem] WHERE [Key] = 'DrivingLicenceEndorsements.Limo'

IF EXISTS (SELECT 1 FROM [Config.CodeGroupItem] WHERE CodeGroupId = @CodeGroupId AND CodeItemId = @CodeItemId)
BEGIN
	DELETE FROM [Config.CodeGroupItem] WHERE CodeGroupId = @CodeGroupId AND CodeItemId = @CodeItemId
END

Print 'End - Code Item - DrivingLicenceEndorsements.Limo'

Print 'Start - Configuration Item - DrivingLicenceEndorsementRules'

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'DrivingLicenceEndorsementRules'

Print 'End - Configuration Item - DrivingLicenceEndorsementRules'

Print 'Start - Configuration Item - Use Custom Footers'

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'UseCustomFooterHtml'

Print 'End - Configuration Item - Use Custom Footers'

Print 'Start - Configuration Item - Workforce Career Custom Footer'

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'CustomFooterHtml-Workforce-CareerExplorer'

Print 'End - Configuration Item - Workforce Career Custom Footer'

Print 'Start - Configuration Item - Workforce Talent Custom Footer'

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'CustomFooterHtml-Workforce-Talent'

Print 'End - Configuration Item - Workforce Talent Custom Footer'

Print 'Start - Configuration Item - Workforce Assist Custom Footer'

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'CustomFooterHtml-Workforce-Assist'

Print 'End - Configuration Item - Workforce Assist Custom Footer'

Print 'Start - Configuration Item - IntegrationClient'

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationClient'

Print 'End - Configuration Item - IntegrationClient'

Print 'Start - Configuration Item - IntegrationSettings'

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationSettings'

Print 'End - Configuration Item - IntegrationSettings'

Print 'Start - Localisation Item - Global.HelpTitle-TermsOfUse.Text'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Global.HelpTitle-TermsOfUse.Text'

Print 'End - Localisation Item - Global.HelpTitle-TermsOfUse.Text' 

Print 'Start - Localisation Item - Global.HelpContent-TermsOfUse.Text'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Global.HelpContent-TermsOfUse.Text'

Print 'End - Localisation Item - Global.HelpContent-TermsOfUse.Text' 

Print 'Start - Localisation Item - Global.HelpTitle-PrivacyAndSecurity.Text'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Global.HelpTitle-PrivacyAndSecurity.Text'

Print 'End - Localisation Item - Global.HelpTitle-PrivacyAndSecurity.Text' 

Print 'Start - Localisation Item - Global.HelpContent-PrivacyAndSecurity.Text'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Global.HelpContent-PrivacyAndSecurity.Text'

Print 'End - Localisation Item - Global.HelpContent-PrivacyAndSecurity.Text' 

Print 'Start - Localisation Item - Global.HelpTitle-Help.Text'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Global.HelpTitle-Help.Text'

Print 'End - Localisation Item - Global.HelpTitle-Help.Text' 

Print 'Start - Localisation Item - Global.HelpContent-Help.Text'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Global.HelpContent-Help.Text'

Print 'End - Localisation Item - Global.HelpContent-Help.Text'

Print 'Start - Localisation Item - Focus.CareerExplorer.WebAuth.Controls.LogInOrRegister.TermsAndConditions.Label'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Focus.CareerExplorer.WebAuth.Controls.LogInOrRegister.TermsAndConditions.Label'

Print 'End - Localisation Item - Focus.CareerExplorer.WebAuth.Controls.LogInOrRegister.TermsAndConditions.Label' 

Print 'Start - Localisation Item - Focus.CareerExplorer.WebAuth.Controls.LogInOrRegister.TermsAndConditions.SearchWithoutSignIn.Label'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Focus.CareerExplorer.WebAuth.Controls.LogInOrRegister.TermsAndConditions.SearchWithoutSignIn.Label'

Print 'End - Localisation Item - Focus.CareerExplorer.WebAuth.Controls.LogInOrRegister.TermsAndConditions.SearchWithoutSignIn.Label' 

Print 'Start - Localisation Item - Focus.CareerExplorer.WebAuth.Controls.LogInOrRegister.Heading2Workforce.Text'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Focus.CareerExplorer.WebAuth.Controls.LogInOrRegister.Heading2Workforce.Text'

Print 'End - Localisation Item - Focus.CareerExplorer.WebAuth.Controls.LogInOrRegister.Heading2Workforce.Text' 

Print 'Start - Localisation Item - Focus.CareerExplorer.WebAuth.Controls.LogInOrRegister.Consent.Label'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Focus.CareerExplorer.WebAuth.Controls.LogInOrRegister.Consent.Label'

Print 'End - Localisation Item - Focus.CareerExplorer.WebAuth.Controls.LogInOrRegister.TermsAndConditions.SearchWithoutSignIn.Label' 

Print 'Start - Localisation Item - Focus.Web.WebAssist.Controls.RegisterJobSeeker.Heading1.Label'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Focus.Web.WebAssist.Controls.RegisterJobSeeker.Heading1.Label'

Print 'End - Localisation Item - Focus.Web.WebAssist.Controls.RegisterJobSeeker.Heading1.Label' 

Print 'Start - Localisation Item - Global.TermsOfUseEducation.Text'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Global.TermsOfUseEducation.Text'

Print 'End - Localisation Item - Global.TermsOfUseEducation.Text'


Print 'Start - Localisation Item - Focus.Web.Code.Controls.User.EmployerRegistrationStep5.TermsOfUseWorkforce.Text'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Focus.Web.Code.Controls.User.EmployerRegistrationStep5.TermsOfUseWorkforce.Text'

Print 'End - Localisation Item - Focus.Web.Code.Controls.User.EmployerRegistrationStep5.TermsOfUseWorkforce.Text' 
	

