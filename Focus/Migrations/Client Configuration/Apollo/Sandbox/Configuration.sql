DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationClient'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('IntegrationClient', '4', 1)
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationSettings'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('IntegrationSettings', '{"LiveInstance":"false", "BaseUrl": "https://developer.aptimus.com/api/", "JobUrlSuffix": "job-service/1/jobs", "Username": "jmacbeth@burning-glass.com", "Password":"01252Apt"}', 1)