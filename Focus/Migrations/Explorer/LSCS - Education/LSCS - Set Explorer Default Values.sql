/*
-
- This script should be run on the Focus database to set the LSCS default values
-
*/
BEGIN TRANSACTION

-- Set default state/area to Texas/Houston Metro Area
UPDATE [Library.StateArea] SET IsDefault = 0

UPDATE
	SA
SET
	IsDefault = 1
FROM
	[Library.State] S
INNER JOIN [Library.StateArea] SA
	ON SA.StateId = S.Id
INNER JOIN [Library.Area] A
	ON A.Id = SA.AreaId
WHERE
	S.Code = 'TX'
	AND A.[Name] = 'Houston Metro Area'
	
COMMIT TRANSACTION