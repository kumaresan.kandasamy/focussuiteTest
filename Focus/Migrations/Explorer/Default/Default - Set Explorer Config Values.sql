/*
-
- This script should be run on the Focus database to set the default client filtering/tiering configuration values
-
*/
Print 'Setting Explorer config to the default settings'

DECLARE @NextId bigint
DECLARE @EndOfBlockNextId bigint
DECLARE @IdsRequired bigint

SET @IdsRequired = 1
-- Add/update ExplorerDegreeFilteringType
IF EXISTS (SELECT 1 FROM ConfigurationItem WHERE [Key] = 'ExplorerDegreeFilteringType')
BEGIN
	UPDATE
		ConfigurationItem
	SET
		VALUE = '1'
	WHERE
		[Key] = 'ExplorerDegreeFilteringType'
END
ELSE
BEGIN
	BEGIN TRANSACTION
	UPDATE KeyTable SET NextId = NextId + @IdsRequired

	SELECT @EndOfBlockNextId = NextId FROM KeyTable

	SELECT @NextId = @EndOfBlockNextId - @IdsRequired
	
	INSERT INTO ConfigurationItem
	(
		Id,
		[Key],
		Value
	)
	VALUES
	(
		@NextId,
		'ExplorerDegreeFilteringType',
		'1'		
	)
		
	COMMIT TRANSACTION
END
-- Add/update ExplorerDegreeClientDataTag
IF EXISTS (SELECT 1 FROM ConfigurationItem WHERE [Key] = 'ExplorerDegreeClientDataTag')
BEGIN
	UPDATE
		ConfigurationItem
	SET
		VALUE = ''
	WHERE
		[Key] = 'ExplorerDegreeClientDataTag'
END
ELSE
BEGIN
	BEGIN TRANSACTION
	UPDATE KeyTable SET NextId = NextId + @IdsRequired

	SELECT @EndOfBlockNextId = NextId FROM KeyTable

	SELECT @NextId = @EndOfBlockNextId - @IdsRequired
	
	INSERT INTO ConfigurationItem
	(
		Id,
		[Key],
		Value
	)
	VALUES
	(
		@NextId,
		'ExplorerDegreeClientDataTag',
		''		
	)
		
	COMMIT TRANSACTION
END

-- Add/update ExplorerUseDegreeTiering
IF EXISTS (SELECT 1 FROM ConfigurationItem WHERE [Key] = 'ExplorerUseDegreeTiering')
BEGIN
	UPDATE
		ConfigurationItem
	SET
		VALUE = 'false'
	WHERE
		[Key] = 'ExplorerUseDegreeTiering'
END
ELSE
BEGIN
	BEGIN TRANSACTION
	UPDATE KeyTable SET NextId = NextId + @IdsRequired

	SELECT @EndOfBlockNextId = NextId FROM KeyTable

	SELECT @NextId = @EndOfBlockNextId - @IdsRequired
	
	INSERT INTO ConfigurationItem
	(
		Id,
		[Key],
		Value
	)
	VALUES
	(
		@NextId,
		'ExplorerUseDegreeTiering',
		'false'		
	)
		
	COMMIT TRANSACTION
END
-- Add/update ExplorerDegreeFilteringType
IF EXISTS (SELECT 1 FROM ConfigurationItem WHERE [Key] = 'ExplorerSectionOrder')
BEGIN
	UPDATE
		ConfigurationItem
	SET
		VALUE = 'Explore|Research|Study'
	WHERE
		[Key] = 'ExplorerSectionOrder'
END
ELSE
BEGIN
	BEGIN TRANSACTION
	UPDATE KeyTable SET NextId = NextId + @IdsRequired

	SELECT @EndOfBlockNextId = NextId FROM KeyTable

	SELECT @NextId = @EndOfBlockNextId - @IdsRequired
	
	INSERT INTO ConfigurationItem
	(
		Id,
		[Key],
		Value
	)
	VALUES
	(
		@NextId,
		'ExplorerSectionOrder',
		'Explore|Research|Study'		
	)
		
	COMMIT TRANSACTION
END

-- Add/update ExplorerUseDegreeTiering
IF EXISTS (SELECT 1 FROM ConfigurationItem WHERE [Key] = 'ExplorerSectionOrder')
BEGIN
	UPDATE
		ConfigurationItem
	SET
		VALUE = 'Explore|Research|Study'
	WHERE
		[Key] = 'ExplorerSectionOrder'
END
ELSE
BEGIN
	BEGIN TRANSACTION
	UPDATE KeyTable SET NextId = NextId + @IdsRequired

	SELECT @EndOfBlockNextId = NextId FROM KeyTable

	SELECT @NextId = @EndOfBlockNextId - @IdsRequired
	
	INSERT INTO ConfigurationItem
	(
		Id,
		[Key],
		Value
	)
	VALUES
	(
		@NextId,
		'ExplorerSectionOrder',
		'Explore|Research|Study'		
	)
		
	COMMIT TRANSACTION
END
