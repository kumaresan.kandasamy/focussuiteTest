
/****** Object:  StoredProcedure [dbo].[Report.JobSeekerActionTotalsForIds]    Script Date: 09/16/2013 10:13:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobSeekerActionTotalsForIds]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.JobSeekerActionTotalsForIds]
GO

/****** Object:  StoredProcedure [dbo].[Report.JobSeekerActionTotalsForIds]    Script Date: 09/16/2013 10:13:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tim Case
-- Create date: 09 September 2013
-- Description:	Get report totals for job seeker actions
-- =============================================
CREATE PROCEDURE [dbo].[Report.JobSeekerActionTotalsForIds]
	@FromDate DATETIME,
	@ToDate DATETIME,
	@JobSeekerIds XML
AS
BEGIN
	SET NOCOUNT ON
	
	CREATE TABLE #JobSeekerIds
	(
		Id BIGINT
	)
	
	INSERT INTO #JobSeekerIds ( Id )
    SELECT t.value('.', 'bigint')
    FROM @JobSeekerIds.nodes('//id') AS x(t) 

	SELECT
		JSA.JobSeekerId AS Id,
		SUM(JSA.Logins) AS Logins,
		SUM(JSA.PostingsViewed) AS PostingsViewed,
		SUM(JSA.ReferralRequests) AS ReferralRequests,
		SUM(JSA.SelfReferrals) AS SelfReferrals,
		SUM(JSA.StaffReferrals) AS StaffReferrals,
		SUM(JSA.NotesAdded) AS NotesAdded,
		SUM(JSA.AddedToLists) AS AddedToLists,
		SUM(JSA.ActivitiesAssigned) AS ActivitiesAssigned,
		SUM(JSA.StaffAssignments) AS StaffAssignments,
		SUM(JSA.EmailsSent) AS EmailsSent,
		SUM(JSA.FollowUpIssues) AS FollowUpIssues,
		SUM(JSA.IssuesResolved) AS IssuesResolved,
		SUM(JSA.Hired) AS Hired,
		SUM(JSA.NotHired) AS NotHired,
		SUM(JSA.FailedToApply) AS FailedToApply,
		SUM(JSA.FailedToReportToInterview) AS FailedToReportToInterview,
		SUM(JSA.InterviewDenied) AS InterviewDenied,
		SUM(JSA.InterviewScheduled) AS InterviewScheduled,
		SUM(JSA.NewApplicant) AS NewApplicant,
		SUM(JSA.Recommended) AS Recommended,
		SUM(JSA.RefusedOffer) AS RefusedOffer,
		SUM(JSA.UnderConsideration) AS UnderConsideration,
		SUM(JSA.UsedOnlineResumeHelp) AS UsedOnlineResumeHelp,
		SUM(JSA.SavedJobAlerts) AS SavedJobAlerts,
		SUM(JSA.TargetingHighGrowthSectors) AS TargetingHighGrowthSectors,
		SUM(JSA.SelfReferralsExternal) AS SelfReferralsExternal,
		SUM(JSA.StaffReferralsExternal) AS StaffReferralsExternal,
		SUM(JSA.ReferralsApproved) AS ReferralsApproved,
		SUM(JSA.FindJobsForSeeker) AS FindJobsForSeeker,
		SUM(JSA.FailedToReportToJob) AS FailedToReportToJob,
		SUM(JSA.FailedToRespondToInvitation) AS FailedToRespondToInvitation,
		SUM(JSA.FoundJobFromOtherSource) AS FoundJobFromOtherSource,
		SUM(JSA.JobAlreadyFilled) AS JobAlreadyFilled,
		SUM(JSA.NotQualified) AS NotQualified,
		SUM(JSA.NotYetPlaced) AS NotYetPlaced,
		SUM(JSA.RefusedReferral) AS RefusedReferral,
		SUM(JSA.SurveyVerySatisfied) AS VerySurveySatisfied,
		SUM(JSA.SurveySatisfied) AS SurveySatisfied,
		SUM(JSA.SurveyDissatisfied) AS SurveyDissatisfied,
		SUM(JSA.SurveyVeryDissatisfied) AS SurveyVeryDissatisfied,
		SUM(JSA.SurveyNoUnexpectedMatches) AS SurveyNoUnexpectedMatches,
		SUM(JSA.SurveyUnexpectedMatches) AS SurveyUnexpectedMatches,
		SUM(JSA.SurveyReceivedInvitations) AS SurveyReceivedInvitations,
		SUM(JSA.SurveyDidNotReceiveInvitations) AS SurveyDidNotReceiveInvitations
	FROM 
		[Report.JobSeekerAction] JSA
	INNER JOIN #JobSeekerIds JSI
		ON JSA.JobSeekerId = JSI.Id
	WHERE 
		JSA.ActionDate BETWEEN @FromDate AND @ToDate
	GROUP BY
		JSA.JobSeekerId
END
GO

GRANT EXEC ON [dbo].[Report.JobSeekerActionTotalsForIds] TO [FocusAgent]
