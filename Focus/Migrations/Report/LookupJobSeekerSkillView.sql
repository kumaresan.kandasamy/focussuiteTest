/****** Object:  View [dbo].[Report.LookupJobSeekerSkillView]    Script Date: 09/12/2013 10:30:57 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Report.LookupJobSeekerSkillView]'))
DROP VIEW [dbo].[Report.LookupJobSeekerSkillView]
GO

/****** Object:  View [dbo].[Report.LookupJobSeekerSkillView]    Script Date: 09/12/2013 10:31:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[Report.LookupJobSeekerSkillView]

AS
	SELECT
		ROW_NUMBER() OVER (ORDER BY @@RowCount) AS Id,
		Data.[Description]
	FROM
	(
		SELECT DISTINCT
			[Description] 
		FROM 
			[Report.JobSeekerData]
		WHERE
			DataType = 1
	) Data

GO


