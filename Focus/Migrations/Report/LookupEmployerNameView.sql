/****** Object:  View [dbo].[Report.LookupEmployerNameView]    Script Date: 09/12/2013 10:25:14 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Report.LookupEmployerNameView]'))
DROP VIEW [dbo].[Report.LookupEmployerNameView]
GO

/****** Object:  View [dbo].[Report.LookupEmployerNameView]    Script Date: 09/12/2013 10:25:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[Report.LookupEmployerNameView]

AS
	SELECT
		ROW_NUMBER() OVER (ORDER BY @@RowCount) AS Id,
		Data.Name
	FROM
	(
		SELECT DISTINCT
			Name
		FROM 
			[Report.Employer]
	) Data

GO


