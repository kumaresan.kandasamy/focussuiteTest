/****** Object:  View [dbo].[Report.LookupJobSeekerMilitaryOccupationTitleView]    Script Date: 09/12/2013 10:28:45 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Report.LookupJobSeekerMilitaryOccupationTitleView]'))
DROP VIEW [dbo].[Report.LookupJobSeekerMilitaryOccupationTitleView]
GO

/****** Object:  View [dbo].[Report.LookupJobSeekerMilitaryOccupationTitleView]    Script Date: 09/12/2013 10:28:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[Report.LookupJobSeekerMilitaryOccupationTitleView]

AS
	SELECT
		ROW_NUMBER() OVER (ORDER BY @@RowCount) AS Id,
		Data.MilitaryOccupationTitle
	FROM
	(
		SELECT DISTINCT
			[Description] AS MilitaryOccupationTitle
		FROM 
			[Report.JobSeekerData]
		WHERE
			DataType = 8
	) Data

GO


