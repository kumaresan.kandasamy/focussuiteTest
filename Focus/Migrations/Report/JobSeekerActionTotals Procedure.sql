
/****** Object:  StoredProcedure [dbo].[Report.JobSeekerActionTotals]    Script Date: 09/09/2013 17:11:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobSeekerActionTotals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.JobSeekerActionTotals]
GO

/****** Object:  StoredProcedure [dbo].[Report.JobSeekerActionTotals]    Script Date: 09/09/2013 17:11:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tim Case
-- Create date: 09 September 2013
-- Description:	Get report totals for job seeker actions
-- =============================================
CREATE PROCEDURE [dbo].[Report.JobSeekerActionTotals]
	@FromDate DATETIME,
	@ToDate DATETIME
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		JobSeekerId AS Id,
		SUM(Logins) AS Logins,
		SUM(PostingsViewed) AS PostingsViewed,
		SUM(ReferralRequests) AS ReferralRequests,
		SUM(SelfReferrals) AS SelfReferrals,
		SUM(StaffReferrals) AS StaffReferrals,
		SUM(NotesAdded) AS NotesAdded,
		SUM(AddedToLists) AS AddedToLists,
		SUM(ActivitiesAssigned) AS ActivitiesAssigned,
		SUM(StaffAssignments) AS StaffAssignments,
		SUM(EmailsSent) AS EmailsSent,
		SUM(FollowUpIssues) AS FollowUpIssues,
		SUM(IssuesResolved) AS IssuesResolved,
		SUM(Hired) AS Hired,
		SUM(NotHired) AS NotHired,
		SUM(FailedToApply) AS FailedToApply,
		SUM(FailedToReportToInterview) AS FailedToReportToInterview,
		SUM(InterviewDenied) AS InterviewDenied,
		SUM(InterviewScheduled) AS InterviewScheduled,
		SUM(NewApplicant) AS NewApplicant,
		SUM(Recommended) AS Recommended,
		SUM(RefusedOffer) AS RefusedOffer,
		SUM(UnderConsideration) AS UnderConsideration,
		SUM(UsedOnlineResumeHelp) AS UsedOnlineResumeHelp,
		SUM(SavedJobAlerts) AS SavedJobAlerts,
		SUM(TargetingHighGrowthSectors) AS TargetingHighGrowthSectors,
		SUM(SelfReferralsExternal) AS SelfReferralsExternal,
		SUM(StaffReferralsExternal) AS StaffReferralsExternal,
		SUM(ReferralsApproved) AS ReferralsApproved,
		SUM(FindJobsForSeeker) AS FindJobsForSeeker,
		SUM(FailedToReportToJob) AS FailedToReportToJob,
		SUM(FailedToRespondToInvitation) AS FailedToRespondToInvitation,
		SUM(FoundJobFromOtherSource) AS FoundJobFromOtherSource,
		SUM(JobAlreadyFilled) AS JobAlreadyFilled,
		SUM(NotQualified) AS NotQualified,
		SUM(NotYetPlaced) AS NotYetPlaced,
		SUM(RefusedReferral) AS RefusedReferral,
		SUM(SurveyVerySatisfied) AS SurveyVerySatisfied,
		SUM(SurveySatisfied) AS SurveySatisfied,
		SUM(SurveyDissatisfied) AS SurveyDissatisfied,
		SUM(SurveyVeryDissatisfied) AS SurveyVeryDissatisfied,
		SUM(SurveyNoUnexpectedMatches) AS SurveyNoUnexpectedMatches,
		SUM(SurveyUnexpectedMatches) AS SurveyUnexpectedMatches,
		SUM(SurveyReceivedInvitations) AS SurveyReceivedInvitations,
		SUM(SurveyDidNotReceiveInvitations) AS SurveyDidNotReceiveInvitations
	FROM 
		[Report.JobSeekerAction]
	WHERE 
		ActionDate BETWEEN @FromDate AND @ToDate
	GROUP BY
		JobSeekerId
END
GO

GRANT EXEC ON [dbo].[Report.JobSeekerActionTotals] TO [FocusAgent]