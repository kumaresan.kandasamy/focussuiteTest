SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobSeeker]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Report.JobSeeker](
	[Id] [bigint] NOT NULL,
	[FocusPersonId] [bigint] NOT NULL DEFAULT(0),
	[FirstName] [nvarchar](100) NOT NULL DEFAULT(''),
	[LastName] [nvarchar](100) NOT NULL DEFAULT(''),
	[CountyId] [bigint] NULL,
	[County] [nvarchar](400) NULL,
	[StateId] [bigint] NULL,
	[State] [nvarchar](400) NULL,
	[PostalCode] [nvarchar](40) NULL,
	[Office] [nvarchar](400) NULL,
	[AccountCreationDate] [datetime] NOT NULL,
	[Salary] [decimal](24, 18) NULL,
	[SalaryFrequency] [int] NULL,
	[EmploymentStatus] [int] NULL,
	[VeteranType] [int] NULL,
	[VeteranDisability] [int] NULL,
	[VeteranTransitionType] [int] NULL,
	[VeteranMilitaryDischarge] [int] NULL,
	[VeteranBranchOfService] [int] NULL,
	[CampaignVeteran] [bit] NULL,
	[DateOfBirth] [datetime] NULL,
	[Gender] [int] NULL,
	[EmailAddress] [nvarchar](500) NULL,
	[DisabilityStatus] [int] NULL,
	[DisabilityType] [int] NULL,
	[EthnicHeritage] [int] NULL,
	[Race] [int] NULL,
	[ResumeSearchable] [bit] NULL,
	[ResumeCount] [int] NOT NULL DEFAULT(0),
	[HasPendingResume] [bit] NOT NULL DEFAULT(0),
	[WillingToWorkOvertime] [bit] NULL,
	[WillingToRelocate] [bit] NULL,
	[MinimumEducationLevel] [int] NULL,
	[LatitudeRadians] [float] NOT NULL DEFAULT(0),
	[LongitudeRadians] [float] NOT NULL DEFAULT(0),
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobSeekerAction]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Report.JobSeekerAction](
	[Id] [bigint] NOT NULL,
	[ActionDate] [datetime] NOT NULL,
	[PostingsViewed] [int] NOT NULL DEFAULT(0),
	[Logins] [int] NOT NULL DEFAULT(0),
	[ReferralRequests] [int] NOT NULL DEFAULT(0),
	[SelfReferrals] [int] NOT NULL DEFAULT(0),
	[StaffReferrals] [int] NOT NULL DEFAULT(0),
	[NotesAdded] [int] NOT NULL DEFAULT(0),
	[AddedToLists] [int] NOT NULL DEFAULT(0),
	[ActivitiesAssigned] [int] NOT NULL DEFAULT(0),
	[StaffAssignments] [int] NOT NULL DEFAULT(0),
	[EmailsSent] [int] NOT NULL DEFAULT(0),
	[FollowUpIssues] [int] NOT NULL DEFAULT(0),
	[IssuesResolved] [int] NOT NULL DEFAULT(0),
	[Hired] [int] NOT NULL DEFAULT(0),
	[NotHired] [int] NOT NULL DEFAULT(0),
	[FailedToApply] [int] NOT NULL DEFAULT(0),
	[FailedToReportToInterview] [int] NOT NULL DEFAULT(0),
	[InterviewDenied] [int] NOT NULL DEFAULT(0),
	[InterviewScheduled] [int] NOT NULL DEFAULT(0),
	[NewApplicant] [int] NOT NULL DEFAULT(0),
	[Recommended] [int] NOT NULL DEFAULT(0),
	[RefusedOffer] [int] NOT NULL DEFAULT(0),
	[UnderConsideration] [int] NOT NULL DEFAULT(0),
	[UsedOnlineResumeHelp] [int] NOT NULL DEFAULT(0),
	[SavedJobAlerts] [int] NOT NULL DEFAULT(0),
	[TargetingHighGrowthSectors] [int] NOT NULL DEFAULT(0),
	[ReferralsApproved] [int] NOT NULL DEFAULT(0),
	[FindJobsForSeeker] [int] NOT NULL DEFAULT(0),
	[FailedToReportToJob] [int] NOT NULL DEFAULT(0),
    [FailedToRespondToInvitation] [int] NOT NULL DEFAULT(0),
    [FoundJobFromOtherSource] [int] NOT NULL DEFAULT(0),
    [JobAlreadyFilled] [int] NOT NULL DEFAULT(0),
    [NotQualified] [int] NOT NULL DEFAULT(0),
    [NotYetPlaced] [int] NOT NULL DEFAULT(0),
    [RefusedReferral] [int] NOT NULL DEFAULT(0),
	[SelfReferralsExternal] [int] NOT NULL DEFAULT(0),
	[StaffReferralsExternal] [int] NOT NULL DEFAULT(0),
	[SurveyVerySatisfied]  [int] NOT NULL DEFAULT(0),
	[SurveySatisfied]  [int] NOT NULL DEFAULT(0),
	[SurveyDissatisfied]  [int] NOT NULL DEFAULT(0),
	[SurveyVeryDissatisfied]  [int] NOT NULL DEFAULT(0),
	[SurveyNoUnexpectedMatches]  [int] NOT NULL DEFAULT(0),
	[SurveyUnexpectedMatches]  [int] NOT NULL DEFAULT(0),
	[SurveyReceivedInvitations]  [int] NOT NULL DEFAULT(0),
	[SurveyDidNotReceiveInvitations]  [int] NOT NULL DEFAULT(0),
	[JobSeekerId] [bigint] NOT NULL DEFAULT(0)
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE referenced_object_id = OBJECT_ID(N'[dbo].[Report.JobSeeker') AND parent_object_id = OBJECT_ID(N'[dbo].[Report.JobSeekerAction]'))
ALTER TABLE [dbo].[Report.JobSeekerAction]  WITH CHECK ADD FOREIGN KEY([JobSeekerId])
REFERENCES [dbo].[Report.JobSeeker] ([Id])
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobSeekerActivityAssignment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Report.JobSeekerActivityAssignment](
	[Id] [bigint] NOT NULL,
	[AssignDate] [datetime] NOT NULL,
	[AssignmentsMade] [int] NOT NULL DEFAULT(0),
	[Activity] [nvarchar](400) NOT NULL DEFAULT (''),
	[ActivityCategory] [nvarchar](400) NOT NULL DEFAULT (''),
	[JobSeekerId] [bigint] NOT NULL DEFAULT(0),
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE referenced_object_id = OBJECT_ID(N'[dbo].[Report.JobSeeker]') AND parent_object_id = OBJECT_ID(N'[dbo].[Report.JobSeekerActivityAssignment]'))
ALTER TABLE [dbo].[Report.JobSeekerActivityAssignment]  WITH CHECK ADD FOREIGN KEY([JobSeekerId])
REFERENCES [dbo].[Report.JobSeeker] ([Id])
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobSeekerData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Report.JobSeekerData](
	[Id] [bigint] NOT NULL,
	[Description] [nvarchar](max) NOT NULL DEFAULT (''),
	[DataType] [int] NOT NULL DEFAULT(0),
	[JobSeekerId] [bigint] NOT NULL DEFAULT(0),
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE referenced_object_id = OBJECT_ID(N'[dbo].[Report.JobSeeker]') AND parent_object_id = OBJECT_ID(N'[dbo].[Report.JobSeekerData]'))
ALTER TABLE [dbo].[Report.JobSeekerData]  WITH CHECK ADD FOREIGN KEY([JobSeekerId])
REFERENCES [dbo].[Report.JobSeeker] ([Id])
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobSeekerOffice]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Report.JobSeekerOffice](
	[Id] [bigint] NOT NULL,
	[OfficeName] [nvarchar](100) NOT NULL,
	[OfficeId] [bigint] NULL,
	[JobSeekerId] [bigint] NOT NULL DEFAULT(0),
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE referenced_object_id = OBJECT_ID(N'[dbo].[Report.JobSeeker]') AND parent_object_id = OBJECT_ID(N'[dbo].[Report.JobSeekerOffice]'))
ALTER TABLE [dbo].[Report.JobSeekerOffice]  WITH CHECK ADD FOREIGN KEY([JobSeekerId])
REFERENCES [dbo].[Report.JobSeeker] ([Id])
GO

