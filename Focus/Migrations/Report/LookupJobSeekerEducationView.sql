/****** Object:  View [dbo].[Report.LookupJobSeekerEducationView]    Script Date: 09/12/2013 10:25:53 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Report.LookupJobSeekerEducationView]'))
DROP VIEW [dbo].[Report.LookupJobSeekerEducationView]
GO

/****** Object:  View [dbo].[Report.LookupJobSeekerEducationView]    Script Date: 09/12/2013 10:25:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[Report.LookupJobSeekerEducationView]

AS
	SELECT
		ROW_NUMBER() OVER (ORDER BY @@RowCount) AS Id,
		Data.[Description]
	FROM
	(
		SELECT DISTINCT
			[Description] 
		FROM 
			[Report.JobSeekerData]
		WHERE
			DataType = 3
	) Data

GO


