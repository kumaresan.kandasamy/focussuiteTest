/****** Object:  View [dbo].[Report.JobOrderView]    Script Date: 09/30/2013 16:06:37 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobOrderView]'))
DROP VIEW [dbo].[Report.JobOrderView]
GO

/****** Object:  View [dbo].[Report.JobOrderView]    Script Date: 09/30/2013 16:06:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Report.JobOrderView]

AS
	SELECT
		J.Id,
		J.FocusJobId,
		J.EmployerId,
		J.JobStatus,
		J.CountyId,
		J.County,
		J.StateId,
		J.[State],
		J.PostalCode,
		J.Office,
		J.MinimumEducationLevel,
		J.PostingFlags,
		J.WorkOpportunitiesTaxCreditHires,
		J.ForeignLabourCertification,
		J.ScreeningPreferences,
		J.MinSalary,
		J.MaxSalary,
		J.SalaryFrequency,
		J.OnetCode,
		J.PostedOn,
		J.ClosingOn,
		J.LatitudeRadians,
		J.LongitudeRadians,
		J.CreatedOn,
		J.UpdatedOn,
		J.JobTitle,
		J.[Description],
		J.ForeignLabourType,
		J.FederalContractor,
		J.CourtOrderedAffirmativeAction,
		E.[Name] AS EmployerName,
		E.FederalEmployerIdentificationNumber
	FROM 
		[Report.JobOrder] J
	INNER JOIN [Report.Employer] E
		ON E.Id = J.EmployerId
GO


