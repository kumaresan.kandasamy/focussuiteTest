/****** Object:  View [dbo].[Data.Application.JobOrderReportView]    Script Date: 09/27/2013 09:47:10 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.JobOrderReportView]'))
DROP VIEW [dbo].[Data.Application.JobOrderReportView]
GO

/****** Object:  View [dbo].[Data.Application.JobOrderReportView]    Script Date: 09/27/2013 09:47:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Data.Application.JobOrderReportView]

AS
	SELECT 
		J.Id,
		J.BusinessUnitId,
		J.OnetId,
		J.JobStatus,
		JA.CountyId,
		JA.StateId,
		JA.PostcodeZip,
		J.JobTitle,
		J.MinimumEducationLevel,
		J.PostingFlags,
		J.WorkOpportunitiesTaxCreditHires,
		J.ForeignLabourCertification,
		J.ForeignLabourCertificationH2A,
		J.ForeignLabourCertificationH2B,
		J.ForeignLabourCertificationOther,
		J.FederalContractor,
		J.CourtOrderedAffirmativeAction,
		J.ScreeningPreferences,
		J.SalaryFrequencyId,
		J.MinSalary,
		J.MaxSalary,
		J.PostedOn,
		J.ClosingOn,
		J.[Description]
	FROM
		[Data.Application.Job] J
	LEFT OUTER JOIN [Data.Application.JobAddress] JA
		ON JA.JobId = J.Id
		AND JA.IsPrimary = 1
GO
