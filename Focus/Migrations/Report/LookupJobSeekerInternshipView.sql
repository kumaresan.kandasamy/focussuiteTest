/****** Object:  View [dbo].[Report.LookupJobSeekerInternshipView]    Script Date: 09/12/2013 10:26:34 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Report.LookupJobSeekerInternshipView]'))
DROP VIEW [dbo].[Report.LookupJobSeekerInternshipView]
GO

/****** Object:  View [dbo].[Report.LookupJobSeekerInternshipView]    Script Date: 09/12/2013 10:26:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[Report.LookupJobSeekerInternshipView]

AS
	SELECT
		ROW_NUMBER() OVER (ORDER BY @@RowCount) AS Id,
		Data.[Description]
	FROM
	(
		SELECT DISTINCT
			[Description] 
		FROM 
			[Report.JobSeekerData]
		WHERE
			DataType = 2
	) Data

GO


