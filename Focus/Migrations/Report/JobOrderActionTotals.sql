
/****** Object:  StoredProcedure [dbo].[Report.JobOrderActionTotals]    Script Date: 09/09/2013 17:11:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobOrderActionTotals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.JobOrderActionTotals]
GO

/****** Object:  StoredProcedure [dbo].[Report.JobOrderActionTotals]    Script Date: 09/09/2013 17:11:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tim Case
-- Create date: 27 September 2013
-- Description:	Get report totals for job Order actions
-- =============================================
CREATE PROCEDURE [dbo].[Report.JobOrderActionTotals]
	@FromDate DATETIME,
	@ToDate DATETIME
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		JobOrderId AS Id,
		SUM(StaffReferrals) AS StaffReferrals,  
		SUM(SelfReferrals) AS SelfReferrals,  
		SUM(ReferralsRequested) AS ReferralsRequested,
		SUM(EmployerInvitationsSent) AS EmployerInvitationsSent,  
		SUM(ApplicantsInterviewed) AS ApplicantsInterviewed,  
		SUM(ApplicantsFailedToShow) AS ApplicantsFailedToShow,  
		SUM(ApplicantsDeniedInterviews) AS ApplicantsDeniedInterviews,  
		SUM(ApplicantsHired) AS ApplicantsHired,  
		SUM(JobOffersRefused) AS JobOffersRefused,  
		SUM(ApplicantsDidNotApply) AS ApplicantsDidNotApply,
		SUM(InvitedJobSeekerViewed) AS InvitedJobSeekerViewed,
		SUM(InvitedJobSeekerClicked) AS InvitedJobSeekerClicked,
		SUM(ApplicantsNotHired) AS ApplicantsNotHired, 
		SUM(ApplicantsNotYetPlaced) AS ApplicantsNotYetPlaced, 
		SUM(FailedToRespondToInvitation) AS FailedToRespondToInvitation, 
		SUM(FailedToReportToJob) AS FailedToReportToJob, 
		SUM(FoundJobFromOtherSource) AS FoundJobFromOtherSource, 
		SUM(JobAlreadyFilled) AS JobAlreadyFilled, 
		SUM(NewApplicant) AS NewApplicant, 
		SUM(NotQualified) AS NotQualified, 
		SUM(ApplicantRecommended) AS ApplicantRecommended, 
		SUM(RefusedReferral) AS RefusedReferral, 
		SUM(UnderConsideration) AS UnderConsideration
	FROM 
		[Report.JobOrderAction]
	WHERE 
		ActionDate BETWEEN @FromDate AND @ToDate
	GROUP BY
		JobOrderId
END
GO

GRANT EXEC ON [dbo].[Report.JobOrderActionTotals] TO [FocusAgent]