/****** Object:  View [dbo].[Data.Application.JobSeekerReportView]    Script Date: 09/27/2013 09:47:10 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.JobSeekerReportView]'))
DROP VIEW [dbo].[Data.Application.JobSeekerReportView]
GO

/****** Object:  View [dbo].[Data.Application.JobSeekerReportView]    Script Date: 09/27/2013 09:47:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Data.Application.JobSeekerReportView]

AS
	SELECT 
		P.Id,
		U.UserType,
		P.FirstName,
		P.LastName,
		P.EmailAddress,
		PA.CountyId,
		PA.StateId,
		PA.PostcodeZip
	FROM
		[Data.Application.Person] P
	INNER JOIN [Data.Application.User] U
		ON U.PersonId = P.Id
	LEFT OUTER JOIN [Data.Application.PersonAddress] PA
		ON PA.PersonId = P.Id
		AND PA.IsPrimary = 1
GO
