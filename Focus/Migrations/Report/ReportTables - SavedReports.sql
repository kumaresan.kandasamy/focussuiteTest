SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.SavedReport]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Report.SavedReport](
	[Id] [bigint] NOT NULL,
	[ReportType] [int] NOT NULL,
	[Criteria] [nvarchar](max) NULL,
	[UserId] [bigint] NULL,
	[Name] [nvarchar](200) NULL,
	[ReportDate] [datetime] NOT NULL,
	[DisplayOnDashboard] [bit] NOT NULL,
	[IsSessionReport] [bit] NOT NULL DEFAULT(0),
	[ReportDisplayType] [int] NULL DEFAULT(0),
 CONSTRAINT [PK__Report.SavedReport] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
