
/****** Object:  View [dbo].[CandidateNoteView]    Script Date: 01/25/2013 12:03:10 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CandidateNoteView]'))
DROP VIEW [dbo].[CandidateNoteView]
GO


/****** Object:  View [dbo].[CandidateNoteView]    Script Date: 01/25/2013 12:03:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[CandidateNoteView]
AS

SELECT   
 CandidateNote.Id AS Id,  
 CandidateNote.Note AS Note,  
 CandidateNote.EmployerId AS EmployerId,  
 Candidate.Id AS CandidateId,  
 CandidateNote.DateAdded AS NoteAddedDateTime,
 Employer.Name AS EmployerName  
FROM   
 CandidateNote WITH (NOLOCK)  
 INNER JOIN Candidate WITH (NOLOCK) ON CandidateNote.CandidateId = Candidate.Id  
 INNER JOIN Employer WITH (NOLOCK) ON CandidateNote.EmployerID = Employer.ID





GO


