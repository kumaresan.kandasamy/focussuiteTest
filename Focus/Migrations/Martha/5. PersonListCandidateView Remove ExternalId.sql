

/****** Object:  View [dbo].[PersonListCandidateView]    Script Date: 01/25/2013 11:59:27 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[PersonListCandidateView]'))
DROP VIEW [dbo].[PersonListCandidateView]
GO


/****** Object:  View [dbo].[PersonListCandidateView]    Script Date: 01/25/2013 11:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[PersonListCandidateView]
AS

SELECT 
	PersonListCandidate.Id AS Id,
	PersonList.ListType AS ListType,
	PersonList.PersonId AS PersonId,
	Candidate.Id AS CandidateId,
	Candidate.FirstName AS CandidateFirstName,
	Candidate.LastName AS CandidateLastName,
	ISNULL(Candidate.IsVeteran, 'false') AS CandidateIsVeteran
FROM 
	PersonListCandidate WITH (NOLOCK)
	INNER JOIN PersonList WITH (NOLOCK) ON PersonListCandidate.PersonListId = PersonList.Id
	INNER JOIN Candidate WITH (NOLOCK) ON PersonListCandidate.CandidateId = Candidate.Id




GO


