DECLARE @NextId BIGINT
DECLARE @IdsRequired BIGINT
DECLARE @Item TABLE (ItemKey NVARCHAR(200), ItemValue NVARCHAR(500))

-- New configuration items
INSERT INTO @Item(ItemKey, ItemValue)
VALUES 
	('DisableAssistAuthentication', 'False'), 
	('DisableTalentAuthentication', 'False')

BEGIN TRANSACTION

-- Remove any items from the temporary table that already exist in the Source database
DELETE I
FROM @Item I
INNER JOIN dbo.ConfigurationItem CI
	ON CI.[Key] = I.ItemKey

-- Get the number of Ids required
SELECT @IdsRequired = COUNT(1) FROM @Item

-- Update the key table, also noting the current next ID
UPDATE 
	dbo.KeyTable 
SET 
	@NextId = NextId,
	NextId = NextId + @IdsRequired 

-- Insert the new items
INSERT INTO dbo.ConfigurationItem (Id, [Key], Value)
SELECT @NextId - 1 + ROW_NUMBER() OVER (ORDER BY ItemKey DESC), ItemKey, ItemValue
FROM @Item

COMMIT TRANSACTION