/****** Object:  View [dbo].[Data.Application.StudentAlumniIssueView]    Script Date: 08/23/2013 10:24:29 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.StudentAlumniIssueView]'))
DROP VIEW [dbo].[Data.Application.StudentAlumniIssueView]
GO

/****** Object:  View [dbo].[Data.Application.StudentAlumniIssueView]    Script Date: 08/23/2013 10:24:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[Data.Application.StudentAlumniIssueView]  
AS  
SELECT 
	p.Id,
	p.FirstName, 
	p.LastName,
	p.DateOfBirth, 
	p.SocialSecurityNumber, 			 
	p.EmailAddress, 
	u.LastLoggedInOn,
	i.NoLoginTriggered,  
    i.JobOfferRejectionTriggered,  
    i.NotReportingToInterviewTriggered,  
    i.NotClickingOnLeadsTriggered,  
    i.NotRespondingToEmployerInvitesTriggered,  
    i.ShowingLowQualityMatchesTriggered,  
    i.PostingLowQualityResumeTriggered,  
    i.PostHireFollowUpTriggered,  
    i.FollowUpRequested,  
    i.NotSearchingJobsTriggered,
    p.EnrollmentStatus,
    p.ProgramAreaId,
    p.DegreeId,
    u.UserType,
    u.ExternalId
FROM  
	dbo.[Data.Application.User] AS u WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Application.Person] AS p WITH (NOLOCK) ON u.PersonId = p.Id 
	LEFT JOIN dbo.[Data.Application.Issues] i on p.Id = i.PersonId         



GO


