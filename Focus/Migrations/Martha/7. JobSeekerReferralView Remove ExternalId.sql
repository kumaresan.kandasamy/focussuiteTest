
/****** Object:  View [dbo].[JobSeekerReferralView]    Script Date: 01/25/2013 12:07:18 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[JobSeekerReferralView]'))
DROP VIEW [dbo].[JobSeekerReferralView]
GO


/****** Object:  View [dbo].[JobSeekerReferralView]    Script Date: 01/25/2013 12:07:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[JobSeekerReferralView]
AS
SELECT 
	dbo.CandidateApplication.Id, 
	dbo.Candidate.FirstName + ' ' + dbo.Candidate.LastName AS Name, 
	dbo.CandidateApplication.CreatedOn AS ReferralDate, 
    dbo.GetBusinessDays(dbo.CandidateApplication.CreatedOn, GETDATE()) AS TimeInQueue, 
    dbo.Job.JobTitle, 
    dbo.BusinessUnit.Name AS EmployerName, 
    dbo.Job.EmployerId AS EmployerId, 
    dbo.Job.Posting, dbo.Candidate.Id AS CandidateId, 
    dbo.Job.Id AS JobId, 
    dbo.CandidateApplication.ApprovalRequiredReason, 
    CASE WHEN dbo.Candidate.IsVeteran = 1 THEN 'Yes' WHEN dbo.Candidate.IsVeteran = 0 THEN 'No' ELSE '' END AS Veteran, 
    dbo.BusinessUnitAddress.TownCity AS Town, 
    dbo.LookupItemsView.Value AS State
FROM  dbo.Candidate WITH (NOLOCK) 
	INNER JOIN dbo.CandidateApplication WITH (NOLOCK) ON dbo.Candidate.Id = dbo.CandidateApplication.CandidateId 
	INNER JOIN dbo.Job WITH (NOLOCK) ON dbo.CandidateApplication.JobId = dbo.Job.Id 
	INNER JOIN dbo.BusinessUnit WITH (NOLOCK) ON dbo.Job.BusinessUnitId = dbo.BusinessUnit.Id 
	INNER JOIN dbo.BusinessUnitAddress ON dbo.BusinessUnit.Id = dbo.BusinessUnitAddress.BusinessUnitId AND BusinessUnitAddress.IsPrimary = 1
	INNER JOIN dbo.LookupItemsView ON dbo.BusinessUnitAddress.StateId = dbo.LookupItemsView.Id
WHERE 
	(dbo.CandidateApplication.ApplicationStatus = 1 AND dbo.CandidateApplication.ApprovalStatus = 1)



GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -373
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Candidate"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 285
               Right = 237
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CandidateApplication"
            Begin Extent = 
               Top = 11
               Left = 298
               Bottom = 152
               Right = 536
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Job"
            Begin Extent = 
               Top = 81
               Left = 710
               Bottom = 222
               Right = 1006
            End
            DisplayFlags = 280
            TopColumn = 11
         End
         Begin Table = "Employer"
            Begin Extent = 
               Top = 448
               Left = 48
               Bottom = 589
               Right = 395
            End
            DisplayFlags = 280
            TopColumn = 18
         End
         Begin Table = "EmployerAddress"
            Begin Extent = 
               Top = 480
               Left = 598
               Bottom = 621
               Right = 825
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "LookupItemsView"
            Begin Extent = 
               Top = 321
               Left = 981
               Bottom = 462
               Right = 1165
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
   ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'JobSeekerReferralView'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'      Column = 1440
         Alias = 900
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'JobSeekerReferralView'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'JobSeekerReferralView'
GO


