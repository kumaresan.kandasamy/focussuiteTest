﻿BEGIN TRAN 
  
-- DECLARE Variables to track counts and ids
DECLARE @StartId BIGINT
DECLARE @NumberRowsToInsert BIGINT

SET @NumberRowsToInsert = 1
  
-- Update the Next ID to be >= the number of rows we expect
UPDATE KeyTable SET NextId = NextId + @NumberRowsToInsert
SELECT @StartId = NextId FROM KeyTable

-- Create a new CareerRegistrationNotificationEmails ConfigurationItem  
INSERT INTO ConfigurationItem
(Id, [Key], Value)
VALUES
(@StartId, 'CareerRegistrationNotificationEmails', 'jmacbeth@burning-glass.com;tread@burning-glass.com;ajones@burning-glass.com;amartins@burning-glass.com;lschofield@burning-glass.com')

COMMIT