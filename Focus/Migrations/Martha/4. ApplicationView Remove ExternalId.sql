/****** Object:  View [dbo].[ApplicationView]    Script Date: 01/25/2013 11:47:46 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ApplicationView]'))
DROP VIEW [dbo].[ApplicationView]
GO

/****** Object:  View [dbo].[ApplicationView]    Script Date: 01/25/2013 11:47:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[ApplicationView]
AS

SELECT
	CandidateApplication.Id AS Id,
	Job.Id AS JobId,
	Job.EmployerId AS EmployerId,
	Candidate.Id AS CandidateId,
	CandidateApplication.ApplicationStatus AS CandidateApplicationStatus,
	CandidateApplication.ApplicationScore AS CandidateApplicationScore,
	CandidateApplication.CreatedOn AS CandidateApplicationReceivedOn,
	CandidateApplication.ApprovalStatus AS CandidateApplicationApprovalStatus,
	Candidate.FirstName AS CandidateFirstName,
	Candidate.LastName AS CandidateLastName,
	Candidate.YearsExperience AS CandidateYearsExperience,
	Candidate.IsVeteran AS CandidateIsVeteran
FROM CandidateApplication WITH (NOLOCK)
	INNER JOIN Job WITH (NOLOCK) ON CandidateApplication.JobId = Job.Id
	INNER JOIN Candidate WITH (NOLOCK) ON CandidateApplication.CandidateId = Candidate.Id


GO


