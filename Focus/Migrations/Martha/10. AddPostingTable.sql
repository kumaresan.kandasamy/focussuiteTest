USE [FocusMartha]
GO

/****** Object:  Table [dbo].[Posting]    Script Date: 01/30/2013 15:32:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Posting](
	[Id] [bigint] NOT NULL,
	[LensPostingId] [nvarchar](150) NOT NULL,
	[JobTitle] [nvarchar](255) NULL,
	[PostingXml] [nvarchar](max) NULL,
	[EmployerName] [nvarchar](255) NULL,
	[JobReference] [nvarchar](12) NULL,
	[Url] [nvarchar](255) NULL,
	[ExternalId] [nvarchar](36) NULL,
	[StatusId] [int] NOT NULL,
	[OriginId] [bigint] NULL,
	[JobId] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Posting]  WITH CHECK ADD FOREIGN KEY([JobId])
REFERENCES [dbo].[Job] ([Id])
GO

ALTER TABLE [dbo].[Posting] ADD  DEFAULT ('') FOR [LensPostingId]
GO

ALTER TABLE [dbo].[Posting] ADD  DEFAULT ((0)) FOR [StatusId]
GO


