IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 24)
BEGIN
	BEGIN TRANSACTION

	INSERT INTO [Config.EmailTemplate] 
	(
		EmailTemplateType, 
		[Subject], 
		Body, 
		Salutation, 
		Recipient
	)
	VALUES 
	(
		24, 
		'Inactivity warning for #FOCUSCAREER#', 
		'This email is being sent to you because you have not logged on to #FOCUSCAREER# for over 60 days.

Please log onto #FOCUSCAREER# within the next 14 days to be able to continue using the system.

#FOCUSCAREERURL#
', 
		'Dear #RECIPIENTNAME#', 
		NULL
	)

	COMMIT TRANSACTION
END

IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 25)
BEGIN
	BEGIN TRANSACTION

	INSERT INTO [Config.EmailTemplate] 
	(
		EmailTemplateType, 
		[Subject], 
		Body, 
		Salutation, 
		Recipient
	)
	VALUES 
	(
		25, 
		'Resume received for similar employee', 
		'We recently received a resume for #MATCHEDCANDIDATES#, who is very similar to your current employee #EMPLOYEENAME#. A copy of #MATCHEDCANDIDATES#''s resume is attached to this email for your reference.

Regards,

#SENDERNAME#', 
		'Dear #RECIPIENTNAME#', 
		NULL
	)

	COMMIT TRANSACTION
END