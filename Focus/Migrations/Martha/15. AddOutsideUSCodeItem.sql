BEGIN TRAN 

-- DECLARE Variables to track counts and ids
DECLARE @StartId BIGINT
DECLARE @NumberRowsToInsert BIGINT
DECLARE @MaxNextId BIGINT
DECLARE @CodeItemCount INT

-- Declare table variables to capture the data we want to push to the db
DECLARE @Item TABLE (GroupKey NVARCHAR(200), ItemKey NVARCHAR(200), Value NVARCHAR(MAX), ParentKey NVARCHAR(200), ExternalId NVARCHAR(36), DisplayOrder int)


-- Populate @Items with Code Items
INSERT INTO @Item VALUES
--Counties
('Counties', 'County.OutsideUS', 'Outside U.S.', null, '0', 999)

-- Determine the number of rows to create
SELECT @CodeItemCount = COUNT(*)  FROM @Item 
-- 1 set for CodeGroup then a set for the LocalisationItem
-- 1 set for CodeItem, CodeGroupItem & LocalisationItem
SET @NumberRowsToInsert = (@CodeItemCount * 3)


-- Update the Next ID to be >= the number of rows we expect
UPDATE KeyTable SET NextId = NextId + @NumberRowsToInsert
SELECT @MaxNextId = NextId FROM KeyTable
SET @StartId = @MaxNextId - @NumberRowsToInsert

-- Create the CodeItems from @Item
INSERT INTO CodeItem (Id, [Key], IsSystem, ParentKey, ExternalId)
SELECT @StartId + ROW_NUMBER() OVER (ORDER BY ItemKey DESC), ItemKey, 1, ParentKey, ExternalId FROM (SELECT DISTINCT ItemKey, ParentKey, ExternalId FROM @Item) AS Tmp
SET @StartId = @StartId + @CodeItemCount

-- Create the CodeItems LocalisationItems from @Item
INSERT INTO LocalisationItem (Id, [Key], Value, LocalisationId, ContextKey, Localised)
SELECT @StartId + ROW_NUMBER() OVER (ORDER BY ItemKey DESC), ItemKey, Value, 12090, '', 0 FROM @Item
SET @StartId = @StartId + @CodeItemCount

-- Create the link between CodeGroup & CodeItem from the @Item table
INSERT INTO CodeGroupItem (Id, DisplayOrder, CodeGroupId, CodeItemId)
SELECT  @StartId + ROW_NUMBER() OVER (ORDER BY ItemKey DESC), DisplayOrder, cg.id, ci.id
FROM @Item i
INNER JOIN CodeGroup cg ON i.GroupKey = cg.[Key] 
INNER JOIN CodeItem ci ON i.ItemKey = ci.[Key]
ORDER BY i.GroupKey, i.ItemKey

COMMIT
