
/****** Object:  View [dbo].[Library.OnetToROnetConversionView]    Script Date: 01/22/2014 18:28:24 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Library.OnetToROnetConversionView]'))
DROP VIEW [dbo].[Library.OnetToROnetConversionView]
GO

/****** Object:  View [dbo].[Library.OnetToROnetConversionView]    Script Date: 01/22/2014 18:28:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Library.OnetToROnetConversionView]
AS
SELECT
	oro.Id,
	oro.OnetId,
	o.OnetCode,
	o.[Key] As OnetKey,
	oro.ROnetId,
	r.Code AS ROnetCode,
	r.NumericCode AS ROnetNumericCode,
	r.[Key] AS ROnetKey,
	oro.BestFit
FROM 
	[Library.Onet] o
INNER JOIN [Library.OnetROnet] oro on o.Id = oro.OnetId
INNER JOIN [Library.ROnet] r ON r.Id = oro.ROnetId
GO


