IF NOT EXISTS
(
	SELECT 1
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_NAME = 'Config.EmailTemplate'
	AND (COLUMN_NAME = 'SenderEmailType' OR COLUMN_NAME = 'ClientSpecificEmailAddress')
)
BEGIN
	ALTER TABLE [Config.EmailTemplate] ADD [SenderEmailType] INT NOT NULL DEFAULT 0;
	ALTER TABLE [Config.EmailTemplate] ADD [ClientSpecificEmailAddress] NVARCHAR(200) NULL;
END