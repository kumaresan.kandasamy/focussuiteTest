
/****** Object:  View [dbo].[Data.Application.BusinessUnitActivityView]    Script Date: 02/25/2014 13:25:32 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.BusinessUnitActivityView]'))
DROP VIEW [dbo].[Data.Application.BusinessUnitActivityView]
GO


/****** Object:  View [dbo].[Data.Application.BusinessUnitActivityView]    Script Date: 02/25/2014 13:25:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[Data.Application.BusinessUnitActivityView]
AS
SELECT		ActionEvent.Id AS Id,
			BusinessUnit.EmployerId, 
			BusinessUnit.Id AS BUId, 
			[User].UserName, 
			BusinessUnit.Name AS BUName, 
			ActionType.Name AS ActionName, 
            ActionEvent.ActionedOn, 
            ActionEvent.Id AS ActionId, 
            Job.Id as JobId, 
            Job.JobTitle,
            BusinessUnit.Id as BusinessUnitId
                      
FROM        dbo.[Data.Core.ActionEvent] AS ActionEvent WITH (NOLOCK) 
			INNER JOIN dbo.[Data.Core.ActionType] AS ActionType WITH (NOLOCK) ON ActionEvent.ActionTypeId = ActionType.Id
			INNER JOIN dbo.[Data.Application.Job] AS Job WITH (NOLOCK) ON Job.Id = ActionEvent.EntityId 
			INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON [User].Id = ActionEvent.UserId
			INNER JOIN dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON BusinessUnit.Id = Job.BusinessUnitId												    
                      
WHERE     (ActionType.Name = 'PostJob') OR
                      (ActionType.Name = 'SaveJob') OR
                      (ActionType.Name = 'HoldJob') OR
                      (ActionType.Name = 'CloseJob') OR
                      (ActionType.Name = 'RefreshJob') OR
                      (ActionType.Name = 'ReactivateJob') OR
                      (ActionType.Name = 'CreateJob')

UNION

SELECT		ActionEvent.Id AS Id,
			BusinessUnit.EmployerId, 
			BusinessUnit.Id AS BUId, 
			[User].UserName, 
			BusinessUnit.Name AS BUName, 
			ActionType.Name AS ActionName, 
            ActionEvent.ActionedOn, 
            ActionEvent.Id AS ActionId, 
            0 as JobId, 
            '' as JobTitle,
            BusinessUnit.Id as BusinessUnitId
                      
FROM        dbo.[Data.Core.ActionEvent] AS ActionEvent WITH (NOLOCK) 
			INNER JOIN dbo.[Data.Core.ActionType] AS ActionType WITH (NOLOCK) ON ActionEvent.ActionTypeId = ActionType.Id
			INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON [User].Id = ActionEvent.UserId
			INNER JOIN dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON BusinessUnit.Id = ActionEvent.EntityId 											    
                      
WHERE       (ActionType.Name = 'UnblockAllEmployerEmployeesAccount') OR
            (ActionType.Name = 'BlockAllEmployerEmployeesAccount')








GO


