 -- Script to add the GivingPositiveFeedback and GivingNegativeFeedback columns to the Issues table and associated views.

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Issues' AND COLUMN_NAME = 'GivingPositiveFeedback')
BEGIN
	ALTER TABLE [dbo].[Data.Application.Issues]
	ADD [GivingPositiveFeedback] BIT DEFAULT 0
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Issues' AND COLUMN_NAME = 'GivingNegativeFeedback')
BEGIN
	ALTER TABLE [dbo].[Data.Application.Issues]
	ADD [GivingNegativeFeedback] BIT DEFAULT 0
END
GO

/** N.B. Views updated in separate files */