/* Script to add a filter to only include active jobs in the ExpiringJobView */

/****** Object:  View [dbo].[Data.Application.ExpiringJobView]    Script Date: 03/20/2014 10:27:37 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.ExpiringJobView]'))
DROP VIEW [dbo].[Data.Application.ExpiringJobView]
GO

/****** Object:  View [dbo].[Data.Application.ExpiringJobView]    Script Date: 03/20/2014 10:27:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Data.Application.ExpiringJobView]
AS
SELECT  Job.Id, 
		Job.JobTitle, 
		Job.ClosingOn, 
		DATEDIFF(day, GETDATE(), Job.ClosingOn) AS DaysToClosing, 
		Job.EmployerId, 
		Job.EmployeeId, 
        Job.PostedOn, 
        [User].Id AS UserID, 
        Person.FirstName, 
        Job.BusinessUnitId, 
        bu.Name AS BusinessUnitName
FROM    dbo.[Data.Application.Job] AS Job WITH (NOLOCK) 
		INNER JOIN dbo.[Data.Application.Employee] AS Employee WITH (NOLOCK) ON Job.EmployeeId = Employee.Id 
		INNER JOIN dbo.[Data.Application.Person] AS Person WITH (NOLOCK) ON Employee.PersonId = Person.Id 
		INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON Person.Id = [User].PersonId 
		LEFT OUTER JOIN dbo.[Data.Application.BusinessUnit] AS bu WITH (NOLOCK) ON bu.Id = Job.BusinessUnitId
WHERE
		Job.JobStatus = 1

GO
