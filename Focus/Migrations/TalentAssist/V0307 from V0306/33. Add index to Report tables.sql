IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobSeeker_FocusPersonId' AND object_id = OBJECT_ID('[Report.JobSeeker]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobSeeker_FocusPersonId]
		ON [Report.JobSeeker] (FocusPersonId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobSeekerAction_JobSeekerId' AND object_id = OBJECT_ID('[Report.JobSeekerAction]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobSeekerAction_JobSeekerId]
		ON [Report.JobSeekerAction] (JobSeekerId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobSeekerActivityAssignment_JobSeekerId' AND object_id = OBJECT_ID('[Report.JobSeekerActivityAssignment]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobSeekerActivityAssignment_JobSeekerId]
		ON [Report.JobSeekerActivityAssignment] (JobSeekerId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobSeekerData_JobSeekerId' AND object_id = OBJECT_ID('[Report.JobSeekerData]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobSeekerData_JobSeekerId]
		ON [Report.JobSeekerData] (JobSeekerId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobSeekerOffice_JobSeekerId' AND object_id = OBJECT_ID('[Report.JobSeekerOffice]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobSeekerOffice_JobSeekerId]
		ON [Report.JobSeekerOffice] (JobSeekerId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobSeekerJobHistory_JobSeekerId' AND object_id = OBJECT_ID('[Report.JobSeekerJobHistory]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobSeekerJobHistory_JobSeekerId]
		ON [Report.JobSeekerJobHistory] (JobSeekerId)
END