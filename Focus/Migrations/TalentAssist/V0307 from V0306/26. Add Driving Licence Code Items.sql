IF NOT EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DrivingLicenceClasses.RegularWithProvisions')
BEGIN
	INSERT INTO [Config.CodeItem]
	(
		[Key],
		IsSystem,
		ExternalId
	)
	VALUES
	(
		'DrivingLicenceClasses.RegularWithProvisions',
		0,
		'7'		
	)

	INSERT INTO [Config.LocalisationItem]
	(
		ContextKey,
		[Key],
		Value,
		Localised,
		LocalisationId
	)
	VALUES
	(
		'',
		'DrivingLicenceClasses.RegularWithProvisions',
		'Regular with provisions',
		0,
		12090		
	)
END

IF NOT EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DrivingLicenceClasses.NonCommercialTruck')
BEGIN
	INSERT INTO [Config.CodeItem]
	(
		[Key],
		IsSystem,
		ExternalId
	)
	VALUES
	(
		'DrivingLicenceClasses.NonCommercialTruck',
		0,
		'8'		
	)

	INSERT INTO [Config.LocalisationItem]
	(
		ContextKey,
		[Key],
		Value,
		Localised,
		LocalisationId
	)
	VALUES
	(
		'',
		'DrivingLicenceClasses.NonCommercialTruck',
		'Non-Commercial Truck',
		0,
		12090		
	)
END

IF NOT EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DrivingLicenceClasses.NonCommercialVehicle')
BEGIN
	INSERT INTO [Config.CodeItem]
	(
		[Key],
		IsSystem,
		ExternalId
	)
	VALUES
	(
		'DrivingLicenceClasses.NonCommercialVehicle',
		0,
		'9'		
	)

	INSERT INTO [Config.LocalisationItem]
	(
		ContextKey,
		[Key],
		Value,
		Localised,
		LocalisationId
	)
	VALUES
	(
		'',
		'DrivingLicenceClasses.NonCommercialVehicle',
		'Non-Commercial Vehicle',
		0,
		12090		
	)
END

IF NOT EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DrivingLicenceEndorsements.Limo')
BEGIN
	INSERT INTO [Config.CodeItem]
	(
		[Key],
		IsSystem,
		ExternalId
	)
	VALUES
	(
		'DrivingLicenceEndorsements.Limo',
		0,
		'drv_limo_flag'		
	)

	INSERT INTO [Config.LocalisationItem]
	(
		ContextKey,
		[Key],
		Value,
		Localised,
		LocalisationId
	)
	VALUES
	(
		'',
		'DrivingLicenceEndorsements.Limo',
		'Limo/Chauffeur',
		0,
		12090		
	)
END

