IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 32)
BEGIN
	INSERT INTO [Config.EmailTemplate]
	(
		EmailTemplateType, 
		[Subject], 
		Body, 
		Salutation, 
		Recipient
	)
	VALUES 
	(
		32, 
		'Message from #FOCUSTALENT#; Job Order Management, ID;#JOBID# will expire in #DAYSLEFTTOJOBEXPIRY# days.', 
		'To extend the closing date for your posting, please log in to your #FOCUSTALENT# account and run the Refresh action against this job order.
		
If you prefer to allow this posting to expire, we will move it to your Closed jobs tab. From there, you will have up to 30 days to refresh the posting before it is officially closed.

Thank you for choosing #FOCUSTALENT# to advertise your position.', 
		'Dear #RECIPIENTNAME#', 
		NULL
	)
END