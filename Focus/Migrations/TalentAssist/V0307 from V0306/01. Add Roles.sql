IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AssistOfficesCreateDelete')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AssistOfficesCreateDelete', 'Assist Create And Delete Offices')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AssistOfficesListStaff')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AssistOfficesListStaff', 'Assist Offices List Staff')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AssistOfficesSetDefault')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AssistOfficesSetDefault', 'Assist Set Default Office')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AssistOfficesViewEdit')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AssistOfficesViewEdit', 'Assist View And Edit Offices')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AssistJobSeekerReports')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AssistJobSeekerReports', 'Assist Job Seeker Reports')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AssistJobOrderReports')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AssistJobOrderReports', 'Assist Job Order Reports')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AssistEmployerReports')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AssistEmployerReports', 'Assist Employer/Hiring Manager Reports')
END