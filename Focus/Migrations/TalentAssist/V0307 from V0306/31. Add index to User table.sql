IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Application.User_Person' AND object_id = OBJECT_ID('[Data.Application.User]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Application.User_Person]
		ON [Data.Application.User] (PersonId)
END
