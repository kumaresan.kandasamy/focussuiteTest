ALTER PROCEDURE [dbo].[Data_Application_GetPostingPersonMatches] 
	@PostingId	bigint,
	@UserId		bigint,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @FirstRec int
	DECLARE @LastRec int
	
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)
	
	DECLARE @PersonMatches AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		Id bigint,
		Score int,
		PersonId bigint,
		FirstName nvarchar(50),
		LastName nvarchar (50)
	)
	
	INSERT INTO @PersonMatches
	SELECT
		ppm.Id,
		ppm.Score,
		ppm.PersonId,
		p.FirstName,
		p.LastName
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		INNER JOIN [Data.Application.Person] p WITH (NOLOCK) ON ppm.PersonId = p.Id
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppm.PostingId = @PostingId AND ppmti.UserId is null
	ORDER BY 
		ppm.Score DESC, p.FirstName, p.LastName
	
	SELECT @RowCount = @@ROWCOUNT
	
	-- Delete Matches not in paged data
	DELETE @PersonMatches
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)
	
	SELECT 
		Id,
		Score,
		PersonId,
		FirstName,
		LastName
	FROM
		@PersonMatches
			
END