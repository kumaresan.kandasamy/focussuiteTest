/****** Object:  View [dbo].[Library.DegreeEducationLevelLensMapping]    Script Date: 11/18/2013 12:24:28 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Library.DegreeEducationLevelLensMapping]'))
DROP VIEW [dbo].[Library.DegreeEducationLevelLensMapping]
GO

/****** Object:  View [dbo].[Library.DegreeEducationLevelLensMapping]    Script Date: 11/18/2013 12:24:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Library.DegreeEducationLevelLensMapping]
AS

SELECT   
	dm.Id,  
	dm.LensId,  
	d.RcipCode,  
	del.Id AS DegreeEducationLevelId,  
	pad.ProgramAreaId AS ProgramAreaId  
FROM 
	[Library.DegreeEducationLevelMapping] dm WITH(NOLOCK)  
INNER JOIN [Library.DegreeEducationLevel] del WITH(NOLOCK) 
	ON del.Id = dm.DegreeEducationLevelId  
INNER JOIN [Library.Degree] d WITH(NOLOCK)
	ON d.Id = del.DegreeId
LEFT OUTER JOIN [Library.ProgramAreaDegree] AS pad WITH(NOLOCK) 
	ON del.DegreeId = pad.DegreeId  

GO


