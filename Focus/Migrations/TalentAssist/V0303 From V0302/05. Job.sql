-- This file contains a log of the SQL statements that LightSpeed ran
-- against your database.  It is specific to the current state of the
-- LightSpeed model and the previous state of your database.  Review the
-- script carefully if you plan to apply it to another database.  Please
-- note that due to scoping issues you may not be able to run all logged
-- SQL as a single batch.

-- Delete column OfficeId and associated foreign key from table Data.application.job
DECLARE @fknamedbe82473322446889cf476760efd0f2b NVARCHAR(600);

SELECT TOP(1) @fknamedbe82473322446889cf476760efd0f2b = fk.name
FROM sys.foreign_key_columns fkc
     left outer join sys.tables t on fkc.parent_object_id = t.object_id
     left outer join sys.foreign_keys fk on fk.object_id = fkc.constraint_object_id
WHERE t.name = 'Data.application.job' and col_name(t.object_id, fkc.parent_column_id) = 'OfficeId';

IF @fknamedbe82473322446889cf476760efd0f2b IS NOT NULL
BEGIN
  DECLARE @dropfkd2282bbddc3247fc85d640da0e3d8b4c NVARCHAR(640);
  SET @dropfkd2282bbddc3247fc85d640da0e3d8b4c = 'ALTER TABLE [dbo].[Data.application.job] DROP CONSTRAINT [' + @fknamedbe82473322446889cf476760efd0f2b + ']';
  EXEC sp_executesql @dropfkd2282bbddc3247fc85d640da0e3d8b4c;
END

DECLARE @dfname0e5077ce99c940f6a462ace7add65ff7 NVARCHAR(600);

SELECT TOP(1) @dfname0e5077ce99c940f6a462ace7add65ff7 = df.name
FROM sys.default_constraints df
     left outer join sys.tables t on df.parent_object_id = t.object_id
WHERE t.name = 'Data.application.job' and col_name(t.object_id, df.parent_column_id) = 'OfficeId';

IF @dfname0e5077ce99c940f6a462ace7add65ff7 IS NOT NULL
BEGIN
  DECLARE @dropdf6f354a6f976c45e1b57296c69778e3b4 NVARCHAR(640);
  SET @dropdf6f354a6f976c45e1b57296c69778e3b4 = 'ALTER TABLE [dbo].[Data.application.job] DROP CONSTRAINT [' + @dfname0e5077ce99c940f6a462ace7add65ff7 + ']';
  EXEC sp_executesql @dropdf6f354a6f976c45e1b57296c69778e3b4;
END

ALTER TABLE [Data.application.job] DROP COLUMN [OfficeId];
/* ERROR:
Office: Delete column OfficeId and associated foreign key from table Data.application.job
  - Incorrect syntax near '.'.
The object 'FK__Data.Appl__Offic__61FB72FB' is dependent on column 'OfficeId'.
ALTER TABLE DROP COLUMN OfficeId failed because one or more objects access this column.
*/;
-- Delete column OfficeUnassigned
DECLARE @fkname5635b6486c6f406487cfe37dd565a6a6 NVARCHAR(600);

SELECT TOP(1) @fkname5635b6486c6f406487cfe37dd565a6a6 = fk.name
FROM sys.foreign_key_columns fkc
     left outer join sys.tables t on fkc.parent_object_id = t.object_id
     left outer join sys.foreign_keys fk on fk.object_id = fkc.constraint_object_id
WHERE t.name = 'Data.application.job' and col_name(t.object_id, fkc.parent_column_id) = 'OfficeUnassigned';

IF @fkname5635b6486c6f406487cfe37dd565a6a6 IS NOT NULL
BEGIN
  DECLARE @dropfk820b49d296ed4d0fbb2251e03e1e2542 NVARCHAR(640);
  SET @dropfk820b49d296ed4d0fbb2251e03e1e2542 = 'ALTER TABLE [dbo].[Data.application.job] DROP CONSTRAINT [' + @fkname5635b6486c6f406487cfe37dd565a6a6 + ']';
  EXEC sp_executesql @dropfk820b49d296ed4d0fbb2251e03e1e2542;
END

DECLARE @dfname4fecd62f7d42410c842ee08aaa99fe51 NVARCHAR(600);

SELECT TOP(1) @dfname4fecd62f7d42410c842ee08aaa99fe51 = df.name
FROM sys.default_constraints df
     left outer join sys.tables t on df.parent_object_id = t.object_id
WHERE t.name = 'Data.application.job' and col_name(t.object_id, df.parent_column_id) = 'OfficeUnassigned';

IF @dfname4fecd62f7d42410c842ee08aaa99fe51 IS NOT NULL
BEGIN
  DECLARE @dropdf399c9f5fea2845e0a9839d1bdda31216 NVARCHAR(640);
  SET @dropdf399c9f5fea2845e0a9839d1bdda31216 = 'ALTER TABLE [dbo].[Data.application.job] DROP CONSTRAINT [' + @dfname4fecd62f7d42410c842ee08aaa99fe51 + ']';
  EXEC sp_executesql @dropdf399c9f5fea2845e0a9839d1bdda31216;
END

ALTER TABLE [Data.application.job] DROP COLUMN [OfficeUnassigned];

-- One or more of the above statements failed.  Review the log for error info.
-- Depending on your database, changes may have been rolled back or may have
-- been partially applied.
