
-- Add table Data.Application.JobOfficeMapper to the database
CREATE TABLE [Data.Application.JobOfficeMapper] (Id BIGINT NOT NULL PRIMARY KEY ,
[OfficeUnassigned] BIT NULL,
[JobId] BIGINT NOT NULL DEFAULT 0,
[OfficeId] BIGINT NOT NULL DEFAULT 0,
FOREIGN KEY (JobId) REFERENCES [Data.Application.Job](Id),
FOREIGN KEY (OfficeId) REFERENCES [Data.Application.Office](Id));
