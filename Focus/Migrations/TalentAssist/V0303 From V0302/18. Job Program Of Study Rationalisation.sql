-- rename DegreeId Column
sp_rename '[Data.Application.JobProgramOfStudy].DegreeId', 'DegreeEducationLevelId', 'COLUMN'

-- realign the data
BEGIN TRANSACTION

UPDATE [Data.Application.JobProgramOfStudy]
SET [Data.Application.JobProgramOfStudy].DegreeEducationLevelId = [Library.DegreeEducationLevel].Id 
FROM [Data.Application.JobProgramOfStudy], [Library.DegreeEducationLevel]
WHERE [Data.Application.JobProgramOfStudy].DegreeEducationLevelId = [Library.DegreeEducationLevel].DegreeId 
AND [Data.Application.JobProgramOfStudy].ProgramOfStudy = [Library.DegreeEducationLevel].Name

COMMIT TRANSACTION



-- Below is some helpful sql to analyse the data prior to running above

--Check for data that cannot be mapped
--SELECT * FROM [Data.Application.JobProgramOfStudy]  WHERE
--Id NOT IN (SELECT jpos.Id FROM [Data.Application.JobProgramOfStudy] jpos
--INNER JOIN [Library.DegreeEducationLevel] del ON jpos.DegreeId = del.DegreeId AND jpos.ProgramOfStudy = del.Name)
--AND Id NOT IN (SELECT jpos.Id FROM [Data.Application.JobProgramOfStudy] jpos
--INNER JOIN [Library.DegreeEducationLevel] del ON jpos.DegreeId = del.Id AND jpos.ProgramOfStudy = del.Name)

-- Check what will be mapped
--SELECT * FROM [Data.Application.JobProgramOfStudy] jpos
--INNER JOIN [Library.DegreeEducationLevel] del ON jpos.DegreeId = del.DegreeId AND jpos.ProgramOfStudy = del.Name