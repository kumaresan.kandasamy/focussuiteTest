SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [dbo].[Library.ROnet]
ALTER TABLE [dbo].[Library.ROnet] DROP CONSTRAINT [FK__Library.R__OnetI__443605EA]

-- Drop constraints from [dbo].[Library.OnetROnet]
ALTER TABLE [dbo].[Library.OnetROnet] DROP CONSTRAINT [FK__Library.O__OnetI__61F08603]

-- Drop constraints from [dbo].[Library.OnetLocalisationItem]
ALTER TABLE [dbo].[Library.OnetLocalisationItem] DROP CONSTRAINT [FK__Library.O__Local__1387E197]

-- Update rows in [dbo].[Library.OnetROnet]
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=1943
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=2 WHERE [Id]=1944
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=1958
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=2 WHERE [Id]=1959
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2050
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=2 WHERE [Id]=2051
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=2 WHERE [Id]=2155
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2200
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=2 WHERE [Id]=2201
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2285
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=2 WHERE [Id]=2286
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2373
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=2 WHERE [Id]=2374
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2391
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=2 WHERE [Id]=2392
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2406
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=2 WHERE [Id]=2407
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2409
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=2 WHERE [Id]=2411
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=2 WHERE [Id]=2431
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=2 WHERE [Id]=2442
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=2 WHERE [Id]=2451
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=2 WHERE [Id]=2453
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2639
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=2 WHERE [Id]=2647
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=94 WHERE [Id]=2649
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=20 WHERE [Id]=2654
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=26 WHERE [Id]=2658
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=102 WHERE [Id]=2660
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=157 WHERE [Id]=2665
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=162 WHERE [Id]=2670
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=158 WHERE [Id]=2673
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=163 WHERE [Id]=2674
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=162 WHERE [Id]=2675
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=184 WHERE [Id]=2678
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=187 WHERE [Id]=2681
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=190 WHERE [Id]=2682
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=190 WHERE [Id]=2684
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=187 WHERE [Id]=2685
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=187 WHERE [Id]=2686
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=187 WHERE [Id]=2687
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=187 WHERE [Id]=2688
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=190 WHERE [Id]=2689
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=190 WHERE [Id]=2690
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=200 WHERE [Id]=2693
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=199 WHERE [Id]=2694
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=199 WHERE [Id]=2695
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=171 WHERE [Id]=2696
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=209 WHERE [Id]=2697
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=168 WHERE [Id]=2698
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=201 WHERE [Id]=2699
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=257 WHERE [Id]=2722
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=260 WHERE [Id]=2734
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=311 WHERE [Id]=2736
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=38 WHERE [Id]=2738
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=239 WHERE [Id]=2752
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=47 WHERE [Id]=2761
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=318 WHERE [Id]=2768
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=399 WHERE [Id]=2777
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=413 WHERE [Id]=2778
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=423 WHERE [Id]=2784
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=449 WHERE [Id]=2785
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=433 WHERE [Id]=2786
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=446 WHERE [Id]=2791
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=446 WHERE [Id]=2792
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=515 WHERE [Id]=2801
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=514 WHERE [Id]=2806
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=549 WHERE [Id]=2807
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=31 WHERE [Id]=2809
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=557 WHERE [Id]=2811
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=569 WHERE [Id]=2817
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=565 WHERE [Id]=2821
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=589 WHERE [Id]=2822
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=577 WHERE [Id]=2826
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=581 WHERE [Id]=2827
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=565 WHERE [Id]=2829
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=580 WHERE [Id]=2831
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=567 WHERE [Id]=2832
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=571 WHERE [Id]=2834
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=571 WHERE [Id]=2835
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=593 WHERE [Id]=2836
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=571 WHERE [Id]=2837
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=613 WHERE [Id]=2838
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=614 WHERE [Id]=2841
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=589 WHERE [Id]=2843
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=699 WHERE [Id]=2844
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=589 WHERE [Id]=2846
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=613 WHERE [Id]=2847
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=611 WHERE [Id]=2849
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=1001 WHERE [Id]=2853
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=607 WHERE [Id]=2854
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=629 WHERE [Id]=2855
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=613 WHERE [Id]=2857
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=581 WHERE [Id]=2858
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=685 WHERE [Id]=2861
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=565 WHERE [Id]=2864
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=615 WHERE [Id]=2865
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=571 WHERE [Id]=2869
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=227 WHERE [Id]=2870
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=685 WHERE [Id]=2872
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=571 WHERE [Id]=2873
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=621 WHERE [Id]=2874
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=576 WHERE [Id]=2876
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=193 WHERE [Id]=2877
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=701 WHERE [Id]=2880
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=701 WHERE [Id]=2882
UPDATE [dbo].[Library.OnetROnet] SET [ROnetId]=687 WHERE [Id]=2886
-- Operation applied to 107 rows out of 107

-- Add row to [dbo].[Library.OnetLocalisationItem]
SET IDENTITY_INSERT [dbo].[Library.OnetLocalisationItem] ON
INSERT INTO [dbo].[Library.OnetLocalisationItem] ([Id], [Key], [PrimaryValue], [SecondaryValue], [LocalisationId]) VALUES (1695236, N'Ronet.43-4051.S1', N'Patient Advocate / Liaison', N'', 12090)
SET IDENTITY_INSERT [dbo].[Library.OnetLocalisationItem] OFF

-- Add rows to [dbo].[Library.OnetROnet]
SET IDENTITY_INSERT [dbo].[Library.OnetROnet] ON
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2887, 1, 112758, 2, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2888, 1, 112761, 222, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2889, 1, 112776, 9, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2890, 1, 112791, 17, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2891, 1, 112803, 28, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2892, 1, 112812, 20, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2893, 1, 112818, 20, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2894, 1, 112821, 20, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2895, 1, 112824, 20, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2896, 1, 112839, 32, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2897, 1, 112845, 32, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2898, 1, 112866, 39, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2899, 1, 112878, 20, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2900, 1, 112887, 440, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2901, 1, 112893, 44, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2902, 1, 112899, 51, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2903, 1, 112908, 551, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2904, 1, 112911, 55, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2905, 1, 112917, 61, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2906, 1, 112926, 16, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2907, 1, 112932, 95, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2908, 1, 112941, 419, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2909, 1, 112944, 20, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2910, 1, 112947, 20, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2911, 1, 112950, 167, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2912, 1, 112953, 28, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2913, 1, 112956, 22, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2914, 1, 112959, 68, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2915, 1, 112962, 69, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2916, 1, 112983, 107, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2917, 1, 112986, 72, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2918, 1, 112989, 586, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2919, 1, 112992, 450, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2920, 1, 113004, 76, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2921, 1, 113007, 76, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2922, 1, 113010, 76, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2923, 1, 113019, 76, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2924, 1, 113028, 79, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2925, 1, 113037, 716, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2926, 1, 113040, 89, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2927, 1, 113055, 96, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2928, 1, 113067, 100, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2929, 1, 113070, 100, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2930, 1, 113097, 96, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2931, 1, 113103, 111, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2932, 1, 113106, 112, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2933, 1, 113112, 114, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2934, 1, 113124, 120, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2935, 1, 113127, 120, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2936, 1, 113136, 151, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2937, 1, 113148, 117, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2938, 1, 113154, 132, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2939, 1, 113175, 137, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2940, 1, 113184, 140, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2941, 1, 113190, 85, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2942, 1, 113217, 147, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2943, 1, 113241, 162, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2944, 1, 113283, 167, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2945, 1, 113343, 179, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2946, 1, 113352, 172, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2947, 1, 113382, 191, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2948, 1, 113397, 188, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2949, 1, 113415, 717, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2950, 1, 113439, 187, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2951, 1, 113448, 191, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2952, 1, 113451, 191, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2953, 1, 113481, 200, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2954, 1, 113484, 197, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2955, 1, 113517, 207, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2956, 1, 113520, 208, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2957, 1, 113556, 137, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2958, 1, 113565, 85, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2959, 1, 113571, 218, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2960, 1, 113592, 259, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2961, 1, 113598, 221, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2962, 1, 113601, 221, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2963, 1, 113607, 137, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2964, 1, 113610, 259, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2965, 1, 113613, 222, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2966, 1, 113625, 225, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2967, 1, 113628, 225, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2968, 1, 113640, 229, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2969, 1, 113643, 559, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2970, 1, 113646, 1001, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2971, 1, 113649, 1001, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2972, 1, 113652, 1001, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2973, 1, 113655, 217, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2974, 1, 113658, 220, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2975, 1, 113670, 232, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2976, 1, 113676, 225, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2977, 1, 113679, 137, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2978, 1, 113685, 235, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2979, 1, 113694, 237, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2980, 1, 113697, 240, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2981, 1, 113706, 234, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2982, 1, 113730, 248, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2983, 1, 113736, 252, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2984, 1, 113739, 252, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2985, 1, 113748, 547, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2986, 1, 113757, 256, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2987, 1, 113760, 259, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2988, 1, 113781, 259, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2989, 1, 113784, 259, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2990, 1, 113796, 259, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2991, 1, 113799, 259, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2992, 1, 113817, 259, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2993, 1, 113826, 259, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2994, 1, 113850, 259, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2995, 1, 113865, 259, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2996, 1, 113871, 259, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2997, 1, 113886, 258, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2998, 1, 113892, 258, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (2999, 1, 113895, 264, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3000, 1, 113898, 264, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3001, 1, 113901, 264, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3002, 1, 113910, 267, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3003, 1, 113913, 264, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3004, 1, 113937, 32, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3005, 1, 113949, 276, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3006, 1, 113955, 278, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3007, 1, 113967, 280, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3008, 1, 113976, 284, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3009, 1, 113982, 286, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3010, 1, 114000, 291, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3011, 1, 114003, 291, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3012, 1, 114006, 28, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3013, 1, 114009, 290, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3014, 1, 114021, 294, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3015, 1, 114027, 295, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3016, 1, 114060, 301, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3017, 1, 114075, 308, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3018, 1, 114090, 311, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3019, 1, 114099, 315, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3020, 1, 114105, 311, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3021, 1, 114201, 322, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3022, 1, 114207, 331, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3023, 1, 114210, 331, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3024, 1, 114222, 324, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3025, 1, 114285, 349, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3026, 1, 114291, 351, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3027, 1, 114294, 351, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3028, 1, 114297, 351, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3029, 1, 114324, 363, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3030, 1, 114333, 371, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3031, 1, 114345, 337, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3032, 1, 114348, 377, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3033, 1, 114351, 378, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3034, 1, 114363, 206, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3035, 1, 114417, 399, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3036, 1, 114426, 400, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3037, 1, 114429, 400, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3038, 1, 114432, 401, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3039, 1, 114441, 402, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3040, 1, 114447, 403, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3041, 1, 114450, 403, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3042, 1, 114453, 402, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3043, 1, 114477, 408, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3044, 1, 114495, 418, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3045, 1, 114504, 415, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3046, 1, 114513, 417, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3047, 1, 114519, 418, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3048, 1, 114540, 422, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3049, 1, 114555, 425, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3050, 1, 114558, 425, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3051, 1, 114567, 428, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3052, 1, 114594, 432, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3053, 1, 114618, 441, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3054, 1, 114633, 308, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3055, 1, 114642, 455, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3056, 1, 114654, 448, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3057, 1, 114660, 450, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3058, 1, 114687, 457, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3059, 1, 114717, 464, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3060, 1, 114747, 474, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3061, 1, 114762, 477, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3062, 1, 114765, 479, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3063, 1, 114771, 482, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3064, 1, 114780, 289, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3065, 1, 114786, 488, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3066, 1, 114798, 492, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3067, 1, 114801, 496, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3068, 1, 114807, 505, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3069, 1, 114810, 311, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3070, 1, 114825, 508, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3071, 1, 114846, 519, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3072, 1, 114873, 1088, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3073, 1, 114885, 522, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3074, 1, 114897, 538, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3075, 1, 114903, 517, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3076, 1, 114930, 551, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3077, 1, 114933, 551, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3078, 1, 114936, 551, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3079, 1, 114939, 535, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3080, 1, 114945, 537, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3081, 1, 114951, 286, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3082, 1, 114960, 540, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3083, 1, 114990, 549, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3084, 1, 114996, 551, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3085, 1, 115008, 149, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3086, 1, 115011, 150, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3087, 1, 115014, 528, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3088, 1, 115017, 31, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3089, 1, 115023, 34, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3090, 1, 115026, 31, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3091, 1, 115029, 32, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3092, 1, 115032, 76, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3093, 1, 115035, 556, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3094, 1, 115038, 444, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3095, 1, 115062, 34, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3096, 1, 115074, 561, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3097, 1, 115077, 561, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3098, 1, 115080, 561, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3099, 1, 115092, 564, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3100, 1, 115128, 588, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3101, 1, 115131, 571, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3102, 1, 115158, 570, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3103, 1, 115200, 577, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3104, 1, 115227, 570, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3105, 1, 115230, 590, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3106, 1, 115233, 591, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3107, 1, 115245, 591, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3108, 1, 115248, 593, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3109, 1, 115251, 596, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3110, 1, 115257, 595, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3111, 1, 115275, 596, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3112, 1, 115278, 596, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3113, 1, 115281, 596, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3114, 1, 115287, 130, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3115, 1, 115293, 600, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3116, 1, 115302, 604, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3117, 1, 115305, 602, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3118, 1, 115362, 608, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3119, 1, 115392, 625, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3120, 1, 115398, 1082, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3121, 1, 115401, 622, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3122, 1, 115407, 630, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3123, 1, 115419, 630, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3124, 1, 115422, 630, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3125, 1, 115428, 655, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3126, 1, 115434, 562, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3127, 1, 115479, 635, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3128, 1, 115494, 637, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3129, 1, 115515, 657, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3130, 1, 115518, 684, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3131, 1, 115521, 684, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3132, 1, 115524, 642, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3133, 1, 115527, 593, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3134, 1, 115542, 1001, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3135, 1, 115545, 683, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3136, 1, 115548, 683, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3137, 1, 115551, 661, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3138, 1, 115554, 683, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3139, 1, 115557, 683, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3140, 1, 115560, 684, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3141, 1, 115575, 648, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3142, 1, 115581, 685, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3143, 1, 115584, 684, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3144, 1, 115587, 643, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3145, 1, 115596, 653, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3146, 1, 115605, 652, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3147, 1, 115611, 654, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3148, 1, 115617, 655, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3149, 1, 115620, 655, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3150, 1, 115629, 684, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3151, 1, 115632, 684, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3152, 1, 115635, 684, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3153, 1, 115638, 684, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3154, 1, 115644, 661, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3155, 1, 115650, 658, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3156, 1, 115665, 565, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3157, 1, 115674, 1002, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3158, 1, 115692, 1002, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3159, 1, 115695, 667, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3160, 1, 115701, 685, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3161, 1, 115707, 685, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3162, 1, 115710, 685, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3163, 1, 115719, 643, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3164, 1, 115728, 685, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3165, 1, 115731, 684, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3166, 1, 115734, 657, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3167, 1, 115737, 684, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3168, 1, 115749, 676, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3169, 1, 115752, 676, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3170, 1, 115758, 678, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3171, 1, 115761, 361, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3172, 1, 115776, 685, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3173, 1, 115779, 685, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3174, 1, 115782, 685, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3175, 1, 115785, 684, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3176, 1, 115788, 684, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3177, 1, 115791, 684, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3178, 1, 115803, 685, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3179, 1, 115806, 684, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3180, 1, 115809, 683, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3181, 1, 115821, 684, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3182, 1, 115827, 530, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3183, 1, 115830, 686, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3184, 1, 115836, 688, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3185, 1, 115839, 690, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3186, 1, 115848, 688, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3187, 1, 115851, 698, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3188, 1, 115884, 589, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3189, 1, 115890, 700, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3190, 1, 115893, 700, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3191, 1, 115902, 702, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3192, 1, 115914, 701, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3193, 1, 115941, 707, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3194, 1, 115953, 571, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3195, 1, 115959, 571, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3196, 1, 115971, 571, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3197, 1, 115977, 571, 2)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3198, 1, 115995, 1083, 2)
SET IDENTITY_INSERT [dbo].[Library.OnetROnet] OFF
-- Operation applied to 312 rows out of 312

-- Add row to [dbo].[Library.ROnet]
SET IDENTITY_INSERT [dbo].[Library.ROnet] ON
INSERT INTO [dbo].[Library.ROnet] ([Id], [Code], [Key], [NumericCode], [OnetId]) VALUES (1088, N'43-4051.S1', N'Ronet.43-4051.S1', N'43-4051.91', 114873)
SET IDENTITY_INSERT [dbo].[Library.ROnet] OFF

-- Add constraints to [dbo].[Library.ROnet]
ALTER TABLE [dbo].[Library.ROnet] ADD CONSTRAINT [FK__Library.R__OnetI__443605EA] FOREIGN KEY ([OnetId]) REFERENCES [dbo].[Library.Onet] ([Id])

-- Add constraints to [dbo].[Library.OnetROnet]
ALTER TABLE [dbo].[Library.OnetROnet] ADD CONSTRAINT [FK__Library.O__OnetI__61F08603] FOREIGN KEY ([OnetId]) REFERENCES [dbo].[Library.Onet] ([Id])

-- Add constraints to [dbo].[Library.OnetLocalisationItem]
ALTER TABLE [dbo].[Library.OnetLocalisationItem] ADD CONSTRAINT [FK__Library.O__Local__1387E197] FOREIGN KEY ([LocalisationId]) REFERENCES [dbo].[Library.Localisation] ([Id])
COMMIT TRANSACTION
GO