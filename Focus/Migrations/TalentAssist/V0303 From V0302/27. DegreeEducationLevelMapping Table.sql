IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Library.DegreeEducationLevelMapping')
BEGIN
	CREATE TABLE [Library.DegreeEducationLevelMapping]
	(
		[Id] [bigint] IDENTITY(1,1) NOT NULL,
		[DegreeEducationLevelId] BIGINT NOT NULL,
		[LensId] INT NOT NULL,
		CONSTRAINT [PK_Library.DegreeEducationLevelMapping] PRIMARY KEY CLUSTERED (
			[Id] ASC
		)
	)
END
GO

IF NOT EXISTS(SELECT 1 FROM [Library.DegreeEducationLevelMapping])
BEGIN
	INSERT INTO [Library.DegreeEducationLevelMapping]
	(
		DegreeEducationLevelId,
		LensId
	)
	SELECT
		DEL.Id,
		CASE D.RcipCode
			WHEN 'BE.1007' THEN 305
			WHEN 'BE.1008' THEN 301
			WHEN 'BE.1009' THEN 303
			WHEN 'BE.1010' THEN 302
			WHEN 'BE.1011' THEN 304
			WHEN 'BE.1012' THEN 306
			WHEN 'BE.1013' THEN 300
		END
	FROM
		[Library.Degree] D
	INNER JOIN [Library.DegreeEducationLevel] DEL
		ON DEL.DegreeId = D.Id
	WHERE 
		D.RcipCode IN ('BE.1007','BE.1008','BE.1009','BE.1010','BE.1011','BE.1012','BE.1013')
		AND DEL.EducationLevel = 18
END