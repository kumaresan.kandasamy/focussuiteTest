/*
Run this script on:

        BOSDEMSVR008.FocusBase    -  This database will be modified

to synchronize it with:

        BOSDEMSVR008.FocusDev

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.0.0 from Red Gate Software Ltd at 10/01/2014 13:31:25

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByPostingCountDesc]'
GO



-- =============================================
-- Author:		Andy Jones
-- Create date: 27 November 2013
-- Description:	Gets spidered employers with good matches ordered by Posting Count Desc
-- =============================================
ALTER PROCEDURE [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByPostingCountDesc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumNumberOfPostings int,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    CREATE TABLE #PersonPostingMatchToIgnore
    (
		PersonPostingMatchId bigint
	)
      
    CREATE TABLE #PersonPostingMatch
	(
		Id bigint,
        PostingId bigint,
        PersonId bigint
	)
      
    CREATE TABLE #PostingPersonMatchCount
    (
		PostingId bigint,
        MatchCount int
    )
      
    CREATE TABLE #Posting
    (
		Id bigint,
        LensPostingId nvarchar(150),
        JobTitle nvarchar(255),
        EmployerName nvarchar(255),
        PostingDate datetime
    )       
    
    CREATE TABLE #Employer
    (
		EmployerName nvarchar(255),
        PostingCount int
	)
      
    CREATE TABLE #RankedEmployer
    (
		Id int IDENTITY PRIMARY KEY,
        LensPostingId nvarchar(150),
        JobTitle nvarchar(255),
        EmployerName nvarchar(255),
        PostingDate datetime,
        NumberOfPostings int
	)      
    
    DECLARE @FirstRec int
    DECLARE @LastRec int
            
    SET @FirstRec = (@PageNumber - 1) * @PageSize
    SET @LastRec = (@PageNumber * @PageSize + 1)
      
    -- Populate the Postings
    INSERT INTO #Posting
	SELECT 
		p.Id,
        p.LensPostingId,
        p.JobTitle,
        p.EmployerName,
        p.CreatedOn
    FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
	WHERE 
		CreatedOn > @MinimumPostingDate
		AND OriginId = '999'
		AND (EmployerName LIKE '%' + IsNull(@EmployerName, '') + '%')
      
       		
	-- Populate the matches to ignore for this user
    INSERT #PersonPostingMatchToIgnore
    SELECT 
		ppm.Id
    FROM
		[Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK)
        INNER JOIN [Data.Application.PersonPostingMatch] ppm WITH (NOLOCK) ON ppmti.PostingId = ppm.PostingId AND ppmti.PersonId = ppm.PersonId
    WHERE 
        EXISTS (SELECT Id FROM #Posting WHERE #Posting.Id = ppmti.PostingId)
        AND ppmti.UserId = @UserID
            
    -- Populate the matches
    INSERT INTO #PersonPostingMatch
    SELECT 
        Id,
        PostingId,
        PersonId
    FROM 
		[Data.Application.PersonPostingMatch] WITH (NOLOCK)
    WHERE
		EXISTS (SELECT Id FROM #Posting WHERE #Posting.Id = PostingId)
            
    -- Remove ignore matches from matches
    DELETE FROM #PersonPostingMatch
    WHERE
		EXISTS (SELECT PersonPostingMatchId FROM #PersonPostingMatchToIgnore WHERE #PersonPostingMatchToIgnore.PersonPostingMatchId = Id)
      
    -- Populate the match counts
    INSERT INTO #PostingPersonMatchCount
    SELECT 
		PostingId, 
        COUNT(Id) 
    FROM 
        #PersonPostingMatch
    GROUP BY
        PostingId
            
    --Populate the employer
    INSERT INTO #Employer
    SELECT 
		p.EmployerName,
        COUNT(Id)
    FROM
		#Posting p
        INNER JOIN #PostingPersonMatchCount ppmc ON p.Id = ppmc.PostingId
	GROUP BY 
		EmployerName
    HAVING 
		COUNT(Id) >= @MinimumNumberOfPostings
		
	SELECT @RowCount = @@ROWCOUNT
		
	-- Delete postings with no matches
	DELETE FROM #Posting 
	WHERE 
		NOT EXISTS (SELECT PostingId FROM #PostingPersonMatchCount WHERE PostingId = Id);
	
	-- Remove non-top postings based on order
	WITH OrderedPostings AS
	(
		SELECT  
			Id,
			RANK() OVER (PARTITION BY EmployerName ORDER BY PostingDate DESC, JobTitle, Id) AS [Rank]
		FROM 
			#Posting 
 	) 
	DELETE FROM #Posting
	WHERE
		NOT EXISTS (SELECT Id FROM OrderedPostings op WHERE op.[Rank] = 1 AND op.Id = #Posting.Id)
		
	-- Rank the employer based on the sort order
    INSERT INTO #RankedEmployer
    SELECT 
		p.LensPostingId,
		p.JobTitle,
		e.EmployerName,
		p.PostingDate,
		e.PostingCount
	FROM 
		#Employer e
	INNER JOIN
		#Posting p ON e.EmployerName = p.EmployerName
	ORDER BY
		e.PostingCount DESC, e.EmployerName
            
    SELECT 
		*
    FROM 
		#RankedEmployer
    WHERE
		Id > @FirstRec AND Id < @LastRec
		
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByEmployerNameAsc]'
GO




-- =============================================
-- Author:		Andy Jones
-- Create date: 27 November 2013
-- Description:	Gets spidered employers with good matches ordered by Employer Name Asc
-- =============================================
ALTER PROCEDURE [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByEmployerNameAsc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumNumberOfPostings int,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    CREATE TABLE #PersonPostingMatchToIgnore
    (
		PersonPostingMatchId bigint
	)
      
    CREATE TABLE #PersonPostingMatch
	(
		Id bigint,
        PostingId bigint,
        PersonId bigint
	)
      
    CREATE TABLE #PostingPersonMatchCount
    (
		PostingId bigint,
        MatchCount int
    )
      
    CREATE TABLE #Posting
    (
		Id bigint,
        LensPostingId nvarchar(150),
        JobTitle nvarchar(255),
        EmployerName nvarchar(255),
        PostingDate datetime
    )       
    
    CREATE TABLE #Employer
    (
		EmployerName nvarchar(255),
        PostingCount int
	)
      
    CREATE TABLE #RankedEmployer
    (
		Id int IDENTITY PRIMARY KEY,
        LensPostingId nvarchar(150),
        JobTitle nvarchar(255),
        EmployerName nvarchar(255),
        PostingDate datetime,
        NumberOfPostings int
	)      
    
    DECLARE @FirstRec int
    DECLARE @LastRec int
            
    SET @FirstRec = (@PageNumber - 1) * @PageSize
    SET @LastRec = (@PageNumber * @PageSize + 1)
      
    -- Populate the Postings
    INSERT INTO #Posting
	SELECT 
		p.Id,
        p.LensPostingId,
        p.JobTitle,
        p.EmployerName,
        p.CreatedOn
    FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
	WHERE 
		CreatedOn > @MinimumPostingDate
		AND OriginId = '999'
		AND (EmployerName LIKE '%' + IsNull(@EmployerName, '') + '%')
      
       		
	-- Populate the matches to ignore for this user
    INSERT #PersonPostingMatchToIgnore
    SELECT 
		ppm.Id
    FROM
		[Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK)
        INNER JOIN [Data.Application.PersonPostingMatch] ppm WITH (NOLOCK) ON ppmti.PostingId = ppm.PostingId AND ppmti.PersonId = ppm.PersonId
    WHERE 
        EXISTS (SELECT Id FROM #Posting WHERE #Posting.Id = ppmti.PostingId)
        AND ppmti.UserId = @UserID
            
    -- Populate the matches
    INSERT INTO #PersonPostingMatch
    SELECT 
        Id,
        PostingId,
        PersonId
    FROM 
		[Data.Application.PersonPostingMatch] WITH (NOLOCK)
    WHERE
		EXISTS (SELECT Id FROM #Posting WHERE #Posting.Id = PostingId)
            
    -- Remove ignore matches from matches
    DELETE FROM #PersonPostingMatch
    WHERE
		EXISTS (SELECT PersonPostingMatchId FROM #PersonPostingMatchToIgnore WHERE #PersonPostingMatchToIgnore.PersonPostingMatchId = Id)
      
    -- Populate the match counts
    INSERT INTO #PostingPersonMatchCount
    SELECT 
		PostingId, 
        COUNT(Id) 
    FROM 
        #PersonPostingMatch
    GROUP BY
        PostingId
            
    --Populate the employer
    INSERT INTO #Employer
    SELECT 
		p.EmployerName,
        COUNT(Id)
    FROM
		#Posting p
        INNER JOIN #PostingPersonMatchCount ppmc ON p.Id = ppmc.PostingId
	GROUP BY 
		EmployerName
    HAVING 
		COUNT(Id) >= @MinimumNumberOfPostings
		
	SELECT @RowCount = @@ROWCOUNT
		
	-- Delete postings with no matches
	DELETE FROM #Posting 
	WHERE 
		NOT EXISTS (SELECT PostingId FROM #PostingPersonMatchCount WHERE PostingId = Id);
	
	-- Remove non-top postings based on order
	WITH OrderedPostings AS
	(
		SELECT  
			Id,
			RANK() OVER (PARTITION BY EmployerName ORDER BY PostingDate DESC, JobTitle, Id) AS [Rank]
		FROM 
			#Posting 
 	) 
	DELETE FROM #Posting
	WHERE
		NOT EXISTS (SELECT Id FROM OrderedPostings op WHERE op.[Rank] = 1 AND op.Id = #Posting.Id)
		
	-- Rank the employer based on the sort order
    INSERT INTO #RankedEmployer
    SELECT 
		p.LensPostingId,
		p.JobTitle,
		e.EmployerName,
		p.PostingDate,
		e.PostingCount
	FROM 
		#Employer e
	INNER JOIN
		#Posting p ON e.EmployerName = p.EmployerName
	ORDER BY
		e.EmployerName, e.PostingCount DESC
            
    SELECT 
		*
    FROM 
		#RankedEmployer
    WHERE
		Id > @FirstRec AND Id < @LastRec
		
END




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByEmployerNameDesc]'
GO




-- =============================================
-- Author:		Andy Jones
-- Create date: 27 November 2013
-- Description:	Gets spidered employers with good matches ordered by Employer Name Desc
-- =============================================
ALTER PROCEDURE [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByEmployerNameDesc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumNumberOfPostings int,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    CREATE TABLE #PersonPostingMatchToIgnore
    (
		PersonPostingMatchId bigint
	)
      
    CREATE TABLE #PersonPostingMatch
	(
		Id bigint,
        PostingId bigint,
        PersonId bigint
	)
      
    CREATE TABLE #PostingPersonMatchCount
    (
		PostingId bigint,
        MatchCount int
    )
      
    CREATE TABLE #Posting
    (
		Id bigint,
        LensPostingId nvarchar(150),
        JobTitle nvarchar(255),
        EmployerName nvarchar(255),
        PostingDate datetime
    )       
    
    CREATE TABLE #Employer
    (
		EmployerName nvarchar(255),
        PostingCount int
	)
      
    CREATE TABLE #RankedEmployer
    (
		Id int IDENTITY PRIMARY KEY,
        LensPostingId nvarchar(150),
        JobTitle nvarchar(255),
        EmployerName nvarchar(255),
        PostingDate datetime,
        NumberOfPostings int
	)      
    
    DECLARE @FirstRec int
    DECLARE @LastRec int
            
    SET @FirstRec = (@PageNumber - 1) * @PageSize
    SET @LastRec = (@PageNumber * @PageSize + 1)
      
    -- Populate the Postings
    INSERT INTO #Posting
	SELECT 
		p.Id,
        p.LensPostingId,
        p.JobTitle,
        p.EmployerName,
        p.CreatedOn
    FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
	WHERE 
		CreatedOn > @MinimumPostingDate
		AND OriginId = '999'
		AND (EmployerName LIKE '%' + IsNull(@EmployerName, '') + '%')
      
       		
	-- Populate the matches to ignore for this user
    INSERT #PersonPostingMatchToIgnore
    SELECT 
		ppm.Id
    FROM
		[Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK)
        INNER JOIN [Data.Application.PersonPostingMatch] ppm WITH (NOLOCK) ON ppmti.PostingId = ppm.PostingId AND ppmti.PersonId = ppm.PersonId
    WHERE 
        EXISTS (SELECT Id FROM #Posting WHERE #Posting.Id = ppmti.PostingId)
        AND ppmti.UserId = @UserID
            
    -- Populate the matches
    INSERT INTO #PersonPostingMatch
    SELECT 
        Id,
        PostingId,
        PersonId
    FROM 
		[Data.Application.PersonPostingMatch] WITH (NOLOCK)
    WHERE
		EXISTS (SELECT Id FROM #Posting WHERE #Posting.Id = PostingId)
            
    -- Remove ignore matches from matches
    DELETE FROM #PersonPostingMatch
    WHERE
		EXISTS (SELECT PersonPostingMatchId FROM #PersonPostingMatchToIgnore WHERE #PersonPostingMatchToIgnore.PersonPostingMatchId = Id)
      
    -- Populate the match counts
    INSERT INTO #PostingPersonMatchCount
    SELECT 
		PostingId, 
        COUNT(Id) 
    FROM 
        #PersonPostingMatch
    GROUP BY
        PostingId
            
    --Populate the employer
    INSERT INTO #Employer
    SELECT 
		p.EmployerName,
        COUNT(Id)
    FROM
		#Posting p
        INNER JOIN #PostingPersonMatchCount ppmc ON p.Id = ppmc.PostingId
	GROUP BY 
		EmployerName
    HAVING 
		COUNT(Id) >= @MinimumNumberOfPostings
		
	SELECT @RowCount = @@ROWCOUNT
		
	-- Delete postings with no matches
	DELETE FROM #Posting 
	WHERE 
		NOT EXISTS (SELECT PostingId FROM #PostingPersonMatchCount WHERE PostingId = Id);
	
	-- Remove non-top postings based on order
	WITH OrderedPostings AS
	(
		SELECT  
			Id,
			RANK() OVER (PARTITION BY EmployerName ORDER BY PostingDate DESC, JobTitle, Id) AS [Rank]
		FROM 
			#Posting 
 	) 
	DELETE FROM #Posting
	WHERE
		NOT EXISTS (SELECT Id FROM OrderedPostings op WHERE op.[Rank] = 1 AND op.Id = #Posting.Id)
		
	-- Rank the employer based on the sort order
    INSERT INTO #RankedEmployer
    SELECT 
		p.LensPostingId,
		p.JobTitle,
		e.EmployerName,
		p.PostingDate,
		e.PostingCount
	FROM 
		#Employer e
	INNER JOIN
		#Posting p ON e.EmployerName = p.EmployerName
	ORDER BY
		e.EmployerName DESC, e.PostingCount DESC
            
    SELECT 
		*
    FROM 
		#RankedEmployer
    WHERE
		Id > @FirstRec AND Id < @LastRec
		
END




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByJobTitleAsc]'
GO




-- =============================================
-- Author:		Andy Jones
-- Create date: 27 November 2013
-- Description:	Gets spidered employers with good matches ordered by Job Title Asc
-- =============================================
ALTER PROCEDURE [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByJobTitleAsc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumNumberOfPostings int,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    CREATE TABLE #PersonPostingMatchToIgnore
    (
		PersonPostingMatchId bigint
	)
      
    CREATE TABLE #PersonPostingMatch
	(
		Id bigint,
        PostingId bigint,
        PersonId bigint
	)
      
    CREATE TABLE #PostingPersonMatchCount
    (
		PostingId bigint,
        MatchCount int
    )
      
    CREATE TABLE #Posting
    (
		Id bigint,
        LensPostingId nvarchar(150),
        JobTitle nvarchar(255),
        EmployerName nvarchar(255),
        PostingDate datetime
    )       
    
    CREATE TABLE #Employer
    (
		EmployerName nvarchar(255),
        PostingCount int
	)
      
    CREATE TABLE #RankedEmployer
    (
		Id int IDENTITY PRIMARY KEY,
        LensPostingId nvarchar(150),
        JobTitle nvarchar(255),
        EmployerName nvarchar(255),
        PostingDate datetime,
        NumberOfPostings int
	)      
    
    DECLARE @FirstRec int
    DECLARE @LastRec int
            
    SET @FirstRec = (@PageNumber - 1) * @PageSize
    SET @LastRec = (@PageNumber * @PageSize + 1)
      
    -- Populate the Postings
    INSERT INTO #Posting
	SELECT 
		p.Id,
        p.LensPostingId,
        p.JobTitle,
        p.EmployerName,
        p.CreatedOn
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
	WHERE 
		CreatedOn > @MinimumPostingDate
		AND OriginId = '999'
		AND (EmployerName LIKE '%' + IsNull(@EmployerName, '') + '%')
      
       		
	-- Populate the matches to ignore for this user
    INSERT #PersonPostingMatchToIgnore
    SELECT 
		ppm.Id
    FROM
		[Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK)
        INNER JOIN [Data.Application.PersonPostingMatch] ppm WITH (NOLOCK) ON ppmti.PostingId = ppm.PostingId AND ppmti.PersonId = ppm.PersonId
    WHERE 
        EXISTS (SELECT Id FROM #Posting WHERE #Posting.Id = ppmti.PostingId)
        AND ppmti.UserId = @UserID
            
    -- Populate the matches
    INSERT INTO #PersonPostingMatch
    SELECT 
        Id,
        PostingId,
        PersonId
    FROM 
		[Data.Application.PersonPostingMatch] WITH (NOLOCK)
    WHERE
		EXISTS (SELECT Id FROM #Posting WHERE #Posting.Id = PostingId)
            
    -- Remove ignore matches from matches
    DELETE FROM #PersonPostingMatch
    WHERE
		EXISTS (SELECT PersonPostingMatchId FROM #PersonPostingMatchToIgnore WHERE #PersonPostingMatchToIgnore.PersonPostingMatchId = Id)
      
    -- Populate the match counts
    INSERT INTO #PostingPersonMatchCount
    SELECT 
		PostingId, 
        COUNT(Id) 
    FROM 
        #PersonPostingMatch
    GROUP BY
        PostingId
            
    --Populate the employer
    INSERT INTO #Employer
    SELECT 
		p.EmployerName,
        COUNT(Id)
    FROM
		#Posting p
        INNER JOIN #PostingPersonMatchCount ppmc ON p.Id = ppmc.PostingId
	GROUP BY 
		EmployerName
    HAVING 
		COUNT(Id) >= @MinimumNumberOfPostings
		
	SELECT @RowCount = @@ROWCOUNT
		
	-- Delete postings with no matches
	DELETE FROM #Posting 
	WHERE 
		NOT EXISTS (SELECT PostingId FROM #PostingPersonMatchCount WHERE PostingId = Id);
	
	-- Remove non-top postings based on order
	WITH OrderedPostings AS
	(
		SELECT  
			Id,
			RANK() OVER (PARTITION BY EmployerName ORDER BY JobTitle, PostingDate DESC, Id) AS [Rank]
		FROM 
			#Posting 
 	) 
	DELETE FROM #Posting
	WHERE
		NOT EXISTS (SELECT Id FROM OrderedPostings op WHERE op.[Rank] = 1 AND op.Id = #Posting.Id)
		
	-- Rank the employer based on the sort order
    INSERT INTO #RankedEmployer
    SELECT 
		p.LensPostingId,
		p.JobTitle,
		e.EmployerName,
		p.PostingDate,
		e.PostingCount
	FROM 
		#Employer e
	INNER JOIN
		#Posting p ON e.EmployerName = p.EmployerName
	ORDER BY
		p.JobTitle, e.EmployerName, e.PostingCount DESC
            
    SELECT 
		*
    FROM 
		#RankedEmployer
    WHERE
		Id > @FirstRec AND Id < @LastRec		
END




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByJobTitleDesc]'
GO




-- =============================================
-- Author:		Andy Jones
-- Create date: 27 November 2013
-- Description:	Gets spidered employers with good matches ordered by Job Title Desc
-- =============================================
ALTER PROCEDURE [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByJobTitleDesc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumNumberOfPostings int,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    CREATE TABLE #PersonPostingMatchToIgnore
    (
		PersonPostingMatchId bigint
	)
      
    CREATE TABLE #PersonPostingMatch
	(
		Id bigint,
        PostingId bigint,
        PersonId bigint
	)
      
    CREATE TABLE #PostingPersonMatchCount
    (
		PostingId bigint,
        MatchCount int
    )
      
    CREATE TABLE #Posting
    (
		Id bigint,
        LensPostingId nvarchar(150),
        JobTitle nvarchar(255),
        EmployerName nvarchar(255),
        PostingDate datetime
    )       
    
    CREATE TABLE #Employer
    (
		EmployerName nvarchar(255),
        PostingCount int
	)
      
    CREATE TABLE #RankedEmployer
    (
		Id int IDENTITY PRIMARY KEY,
        LensPostingId nvarchar(150),
        JobTitle nvarchar(255),
        EmployerName nvarchar(255),
        PostingDate datetime,
        NumberOfPostings int
	)      
    
    DECLARE @FirstRec int
    DECLARE @LastRec int
            
    SET @FirstRec = (@PageNumber - 1) * @PageSize
    SET @LastRec = (@PageNumber * @PageSize + 1)
      
    -- Populate the Postings
    INSERT INTO #Posting
	SELECT 
		p.Id,
        p.LensPostingId,
        p.JobTitle,
        p.EmployerName,
        p.CreatedOn	
    FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
	WHERE 
		CreatedOn > @MinimumPostingDate
		AND OriginId = '999'
		AND (EmployerName LIKE '%' + IsNull(@EmployerName, '') + '%')
      
       		
	-- Populate the matches to ignore for this user
    INSERT #PersonPostingMatchToIgnore
    SELECT 
		ppm.Id
    FROM
		[Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK)
        INNER JOIN [Data.Application.PersonPostingMatch] ppm WITH (NOLOCK) ON ppmti.PostingId = ppm.PostingId AND ppmti.PersonId = ppm.PersonId
    WHERE 
        EXISTS (SELECT Id FROM #Posting WHERE #Posting.Id = ppmti.PostingId)
        AND ppmti.UserId = @UserID
            
    -- Populate the matches
    INSERT INTO #PersonPostingMatch
    SELECT 
        Id,
        PostingId,
        PersonId
    FROM 
		[Data.Application.PersonPostingMatch] WITH (NOLOCK)
    WHERE
		EXISTS (SELECT Id FROM #Posting WHERE #Posting.Id = PostingId)
            
    -- Remove ignore matches from matches
    DELETE FROM #PersonPostingMatch
    WHERE
		EXISTS (SELECT PersonPostingMatchId FROM #PersonPostingMatchToIgnore WHERE #PersonPostingMatchToIgnore.PersonPostingMatchId = Id)
      
    -- Populate the match counts
    INSERT INTO #PostingPersonMatchCount
    SELECT 
		PostingId, 
        COUNT(Id) 
    FROM 
        #PersonPostingMatch
    GROUP BY
        PostingId
            
    --Populate the employer
    INSERT INTO #Employer
    SELECT 
		p.EmployerName,
        COUNT(Id)
    FROM
		#Posting p
        INNER JOIN #PostingPersonMatchCount ppmc ON p.Id = ppmc.PostingId
	GROUP BY 
		EmployerName
    HAVING 
		COUNT(Id) >= @MinimumNumberOfPostings
		
	SELECT @RowCount = @@ROWCOUNT
		
	-- Delete postings with no matches
	DELETE FROM #Posting 
	WHERE 
		NOT EXISTS (SELECT PostingId FROM #PostingPersonMatchCount WHERE PostingId = Id);
	
	-- Remove non-top postings based on order
	WITH OrderedPostings AS
	(
		SELECT  
			Id,
			RANK() OVER (PARTITION BY EmployerName ORDER BY JobTitle DESC, PostingDate DESC, Id) AS [Rank]
		FROM 
			#Posting 
 	) 
	DELETE FROM #Posting
	WHERE
		NOT EXISTS (SELECT Id FROM OrderedPostings op WHERE op.[Rank] = 1 AND op.Id = #Posting.Id)
		
	-- Rank the employer based on the sort order
    INSERT INTO #RankedEmployer
    SELECT 
		p.LensPostingId,
		p.JobTitle,
		e.EmployerName,
		p.PostingDate,
		e.PostingCount
	FROM 
		#Employer e
	INNER JOIN
		#Posting p ON e.EmployerName = p.EmployerName
	ORDER BY
		p.JobTitle DESC, e.EmployerName, e.PostingCount DESC
            
    SELECT 
		*
    FROM 
		#RankedEmployer
    WHERE
		Id > @FirstRec AND Id < @LastRec	
		
END




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByPostingDateDesc]'
GO



-- =============================================
-- Author:		Andy Jones
-- Create date: 27 November 2013
-- Description:	Gets spidered employers with good matches ordered by Posting Date Desc
-- =============================================
ALTER PROCEDURE [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByPostingDateDesc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumNumberOfPostings int,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    CREATE TABLE #PersonPostingMatchToIgnore
    (
		PersonPostingMatchId bigint
	)
      
    CREATE TABLE #PersonPostingMatch
	(
		Id bigint,
        PostingId bigint,
        PersonId bigint
	)
      
    CREATE TABLE #PostingPersonMatchCount
    (
		PostingId bigint,
        MatchCount int
    )
      
    CREATE TABLE #Posting
    (
		Id bigint,
        LensPostingId nvarchar(150),
        JobTitle nvarchar(255),
        EmployerName nvarchar(255),
        PostingDate datetime
    )       
    
    CREATE TABLE #Employer
    (
		EmployerName nvarchar(255),
        PostingCount int
	)
      
    CREATE TABLE #RankedEmployer
    (
		Id int IDENTITY PRIMARY KEY,
        LensPostingId nvarchar(150),
        JobTitle nvarchar(255),
        EmployerName nvarchar(255),
        PostingDate datetime,
        NumberOfPostings int
	)      
    
    DECLARE @FirstRec int
    DECLARE @LastRec int
            
    SET @FirstRec = (@PageNumber - 1) * @PageSize
    SET @LastRec = (@PageNumber * @PageSize + 1)
      
    -- Populate the Postings
    INSERT INTO #Posting
	SELECT 
		p.Id,
        p.LensPostingId,
        p.JobTitle,
        p.EmployerName,
        p.CreatedOn
    FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
	WHERE 
		CreatedOn > @MinimumPostingDate
		AND OriginId = '999'
		AND (EmployerName LIKE '%' + IsNull(@EmployerName, '') + '%')
      
       		
	-- Populate the matches to ignore for this user
    INSERT #PersonPostingMatchToIgnore
    SELECT 
		ppm.Id
    FROM
		[Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK)
        INNER JOIN [Data.Application.PersonPostingMatch] ppm WITH (NOLOCK) ON ppmti.PostingId = ppm.PostingId AND ppmti.PersonId = ppm.PersonId
    WHERE 
        EXISTS (SELECT Id FROM #Posting WHERE #Posting.Id = ppmti.PostingId)
        AND ppmti.UserId = @UserID
            
    -- Populate the matches
    INSERT INTO #PersonPostingMatch
    SELECT 
        Id,
        PostingId,
        PersonId
    FROM 
		[Data.Application.PersonPostingMatch] WITH (NOLOCK)
    WHERE
		EXISTS (SELECT Id FROM #Posting WHERE #Posting.Id = PostingId)
            
    -- Remove ignore matches from matches
    DELETE FROM #PersonPostingMatch
    WHERE
		EXISTS (SELECT PersonPostingMatchId FROM #PersonPostingMatchToIgnore WHERE #PersonPostingMatchToIgnore.PersonPostingMatchId = Id)
      
    -- Populate the match counts
    INSERT INTO #PostingPersonMatchCount
    SELECT 
		PostingId, 
        COUNT(Id) 
    FROM 
        #PersonPostingMatch
    GROUP BY
        PostingId
            
    --Populate the employer
    INSERT INTO #Employer
    SELECT 
		p.EmployerName,
        COUNT(Id)
    FROM
		#Posting p
        INNER JOIN #PostingPersonMatchCount ppmc ON p.Id = ppmc.PostingId
	GROUP BY 
		EmployerName
    HAVING 
		COUNT(Id) >= @MinimumNumberOfPostings
		
	SELECT @RowCount = @@ROWCOUNT
		
	-- Delete postings with no matches
	DELETE FROM #Posting 
	WHERE 
		NOT EXISTS (SELECT PostingId FROM #PostingPersonMatchCount WHERE PostingId = Id);
	
	-- Remove non-top postings based on order
	WITH OrderedPostings AS
	(
		SELECT  
			Id,
			RANK() OVER (PARTITION BY EmployerName ORDER BY PostingDate DESC, JobTitle, Id) AS [Rank]
		FROM 
			#Posting 
 	) 
	DELETE FROM #Posting
	WHERE
		NOT EXISTS (SELECT Id FROM OrderedPostings op WHERE op.[Rank] = 1 AND op.Id = #Posting.Id)
		
	-- Rank the employer based on the sort order
    INSERT INTO #RankedEmployer
    SELECT 
		p.LensPostingId,
		p.JobTitle,
		e.EmployerName,
		p.PostingDate,
		e.PostingCount
	FROM 
		#Employer e
	INNER JOIN
		#Posting p ON e.EmployerName = p.EmployerName
	ORDER BY
		p.PostingDate DESC, e.EmployerName, e.PostingCount DESC
            
    SELECT 
		*
    FROM 
		#RankedEmployer
    WHERE
		Id > @FirstRec AND Id < @LastRec
		
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByPostingDateAsc]'
GO




-- =============================================
-- Author:		Andy Jones
-- Create date: 27 November 2013
-- Description:	Gets spidered employers with good matches ordered by Posting Date Asc
-- =============================================
ALTER PROCEDURE [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByPostingDateAsc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumNumberOfPostings int,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    CREATE TABLE #PersonPostingMatchToIgnore
    (
		PersonPostingMatchId bigint
	)
      
    CREATE TABLE #PersonPostingMatch
	(
		Id bigint,
        PostingId bigint,
        PersonId bigint
	)
      
    CREATE TABLE #PostingPersonMatchCount
    (
		PostingId bigint,
        MatchCount int
    )
      
    CREATE TABLE #Posting
    (
		Id bigint,
        LensPostingId nvarchar(150),
        JobTitle nvarchar(255),
        EmployerName nvarchar(255),
        PostingDate datetime
    )       
    
    CREATE TABLE #Employer
    (
		EmployerName nvarchar(255),
        PostingCount int
	)
      
    CREATE TABLE #RankedEmployer
    (
		Id int IDENTITY PRIMARY KEY,
        LensPostingId nvarchar(150),
        JobTitle nvarchar(255),
        EmployerName nvarchar(255),
        PostingDate datetime,
        NumberOfPostings int
	)      
    
    DECLARE @FirstRec int
    DECLARE @LastRec int
            
    SET @FirstRec = (@PageNumber - 1) * @PageSize
    SET @LastRec = (@PageNumber * @PageSize + 1)
      
    -- Populate the Postings
    INSERT INTO #Posting
	SELECT 
		p.Id,
        p.LensPostingId,
        p.JobTitle,
        p.EmployerName,
        p.CreatedOn        
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
	WHERE 
		CreatedOn > @MinimumPostingDate
		AND OriginId = '999'
		AND (EmployerName LIKE '%' + IsNull(@EmployerName, '') + '%')
      
       		
	-- Populate the matches to ignore for this user
    INSERT #PersonPostingMatchToIgnore
    SELECT 
		ppm.Id
    FROM
		[Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK)
        INNER JOIN [Data.Application.PersonPostingMatch] ppm WITH (NOLOCK) ON ppmti.PostingId = ppm.PostingId AND ppmti.PersonId = ppm.PersonId
    WHERE 
        EXISTS (SELECT Id FROM #Posting WHERE #Posting.Id = ppmti.PostingId)
        AND ppmti.UserId = @UserID
            
    -- Populate the matches
    INSERT INTO #PersonPostingMatch
    SELECT 
        Id,
        PostingId,
        PersonId
    FROM 
		[Data.Application.PersonPostingMatch] WITH (NOLOCK)
    WHERE
		EXISTS (SELECT Id FROM #Posting WHERE #Posting.Id = PostingId)
            
    -- Remove ignore matches from matches
    DELETE FROM #PersonPostingMatch
    WHERE
		EXISTS (SELECT PersonPostingMatchId FROM #PersonPostingMatchToIgnore WHERE #PersonPostingMatchToIgnore.PersonPostingMatchId = Id)
      
    -- Populate the match counts
    INSERT INTO #PostingPersonMatchCount
    SELECT 
		PostingId, 
        COUNT(Id) 
    FROM 
        #PersonPostingMatch
    GROUP BY
        PostingId
            
    --Populate the employer
    INSERT INTO #Employer
    SELECT 
		p.EmployerName,
        COUNT(Id)
    FROM
		#Posting p
        INNER JOIN #PostingPersonMatchCount ppmc ON p.Id = ppmc.PostingId
	GROUP BY 
		EmployerName
    HAVING 
		COUNT(Id) >= @MinimumNumberOfPostings
		
	SELECT @RowCount = @@ROWCOUNT
		
	-- Delete postings with no matches
	DELETE FROM #Posting 
	WHERE 
		NOT EXISTS (SELECT PostingId FROM #PostingPersonMatchCount WHERE PostingId = Id);
	
	-- Remove non-top postings based on order
	WITH OrderedPostings AS
	(
		SELECT  
			Id,
			RANK() OVER (PARTITION BY EmployerName ORDER BY PostingDate, JobTitle, Id) AS [Rank]
		FROM 
			#Posting 
 	) 
	DELETE FROM #Posting
	WHERE
		NOT EXISTS (SELECT Id FROM OrderedPostings op WHERE op.[Rank] = 1 AND op.Id = #Posting.Id)
 	
	-- Rank the employer based on the sort order
    INSERT INTO #RankedEmployer
    SELECT 
		p.LensPostingId,
		p.JobTitle,
		e.EmployerName,
		p.PostingDate,
		e.PostingCount
	FROM 
		#Employer e
	INNER JOIN
		#Posting p ON e.EmployerName = p.EmployerName
	ORDER BY
		p.PostingDate, e.EmployerName, e.PostingCount DESC
            
    SELECT 
		*
    FROM 
		#RankedEmployer
    WHERE
		Id > @FirstRec AND Id < @LastRec
		
END




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO