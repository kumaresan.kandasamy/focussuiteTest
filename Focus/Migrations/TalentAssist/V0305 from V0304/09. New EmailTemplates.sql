IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 29)
BEGIN
	INSERT INTO [Config.EmailTemplate]
	(
		EmailTemplateType, 
		[Subject], 
		Body, 
		Salutation, 
		Recipient
	)
	VALUES 
	(
		29, 
		'Your Veteran Priority of Service Job Alert', 
		'A new job has been posted that is a good match against your resume. 

To access the job posting, click on the following link:

#JOBLINKURL#

Ensure you are logged into #FOCUSCAREER# if you don''t see the posting straight away.', 
		'Dear #RECIPIENTNAME#', 
		NULL
	)
END

IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 30)
BEGIN
	INSERT INTO [Config.EmailTemplate]
	(
		EmailTemplateType, 
		[Subject], 
		Body, 
		Salutation, 
		Recipient
	)
	VALUES 
	(
		30, 
		'Job seeker screening notification, #EMPLOYERNAME#, #JOBTITLE#', 
		'A new job has been posted that requires your staff to screen all job seekers. 

To view the posting within #FOCUSASSIST#, click on the following link:

#JOBLINKURL#
', 
		'', 
		NULL
	)
END


IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 31)
BEGIN
	INSERT INTO [Config.EmailTemplate]
	(
		EmailTemplateType, 
		[Subject], 
		Body, 
		Salutation, 
		Recipient
	)
	VALUES 
	(
		31, 
		'New applicant for your #JOBTITLE# posting', 
		'An application has been received via #FOCUSCAREER# against your posting for #JOBTITLE#

In accordance with the interview contact preference selected on this posting, please find a copy of the job seeker''s resume attached for your perusal. 

To view your posting within #FOCUSTALENT#, please click on the following link:

#JOBLINKURL#
', 
		'Dear #RECIPIENTNAME#', 
		NULL
	)
END
GO