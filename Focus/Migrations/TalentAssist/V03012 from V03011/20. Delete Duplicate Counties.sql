DECLARE @Counties TABLE
(
	[OldId] BIGINT,
	[NewId] BIGINT
)
INSERT INTO @Counties ([OldId], [NewId])
SELECT 
	SUM(CASE [Key] WHEN 'County.CapeGirardeauMO' THEN Id END),
	SUM(CASE [Key] WHEN 'County.Cape GirardeauMO' THEN Id END)
FROM [Config.CodeItem] 
WHERE [Key] IN ('County.CapeGirardeauMO', 'County.Cape GirardeauMO')
UNION
SELECT 
	SUM(CASE [Key] WHEN 'County.CityofBristolVA' THEN Id END),
	SUM(CASE [Key] WHEN 'County.City of BristolVA' THEN Id END)
FROM [Config.CodeItem] 
WHERE [Key] IN ('County.CityofBristolVA', 'County.City of BristolVA')
UNION
SELECT 
	SUM(CASE [Key] WHEN 'County.CityofNortonVA' THEN Id END),
	SUM(CASE [Key] WHEN 'County.City of NortonVA' THEN Id END)
FROM [Config.CodeItem] 
WHERE [Key] IN ('County.CityofNortonVA', 'County.City of NortonVA')
UNION
SELECT 
	SUM(CASE [Key] WHEN 'County.NewMadridMO' THEN Id END),
	SUM(CASE [Key] WHEN 'County.New MadridMO' THEN Id END)
FROM [Config.CodeItem] 
WHERE [Key] IN ('County.NewMadridMO', 'County.New MadridMO')

--BEGIN TRANSACTION

UPDATE 
	A
SET
	CountyId = C.[NewId]
FROM
	[Data.Application.PersonAddress] A
INNER JOIN @Counties C
	ON C.[OldId] = A.CountyId
WHERE
	C.[OldId] IS NOT NULL
	AND C.[NewId] IS NOT NULL

UPDATE 
	A
SET
	CountyId = C.[NewId]
FROM
	[Data.Application.Resume] A
INNER JOIN @Counties C
	ON C.[OldId] = A.CountyId
WHERE
	C.[OldId] IS NOT NULL
	AND C.[NewId] IS NOT NULL

UPDATE 
	A
SET
	CountyId = C.[NewId]
FROM
	[Data.Application.Office] A
INNER JOIN @Counties C
	ON C.[OldId] = A.CountyId
WHERE
	C.[OldId] IS NOT NULL
	AND C.[NewId] IS NOT NULL

UPDATE 
	A
SET
	CountyId = C.[NewId]
FROM
	[Report.JobSeeker] A
INNER JOIN @Counties C
	ON C.[OldId] = A.CountyId
WHERE
	C.[OldId] IS NOT NULL
	AND C.[NewId] IS NOT NULL

UPDATE 
	A
SET
	CountyId = C.[NewId]
FROM
	[Report.Employer] A
INNER JOIN @Counties C
	ON C.[OldId] = A.CountyId
WHERE
	C.[OldId] IS NOT NULL
	AND C.[NewId] IS NOT NULL

UPDATE 
	A
SET
	CountyId = C.[NewId]
FROM
	[Data.Application.BusinessUnitAddress] A
INNER JOIN @Counties C
	ON C.[OldId] = A.CountyId
WHERE
	C.[OldId] IS NOT NULL
	AND C.[NewId] IS NOT NULL

UPDATE 
	A
SET
	CountyId = C.[NewId]
FROM
	[Report.JobOrder] A
INNER JOIN @Counties C
	ON C.[OldId] = A.CountyId
WHERE
	C.[OldId] IS NOT NULL
	AND C.[NewId] IS NOT NULL

UPDATE 
	A
SET
	CountyId = C.[NewId]
FROM
	[Data.Application.EmployerAddress] A
INNER JOIN @Counties C
	ON C.[OldId] = A.CountyId
WHERE
	C.[OldId] IS NOT NULL
	AND C.[NewId] IS NOT NULL

UPDATE 
	A
SET
	CountyId = C.[NewId]
FROM
	[Data.Application.JobAddress] A
INNER JOIN @Counties C
	ON C.[OldId] = A.CountyId
WHERE
	C.[OldId] IS NOT NULL
	AND C.[NewId] IS NOT NULL

DELETE 
	CGI
FROM 
	[Config.CodeGroupItem] CGI
INNER JOIN @Counties C
	ON C.[OldId] = CGI.CodeItemId
WHERE
	C.[OldId] IS NOT NULL

DELETE 
	CI
FROM 
	[Config.CodeItem] CI
INNER JOIN @Counties C
	ON C.[OldId] = CI.Id
WHERE
	C.[OldId] IS NOT NULL

--ROLLBACK TRANSACTION

