﻿BEGIN TRANSACTION

DECLARE @NextId bigint
DECLARE @EndOfBlockNextId bigint
DECLARE @IdsRequired bigint

SELECT @IdsRequired = 5

UPDATE KeyTable SET NextId = NextId + @IdsRequired

SELECT @EndOfBlockNextId = NextId FROM KeyTable

SELECT @NextId = @EndOfBlockNextId - @IdsRequired

INSERT INTO EmailTemplate (Id, EmailTemplateType, [Subject], Body, Salutation, Recipient)
VALUES (@NextId, 13, 'Position about to expire', 'The following job order will expire in {4} days.

To extend the closing date on your posting, please log in to your OK Job Match account and run the Refresh action found against the job order.

Job order title; {0}
Job order ID; {1}
Job order closing date; {2}
DBA name; {3}', 'Dear #RECIPIENTNAME#', Null)

SELECT @NextId = @NextId + 1

INSERT INTO EmailTemplate (Id, EmailTemplateType, [Subject], Body, Salutation, Recipient)
VALUES (@NextId, 19, 'Thankyou for your account request', 'We have reviewed your request to register with #FOCUSTALENT# but must place your request on temporary hold for the following reasons:



If you have any questions, please feel free to contact me at #SENDERPHONENUMBER# or #SENDEREMAILADDRESS#.

Regards,  #SENDERNAME#', 'Dear #RECIPIENTNAME#', Null)


COMMIT TRANSACTION
