

CREATE VIEW [dbo].[BusinessUnitPlacementsView]
AS
SELECT     dbo.BusinessUnit.Id AS BuId, dbo.Candidate.Id AS JobSeekerId, dbo.BusinessUnit.Name AS BuName, dbo.Job.JobTitle, dbo.CandidateApplication.ApplicationStatus, 
                      dbo.Candidate.FirstName, dbo.Candidate.LastName
FROM         dbo.BusinessUnit WITH (NOLOCK) INNER JOIN
                      dbo.Job WITH (NOLOCK) ON dbo.BusinessUnit.Id = dbo.Job.BusinessUnitId INNER JOIN
                      dbo.CandidateApplication WITH (NOLOCK) ON dbo.Job.Id = dbo.CandidateApplication.JobId INNER JOIN
                      dbo.Candidate WITH (NOLOCK) ON dbo.CandidateApplication.CandidateId = dbo.Candidate.Id
WHERE     (dbo.CandidateApplication.ApplicationStatus = 12)

GO