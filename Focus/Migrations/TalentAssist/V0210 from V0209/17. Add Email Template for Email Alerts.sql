IF NOT EXISTS(SELECT TOP 1 1 FROM dbo.EmailTemplate WHERE EmailTemplateType = 21)
BEGIN
	BEGIN TRANSACTION

	DECLARE @NextId bigint
	DECLARE @EndOfBlockNextId bigint
	DECLARE @IdsRequired bigint

	SELECT @IdsRequired = 1

	UPDATE KeyTable SET NextId = NextId + @IdsRequired

	SELECT @EndOfBlockNextId = NextId FROM KeyTable

	SELECT @NextId = @EndOfBlockNextId - @IdsRequired

	INSERT INTO dbo.EmailTemplate 
	(
		Id, 
		EmailTemplateType, 
		[Subject], 
		Body, 
		Salutation, 
		Recipient
	)
	VALUES 
	(
		@NextId, 
		21, 
		'Sent from #FOCUSASSIST# - New #APPROVALQUEUENAME# to approve', 
		'There are #APPROVALCOUNT# records for you to approve in #FOCUSASSIST#.

You have received this email because an alert has been set up in Focus/Assist to send alerts to this email address.
', 
		'Dear #RECIPIENTNAME#', 
		NULL
	)

	COMMIT TRANSACTION
END