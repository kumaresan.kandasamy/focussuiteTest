IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAlert]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[UserAlert]
	(
		[Id] [bigint] NOT NULL,
		[NewReferralRequests] [bit] NOT NULL DEFAULT(0),
		[EmployerAccountRequests] [bit] NOT NULL DEFAULT(0),
		[NewJobPostings] [bit] NOT NULL DEFAULT(0),
		[CreatedOn] [datetime] NOT NULL,
		[UpdatedOn] [datetime] NOT NULL,
		[UserId] [bigint] NOT NULL,
		[Frequency] [nchar](1) NOT NULL,
		PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)
	)
END
GO
