﻿CREATE VIEW [dbo].[HiringManagerActivityView]
AS
SELECT ebu.Id AS EmployeeBuId, ae.UserId, at.Name AS ActionName, p.FirstName AS EmployeeFirstName, p.LastName AS EmployeeLastName, ae.ActionedOn
FROM ActionType at WITH (NOLOCK)
INNER JOIN ActionEvent ae WITH (NOLOCK) ON at.Id = ae.ActionTypeId
INNER JOIN Employee e WITH (NOLOCK) ON e.Id = ae.EntityId
INNER JOIN Person p WITH (NOLOCK) ON e.PersonId = p.Id
INNER JOIN EmployeeBusinessUnit ebu WITH (NOLOCK) ON ebu.EmployeeId = e.Id 
WHERE     (at.Name = 'AssignActivityToEmployee')       
UNION All
SELECT ebu.Id, ae.UserId, at.Name, p.[FirstName], p.LastName, ae.ActionedOn
FROM EmployeeBusinessUnit ebu WITH (NOLOCK)
INNER JOIN Employee e WITH (NOLOCK) ON e.Id = ebu.EmployeeId 
INNER JOIN Person p WITH (NOLOCK) ON e.PersonId = p.Id
INNER JOIN [User] u WITH (NOLOCK) ON p.Id = u.PersonId 
INNER JOIN ActionEvent ae WITH (NOLOCK) ON u.Id = ae.UserId 
INNER JOIN ActionType at WITH (NOLOCK) ON at.Id = ae.ActionTypeId 
WHERE     (at.Name = 'PostJob') OR
          (at.Name = 'SaveJob') OR
          (at.Name = 'HoldJob') OR
          (at.Name = 'CloseJob') OR
          (at.Name = 'RefreshJob') OR
          (at.Name = 'ReactivateJob') OR
          (at.Name = 'CreateJob') 

GO