
CREATE VIEW [dbo].[BusinessUnitActivityView]
AS
SELECT     dbo.BusinessUnit.EmployerId, dbo.BusinessUnit.Id AS BUId, dbo.[User].UserName, dbo.BusinessUnit.Name AS BUName, dbo.ActionType.Name AS ActionName, 
                      dbo.ActionEvent.ActionedOn, dbo.ActionEvent.Id AS ActionId, dbo.Job.Id as JobId, dbo.Job.JobTitle
                      
FROM         dbo.ActionEvent
						INNER JOIN dbo.ActionType WITH (NOLOCK) ON dbo.ActionEvent.ActionTypeId = dbo.ActionType.Id
						INNER JOIN dbo.Job WITH (NOLOCK) ON dbo.Job.Id = dbo.ActionEvent.EntityId 
						INNER JOIN dbo.[User] WITH (NOLOCK) ON dbo.[User].Id = dbo.ActionEvent.UserId
						INNER JOIN dbo.BusinessUnit WITH (NOLOCK) ON dbo.BusinessUnit.Id = dbo.Job.BusinessUnitId												    
                      
WHERE     (dbo.ActionType.Name = 'PostJob') OR
                      (dbo.ActionType.Name = 'SaveJob') OR
                      (dbo.ActionType.Name = 'HoldJob') OR
                      (dbo.ActionType.Name = 'CloseJob') OR
                      (dbo.ActionType.Name = 'RefreshJob') OR
                      (dbo.ActionType.Name = 'ReactivateJob') OR
                      (dbo.ActionType.Name = 'CreateJob')

GO
