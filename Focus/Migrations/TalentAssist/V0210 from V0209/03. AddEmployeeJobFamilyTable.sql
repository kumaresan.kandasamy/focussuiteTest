-- This file contains a log of the SQL statements that LightSpeed ran
-- against your database.  It is specific to the current state of the
-- LightSpeed model and the previous state of your database.  Review the
-- script carefully if you plan to apply it to another database.  Please
-- note that due to scoping issues you may not be able to run all logged
-- SQL as a single batch.

-- Add table EmployeeJobFamily to the database
CREATE TABLE [EmployeeJobFamily] (Id BIGINT NOT NULL PRIMARY KEY ,
[JobFamilyId] BIGINT NOT NULL DEFAULT '',
[EmployeeId] BIGINT NOT NULL DEFAULT 0,
FOREIGN KEY (EmployeeId) REFERENCES [Employee](Id));


