CREATE VIEW [dbo].[ActiveUserAlertView]
AS
SELECT 
	UA.Id,
	UA.UserId,
	UA.NewReferralRequests,
	UA.EmployerAccountRequests,
	UA.NewJobPostings,
	UA.Frequency,
	P.FirstName,
	P.LastName,
	P.EmailAddress
FROM 
	dbo.UserAlert UA WITH (NOLOCK)
INNER JOIN dbo.[User] U WITH (NOLOCK)
	ON U.Id = UA.UserId
INNER JOIN dbo.Person P WITH (NOLOCK)
	ON P.Id = U.PersonId
WHERE 
	UA.NewReferralRequests = 1
	OR UA.EmployerAccountRequests = 1
	OR UA.NewJobPostings = 1


