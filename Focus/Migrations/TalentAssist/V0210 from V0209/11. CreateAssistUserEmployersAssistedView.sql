
CREATE VIEW [dbo].[AssistUserEmployersAssistedView]
AS

SELECT   dbo.[User].Id AS UserId,
					ISNULL(PersonJob.FirstName + ' ' + PersonJob.LastName,ISNULL(PersonAction.FirstName + ' ' + PersonAction.LastName, ISNULL(PersonSearch.FirstName + ' ' + PersonSearch.LastName, ISNULL(PersonCompany .FirstName + ' ' + PersonCompany.LastName, ISNULL(PersonLogo .FirstName + ' ' + PersonLogo.LastName, PersonDesc .FirstName + ' ' + PersonDesc.LastName))))) AS Name,
				  ISNULL(BUDesc.Name,ISNULL(BULogo.Name ,ISNULL(BUAction.Name, ISNULL(BUJob.Name, 'All registered Business Units')))) AS BusinessUnitName,
				  dbo.ActionType.Name AS ActionName,
				  dbo.ActionEvent.ActionedOn,
				  dbo.Job.JobTitle,
				  dbo.SavedSearch.Name AS SavedSearchName,			
				  dbo.ActionEvent.AdditionalDetails

FROM    dbo.[ActionEvent] WITH (NOLOCK)
INNER JOIN dbo.[User] WITH (NOLOCK) ON dbo.[User].Id = dbo.ActionEvent.UserId

-- needed for ActionName
INNER JOIN dbo.ActionType WITH (NOLOCK) ON dbo.ActionEvent.ActionTypeId = dbo.ActionType.Id

-- Job actions have JobId as EntityId - Also Invite Candidate To Apply
LEFT OUTER JOIN dbo.Job WITH (NOLOCK) ON dbo.Job.Id	= dbo.ActionEvent.EntityId
LEFT OUTER JOIN dbo.BusinessUnit AS BUJob WITH (NOLOCK) ON BUJob.Id = dbo.Job.BusinessUnitId
LEFT OUTER JOIN dbo.Employee AS EmployeeJob WITH (NOLOCK) ON EmployeeJob.Id = dbo.Job.EmployeeId
LEFT OUTER JOIN dbo.Person AS PersonJob WITH (NOLOCK) ON PersonJob.Id = EmployeeJob.PersonId

--Save BusinessUnitLogo has BusinessUnitLogo as EntityId
LEFT OUTER JOIN dbo.BusinessUnitLogo WITH (NOLOCK) ON dbo.BusinessUnitLogo.Id = dbo.ActionEvent.EntityId
LEFT OUTER JOIN dbo.BusinessUnit AS BULogo WITH (NOLOCK) ON BULogo.Id = dbo.BusinessUnitLogo.BusinessUnitId
LEFT OUTER JOIN dbo.[User] AS UserLogo WITH (NOLOCK) ON UserLogo.Id = dbo.ActionEvent.EntityIdAdditional01
LEFT OUTER JOIN dbo.Person AS PersonLogo WITH (NOLOCK) ON PersonLogo.Id = UserLogo.PersonId

--Save BusinessUnitDescription has BusinessUnitDescription as EntityId
LEFT OUTER JOIN dbo.BusinessUnitDescription WITH (NOLOCK) ON dbo.BusinessUnitDescription.Id = dbo.ActionEvent.EntityId
LEFT OUTER JOIN dbo.BusinessUnit AS BUDesc WITH (NOLOCK) ON BUDesc.Id = dbo.BusinessUnitDescription.BusinessUnitId
LEFT OUTER JOIN dbo.[User] AS UserDesc WITH (NOLOCK) ON UserDesc.Id = dbo.ActionEvent.EntityIdAdditional01
LEFT OUTER JOIN dbo.Person AS PersonDesc WITH (NOLOCK) ON PersonDesc.Id = UserDesc.PersonId

-- Save company info Action has BUId as EntityId and UserId as Additional
LEFT OUTER JOIN dbo.BusinessUnit AS BUAction WITH (NOLOCK) ON BUAction.Id = dbo.ActionEvent.EntityId
LEFT OUTER JOIN dbo.[User] AS UserCompany WITH (NOLOCK) ON UserCompany.Id = dbo.ActionEvent.EntityIdAdditional01
LEFT OUTER JOIN dbo.Person AS PersonCompany WITH (NOLOCK) ON PersonCompany.Id = UserCompany.PersonId

-- SaveContactInfo Action and SendCandidateEmails has UserId as EntityId
LEFT OUTER JOIN dbo.[User] AS UserAction WITH (NOLOCK) ON UserAction.Id = dbo.ActionEvent.EntityId
LEFT OUTER JOIN dbo.Person AS PersonAction WITH (NOLOCK) ON PersonAction.Id = UserAction.PersonId

-- Save Talent Alert has SavedSearchId as EntityId
LEFT OUTER JOIN dbo.SavedSearch WITH (NOLOCK) ON dbo.SavedSearch.Id = dbo.ActionEvent.EntityId
LEFT OUTER JOIN dbo.SavedSearchUser   WITH (NOLOCK) ON dbo.SavedSearch.Id = dbo.SavedSearchUser.SavedSearchId
LEFT OUTER JOIN dbo.[User] AS UserSearch WITH (NOLOCK) ON UserSearch.Id = dbo.SavedSearchUser.UserId
LEFT OUTER JOIN dbo.Person AS PersonSearch WITH (NOLOCK) ON PersonSearch.Id = UserSearch.PersonId


WHERE    ((dbo.ActionType.Name = 'PostJob') OR
                      (dbo.ActionType.Name = 'SaveJob') OR
                      (dbo.ActionType.Name = 'RefreshJob') OR
                      (dbo.ActionType.Name = 'ReactivateJob') OR
                      (dbo.ActionType.Name = 'CloseJob') OR
                      (dbo.ActionType.Name = 'HoldJob') OR                     
                      (dbo.ActionType.Name = 'DuplicateJob') OR   
                      (dbo.ActionType.Name = 'UpdateUser') OR     
                      (dbo.ActionType.Name = 'SaveBusinessUnit') OR   
                      (dbo.ActionType.Name = 'SaveBusinessUnitLogo') OR   
                      (dbo.ActionType.Name = 'SaveBusinessUnitDescription') OR                       
                      (dbo.ActionType.Name = 'SaveCandidateSavedSearch') OR
                      (dbo.ActionType.Name = 'InviteJobSeekerToApply') OR
                      (dbo.ActionType.Name = 'SendCandidateEmails'))


GO
