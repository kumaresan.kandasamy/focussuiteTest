
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[AssistUserActivityView]
AS

SELECT     dbo.[User].Id AS UserId, dbo.ActionType.Name AS ActionName,  dbo.ActionEvent.ActionedOn
FROM         dbo.[User] WITH (NOLOCK) INNER JOIN
                      dbo.ActionEvent WITH (NOLOCK) ON dbo.[User].Id = dbo.ActionEvent.UserId INNER JOIN
                      dbo.ActionType WITH (NOLOCK) ON dbo.ActionEvent.ActionTypeId = dbo.ActionType.Id
WHERE    ((dbo.ActionType.Name = 'PostJob') OR
                      (dbo.ActionType.Name = 'CreateCandidateApplication') OR
                      (dbo.ActionType.Name = 'CloseJob') OR
                      (dbo.ActionType.Name = 'RefreshJob') OR
                      (dbo.ActionType.Name = 'ReactivateJob') OR
                      (dbo.ActionType.Name = 'CreateJob') OR                     
                      (dbo.ActionType.Name = 'EditJob') OR                     
                      (dbo.ActionType.Name = 'HoldJob'))

GO


