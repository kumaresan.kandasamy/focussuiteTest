
ALTER VIEW [dbo].[JobView]
AS
SELECT j.Id, j.EmployerId, e.Name AS EmployerName, j.JobTitle, bu.Name AS BusinessUnitName, j.BusinessUnitId, j.JobStatus, j.PostedOn, j.ClosingOn, j.HeldOn, 
               j.ClosedOn, j.UpdatedOn, ISNULL(cag.ApplicationCount, 0) AS ApplicationCount, j.EmployeeId, j.ApprovalStatus, ISNULL(crg.ReferralCount, 0) AS ReferralCount, 
               j.FederalContractor, j.ForeignLabourCertificationH2A, j.ForeignLabourCertificationH2B, j.ForeignLabourCertificationOther, j.CourtOrderedAffirmativeAction, 
               u.UserName, p.FirstName AS HiringManagerFirstName, p.LastName AS HiringManagerLastName, j.CreatedOn, j.YellowProfanityWords, j.IsConfidential
FROM  dbo.Job AS j WITH (NOLOCK) LEFT OUTER JOIN
               dbo.Employee AS employee WITH (NOLOCK) ON j.EmployeeId = employee.Id LEFT OUTER JOIN
               dbo.Person AS p WITH (NOLOCK) ON employee.PersonId = p.Id LEFT OUTER JOIN
               dbo.[User] AS u WITH (NOLOCK) ON employee.PersonId = u.PersonId LEFT OUTER JOIN
               dbo.Employer AS e WITH (NOLOCK) ON j.EmployerId = e.Id LEFT OUTER JOIN
               dbo.BusinessUnit AS bu WITH (NOLOCK) ON bu.Id = j.BusinessUnitId LEFT OUTER JOIN
                   (SELECT JobId, COUNT(1) AS ApplicationCount
                    FROM   dbo.CandidateApplication AS ca WITH (NOLOCK)
                    WHERE (ApprovalStatus = 2)
                    GROUP BY JobId) AS cag ON cag.JobId = j.Id LEFT OUTER JOIN
                   (SELECT JobId, COUNT(1) AS ReferralCount
                    FROM   dbo.CandidateApplication AS ca WITH (NOLOCK)
                    WHERE (ApprovalStatus = 1)
                    GROUP BY JobId) AS crg ON crg.JobId = j.Id

GO


