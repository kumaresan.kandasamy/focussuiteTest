USE FocusOKJobMatchStage
BEGIN TRAN

DELETE FROM PostingSurvey
DELETE FROM JobAddress
DELETE FROM JobLocation
DELETE FROM JobSpecialRequirement
DELETE FROM JobCertificate
DELETE FROM JobLicence
DELETE FROM CandidateApplication
DELETE FROM JobLanguage
DELETE FROM Job
DELETE FROM JobSkill
DELETE FROM Note
DELETE FROM PostingSurvey
DELETE FROM EmployeeBusinessUnit WHERE EmployeeId != 29452 OR BusinessUnitId IN (29454,29456)

DELETE FROM SavedSearchEmployee
DELETE FROM BusinessUnitLogo WHERE Id NOT IN (78030)
DELETE FROM BusinessUnitAddress WHERE BusinessUnitId NOT IN (SELECT Id FROM BusinessUnit bu WHERE bu.EmployerId IN(13930,29441))
DELETE FROM BusinessUnitDescription WHERE BusinessUnitId NOT IN (SELECT Id FROM BusinessUnit bu WHERE bu.EmployerId IN(13930,29441))
DELETE FROM BusinessUnit WHERE EmployerId NOT IN (13930, 29441) OR Id IN (29454,29456)
DELETE FROM PersonListCandidate
DELETE FROM PersonList
DELETE FROM CandidateNote
DELETE FROM CandidateWorkHistory
DELETE FROM Candidate
DELETE FROM SavedSearchUser
DELETE FROM SavedSearch
DELETE FROM Employee WHERE Id != 29452
DELETE FROM PhoneNumber WHERE PersonId NOT IN (1,17280,17281 )
DELETE FROM MessageText
DELETE FROM DismissedMessage
DELETE FROM [Message]
DELETE FROM UserRole WHERE UserId NOT IN (2,17370, 17371)
DELETE FROM [User] WHERE PersonId NOT IN (1,17280,17281 )--NOT IN (2,17370, 17371)
DELETE FROM PersonAddress WHERE PersonId NOT IN (29452)
DELETE FROM Person WHERE Id NOT IN (1,17280,17281 )
DELETE FROM EmployerAddress WHERE EmployerId NOT IN (13930, 29441)

DELETE FROM CertificateLicence
DELETE FROM SavedMessageText
DELETE FROM SavedMessage
DELETE FROM SessionState
DELETE FROM [Session]
DELETE FROM ActionEvent
DELETE FROM StatusLog
DELETE FROM EntityType
DELETE FROM ActionType
--DELETE FROM CertificateLicenceLocalisationItem
--DELETE FROM JobTaskLocalisationItem
--DELETE FROM OnetLocalisationItem
--DELETE FROM LocalisationItem
--DELETE FROM Localisation
--DELETE FROM NAICS
--DELETE FROM CodeGroupItem
--DELETE FROM CodeGroup
--DELETE FROM CodeItem
--DELETE FROM JobTaskMultiOption
--DELETE FROM JobTask
--DELETE FROM [Role]
--DELETE FROM Profanity
--DELETE FROM ConfigurationItem
--DELETE FROM EmailTemplate
DELETE FROM Employer WHERE id NOT IN (13930, 29441)
--DELETE FROM ApplicationImage
DELETE FROM SingleSignOn
--DELETE FROM OnetWord
--DELETE FROM OnetRing
--DELETE FROM OnetPhrase
--DELETE FROM JobTaskMultiOption
--DELETE FROM JobTask
--DELETE FROM OnetTask
--DELETE FROM Onet

DECLARE @TmpTable TABLE
(
	NumberOfRows BIGINT, 
	TableName NVARCHAR(255)
)

INSERT INTO @TmpTable SELECT COUNT(*), '[NAICS]' FROM [NAICS]
INSERT INTO @TmpTable SELECT COUNT(*), '[JobSkill]' FROM [JobSkill]
INSERT INTO @TmpTable SELECT COUNT(*), '[CodeGroup]' FROM [CodeGroup]
INSERT INTO @TmpTable SELECT COUNT(*), '[OnetWord]' FROM [OnetWord]
INSERT INTO @TmpTable SELECT COUNT(*), '[SavedSearchEmployee]' FROM [SavedSearchEmployee]
INSERT INTO @TmpTable SELECT COUNT(*), '[CodeItem]' FROM [CodeItem]
INSERT INTO @TmpTable SELECT COUNT(*), '[LocalisationItem]' FROM [LocalisationItem]
INSERT INTO @TmpTable SELECT COUNT(*), '[User]' FROM [User]
INSERT INTO @TmpTable SELECT COUNT(*), '[EmployerPhoneNumber]' FROM [EmployerPhoneNumber]
INSERT INTO @TmpTable SELECT COUNT(*), '[PhoneNumber]' FROM [PhoneNumber]
INSERT INTO @TmpTable SELECT COUNT(*), '[JobLanguage]' FROM [JobLanguage]
INSERT INTO @TmpTable SELECT COUNT(*), '[PersonListCandidate]' FROM [PersonListCandidate]
INSERT INTO @TmpTable SELECT COUNT(*), '[EntityType]' FROM [EntityType]
INSERT INTO @TmpTable SELECT COUNT(*), '[JobCertificate]' FROM [JobCertificate]
INSERT INTO @TmpTable SELECT COUNT(*), '[Note]' FROM [Note]
INSERT INTO @TmpTable SELECT COUNT(*), '[EmployeeBusinessUnit]' FROM [EmployeeBusinessUnit]
INSERT INTO @TmpTable SELECT COUNT(*), '[Person]' FROM [Person]
INSERT INTO @TmpTable SELECT COUNT(*), '[JobLicence]' FROM [JobLicence]
INSERT INTO @TmpTable SELECT COUNT(*), '[Candidate]' FROM [Candidate]
INSERT INTO @TmpTable SELECT COUNT(*), '[JobTask]' FROM [JobTask]
INSERT INTO @TmpTable SELECT COUNT(*), '[Session]' FROM [Session]
INSERT INTO @TmpTable SELECT COUNT(*), '[SavedSearch]' FROM [SavedSearch]
INSERT INTO @TmpTable SELECT COUNT(*), '[JobNote]' FROM [JobNote]
INSERT INTO @TmpTable SELECT COUNT(*), '[Employee]' FROM [Employee]
INSERT INTO @TmpTable SELECT COUNT(*), '[ApplicationImage]' FROM [ApplicationImage]
INSERT INTO @TmpTable SELECT COUNT(*), '[JobTaskMultiOption]' FROM [JobTaskMultiOption]
INSERT INTO @TmpTable SELECT COUNT(*), '[KeyTable]' FROM [KeyTable]
INSERT INTO @TmpTable SELECT COUNT(*), '[PostingSurvey]' FROM [PostingSurvey]
INSERT INTO @TmpTable SELECT COUNT(*), '[SingleSignOn]' FROM [SingleSignOn]
INSERT INTO @TmpTable SELECT COUNT(*), '[Role]' FROM [Role]
INSERT INTO @TmpTable SELECT COUNT(*), '[Localisation]' FROM [Localisation]
INSERT INTO @TmpTable SELECT COUNT(*), '[SessionState]' FROM [SessionState]
INSERT INTO @TmpTable SELECT COUNT(*), '[Profanity]' FROM [Profanity]
INSERT INTO @TmpTable SELECT COUNT(*), '[ActionType]' FROM [ActionType]
INSERT INTO @TmpTable SELECT COUNT(*), '[UserRole]' FROM [UserRole]
INSERT INTO @TmpTable SELECT COUNT(*), '[ConfigurationItem]' FROM [ConfigurationItem]
INSERT INTO @TmpTable SELECT COUNT(*), '[PersonList]' FROM [PersonList]
INSERT INTO @TmpTable SELECT COUNT(*), '[EmployerLogo]' FROM [BusinessUnitLogo]
INSERT INTO @TmpTable SELECT COUNT(*), '[Message]' FROM [Message]
INSERT INTO @TmpTable SELECT COUNT(*), '[OnetLocalisationItem]' FROM [OnetLocalisationItem]
INSERT INTO @TmpTable SELECT COUNT(*), '[CandidateNote]' FROM [CandidateNote]
INSERT INTO @TmpTable SELECT COUNT(*), '[CodeGroupItem]' FROM [CodeGroupItem]
INSERT INTO @TmpTable SELECT COUNT(*), '[DismissedMessage]' FROM [DismissedMessage]
INSERT INTO @TmpTable SELECT COUNT(*), '[EmployerDescription]' FROM [BusinessUnitDescription]
INSERT INTO @TmpTable SELECT COUNT(*), '[JobTaskLocalisationItem]' FROM [JobTaskLocalisationItem]
INSERT INTO @TmpTable SELECT COUNT(*), '[Onet]' FROM [Onet]
INSERT INTO @TmpTable SELECT COUNT(*), '[ActionEvent]' FROM [ActionEvent]
INSERT INTO @TmpTable SELECT COUNT(*), '[MessageText]' FROM [MessageText]
INSERT INTO @TmpTable SELECT COUNT(*), '[EmailTemplate]' FROM [EmailTemplate]
INSERT INTO @TmpTable SELECT COUNT(*), '[EmployerAddress]' FROM [EmployerAddress]
INSERT INTO @TmpTable SELECT COUNT(*), '[Job]' FROM [Job]
INSERT INTO @TmpTable SELECT COUNT(*), '[CandidateApplication]' FROM [CandidateApplication]
INSERT INTO @TmpTable SELECT COUNT(*), '[PersonAddress]' FROM [PersonAddress]
INSERT INTO @TmpTable SELECT COUNT(*), '[CertificateLicence]' FROM [CertificateLicence]
INSERT INTO @TmpTable SELECT COUNT(*), '[JobAddress]' FROM [JobAddress]
INSERT INTO @TmpTable SELECT COUNT(*), '[CertificateLicenceLocalisationItem]' FROM [CertificateLicenceLocalisationItem]
INSERT INTO @TmpTable SELECT COUNT(*), '[EmployeeCandidate]' FROM [EmployeeCandidate]
INSERT INTO @TmpTable SELECT COUNT(*), '[JobLocation]' FROM [JobLocation]
INSERT INTO @TmpTable SELECT COUNT(*), '[OnetRing]' FROM [OnetRing]
INSERT INTO @TmpTable SELECT COUNT(*), '[SavedMessage]' FROM [SavedMessage]
INSERT INTO @TmpTable SELECT COUNT(*), '[JobSpecialRequirement]' FROM [JobSpecialRequirement]
INSERT INTO @TmpTable SELECT COUNT(*), '[OnetTask]' FROM [OnetTask]
INSERT INTO @TmpTable SELECT COUNT(*), '[OnetPhrase]' FROM [OnetPhrase]
INSERT INTO @TmpTable SELECT COUNT(*), '[Employer]' FROM [Employer]
INSERT INTO @TmpTable SELECT COUNT(*), '[BusinessUnit]' FROM [BusinessUnit]
INSERT INTO @TmpTable SELECT COUNT(*), '[SavedMessageText]' FROM [SavedMessageText]

--UPDATE KeyTable SET NextId = 1000000

SELECT * FROM @TmpTable WHERE NumberOfRows > 0

COMMIT 