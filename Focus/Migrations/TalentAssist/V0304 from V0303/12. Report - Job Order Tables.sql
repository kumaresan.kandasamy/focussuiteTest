IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobOrder' AND COLUMN_NAME = 'FocusBusinessUnitId')
BEGIN
	ALTER TABLE [Report.JobOrder] ADD FocusBusinessUnitId BIGINT NULL
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobOrderAction' AND COLUMN_NAME = 'ReferralsRequested')
BEGIN
	ALTER TABLE [Report.JobOrderAction] ADD ReferralsRequested INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobOrderAction' AND COLUMN_NAME = 'ApplicantsNotHired')
BEGIN
	ALTER TABLE [Report.JobOrderAction] ADD ApplicantsNotHired INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobOrderAction' AND COLUMN_NAME = 'ApplicantsNotYetPlaced')
BEGIN
	ALTER TABLE [Report.JobOrderAction] ADD ApplicantsNotYetPlaced INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobOrderAction' AND COLUMN_NAME = 'FailedToRespondToInvitation')
BEGIN
	ALTER TABLE [Report.JobOrderAction] ADD FailedToRespondToInvitation INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobOrderAction' AND COLUMN_NAME = 'FailedToReportToJob')
BEGIN
	ALTER TABLE [Report.JobOrderAction] ADD FailedToReportToJob INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobOrderAction' AND COLUMN_NAME = 'FoundJobFromOtherSource')
BEGIN
	ALTER TABLE [Report.JobOrderAction] ADD FoundJobFromOtherSource INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobOrderAction' AND COLUMN_NAME = 'JobAlreadyFilled')
BEGIN
	ALTER TABLE [Report.JobOrderAction] ADD JobAlreadyFilled INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobOrderAction' AND COLUMN_NAME = 'NewApplicant')
BEGIN
	ALTER TABLE [Report.JobOrderAction] ADD NewApplicant INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobOrderAction' AND COLUMN_NAME = 'NotQualified')
BEGIN
	ALTER TABLE [Report.JobOrderAction] ADD NotQualified INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobOrderAction' AND COLUMN_NAME = 'ApplicantRecommended')
BEGIN
	ALTER TABLE [Report.JobOrderAction] ADD ApplicantRecommended INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobOrderAction' AND COLUMN_NAME = 'RefusedReferral')
BEGIN
	ALTER TABLE [Report.JobOrderAction] ADD RefusedReferral INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobOrderAction' AND COLUMN_NAME = 'UnderConsideration')
BEGIN
	ALTER TABLE [Report.JobOrderAction] ADD UnderConsideration INT NOT NULL DEFAULT(0)
END
GO

/*
IF EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobOrderAction' AND COLUMN_NAME = 'ApplicantsRejected')
BEGIN
	DECLARE @Sql NVARCHAR(500)

	SELECT @Sql = 'ALTER TABLE [' + T.name + '] DROP CONSTRAINT [' + D.Name + ']'
	FROM sys.tables T
	INNER JOIN sys.default_constraints D
		ON D.parent_object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.object_id = T.object_id
		AND C.column_id = D.parent_column_id
	WHERE T.name = 'Report.JobOrderAction'
	AND C.name = 'ApplicantsRejected'

	IF @Sql IS NOT NULL
		EXECUTE sp_executeSql @Sql
		
	ALTER TABLE [Report.JobOrderAction] DROP COLUMN ApplicantsRejected
END
GO
*/

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobOrderOffice]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Report.JobOrderOffice](
	[Id] [bigint] NOT NULL,
	[OfficeName] [nvarchar](100) NOT NULL,
	[OfficeId] [bigint] NULL,
	[JobOrderId] [bigint] NOT NULL DEFAULT(0),
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE referenced_object_id = OBJECT_ID(N'[dbo].[Report.JobOrder]') AND parent_object_id = OBJECT_ID(N'[dbo].[Report.JobOrderOffice]'))
ALTER TABLE [dbo].[Report.JobOrderOffice]  WITH CHECK ADD FOREIGN KEY([JobOrderId])
REFERENCES [dbo].[Report.JobOrder] ([Id])
GO

