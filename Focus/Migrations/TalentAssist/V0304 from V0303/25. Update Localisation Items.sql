UPDATE [Config.LocalisationItem] SET Value = 'Job seeker' WHERE [Key] = 'Global.CandidateType.Workforce'

UPDATE [Config.LocalisationItem] SET Value = 'Job seekers' WHERE [Key] = 'Global.CandidateTypes.Workforce'

UPDATE 
	LI
SET 
	[Value] = 'Failed to report to interview'
FROM 
	[Config.LocalisationItem] LI 
INNER JOIN [Config.Localisation] L
	ON L.Id = LI.LocalisationId
WHERE 
	L.Culture = '**-**'
	AND LI.[Key] = 'ApplicationStatusTypes.FailedToShow'

UPDATE 
	LI
SET 
	[Value] = 'Failed to apply to job'
FROM 
	[Config.LocalisationItem] LI 
INNER JOIN [Config.Localisation] L
	ON L.Id = LI.LocalisationId
WHERE 
	L.Culture = '**-**'
	AND LI.[Key] = 'ApplicationStatusTypes.DidNotApply'
