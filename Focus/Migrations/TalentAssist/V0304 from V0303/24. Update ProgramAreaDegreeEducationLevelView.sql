ALTER VIEW [dbo].[Library.ProgramAreaDegreeEducationLevelView]
AS
SELECT
	NEWID() AS Id,
	PA.Id AS ProgramAreaId,
	PA.Name AS ProgramAreaName,
	D.Id AS DegreeId,
	D.Name AS DegreeName,
	D.IsClientData,
	D.ClientDataTag,
	DEL.Id AS DegreeEducationLevelId,
	DEL.Name AS DegreeEducationLevelName,
	DEL.EducationLevel
FROM
	dbo.[Library.ProgramArea] PA
	INNER JOIN dbo.[Library.ProgramAreaDegree] PAD ON PAD.ProgramAreaId = PA.Id
	INNER JOIN dbo.[Library.Degree] D ON D.Id = PAD.DegreeId
	INNER JOIN dbo.[Library.DegreeEducationLevel] DEL ON DEL.DegreeId = D.Id