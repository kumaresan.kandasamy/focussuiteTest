BEGIN TRANSACTION

IF EXISTS (SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'BatchProcessSettings')
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		[Value] = '[{"Process":1,"Identifier":"75965dec0c6f4cbba3eaedbbd2ff3262"},{"Process":2,"Identifier":"c1144ccbed7749359aaa964512a7e900"},{"Process":3,"Identifier":"97d8c47be2cb45deb7d93e08c2864702"},{"Process":4,"Identifier":"35cbf2025ffb44899528a2adb31a198f"},{"Process":5,"Identifier":"58481ee4f600419c9cf4960aa2309514"},{"Process":6,"Identifier":"6d4c26985c7245c096999cd42bab4677"},{"Process":7,"Identifier":"506c2147a60941934539a6bc5170e2"}]'
	WHERE
		[Key] = 'BatchProcessSettings'
END
ELSE
BEGIN

	DECLARE @NextId bigint
	DECLARE @EndOfBlockNextId bigint
	DECLARE @IdsRequired bigint

	SELECT @IdsRequired = 5

	UPDATE KeyTable SET NextId = NextId + @IdsRequired

	SELECT @EndOfBlockNextId = NextId FROM KeyTable

	SELECT @NextId = @EndOfBlockNextId - @IdsRequired
	
	SET IDENTITY_INSERT [Config.ConfigurationItem] ON
	
	INSERT INTO [Config.ConfigurationItem]
	(
		Id,
		[Key],
		[Value],
		InternalOnly
	)
	VALUES
	(
		@NextId,
		'BatchProcessSettings',
		'[{"Process":1,"Identifier":"75965dec0c6f4cbba3eaedbbd2ff3262"},{"Process":2,"Identifier":"c1144ccbed7749359aaa964512a7e900"},{"Process":3,"Identifier":"97d8c47be2cb45deb7d93e08c2864702"},{"Process":4,"Identifier":"35cbf2025ffb44899528a2adb31a198f"},{"Process":5,"Identifier":"58481ee4f600419c9cf4960aa2309514"},{"Process":6,"Identifier":"6d4c26985c7245c096999cd42bab4677"},{"Process":7,"Identifier":"506c2147a60941934539a6bc5170e2"}]',
		1
	)
	
	SET IDENTITY_INSERT [Config.ConfigurationItem] OFF	
	
END

COMMIT TRANSACTION