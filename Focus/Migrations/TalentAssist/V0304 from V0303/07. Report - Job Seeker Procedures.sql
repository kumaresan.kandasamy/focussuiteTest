
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobSeekerActivityTotals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.JobSeekerActivityTotals]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tim Case
-- Create date: 21 October 2013
-- Description:	Get report totals for job seeker actions
-- =============================================
CREATE PROCEDURE [dbo].[Report.JobSeekerActivityTotals]
	@FromDate DATETIME,
	@ToDate DATETIME,
	@Category NVARCHAR(500),
	@Activity NVARCHAR(500) = NULL
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		JSA.JobSeekerId AS Id,
		SUM(JSA.AssignmentsMade) AS AssignmentsMade
	FROM 
		[Report.JobSeekerActivityAssignment] JSA
	WHERE 
		JSA.AssignDate BETWEEN @FromDate AND @ToDate
		AND JSA.ActivityCategory = @Category
		AND JSA.Activity = ISNULL(@Activity, JSA.Activity)
	GROUP BY
		JSA.JobSeekerId
END
GO

GRANT EXEC ON [dbo].[Report.JobSeekerActivityTotals] TO [FocusAgent]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobSeekerActivityTotalsForIds]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.JobSeekerActivityTotalsForIds]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tim Case
-- Create date: 21 October 2013
-- Description:	Get report totals for job seeker actions
-- =============================================
CREATE PROCEDURE [dbo].[Report.JobSeekerActivityTotalsForIds]
	@FromDate DATETIME,
	@ToDate DATETIME,
	@Category NVARCHAR(500),
	@Activity NVARCHAR(500) = NULL,
	@JobSeekerIds XML
AS
BEGIN
	SET NOCOUNT ON
	
	CREATE TABLE #JobSeekerIds
	(
		Id BIGINT
	)
	
	INSERT INTO #JobSeekerIds ( Id )
    SELECT t.value('.', 'bigint')
    FROM @JobSeekerIds.nodes('//id') AS x(t) 
	
	SELECT
		JSA.JobSeekerId AS Id,
		SUM(JSA.AssignmentsMade) AS AssignmentsMade
	FROM 
		[Report.JobSeekerActivityAssignment] JSA
	INNER JOIN #JobSeekerIds JSI
		ON JSA.JobSeekerId = JSI.Id
	WHERE 
		JSA.AssignDate BETWEEN @FromDate AND @ToDate
		AND JSA.ActivityCategory = @Category
		AND JSA.Activity = ISNULL(@Activity, JSA.Activity)
	GROUP BY
		JSA.JobSeekerId
END
GO

GRANT EXEC ON [dbo].[Report.JobSeekerActivityTotalsForIds] TO [FocusAgent]
GO

/****** Object:  StoredProcedure [dbo].[Report.JobSeekerActionTotals]    Script Date: 09/09/2013 17:11:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobSeekerActionTotals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.JobSeekerActionTotals]
GO

/****** Object:  StoredProcedure [dbo].[Report.JobSeekerActionTotals]    Script Date: 09/09/2013 17:11:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tim Case
-- Create date: 09 September 2013
-- Description:	Get report totals for job seeker actions
-- =============================================
CREATE PROCEDURE [dbo].[Report.JobSeekerActionTotals]
	@FromDate DATETIME,
	@ToDate DATETIME
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		JobSeekerId AS Id,
		SUM(Logins) AS Logins,
		SUM(PostingsViewed) AS PostingsViewed,
		SUM(ReferralRequests) AS ReferralRequests,
		SUM(SelfReferrals) AS SelfReferrals,
		SUM(StaffReferrals) AS StaffReferrals,
		SUM(NotesAdded) AS NotesAdded,
		SUM(AddedToLists) AS AddedToLists,
		SUM(ActivitiesAssigned) AS ActivitiesAssigned,
		SUM(StaffAssignments) AS StaffAssignments,
		SUM(EmailsSent) AS EmailsSent,
		SUM(FollowUpIssues) AS FollowUpIssues,
		SUM(IssuesResolved) AS IssuesResolved,
		SUM(Hired) AS Hired,
		SUM(NotHired) AS NotHired,
		SUM(FailedToApply) AS FailedToApply,
		SUM(FailedToReportToInterview) AS FailedToReportToInterview,
		SUM(InterviewDenied) AS InterviewDenied,
		SUM(InterviewScheduled) AS InterviewScheduled,
		SUM(NewApplicant) AS NewApplicant,
		SUM(Recommended) AS Recommended,
		SUM(RefusedOffer) AS RefusedOffer,
		SUM(UnderConsideration) AS UnderConsideration,
		SUM(UsedOnlineResumeHelp) AS UsedOnlineResumeHelp,
		SUM(SavedJobAlerts) AS SavedJobAlerts,
		SUM(TargetingHighGrowthSectors) AS TargetingHighGrowthSectors,
		SUM(SelfReferralsExternal) AS SelfReferralsExternal,
		SUM(StaffReferralsExternal) AS StaffReferralsExternal,
		SUM(ReferralsApproved) AS ReferralsApproved,
		SUM(FindJobsForSeeker) AS FindJobsForSeeker,
		SUM(FailedToReportToJob) AS FailedToReportToJob,
		SUM(FailedToRespondToInvitation) AS FailedToRespondToInvitation,
		SUM(FoundJobFromOtherSource) AS FoundJobFromOtherSource,
		SUM(JobAlreadyFilled) AS JobAlreadyFilled,
		SUM(NotQualified) AS NotQualified,
		SUM(NotYetPlaced) AS NotYetPlaced,
		SUM(RefusedReferral) AS RefusedReferral,
		SUM(SurveyVerySatisfied) AS SurveyVerySatisfied,
		SUM(SurveySatisfied) AS SurveySatisfied,
		SUM(SurveyDissatisfied) AS SurveyDissatisfied,
		SUM(SurveyVeryDissatisfied) AS SurveyVeryDissatisfied,
		SUM(SurveyNoUnexpectedMatches) AS SurveyNoUnexpectedMatches,
		SUM(SurveyUnexpectedMatches) AS SurveyUnexpectedMatches,
		SUM(SurveyReceivedInvitations) AS SurveyReceivedInvitations,
		SUM(SurveyDidNotReceiveInvitations) AS SurveyDidNotReceiveInvitations
	FROM 
		[Report.JobSeekerAction]
	WHERE 
		ActionDate BETWEEN @FromDate AND @ToDate
	GROUP BY
		JobSeekerId
END
GO

GRANT EXEC ON [dbo].[Report.JobSeekerActionTotals] TO [FocusAgent]
GO

/****** Object:  StoredProcedure [dbo].[Report.JobSeekerActionTotalsForIds]    Script Date: 09/16/2013 10:13:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobSeekerActionTotalsForIds]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.JobSeekerActionTotalsForIds]
GO

/****** Object:  StoredProcedure [dbo].[Report.JobSeekerActionTotalsForIds]    Script Date: 09/16/2013 10:13:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tim Case
-- Create date: 09 September 2013
-- Description:	Get report totals for job seeker actions
-- =============================================
CREATE PROCEDURE [dbo].[Report.JobSeekerActionTotalsForIds]
	@FromDate DATETIME,
	@ToDate DATETIME,
	@JobSeekerIds XML
AS
BEGIN
	SET NOCOUNT ON
	
	CREATE TABLE #JobSeekerIds
	(
		Id BIGINT
	)
	
	INSERT INTO #JobSeekerIds ( Id )
    SELECT t.value('.', 'bigint')
    FROM @JobSeekerIds.nodes('//id') AS x(t) 

	SELECT
		JSA.JobSeekerId AS Id,
		SUM(JSA.Logins) AS Logins,
		SUM(JSA.PostingsViewed) AS PostingsViewed,
		SUM(JSA.ReferralRequests) AS ReferralRequests,
		SUM(JSA.SelfReferrals) AS SelfReferrals,
		SUM(JSA.StaffReferrals) AS StaffReferrals,
		SUM(JSA.NotesAdded) AS NotesAdded,
		SUM(JSA.AddedToLists) AS AddedToLists,
		SUM(JSA.ActivitiesAssigned) AS ActivitiesAssigned,
		SUM(JSA.StaffAssignments) AS StaffAssignments,
		SUM(JSA.EmailsSent) AS EmailsSent,
		SUM(JSA.FollowUpIssues) AS FollowUpIssues,
		SUM(JSA.IssuesResolved) AS IssuesResolved,
		SUM(JSA.Hired) AS Hired,
		SUM(JSA.NotHired) AS NotHired,
		SUM(JSA.FailedToApply) AS FailedToApply,
		SUM(JSA.FailedToReportToInterview) AS FailedToReportToInterview,
		SUM(JSA.InterviewDenied) AS InterviewDenied,
		SUM(JSA.InterviewScheduled) AS InterviewScheduled,
		SUM(JSA.NewApplicant) AS NewApplicant,
		SUM(JSA.Recommended) AS Recommended,
		SUM(JSA.RefusedOffer) AS RefusedOffer,
		SUM(JSA.UnderConsideration) AS UnderConsideration,
		SUM(JSA.UsedOnlineResumeHelp) AS UsedOnlineResumeHelp,
		SUM(JSA.SavedJobAlerts) AS SavedJobAlerts,
		SUM(JSA.TargetingHighGrowthSectors) AS TargetingHighGrowthSectors,
		SUM(JSA.SelfReferralsExternal) AS SelfReferralsExternal,
		SUM(JSA.StaffReferralsExternal) AS StaffReferralsExternal,
		SUM(JSA.ReferralsApproved) AS ReferralsApproved,
		SUM(JSA.FindJobsForSeeker) AS FindJobsForSeeker,
		SUM(JSA.FailedToReportToJob) AS FailedToReportToJob,
		SUM(JSA.FailedToRespondToInvitation) AS FailedToRespondToInvitation,
		SUM(JSA.FoundJobFromOtherSource) AS FoundJobFromOtherSource,
		SUM(JSA.JobAlreadyFilled) AS JobAlreadyFilled,
		SUM(JSA.NotQualified) AS NotQualified,
		SUM(JSA.NotYetPlaced) AS NotYetPlaced,
		SUM(JSA.RefusedReferral) AS RefusedReferral,
		SUM(JSA.SurveyVerySatisfied) AS VerySurveySatisfied,
		SUM(JSA.SurveySatisfied) AS SurveySatisfied,
		SUM(JSA.SurveyDissatisfied) AS SurveyDissatisfied,
		SUM(JSA.SurveyVeryDissatisfied) AS SurveyVeryDissatisfied,
		SUM(JSA.SurveyNoUnexpectedMatches) AS SurveyNoUnexpectedMatches,
		SUM(JSA.SurveyUnexpectedMatches) AS SurveyUnexpectedMatches,
		SUM(JSA.SurveyReceivedInvitations) AS SurveyReceivedInvitations,
		SUM(JSA.SurveyDidNotReceiveInvitations) AS SurveyDidNotReceiveInvitations
	FROM 
		[Report.JobSeekerAction] JSA
	INNER JOIN #JobSeekerIds JSI
		ON JSA.JobSeekerId = JSI.Id
	WHERE 
		JSA.ActionDate BETWEEN @FromDate AND @ToDate
	GROUP BY
		JSA.JobSeekerId
END
GO

GRANT EXEC ON [dbo].[Report.JobSeekerActionTotalsForIds] TO [FocusAgent]
GO