IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Config.ConfigurationItem' AND COLUMN_NAME = 'InternalOnly')
BEGIN
	ALTER TABLE [Config.ConfigurationItem] ADD InternalOnly BIT NOT NULL DEFAULT(1)
END
GO

UPDATE
	[Config.ConfigurationItem]
SET
	InternalOnly = 0
WHERE
	[Key] IN 
	(
		'CareerSearchShowAllJobs',
		'CareerSearchMinimumStarMatchScore',
		'CareerSearchDefaultRadiusId',
		'CareerSearchCentrePointType',
		'CareerSearchRadius',
		'CareerSearchZipcode',
		'DefaultStateKey',
		'NearbyStateKeys',
		'CareerSearchResumesSearchable',
		'JobSeekerSendInactivityEmail',
		'JobSeekerInactivityEmailLimit',
		'JobSeekerInactivityEmailGrace'
	)
