/****** Object:  View [dbo].[Report.EmployerView]    Script Date: 09/30/2013 16:06:37 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Report.EmployerView]'))
DROP VIEW [dbo].[Report.EmployerView]
GO

/****** Object:  View [dbo].[Report.EmployerView]    Script Date: 09/30/2013 16:06:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Report.EmployerView]

AS
	SELECT
		E.Id,
		E.FocusBusinessUnitId,
		E.FocusEmployerId,
		E.[Name],
		E.CountyId,
		E.County,
		E.StateId,
		E.[State],
		E.PostalCode,
		E.Office,
		E.LatitudeRadians,
		E.LongitudeRadians,
		E.FederalEmployerIdentificationNumber,
		J.JobTitle,
		J.WorkOpportunitiesTaxCreditHires,
		J.ForeignLabourCertification,
		J.ForeignLabourType,
		J.FederalContractor,
		J.CourtOrderedAffirmativeAction,
		J.MinSalary,
		J.MaxSalary,
		J.SalaryFrequency,
		J.JobStatus,
		J.MinimumEducationLevel
	FROM 
		[Report.Employer] E
	LEFT OUTER JOIN [Report.JobOrder] J
		ON J.EmployerId = E.Id 
GO


/****** Object:  StoredProcedure [dbo].[Report.EmployerKeywordFilter]    Script Date: 09/13/2013 12:00:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.EmployerKeywordFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.EmployerKeywordFilter]
GO

/****** Object:  StoredProcedure [dbo].[Report.EmployerKeywordFilter]    Script Date: 09/13/2013 12:00:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tim Case
-- Create date: 04 October 2013
-- Description:	Filter job Orders by keyword
-- =============================================
CREATE PROCEDURE [dbo].[Report.EmployerKeywordFilter]
	@Keywords XML,
	@CheckName BIT,
	@CheckJobTitle BIT,
	@CheckDescription BIT = 0,
	@SearchAll BIT
AS
BEGIN
	SET NOCOUNT ON
	
	CREATE TABLE #Keywords
	(
		KeywordId INT IDENTITY(1, 1),
		NameKeyword NVARCHAR(500),
		JobTitleKeyword NVARCHAR(500),
		DescriptionKeyword NVARCHAR(500)
	)
	
	DECLARE @KeywordsToCheck INT
	
	INSERT INTO #KeyWords ( NameKeyword, JobTitleKeyword, DescriptionKeyword )
    SELECT
		CASE @CheckName WHEN 1 THEN '%' + t.value('.', 'varchar(500)') + '%' END,
		CASE @CheckJobTitle WHEN 1 THEN '%' + t.value('.', 'varchar(500)') + '%' END,
		CASE @CheckDescription WHEN 1 THEN '%' + t.value('.', 'varchar(500)') + '%' END
    FROM @Keywords.nodes('//keyword') AS x(t) 
    
	SET @KeywordsToCheck = CASE @SearchAll WHEN 1 THEN @@ROWCOUNT ELSE 1 END
	
	SELECT DISTINCT
		E.Id
	FROM 
		[Report.Employer] E
	INNER JOIN [Report.JobOrder] J
		ON J.EmployerId = E.Id
	INNER JOIN #Keywords K
		ON E.Name LIKE K.NameKeyword
		OR J.JobTitle LIKE K.JobTitleKeyword
		OR J.[Description] LIKE K.DescriptionKeyword
	GROUP BY 
		E.Id,
		J.Id
	HAVING
		COUNT(DISTINCT K.KeywordId) >= @KeywordsToCheck
END
GO

GRANT EXEC ON [dbo].[Report.EmployerKeywordFilter] TO [FocusAgent]
GO


/****** Object:  StoredProcedure [dbo].[Report.EmployerActionTotals]    Script Date: 09/09/2013 17:11:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.EmployerActionTotals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.EmployerActionTotals]
GO

/****** Object:  StoredProcedure [dbo].[Report.EmployerActionTotals]    Script Date: 09/09/2013 17:11:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tim Case
-- Create date: 27 September 2013
-- Description:	Get report totals for employer actions
-- =============================================
CREATE PROCEDURE [dbo].[Report.EmployerActionTotals]
	@FromDate DATETIME,
	@ToDate DATETIME
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		EmployerId AS Id,
		SUM(JobOrdersEdited) AS JobOrdersEdited,
		SUM(JobSeekersInterviewed) AS JobSeekersInterviewed,
		SUM(JobSeekersHired) AS JobSeekersHired,
		SUM(JobOrdersCreated) AS JobOrdersCreated,
		SUM(JobOrdersPosted) AS JobOrdersPosted,
		SUM(JobOrdersPutOnHold) AS JobOrdersPutOnHold,
		SUM(JobOrdersRefreshed) AS JobOrdersRefreshed,
		SUM(JobOrdersClosed) AS JobOrdersClosed,
		SUM(InvitationsSent) AS InvitationsSent,
		SUM(JobSeekersNotHired) AS JobSeekersNotHired,
		SUM(SelfReferrals) AS SelfReferrals,
		SUM(StaffReferrals) AS StaffReferrals
	FROM 
		[Report.EmployerAction]
	WHERE 
		ActionDate BETWEEN @FromDate AND @ToDate
	GROUP BY
		EmployerId
END
GO

GRANT EXEC ON [dbo].[Report.EmployerActionTotals] TO [FocusAgent]
GO


/****** Object:  StoredProcedure [dbo].[Report.EmployerActionTotalsForIds]    Script Date: 09/16/2013 10:13:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.EmployerActionTotalsForIds]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.EmployerActionTotalsForIds]
GO

/****** Object:  StoredProcedure [dbo].[Report.EmployerActionTotalsForIds]    Script Date: 09/16/2013 10:13:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tim Case
-- Create date: 04 October 2013
-- Description:	Get report totals for employer actions
-- =============================================
CREATE PROCEDURE [dbo].[Report.EmployerActionTotalsForIds]
	@FromDate DATETIME,
	@ToDate DATETIME,
	@EmployerIds XML
AS
BEGIN
	SET NOCOUNT ON
	
	CREATE TABLE #EmployerIds
	(
		Id BIGINT
	)
	
	INSERT INTO #EmployerIds ( Id )
    SELECT t.value('.', 'bigint')
    FROM @EmployerIds.nodes('//id') AS x(t) 

	SELECT
		EA.EmployerId AS Id,
		SUM(EA.JobOrdersEdited) AS JobOrdersEdited,
		SUM(EA.JobSeekersInterviewed) AS JobSeekersInterviewed,
		SUM(EA.JobSeekersHired) AS JobSeekersHired,
		SUM(EA.JobOrdersCreated) AS JobOrdersCreated, 
		SUM(EA.JobOrdersPosted) AS JobOrdersPosted, 
		SUM(EA.JobOrdersPutOnHold) AS JobOrdersPutOnHold, 
		SUM(EA.JobOrdersRefreshed) AS JobOrdersRefreshed, 
		SUM(EA.JobOrdersClosed) AS JobOrdersClosed, 
		SUM(EA.InvitationsSent) AS InvitationsSent, 
		SUM(EA.JobSeekersNotHired) AS JobSeekersNotHired, 
		SUM(EA.SelfReferrals) AS SelfReferrals, 
		SUM(EA.StaffReferrals) AS StaffReferrals
	FROM 
		[Report.EmployerAction] EA
	INNER JOIN #EmployerIds EI
		ON EA.EmployerId = EI.Id
	WHERE 
		EA.ActionDate BETWEEN @FromDate AND @ToDate
	GROUP BY
		EA.EmployerId
END
GO

GRANT EXEC ON [dbo].[Report.EmployerActionTotalsForIds] TO [FocusAgent]
GO