/****** Object:  View [dbo].[Data.Application.ActiveSearchAlertView]    Script Date: 11/14/2013 13:00:49 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.ActiveSearchAlertView]'))
DROP VIEW [dbo].[Data.Application.ActiveSearchAlertView]
GO

/****** Object:  View [dbo].[Data.Application.ActiveSearchAlertView]    Script Date: 11/14/2013 13:00:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Data.Application.ActiveSearchAlertView]
AS

SELECT 
	SavedSearch.Id,
	Name,
	SearchCriteria,
	AlertEmailRequired,
	AlertEmailFrequency,
	AlertEmailFormat,
	AlertEmailAddress,
	AlertEmailScheduledOn,
	[Type],
	SearchWithActiveUser.Recipient
FROM 
	[Data.Application.SavedSearch] AS SavedSearch WITH (NOLOCK)
	INNER JOIN (SELECT DISTINCT
					SavedSearchId,
					[Person].FirstName AS Recipient
				FROM 
					[Data.Application.SavedSearchUser] AS SavedSearchUser WITH (NOLOCK)
					INNER JOIN [Data.Application.User] AS [User] WITH (NOLOCK) ON SavedSearchUser.UserId = [User].Id 
					INNER JOIN [Data.Application.Person] AS [Person] WITH (NOLOCK) ON [User].PersonId = [Person].Id
				WHERE
					[User].[Enabled] = 1) AS SearchWithActiveUser ON SavedSearch.Id = SearchWithActiveUser.SavedSearchId
WHERE 
	AlertEmailRequired = 1 







GO


