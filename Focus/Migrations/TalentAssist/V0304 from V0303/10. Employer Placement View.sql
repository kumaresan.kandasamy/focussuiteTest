ALTER VIEW [dbo].[Data.Application.EmployerPlacementsView]
AS
SELECT 
	[Application].Id, 
	BusinessUnit.Id AS BusinessUnitId,
	BusinessUnit.Name AS EmployerName,
	[Resume].PersonId AS JobSeekerId, 
	[Resume].FirstName + ' ' + [Resume].LastName AS Name, 
	BusinessUnit.EmployerId AS EmployerId,
    Job.JobTitle,
    [Application].StatusLastChangedOn AS StartDate
  
FROM  
	[Data.Application.Application] AS [Application] WITH (NOLOCK) 
	INNER JOIN [Data.Application.Posting] AS Posting WITH (NOLOCK) ON [Application].PostingId = Posting.Id
	INNER JOIN [Data.Application.Job] AS Job WITH (NOLOCK) ON Posting.JobId = Job.Id 
	INNER JOIN [Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON Job.BusinessUnitId = BusinessUnit.Id 
	INNER JOIN [Data.Application.Resume] AS [Resume] WITH (NOLOCK) ON [Application].ResumeId = [Resume].Id
WHERE 
	([Application].ApplicationStatus = 12)