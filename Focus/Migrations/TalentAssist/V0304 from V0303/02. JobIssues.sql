/****** Object:  Table [dbo].[Data.Application.CandidateSearchHistory]    Script Date: 10/24/2013 11:13:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Data.Application.CandidateSearchHistory](
	[Id] [bigint] NOT NULL,
	[JobId] [bigint] NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Data.Application.CandidateSearchHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Data.Application.CandidateSearchHistory]  WITH CHECK ADD  CONSTRAINT [FK_Data.Application.CandidateSearchHistory_Data.Application.Job] FOREIGN KEY([JobId])
REFERENCES [dbo].[Data.Application.Job] ([Id])
GO

ALTER TABLE [dbo].[Data.Application.CandidateSearchHistory] CHECK CONSTRAINT [FK_Data.Application.CandidateSearchHistory_Data.Application.Job]
GO

/****** Object:  Table [dbo].[Data.Application.CandidateSearchResult]    Script Date: 10/24/2013 11:13:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Data.Application.CandidateSearchResult](
	[Id] [bigint] NOT NULL,
	[CandidateSearchHistoryId] [bigint] NOT NULL,
	[PersonId] [bigint] NOT NULL,
	[Score] [int] NOT NULL,
 CONSTRAINT [PK_Data.Application.CandidateSearchResults] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Data.Application.CandidateSearchResult]  WITH CHECK ADD  CONSTRAINT [FK_Data.Application.CandidateSearchResult_Data.Application.CandidateSearchHistory] FOREIGN KEY([CandidateSearchHistoryId])
REFERENCES [dbo].[Data.Application.CandidateSearchHistory] ([Id])
GO

ALTER TABLE [dbo].[Data.Application.CandidateSearchResult] CHECK CONSTRAINT [FK_Data.Application.CandidateSearchResult_Data.Application.CandidateSearchHistory]
GO

/****** Object:  Table [dbo].[Data.Application.JobIssues]    Script Date: 10/29/2013 11:57:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Data.Application.JobIssues](
	[Id] [bigint] NOT NULL,
	[JobId] [bigint] NOT NULL,
	[LowQualityMatches] [bit] NOT NULL,
	[LowQualityMatchesResolvedDate] [datetime] NULL,
	[LowReferralActivity] [bit] NOT NULL,
	[LowReferralActivityResolvedDate] [datetime] NULL,
	[NotViewingReferrals] [bit] NOT NULL,
	[NotViewingReferralsResolvedDate] [datetime] NULL,
	[SearchingButNotInviting] [bit] NOT NULL,
	[SearchingButNotInvitingResolvedDate] [datetime] NULL,
	[ClosingDateRefreshed] [bit] NOT NULL,
	[EarlyJobClosing] [bit] NOT NULL,
	[NegativeSurveyResponse] [bit] NOT NULL,
	[PositiveSurveyResponse] [bit] NOT NULL,
	[FollowUpRequested] [bit] NOT NULL,
 CONSTRAINT [PK_JobIssues] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Data.Application.JobIssues]  WITH CHECK ADD  CONSTRAINT [FK_Data.Application.JobIssues_Data.Application.Job] FOREIGN KEY([JobId])
REFERENCES [dbo].[Data.Application.Job] ([Id])
GO

ALTER TABLE [dbo].[Data.Application.JobIssues] CHECK CONSTRAINT [FK_Data.Application.JobIssues_Data.Application.Job]
GO



