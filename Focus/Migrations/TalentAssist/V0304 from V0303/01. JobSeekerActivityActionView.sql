/****** Object:  View [dbo].[Data.Application.JobSeekerActivityActionView]    Script Date: 10/21/2013 17:52:18 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.JobSeekerActivityActionView]'))
DROP VIEW [dbo].[Data.Application.JobSeekerActivityActionView]
GO

/****** Object:  View [dbo].[Data.Application.JobSeekerActivityActionView]    Script Date: 10/21/2013 17:52:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create VIEW [dbo].[Data.Application.JobSeekerActivityActionView]
AS

SELECT ae.Id, 
	ae.UserId,
	p.LastName + ', ' + p.FirstName AS 'UserName',
	ae.ActionedOn,
	at.Name AS 'ActionType',
	COALESCE(j.JobTitle, j2.JobTitle, pos.JobTitle, j3.JobTitle)  AS 'JobTitle',
	COALESCE(bu.Name, bu2.Name, pos.EmployerName, bu1.Name) AS 'BusinessUnitName',
	COALESCE(js1.Id, js2.Id, js3.Id, js4.Id) AS 'JobSeekerId',
	ae.EntityIdAdditional01,
	ae.AdditionalDetails
FROM [dbo].[Data.Core.ActionEvent] ae WITH (NOLOCK)
	inner join [Data.Core.ActionType] at WITH (NOLOCK) on at.Id = ae.ActionTypeId
	inner join [Data.Application.User] u WITH (NOLOCK) on u.Id = ae.UserId
	inner join [Data.Application.Person] p WITH (NOLOCK) on p.Id = u.PersonId
	
	-- Application related actions have Application as EntityId
	left join [Data.Application.Application] a WITH (NOLOCK) on a.Id = ae.EntityId
	left join [Data.Application.Posting] po WITH (NOLOCK) on po.Id = a.PostingId
	left join [Data.Application.Job] j WITH (NOLOCK) on j.Id = po.JobId	
	left join [Data.Application.BusinessUnit] bu WITH (NOLOCK) on bu.Id = j.BusinessUnitId
	left join [Data.Application.Resume] r1 WITH (NOLOCK) on r1.Id = a.ResumeId
	left join [Data.Application.Person] js1 WITH (NOLOCK) on js1.Id = r1.PersonId
	
	-- SaveResume has Resume as EntityId
	left join [Data.Application.Resume] r2 WITH (NOLOCK) on r2.Id = ae.EntityId
	left join [Data.Application.Person] js2 WITH (NOLOCK) on js2.Id = r2.PersonId
	
	-- AssignActivity has Person as EntityId
	left join [Data.Application.Person] js3 WITH (NOLOCK) on js3.Id = ae.EntityId
	
	-- InviteJobSeekerToApply has JobId as EntityId and Additional as JobSeekerId
	left join [Data.Application.Job] j2 WITH (NOLOCK) on j2.Id = ae.EntityId
	left join [Data.Application.BusinessUnit] bu2 WITH (NOLOCK) on bu2.Id = j2.BusinessUnitId
	left join [Data.Application.Person] js4 WITH (NOLOCK) on js4.Id = ae.EntityIdAdditional01
	
	-- External referral takes info from Posting
	left join [Data.Application.Posting] pos WITH (NOLOCK) on pos.Id = ae.EntityId
	--
	left join [Data.Application.Job] j3 on j3.Id = ae.EntityIdAdditional01
	left join [Data.Application.BusinessUnit] bu1 WITH (NOLOCK) on bu1.Id = j3.BusinessUnitId
	
	
	

where at.Name IN (
	'SelfReferral',
	'ReferralRequest',
	'SaveResume',
	'CreateNewResume',
	'InviteJobSeekerToApply',
	'UpdateApplicationStatusToRecommended',
	'UpdateApplicationStatusToFailedToShow',
	'UpdateApplicationStatusToHired',
	'UpdateApplicationStatusToInterviewScheduled',									
	'UpdateApplicationStatusToNewApplicant',
	'UpdateApplicationStatusToNotApplicable',
	'UpdateApplicationStatusToOfferMade',
	'UpdateApplicationStatusToNotHired',
	'UpdateApplicationStatusToUnderConsideration',
	'UpdateApplicationStatusToDidNotApply',
	'UpdateApplicationStatusToInterviewDenied',
	'UpdateApplicationStatusToRefusedOffer',
	'UpdateApplicationStatusToSelfReferred',
	'ExternalReferral',
	'AssignActivityToCandidate', 
	'StaffReferral',
	'CreateCandidateApplication',
	'ApproveCandidateReferral'
)				


GO


