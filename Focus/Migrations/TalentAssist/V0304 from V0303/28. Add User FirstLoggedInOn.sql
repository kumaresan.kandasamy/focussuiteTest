-- Add column FirstLoggedInOn of type DateTime to table Data.application.user
ALTER TABLE [Data.application.user] ADD [FirstLoggedInOn] DATETIME NULL;
