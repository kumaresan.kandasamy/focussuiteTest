IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.EmployerAction' AND COLUMN_NAME = 'JobOrdersCreated')
BEGIN
	ALTER TABLE [Report.EmployerAction] ADD JobOrdersCreated INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.EmployerAction' AND COLUMN_NAME = 'JobOrdersPosted')
BEGIN
	ALTER TABLE [Report.EmployerAction] ADD JobOrdersPosted INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.EmployerAction' AND COLUMN_NAME = 'JobOrdersPutOnHold')
BEGIN
	ALTER TABLE [Report.EmployerAction] ADD JobOrdersPutOnHold INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.EmployerAction' AND COLUMN_NAME = 'JobOrdersRefreshed')
BEGIN
	ALTER TABLE [Report.EmployerAction] ADD JobOrdersRefreshed INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.EmployerAction' AND COLUMN_NAME = 'JobOrdersClosed')
BEGIN
	ALTER TABLE [Report.EmployerAction] ADD JobOrdersClosed INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.EmployerAction' AND COLUMN_NAME = 'InvitationsSent')
BEGIN
	ALTER TABLE [Report.EmployerAction] ADD InvitationsSent INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.EmployerAction' AND COLUMN_NAME = 'JobSeekersNotHired')
BEGIN
	ALTER TABLE [Report.EmployerAction] ADD JobSeekersNotHired INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.EmployerAction' AND COLUMN_NAME = 'SelfReferrals')
BEGIN
	ALTER TABLE [Report.EmployerAction] ADD SelfReferrals INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.EmployerAction' AND COLUMN_NAME = 'StaffReferrals')
BEGIN
	ALTER TABLE [Report.EmployerAction] ADD StaffReferrals INT NOT NULL DEFAULT(0)
END
GO

/*
IF EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.EmployerAction' AND COLUMN_NAME = 'JobOrdersOpen')
BEGIN
	DECLARE @Sql NVARCHAR(500)

	SELECT @Sql = 'ALTER TABLE [' + T.name + '] DROP CONSTRAINT [' + D.Name + ']'
	FROM sys.tables T
	INNER JOIN sys.default_constraints D
		ON D.parent_object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.object_id = T.object_id
		AND C.column_id = D.parent_column_id
	WHERE T.name = 'Report.EmployerAction'
	AND C.name = 'JobOrdersOpen'

	IF @Sql IS NOT NULL
		EXECUTE sp_executeSql @Sql
		
	ALTER TABLE [Report.EmployerAction] DROP COLUMN JobOrdersOpen
END
GO

IF EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.EmployerAction' AND COLUMN_NAME = 'JobSeekersRejected')
BEGIN
	DECLARE @Sql NVARCHAR(500)

	SELECT @Sql = 'ALTER TABLE [' + T.name + '] DROP CONSTRAINT [' + D.Name + ']'
	FROM sys.tables T
	INNER JOIN sys.default_constraints D
		ON D.parent_object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.object_id = T.object_id
		AND C.column_id = D.parent_column_id
	WHERE T.name = 'Report.EmployerAction'
	AND C.name = 'JobSeekersRejected'

	IF @Sql IS NOT NULL
		EXECUTE sp_executeSql @Sql
		
	ALTER TABLE [Report.EmployerAction] DROP COLUMN JobSeekersRejected
END
GO

IF EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.EmployerAction' AND COLUMN_NAME = 'ResumesReferred')
BEGIN
	DECLARE @Sql NVARCHAR(500)

	SELECT @Sql = 'ALTER TABLE [' + T.name + '] DROP CONSTRAINT [' + D.Name + ']'
	FROM sys.tables T
	INNER JOIN sys.default_constraints D
		ON D.parent_object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.object_id = T.object_id
		AND C.column_id = D.parent_column_id
	WHERE T.name = 'Report.EmployerAction'
	AND C.name = 'ResumesReferred'

	IF @Sql IS NOT NULL
		EXECUTE sp_executeSql @Sql
		
	ALTER TABLE [Report.EmployerAction] DROP COLUMN ResumesReferred
END
GO
*/

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.EmployerOffice]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Report.EmployerOffice](
	[Id] [bigint] NOT NULL,
	[OfficeName] [nvarchar](100) NOT NULL,
	[OfficeId] [bigint] NULL,
	[EmployerId] [bigint] NOT NULL DEFAULT(0),
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE referenced_object_id = OBJECT_ID(N'[dbo].[Report.Employer]') AND parent_object_id = OBJECT_ID(N'[dbo].[Report.EmployerOffice]'))
ALTER TABLE [dbo].[Report.EmployerOffice]  WITH CHECK ADD FOREIGN KEY([EmployerId])
REFERENCES [dbo].[Report.Employer] ([Id])
GO

