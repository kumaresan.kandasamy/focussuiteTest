IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Report.LookupJobSeekerOfficeView]'))
DROP VIEW [dbo].[Report.LookupJobSeekerOfficeView]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Report.LookupJobSeekerCountyView]'))
DROP VIEW [dbo].[Report.LookupJobSeekerCountyView]
GO
