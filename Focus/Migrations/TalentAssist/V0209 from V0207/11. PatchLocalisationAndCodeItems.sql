BEGIN TRAN

DECLARE @StartId BIGINT
DECLARE @NumberRowsToInsert BIGINT
DECLARE @MaxNextId BIGINT
DECLARE @CodeGroupCount INT
DECLARE @CodeItemCount INT
DECLARE @LocalisationItemCount INT

-- Declare table variables to capture the data we want to push to the db
DECLARE @Group TABLE (GroupKey NVARCHAR(200))
DECLARE @Item TABLE (GroupKey NVARCHAR(200), ItemKey NVARCHAR(200), ParentKey NVARCHAR(200) )
DECLARE @Local  TABLE (LocalKey NVARCHAR(200), Value NVARCHAR(MAX))

INSERT INTO @Group VALUES ('SecurityQuestions')

INSERT INTO @Item VALUES 
 ('SecurityQuestions', 'SecurityQuestions.WhatIsYourFavoriteColor?' , NULL),
 ('SecurityQuestions', 'SecurityQuestions.WhatIsYourMother''sMaidenName?' , NULL),
 ('SecurityQuestions', 'SecurityQuestions.WhatIsYourPet''sName?' , NULL),
 ('SecurityQuestions', 'SecurityQuestions.WhatStateWereYouBornIn?' , NULL),
 ('SecurityQuestions', 'SecurityQuestions.WhatWasTheFirstCarYouOwned?' , NULL),
 ('SecurityQuestions', 'SecurityQuestions.WhatCityOrTownWereYouBornIn?' , NULL),
 ('StaffIssues', 'StaffIssues.FiltersProfanityCriminalBackground' , 'FeedbackIssues.StaffIssues')
 
-- Determine the number of rows to create
SELECT @CodeGroupCount = COUNT(*)  FROM @Group 
SELECT @CodeItemCount = COUNT(*)  FROM @Item 
SELECT @LocalisationItemCount = COUNT(*)  FROM @Item 
-- 1 set for CodeGroup then a set for the LocalisationItem
-- 1 set for CodeItem, CodeGroupItem & LocalisationItem
SET @NumberRowsToInsert = (@CodeGroupCount) + (@CodeItemCount * 2) + @LocalisationItemCount + 1

-- Update the Next ID to be >= the number of rows we expect
UPDATE KeyTable SET NextId = NextId + @NumberRowsToInsert
SELECT @MaxNextId = NextId FROM KeyTable
SET @StartId = @MaxNextId - @NumberRowsToInsert

-- Create the CodeGroups from @Group
INSERT INTO CodeGroup (Id, [Key])
SELECT @StartId + ROW_NUMBER() OVER (ORDER BY GroupKey DESC), GroupKey FROM @Group
SET @StartId = @StartId + @CodeGroupCount

-- Create the CodeItems from @Item
INSERT INTO CodeItem (Id, [Key], IsSystem, ParentKey)
SELECT @StartId + ROW_NUMBER() OVER (ORDER BY ItemKey DESC), ItemKey, 0, ParentKey  FROM (SELECT DISTINCT ItemKey, ParentKey FROM @Item) AS Tmp
SET @StartId = @StartId + @CodeItemCount

UPDATE CodeItem SET IsSystem = 1 WHERE [Key] = 'StaffIssues.FiltersProfanityCriminalBackground'

-- Create the link between CodeGroup & CodeItem from the @Item table
INSERT INTO CodeGroupItem (Id, DisplayOrder, CodeGroupId, CodeItemId)
SELECT  @StartId + ROW_NUMBER() OVER (ORDER BY ItemKey DESC), 0, cg.id, ci.id
FROM @Item i
INNER JOIN CodeGroup cg ON i.GroupKey = cg.[Key] 
INNER JOIN CodeItem ci ON i.ItemKey = ci.[Key]
ORDER BY i.GroupKey, i.ItemKey
SET @StartId = @StartId + @CodeItemCount

--Localisaation
INSERT INTO @Local VALUES 
( 'System.Email.ExplorerRegistration.Subject', '#FOCUSEXPLORER# Registration - #FULLNAME#'),
( 'System.Email.ExplorerRegistration.Body', 'The following user registered in Focus Explorer on #NOWDATETIME#.    Firstname: #FIRSTNAME#  Lastname: #LASTNAME#  Email address: #EMAILADDRESS#  Phone number: #PHONENUMBER#'),
( 'Global.ExplorerDisclaimer.Text', '<div class="heading">Welcome to Phoenix Career Explorer!</div>The Phoenix Career Explorer Tool allows you to explore specific job titles, employers, and programs of study as they relate to salary and hiring trends, typical education, skills, experience levels, and career trajectories in their selected geographic area. Please select one of three paths below to begin exploring. Please note that it is your responsibility to research which programs are available from University of Phoenix and to determine whether those programs and your current or proposed course of study is appropriate for your career goals.'),
( 'System.Email.TalentUserActivation.Subject', 'Notification of #FOCUSTALENT# User Account Validation'),
( 'System.Email.TalentUserActivation.Body', 'Dear {0}:        You have logged into #FOCUSTALENT# for the first time, as a security measure please click on the link below to validate your account.       {1}        If you have problems with the above link, please try the following:      1. Select and copy this entire link: {1}    2. Open a browser window and paste the link in the address bar.    3. Click Go or, on your keyboard, press Enter or Return.      If you encounter any problems, please contact our help desk staff at #SUPPORTPHONE# or email us at #SUPPORTEMAIL#.'),
( 'SecurityQuestions.WhatIsYourFavoriteColor?', 'What is your favorite color?'),
( 'SecurityQuestions.WhatIsYourMother''sMaidenName?', 'What is your mother''s maiden name?'),
( 'SecurityQuestions.WhatIsYourPet''sName?', 'What is your pet''s name?'),
( 'SecurityQuestions.WhatStateWereYouBornIn?', 'What state were you born in?'),
( 'SecurityQuestions.WhatWasTheFirstCarYouOwned?', 'What was the first car you owned?'),
( 'SecurityQuestions.WhatCityOrTownWereYouBornIn?', 'What city or town were you born in?'),
( 'StaffIssues.FiltersProfanityCriminalBackground', 'Filters � profanity, criminal background')


-- Create the CodeItems LocalisationItems from @Item
INSERT INTO LocalisationItem (Id, [Key], Value, LocalisationId, ContextKey, Localised)
SELECT @StartId + ROW_NUMBER() OVER (ORDER BY LocalKey DESC), LocalKey, Value, 12090, '', 0 FROM @Local
SET @StartId = @StartId + @LocalisationItemCount

--EmailTemplate
INSERT INTO EmailTemplate VALUES (@StartId + 1, 13, 'Position about to expire', '#FREETEXT# Currently, this post is approaching 45 days as an active opening.    Our experience with today�s job market indicates that many job seekers search only for openings that are posted within the last 30 days.  To ensure that your job remains viable, we encourage you to Refresh your posting, as often as necessary. A Refresh icon appears for each post in your Active job listing.        Please follow this link to log in and Refresh this post:  #JOBLINKURL#       Thank you,     Focus|Talent Support.#EMPLOYERNAME#', 'Dear #RECIPIENTNAME#', null )


UPDATE LocalisationItem SET 
[Value] = 'Your job for the position of {0} posted on {1} is set to close on {2}.' 
WHERE [Key] = 'AlertMessage.JobExpiring'

UPDATE LocalisationItem SET 
[Value] = 'Dear {0}:

Thank you for registering with #FOCUSTALENT#. We have reviewed your application and validated your account. You may now create new job postings, review resume matches, notify candidates by email, manage your job applicants, and create candidate scouts with daily or weekly email alerts to your inbox.

{1}

If you have problems with the above link, please try the following:
1. Select and copy this entire link: (display authentication link)
2. Open a browser window and paste the link in the address bar.
3. Click Go or, on your keyboard, press Enter or Return.
If you encounter any problems, please contact our help desk staff at #SUPPORTPHONE# or email us at #SUPPORTEMAIL#.
'
WHERE [Key] = 'System.Email.AccountActivation.Body'

UPDATE LocalisationItem SET 
[Value] = 'Dear {0}:    

You have logged into #FOCUSASSIST# for the first time, as a security measure please click on the link below to validate your account.   

{1}    

If you have problems with the above link, please try the following:  

1. Select and copy this entire link: {1}  
2. Open a browser window and paste the link in the address bar.  
3. Click Go or, on your keyboard, press Enter or Return.  

If you encounter any problems, please contact our help desk staff at #SUPPORTPHONE# or email us at #SUPPORTEMAIL#.' 
WHERE [Key] = 'System.Email.AssistUserActivation.Body'

UPDATE LocalisationItem SET 
[Value] = 'This is the first time you have tried to log into #FOCUSASSIST#, as a security measure you will be sent a validation email.' 
WHERE [Key] = 'System.Email.AssistUserActivation.EmailWarning'

UPDATE LocalisationItem SET 
[Value] = 'Notification of #FOCUSASSIST# User Account Validation' 
WHERE [Key] = 'System.Email.AssistUserActivation.Subject'

UPDATE LocalisationItem SET 
[Value] = 'The following job order has been automatically closed as it has reached it''s closing date.
  
  Job order title; {0}
  Job order ID;	{1}
  Job order closing date; {2}
  DBA name; {3}' 
WHERE [Key] = 'System.Email.JobExpired.Body'

UPDATE LocalisationItem SET 
[Value] = 'The following job order will expire in {4} days.
  To extend the closing date on your posting, please log in to your #FOCUSTALENT# account and run the Refresh action found against the job order.
  
  Job order title; {0}
  Job order ID;	{1}
  Job order closing date; {2}
  DBA name; {3}' 
WHERE [Key] = 'System.Email.JobExpiring.Body'

UPDATE LocalisationItem SET 
[Value] = 'The following user registered in Focus Explorer on #NOWDATETIME#.

Firstname: #FIRSTNAME#
Lastname: #LASTNAME#
Email address: #EMAILADDRESS#
Phone number: #PHONENUMBER#'
WHERE [Key] = 'System.Email.ExplorerRegistration.Body'

UPDATE LocalisationItem SET 
[Value] = 'Dear {0}:    

You have logged into #FOCUSTALENT# for the first time, as a security measure please click on the link below to validate your account.   

{1}    

If you have problems with the above link, please try the following:  

1. Select and copy this entire link: {1}  
2. Open a browser window and paste the link in the address bar.  
3. Click Go or, on your keyboard, press Enter or Return.  

If you encounter any problems, please contact our help desk staff at #SUPPORTPHONE# or email us at #SUPPORTEMAIL#.'
WHERE [Key] = 'System.Email.TalentUserActivation.Body'



commit


