-- Update the model

/*
Run this script on:

        10.0.1.35.FocusDev    -  This database will be modified

to synchronize it with:

        10.0.1.35.FocusUnit

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.0.0 from Red Gate Software Ltd at 13/12/2012 19:53:17

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[Job]'
GO
ALTER TABLE [dbo].[Job] ADD
[BusinessUnitDescriptionId] [bigint] NULL,
[BusinessUnitLogoId] [bigint] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[BusinessUnit]'
GO
ALTER TABLE [dbo].[BusinessUnit] ADD
[Url] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OwnershipTypeId] [bigint] NOT NULL CONSTRAINT [DF__BusinessU__Owner__507BE13E] DEFAULT ((0)),
[IndustrialClassification] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BusinessU__Indus__51700577] DEFAULT (''),
[PrimaryPhone] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BusinessU__Prima__526429B0] DEFAULT (''),
[PrimaryPhoneExtension] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrimaryPhoneType] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AlternatePhone1] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AlternatePhone1Type] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AlternatePhone2] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AlternatePhone2Type] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[JobPostingReferralView]'
GO
EXEC sp_refreshview N'[dbo].[JobPostingReferralView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[JobView]'
GO
EXEC sp_refreshview N'[dbo].[JobView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[ExpiringJobView]'
GO
EXEC sp_refreshview N'[dbo].[ExpiringJobView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[EmployeeBusinessUnit]'
GO
ALTER TABLE [dbo].[EmployeeBusinessUnit] ADD
[Default] [bit] NOT NULL CONSTRAINT [DF__EmployeeB__Defau__7795AE5F] DEFAULT ((0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BusinessUnitAddress]'
GO
CREATE TABLE [dbo].[BusinessUnitAddress]
(
[Id] [bigint] NOT NULL,
[Line1] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Line2] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Line3] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TownCity] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BusinessU__TownC__581D0306] DEFAULT (''),
[CountyId] [bigint] NULL,
[PostcodeZip] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BusinessU__Postc__5911273F] DEFAULT (''),
[StateId] [bigint] NOT NULL CONSTRAINT [DF__BusinessU__State__5A054B78] DEFAULT ((0)),
[CountryId] [bigint] NOT NULL CONSTRAINT [DF__BusinessU__Count__5AF96FB1] DEFAULT ((0)),
[IsPrimary] [bit] NOT NULL CONSTRAINT [DF__BusinessU__IsPri__5BED93EA] DEFAULT ((0)),
[PublicTransitAccessible] [bit] NOT NULL CONSTRAINT [DF__BusinessU__Publi__5CE1B823] DEFAULT ((0)),
[BusinessUnitId] [bigint] NOT NULL CONSTRAINT [DF__BusinessU__Busin__5DD5DC5C] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__Business__3214EC075634BA94] on [dbo].[BusinessUnitAddress]'
GO
ALTER TABLE [dbo].[BusinessUnitAddress] ADD CONSTRAINT [PK__Business__3214EC075634BA94] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BusinessUnitDescription]'
GO
CREATE TABLE [dbo].[BusinessUnitDescription]
(
[Id] [bigint] NOT NULL,
[Title] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BusinessU__Title__638EB5B2] DEFAULT (''),
[Description] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BusinessU__Descr__6482D9EB] DEFAULT (''),
[IsPrimary] [bit] NOT NULL CONSTRAINT [DF__BusinessU__IsPri__6576FE24] DEFAULT ((0)),
[BusinessUnitId] [bigint] NOT NULL CONSTRAINT [DF__BusinessU__Busin__666B225D] DEFAULT ((0)),
[EmployerDescriptionId] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BusinessU__Emplo__74B941B4] DEFAULT ('')
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__Business__3214EC0761A66D40] on [dbo].[BusinessUnitDescription]'
GO
ALTER TABLE [dbo].[BusinessUnitDescription] ADD CONSTRAINT [PK__Business__3214EC0761A66D40] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BusinessUnitLogo]'
GO
CREATE TABLE [dbo].[BusinessUnitLogo]
(
[Id] [bigint] NOT NULL,
[Name] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BusinessUn__Name__6C23FBB3] DEFAULT (''),
[Logo] [varbinary] (max) NOT NULL,
[BusinessUnitId] [bigint] NOT NULL CONSTRAINT [DF__BusinessU__Busin__6D181FEC] DEFAULT ((0)),
[EmployerLogoId] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BusinessU__Emplo__71DCD509] DEFAULT ('')
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__Business__3214EC076A3BB341] on [dbo].[BusinessUnitLogo]'
GO
ALTER TABLE [dbo].[BusinessUnitLogo] ADD CONSTRAINT [PK__Business__3214EC076A3BB341] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[JobActionEventView]'
GO
EXEC sp_refreshview N'[dbo].[JobActionEventView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[ApplicationView]'
GO
EXEC sp_refreshview N'[dbo].[ApplicationView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[ApplicationStatusLogView]'
GO
EXEC sp_refreshview N'[dbo].[ApplicationStatusLogView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[EmployeeSearchView]'
GO
EXEC sp_refreshview N'[dbo].[EmployeeSearchView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[JobSeekerReferralView]'
GO
EXEC sp_refreshview N'[dbo].[JobSeekerReferralView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[BusinessUnitAddress]'
GO
ALTER TABLE [dbo].[BusinessUnitAddress] ADD CONSTRAINT [FK__BusinessU__Busin__5ECA0095] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[BusinessUnit] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Job]'
GO
ALTER TABLE [dbo].[Job] ADD CONSTRAINT [FK__Job__BusinessUni__75AD65ED] FOREIGN KEY ([BusinessUnitDescriptionId]) REFERENCES [dbo].[BusinessUnitDescription] ([Id])
ALTER TABLE [dbo].[Job] ADD CONSTRAINT [FK__Job__BusinessUni__76A18A26] FOREIGN KEY ([BusinessUnitLogoId]) REFERENCES [dbo].[BusinessUnitLogo] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[BusinessUnitDescription]'
GO
ALTER TABLE [dbo].[BusinessUnitDescription] ADD CONSTRAINT [FK__BusinessU__Busin__675F4696] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[BusinessUnit] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[BusinessUnitLogo]'
GO
ALTER TABLE [dbo].[BusinessUnitLogo] ADD CONSTRAINT [FK__BusinessU__Busin__6E0C4425] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[BusinessUnit] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO


-- Migrate data

UPDATE 
	BusinessUnit
SET
	BusinessUnit.Url = Employer.Url,
	BusinessUnit.OwnershipTypeId = Employer.OwnershipTypeId,
	BusinessUnit.IndustrialClassification = Employer.IndustrialClassification,
	BusinessUnit.PrimaryPhone = Employer.PrimaryPhone,
	BusinessUnit.PrimaryPhoneExtension = Employer.PrimaryPhoneExtension,
	BusinessUnit.PrimaryPhoneType = Employer.PrimaryPhoneType,
	BusinessUnit.AlternatePhone1 = Employer.AlternatePhone1,
	BusinessUnit.AlternatePhone1Type = Employer.AlternatePhone1Type,
	BusinessUnit.AlternatePhone2 = Employer.AlternatePhone2,
	BusinessUnit.AlternatePhone2Type = Employer.AlternatePhone2Type
FROM 
	BusinessUnit BU
INNER JOIN 
	Employer
ON 
	BU.EmployerId = Employer.Id


DECLARE @NextId bigint
DECLARE @EndOfBlockNextId bigint
DECLARE @IdsRequired bigint

SELECT @IdsRequired = COUNT(Employee.Id) + 50 FROM Employee
INNER JOIN BusinessUnit ON Employee.EmployerId = BusinessUnit.EmployerId

UPDATE KeyTable SET NextId = NextId + @IdsRequired 

SELECT @EndOfBlockNextId = NextId FROM KeyTable

SELECT @NextId = @EndOfBlockNextId - @IdsRequired

INSERT INTO EmployeeBusinessUnit(Id, EmployeeId, BusinessUnitId, [Default])
SELECT ROW_NUMBER() OVER (ORDER BY Employee.Id) + @NextId, Employee.Id, BusinessUnit.Id, BusinessUnit.IsPrimary   
FROM Employee
INNER JOIN BusinessUnit ON Employee.EmployerId = BusinessUnit.EmployerId

SELECT @IdsRequired = COUNT(BusinessUnit.Id) + 50 
FROM BusinessUnit
INNER JOIN EmployerDescription ON BusinessUnit.EmployerId = EmployerDescription.EmployerId

UPDATE KeyTable SET NextId = NextId + @IdsRequired 

SELECT @EndOfBlockNextId = NextId FROM KeyTable

SELECT @NextId = @EndOfBlockNextId - @IdsRequired

INSERT INTO BusinessUnitDescription(Id, Title, [Description], IsPrimary, BusinessUnitId, EmployerDescriptionId)
SELECT ROW_NUMBER() OVER (ORDER BY BusinessUnit.Id) + @NextId, EmployerDescription.Title, EmployerDescription.[Description],
EmployerDescription.IsPrimary, BusinessUnit.Id, EmployerDescription.Id 
FROM BusinessUnit
INNER JOIN EmployerDescription ON BusinessUnit.EmployerId = EmployerDescription.EmployerId

SELECT @IdsRequired = COUNT(BusinessUnit.Id) + 50 
FROM BusinessUnit
INNER JOIN EmployerAddress ON BusinessUnit.EmployerId = EmployerAddress.EmployerId

UPDATE KeyTable SET NextId = NextId + @IdsRequired 

SELECT @EndOfBlockNextId = NextId FROM KeyTable

SELECT @NextId = @EndOfBlockNextId - @IdsRequired

INSERT INTO BusinessUnitAddress(Id, Line1, Line2, Line3, TownCity, CountyId, PostcodeZip, StateId, CountryId, IsPrimary, 
PublicTransitAccessible, BusinessUnitId)
SELECT ROW_NUMBER() OVER (ORDER BY BusinessUnit.Id) + @NextId, Line1, Line2, Line3, TownCity, CountyId, PostcodeZip, StateId, CountryId,
EmployerAddress.IsPrimary, PublicTransitAccessible, BusinessUnit.Id 
FROM BusinessUnit
INNER JOIN EmployerAddress ON BusinessUnit.EmployerId = EmployerAddress.EmployerId

SELECT @IdsRequired = COUNT(BusinessUnit.Id) + 50 
FROM BusinessUnit
INNER JOIN EmployerLogo ON BusinessUnit.EmployerId = EmployerLogo.EmployerId

UPDATE KeyTable SET NextId = NextId + @IdsRequired 

SELECT @EndOfBlockNextId = NextId FROM KeyTable

SELECT @NextId = @EndOfBlockNextId - @IdsRequired

INSERT INTO BusinessUnitLogo(Id, Name, Logo, BusinessUnitId, EmployerLogoId)
SELECT ROW_NUMBER() OVER (ORDER BY BusinessUnit.Id) + @NextId, EmployerLogo.Name, Logo, BusinessUnit.Id, EmployerLogo.Id 
FROM BusinessUnit
INNER JOIN EmployerLogo ON BusinessUnit.EmployerId = EmployerLogo.EmployerId	

UPDATE Job
SET BusinessUnitDescriptionId = BusinessUnitDescription.Id
FROM Job J
INNER JOIN BusinessUnitDescription ON J.BusinessUnitId = BusinessUnitDescription.BusinessUnitId 
AND J.EmployerDescriptionId  = BusinessUnitDescription.EmployerDescriptionId

UPDATE Job
SET BusinessUnitLogoId = BusinessUnitLogo.Id
FROM Job J
INNER JOIN BusinessUnitLogo ON J.BusinessUnitId = BusinessUnitLogo.BusinessUnitId 
AND J.EmployerLogoId  = BusinessUnitLogo.EmployerLogoId


-- Update the model

/*
Run this script on:

        10.0.1.35.FocusDev    -  This database will be modified

to synchronize it with:

        10.0.1.35.FocusUnit

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.0.0 from Red Gate Software Ltd at 15/12/2012 10:55:12

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [dbo].[EmployerLogo]'
GO
ALTER TABLE [dbo].[EmployerLogo] DROP CONSTRAINT[FK__EmployerL__Emplo__45BE5BA9]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[Job]'
GO
ALTER TABLE [dbo].[Job] DROP CONSTRAINT[FK__Job__EmployerLog__6AEFE058]
ALTER TABLE [dbo].[Job] DROP CONSTRAINT[FK__Job__EmployerDes__0CA5D9DE]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[EmployerDescription]'
GO
ALTER TABLE [dbo].[EmployerDescription] DROP CONSTRAINT[FK__EmployerD__Emplo__4D5F7D71]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[EmployerDescription]'
GO
ALTER TABLE [dbo].[EmployerDescription] DROP CONSTRAINT [PK__Employer__3214EC07489AC854]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[EmployerDescription]'
GO
ALTER TABLE [dbo].[EmployerDescription] DROP CONSTRAINT [DF__EmployerD__Title__4A8310C6]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[EmployerDescription]'
GO
ALTER TABLE [dbo].[EmployerDescription] DROP CONSTRAINT [DF__EmployerD__Descr__4B7734FF]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[EmployerDescription]'
GO
ALTER TABLE [dbo].[EmployerDescription] DROP CONSTRAINT [DF__EmployerD__Emplo__4C6B5938]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[EmployerDescription]'
GO
ALTER TABLE [dbo].[EmployerDescription] DROP CONSTRAINT [DF__EmployerD__IsPri__24B26D99]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[BusinessUnitDescription]'
GO
ALTER TABLE [dbo].[BusinessUnitDescription] DROP CONSTRAINT [DF__BusinessU__Emplo__74B941B4]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[BusinessUnitLogo]'
GO
ALTER TABLE [dbo].[BusinessUnitLogo] DROP CONSTRAINT [DF__BusinessU__Emplo__71DCD509]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[EmployerLogo]'
GO
ALTER TABLE [dbo].[EmployerLogo] DROP CONSTRAINT [DF__EmployerL__Emplo__44CA3770]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[EmployerDescription]'
GO
DROP TABLE [dbo].[EmployerDescription]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[EmployerLogo]'
GO
ALTER TABLE [dbo].[EmployerLogo] DROP
COLUMN [EmployerId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Job]'
GO
ALTER TABLE [dbo].[Job] DROP
COLUMN [EmployerLogoId],
COLUMN [EmployerDescriptionId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[JobPostingReferralView]'
GO
EXEC sp_refreshview N'[dbo].[JobPostingReferralView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[JobView]'
GO
EXEC sp_refreshview N'[dbo].[JobView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[ExpiringJobView]'
GO
EXEC sp_refreshview N'[dbo].[ExpiringJobView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[BusinessUnitDescription]'
GO
ALTER TABLE [dbo].[BusinessUnitDescription] DROP
COLUMN [EmployerDescriptionId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[BusinessUnitLogo]'
GO
ALTER TABLE [dbo].[BusinessUnitLogo] DROP
COLUMN [EmployerLogoId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[JobActionEventView]'
GO
EXEC sp_refreshview N'[dbo].[JobActionEventView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[ApplicationView]'
GO
EXEC sp_refreshview N'[dbo].[ApplicationView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[ApplicationStatusLogView]'
GO
EXEC sp_refreshview N'[dbo].[ApplicationStatusLogView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[EmployeeSearchView]'
GO
EXEC sp_refreshview N'[dbo].[EmployeeSearchView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[JobSeekerReferralView]'
GO
EXEC sp_refreshview N'[dbo].[JobSeekerReferralView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

-- Remove employer logo table

/*
Run this script on:

        10.0.1.35.FocusDev    -  This database will be modified

to synchronize it with:

        10.0.1.35.FocusUnit

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.0.0 from Red Gate Software Ltd at 16/12/2012 10:27:31

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [dbo].[BusinessUnitLogo]'
GO
ALTER TABLE [dbo].[BusinessUnitLogo] DROP CONSTRAINT[FK__BusinessU__Busin__6E0C4425]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[BusinessUnitLogo]'
GO
ALTER TABLE [dbo].[BusinessUnitLogo] DROP CONSTRAINT [DF__BusinessU__Busin__6D181FEC]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[BusinessUnitLogo]'
GO
ALTER TABLE [dbo].[BusinessUnitLogo] ALTER COLUMN [BusinessUnitId] [bigint] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[JobPostingReferralView]'
GO
EXEC sp_refreshview N'[dbo].[JobPostingReferralView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[JobView]'
GO
EXEC sp_refreshview N'[dbo].[JobView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[ExpiringJobView]'
GO
EXEC sp_refreshview N'[dbo].[ExpiringJobView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[JobActionEventView]'
GO
EXEC sp_refreshview N'[dbo].[JobActionEventView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[ApplicationView]'
GO
EXEC sp_refreshview N'[dbo].[ApplicationView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[ApplicationStatusLogView]'
GO
EXEC sp_refreshview N'[dbo].[ApplicationStatusLogView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[EmployeeSearchView]'
GO
EXEC sp_refreshview N'[dbo].[EmployeeSearchView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[JobSeekerReferralView]'
GO
EXEC sp_refreshview N'[dbo].[JobSeekerReferralView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[BusinessUnitLogo]'
GO
ALTER TABLE [dbo].[BusinessUnitLogo] ADD CONSTRAINT [FK__BusinessU__Busin__6E0C4425] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[BusinessUnit] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

-- Migrate data

INSERT INTO BusinessUnitLogo(Id, Name, Logo)
SELECT EmployerLogo.Id, EmployerLogo.Name, EmployerLogo.Logo
FROM EmployerLogo

-- Update the model

/*
Run this script on:

        10.0.1.35.FocusDev    -  This database will be modified

to synchronize it with:

        10.0.1.35.FocusUnit

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.0.0 from Red Gate Software Ltd at 16/12/2012 10:35:04

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping constraints from [dbo].[EmployerLogo]'
GO
ALTER TABLE [dbo].[EmployerLogo] DROP CONSTRAINT [PK__Employer__3214EC0741EDCAC5]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[EmployerLogo]'
GO
ALTER TABLE [dbo].[EmployerLogo] DROP CONSTRAINT [DF__EmployerLo__Name__43D61337]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[EmployerLogo]'
GO
DROP TABLE [dbo].[EmployerLogo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

-- Update views

/*
Run this script on:

        10.0.1.35.FocusDev    -  This database will be modified

to synchronize it with:

        10.0.1.35.FocusUnit

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.0.0 from Red Gate Software Ltd at 04/01/2013 10:53:39

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[EmployerAccountReferralView]'
GO

ALTER VIEW [dbo].[EmployerAccountReferralView]
AS
SELECT 
	Employee.Id,
	Employer.Id AS EmployerId,
	Employer.Name AS EmployerName,
	Employee.Id AS EmployeeId,
	Person.FirstName AS EmployeeFirstName,
	Person.LastName AS EmployeeLastName,
	[USER].CreatedOn AS AccountCreationDate,
	[dbo].[GetBusinessDays]([USER].CreatedOn, GETDATE()) AS TimeInQueue,
	BusinessUnitAddress.Line1 AS EmployerAddressLine1,
	BusinessUnitAddress.TownCity AS EmployerAddressTownCity,
	BusinessUnitAddress.StateId AS EmployerAddressStateId,
	BusinessUnitAddress.PostcodeZip AS EmployerAddressPostcodeZip,
	CASE 
		WHEN BusinessUnit.PrimaryPhoneType = 'Phone' THEN BusinessUnit.PrimaryPhone
		WHEN BusinessUnit.PrimaryPhoneType = 'Mobile' THEN BusinessUnit.PrimaryPhone
		WHEN BusinessUnit.AlternatePhone1Type = 'Phone' THEN BusinessUnit.AlternatePhone1
		WHEN BusinessUnit.AlternatePhone1Type = 'Mobile' THEN BusinessUnit.AlternatePhone1
		WHEN BusinessUnit.AlternatePhone2Type = 'Phone' THEN BusinessUnit.AlternatePhone2
		WHEN BusinessUnit.AlternatePhone2Type = 'Mobile' THEN BusinessUnit.AlternatePhone2
	END AS EmployerPhoneNumber,
	CASE 
		WHEN BusinessUnit.PrimaryPhoneType = 'Fax' THEN BusinessUnit.PrimaryPhone
		WHEN BusinessUnit.AlternatePhone1Type = 'Fax' THEN BusinessUnit.AlternatePhone1
		WHEN BusinessUnit.AlternatePhone2Type = 'Fax' THEN BusinessUnit.AlternatePhone2
	END AS EmployerFaxNumber,
	PhoneNumber.Number AS EmployeePhoneNumber,
	FaxNumber.Number AS EmployeeFaxNumber,
	BusinessUnit.Url AS EmployerUrl,
	Employer.FederalEmployerIdentificationNumber AS EmployerFederalEmployerIdentificationNumber,
	Employer.StateEmployerIdentificationNumber AS EmployerStateEmployerIdentificationNumber,
	BusinessUnit.IndustrialClassification AS EmployerIndustrialClassification,
	PersonAddress.Line1 AS EmployeeAddressLine1,
	PersonAddress.TownCity AS EmployeeAddressTownCity,
	PersonAddress.StateId AS EmployeeAddressStateId,
	PersonAddress.PostcodeZip AS EmployeeAddressPostcodeZip,
	Person.EmailAddress AS EmployeeEmailAddress
FROM 
	Employer WITH (NOLOCK)
	INNER JOIN Employee  WITH (NOLOCK) ON Employer.Id = Employee.EmployerId
	INNER JOIN Person  WITH (NOLOCK) ON Employee.PersonId = Person.Id
	INNER JOIN [User]  WITH (NOLOCK) ON Person.Id = [User].PersonId
	INNER JOIN dbo.EmployeeBusinessUnit WITH (NOLOCK) ON dbo.Employee.Id = dbo.EmployeeBusinessUnit.EmployeeId AND dbo.EmployeeBusinessUnit.[Default] = 1
	INNER JOIN dbo.BusinessUnit WITH (NOLOCK) ON dbo.EmployeeBusinessUnit.BusinessUnitId = dbo.BusinessUnit.Id
	INNER JOIN dbo.BusinessUnitAddress WITH (NOLOCK) ON dbo.BusinessUnit.Id = dbo.BusinessUnitAddress.BusinessUnitId AND dbo.BusinessUnitAddress.IsPrimary = 1
	LEFT OUTER JOIN PersonAddress  WITH (NOLOCK) ON Person.Id = PersonAddress.PersonId 
	LEFT OUTER JOIN PhoneNumber WITH (NOLOCK) ON Person.Id = PhoneNumber.PersonId AND (PhoneNumber.PhoneType = 0 OR PhoneNumber.PhoneType = 1) AND PhoneNumber.IsPrimary = 1
	LEFT OUTER JOIN PhoneNumber AS FaxNumber WITH (NOLOCK) ON Person.Id = PhoneNumber.PersonId AND PhoneNumber.PhoneType = 4
WHERE
	Employer.ApprovalStatus = 1 OR Employee.ApprovalStatus = 1

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[JobPostingReferralView]'
GO

ALTER VIEW [dbo].[JobPostingReferralView]
AS
SELECT 
	Job.Id,
	Job.JobTitle,
	Job.EmployerId AS EmployerId,
	BusinessUnit.Name AS EmployerName,
	Job.AwaitingApprovalOn,
	[dbo].[GetBusinessDays](Job.AwaitingApprovalOn, GETDATE()) AS TimeInQueue,
	Person.FirstName AS EmployeeFirstName,
	Person.LastName AS EmployeeLastName,
	Employee.Id AS EmployeeId
FROM
	Job WITH (NOLOCK)
	INNER JOIN BusinessUnit WITH (NOLOCK) ON Job.BusinessUnitId = BusinessUnit.Id
	LEFT OUTER JOIN Employee WITH (NOLOCK) ON Job.EmployeeId = Employee.Id
	LEFT OUTER JOIN Person WITH (NOLOCK) ON Employee.PersonId = Person.Id
WHERE 
	Job.ApprovalStatus = 1
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[LookupItemsView]'
GO
EXEC sp_refreshview N'[dbo].[LookupItemsView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[EmployeeSearchView]'
GO

ALTER VIEW [dbo].[EmployeeSearchView] AS
SELECT     
 dbo.Employee.Id,     
 dbo.Person.FirstName,     
 dbo.Person.LastName,    
 dbo.Person.EmailAddress,    
 dbo.PhoneNumber.Number AS PhoneNumber,    
 dbo.Employer.Id AS EmployerId,     
 dbo.Employer.Name AS EmployerName,     
 dbo.BusinessUnit.IndustrialClassification,     
 dbo.Employer.FederalEmployerIdentificationNumber,     
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 1)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 2)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 4)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 8)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 16)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 32)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 64)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 128)) AS WorkOpportunitiesTaxCreditHires,     
 dbo.Person.JobTitle,     
 SUM(DISTINCT(PostingFlags & 1)) +    
 SUM(DISTINCT(PostingFlags & 2)) +    
 SUM(DISTINCT(PostingFlags & 4)) +    
 SUM(DISTINCT(PostingFlags & 8)) AS PostingFlags,     
 Max(dbo.Job.ScreeningPreferences) AS ScreeningPreferences,    
 dbo.Employer.CreatedOn AS EmployerCreatedOn,    
 dbo.[User].[Enabled] AS AccountEnabled,    
 dbo.Employer.ApprovalStatus AS EmployerApprovalStatus,    
 dbo.Employee.ApprovalStatus AS EmployeeApprovalStatus,  
 dbo.BusinessUnitAddress.TownCity,  
 dbo.LookupItemsView.Value AS [State],
 dbo.BusinessUnit.Name AS BusinessUnitName
FROM      
 dbo.Person WITH (NOLOCK)    
 INNER JOIN dbo.Employee WITH (NOLOCK) ON dbo.Person.Id = dbo.Employee.PersonId    
 INNER JOIN dbo.Employer WITH (NOLOCK) ON dbo.Employee.EmployerId = dbo.Employer.Id  
 LEFT OUTER JOIN dbo.PhoneNumber WITH (NOLOCK) ON dbo.Person.Id = dbo.PhoneNumber.PersonId AND dbo.PhoneNumber.IsPrimary = 1    
 INNER JOIN dbo.[User] WITH (NOLOCK) ON dbo.Person.Id = dbo.[User].PersonId     
 INNER JOIN dbo.EmployeeBusinessUnit WITH (NOLOCK) ON dbo.Employee.Id = dbo.EmployeeBusinessUnit.EmployeeId
 INNER JOIN dbo.BusinessUnit WITH (NOLOCK) ON dbo.EmployeeBusinessUnit.BusinessUnitId = dbo.BusinessUnit.Id
 INNER JOIN dbo.BusinessUnitAddress WITH (NOLOCK) ON dbo.BusinessUnit.Id = dbo.BusinessUnitAddress.BusinessUnitId
 INNER JOIN dbo.LookupItemsView WITH (NOLOCK) ON dbo.BusinessUnitAddress.StateId = dbo.LookupItemsView.Id
 LEFT OUTER JOIN dbo.Job WITH (NOLOCK) ON dbo.BusinessUnit.Id = dbo.Job.BusinessUnitId
GROUP BY    
 dbo.Employee.Id,     
 dbo.Person.FirstName,     
 dbo.Person.LastName,    
 dbo.Person.EmailAddress,    
 dbo.PhoneNumber.Number,     
 dbo.Employer.Id,     
 dbo.Employer.Name,     
  dbo.BusinessUnit.IndustrialClassification,     
  dbo.Employer.FederalEmployerIdentificationNumber,    
  dbo.Person.JobTitle,    
  dbo.Employer.CreatedOn,    
  dbo.[User].[Enabled],    
  dbo.Employer.ApprovalStatus,    
  dbo.Employee.ApprovalStatus,  
 dbo.BusinessUnitAddress.TownCity,  
 dbo.LookupItemsView.Value,
 dbo.BusinessUnit.Name

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[JobSeekerReferralView]'
GO


ALTER VIEW [dbo].[JobSeekerReferralView]
AS
SELECT 
	dbo.CandidateApplication.Id, 
	dbo.Candidate.FirstName + ' ' + dbo.Candidate.LastName AS Name, 
	dbo.CandidateApplication.CreatedOn AS ReferralDate, 
    dbo.GetBusinessDays(dbo.CandidateApplication.CreatedOn, GETDATE()) AS TimeInQueue, 
    dbo.Job.JobTitle, 
    dbo.BusinessUnit.Name AS EmployerName, 
    dbo.Job.EmployerId AS EmployerId, 
    dbo.Job.Posting, dbo.Candidate.Id AS CandidateId, 
    dbo.Job.Id AS JobId, 
    dbo.Candidate.ExternalId AS CandidateExternalId, 
    dbo.CandidateApplication.ApprovalRequiredReason, 
    CASE WHEN dbo.Candidate.IsVeteran = 1 THEN 'Yes' WHEN dbo.Candidate.IsVeteran = 0 THEN 'No' ELSE '' END AS Veteran, 
    dbo.BusinessUnitAddress.TownCity AS Town, 
    dbo.LookupItemsView.Value AS State
FROM  dbo.Candidate WITH (NOLOCK) 
	INNER JOIN dbo.CandidateApplication WITH (NOLOCK) ON dbo.Candidate.Id = dbo.CandidateApplication.CandidateId 
	INNER JOIN dbo.Job WITH (NOLOCK) ON dbo.CandidateApplication.JobId = dbo.Job.Id 
	INNER JOIN dbo.BusinessUnit WITH (NOLOCK) ON dbo.Job.BusinessUnitId = dbo.BusinessUnit.Id 
	INNER JOIN dbo.BusinessUnitAddress ON dbo.BusinessUnit.Id = dbo.BusinessUnitAddress.BusinessUnitId AND BusinessUnitAddress.IsPrimary = 1
	INNER JOIN dbo.LookupItemsView ON dbo.BusinessUnitAddress.StateId = dbo.LookupItemsView.Id
WHERE 
	(dbo.CandidateApplication.ApplicationStatus = 1 AND dbo.CandidateApplication.ApprovalStatus = 1)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO







