/****** Object:  View [dbo].[JobActionEventView]    Script Date: 01/09/2013 15:58:57 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[JobActionEventView]'))
DROP VIEW [dbo].[JobActionEventView]
GO

/****** Object:  View [dbo].[JobActionEventView]    Script Date: 01/09/2013 15:59:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[JobActionEventView]
AS
SELECT ae.Id, j.Id AS JobId, ae.UserId, p.FirstName, p.LastName, ae.ActionedOn, at.Name AS ActionName, ae.AdditionalDetails
FROM  dbo.ActionType AS at INNER JOIN
               dbo.ActionEvent AS ae ON at.Id = ae.ActionTypeId INNER JOIN
               dbo.Job AS j ON ae.EntityId = j.Id INNER JOIN
               dbo.[User] AS u ON ae.UserId = u.Id INNER JOIN
               dbo.Person AS p ON u.PersonId = p.Id
WHERE (at.Name = 'PostJob') OR
               (at.Name = 'SaveJob') OR
               (at.Name = 'HoldJob') OR
               (at.Name = 'CloseJob') OR
               (at.Name = 'RefreshJob') OR
               (at.Name = 'ReactivateJob') OR
               (at.Name = 'CreateJob') OR
               (at.Name = 'ApprovePostingReferral')
UNION ALL
SELECT ae.Id, ca.JobId, ae.UserId, p.FirstName, p.LastName, ae.ActionedOn, at.Name AS ActionName, ae.AdditionalDetails
FROM  dbo.ActionType AS at INNER JOIN
               dbo.ActionEvent AS ae ON at.Id = ae.ActionTypeId INNER JOIN
               dbo.CandidateApplication AS ca ON ae.EntityId = ca.Id INNER JOIN
               dbo.[User] AS u ON ae.UserId = u.Id INNER JOIN
               dbo.Person AS p ON u.PersonId = p.Id
WHERE (at.Name = 'ApproveCandidateReferral') OR
               (at.Name = 'SelfReferred') OR
               (at.Name = 'DenyCandidateReferral') OR
               (at.Name = 'CreateCandidateApplication')


GO


