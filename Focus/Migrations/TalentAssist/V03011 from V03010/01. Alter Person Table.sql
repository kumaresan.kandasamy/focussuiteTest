IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Person' AND COLUMN_NAME = 'IsVeteran')
BEGIN
	ALTER TABLE [Data.Application.Person] 
		ADD IsVeteran BIT NULL
END
GO

UPDATE
	P
SET
	IsVeteran = R.IsVeteran
FROM
	[Data.Application.Person] P
INNER JOIN [Data.Application.Resume] R
	ON R.PersonId = P.Id
WHERE
	R.IsDefault = 1
	AND R.StatusId = 1