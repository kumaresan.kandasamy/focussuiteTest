IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.ApplicationStatusLogBasicView]'))
DROP VIEW [dbo].[Data.Application.ApplicationStatusLogBasicView]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Data.Application.ApplicationStatusLogBasicView]
AS
SELECT 
	StatusLog.Id AS Id, 
	StatusLog.ActionedOn AS ActionedOn, 
    StatusLog.NewStatus AS CandidateApplicationStatus, 
	[Application].ApplicationScore AS CandidateApplicationScore, 
    [Application].ApprovalStatus AS CandidateApprovalStatus,
    [Resume].PersonId,
    Posting.JobId
FROM  
	[Data.Core.StatusLog] AS StatusLog WITH (NOLOCK) 
INNER JOIN [Data.Application.Application] AS [Application] WITH (NOLOCK) 
	ON StatusLog.EntityId = [Application].Id 
INNER JOIN [Data.Application.Resume] AS [Resume] WITH (NOLOCK) 
	ON [Application].ResumeId = [Resume].Id 
INNER JOIN [Data.Application.Posting] AS Posting WITH (NOLOCK) 
	ON [Application].PostingId = Posting.Id
WHERE
	Posting.JobId IS NOT NULL
GO


