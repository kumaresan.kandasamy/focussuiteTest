/****** Object:  View [dbo].[Data.Application.CandidateView]    Script Date: 06/04/2014 17:33:22 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.CandidateView]'))
DROP VIEW [dbo].[Data.Application.CandidateView]
GO

/****** Object:  View [dbo].[Data.Application.CandidateView]    Script Date: 06/04/2014 17:33:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Data.Application.CandidateView]
AS
SELECT	Person.[Id],
		Person.FirstName, 
		Person.LastName, 
		Person.DateOfBirth, 
		Person.SocialSecurityNumber, 
        Person.EmailAddress, 
        [Resume].AddressLine1,
        [Resume].TownCity, 
        [Resume].StateId,
        [Resume].CountryId,
        [Resume].PostcodeZip, 
        [Resume].HomePhone,
        [Resume].[Id] AS DefaultResumeId,
        [Resume].[IsSearchable] as IsSearchable,
        [Resume].WordCount AS ResumeWordCount,
        [Resume].SkillCount AS ResumeSkillCount,
        [User].Id AS UserId,
        [User].CreatedOn,
        [User].ExternalId AS ClientId,
        [User].[Enabled],
        [User].LoggedInOn AS LastLoggedInOn
FROM  
	[dbo].[Data.Application.Person] AS Person WITH (NOLOCK)
INNER JOIN dbo.[Data.Application.Resume] AS [Resume] WITH (NOLOCK) 
	ON [Resume].PersonId = Person.Id 
	AND [Resume].IsDefault = 1
	AND [Resume].StatusId = 1
INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) 
	ON [User].PersonId = Person.Id
GO


