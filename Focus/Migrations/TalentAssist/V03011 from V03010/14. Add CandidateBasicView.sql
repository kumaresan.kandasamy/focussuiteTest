IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.CandidateBasicView]'))
DROP VIEW [dbo].[Data.Application.CandidateBasicView]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Data.Application.CandidateBasicView]
AS
SELECT	Person.[Id],
		Person.FirstName, 
		Person.LastName, 
        Person.EmailAddress, 
        [User].Id AS UserId,
        [User].CreatedOn,
        [User].ExternalId AS ClientId,
        [User].[Enabled],
        [User].LoggedInOn AS LastLoggedInOn
FROM  
	[dbo].[Data.Application.Person] AS Person WITH (NOLOCK)
INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) 
	ON [User].PersonId = Person.Id
WHERE EXISTS
(
	SELECT 
		Id 
	FROM 
		dbo.[Data.Application.Resume] AS [Resume] WITH (NOLOCK) 
	WHERE 
		[Resume].PersonId = Person.Id 
		AND [Resume].IsDefault = 1
		AND [Resume].StatusId = 1
)
GO


