ALTER TABLE [Library.SkillCategory]
ALTER COLUMN [Name] NVARCHAR(255)

GO

ALTER TABLE [Library.ProgramArea]
ADD IsClientData BIT NOT NULL DEFAULT 0

GO


ALTER TABLE [Library.ProgramArea]
ADD ClientDataTag NVARCHAR(255)

GO