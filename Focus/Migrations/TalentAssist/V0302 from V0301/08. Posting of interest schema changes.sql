
/****** Object:  Table [dbo].[Data.Application.JobPostingOfInterestSent]    Script Date: 08/30/2013 12:44:55 ******/
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.JobPostingOfInterestSent]') AND type in (N'U'))
BEGIN

	/****** Object:  Table [dbo].[Data.Application.JobPostingOfInterestSent]    Script Date: 08/30/2013 12:45:04 ******/
	SET ANSI_NULLS ON

	SET QUOTED_IDENTIFIER ON

	CREATE TABLE [dbo].[Data.Application.JobPostingOfInterestSent](
		[Id] [bigint] NOT NULL,
		[CreatedOn] [datetime] NOT NULL,
		[PostingId] [bigint] NOT NULL,
		[JobId] [bigint] NULL,
		[PersonId] [bigint] NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]


	ALTER TABLE [dbo].[Data.Application.JobPostingOfInterestSent]  WITH CHECK ADD FOREIGN KEY([JobId])
	REFERENCES [dbo].[Data.Application.Job] ([Id])

	ALTER TABLE [dbo].[Data.Application.JobPostingOfInterestSent]  WITH CHECK ADD FOREIGN KEY([PersonId])
	REFERENCES [dbo].[Data.Application.Person] ([Id])

	ALTER TABLE [dbo].[Data.Application.JobPostingOfInterestSent]  WITH CHECK ADD FOREIGN KEY([PostingId])
	REFERENCES [dbo].[Data.Application.Posting] ([Id])

END

/****** Object:  View [dbo].[Data.Application.JobPostingOfInterestSentView]    Script Date: 08/30/2013 16:39:17 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.JobPostingOfInterestSentView]'))
DROP VIEW [dbo].[Data.Application.JobPostingOfInterestSentView]
GO

/****** Object:  View [dbo].[Data.Application.JobPostingOfInterestSentView]    Script Date: 08/30/2013 16:39:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Data.Application.JobPostingOfInterestSentView]
AS
SELECT
	jp.Id,
	jp.CreatedOn AS SentOn,
	jp.PersonId,
	p.JobTitle,
	p.EmployerName,
	p.LensPostingId,
	j.JobStatus	
FROM
	[Data.Application.JobPostingOfInterestSent] jp
INNER JOIN [Data.Application.Posting] p on jp.PostingId = p.Id
LEFT JOIN [Data.Application.Job] j ON jp.JobId = j.Id

GO

