SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Update rows in [dbo].[Config.EmailTemplate]
UPDATE [dbo].[Config.EmailTemplate] SET [Subject]=N'Thankyou for your account request' WHERE [Id]=1114297
UPDATE [dbo].[Config.EmailTemplate] SET [Body]=N'Thank you for your application for the #JOBTITLE# position at #EMPLOYERNAME#. 

We are reviewing applicants and will get back to you if we believe you are a suitable match.

You can review this job through Focus/Career by clicking the following link: #JOBLINKURL#.

Regards,    
#SENDERNAME#' WHERE [Id]=1577651
UPDATE [dbo].[Config.EmailTemplate] SET [Subject]=N'Thank you for your application', [Body]=N'Thank you for your application for the #JOBTITLE# position at #EMPLOYERNAME#. We have been contacted by many qualified applicants and unfortunately, will not be considering you further for this position.

We thank you for your time, and wish you the best of luck in the future.

Regards,    
#SENDERNAME#' WHERE [Id]=1577652
UPDATE [dbo].[Config.EmailTemplate] SET [Body]=N'#EMPLOYERNAME# has indicated an interest in you for the following position at their company:

#JOBTITLE#    To apply for this position, please submit a resume through Focus/Career by clicking the following link: #JOBLINKURL#
We ask that you reply to this request within three days.

Sincerely,

#SENDERNAME#' WHERE [Id]=1577653
UPDATE [dbo].[Config.EmailTemplate] SET [Body]=N'#EMPLOYERNAME# has indicated an interest in you for a position at their company.

To apply for this position, please submit a resume through Focus/Career by clicking the following link: #JOBLINKURL#
We ask that you reply to this request within three days.

Sincerely,

#SENDERNAME#' WHERE [Id]=1577654
-- Operation applied to 5 rows out of 5



-- Add rows to [dbo].[Config.EmailTemplate]
SET IDENTITY_INSERT [dbo].[Config.EmailTemplate] ON

INSERT INTO [dbo].[Config.EmailTemplate] ([Id], [EmailTemplateType], [Subject], [Body], [Salutation], [Recipient]) VALUES (5517469, 28, N'Job posting that might interest you', N'The following posting(s) might be of interest to you and you should consider putting forward an application.

To apply, please submit a resume through Focus/Career by clicking on the following link(s)
#JOBLINKURL#


Regards, 
#SENDERNAME#', N'Dear #RECIPIENTNAME#', NULL)
INSERT INTO [dbo].[Config.EmailTemplate] ([Id], [EmailTemplateType], [Subject], [Body], [Salutation], [Recipient]) VALUES (5517470, 26, N'Invitation to Register with #FOCUSCAREER#', N'Welcome!

You are invited to register with #FOCUSCAREER#�. #FOCUSCAREER# matches students with jobs based on career pathways of people with similar skills � not just keywords.

To join #FOCUSCAREER#, please click on the link below to authenticate your email address.
#AUTHENTICATEURL#

Your unique PIN number is: #PINNUMBER#

This PIN is used as an initial method of identifying your account. After confirming your email address, you will be asked to complete an online registration, including setting a password for future access to #FOCUSCAREER#. 

If you encounter any problems, please contact our help desk staff at #SUPPORTPHONE# or email us at #SUPPORTEMAIL#.
', N'Dear #RECIPIENTNAME#', NULL)
INSERT INTO [dbo].[Config.EmailTemplate] ([Id], [EmailTemplateType], [Subject], [Body], [Salutation], [Recipient]) VALUES (5517471, 24, N'Inactivity warning for #FOCUSCAREER#', N'This email is being sent to you because you have not logged on to #FOCUSCAREER# for over 60 days.

Please log onto #FOCUSCAREER# within the next 14 days to be able to continue using the system.

#FOCUSCAREERURL#
', N'Dear #RECIPIENTNAME#', NULL)
INSERT INTO [dbo].[Config.EmailTemplate] ([Id], [EmailTemplateType], [Subject], [Body], [Salutation], [Recipient]) VALUES (5517472, 25, N'Resume received for similar employee', N'We recently received a resume for #MATCHEDCANDIDATES#, who is very similar to your current employee #EMPLOYEENAME#. A copy of #MATCHEDCANDIDATES#''s resume is attached to this email for your reference.

Regards,

#SENDERNAME#', N'Dear #RECIPIENTNAME#', NULL)

INSERT INTO [dbo].[Config.EmailTemplate] ([Id], [EmailTemplateType], [Subject], [Body], [Salutation], [Recipient]) VALUES (5517468, 27, N'Confirmation of  #FOCUSASSIST# account', N'A user account has been set up for you on #FOCUSASSIST#.

Your login credentials are as follows:

Username:  #USERNAME#
Password:  #PASSWORD#

You can change your password to something more memorable once you have logged in by navigating to "Account Settings" on your homepage and clicking "Change Password".

You can access #FOCUSASSIST# login page by clicking on the following link:
 #URL#

Regards,
 #SENDERNAME#', N'Hi  #RECIPIENTNAME#', NULL)

SET IDENTITY_INSERT [dbo].[Config.EmailTemplate] OFF
-- Operation applied to 1 rows out of 4
COMMIT TRANSACTION
GO