SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping extended properties'
GO
EXEC sp_dropextendedproperty N'MS_DiagramPane1', 'SCHEMA', N'dbo', 'VIEW', N'Library.OnetTaskView', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_dropextendedproperty N'MS_DiagramPaneCount', 'SCHEMA', N'dbo', 'VIEW', N'Library.OnetTaskView', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[Data.Application.Employer]'
GO
ALTER TABLE [dbo].[Data.Application.Employer] DROP CONSTRAINT[FK__Data.Appl__Assig__5D36BDDE]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[Data.Application.JobPostingOfInterestSent]'
GO
ALTER TABLE [dbo].[Data.Application.JobPostingOfInterestSent] DROP CONSTRAINT[FK__Data.Appl__Posti__52B92F6B]
ALTER TABLE [dbo].[Data.Application.JobPostingOfInterestSent] DROP CONSTRAINT[FK__Data.Appl__Perso__51C50B32]
ALTER TABLE [dbo].[Data.Application.JobPostingOfInterestSent] DROP CONSTRAINT[FK__Data.Appl__JobId__50D0E6F9]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore]'
GO
ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore] DROP CONSTRAINT[FK__Data.Appl__Perso__418EA369]
ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore] DROP CONSTRAINT[FK__Data.Appl__Recen__4282C7A2]
ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore] DROP CONSTRAINT[FK__Data.Appl__UserI__4376EBDB]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[Library.OnetROnet]'
GO
ALTER TABLE [dbo].[Library.OnetROnet] DROP CONSTRAINT[FK__Library.O__OnetI__61F08603]
ALTER TABLE [dbo].[Library.OnetROnet] DROP CONSTRAINT[FK__Library.O__ROnet__62E4AA3C]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Data.Application.JobPostingOfInterestSent]'
GO
ALTER TABLE [dbo].[Data.Application.JobPostingOfInterestSent] DROP CONSTRAINT [PK__Data.App__3214EC074EE89E87]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore]'
GO
ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore] DROP CONSTRAINT [PK__Data.App__3214EC073CC9EE4C]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore]'
GO
ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore] DROP CONSTRAINT [DF__Data.Appl__Perso__3EB236BE]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore]'
GO
ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore] DROP CONSTRAINT [DF__Data.Appl__Recen__3FA65AF7]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore]'
GO
ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore] DROP CONSTRAINT [DF__Data.Appl__UserI__409A7F30]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Library.OnetROnet]'
GO
ALTER TABLE [dbo].[Library.OnetROnet] DROP CONSTRAINT [PK__Library.__3214EC075C37ACAD]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Library.OnetROnet]'
GO
ALTER TABLE [dbo].[Library.OnetROnet] DROP CONSTRAINT [DF__Library.O__OnetR__5E1FF51F]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Library.OnetROnet]'
GO
ALTER TABLE [dbo].[Library.OnetROnet] DROP CONSTRAINT [DF__Library.O__ROnet__5F141958]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Library.OnetROnet]'
GO
ALTER TABLE [dbo].[Library.OnetROnet] DROP CONSTRAINT [DF__Library.O__OnetI__60083D91]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Library.OnetROnet]'
GO
ALTER TABLE [dbo].[Library.OnetROnet] DROP CONSTRAINT [DF__Library.O__ROnet__60FC61CA]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Data.Migration.JobSeekerRequest]'
GO
ALTER TABLE [dbo].[Data.Migration.JobSeekerRequest] DROP CONSTRAINT [DF__Data.Migr__Recor__324C5FD9]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Data.Migration.UserRequest]'
GO
ALTER TABLE [dbo].[Data.Migration.UserRequest] DROP CONSTRAINT [DF__Data.Migr__Recor__31583BA0]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Library.ProgramArea]'
GO
ALTER TABLE [dbo].[Library.ProgramArea] DROP CONSTRAINT [DF__Library.P__IsCli__54A177DD]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data.Application.Job]'
GO
ALTER TABLE [dbo].[Data.Application.Job] ADD
[OfficeId] [bigint] NULL,
[OfficeUnassigned] [bit] NULL,
[AssignedToId] [bigint] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN [HideWorkWeekOnPosting] [bit] NOT NULL
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN [DescriptionPath] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data.Application.Person]'
GO
ALTER TABLE [dbo].[Data.Application.Person] ADD
[AssignedToId] [bigint] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data.Application.Employer]'
GO
ALTER TABLE [dbo].[Data.Application.Employer] ADD
[OfficeId] [bigint] NULL,
[OfficeUnassigned] [bit] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data.Application.JobActionEventView]'
GO

ALTER VIEW [dbo].[Data.Application.JobActionEventView]
AS
SELECT	ae.Id, 
		j.Id AS JobId, 
		ae.UserId, 
		p.FirstName, 
		p.LastName, 
		ae.ActionedOn, 
		at.Name AS ActionName, 
		ae.AdditionalDetails,
		'' AS AdditionalName
FROM  dbo.[Data.Core.ActionType] AS at WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Core.ActionEvent] AS ae WITH (NOLOCK) ON at.Id = ae.ActionTypeId
	INNER JOIN dbo.[Data.Application.Job] AS j WITH (NOLOCK) ON ae.EntityId = j.Id 
	INNER JOIN dbo.[Data.Application.User] AS u WITH (NOLOCK) ON ae.UserId = u.Id 
	INNER JOIN dbo.[Data.Application.Person] AS p WITH (NOLOCK) ON u.PersonId = p.Id
WHERE at.Name IN (
	'PostJob',
	'SaveJob',
	'HoldJob',
	'CloseJob',
	'RefreshJob',
	'ReactivateJob',
	'CreateJob',
	'ApprovePostingReferral')
	
UNION ALL

SELECT 
	ae.Id, 
	po.JobId, 
	ae.UserId, 
	p.FirstName, 
	p.LastName, 
	ae.ActionedOn, 
	at.Name AS ActionName, 
	ae.AdditionalDetails,
	ISNULL(js.FirstName + ' ' + js.LastName, '') AS AdditionalName
FROM  dbo.[Data.Core.ActionType] AS at WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Core.ActionEvent] AS ae WITH (NOLOCK) ON at.Id = ae.ActionTypeId 
	INNER JOIN dbo.[Data.Application.Application] AS ca WITH (NOLOCK) ON ae.EntityId = ca.Id 
	INNER JOIN dbo.[Data.Application.User] AS u WITH (NOLOCK) ON ae.UserId = u.Id 
	INNER JOIN dbo.[Data.Application.Person] AS p WITH (NOLOCK) ON u.PersonId = p.Id 
	INNER JOIN dbo.[Data.Application.Posting] AS po WITH (NOLOCK) ON ca.PostingId = po.Id
	LEFT OUTER JOIN dbo.[Data.Application.Person] js WITH (NOLOCK) ON js.Id = ae.EntityIdAdditional02 
WHERE at.Name IN (
	'ApproveCandidateReferral', 
	'SelfReferred', 
	'DenyCandidateReferral', 
	'CreateCandidateApplication',
	'WaivedRequirements')



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data.Application.JobSeekerActivityActionView]'
GO

ALTER VIEW [dbo].[Data.Application.JobSeekerActivityActionView]
AS

SELECT ae.Id, 
	ae.UserId,
	p.LastName + ', ' + p.FirstName AS 'UserName',
	ae.ActionedOn,
	at.Name AS 'ActionType',
	COALESCE(j.JobTitle, j2.JobTitle, pos.JobTitle, j3.JobTitle)  AS 'JobTitle',
	COALESCE(bu.Name, bu2.Name, pos.EmployerName, bu1.Name) AS 'BusinessUnitName',
	COALESCE(js1.Id, js2.Id, js3.Id, js4.Id) AS 'JobSeekerId',
	ae.EntityIdAdditional01,
	ae.AdditionalDetails
FROM [dbo].[Data.Core.ActionEvent] ae WITH (NOLOCK)
	inner join [Data.Core.ActionType] at WITH (NOLOCK) on at.Id = ae.ActionTypeId
	inner join [Data.Application.User] u WITH (NOLOCK) on u.Id = ae.UserId
	inner join [Data.Application.Person] p WITH (NOLOCK) on p.Id = u.PersonId
	
	-- Application related actions have Application as EntityId
	left join [Data.Application.Application] a WITH (NOLOCK) on a.Id = ae.EntityId
	left join [Data.Application.Posting] po WITH (NOLOCK) on po.Id = a.PostingId
	left join [Data.Application.Job] j WITH (NOLOCK) on j.Id = po.JobId	
	left join [Data.Application.BusinessUnit] bu WITH (NOLOCK) on bu.Id = j.BusinessUnitId
	left join [Data.Application.Resume] r1 WITH (NOLOCK) on r1.Id = a.ResumeId
	left join [Data.Application.Person] js1 WITH (NOLOCK) on js1.Id = r1.PersonId
	
	-- SaveResume has Resume as EntityId
	left join [Data.Application.Resume] r2 WITH (NOLOCK) on r2.Id = ae.EntityId
	left join [Data.Application.Person] js2 WITH (NOLOCK) on js2.Id = r2.PersonId
	
	-- AssignActivity has Person as EntityId
	left join [Data.Application.Person] js3 WITH (NOLOCK) on js3.Id = ae.EntityId
	
	-- InviteJobSeekerToApply has JobId as EntityId and Additional as JobSeekerId
	left join [Data.Application.Job] j2 WITH (NOLOCK) on j2.Id = ae.EntityId
	left join [Data.Application.BusinessUnit] bu2 WITH (NOLOCK) on bu2.Id = j2.BusinessUnitId
	left join [Data.Application.Person] js4 WITH (NOLOCK) on js4.Id = ae.EntityIdAdditional01
	
	-- External referral takes info from Posting
	left join [Data.Application.Posting] pos WITH (NOLOCK) on pos.Id = ae.EntityId
	--
	left join [Data.Application.Job] j3 on j3.Id = ae.EntityIdAdditional01
	left join [Data.Application.BusinessUnit] bu1 WITH (NOLOCK) on bu1.Id = j3.BusinessUnitId
	
	
	

where at.Name IN (
	'SelfReferral',
	'SaveResume',
	'CreateNewResume',
	'InviteJobSeekerToApply',
	'UpdateApplicationStatusToRecommended',
	'UpdateApplicationStatusToFailedToShow',
	'UpdateApplicationStatusToHired',
	'UpdateApplicationStatusToInterviewScheduled',									
	'UpdateApplicationStatusToNewApplicant',
	'UpdateApplicationStatusToNotApplicable',
	'UpdateApplicationStatusToOfferMade',
	'UpdateApplicationStatusToNotHired',
	'UpdateApplicationStatusToUnderConsideration',
	'UpdateApplicationStatusToDidNotApply',
	'UpdateApplicationStatusToInterviewDenied',
	'UpdateApplicationStatusToRefusedOffer',
	'UpdateApplicationStatusToSelfReferred',
	'ExternalReferral',
	'AssignActivityToCandidate', 
	'StaffReferral',
	'CreateCandidateApplication'
)				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data.Application.Issues]'
GO
ALTER TABLE [dbo].[Data.Application.Issues] ADD
[LastLoggedInDateForAlert] [datetime] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data.Application.JobPostingOfInterestSent]'
GO
ALTER TABLE [dbo].[Data.Application.JobPostingOfInterestSent] ALTER COLUMN [PostingId] [bigint] NOT NULL
ALTER TABLE [dbo].[Data.Application.JobPostingOfInterestSent] ALTER COLUMN [PersonId] [bigint] NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__Data.App__3214EC07330B79E8] on [dbo].[Data.Application.JobPostingOfInterestSent]'
GO
ALTER TABLE [dbo].[Data.Application.JobPostingOfInterestSent] ADD CONSTRAINT [PK__Data.App__3214EC07330B79E8] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore]'
GO
ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore] ALTER COLUMN [PersonId] [bigint] NOT NULL
ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore] ALTER COLUMN [RecentlyPlacedId] [bigint] NOT NULL
ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore] ALTER COLUMN [UserId] [bigint] NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__Data.App__3214EC0708211BE3] on [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore]'
GO
ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore] ADD CONSTRAINT [PK__Data.App__3214EC0708211BE3] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Library.ProgramArea]'
GO
ALTER TABLE [dbo].[Library.ProgramArea] ALTER COLUMN [IsClientData] [bit] NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Data.Application.PersonOfficeMapper]'
GO
CREATE TABLE [dbo].[Data.Application.PersonOfficeMapper]
(
[Id] [bigint] NOT NULL,
[StateId] [bigint] NULL,
[PersonId] [bigint] NOT NULL CONSTRAINT [DF__Data.Appl__Perso__5E2AE217] DEFAULT ((0)),
[OfficeId] [bigint] NULL,
[OfficeUnassigned] [bit] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__Data.App__3214EC075C4299A5] on [dbo].[Data.Application.PersonOfficeMapper]'
GO
ALTER TABLE [dbo].[Data.Application.PersonOfficeMapper] ADD CONSTRAINT [PK__Data.App__3214EC075C4299A5] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data.Application.JobPostingReferralView]'
GO




ALTER VIEW [dbo].[Data.Application.JobPostingReferralView]
AS
SELECT 
	Job.Id,
	Job.JobTitle,
	Job.EmployerId AS EmployerId,
	BusinessUnit.Name AS EmployerName,
	Job.AwaitingApprovalOn,
	[dbo].[Data.Application.GetBusinessDays](Job.AwaitingApprovalOn, GETDATE()) AS TimeInQueue,
	Person.FirstName AS EmployeeFirstName,
	Person.LastName AS EmployeeLastName,
	Employee.Id AS EmployeeId,
	Job.OfficeId
FROM
	[Data.Application.Job] AS Job WITH (NOLOCK)
	INNER JOIN [Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON Job.BusinessUnitId = BusinessUnit.Id
	LEFT OUTER JOIN [Data.Application.Employee] AS Employee WITH (NOLOCK) ON Job.EmployeeId = Employee.Id
	LEFT OUTER JOIN [Data.Application.Person] AS Person WITH (NOLOCK) ON Employee.PersonId = Person.Id
WHERE 
	Job.ApprovalStatus = 1



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data.Application.EmployerAccountReferralView]'
GO




ALTER VIEW [dbo].[Data.Application.EmployerAccountReferralView]
AS
SELECT 
	Employee.Id,
	Employer.Id AS EmployerId,
	Employer.Name AS EmployerName,
	Employee.Id AS EmployeeId,
	Person.FirstName AS EmployeeFirstName,
	Person.LastName AS EmployeeLastName,
	[User].CreatedOn AS AccountCreationDate,
	[dbo].[Data.Application.GetBusinessDays]([User].CreatedOn, GETDATE()) AS TimeInQueue,
	BusinessUnitAddress.Line1 AS EmployerAddressLine1,
	BusinessUnitAddress.Line2 AS EmployerAddressLine2,
	BusinessUnitAddress.TownCity AS EmployerAddressTownCity,
	BusinessUnitAddress.StateId AS EmployerAddressStateId,
	BusinessUnitAddress.PostcodeZip AS EmployerAddressPostcodeZip,
	CASE 
		WHEN BusinessUnit.PrimaryPhoneType = 'Phone' THEN BusinessUnit.PrimaryPhone
		WHEN BusinessUnit.PrimaryPhoneType = 'Mobile' THEN BusinessUnit.PrimaryPhone
		WHEN BusinessUnit.AlternatePhone1Type = 'Phone' THEN BusinessUnit.AlternatePhone1
		WHEN BusinessUnit.AlternatePhone1Type = 'Mobile' THEN BusinessUnit.AlternatePhone1
		WHEN BusinessUnit.AlternatePhone2Type = 'Phone' THEN BusinessUnit.AlternatePhone2
		WHEN BusinessUnit.AlternatePhone2Type = 'Mobile' THEN BusinessUnit.AlternatePhone2
	END AS EmployerPhoneNumber,
	CASE 
		WHEN BusinessUnit.PrimaryPhoneType = 'Fax' THEN BusinessUnit.PrimaryPhone
		WHEN BusinessUnit.AlternatePhone1Type = 'Fax' THEN BusinessUnit.AlternatePhone1
		WHEN BusinessUnit.AlternatePhone2Type = 'Fax' THEN BusinessUnit.AlternatePhone2
	END AS EmployerFaxNumber,
	PhoneNumber.Number AS EmployeePhoneNumber,
	FaxNumber.Number AS EmployeeFaxNumber,
	BusinessUnit.Url AS EmployerUrl,
	Employer.FederalEmployerIdentificationNumber AS EmployerFederalEmployerIdentificationNumber,
	Employer.StateEmployerIdentificationNumber AS EmployerStateEmployerIdentificationNumber,
	BusinessUnit.IndustrialClassification AS EmployerIndustrialClassification,
	PersonAddress.Line1 AS EmployeeAddressLine1,
	PersonAddress.TownCity AS EmployeeAddressTownCity,
	PersonAddress.StateId AS EmployeeAddressStateId,
	PersonAddress.PostcodeZip AS EmployeeAddressPostcodeZip,
	Person.EmailAddress AS EmployeeEmailAddress,
	Employee.ApprovalStatus AS EmployeeApprovalStatus,
	Employer.ApprovalStatus AS EmployerApprovalStatus,
	Employer.OfficeId
FROM 
	[Data.Application.Employer] AS Employer WITH (NOLOCK)
	INNER JOIN [Data.Application.Employee] AS Employee  WITH (NOLOCK) ON Employer.Id = Employee.EmployerId
	INNER JOIN [Data.Application.Person] AS Person WITH (NOLOCK) ON Employee.PersonId = Person.Id
	INNER JOIN [Data.Application.User] AS [User]  WITH (NOLOCK) ON Person.Id = [User].PersonId
	INNER JOIN dbo.[Data.Application.EmployeeBusinessUnit] AS [EmployeeBusinessUnit] WITH (NOLOCK) ON Employee.Id = EmployeeBusinessUnit.EmployeeId AND EmployeeBusinessUnit.[Default] = 1
	INNER JOIN dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON EmployeeBusinessUnit.BusinessUnitId = BusinessUnit.Id
	INNER JOIN dbo.[Data.Application.BusinessUnitAddress] AS BusinessUnitAddress WITH (NOLOCK) ON BusinessUnit.Id = BusinessUnitAddress.BusinessUnitId AND BusinessUnitAddress.IsPrimary = 1
	LEFT OUTER JOIN [Data.Application.PersonAddress] AS PersonAddress WITH (NOLOCK) ON Person.Id = PersonAddress.PersonId 
	LEFT OUTER JOIN [Data.Application.PhoneNumber] AS PhoneNumber WITH (NOLOCK) ON Person.Id = PhoneNumber.PersonId AND (PhoneNumber.PhoneType = 0 OR PhoneNumber.PhoneType = 1) AND PhoneNumber.IsPrimary = 1
	LEFT OUTER JOIN [Data.Application.PhoneNumber] AS FaxNumber WITH (NOLOCK) ON Person.Id = PhoneNumber.PersonId AND PhoneNumber.PhoneType = 4
WHERE
	Employer.ApprovalStatus = 1 OR Employee.ApprovalStatus = 1 OR Employee.ApprovalStatus = 4 OR Employer.ApprovalStatus = 4








GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data.Application.JobView]'
GO



ALTER view [dbo].[Data.Application.JobView] as
SELECT j.Id, 
	j.EmployerId, 
	e.Name AS EmployerName, 
	j.JobTitle, 
	bu.Name AS BusinessUnitName, 
	j.BusinessUnitId, 
	j.JobStatus, 
	j.PostedOn, 
	j.ClosingOn, 
	j.HeldOn,   
    j.ClosedOn, 
    j.UpdatedOn, 
    ISNULL(cag.ApplicationCount, 0) AS ApplicationCount, 
    j.EmployeeId, j.ApprovalStatus, 
    ISNULL(crg.ReferralCount, 0) AS ReferralCount,   
    j.FederalContractor, 
    j.ForeignLabourCertificationH2A, 
    j.ForeignLabourCertificationH2B, 
    j.ForeignLabourCertificationOther, 
    j.CourtOrderedAffirmativeAction,   
    u.UserName, 
    p.FirstName AS HiringManagerFirstName, 
    p.LastName AS HiringManagerLastName, 
    j.CreatedOn, 
    j.YellowProfanityWords, 
    j.IsConfidential, 
    j.CreatedBy,
    j.UpdatedBy, 
    j.JobType,
    j.OfficeId,
    j.AssignedToId
FROM  dbo.[Data.Application.Job] AS j WITH (NOLOCK) 
	LEFT OUTER JOIN dbo.[Data.Application.Employee] AS employee WITH (NOLOCK) ON j.EmployeeId = employee.Id 
	LEFT OUTER JOIN dbo.[Data.Application.Person] AS p WITH (NOLOCK) ON employee.PersonId = p.Id 
	LEFT OUTER JOIN dbo.[Data.Application.User] AS u WITH (NOLOCK) ON employee.PersonId = u.PersonId 
	LEFT OUTER JOIN dbo.[Data.Application.Employer] AS e WITH (NOLOCK) ON j.EmployerId = e.Id 
	LEFT OUTER JOIN dbo.[Data.Application.BusinessUnit] AS bu WITH (NOLOCK) ON bu.Id = j.BusinessUnitId 
	LEFT OUTER JOIN (SELECT p.JobId, COUNT(1) AS ApplicationCount  
                    FROM dbo.[Data.Application.Application] AS ca WITH (NOLOCK) 
                    INNER JOIN dbo.[Data.Application.Posting] AS p WITH (NOLOCK) ON ca.PostingId = p.Id  
                    WHERE (ca.ApprovalStatus = 2)  
                    GROUP BY p.JobId) AS cag ON cag.JobId = j.Id 
	LEFT OUTER JOIN (SELECT p.JobId, COUNT(1) AS ReferralCount  
                    FROM dbo.[Data.Application.Application] AS ca WITH (NOLOCK) 
                    INNER JOIN dbo.[Data.Application.Posting] AS p WITH (NOLOCK) ON ca.PostingId = p.Id  
                    WHERE (ca.ApprovalStatus = 1)  
                    GROUP BY p.JobId) AS crg ON crg.JobId = j.Id  




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data.Application.CandidateIssueView]'
GO


ALTER VIEW [dbo].[Data.Application.CandidateIssueView]  
AS  
SELECT 
	p.Id, 
	p.FirstName, 
	p.LastName, 
	re.DateOfBirth, 
	p.SocialSecurityNumber, 
	re.TownCity, 
	re.StateId, 
	p.EmailAddress, 
	u.LastLoggedInOn,
	i.NoLoginTriggered,
	i.JobOfferRejectionTriggered,
	i.NotReportingToInterviewTriggered,  
    i.NotClickingOnLeadsTriggered,
    i.NotRespondingToEmployerInvitesTriggered,
    i.ShowingLowQualityMatchesTriggered,
    i.PostingLowQualityResumeTriggered,
    i.PostHireFollowUpTriggered,
    i.FollowUpRequested,
    i.NotSearchingJobsTriggered,
    pom.OfficeId,
    p.AssignedToId
FROM  dbo.[Data.Application.User] AS u WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Application.Person] AS p WITH (NOLOCK) ON u.PersonId = p.Id 
	LEFT JOIN dbo.[Data.Application.Resume] AS re WITH (NOLOCK) ON p.Id = re.PersonId AND re.IsDefault = 1 AND re.IsActive = 1
    LEFT JOIN dbo.[Data.Application.Issues] AS i WITH (NOLOCK) on p.Id = i.PersonId  
    LEFT JOIN dbo.[Data.Application.PersonOfficeMapper] AS pom WITH (NOLOCK) ON p.Id = pom.PersonId
WHERE (u.UserType & 4 = 4)





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data.Application.EmployeeSearchView]'
GO

ALTER VIEW [dbo].[Data.Application.EmployeeSearchView] AS
SELECT     
	Employee.Id,     
	Person.FirstName,     
	Person.LastName,    
	Person.EmailAddress,    
	PhoneNumber.Number AS PhoneNumber,    
	Employer.Id AS EmployerId,     
	Employer.Name AS EmployerName,     
	BusinessUnit.IndustrialClassification,     
	Employer.FederalEmployerIdentificationNumber,     
	NULL AS WorkOpportunitiesTaxCreditHires,     
	Person.JobTitle,     
	NULL AS PostingFlags,     
	NULL AS ScreeningPreferences,    
	Employer.CreatedOn AS EmployerCreatedOn,    
	[User].[Enabled] AS AccountEnabled,    
	Employer.ApprovalStatus AS EmployerApprovalStatus,    
	Employee.ApprovalStatus AS EmployeeApprovalStatus,  
	BusinessUnitAddress.TownCity,  
	BusinessUnitAddress.StateId,
	'' AS [State],
	BusinessUnit.Name AS BusinessUnitName,
	BusinessUnit.Id AS BusinessUnitId,
	[User].LoggedInOn AS LastLogin,
	EmployeeBusinessUnit.Id AS EmployeeBusinessUnitId
FROM      
	dbo.[Data.Application.Person] AS Person WITH (NOLOCK)    
	INNER JOIN dbo.[Data.Application.Employee] AS Employee WITH (NOLOCK) ON Person.Id = Employee.PersonId    
	INNER JOIN dbo.[Data.Application.Employer] AS Employer WITH (NOLOCK) ON Employee.EmployerId = Employer.Id  
	LEFT OUTER JOIN dbo.[Data.Application.PhoneNumber] AS PhoneNumber WITH (NOLOCK) ON Person.Id = PhoneNumber.PersonId AND PhoneNumber.IsPrimary = 1    
	INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON Person.Id = [User].PersonId     
	INNER JOIN dbo.[Data.Application.EmployeeBusinessUnit] AS EmployeeBusinessUnit WITH (NOLOCK) ON Employee.Id = EmployeeBusinessUnit.EmployeeId
	INNER JOIN dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON EmployeeBusinessUnit.BusinessUnitId = BusinessUnit.Id
	INNER JOIN dbo.[Data.Application.BusinessUnitAddress] AS BusinessUnitAddress WITH (NOLOCK) ON BusinessUnit.Id = BusinessUnitAddress.BusinessUnitId


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Library.OnetTaskView]'
GO

ALTER VIEW [dbo].[Library.OnetTaskView]
AS
SELECT 
	ot.Id,
	ot.OnetId,
	ot.[Key] AS TaskKey,
	oli.PrimaryValue AS Task,
	oli.SecondaryValue AS TaskPastTense,
	l.Culture AS Culture
FROM  
	dbo.[Library.OnetTask] AS ot WITH (NOLOCK)
	INNER JOIN  dbo.[Library.OnetLocalisationItem] AS oli WITH (NOLOCK) ON ot.[Key] = oli.[Key] 
	INNER JOIN [Library.Localisation] AS l WITH (NOLOCK) ON oli.LocalisationId = l.Id
	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Data.Application.PersonPostcodeMapper]'
GO
CREATE TABLE [dbo].[Data.Application.PersonPostcodeMapper]
(
[Id] [bigint] NOT NULL,
[PostcodeZip] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Data.Appl__Postc__035C66C6] DEFAULT (''),
[OwnedById] [bigint] NULL,
[PersonId] [bigint] NOT NULL CONSTRAINT [DF__Data.Appl__Perso__04508AFF] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__Data.App__3214EC0701741E54] on [dbo].[Data.Application.PersonPostcodeMapper]'
GO
ALTER TABLE [dbo].[Data.Application.PersonPostcodeMapper] ADD CONSTRAINT [PK__Data.App__3214EC0701741E54] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Library.ROnet]'
GO
ALTER TABLE [dbo].[Library.ROnet] ADD
[OnetId] [bigint] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Data.Application.HowToApplySent]'
GO
CREATE TABLE [dbo].[Data.Application.HowToApplySent]
(
[Id] [bigint] NOT NULL,
[CreatedOn] [datetime] NOT NULL,
[PostingId] [bigint] NOT NULL CONSTRAINT [DF__Data.Appl__Posti__23C93658] DEFAULT ((0)),
[JobId] [bigint] NULL CONSTRAINT [DF__Data.Appl__JobId__24BD5A91] DEFAULT ((0)),
[PersonId] [bigint] NOT NULL CONSTRAINT [DF__Data.Appl__Perso__25B17ECA] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__Data.App__3214EC0721E0EDE6] on [dbo].[Data.Application.HowToApplySent]'
GO
ALTER TABLE [dbo].[Data.Application.HowToApplySent] ADD CONSTRAINT [PK__Data.App__3214EC0721E0EDE6] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data.Migration.JobSeekerRequest]'
GO
ALTER TABLE [dbo].[Data.Migration.JobSeekerRequest] ALTER COLUMN [RecordType] [int] NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Data.Application.Office]'
GO
CREATE TABLE [dbo].[Data.Application.Office]
(
[Id] [bigint] NOT NULL,
[OfficeName] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Data.Appl__Offic__1FF8A574] DEFAULT (''),
[Line1] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Line2] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TownCity] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountyId] [bigint] NULL,
[StateId] [bigint] NOT NULL CONSTRAINT [DF__Data.Appl__State__20ECC9AD] DEFAULT ((0)),
[CountryId] [bigint] NULL,
[PostcodeZip] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Data.Appl__Postc__21E0EDE6] DEFAULT (''),
[AssignedPostcodeZip] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnassignedDefault] [bit] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__Data.App__3214EC071E105D02] on [dbo].[Data.Application.Office]'
GO
ALTER TABLE [dbo].[Data.Application.Office] ADD CONSTRAINT [PK__Data.App__3214EC071E105D02] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data.Migration.UserRequest]'
GO
ALTER TABLE [dbo].[Data.Migration.UserRequest] ALTER COLUMN [RecordType] [int] NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Library.OnetROnet]'
GO
ALTER TABLE [dbo].[Library.OnetROnet] ALTER COLUMN [OnetROnetVariance] [int] NOT NULL
ALTER TABLE [dbo].[Library.OnetROnet] ALTER COLUMN [ROnetOnetVariance] [int] NOT NULL
ALTER TABLE [dbo].[Library.OnetROnet] ALTER COLUMN [OnetId] [bigint] NOT NULL
ALTER TABLE [dbo].[Library.OnetROnet] ALTER COLUMN [ROnetId] [bigint] NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__Library.__3214EC072A4129BD] on [dbo].[Library.OnetROnet]'
GO
ALTER TABLE [dbo].[Library.OnetROnet] ADD CONSTRAINT [PK__Library.__3214EC072A4129BD] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[Library.OnetView]'
GO
EXEC sp_refreshview N'[dbo].[Library.OnetView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Library.CareerAreaJobDegreeView]'
GO
create view [dbo].[Library.CareerAreaJobDegreeView]
as
select
	NEWID() as Id,
	ca.Id as CareerAreaId,
	jca.JobId as JobId,
	del.Id as DegreeEducationLevelId,
	del.EducationLevel as EducationLevel,
	d.Id as DegreeId,
	d.IsClientData as IsClientData,
	d.ClientDataTag as ClientDataTag
from
	[Library.CareerArea] ca
inner join [Library.JobCareerArea] jca
	on ca.Id = jca.CareerAreaId
inner join [Library.JobDegreeEducationLevel] jdel
	on jca.JobId = jdel.JobId
inner join [Library.DegreeEducationLevel] del
	on jdel.DegreeEducationLevelId = del.Id
inner join [Library.Degree] d
	on del.DegreeId = d.Id
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data.Application.StudentAlumniIssueView]'
GO


ALTER VIEW [dbo].[Data.Application.StudentAlumniIssueView]  
AS  
SELECT 
	p.Id,
	p.FirstName, 
	p.LastName,
	p.DateOfBirth, 
	p.SocialSecurityNumber, 			 
	p.EmailAddress, 
	u.LastLoggedInOn,
	i.NoLoginTriggered,  
    i.JobOfferRejectionTriggered,  
    i.NotReportingToInterviewTriggered,  
    i.NotClickingOnLeadsTriggered,  
    i.NotRespondingToEmployerInvitesTriggered,  
    i.ShowingLowQualityMatchesTriggered,  
    i.PostingLowQualityResumeTriggered,  
    i.PostHireFollowUpTriggered,  
    i.FollowUpRequested,  
    i.NotSearchingJobsTriggered,
    p.EnrollmentStatus,
    p.ProgramAreaId,
    p.DegreeId,
    u.UserType,
    u.ExternalId
FROM  
	dbo.[Data.Application.User] AS u WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Application.Person] AS p WITH (NOLOCK) ON u.PersonId = p.Id 
	LEFT JOIN dbo.[Data.Application.Issues] i on p.Id = i.PersonId         


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data.Application.JobSeekerReferralView]'
GO


ALTER VIEW [dbo].[Data.Application.JobSeekerReferralView]
AS
SELECT 
	[Application].Id, 
	[Resume].FirstName + ' ' + [Resume].LastName AS Name, 
	[Application].CreatedOn AS ReferralDate, 
    dbo.[Data.Application.GetBusinessDays]([Application].CreatedOn, GETDATE()) AS TimeInQueue, 
    Job.JobTitle, 
    BusinessUnit.Name AS EmployerName, 
    Job.EmployerId AS EmployerId, 
    Job.PostingHtml AS Posting, 
    [Resume].PersonId AS CandidateId, 
    Job.Id AS JobId, 
    [Application].ApprovalRequiredReason, 
    CASE WHEN [Resume].IsVeteran = 1 THEN 'Yes' WHEN [Resume].IsVeteran = 0 THEN 'No' ELSE '' END AS Veteran, 
    BusinessUnitAddress.TownCity AS Town, 
    '' AS [State],
    BusinessUnitAddress.StateID,
    pmo.OfficeId
FROM  
	dbo.[Data.Application.Application] AS [Application] WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Application.Posting] AS Posting WITH (NOLOCK) ON [Application].PostingId = Posting.Id
	INNER JOIN dbo.[Data.Application.Job] AS Job WITH (NOLOCK) ON Posting.JobId = Job.Id 
	INNER JOIN dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON Job.BusinessUnitId = BusinessUnit.Id 
	INNER JOIN dbo.[Data.Application.BusinessUnitAddress] AS BusinessUnitAddress ON BusinessUnit.Id = BusinessUnitAddress.BusinessUnitId AND BusinessUnitAddress.IsPrimary = 1
	INNER JOIN dbo.[Data.Application.Resume] AS [Resume] WITH (NOLOCK) ON [Application].ResumeId = [Resume].Id
	LEFT JOIN dbo.[Data.Application.PersonOfficeMapper] AS pmo WITH (NOLOCK) ON [Resume].PersonId = pmo.PersonId
WHERE 
	([Application].ApplicationStatus = 1 AND [Application].ApprovalStatus = 1)




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[Data.Application.Job]'
GO
ALTER TABLE [dbo].[Data.Application.Job] ADD CONSTRAINT [DF__Data.Appl__HideW__2D87AABC] DEFAULT ((0)) FOR [HideWorkWeekOnPosting]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[Data.Application.Job] ADD CONSTRAINT [DF__Data.Appl__Descr__2E7BCEF5] DEFAULT ((0)) FOR [DescriptionPath]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[Data.Application.JobPostingOfInterestSent]'
GO
ALTER TABLE [dbo].[Data.Application.JobPostingOfInterestSent] ADD CONSTRAINT [DF__Data.Appl__Posti__34F3C25A] DEFAULT ((0)) FOR [PostingId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[Data.Application.JobPostingOfInterestSent] ADD CONSTRAINT [DF__Data.Appl__Perso__35E7E693] DEFAULT ((0)) FOR [PersonId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore]'
GO
ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore] ADD CONSTRAINT [DF__Data.Appl__Perso__0A096455] DEFAULT ((0)) FOR [PersonId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore] ADD CONSTRAINT [DF__Data.Appl__Recen__0AFD888E] DEFAULT ((0)) FOR [RecentlyPlacedId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore] ADD CONSTRAINT [DF__Data.Appl__UserI__0BF1ACC7] DEFAULT ((0)) FOR [UserId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[Data.Migration.JobSeekerRequest]'
GO
ALTER TABLE [dbo].[Data.Migration.JobSeekerRequest] ADD CONSTRAINT [DF__Data.Migr__Recor__2C5E7C59] DEFAULT ((0)) FOR [RecordType]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[Data.Migration.UserRequest]'
GO
ALTER TABLE [dbo].[Data.Migration.UserRequest] ADD CONSTRAINT [DF__Data.Migr__Recor__2B6A5820] DEFAULT ((0)) FOR [RecordType]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[Library.OnetROnet]'
GO
ALTER TABLE [dbo].[Library.OnetROnet] ADD CONSTRAINT [DF__Library.O__OnetR__2C29722F] DEFAULT ((0)) FOR [OnetROnetVariance]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[Library.OnetROnet] ADD CONSTRAINT [DF__Library.O__ROnet__2D1D9668] DEFAULT ((0)) FOR [ROnetOnetVariance]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[Library.OnetROnet] ADD CONSTRAINT [DF__Library.O__OnetI__2E11BAA1] DEFAULT ((0)) FOR [OnetId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[Library.OnetROnet] ADD CONSTRAINT [DF__Library.O__ROnet__2F05DEDA] DEFAULT ((0)) FOR [ROnetId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[Library.ProgramArea]'
GO
ALTER TABLE [dbo].[Library.ProgramArea] ADD CONSTRAINT [DF__Library.P__IsCli__157B1701] DEFAULT ((0)) FOR [IsClientData]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Data.Application.Employer]'
GO
ALTER TABLE [dbo].[Data.Application.Employer] ADD CONSTRAINT [FK__Data.Appl__Offic__67B44C51] FOREIGN KEY ([OfficeId]) REFERENCES [dbo].[Data.Application.Office] ([Id])
ALTER TABLE [dbo].[Data.Application.Employer] ADD CONSTRAINT [FK__Data.Appl__Assig__4341E1B1] FOREIGN KEY ([AssignedToId]) REFERENCES [dbo].[Data.Application.Person] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Data.Application.HowToApplySent]'
GO
ALTER TABLE [dbo].[Data.Application.HowToApplySent] ADD CONSTRAINT [FK__Data.Appl__Posti__26A5A303] FOREIGN KEY ([PostingId]) REFERENCES [dbo].[Data.Application.Posting] ([Id])
ALTER TABLE [dbo].[Data.Application.HowToApplySent] ADD CONSTRAINT [FK__Data.Appl__JobId__2799C73C] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id])
ALTER TABLE [dbo].[Data.Application.HowToApplySent] ADD CONSTRAINT [FK__Data.Appl__Perso__288DEB75] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Data.Application.Job]'
GO
ALTER TABLE [dbo].[Data.Application.Job] ADD CONSTRAINT [FK__Data.Appl__Offic__61FB72FB] FOREIGN KEY ([OfficeId]) REFERENCES [dbo].[Data.Application.Office] ([Id])
ALTER TABLE [dbo].[Data.Application.Job] ADD CONSTRAINT [FK__Data.Appl__Assig__509BDCCF] FOREIGN KEY ([AssignedToId]) REFERENCES [dbo].[Data.Application.Person] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Data.Application.JobPostingOfInterestSent]'
GO
ALTER TABLE [dbo].[Data.Application.JobPostingOfInterestSent] ADD CONSTRAINT [FK__Data.Appl__Posti__36DC0ACC] FOREIGN KEY ([PostingId]) REFERENCES [dbo].[Data.Application.Posting] ([Id])
ALTER TABLE [dbo].[Data.Application.JobPostingOfInterestSent] ADD CONSTRAINT [FK__Data.Appl__Perso__38C4533E] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id])
ALTER TABLE [dbo].[Data.Application.JobPostingOfInterestSent] ADD CONSTRAINT [FK__Data.Appl__JobId__37D02F05] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Data.Application.PersonOfficeMapper]'
GO
ALTER TABLE [dbo].[Data.Application.PersonOfficeMapper] ADD CONSTRAINT [FK__Data.Appl__Offic__60132A89] FOREIGN KEY ([OfficeId]) REFERENCES [dbo].[Data.Application.Office] ([Id])
ALTER TABLE [dbo].[Data.Application.PersonOfficeMapper] ADD CONSTRAINT [FK__Data.Appl__Perso__5F1F0650] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Data.Application.Person]'
GO
ALTER TABLE [dbo].[Data.Application.Person] ADD CONSTRAINT [FK__Data.Appl__Assig__52842541] FOREIGN KEY ([AssignedToId]) REFERENCES [dbo].[Data.Application.Person] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Data.Application.PersonPostcodeMapper]'
GO
ALTER TABLE [dbo].[Data.Application.PersonPostcodeMapper] ADD CONSTRAINT [FK__Data.Appl__Owned__0544AF38] FOREIGN KEY ([OwnedById]) REFERENCES [dbo].[Data.Application.Person] ([Id])
ALTER TABLE [dbo].[Data.Application.PersonPostcodeMapper] ADD CONSTRAINT [FK__Data.Appl__Perso__0638D371] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore]'
GO
ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore] ADD CONSTRAINT [FK__Data.Appl__Perso__0CE5D100] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id])
ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore] ADD CONSTRAINT [FK__Data.Appl__Recen__0DD9F539] FOREIGN KEY ([RecentlyPlacedId]) REFERENCES [dbo].[Data.Application.RecentlyPlaced] ([Id])
ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore] ADD CONSTRAINT [FK__Data.Appl__UserI__0ECE1972] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Data.Application.User] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Library.OnetROnet]'
GO
ALTER TABLE [dbo].[Library.OnetROnet] ADD CONSTRAINT [FK__Library.O__OnetI__61F08603] FOREIGN KEY ([OnetId]) REFERENCES [dbo].[Library.Onet] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Library.ROnet]'
GO
ALTER TABLE [dbo].[Library.ROnet] ADD CONSTRAINT [FK__Library.R__OnetI__443605EA] FOREIGN KEY ([OnetId]) REFERENCES [dbo].[Library.Onet] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating extended properties'
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "o"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 148
               Right = 232
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "oli"
            Begin Extent = 
               Top = 7
               Left = 280
               Bottom = 148
               Right = 467
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 154
               Left = 48
               Bottom = 259
               Right = 232
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'Library.OnetView', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'Library.OnetView', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO