
BEGIN TRANSACTION

-- Populate Posting Ids
UPDATE 
	[Data.Application.PersonPostingMatch]
SET
	PostingId = p.Id
FROM 
	[Data.Application.PersonPostingMatch] ppm
	INNER JOIN  [Data.Application.Posting] p ON ppm.JobId = p.JobId
	
-- Delete records with null
DELETE FROM [Data.Application.PersonPostingMatch] WHERE PostingId is null

COMMIT TRANSACTION
