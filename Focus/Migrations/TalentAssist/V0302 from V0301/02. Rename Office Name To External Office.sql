sp_rename '[Data.Application.Person].Office', 'ExternalOffice', 'COLUMN'
GO

ALTER TABLE [Data.Application.Person] ADD Manager bit null

ALTER VIEW [dbo].[Data.Application.StaffProfile]
AS
SELECT 
	[User].Id, 
	Person.FirstName, 
	Person.LastName,
	Person.EmailAddress,
	Person.ExternalOffice,
	[User].[Enabled],
	pn.Number as PhoneNumber,
	pn1.Number as FaxNumber,
	pa.TownCity,
	pa.StateId
FROM  
	dbo.[Data.Application.Person] AS Person WITH (NOLOCK)
	INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON Person.Id = [User].PersonId
	LEFT JOIN dbo.[Data.Application.PersonAddress] pa WITH (NOLOCK) ON Person.Id = pa.PersonId
	LEFT JOIN dbo.[Data.Application.PhoneNumber] pn WITH (NOLOCK) ON Person.Id = pn.PersonId and pn.PhoneType = 0
	LEFT JOIN dbo.[Data.Application.PhoneNumber] pn1 WITH (NOLOCK) ON Person.Id = pn1.PersonId and pn.PhoneType = 4

GO

ALTER VIEW [dbo].[Data.Application.StaffSearchView]
AS

SELECT 
	[User].Id, 
	Person.FirstName, 
	Person.LastName,
	Person.EmailAddress,
	Person.ExternalOffice,
	[User].[Enabled],
	Person.Id AS PersonId,
	Person.Manager
FROM  
	[Data.Application.Person] AS Person WITH (NOLOCK)
	INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON Person.Id = [User].PersonId
	INNER JOIN dbo.[Data.Application.UserRole] AS UserRole WITH (NOLOCK) ON [User].Id = UserRole.UserId
	INNER JOIN dbo.[Data.Application.Role] AS Role WITH (NOLOCK) ON UserRole.RoleId = [Role].Id
WHERE
	[Role].[Key] = 'AssistUser'	

GO