BEGIN TRANSACTION

ALTER TABLE [Data.Application.PersonPostingMatch] ALTER COLUMN [PostingId] BIGINT NOT NULL
ALTER TABLE [dbo].[Data.Application.PersonPostingMatch] ADD CONSTRAINT [DF__Data.Appl__Posti__750E476F] DEFAULT ((0)) FOR [PostingId]
ALTER TABLE [dbo].[Data.Application.PersonPostingMatch] ADD CONSTRAINT [FK__Data.Appl__Posti__76026BA8] FOREIGN KEY ([PostingId]) REFERENCES [dbo].[Data.Application.Posting] ([Id])
ALTER TABLE [dbo].[Data.Application.PersonPostingMatch] DROP CONSTRAINT [FK__Data.Appl__JobId__7814D14C]
ALTER TABLE [dbo].[Data.Application.PersonPostingMatch] DROP COLUMN [JobId]

COMMIT TRANSACTION