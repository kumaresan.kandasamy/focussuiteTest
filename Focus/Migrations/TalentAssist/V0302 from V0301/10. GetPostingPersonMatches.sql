SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		Andy Jones
-- Create date: 4 September 2013
-- Description:	Gets Person Matches For a Posting For a User
-- =========================================================
CREATE PROCEDURE [dbo].[Data_Application_GetPostingPersonMatches] 
	@PostingId	bigint,
	@UserId		bigint
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ppm.Id,
		ppm.Score,
		ppm.PersonId,
		p.FirstName,
		p.LastName
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		INNER JOIN [Data.Application.Person] p WITH (NOLOCK) ON ppm.PersonId = p.Id
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppm.PostingId = @PostingId AND ppmti.UserId is null
		
END
GO

-- Permissions
GRANT EXECUTE ON  [dbo].[Data_Application_GetPostingPersonMatches] TO [FocusAgent]
GO