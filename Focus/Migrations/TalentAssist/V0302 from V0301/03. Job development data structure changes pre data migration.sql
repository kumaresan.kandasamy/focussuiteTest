BEGIN TRANSACTION

EXEC sp_rename '[Data.Application.OpenPositionMatch]', 'Data.Application.PersonPostingMatch'

EXEC sp_rename '[Data.Application.OpenPositionMatchToIgnore]', 'Data.Application.PersonPostingMatchToIgnore'

EXEC sp_rename '[Data.Application.PersonPostingMatchToIgnore].OpenPositionMatchId', 'PersonPostingMatchId','COLUMN' 


UPDATE [Data.Core.ActionType] SET Name = 'IgnorePersonPostingMatch' WHERE Name = 'IgnoreOpenPositionMatch'
UPDATE [Data.Core.ActionType] SET Name = 'AllowPersonPostingMatch' WHERE Name = 'AllowOpenPositionMatch'

-- Add Posting Id to PersonPostingMatch
ALTER TABLE [Data.Application.PersonPostingMatch] ADD [PostingId] BIGINT NULL

DROP VIEW [dbo].[Data.Application.RecentlyPlacedMatchView]

DROP TABLE [Data.Application.PersonPostingMatchToIgnore]

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[Data.Application.PersonPostingMatchToIgnore]'
GO
CREATE TABLE [dbo].[Data.Application.PersonPostingMatchToIgnore]
(
[Id] [bigint] NOT NULL,
[IgnoredOn] [datetime] NOT NULL,
[PostingId] [bigint] NOT NULL CONSTRAINT [DF__Data.Appl__Posti__7F8BD5E2] DEFAULT ((0)),
[PersonId] [bigint] NOT NULL CONSTRAINT [DF__Data.Appl__Perso__007FFA1B] DEFAULT ((0)),
[UserId] [bigint] NOT NULL CONSTRAINT [DF__Data.Appl__UserI__01741E54] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__Data.App__3214EC077DA38D70] on [dbo].[Data.Application.PersonPostingMatchToIgnore]'
GO
ALTER TABLE [dbo].[Data.Application.PersonPostingMatchToIgnore] ADD CONSTRAINT [PK__Data.App__3214EC077DA38D70] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Data.Application.PersonPostingMatchToIgnore]'
GO
ALTER TABLE [dbo].[Data.Application.PersonPostingMatchToIgnore] ADD CONSTRAINT [FK__Data.Appl__Posti__0268428D] FOREIGN KEY ([PostingId]) REFERENCES [dbo].[Data.Application.Posting] ([Id])
ALTER TABLE [dbo].[Data.Application.PersonPostingMatchToIgnore] ADD CONSTRAINT [FK__Data.Appl__Perso__035C66C6] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id])
ALTER TABLE [dbo].[Data.Application.PersonPostingMatchToIgnore] ADD CONSTRAINT [FK__Data.Appl__UserI__04508AFF] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Data.Application.User] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO


DROP TABLE [Data.Application.RecentlyPlacedMatchesToIgnore]

CREATE TABLE [Data.Application.RecentlyPlacedMatchesToIgnore] (Id BIGINT NOT NULL PRIMARY KEY ,
[IgnoredOn] DATETIME NOT NULL,
[PersonId] BIGINT NOT NULL DEFAULT 0,
[RecentlyPlacedId] BIGINT NOT NULL DEFAULT 0,
[UserId] BIGINT NOT NULL DEFAULT 0,
FOREIGN KEY (PersonId) REFERENCES [Data.Application.Person](Id),
FOREIGN KEY (RecentlyPlacedId) REFERENCES [Data.Application.RecentlyPlaced](Id),
FOREIGN KEY (UserId) REFERENCES [Data.Application.User](Id));

COMMIT TRANSACTION

