SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [dbo].[Library.MilitaryOccupationROnet]
ALTER TABLE [dbo].[Library.MilitaryOccupationROnet] DROP CONSTRAINT [FK__Library.M__Milit__025D5595]
ALTER TABLE [dbo].[Library.MilitaryOccupationROnet] DROP CONSTRAINT [FK__Library.M__ROnet__0169315C]

-- Drop constraints from [dbo].[Library.MilitaryOccupationGroupROnet]
ALTER TABLE [dbo].[Library.MilitaryOccupationGroupROnet] DROP CONSTRAINT [FK__Library.M__Milit__3AA1AEB8]
ALTER TABLE [dbo].[Library.MilitaryOccupationGroupROnet] DROP CONSTRAINT [FK__Library.M__ROnet__39AD8A7F]

-- Drop constraints from [dbo].[Library.MilitaryOccupation]
ALTER TABLE [dbo].[Library.MilitaryOccupation] DROP CONSTRAINT [FK__Library.M__Milit__740F363E]

-- Drop constraints from [dbo].[Library.LocalisationItem]
ALTER TABLE [dbo].[Library.LocalisationItem] DROP CONSTRAINT [FK__Library.L__Local__119F9925]

-- Delete rows from [dbo].[Library.MilitaryOccupationROnet]
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=38
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=124
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=176
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=235
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=254
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=262
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=269
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=299
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=300
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=304
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=311
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=341
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=359
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=381
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=382
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=392
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=393
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=399
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=407
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=410
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=430
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=445
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=467
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=468
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=478
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=479
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=482
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=504
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=505
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=517
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=518
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=528
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=529
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=546
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=566
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=589
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=614
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=615
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=625
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=626
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=629
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=651
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=652
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=664
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=665
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=675
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=676
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=688
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=689
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=691
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=703
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=704
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=712
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=713
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=720
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=737
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=738
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=748
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=749
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=752
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=767
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=782
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=797
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=819
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=820
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=834
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=861
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=886
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=913
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=938
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=965
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=992
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1029
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1056
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1081
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1107
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1166
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1188
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1213
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1214
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1226
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1242
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1245
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1257
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1298
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1299
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1311
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1354
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1380
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1405
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1406
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1420
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1447
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1472
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1473
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1496
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1518
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1530
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1539
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1546
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1553
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1568
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1590
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1612
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1634
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1656
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1678
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1692
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1708
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1735
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1739
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1768
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1775
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1795
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1799
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1869
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1921
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1949
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1964
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=1979
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2005
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2024
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2047
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2073
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2088
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2095
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2126
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2208
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2240
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2318
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2365
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2395
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2413
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2422
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2436
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2447
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2460
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2473
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2488
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2514
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2540
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2568
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2579
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2590
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2603
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2614
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2625
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2656
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2683
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2709
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2735
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2775
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2779
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2833
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2901
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2940
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2941
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2972
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=2994
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3015
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3027
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3033
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3035
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3041
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3043
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3069
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3100
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3113
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3126
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3134
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3145
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3156
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3169
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3179
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3180
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3187
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3199
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3215
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3225
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3226
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3233
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3247
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3260
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3265
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3282
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3290
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3306
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3313
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3337
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3338
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3345
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3358
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3359
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3366
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3387
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3394
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3424
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3429
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3431
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3446
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3460
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3474
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3477
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3495
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3499
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3523
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3591
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3595
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3602
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=3856
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4260
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4283
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4294
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4305
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4316
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4327
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4338
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4349
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4360
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4371
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4382
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4395
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4408
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4419
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4432
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4443
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4454
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4465
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4476
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4487
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4500
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4511
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4522
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4530
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4548
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4601
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4620
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4649
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4667
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4678
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4689
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4697
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4712
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4727
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4742
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4757
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4779
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4806
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4833
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4852
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4882
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4908
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4934
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4961
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=4987
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5005
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5023
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5053
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5068
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5090
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5116
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5146
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5168
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5194
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5216
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5232
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5251
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5277
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5304
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5330
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5346
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5364
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5380
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5388
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5406
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5432
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5458
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5484
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5510
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5535
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5536
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5548
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5549
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5561
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5562
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5574
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5575
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5587
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5588
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5600
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5601
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5613
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5614
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5626
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5627
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5630
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5646
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5680
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5708
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5733
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5735
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5754
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5786
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5796
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5800
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5809
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5835
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5859
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5877
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5903
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5929
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5955
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5996
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=5998
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6012
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6026
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6041
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6105
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6121
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6156
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6174
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6191
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6206
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6224
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6242
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6265
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6271
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6277
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6283
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6484
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6513
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6633
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6637
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6657
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6671
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6697
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6727
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6788
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6880
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=6907
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7043
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7044
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7056
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7057
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7069
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7070
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7084
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7109
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7110
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7137
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7162
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7163
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7175
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7176
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7188
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7213
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7214
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7226
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7227
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7241
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7266
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7267
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7276
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7294
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7295
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7307
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7308
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7320
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7321
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7333
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7334
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7341
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7358
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7359
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7366
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7383
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7384
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7391
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7404
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7415
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7428
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7429
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7436
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7449
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7450
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7457
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7470
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7471
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7478
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7494
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7504
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7505
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7512
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7521
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7529
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7544
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7557
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7578
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7615
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7629
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7638
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7653
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7668
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7694
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7720
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7746
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7762
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7797
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7817
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7820
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7844
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7845
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7852
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7869
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7892
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7915
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7938
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7961
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7980
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=7996
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8051
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8074
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8097
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8120
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8143
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8166
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8190
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8206
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8225
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8251
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8265
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8272
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8281
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8288
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8295
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8314
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8336
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8350
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8357
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8364
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8370
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8392
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8406
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8420
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8434
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8448
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8463
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8488
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8513
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8529
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8715
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8722
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8750
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8751
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8780
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8805
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8811
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8830
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8844
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8859
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8873
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8888
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8910
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8932
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8958
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8972
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=8995
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9017
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9031
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9077
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9099
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9121
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9161
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9183
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9209
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9235
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9261
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9287
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9313
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9340
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9365
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9366
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9378
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9379
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9393
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9419
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9441
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9463
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9485
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9507
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9521
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9548
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9564
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9579
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9590
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9608
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9623
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9645
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9667
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9681
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9697
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9719
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9741
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9763
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9777
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9792
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9814
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9836
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9850
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9853
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9865
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9875
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9882
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9889
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9946
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9964
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=9971
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10005
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10012
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10021
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10032
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10047
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10053
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10062
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10093
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10128
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10152
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10165
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10176
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10187
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10198
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10209
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10225
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10247
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10258
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10273
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10295
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10308
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10476
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10490
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10497
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10517
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10524
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10544
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10551
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10571
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10578
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10632
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10633
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10674
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10700
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10767
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10836
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=10865
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=11108
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=11272
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=11288
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=11307
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=11329
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=11376
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=11431
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=11445
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=11474
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12072
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12087
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12118
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12136
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12151
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12353
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12363
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12383
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12400
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12409
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12426
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12435
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12452
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12461
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12482
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12504
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12524
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12544
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12565
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12586
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12624
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12656
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12665
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12715
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12900
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12929
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12938
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12949
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=12991
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13007
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13055
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13079
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13111
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13120
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13131
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13146
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13172
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13188
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13194
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13225
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13241
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13267
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13315
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13316
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13467
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13489
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13511
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13533
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13555
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13577
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13599
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13621
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13644
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13683
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13701
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13727
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13753
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13768
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13892
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13916
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=13929
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=14078
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=14101
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=14102
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=14112
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=14113
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=14123
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=14124
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=14127
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=14154
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=14166
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=14175
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=14472
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=14483
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=14504
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=14518
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15138
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15156
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15157
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15160
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15177
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15202
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15235
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15236
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15239
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15250
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15254
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15274
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15296
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15317
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15336
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15340
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15431
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15438
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15508
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15515
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15535
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15539
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15578
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15590
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15599
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15611
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15620
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15632
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15638
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15640
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15654
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15656
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15700
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15722
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15748
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15767
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15789
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15808
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15823
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15846
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15872
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15899
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15926
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15945
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=15968
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16010
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16020
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16021
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16037
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16038
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16048
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16049
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16059
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16060
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16081
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16082
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16128
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16159
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16191
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16234
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16297
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16332
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16347
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16362
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16380
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16398
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16416
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16431
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16449
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16464
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16475
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16493
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16558
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16573
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16597
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16665
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16688
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16706
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16728
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16750
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16768
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16786
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16804
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16822
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16840
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16858
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16876
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16894
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16916
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16930
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16948
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16966
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16984
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=16999
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17017
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17035
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17053
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17071
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17097
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17111
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17117
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17141
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17142
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17154
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17155
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17167
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17168
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17355
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17380
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17408
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17426
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17453
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17471
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17501
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17550
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17571
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17582
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17601
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17642
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17674
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17675
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17685
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17686
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17898
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17899
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17912
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17942
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17943
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17955
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17956
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=17984
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18025
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18051
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18124
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18143
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18283
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18298
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18345
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18360
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18521
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18592
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18594
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18768
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18769
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18817
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18831
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18845
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18851
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18889
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=18927
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19023
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19056
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19074
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19092
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19107
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19193
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19226
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19326
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19348
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19366
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19384
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19406
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19420
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19438
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19456
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19478
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19492
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19511
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19529
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19547
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19565
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19579
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19597
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19615
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19653
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19671
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19689
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19707
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19725
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19743
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19761
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19779
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19797
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19815
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19830
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19845
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19860
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19875
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19889
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19907
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19925
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19943
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19961
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19979
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=19997
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20015
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20034
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20048
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20070
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20084
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20102
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20120
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20138
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20156
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20174
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20192
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20210
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20228
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20246
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20264
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20282
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20300
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20318
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20336
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20354
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20369
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20387
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20427
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20442
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20457
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20471
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20489
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20499
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20517
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20545
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20564
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20580
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20651
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20666
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20801
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20821
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20848
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20871
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20883
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20906
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20929
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20955
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20973
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=20987
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51009
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51034
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51052
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51070
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51088
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51106
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51124
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51146
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51164
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51182
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51200
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51218
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51236
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51254
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51272
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51290
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51317
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51381
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51393
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51405
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51418
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51434
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51448
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51459
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51472
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51485
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51498
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51508
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51509
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51516
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51535
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51546
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51562
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51576
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51580
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51606
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51621
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51625
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51652
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51656
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51673
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51700
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51711
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51724
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51730
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51747
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51771
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51784
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51794
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51795
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51802
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51811
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51823
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51824
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51831
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51844
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51845
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51852
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51868
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51952
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=51979
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52005
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52027
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52053
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52095
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52104
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52113
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52122
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52131
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52223
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52229
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52247
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52250
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52268
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52271
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52289
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52439
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52465
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52488
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52511
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52523
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52554
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52566
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52599
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52611
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52636
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52637
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52644
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52665
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52675
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52676
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52683
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52704
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52714
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52715
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52722
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52743
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52753
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52754
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52761
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52782
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52795
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52815
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52839
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52863
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52887
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52896
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52897
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52905
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52906
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=52909
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53209
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53237
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53265
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53305
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53323
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53341
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53359
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53381
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53399
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53417
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53435
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53453
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53471
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53489
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53517
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53559
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53575
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53608
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53643
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53661
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53679
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53697
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53715
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53733
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53751
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53792
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53806
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53824
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53856
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53874
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53894
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53911
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53938
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53952
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=53995
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54009
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54032
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54052
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54064
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54073
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54085
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54104
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54106
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54111
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54118
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54140
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54203
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54219
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54246
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54287
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54330
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54348
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54361
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54372
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54417
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54431
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54478
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54503
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54580
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54590
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54627
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54631
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54651
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54717
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54720
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54770
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54808
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54817
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54847
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54877
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54907
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54939
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=54979
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55005
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55017
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55050
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55076
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55088
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55121
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55192
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55193
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55202
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55203
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55228
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55229
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55241
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55257
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55277
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55284
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55289
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55304
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55305
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55311
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55328
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55363
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55379
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55396
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55413
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55465
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55516
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55530
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55582
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55636
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55658
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55666
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55766
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55784
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55802
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55966
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=55967
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67123
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67124
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67190
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67191
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67203
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67204
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67225
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67240
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67255
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67270
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67285
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67300
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67315
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67332
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67354
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67369
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67702
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67732
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67758
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67759
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67767
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67788
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67801
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67813
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67826
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67844
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67857
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67875
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67888
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67906
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67919
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67937
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67950
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67962
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67978
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67988
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=67994
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68011
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68028
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68049
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68068
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68076
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68080
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68097
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68114
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68130
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68146
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68154
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68158
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68175
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68192
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68208
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68224
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68232
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68236
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68253
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68270
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68283
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68299
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68312
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68325
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68338
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68348
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68357
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68366
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68375
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68384
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68393
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68402
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68411
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68420
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68448
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68485
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68522
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68559
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68596
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68714
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68730
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68735
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68745
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68753
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68760
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68765
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68784
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68792
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68796
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68801
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68822
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68838
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68845
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68854
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68870
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68877
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68889
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68906
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68913
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68922
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68980
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68981
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68996
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=68997
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72010
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72011
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72023
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72024
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72037
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72038
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72048
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72049
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72059
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72060
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72072
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72073
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72088
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72089
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72104
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72105
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72120
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72121
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72136
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72137
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72152
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72153
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72166
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72167
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72177
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72178
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72188
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72189
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72211
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72230
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72241
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72279
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72296
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72345
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72347
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72406
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72408
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72503
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72516
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72532
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72548
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72552
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72567
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72583
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72589
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72593
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72608
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72621
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72637
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72651
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72671
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72695
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72724
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72732
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72745
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72760
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72792
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72807
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72822
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72837
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72852
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72867
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=72945
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73156
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73225
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73228
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73259
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73260
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73270
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73327
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73341
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73366
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73367
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73379
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73380
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73392
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73393
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73405
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73406
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73409
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73445
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73457
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73497
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73526
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73537
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73577
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73578
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73602
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73603
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73609
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73631
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73632
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73644
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73645
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73652
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73660
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73674
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73697
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73698
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73704
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73723
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73724
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73734
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73735
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73745
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73746
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73778
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73790
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73810
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73823
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73843
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73857
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73878
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=73891
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74017
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74040
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74071
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74089
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74107
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74125
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74143
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74161
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74179
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74197
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74215
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74233
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74251
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74269
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74287
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74305
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74323
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74341
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74360
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74383
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74411
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74439
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74467
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74500
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74513
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74522
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74538
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74551
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74562
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74570
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74590
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74595
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74599
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74626
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74627
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74633
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74648
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74653
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74669
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74690
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74705
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74720
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74730
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74745
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74749
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74762
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74792
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74812
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74824
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74850
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74868
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74889
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74907
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74925
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74942
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74970
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=74998
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75026
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75054
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75082
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75110
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75138
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75166
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75187
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75194
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75206
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75213
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75225
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75232
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75244
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75251
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75263
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75270
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75282
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75289
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75301
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75308
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75320
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75327
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75339
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75346
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75358
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75365
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75377
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75384
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75396
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75403
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75415
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75425
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75453
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75467
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75481
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75495
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75509
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75523
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75537
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75551
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75565
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75579
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75593
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75605
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75606
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75618
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75619
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75631
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75632
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75644
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75645
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75657
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75658
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75670
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75671
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75809
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75813
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75817
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75821
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75825
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75829
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75847
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75854
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75866
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75873
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75885
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75892
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75904
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75911
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75923
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75930
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75942
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75949
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75961
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75968
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75980
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75987
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=75999
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76006
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76018
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76025
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76037
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76044
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76056
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76063
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76075
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76082
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76094
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76101
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76113
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76120
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76132
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76139
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76151
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76158
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76170
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76177
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76189
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76196
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76208
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76215
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76227
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76234
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76246
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76253
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76265
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76272
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76284
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76291
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76303
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76310
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76322
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76329
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76341
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76348
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76360
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76367
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76379
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76386
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76398
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76405
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76417
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76424
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76436
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76443
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76455
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76462
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76474
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76481
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76493
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76500
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76512
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76519
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76531
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76538
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76550
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76557
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76569
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76576
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76588
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76595
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76607
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76614
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76626
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76633
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76645
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76652
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76664
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76671
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76683
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76690
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76702
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76709
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76721
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76728
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76740
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76747
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76759
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76766
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76778
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76785
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76797
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76804
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76816
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76823
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76835
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76842
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76854
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76861
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76873
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76880
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76892
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76899
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76911
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76918
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76930
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76937
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76949
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76959
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76975
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76982
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=76994
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77001
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77013
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77020
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77032
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77039
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77051
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77058
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77070
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77077
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77089
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77096
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77108
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77115
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77127
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77134
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77146
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77153
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77165
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77172
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77184
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77191
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77203
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77210
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77222
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77229
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77241
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77248
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77260
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77267
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77279
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77286
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77298
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77305
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77317
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77324
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77336
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77343
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77355
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77362
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77374
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77381
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77393
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77400
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77412
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77419
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77431
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77438
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77450
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77457
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77469
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77476
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77488
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77495
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77507
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77514
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77526
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77533
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77545
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77552
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77564
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77571
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77583
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77590
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77602
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77609
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77621
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77628
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77640
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77647
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77659
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77666
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77678
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77685
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77694
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77701
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77713
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77720
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77732
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77739
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77751
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77758
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77770
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77777
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77789
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77796
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77808
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77815
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77827
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77834
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77846
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77853
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77865
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77872
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77884
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77891
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77903
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77910
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77922
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77929
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77941
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77948
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77960
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77967
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77979
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77986
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=77998
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78005
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78017
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78113
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78119
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78131
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78137
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78149
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78155
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78167
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78173
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78263
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78267
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78294
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78308
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78312
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78339
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78353
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78357
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78384
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78398
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78402
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78429
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78443
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78457
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78461
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78476
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78485
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78501
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78508
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78520
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78527
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78539
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78546
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78558
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78565
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78577
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78584
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78596
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78603
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78615
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78622
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78634
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78641
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78653
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78660
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78672
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78679
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78691
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78698
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78710
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78717
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78729
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78736
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78748
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78755
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78767
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78774
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78786
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78793
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78805
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78812
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78824
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78831
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78843
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78850
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78862
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78869
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78881
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78888
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78900
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78907
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78919
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78926
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78938
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78945
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78957
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78964
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78976
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78983
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=78995
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79002
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79014
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79021
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79033
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79040
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79052
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79059
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79071
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79078
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79090
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79097
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79109
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79116
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79128
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79135
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79147
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79154
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79166
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79173
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79185
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79192
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79204
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79211
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79223
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79230
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79242
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79249
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79261
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79268
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79280
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79287
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79299
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79306
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79318
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79325
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79337
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79344
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79356
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79363
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79375
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79382
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79394
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79401
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79413
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79420
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79432
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79439
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79451
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79458
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79470
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79477
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79489
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79496
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79508
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79515
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79527
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79534
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79548
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79560
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79567
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79579
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79586
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79598
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79605
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79617
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79624
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79636
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79643
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79655
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79662
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79674
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79681
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79693
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79700
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79712
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79719
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79731
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79738
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79750
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79757
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79769
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79776
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79788
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79795
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79807
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79814
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79826
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79833
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79845
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79852
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79864
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79871
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79883
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79890
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79902
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79909
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79921
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79928
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79940
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79947
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79959
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79966
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79978
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79985
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=79997
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80007
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80023
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80030
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80042
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80049
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80061
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80068
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80080
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80087
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80099
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80106
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80118
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80125
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80137
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80144
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80156
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80163
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80175
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80182
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80194
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80201
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80213
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80220
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80232
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80239
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80251
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80258
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80270
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80277
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80289
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80296
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80308
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80315
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80327
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80334
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80346
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80353
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80365
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80372
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80384
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80391
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80403
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80410
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80425
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80432
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80444
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80451
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80463
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80470
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80482
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80489
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80501
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80508
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80520
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80527
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80539
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80546
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80558
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80565
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80577
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80584
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80596
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80603
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80615
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80622
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80634
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80641
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80653
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80660
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80672
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80679
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80691
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80698
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80710
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80717
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80729
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80736
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80748
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80755
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80767
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80774
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80786
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80793
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80805
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80812
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80824
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80831
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80843
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80850
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80862
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80869
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80881
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80888
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80900
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80907
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80919
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80926
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80938
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80945
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80957
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80964
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80976
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80983
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=80995
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81002
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81014
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81021
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81033
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81040
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81052
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81059
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81071
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81078
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81090
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81097
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81109
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81116
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81128
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81135
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81147
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81154
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81166
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81173
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81185
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81192
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81204
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81211
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81223
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81230
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81242
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81249
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81261
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81268
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81280
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81287
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81299
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81306
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81318
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81325
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81337
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81344
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81356
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81363
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81375
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81382
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81394
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81401
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81413
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81420
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81432
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81439
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81451
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81458
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81470
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81477
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81489
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81496
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81508
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81515
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81527
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81534
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81546
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81553
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81565
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81572
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81584
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81591
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81603
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81610
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81622
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81629
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81641
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81648
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81660
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81667
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81679
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81686
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81698
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81705
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81717
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81724
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81736
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81743
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81755
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81762
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81774
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81781
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81793
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81800
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81812
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81819
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81831
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81838
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81850
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81857
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81869
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81876
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81888
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81895
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81907
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81914
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81926
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=81933
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82045
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82055
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82065
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82075
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82085
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82095
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82105
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82115
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82125
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82135
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82145
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82155
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82165
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82175
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82185
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82195
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82205
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82215
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82225
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82235
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82245
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82255
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82265
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82498
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82509
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82520
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82531
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82542
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82569
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82582
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82595
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82608
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82621
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82634
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82647
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82658
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82664
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82682
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82683
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82690
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82706
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82713
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82725
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82732
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82744
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82751
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82763
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82770
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82782
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82789
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82801
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82808
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82820
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82827
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82839
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82846
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82858
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82865
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82877
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82884
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82896
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82903
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82915
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82922
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82934
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82941
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82953
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82960
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82972
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82979
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82991
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=82998
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83010
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83017
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83029
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83036
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83048
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83055
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83067
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83074
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83086
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83093
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83105
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83112
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83124
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83131
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83143
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83150
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83162
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83169
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83181
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83188
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83200
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83207
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83219
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83226
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83238
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83245
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83257
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83264
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83276
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83283
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83295
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83302
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83314
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83321
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83333
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83340
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83352
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83359
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83371
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83378
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83390
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83397
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83409
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83416
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83428
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83435
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83447
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83454
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83466
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83473
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83485
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83492
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83504
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83511
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83523
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83530
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83542
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83549
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83561
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83568
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83580
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83587
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83599
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83606
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83618
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83625
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83637
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83644
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83656
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83663
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83675
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83682
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83694
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83701
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83713
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83720
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83732
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83739
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83751
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83758
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83770
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83777
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83789
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83796
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83808
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83815
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83827
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83834
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83846
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83853
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83865
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83872
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83884
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83891
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83903
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83910
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83922
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83929
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83941
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83948
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83960
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83967
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83979
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83986
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=83998
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84005
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84017
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84024
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84036
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84043
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84055
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84062
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84074
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84081
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84093
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84100
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84112
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84119
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84131
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84141
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84157
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84164
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84176
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84183
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84195
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84202
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84214
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84221
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84230
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84242
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84259
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84274
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84275
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84282
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84295
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84296
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84303
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84338
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84366
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84394
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84423
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84441
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84459
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84477
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84493
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84520
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84547
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84574
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84601
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84624
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84642
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84660
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84678
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84696
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84714
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84732
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84750
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84768
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84785
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84813
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84841
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84869
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84897
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84925
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84953
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=84981
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85009
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85037
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85065
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85094
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85112
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85130
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85148
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85166
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85184
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85202
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85220
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85236
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85263
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85290
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85317
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85344
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85373
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85391
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85410
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85440
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85470
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85500
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85530
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85559
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85577
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85595
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85613
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85631
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85649
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85667
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85685
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85703
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85721
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85739
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85757
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85775
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85792
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85820
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85848
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85876
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85904
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85932
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85960
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=85988
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86016
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86044
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86072
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86100
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86129
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86154
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86172
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86190
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86208
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86227
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86257
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86286
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86304
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86322
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86340
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86359
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86495
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86633
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86750
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86867
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86913
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86960
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=86961
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87016
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87074
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87075
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87082
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87096
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87110
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87144
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87157
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87191
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87198
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87230
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87256
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87269
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87280
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87291
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87303
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87304
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87311
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87320
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87338
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87356
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87374
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87392
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87410
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87428
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87446
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87464
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87482
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87500
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87518
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87536
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87554
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87572
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87590
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87608
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87626
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87644
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87662
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87680
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87698
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87716
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87734
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87752
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87770
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87788
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87806
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87824
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87842
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87875
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87893
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87911
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87929
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87950
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87958
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87976
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=87994
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88015
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88023
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88041
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88059
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88080
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88088
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88106
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88136
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88154
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88172
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88190
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88208
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88226
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88244
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88262
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88280
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88296
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88324
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88352
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88381
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88409
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88437
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88465
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88493
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88521
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88549
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88577
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88602
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88620
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88645
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88652
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88675
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88676
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88700
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88710
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88711
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88718
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88734
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88747
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88771
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88786
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88795
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88805
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88809
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88820
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88824
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88835
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88839
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88850
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88854
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88865
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88869
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88880
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88884
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88925
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88942
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88959
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88976
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=88993
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89018
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89034
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89050
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89066
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89082
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89098
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89114
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89130
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89146
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89162
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89178
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89194
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89207
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89225
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89238
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89256
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89269
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89299
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89336
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89373
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89411
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89534
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89548
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89562
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89576
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89590
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89604
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89618
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89641
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89655
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89669
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89683
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89697
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89711
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89734
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89748
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89762
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89776
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89790
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89804
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89827
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89850
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=89998
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90350
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90379
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90408
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90437
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90466
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90495
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90518
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90537
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90556
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90560
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90583
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90599
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90609
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90613
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90640
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90655
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90656
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90663
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90677
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90689
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90690
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90697
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90710
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90711
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90718
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90731
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90732
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90739
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90752
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90753
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90760
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90774
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90787
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90803
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90811
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90824
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90842
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90855
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90867
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90883
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90891
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90895
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90906
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90920
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90939
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90955
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90971
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90979
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90983
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=90994
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91008
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91027
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91043
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91059
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91067
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91071
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91082
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91096
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91115
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91131
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91147
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91155
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91159
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91170
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91184
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91203
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91216
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91259
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91287
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91595
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91609
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91623
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91640
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91695
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91770
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91786
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91802
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91818
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91834
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91850
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91866
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91882
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91898
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91914
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91930
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91946
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91962
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91975
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=91988
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92001
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92014
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92026
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92027
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92037
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92038
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92048
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92049
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92059
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92060
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92070
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92071
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92078
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92090
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92102
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92114
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92126
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92138
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92150
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92162
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92174
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92186
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92198
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92210
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92226
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92378
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92396
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92414
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92432
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92450
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92468
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92486
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92504
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92522
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92540
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92558
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92576
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92594
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92612
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92630
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92648
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92666
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92684
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92702
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92715
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92732
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92749
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92766
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=92783
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93142
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93161
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93175
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93189
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93200
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93207
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93251
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93255
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93265
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93269
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93279
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93283
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93293
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93297
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93307
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93311
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93321
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93325
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93331
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93333
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93339
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93341
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93347
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93349
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93355
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93357
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93363
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93365
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93371
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93373
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93379
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93381
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93387
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93389
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93395
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93397
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93403
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93405
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93411
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93413
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93419
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93421
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93712
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93719
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93741
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93757
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93768
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93775
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93794
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93801
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93850
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93866
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93891
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93892
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93893
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=93957
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94050
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94064
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94070
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94084
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94090
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94104
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94121
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94134
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94147
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94171
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94323
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94338
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94349
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94361
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94374
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94389
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94405
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94420
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94433
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94448
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94463
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94639
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94655
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94668
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94691
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94707
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=94886
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95000
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95028
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95053
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95065
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95085
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95116
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95125
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95145
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95172
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95181
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95201
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95213
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95235
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95247
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95259
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95271
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95283
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95295
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95468
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95578
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=95608
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96136
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96137
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96156
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96157
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96176
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96177
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96196
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96197
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96216
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96217
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96289
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96295
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96301
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96307
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96313
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96319
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96422
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96447
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96472
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96497
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96522
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96547
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96909
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96926
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96944
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96947
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96967
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=96984
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97002
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97005
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97024
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97041
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97059
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97061
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97081
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97098
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97115
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97134
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97136
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97141
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97148
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97167
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97174
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97193
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97200
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97219
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97226
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97245
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97252
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97271
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97278
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97297
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97304
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97323
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97330
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97393
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97460
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97505
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97528
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97529
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97562
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97582
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97587
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97591
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97610
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97628
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97646
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97664
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97682
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97712
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97726
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97730
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97751
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97790
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97791
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97803
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97804
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97826
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97840
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97851
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97857
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97874
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97901
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97915
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97927
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97928
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97945
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97959
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97966
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97973
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97980
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97987
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=97994
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98001
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98008
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98015
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98022
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98029
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98036
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98043
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98050
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98057
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98064
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98071
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98078
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98085
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98092
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98099
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98106
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98113
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98120
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98127
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98134
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98141
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98148
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98155
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98162
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98169
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98176
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98183
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98190
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98197
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98204
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98211
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98218
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98225
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98232
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98239
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98246
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98253
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98260
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98267
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98274
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98281
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98288
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98295
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98302
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98309
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98316
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98323
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98330
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98341
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98348
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98355
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98362
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98369
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98376
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98383
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98390
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98397
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98404
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98411
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98418
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98425
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98432
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98439
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98446
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98453
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98460
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98467
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98474
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98481
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98488
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98495
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98502
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98509
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98516
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98523
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98530
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98537
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98544
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98551
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98558
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98565
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98572
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98579
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98586
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98593
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98600
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98607
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98614
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98621
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98637
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98644
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98651
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98658
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98665
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98672
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98679
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98686
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98693
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98700
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98707
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98714
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98729
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98736
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98755
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98762
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98769
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98776
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98783
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98801
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98802
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=98996
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99054
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99061
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99085
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99089
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99112
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99119
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99139
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99141
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99150
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99162
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99171
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99183
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99190
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99214
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99215
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99224
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99245
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99260
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99269
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99284
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99299
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99314
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99329
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99344
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99365
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99376
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99414
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99421
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99437
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99444
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99519
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99540
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99545
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99560
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99586
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99600
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99718
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99757
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99771
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99778
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99823
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=99909
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100013
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100030
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100087
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100117
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100139
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100159
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100173
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100193
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100250
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100314
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100321
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100328
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100335
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100342
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100349
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100356
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100363
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100370
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100377
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100384
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100391
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100398
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100405
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100412
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100419
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100426
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100433
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100440
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100447
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100454
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100461
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100468
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100475
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100482
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100489
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100496
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100503
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100510
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100517
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100524
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100531
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100538
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100545
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100552
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100559
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100566
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100573
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100580
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100587
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100594
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100601
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100608
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100615
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100622
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100629
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100636
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100643
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100650
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100657
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100664
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100671
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100678
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100685
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100692
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100699
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100706
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100713
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100720
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100727
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100734
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100741
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100748
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100755
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100762
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100769
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100776
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100783
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100790
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100797
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100804
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100811
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100818
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100825
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100832
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100836
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100863
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100870
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100877
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100884
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100891
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100898
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100905
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100912
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100919
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100926
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100933
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100940
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100947
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100954
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100961
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100968
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100975
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100982
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100989
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=100996
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101003
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101010
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101017
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101024
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101031
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101038
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101045
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101052
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101059
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101066
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101073
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101080
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101087
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101094
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101101
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101108
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101115
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101122
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101129
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101136
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101143
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101150
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101157
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101164
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101171
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101178
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101185
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101192
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101199
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101206
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101213
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101220
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101227
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101234
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101241
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101248
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101255
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101262
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101269
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101276
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101283
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101290
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101297
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101304
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101311
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101318
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101325
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101332
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101339
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101346
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101353
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101360
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101367
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101374
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101381
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101388
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101395
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101402
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101409
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101416
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101551
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101613
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101625
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101651
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101694
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101707
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101770
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101801
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101829
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101863
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101900
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101925
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101940
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101955
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=101963
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102036
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102037
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102047
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102048
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102058
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102059
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102069
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102070
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102080
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102081
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102091
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102092
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102102
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102103
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102113
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102114
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102124
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102125
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102135
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102136
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102146
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102147
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102173
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102180
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102233
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102240
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102260
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102281
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102303
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102304
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102314
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102315
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102325
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102326
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102339
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102340
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102347
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102378
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102387
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102394
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102413
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102420
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102439
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102446
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102465
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102472
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102491
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102498
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102517
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102524
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102543
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102550
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102665
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102695
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102725
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102755
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102780
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102812
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102814
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102823
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102835
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102841
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102843
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102852
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102864
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102870
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102872
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102947
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102954
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102961
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102968
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102975
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102982
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102989
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=102996
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103003
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103010
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103017
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103024
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103031
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103038
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103045
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103052
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103059
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103066
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103073
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103080
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103087
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103094
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103101
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103108
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103115
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103122
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103129
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103136
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103143
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103150
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103157
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103164
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103171
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103178
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103185
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103192
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103199
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103206
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103213
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103220
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103227
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103234
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103241
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103248
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103255
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103262
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103269
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103276
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103283
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103290
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103297
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103304
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103311
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103318
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103325
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103332
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103339
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103346
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103353
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103360
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103367
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103374
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103381
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103388
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103406
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103413
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103420
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103427
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103434
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103488
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103639
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103656
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103657
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103672
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103681
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103697
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103706
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103721
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103724
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103738
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103749
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103766
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103767
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103827
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103828
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103863
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103870
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=103903
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104100
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104131
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104150
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104169
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104188
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104210
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104222
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104225
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104253
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104254
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104264
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104265
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104275
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104276
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104283
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104463
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104487
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104514
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104561
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104586
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104601
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104682
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104711
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104729
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104731
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104737
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104739
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104748
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104760
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104803
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104804
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104833
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104840
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104866
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104873
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104912
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104916
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=104962
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105002
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105034
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105041
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105048
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105055
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105062
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105069
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105076
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105083
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105090
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105097
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105104
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105111
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105118
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105125
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105132
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105139
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105146
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105153
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105160
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105167
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105174
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105181
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105188
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105195
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105202
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105209
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105216
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105223
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105230
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105237
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105244
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105251
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105258
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105265
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105272
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105279
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105286
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105293
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105300
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105307
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105314
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105321
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105328
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105335
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105342
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105349
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105356
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105363
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105370
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105377
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105384
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105391
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105398
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105405
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105412
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105419
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105426
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105433
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105440
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105447
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105454
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105461
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105468
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105475
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105482
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105489
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105496
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105503
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105510
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105517
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105524
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105531
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105538
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105545
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105552
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105559
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105566
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105573
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105580
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105587
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105594
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105601
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105608
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105615
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105622
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105629
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105636
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105643
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105650
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105657
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105664
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105671
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105678
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105685
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105692
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105699
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105706
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105713
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105720
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105727
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105734
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105741
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105748
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105755
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105762
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105769
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105776
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105821
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105853
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105855
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105974
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=105975
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=106062
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=106118
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=106136
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=106154
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=106172
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=106190
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=106206
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=106239
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=106272
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=106308
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=106341
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=106374
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=106410
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=106446
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=106482
DELETE FROM [dbo].[Library.MilitaryOccupationROnet] WHERE [Id]=106520
-- Operation applied to 3504 rows out of 3504

-- Delete rows from [dbo].[Library.MilitaryOccupationGroupROnet]
DELETE FROM [dbo].[Library.MilitaryOccupationGroupROnet] WHERE [Id]=6
DELETE FROM [dbo].[Library.MilitaryOccupationGroupROnet] WHERE [Id]=10
DELETE FROM [dbo].[Library.MilitaryOccupationGroupROnet] WHERE [Id]=27
DELETE FROM [dbo].[Library.MilitaryOccupationGroupROnet] WHERE [Id]=104
DELETE FROM [dbo].[Library.MilitaryOccupationGroupROnet] WHERE [Id]=127
DELETE FROM [dbo].[Library.MilitaryOccupationGroupROnet] WHERE [Id]=186
DELETE FROM [dbo].[Library.MilitaryOccupationGroupROnet] WHERE [Id]=205
DELETE FROM [dbo].[Library.MilitaryOccupationGroupROnet] WHERE [Id]=232
DELETE FROM [dbo].[Library.MilitaryOccupationGroupROnet] WHERE [Id]=266
DELETE FROM [dbo].[Library.MilitaryOccupationGroupROnet] WHERE [Id]=271
DELETE FROM [dbo].[Library.MilitaryOccupationGroupROnet] WHERE [Id]=280
DELETE FROM [dbo].[Library.MilitaryOccupationGroupROnet] WHERE [Id]=310
-- Operation applied to 12 rows out of 12


-- Update rows in [dbo].[Library.MilitaryOccupation]
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=2
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=3
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=4
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=5
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=6
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=7
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=8
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=9
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=2 WHERE [Id]=10
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=2 WHERE [Id]=11
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=2 WHERE [Id]=12
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=2 WHERE [Id]=13
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=2 WHERE [Id]=14
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=2 WHERE [Id]=15
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=2 WHERE [Id]=16
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=2 WHERE [Id]=17
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=2 WHERE [Id]=18
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=2 WHERE [Id]=19
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=2 WHERE [Id]=20
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=21
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=22
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=11 WHERE [Id]=23
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=24
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=25
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=26
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=27
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=28
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=36
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=37
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=38
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=39
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=17 WHERE [Id]=40
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=4 WHERE [Id]=41
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=11 WHERE [Id]=42
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=11 WHERE [Id]=43
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=11 WHERE [Id]=44
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=11 WHERE [Id]=45
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=11 WHERE [Id]=46
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=11 WHERE [Id]=47
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=48
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=49
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=50
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=51
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=52
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=53
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=17 WHERE [Id]=54
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=17 WHERE [Id]=55
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=12 WHERE [Id]=56
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=63
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=4 WHERE [Id]=64
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=4 WHERE [Id]=65
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=4 WHERE [Id]=66
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=4 WHERE [Id]=67
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=4 WHERE [Id]=68
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=8 WHERE [Id]=69
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=15 WHERE [Id]=70
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=71
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=72
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=73
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=74
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=75
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=76
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=77
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=78
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=79
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=80
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=81
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=82
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=83
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=84
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=85
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=86
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=87
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=88
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=89
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=90
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=91
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=92
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=93
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=94
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=95
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=96
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=97
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=98
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=17 WHERE [Id]=99
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=17 WHERE [Id]=100
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=101
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=102
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=103
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=104
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=105
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=106
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=107
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=108
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=109
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=110
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=111
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=112
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=113
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=114
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=115
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=116
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=117
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=118
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=119
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=120
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=121
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=122
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=123
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=124
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=125
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=126
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=127
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=128
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=129
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=130
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=131
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=132
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=133
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=134
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=135
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=136
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=137
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=138
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=139
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=140
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=141
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=142
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=143
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=144
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=145
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=146
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=147
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=148
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=149
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=150
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=151
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=152
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=153
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=154
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=155
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=156
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=157
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=158
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=159
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=160
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=161
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=162
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=163
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=164
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=165
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=166
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=167
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=168
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=169
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=170
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=171
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=172
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=173
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=174
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=175
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=176
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=177
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=178
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=179
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=180
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=181
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=182
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=183
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=184
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=3 WHERE [Id]=185
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=12 WHERE [Id]=186
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=12 WHERE [Id]=187
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=12 WHERE [Id]=188
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=12 WHERE [Id]=189
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=12 WHERE [Id]=190
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=191
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=192
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=193
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=194
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=195
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=196
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=17 WHERE [Id]=197
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=10 WHERE [Id]=198
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=8 WHERE [Id]=199
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=8 WHERE [Id]=200
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=8 WHERE [Id]=201
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=8 WHERE [Id]=202
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=8 WHERE [Id]=203
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=8 WHERE [Id]=204
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=8 WHERE [Id]=205
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=8 WHERE [Id]=206
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=8 WHERE [Id]=207
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=8 WHERE [Id]=208
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=8 WHERE [Id]=209
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=210
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=211
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=212
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=213
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=214
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=215
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=216
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=217
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=2 WHERE [Id]=218
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=2 WHERE [Id]=219
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=220
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=221
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=222
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=223
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=224
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=225
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=226
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=227
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=228
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=229
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=230
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=12 WHERE [Id]=231
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=12 WHERE [Id]=232
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=12 WHERE [Id]=233
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=12 WHERE [Id]=234
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=12 WHERE [Id]=235
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=12 WHERE [Id]=237
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=12 WHERE [Id]=238
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=12 WHERE [Id]=239
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=12 WHERE [Id]=240
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=12 WHERE [Id]=241
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=12 WHERE [Id]=242
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=12 WHERE [Id]=243
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=244
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=245
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=246
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=247
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=248
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=249
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=2 WHERE [Id]=250
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=2 WHERE [Id]=251
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=2 WHERE [Id]=252
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=2 WHERE [Id]=253
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=254
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=255
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=256
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=257
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=258
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=259
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=260
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=261
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=262
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=263
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=264
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=265
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=266
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=267
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=268
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=269
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=270
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=271
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=272
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=273
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=274
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=275
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=276
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=277
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=278
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=279
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=280
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=281
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=282
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=283
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=284
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=285
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=286
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=287
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=288
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=289
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=290
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=291
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=292
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=293
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=294
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=295
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=296
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=297
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=298
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=299
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=300
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=301
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=302
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=303
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=304
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=305
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=306
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=307
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=308
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=309
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=310
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=311
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=312
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=313
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=314
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=315
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=316
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=317
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=318
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=319
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=320
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=321
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=322
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=323
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=324
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=325
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=326
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=327
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=328
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=329
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=330
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=331
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=332
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=333
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=334
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=335
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=336
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=337
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=338
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=339
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=340
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=341
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=342
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=343
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=344
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=345
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=346
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=347
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=348
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=349
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=350
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=351
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=352
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=353
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=354
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=355
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=356
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=357
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=358
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=359
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=360
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=361
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=362
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=363
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=364
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=365
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=366
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=367
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=368
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=369
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=370
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=371
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=372
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=373
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=374
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=375
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=376
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=377
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=378
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=379
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=380
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=381
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=382
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=383
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=384
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=385
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=386
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=387
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=388
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=389
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=390
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=391
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=392
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=393
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=394
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=395
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=396
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=397
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=398
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=399
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=400
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=401
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=402
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=403
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=404
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=405
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=406
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=407
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=408
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=409
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=410
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=411
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=412
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=413
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=414
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=415
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=416
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=417
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=418
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=419
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=420
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=421
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=422
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=423
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=424
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=425
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=426
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=427
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=428
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=429
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=430
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=431
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=432
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=433
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=434
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=435
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=436
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=437
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=438
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=439
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=440
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=441
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=442
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=443
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=444
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=445
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=446
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=447
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=448
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=449
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=450
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=451
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=452
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=453
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=454
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=455
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=456
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=457
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=458
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=459
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=460
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=461
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=462
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=463
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=464
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=465
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=466
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=467
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=468
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=469
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=470
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=471
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=472
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=473
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=474
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=475
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=476
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=477
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=478
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=479
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=480
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=481
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=482
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=483
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=484
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=485
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=486
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=487
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=488
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=489
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=490
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=491
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=492
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=493
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=494
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=495
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=496
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=497
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=498
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=499
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=500
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=501
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=502
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=503
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=504
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=505
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=506
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=507
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=508
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=509
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=510
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=511
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=512
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=513
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=514
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=515
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=516
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=517
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=518
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=519
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=520
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=521
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=522
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=523
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=524
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=525
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=526
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=527
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=528
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=529
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=530
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=531
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=532
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=533
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=534
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=535
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=536
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=537
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=538
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=539
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=540
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=541
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=542
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=543
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=544
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=545
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=546
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=547
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=548
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=549
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=550
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=551
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=552
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=553
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=554
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=555
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=556
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=557
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=558
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=559
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=560
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=561
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=562
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=563
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=564
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=565
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=566
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=567
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=568
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=569
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=570
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=571
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=572
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=573
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=574
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=575
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=576
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=577
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=578
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=579
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=580
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=581
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=582
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=583
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=584
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=585
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=586
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=587
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=588
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=589
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=590
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=591
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=592
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=593
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=594
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=595
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=596
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=597
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=598
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=599
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=600
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=601
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=602
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=603
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=604
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=605
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=606
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=607
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=608
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=609
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=610
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=611
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=612
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=613
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=614
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=615
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=616
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=617
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=618
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=619
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=620
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=621
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=622
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=623
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=624
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=625
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=626
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=627
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=9 WHERE [Id]=628
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=629
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=630
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=631
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=632
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=633
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=634
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=635
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=636
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=637
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=638
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=639
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=640
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=641
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=642
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=643
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=644
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=645
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=646
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=647
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=648
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=649
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=650
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=651
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=6 WHERE [Id]=652
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=2 WHERE [Id]=653
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=654
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=655
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=656
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=657
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=658
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=659
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=660
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=661
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=662
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=663
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=664
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=665
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=666
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=667
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=668
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=669
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=670
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=17 WHERE [Id]=4851
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=17 WHERE [Id]=4852
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=5 WHERE [Id]=4935
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=5051
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=5052
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=5053
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=5054
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=5055
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=5056
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=5057
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=5058
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=13 WHERE [Id]=5059
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=14 WHERE [Id]=5072
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=17 WHERE [Id]=5421
UPDATE [dbo].[Library.MilitaryOccupation] SET [MilitaryOccupationGroupId]=14 WHERE [Id]=5480
-- Operation applied to 670 rows out of 670

-- Add rows to [dbo].[Library.LocalisationItem]
SET IDENTITY_INSERT [dbo].[Library.LocalisationItem] ON
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754825, N'MilitaryOccupation.US.MC.6046.0.AircraftMaintenanceAdministrationSpecialist', N'Aircraft Maintenance Administration Specialist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754826, N'MilitaryOccupation.US.NAVY.AZ.0.AviationMaintenanceAdministrationman', N'Aviation Maintenance Administrationman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754827, N'MilitaryOccupation.US.NAVY.6399.0.AviationMaintenanceAdministrationmanBasic', N'Aviation Maintenance Administrationman Basic', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754828, N'MilitaryOccupation.US.NAVY.9750.0.FunctionalSupportandAdministration', N'Functional Support and Administration', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754829, N'MilitaryOccupation.US.AF.8C000.0.AirmanandFamilyReadinessCenterReadinessNCO', N'Airman and Family Readiness Center Readiness NCO', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754830, N'MilitaryOccupation.US.AF.8H000.0.AirmanDormLeader', N'Airman Dorm Leader', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754831, N'MilitaryOccupation.US.AF.8A100.0.CareerAssistanceAdvisor', N'Career Assistance Advisor', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754832, N'MilitaryOccupation.US.CG.360.0.Yeoman', N'Yeoman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754833, N'MilitaryOccupation.US.MC.0100.0.BasicPersonnelandAdministrationMarine', N'Basic Personnel and Administration Marine', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754834, N'MilitaryOccupation.US.MC.8911.0.BarracksandGroundsMarine', N'Barracks and Grounds Marine', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754835, N'MilitaryOccupation.US.NAVY.92FO.0.ForceMasterChiefPettyOfficer', N'Force Master Chief Petty Officer', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754836, N'MilitaryOccupation.US.CG.990.0.MasterChiefPettyOfficeroftheCoastGuard', N'Master Chief Petty Officer of the Coast Guard', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754837, N'MilitaryOccupation.US.CG.910.0.PettyOfficer', N'Petty Officer', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754838, N'MilitaryOccupation.US.MC.6012.0.AviationMaintenanceControllerProductionController', N'Aviation Maintenance Controller/Production Controller', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754839, N'MilitaryOccupation.US.CG.525.0.AviationMaintenanceTechnician', N'Aviation Maintenance Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754840, N'MilitaryOccupation.US.MC.6018.0.AviationQualityAssuranceRepresentativeQARInspector', N'Aviation Quality Assurance Representative (QAR)/Inspector', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754841, N'MilitaryOccupation.US.CG.553.0.AvionicsElectricalTechnician', N'Avionics Electrical Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754842, N'MilitaryOccupation.US.MC.6000.0.BasicAircraftMaintenanceMarine', N'Basic Aircraft Maintenance Marine', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754843, N'MilitaryOccupation.US.MC.6500.0.BasicAviationOrdnanceMarine', N'Basic Aviation Ordnance Marine', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754844, N'MilitaryOccupation.US.CG.535.0.AviationSurvivalTechnician', N'Aviation Survival Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754845, N'MilitaryOccupation.US.CG.610.0.Airman', N'Airman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754846, N'MilitaryOccupation.US.NAVY.9700.0.InfantryGunCrewsandSeamanshipSpecialists', N'Infantry, Gun Crews, and Seamanship Specialists', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754847, N'MilitaryOccupation.US.NAVY.5323.0.SDVPilotNavigatorDDSOperator', N'SDV Pilot/Navigator/DDS Operator', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754848, N'MilitaryOccupation.US.NAVY.SB.0.SpecialWarfareBoatOperator', N'Special Warfare Boat Operator', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754849, N'MilitaryOccupation.US.NAVY.SO.0.SpecialWarfareOperator', N'Special Warfare Operator', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754850, N'MilitaryOccupation.US.AF.1T231.0.PararescueApprentice', N'Pararescue Apprentice', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754851, N'MilitaryOccupation.US.AF.1T271.0.PararescueCraftsman', N'Pararescue Craftsman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754852, N'MilitaryOccupation.US.AF.1T211.0.PararescueHelper', N'Pararescue Helper', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754853, N'MilitaryOccupation.US.AF.1T251.0.PararescueJourneyman', N'Pararescue Journeyman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754854, N'MilitaryOccupation.US.AF.1T200.0.PararescueManager', N'Pararescue Manager', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754855, N'MilitaryOccupation.US.AF.1T291.0.PararescueSuperintendent', N'Pararescue Superintendent', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754856, N'MilitaryOccupation.US.MC.5832.0.CorrectionalCounselor', N'Correctional Counselor', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754857, N'MilitaryOccupation.US.NAVY.9516.0.CorrectionalCounselor', N'Correctional Counselor', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754858, N'MilitaryOccupation.US.CG.401.0.Investigator', N'Investigator', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754859, N'MilitaryOccupation.US.MC.5814.0.PhysicalSecuritySpecialist', N'Physical Security Specialist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754860, N'MilitaryOccupation.US.CG.741.0.PortSecuritySpecialist', N'Port Security Specialist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754861, N'MilitaryOccupation.US.NAVY.9556.0.SecuritySpecialist', N'Security Specialist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754862, N'MilitaryOccupation.US.ARMY.18F.0.SpecialForcesAssistantOperationsandIntelligenceSergeant', N'Special Forces Assistant Operations and Intelligence Sergeant', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754863, N'MilitaryOccupation.US.ARMY.18B.0.SpecialForcesWeaponsSergeant', N'Special Forces Weapons Sergeant', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754864, N'MilitaryOccupation.US.NAVY.9190.0.SpecialSecurityAssistant', N'Special Security Assistant', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754865, N'MilitaryOccupation.US.CG.170.0.GunnersMate', N'Gunner''s Mate', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754866, N'MilitaryOccupation.US.ARMY.18C.0.SpecialForcesEngineerSergeant', N'Special Forces Engineer Sergeant', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754867, N'MilitaryOccupation.US.NAVY.9554.0.MasterNavalParachutist', N'Master Naval Parachutist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754868, N'MilitaryOccupation.US.MC.8023.0.Parachutist', N'Parachutist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754869, N'MilitaryOccupation.US.MC.0323.0.ReconnaissanceManParachutistQualified', N'Reconnaissance Man, Parachutist Qualified', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754870, N'MilitaryOccupation.US.NAVY.53B9.0.SpecialOperationsBoats', N'Special Operations Boats', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754871, N'MilitaryOccupation.US.NAVY.9557.0.JointSpecialOperations', N'Joint Special Operations', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754872, N'MilitaryOccupation.US.NAVY.53S9.0.SpecialOperations', N'Special Operations', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754873, N'MilitaryOccupation.US.ARMY.12Y.0.GeospatialEngineer', N'Geospatial Engineer', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754874, N'MilitaryOccupation.US.MC.1100.0.BasicUtilitiesMarine', N'Basic Utilities Marine', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754875, N'MilitaryOccupation.US.NAVY.4123.0.CG47GasTurbineElectricalMaintenanceTechnician', N'CG 47 Gas Turbine Electrical Maintenance Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754876, N'MilitaryOccupation.US.NAVY.4124.0.CG47GasTurbineMechanicalMaintenanceTechnician', N'CG 47 Gas Turbine Mechanical Maintenance Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754877, N'MilitaryOccupation.US.NAVY.4125.0.DDG51GasTurbineElectricalMaintenanceTechnician', N'DDG 51 Gas Turbine Electrical Maintenance Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754878, N'MilitaryOccupation.US.NAVY.4126.0.DDG51GasTurbineMechanicalMaintenanceTechnician', N'DDG-51 Gas Turbine Mechanical Maintenance Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754879, N'MilitaryOccupation.US.NAVY.4146.0.DDGMGasTurbineMechanicalMaintenanceTechnician', N'DDG-M Gas Turbine Mechanical Maintenance Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754880, N'MilitaryOccupation.US.CG.270.0.ElectriciansMate', N'Electrician''s Mate', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754881, N'MilitaryOccupation.US.NAVY.4129.0.FFG7GasTurbineElectricalMaintenanceTechnician', N'FFG-7 Gas Turbine Electrical Maintenance Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754882, N'MilitaryOccupation.US.NAVY.4128.0.FFG7GasTurbineMechanicalMaintenanceTechnician', N'FFG-7 Gas Turbine Mechanical Maintenance Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754883, N'MilitaryOccupation.US.ARMY.88T.0.RailwaySectionRepairerRC', N'Railway Section Repairer (RC)', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754884, N'MilitaryOccupation.US.CG.150.0.Seaman', N'Seaman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754885, N'MilitaryOccupation.US.NAVY.SN.0.Seaman', N'Seaman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754886, N'MilitaryOccupation.US.AF.6C031.0.ContractingApprentice', N'Contracting Apprentice', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754887, N'MilitaryOccupation.US.AF.6C011.0.ContractingHelper', N'Contracting Helper', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754888, N'MilitaryOccupation.US.AF.6C051.0.ContractingJourneyman', N'Contracting Journeyman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754889, N'MilitaryOccupation.US.CG.200.0.MachineryTechnician', N'Machinery Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754890, N'MilitaryOccupation.US.AF.2R031.0.MaintenanceManagementAnalysisApprentice', N'Maintenance Management Analysis Apprentice', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754891, N'MilitaryOccupation.US.AF.2R011.0.MaintenanceManagementAnalysisHelper', N'Maintenance Management Analysis Helper', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754892, N'MilitaryOccupation.US.AF.2R051.0.MaintenanceManagementAnalysisJourneyman', N'Maintenance Management Analysis Journeyman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754893, N'MilitaryOccupation.US.AF.2R131.0.MaintenanceManagementProductionApprentice', N'Maintenance Management Production,  Apprentice', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754894, N'MilitaryOccupation.US.AF.2R111.0.MaintenanceManagementProductionHelper', N'Maintenance Management Production,  Helper', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754895, N'MilitaryOccupation.US.AF.2R151.0.MaintenanceManagementProductionJourneyman', N'Maintenance Management Production, Journeyman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754896, N'MilitaryOccupation.US.NAVY.CU.0.MasterChiefConstructionman', N'Master Chief Constructionman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754897, N'MilitaryOccupation.US.NAVY.UC.0.MasterChiefUtilitiesman', N'Master Chief Utilitiesman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754898, N'MilitaryOccupation.US.NAVY.4952.0.NAMTSPipefitter', N'NAMTS Pipefitter', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754899, N'MilitaryOccupation.US.ARMY.12P.0.PrimePowerProductionSpecialist', N'Prime Power Production Specialist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754900, N'MilitaryOccupation.US.ARMY.12G.0.QuarryingSpecialistRC', N'Quarrying Specialist (RC)', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754901, N'MilitaryOccupation.US.NAVY.5707.0.WaterWellDrillingTechnician', N'Water Well Drilling Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754902, N'MilitaryOccupation.US.NAVY.EQ.0.MasterChiefEquipmentman', N'Master Chief Equipmentman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754903, N'MilitaryOccupation.US.NAVY.9583.0.Locksmith', N'Locksmith', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754904, N'MilitaryOccupation.US.NAVY.0416.0.AcousticIntelligenceSpecialist', N'Acoustic Intelligence Specialist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754905, N'MilitaryOccupation.US.MC.0200.0.BasicIntelligenceMarine', N'Basic Intelligence Marine', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754906, N'MilitaryOccupation.US.NAVY.5326.0.CombatantSwimmerSEAL', N'Combatant Swimmer (SEAL)', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754907, N'MilitaryOccupation.US.NAVY.9720.0.CommunicationsandIntelligenceSpecialists', N'Communications and Intelligence Specialists', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754908, N'MilitaryOccupation.US.NAVY.00CA.0.CryptologicTechnicianAdministrativeNEC', N'Cryptologic Technician Administrative NEC', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754909, N'MilitaryOccupation.US.NAVY.3912.0.ExpeditionaryWarfareIntelligenceSpecialist', N'Expeditionary Warfare Intelligence Specialist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754910, N'MilitaryOccupation.US.AF.1N000.0.IntelligenceManager', N'Intelligence Manager', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754911, N'MilitaryOccupation.US.NAVY.3999.0.IntelligenceSpecialistBasic', N'Intelligence Specialist Basic', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754912, N'MilitaryOccupation.US.NAVY.0505.0.IUSSAnalyst', N'IUSS Analyst', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754913, N'MilitaryOccupation.US.NAVY.0507.0.IUSSMasterAnalyst', N'IUSS Master Analyst', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754914, N'MilitaryOccupation.US.NAVY.0450.0.JourneymanLevelAcousticAnalyst', N'Journeyman Level Acoustic Analyst', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754915, N'MilitaryOccupation.US.MC.6049.0.NALCOMISApplicationAdministratorAnalyst', N'NALCOMIS Application Administrator/Analyst', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754916, N'MilitaryOccupation.US.NAVY.9102.0.NationalOPELINTAnalyst', N'National OPELINT Analyst', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754917, N'MilitaryOccupation.US.NAVY.3910.0.NavalImageryInterpreter', N'Naval Imagery Interpreter', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754918, N'MilitaryOccupation.US.NAVY.3924.0.OperationalIntelligenceOPINTELAnalyst', N'Operational Intelligence (OPINTEL) Analyst', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754919, N'MilitaryOccupation.US.CG.205.0.OperationsSpecialists', N'Operations Specialist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754920, N'MilitaryOccupation.US.NAVY.0501.0.SonarSubmarinesLeadingChiefPettyOfficer', N'Sonar (Submarines) Leading Chief Petty Officer', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754921, N'MilitaryOccupation.US.NAVY.3923.0.StrikePlanningApplications', N'Strike Planning Applications', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754922, N'MilitaryOccupation.US.CG.275.0.InformationSystemsTechnician', N'Information Systems Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754923, N'MilitaryOccupation.US.CG.420.0.Storekeeper', N'Storekeeper', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754924, N'MilitaryOccupation.US.MC.3044.0.BasicContingencyContractSpecialist', N'Basic Contingency Contract Specialist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754925, N'MilitaryOccupation.US.MC.6617.0.EnlistedAviationLogistician', N'Enlisted Aviation Logistician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754926, N'MilitaryOccupation.US.NAVY.90SP.0.IndividualGWOTIAILOSpecialOerationsSupportTeam', N'Individual GWOT IA/ILO Special Operations Support Team', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754927, N'MilitaryOccupation.US.NAVY.9780.0.ServiceandSupplyHandlers', N'Service and Supply Handlers', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754928, N'MilitaryOccupation.US.MC.4100.0.BasicMarineCorpsCommunityServicesMarine', N'Basic Marine Corps Community Services Marine', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754929, N'MilitaryOccupation.US.NAVY.3199.0.ShipServicemanBasic', N'Ship Serviceman Basic', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754930, N'MilitaryOccupation.US.NAVY.SH.0.ShipsServiceman', N'Ship''s Serviceman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754931, N'MilitaryOccupation.US.NAVY.9517.0.Ships3MSystemCoordinator', N'Ship''s 3-M System Coordinator', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754932, N'MilitaryOccupation.US.NAVY.ABH.0.AviationBoatswainsMateAircraftHandling', N'Aviation Boatswain''s Mate, Aircraft Handling', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754933, N'MilitaryOccupation.US.NAVY.90SN.0.IndividualGWOTIAILOSpecialOperationsSupportNotDeployed', N'Individual GWOT IA/ILO Special Operations Support, Not Deployed', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754934, N'MilitaryOccupation.US.AF.1N490.0.FusionAnalysisSuperintendent', N'Fusion Analysis Superintendent', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754935, N'MilitaryOccupation.US.NAVY.00LI.0.LithographerTrackingNEC', N'Lithographer Tracking NEC', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754936, N'MilitaryOccupation.US.CG.500.0.FoodServiceSpecialist', N'Food Service Specialist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754937, N'MilitaryOccupation.US.ARMY.68R.0.VeterinaryFoodInspectionSpecialist', N'Veterinary Food Inspection Specialist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754938, N'MilitaryOccupation.US.MC.3300.0.BasicFoodServiceMarine', N'Basic Food Service Marine', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754939, N'MilitaryOccupation.US.ARMY.42R.0.ArmyBandperson', N'Army Bandperson', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754940, N'MilitaryOccupation.US.NAVY.3852.0.Arranger', N'Arranger', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754941, N'MilitaryOccupation.US.MC.5500.0.BasicMusician', N'Basic Musician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754942, N'MilitaryOccupation.US.NAVY.3804.0.BassoonInstrumentalist', N'Bassoon Instrumentalist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754943, N'MilitaryOccupation.US.NAVY.3854.0.CeremonialConductorDrumMajor', N'Ceremonial Conductor-Drum Major', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754944, N'MilitaryOccupation.US.NAVY.3803.0.ClarinetInstrumentalist', N'Clarinet Instrumentalist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754945, N'MilitaryOccupation.US.MC.5521.0.DrumMajor', N'Drum Major', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754946, N'MilitaryOccupation.US.NAVY.3815.0.ElectricBassStringBassInstrumentalist', N'Electric Bass/String Bass Instrumentalist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754947, N'MilitaryOccupation.US.NAVY.3851.0.EnlistedBandLeader', N'Enlisted Band Leader', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754948, N'MilitaryOccupation.US.MC.5519.0.EnlistedConductor', N'Enlisted Conductor', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754949, N'MilitaryOccupation.US.NAVY.3808.0.EuphoniumInstrumentalist', N'Euphonium Instrumentalist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754950, N'MilitaryOccupation.US.NAVY.3801.0.FlutePiccoloInstrumentalist', N'Flute/Piccolo Instrumentalist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754951, N'MilitaryOccupation.US.NAVY.3807.0.FrenchHornInstrumentalist', N'French Horn Instrumentalist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754952, N'MilitaryOccupation.US.NAVY.3812.0.GuitarInstrumentalist', N'Guitar Instrumentalist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754953, N'MilitaryOccupation.US.MC.5523.0.InstrumentRepairTechnician', N'Instrument Repair Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754954, N'MilitaryOccupation.US.NAVY.3814.0.KeyboardInstrumentalist', N'Keyboard Instrumentalist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754955, N'MilitaryOccupation.US.MC.5512.0.MemberTheCommandantsOwnUSMarineDrumandBugleCorps', N'Member, "The Commandant''s Own," U.S. Marine Drum and Bugle Corps', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754956, N'MilitaryOccupation.US.MC.5511.0.MemberThePresidentsOwnUnitedStatesMarineBand', N'Member, The President''s Own, United States Marine Band', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754957, N'MilitaryOccupation.US.CG.840.0.Musician', N'Musician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754958, N'MilitaryOccupation.US.MC.5524.0.Musician', N'Musician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754959, N'MilitaryOccupation.US.NAVY.MU.0.Musician', N'Musician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754960, N'MilitaryOccupation.US.NAVY.3899.0.MusicianBasic', N'Musician Basic', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754961, N'MilitaryOccupation.US.MC.5528.0.MusicianBassoon', N'Musician, Bassoon', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754962, N'MilitaryOccupation.US.MC.5534.0.MusicianClarinet', N'Musician, Clarinet', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754963, N'MilitaryOccupation.US.MC.5548.0.MusicianElectricBass', N'Musician, Electric Bass', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754964, N'MilitaryOccupation.US.MC.5543.0.MusicianEuphonium', N'Musician, Euphonium', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754965, N'MilitaryOccupation.US.MC.5536.0.MusicianFlutePiccolo', N'Musician, Flute/Piccolo', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754966, N'MilitaryOccupation.US.MC.5566.0.MusicianGuitar', N'Musician, Guitar', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754967, N'MilitaryOccupation.US.MC.5544.0.MusicianHorn', N'Musician, Horn', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754968, N'MilitaryOccupation.US.MC.5526.0.MusicianOboe', N'Musician, Oboe', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754969, N'MilitaryOccupation.US.MC.5563.0.MusicianPercussionDrumsTimpaniMallets', N'Musician, Percussion (Drums, Timpani, Mallets)', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754970, N'MilitaryOccupation.US.MC.5565.0.MusicianPiano', N'Musician, Piano', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754971, N'MilitaryOccupation.US.MC.5537.0.MusicianSaxophone', N'Musician, Saxophone', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754972, N'MilitaryOccupation.US.MC.5546.0.MusicianTrombone', N'Musician, Trombone', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754973, N'MilitaryOccupation.US.MC.5541.0.MusicianTrumpet', N'Musician, Trumpet', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754974, N'MilitaryOccupation.US.MC.5547.0.MusicianTubaSousaphone', N'Musician, Tuba/Sousaphone', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754975, N'MilitaryOccupation.US.NAVY.3802.0.OboeInstrumentalist', N'Oboe Instrumentalist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754976, N'MilitaryOccupation.US.NAVY.3813.0.PercussionInstrumentalist', N'Percussion Instrumentalist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754977, N'MilitaryOccupation.US.AF.3N231.0.PremierBandApprentice', N'Premier Band Apprentice', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754978, N'MilitaryOccupation.US.AF.3N271.0.PremierBandCraftsman', N'Premier Band Craftsman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754979, N'MilitaryOccupation.US.AF.3N211.0.PremierBandHelper', N'Premier Band Helper', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754980, N'MilitaryOccupation.US.AF.3N200.0.PremierBandManager', N'Premier Band Manager', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754981, N'MilitaryOccupation.US.AF.3N291.0.PremierBandSuperintendent', N'Premier Band Superintendent', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754982, N'MilitaryOccupation.US.AF.3N131Q.0.RegionalBandApprenticeBagpipe', N'Regional Band Apprentice, Bagpipe', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754983, N'MilitaryOccupation.US.AF.3N131H.0.RegionalBandApprenticeBaritoneOrEuphonium', N'Regional Band Apprentice, Baritone Or Euphonium', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754984, N'MilitaryOccupation.US.AF.3N131C.0.RegionalBandApprenticeBassoon', N'Regional Band Apprentice, Bassoon', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754985, N'MilitaryOccupation.US.AF.3N131A.0.RegionalBandApprenticeClarinet', N'Regional Band Apprentice, Clarinet', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754986, N'MilitaryOccupation.US.AF.3N131G.0.RegionalBandApprenticeCornetOrTrumpet', N'Regional Band Apprentice, Cornet Or Trumpet', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754987, N'MilitaryOccupation.US.AF.3N131S.0.RegionalBandApprenticeElectricBassStringBass', N'Regional Band Apprentice, Electric Bass/String Bass', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754988, N'MilitaryOccupation.US.AF.3N131E.0.RegionalBandApprenticeFluteOrPiccolo', N'Regional Band Apprentice, Flute Or Piccolo', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754989, N'MilitaryOccupation.US.AF.3N131F.0.RegionalBandApprenticeFrenchHorn', N'Regional Band Apprentice, French Horn', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754990, N'MilitaryOccupation.US.AF.3N131N.0.RegionalBandApprenticeGuitar', N'Regional Band Apprentice, Guitar', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754991, N'MilitaryOccupation.US.AF.3N131Z.0.RegionalBandApprenticeInstrumentalistGeneralAirNationalGuardBands', N'Regional Band Apprentice, Instrumentalist, General (Air National Guard Bands)', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754992, N'MilitaryOccupation.US.AF.3N131T.0.RegionalBandApprenticeMilitaryBandSupportUSAFBand', N'Regional Band Apprentice, Military Band Support (USAF Band)', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754993, N'MilitaryOccupation.US.AF.3N131P.0.RegionalBandApprenticeMusicArranger', N'Regional Band Apprentice, Music Arranger', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754994, N'MilitaryOccupation.US.AF.3N131D.0.RegionalBandApprenticeOboe', N'Regional Band Apprentice, Oboe', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754995, N'MilitaryOccupation.US.AF.3N131L.0.RegionalBandApprenticePercussion', N'Regional Band Apprentice, Percussion', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754996, N'MilitaryOccupation.US.AF.3N131M.0.RegionalBandApprenticePiano', N'Regional Band Apprentice, Piano', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754997, N'MilitaryOccupation.US.AF.3N131B.0.RegionalBandApprenticeSaxophone', N'Regional Band Apprentice, Saxophone', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754998, N'MilitaryOccupation.US.AF.3N131U.0.RegionalBandApprenticeSteelGuitar', N'Regional Band Apprentice, Steel Guitar', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4754999, N'MilitaryOccupation.US.AF.3N131J.0.RegionalBandApprenticeTrombone', N'Regional Band Apprentice, Trombone', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755000, N'MilitaryOccupation.US.AF.3N131K.0.RegionalBandApprenticeTuba', N'Regional Band Apprentice, Tuba', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755001, N'MilitaryOccupation.US.AF.3N131R.0.RegionalBandApprenticeVocalist', N'Regional Band Apprentice, Vocalist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755002, N'MilitaryOccupation.US.AF.3N171Q.0.RegionalBandCraftsmanBagpipe', N'Regional Band Craftsman, Bagpipe', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755003, N'MilitaryOccupation.US.AF.3N171H.0.RegionalBandCraftsmanBaritoneorEuphonium', N'Regional Band Craftsman, Baritone or Euphonium', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755004, N'MilitaryOccupation.US.AF.3N171C.0.RegionalBandCraftsmanBassoon', N'Regional Band Craftsman, Bassoon', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755005, N'MilitaryOccupation.US.AF.3N171A.0.RegionalBandCraftsmanClarinet', N'Regional Band Craftsman, Clarinet', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755006, N'MilitaryOccupation.US.AF.3N171G.0.RegionalBandCraftsmanCornetorTrumpet', N'Regional Band Craftsman, Cornet or Trumpet', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755007, N'MilitaryOccupation.US.AF.3N171S.0.RegionalBandCraftsmanElectricBassstringBass', N'Regional Band Craftsman, Electric Bass/string Bass', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755008, N'MilitaryOccupation.US.AF.3N171E.0.RegionalBandCraftsmanFluteorPiccolo', N'Regional Band Craftsman, Flute or Piccolo', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755009, N'MilitaryOccupation.US.AF.3N171F.0.RegionalBandCraftsmanFrenchHorn', N'Regional Band Craftsman, French Horn', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755010, N'MilitaryOccupation.US.AF.3N171N.0.RegionalBandCraftsmanGuitar', N'Regional Band Craftsman, Guitar', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755011, N'MilitaryOccupation.US.AF.3N171Z.0.RegionalBandCraftsmanInstrumentalistGeneralAirNationalGuardBands', N'Regional Band Craftsman, Instrumentalist, General (Air National Guard Bands)', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755012, N'MilitaryOccupation.US.AF.3N171T.0.RegionalBandCraftsmanMilitaryBandSupportUSAFBand', N'Regional Band Craftsman, Military Band Support (USAF Band)', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755013, N'MilitaryOccupation.US.AF.3N171P.0.RegionalBandCraftsmanMusicArranger', N'Regional Band Craftsman, Music Arranger', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755014, N'MilitaryOccupation.US.AF.3N171D.0.RegionalBandCraftsmanOboe', N'Regional Band Craftsman, Oboe', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755015, N'MilitaryOccupation.US.AF.3N171L.0.RegionalBandCraftsmanPercussion', N'Regional Band Craftsman, Percussion', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755016, N'MilitaryOccupation.US.AF.3N171M.0.RegionalBandCraftsmanPiano', N'Regional Band Craftsman, Piano', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755017, N'MilitaryOccupation.US.AF.3N171B.0.RegionalBandCraftsmanSaxophone', N'Regional Band Craftsman, Saxophone', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755018, N'MilitaryOccupation.US.AF.3N171U.0.RegionalBandCraftsmanSteelGuitar', N'Regional Band Craftsman, Steel Guitar', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755019, N'MilitaryOccupation.US.AF.3N171J.0.RegionalBandCraftsmanTrombone', N'Regional Band Craftsman, Trombone', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755020, N'MilitaryOccupation.US.AF.3N171K.0.RegionalBandCraftsmanTuba', N'Regional Band Craftsman, Tuba', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755021, N'MilitaryOccupation.US.AF.3N171R.0.RegionalBandCraftsmanVocalist', N'Regional Band Craftsman, Vocalist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755022, N'MilitaryOccupation.US.AF.3N111Q.0.RegionalBandHelperBagpipe', N'Regional Band Helper, Bagpipe', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755023, N'MilitaryOccupation.US.AF.3N111H.0.RegionalBandHelperBaritoneorEuphonium', N'Regional Band Helper, Baritone or Euphonium', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755024, N'MilitaryOccupation.US.AF.3N111C.0.RegionalBandHelperBassoon', N'Regional Band Helper, Bassoon', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755025, N'MilitaryOccupation.US.AF.3N111A.0.RegionalBandHelperClarinet', N'Regional Band Helper, Clarinet', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755026, N'MilitaryOccupation.US.AF.3N111G.0.RegionalBandHelperCornetorTrumpet', N'Regional Band Helper, Cornet or Trumpet', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755027, N'MilitaryOccupation.US.AF.3N111S.0.RegionalBandHelperElectricBassStringBass', N'Regional Band Helper, Electric Bass/String Bass', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755028, N'MilitaryOccupation.US.AF.3N111E.0.RegionalBandHelperFluteorPiccolo', N'Regional Band Helper, Flute or Piccolo', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755029, N'MilitaryOccupation.US.AF.3N111F.0.RegionalBandHelperFrenchHorn', N'Regional Band Helper, French Horn', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755030, N'MilitaryOccupation.US.AF.3N111N.0.RegionalBandHelperGuitar', N'Regional Band Helper, Guitar', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755031, N'MilitaryOccupation.US.AF.3N111Z.0.RegionalBandHelperInstrumentalistGeneralAirNationalGuardBands', N'Regional Band Helper, Instrumentalist, General (Air National Guard Bands)', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755032, N'MilitaryOccupation.US.AF.3N111T.0.RegionalBandHelperMilitaryBandSupportUSAFBand', N'Regional Band Helper, Military Band Support (USAF Band)', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755033, N'MilitaryOccupation.US.AF.3N111P.0.RegionalBandHelperMusicArranger', N'Regional Band Helper, Music Arranger', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755034, N'MilitaryOccupation.US.AF.3N111D.0.RegionalBandHelperOboe', N'Regional Band Helper, Oboe', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755035, N'MilitaryOccupation.US.AF.3N111L.0.RegionalBandHelperPercussion', N'Regional Band Helper, Percussion', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755036, N'MilitaryOccupation.US.AF.3N111M.0.RegionalBandHelperPiano', N'Regional Band Helper, Piano', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755037, N'MilitaryOccupation.US.AF.3N111B.0.RegionalBandHelperSaxophone', N'Regional Band Helper, Saxophone', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755038, N'MilitaryOccupation.US.AF.3N111U.0.RegionalBandHelperSteelGuitar', N'Regional Band Helper, Steel Guitar', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755039, N'MilitaryOccupation.US.AF.3N111J.0.RegionalBandHelperTrombone', N'Regional Band Helper, Trombone', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755040, N'MilitaryOccupation.US.AF.3N111K.0.RegionalBandHelperTuba', N'Regional Band Helper, Tuba', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755041, N'MilitaryOccupation.US.AF.3N111R.0.RegionalBandHelperVocalist', N'Regional Band Helper, Vocalist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755042, N'MilitaryOccupation.US.AF.3N151Q.0.RegionalBandJourneymanBagpipe', N'Regional Band Journeyman, Bagpipe', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755043, N'MilitaryOccupation.US.AF.3N151H.0.RegionalBandJourneymanBaritoneOrEuphonium', N'Regional Band Journeyman, Baritone Or Euphonium', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755044, N'MilitaryOccupation.US.AF.3N151C.0.RegionalBandJourneymanBassoon', N'Regional Band Journeyman, Bassoon', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755045, N'MilitaryOccupation.US.AF.3N151A.0.RegionalBandJourneymanClarinet', N'Regional Band Journeyman, Clarinet', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755046, N'MilitaryOccupation.US.AF.3N151G.0.RegionalBandJourneymanCornetOrTrumpet', N'Regional Band Journeyman, Cornet Or Trumpet', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755047, N'MilitaryOccupation.US.AF.3N151S.0.RegionalBandJourneymanElectricBassStringBass', N'Regional Band Journeyman, Electric Bass/String Bass', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755048, N'MilitaryOccupation.US.AF.3N151E.0.RegionalBandJourneymanFluteOrPiccolo', N'Regional Band Journeyman, Flute Or Piccolo', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755049, N'MilitaryOccupation.US.AF.3N151F.0.RegionalBandJourneymanFrenchHorn', N'Regional Band Journeyman, French Horn', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755050, N'MilitaryOccupation.US.AF.3N151N.0.RegionalBandJourneymanGuitar', N'Regional Band Journeyman, Guitar', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755051, N'MilitaryOccupation.US.AF.3N151Z.0.RegionalBandJourneymanInstrumentalistGeneralAirNationalGuardBands', N'Regional Band Journeyman, Instrumentalist, General (Air National Guard Bands)', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755052, N'MilitaryOccupation.US.AF.3N151T.0.RegionalBandJourneymanMilitaryBandSupportUSAFBand', N'Regional Band Journeyman, Military Band Support (USAF Band)', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755053, N'MilitaryOccupation.US.AF.3N151P.0.RegionalBandJourneymanMusicArranger', N'Regional Band Journeyman, Music Arranger', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755054, N'MilitaryOccupation.US.AF.3N151D.0.RegionalBandJourneymanOboe', N'Regional Band Journeyman, Oboe', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755055, N'MilitaryOccupation.US.AF.3N151L.0.RegionalBandJourneymanPercussion', N'Regional Band Journeyman, Percussion', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755056, N'MilitaryOccupation.US.AF.3N151M.0.RegionalBandJourneymanPiano', N'Regional Band Journeyman, Piano', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755057, N'MilitaryOccupation.US.AF.3N151B.0.RegionalBandJourneymanSaxophone', N'Regional Band Journeyman, Saxophone', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755058, N'MilitaryOccupation.US.AF.3N151U.0.RegionalBandJourneymanSteelGuitar', N'Regional Band Journeyman, Steel Guitar', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755059, N'MilitaryOccupation.US.AF.3N151J.0.RegionalBandJourneymanTrombone', N'Regional Band Journeyman, Trombone', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755060, N'MilitaryOccupation.US.AF.3N151K.0.RegionalBandJourneymanTuba', N'Regional Band Journeyman, Tuba', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755061, N'MilitaryOccupation.US.AF.3N151R.0.RegionalBandJourneymanVocalist', N'Regional Band Journeyman, Vocalist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755062, N'MilitaryOccupation.US.AF.3N100.0.RegionalBandManager', N'Regional Band Manager', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755063, N'MilitaryOccupation.US.AF.3N191.0.RegionalBandSuperintendent', N'Regional Band Superintendent', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755064, N'MilitaryOccupation.US.NAVY.3805.0.SaxophoneInstrumentalist', N'Saxophone Instrumentalist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755065, N'MilitaryOccupation.US.MC.5522.0.SmallEnsembleLeader', N'Small Ensemble Leader', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755066, N'MilitaryOccupation.US.ARMY.42S.0.SpecialBandMember', N'Special Band Member', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755067, N'MilitaryOccupation.US.NAVY.3809.0.TromboneInstrumentalist', N'Trombone Instrumentalist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755068, N'MilitaryOccupation.US.NAVY.3806.0.TrumpetInstrumentalist', N'Trumpet Instrumentalist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755069, N'MilitaryOccupation.US.NAVY.3811.0.TubaInstrumentalist', N'Tuba Instrumentalist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755070, N'MilitaryOccupation.US.NAVY.3853.0.UnitLeader', N'Unit Leader', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755071, N'MilitaryOccupation.US.NAVY.3825.0.VocalistEntertainer', N'Vocalist/Entertainer', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755072, N'MilitaryOccupation.US.NAVY.4131.0.LCACCraftEngineerAssistantOperator', N'LCAC Craft Engineer/Assistant Operator', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755073, N'MilitaryOccupation.US.ARMY.88U.0.RailwayOperationsCrewmemberRC', N'Railway Operations Crewmember (RC)', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755074, N'MilitaryOccupation.US.CG.100.0.BoatswainsMate', N'Boatswain''s Mate', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755075, N'MilitaryOccupation.US.MC.0316.0.CombatRubberReconnaissanceCraftCRRCCoxswain', N'Combat Rubber Reconnaissance Craft (CRRC) Coxswain', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755076, N'MilitaryOccupation.US.MC.0314.0.RigidRaidingCraftRRCRigidHullInflatableBoatRHIBCoxswain', N'Rigid Raiding Craft (RRC)/Rigid Hull Inflatable Boat (RHIB) Coxswain', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755077, N'MilitaryOccupation.US.AF.8P000.0.Courier', N'Courier', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755078, N'MilitaryOccupation.US.NAVY.3001.0.IndependentDutyPostalClerk', N'Independent Duty Postal Clerk', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755079, N'MilitaryOccupation.US.AF.8M000.0.Postal', N'Postal', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755080, N'MilitaryOccupation.US.NAVY.00PC.0.PostalClerkPCTrackingNEC', N'Postal Clerk (PC) Tracking NEC', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755081, N'MilitaryOccupation.US.ARMY.25M.0.MultimediaIllustrator', N'Multimedia Illustrator', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755082, N'MilitaryOccupation.US.CG.340.0.PublicAffairsSpecialist', N'Public Affairs Specialist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755083, N'MilitaryOccupation.US.CG.890.0.HealthServicesDental', N'Health Services - Dental', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755084, N'MilitaryOccupation.US.NAVY.5392.0.NavalSpecialWarfareMedic', N'Naval Special Warfare Medic', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755085, N'MilitaryOccupation.US.ARMY.18D.0.SpecialForcesMedicalSergeant', N'Special Forces Medical Sergeant', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755086, N'MilitaryOccupation.US.NAVY.8454.0.ElectroneurodiagnosticTechnologist', N'Electroneurodiagnostic Technologist', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755087, N'MilitaryOccupation.US.CG.870.0.HealthServicesTechnician', N'Health Services Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755088, N'MilitaryOccupation.US.CG.790.0.MarineScienceTechnician', N'Marine Science Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755089, N'MilitaryOccupation.US.AF.4A231.0.BiomedicalEquipmentApprentice', N'Biomedical Equipment Apprentice', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755090, N'MilitaryOccupation.US.AF.4A271.0.BiomedicalEquipmentCraftsman', N'Biomedical Equipment Craftsman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755091, N'MilitaryOccupation.US.AF.4A211.0.BiomedicalEquipmentHelper', N'Biomedical Equipment Helper', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755092, N'MilitaryOccupation.US.AF.4A251.0.BiomedicalEquipmentJourneyman', N'Biomedical Equipment Journeyman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755093, N'MilitaryOccupation.US.AF.4A200.0.BiomedicalEquipmentManager', N'Biomedical Equipment Manager', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755094, N'MilitaryOccupation.US.AF.4A291.0.BiomedicalEquipmentSuperintendent', N'Biomedical Equipment Superintendent', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755095, N'MilitaryOccupation.US.NAVY.FN.0.Fireman', N'Fireman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755096, N'MilitaryOccupation.US.CG.210.0.DamageControlman', N'Damage Controlman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755097, N'MilitaryOccupation.US.AF.3E032.0.ElectricalPowerProductionApprentice', N'Electrical Power Production Apprentice', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755098, N'MilitaryOccupation.US.AF.3E072.0.ElectricalPowerProductionCraftsman', N'Electrical Power Production Craftsman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755099, N'MilitaryOccupation.US.AF.3E012.0.ElectricalPowerProductionHelper', N'Electrical Power Production Helper', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755100, N'MilitaryOccupation.US.AF.3E052.0.ElectricalPowerProductionJourneyman', N'Electrical Power Production Journeyman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755101, N'MilitaryOccupation.US.NAVY.9760.0.ElectricalMechanicalEquipmentRepairman', N'Electrical/Mechanical Equipment Repairman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755102, N'MilitaryOccupation.US.NAVY.4508.0.ElectronicAutomaticBoilerControlsConsoleOperator', N'Electronic Automatic Boiler Controls Console Operator', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755103, N'MilitaryOccupation.US.NAVY.4382.0.FFG7ClassAuxiliariesMechanicalSystemTechnician', N'FFG-7 Class Auxiliaries Mechanical System Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755104, N'MilitaryOccupation.US.CG.320.0.Fireman', N'Fireman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755105, N'MilitaryOccupation.US.NAVY.9581.0.Rubber&PlasticsWorker', N'Rubber & Plastics Worker', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755106, N'MilitaryOccupation.US.ARMY.18E.0.SpecialForcesCommunicationsSergeant', N'Special Forces Communications Sergeant', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755107, N'MilitaryOccupation.US.NAVY.9710.0.ElectronicEquipmentRepairman', N'Electronic Equipment Repairman', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755108, N'MilitaryOccupation.US.CG.240.0.ElectronicsTechnician', N'Electronics Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755109, N'MilitaryOccupation.US.NAVY.9535.0.AdvanceSealDeliverySystemMaintainer', N'Advance Seal Delivery System Maintainer', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755110, N'MilitaryOccupation.US.NAVY.4398.0.AuxiliarySystemsTechnician', N'Auxiliary Systems Technician', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755111, N'MilitaryOccupation.US.ARMY.88P.0.RailwayEquipmentRepairerRC', N'Railway Equipment Repairer (RC)', 12090)
INSERT INTO [dbo].[Library.LocalisationItem] ([Id], [Key], [Value], [LocalisationId]) VALUES (4755112, N'MilitaryOccupation.US.NAVY.9534.0.SealDeliveryVehicleSDVTeamTechnician', N'Seal Delivery Vehicle (SDV) Team Technician', 12090)
SET IDENTITY_INSERT [dbo].[Library.LocalisationItem] OFF
-- Operation applied to 288 rows out of 288

-- Add rows to [dbo].[Library.MilitaryOccupationGroup]
SET IDENTITY_INSERT [dbo].[Library.MilitaryOccupationGroup] ON
INSERT INTO [dbo].[Library.MilitaryOccupationGroup] ([Id], [Name]) VALUES (22, N'Musician')
INSERT INTO [dbo].[Library.MilitaryOccupationGroup] ([Id], [Name]) VALUES (23, N'Postal')
SET IDENTITY_INSERT [dbo].[Library.MilitaryOccupationGroup] OFF
-- Operation applied to 2 rows out of 2

-- Add rows to [dbo].[Library.MilitaryOccupation]
SET IDENTITY_INSERT [dbo].[Library.MilitaryOccupation] ON
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9540, N'6046', N'MilitaryOccupation.US.MC.6046.0.AircraftMaintenanceAdministrationSpecialist', 1000528, 0, 1)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9541, N'AZ', N'MilitaryOccupation.US.NAVY.AZ.0.AviationMaintenanceAdministrationman', 1000527, 0, 1)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9542, N'6399', N'MilitaryOccupation.US.NAVY.6399.0.AviationMaintenanceAdministrationmanBasic', 1000527, 0, 1)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9543, N'9750', N'MilitaryOccupation.US.NAVY.9750.0.FunctionalSupportandAdministration', 1000527, 0, 1)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9544, N'8C000', N'MilitaryOccupation.US.AF.8C000.0.AirmanandFamilyReadinessCenterReadinessNCO', 1000531, 0, 1)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9545, N'8H000', N'MilitaryOccupation.US.AF.8H000.0.AirmanDormLeader', 1000531, 0, 1)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9546, N'8A100', N'MilitaryOccupation.US.AF.8A100.0.CareerAssistanceAdvisor', 1000531, 0, 1)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9547, N'360', N'MilitaryOccupation.US.CG.360.0.Yeoman', 1000529, 0, 1)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9548, N'0100', N'MilitaryOccupation.US.MC.0100.0.BasicPersonnelandAdministrationMarine', 1000528, 0, 1)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9549, N'8911', N'MilitaryOccupation.US.MC.8911.0.BarracksandGroundsMarine', 1000528, 0, 1)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9550, N'92FO', N'MilitaryOccupation.US.NAVY.92FO.0.ForceMasterChiefPettyOfficer', 1000527, 0, 1)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9551, N'990', N'MilitaryOccupation.US.CG.990.0.MasterChiefPettyOfficeroftheCoastGuard', 1000529, 0, 1)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9552, N'910', N'MilitaryOccupation.US.CG.910.0.PettyOfficer', 1000529, 0, 1)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9553, N'6012', N'MilitaryOccupation.US.MC.6012.0.AviationMaintenanceControllerProductionController', 1000528, 0, 2)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9554, N'525', N'MilitaryOccupation.US.CG.525.0.AviationMaintenanceTechnician', 1000529, 0, 2)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9555, N'6018', N'MilitaryOccupation.US.MC.6018.0.AviationQualityAssuranceRepresentativeQARInspector', 1000528, 0, 2)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9556, N'553', N'MilitaryOccupation.US.CG.553.0.AvionicsElectricalTechnician', 1000529, 0, 2)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9557, N'6000', N'MilitaryOccupation.US.MC.6000.0.BasicAircraftMaintenanceMarine', 1000528, 0, 2)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9558, N'6500', N'MilitaryOccupation.US.MC.6500.0.BasicAviationOrdnanceMarine', 1000528, 0, 2)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9559, N'535', N'MilitaryOccupation.US.CG.535.0.AviationSurvivalTechnician', 1000529, 0, 2)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9560, N'610', N'MilitaryOccupation.US.CG.610.0.Airman', 1000529, 0, 2)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9561, N'9700', N'MilitaryOccupation.US.NAVY.9700.0.InfantryGunCrewsandSeamanshipSpecialists', 1000527, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9562, N'5323', N'MilitaryOccupation.US.NAVY.5323.0.SDVPilotNavigatorDDSOperator', 1000527, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9563, N'SB', N'MilitaryOccupation.US.NAVY.SB.0.SpecialWarfareBoatOperator', 1000527, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9564, N'SO', N'MilitaryOccupation.US.NAVY.SO.0.SpecialWarfareOperator', 1000527, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9565, N'1T231', N'MilitaryOccupation.US.AF.1T231.0.PararescueApprentice', 1000531, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9566, N'1T271', N'MilitaryOccupation.US.AF.1T271.0.PararescueCraftsman', 1000531, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9567, N'1T211', N'MilitaryOccupation.US.AF.1T211.0.PararescueHelper', 1000531, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9568, N'1T251', N'MilitaryOccupation.US.AF.1T251.0.PararescueJourneyman', 1000531, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9569, N'1T200', N'MilitaryOccupation.US.AF.1T200.0.PararescueManager', 1000531, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9570, N'1T291', N'MilitaryOccupation.US.AF.1T291.0.PararescueSuperintendent', 1000531, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9571, N'5832', N'MilitaryOccupation.US.MC.5832.0.CorrectionalCounselor', 1000528, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9572, N'9516', N'MilitaryOccupation.US.NAVY.9516.0.CorrectionalCounselor', 1000527, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9573, N'401', N'MilitaryOccupation.US.CG.401.0.Investigator', 1000529, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9574, N'5814', N'MilitaryOccupation.US.MC.5814.0.PhysicalSecuritySpecialist', 1000528, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9575, N'741', N'MilitaryOccupation.US.CG.741.0.PortSecuritySpecialist', 1000529, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9576, N'9556', N'MilitaryOccupation.US.NAVY.9556.0.SecuritySpecialist', 1000527, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9577, N'18F', N'MilitaryOccupation.US.ARMY.18F.0.SpecialForcesAssistantOperationsandIntelligenceSergeant', 1000530, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9578, N'18B', N'MilitaryOccupation.US.ARMY.18B.0.SpecialForcesWeaponsSergeant', 1000530, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9579, N'9190', N'MilitaryOccupation.US.NAVY.9190.0.SpecialSecurityAssistant', 1000527, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9580, N'170', N'MilitaryOccupation.US.CG.170.0.GunnersMate', 1000529, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9581, N'18C', N'MilitaryOccupation.US.ARMY.18C.0.SpecialForcesEngineerSergeant', 1000530, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9582, N'9554', N'MilitaryOccupation.US.NAVY.9554.0.MasterNavalParachutist', 1000527, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9583, N'8023', N'MilitaryOccupation.US.MC.8023.0.Parachutist', 1000528, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9584, N'0323', N'MilitaryOccupation.US.MC.0323.0.ReconnaissanceManParachutistQualified', 1000528, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9585, N'53B9', N'MilitaryOccupation.US.NAVY.53B9.0.SpecialOperationsBoats', 1000527, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9586, N'9557', N'MilitaryOccupation.US.NAVY.9557.0.JointSpecialOperations', 1000527, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9587, N'53S9', N'MilitaryOccupation.US.NAVY.53S9.0.SpecialOperations', 1000527, 0, 3)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9588, N'12Y', N'MilitaryOccupation.US.ARMY.12Y.0.GeospatialEngineer', 1000530, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9589, N'1100', N'MilitaryOccupation.US.MC.1100.0.BasicUtilitiesMarine', 1000528, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9590, N'4123', N'MilitaryOccupation.US.NAVY.4123.0.CG47GasTurbineElectricalMaintenanceTechnician', 1000527, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9591, N'4124', N'MilitaryOccupation.US.NAVY.4124.0.CG47GasTurbineMechanicalMaintenanceTechnician', 1000527, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9592, N'4125', N'MilitaryOccupation.US.NAVY.4125.0.DDG51GasTurbineElectricalMaintenanceTechnician', 1000527, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9593, N'4126', N'MilitaryOccupation.US.NAVY.4126.0.DDG51GasTurbineMechanicalMaintenanceTechnician', 1000527, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9594, N'4146', N'MilitaryOccupation.US.NAVY.4146.0.DDGMGasTurbineMechanicalMaintenanceTechnician', 1000527, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9595, N'270', N'MilitaryOccupation.US.CG.270.0.ElectriciansMate', 1000529, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9596, N'4129', N'MilitaryOccupation.US.NAVY.4129.0.FFG7GasTurbineElectricalMaintenanceTechnician', 1000527, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9597, N'4128', N'MilitaryOccupation.US.NAVY.4128.0.FFG7GasTurbineMechanicalMaintenanceTechnician', 1000527, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9598, N'88T', N'MilitaryOccupation.US.ARMY.88T.0.RailwaySectionRepairerRC', 1000530, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9599, N'150', N'MilitaryOccupation.US.CG.150.0.Seaman', 1000529, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9600, N'SN', N'MilitaryOccupation.US.NAVY.SN.0.Seaman', 1000527, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9601, N'6C031', N'MilitaryOccupation.US.AF.6C031.0.ContractingApprentice', 1000531, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9602, N'6C011', N'MilitaryOccupation.US.AF.6C011.0.ContractingHelper', 1000531, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9603, N'6C051', N'MilitaryOccupation.US.AF.6C051.0.ContractingJourneyman', 1000531, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9604, N'200', N'MilitaryOccupation.US.CG.200.0.MachineryTechnician', 1000529, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9605, N'2R031', N'MilitaryOccupation.US.AF.2R031.0.MaintenanceManagementAnalysisApprentice', 1000531, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9606, N'2R011', N'MilitaryOccupation.US.AF.2R011.0.MaintenanceManagementAnalysisHelper', 1000531, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9607, N'2R051', N'MilitaryOccupation.US.AF.2R051.0.MaintenanceManagementAnalysisJourneyman', 1000531, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9608, N'2R131', N'MilitaryOccupation.US.AF.2R131.0.MaintenanceManagementProductionApprentice', 1000531, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9609, N'2R111', N'MilitaryOccupation.US.AF.2R111.0.MaintenanceManagementProductionHelper', 1000531, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9610, N'2R151', N'MilitaryOccupation.US.AF.2R151.0.MaintenanceManagementProductionJourneyman', 1000531, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9611, N'CU', N'MilitaryOccupation.US.NAVY.CU.0.MasterChiefConstructionman', 1000527, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9612, N'UC', N'MilitaryOccupation.US.NAVY.UC.0.MasterChiefUtilitiesman', 1000527, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9613, N'4952', N'MilitaryOccupation.US.NAVY.4952.0.NAMTSPipefitter', 1000527, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9614, N'12P', N'MilitaryOccupation.US.ARMY.12P.0.PrimePowerProductionSpecialist', 1000530, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9615, N'12G', N'MilitaryOccupation.US.ARMY.12G.0.QuarryingSpecialistRC', 1000530, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9616, N'5707', N'MilitaryOccupation.US.NAVY.5707.0.WaterWellDrillingTechnician', 1000527, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9617, N'EQ', N'MilitaryOccupation.US.NAVY.EQ.0.MasterChiefEquipmentman', 1000527, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9618, N'9583', N'MilitaryOccupation.US.NAVY.9583.0.Locksmith', 1000527, 0, 4)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9619, N'0416', N'MilitaryOccupation.US.NAVY.0416.0.AcousticIntelligenceSpecialist', 1000527, 0, 6)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9620, N'0200', N'MilitaryOccupation.US.MC.0200.0.BasicIntelligenceMarine', 1000528, 0, 6)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9621, N'5326', N'MilitaryOccupation.US.NAVY.5326.0.CombatantSwimmerSEAL', 1000527, 0, 6)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9622, N'9720', N'MilitaryOccupation.US.NAVY.9720.0.CommunicationsandIntelligenceSpecialists', 1000527, 0, 6)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9623, N'00CA', N'MilitaryOccupation.US.NAVY.00CA.0.CryptologicTechnicianAdministrativeNEC', 1000527, 0, 6)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9624, N'3912', N'MilitaryOccupation.US.NAVY.3912.0.ExpeditionaryWarfareIntelligenceSpecialist', 1000527, 0, 6)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9625, N'1N000', N'MilitaryOccupation.US.AF.1N000.0.IntelligenceManager', 1000531, 0, 6)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9626, N'3999', N'MilitaryOccupation.US.NAVY.3999.0.IntelligenceSpecialistBasic', 1000527, 0, 6)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9627, N'0505', N'MilitaryOccupation.US.NAVY.0505.0.IUSSAnalyst', 1000527, 0, 6)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9628, N'0507', N'MilitaryOccupation.US.NAVY.0507.0.IUSSMasterAnalyst', 1000527, 0, 6)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9629, N'0450', N'MilitaryOccupation.US.NAVY.0450.0.JourneymanLevelAcousticAnalyst', 1000527, 0, 6)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9630, N'6049', N'MilitaryOccupation.US.MC.6049.0.NALCOMISApplicationAdministratorAnalyst', 1000528, 0, 6)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9631, N'9102', N'MilitaryOccupation.US.NAVY.9102.0.NationalOPELINTAnalyst', 1000527, 0, 6)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9632, N'3910', N'MilitaryOccupation.US.NAVY.3910.0.NavalImageryInterpreter', 1000527, 0, 6)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9633, N'3924', N'MilitaryOccupation.US.NAVY.3924.0.OperationalIntelligenceOPINTELAnalyst', 1000527, 0, 6)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9634, N'205', N'MilitaryOccupation.US.CG.205.0.OperationsSpecialists', 1000529, 0, 6)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9635, N'0501', N'MilitaryOccupation.US.NAVY.0501.0.SonarSubmarinesLeadingChiefPettyOfficer', 1000527, 0, 6)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9636, N'3923', N'MilitaryOccupation.US.NAVY.3923.0.StrikePlanningApplications', 1000527, 0, 6)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9637, N'275', N'MilitaryOccupation.US.CG.275.0.InformationSystemsTechnician', 1000529, 0, 7)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9638, N'420', N'MilitaryOccupation.US.CG.420.0.Storekeeper', 1000529, 0, 10)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9639, N'3044', N'MilitaryOccupation.US.MC.3044.0.BasicContingencyContractSpecialist', 1000528, 0, 10)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9640, N'6617', N'MilitaryOccupation.US.MC.6617.0.EnlistedAviationLogistician', 1000528, 0, 10)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9641, N'90SP', N'MilitaryOccupation.US.NAVY.90SP.0.IndividualGWOTIAILOSpecialOerationsSupportTeam', 1000527, 0, 10)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9642, N'9780', N'MilitaryOccupation.US.NAVY.9780.0.ServiceandSupplyHandlers', 1000527, 0, 10)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9643, N'4100', N'MilitaryOccupation.US.MC.4100.0.BasicMarineCorpsCommunityServicesMarine', 1000528, 0, 10)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9644, N'3199', N'MilitaryOccupation.US.NAVY.3199.0.ShipServicemanBasic', 1000527, 0, 10)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9645, N'SH', N'MilitaryOccupation.US.NAVY.SH.0.ShipsServiceman', 1000527, 0, 10)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9646, N'9517', N'MilitaryOccupation.US.NAVY.9517.0.Ships3MSystemCoordinator', 1000527, 0, 10)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9647, N'ABH', N'MilitaryOccupation.US.NAVY.ABH.0.AviationBoatswainsMateAircraftHandling', 1000527, 0, 10)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9648, N'90SN', N'MilitaryOccupation.US.NAVY.90SN.0.IndividualGWOTIAILOSpecialOperationsSupportNotDeployed', 1000527, 0, 10)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9649, N'1N490', N'MilitaryOccupation.US.AF.1N490.0.FusionAnalysisSuperintendent', 1000531, 0, 17)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9650, N'00LI', N'MilitaryOccupation.US.NAVY.00LI.0.LithographerTrackingNEC', 1000527, 0, 17)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9651, N'500', N'MilitaryOccupation.US.CG.500.0.FoodServiceSpecialist', 1000529, 0, 17)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9652, N'68R', N'MilitaryOccupation.US.ARMY.68R.0.VeterinaryFoodInspectionSpecialist', 1000530, 0, 17)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9653, N'3300', N'MilitaryOccupation.US.MC.3300.0.BasicFoodServiceMarine', 1000528, 0, 17)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9654, N'42R', N'MilitaryOccupation.US.ARMY.42R.0.ArmyBandperson', 1000530, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9655, N'3852', N'MilitaryOccupation.US.NAVY.3852.0.Arranger', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9656, N'5500', N'MilitaryOccupation.US.MC.5500.0.BasicMusician', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9657, N'3804', N'MilitaryOccupation.US.NAVY.3804.0.BassoonInstrumentalist', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9658, N'3854', N'MilitaryOccupation.US.NAVY.3854.0.CeremonialConductorDrumMajor', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9659, N'3803', N'MilitaryOccupation.US.NAVY.3803.0.ClarinetInstrumentalist', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9660, N'5521', N'MilitaryOccupation.US.MC.5521.0.DrumMajor', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9661, N'3815', N'MilitaryOccupation.US.NAVY.3815.0.ElectricBassStringBassInstrumentalist', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9662, N'3851', N'MilitaryOccupation.US.NAVY.3851.0.EnlistedBandLeader', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9663, N'5519', N'MilitaryOccupation.US.MC.5519.0.EnlistedConductor', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9664, N'3808', N'MilitaryOccupation.US.NAVY.3808.0.EuphoniumInstrumentalist', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9665, N'3801', N'MilitaryOccupation.US.NAVY.3801.0.FlutePiccoloInstrumentalist', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9666, N'3807', N'MilitaryOccupation.US.NAVY.3807.0.FrenchHornInstrumentalist', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9667, N'3812', N'MilitaryOccupation.US.NAVY.3812.0.GuitarInstrumentalist', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9668, N'5523', N'MilitaryOccupation.US.MC.5523.0.InstrumentRepairTechnician', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9669, N'3814', N'MilitaryOccupation.US.NAVY.3814.0.KeyboardInstrumentalist', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9670, N'5512', N'MilitaryOccupation.US.MC.5512.0.MemberTheCommandantsOwnUSMarineDrumandBugleCorps', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9671, N'5511', N'MilitaryOccupation.US.MC.5511.0.MemberThePresidentsOwnUnitedStatesMarineBand', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9672, N'840', N'MilitaryOccupation.US.CG.840.0.Musician', 1000529, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9673, N'5524', N'MilitaryOccupation.US.MC.5524.0.Musician', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9674, N'MU', N'MilitaryOccupation.US.NAVY.MU.0.Musician', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9675, N'3899', N'MilitaryOccupation.US.NAVY.3899.0.MusicianBasic', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9676, N'5528', N'MilitaryOccupation.US.MC.5528.0.MusicianBassoon', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9677, N'5534', N'MilitaryOccupation.US.MC.5534.0.MusicianClarinet', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9678, N'5548', N'MilitaryOccupation.US.MC.5548.0.MusicianElectricBass', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9679, N'5543', N'MilitaryOccupation.US.MC.5543.0.MusicianEuphonium', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9680, N'5536', N'MilitaryOccupation.US.MC.5536.0.MusicianFlutePiccolo', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9681, N'5566', N'MilitaryOccupation.US.MC.5566.0.MusicianGuitar', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9682, N'5544', N'MilitaryOccupation.US.MC.5544.0.MusicianHorn', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9683, N'5526', N'MilitaryOccupation.US.MC.5526.0.MusicianOboe', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9684, N'5563', N'MilitaryOccupation.US.MC.5563.0.MusicianPercussionDrumsTimpaniMallets', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9685, N'5565', N'MilitaryOccupation.US.MC.5565.0.MusicianPiano', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9686, N'5537', N'MilitaryOccupation.US.MC.5537.0.MusicianSaxophone', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9687, N'5546', N'MilitaryOccupation.US.MC.5546.0.MusicianTrombone', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9688, N'5541', N'MilitaryOccupation.US.MC.5541.0.MusicianTrumpet', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9689, N'5547', N'MilitaryOccupation.US.MC.5547.0.MusicianTubaSousaphone', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9690, N'3802', N'MilitaryOccupation.US.NAVY.3802.0.OboeInstrumentalist', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9691, N'3813', N'MilitaryOccupation.US.NAVY.3813.0.PercussionInstrumentalist', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9692, N'3N231', N'MilitaryOccupation.US.AF.3N231.0.PremierBandApprentice', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9693, N'3N271', N'MilitaryOccupation.US.AF.3N271.0.PremierBandCraftsman', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9694, N'3N211', N'MilitaryOccupation.US.AF.3N211.0.PremierBandHelper', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9695, N'3N200', N'MilitaryOccupation.US.AF.3N200.0.PremierBandManager', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9696, N'3N291', N'MilitaryOccupation.US.AF.3N291.0.PremierBandSuperintendent', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9697, N'3N131Q', N'MilitaryOccupation.US.AF.3N131Q.0.RegionalBandApprenticeBagpipe', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9698, N'3N131H', N'MilitaryOccupation.US.AF.3N131H.0.RegionalBandApprenticeBaritoneOrEuphonium', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9699, N'3N131C', N'MilitaryOccupation.US.AF.3N131C.0.RegionalBandApprenticeBassoon', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9700, N'3N131A', N'MilitaryOccupation.US.AF.3N131A.0.RegionalBandApprenticeClarinet', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9701, N'3N131G', N'MilitaryOccupation.US.AF.3N131G.0.RegionalBandApprenticeCornetOrTrumpet', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9702, N'3N131S', N'MilitaryOccupation.US.AF.3N131S.0.RegionalBandApprenticeElectricBassStringBass', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9703, N'3N131E', N'MilitaryOccupation.US.AF.3N131E.0.RegionalBandApprenticeFluteOrPiccolo', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9704, N'3N131F', N'MilitaryOccupation.US.AF.3N131F.0.RegionalBandApprenticeFrenchHorn', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9705, N'3N131N', N'MilitaryOccupation.US.AF.3N131N.0.RegionalBandApprenticeGuitar', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9706, N'3N131Z', N'MilitaryOccupation.US.AF.3N131Z.0.RegionalBandApprenticeInstrumentalistGeneralAirNationalGuardBands', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9707, N'3N131T', N'MilitaryOccupation.US.AF.3N131T.0.RegionalBandApprenticeMilitaryBandSupportUSAFBand', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9708, N'3N131P', N'MilitaryOccupation.US.AF.3N131P.0.RegionalBandApprenticeMusicArranger', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9709, N'3N131D', N'MilitaryOccupation.US.AF.3N131D.0.RegionalBandApprenticeOboe', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9710, N'3N131L', N'MilitaryOccupation.US.AF.3N131L.0.RegionalBandApprenticePercussion', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9711, N'3N131M', N'MilitaryOccupation.US.AF.3N131M.0.RegionalBandApprenticePiano', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9712, N'3N131B', N'MilitaryOccupation.US.AF.3N131B.0.RegionalBandApprenticeSaxophone', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9713, N'3N131U', N'MilitaryOccupation.US.AF.3N131U.0.RegionalBandApprenticeSteelGuitar', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9714, N'3N131J', N'MilitaryOccupation.US.AF.3N131J.0.RegionalBandApprenticeTrombone', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9715, N'3N131K', N'MilitaryOccupation.US.AF.3N131K.0.RegionalBandApprenticeTuba', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9716, N'3N131R', N'MilitaryOccupation.US.AF.3N131R.0.RegionalBandApprenticeVocalist', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9717, N'3N171Q', N'MilitaryOccupation.US.AF.3N171Q.0.RegionalBandCraftsmanBagpipe', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9718, N'3N171H', N'MilitaryOccupation.US.AF.3N171H.0.RegionalBandCraftsmanBaritoneorEuphonium', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9719, N'3N171C', N'MilitaryOccupation.US.AF.3N171C.0.RegionalBandCraftsmanBassoon', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9720, N'3N171A', N'MilitaryOccupation.US.AF.3N171A.0.RegionalBandCraftsmanClarinet', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9721, N'3N171G', N'MilitaryOccupation.US.AF.3N171G.0.RegionalBandCraftsmanCornetorTrumpet', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9722, N'3N171S', N'MilitaryOccupation.US.AF.3N171S.0.RegionalBandCraftsmanElectricBassstringBass', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9723, N'3N171E', N'MilitaryOccupation.US.AF.3N171E.0.RegionalBandCraftsmanFluteorPiccolo', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9724, N'3N171F', N'MilitaryOccupation.US.AF.3N171F.0.RegionalBandCraftsmanFrenchHorn', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9725, N'3N171N', N'MilitaryOccupation.US.AF.3N171N.0.RegionalBandCraftsmanGuitar', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9726, N'3N171Z', N'MilitaryOccupation.US.AF.3N171Z.0.RegionalBandCraftsmanInstrumentalistGeneralAirNationalGuardBands', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9727, N'3N171T', N'MilitaryOccupation.US.AF.3N171T.0.RegionalBandCraftsmanMilitaryBandSupportUSAFBand', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9728, N'3N171P', N'MilitaryOccupation.US.AF.3N171P.0.RegionalBandCraftsmanMusicArranger', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9729, N'3N171D', N'MilitaryOccupation.US.AF.3N171D.0.RegionalBandCraftsmanOboe', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9730, N'3N171L', N'MilitaryOccupation.US.AF.3N171L.0.RegionalBandCraftsmanPercussion', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9731, N'3N171M', N'MilitaryOccupation.US.AF.3N171M.0.RegionalBandCraftsmanPiano', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9732, N'3N171B', N'MilitaryOccupation.US.AF.3N171B.0.RegionalBandCraftsmanSaxophone', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9733, N'3N171U', N'MilitaryOccupation.US.AF.3N171U.0.RegionalBandCraftsmanSteelGuitar', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9734, N'3N171J', N'MilitaryOccupation.US.AF.3N171J.0.RegionalBandCraftsmanTrombone', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9735, N'3N171K', N'MilitaryOccupation.US.AF.3N171K.0.RegionalBandCraftsmanTuba', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9736, N'3N171R', N'MilitaryOccupation.US.AF.3N171R.0.RegionalBandCraftsmanVocalist', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9737, N'3N111Q', N'MilitaryOccupation.US.AF.3N111Q.0.RegionalBandHelperBagpipe', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9738, N'3N111H', N'MilitaryOccupation.US.AF.3N111H.0.RegionalBandHelperBaritoneorEuphonium', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9739, N'3N111C', N'MilitaryOccupation.US.AF.3N111C.0.RegionalBandHelperBassoon', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9740, N'3N111A', N'MilitaryOccupation.US.AF.3N111A.0.RegionalBandHelperClarinet', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9741, N'3N111G', N'MilitaryOccupation.US.AF.3N111G.0.RegionalBandHelperCornetorTrumpet', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9742, N'3N111S', N'MilitaryOccupation.US.AF.3N111S.0.RegionalBandHelperElectricBassStringBass', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9743, N'3N111E', N'MilitaryOccupation.US.AF.3N111E.0.RegionalBandHelperFluteorPiccolo', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9744, N'3N111F', N'MilitaryOccupation.US.AF.3N111F.0.RegionalBandHelperFrenchHorn', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9745, N'3N111N', N'MilitaryOccupation.US.AF.3N111N.0.RegionalBandHelperGuitar', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9746, N'3N111Z', N'MilitaryOccupation.US.AF.3N111Z.0.RegionalBandHelperInstrumentalistGeneralAirNationalGuardBands', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9747, N'3N111T', N'MilitaryOccupation.US.AF.3N111T.0.RegionalBandHelperMilitaryBandSupportUSAFBand', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9748, N'3N111P', N'MilitaryOccupation.US.AF.3N111P.0.RegionalBandHelperMusicArranger', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9749, N'3N111D', N'MilitaryOccupation.US.AF.3N111D.0.RegionalBandHelperOboe', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9750, N'3N111L', N'MilitaryOccupation.US.AF.3N111L.0.RegionalBandHelperPercussion', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9751, N'3N111M', N'MilitaryOccupation.US.AF.3N111M.0.RegionalBandHelperPiano', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9752, N'3N111B', N'MilitaryOccupation.US.AF.3N111B.0.RegionalBandHelperSaxophone', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9753, N'3N111U', N'MilitaryOccupation.US.AF.3N111U.0.RegionalBandHelperSteelGuitar', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9754, N'3N111J', N'MilitaryOccupation.US.AF.3N111J.0.RegionalBandHelperTrombone', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9755, N'3N111K', N'MilitaryOccupation.US.AF.3N111K.0.RegionalBandHelperTuba', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9756, N'3N111R', N'MilitaryOccupation.US.AF.3N111R.0.RegionalBandHelperVocalist', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9757, N'3N151Q', N'MilitaryOccupation.US.AF.3N151Q.0.RegionalBandJourneymanBagpipe', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9758, N'3N151H', N'MilitaryOccupation.US.AF.3N151H.0.RegionalBandJourneymanBaritoneOrEuphonium', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9759, N'3N151C', N'MilitaryOccupation.US.AF.3N151C.0.RegionalBandJourneymanBassoon', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9760, N'3N151A', N'MilitaryOccupation.US.AF.3N151A.0.RegionalBandJourneymanClarinet', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9761, N'3N151G', N'MilitaryOccupation.US.AF.3N151G.0.RegionalBandJourneymanCornetOrTrumpet', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9762, N'3N151S', N'MilitaryOccupation.US.AF.3N151S.0.RegionalBandJourneymanElectricBassStringBass', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9763, N'3N151E', N'MilitaryOccupation.US.AF.3N151E.0.RegionalBandJourneymanFluteOrPiccolo', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9764, N'3N151F', N'MilitaryOccupation.US.AF.3N151F.0.RegionalBandJourneymanFrenchHorn', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9765, N'3N151N', N'MilitaryOccupation.US.AF.3N151N.0.RegionalBandJourneymanGuitar', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9766, N'3N151Z', N'MilitaryOccupation.US.AF.3N151Z.0.RegionalBandJourneymanInstrumentalistGeneralAirNationalGuardBands', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9767, N'3N151T', N'MilitaryOccupation.US.AF.3N151T.0.RegionalBandJourneymanMilitaryBandSupportUSAFBand', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9768, N'3N151P', N'MilitaryOccupation.US.AF.3N151P.0.RegionalBandJourneymanMusicArranger', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9769, N'3N151D', N'MilitaryOccupation.US.AF.3N151D.0.RegionalBandJourneymanOboe', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9770, N'3N151L', N'MilitaryOccupation.US.AF.3N151L.0.RegionalBandJourneymanPercussion', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9771, N'3N151M', N'MilitaryOccupation.US.AF.3N151M.0.RegionalBandJourneymanPiano', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9772, N'3N151B', N'MilitaryOccupation.US.AF.3N151B.0.RegionalBandJourneymanSaxophone', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9773, N'3N151U', N'MilitaryOccupation.US.AF.3N151U.0.RegionalBandJourneymanSteelGuitar', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9774, N'3N151J', N'MilitaryOccupation.US.AF.3N151J.0.RegionalBandJourneymanTrombone', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9775, N'3N151K', N'MilitaryOccupation.US.AF.3N151K.0.RegionalBandJourneymanTuba', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9776, N'3N151R', N'MilitaryOccupation.US.AF.3N151R.0.RegionalBandJourneymanVocalist', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9777, N'3N100', N'MilitaryOccupation.US.AF.3N100.0.RegionalBandManager', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9778, N'3N191', N'MilitaryOccupation.US.AF.3N191.0.RegionalBandSuperintendent', 1000531, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9779, N'3805', N'MilitaryOccupation.US.NAVY.3805.0.SaxophoneInstrumentalist', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9780, N'5522', N'MilitaryOccupation.US.MC.5522.0.SmallEnsembleLeader', 1000528, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9781, N'42S', N'MilitaryOccupation.US.ARMY.42S.0.SpecialBandMember', 1000530, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9782, N'3809', N'MilitaryOccupation.US.NAVY.3809.0.TromboneInstrumentalist', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9783, N'3806', N'MilitaryOccupation.US.NAVY.3806.0.TrumpetInstrumentalist', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9784, N'3811', N'MilitaryOccupation.US.NAVY.3811.0.TubaInstrumentalist', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9785, N'3853', N'MilitaryOccupation.US.NAVY.3853.0.UnitLeader', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9786, N'3825', N'MilitaryOccupation.US.NAVY.3825.0.VocalistEntertainer', 1000527, 0, 22)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9787, N'4131', N'MilitaryOccupation.US.NAVY.4131.0.LCACCraftEngineerAssistantOperator', 1000527, 0, 11)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9788, N'88U', N'MilitaryOccupation.US.ARMY.88U.0.RailwayOperationsCrewmemberRC', 1000530, 0, 11)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9789, N'100', N'MilitaryOccupation.US.CG.100.0.BoatswainsMate', 1000529, 0, 11)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9790, N'0316', N'MilitaryOccupation.US.MC.0316.0.CombatRubberReconnaissanceCraftCRRCCoxswain', 1000528, 0, 11)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9791, N'0314', N'MilitaryOccupation.US.MC.0314.0.RigidRaidingCraftRRCRigidHullInflatableBoatRHIBCoxswain', 1000528, 0, 11)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9792, N'8P000', N'MilitaryOccupation.US.AF.8P000.0.Courier', 1000531, 0, 23)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9793, N'3001', N'MilitaryOccupation.US.NAVY.3001.0.IndependentDutyPostalClerk', 1000527, 0, 23)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9794, N'8M000', N'MilitaryOccupation.US.AF.8M000.0.Postal', 1000531, 0, 23)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9795, N'00PC', N'MilitaryOccupation.US.NAVY.00PC.0.PostalClerkPCTrackingNEC', 1000527, 0, 23)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9796, N'25M', N'MilitaryOccupation.US.ARMY.25M.0.MultimediaIllustrator', 1000530, 0, 12)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9797, N'340', N'MilitaryOccupation.US.CG.340.0.PublicAffairsSpecialist', 1000529, 0, 12)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9798, N'890', N'MilitaryOccupation.US.CG.890.0.HealthServicesDental', 1000529, 0, 13)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9799, N'5392', N'MilitaryOccupation.US.NAVY.5392.0.NavalSpecialWarfareMedic', 1000527, 0, 13)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9800, N'18D', N'MilitaryOccupation.US.ARMY.18D.0.SpecialForcesMedicalSergeant', 1000530, 0, 13)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9801, N'8454', N'MilitaryOccupation.US.NAVY.8454.0.ElectroneurodiagnosticTechnologist', 1000527, 0, 13)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9802, N'870', N'MilitaryOccupation.US.CG.870.0.HealthServicesTechnician', 1000529, 0, 13)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9803, N'790', N'MilitaryOccupation.US.CG.790.0.MarineScienceTechnician', 1000529, 0, 13)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9804, N'4A231', N'MilitaryOccupation.US.AF.4A231.0.BiomedicalEquipmentApprentice', 1000531, 0, 14)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9805, N'4A271', N'MilitaryOccupation.US.AF.4A271.0.BiomedicalEquipmentCraftsman', 1000531, 0, 14)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9806, N'4A211', N'MilitaryOccupation.US.AF.4A211.0.BiomedicalEquipmentHelper', 1000531, 0, 14)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9807, N'4A251', N'MilitaryOccupation.US.AF.4A251.0.BiomedicalEquipmentJourneyman', 1000531, 0, 14)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9808, N'4A200', N'MilitaryOccupation.US.AF.4A200.0.BiomedicalEquipmentManager', 1000531, 0, 14)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9809, N'4A291', N'MilitaryOccupation.US.AF.4A291.0.BiomedicalEquipmentSuperintendent', 1000531, 0, 14)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9810, N'FN', N'MilitaryOccupation.US.NAVY.FN.0.Fireman', 1000527, 0, 14)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9811, N'210', N'MilitaryOccupation.US.CG.210.0.DamageControlman', 1000529, 0, 14)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9812, N'3E032', N'MilitaryOccupation.US.AF.3E032.0.ElectricalPowerProductionApprentice', 1000531, 0, 14)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9813, N'3E072', N'MilitaryOccupation.US.AF.3E072.0.ElectricalPowerProductionCraftsman', 1000531, 0, 14)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9814, N'3E012', N'MilitaryOccupation.US.AF.3E012.0.ElectricalPowerProductionHelper', 1000531, 0, 14)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9815, N'3E052', N'MilitaryOccupation.US.AF.3E052.0.ElectricalPowerProductionJourneyman', 1000531, 0, 14)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9816, N'9760', N'MilitaryOccupation.US.NAVY.9760.0.ElectricalMechanicalEquipmentRepairman', 1000527, 0, 14)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9817, N'4508', N'MilitaryOccupation.US.NAVY.4508.0.ElectronicAutomaticBoilerControlsConsoleOperator', 1000527, 0, 14)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9818, N'4382', N'MilitaryOccupation.US.NAVY.4382.0.FFG7ClassAuxiliariesMechanicalSystemTechnician', 1000527, 0, 14)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9819, N'320', N'MilitaryOccupation.US.CG.320.0.Fireman', 1000529, 0, 14)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9820, N'9581', N'MilitaryOccupation.US.NAVY.9581.0.Rubber&PlasticsWorker', 1000527, 0, 14)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9821, N'18E', N'MilitaryOccupation.US.ARMY.18E.0.SpecialForcesCommunicationsSergeant', 1000530, 0, 15)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9822, N'9710', N'MilitaryOccupation.US.NAVY.9710.0.ElectronicEquipmentRepairman', 1000527, 0, 15)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9823, N'240', N'MilitaryOccupation.US.CG.240.0.ElectronicsTechnician', 1000529, 0, 15)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9824, N'9535', N'MilitaryOccupation.US.NAVY.9535.0.AdvanceSealDeliverySystemMaintainer', 1000527, 0, 16)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9825, N'4398', N'MilitaryOccupation.US.NAVY.4398.0.AuxiliarySystemsTechnician', 1000527, 0, 16)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9826, N'88P', N'MilitaryOccupation.US.ARMY.88P.0.RailwayEquipmentRepairerRC', 1000530, 0, 16)
INSERT INTO [dbo].[Library.MilitaryOccupation] ([Id], [Code], [Key], [BranchOfServiceId], [IsCommissioned], [MilitaryOccupationGroupId]) VALUES (9827, N'9534', N'MilitaryOccupation.US.NAVY.9534.0.SealDeliveryVehicleSDVTeamTechnician', 1000527, 0, 16)
SET IDENTITY_INSERT [dbo].[Library.MilitaryOccupation] OFF
-- Operation applied to 288 rows out of 288

-- Add rows to [dbo].[Library.MilitaryOccupationGroupROnet]
SET IDENTITY_INSERT [dbo].[Library.MilitaryOccupationGroupROnet] ON
INSERT INTO [dbo].[Library.MilitaryOccupationGroupROnet] ([Id], [ROnetId], [MilitaryOccupationGroupId]) VALUES (319, 539, 23)
INSERT INTO [dbo].[Library.MilitaryOccupationGroupROnet] ([Id], [ROnetId], [MilitaryOccupationGroupId]) VALUES (320, 313, 22)
INSERT INTO [dbo].[Library.MilitaryOccupationGroupROnet] ([Id], [ROnetId], [MilitaryOccupationGroupId]) VALUES (321, 536, 23)
INSERT INTO [dbo].[Library.MilitaryOccupationGroupROnet] ([Id], [ROnetId], [MilitaryOccupationGroupId]) VALUES (322, 298, 22)
INSERT INTO [dbo].[Library.MilitaryOccupationGroupROnet] ([Id], [ROnetId], [MilitaryOccupationGroupId]) VALUES (323, 551, 23)
INSERT INTO [dbo].[Library.MilitaryOccupationGroupROnet] ([Id], [ROnetId], [MilitaryOccupationGroupId]) VALUES (324, 538, 23)
INSERT INTO [dbo].[Library.MilitaryOccupationGroupROnet] ([Id], [ROnetId], [MilitaryOccupationGroupId]) VALUES (325, 81, 22)
INSERT INTO [dbo].[Library.MilitaryOccupationGroupROnet] ([Id], [ROnetId], [MilitaryOccupationGroupId]) VALUES (326, 299, 22)
INSERT INTO [dbo].[Library.MilitaryOccupationGroupROnet] ([Id], [ROnetId], [MilitaryOccupationGroupId]) VALUES (327, 303, 22)
INSERT INTO [dbo].[Library.MilitaryOccupationGroupROnet] ([Id], [ROnetId], [MilitaryOccupationGroupId]) VALUES (328, 311, 22)
SET IDENTITY_INSERT [dbo].[Library.MilitaryOccupationGroupROnet] OFF
-- Operation applied to 10 rows out of 10

-- Add rows to [dbo].[Library.MilitaryOccupationROnet]
SET IDENTITY_INSERT [dbo].[Library.MilitaryOccupationROnet] ON
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113295, 530, 9793)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113296, 532, 9793)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113297, 540, 9793)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113298, 551, 9793)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113299, 13, 9639)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113300, 68, 9639)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113301, 530, 9639)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113302, 69, 9639)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113303, 75, 9639)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113304, 78, 9639)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113305, 26, 9639)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113306, 716, 9639)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113307, 63, 9639)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113308, 511, 9639)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113309, 401, 9644)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113310, 68, 9644)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113311, 469, 9644)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113312, 419, 9644)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113313, 287, 9644)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113314, 465, 9644)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113315, 286, 9644)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113316, 470, 9644)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113317, 471, 9644)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113318, 466, 9644)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113319, 694, 9644)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113320, 492, 9644)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113321, 467, 9644)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113322, 539, 9644)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113323, 1081, 9644)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113324, 8, 9644)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113325, 420, 9653)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113326, 422, 9653)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113327, 42, 9653)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113328, 421, 9653)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113329, 636, 2889)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113330, 420, 2889)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113331, 422, 2889)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113332, 296, 9665)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113333, 295, 9665)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113334, 297, 9665)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113335, 296, 9690)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113336, 295, 9690)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113337, 297, 9690)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113338, 296, 9659)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113339, 295, 9659)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113340, 297, 9659)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113341, 296, 9657)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113342, 295, 9657)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113343, 297, 9657)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113344, 296, 9779)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113345, 295, 9779)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113346, 297, 9779)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113347, 296, 9783)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113348, 295, 9783)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113349, 297, 9783)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113350, 296, 9666)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113351, 295, 9666)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113352, 297, 9666)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113353, 296, 9664)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113354, 295, 9664)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113355, 297, 9664)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113356, 296, 9782)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113357, 295, 9782)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113358, 297, 9782)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113359, 296, 9784)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113360, 295, 9784)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113361, 297, 9784)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113362, 296, 9667)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113363, 295, 9667)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113364, 297, 9667)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113365, 296, 9691)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113366, 295, 9691)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113367, 297, 9691)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113368, 296, 9669)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113369, 295, 9669)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113370, 297, 9669)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113371, 296, 9661)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113372, 295, 9661)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113373, 297, 9661)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113374, 296, 9786)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113375, 295, 9786)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113376, 297, 9786)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113377, 296, 9662)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113378, 295, 9662)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113379, 297, 9662)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113380, 296, 9655)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113381, 295, 9655)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113382, 297, 9655)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113383, 296, 9785)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113384, 295, 9785)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113385, 297, 9785)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113386, 296, 9658)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113387, 295, 9658)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113388, 297, 9658)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113389, 296, 9675)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113390, 295, 9675)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113391, 297, 9675)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113392, 231, 9632)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113393, 137, 9632)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113394, 414, 9632)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113395, 415, 9632)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113396, 194, 9632)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113397, 401, 9624)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113398, 406, 9624)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113399, 231, 9624)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113400, 126, 9624)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113401, 115, 9624)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113402, 407, 9624)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113403, 419, 9624)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113404, 411, 9624)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113405, 244, 9624)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113406, 408, 9624)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113407, 416, 9624)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113408, 414, 9624)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113409, 415, 9624)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113410, 418, 9624)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113411, 401, 9636)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113412, 406, 9636)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113413, 231, 9636)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113414, 407, 9636)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113415, 419, 9636)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113416, 411, 9636)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113417, 244, 9636)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113418, 408, 9636)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113419, 416, 9636)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113420, 414, 9636)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113421, 415, 9636)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113422, 418, 9636)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113423, 401, 9633)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113424, 406, 9633)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113425, 231, 9633)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113426, 126, 9633)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113427, 115, 9633)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113428, 407, 9633)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113429, 419, 9633)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113430, 411, 9633)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113431, 244, 9633)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113432, 408, 9633)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113433, 416, 9633)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113434, 414, 9633)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113435, 415, 9633)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113436, 418, 9633)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113437, 401, 9626)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113438, 406, 9626)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113439, 231, 9626)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113440, 126, 9626)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113441, 115, 9626)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113442, 407, 9626)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113443, 419, 9626)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113444, 411, 9626)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113445, 244, 9626)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113446, 408, 9626)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113447, 416, 9626)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113448, 414, 9626)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113449, 415, 9626)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113450, 418, 9626)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113451, 419, 9643)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113452, 465, 9643)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113453, 286, 9643)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113454, 42, 9643)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113455, 421, 9643)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113456, 466, 9643)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113457, 467, 9643)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113458, 537, 9643)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113459, 187, 9590)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113460, 12, 9590)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113461, 597, 9590)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113462, 629, 9590)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113463, 625, 9590)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113464, 662, 9590)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113465, 1002, 9590)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113466, 607, 9591)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113467, 610, 9591)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113468, 191, 9591)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113469, 629, 9591)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113470, 625, 9591)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113471, 188, 9591)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113472, 662, 9591)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113473, 1002, 9591)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113474, 187, 9592)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113475, 603, 9592)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113476, 602, 9592)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113477, 1082, 9592)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113478, 597, 9592)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113479, 629, 9592)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113480, 625, 9592)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113481, 1002, 9592)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113482, 664, 9592)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113483, 563, 9593)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113484, 191, 9593)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113485, 1082, 9593)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113486, 629, 9593)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113487, 625, 9593)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113488, 188, 9593)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113489, 1002, 9593)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113490, 664, 9593)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113491, 665, 9593)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113492, 607, 9597)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113493, 610, 9597)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113494, 191, 9597)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113495, 629, 9597)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113496, 625, 9597)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113497, 188, 9597)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113498, 662, 9597)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113499, 1002, 9597)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113500, 187, 9596)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113501, 12, 9596)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113502, 597, 9596)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113503, 629, 9596)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113504, 625, 9596)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113505, 662, 9596)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113506, 1002, 9596)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113507, 563, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113508, 585, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113509, 611, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113510, 689, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113511, 603, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113512, 602, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113513, 584, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113514, 709, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113515, 612, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113516, 620, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113517, 582, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113518, 621, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113519, 571, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113520, 577, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113521, 701, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113522, 581, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113523, 702, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113524, 1081, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113525, 705, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113526, 418, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113527, 688, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113528, 686, 9787)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113529, 563, 9594)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113530, 191, 9594)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113531, 1082, 9594)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113532, 629, 9594)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113533, 625, 9594)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113534, 188, 9594)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113535, 1002, 9594)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113536, 664, 9594)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113537, 665, 9594)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113538, 607, 9818)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113539, 610, 9818)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113540, 191, 9818)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113541, 629, 9818)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113542, 625, 9818)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113543, 188, 9818)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113544, 662, 9818)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113545, 1002, 9818)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113546, 611, 9825)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113547, 1082, 9825)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113548, 612, 9825)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113549, 617, 9825)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113550, 620, 9825)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113551, 193, 9825)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113552, 621, 9825)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113553, 630, 9825)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113554, 613, 9825)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113555, 664, 9825)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113556, 563, 9817)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113557, 662, 9817)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113558, 633, 9817)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113559, 1001, 9817)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113560, 1002, 9817)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113561, 632, 9817)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113562, 563, 9613)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113563, 577, 9613)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113564, 578, 9613)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113565, 648, 9613)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113566, 635, 9562)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113567, 401, 9562)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113568, 585, 9562)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113569, 405, 9562)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113570, 409, 9562)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113571, 419, 9562)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113572, 410, 9562)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113573, 411, 9562)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113574, 701, 9562)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113575, 416, 9562)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113576, 412, 9562)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113577, 702, 9562)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113578, 414, 9562)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113579, 415, 9562)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113580, 418, 9562)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113581, 409, 9621)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113582, 137, 9621)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113583, 417, 9621)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113584, 411, 9621)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113585, 595, 9621)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113586, 416, 9621)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113587, 412, 9621)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113588, 517, 9799)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113589, 13, 9799)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113590, 500, 9799)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113591, 353, 9799)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113592, 367, 9799)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113593, 390, 9799)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113594, 363, 9799)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113595, 501, 9799)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113596, 366, 9799)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113597, 365, 9799)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113598, 496, 9799)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113599, 391, 9799)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113600, 84, 9799)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113601, 258, 9799)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113602, 296, 9656)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113603, 295, 9656)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113604, 297, 9656)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113605, 296, 9671)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113606, 295, 9671)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113607, 297, 9671)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113608, 296, 9670)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113609, 295, 9670)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113610, 297, 9670)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113611, 296, 9663)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113612, 295, 9663)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113613, 297, 9663)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113614, 296, 9660)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113615, 295, 9660)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113616, 297, 9660)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113617, 13, 9606)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113618, 500, 9606)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113619, 130, 9606)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113620, 126, 9606)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113621, 689, 9606)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113622, 541, 9606)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113623, 531, 9606)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113624, 127, 9606)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113625, 131, 9606)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113626, 544, 9606)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113627, 535, 9606)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113628, 536, 9606)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113629, 705, 9606)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113630, 688, 9606)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113631, 686, 9606)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113632, 13, 9605)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113633, 500, 9605)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113634, 530, 9605)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113635, 130, 9605)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113636, 126, 9605)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113637, 689, 9605)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113638, 541, 9605)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113639, 531, 9605)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113640, 127, 9605)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113641, 131, 9605)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113642, 544, 9605)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113643, 536, 9605)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113644, 705, 9605)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113645, 688, 9605)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113646, 686, 9605)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113647, 13, 9607)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113648, 500, 9607)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113649, 130, 9607)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113650, 126, 9607)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113651, 689, 9607)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113652, 541, 9607)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113653, 531, 9607)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113654, 597, 9607)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113655, 127, 9607)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113656, 131, 9607)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113657, 544, 9607)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113658, 536, 9607)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113659, 705, 9607)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113660, 688, 9607)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113661, 686, 9607)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113662, 13, 9609)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113663, 500, 9609)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113664, 530, 9609)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113665, 533, 9609)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113666, 689, 9609)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113667, 541, 9609)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113668, 531, 9609)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113669, 544, 9609)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113670, 535, 9609)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113671, 536, 9609)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113672, 539, 9609)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113673, 705, 9609)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113674, 688, 9609)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113675, 537, 9609)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113676, 686, 9609)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113677, 13, 9608)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113678, 500, 9608)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113679, 530, 9608)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113680, 533, 9608)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113681, 689, 9608)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113682, 541, 9608)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113683, 531, 9608)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113684, 544, 9608)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113685, 535, 9608)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113686, 536, 9608)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113687, 539, 9608)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113688, 705, 9608)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113689, 688, 9608)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113690, 537, 9608)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113691, 686, 9608)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113692, 13, 9610)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113693, 500, 9610)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113694, 530, 9610)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113695, 533, 9610)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113696, 689, 9610)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113697, 541, 9610)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113698, 531, 9610)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113699, 597, 9610)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113700, 544, 9610)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113701, 535, 9610)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113702, 536, 9610)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113703, 705, 9610)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113704, 688, 9610)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113705, 537, 9610)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113706, 686, 9610)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113707, 296, 9780)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113708, 295, 9780)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113709, 297, 9780)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113710, 296, 9668)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113711, 295, 9668)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113712, 297, 9668)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113713, 296, 9673)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113714, 295, 9673)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113715, 297, 9673)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113716, 296, 9683)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113717, 295, 9683)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113718, 297, 9683)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113719, 296, 9676)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113720, 295, 9676)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113721, 297, 9676)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113722, 296, 9677)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113723, 295, 9677)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113724, 297, 9677)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113725, 296, 9680)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113726, 295, 9680)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113727, 297, 9680)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113728, 296, 9686)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113729, 295, 9686)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113730, 297, 9686)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113731, 296, 9688)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113732, 295, 9688)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113733, 297, 9688)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113734, 296, 9679)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113735, 295, 9679)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113736, 297, 9679)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113737, 296, 9682)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113738, 295, 9682)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113739, 297, 9682)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113740, 296, 9687)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113741, 295, 9687)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113742, 297, 9687)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113743, 296, 9689)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113744, 295, 9689)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113745, 297, 9689)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113746, 296, 9678)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113747, 295, 9678)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113748, 297, 9678)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113749, 296, 9684)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113750, 295, 9684)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113751, 297, 9684)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113752, 296, 9685)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113753, 295, 9685)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113754, 297, 9685)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113755, 296, 9681)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113756, 295, 9681)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113757, 297, 9681)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113758, 593, 9616)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113759, 137, 9616)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113760, 720, 9616)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113761, 596, 9616)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113762, 591, 9616)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113763, 401, 9574)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113764, 404, 9574)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113765, 405, 9574)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113766, 409, 9574)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113767, 2, 9574)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113768, 410, 9574)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113769, 411, 9574)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113770, 416, 9574)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113771, 412, 9574)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113772, 414, 9574)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113773, 415, 9574)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113774, 418, 9574)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113775, 405, 9571)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113776, 244, 9571)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113777, 607, 9557)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113778, 635, 9557)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113779, 608, 9557)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113780, 604, 9557)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113781, 706, 9557)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113782, 601, 9557)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113783, 187, 9557)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113784, 602, 9557)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113785, 191, 9557)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113786, 630, 9557)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113787, 13, 9553)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113788, 500, 9553)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113789, 607, 9553)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113790, 601, 9553)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113791, 187, 9553)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113792, 597, 9553)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113793, 188, 9553)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113794, 536, 9553)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113795, 607, 9555)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113796, 706, 9555)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113797, 20, 9555)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113798, 632, 9555)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113799, 685, 9555)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113800, 233, 9555)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113801, 675, 9555)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113802, 21, 9555)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113803, 505, 9540)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113804, 13, 9540)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113805, 500, 9540)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113806, 545, 9540)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113807, 130, 9540)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113808, 546, 9540)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113809, 126, 9540)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113810, 541, 9540)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113811, 519, 9540)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113812, 127, 9540)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113813, 131, 9540)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113814, 496, 9540)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113815, 547, 9540)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113816, 553, 9540)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113817, 554, 9540)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113818, 528, 9540)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113819, 118, 9630)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113820, 138, 9630)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113821, 139, 9630)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113822, 126, 9630)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113823, 78, 9630)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113824, 26, 9630)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113825, 127, 9630)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113826, 131, 9630)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113827, 63, 9630)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113828, 79, 9630)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113829, 505, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113830, 13, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113831, 500, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113832, 530, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113833, 533, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113834, 518, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113835, 541, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113836, 519, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113837, 527, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113838, 514, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113839, 544, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113840, 496, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113841, 551, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113842, 554, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113843, 528, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113844, 535, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113845, 536, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113846, 246, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113847, 537, 9542)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113848, 538, 9558)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113849, 607, 9558)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113850, 635, 9558)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113851, 601, 9558)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113852, 530, 9558)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113853, 689, 9558)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113854, 709, 9558)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113855, 531, 9558)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113856, 540, 9558)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113857, 1083, 9558)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113858, 630, 9558)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113859, 536, 9558)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113860, 539, 9558)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113861, 705, 9558)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113862, 418, 9558)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113863, 688, 9558)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113864, 537, 9558)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113865, 686, 9558)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113866, 530, 9640)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113867, 689, 9640)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113868, 531, 9640)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113869, 78, 9640)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113870, 26, 9640)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113871, 1081, 9640)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113872, 705, 9640)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113873, 25, 9640)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113874, 418, 9640)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113875, 688, 9640)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113876, 686, 9640)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113877, 401, 9583)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113878, 530, 9583)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113879, 405, 9583)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113880, 689, 9583)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113881, 353, 9583)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113882, 531, 9583)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113883, 419, 9583)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113884, 411, 9583)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113885, 416, 9583)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113886, 412, 9583)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113887, 414, 9583)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113888, 415, 9583)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113889, 418, 9583)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113890, 296, 9777)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113891, 295, 9777)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113892, 297, 9777)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113893, 296, 9740)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113894, 295, 9740)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113895, 297, 9740)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113896, 296, 9752)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113897, 295, 9752)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113898, 297, 9752)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113899, 296, 9739)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113900, 295, 9739)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113901, 297, 9739)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113902, 296, 9749)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113903, 295, 9749)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113904, 297, 9749)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113905, 296, 9743)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113906, 295, 9743)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113907, 297, 9743)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113908, 296, 9744)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113909, 295, 9744)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113910, 297, 9744)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113911, 296, 9741)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113912, 295, 9741)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113913, 297, 9741)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113914, 296, 9738)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113915, 295, 9738)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113916, 297, 9738)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113917, 296, 9754)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113918, 295, 9754)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113919, 297, 9754)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113920, 296, 9755)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113921, 295, 9755)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113922, 297, 9755)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113923, 296, 9750)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113924, 295, 9750)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113925, 297, 9750)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113926, 296, 9751)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113927, 295, 9751)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113928, 297, 9751)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113929, 296, 9745)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113930, 295, 9745)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113931, 297, 9745)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113932, 296, 9748)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113933, 295, 9748)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113934, 297, 9748)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113935, 296, 9737)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113936, 295, 9737)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113937, 297, 9737)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113938, 296, 9756)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113939, 295, 9756)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113940, 297, 9756)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113941, 296, 9742)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113942, 295, 9742)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113943, 297, 9742)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113944, 296, 9747)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113945, 295, 9747)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113946, 297, 9747)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113947, 296, 9753)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113948, 295, 9753)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113949, 297, 9753)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113950, 296, 9746)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113951, 295, 9746)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113952, 297, 9746)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113953, 296, 9700)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113954, 295, 9700)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113955, 297, 9700)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113956, 296, 9712)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113957, 295, 9712)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113958, 297, 9712)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113959, 296, 9699)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113960, 295, 9699)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113961, 297, 9699)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113962, 296, 9709)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113963, 295, 9709)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113964, 297, 9709)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113965, 296, 9703)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113966, 295, 9703)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113967, 297, 9703)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113968, 296, 9704)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113969, 295, 9704)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113970, 297, 9704)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113971, 296, 9701)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113972, 295, 9701)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113973, 297, 9701)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113974, 296, 9698)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113975, 295, 9698)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113976, 297, 9698)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113977, 296, 9714)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113978, 295, 9714)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113979, 297, 9714)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113980, 296, 9715)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113981, 295, 9715)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113982, 297, 9715)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113983, 296, 9710)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113984, 295, 9710)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113985, 297, 9710)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113986, 296, 9711)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113987, 295, 9711)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113988, 297, 9711)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113989, 296, 9705)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113990, 295, 9705)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113991, 297, 9705)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113992, 296, 9708)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113993, 295, 9708)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113994, 297, 9708)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113995, 296, 9697)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113996, 295, 9697)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113997, 297, 9697)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113998, 296, 9716)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (113999, 295, 9716)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114000, 297, 9716)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114001, 296, 9702)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114002, 295, 9702)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114003, 297, 9702)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114004, 296, 9707)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114005, 295, 9707)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114006, 297, 9707)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114007, 296, 9713)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114008, 295, 9713)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114009, 297, 9713)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114010, 296, 9706)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114011, 295, 9706)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114012, 297, 9706)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114013, 296, 9760)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114014, 295, 9760)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114015, 297, 9760)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114016, 296, 9772)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114017, 295, 9772)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114018, 297, 9772)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114019, 296, 9759)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114020, 295, 9759)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114021, 297, 9759)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114022, 296, 9769)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114023, 295, 9769)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114024, 297, 9769)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114025, 296, 9763)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114026, 295, 9763)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114027, 297, 9763)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114028, 296, 9764)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114029, 295, 9764)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114030, 297, 9764)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114031, 296, 9761)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114032, 295, 9761)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114033, 297, 9761)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114034, 296, 9758)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114035, 295, 9758)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114036, 297, 9758)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114037, 296, 9774)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114038, 295, 9774)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114039, 297, 9774)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114040, 296, 9775)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114041, 295, 9775)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114042, 297, 9775)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114043, 296, 9770)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114044, 295, 9770)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114045, 297, 9770)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114046, 296, 9771)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114047, 295, 9771)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114048, 297, 9771)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114049, 296, 9765)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114050, 295, 9765)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114051, 297, 9765)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114052, 296, 9768)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114053, 295, 9768)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114054, 297, 9768)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114055, 296, 9757)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114056, 295, 9757)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114057, 297, 9757)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114058, 296, 9776)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114059, 295, 9776)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114060, 297, 9776)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114061, 296, 9762)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114062, 295, 9762)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114063, 297, 9762)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114064, 296, 9767)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114065, 295, 9767)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114066, 297, 9767)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114067, 296, 9773)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114068, 295, 9773)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114069, 297, 9773)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114070, 296, 9766)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114071, 295, 9766)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114072, 297, 9766)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114073, 296, 9720)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114074, 295, 9720)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114075, 297, 9720)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114076, 296, 9732)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114077, 295, 9732)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114078, 297, 9732)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114079, 296, 9719)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114080, 295, 9719)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114081, 297, 9719)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114082, 296, 9729)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114083, 295, 9729)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114084, 297, 9729)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114085, 296, 9723)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114086, 295, 9723)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114087, 297, 9723)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114088, 296, 9724)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114089, 295, 9724)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114090, 297, 9724)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114091, 296, 9721)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114092, 295, 9721)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114093, 297, 9721)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114094, 296, 9718)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114095, 295, 9718)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114096, 297, 9718)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114097, 296, 9734)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114098, 295, 9734)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114099, 297, 9734)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114100, 296, 9735)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114101, 295, 9735)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114102, 297, 9735)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114103, 296, 9730)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114104, 295, 9730)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114105, 297, 9730)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114106, 296, 9731)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114107, 295, 9731)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114108, 297, 9731)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114109, 296, 9725)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114110, 295, 9725)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114111, 297, 9725)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114112, 296, 9728)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114113, 295, 9728)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114114, 297, 9728)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114115, 296, 9717)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114116, 295, 9717)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114117, 297, 9717)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114118, 296, 9736)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114119, 295, 9736)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114120, 297, 9736)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114121, 296, 9722)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114122, 295, 9722)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114123, 297, 9722)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114124, 296, 9727)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114125, 295, 9727)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114126, 297, 9727)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114127, 296, 9733)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114128, 295, 9733)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114129, 297, 9733)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114130, 296, 9726)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114131, 295, 9726)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114132, 297, 9726)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114133, 296, 9778)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114134, 295, 9778)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114135, 297, 9778)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114136, 296, 9695)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114137, 295, 9695)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114138, 297, 9695)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114139, 296, 9694)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114140, 295, 9694)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114141, 297, 9694)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114142, 296, 9692)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114143, 295, 9692)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114144, 297, 9692)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114145, 296, 9693)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114146, 295, 9693)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114147, 297, 9693)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114148, 296, 9696)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114149, 295, 9696)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114150, 297, 9696)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114151, 296, 9654)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114152, 295, 9654)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114153, 297, 9654)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114154, 296, 9781)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114155, 295, 9781)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114156, 297, 9781)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114157, 390, 9801)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114158, 345, 9801)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114159, 375, 9801)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114160, 624, 9808)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114161, 12, 9808)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114162, 432, 9808)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114163, 597, 9808)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114164, 625, 9808)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114165, 630, 9808)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114166, 624, 9806)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114167, 12, 9806)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114168, 432, 9806)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114169, 597, 9806)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114170, 629, 9806)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114171, 625, 9806)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114172, 630, 9806)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114173, 624, 9804)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114174, 12, 9804)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114175, 432, 9804)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114176, 597, 9804)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114177, 625, 9804)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114178, 630, 9804)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114179, 624, 9807)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114180, 12, 9807)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114181, 432, 9807)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114182, 597, 9807)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114183, 625, 9807)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114184, 630, 9807)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114185, 624, 9805)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114186, 12, 9805)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114187, 432, 9805)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114188, 597, 9805)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114189, 625, 9805)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114190, 630, 9805)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114191, 624, 9809)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114192, 12, 9809)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114193, 432, 9809)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114194, 597, 9809)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114195, 625, 9809)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114196, 630, 9809)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114197, 12, 9549)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114198, 432, 9549)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114199, 597, 9549)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114200, 57, 9549)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114201, 463, 9549)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114202, 401, 9631)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114203, 406, 9631)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114204, 231, 9631)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114205, 115, 9631)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114206, 137, 9631)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114207, 407, 9631)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114208, 419, 9631)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114209, 411, 9631)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114210, 408, 9631)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114211, 414, 9631)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114212, 415, 9631)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114213, 552, 9579)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114214, 13, 9579)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114215, 500, 9579)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114216, 546, 9579)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114217, 126, 9579)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114218, 533, 9579)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114219, 145, 9579)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114220, 518, 9579)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114221, 541, 9579)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114222, 519, 9579)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114223, 527, 9579)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114224, 514, 9579)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114225, 544, 9579)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114226, 496, 9579)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114227, 554, 9579)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114228, 528, 9579)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114229, 535, 9579)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114230, 13, 9585)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114231, 401, 9585)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114232, 405, 9585)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114233, 409, 9585)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114234, 419, 9585)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114235, 2, 9585)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114236, 410, 9585)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114237, 411, 9585)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114238, 416, 9585)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114239, 412, 9585)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114240, 414, 9585)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114241, 415, 9585)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114242, 84, 9585)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114243, 418, 9585)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114244, 401, 9587)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114245, 405, 9587)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114246, 409, 9587)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114247, 419, 9587)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114248, 2, 9587)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114249, 411, 9587)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114250, 416, 9587)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114251, 412, 9587)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114252, 414, 9587)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114253, 415, 9587)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114254, 418, 9587)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114255, 405, 9572)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114256, 244, 9572)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114257, 538, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114258, 530, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114259, 532, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114260, 533, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114261, 689, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114262, 531, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114263, 540, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114264, 78, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114265, 26, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114266, 597, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114267, 625, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114268, 716, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114269, 57, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114270, 536, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114271, 539, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114272, 1081, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114273, 25, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114274, 688, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114275, 537, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114276, 686, 9646)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114277, 635, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114278, 563, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114279, 585, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114280, 611, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114281, 603, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114282, 602, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114283, 584, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114284, 709, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114285, 1082, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114286, 612, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114287, 617, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114288, 620, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114289, 582, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114290, 621, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114291, 571, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114292, 577, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114293, 630, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114294, 701, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114295, 581, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114296, 664, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114297, 705, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114298, 25, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114299, 688, 9827)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114300, 607, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114301, 608, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114302, 604, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114303, 609, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114304, 610, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114305, 614, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114306, 611, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114307, 12, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114308, 612, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114309, 619, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114310, 620, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114311, 629, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114312, 625, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114313, 621, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114314, 700, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114315, 630, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114316, 628, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114317, 613, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114318, 588, 9824)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114319, 401, 9582)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114320, 530, 9582)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114321, 405, 9582)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114322, 689, 9582)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114323, 353, 9582)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114324, 531, 9582)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114325, 419, 9582)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114326, 2, 9582)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114327, 411, 9582)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114328, 416, 9582)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114329, 412, 9582)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114330, 414, 9582)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114331, 415, 9582)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114332, 25, 9582)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114333, 418, 9582)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114334, 552, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114335, 118, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114336, 606, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114337, 401, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114338, 130, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114339, 405, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114340, 406, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114341, 231, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114342, 126, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114343, 145, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114344, 407, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114345, 419, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114346, 514, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114347, 127, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114348, 131, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114349, 411, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114350, 600, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114351, 408, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114352, 1085, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114353, 416, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114354, 414, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114355, 415, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114356, 132, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114357, 129, 9576)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114358, 401, 9586)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114359, 405, 9586)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114360, 409, 9586)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114361, 419, 9586)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114362, 2, 9586)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114363, 411, 9586)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114364, 416, 9586)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114365, 412, 9586)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114366, 414, 9586)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114367, 415, 9586)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114368, 84, 9586)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114369, 418, 9586)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114370, 641, 9820)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114371, 644, 9820)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114372, 684, 9820)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114373, 720, 9820)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114374, 683, 9820)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114375, 630, 9820)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114376, 627, 9618)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114377, 625, 9618)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114378, 401, 9561)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114379, 404, 9561)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114380, 405, 9561)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114381, 533, 9561)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114382, 402, 9561)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114383, 409, 9561)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114384, 407, 9561)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114385, 419, 9561)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114386, 410, 9561)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114387, 411, 9561)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114388, 416, 9561)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114389, 412, 9561)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114390, 414, 9561)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114391, 415, 9561)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114392, 418, 9561)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114393, 606, 9822)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114394, 604, 9822)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114395, 187, 9822)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114396, 603, 9822)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114397, 602, 9822)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114398, 573, 9822)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114399, 584, 9822)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114400, 619, 9822)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114401, 630, 9822)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114402, 600, 9822)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114403, 583, 9822)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114404, 605, 9822)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114405, 599, 9822)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114406, 401, 9622)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114407, 406, 9622)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114408, 231, 9622)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114409, 126, 9622)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114410, 115, 9622)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114411, 407, 9622)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114412, 419, 9622)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114413, 411, 9622)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114414, 244, 9622)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114415, 408, 9622)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114416, 416, 9622)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114417, 414, 9622)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114418, 415, 9622)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114419, 418, 9622)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114420, 13, 9543)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114421, 500, 9543)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114422, 541, 9543)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114423, 77, 9543)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114424, 527, 9543)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114425, 514, 9543)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114426, 544, 9543)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114427, 496, 9543)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114428, 499, 9543)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114429, 76, 9543)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114430, 535, 9543)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114431, 607, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114432, 604, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114433, 610, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114434, 563, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114435, 611, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114436, 187, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114437, 603, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114438, 602, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114439, 573, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114440, 584, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114441, 634, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114442, 90, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114443, 191, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114444, 612, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114445, 617, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114446, 190, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114447, 620, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114448, 193, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114449, 188, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114450, 621, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114451, 630, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114452, 600, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114453, 581, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114454, 613, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114455, 664, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114456, 605, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114457, 647, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114458, 599, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114459, 622, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114460, 626, 9816)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114461, 68, 9642)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114462, 78, 9642)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114463, 26, 9642)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114464, 716, 9642)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114465, 511, 9642)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114466, 1081, 9642)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114467, 635, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114468, 604, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114469, 586, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114470, 562, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114471, 585, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114472, 570, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114473, 187, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114474, 603, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114475, 602, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114476, 573, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114477, 584, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114478, 634, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114479, 90, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114480, 629, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114481, 625, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114482, 662, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114483, 1002, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114484, 630, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114485, 600, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114486, 583, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114487, 605, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114488, 599, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114489, 622, 9814)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114490, 635, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114491, 604, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114492, 586, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114493, 562, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114494, 187, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114495, 603, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114496, 602, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114497, 573, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114498, 584, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114499, 634, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114500, 90, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114501, 629, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114502, 625, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114503, 662, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114504, 1002, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114505, 630, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114506, 600, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114507, 583, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114508, 605, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114509, 599, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114510, 622, 9812)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114511, 635, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114512, 604, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114513, 586, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114514, 562, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114515, 187, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114516, 603, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114517, 602, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114518, 573, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114519, 584, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114520, 634, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114521, 90, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114522, 662, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114523, 1002, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114524, 630, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114525, 600, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114526, 583, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114527, 605, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114528, 599, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114529, 622, 9815)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114530, 225, 9652)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114531, 23, 9652)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114532, 20, 9652)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114533, 685, 9652)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114534, 233, 9652)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114535, 675, 9652)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114536, 21, 9652)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114537, 13, 9602)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114538, 68, 9602)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114539, 530, 9602)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114540, 69, 9602)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114541, 75, 9602)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114542, 511, 9602)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114543, 13, 9601)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114544, 68, 9601)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114545, 530, 9601)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114546, 69, 9601)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114547, 75, 9601)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114548, 63, 9601)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114549, 511, 9601)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114550, 13, 9603)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114551, 68, 9603)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114552, 530, 9603)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114553, 69, 9603)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114554, 75, 9603)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114555, 78, 9603)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114556, 26, 9603)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114557, 716, 9603)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114558, 63, 9603)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114559, 511, 9603)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114560, 635, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114561, 604, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114562, 586, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114563, 562, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114564, 187, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114565, 603, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114566, 602, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114567, 573, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114568, 584, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114569, 634, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114570, 90, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114571, 662, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114572, 1002, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114573, 630, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114574, 600, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114575, 583, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114576, 605, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114577, 599, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114578, 622, 9813)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114579, 13, 9623)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114580, 500, 9623)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114581, 303, 9623)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114582, 406, 9623)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114583, 231, 9623)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114584, 145, 9623)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114585, 115, 9623)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114586, 407, 9623)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114587, 310, 9623)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114588, 544, 9623)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114589, 411, 9623)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114590, 408, 9623)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114591, 416, 9623)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114592, 412, 9623)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114593, 414, 9623)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114594, 415, 9623)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114595, 194, 9623)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114596, 650, 9650)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114597, 653, 9650)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114598, 553, 9650)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114599, 652, 9650)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114600, 651, 9650)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114601, 530, 9795)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114602, 532, 9795)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114603, 540, 9795)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114604, 551, 9795)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114605, 590, 9615)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114606, 562, 9615)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114607, 585, 9615)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114608, 570, 9615)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114609, 593, 9615)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114610, 597, 9615)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114611, 594, 9615)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114612, 571, 9615)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114613, 595, 9615)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114614, 630, 9615)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114615, 589, 9615)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114616, 588, 9615)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114617, 186, 9614)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114618, 586, 9614)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114619, 603, 9614)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114620, 602, 9614)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114621, 573, 9614)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114622, 584, 9614)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114623, 89, 9614)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114624, 479, 9614)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114625, 90, 9614)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114626, 534, 9614)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114627, 662, 9614)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114628, 1002, 9614)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114629, 583, 9614)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114630, 622, 9614)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114631, 126, 9588)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114632, 137, 9588)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114633, 408, 9588)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114634, 629, 9826)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114635, 699, 9826)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114636, 700, 9826)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114637, 630, 9826)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114638, 589, 9826)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114639, 588, 9826)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114640, 629, 9598)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114641, 699, 9598)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114642, 700, 9598)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114643, 630, 9598)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114644, 589, 9598)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114645, 588, 9598)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114646, 629, 9788)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114647, 699, 9788)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114648, 700, 9788)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114649, 630, 9788)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114650, 589, 9788)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114651, 705, 9788)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114652, 588, 9788)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114653, 235, 9546)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114654, 522, 9546)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114655, 236, 9546)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114656, 77, 9546)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114657, 527, 9546)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114658, 29, 9546)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114659, 30, 9546)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114660, 237, 9546)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114661, 55, 9544)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114662, 247, 9544)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114663, 56, 9544)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114664, 241, 9544)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114665, 243, 9544)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114666, 77, 9544)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114667, 463, 9544)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114668, 246, 9544)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114669, 59, 9544)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114670, 240, 9544)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114671, 55, 9545)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114672, 56, 9545)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114673, 463, 9545)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114674, 240, 9545)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114675, 530, 9794)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114676, 532, 9794)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114677, 540, 9794)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114678, 551, 9794)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114679, 538, 9792)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114680, 530, 9792)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114681, 532, 9792)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114682, 533, 9792)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114683, 689, 9792)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114684, 531, 9792)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114685, 540, 9792)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114686, 536, 9792)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114687, 539, 9792)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114688, 418, 9792)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114689, 538, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114690, 530, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114691, 532, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114692, 533, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114693, 689, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114694, 709, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114695, 531, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114696, 78, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114697, 26, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114698, 716, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114699, 712, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114700, 679, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114701, 536, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114702, 1081, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114703, 705, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114704, 25, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114705, 418, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114706, 688, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114707, 537, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114708, 686, 9648)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114709, 530, 9641)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114710, 689, 9641)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114711, 531, 9641)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114712, 540, 9641)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114713, 78, 9641)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114714, 26, 9641)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114715, 716, 9641)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114716, 632, 9641)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114717, 511, 9641)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114718, 536, 9641)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114719, 1081, 9641)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114720, 25, 9641)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114721, 688, 9641)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114722, 686, 9641)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114723, 401, 9578)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114724, 406, 9578)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114725, 231, 9578)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114726, 407, 9578)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114727, 419, 9578)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114728, 411, 9578)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114729, 244, 9578)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114730, 416, 9578)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114731, 414, 9578)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114732, 415, 9578)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114733, 84, 9578)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114734, 418, 9578)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114735, 586, 9581)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114736, 562, 9581)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114737, 35, 9581)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114738, 75, 9581)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114739, 137, 9581)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114740, 194, 9581)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114741, 155, 9581)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114742, 517, 9800)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114743, 13, 9800)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114744, 500, 9800)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114745, 353, 9800)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114746, 367, 9800)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114747, 363, 9800)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114748, 501, 9800)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114749, 366, 9800)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114750, 365, 9800)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114751, 49, 9800)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114752, 391, 9800)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114753, 374, 9800)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114754, 623, 9821)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114755, 303, 9821)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114756, 11, 9821)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114757, 130, 9821)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114758, 584, 9821)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114759, 127, 9821)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114760, 131, 9821)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114761, 132, 9821)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114762, 129, 9821)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114763, 401, 9577)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114764, 406, 9577)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114765, 231, 9577)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114766, 115, 9577)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114767, 407, 9577)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114768, 419, 9577)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114769, 411, 9577)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114770, 244, 9577)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114771, 416, 9577)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114772, 414, 9577)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114773, 415, 9577)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114774, 418, 9577)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114775, 13, 9550)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114776, 500, 9550)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114777, 3, 9550)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114778, 77, 9550)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114779, 29, 9550)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114780, 2, 9550)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114781, 30, 9550)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114782, 84, 9550)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114783, 636, 2890)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114784, 420, 2890)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114785, 422, 2890)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114786, 636, 3151)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114787, 420, 3151)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114788, 422, 3151)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114789, 13, 9647)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114790, 530, 9647)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114791, 533, 9647)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114792, 689, 9647)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114793, 12, 9647)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114794, 531, 9647)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114795, 78, 9647)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114796, 26, 9647)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114797, 632, 9647)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114798, 25, 9647)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114799, 418, 9647)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114800, 688, 9647)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114801, 686, 9647)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114802, 401, 9625)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114803, 406, 9625)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114804, 231, 9625)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114805, 126, 9625)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114806, 115, 9625)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114807, 407, 9625)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114808, 419, 9625)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114809, 411, 9625)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114810, 244, 9625)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114811, 408, 9625)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114812, 416, 9625)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114813, 414, 9625)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114814, 415, 9625)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114815, 418, 9625)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114816, 505, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114817, 13, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114818, 500, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114819, 530, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114820, 533, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114821, 518, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114822, 541, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114823, 519, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114824, 527, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114825, 514, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114826, 544, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114827, 496, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114828, 551, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114829, 554, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114830, 528, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114831, 535, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114832, 536, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114833, 246, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114834, 537, 9541)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114835, 562, 9611)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114836, 35, 9611)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114837, 584, 9611)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114838, 12, 9611)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114839, 612, 9611)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114840, 617, 9611)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114841, 620, 9611)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114842, 597, 9611)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114843, 621, 9611)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114844, 571, 9611)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114845, 2, 9611)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114846, 562, 9617)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114847, 12, 9617)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114848, 597, 9617)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114849, 571, 9617)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114850, 604, 9810)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114851, 623, 9810)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114852, 585, 9810)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114853, 611, 9810)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114854, 187, 9810)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114855, 603, 9810)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114856, 602, 9810)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114857, 573, 9810)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114858, 584, 9810)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114859, 634, 9810)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114860, 612, 9810)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114861, 193, 9810)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114862, 1002, 9810)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114863, 613, 9810)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114864, 583, 9810)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114865, 622, 9810)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114866, 626, 9810)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114867, 296, 9674)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114868, 295, 9674)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114869, 297, 9674)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114870, 552, 9548)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114871, 500, 9548)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114872, 545, 9548)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114873, 546, 9548)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114874, 533, 9548)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114875, 518, 9548)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114876, 541, 9548)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114877, 527, 9548)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114878, 514, 9548)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114879, 544, 9548)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114880, 496, 9548)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114881, 551, 9548)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114882, 554, 9548)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114883, 535, 9548)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114884, 585, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114885, 689, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114886, 603, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114887, 602, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114888, 12, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114889, 709, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114890, 540, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114891, 629, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114892, 571, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114893, 685, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114894, 630, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114895, 701, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114896, 702, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114897, 536, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114898, 1081, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114899, 705, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114900, 25, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114901, 418, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114902, 688, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114903, 686, 9789)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114904, 635, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114905, 585, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114906, 689, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114907, 602, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114908, 584, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114909, 709, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114910, 540, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114911, 582, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114912, 621, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114913, 571, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114914, 577, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114915, 630, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114916, 701, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114917, 581, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114918, 702, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114919, 705, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114920, 418, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114921, 688, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114922, 686, 9599)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114923, 401, 9580)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114924, 405, 9580)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114925, 187, 9580)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114926, 602, 9580)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114927, 419, 9580)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114928, 188, 9580)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114929, 410, 9580)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114930, 411, 9580)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114931, 416, 9580)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114932, 412, 9580)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114933, 414, 9580)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114934, 415, 9580)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114935, 418, 9580)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114936, 401, 9620)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114937, 404, 9620)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114938, 405, 9620)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114939, 406, 9620)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114940, 231, 9620)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114941, 115, 9620)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114942, 137, 9620)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114943, 407, 9620)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114944, 419, 9620)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114945, 399, 9620)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114946, 411, 9620)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114947, 244, 9620)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114948, 408, 9620)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114949, 416, 9620)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114950, 412, 9620)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114951, 414, 9620)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114952, 415, 9620)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114953, 418, 9620)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114954, 611, 9604)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114955, 603, 9604)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114956, 602, 9604)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114957, 612, 9604)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114958, 617, 9604)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114959, 620, 9604)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114960, 645, 9604)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114961, 193, 9604)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114962, 675, 9604)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114963, 581, 9604)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114964, 613, 9604)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114965, 647, 9604)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114966, 648, 9604)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114967, 626, 9604)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114968, 505, 9634)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114969, 187, 9634)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114970, 602, 9634)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114971, 137, 9634)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114972, 600, 9634)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114973, 194, 9634)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114974, 155, 9634)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114975, 129, 9634)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114976, 605, 9634)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114977, 599, 9634)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114978, 635, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114979, 623, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114980, 565, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114981, 586, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114982, 562, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114983, 585, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114984, 570, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114985, 584, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114986, 643, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114987, 619, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114988, 620, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114989, 575, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114990, 582, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114991, 629, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114992, 625, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114993, 621, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114994, 571, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114995, 577, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114996, 633, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114997, 578, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114998, 632, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (114999, 630, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115000, 581, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115001, 664, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115002, 647, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115003, 622, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115004, 648, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115005, 626, 9811)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115006, 604, 9823)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115007, 187, 9823)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115008, 603, 9823)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115009, 602, 9823)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115010, 584, 9823)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115011, 634, 9823)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115012, 630, 9823)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115013, 605, 9823)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115014, 604, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115015, 586, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115016, 562, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115017, 585, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115018, 570, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115019, 187, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115020, 603, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115021, 602, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115022, 573, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115023, 584, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115024, 634, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115025, 90, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115026, 629, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115027, 625, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115028, 630, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115029, 600, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115030, 583, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115031, 605, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115032, 599, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115033, 622, 9595)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115034, 118, 9637)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115035, 623, 9637)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115036, 130, 9637)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115037, 135, 9637)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115038, 126, 9637)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115039, 127, 9637)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115040, 131, 9637)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115041, 132, 9637)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115042, 129, 9637)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115043, 136, 9637)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115044, 635, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115045, 604, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115046, 610, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115047, 585, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115048, 611, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115049, 689, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115050, 612, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115051, 620, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115052, 582, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115053, 629, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115054, 630, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115055, 378, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115056, 377, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115057, 701, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115058, 581, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115059, 702, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115060, 613, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115061, 705, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115062, 418, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115063, 688, 9791)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115064, 635, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115065, 604, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115066, 610, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115067, 585, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115068, 611, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115069, 689, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115070, 612, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115071, 620, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115072, 582, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115073, 629, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115074, 630, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115075, 378, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115076, 377, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115077, 701, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115078, 581, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115079, 702, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115080, 613, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115081, 705, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115082, 418, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115083, 688, 9790)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115084, 604, 9819)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115085, 623, 9819)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115086, 585, 9819)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115087, 611, 9819)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115088, 187, 9819)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115089, 603, 9819)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115090, 602, 9819)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115091, 573, 9819)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115092, 584, 9819)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115093, 634, 9819)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115094, 612, 9819)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115095, 193, 9819)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115096, 1002, 9819)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115097, 613, 9819)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115098, 583, 9819)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115099, 622, 9819)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115100, 626, 9819)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115101, 401, 9584)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115102, 530, 9584)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115103, 405, 9584)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115104, 689, 9584)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115105, 353, 9584)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115106, 531, 9584)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115107, 419, 9584)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115108, 411, 9584)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115109, 416, 9584)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115110, 412, 9584)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115111, 414, 9584)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115112, 415, 9584)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115113, 418, 9584)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115114, 4, 9797)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115115, 472, 9797)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115116, 311, 9797)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115117, 312, 9797)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115118, 303, 9797)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115119, 299, 9797)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115120, 81, 9797)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115121, 88, 9797)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115122, 477, 9797)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115123, 314, 9797)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115124, 298, 9797)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115125, 485, 9797)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115126, 13, 9547)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115127, 500, 9547)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115128, 518, 9547)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115129, 541, 9547)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115130, 77, 9547)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115131, 527, 9547)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115132, 29, 9547)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115133, 544, 9547)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115134, 496, 9547)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115135, 528, 9547)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115136, 76, 9547)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115137, 535, 9547)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115138, 246, 9547)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115139, 84, 9547)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115140, 401, 9573)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115141, 69, 9573)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115142, 406, 9573)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115143, 231, 9573)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115144, 407, 9573)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115145, 419, 9573)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115146, 411, 9573)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115147, 244, 9573)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115148, 416, 9573)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115149, 414, 9573)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115150, 415, 9573)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115151, 401, 9619)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115152, 406, 9619)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115153, 231, 9619)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115154, 115, 9619)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115155, 407, 9619)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115156, 419, 9619)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115157, 411, 9619)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115158, 244, 9619)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115159, 408, 9619)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115160, 416, 9619)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115161, 414, 9619)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115162, 415, 9619)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115163, 418, 9619)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115164, 68, 9638)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115165, 419, 9638)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115166, 465, 9638)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115167, 286, 9638)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115168, 466, 9638)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115169, 467, 9638)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115170, 1081, 9638)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115171, 8, 9638)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115172, 401, 9629)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115173, 69, 9629)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115174, 406, 9629)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115175, 231, 9629)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115176, 115, 9629)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115177, 137, 9629)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115178, 407, 9629)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115179, 419, 9629)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115180, 411, 9629)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115181, 244, 9629)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115182, 408, 9629)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115183, 416, 9629)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115184, 414, 9629)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115185, 415, 9629)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115186, 418, 9629)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115187, 13, 9563)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115188, 401, 9563)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115189, 405, 9563)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115190, 409, 9563)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115191, 419, 9563)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115192, 2, 9563)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115193, 410, 9563)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115194, 411, 9563)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115195, 416, 9563)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115196, 412, 9563)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115197, 414, 9563)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115198, 415, 9563)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115199, 84, 9563)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115200, 418, 9563)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115201, 13, 9645)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115202, 401, 9645)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115203, 68, 9645)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115204, 12, 9645)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115205, 419, 9645)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115206, 716, 9645)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115207, 287, 9645)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115208, 465, 9645)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115209, 286, 9645)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115210, 2, 9645)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115211, 466, 9645)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115212, 467, 9645)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115213, 1081, 9645)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115214, 8, 9645)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115215, 635, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115216, 530, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115217, 585, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115218, 689, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115219, 602, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115220, 584, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115221, 709, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115222, 531, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115223, 540, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115224, 582, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115225, 419, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115226, 621, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115227, 571, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115228, 577, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115229, 630, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115230, 701, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115231, 416, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115232, 581, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115233, 702, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115234, 536, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115235, 539, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115236, 1081, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115237, 705, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115238, 418, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115239, 688, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115240, 686, 9600)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115241, 13, 9564)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115242, 401, 9564)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115243, 405, 9564)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115244, 409, 9564)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115245, 419, 9564)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115246, 2, 9564)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115247, 410, 9564)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115248, 411, 9564)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115249, 416, 9564)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115250, 412, 9564)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115251, 414, 9564)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115252, 415, 9564)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115253, 84, 9564)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115254, 418, 9564)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115255, 586, 9612)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115256, 603, 9612)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115257, 602, 9612)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115258, 573, 9612)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115259, 584, 9612)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115260, 89, 9612)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115261, 479, 9612)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115262, 90, 9612)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115263, 662, 9612)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115264, 577, 9612)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115265, 578, 9612)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115266, 1002, 9612)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115267, 583, 9612)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115268, 622, 9612)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115269, 636, 9651)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115270, 420, 9651)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115271, 422, 9651)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115272, 428, 9651)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115273, 42, 9651)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115274, 421, 9651)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115275, 3, 9635)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115276, 137, 9635)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115277, 407, 9635)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115278, 2, 9635)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115279, 702, 9635)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115280, 705, 9635)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115281, 25, 9635)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115282, 401, 9627)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115283, 69, 9627)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115284, 406, 9627)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115285, 231, 9627)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115286, 115, 9627)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115287, 137, 9627)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115288, 407, 9627)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115289, 411, 9627)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115290, 244, 9627)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115291, 408, 9627)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115292, 416, 9627)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115293, 414, 9627)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115294, 415, 9627)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115295, 418, 9627)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115296, 401, 9628)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115297, 69, 9628)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115298, 406, 9628)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115299, 231, 9628)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115300, 115, 9628)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115301, 137, 9628)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115302, 407, 9628)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115303, 411, 9628)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115304, 244, 9628)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115305, 408, 9628)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115306, 416, 9628)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115307, 414, 9628)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115308, 415, 9628)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115309, 418, 9628)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115310, 607, 9554)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115311, 608, 9554)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115312, 604, 9554)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115313, 609, 9554)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115314, 610, 9554)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115315, 601, 9554)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115316, 611, 9554)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115317, 612, 9554)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115318, 620, 9554)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115319, 597, 9554)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115320, 629, 9554)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115321, 188, 9554)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115322, 630, 9554)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115323, 613, 9554)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115324, 615, 9554)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115325, 607, 9559)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115326, 608, 9559)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115327, 610, 9559)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115328, 706, 9559)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115329, 601, 9559)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115330, 611, 9559)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115331, 353, 9559)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115332, 612, 9559)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115333, 620, 9559)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115334, 417, 9559)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115335, 621, 9559)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115336, 613, 9559)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115337, 607, 9556)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115338, 604, 9556)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115339, 706, 9556)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115340, 601, 9556)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115341, 187, 9556)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115342, 603, 9556)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115343, 602, 9556)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115344, 573, 9556)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115345, 584, 9556)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115346, 630, 9556)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115347, 583, 9556)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115348, 622, 9556)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115349, 607, 9560)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115350, 606, 9560)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115351, 608, 9560)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115352, 604, 9560)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115353, 609, 9560)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115354, 610, 9560)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115355, 611, 9560)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115356, 612, 9560)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115357, 619, 9560)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115358, 620, 9560)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115359, 625, 9560)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115360, 621, 9560)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115361, 571, 9560)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115362, 700, 9560)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115363, 613, 9560)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115364, 615, 9560)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115365, 588, 9560)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115366, 401, 9575)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115367, 405, 9575)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115368, 409, 9575)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115369, 2, 9575)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115370, 701, 9575)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115371, 702, 9575)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115372, 414, 9575)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115373, 418, 9575)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115374, 586, 9803)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115375, 12, 9803)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115376, 403, 9803)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115377, 587, 9803)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115378, 378, 9803)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115379, 377, 9803)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115380, 705, 9803)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115381, 25, 9803)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115382, 296, 9672)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115383, 295, 9672)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115384, 297, 9672)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115385, 517, 9802)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115386, 13, 9802)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115387, 500, 9802)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115388, 40, 9802)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115389, 367, 9802)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115390, 371, 9802)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115391, 380, 9802)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115392, 390, 9802)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115393, 364, 9802)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115394, 363, 9802)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115395, 501, 9802)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115396, 366, 9802)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115397, 365, 9802)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115398, 496, 9802)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115399, 391, 9802)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115400, 13, 9798)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115401, 500, 9798)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115402, 358, 9798)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115403, 389, 9798)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115404, 346, 9798)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115405, 367, 9798)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115406, 364, 9798)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115407, 363, 9798)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115408, 501, 9798)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115409, 366, 9798)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115410, 365, 9798)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115411, 496, 9798)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115412, 391, 9798)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115413, 13, 9552)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115414, 500, 9552)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115415, 3, 9552)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115416, 77, 9552)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115417, 2, 9552)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115418, 84, 9552)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115419, 13, 9551)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115420, 500, 9551)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115421, 3, 9551)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115422, 77, 9551)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115423, 29, 9551)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115424, 2, 9551)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115425, 30, 9551)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115426, 84, 9551)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115427, 586, 9589)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115428, 616, 9589)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115429, 611, 9589)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115430, 612, 9589)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115431, 617, 9589)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115432, 620, 9589)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115433, 193, 9589)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115434, 534, 9589)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115435, 621, 9589)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115436, 577, 9589)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115437, 578, 9589)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115438, 1002, 9589)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115439, 630, 9589)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115440, 613, 9589)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115441, 439, 9589)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115442, 622, 9589)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115443, 626, 9589)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115444, 3, 9649)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115445, 137, 9649)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115446, 127, 9649)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115447, 131, 9649)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115448, 63, 9649)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115449, 129, 9649)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115450, 401, 9569)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115451, 530, 9569)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115452, 405, 9569)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115453, 689, 9569)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115454, 353, 9569)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115455, 531, 9569)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115456, 419, 9569)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115457, 2, 9569)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115458, 411, 9569)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115459, 416, 9569)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115460, 412, 9569)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115461, 414, 9569)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115462, 415, 9569)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115463, 25, 9569)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115464, 418, 9569)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115465, 401, 9567)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115466, 530, 9567)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115467, 405, 9567)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115468, 689, 9567)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115469, 353, 9567)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115470, 531, 9567)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115471, 419, 9567)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115472, 411, 9567)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115473, 416, 9567)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115474, 412, 9567)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115475, 414, 9567)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115476, 415, 9567)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115477, 418, 9567)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115478, 401, 9565)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115479, 530, 9565)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115480, 405, 9565)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115481, 689, 9565)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115482, 353, 9565)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115483, 531, 9565)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115484, 419, 9565)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115485, 411, 9565)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115486, 416, 9565)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115487, 412, 9565)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115488, 414, 9565)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115489, 415, 9565)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115490, 418, 9565)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115491, 401, 9568)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115492, 530, 9568)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115493, 405, 9568)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115494, 689, 9568)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115495, 353, 9568)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115496, 531, 9568)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115497, 419, 9568)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115498, 2, 9568)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115499, 411, 9568)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115500, 416, 9568)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115501, 412, 9568)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115502, 414, 9568)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115503, 415, 9568)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115504, 25, 9568)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115505, 418, 9568)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115506, 401, 9566)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115507, 530, 9566)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115508, 405, 9566)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115509, 689, 9566)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115510, 353, 9566)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115511, 531, 9566)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115512, 419, 9566)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115513, 2, 9566)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115514, 411, 9566)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115515, 416, 9566)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115516, 412, 9566)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115517, 414, 9566)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115518, 415, 9566)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115519, 25, 9566)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115520, 418, 9566)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115521, 401, 9570)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115522, 530, 9570)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115523, 405, 9570)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115524, 689, 9570)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115525, 353, 9570)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115526, 531, 9570)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115527, 419, 9570)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115528, 2, 9570)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115529, 411, 9570)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115530, 416, 9570)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115531, 412, 9570)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115532, 414, 9570)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115533, 415, 9570)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115534, 25, 9570)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115535, 418, 9570)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115536, 277, 9796)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115537, 278, 9796)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115538, 284, 9796)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115539, 279, 9796)
INSERT INTO [dbo].[Library.MilitaryOccupationROnet] ([Id], [ROnetId], [MilitaryOccupationId]) VALUES (115540, 288, 9796)
SET IDENTITY_INSERT [dbo].[Library.MilitaryOccupationROnet] OFF
-- Operation applied to 2246 rows out of 2246

-- Add constraints to [dbo].[Library.MilitaryOccupationROnet]
ALTER TABLE [dbo].[Library.MilitaryOccupationROnet] ADD CONSTRAINT [FK__Library.M__Milit__025D5595] FOREIGN KEY ([MilitaryOccupationId]) REFERENCES [dbo].[Library.MilitaryOccupation] ([Id])
ALTER TABLE [dbo].[Library.MilitaryOccupationROnet] ADD CONSTRAINT [FK__Library.M__ROnet__0169315C] FOREIGN KEY ([ROnetId]) REFERENCES [dbo].[Library.ROnet] ([Id])

-- Add constraints to [dbo].[Library.MilitaryOccupationGroupROnet]
ALTER TABLE [dbo].[Library.MilitaryOccupationGroupROnet] ADD CONSTRAINT [FK__Library.M__Milit__3AA1AEB8] FOREIGN KEY ([MilitaryOccupationGroupId]) REFERENCES [dbo].[Library.MilitaryOccupationGroup] ([Id])
ALTER TABLE [dbo].[Library.MilitaryOccupationGroupROnet] ADD CONSTRAINT [FK__Library.M__ROnet__39AD8A7F] FOREIGN KEY ([ROnetId]) REFERENCES [dbo].[Library.ROnet] ([Id])

-- Add constraints to [dbo].[Library.MilitaryOccupation]
ALTER TABLE [dbo].[Library.MilitaryOccupation] ADD CONSTRAINT [FK__Library.M__Milit__740F363E] FOREIGN KEY ([MilitaryOccupationGroupId]) REFERENCES [dbo].[Library.MilitaryOccupationGroup] ([Id])

-- Add constraints to [dbo].[Library.LocalisationItem]
ALTER TABLE [dbo].[Library.LocalisationItem] ADD CONSTRAINT [FK__Library.L__Local__119F9925] FOREIGN KEY ([LocalisationId]) REFERENCES [dbo].[Library.Localisation] ([Id])
COMMIT TRANSACTION
GO