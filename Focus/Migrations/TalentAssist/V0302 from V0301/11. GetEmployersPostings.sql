SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================
-- Author:		Andy Jones
-- Create date: 5 September 2013
-- Description:	Get Employers Postings Ordered By Number of Positions Asc 
-- ======================================================================
CREATE PROCEDURE Data_Application_GetEmployersPostingsOrderByNumberOfPositionsAsc 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@SpideredJobs bit = null,
	@HasMatches bit = null,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
	
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PersonPostingMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)

	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		OriginId bigint,
		EmployerName nvarchar(255),
		PostingDate datetime,
		EmployerEmailAddress nvarchar(255),
		HasPersonMatches bit, 
		EmployerTableId int
	)

	DECLARE @Employers AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int
	)

	-- Populate the matches for this User
	INSERT INTO @PersonPostingMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
	GROUP BY 
		ppm.PostingId

	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.OriginId,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		'' AS EmployerEmailAddress,
		CASE
			WHEN ppm.MatchCount > 0 THEN 1
			ELSE 0
		END AS HasPersonMatches,
		null
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		LEFT OUTER JOIN @PersonPostingMatches ppm  ON p.Id = ppm.PostingId
	WHERE 
		((p.OriginId = 999 AND @SpideredJobs = 1) OR (p.OriginId <> 999 AND @SpideredJobs = 0) OR @SpideredJobs is null)
		AND (p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		AND ((@HasMatches = 1 AND ppm.MatchCount > 0) OR (@HasMatches = 0 AND(ppm.MatchCount = 0 OR ppm.MatchCount is null)) OR @HasMatches is null)
		
	-- Populate the employer details
	INSERT INTO @Employers
	SELECT 
		EmployerName,
		COUNT(Id)
	FROM 
		@Postings 
	GROUP BY 
		EmployerName
	ORDER BY 
		COUNT(Id) desc, EmployerName	

	SELECT @RowCount = @@ROWCOUNT

	-- Delete Employers not in paged data
	DELETE @Employers
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)

	-- Delete Postings whose Employers not in paged data
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- Update the Postings table to allow sorting
	UPDATE @Postings 
	SET
		EmployerTableId = e.TableId
	FROM 
		@Employers e
		INNER JOIN @Postings p ON e.EmployerName = p.EmployerName

	-- Return data
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		OriginId,
		EmployerName,
		PostingDate,
		EmployerEmailAddress,
		HasPersonMatches
	FROM  
		@Postings
	ORDER BY 
		EmployerTableId, PostingDate DESC
END
GO

-- ======================================================================
-- Author:		Andy Jones
-- Create date: 5 September 2013
-- Description:	Get Employers Postings Ordered By Employer Name Asc 
-- ======================================================================
CREATE PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByEmployerNameAsc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@SpideredJobs bit = null,
	@HasMatches bit = null,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
	
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PersonPostingMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)

	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		OriginId bigint,
		EmployerName nvarchar(255),
		PostingDate datetime,
		EmployerEmailAddress nvarchar(255),
		HasPersonMatches bit, 
		EmployerTableId int
	)

	DECLARE @Employers AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int
	)

	-- Populate the matches for this User
	INSERT INTO @PersonPostingMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
	GROUP BY 
		ppm.PostingId

	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.OriginId,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		'' AS EmployerEmailAddress,
		CASE
			WHEN ppm.MatchCount > 0 THEN 1
			ELSE 0
		END AS HasPersonMatches,
		null
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		LEFT OUTER JOIN @PersonPostingMatches ppm  ON p.Id = ppm.PostingId
	WHERE 
		((p.OriginId = 999 AND @SpideredJobs = 1) OR (p.OriginId <> 999 AND @SpideredJobs = 0) OR @SpideredJobs is null)
		AND (p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		AND ((@HasMatches = 1 AND ppm.MatchCount > 0) OR (@HasMatches = 0 AND(ppm.MatchCount = 0 OR ppm.MatchCount is null)) OR @HasMatches is null)
		
	-- Populate the employer details
	INSERT INTO @Employers
	SELECT 
		EmployerName,
		COUNT(Id)
	FROM 
		@Postings 
	GROUP BY 
		EmployerName
	ORDER BY 
		EmployerName	

	SELECT @RowCount = @@ROWCOUNT

	-- Delete Employers not in paged data
	DELETE @Employers
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)

	-- Delete Postings whose Employers not in paged data
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- Update the Postings table to allow sorting
	UPDATE @Postings 
	SET
		EmployerTableId = e.TableId
	FROM 
		@Employers e
		INNER JOIN @Postings p ON e.EmployerName = p.EmployerName

	-- Return data
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		OriginId,
		EmployerName,
		PostingDate,
		EmployerEmailAddress,
		HasPersonMatches
	FROM  
		@Postings
	ORDER BY 
		EmployerTableId, EmployerName, PostingDate DESC
END

GO

-- ======================================================================
-- Author:		Andy Jones
-- Create date: 5 September 2013
-- Description:	Get Employers Postings Ordered By Employer Name Desc 
-- ======================================================================
CREATE PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByEmployerNameDesc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@SpideredJobs bit = null,
	@HasMatches bit = null,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
	
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PersonPostingMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)

	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		OriginId bigint,
		EmployerName nvarchar(255),
		PostingDate datetime,
		EmployerEmailAddress nvarchar(255),
		HasPersonMatches bit, 
		EmployerTableId int
	)

	DECLARE @Employers AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int
	)

	-- Populate the matches for this User
	INSERT INTO @PersonPostingMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
	GROUP BY 
		ppm.PostingId

	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.OriginId,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		'' AS EmployerEmailAddress,
		CASE
			WHEN ppm.MatchCount > 0 THEN 1
			ELSE 0
		END AS HasPersonMatches,
		null
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		LEFT OUTER JOIN @PersonPostingMatches ppm  ON p.Id = ppm.PostingId
	WHERE 
		((p.OriginId = 999 AND @SpideredJobs = 1) OR (p.OriginId <> 999 AND @SpideredJobs = 0) OR @SpideredJobs is null)
		AND (p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		AND ((@HasMatches = 1 AND ppm.MatchCount > 0) OR (@HasMatches = 0 AND(ppm.MatchCount = 0 OR ppm.MatchCount is null)) OR @HasMatches is null)
		
	-- Populate the employer details
	INSERT INTO @Employers
	SELECT 
		EmployerName,
		COUNT(Id)
	FROM 
		@Postings 
	GROUP BY 
		EmployerName
	ORDER BY 
		EmployerName DESC	

	SELECT @RowCount = @@ROWCOUNT

	-- Delete Employers not in paged data
	DELETE @Employers
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)

	-- Delete Postings whose Employers not in paged data
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- Update the Postings table to allow sorting
	UPDATE @Postings 
	SET
		EmployerTableId = e.TableId
	FROM 
		@Employers e
		INNER JOIN @Postings p ON e.EmployerName = p.EmployerName

	-- Return data
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		OriginId,
		EmployerName,
		PostingDate,
		EmployerEmailAddress,
		HasPersonMatches
	FROM  
		@Postings
	ORDER BY 
		EmployerTableId, PostingDate DESC
END

GO

-- ======================================================================
-- Author:		Andy Jones
-- Create date: 5 September 2013
-- Description:	Get Employers Postings Ordered By Job Title Desc 
-- ======================================================================
CREATE PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByJobTitleDesc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@SpideredJobs bit = null,
	@HasMatches bit = null,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
	
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PersonPostingMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)

	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		OriginId bigint,
		EmployerName nvarchar(255),
		PostingDate datetime,
		EmployerEmailAddress nvarchar(255),
		HasPersonMatches bit, 
		EmployerTableId int
	)

	DECLARE @Employers AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int
	)

	-- Populate the matches for this User
	INSERT INTO @PersonPostingMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
	GROUP BY 
		ppm.PostingId

	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.OriginId,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		'' AS EmployerEmailAddress,
		CASE
			WHEN ppm.MatchCount > 0 THEN 1
			ELSE 0
		END AS HasPersonMatches,
		null
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		LEFT OUTER JOIN @PersonPostingMatches ppm  ON p.Id = ppm.PostingId
	WHERE 
		((p.OriginId = 999 AND @SpideredJobs = 1) OR (p.OriginId <> 999 AND @SpideredJobs = 0) OR @SpideredJobs is null)
		AND (p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		AND ((@HasMatches = 1 AND ppm.MatchCount > 0) OR (@HasMatches = 0 AND(ppm.MatchCount = 0 OR ppm.MatchCount is null)) OR @HasMatches is null)
		
	-- Populate the employer details
	INSERT INTO @Employers
	SELECT 
		EmployerName,
		COUNT(Id)
	FROM 
		@Postings 
	GROUP BY 
		EmployerName
	ORDER BY 
		MAX(JobTitle) DESC
		

	SELECT @RowCount = @@ROWCOUNT

	-- Delete Employers not in paged data
	DELETE @Employers
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)

	-- Delete Postings whose Employers not in paged data
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- Update the Postings table to allow sorting
	UPDATE @Postings 
	SET
		EmployerTableId = e.TableId
	FROM 
		@Employers e
		INNER JOIN @Postings p ON e.EmployerName = p.EmployerName

	-- Return data
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		OriginId,
		EmployerName,
		PostingDate,
		EmployerEmailAddress,
		HasPersonMatches
	FROM  
		@Postings
	ORDER BY 
		EmployerTableId, JobTitle DESC, PostingDate DESC
END

GO

-- ======================================================================
-- Author:		Andy Jones
-- Create date: 5 September 2013
-- Description:	Get Employers Postings Ordered By Job Title Asc 
-- ======================================================================
CREATE PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByJobTitleAsc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@SpideredJobs bit = null,
	@HasMatches bit = null,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
	
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PersonPostingMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)

	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		OriginId bigint,
		EmployerName nvarchar(255),
		PostingDate datetime,
		EmployerEmailAddress nvarchar(255),
		HasPersonMatches bit, 
		EmployerTableId int
	)

	DECLARE @Employers AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int
	)

	-- Populate the matches for this User
	INSERT INTO @PersonPostingMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
	GROUP BY 
		ppm.PostingId

	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.OriginId,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		'' AS EmployerEmailAddress,
		CASE
			WHEN ppm.MatchCount > 0 THEN 1
			ELSE 0
		END AS HasPersonMatches,
		null
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		LEFT OUTER JOIN @PersonPostingMatches ppm  ON p.Id = ppm.PostingId
	WHERE 
		((p.OriginId = 999 AND @SpideredJobs = 1) OR (p.OriginId <> 999 AND @SpideredJobs = 0) OR @SpideredJobs is null)
		AND (p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		AND ((@HasMatches = 1 AND ppm.MatchCount > 0) OR (@HasMatches = 0 AND(ppm.MatchCount = 0 OR ppm.MatchCount is null)) OR @HasMatches is null)
		
	-- Populate the employer details
	INSERT INTO @Employers
	SELECT 
		EmployerName,
		COUNT(Id)
	FROM 
		@Postings 
	GROUP BY 
		EmployerName
	ORDER BY 
		MIN(JobTitle)
		

	SELECT @RowCount = @@ROWCOUNT

	-- Delete Employers not in paged data
	DELETE @Employers
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)

	-- Delete Postings whose Employers not in paged data
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- Update the Postings table to allow sorting
	UPDATE @Postings 
	SET
		EmployerTableId = e.TableId
	FROM 
		@Employers e
		INNER JOIN @Postings p ON e.EmployerName = p.EmployerName

	-- Return data
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		OriginId,
		EmployerName,
		PostingDate,
		EmployerEmailAddress,
		HasPersonMatches
	FROM  
		@Postings
	ORDER BY 
		EmployerTableId, JobTitle, PostingDate DESC
END

GO

-- ======================================================================
-- Author:		Andy Jones
-- Create date: 5 September 2013
-- Description:	Get Employers Postings Ordered By Posting Date Asc 
-- ======================================================================
CREATE PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByPostingDateAsc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@SpideredJobs bit = null,
	@HasMatches bit = null,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
	
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PersonPostingMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)

	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		OriginId bigint,
		EmployerName nvarchar(255),
		PostingDate datetime,
		EmployerEmailAddress nvarchar(255),
		HasPersonMatches bit, 
		EmployerTableId int
	)

	DECLARE @Employers AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int
	)

	-- Populate the matches for this User
	INSERT INTO @PersonPostingMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
	GROUP BY 
		ppm.PostingId

	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.OriginId,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		'' AS EmployerEmailAddress,
		CASE
			WHEN ppm.MatchCount > 0 THEN 1
			ELSE 0
		END AS HasPersonMatches,
		null
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		LEFT OUTER JOIN @PersonPostingMatches ppm  ON p.Id = ppm.PostingId
	WHERE 
		((p.OriginId = 999 AND @SpideredJobs = 1) OR (p.OriginId <> 999 AND @SpideredJobs = 0) OR @SpideredJobs is null)
		AND (p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		AND ((@HasMatches = 1 AND ppm.MatchCount > 0) OR (@HasMatches = 0 AND(ppm.MatchCount = 0 OR ppm.MatchCount is null)) OR @HasMatches is null)
	
	-- Populate the employer details
	INSERT INTO @Employers
	SELECT 
		EmployerName,
		COUNT(Id)
	FROM 
		@Postings 
	GROUP BY 
		EmployerName
	ORDER BY 
		MAX(PostingDate)
		

	SELECT @RowCount = @@ROWCOUNT

	-- Delete Employers not in paged data
	DELETE @Employers
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)

	-- Delete Postings whose Employers not in paged data
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- Update the Postings table to allow sorting
	UPDATE @Postings 
	SET
		EmployerTableId = e.TableId
	FROM 
		@Employers e
		INNER JOIN @Postings p ON e.EmployerName = p.EmployerName

	-- Return data
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		OriginId,
		EmployerName,
		PostingDate,
		EmployerEmailAddress,
		HasPersonMatches
	FROM  
		@Postings
	ORDER BY 
		EmployerTableId, EmployerName, PostingDate DESC
END

GO

-- ======================================================================
-- Author:		Andy Jones
-- Create date: 5 September 2013
-- Description:	Get Employers Postings Ordered By Posting Date Desc 
-- ======================================================================
CREATE PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByPostingDateDesc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@SpideredJobs bit = null,
	@HasMatches bit = null,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
	
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PersonPostingMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)

	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		OriginId bigint,
		EmployerName nvarchar(255),
		PostingDate datetime,
		EmployerEmailAddress nvarchar(255),
		HasPersonMatches bit, 
		EmployerTableId int
	)

	DECLARE @Employers AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int
	)

	-- Populate the matches for this User
	INSERT INTO @PersonPostingMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
	GROUP BY 
		ppm.PostingId

	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.OriginId,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		'' AS EmployerEmailAddress,
		CASE
			WHEN ppm.MatchCount > 0 THEN 1
			ELSE 0
		END AS HasPersonMatches,
		null
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		LEFT OUTER JOIN @PersonPostingMatches ppm  ON p.Id = ppm.PostingId
	WHERE 
		((p.OriginId = 999 AND @SpideredJobs = 1) OR (p.OriginId <> 999 AND @SpideredJobs = 0) OR @SpideredJobs is null)
		AND (p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		AND ((@HasMatches = 1 AND ppm.MatchCount > 0) OR (@HasMatches = 0 AND(ppm.MatchCount = 0 OR ppm.MatchCount is null)) OR @HasMatches is null)
		
	-- Populate the employer details
	INSERT INTO @Employers
	SELECT 
		EmployerName,
		COUNT(Id)
	FROM 
		@Postings 
	GROUP BY 
		EmployerName
	ORDER BY 
		MAX(PostingDate) DESC
		

	SELECT @RowCount = @@ROWCOUNT

	-- Delete Employers not in paged data
	DELETE @Employers
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)

	-- Delete Postings whose Employers not in paged data
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- Update the Postings table to allow sorting
	UPDATE @Postings 
	SET
		EmployerTableId = e.TableId
	FROM 
		@Employers e
		INNER JOIN @Postings p ON e.EmployerName = p.EmployerName

	-- Return data
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		OriginId,
		EmployerName,
		PostingDate,
		EmployerEmailAddress,
		HasPersonMatches
	FROM  
		@Postings
	ORDER BY 
		EmployerTableId, EmployerName, PostingDate DESC
END

GO

-- Permissions
GRANT EXECUTE ON  [dbo].[Data_Application_GetEmployersPostingsOrderByNumberOfPositionsAsc] TO [FocusAgent]
GO
GRANT EXECUTE ON  [dbo].[Data_Application_GetEmployersPostingsOrderByEmployerNameAsc] TO [FocusAgent]
GO
GRANT EXECUTE ON  [dbo].[Data_Application_GetEmployersPostingsOrderByEmployerNameDesc] TO [FocusAgent]
GO
GRANT EXECUTE ON  [dbo].[Data_Application_GetEmployersPostingsOrderByJobTitleDesc] TO [FocusAgent]
GO
GRANT EXECUTE ON  [dbo].[Data_Application_GetEmployersPostingsOrderByJobTitleAsc] TO [FocusAgent]
GO
GRANT EXECUTE ON  [dbo].[Data_Application_GetEmployersPostingsOrderByPostingDateAsc] TO [FocusAgent]
GO
GRANT EXECUTE ON  [dbo].[Data_Application_GetEmployersPostingsOrderByPostingDateDesc] TO [FocusAgent]
GO
