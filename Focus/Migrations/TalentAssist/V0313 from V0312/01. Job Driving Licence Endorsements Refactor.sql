BEGIN TRANSACTION

CREATE TABLE [Data.Application.JobDrivingLicenceEndorsement] (Id BIGINT NOT NULL PRIMARY KEY ,
[DrivingLicenceEndorsementId] BIGINT NOT NULL DEFAULT 0,
[JobId] BIGINT NOT NULL DEFAULT 0,
FOREIGN KEY (JobId) REFERENCES [Data.Application.Job](Id));

DECLARE @PassTransportId bigint
DECLARE @HazardousMaterialsId bigint
DECLARE @TankVehicleId BIGINT
DECLARE @TankHazardId BIGINT
DECLARE @DoublesTriplesId BIGINT
DECLARE @AirbrakesId BIGINT
DECLARE @SchoolBusId BIGINT
DECLARE @MotorcycleId BIGINT
DECLARE @StartId BIGINT
DECLARE @NumberRowsToInsert BIGINT

DECLARE @JobEndorsements TABLE (JobId BIGINT, EndorsementId BIGINT)

SELECT @StartId = NextId + 1 FROM KeyTable

SELECT @PassTransportId = Id FROM [Config.CodeItem] WHERE ExternalId = 'drv_pass_flag'
SELECT @HazardousMaterialsId = Id FROM [Config.CodeItem] WHERE ExternalId = 'drv_hazard_flag'
SELECT @TankVehicleId = Id FROM [Config.CodeItem] WHERE ExternalId = 'drv_tank_flag'
SELECT @TankHazardId = Id FROM [Config.CodeItem] WHERE ExternalId = 'drv_tankhazard_flag'
SELECT @DoublesTriplesId = Id FROM [Config.CodeItem] WHERE ExternalId = 'drv_double_flag'
SELECT @AirbrakesId = Id FROM [Config.CodeItem] WHERE ExternalId = 'drv_airbrake_flag'
SELECT @SchoolBusId = Id FROM [Config.CodeItem] WHERE ExternalId = 'drv_bus_flag'
SELECT @MotorcycleId = Id FROM [Config.CodeItem] WHERE ExternalId = 'drv_cycle_flag'

INSERT INTO @JobEndorsements
SELECT Id, @PassTransportId
FROM [Data.Application.Job] 
WHERE DrivingLicenceEndorsements & 1 = 1

INSERT INTO @JobEndorsements
SELECT Id, @HazardousMaterialsId 
FROM [Data.Application.Job] 
WHERE DrivingLicenceEndorsements & 2 = 2

INSERT INTO @JobEndorsements
SELECT Id, @TankVehicleId
FROM [Data.Application.Job] 
WHERE DrivingLicenceEndorsements & 4 = 4

INSERT INTO @JobEndorsements
SELECT Id, @TankHazardId
FROM [Data.Application.Job] 
WHERE DrivingLicenceEndorsements & 8 = 8

INSERT INTO @JobEndorsements
SELECT Id, @DoublesTriplesId
FROM [Data.Application.Job] 
WHERE DrivingLicenceEndorsements & 16 = 16

INSERT INTO @JobEndorsements
SELECT Id, @AirbrakesId 
FROM [Data.Application.Job] 
WHERE DrivingLicenceEndorsements & 32 = 32

INSERT INTO @JobEndorsements
SELECT Id, @SchoolBusId 
FROM [Data.Application.Job] 
WHERE DrivingLicenceEndorsements & 64 = 64

INSERT INTO @JobEndorsements
SELECT Id, @MotorcycleId 
FROM [Data.Application.Job] 
WHERE DrivingLicenceEndorsements & 128 = 128

SELECT @NumberRowsToInsert = COUNT(1) FROM @JobEndorsements

UPDATE KeyTable SET NextId = @StartId + 25

INSERT INTO [Data.Application.JobDrivingLicenceEndorsement] (Id, DrivingLicenceEndorsementId, JobId)
SELECT @StartId + ROW_NUMBER() OVER (ORDER BY JobId, EndorsementId), EndorsementId, JobId FROM @JobEndorsements

-- Uncomment below to see the records added
--SELECT * FROM [Data.Application.JobDrivingLicenceEndorsement]

ALTER TABLE [Data.application.job] DROP COLUMN [DrivingLicenceEndorsements];

COMMIT TRANSACTION

