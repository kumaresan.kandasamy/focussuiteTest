
/*
Run this script on:

        BOSDBSVR001.FocusStage    -  This database will be modified

to synchronize it with:

        BOSDBSVR001.FocusDev

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.0.0 from Red Gate Software Ltd at 29/11/2012 17:12:33

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[Message]'
GO
ALTER TABLE [dbo].[Message] ADD
[UserId] [bigint] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--PRINT N'Creating [dbo].[StatusLog]'
--GO
--CREATE TABLE [dbo].[StatusLog]
--(
--[Id] [bigint] NOT NULL,
--[EntityId] [bigint] NOT NULL CONSTRAINT [DF__StatusLog__Entit__00EA0E6F] DEFAULT ((0)),
--[OriginalStatus] [bigint] NULL,
--[NewStatus] [bigint] NOT NULL CONSTRAINT [DF__StatusLog__NewSt__01DE32A8] DEFAULT ((0)),
--[UserId] [bigint] NOT NULL CONSTRAINT [DF__StatusLog__UserI__02D256E1] DEFAULT ((0)),
--[ActionedOn] [datetime] NOT NULL,
--[EntityTypeId] [bigint] NOT NULL CONSTRAINT [DF__StatusLog__Entit__03C67B1A] DEFAULT ((0))
--)
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
--PRINT N'Creating primary key [PK__StatusLo__3214EC077F01C5FD] on [dbo].[StatusLog]'
--GO
--ALTER TABLE [dbo].[StatusLog] ADD CONSTRAINT [PK__StatusLo__3214EC077F01C5FD] PRIMARY KEY CLUSTERED  ([Id])
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
PRINT N'Altering [dbo].[Job]'
GO
ALTER TABLE [dbo].[Job] ADD
[HiringFromTaxCreditProgramNotificationSent] [bit] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ApplicationView]'
GO







ALTER VIEW [dbo].[ApplicationView]
AS

SELECT
	CandidateApplication.Id AS Id,
	Job.Id AS JobId,
	Job.EmployerId AS EmployerId,
	Candidate.Id AS CandidateId,
	Candidate.ExternalId AS CandidateExternalId,
	CandidateApplication.ApplicationStatus AS CandidateApplicationStatus,
	CandidateApplication.ApplicationScore AS CandidateApplicationScore,
	CandidateApplication.CreatedOn AS CandidateApplicationReceivedOn,
	CandidateApplication.ApprovalStatus AS CandidateApplicationApprovalStatus,
	Candidate.FirstName AS CandidateFirstName,
	Candidate.LastName AS CandidateLastName,
	Candidate.YearsExperience AS CandidateYearsExperience,
	Candidate.IsVeteran AS CandidateIsVeteran
FROM CandidateApplication WITH (NOLOCK)
	INNER JOIN Job WITH (NOLOCK) ON CandidateApplication.JobId = Job.Id
	INNER JOIN Candidate WITH (NOLOCK) ON CandidateApplication.CandidateId = Candidate.Id







GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[JobPostingReferralView]'
GO
EXEC sp_refreshview N'[dbo].[JobPostingReferralView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[JobView]'
GO
ALTER VIEW dbo.JobView
AS
SELECT j.Id, j.EmployerId, e.Name AS EmployerName, j.JobTitle, bu.Name AS BusinessUnitName, j.JobStatus, j.PostedOn, j.ClosingOn, j.HeldOn, j.ClosedOn, j.UpdatedOn, 
               ISNULL(cag.ApplicationCount, 0) AS ApplicationCount, j.EmployeeId, j.ApprovalStatus, ISNULL(crg.ReferralCount, 0) AS ReferralCount, j.FederalContractor, 
               j.ForeignLabourCertificationH2A, j.ForeignLabourCertificationH2B, j.ForeignLabourCertificationOther, j.CourtOrderedAffirmativeAction, u.UserName, j.CreatedOn, 
               j.YellowProfanityWords
FROM  dbo.Job AS j WITH (NOLOCK) LEFT OUTER JOIN
               dbo.Employee AS employee WITH (NOLOCK) ON j.EmployeeId = employee.Id LEFT OUTER JOIN
               dbo.[User] AS u WITH (NOLOCK) ON employee.PersonId = u.PersonId LEFT OUTER JOIN
               dbo.Employer AS e WITH (NOLOCK) ON j.EmployerId = e.Id LEFT OUTER JOIN
               dbo.BusinessUnit AS bu WITH (NOLOCK) ON bu.Id = j.BusinessUnitId AND bu.IsPrimary = 1 LEFT OUTER JOIN
                   (SELECT JobId, COUNT(1) AS ApplicationCount
                    FROM   dbo.CandidateApplication AS ca WITH (NOLOCK)
                    WHERE (ApprovalStatus = 2)
                    GROUP BY JobId) AS cag ON cag.JobId = j.Id LEFT OUTER JOIN
                   (SELECT JobId, COUNT(1) AS ReferralCount
                    FROM   dbo.CandidateApplication AS ca WITH (NOLOCK)
                    WHERE (ApprovalStatus = 1)
                    GROUP BY JobId) AS crg ON crg.JobId = j.Id
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--PRINT N'Creating [dbo].[NoteReminder]'
--GO
--CREATE TABLE [dbo].[NoteReminder]
--(
--[Id] [bigint] NOT NULL,
--[NoteReminderType] [int] NOT NULL CONSTRAINT [DF__NoteRemin__NoteR__644DCFC1] DEFAULT ((0)),
--[Text] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__NoteRemind__Text__6541F3FA] DEFAULT (''),
--[EntityId] [bigint] NOT NULL CONSTRAINT [DF__NoteRemin__Entit__66361833] DEFAULT ((0)),
--[EntityType] [int] NOT NULL CONSTRAINT [DF__NoteRemin__Entit__672A3C6C] DEFAULT ((0)),
--[ReminderVia] [int] NULL,
--[ReminderDue] [datetime] NULL,
--[ReminderSentOn] [datetime] NULL,
--[CreatedBy] [bigint] NOT NULL CONSTRAINT [DF__NoteRemin__Creat__681E60A5] DEFAULT ((0)),
--[CreatedOn] [datetime] NOT NULL,
--[ReminderUserId] [bigint] NULL
--)
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
--PRINT N'Creating primary key [PK__NoteRemi__3214EC076265874F] on [dbo].[NoteReminder]'
--GO
--ALTER TABLE [dbo].[NoteReminder] ADD CONSTRAINT [PK__NoteRemi__3214EC076265874F] PRIMARY KEY CLUSTERED  ([Id])
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
--PRINT N'Creating index [IX_NoteReminder_NoteReminderType] on [dbo].[NoteReminder]'
--GO
--CREATE NONCLUSTERED INDEX [IX_NoteReminder_NoteReminderType] ON [dbo].[NoteReminder] ([NoteReminderType])
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
--PRINT N'Creating index [IX_NoteReminder_EntityId] on [dbo].[NoteReminder]'
--GO
--CREATE NONCLUSTERED INDEX [IX_NoteReminder_EntityId] ON [dbo].[NoteReminder] ([EntityId])
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
--PRINT N'Altering [dbo].[SavedSearch]'
--GO
--ALTER TABLE [dbo].[SavedSearch] ADD
--[Type] [int] NULL
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
--PRINT N'Altering [dbo].[EmailTemplate]'
--GO
--ALTER TABLE [dbo].[EmailTemplate] ADD
--[Recipient] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
--PRINT N'Creating [dbo].[SavedSearchUser]'
--GO
--CREATE TABLE [dbo].[SavedSearchUser]
--(
--[Id] [int] NOT NULL,
--[SavedSearchId] [bigint] NOT NULL CONSTRAINT [DF__SavedSear__Saved__75785BC3] DEFAULT ((0)),
--[UserId] [bigint] NOT NULL CONSTRAINT [DF__SavedSear__UserI__766C7FFC] DEFAULT ((0))
--)
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
--PRINT N'Creating primary key [PK__SavedSea__3214EC0773901351] on [dbo].[SavedSearchUser]'
--GO
--ALTER TABLE [dbo].[SavedSearchUser] ADD CONSTRAINT [PK__SavedSea__3214EC0773901351] PRIMARY KEY CLUSTERED  ([Id])
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
PRINT N'Creating [dbo].[ApplicationStatusLogView]'
GO










CREATE VIEW [dbo].[ApplicationStatusLogView]
AS

SELECT 
	StatusLog.Id AS Id,
	StatusLog.ActionedOn AS ActionedOn,
	CandidateApplication.ApplicationScore AS CandidateApplicationScore,
	Candidate.FirstName AS CandidateFirstName,
	Candidate.LastName AS CandidateLastName,
	Candidate.YearsExperience AS CandidateYearsExperience,
	Candidate.IsVeteran AS CandidateIsVeteran,
	StatusLog.NewStatus AS CandidateApplicationStatus,
	CandidateApplication.JobId AS JobId,
	LatestJob.JobTitle AS LatestJobTitle,
	LatestJob.Employer AS LatestJobEmployer,
	LatestJob.StartYear AS LatestJobStartYear,
	LatestJob.EndYear AS LatestJobEndYear,
	CandidateApplication.Id AS CandidateApplicationId
FROM StatusLog WITH (NOLOCK)
	INNER JOIN CandidateApplication WITH (NOLOCK) ON StatusLog.EntityId = CandidateApplication.Id
	INNER JOIN Candidate WITH (NOLOCK) ON CandidateApplication.CandidateId = Candidate.Id 
	LEFT OUTER JOIN (SELECT JobTitle, Employer, StartYear, EndYear, CandidateId FROM 
						(SELECT JobTitle, Employer, StartYear, EndYear, CandidateId, RANK() OVER (PARTITION BY CandidateId ORDER BY EndYear desc, StartYear desc, Id) RankScore
						FROM CandidateWorkHistory WITH (NOLOCK)) AS RankedCandidateWorkHistory WHERE RankScore = 1) AS LatestJob 
	ON Candidate.Id = LatestJob.CandidateId










GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[ExpiringJobView]'
GO
EXEC sp_refreshview N'[dbo].[ExpiringJobView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[JobSeekerReferralView]'
GO

ALTER VIEW [dbo].[JobSeekerReferralView]
AS
SELECT dbo.CandidateApplication.Id, dbo.Candidate.FirstName + ' ' + dbo.Candidate.LastName AS Name, dbo.CandidateApplication.CreatedOn AS ReferralDate, 
               dbo.GetBusinessDays(dbo.CandidateApplication.CreatedOn, GETDATE()) AS TimeInQueue, dbo.Job.JobTitle, dbo.Employer.Name AS EmployerName, 
               dbo.Employer.Id AS EmployerId, dbo.Job.Posting, dbo.Candidate.Id AS CandidateId, dbo.Job.Id AS JobId, dbo.Candidate.ExternalId AS CandidateExternalId, 
               dbo.CandidateApplication.ApprovalRequiredReason, 
               CASE WHEN dbo.Candidate.IsVeteran = 1 THEN 'Yes' WHEN dbo.Candidate.IsVeteran = 0 THEN 'No' ELSE '' END AS Veteran, 
               dbo.EmployerAddress.TownCity AS Town, dbo.LookupItemsView.Value AS State
FROM  dbo.Candidate WITH (NOLOCK) INNER JOIN
               dbo.CandidateApplication WITH (NOLOCK) ON dbo.Candidate.Id = dbo.CandidateApplication.CandidateId INNER JOIN
               dbo.Job WITH (NOLOCK) ON dbo.CandidateApplication.JobId = dbo.Job.Id INNER JOIN
               dbo.Employer WITH (NOLOCK) ON dbo.Job.EmployerId = dbo.Employer.Id INNER JOIN
               dbo.EmployerAddress ON dbo.Employer.Id = dbo.EmployerAddress.EmployerId INNER JOIN
               dbo.LookupItemsView ON dbo.EmployerAddress.StateId = dbo.LookupItemsView.Id
WHERE (dbo.CandidateApplication.ApplicationStatus = 1 AND dbo.CandidateApplication.ApprovalStatus = 1)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[EmployeeSearchView]'
GO
EXEC sp_refreshview N'[dbo].[EmployeeSearchView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[NoteReminderView]'
GO


CREATE VIEW [dbo].[NoteReminderView]
AS

SELECT
	NoteReminder.Id,
	NoteReminder.NoteReminderType,
	NoteReminder.EntityId,
	[Text],
	Person.FirstName AS CreatedByFirstname,
	Person.LastName AS CreatedByLastname,
	NoteReminder.CreatedOn,
	NoteReminder.EntityType
FROM
	NoteReminder WITH (NOLOCK)
	INNER JOIN [User] ON NoteReminder.CreatedBy = [User].Id
	INNER JOIN [Person] ON [User].PersonId = Person.Id

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[JobActionEventView]'
GO
CREATE VIEW dbo.JobActionEventView
AS
SELECT ae.Id, j.Id AS JobId, ae.UserId, p.FirstName, p.LastName, ae.ActionedOn, at.Name AS ActionName, ae.AdditionalDetails
FROM  dbo.ActionType AS at INNER JOIN
               dbo.ActionEvent AS ae ON at.Id = ae.ActionTypeId INNER JOIN
               dbo.Job AS j ON ae.EntityId = j.Id INNER JOIN
               dbo.[User] AS u ON ae.UserId = u.Id INNER JOIN
               dbo.Person AS p ON u.PersonId = p.Id
WHERE (at.Name = 'PostJob') OR
               (at.Name = 'SaveJob')  OR
               (at.Name = 'HoldJob')  OR
               (at.Name = 'CloseJob') OR
               (at.Name = 'RefreshJob') OR
               (at.Name = 'ReactivateJob') OR
               (at.Name = 'CreateJob')  OR
               (at.Name = 'ApprovePostingReferral') 
UNION ALL
SELECT ae.Id, ca.JobId, ae.UserId, p.FirstName, p.LastName, ae.ActionedOn, at.Name AS ActionName, ae.AdditionalDetails
FROM  dbo.ActionType AS at INNER JOIN
               dbo.ActionEvent AS ae ON at.Id = ae.ActionTypeId INNER JOIN
               dbo.CandidateApplication AS ca ON ae.EntityId = ca.Id INNER JOIN
               dbo.[User] AS u ON ae.UserId = u.Id INNER JOIN
               dbo.Person AS p ON u.PersonId = p.Id
WHERE (at.Name = 'ApproveCandidateReferral')  OR
               (at.Name = 'SelfReferred') OR
               (at.Name = 'DenyCandidateReferral') OR
               (at.Name = 'CreateCandidateApplication')  OR
               (at.Name = 'UpdateApplicationStatusToSelfReferred')





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--PRINT N'Adding foreign keys to [dbo].[NoteReminder]'
--GO
--ALTER TABLE [dbo].[NoteReminder] ADD CONSTRAINT [FK__NoteRemin__Remin__691284DE] FOREIGN KEY ([ReminderUserId]) REFERENCES [dbo].[User] ([Id])
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
--PRINT N'Adding foreign keys to [dbo].[SavedSearchUser]'
--GO
--ALTER TABLE [dbo].[SavedSearchUser] ADD CONSTRAINT [FK__SavedSear__Saved__7760A435] FOREIGN KEY ([SavedSearchId]) REFERENCES [dbo].[SavedSearch] ([Id])
--ALTER TABLE [dbo].[SavedSearchUser] ADD CONSTRAINT [FK__SavedSear__UserI__7854C86E] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id])
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
--PRINT N'Adding foreign keys to [dbo].[StatusLog]'
--GO
--ALTER TABLE [dbo].[StatusLog] ADD CONSTRAINT [FK__StatusLog__Entit__04BA9F53] FOREIGN KEY ([EntityTypeId]) REFERENCES [dbo].[EntityType] ([Id])
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
--PRINT N'Altering extended properties'
--GO
--EXEC sp_updateextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
--Begin DesignProperties = 
--   Begin PaneConfigurations = 
--      Begin PaneConfiguration = 0
--         NumPanes = 4
--         Configuration = "(H (1[42] 4[25] 2[25] 3) )"
--      End
--      Begin PaneConfiguration = 1
--         NumPanes = 3
--         Configuration = "(H (1 [50] 4 [25] 3))"
--      End
--      Begin PaneConfiguration = 2
--         NumPanes = 3
--         Configuration = "(H (1 [50] 2 [25] 3))"
--      End
--      Begin PaneConfiguration = 3
--         NumPanes = 3
--         Configuration = "(H (4 [30] 2 [40] 3))"
--      End
--      Begin PaneConfiguration = 4
--         NumPanes = 2
--         Configuration = "(H (1 [56] 3))"
--      End
--      Begin PaneConfiguration = 5
--         NumPanes = 2
--         Configuration = "(H (2 [66] 3))"
--      End
--      Begin PaneConfiguration = 6
--         NumPanes = 2
--         Configuration = "(H (4 [50] 3))"
--      End
--      Begin PaneConfiguration = 7
--         NumPanes = 1
--         Configuration = "(V (3))"
--      End
--      Begin PaneConfiguration = 8
--         NumPanes = 3
--         Configuration = "(H (1[56] 4[18] 2) )"
--      End
--      Begin PaneConfiguration = 9
--         NumPanes = 2
--         Configuration = "(H (1 [75] 4))"
--      End
--      Begin PaneConfiguration = 10
--         NumPanes = 2
--         Configuration = "(H (1[66] 2) )"
--      End
--      Begin PaneConfiguration = 11
--         NumPanes = 2
--         Configuration = "(H (4 [60] 2))"
--      End
--      Begin PaneConfiguration = 12
--         NumPanes = 1
--         Configuration = "(H (1) )"
--      End
--      Begin PaneConfiguration = 13
--         NumPanes = 1
--         Configuration = "(V (4))"
--      End
--      Begin PaneConfiguration = 14
--         NumPanes = 1
--         Configuration = "(V (2))"
--      End
--      ActivePaneConfig = 0
--   End
--   Begin DiagramPane = 
--      Begin Origin = 
--         Top = 0
--         Left = 0
--      End
--      Begin Tables = 
--         Begin Table = "j"
--            Begin Extent = 
--               Top = 7
--               Left = 48
--               Bottom = 148
--               Right = 344
--            End
--            DisplayFlags = 280
--            TopColumn = 70
--         End
--         Begin Table = "employee"
--            Begin Extent = 
--               Top = 148
--               Left = 409
--               Bottom = 289
--               Right = 593
--            End
--            DisplayFlags = 280
--            TopColumn = 0
--         End
--         Begin Table = "u"
--            Begin Extent = 
--               Top = 6
--               Left = 1065
--               Bottom = 147
--               Right = 1280
--            End
--            DisplayFlags = 280
--            TopColumn = 0
--         End
--         Begin Table = "e"
--            Begin Extent = 
--               Top = 227
--               Left = 887
--               Bottom = 368
--               Right = 1234
--            End
--            DisplayFlags = 280
--            TopColumn = 9
--         End
--         Begin Table = "bu"
--            Begin Extent = 
--               Top = 154
--               Left = 48
--               Bottom = 295
--               Right = 232
--            End
--            DisplayFlags = 280
--            TopColumn = 0
--         End
--         Begin Table = "cag"
--            Begin Extent = 
--               Top = 7
--               Left = 787
--               Bottom = 112
--               Right = 977
--            End
--            DisplayFlags = 280
--            TopColumn = 0
--         End
--         Begin Table = "crg"
--            Begin Extent = 
--               Top = 7
--               Left = 392
--               Bottom = 112
--               Right = 576
--            End
--            DisplayFlags = 280
--            TopColumn = 0
-- ', 'SCHEMA', N'dbo', 'VIEW', N'JobView', NULL, NULL
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
--EXEC sp_updateextendedproperty N'MS_DiagramPane2', N'        End
--      End
--   End
--   Begin SQLPane = 
--   End
--   Begin DataPane = 
--      Begin ParameterDefaults = ""
--      End
--   End
--   Begin CriteriaPane = 
--      Begin ColumnWidths = 11
--         Column = 1440
--         Alias = 1536
--         Table = 1176
--         Output = 720
--         Append = 1400
--         NewValue = 1170
--         SortType = 1356
--         SortOrder = 1416
--         GroupBy = 1350
--         Filter = 1356
--         Or = 1350
--         Or = 1350
--         Or = 1350
--      End
--   End
--End
--', 'SCHEMA', N'dbo', 'VIEW', N'JobView', NULL, NULL
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
--PRINT N'Creating extended properties'
--GO
--EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
--Begin DesignProperties = 
--   Begin PaneConfigurations = 
--      Begin PaneConfiguration = 0
--         NumPanes = 4
--         Configuration = "(H (1[41] 4[20] 2[33] 3) )"
--      End
--      Begin PaneConfiguration = 1
--         NumPanes = 3
--         Configuration = "(H (1 [50] 4 [25] 3))"
--      End
--      Begin PaneConfiguration = 2
--         NumPanes = 3
--         Configuration = "(H (1 [50] 2 [25] 3))"
--      End
--      Begin PaneConfiguration = 3
--         NumPanes = 3
--         Configuration = "(H (4 [30] 2 [40] 3))"
--      End
--      Begin PaneConfiguration = 4
--         NumPanes = 2
--         Configuration = "(H (1 [56] 3))"
--      End
--      Begin PaneConfiguration = 5
--         NumPanes = 2
--         Configuration = "(H (2 [66] 3))"
--      End
--      Begin PaneConfiguration = 6
--         NumPanes = 2
--         Configuration = "(H (4 [50] 3))"
--      End
--      Begin PaneConfiguration = 7
--         NumPanes = 1
--         Configuration = "(V (3))"
--      End
--      Begin PaneConfiguration = 8
--         NumPanes = 3
--         Configuration = "(H (1[56] 4[18] 2) )"
--      End
--      Begin PaneConfiguration = 9
--         NumPanes = 2
--         Configuration = "(H (1 [75] 4))"
--      End
--      Begin PaneConfiguration = 10
--         NumPanes = 2
--         Configuration = "(H (1[66] 2) )"
--      End
--      Begin PaneConfiguration = 11
--         NumPanes = 2
--         Configuration = "(H (4 [60] 2))"
--      End
--      Begin PaneConfiguration = 12
--         NumPanes = 1
--         Configuration = "(H (1) )"
--      End
--      Begin PaneConfiguration = 13
--         NumPanes = 1
--         Configuration = "(V (4))"
--      End
--      Begin PaneConfiguration = 14
--         NumPanes = 1
--         Configuration = "(V (2))"
--      End
--      ActivePaneConfig = 0
--   End
--   Begin DiagramPane = 
--      Begin Origin = 
--         Top = 0
--         Left = 0
--      End
--      Begin Tables = 
--         Begin Table = "at"
--            Begin Extent = 
--               Top = 7
--               Left = 48
--               Bottom = 148
--               Right = 232
--            End
--            DisplayFlags = 280
--            TopColumn = 0
--         End
--         Begin Table = "ae"
--            Begin Extent = 
--               Top = 154
--               Left = 48
--               Bottom = 295
--               Right = 255
--            End
--            DisplayFlags = 280
--            TopColumn = 6
--         End
--         Begin Table = "ca"
--            Begin Extent = 
--               Top = 301
--               Left = 48
--               Bottom = 442
--               Right = 286
--            End
--            DisplayFlags = 280
--            TopColumn = 0
--         End
--         Begin Table = "u"
--            Begin Extent = 
--               Top = 448
--               Left = 48
--               Bottom = 589
--               Right = 263
--            End
--            DisplayFlags = 280
--            TopColumn = 0
--         End
--         Begin Table = "p"
--            Begin Extent = 
--               Top = 595
--               Left = 48
--               Bottom = 736
--               Right = 268
--            End
--            DisplayFlags = 280
--            TopColumn = 0
--         End
--      End
--   End
--   Begin SQLPane = 
--   End
--   Begin DataPane = 
--      Begin ParameterDefaults = ""
--      End
--      Begin ColumnWidths = 9
--         Width = 284
--         Width = 1200
--         Width = 1200
--         Width = 1200
--         Width = 1200
--         Width = 1200
--         Width = 1200
--         Width = 1200
--         Width = 1200
--      End
--   End
--   Begin CriteriaPane = 
--      Begin ColumnWidths = 26
--         Column = 1440
--         Alias = 900
--         Table = 1176
--         Outp', 'SCHEMA', N'dbo', 'VIEW', N'JobActionEventView', NULL, NULL
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
--EXEC sp_addextendedproperty N'MS_DiagramPane2', N'ut = 720
--         Append = 1400
--         NewValue = 1170
--         SortType = 1356
--         SortOrder = 1416
--         GroupBy = 1350
--         Filter = 1356
--         Or = 1350
--         Or = 1350
--         Or = 1350
--         Or = 1350
--         Or = 1350
--         Or = 1350
--         Or = 1350
--         Or = 1350
--         Or = 1350
--         Or = 1350
--         Or = 1350
--         Or = 1350
--         Or = 1350
--         Or = 1350
--         Or = 1350
--         Or = 1350
--         Or = 1350
--         Or = 1350
--         Or = 1350
--      End
--   End
--End
--', 'SCHEMA', N'dbo', 'VIEW', N'JobActionEventView', NULL, NULL
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
--DECLARE @xp int
--SELECT @xp=2
--EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'JobActionEventView', NULL, NULL
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO