SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE [dbo].[Job] ADD ForeignLabourCertificationH2A BIT NULL
ALTER TABLE [dbo].[Job] ADD ForeignLabourCertificationH2B BIT NULL
ALTER TABLE [dbo].[Job] ADD ForeignLabourCertificationOther BIT NULL
ALTER TABLE [dbo].[Job] ADD MinimumExperienceMonths INT NULL

CREATE TABLE [dbo].[NoteReminder](
	[Id] [bigint] NOT NULL,
	[NoteReminderType] [int] NOT NULL,
	[Text] [nvarchar](max) NOT NULL,
	[EntityId] [bigint] NOT NULL,
	[EntityType] [int] NOT NULL,
	[ReminderVia] [int] NULL,
	[ReminderDue] [datetime] NULL,
	[ReminderSentOn] [datetime] NULL,
	[CreatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ReminderUserId] [bigint] NULL,
 CONSTRAINT [PK__NoteRemi__3214EC076265874F] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[NoteReminder]  WITH CHECK ADD  CONSTRAINT [FK__NoteRemin__Remin__691284DE] FOREIGN KEY([ReminderUserId])
REFERENCES [dbo].[User] ([Id])
GO

ALTER TABLE [dbo].[NoteReminder] CHECK CONSTRAINT [FK__NoteRemin__Remin__691284DE]
GO

ALTER TABLE [dbo].[NoteReminder] ADD  CONSTRAINT [DF__NoteRemin__NoteR__644DCFC1]  DEFAULT ((0)) FOR [NoteReminderType]
GO

ALTER TABLE [dbo].[NoteReminder] ADD  CONSTRAINT [DF__NoteRemind__Text__6541F3FA]  DEFAULT ('') FOR [Text]
GO

ALTER TABLE [dbo].[NoteReminder] ADD  CONSTRAINT [DF__NoteRemin__Entit__66361833]  DEFAULT ((0)) FOR [EntityId]
GO

ALTER TABLE [dbo].[NoteReminder] ADD  CONSTRAINT [DF__NoteRemin__Entit__672A3C6C]  DEFAULT ((0)) FOR [EntityType]
GO

ALTER TABLE [dbo].[NoteReminder] ADD  CONSTRAINT [DF__NoteRemin__Creat__681E60A5]  DEFAULT ((0)) FOR [CreatedBy]
GO

CREATE NONCLUSTERED INDEX [IX_NoteReminder_EntityId] ON [dbo].[NoteReminder] 
(
	[EntityId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_NoteReminder_NoteReminderType] ON [dbo].[NoteReminder] 
(
	[NoteReminderType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO



SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StatusLog](
	[Id] [bigint] NOT NULL,
	[EntityId] [bigint] NOT NULL,
	[OriginalStatus] [bigint] NULL,
	[NewStatus] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[ActionedOn] [datetime] NOT NULL,
	[EntityTypeId] [bigint] NOT NULL,
 CONSTRAINT [PK__StatusLo__3214EC077F01C5FD] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[StatusLog]  WITH CHECK ADD  CONSTRAINT [FK__StatusLog__Entit__04BA9F53] FOREIGN KEY([EntityTypeId])
REFERENCES [dbo].[EntityType] ([Id])
GO

ALTER TABLE [dbo].[StatusLog] CHECK CONSTRAINT [FK__StatusLog__Entit__04BA9F53]
GO

ALTER TABLE [dbo].[StatusLog] ADD  CONSTRAINT [DF__StatusLog__Entit__00EA0E6F]  DEFAULT ((0)) FOR [EntityId]
GO

ALTER TABLE [dbo].[StatusLog] ADD  CONSTRAINT [DF__StatusLog__NewSt__01DE32A8]  DEFAULT ((0)) FOR [NewStatus]
GO

ALTER TABLE [dbo].[StatusLog] ADD  CONSTRAINT [DF__StatusLog__UserI__02D256E1]  DEFAULT ((0)) FOR [UserId]
GO

ALTER TABLE [dbo].[StatusLog] ADD  CONSTRAINT [DF__StatusLog__Entit__03C67B1A]  DEFAULT ((0)) FOR [EntityTypeId]
GO

/****** Object:  Table [dbo].[SavedSearchUser]    Script Date: 01/09/2013 14:26:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SavedSearchUser](
	[Id] [int] NOT NULL,
	[SavedSearchId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
 CONSTRAINT [PK__SavedSea__3214EC0773901351] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SavedSearchUser]  WITH CHECK ADD  CONSTRAINT [FK__SavedSear__Saved__7760A435] FOREIGN KEY([SavedSearchId])
REFERENCES [dbo].[SavedSearch] ([Id])
GO

ALTER TABLE [dbo].[SavedSearchUser] CHECK CONSTRAINT [FK__SavedSear__Saved__7760A435]
GO

ALTER TABLE [dbo].[SavedSearchUser]  WITH CHECK ADD  CONSTRAINT [FK__SavedSear__UserI__7854C86E] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO

ALTER TABLE [dbo].[SavedSearchUser] CHECK CONSTRAINT [FK__SavedSear__UserI__7854C86E]
GO

ALTER TABLE [dbo].[SavedSearchUser] ADD  CONSTRAINT [DF__SavedSear__Saved__75785BC3]  DEFAULT ((0)) FOR [SavedSearchId]
GO

ALTER TABLE [dbo].[SavedSearchUser] ADD  CONSTRAINT [DF__SavedSear__UserI__766C7FFC]  DEFAULT ((0)) FOR [UserId]
GO

ALTER TABLE [dbo].[EmailTemplate] ADD Recipient NVARCHAR(100) NULL

ALTER TABLE [dbo].[SavedSearch] ADD [Type] INT NULL