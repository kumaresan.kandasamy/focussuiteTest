﻿BEGIN TRANSACTION

UPDATE CandidateApplication SET ApplicationStatus = 13 WHERE ApplicationStatus = 6
UPDATE CandidateApplication SET ApplicationStatus = 6 WHERE ApplicationStatus = 4
UPDATE CandidateApplication SET ApplicationStatus = 6 WHERE ApplicationStatus = 7
UPDATE CandidateApplication SET ApplicationStatus = 12 WHERE ApplicationStatus = 5
UPDATE CandidateApplication SET ApplicationStatus = 4 WHERE ApplicationStatus = 9
UPDATE CandidateApplication SET ApplicationStatus = 9 WHERE ApplicationStatus = 8
UPDATE CandidateApplication SET ApplicationStatus = 4 WHERE ApplicationStatus = 3
UPDATE CandidateApplication SET ApplicationStatus = 3 WHERE ApplicationStatus = 2 AND ApprovalStatus = 2
UPDATE CandidateApplication SET ApplicationStatus = 1 WHERE ApplicationStatus = 2 AND ApprovalStatus <> 2
UPDATE CandidateApplication SET ApplicationStatus = 2 WHERE ApplicationStatus = 1

UPDATE ActionType SET Name='UpdateApplicationStatusToNotHired' WHERE Name='UpdateApplicationStatusToRejected'

COMMIT TRANSACTION