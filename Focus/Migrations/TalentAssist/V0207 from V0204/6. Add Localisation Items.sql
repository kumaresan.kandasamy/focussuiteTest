﻿BEGIN TRANSACTION

DECLARE @NextId bigint
DECLARE @EndOfBlockNextId bigint
DECLARE @LocalisationId bigint
DECLARE @IdsRequired bigint

SELECT @IdsRequired = 20

UPDATE KeyTable SET NextId = NextId + @IdsRequired

SELECT @EndOfBlockNextId = NextId FROM KeyTable

SELECT @NextId = @EndOfBlockNextId - @IdsRequired

SELECT @LocalisationId = [Id] FROM Localisation WHERE Culture = '**-**'

INSERT INTO LocalisationItem (Id, [Key], [Value], LocalisationId, ContextKey, Localised)
VALUES(@NextId, 'ApplicationStatusTypes.DidNotApply', 'Did not apply', @LocalisationId, '', 0)

SELECT @NextId = @NextId + 1

INSERT INTO LocalisationItem (Id, [Key], [Value], LocalisationId, ContextKey, Localised)
VALUES(@NextId, 'ApplicationStatusTypes.FailedToShow', 'Failed to show', @LocalisationId, '', 0)

SELECT @NextId = @NextId + 1

INSERT INTO LocalisationItem (Id, [Key], [Value], LocalisationId, ContextKey, Localised)
VALUES(@NextId, 'ApplicationStatusTypes.FoundJobFromOtherSource', 'Found job from other source', @LocalisationId, '', 0)

SELECT @NextId = @NextId + 1

INSERT INTO LocalisationItem (Id, [Key], [Value], LocalisationId, ContextKey, Localised)
VALUES(@NextId, 'ApplicationStatusTypes.Hired', 'Hired', @LocalisationId, '', 0)

SELECT @NextId = @NextId + 1

INSERT INTO LocalisationItem (Id, [Key], [Value], LocalisationId, ContextKey, Localised)
VALUES(@NextId, 'ApplicationStatusTypes.InterviewDenied', 'Interview denied', @LocalisationId, '', 0)

SELECT @NextId = @NextId + 1

INSERT INTO LocalisationItem (Id, [Key], [Value], LocalisationId, ContextKey, Localised)
VALUES(@NextId, 'ApplicationStatusTypes.Interviewed', 'Interviewed', @LocalisationId, '', 0)

SELECT @NextId = @NextId + 1

INSERT INTO LocalisationItem (Id, [Key], [Value], LocalisationId, ContextKey, Localised)
VALUES(@NextId, 'ApplicationStatusTypes.InterviewScheduled', 'Interview scheduled', @LocalisationId, '', 0)

SELECT @NextId = @NextId + 1

INSERT INTO LocalisationItem (Id, [Key], [Value], LocalisationId, ContextKey, Localised)
VALUES(@NextId, 'ApplicationStatusTypes.NewApplicant', 'New applicant', @LocalisationId, '', 0)

SELECT @NextId = @NextId + 1

INSERT INTO LocalisationItem (Id, [Key], [Value], LocalisationId, ContextKey, Localised)
VALUES(@NextId, 'ApplicationStatusTypes.NotApplicable', 'Not applicable', @LocalisationId, '', 0)

SELECT @NextId = @NextId + 1

INSERT INTO LocalisationItem (Id, [Key], [Value], LocalisationId, ContextKey, Localised)
VALUES(@NextId, 'ApplicationStatusTypes.NotHired', 'Not hired', @LocalisationId, '', 0)

SELECT @NextId = @NextId + 1

INSERT INTO LocalisationItem (Id, [Key], [Value], LocalisationId, ContextKey, Localised)
VALUES(@NextId, 'ApplicationStatusTypes.OfferMade', 'Offer made', @LocalisationId, '', 0)

SELECT @NextId = @NextId + 1

INSERT INTO LocalisationItem (Id, [Key], [Value], LocalisationId, ContextKey, Localised)
VALUES(@NextId, 'ApplicationStatusTypes.Recommended', 'Recommended', @LocalisationId, '', 0)

SELECT @NextId = @NextId + 1

INSERT INTO LocalisationItem (Id, [Key], [Value], LocalisationId, ContextKey, Localised)
VALUES(@NextId, 'ApplicationStatusTypes.RefusedOffer', 'Refused offer', @LocalisationId, '', 0)

SELECT @NextId = @NextId + 1

INSERT INTO LocalisationItem (Id, [Key], [Value], LocalisationId, ContextKey, Localised)
VALUES(@NextId, 'ApplicationStatusTypes.SelfReferred', 'Self referred', @LocalisationId, '', 0)

SELECT @NextId = @NextId + 1

INSERT INTO LocalisationItem (Id, [Key], [Value], LocalisationId, ContextKey, Localised)
VALUES(@NextId, 'ApplicationStatusTypes.UnderConsideration', 'Under consideration', @LocalisationId, '', 0)

SELECT @NextId = @NextId + 1

INSERT INTO LocalisationItem (Id, [Key], [Value], LocalisationId, ContextKey, Localised)
VALUES(@NextId, 'AlertMessage.JobReminder', 'Reminder for {0} (job order #{1}) - {2}', @LocalisationId, '', 0)

COMMIT TRANSACTION