DECLARE @LocalisationId bigint
SELECT @LocalisationId = [Id] FROM [Config.Localisation] WHERE Culture = '**-**'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Focus.CareerExplorer.WebExplorer.Explore.Explorer.Label.ResearchStudy.Education' AND LocalisationId = @LocalisationId
DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Focus.CareerExplorer.WebAuth.Controls.RegistrationConfirmation.ResearchStudyLabel.Text' AND LocalisationId = @LocalisationId

INSERT INTO [Config.LocalisationItem] ([Key], [Value], LocalisationId, ContextKey, Localised)
SELECT 'Focus.CareerExplorer.WebExplorer.Explore.Explorer.Label.ResearchStudy.Education', 'Research a program of study or skill', @LocalisationId, '', 0
INSERT INTO [Config.LocalisationItem] ([Key], [Value], LocalisationId, ContextKey, Localised)
SELECT 'Focus.CareerExplorer.WebAuth.Controls.RegistrationConfirmation.ResearchStudyLabel.Text', 'Research a program of study or skill', @LocalisationId, '', 0


/*
DECLARE @LocalisationId bigint
SELECT @LocalisationId = [Id] FROM [Config.Localisation] WHERE Culture = '**-**'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Focus.CareerExplorer.WebExplorer.Explore.Explorer.Label.ResearchStudy.Education' AND LocalisationId = @LocalisationId
DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Focus.CareerExplorer.WebAuth.Controls.RegistrationConfirmation.ResearchStudyLabel.Text' AND LocalisationId = @LocalisationId
 
*/