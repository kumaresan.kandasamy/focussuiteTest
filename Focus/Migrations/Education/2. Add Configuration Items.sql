BEGIN TRANSACTION

IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'DefaultStateKey')
BEGIN
	INSERT INTO [Config.ConfigurationItem] ([Key], [Value])
	VALUES ('DefaultStateKey', 'State.TX')
END
ELSE
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		Value = 'State.TX'
	WHERE
		[Key] = 'DefaultStateKey'
END

DECLARE @StateList NVARCHAR(MAX)

SELECT 
	@StateList = ISNULL(@StateList + ',', '') + LV.[Key] 
FROM 
	[Config.LookupItemsView] LV 
WHERE
	LV.LookupType = 'States'
	AND LV.[Key] NOT IN ('State.AE', 'State.AA', 'State.AP', 'State.AS', 'State.FM', 'State.MH', 'State.MP', 'State.ZZ', 'State.PW', 'State.GU', 'State.PR', 'State.VI')
	
IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'NearbyStateKeys')
BEGIN
	INSERT INTO [Config.ConfigurationItem] ([Key], [Value])
	VALUES ('NearbyStateKeys', @StateList)
END
ELSE
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		Value = @StateList
	WHERE
		[Key] = 'NearbyStateKeys'
END

COMMIT TRANSACTION