IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 22)
BEGIN
	BEGIN TRANSACTION

	INSERT INTO [Config.EmailTemplate] 
	(
		EmailTemplateType, 
		[Subject], 
		Body, 
		Salutation, 
		Recipient
	)
	VALUES 
	(
		22, 
		'Sent from Focus/Career - New jobs matching your search', 
		'Your new job matches for #SAVEDSEARCHNAME# saved search.

#MATCHEDJOBS#

You have received this email because an alert has been set up in Focus/Career to send alerts to this email address.
', 
		'Dear #RECIPIENTNAME#', 
		NULL
	)

	COMMIT TRANSACTION
END

IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 23)
BEGIN
	BEGIN TRANSACTION

	INSERT INTO [Config.EmailTemplate] 
	(
		EmailTemplateType, 
		[Subject], 
		Body, 
		Salutation, 
		Recipient
	)
	VALUES 
	(
		23, 
		'New Password for #FOCUSCAREER#', 
		'This email is being sent to you because you forgot your password. The following new password has been generated for you:

Email Address: #EMAILADDRESS#
Password: #PASSWORD#

With this information, you should have no problem accessing your account. We do encourage you to come back often to #FOCUSCAREER# to manage your resume and job search.
We wish you luck and look forward to helping you find your next job!

#FOCUSCAREERURL#
', 
		'Dear #RECIPIENTNAME#', 
		NULL
	)

	COMMIT TRANSACTION
END

IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 26)
BEGIN
	BEGIN TRANSACTION

	INSERT INTO [Config.EmailTemplate] 
	(
		EmailTemplateType, 
		[Subject], 
		Body, 
		Salutation, 
		Recipient
	)
	VALUES 
	(
		26, 
		'Invitation to Register with #FOCUSCAREER#', 
		'Welcome!

You are invited to register with #FOCUSCAREER#�. #FOCUSCAREER# matches students with jobs based on career pathways of people with similar skills � not just keywords.

To join #FOCUSCAREER#, please click on the link below to authenticate your email address.
#AUTHENTICATEURL#

Your unique PIN number is: #PINNUMBER#

This PIN is used as an initial method of identifying your account. After confirming your email address, you will be asked to complete an online registration, including setting a password for future access to #FOCUSCAREER#. 

If you encounter any problems, please contact our help desk staff at #SUPPORTPHONE# or email us at #SUPPORTEMAIL#.
', 
		'Dear #RECIPIENTNAME#', 
		NULL
	)

	COMMIT TRANSACTION
END