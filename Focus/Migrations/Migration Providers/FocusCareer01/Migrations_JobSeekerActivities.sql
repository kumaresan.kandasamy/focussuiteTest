IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_JobSeekerActivities]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_JobSeekerActivities]
GO

CREATE PROCEDURE [dbo].[Migrations_JobSeekerActivities]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@EOS VARCHAR(30) = NULL,
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.Activities
	
--IF @CutOffDate IS NULL
--	SET @CutOffDate = DATEADD(YEAR, -1, GETDATE())
	
SELECT TOP (@BatchSize) 
	A.ActivityId,
	A.CustomerActivityId,
	A.ActivityTime,
	CAST(A.ActivityId AS VARCHAR(10)) AS MigrationId,
	CAST(A.JobseekerId AS VARCHAR(10)) AS JobSeekerMigrationId
FROM 
	dbo.Activities A
INNER JOIN dbo.JobSeeker JS
	ON JS.JobSeekerId = A.JobSeekerId
INNER JOIN dbo.CustomerRepDetails CRD
	ON CRD.CustomerRepId = JS.CustomerRepId
INNER JOIN dbo.Customer C
	ON C.CustomerID = CRD.CustomerID
WHERE
	C.[name] = @CustomerId
	AND A.ActivityId > @LastId
	AND JS.EmailAddress LIKE '%@%'
	AND (@CutOffDate IS NULL OR JS.LastLogin > @CutOffDate)
	AND (@CutOffDate IS NULL OR A.activitytime > @CutOffDate)
ORDER BY
	A.ActivityId
GO

--GRANT EXEC ON [Migrations_JobSeekerActivities] TO [FocusAgent]
