IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_JobOrderRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_JobOrderRequests]
GO

CREATE PROCEDURE [Migrations_JobOrderRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.Posting
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())

SELECT TOP (@BatchSize)
	P.PostingID,
	P.EmployerId,
	ISNULL(P.[status], 0) AS [Status],
	I.JobTitle,
	I.[Description],
	P.DateCreated AS CreatedOn,
	P.LastModified AS UpdatedOn,
	P.DateCreated AS PostedOn,
	I.CloseDate AS ClosingOn,
	I.CloseDate AS ClosedOn,
	I.Positions AS NumberOfOpenings,
	P.LastModified AS ApprovedOn,
	I.[Address] AS JobAddressLine1,
	I.City AS JobAddressTownCity,
	I.[State] AS JobAddressState,
	I.zipcode AS JobAddressPostcodeZip,
	CASE
		WHEN I.City = E.City AND I.[State] = E.[State] AND I.ZipCode = E.postalcode THEN 1
		ELSE 0
	END AS IsMainSite,
	TP.Posting AS JobXml,
	J.LensJobId,
	P.PostingId AS ExternalId,
	P.EmployerRepId AS EmployeeMigrationId,
	P.PostingId AS JobMigrationId
FROM
	dbo.Posting P
INNER JOIN dbo.PostingInfo I
	ON I.PostingID = P.PostingID
INNER JOIN dbo.Employer E
	ON E.EmployerID = P.EmployerID
INNER JOIN dbo.Customer C
	ON C.CustomerID = E.CustomerId
INNER JOIN dbo.TaggedPostings TP
	ON TP.PostingId = P.PostingId
INNER JOIN dbo.Jobs J
	ON J.CustomerJobId = P.EOSRegistrationId
WHERE
	C.[name] = @CustomerId
	AND P.PostingId > @LastId
	AND P.[status] <> 1791 -- DELETED
	AND P.LastModified > @CutOffDate
ORDER BY
	P.PostingId
GO

--GRANT EXEC ON [Migrations_JobOrderRequests] TO [FocusAgent]
