IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_Applications]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_Applications]
GO

CREATE PROCEDURE [dbo].[Migrations_Applications]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.Applicants
	
--IF @CutOffDate IS NULL
--	SET @CutOffDate = DATEADD(YEAR, -1, GETDATE())

SELECT TOP (@BatchSize) 
	A.ApplicantId,
	A.DateApplied,
	A.Score,
	A.[Status],
	A.StaffId,
	CAST(A.ApplicantId AS VARCHAR(10)) AS MigrationId,
	CAST(A.JobseekerId AS VARCHAR(10)) AS JobSeekerMigrationId,
	CAST(A.PostingId AS VARCHAR(10)) AS JobOrderMigrationId
FROM 
	dbo.Applicants A
INNER JOIN dbo.JobSeeker JS
	ON JS.JobSeekerId = A.JobSeekerId
INNER JOIN dbo.CustomerRepDetails CRD
	ON CRD.CustomerRepId = JS.CustomerRepId
INNER JOIN dbo.Customer C
	ON C.CustomerID = CRD.CustomerID
WHERE
	C.[name] = @CustomerId
	AND A.ApplicantId > @LastId
	AND JS.EmailAddress LIKE '%@%'
	AND (@CutOffDate IS NULL OR JS.LastLogin > @CutOffDate)
ORDER BY
	A.ApplicantId
GO

--GRANT EXEC ON [Migrations_Applications] TO [FocusAgent]
