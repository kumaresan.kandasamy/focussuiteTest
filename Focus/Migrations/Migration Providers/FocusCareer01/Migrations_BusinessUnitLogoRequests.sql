IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_BusinessUnitLogoRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_BusinessUnitLogoRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_BusinessUnitLogoRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30)
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.EmployerLogos

SELECT
	TL.LogoId,
	TL.[Name],
	EL.Logo,
    CAST(TL.LogoId AS VARCHAR(10)) AS MigrationId,
	CAST(TL.EmployeeMigrationId AS VARCHAR(10)) AS EmployeeMigrationId
FROM
(
	SELECT TOP (@BatchSize)
		EL.LogoId,
		EL.LogoName AS [Name],
		MIN(ERD.EmployerRepId) AS EmployeeMigrationId
	FROM
		dbo.EmployerLogos EL
	INNER JOIN dbo.Employer E
		ON E.EmployerID = EL.EmployerID
	INNER JOIN dbo.Customer C
		ON C.CustomerID = E.CustomerId
	INNER JOIN dbo.EmployerRepDetails ERD
		ON ERD.EmployerId = E.EmployerId
	WHERE
		C.[name] = @CustomerId
		AND EL.LogoId > @LastId
	GROUP BY
		EL.LogoId,
		EL.LogoName
) TL
	INNER JOIN dbo.EmployerLogos EL
		ON EL.LogoId = TL.LogoId
ORDER BY
	EL.LogoId

GO

--GRANT EXEC ON [Migrations_BusinessUnitLogoRequests] TO [FocusAgent]


