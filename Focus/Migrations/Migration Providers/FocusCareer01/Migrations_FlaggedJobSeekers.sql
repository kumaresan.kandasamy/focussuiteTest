IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_FlaggedJobSeekers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_FlaggedJobSeekers]
GO

CREATE PROCEDURE [dbo].[Migrations_FlaggedJobSeekers]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.ApplicantData
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -1, GETDATE())

SELECT TOP (@BatchSize) 
	AD.ApplicantId,
	CAST(AD.ApplicantId AS VARCHAR(10)) AS MigrationId,
	CAST(AD.JobseekerId AS VARCHAR(10)) AS JobSeekerMigrationId,
	CAST(ERD.EmployerRepId AS VARCHAR(10)) AS EmployeeeMigrationId
FROM 
	dbo.ApplicantData AD
INNER JOIN dbo.JobSeeker JS
	ON JS.JobSeekerId = AD.JobSeekerId
INNER JOIN dbo.EmployerRepDetails ERD
	ON ERD.EmployerRepId = AD.EmployerRepId
INNER JOIN dbo.Employer E
	ON E.EmployerId = ERD.EmployerId
INNER JOIN dbo.Customer C
	ON C.CustomerID = E.CustomerId	
WHERE
	C.[name] = @CustomerId
	AND AD.ApplicantId > @LastId
	AND JS.EmailAddress LIKE '%@%'
	AND JS.LastLogin > @CutOffDate
	AND AD.Flagged = 1
ORDER BY
	AD.ApplicantId
GO

--GRANT EXEC ON [Migrations_FlaggedJobSeekers] TO [FocusAgent]
