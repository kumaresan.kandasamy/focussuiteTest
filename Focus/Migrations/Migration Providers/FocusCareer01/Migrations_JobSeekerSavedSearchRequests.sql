IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_JobSeekerSavedSearchRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_JobSeekerSavedSearchRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_JobSeekerSavedSearchRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.savedsearches
	
--IF @CutOffDate IS NULL
--	SET @CutOffDate = DATEADD(YEAR, -1, GETDATE())

SELECT TOP (@BatchSize)
	ss.searchname AS [Name],
	ssd.searchcriteria AS CriteriaXml,
	CAST(SS.savedsearchid AS VARCHAR(10)) AS MigrationId,
	CAST(SS.jobseekerid AS VARCHAR(10)) AS JobSeekerMigrationId,
	SS.savedsearchid AS SavedSearchId
FROM
	dbo.savedsearches SS
INNER JOIN dbo.savedsearchdetails SSD
	ON SSD.savedsearchid = SS.savedsearchid
INNER JOIN dbo.jobseeker JS
	ON JS.JobSeekerId = SS.JobSeekerId
INNER JOIN CustomerRepDetails CRD
	ON CRD.CustomerRepId = JS.CustomerRepId
INNER JOIN Customer C
	ON C.CustomerID = CRD.CustomerID
WHERE
	C.Name = @CustomerId
	AND SS.savedsearchid > @LastId
	AND JS.EmailAddress LIKE '%@%'
	AND (@CutOffDate IS NULL OR JS.LastLogin > @CutOffDate)
	AND ss.searchname <> 'JOB LEADS'
ORDER BY
	SS.savedsearchid
GO

--GRANT EXEC ON [Migrations_JobSeekerSavedSearchRequests] TO [FocusAgent]
