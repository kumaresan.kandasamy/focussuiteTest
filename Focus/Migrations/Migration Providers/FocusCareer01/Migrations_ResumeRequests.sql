IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_ResumeRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_ResumeRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_ResumeRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.[Resume]
	
--IF @CutOffDate IS NULL
--	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())

SELECT TOP (@BatchSize)
	R.registereddate AS ResumeDate,
	JS.emailaddress AS EmailAddress,
	'Resume' + ISNULL(BR.fileextension, '.html') AS ResumeName,
	ISNULL(BR.FileExtension, '') AS FileExtension,
	BR.[resume] AS ResumeData,
	TR.[resume] AS TaggedResume,
	CAST(R.resumeid AS VARCHAR(10)) AS MigrationId,
	CAST(R.jobseekerid AS VARCHAR(10)) AS JobSeekerMigrationId,
	R.resumeid As ResumeId
FROM
	dbo.[Resume] R 
INNER JOIN dbo.taggedresumes TR
	ON TR.resumeid = R.resumeid
INNER JOIN dbo.jobseeker JS
	ON JS.JobSeekerID = R.JobSeekerId
INNER JOIN CustomerRepDetails CRD
	ON CRD.CustomerRepId = JS.CustomerRepId
INNER JOIN Customer C
	ON C.CustomerID = CRD.CustomerID
LEFT OUTER JOIN dbo.binaryresumes BR
	ON BR.jobseekerid = R.jobseekerid
WHERE
	C.Name = @CustomerId
	AND R.ResumeId > @LastId
	AND R.active = 1
	AND DATALENGTH(TR.[Resume]) <> 0
	AND JS.EmailAddress LIKE '%@%'
	AND (@CutOffDate IS NULL OR JS.LastLogin > @CutOffDate)
ORDER BY
	R.ResumeId
GO

--GRANT EXEC ON [Migrations_ResumeRequests] TO [FocusAgent]