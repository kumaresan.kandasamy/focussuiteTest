IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_EmployerRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_EmployerRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_EmployerRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.EmployerRepDetails
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())

SELECT TOP (@BatchSize)
	ERD.EmployerRepId,
	U.UserName AS UserName,
	ERD.PersonalTitle AS EmployeePersonTitle,
	ERD.FirstName AS EmployeePersonFirstName,
	ISNULL(LEFT(ERD.MiddleName, 1), '') AS EmployeePersonMiddleInitial,
	ERD.LastName AS EmployeePersonLastName,
	ISNULL(ERD.Designation, '') AS EmployeePersonJobTitle,
	ERD.Street1 AS EmployeeAddressLine1,
	ISNULL(ERD.Street2, '') AS EmployeeAddressLine2,
	ERD.City AS EmployeeAddressTownCity,
	ISNULL(ERD.County, '') AS EmployeeAddressCounty,
	ERD.[State] AS EmployeeAddressState,
	ERD.Country  AS EmployeeAddressCountry,
	ERD.PostalCode AS EmployeeAddressPostcodeZip,
	SUBSTRING(ERD.Phone1, 2, 20) AS EmployeePrimaryPhone,
	LEFT(ERD.Phone1, 1) AS EmployeePrimaryPhoneType,
	SUBSTRING(ERD.Phone2, 2, 20) AS EmployeeAlternatePhone1,
	LEFT(ERD.Phone2, 1) AS EmployeeAlternatePhoneType1,
	SUBSTRING(ERD.Phone3, 2, 20) AS EmployeeAlternatePhone2,
	LEFT(ERD.Phone3, 1) AS EmployeeAlternatePhoneType2,
	ERD.EmailAddress AS EmployeePersonEmailAddress,
	ERD.TimeCreated AS EmployeeCreatedOn,
	ERD.LastModified AS EmployeeUpdatedOn,
	E.LegalName AS EmployerName,
	ISNULL(ED.DescriptionId, 0 - E.employerid) AS EmployerDescriptionId,
	ISNULL(ED.[Description], '') AS EmployerDescription,
	ISNULL(ED.DescriptionName, 'Company Description') AS EmployerDescriptionTitle,
	E.[Status] AS EmployerStatus,
	E.Fein AS FederalEmployerIdentificationNumber,
	E.Sein AS StateEmployerIdentificationNumber,
	E.Url AS EmployerUrl,
	E.OwnershipType AS EmployerOwnershipType,
	SUBSTRING(E.Phone1, 2, 20) AS EmployerPrimaryPhone,
	LEFT(E.Phone1, 1) AS EmployerPrimaryPhoneType,
	SUBSTRING(E.Phone2, 2, 20) AS EmployerAlternatePhone1,
	LEFT(E.Phone2, 1) AS EmployerAlternatePhoneType1,
	SUBSTRING(E.Phone3, 2, 20) AS EmployerAlternatePhone2,
	LEFT(E.Phone3, 1) AS EmployerAlternatePhoneType2,
	E.IndustryClass AS EmployerIndustrialClassification,
	E.TimeCreated AS EmployerCreatedOn,
	E.LastModified AS EmployerUpdatedOn,
	E.Street1 AS EmployerAddressLine1,
	ISNULL(E.Street2, '') AS EmployerAddressLine2,
	E.City AS EmployerAddressTownCity,
	ISNULL(E.County, '') AS EmployerAddressCounty,
	E.[State] AS EmployerAddressState,
	E.Country  AS EmployerAddressCountry,
	E.PostalCode AS EmployerAddressPostcodeZip,
	E.EmployerId AS EmployerExternalId,
	CAST(ERD.EmployerRepId AS VARCHAR(10)) AS MigrationId
FROM
	dbo.Employer E
INNER JOIN dbo.Customer C
	ON C.CustomerID = E.CustomerId	
INNER JOIN dbo.EmployerRepDetails ERD
	ON ERD.EmployerID = E.EmployerID
INNER JOIN dbo.Users U
	ON U.UserID = ERD.UserID
LEFT OUTER JOIN dbo.EmployerDescriptions ED
	ON ED.EmployerID = E.EmployerID
	AND ED.IsDefault = 1
	AND DATALENGTH(ED.[Description]) > 0
WHERE
	C.[name] = @CustomerId
	AND ERD.EmployerRepId > @LastId
	AND (E.LastModified > @CutOffDate OR ERD.LastModified > @CutOffDate)
ORDER BY
	ERD.EmployerRepId
GO

--GRANT EXEC ON [Migrations_EmployerRequests] TO [FocusAgent]
