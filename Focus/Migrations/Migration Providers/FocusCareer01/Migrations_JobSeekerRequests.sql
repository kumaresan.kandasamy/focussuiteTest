IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_JobSeekerRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_JobSeekerRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_JobSeekerRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.JobSeeker
	
--IF @CutOffDate IS NULL
--	SET @CutOffDate = DATEADD(YEAR, -1, GETDATE())

SELECT TOP (@BatchSize)
	JS.username AS UserName,
	JS.emailaddress AS EmailAddress,
	ISNULL(JS.SSN, '') AS SSN,
	ISNULL(JS.securityquestion, '') AS SecurityQuestion,
	ISNULL(JS.securityanswer, '') AS SecurityAnswer,
	ISNULL(JS.Reserved02, '') AS ExtraInfo,
	JS.lastlogin,
/*
	ISNULL(RI.firstname, '') AS FirstName,
	ISNULL(RI.middlename, '') AS MiddleInitial,
	ISNULL(RI.lastname, '') AS LastName,
	RI.dob AS DateOfBirth,
	ISNULL(RI.street, '') AS AddressLine,
	'' AS AddressLine2,
	ISNULL(RI.city, '') AS AddressCity,
	ISNULL(RI.state, '') AS AddressState,
	ISNULL(RI.country, '') AS AddressCountry,
	'' AS AddressCounty,
	ISNULL(RI.zipcode, '') AS AddressZip,
	ISNULL(RI.homephone, '') AS HomePhone,
	ISNULL(RI.mobilephone, '') AS MobilePhone,
*/
	TR.[Resume] AS [Resume],
	CAST(JS.jobseekerid AS VARCHAR(10)) AS MigrationId,
	JS.jobseekerid AS JobSeekerId
FROM
	dbo.JobSeeker JS --WITH (INDEX (UQ__jobseeke__1629975214E61A24))
INNER JOIN CustomerRepDetails CRD
	ON CRD.CustomerRepId = JS.CustomerRepId
INNER JOIN Customer C
	ON C.CustomerID = CRD.CustomerID
LEFT OUTER JOIN
( 
	dbo.[Resume] R --WITH (INDEX (IX_resume_jobseekerid))
	INNER JOIN dbo.TaggedResumes TR
		ON TR.ResumeId = R.ResumeId
)
	ON R.JobSeekerId = JS.JobSeekerId
/*
LEFT OUTER JOIN dbo.ResumeInfo RI
	ON RI.ResumeId = R.ResumeId
*/
WHERE
	C.Name = @CustomerId
	AND JS.jobseekerid > @LastId
	AND JS.EmailAddress LIKE '%@%'
	AND (@CutOffDate IS NULL OR JS.LastLogin > @CutOffDate)
ORDER BY
	JS.jobseekerid
GO

--GRANT EXEC ON [dbo].[Migrations_JobSeekerRequests] TO [FocusAgent]