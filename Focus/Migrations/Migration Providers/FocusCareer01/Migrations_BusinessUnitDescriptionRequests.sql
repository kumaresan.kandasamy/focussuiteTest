IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_BusinessUnitDescriptionRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_BusinessUnitDescriptionRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_BusinessUnitDescriptionRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30)
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.EmployerDescriptions

SELECT TOP (@BatchSize)
	ED.DescriptionId,
	ED.DescriptionName AS Title,
    CAST(ED.[Description] AS VARCHAR(MAX)) AS [Description],
    CAST(0 AS BIT) AS IsPrimary,
    CAST(ED.DescriptionId AS VARCHAR(10)) AS MigrationId,
	CAST(MIN(ERD.EmployerRepId) AS VARCHAR(10)) AS EmployeeMigrationId
FROM
	dbo.EmployerDescriptions ED
INNER JOIN dbo.Employer E
	ON E.EmployerID = ED.EmployerID
INNER JOIN dbo.Customer C
	ON C.CustomerID = E.CustomerId
INNER JOIN dbo.EmployerRepDetails ERD
	ON ERD.EmployerId = E.EmployerId
WHERE
	C.[name] = @CustomerId
	AND ED.IsDefault = 0
	AND DATALENGTH(ED.[Description]) > 0
	AND ED.DescriptionId > @LastId
GROUP BY
	ED.DescriptionId,
	ED.DescriptionName,
	CAST(ED.[Description] AS VARCHAR(MAX))
ORDER BY
	ED.DescriptionId
GO

--GRANT EXEC ON [Migrations_BusinessUnitDescriptionRequests] TO [FocusAgent]

