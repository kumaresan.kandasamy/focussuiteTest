IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_ViewedPostingRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_ViewedPostingRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_ViewedPostingRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.jobsviewed

IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -1, GETDATE())
	
SELECT TOP (@BatchSize)
	JV.ViewedTime AS ViewedDate,
	J.lensjobid AS LensPostingId,
	CAST(JV.viewedjobid AS VARCHAR(10)) AS MigrationId,
	CAST(JV.jobseekerid AS VARCHAR(10)) AS JobSeekerMigrationId,
	JV.viewedjobid AS JobsViewedId
FROM
	dbo.recentlyviewedjobs JV
INNER JOIN dbo.jobs J
	ON J.focusjobid = JV.focusjobid
INNER JOIN dbo.jobseeker JS
	ON JS.JobSeekerId = JV.JobSeekerId
INNER JOIN CustomerRepDetails CRD
	ON CRD.CustomerRepId = JS.CustomerRepId
INNER JOIN Customer C
	ON C.CustomerID = CRD.CustomerID
WHERE
	C.Name = @CustomerId
	AND JV.viewedjobid > @LastId
	AND JS.EmailAddress LIKE '%@%'
	AND JS.LastLogin > @CutOffDate
	AND JV.viewedjobid IN
	(
		SELECT TOP 5
			JV2.viewedjobid
		FROM 
			dbo.recentlyviewedjobs JV2
		WHERE 
			JV2.jobseekerid = JV.jobseekerid
		ORDER BY 
			JV2.ViewedTime DESC
	)
ORDER BY
	JV.viewedjobid
GO

--GRANT EXEC ON [Migrations_ViewedPostingRequests] TO [FocusAgent]