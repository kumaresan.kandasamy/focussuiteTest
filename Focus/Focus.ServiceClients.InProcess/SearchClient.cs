﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.Criteria.Onet;
using Focus.Core.Criteria.OnetTask;
using Focus.Core.Criteria.SavedSearch;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Messages.SearchService;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Focus.ServiceClients.InProcess.Extensions;
using Focus.ServiceClients.Interfaces;
using Focus.Web.Core.Models;
using Framework.Core;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.InProcess
{
	public class SearchClient : ClientBase, ISearchClient
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SearchClient" /> class.
		/// </summary>
		/// <param name="appContext">The application context.</param>
		/// <param name="runtime">The runtime.</param>
		public SearchClient(AppContextModel appContext, ServiceClientRuntime runtime) : base(appContext, runtime) { }
		
		/// <summary>
		/// Searches the candidates.
		/// </summary>
		/// <param name="searchId">The search id.</param>
		/// <param name="searchCriteria">The search criteria.</param>
		/// <returns></returns>
		public CandidateSearchResultModel SearchCandidates(Guid searchId, CandidateSearchCriteria searchCriteria)
		{
			var request = new CandidateSearchRequest().Prepare(AppContext, Runtime.Settings);
			
			request.SearchId = searchId;
			request.Criteria = searchCriteria;

			if (Runtime.Settings.Module == FocusModules.Assist)
				request.Criteria.MinimumDocumentCount = (searchCriteria.SearchType == CandidateSearchTypes.CandidatesLikeThis) ? 4 : Runtime.Settings.TalentPoolSearchMaximumRecordsToReturn;

			if (request.Criteria.MinimumDocumentCount == 0)
				request.Criteria.MinimumDocumentCount = Runtime.Settings.TalentPoolSearchMaximumRecordsToReturn;
			
			var response = SearchService.SearchCandidates(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return new CandidateSearchResultModel { SearchId = response.SearchId, Candidates = response.Candidates, HighestStarRating = response.HighestStarRating };			
		}

		/// <summary>
		/// Gets the candidate saved search.
		/// </summary>
		/// <param name="searchId">The search id.</param>
		/// <returns></returns>
		public CandidateSavedSearchView GetCandidateSavedSearch(long searchId)
		{
			var request = new GetCandidateSavedSearchRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new SavedSearchCriteria { SavedSearchId = searchId, FetchOption = CriteriaBase.FetchOptions.Single};

      var response = SearchService.GetCandidateSavedSearch(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return new CandidateSavedSearchView
			       	{
			       		SavedSearch = response.SavedSearch,
			       		Criteria = response.Criteria
			       	};
		}

		/// <summary>
		/// Gets the candidate saved search.
		/// </summary>
		/// <returns></returns>
		public PagedList<SavedSearchDto> GetCandidateSavedSearch(SavedSearchCriteria criteria)
		{
			var request = new GetCandidateSavedSearchRequest().Prepare(AppContext, Runtime.Settings);

		  request.Criteria = new SavedSearchCriteria
		                       {
		                         UserId = AppContext.User.UserId,
		                         FetchOption = CriteriaBase.FetchOptions.PagedList,
		                         PageIndex = criteria.PageIndex,
		                         PageSize = criteria.PageSize
		                       };

		  var response = SearchService.GetCandidateSavedSearch(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

		  return response.SavedSearchesPaged;
		}

    /// <summary>
    /// Deletes candidate saved search.
    /// </summary>
    /// <returns></returns>
    public void DeleteCandidateSavedSearch(long searchId)
    {
      var request = new DeleteSavedSearchRequest().Prepare(AppContext, Runtime.Settings);
      request.SavedSearchId = searchId;

      var response = SearchService.DeleteSavedSearch(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);
    }

		/// <summary>
		/// Gets the candidate saved search view.
		/// </summary>
		/// <returns></returns>
		public List<SavedSearchView> GetCandidateSavedSearchView()
		{
			var request = new GetCandidateSavedSearchViewRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new SavedSearchCriteria { UserId = AppContext.User.UserId };

			var response = SearchService.GetCandidateSavedSearchView(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.SavedSearches;
		}

		/// <summary>
		/// Saves the candidate search.
		/// </summary>
		/// <param name="searchCriteria">The search criteria.</param>
		/// <param name="id">The id.</param>
		/// <param name="name">The name.</param>
		/// <param name="alertEmailRequired">if set to <c>true</c> [alert email required].</param>
		/// <param name="alertEmailFrequency">The alert email frequency.</param>
		/// <param name="alertEmailFormat">The alert email format.</param>
		/// <param name="alertEmailAddress">The alert email address.</param>
		public void SaveCandidateSearch(CandidateSearchCriteria searchCriteria, long id, string name, bool alertEmailRequired, 
																		EmailAlertFrequencies alertEmailFrequency, EmailFormats alertEmailFormat, string alertEmailAddress)
		{
			var serviceRequest = new SaveCandidateSavedSearchRequest().Prepare(AppContext, Runtime.Settings);
			serviceRequest.Criteria = searchCriteria;
			serviceRequest.Id = id;
			serviceRequest.Name = name;
			serviceRequest.AlertEmailRequired = alertEmailRequired;
			serviceRequest.AlertEmailFrequency = alertEmailFrequency;
			serviceRequest.AlertEmailFormat = alertEmailFormat;
			serviceRequest.AlertEmailAddress = alertEmailAddress;

			switch (Runtime.Settings.Module)
			{
				case FocusModules.Talent:
					serviceRequest.SearchType = SavedSearchTypes.TalentCandidateSearch;
					break;
				case FocusModules.Assist:
					serviceRequest.SearchType = SavedSearchTypes.AssistCandidateSearch;
					break;
				default:
					throw new Exception("Module not supported");
			}
			
			var response = SearchService.SaveCandidateSearch(serviceRequest);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);
		}

		/// <summary>
		/// Extracts the job description.
		/// </summary>
		/// <param name="binaryFileData">The binary file data.</param>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="contentType">Type of the content.</param>
		/// <returns></returns>
		public string ExtractJobDescription(byte[] binaryFileData, string fileName, string contentType)
		{
			var request = new ConvertBinaryDataRequest().Prepare(AppContext, Runtime.Settings);
			//request.SearchServer = App.Settings.SearchServerDetails;
			request.BinaryFileData = binaryFileData;
			request.FileName = fileName;
			request.ContentType = contentType;

      var response = SearchService.ConvertBinaryDataToText(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Text;
		}

		/// <summary>
		/// Clarifies the text.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="minScore">The min score.</param>
		/// <param name="maxSearchCount">The max search count.</param>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		public List<string> ClarifyText(string text, int minScore, int maxSearchCount, string type)
		{
			var request = new ClarifyWordRequest().Prepare(AppContext, Runtime.Settings);
			//request.SearchServer = App.Settings.SearchServerDetails;
			request.Text = text;

			var response = SearchService.GetClarifiedWords(request, minScore, maxSearchCount, type);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.ClarifiedWords;
		}

    /// <summary>
    /// Gets the onets.
    /// </summary>
    /// <param name="jobTasksOnly">Whether to only get onets with job tasks available.</param>
    /// <param name="listSize">The number to get.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public List<OnetDetailsView> GetOnets(bool jobTasksOnly, int? listSize = null)
    {
      var request = new OnetRequest().Prepare(AppContext, Runtime.Settings);
      //request.SearchServer = App.Settings.SearchServerDetails;
      request.Criteria = new OnetCriteria { OnetsWithJobTasks = jobTasksOnly, ListSize = listSize.GetValueOrDefault(int.MaxValue) };

      var response = SearchService.GetOnets(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.Onets;
    }
		
		/// <summary>
		/// Gets the onets.
		/// </summary>
		/// <param name="jobFamilyId">The job family id.</param>
		/// <returns></returns>
		public List<OnetDetailsView> GetOnets(long jobFamilyId)
		{
			var request = new OnetRequest().Prepare(AppContext, Runtime.Settings);
			//request.SearchServer = App.Settings.SearchServerDetails;
			request.Criteria = new OnetCriteria { JobFamilyId = jobFamilyId };

			var response = SearchService.GetOnets(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Onets;
		}


    /// <summary>
    /// Gets the R onet code by id.
    /// </summary>
    /// <param name="ronetId">The ronet id.</param>
    /// <returns></returns>
    public string GetROnetCodeById(long ronetId)
    {
      return SearchService.GetROnetCodeById(ronetId);
    }

		/// <summary>
		/// Gets the onets.
		/// </summary>
		/// <param name="jobTitle">The job title.</param>
		/// <param name="listSize">Size of the list.</param>
		/// <returns></returns>
		public List<OnetDetailsView> GetOnets(string jobTitle, int listSize)
		{
			var request = new OnetRequest().Prepare(AppContext, Runtime.Settings);
			//request.SearchServer = App.Settings.SearchServerDetails;
			request.Criteria = new OnetCriteria { JobTitle = jobTitle, ListSize = listSize, FetchOption = CriteriaBase.FetchOptions.List };

			var response = SearchService.GetOnets(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Onets;
		}

    /// <summary>
    /// Gets the onets.
    /// </summary>
    /// <param name="jobTitle">The job title.</param>
    /// <param name="listSize">Size of the list.</param>
    /// <returns></returns>
    public List<ROnetDetailsView> GetROnets(string jobTitle, int listSize)
    {
      var request = new ROnetRequest().Prepare(AppContext, Runtime.Settings);
      //request.SearchServer = App.Settings.SearchServerDetails;
      request.Criteria = new OnetCriteria { JobTitle = jobTitle, ListSize = listSize, FetchOption = CriteriaBase.FetchOptions.List };

      var response = SearchService.GetROnets(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ROnets;
    }

	  /// <summary>
	  /// Gets the onet for an ronet.
	  /// </summary>
	  /// <returns></returns>
	  public long? GetROnetOnet(long rOnetId)
    {
      var request = new OnetRequest().Prepare(AppContext, Runtime.Settings);
      //request.SearchServer = App.Settings.SearchServerDetails;
      request.Criteria = new OnetCriteria {ROnetId = rOnetId,FetchOption = CriteriaBase.FetchOptions.List };

      var response = SearchService.GetOnet(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.OnetId;
    }

    /// <summary>
    /// Gets the ronetId for an ronet code.
    /// </summary>
    /// <returns></returns>
    public long? GetROnetByCode(string ronetCode)
    {
      var request = new ROnetRequest().Prepare(AppContext, Runtime.Settings);
     
      request.ROnetCriteria = new Core.Criteria.ROnet.ROnetCodeCriteria { ROnetCode = ronetCode};

      var response = SearchService.GetROnet(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ROnetId;
    }

		/// <summary>
		/// Gets the onets.
		/// </summary>
		/// <param name="onetId">The onet id.</param>
		/// <param name="jobTitle">The job title.</param>
		/// <param name="listSize">Size of the list.</param>
		/// <returns></returns>
		public List<OnetDetailsView> GetOnets(long onetId, string jobTitle, int listSize)
		{
			var request = new OnetRequest().Prepare(AppContext, Runtime.Settings);
			//request.SearchServer = App.Settings.SearchServerDetails;
			request.Criteria = new OnetCriteria { OnetId = onetId, JobTitle = jobTitle, ListSize = listSize, FetchOption = CriteriaBase.FetchOptions.List };	

			var response = SearchService.GetOnets(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Onets;
		}

    /// <summary>
    /// Gets the onets.
    /// </summary>
    /// <param name="onetId">The onet id.</param>
    /// <param name="jobTitle">The job title.</param>
    /// <param name="listSize">Size of the list.</param>
    /// <returns></returns>
    public List<ROnetDetailsView> GetROnets(long onetId, string jobTitle, int listSize)
    {
      var request = new ROnetRequest().Prepare(AppContext, Runtime.Settings);
      //request.SearchServer = App.Settings.SearchServerDetails;
      request.Criteria = new OnetCriteria { OnetId = onetId, JobTitle = jobTitle, ListSize = listSize, FetchOption = CriteriaBase.FetchOptions.List };

      var response = SearchService.GetROnets(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ROnets;
    }

	  /// <summary>
	  /// Gets the onet.
	  /// </summary>
	  /// <param name="onetId"></param>
	  /// <returns></returns>
	  public long? GetOnet(long onetId)
		{
			var request = new OnetRequest().Prepare(AppContext, Runtime.Settings);
			//request.SearchServer = App.Settings.SearchServerDetails;
			request.Criteria = new OnetCriteria { OnetId = onetId, ListSize = 1, FetchOption = CriteriaBase.FetchOptions.Single };

			var response = SearchService.GetOnet(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

	    return response.OnetId;
		}

    /// <summary>
    /// Gets the onet for an ronet.
    /// </summary>
    /// <returns></returns>
    public long? GetOnetByROnet(long rOnetId)
    {
      var request = new OnetRequest().Prepare(AppContext, Runtime.Settings);
      //request.SearchServer = App.Settings.SearchServerDetails;
      request.Criteria = new OnetCriteria { ROnetId = rOnetId, FetchOption = CriteriaBase.FetchOptions.List };

      var response = SearchService.GetOnet(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.OnetId;
    }

    /// <summary>
    /// Gets the onet for an ronet.
    /// </summary>
    /// <returns></returns>
    public long? GetROnetByOnet(long onetId)
    {
      var request = new ROnetRequest().Prepare(AppContext, Runtime.Settings);
      
      request.Criteria = new OnetCriteria { OnetId = onetId, FetchOption = CriteriaBase.FetchOptions.List };

      var response = SearchService.GetROnet(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ROnetId;
    }


		/// <summary>
		/// Gets the search results.
		/// </summary>
		/// <param name="searchCriteria">The search criteria.</param>
		/// <param name="count">The count.</param>
		/// <param name="manualSearch">if set to <c>true</c> [manual search].</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<PostingSearchResultView> GetSearchResults(SearchCriteria searchCriteria, int? count = null, bool manualSearch = true)
		{
			var request = new LensSearchRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = searchCriteria;
			request.DocumentType = DocumentType.Posting;
			request.MaximumDocumentCount = count ?? Runtime.Settings.MaximumNoDocumentToReturnInSearch;
			request.ManualSearch = manualSearch;

			if (Runtime.Settings.JobDescriptionCharsShown > 0)
			{
				if (request.Criteria.RequiredResultCriteria.IsNull())
					request.Criteria.RequiredResultCriteria = new RequiredResultCriteria();

				request.Criteria.RequiredResultCriteria.IncludeDuties = true;
			}
				

		  int? originalDocCount = null;

		  if (request.Criteria.RequiredResultCriteria.IsNotNull())
		  {
		    originalDocCount = request.Criteria.RequiredResultCriteria.MaximumDocumentCount;
		    request.Criteria.RequiredResultCriteria.MaximumDocumentCount = count;
		  }

		  var response = SearchService.LensSearch(request);

		  if (originalDocCount.IsNotNull())
		    request.Criteria.RequiredResultCriteria.MaximumDocumentCount = originalDocCount;

			Correlate(request, response);

      //TODO: Martha (new activity functionality) - check below
      //To avoid commands generated from saved searches in dashboard 
      //if (userId.IsNotNullOrEmpty() && count > 50)
      //{
      //  var jobSeekerActivity = CareerApp.GetSessionValue<JobSeekerActivity>("Career:JobSeekerActivity");
      //  if (jobSeekerActivity.IsNotNull())
      //    jobSeekerActivity.SearchesMade = jobSeekerActivity.SearchesMade + 1;
      //}

      if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Postings;
		}

		/// <summary>
		/// Gets the more jobs like this.
		/// </summary>
		/// <param name="searchCriteria">The search criteria.</param>
		/// <returns></returns>
		public List<PostingSearchResultView> GetMoreJobsLikeThis(SearchCriteria searchCriteria)
		{
			var request = new LensSearchRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = searchCriteria;
			request.DocumentType = DocumentType.Posting;
			
			var response = SearchService.LensSearch(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Postings;
		}

		/// <summary>
		/// Gets the onet.
		/// </summary>
		/// <param name="onetCode">The onet code.</param>
		/// <returns></returns>
		public List<string> GetOnetKeywords(string onetCode)
		{
			var request = new OnetKeywordsRequest().Prepare(AppContext, Runtime.Settings);
			request.OnetCode = onetCode;

			var response = SearchService.GetOnetKeywords(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Keywords;
		}
		
		/// <summary>
		/// Gets the onet tasks.
		/// </summary>
		/// <param name="onetId">The onet id.</param>
		/// <returns></returns>
		public List<OnetTaskView> GetOnetTasks(long onetId)
		{
			var request = new OnetTaskRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new OnetTaskCriteria { OnetId = onetId, ListSize = 100 };

			var response = SearchService.GetOnetTasks(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.OnetTasks;
		}

		/// <summary>
		/// Gets the resume search session.
		/// </summary>
		/// <param name="searchId">The search identifier.</param>
		/// <param name="starRating">The star rating.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<ResumeView> GetResumeSearchSession(Guid searchId, int? starRating)
		{
			var request = new GetResumeSearchSessionRequest().Prepare(AppContext, Runtime.Settings);
			request.SearchId = searchId;
			request.StarRating = starRating;

			var response = SearchService.GetResumeSearchSession(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Resumes;
		}
	}
}