﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Candidate;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.Criteria.JobSeeker;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Messages.CandidateService;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Messages.PostingService;
using Focus.Core.Messages.ResumeService;
using Focus.Core.Models;
using Focus.Core.Views;
using Focus.ServiceClients.InProcess.Extensions;
using Focus.ServiceClients.Interfaces;
using Focus.Web.Core.Models;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.InProcess
{
	public class CandidateClient : ClientBase, ICandidateClient
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CandidateClient" /> class.
		/// </summary>
		/// <param name="appContext">The application context.</param>
		/// <param name="runtime">The runtime.</param>
		public CandidateClient(AppContextModel appContext, ServiceClientRuntime runtime) : base(appContext, runtime)
		{
		}

		/// <summary>
		/// Emails the candidate.
		/// </summary>
		/// <param name="personId">The person id of the candidate.</param>
		/// <param name="emailSubject">The email subject.</param>
		/// <param name="emailBody">The email body.</param>
		/// <param name="detectUrl">Whether to detect URLs</param>
		/// <param name="bccSender">if set to <c>true</c> [BCC sender].</param>
		/// <param name="jobId">The job id.</param>
		/// <param name="isInviteToApply">if set to <c>true</c> [is invite to apply].</param>
		/// <param name="candidateName">Name of the candidate.</param>
		/// <param name="senderAddress">Sender address</param>
    /// <param name="contactRequest">Whether this is a Contact Request Message</param>
		public void SendCandidateEmail(long personId, string emailSubject, string emailBody, bool detectUrl,
			bool bccSender = false, long jobId = 0, bool isInviteToApply = false, string candidateName = "",
			string senderAddress = null, bool contactRequest = false)
		{
			var emails = new List<CandidateEmailView>
			{
				new CandidateEmailView
				{
					PersonId = personId,
					CandidateName = candidateName,
					Subject = emailSubject,
					Body = emailBody,
					BccRequestor = bccSender,
					JobId = jobId,
					IsInviteToApply = isInviteToApply,
					SenderAddress = senderAddress,
          ContactRequest = contactRequest
				}
			};

			SendCandidateEmails(emails, detectUrl);
		}

		/// <summary>
		/// Sends the candidate emails.
		/// </summary>
		/// <param name="emails">The emails.</param>
		/// <param name="detectUrl">Whether to detect URLs</param>
		/// <param name="sendCopy">if set to <c>true</c> [send copy].</param>
		/// <param name="footerType">Type of the footer.</param>
		/// <param name="footerUrl">The footer URL.</param>
		/// <param name="checkUnsubscribed">Whether to check if the job seeker is unsubscribed</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public int SendCandidateEmails(List<CandidateEmailView> emails, bool detectUrl, bool sendCopy = false,
			EmailFooterTypes footerType = EmailFooterTypes.None, string footerUrl = null, bool checkUnsubscribed = false)
		{
			var request = new SendCandidateEmailsRequest().Prepare(AppContext, Runtime.Settings);
			request.Emails = emails;
			request.DetectUrl = detectUrl;
			request.SendCopy = sendCopy;
			request.FooterType = footerType;
			request.FooterUrl = footerUrl;
			request.CheckSubscribed = checkUnsubscribed;

			var response = CandidateService.SendCandidateEmails(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.UnsubscribedCount;
		}


		/// <summary>
		/// Gets the resume.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="returnFullResume">if set to <c>true</c> to return the full resume.</param>
		/// <param name="logView">Whether to log the fact the resume has been viewed</param>
		/// <param name="includeAge">Whether to display the age in the resume</param>
		/// <param name="noResumeMessage">A message to show if no resume is found</param>
		/// <returns></returns>
		public string GetResumeAsHtml(long personId, bool returnFullResume = false, bool logView = true, bool includeAge = false, string noResumeMessage = null, long jobId = 0)
		{
			var request = new GetResumeAsHtmlRequest().Prepare(AppContext, Runtime.Settings);
			request.PersonId = personId;
			request.ReturnFullResume = returnFullResume;
			request.LogView = logView;
			request.IncludeAge = includeAge;
            request.ApplicationJobId = jobId;

			var response = CandidateService.GetResumeAsHtml(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				// TODO: return to normal operation once all resumes available in database
				return string.Concat("<body>", (noResumeMessage ?? "No resume available for this candidate"), "</body>");
			//throw new ServiceCallException(response.Message, (int)response.Error);

			return response.ResumeHtml;
		}

		/// <summary>
		/// Gets the resume.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <param name="logView">Whether to log an action to record the resume has been viewed.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public string GetResumeByIdAsHtml(long resumeId, bool logView = true)
		{
			var request = new GetResumeAsHtmlRequest().Prepare(AppContext, Runtime.Settings);
			request.ResumeId = resumeId;
			request.ReturnFullResume = true;
			request.LogView = logView;

			var response = CandidateService.GetResumeAsHtml(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.ResumeHtml;
		}

		/// <summary>
		/// Gets the resume.
		/// </summary>
		/// <param name="personId"></param>
		/// <param name="returnFullResume">if set to <c>true</c> to return the full resume.</param>
		/// <returns></returns>
		public ResumeView GetResume(long personId, bool returnFullResume = false)
		{
			var request = new GetResumeRequest().Prepare(AppContext, Runtime.Settings);
			request.PersonId = personId;
			request.ReturnFullResume = returnFullResume;

			var response = CandidateService.GetResume(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
			{
				// Handle exceptions
				switch (response.Error)
				{
					case ErrorTypes.NoCandidateIdProvided:
						throw new ArgumentNullException("personId", response.Message);
					default:
						throw new ServiceCallException(response.Message, (int) response.Error);
				}
			}

			return response.ResumeDetails;
		}

		/// <summary>
		/// Gets the resume by id.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <returns></returns>
		public ResumeView GetResumeById(long resumeId)
		{
			var request = new GetResumeRequest().Prepare(AppContext, Runtime.Settings);
			request.ResumeId = resumeId;

			var response = CandidateService.GetResumeById(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
			{
				// Handle exceptions
				switch (response.Error)
				{
					case ErrorTypes.NoCandidateIdProvided:
						throw new ArgumentNullException("resumeId", response.Message);
					default:
						throw new ServiceCallException(response.Message, (int) response.Error);
				}
			}

			return response.ResumeDetails;
		}

		/// <summary>
		/// Gets the resumes.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		public List<ResumeSummary> GetResumes(long personId)
		{
			var request = new GetResumesRequest().Prepare(AppContext, Runtime.Settings);
			request.PersonId = personId;

			var response = CandidateService.GetResumes(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.CandidateResumes;
		}

		/// <summary>
		/// Gets the job seeker referrals.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public PagedList<JobSeekerReferralAllStatusViewDto> GetJobSeekerReferrals(JobSeekerReferralCriteria criteria)
		{
			var request = new JobSeekerReferralViewRequest().Prepare(AppContext, Runtime.Settings);
			criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;

			request.Criteria = criteria;

			var response = CandidateService.GetJobSeekerReferralView(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.ReferralViewsPaged;
		}

		/// <summary>
		/// Gets the job seeker referrals.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<JobSeekerView> SearchJobSeekers(JobSeekerCriteria criteria)
		{
			var request = new JobSeekerSearchRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = criteria;

			var response = CandidateService.SearchJobSeekers(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.SearchResults;
		}

		/// <summary>
		/// Gets the job seeker referral.
		/// </summary>
		/// <param name="candidateApplicationId">The candidate application id.</param>
		/// <returns></returns>
		public JobSeekerReferralAllStatusViewDto GetJobSeekerReferral(long candidateApplicationId)
		{
			var request = new JobSeekerReferralViewRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobSeekerReferralCriteria
			{
				Id = candidateApplicationId,
				FetchOption = CriteriaBase.FetchOptions.Single
			};

			var response = CandidateService.GetJobSeekerReferralView(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.Referral;
		}

		/// <summary>
		/// Approves the candidate referral.
		/// </summary>
		/// <param name="candidateApplicationId">The candidate application id.</param>
		/// <param name="autoApproval">if set to <c>true</c> [auto approval].</param>
		/// <returns>
		/// A message if there was a problem approving it, blank otherwise
		/// </returns>
		/// <exception cref="ServiceCallException"></exception>
		public string ApproveCandidateReferral(long candidateApplicationId, int lockVersion, bool autoApproval = false)
		{
			var request = new CandidateApplicationRequest().Prepare(AppContext, Runtime.Settings);
			request.Id = candidateApplicationId;
			request.AutoApproval = autoApproval;
			request.LockVersion = lockVersion;

			var response = CandidateService.ApproveReferral(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
			{
				if (response.HasRaisedException)
					throw new ServiceCallException(response.Message, (int) response.Error);

				return response.Message;
			}

			return string.Empty;
		}

		/// <summary>
		/// Denies the candidate referral.
		/// </summary>
		/// <param name="candidateApplicationId">The candidate application id.</param>
		/// <param name="applicationLockVersion">The application lock version.</param>
		/// <param name="referralStatusReasons">The referral status reasons.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void DenyCandidateReferral(long candidateApplicationId, int applicationLockVersion,
			List<KeyValuePair<long, string>> referralStatusReasons = null)
		{
			var request = new CandidateApplicationRequest().Prepare(AppContext, Runtime.Settings);
			request.Id = candidateApplicationId;
			request.LockVersion = applicationLockVersion;
			request.ReferralStatusReasons = referralStatusReasons;

			var response = CandidateService.DenyReferral(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Puts the candidate referral on hold
		/// </summary>
		/// <param name="candidateApplicationId">The candidate application id.</param>
		/// <param name="onHoldReason">The reason the referral was put on hold.</param>
		/// <param name="applicationLockVersion">The application lock version.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void HoldCandidateReferral(long candidateApplicationId, ApplicationOnHoldReasons onHoldReason,
			int applicationLockVersion)
		{
			var request = new CandidateApplicationRequest().Prepare(AppContext, Runtime.Settings);
			request.Id = candidateApplicationId;
			request.LockVersion = applicationLockVersion;
			request.OnHoldReason = onHoldReason;

			var response = CandidateService.HoldReferral(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Gets the resume.
		/// </summary>
		/// <param name="activityId">The activity id.</param>
		/// <param name="jobSeeker">The job seeker.</param>
		/// <param name="actionedDate">The actioned date.</param>
		/// <param name="isSelfAssigned">Whether the job seeker is assigning themselves</param>
		public bool AssignActivity(long activityId, JobSeekerView jobSeeker, DateTime? actionedDate = null,
			bool isSelfAssigned = false)
		{
			var request = new AssignActivityOrActionRequest().Prepare(AppContext, Runtime.Settings);

			request.ActivityId = activityId;

			request.PersonId = jobSeeker.PersonId;

			// TODO: check integration in place to remove null check
			request.ClientId = jobSeeker.ClientId ?? jobSeeker.PersonId.ToString();
			request.UserName = jobSeeker.UserName;
			request.FirstName = jobSeeker.FirstName;
			request.LastName = jobSeeker.LastName;
			request.PhoneNumber = jobSeeker.PhoneNumber;
			request.EmailAddress = jobSeeker.EmailAddress;
			request.ActivityTime = actionedDate;
			request.IsSelfAssigned = isSelfAssigned;

			var response = CandidateService.AssignActivity(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

            if (response.FocusId == null)
                return false;
            else
                return true;

		}

		/// <summary>
		/// Assigns the activity to multiple users.
		/// </summary>
		/// <param name="activityId">The activity id.</param>
		/// <param name="jobSeekers">The job seekers.</param>
		/// <param name="actionedDate"></param>
		/// <exception cref="ServiceCallException"></exception>
		public bool AssignActivityToMultipleUsers(long activityId, List<JobSeekerView> jobSeekers,
			DateTime? actionedDate = null)
		{
			var request = new AssignActivityOrActionRequest().Prepare(AppContext, Runtime.Settings);

			request.ActivityId = activityId;
			request.Jobseekers = jobSeekers;
			request.ActivityTime = actionedDate;

			var response = CandidateService.AssignActivityToMultipleUsers(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
            if (response.FocusId == null)
                return false;
            else
                return true;
		}

		/// <summary>
		/// Gets the job seeker lists.
		/// </summary>
		/// <param name="listType">Type of the list.</param>
		/// <returns></returns>
		public List<SelectView> GetJobSeekerLists(ListTypes listType)
		{
			var request = new GetJobSeekerListsRequest().Prepare(AppContext, Runtime.Settings);
			request.ListType = listType;

			var response = CandidateService.GetJobSeekerLists(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.Lists;
		}

		/// <summary>
		/// Gets the job seeker referrals.
		/// </summary>
		/// <param name="listId">The list id.</param>
		/// <returns></returns>
		public List<JobSeekerView> GetJobSeekersForList(long listId)
		{
			var request = new GetJobSeekersForListRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobSeekerListCriteria
			{
				ListId = listId,
				FetchOption = CriteriaBase.FetchOptions.List
			};

			var response = CandidateService.GetJobSeekersForList(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.JobSeekers;
		}

		/// <summary>
		/// Gets the job seeker referrals.
		/// </summary>
		/// <param name="listId">The list id.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public PagedList<JobSeekerView> GetJobSeekersForPagedList(long listId, int pageIndex, int pageSize)
		{
			var request = new GetJobSeekersForListRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobSeekerListCriteria
			{
				ListId = listId,
				PageIndex = pageIndex,
				PageSize = pageSize,
				FetchOption = CriteriaBase.FetchOptions.PagedList
			};

			var response = CandidateService.GetJobSeekersForList(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.PagedJobSeekers;
		}

		/// <summary>
		/// Adds the job seeker to list.
		/// </summary>
		/// <param name="listType">Type of the list.</param>
		/// <param name="newListName">New name of the list.</param>
		/// <param name="jobSeekers">The job seekers.</param>
		/// <returns></returns>
		public string AddJobSeekersToList(ListTypes listType, string newListName, JobSeekerView[] jobSeekers)
		{
			var request = new AddJobSeekersToListRequest().Prepare(AppContext, Runtime.Settings);
			request.ListType = listType;
			request.NewListName = newListName;
			request.JobSeekers = jobSeekers;

			var response = CandidateService.AddJobSeekersToList(request);

			return response.Acknowledgement == AcknowledgementType.Failure ? response.Message : null;
		}

		/// <summary>
		/// Adds the job seeker to list.
		/// </summary>
		/// <param name="listType">Type of the list.</param>
		/// <param name="listId">The list id.</param>
		/// <param name="jobSeekers">The job seekers.</param>
		/// <returns></returns>
		public string AddJobSeekersToList(ListTypes listType, long listId, JobSeekerView[] jobSeekers)
		{
			var request = new AddJobSeekersToListRequest().Prepare(AppContext, Runtime.Settings);
			request.ListType = listType;
			request.ListId = listId;
			request.JobSeekers = jobSeekers;

			var response = CandidateService.AddJobSeekersToList(request);

			return response.Acknowledgement == AcknowledgementType.Failure ? response.Message : null;
		}

		/// <summary>
		/// Removes the job seekers from list.
		/// </summary>
		/// <param name="listId">The list id.</param>
		/// <param name="jobSeekers">The job seekers.</param>
		public void RemoveJobSeekersFromList(long listId, JobSeekerView[] jobSeekers)
		{
			var request = new RemoveJobSeekersFromListRequest().Prepare(AppContext, Runtime.Settings);
			request.ListId = listId;
			request.JobSeekers = jobSeekers;

			var response = CandidateService.RemoveJobSeekersFromList(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Deletes the job seeker list.
		/// </summary>
		/// <param name="listId">The list id.</param>
		public void DeleteJobSeekerList(long listId)
		{
			var request = new DeleteJobSeekerListRequest().Prepare(AppContext, Runtime.Settings);
			request.ListId = listId;

			var response = CandidateService.DeleteJobSeekerList(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Assigns the job seeker to admin.
		/// </summary>
		/// <param name="adminId">The admin id.</param>
		/// <param name="jobSeeker">The job seeker.</param>
		/// <returns></returns>
		public string AssignJobSeekerToAdmin(long adminId, JobSeekerView jobSeeker)
		{
			var request = new AssignJobSeekerToAdminRequest().Prepare(AppContext, Runtime.Settings);
			request.AdminId = adminId;
			request.PersonId = jobSeeker.PersonId;
			request.ClientId = jobSeeker.ClientId;
			request.UserName = jobSeeker.UserName;
			request.FirstName = jobSeeker.FirstName;
			request.LastName = jobSeeker.LastName;
			request.PhoneNumber = jobSeeker.PhoneNumber;
			request.EmailAddress = jobSeeker.EmailAddress;

			var response = CandidateService.AssignJobSeekerToAdmin(request);

			return response.Acknowledgement == AcknowledgementType.Failure ? response.Message : null;
		}

		/// <summary>
		/// Gets the candidate candidate system defaults.
		/// </summary>
		/// <returns></returns>
		public CandidateSystemDefaultsView GetCandidateCandidateSystemDefaults()
		{
			var request = new GetCandidateSystemDefaultsRequest().Prepare(AppContext, Runtime.Settings);

			var response = CandidateService.GetCandidateSystemDefaults(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.SystemDefaults;
		}

		/// <summary>
		/// Updates the candidate system defaults.
		/// </summary>
		/// <param name="systemDefaults">The system defaults.</param>
		public void UpdateCandidateSystemDefaults(CandidateSystemDefaultsView systemDefaults)
		{
			var request = new UpdateCandidateSystemDefaultsRequest().Prepare(AppContext, Runtime.Settings);
			request.SystemDefaults = systemDefaults;

			var response = CandidateService.UpdateCandidateSystemDefaults(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Creates the homepage alert.
		/// </summary>
		/// <param name="homepageAlert">The homepage alert.</param>
		public void CreateHomepageAlert(JobSeekerHomepageAlertView homepageAlert)
		{
			var homepageAlerts = new List<JobSeekerHomepageAlertView> {homepageAlert};
			CreateHomepageAlert(homepageAlerts);
		}

		/// <summary>
		/// Creates the homepage alert.
		/// </summary>
		/// <param name="homepageAlerts">The homepage alerts.</param>
		public void CreateHomepageAlert(List<JobSeekerHomepageAlertView> homepageAlerts)
		{
			var request = new CreateJobSeekerHomepageAlertRequest().Prepare(AppContext, Runtime.Settings);
			request.HomepageAlerts = homepageAlerts;

			var response = CandidateService.CreateJobSeekerHomepageAlert(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Gets the person note.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		public PersonNoteDto GetPersonNote(long personId, long? employerId)
		{
			var request = new GetPersonNoteRequest().Prepare(AppContext, Runtime.Settings);
			request.PersonId = personId;
			request.EmployerId = employerId;

			var response = CandidateService.GetPersonNote(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.PersonNote;
		}

		/// <summary>
		/// Saves the note.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="employerId">The employer id.</param>
		/// <param name="note">The note.</param>
		public void SavePersonNote(long personId, long? employerId, string note)
		{
			var request = new SavePersonNoteRequest().Prepare(AppContext, Runtime.Settings);
			request.PersonId = personId;
			request.EmployerId = employerId;
			request.Note = note;

			var response = CandidateService.SavePersonNote(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Resfers the candidate.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="jobId">The job id.</param>
		/// <param name="score">The score.</param>
		/// <param name="waivedRequirements">The waived requirements.</param>
		/// <param name="staffReferral">if set to <c>true</c> [staff referral].</param>
		/// <returns>Whether the application was queued due to being a veteran job (and job seeker is not a veteran)</returns>
		/// <exception cref="ServiceCallException"></exception>
		public bool ReferCandidate(long personId, long jobId, int score, List<string> waivedRequirements, bool staffReferral)
		{
			var request = new ReferCandidateRequest
			{
				PersonId = personId,
				JobId = jobId,
				ApplicationScore = score,
				WaivedRequirements = waivedRequirements,
				StaffReferral = staffReferral
			}.Prepare(AppContext, Runtime.Settings);

			var response = CandidateService.ReferCandidate(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.ApplicationQueued;
		}

		/// <summary>
		/// Toggles the candidate flag.
		/// </summary>
		/// <param name="personId">The person id.</param>
		public void ToggleCandidateFlag(long personId)
		{
			var request = new ToggleCandidateFlagRequest().Prepare(AppContext, Runtime.Settings);
			request.PersonId = personId;

			var response = CandidateService.ToggleCandidateFlag(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Updates the application status.
		/// </summary>
		/// <param name="personId">The candidate external id.</param>
		/// <param name="jobId">The job id.</param>
		/// <param name="newStatus">The new status.</param>
		public void UpdateApplicationStatus(long personId, long jobId, ApplicationStatusTypes newStatus)
		{
			var request = new UpdateApplicationStatusRequest().Prepare(AppContext, Runtime.Settings);
			request.PersonId = personId;
			request.JobId = jobId;
			request.NewStatus = newStatus;

			var response = CandidateService.UpdateApplicationStatus(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Updates the application status.
		/// </summary>
		/// <param name="candidateApplicationId">The candidate application id.</param>
		/// <param name="newStatus">The new status.</param>
		public void UpdateApplicationStatus(long candidateApplicationId, ApplicationStatusTypes newStatus)
		{
			var request = new UpdateApplicationStatusRequest().Prepare(AppContext, Runtime.Settings);
			request.ApplicationId = candidateApplicationId;
			request.NewStatus = newStatus;

			var response = CandidateService.UpdateApplicationStatus(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Gets the flagged candidates.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<ResumeView> GetFlaggedCandidates(FlaggedCandidateCriteria criteria)
		{
			var request = new GetFlaggedCandidatesRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = criteria;

			var response = CandidateService.GetFlaggedCandidates(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.Candidates;
		}

		/// <summary>
		/// Gets the applicant resume views.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<ResumeView> GetApplicantResumeViews(ResumeViewCriteria criteria)
		{
			var request = new ResumeViewRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = criteria;
			request.Criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;

			var response = CandidateService.GetApplicantResumeView(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.ResumeViewsPaged;
		}

		/// <summary>
		/// Gets the application.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public ApplicationDto GetApplication(long personId, long jobId)
		{
			var request = new GetApplicationRequest {PersonId = personId, JobId = jobId}.Prepare(AppContext, Runtime.Settings);
			var response = CandidateService.GetApplication(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.Application ?? new ApplicationDto();
		}

		/// <summary>
		/// Gets the application.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="postingId">The posting id.</param>
		/// <returns></returns>
		public ApplicationDto GetApplicationForPosting(long personId, long postingId)
		{
			var request = new GetApplicationRequest {PersonId = personId, PostingId = postingId}.Prepare(AppContext,
				Runtime.Settings);
			var response = CandidateService.GetApplication(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.Application ?? new ApplicationDto();
		}

		/// <summary>
		/// Gets the applications for a list of people
		/// </summary>
		/// <param name="personIds">The list of person ids.</param>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public Dictionary<long, ApplicationDto> GetApplication(List<long> personIds, long jobId)
		{
			var request = new GetApplicationRequest {PersonIds = personIds, JobId = jobId}.Prepare(AppContext, Runtime.Settings);
			var response = CandidateService.GetApplication(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			personIds.ForEach(id =>
			{
				if (!response.Applications.ContainsKey(id))
					response.Applications.Add(id, new ApplicationDto());
			});

			return response.Applications;
		}

		/// <summary>
		/// Gets the application views for a candidate
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="referralsRequired">The number required.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<ApplicationViewDto> GetApplicationView(long personId, int? referralsRequired = null)
		{
			var request = new GetApplicationViewRequest
			{
				PersonId = personId,
				ReferralsRequired = referralsRequired
			}.Prepare(AppContext, Runtime.Settings);

			var response = CandidateService.GetApplicationViews(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.ApplicationViews;
		}

		/// <summary>
		/// Gets the application status log views.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<ApplicationStatusLogViewDto> GetApplicationStatusLogViews(ApplicationStatusLogViewCriteria criteria)
		{
			var request = new ApplicationStatusLogViewRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = criteria;
			request.Criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;

			var response = CandidateService.GetApplicationStatusLogView(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.ApplicationStatusLogViewsPaged;
		}

		/// <summary>
		/// Gets the application status log views grouped.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<EmployerJobReferralListItem> GetApplicationStatusLogViewsGrouped(
			ApplicationStatusLogViewCriteria criteria)
		{
			var request = new ApplicationStatusLogViewRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = criteria;
			request.Criteria.FetchOption = CriteriaBase.FetchOptions.Grouped;

			var response = CandidateService.GetApplicationStatusLogView(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.EmployerJobReferralListItems;
		}

		/// <summary>
		/// Gets the job seeker activity view.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="getStaffReferrals">Whether to populate the Staff Referrals field</param>
		/// <param name="getHighGrowthSectors">Whether to populate the Staff Referrals field</param>
		/// <returns></returns>
		public JobSeekerActivityView GetJobSeekerActivityView(JobSeekerCriteria criteria, bool getStaffReferrals = true,
			bool getHighGrowthSectors = true)
		{
			var request = new JobSeekerActivityViewRequest
			{
				Criteria = criteria,
				GetStaffReferrals = getStaffReferrals,
				GetHighGrowthSectors = getHighGrowthSectors
			}.Prepare(AppContext, Runtime.Settings);

			var response = CandidateService.GetJobSeekerActivityView(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.JobSeekerActivityView;
		}

		/// <summary>
		/// Gets the job seeker profile.
		/// </summary>
		/// <param name="jobSeekerId">The job seeker id.</param>
		/// <param name="showContactDetails"></param>
		/// <returns></returns>
		public JobSeekerProfileModel GetJobSeekerProfile(long jobSeekerId, bool showContactDetails = false)
		{
			var request =
				new JobSeekerProfileRequest
				{
					JobSeekerId = jobSeekerId,
					ShowContactDetails = showContactDetails,
					CountryCode = Runtime.Settings.DefaultCountryKey
				}.Prepare(AppContext, Runtime.Settings);

			var response = CandidateService.GetJobSeekerProfile(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return new JobSeekerProfileModel
			{
				JobSeekerProfileView = response.JobSeekerProfileView,
				ApplicantsDidNotApply = response.ApplicantsDidNotApply,
				ApplicantsFailedToShow = response.ApplicantsFailedToShow,
				ApplicantsHired = response.ApplicantsHired,
				ApplicantsInterviewDenied = response.ApplicantsInterviewDenied,
				ApplicantsInterviewed = response.ApplicantsInterviewed,
				ApplicantsRefused = response.ApplicantsRefused,
				ApplicantsRejected = response.ApplicantsRejected,
				ApplicantsFailedToRespondToInvitation = response.ApplicantsFailedToRespondToInvitation,
				ApplicantsFailedToReportToJob = response.ApplicantsFailedToReportToJob,
				ApplicantsFoundJobFromOtherSource = response.ApplicantsFoundJobFromOtherSource,
				ApplicantsJobAlreadyFilled = response.ApplicantsJobAlreadyFilled,
				ApplicantsNewApplicant = response.ApplicantsNewApplicant,
				ApplicantsNotQualified = response.ApplicantsNotQualified,
				ApplicantsNotYetPlaced = response.ApplicantsNotYetPlaced,
				ApplicantsRecommended = response.ApplicantsRecommended,
				ApplicantsRefusedReferral = response.ApplicantsRefusedReferral,
				ApplicantsUnderConsideration = response.ApplicantsUnderConsideration,
				MatchQualityDissatisfied = response.MatchQualityDissatisfied,
				MatchQualitySatisfied = response.MatchQualitySatisfied,
				MatchQualitySomewhatDissatisfied = response.MatchQualitySomewhatDissatisfied,
				MatchQualitySomewhatSatisfied = response.MatchQualitySomewhatSatisfied,
				NotReceiveUnexpectedMatches = response.NotReceiveUnexpectedMatches,
				NotEmployerInvitationsReceive = response.NotEmployerInvitationsReceive,
				ReceivedEmployerInvitations = response.ReceivedEmployerInvitations,
				ReceivedUnexpectedResults = response.ReceivedUnexpectedResults,
				ResumeId = response.ResumeId,
				ResumeLength = response.ResumeLength,
				NumberOfResumes = response.NumberOfResumes,
				EmploymentStatus = response.EmploymentStatus,
				EducationLevel = response.EducationLevel,
				EnrollmentStatus = response.EnrollmentStatus,
				SkillsCodedFromResume = response.SkillsCodedFromResume,
				ResumeIsSearchable = response.ResumeIsSearchable,
				WillingToWorkOvertime = response.WillingToWorkOvertime,
				WillingToRelocate = response.WillingToRelocate,
				NumberOfIncompleteResumes = response.NumberOfIncompleteResumes,
				UnemploymentInfo = response.UnemploymentInfo,
                HomelessWithoutShelter = response.HomelessWithoutShelter,
                HomelessWithShelter = response.HomelessWithShelter,
                RunawayYouth = response.RunawayYouth,
                ExOffender = response.ExOffender,
				VeteranInfo = response.VeteranInfo,
				DisabilityStatus = response.DisabilityStatus,
				DisabilityCategoryIds = response.DisabilityCategoryIds,
                DisabilityCategoryId = response.DisabilityCategoryId,
                OfficeIds = response.OfficeIds,
				PrimaryPhone = response.PrimaryPhone,
				IsMSFW = response.IsMSFW,
                MSFWQuestions = response.MSFWQuestions,
                LowLevelLiteracy = response.LowLevelLiteracy,
                PreferredLanguage = response.PreferredLanguage,
                CommonLanguage = response.CommonLanguage,
                NativeLanguage = response.NativeLanguage,
                CulturalBarriers = response.CulturalBarriers,
                NoOfDependents = response.NoOfDependents,
                EstMonthlyIncome = response.EstMonthlyIncome,
                LowIncomeStatus = response.LowIncomeStatus,
                SingleParent = response.SingleParent,
                DisplacedHomemaker = response.DisplacedHomemaker

			};
		}

		/// <summary>
		/// Gets the job seeker activities.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<JobSeekerActivityActionViewDto> GetJobSeekerActivities(JobSeekerCriteria criteria)
		{
			var request = new JobSeekerActivityActionViewRequest {Criteria = criteria}.Prepare(AppContext, Runtime.Settings);

			var response = CandidateService.GetJobSeekerActivities(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error, response.Exception);

			return response.JobSeekerActivityActionViewPaged;
		}

		/// <summary>
		/// Gets the job seeker activity users.
		/// </summary>
		/// <param name="jobSeekerId">The job seeker id.</param>
		/// <returns></returns>
		public List<UserView> GetJobSeekerActivityUsers(long jobSeekerId)
		{
			var request =
				new JobSeekerActivityActionViewRequest {Criteria = new JobSeekerCriteria {JobSeekerId = jobSeekerId}}.Prepare(
					AppContext, Runtime.Settings);
			var response = CandidateService.GetJobSeekerActivityUsers(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error, response.Exception);

			return response.JobSeekerActivityActionViewUserList;
		}

		// <summary>
		// Gets the job seekers and issues paged list.
		// </summary>
		// <param name="criteria">The criteria.</param>
		// <returns></returns>
		// <exception cref="ServiceCallException"></exception>
		public PagedList<JobSeekerDashboardModel> GetCandidatesAndIssuesPagedList(JobSeekerCriteria criteria)
		{
			criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;
			var request = new GetCandidatesAndIssuesRequest {Criteria = criteria}.Prepare(AppContext, Runtime.Settings);
			var response = CandidateService.GetCandidatesAndIssues(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error, response.Exception);

			return response.CandidatesAndIssuesPaged;
		}


		/// <summary>
		/// Gets the candidate and issues.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public CandidateIssueViewDto GetCandidateAndIssues(long personId)
		{
			var criteria = new JobSeekerCriteria {PersonId = personId, FetchOption = CriteriaBase.FetchOptions.Single};
			var request = new GetCandidatesAndIssuesRequest {Criteria = criteria}.Prepare(AppContext, Runtime.Settings);
			var response = CandidateService.GetCandidatesAndIssues(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error, response.Exception);

			return response.CandidateAndIssues;
		}

		/// <summary>
		/// Gets the student alumni issues paged list.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<StudentAlumniIssueViewDto> GetStudentAlumniIssuesPagedList(JobSeekerCriteria criteria)
		{
			criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;
			var request = new GetStudentAlumniIssuesRequest {Criteria = criteria}.Prepare(AppContext, Runtime.Settings);
			var response = CandidateService.GetStudentAlumniIssues(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error, response.Exception);

			return response.StudentAlumniIssuesPaged;
		}

		/// <summary>
		/// Gets the candidate.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public CandidateViewDto GetCandidate(long personId)
		{
			var request = new GetCandidateRequest
			{
				PersonId = personId,
				GetPersonRecord = false
			}.Prepare(AppContext, Runtime.Settings);

			var response = CandidateService.GetCandidate(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error, response.Exception);

			return response.Candidate;
		}

		/// <summary>
		/// Gets the candidate.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public PersonDto GetCandidatePerson(long personId)
		{
			var request = new GetCandidateRequest
			{
				PersonId = personId,
				GetPersonRecord = true
			}.Prepare(AppContext, Runtime.Settings);

			var response = CandidateService.GetCandidate(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error, response.Exception);

			return response.Person;
		}

		/// <summary>
		/// Flags the candidate for follow up.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void FlagCandidateForFollowUp(long personId)
		{
			var request = new FlagCandidateForFollowUpRequest {PersonId = personId}.Prepare(AppContext, Runtime.Settings);
			var response = CandidateService.FlagCandidateForFollowUp(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error, response.Exception);

		}

		/// <summary>
		/// Resolves the candidate issues.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="issuesResolved">The issues resolved.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void ResolveCandidateIssues(long personId, List<CandidateIssueType> issuesResolved, bool autoresolve = false)
		{

			var request =
				new ResolveCandidateIssuesRequest {PersonId = personId, IssuesResolved = issuesResolved}.Prepare(AppContext,
					Runtime.Settings);
			var response = CandidateService.ResolveCandidateIssues(request, autoresolve);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error, response.Exception);
		}

		/// <summary>
		/// Gets the open position jobs.
		/// </summary>
		/// <param name="candidateId">The candidate id.</param>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		public List<OpenPositionMatchesViewDto> GetOpenPositionJobs(long candidateId, long employerId)
		{
			var request =
				new CandidateOpenPositionMatchesRequest {CandidateId = candidateId, EmployerId = employerId}.Prepare(AppContext,
					Runtime.Settings);
			var response = CandidateService.GetOpenPositionJobs(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error, response.Exception);

			return response.CandidateOpenPositionMatches;
		}

		/// <summary>
		/// Ignores person to posting match.
		/// </summary>
		/// <param name="postingId">The posting id.</param>
		/// <param name="personId">The person id.</param>
		public void IgnorePersonPostingMatch(long postingId, long personId)
		{
			var request = new IgnorePersonPostingMatchRequest {PostingId = postingId, PersonId = personId}.Prepare(AppContext,
				Runtime.Settings);
			var response = CandidateService.IgnorePersonPostingMatch(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error, response.Exception);
		}

		/// <summary>
		/// Allows the person to posting match.
		/// </summary>
		/// <param name="postingId">The posting id.</param>
		/// <param name="personId">The person id.</param>
		public void AllowPersonPostingMatch(long postingId, long personId)
		{
			var request = new AllowPersonPostingMatchRequest {PostingId = postingId, PersonId = personId}.Prepare(AppContext,
				Runtime.Settings);
			var response = CandidateService.AllowPersonPostingMatch(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Registers action when a user runs a job search for a job seeker
		/// </summary>
		/// <param name="candidatePersonId">The candidate person id.</param>
		public void RegisterFindJobsForSeeker(long candidatePersonId)
		{
			var request = new RegisterFindJobsForSeekerRequest {CandidatePersonId = candidatePersonId}.Prepare(AppContext,
				Runtime.Settings);
			var response = CandidateService.RegisterFindJobsForSeeker(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Registers action when job seeker click how to apply.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="candidatePersonId">The candidate person id.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void RegisterHowToApply(string lensPostingId, long candidatePersonId)
		{
			var request =
				new RegisterHowToApplyRequest {LensPostingId = lensPostingId, CandidatePersonId = candidatePersonId}.Prepare(
					AppContext, Runtime.Settings);
			var response = CandidateService.RegisterHowToApply(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Registers the self referral.
		/// </summary>
		/// <param name="postingId">The posting id.</param>
		/// <param name="candidatePersonId">The candidate person id.</param>
		/// <param name="referralScore">The referral score.</param>
		/// <param name="referralActionType">the action type</param>
		/// <returns>
		/// A boolean indicated if the action
		/// </returns>
		/// <exception cref="ServiceCallException"></exception>
		public bool RegisterSelfReferral(long postingId, long candidatePersonId, int referralScore,
			ActionTypes referralActionType = ActionTypes.ExternalReferral)
		{
			var request = new RegisterSelfReferralRequest
			{
				PostingId = postingId,
				CandidatePersonId = candidatePersonId,
				ReferralScore = referralScore,
				ReferralActionType = referralActionType
			}.Prepare(AppContext, Runtime.Settings);

			var response = CandidateService.RegisterSelfReferral(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.AlreadyLogged;
		}

		/// <summary>
		/// Shows the not what youre looking for.
		/// </summary>
		/// <param name="personId">The person id.</param>
		public void ShowNotWhatYoureLookingFor(long personId)
		{
			var request = new LogUIActionRequest {PersonId = personId}.Prepare(AppContext, Runtime.Settings);
			var response = CandidateService.ShowNotWhatYourLookingFor(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Views the job details.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="lensPostingId">The lens posting id.</param>
		public void ViewJobDetails(long? personId, string lensPostingId)
		{
			var request = new LogUIActionRequest {LensPostingId = lensPostingId, PersonId = personId}.Prepare(AppContext,
				Runtime.Settings);
			var response = CandidateService.ViewJobDetails(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Assigns to staff member.
		/// </summary>
		/// <param name="jobSeekerIds">The job seeker ids.</param>
		/// <param name="personId">The person id.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void AssignToStaffMember(List<long> jobSeekerIds, long personId)
		{
			var request =
				new UpdateAssignedStaffMemberRequest {JobSeekerIds = jobSeekerIds, PersonId = personId}.Prepare(AppContext,
					Runtime.Settings);
			var response = CandidateService.UpdateAssignedStaffMember(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

	  /// <summary>
	  /// Registers the job posting of interest sent.
	  /// </summary>
	  /// <param name="postingId">The lens posting id.</param>
	  /// <param name="personId">The person unique identifier.</param>
	  /// <param name="score">The matching score.</param>
	  /// <param name="createdBy">Current user id</param>
	  /// <param name="isRecommendation">Whether this is a job recommendation from Assist</param>
	  /// <param name="queueRecommendation">Whether this job recommendation should be queued</param>
	  public void RegisterJobPostingOfInterestSent(string postingId, long personId, int score, long createdBy, bool isRecommendation = true, bool queueRecommendation = false)
		{
			var request = new RegisterJobPostingOfInterestSentRequest
			{
				CandidatePersonId = personId, 
				LensPostingId = postingId,
				Score = score,
				IsRecommendation = isRecommendation,
        QueueRecommendation = queueRecommendation,
        CreatedBy = createdBy
			}.Prepare(AppContext, Runtime.Settings);
			var response = CandidateService.RegisterJobPostingOfInterestSent(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);
		}

		/// <summary>
		/// Gets the postings of interest received.
		/// </summary>
		/// <param name="numberOfDays">The number of days worth of jobs to get.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<JobPostingOfInterestSentViewDto> GetPostingsOfInterestReceived(int numberOfDays)
		{
			var request = new GetPostingsOfInterestReceivedRequest
			{
				NumberOfDays = numberOfDays
			}.Prepare(AppContext, Runtime.Settings);
			var response = CandidateService.GetPostingsOfInterestReceived(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.PostingsSent;
		}

		/// <summary>
		/// Gets a postings of interest received for a specific job.
		/// </summary>
		/// <param name="lensPostingId">The number of days worth of jobs to get.</param>
		/// <returns></returns>
		public JobPostingOfInterestSentViewDto GetPostingsOfInterestReceived(string lensPostingId)
		{
			var request = new GetPostingsOfInterestReceivedRequest
			{
				LensPostingId = lensPostingId
			}.Prepare(AppContext, Runtime.Settings);
			var response = CandidateService.GetPostingsOfInterestReceived(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.PostingSent;
		}

		/// <summary>
		/// Marks the application viewed.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="candidateId">The candidate id.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void MarkApplicationViewed(long jobId, long candidateId)
		{
			var request = new MarkApplicationViewedRequest {JobId = jobId, CandidateId = candidateId}.Prepare(AppContext,
				Runtime.Settings);
			var response = CandidateService.MarkApplicationViewed(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Gets the new applicant referral view paged list.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="excludeVeteranPriorityJobs">Whether to exclude veteran priority jobs if job seeker is not a veteran</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public PagedList<NewApplicantReferralViewDto> GetNewApplicantReferralViewPagedList(long personId,
			bool excludeVeteranPriorityJobs = false)
		{
			var request = new NewApplicantReferralViewRequest
			{
				Criteria = new NewApplicantReferralViewCriteria
				{
					PersonId = personId,
					ExcludeVeteranPriorityJobs = excludeVeteranPriorityJobs,
					FetchOption = CriteriaBase.FetchOptions.PagedList
				}
			}.Prepare(AppContext, Runtime.Settings);
			var response = CandidateService.GetNewApplicantReferralView(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.NewApplicantReferralViewPaged;
		}

		/// <summary>
		/// Gets the latest survey results for a job seeker.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public JobSeekerSurveyDto GetLatestJobSeekerSurvey(long? personId)
		{
			var request = new JobSeekerSurveyRequest {PersonId = personId}.Prepare(AppContext, Runtime.Settings);
			var response = CandidateService.GetLatestSurvey(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.JobSeekerSurvey;
		}

		/// <summary>
		/// Saves the job seeker survey.
		/// </summary>
		/// <param name="survey">The survey.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public JobSeekerSurveyDto SaveJobSeekerSurvey(JobSeekerSurveyDto survey)
		{
			var request = new JobSeekerSurveyRequest {JobSeekerSurvey = survey}.Prepare(AppContext, Runtime.Settings);
			var response = CandidateService.SaveJobSeekerSurvey(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.JobSeekerSurvey;
		}

		/// <summary>
		/// Gets the referral view list.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="numberOfReferrals">The number of referrals.</param>
		/// <param name="postingId">Restrict to a single posting.</param>
		/// <param name="dayLimit">.</param>
		/// <param name="ignoreVeteranPriorityService">Whether to ignore the veteran priority service.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<ReferralViewDto> GetReferralViewList(long personId, int? numberOfReferrals, long? postingId = null,
			int? dayLimit = null, bool ignoreVeteranPriorityService = false)
		{
			var request = new GetReferralViewRequest
			{
				PersonId = personId,
				NumberOfReferrals = numberOfReferrals,
				PostingId = postingId,
				DayLimit = dayLimit,
				IgnoreVeteranPriorityService = ignoreVeteranPriorityService
			}.Prepare(AppContext, Runtime.Settings);

			var response = CandidateService.GetReferralViews(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.ReferralViews;
		}

		/// <summary>
		/// Gets the campuses.
		/// </summary>
		/// <returns>A list of all campuses</returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<CampusDto> GetCampuses()
		{
			var request = new GetCampusesRequest().Prepare(AppContext, Runtime.Settings);

			var response = CandidateService.GetCampuses(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.Campuses;
		}

		/// <summary>
		/// Gets a specific campus.
		/// </summary>
		/// <param name="campusId">The id of the campus required.</param>
		/// <returns>
		/// The campus
		/// </returns>
		/// <exception cref="ServiceCallException"></exception>
		public CampusDto GetCampus(long campusId)
		{
			var request = new GetCampusesRequest
			{
				CampusId = campusId
			}.Prepare(AppContext, Runtime.Settings);

			var response = CandidateService.GetCampuses(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.Campus;
		}

		/// <summary>
		/// Gets the UI claimant status.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="checkExternalRepository">Whether to check the external repository.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public bool GetUiClaimantStatus(long personId, bool checkExternalRepository = true)
		{
			var request = new UiClaimantStatusRequest
			{
				PersonId = personId,
				CheckExternalSystem = checkExternalRepository
			}.Prepare(AppContext, Runtime.Settings);

			var response = CandidateService.GetUiClaimantStatus(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.IsUiClaimant;
		}

		/// <summary>
		/// Gets the invitees for job.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns>PagedList&lt;ResumeView&gt;.</returns>
		/// <exception cref="ServiceCallException"></exception>
		public PagedList<ResumeView> GetInviteesForJob(InviteesResumeViewCriteria criteria)
		{
			var request = new InviteesForJobRequest {InviteesCriteria = criteria}.Prepare(AppContext, Runtime.Settings);

			var response = CandidateService.GetInviteesForJob(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.InviteesResumeViews;
		}

		/// <summary>
		/// Determines whether [is candidate exisiting job invitee] [the specified job identifier].
		/// </summary>
		/// <param name="jobId">The job identifier.</param>
		/// <param name="candidateId">The candidate identifier.</param>
		/// <returns></returns>
		public ExistingApplicant IsCandidateExistingJobInviteeOrApplicant(long jobId, long candidateId)
		{
			var criteria = new IsCandidateExistingInviteeOrApplicantCriteria
			{
				JobId = jobId,
				CandidateId = candidateId
			};
			var request =
				new IsCandidateExistingInviteeOrApplicantRequest {IsCandidateInviteeCriteria = criteria}.Prepare(AppContext,
					Runtime.Settings);

			var response = CandidateService.IsCandidateExisitingJobInviteeOrApplicant(request);

			return new ExistingApplicant
			{
				Exists = response.IsCandidateExistingJobInviteeOrApplicant,
				IsApplicationPendingApproval = response.IsCandidateExistingApplicantToScreenedJobPendingApproval
			};
		}

		/// <summary>
		/// Sets the MSFW verified status.
		/// </summary>
		/// <param name="personId">The Id of person</param>
		/// <param name="isVerified">Whether the person is verified as MSFW</param>
		/// <param name="questions">Any questions to be set.</param>
		public void VerifyMSFWStatus(long personId, bool isVerified, List<long> questions = null)
		{
			var request = new VerifyMSFWStatusRequest()
			{
				PersonId = personId,
				IsVerified = isVerified,
				MSFWQuestions = questions
			}.Prepare(AppContext, Runtime.Settings);

			var response = CandidateService.VerifyMSFWStatus(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Checks if candidate is invited to apply for the specified job
		/// </summary>
		/// <param name="jobId"></param>
		/// <param name="personId"></param>
		/// <returns></returns>
		public bool IsCandidateInvitedForThisJob(long jobId, long personId)
		{
			return CandidateService.IsCandidateInvitedToApplyForThisJob(jobId, personId);
		}
	}
}
