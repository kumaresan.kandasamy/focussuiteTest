﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core.Settings.Interfaces;
using Focus.ServiceClients.Interfaces;
using Focus.Services;
using Focus.Web.Core.Models;
using Framework.ServiceLocation;

#endregion

namespace Focus.ServiceClients.InProcess
{
	public class ServiceClientRuntime : IServiceClientRuntime
	{
		private static ServicesRuntime<StructureMapServiceLocator> _servicesRuntime;

		public IAppSettings Settings { get; set; }

		/// <summary>
		/// Resets the data.
		/// </summary>
		public void ResetData() { }

		/// <summary>
		/// Starts the App.
		/// </summary>
		public void Start()
		{
			// Initialize the services
			_servicesRuntime = new ServicesRuntime<StructureMapServiceLocator>(ServiceRuntimeEnvironment.Web, new StructureMapServiceLocator());
			_servicesRuntime.Start();

			PopulateSettings();
		}

		/// <summary>
		/// Shutdowns the App.
		/// </summary>
		public void Shutdown()
		{
			_servicesRuntime.Shutdown();
		}

		/// <summary>
		/// Begins the request.
		/// </summary>
		public void BeginRequest()
		{
			_servicesRuntime.BeginRequest();
		}

		/// <summary>
		/// Ends the request.
		/// </summary>
		public void EndRequest()
		{
			_servicesRuntime.EndRequest();
		}

		/// <summary>
		/// Starts session.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <returns></returns>
		/// <value>
		/// The session id.
		/// </value>
		public Guid StartSession(AppContextModel context)
		{
			var authenticationClient = new AuthenticationClient(context, this);
			return authenticationClient.StartSession();
		}

		/// <summary>
		/// Refreshes the settings.
		/// </summary>
		public void RefreshSettings()
		{
			PopulateSettings();
		}

		/// <summary>
		/// Populates the settings.
		/// </summary>
		private void PopulateSettings()
		{
			var coreClient = new CoreClient(new AppContextModel(), this);
			Settings = coreClient.GetConfiguration();
		}

		/// <summary>
		/// Accounts the client instance.
		/// </summary>
		/// <param name="appContext">The application context.</param>
		/// <returns></returns>
		public IAccountClient AccountClient(AppContextModel appContext)
		{
			return new AccountClient(appContext, this);
		}

		/// <summary>
		/// Gets an annotation client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		public IAnnotationClient AnnotationClient(AppContextModel appContext)
		{
			return new AnnotationClient(appContext, this);
		}

		/// <summary>
		/// Gets an authentication client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		public IAuthenticationClient AuthenticationClient(AppContextModel appContext)
		{
			return new AuthenticationClient(appContext, this);
		}

		/// <summary>
		/// Gets a candidate client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		public ICandidateClient CandidateClient(AppContextModel appContext)
		{
			return new CandidateClient(appContext, this);
		}

		/// <summary>
		/// Gets a core client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		public ICoreClient CoreClient(AppContextModel appContext)
		{
			return new CoreClient(appContext, this);
		}

		/// <summary>
		/// Gets an employee client.
		/// </summary>
		//// <param name="appContext">The context model.</param>
		public IEmployeeClient EmployeeClient(AppContextModel appContext)
		{
			return new EmployeeClient(appContext, this);
		}

		/// <summary>
		/// Gets an employer client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		public IEmployerClient EmployerClient(AppContextModel appContext)
		{
			return new EmployerClient(appContext, this);
		}

		/// <summary>
		/// Gets an explorer client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		public IExplorerClient ExplorerClient(AppContextModel appContext)
		{
			return new ExplorerClient(appContext, this);
		}

		/// <summary>
		/// Gets a job client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		public IJobClient JobClient(AppContextModel appContext)
		{
			return new JobClient(appContext, this);
		}

		/// <summary>
		/// Gets an occupation client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		public IOccupationClient OccupationClient(AppContextModel appContext)
		{
			return new OccupationClient(appContext, this);
		}

		/// <summary>
		/// Gets an organization client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		public IOrganizationClient OrganizationClient(AppContextModel appContext)
		{
			return new OrganizationClient(appContext, this);
		}

		/// <summary>
		/// Gets a posting client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		public IPostingClient PostingClient(AppContextModel appContext)
		{
			return new PostingClient(appContext, this);
		}

		/// <summary>
		/// Gets a processor client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		public IProcessorClient ProcessorClient(AppContextModel appContext)
		{
			return new ProcessorClient(appContext, this);
		}

		/// <summary>
		/// Gets a report client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		public IReportClient ReportClient(AppContextModel appContext)
		{
			return new ReportClient(appContext, this);
		}

		/// <summary>
		/// Gets a resume client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		public IResumeClient ResumeClient(AppContextModel appContext)
		{
			return new ResumeClient(appContext, this);
		}

		/// <summary>
		/// Gets a search client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		public ISearchClient SearchClient(AppContextModel appContext)
		{
			return new SearchClient(appContext, this);
		}

		/// <summary>
		/// Gets a staff client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		public IStaffClient StaffClient(AppContextModel appContext)
		{
			return new StaffClient(appContext, this);
		}

		/// <summary>
		/// Gets an utility client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		public IUtilityClient UtilityClient(AppContextModel appContext)
		{
			return new UtilityClient(appContext, this);
		}
	}
}
