﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Activity;
using Focus.Core.Criteria.CaseManagement;
using Focus.Core.Criteria.CertificateLicence;
using Focus.Core.Criteria.Document;
using Focus.Core.Criteria.EducationInternship;
using Focus.Core.Criteria.IndustryClassification;
using Focus.Core.Criteria.Language;
using Focus.Core.Criteria.NoteReminder;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.EmailTemplate;
using Focus.Core.Messages;
using Focus.Core.Messages.CoreService;
using Focus.Core.Models.Assist;
using Focus.Core.Settings.Interfaces;
using Focus.Core.Views;
using Focus.ServiceClients.InProcess.Extensions;
using Focus.ServiceClients.Interfaces;
using Focus.Services.Core;
using Focus.Web.Core.Models;
using LocalisationDto = Focus.Core.DataTransferObjects.FocusCore.LocalisationDto;

using Framework.Core;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.InProcess
{
    public class CoreClient : ClientBase, ICoreClient
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CoreClient" /> class.
        /// </summary>
        /// <param name="appContext">The application context.</param>
        /// <param name="runtime">The runtime.</param>
        public CoreClient(AppContextModel appContext, ServiceClientRuntime runtime) : base(appContext, runtime) { }

        /// <summary>
        /// Gets the version info.
        /// </summary>
        /// <returns></returns>
        public string GetVersionInfo()
        {
            var request = new GetVersionInfoRequest().Prepare(AppContext, Runtime.Settings);

            var response = CoreService.GetVersionInfo(request);
            Correlate(request, response);

            return response.VersionInfo;
        }

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <returns></returns>
        public IAppSettings GetConfiguration()
        {
            var request = new GetConfigurationRequest().Prepare(AppContext, Runtime.Settings, Guid.Empty, false);

            var response = CoreService.GetConfigurationItems(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException("Unable to get the configuration because: " + response.Message, (int)response.Error, response.Exception);

            return new AppSettings(response.ConfigurationItems);
        }

        /// <summary>
        /// Gets the Configuration Items for an optional key 
        /// </summary>
        /// <param name="key">An optional key</param>
        /// <returns>A list of configuration items</returns>
        public List<ConfigurationItemView> GetConfigurationItems(string key = "")
        {
            var request = new GetConfigurationRequest().Prepare(AppContext, Runtime.Settings, Guid.Empty);

            var response = CoreService.GetConfigurationItems(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException("Unable to get the configuration because: " + response.Message, (int)response.Error, response.Exception);

            return response.ConfigurationItems.Where(ci => ci.Key == key || key.Length == 0).ToList();
        }

        /// <summary>
        /// Saves the configuration items.
        /// </summary>
        /// <param name="configurationItems">The configuration items.</param>
        public bool SaveConfigurationItems(List<ConfigurationItemDto> configurationItems)
        {
            var request = new SaveConfigurationItemsRequest().Prepare(AppContext, Runtime.Settings);
            request.ConfigurationItems = configurationItems;

            var response = CoreService.SaveConfigurationItems(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.Acknowledgement == AcknowledgementType.Success;
        }

        /// <summary>
        /// Gets the localisation dictionary.
        /// </summary>
        /// <returns></returns>
        public LocalisationDictionary GetLocalisationDictionary()
        {
            var request = new GetLocalisationDictionaryRequest().Prepare(AppContext, Runtime.Settings);

            var response = CoreService.GetLocalisationDictionary(request);
            Correlate(request, response);

            return response.LocalisationDictionary;
        }

        /// <summary>
        /// Saves changes to localisation items.
        /// </summary>
        /// <param name="items">The localisation items to update</param>
        /// <param name="culture">An optional culture</param>
        /// <returns>The updated localisation dictionary</returns>
        public LocalisationDictionary SaveLocalisationItems(List<LocalisationItemDto> items, string culture = Constants.DefaultCulture)
        {
            var request = new SaveLocalisationItemsRequest { Items = items, Culture = culture }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.SaveLocalisationItems(request);
            Correlate(request, response);

            return response.LocalisationDictionary;
        }

        /// <summary>
        /// Saves the default localisation items.
        /// </summary>
        /// <param name="items">The items.</param>
        public void SaveDefaultLocalisationItems(List<KeyValuePairSerializable<string, string>> items)
        {
            var request = new SaveDefaultLocalisationItemsRequest { Items = items }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.SaveDefaultLocalisationItems(request);
            Correlate(request, response);
        }

        /// <summary>
        /// Gets the licenses and certificates.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="listSize">Size of the list.</param>
        /// <returns></returns>
        public List<CertificateLicenseView> GetLicencesAndCertifications(string name, int listSize)
        {
            var request = new GetCertificateLicenseRequest().Prepare(AppContext, Runtime.Settings);
            request.Criteria = new CertificateLicenseCriteria { LikeName = name, ListSize = listSize };

            var response = CoreService.GetCertificateLicenses(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.CertificateLicenses.ToList();
        }

        /// <summary>
        /// Gets the licenses.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="listSize">Size of the list.</param>
        /// <returns></returns>
        public List<CertificateLicenseView> GetLicenses(string name, int listSize)
        {
            var request = new GetCertificateLicenseRequest().Prepare(AppContext, Runtime.Settings);
            request.Criteria = new CertificateLicenseCriteria { LikeName = name, IsLicence = true, ListSize = listSize };

            var response = CoreService.GetCertificateLicenses(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.CertificateLicenses.ToList();
        }

        /// <summary>
        /// Gets the certificates.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="listSize">Size of the list.</param>
        /// <returns></returns>
        public List<CertificateLicenseView> GetCertificates(string name, int listSize)
        {
            var request = new GetCertificateLicenseRequest().Prepare(AppContext, Runtime.Settings);
            request.Criteria = new CertificateLicenseCriteria { LikeName = name, IsCertificate = true, ListSize = listSize };

            var response = CoreService.GetCertificateLicenses(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.CertificateLicenses.ToList();
        }

        /// <summary>
        /// Gets the languages.
        /// </summary>
        /// <param name="language">The language like.</param>
        /// <param name="listSize">Size of the list.</param>
        /// <returns></returns>
        public List<LookupItemView> GetLanguages(string language, int listSize)
        {
            var request = new GetLanguageRequest().Prepare(AppContext, Runtime.Settings);
            request.Criteria = new LanguageCriteria { LanguageLike = language, ListSize = listSize };

            var response = CoreService.GetLanguage(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.Languages;
        }

        /// <summary>
        /// Gets the lookup.
        /// </summary>
        /// <param name="lookupType">Type of the lookup.</param>
        /// <param name="parentId">Parent id</param>
        /// <param name="key">The key.</param>
        /// <param name="prefix">An optional prefix</param>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        public IList<LookupItemView> GetLookup(LookupTypes lookupType, long parentId = 0, string key = null, string prefix = null)
        {
            var request = new GetLookupRequest().Prepare(AppContext, Runtime.Settings);
            request.LookupType = lookupType;
            request.ParentId = parentId;
            request.Key = key;
            request.Prefix = prefix;

            var response = CoreService.GetLookup(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.LookupItems;
        }

        /// <summary>
        /// Gets the generic job titles.
        /// </summary>
        /// <param name="jobTitle">The job title.</param>
        /// <param name="listSize">Size of the list.</param>
        /// <returns></returns>
        public List<string> GetGenericJobTitles(string jobTitle, int listSize)
        {
            var request = new GetGenericJobTitlesRequest().Prepare(AppContext, Runtime.Settings);
            request.JobTitle = jobTitle;
            request.ListSize = listSize;

            var response = CoreService.GetGenericJobTitles(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.JobTitles;
        }

        /// <summary>
        /// Gets the naics.
        /// </summary>
        /// <param name="naicsPrefix">Prefix of the naics.</param>
        /// <param name="listSize">Size of the list.</param>
        /// <param name="childrenOnly">Whether to get only the child NAICS</param>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        public List<NaicsView> GetNaics(string naicsPrefix, int listSize, bool childrenOnly = false)
        {
            var request = new GetNaicsRequest().Prepare(AppContext, Runtime.Settings);
            request.Prefix = naicsPrefix;
            request.ListSize = listSize;
            request.ChildrenOnly = childrenOnly;

            var response = CoreService.GetNaics(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.Naics;
        }

        /// <summary>
        /// Gets the state and county for postal code.
        /// </summary>
        /// <param name="postalCode">The postal code.</param>
        /// <param name="stateLookupId">The state lookup id.</param>
        /// <param name="countyLookupId">The county lookup id.</param>
        /// <param name="cityName">The city name</param>
        public void GetStateCityAndCountyForPostalCode(string postalCode, out long stateLookupId, out long countyLookupId, out string cityName)
        {
            stateLookupId = countyLookupId = 0;
            cityName = "";

            var request = new GetStateCityAndCountyForPostalCodeRequest().Prepare(AppContext, Runtime.Settings);
            request.PostalCode = postalCode;

            var response = CoreService.GetStateCityAndCountyForPostalCode(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            if (response.StateLookupId.HasValue) stateLookupId = response.StateLookupId.Value;
            if (response.CountyLookupId.HasValue) countyLookupId = response.CountyLookupId.Value;
            if (response.CityName.IsNotNullOrEmpty()) cityName = response.CityName;
        }

        /// <summary>
        /// Establishes the location.
        /// </summary>
        /// <param name="postalCode">The postal code.</param>
        /// <param name="cityName">Name of the city.</param>
        /// <param name="stateName">Name of the state.</param>
        /// <returns></returns>
        public PostalCodeViewDto EstablishLocation(string postalCode, string cityName, string stateName)
        {
            var request = new EstablishLocationRequest().Prepare(AppContext, Runtime.Settings);
            request.PostalCode = postalCode;
            request.City = cityName;
            request.State = stateName;

            var response = CoreService.EstablishLocation(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                return null;

            return response.Location;
        }

        /// <summary>
        /// Gets the email template preview.
        /// </summary>
        /// <param name="emailTemplateType">Type of the email template.</param>
        /// <param name="templateValues">The template values.</param>
        /// <returns></returns>
        public EmailTemplateView GetEmailTemplatePreview(EmailTemplateTypes emailTemplateType, EmailTemplateData templateValues)
        {
            var request = new EmailTemplateRequest().Prepare(AppContext, Runtime.Settings);
            request.EmailTemplateType = emailTemplateType;
            request.TemplateValues = templateValues;

            var response = CoreService.GetEmailTemplatePreview(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.EmailTemplatePreview;
        }

        /// <summary>
        /// Gets the sender email address for template.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public string GetSenderEmailAddressForTemplate(EmailTemplateTypes type)
        {
            var template = GetEmailTemplate(type);

            return GetSenderEmailAddressForTemplate(template);
        }

        /// <summary>
        /// Gets the sender email address for template.
        /// </summary>
        /// <param name="template">The type.</param>
        /// <returns></returns>
        public string GetSenderEmailAddressForTemplate(EmailTemplateDto template)
        {
            string emailAddress;

            switch (template.SenderEmailType)
            {
                case SenderEmailTypes.ClientSpecific:
                    emailAddress = template.ClientSpecificEmailAddress;
                    break;

                case SenderEmailTypes.StaffUser:
                    emailAddress = AppContext.User.EmailAddress;
                    break;

                default:
                    emailAddress = Runtime.Settings.SystemDefaultSenderEmailAddress;
                    break;
            }

            return emailAddress;
        }

        /// <summary>
        /// Gets the email template.
        /// </summary>
        /// <param name="emailTemplateType">Type of the email template.</param>
        /// <returns></returns>
        public EmailTemplateDto GetEmailTemplate(EmailTemplateTypes emailTemplateType)
        {
            var request = new EmailTemplateRequest().Prepare(AppContext, Runtime.Settings);
            request.EmailTemplateType = emailTemplateType;

            var response = CoreService.GetEmailTemplate(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.EmailTemplate;
        }

        /// <summary>
        /// Gets the localisation item.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public LocalisationDictionaryEntry GetLocalisationItem(string key)
        {
            var request = new GetLocalisationDictionaryRequest().Prepare(AppContext, Runtime.Settings);
            request.Key = key;

            var response = CoreService.GetLocalisationItem(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.LocalisationDictionaryEntry;
        }

        /// <summary>
        /// Saves the email template.
        /// </summary>
        /// <param name="emailTemplate">The email template.</param>
        public void SaveEmailTemplate(EmailTemplateDto emailTemplate)
        {
            var request = new SaveEmailTemplateRequest().Prepare(AppContext, Runtime.Settings);
            request.EmailTemplate = emailTemplate;

            var response = CoreService.SaveEmailTemplate(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }


        /// <summary>
        /// Gets the emails sent regarding referrals
        /// </summary>
        /// <param name="employeeId">The id of the employee.</param>
        /// <param name="applicationId">The id of the application.</param>
        /// <param name="approvalStatus">The approval status relating to the email</param>
        /// <param name="emailsRequired">The number of emails required.</param>
        /// <param name="jobId">the job posting Id</param>
        /// <returns>A list of emails (body text only) sent</returns>
        public List<ReferralEmailDto> GetReferralEmails(long? employeeId = null, long? applicationId = null, ApprovalStatuses? approvalStatus = null, int? emailsRequired = null, long? jobId = null)
        {
            var request = new GetReferralEmailRequest
            {
                EmployeeId = employeeId,
                ApplicationId = applicationId,
                ApprovalStatus = approvalStatus,
                EmailsRequired = emailsRequired,
                JobId = jobId
            }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.GetReferralEmails(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.EmailsSent;
        }

        /// <summary>
        /// Saves an email regarding a referral.
        /// </summary>
        /// <param name="emailSubject">The email subject.</param>
        /// <param name="emailBody">The email body.</param>
        /// <param name="approvalStatus">The approval status (if relevant).</param>
        /// <param name="employeeId">The employee id.</param>
        /// <param name="applicationId">The application id.</param>
        /// <param name="jobId">the job posting id</param>
        /// <exception cref="ServiceCallException"></exception>
        public void SaveReferralEmail(string emailSubject, string emailBody, ApprovalStatuses approvalStatus, long? employeeId = null, long? applicationId = null, long? jobId = null)
        {
            var request = new SaveReferralEmailRequest().Prepare(AppContext, Runtime.Settings);
            request.EmployeeId = employeeId;
            request.ApplicationId = applicationId;
            request.EmailSubject = emailSubject;
            request.EmailBody = emailBody;
            request.ApprovalStatus = approvalStatus;
            request.JobId = jobId;

            var response = CoreService.SaveReferralEmail(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }

        /// <summary>
        /// Profiles the event.
        /// </summary>
        /// <param name="server">The server.</param>
        /// <param name="inTime">The in time.</param>
        /// <param name="outTime">The out time.</param>
        /// <param name="milliseconds">The milliseconds.</param>
        /// <param name="declaringType">Type of the declaring.</param>
        /// <param name="method">The method.</param>
        /// <param name="parameters">The parameters.</param>
        public void ProfileEvent(string server, DateTime inTime, DateTime outTime, long milliseconds, string declaringType, string method, string parameters)
        {
            var request = new ProfilerEventRequest().Prepare(AppContext, Runtime.Settings);
            request.Server = server;
            request.InTime = inTime;
            request.OutTime = outTime;
            request.Milliseconds = milliseconds;
            request.DeclaringType = declaringType;
            request.Method = method;
            request.Parameters = parameters;

            var response = CoreService.ProfileEvent(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }

        /// <summary>
        /// Gets the messages.
        /// </summary>
        /// <param name="module">The module to get the messages for</param>
        /// <returns></returns>
        public List<MessageView> GetMessages(FocusModules module)
        {
            var request = new GetMessageRequest().Prepare(AppContext, Runtime.Settings);
            request.Module = module;

            var response = CoreService.GetMessage(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return (response.Messages.IsNotNull() ? response.Messages : new List<MessageView>());
        }

        /// <summary>
        /// Gets recent messages.
        /// </summary>
        /// <param name="orderBy">The order of the messages</param>
        /// <returns></returns>
        public List<MessageView> GetRecentMessages(string orderBy)
        {
            var request = new GetMessageRequest
            {
                OrderBy = orderBy
            }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.GetRecentMessages(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return (response.Messages.IsNotNull() ? response.Messages : new List<MessageView>());
        }

        /// <summary>
        /// Expires a messages.
        /// </summary>
        /// <param name="messageId">The id of the message to expire</param>
        /// <returns></returns>
        public void ExpireMessage(long messageId)
        {
            var request = new DismissMessageRequest().Prepare(AppContext, Runtime.Settings);
            request.MessageId = messageId;

            var response = CoreService.ExpireMessage(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }

        /// <summary>
        /// Dismisses the message.
        /// </summary>
        /// <param name="messageId">The message id.</param>
        public void DismissMessage(long messageId)
        {
            var request = new DismissMessageRequest().Prepare(AppContext, Runtime.Settings);
            request.MessageId = messageId;

            var response = CoreService.DismissMessage(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }

        /// <summary>
        /// Creates the homepage alert.
        /// </summary>
        /// <param name="homepageAlert">The homepage alert.</param>
        public void CreateHomepageAlert(HomepageAlertView homepageAlert)
        {
            var homepageAlerts = new List<HomepageAlertView> { homepageAlert };
            CreateHomepageAlert(homepageAlerts);
        }

        /// <summary>
        /// Creates the homepage alert.
        /// </summary>
        /// <param name="homepageAlerts">The homepage alerts.</param>
        public void CreateHomepageAlert(List<HomepageAlertView> homepageAlerts)
        {
            var request = new CreateHomepageAlertRequest().Prepare(AppContext, Runtime.Settings);
            request.HomepageAlerts = homepageAlerts;

            var response = CoreService.CreateHomepageAlert(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }

        /// <summary>
        /// Deletes the saved message.
        /// </summary>
        /// <param name="savedMessageId">The saved message id.</param>
        public void DeleteSavedMessage(long savedMessageId)
        {
            var request = new DeleteSavedMessageRequest().Prepare(AppContext, Runtime.Settings);
            request.SavedMessageId = savedMessageId;

            var response = CoreService.DeleteSavedMessage(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }

        /// <summary>
        /// Gets the non default cultures.
        /// </summary>
        /// <returns></returns>
        public List<LocalisationDto> GetNonDefaultCultures()
        {
            var request = new GetLocalisationRequest().Prepare(AppContext, Runtime.Settings);

            var response = CoreService.GetLocalisation(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.Localisations.Where(x => x.Culture != Constants.DefaultCulture).ToList();
        }

        /// <summary>
        /// Gets the default culture.
        /// </summary>
        /// <returns></returns>
        public LocalisationDto GetDefaultCulture()
        {
            var request = new GetLocalisationRequest().Prepare(AppContext, Runtime.Settings);

            var response = CoreService.GetLocalisation(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.Localisations.FirstOrDefault(x => x.Culture == Constants.DefaultCulture);
        }

        /// <summary>
        /// Saves the message texts.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageTexts">The message texts.</param>
        public void SaveMessageTexts(SavedMessageDto message, List<SavedMessageTextDto> messageTexts)
        {
            var request = new SaveMessageTextsRequest().Prepare(AppContext, Runtime.Settings);
            request.Message = message;
            request.MessageTexts = messageTexts;

            var response = CoreService.SaveMessageTexts(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }

        /// <summary>
        /// Gets the saved messages.
        /// </summary>
        /// <param name="audience">The audience.</param>
        /// <returns></returns>
        public List<SavedMessageDto> GetSavedMessages(MessageAudiences audience)
        {
            var request = new GetSavedMessageRequest().Prepare(AppContext, Runtime.Settings);
            request.Audience = audience;

            var response = CoreService.GetSavedMessage(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.SavedMessages;
        }

        /// <summary>
        /// Gets the saved message texts.
        /// </summary>
        /// <param name="savedMessageId">The saved message id.</param>
        /// <returns></returns>
        public List<SavedMessageTextDto> GetSavedMessageTexts(long savedMessageId)
        {
            var request = new GetSavedMessageTextsRequest().Prepare(AppContext, Runtime.Settings);
            request.SavedMessageId = savedMessageId;

            var response = CoreService.GetSavedMessageTexts(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.SavedMessageTexts;
        }

        /// <summary>
        /// Gets the application image.
        /// </summary>
        /// <param name="imageType">Type of the image.</param>
        /// <returns></returns>
        public ApplicationImageDto GetApplicationImage(ApplicationImageTypes imageType)
        {
            var request = new ApplicationImageRequest().Prepare(AppContext, Runtime.Settings);
            request.Type = imageType;

            var response = CoreService.GetApplicationImage(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.ApplicationImage;
        }

        /// <summary>
        /// Saves the application image.
        /// </summary>
        /// <param name="applicationImage">The application image.</param>
        /// <returns></returns>
        public void SaveApplicationImage(ApplicationImageDto applicationImage)
        {
            var request = new SaveApplicationImageRequest().Prepare(AppContext, Runtime.Settings);
            request.ApplicationImage = applicationImage;

            var response = CoreService.SaveApplicationImage(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }

        /// <summary>
        /// Sends the feedback.
        /// </summary>
        /// <param name="subject">The subject.</param>
        /// <param name="detail">The detail.</param>
        public void SendFeedback(string subject, string detail)
        {
            var request = new FeedbackRequest().Prepare(AppContext, Runtime.Settings);
            request.Subject = subject;
            request.Detail = detail;

            var response = CoreService.SendFeedback(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }

        /// <summary>
        /// Saves the note reminder.
        /// </summary>
        /// <param name="noteReminder">The note reminder.</param>
        /// <param name="recipients">Reminder recipients</param>
        public NoteReminderDto SaveNoteReminder(NoteReminderDto noteReminder, List<NoteReminderRecipientDto> recipients)
        {
            var request = new SaveNoteReminderRequest().Prepare(AppContext, Runtime.Settings);
            request.NoteReminder = noteReminder;
            request.ReminderRecipients = recipients;

            var response = CoreService.SaveNoteReminder(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.NoteReminder;
        }

        /// <summary>
        /// Gets the notes and reminders.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public PagedList<NoteReminderViewDto> GetNotesReminders(NoteReminderCriteria criteria)
        {
            var request = new NoteReminderViewRequest().Prepare(AppContext, Runtime.Settings);
            request.Criteria = criteria;

            var response = CoreService.GetNoteReminder(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.NoteRemindersPaged;
        }

        /// <summary>
        /// Gets the notes and reminders.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public List<NoteReminderViewDto> GetAllNotesReminders(NoteReminderCriteria criteria)
        {
            var request = new NoteReminderViewRequest().Prepare(AppContext, Runtime.Settings);
            request.Criteria = criteria;

            var response = CoreService.GetNoteReminder(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.NoteReminders;
        }

        /// <summary>
        /// Sends the reminder.
        /// </summary>
        /// <param name="noteReminderId">The note reminder id.</param>
        public void SendReminder(long noteReminderId)
        {
            var request = new SendReminderRequest().Prepare(AppContext, Runtime.Settings);
            request.Id = noteReminderId;

            var response = CoreService.SendReminder(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }

        /// <summary>
        /// Gets the industry classification lookup.
        /// </summary>
        /// <param name="parentId">The parent id.</param>
        /// <param name="codeLength">Length of the code.</param>
        /// <returns></returns>
        public Dictionary<string, string> GetIndustryClassificationLookup(long? parentId, int? codeLength = null)
        {
            var request = new IndustryClassificationRequest().Prepare(AppContext, Runtime.Settings);
            request.Criteria = new IndustryClassificationCriteria
            {
                ParentId = parentId,
                CodeLength = codeLength,
                FetchOption = CriteriaBase.FetchOptions.Lookup
            };

            var response = CoreService.GetIndustryClassification(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.IndustryClassificationLookup;
        }

        /// <summary>
        /// Gets the industry classification.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public NAICSDto GetIndustryClassification(long id)
        {
            var request = new IndustryClassificationRequest().Prepare(AppContext, Runtime.Settings);
            request.Criteria = new IndustryClassificationCriteria
            {
                Id = id,
                FetchOption = CriteriaBase.FetchOptions.Single
            };

            var response = CoreService.GetIndustryClassification(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.IndustryClassification;
        }

        /// <summary>
        /// Gets the industry classification.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        public NAICSDto GetIndustryClassification(string code)
        {
            var request = new IndustryClassificationRequest().Prepare(AppContext, Runtime.Settings);
            request.Criteria = new IndustryClassificationCriteria
            {
                Code = code,
                FetchOption = CriteriaBase.FetchOptions.Single
            };

            var response = CoreService.GetIndustryClassification(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.IndustryClassification;
        }

        /// <summary>
        /// Gets the activity categories.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        public List<ActivityCategoryDto> GetActivityCategories(ActivityType type)
        {
            var request = new GetActivityCategoriesRequest { ActivityType = type }.Prepare(AppContext, Runtime.Settings);
            var response = CoreService.GetActivityCategories(request);

            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.ActivityCategories;
        }

        /// <summary>
        /// Saves changes to an activity category
        /// </summary>
        /// <param name="type">The type of the category (Employer or Job Seeker)</param>
        /// <param name="name">The name of the category</param>
        /// <param name="activityCategoryId">The Id if this is an existing category</param>
        /// <returns></returns>
        public bool SaveActivityCategory(ActivityType type, string name, long? activityCategoryId = null)
        {
            var request = new SaveActivityCategoryRequest
            {
                Id = activityCategoryId,
                ActivityType = type,
                Name = name
            }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.SaveActivityCategory(request);

            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.Saved;
        }

        /// <summary>
        /// Deletes an activity category
        /// </summary>
        /// <param name="activityCategoryId">The Id of the category</param>
        /// <returns></returns>
        public bool DeleteActivityCategory(long activityCategoryId)
        {
            var request = new DeleteActivityCategoryRequest { Id = activityCategoryId }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.DeleteActivityCategory(request);

            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.Deleted;
        }

        /// <summary>
        /// Gets the activities.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        public PagedList<ActivityViewDto> GetActivitiesPaged(ActivityCriteria criteria)
        {
            criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;
            var request = new GetActivitiesRequest { Criteria = criteria }.Prepare(AppContext, Runtime.Settings);
            var response = CoreService.GetActivities(request);

            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.ActivitiesPaged;
        }

        /// <summary>
        /// Gets all activities.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        public ActivityViewDto[] GetAllActivities()
        {
            var request = new GetActivitiesRequest { Criteria = new ActivityCriteria { FetchOption = CriteriaBase.FetchOptions.List } }.Prepare(AppContext, Runtime.Settings);
            var response = CoreService.GetActivities(request);

            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.Activities.ToArray();
        }

        /// <summary>
        /// Gets the activities look up.
        /// </summary>
        /// <param name="categoryId">The category id.</param>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        public Dictionary<long, string> GetActivitiesLookUp(long categoryId)
        {
            var criteria = new ActivityCriteria
            {
                ActivityCategoryId = categoryId,
                FetchOption = CriteriaBase.FetchOptions.Lookup,
                VisibleOnly = true
            };

            var request = new GetActivitiesRequest { Criteria = criteria }.Prepare(AppContext, Runtime.Settings);
            var response = CoreService.GetActivities(request);

            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.ActivitiesLookUp;
        }

        /// <summary>
        /// Gets the education internship categories.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        public List<EducationInternshipCategoryDto> GetEducationInternshipCategories()
        {
            return GetEducationInternshipCategories(false);
        }

        /// <summary>
        /// Gets the education internship categories.
        /// </summary>
        /// <param name="includeSkills">Specifies whether to include each category's skills.</param>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        public List<EducationInternshipCategoryDto> GetEducationInternshipCategories(bool includeSkills)
        {
            var request = new GetEducationInternshipCategoriesRequest { IncludeSkills = includeSkills };
            var response = CoreService.GetEducationInternshipCategories(request.Prepare(AppContext, Runtime.Settings));

            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.EducationInternshipCategories;
        }

        /// <summary>
        /// Gets the education internship statements.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        public List<EducationInternshipStatementDto> GetEducationInternshipStatements(EducationInternshipStatementCriteria criteria)
        {
            var request = new GetEducationInternshipStatementsRequest { Criteria = criteria }.Prepare(AppContext, Runtime.Settings);
            var response = CoreService.GetEducationInternshipStatements(request);

            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.EducationInternshipStatements;
        }

        /// <summary>
        /// Gets the activity.
        /// </summary>
        /// <param name="activityId">The activity id.</param>
        /// <returns></returns>
        public ActivityViewDto GetActivity(long activityId)
        {

            var criteria = new ActivityCriteria
            {
                ActivityId = activityId,
                FetchOption = CriteriaBase.FetchOptions.Single
            };

            var request = new GetActivitiesRequest { Criteria = criteria }.Prepare(AppContext, Runtime.Settings);
            var response = CoreService.GetActivities(request);

            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.Activity;
        }

        /// <summary>
        /// Saves the activities.
        /// </summary>
        /// <param name="activities">The activities.</param>
        /// <exception cref="ServiceCallException"></exception>
        public string SaveActivities(List<ActivityViewDto> activities)
        {
            var request = new SaveActivitiesRequest { Activities = activities }.Prepare(AppContext, Runtime.Settings);
            var response = CoreService.SaveActivities(request);
            Correlate(request, response);

            return response.Message;
        }

        /// <summary>
        /// Saves changes to an activity
        /// </summary>
        /// <param name="activityCategoryId">The id of the category</param>
        /// <param name="name">The name of the activity</param>
        /// <param name="externalId">The external id.</param>
        /// <param name="visible">Whether the activity is visible.</param>
        /// <param name="activityId">The Id if this is an existing category.</param>
        /// <returns></returns>
        public bool SaveActivity(long activityCategoryId, string name, long externalId, bool visible, long? activityId = null)
        {
            var request = new SaveActivitiesRequest
            {
                Id = activityId,
                ActivityCategoryId = activityCategoryId,
                Name = name,
                Visible = visible,
                ExternalId = externalId
            }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.SaveActivity(request);

            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.Saved;
        }

        /// <summary>
        /// Saves an activity
        /// </summary>
        /// <param name="activityId">The Id of the activity</param>
        /// <returns></returns>
        public bool DeleteActivity(long activityId)
        {
            var request = new DeleteActivityRequest { Id = activityId }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.DeleteActivity(request);

            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.Deleted;
        }

        /// <summary>
        /// Gets the action event.
        /// </summary>
        /// <param name="actionEventId">The Id.</param>
        /// <returns></returns>
        public DateTime GetActionEventDate(long id)
        {
            var request = new GetActionEventRequest { ActionEventId = id }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.GetActionEvent(request);

            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.ActionEvent.ActionedOn;
        }


        ///<summary>
        ///Checks the last action before activating an account
        ///</summary>
        ///<param name="PersonId">The Id</param>
        ///<returns></returns>
        public bool CheckLastAction(long? id)
        {
            var request = new CheckLastActionRequest { PersonId = id }.Prepare(AppContext, Runtime.Settings);
            var response = CoreService.CheckLastAction(request);
            Correlate(request, response);
            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
            return response.IsAccountActivated;
        }

        /// <summary>
        /// Backdates the action event.
        /// </summary>
        /// <param name="id">The Id.</param>
        /// <param name="backToDate">The date to backed to.</param>
        public void BackdateActionEvent(long id, DateTime backToDate, string activityTypeId)
        {
            var request = new BackdateActionEventRequest { activityId = id, backdateActivityTo = backToDate, activityTypeId = activityTypeId }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.BackdateActionEvent(request);

            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }

        public bool DeleteActionEvent(long id, string activityTypeId)
        {
            var request = new DeleteActionEventRequest { actionId = id, activityTypeId = activityTypeId }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.DeleteActionEvent(request);

            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.isDeleted;
        }

        /// <summary>
        /// Saves the self service activity.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="actingOnBehalfOfUserId">The userId of the person who the action is being carried out on.</param>
        /// <param name="actingOnBehalfOfPersonId">The person of the person who the action is being carried out on.</param>
        /// <exception cref="ServiceCallException"></exception>
        public void SaveSelfService(ActionTypes action, long? actingOnBehalfOfUserId = null, long? actingOnBehalfOfPersonId = null)
        {
            var request = new SaveSelfServiceRequest
            {
                Action = action,
                ActingOnBehalfOfUserId = actingOnBehalfOfUserId,
                ActingOnBehalfOfPersonId = actingOnBehalfOfPersonId
            }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.SaveSelfService(request);

            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }

        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="to">To.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <param name="isHtml">if set to <c>true</c> [is HTML].</param>
        /// <param name="attachment">The attachment.</param>
        /// <param name="attachmentName">Name of the attachment.</param>
        /// <param name="detectUrl">Whether to detect URLs in the body and convert to hyperlinks</param>
        /// <exception cref="ServiceCallException"></exception>
        public void SendEmail(string to, string subject, string body, bool isHtml = false, byte[] attachment = null, string attachmentName = null, bool detectUrl = false, DateTime? sendByFutureDate = null)
        {
            var request =
              new SendEmailRequest
              {
                  EmailToAddresses = to,
                  EmailSubject = subject,
                  EmailBody = body,
                  IsHtml = isHtml,
                  Attachment = attachment,
                  AttachmentName = attachmentName,
                  DetectUrl = detectUrl,
                  SendOnFutureDate = sendByFutureDate
              }.Prepare(AppContext, Runtime.Settings);
            var response = CoreService.SendEmail(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }

        public void SendEmailFromTemplate(string to, EmailTemplateTypes type, EmailTemplateData data, bool isHtml = false, byte[] attachment = null, string attachmentName = null, bool detectUrl = false, string cc = null, string bcc = null, bool sendAsync = false)
        {
            var request =
                new SendEmailFromTemplateRequest
                {
                    EmailToAddresses = to,
                    CC = cc,
                    BCC = bcc,
                    TemplateType = type,
                    TemplateData = data,
                    SendAsync = sendAsync,
                    IsHtml = isHtml,
                    Attachment = attachment,
                    AttachmentName = attachmentName,
                    DetectUrl = detectUrl,
                }.Prepare(AppContext, Runtime.Settings);
            var response = CoreService.SendEmailFromTemplate(request);
            Correlate(request, response);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }

        /// <summary>
        /// Gets the job name from R onet.
        /// </summary>
        /// <param name="roNet">The ro net.</param>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        public string GetJobNameFromROnet(string roNet)
        {
            var request = new JobNameRequest { RONet = roNet }.Prepare(AppContext, Runtime.Settings);
            var response = CoreService.GetJobNameFromROnet(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.JobName;
        }

        public string GetCareerAreaName(long careerAreaId)
        {
            var request = new CareerAreaNameRequest { CareerAreaId = careerAreaId }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.GetCareerAreaName(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.CareerAreaName;
        }

        #region Session Helpers

        public T GetSessionValue<T>(string key, T defaultValue = default(T), IEnumerable<Type> knownTypes = null)
        {
            var request = new SessionRequest<T>
            {
                Key = key,
                Value = defaultValue,
                UseValueAsDefault = true,
                KnownTypes = knownTypes
            }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.GetSessionValue(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.Value;
        }

        public T SetSessionValue<T>(string key, T value, IEnumerable<Type> knownTypes = null)
        {
            var request = new SessionRequest<T>
            {
                Key = key,
                Value = value,
                KnownTypes = knownTypes
            }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.SetSessionValue(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.Value;
        }

        public void RemoveSessionValue(string key)
        {
            var request = new SessionRequest { Key = key }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.RemoveSessionValue(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }

        public void ClearSession()
        {
            var request = new SessionRequest().Prepare(AppContext, Runtime.Settings);

            var response = CoreService.ClearSession(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }
        #endregion

        #region Upload File Services

        /// <summary>
        /// Gets the validation schema
        /// </summary>
        /// <param name="schemaType">The schema type</param>
        /// <returns>The response containing the csv template</returns>
        public XDocument GetValidationSchema(ValidationSchema schemaType)
        {
            var request = new ValidationSchemaRequest
            {
                SchemaType = schemaType,
                GetTemplate = false
            }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.GetValidationSchema(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.ValidationSchema;
        }

        /// <summary>
        /// Gets the csv template for a specific schema type
        /// </summary>
        /// <param name="schemaType">The schema type</param>
        /// <returns>The response containing the csv template</returns>
        public string GetCSVTemplateForSchema(ValidationSchema schemaType)
        {
            var request = new ValidationSchemaRequest
            {
                SchemaType = schemaType,
                GetTemplate = true
            }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.GetCSVTemplateForSchema(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.Template;
        }

        /// <summary>
        /// Validates for a file against a specific schema type
        /// </summary>
        /// <param name="fileName">The file name.</param>
        /// <param name="schemaType">The schema type</param>
        /// <param name="fileBytes">The file bytes.</param>
        /// <param name="fileType">The type of the file.</param>
        /// <param name="customInfo">Information specific to the upload.</param>
        /// <param name="ignoreHeader">Whether to ignore the header record.</param>
        /// <returns>The upload file id</returns>
        public long ValidateUploadFile(string fileName, ValidationSchema schemaType, byte[] fileBytes, UploadFileType fileType, Dictionary<string, string> customInfo, bool ignoreHeader)
        {
            var request = new UploadFileValidationRequest
            {
                FileName = fileName,
                SchemaType = schemaType,
                FileBytes = fileBytes,
                FileType = fileType,
                CustomInformation = customInfo,
                IgnoreHeader = ignoreHeader
            }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.ValidateUploadFile(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.UploadFileId;
        }

        /// <summary>
        /// Gets the processing of an upload file
        /// </summary>
        /// <param name="uploadFileId">The file id</param>
        /// <returns>The processing status of the file</returns>
        public UploadFileProcessingState GetUploadFileProcessingState(long uploadFileId)
        {
            var request = new UploadFileDetailsRequest
            {
                UploadFileId = uploadFileId
            }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.GetUploadFileProcessingState(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.ProcessingState;
        }

        /// <summary>
        /// Gets the details of an upload file
        /// </summary>
        /// <param name="uploadFileId">The file id</param>
        /// <param name="exceptionsOnly">Whether to only return rows in exception</param>
        /// <param name="basicDataOnly">Whether to only return basic data only</param>
        /// <returns>
        /// The validation status of the file
        /// </returns>
        /// <exception cref="ServiceCallException"></exception>
        public List<UploadRecordDto> GetUploadFileDetails(long uploadFileId, bool exceptionsOnly, bool basicDataOnly = false)
        {
            var request = new UploadFileDetailsRequest
            {
                UploadFileId = uploadFileId,
                ExceptionsOnly = exceptionsOnly,
                BasicDataOnly = basicDataOnly
            }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.GetUploadFileDetails(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.ValidationResults;
        }

        /// <summary>
        /// Commits the upload file after validation
        /// </summary>
        /// <param name="uploadFileId">The file id</param>
        public void CommitUploadFile(long uploadFileId)
        {
            var request = new UploadFileCommitRequest
            {
                UploadFileId = uploadFileId
            }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.CommitUploadFile(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }

        #endregion

        #region Integation

        /// <summary>
        /// Gets the case management records.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns>A paged list of records</returns>
        public PagedList<FailedIntegrationMessageDto> GetCaseManagementErrorsPagedList(CaseManagementCriteria criteria)
        {
            var request = new CaseManagementRequest().Prepare(AppContext, Runtime.Settings);

            criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;
            request.Criteria = criteria;

            var response = CoreService.GetCaseManagementRecords(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.CaseManagementMessagesPaged;
        }

        /// <summary>
        /// Gets the case management error.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// The record with the specified identifier
        /// </returns>
        /// <exception cref="ServiceCallException"></exception>
        public FailedIntegrationMessageDto GetCaseManagementError(long id)
        {
            var request = new CaseManagementRequest().Prepare(AppContext, Runtime.Settings);
            request.FailedIntegrationMessageId = id;
            var response = CoreService.GetCaseManagementRecord(request);
            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
            return response.CaseManagementMessage;
        }

        /// <summary>
        /// Ignores a case manangement record
        /// </summary>
        /// <param name="id">The id of the record to ignore</param>
        /// <returns>A boolean indicating success or failure</returns>
        public bool IgnoreCaseManagementMessage(long id)
        {
            var request = new CaseManagementCommandRequest
            {
                Id = id
            }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.IgnoreCaseManagementMessage(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.Success;
        }

        /// <summary>
        /// Resends a case manangement record
        /// </summary>
        /// <param name="id">The id of the record to ignore</param>
        /// <returns>A boolean indicating success or failure</returns>
        public bool ResendCaseManagementMessage(long id)
        {
            var request = new CaseManagementCommandRequest
            {
                Id = id
            }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.ResendCaseManagementMessage(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.Success;
        }

        /// <summary>
        /// Gets the integration request responses.
        /// </summary>
        /// <param name="caseManagementErrorId">The case management error identifier.</param>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        public List<IntegrationRequestResponseMessagesDto> GetIntegrationRequestResponses(long caseManagementErrorId)
        {
            var request = new GetIntegrationRequestResponsesRequest { CaseManagementErrorId = caseManagementErrorId }.Prepare(AppContext, Runtime.Settings);

            var response = CoreService.GetIntegrationRequestResponses(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.RequestResponseMessages;
        }

        #endregion

        #region Documents

        /// <summary>
        /// Gets the documents.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public List<DocumentDto> GetDocuments(DocumentCriteria criteria)
        {
            var request = new GetDocumentsRequest().Prepare(AppContext, Runtime.Settings);
            request.Criteria = criteria;
            request.Criteria.FetchOption = CriteriaBase.FetchOptions.List;

            var response = CoreService.GetDocuments(request);

            return response.Documents;
        }

        /// <summary>
        /// Gets the grouped documents.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public List<GroupedDocumentsModel> GetGroupedDocuments(DocumentCriteria criteria)
        {
            var request = new GetDocumentsRequest().Prepare(AppContext, Runtime.Settings);
            request.Criteria = criteria;
            request.Grouped = true;
            request.Criteria.FetchOption = CriteriaBase.FetchOptions.List;

            var response = CoreService.GetDocuments(request);

            return response.GroupedDocuments;
        }

        /// <summary>
        /// Gets the document.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public DocumentDto GetDocument(long id)
        {
            var request = new GetDocumentsRequest().Prepare(AppContext, Runtime.Settings);
            request.Criteria = new DocumentCriteria { DocumentId = id, FetchOption = CriteriaBase.FetchOptions.Single };

            var response = CoreService.GetDocuments(request);

            return response.Document;
        }

        /// <summary>
        /// Uploads the document.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <returns></returns>
        public List<ErrorTypes> UploadDocument(DocumentDto document)
        {
            var request = new GetDocumentsRequest().Prepare(AppContext, Runtime.Settings);
            request.Document = document;

            var response = CoreService.UploadDocument(request);

            return response.Errors;
        }

        /// <summary>
        /// Deletes the document.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <exception cref="ServiceCallException"></exception>
        public void DeleteDocument(long id)
        {
            var request = new GetDocumentsRequest().Prepare(AppContext, Runtime.Settings);
            request.Criteria = new DocumentCriteria { DocumentId = id };

            var response = CoreService.DeleteDocument(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }

        #endregion
    }
}