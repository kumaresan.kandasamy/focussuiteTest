﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Messages;
using Focus.Core.Messages.OccupationService;
using Focus.Core.Models.Career;
using Focus.ServiceClients.InProcess.Extensions;
using Focus.ServiceClients.Interfaces;
using Focus.Web.Core.Models;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.InProcess
{
	public class OccupationClient : ClientBase, IOccupationClient
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="OccupationClient" /> class.
		/// </summary>
		/// <param name="appContext">The application context.</param>
		/// <param name="runtime">The runtime.</param>
		public OccupationClient(AppContextModel appContext, ServiceClientRuntime runtime) : base(appContext, runtime) { }
		
		/// <summary>
		/// Gets the resume builder questions.
		/// </summary>
		/// <param name="onetCode">The onet code.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<Question> ResumeBuilderQuestions(string onetCode)
		{
			var request = new ResumeBuilderQuestionsRequest().Prepare(AppContext, Runtime.Settings);
			request.OnetCode = onetCode;

			var response = OccupationService.GetResumeBuilderQuestions(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Questions;
		}

		/// <summary>
		/// Gets the statements
		/// </summary>
		/// <param name="onetCode">The onet code.</param>
		/// <param name="tense">The tense of the statement i.e. past or present.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<string> Statements(string onetCode, StatementTense tense = StatementTense.Present)
		{
			var request = new StatementsRequest().Prepare(AppContext, Runtime.Settings);
			request.OnetCode = onetCode;
			request.Tense = tense;

			var response = OccupationService.GetStatements(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Statements;
		}

    /// <summary>
    /// Gets the onet skills
    /// </summary>
    /// <param name="onetCode">The onet code.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public List<string> OnetSkills(string onetCode)
    {
      var request = new OnetSkillsRequest().Prepare(AppContext, Runtime.Settings);
      request.OnetCode = onetCode;

      var response = OccupationService.GetOnetSkills(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.OnetSkills;
    }

		/// <summary>
		/// MOCs the job titles.
		/// </summary>
		/// <param name="jobTitle">The job title.</param>
		/// <param name="socCount">The soc count.</param>
		/// <returns></returns>
		public List<OnetCode> MOCJobTitles(string jobTitle, int socCount)
		{
			var request = new OccupationCodeRequest().Prepare(AppContext, Runtime.Settings);

			var val = jobTitle.IndexOf(" - ");
			if (val > 0)
			{
				request.OccupationCode = jobTitle.Substring(0, val);
				request.OccupationTitle = jobTitle.Substring(val + 3);
			}
			else
			{
				request.OccupationCode = jobTitle;
				request.OccupationTitle = jobTitle;
			}
			request.SOCCount = socCount;

			var response = OccupationService.GetMocOccupationCodes(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.OnetCodes;
		}

    /// <summary>
    /// MOCs for a given occupation
    /// </summary>
    /// <param name="militaryOccupationId">The soc count.</param>
    /// <returns></returns>
    public List<OnetCode> GetMocOccupationCodes(long militaryOccupationId)
    {
      var request = new OccupationCodeRequest().Prepare(AppContext, Runtime.Settings);
      request.OccupationId = militaryOccupationId;
      request.IgnoreJobTaskAvailability = true;

      var response = OccupationService.GetMocOccupationCodes(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.OnetCodes;
    }

		public List<OnetCode> GenericJobTitles(string jobTitle, int socCount)
		{
			var request = new OccupationCodeRequest().Prepare(AppContext, Runtime.Settings);
			request.OccupationTitle = jobTitle;
			request.SOCCount = socCount;

			var response = OccupationService.GetOccupationCodes(request);
			
			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.OnetCodes;
		}

    /// <summary>
    /// Gets the military occupation job titles with a specific prefix
    /// </summary>
    /// <param name="jobTitlePrefix">The job title on which to look for a match as a prefix.</param>
    /// <param name="listSize">Size of the list.</param>
    /// <returns>A list of military job titles</returns>
		public List<MilitaryOccupationJobTitleViewDto> GetMilitaryOccupationJobTitles(string jobTitlePrefix, int listSize)
		{
		  return GetMilitaryOccupationJobTitles(jobTitlePrefix, listSize, false);
		}

    /// <summary>
    /// Gets the military occupation job titles.
    /// </summary>
    /// <param name="jobTitle">The job title on which to look for a match.</param>
    /// <param name="listSize">Size of the list.</param>
    /// <param name="fullTitleSearch">If set to true look for text anywhere in title. If false, check the prefix ony</param>
    /// <returns>A list of military job titles</returns>
    public List<MilitaryOccupationJobTitleViewDto> GetMilitaryOccupationJobTitles(string jobTitle, int listSize, bool fullTitleSearch)
    {
      var request = new MilitaryOccupationJobTitleRequest().Prepare(AppContext, Runtime.Settings);
      request.JobTitle = jobTitle;
      request.FullTitleSearch = fullTitleSearch;
      request.ListSize = listSize;

      var response = OccupationService.GetMilitaryOccupationJobTitles(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.JobTitles;
    }

    /// <summary>
    /// Gets the job title for a specific military occupation
    /// </summary>
    /// <param name="militaryOccupationId">The Id of the military occupation</param>
    /// <returns>The job title for the military occupation</returns>
    public MilitaryOccupationJobTitleViewDto GetMilitaryOccupationJobTitle(long militaryOccupationId)
    {
      var request = new MilitaryOccupationJobTitleRequest().Prepare(AppContext, Runtime.Settings);
      request.MilitaryOccupationId = militaryOccupationId;

      var response = OccupationService.GetMilitaryOccupationJobTitles(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.JobTitle;
    }

    /// <summary>
    /// Converts the onet to r onet.
    /// </summary>
    /// <param name="onet">The onet.</param>
    /// <returns></returns>
    public OnetToROnetConversionViewDto ConvertOnetToROnet(string onet)
    {
      var request = new OnetToROnetConversionRequest {Onet = onet}.Prepare(AppContext, Runtime.Settings);
      var response = OccupationService.ConvertOnetToROnet(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.BestFitROnet;
    }
	}
}
