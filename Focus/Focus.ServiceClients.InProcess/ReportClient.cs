﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria.Report;
using Focus.Core.Messages;
using Focus.Core.Messages.ReportService;
using Focus.Core.Models;
using Focus.Core.Models.Report;
using Focus.Core.Views;
using Focus.ServiceClients.InProcess.Extensions;
using Focus.ServiceClients.Interfaces;
using Focus.Web.Core.Models;
using Framework.Core;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.InProcess
{
	public class ReportClient : ClientBase, IReportClient
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ReportClient" /> class.
		/// </summary>
		/// <param name="appContext">The application context.</param>
		/// <param name="runtime">The runtime.</param>
		public ReportClient(AppContextModel appContext, ServiceClientRuntime runtime) : base(appContext, runtime) { }
		
		#region Approval Queue Report

    /// <summary>
		/// Gets the approval queue status report.
		/// </summary>
		/// <returns></returns>
		public ApprovalQueueStatusReportView GetApprovalQueueStatusReport()
		{
			var request = new ApprovalQueueStatusReportRequest().Prepare(AppContext, Runtime.Settings);
			request.UserId = AppContext.User.UserId;

			var response = ReportService.GetApprovalQueueStatusReport(request);
			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.ReportView;
		}

    #endregion

    #region Job Seeker Reports

    /// <summary>
    /// Gets the job seekers report.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public PagedList<JobSeekersReportView> GetJobSeekersReport(JobSeekersReportCriteria criteria)
    {
      var request = new JobSeekersReportRequest { ReportCriteria = criteria }.Prepare(AppContext, Runtime.Settings);
      var response = ReportService.GetJobSeekers(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.JobSeekers;
    }

	  /// <summary>
	  /// Gets the job seekers as a datatable.
	  /// </summary>
	  /// <param name="criteria">The criteria.</param>
	  /// <param name="jobSeekers">An optional list of job seekers if already read. If left as null, the report is re-run based on the criteria</param>
	  /// <returns>A datatable of job seekers</returns>
	  public ReportDataTableModel GetJobSeekersDataTable(JobSeekersReportCriteria criteria, List<JobSeekersReportView> jobSeekers = null)
    {
      var request = new JobSeekersDataTableRequest
      {
        ReportCriteria = criteria,
        JobSeekers = jobSeekers,
        ReportName = criteria.ReportTitle,
      }.Prepare(AppContext, Runtime.Settings);

      var response = ReportService.GetJobSeekersDataTable(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ReportData;
    }

	  /// <summary>
	  /// Gets the job seekers as an export file
	  /// </summary>
	  /// <param name="criteria">The criteria.</param>
	  /// <param name="reportName">The name of the data table required</param>
	  /// <param name="exportType">Whether to export as Excel or a PDF</param>
	  /// <param name="jobSeekers">An optional list of job seekers if already read. If left as null, the report is re-run based on the criteria</param>
	  /// <returns>An export file of job seekers</returns>
	  public byte[] GetJobSeekersExportBytes(JobSeekersReportCriteria criteria, string reportName, ReportExportType exportType, List<JobSeekersReportView> jobSeekers = null)
    {
      var request = new JobSeekersExportRequest
      {
        ReportCriteria = criteria,
        JobSeekers = jobSeekers,
        ReportName = reportName,
        ExportType = exportType
      }.Prepare(AppContext, Runtime.Settings);

      var response = ReportService.GetJobSeekersExportBytes(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ExportBytes;
    }

    #endregion

    #region job order reports

    /// <summary>
    /// Gets the job orders report.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public PagedList<JobOrdersReportView> GetJobOrdersReport(JobOrdersReportCriteria criteria)
    {
      var request = new JobOrdersReportRequest { ReportCriteria = criteria }.Prepare(AppContext, Runtime.Settings);
      var response = ReportService.GetJobOrders(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.JobOrders;
    }

    /// <summary>
    /// Gets the job seekers as a datatable.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="jobOrders">The job orders.</param>
    /// <returns>
    /// A datatable of job seekers
    /// </returns>
    /// <exception cref="ServiceCallException"></exception>
    public ReportDataTableModel GetJobOrdersDataTable(JobOrdersReportCriteria criteria, List<JobOrdersReportView> jobOrders = null)
    {
      var request = new JobOrdersDataTableRequest
      {
        ReportCriteria = criteria,
        JobOrders = jobOrders,
        ReportName = criteria.ReportTitle,
      }.Prepare(AppContext, Runtime.Settings);

      var response = ReportService.GetJobOrdersDataTable(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ReportData;
    }

    /// <summary>
    /// Gets the job orders as an export file
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="reportName">The name of the data table required</param>
    /// <param name="exportType">Whether to export as Excel or a PDF</param>
    /// <param name="jobOrders">An optional list of job orders if already read. If left as null, the report is re-run based on the criteria</param>
    /// <returns>An export file of job orders</returns>
    public byte[] GetJobOrdersExportBytes(JobOrdersReportCriteria criteria, string reportName, ReportExportType exportType, List<JobOrdersReportView> jobOrders = null)
    {
      var request = new JobOrdersExportRequest
      {
        ReportCriteria = criteria,
        JobOrders = jobOrders,
        ReportName = reportName,
        ExportType = exportType
      }.Prepare(AppContext, Runtime.Settings);

      var response = ReportService.GetJobOrdersExportBytes(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ExportBytes;
    }

    #endregion

    #region Supply Demand Report

    /// <summary>
    /// Gets the supply demand report.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public PagedList<SupplyDemandReportView> GetSupplyDemandReport(SupplyDemandReportCriteria criteria)
    {
      var request = new SupplyDemandReportRequest { ReportCriteria = criteria }.Prepare(AppContext, Runtime.Settings);
      var response = ReportService.GetSupplyDemand(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.SupplyDemand;
    }

    /// <summary>
    /// Gets the supply demand as a datatable.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="supplyDemand">The supply demand.</param>
    /// <returns>
    /// A datatable of supply demand
    /// </returns>
    /// <exception cref="ServiceCallException"></exception>
    public ReportDataTableModel GetSupplyDemandDataTable(SupplyDemandReportCriteria criteria, List<SupplyDemandReportView> supplyDemand = null)
    {
      var request = new SupplyDemandDataTableRequest
      {
        ReportCriteria = criteria,
        SupplyDemand = supplyDemand,
        ReportName = criteria.ReportTitle,
      }.Prepare(AppContext, Runtime.Settings);

      var response = ReportService.GetSupplyDemandDataTable(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ReportData;
    }

    /// <summary>
    /// Gets the supply demand report as an export file
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="reportName">The name of the data table required</param>
    /// <param name="exportType">Whether to export as Excel or a PDF</param>
    /// <param name="supplyDemand">An optional list of supply demand if already read. If left as null, the report is re-run based on the criteria</param>
    /// <returns>An export file of employers</returns>
    public byte[] GetSupplyDemandExportBytes(SupplyDemandReportCriteria criteria, string reportName, ReportExportType exportType, List<SupplyDemandReportView> supplyDemand = null)
    {
      var request = new SupplyDemandExportRequest
      {
        ReportCriteria = criteria,
        SupplyDemand = supplyDemand,
        ReportName = reportName,
        ExportType = exportType
      }.Prepare(AppContext, Runtime.Settings);

      var response = ReportService.GetSupplyDemandExportBytes(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ExportBytes;
    }

    #endregion

    #region employer reports

    /// <summary>
    /// Gets the employers report.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public PagedList<EmployersReportView> GetEmployersReport(EmployersReportCriteria criteria)
    {
      var request = new EmployersReportRequest { ReportCriteria = criteria }.Prepare(AppContext, Runtime.Settings);
      var response = ReportService.GetEmployers(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.Employers;
    }

    /// <summary>
    /// Gets the employers as a datatable.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="employers">The employers.</param>
    /// <returns>
    /// A datatable of employers
    /// </returns>
    /// <exception cref="ServiceCallException"></exception>
    public ReportDataTableModel GetEmployersDataTable(EmployersReportCriteria criteria, List<EmployersReportView> employers = null)
    {
      var request = new EmployersDataTableRequest
      {
        ReportCriteria = criteria,
        Employers = employers,
        ReportName = criteria.ReportTitle,
      }.Prepare(AppContext, Runtime.Settings);

      var response = ReportService.GetEmployersDataTable(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ReportData;
    }

    /// <summary>
    /// Gets the employers as an export file
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="reportName">The name of the data table required</param>
    /// <param name="exportType">Whether to export as Excel or a PDF</param>
    /// <param name="employers">An optional list of employers if already read. If left as null, the report is re-run based on the criteria</param>
    /// <returns>An export file of employers</returns>
    public byte[] GetEmployersExportBytes(EmployersReportCriteria criteria, string reportName, ReportExportType exportType, List<EmployersReportView> employers = null)
    {
      var request = new EmployersExportRequest
      {
        ReportCriteria = criteria,
        Employers = employers,
        ReportName = reportName,
        ExportType = exportType
      }.Prepare(AppContext, Runtime.Settings);

      var response = ReportService.GetEmployersExportBytes(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ExportBytes;
    }

    #endregion

    #region Shared Reporting Methods

    /// <summary>
    /// Runs a report and returns an export file
    /// </summary>
    /// <param name="criteria">The criteria to run the report.</param>
    /// <param name="reportName">The name of the data table required</param>
    /// <param name="exportType">Whether to export as Excel or a PDF</param>
    /// <returns>The export file of job seekers</returns>
    public byte[] GetReportExportBytes(IReportCriteria criteria, string reportName, ReportExportType exportType)
    {
      switch (criteria.ReportType)
      {
        case ReportType.JobSeeker:
          return GetJobSeekersExportBytes((JobSeekersReportCriteria)criteria, reportName, exportType);

        case ReportType.JobOrder:
          return GetJobOrdersExportBytes((JobOrdersReportCriteria)criteria, reportName, exportType);

        case ReportType.Employer:
          return GetEmployersExportBytes((EmployersReportCriteria)criteria, reportName, exportType);
        
        case ReportType.SupplyDemand:
          return GetSupplyDemandExportBytes((SupplyDemandReportCriteria)criteria, reportName, exportType);

        default:
          throw new Exception("Unsupported report criteria");
      }
    }

    /// <summary>
    /// Converts a data table for a report into a file for export
    /// </summary>
    /// <param name="reportData">The report data table</param>
    /// <param name="criteriaDisplay">A list of criteria to display in the export file</param>
    /// <param name="reportName">The name of the data table required</param>
    /// <param name="exportType">Whether to export as Excel or a PDF</param>
    /// <returns>The export file of job seekers</returns>
    public byte[] ConvertDataTableToExportBytes(ReportDataTableModel reportData, List<string> criteriaDisplay, string reportName, ReportExportType exportType)
    {
      var request = new DataTableExportRequest
      {
        ReportData = reportData,
        CriteriaDisplay = criteriaDisplay,
        ExportType = exportType
      }.Prepare(AppContext, Runtime.Settings);

      var response = ReportService.ConvertDataTableToExportBytes(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ExportBytes;
    }

    #endregion

    #region Saved Reports

    /// <summary>
    /// Saves the report.
    /// </summary>
    /// <param name="reportName">Name of the report.</param>
    /// <param name="reportCriteria">The report criteria.</param>
    /// <param name="displayOnDashboard">Add to dashboard flag </param>
    public void SaveReport(string reportName, IReportCriteria reportCriteria, bool displayOnDashboard)
    {
      var request = new SaveReportRequest
      {
        ReportCriteria = reportCriteria,
        ReportName = reportName,
        DisplayOnDashboard = displayOnDashboard,
        IsSessionReport = false
      }.Prepare(AppContext, Runtime.Settings);

      var response = ReportService.SaveReport(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);
    }

    /// <summary>
    /// Saves the report to a session state
    /// </summary>
    /// <param name="reportCriteria">The report criteria.</param>
    public long SaveReportToSession(IReportCriteria reportCriteria)
    {
      var request = new SaveReportRequest
      {
        ReportCriteria = reportCriteria,
        IsSessionReport = true
      }.Prepare(AppContext, Runtime.Settings);

      var response = ReportService.SaveReport(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ReportId;
    }

    /// <summary>
    /// Gets the saved report
    /// </summary>
    /// <param name="reportId">The id for the saved report</param>
    public IReportCriteria GetSavedReport(long reportId)
    {
      var request = new GetSavedReportRequest
      {
        ReportId =  reportId,
        IsSessionReport = false
      }.Prepare(AppContext, Runtime.Settings);

      var response = ReportService.GetSavedReport(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ReportCriteria;
    }

	  /// <summary>
	  /// Gets the report from session state
	  /// </summary>
    /// <param name="reportId">The id for the saved report</param>
	  public IReportCriteria GetSavedReportFromSession(long reportId)
    {
      var request = new GetSavedReportRequest
      {
        ReportId = reportId,
        IsSessionReport = true
      }.Prepare(AppContext, Runtime.Settings);

      var response = ReportService.GetSavedReport(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ReportCriteria;
    }

    /// <summary>
    /// Gets all saved reports for a user
    /// </summary>
    /// <param name="dashboardReports">Whether to get only dashboard report, non-dashboard reports, or both</param>
    public List<Core.DataTransferObjects.Report.SavedReportDto> GetSavedReportList(bool? dashboardReports = null)
    {
      var request = new GetSavedReportListRequest
      {
        DashboardReports = dashboardReports
      }.Prepare(AppContext, Runtime.Settings);

      var response = ReportService.GetSavedReportList(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.SavedReports;
    }

    /// <summary>
    /// Deletes a report
    /// </summary>
    /// <param name="reportId">The id of the report to delete</param>
    public void DeleteReport(long reportId)
    {
      var request = new DeleteReportRequest
      {
        ReportId = reportId
      }.Prepare(AppContext, Runtime.Settings);

      var response = ReportService.DeleteReport(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);
    }

    #endregion

    #region Report type-aheads and drop-downs

    /// <summary>
    /// Gets the reporting look ups.
    /// </summary>
    /// <param name="lookUp">The look up.</param>
    /// <param name="searchString">The search string.</param>
    /// <returns>A list of matching string for the lookup type</returns>
    public string[] GetReportLookUpData(ReportLookUpType lookUp, string searchString)
    {
      var request = new LookupReportDataRequest
      {
        LookUpType = lookUp,
        SearchString = searchString
      }.Prepare(AppContext, Runtime.Settings);
      var response = ReportService.LookUpReportData(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.LookupData ?? new string[0];
    }

    /// <summary>
    /// Gets the reporting offices
    /// </summary>
    /// <param name="reportType">The type of report.</param>
    /// <returns>A list of offices for the type</returns>
    public List<ReportOffice> GetReportOfficeLookup(ReportType reportType)
    {
      var request = new OfficeLookupRequest
      {
        ReportType = reportType
      }.Prepare(AppContext, Runtime.Settings);
      var response = ReportService.OfficeLookup(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.Offices;
    }

    #endregion

		#region Statistics

		/// <summary>
		/// Gets the statistics.
		/// </summary>
		/// <param name="statisticTypes">The statistic types.</param>
		/// <param name="statisticDate">The statistic date.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<KeyValuePair<StatisticType, string>> GetStatistics(List<StatisticType> statisticTypes, DateTime statisticDate )
		{
			var request = new GetStatisticsRequest().Prepare(AppContext, Runtime.Settings);
			request.StatisticTypes = statisticTypes;
			request.StatisticDate = statisticDate;

			var response = ReportService.GetStatistics(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Statistics;
		}

		#endregion

		#region Generate Employer Activity Report

    /// <summary>
    /// Generates the employer activity report.
    /// </summary>
    /// <param name="reportType">Type of the report.</param>
    /// <param name="fromDate">From date.</param>
    /// <param name="toDate">To date.</param>
    /// <param name="recipients">The recipients.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
		public ActivityReportValidationModel GenerateActivityReport(AsyncReportType reportType, DateTime fromDate, DateTime toDate, string recipients)
		{
			var request = new ActivityReportRequest
			{
        ReportType = reportType,
				FromDate = fromDate,
				ToDate = toDate,
				Recipients = recipients
			}.Prepare(AppContext, Runtime.Settings);

			var response = ReportService.GenerateActivityReport(request);

			var validationModel = new ActivityReportValidationModel();

			if (response.Acknowledgement == AcknowledgementType.Failure && response.Error != ErrorTypes.ValidationFailed)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
			
			if (response.Acknowledgement == AcknowledgementType.Failure && response.Error == ErrorTypes.ValidationFailed)
			{
				validationModel.Errors = response.Errors;
				validationModel.InvalidEmails = response.InvalidEmails;
			}

			return validationModel;
		}

		#endregion
	}
}