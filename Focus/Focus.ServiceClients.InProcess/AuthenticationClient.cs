﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using BgtSSO;
using Focus.Core;
using Focus.Core.Messages;
using Focus.Core.Messages.AuthenticationService;
using Focus.Core.Models;
using Focus.Core.Views;
using Focus.ServiceClients.InProcess.Extensions;
using Focus.ServiceClients.Interfaces;
using Focus.Web.Core.Models;
using Framework.Core;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.InProcess
{
	public class AuthenticationClient : ClientBase, IAuthenticationClient
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="AuthenticationClient" /> class.
		/// </summary>
		/// <param name="appContext">The application context.</param>
		/// <param name="runtime">The runtime.</param>
		public AuthenticationClient(AppContextModel appContext, ServiceClientRuntime runtime) : base(appContext, runtime) { }

		/// <summary>
		/// Validates the user's Session
		/// </summary>
		public ErrorTypes ValidateSession()
		{
			var request = new SessionRequest().Prepare(AppContext, Runtime.Settings);
			var response = AuthenticationService.ValidateSession(request);

			Correlate(request, response);

			return response.Acknowledgement == AcknowledgementType.Success ? ErrorTypes.Ok : response.Error;
		}

		/// <summary>
		/// StartSession must be the first call to the service for each user. 
		/// This is irrespective of whether user is logging in or not.
		/// </summary>
		/// <returns>Unique security token that is valid for the duration of the session.</returns>
		public Guid StartSession()
		{
			var request = new SessionRequest { ClientTag = AppContext.ClientTag, RequestId = AppContext.RequestId, VersionNumber = Constants.SystemVersion };
			var response = AuthenticationService.StartSession(request);

			Correlate(request, response);

			return response.SessionId;
		}

		/// <summary>
		/// Logs in the specified user to career/explorer
		/// </summary>
		/// <param name="userName">The userName.</param>
		/// <param name="password">The password.</param>
		/// <param name="integrationUser">The module to login to.</param>
		/// <param name="validationUrl">The validation URL.</param>
		/// <returns></returns>
		public LoggedInUserModel LogInToCareer(string userName, string password, string validationUrl, out ValidatedUserView integrationUser)
		{
			var request = new LogInRequest().Prepare(AppContext, Runtime.Settings);
			request.UserName = userName;
			request.Password = password;
			request.ModuleToLoginTo = Runtime.Settings.Module;
			request.ValidationUrl = validationUrl;
			request.Type = Runtime.Settings.IntegrationClient != IntegrationClient.Standalone ? LoginType.IntegrationClient : LoginType.Standard;

			var response = AuthenticationService.LogIn(request);

			var user = LogIn(response);

			user.AccountReactivated = response.AccountReactivateRequest;

			integrationUser = response.IsIntegrationUser ? response.IntegrationDetails : null;

			return user;
		}

		/// <summary>
		/// Logs in the specified user.
		/// </summary>
		/// <param name="userName">The userName.</param>
		/// <param name="password">The password.</param>
		/// <param name="moduleToLoginTo">The module to login to.</param>
		/// <param name="validationUrl">The validation URL.</param>
		/// <param name="allowResetSessionId">If set to true, the session id can be reset if it is invalid.</param>
		/// <returns></returns>
		public LoggedInUserModel LogIn(string userName, string password, FocusModules moduleToLoginTo, string validationUrl = "", bool allowResetSessionId = false)
		{
			var request = new LogInRequest().Prepare(AppContext, Runtime.Settings);
			request.UserName = userName;
			request.Password = password;
			request.ModuleToLoginTo = moduleToLoginTo;
			request.ValidationUrl = validationUrl;
			request.AllowResetSessionId = allowResetSessionId;
			request.Type = Runtime.Settings.IntegrationClient != IntegrationClient.Standalone ? LoginType.IntegrationClient : LoginType.Standard;

			var response = AuthenticationService.LogIn(request);

			var user = LogIn(response);
			user.UserContext.IsEnabled = response.IsEnabled;

			if (allowResetSessionId && response.ResetSessionId.IsNotNull())
				user.ResetSessionId = response.ResetSessionId;

			return user;
		}

		public LoggedInUserModel LogIn(ISSOProfile profile, FocusModules moduleToLoginTo)
		{
			var request = new LogInRequest().Prepare(AppContext, Runtime.Settings);

			request.Type = LoginType.SSO;
			request.ExternalId = profile.Id;
			request.EmailAddress = profile.EmailAddress;
			request.FirstName = profile.FirstName;
			request.LastName = profile.LastName;
			request.ScreenName = profile.ScreenName;
			request.ModuleToLoginTo = moduleToLoginTo;
			request.CreateRoles = profile.CreateRoles;
			request.UpdateRoles = profile.UpdateRoles;

			var response = AuthenticationService.LogIn(request);

			var user = LogIn(response);
			user.UserContext.IsEnabled = response.IsEnabled;

			return user;
		}

		private LoggedInUserModel LogIn(LogInResponse response)
		{
			if (response.Acknowledgement == AcknowledgementType.Failure && !response.AccountReactivateRequest)
				throw new ServiceCallException(response.Message, (int)response.Error);

			var userContext = new UserContext(response.UserId, response.UserId, response.FirstName, response.LastName,
				response.EmailAddress, response.PersonId, response.EmployeeId, response.EmployerId,
				response.ExternalUserId, response.ExternalUserName, response.ExternalPassword,
				response.ExternalOfficeId, response.ScreenName, response.UserName,
				response.LastLoggedInOn, response.IsMigrated, response.RegulationsConsent);

			userContext.IsEnabled = response.IsEnabled;

			userContext.DefaultResumeId = response.DefaultResumeId;

			if (Runtime.Settings.Theme == FocusThemes.Education)
			{
				userContext.ProgramAreaId = response.ProgramAreaId;
				userContext.DegreeEducationLevelId = response.DegreeId;
				userContext.EnrollmentStatus = response.EnrollmentStatus;
				userContext.CampusId = response.CampusId;
			}

			#region set App.UserData values

			var userData = new UserDataModel
			{
				CareerUserDataCheck = true,
				DefaultResumeId = response.DefaultResumeId,
				DefaultResumeCompletionStatus = response.DefaultResumeCompletionStatus,
				Profile = response.Profile
			};

			#endregion

			return new LoggedInUserModel
			{
				UserContext = userContext,
				UserData = userData
			};
		}

		/// <summary>
		/// Gets the App.UserData data containing career data on resumes and skills
		/// </summary>
		public UserDataModel GetCareerUserData()
		{
			if (!AppContext.User.IsAuthenticated)
				return null;

			var request = new CareerUserDataRequest().Prepare(AppContext, Runtime.Settings);

			var response = AuthenticationService.GetCareerUserData(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return new UserDataModel
			{
				CareerUserDataCheck = true,
				DefaultResumeId = response.DefaultResumeId,
				DefaultResumeCompletionStatus = response.DefaultResumeCompletionStatus,
				Profile = response.Profile,
			};
		}

		/// <summary>
		/// Logs the out.
		/// </summary>
		public void LogOut()
		{
			var request = new LogOutRequest().Prepare(AppContext, Runtime.Settings);
			AuthenticationService.LogOut(request);
		}

		//public IList<LookupItemView> CheckUserHasASavedQuestion(string userName, string emailAddress)
		//{
		//  var request = new CheckUserHasASavedQuestionRequest().Prepare(AppContext, Runtime.Settings);
		//  request.UserName = userName;
		//  request.EmailAddress = emailAddress;

		//  var response = CheckUserHasASavedQuestion(request);

		//  if (response.Acknowledgement == AcknowledgementType.Failure)
		//    throw new ServiceCallException(response.Message, (int)response.Error);

		//  return response.AttemptsFailed
		//    ? response.AttemptsLeft
		//    : (int?)null;

		//  var response = AuthenticationService.CheckUserHasASavedQuestion(userName, emailAddress);

		//  return response;
		//} 

		/// <summary>
		/// Processes the forgotten password.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <param name="emailAddress">The email address.</param>
		/// <param name="changePasswordUrl">The change password URL.</param>
		/// <param name="module">The Focus module in use</param>
		/// <param name="monthOfBirth"></param>
		/// <param name="pinRegistrationUrl">The pin registration URL (for Education, in case of user not yet being registered)</param>
		/// <param name="securityQuestion"></param>
		/// <param name="securityQuestionId"></param>
		/// <param name="securityAnswer"></param>
		/// <param name="dayOfBirth"></param>
		/// <exception cref="ServiceCallException"></exception>
		public int? ProcessForgottenPassword(string userName, string emailAddress, string changePasswordUrl, FocusModules module, string securityQuestion = null, long? securityQuestionId = null, string securityAnswer = null, int dayOfBirth = 0, int monthOfBirth = 0, string pinRegistrationUrl = "")
		{
			var request = new ProcessForgottenPasswordRequest().Prepare(AppContext, Runtime.Settings);
			request.UserName = userName;
			request.EmailAddress = emailAddress;
			request.ResetPasswordUrl = changePasswordUrl;
			request.Module = module;
			request.PinRegistrationUrl = pinRegistrationUrl;
			request.SecurityQuestion = securityQuestion;
			request.SecurityQuestionId = securityQuestionId;
			request.SecurityAnswer = securityAnswer;
			request.DayOfBirth = dayOfBirth;
			request.MonthOfBirth = monthOfBirth;

			var response = AuthenticationService.ProcessForgottenPassword(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.AttemptsFailed
				? response.AttemptsLeft
				: (int?)null;
		}

		/// <summary>
		/// Creates the employee single sign on.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="userId"> </param>
		/// <returns></returns>
		public Guid CreateEmployeeSingleSignOn(long employeeId, long? userId)
		{
			var request = new CreateSingleSignOnRequest().Prepare(AppContext, Runtime.Settings);
			request.AccountEmployeeId = employeeId;
			request.AccountUserId = userId;

			var response = AuthenticationService.CreateSingleSignOn(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.SingleSignOnKey;
		}

		/// <summary>
		/// Creates the employee single sign on.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <returns></returns>
		public Guid CreateEmployeeSingleSignOn(string userName)
		{
			var request = new CreateSingleSignOnRequest().Prepare(AppContext, Runtime.Settings);
			request.UserName = userName;

			var response = AuthenticationService.CreateSingleSignOn(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.SingleSignOnKey;
		}

		/// <summary>
		/// Creates the candidate single sign on.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="userId">The user id.</param>
		/// <returns></returns>
		public Guid CreateCandidateSingleSignOn(long personId, long? userId)
		{
			var request = new CreateSingleSignOnRequest().Prepare(AppContext, Runtime.Settings);
			request.AccountPersonId = personId;
			request.AccountUserId = userId;

			var response = AuthenticationService.CreateSingleSignOn(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.SingleSignOnKey;
		}

		/// <summary>
		/// Validates the single sign on.
		/// </summary>
		/// <param name="singleSignOnKey">The single sign on key.</param>
		/// <returns></returns>
		public LoggedInUserModel ValidateSingleSignOn(Guid singleSignOnKey)
		{
			var request = new ValidateSingleSignOnRequest().Prepare(AppContext, Runtime.Settings);
			request.SingleSignOnKey = singleSignOnKey;

			var response = AuthenticationService.ValidateSingleSignOn(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			var userContext = new UserContext(
				response.UserId,
				response.ActionerId != 0 ? response.ActionerId : response.UserId,
				response.FirstName,
				response.LastName,
				response.EmailAddress,
				response.PersonId,
				response.EmployeeId,
				response.EmployerId,
				response.ScreenName,
				response.UserName)
			{
				ExternalOfficeId = response.ExternalOfficeId,
				ExternalPassword = response.ExternalPassword,
				ExternalUserId = response.ExternalAdminId,
				ExternalUserName = response.ExternalUsername
			};
			if (Runtime.Settings.Theme == FocusThemes.Education)
			{
				userContext.ProgramAreaId = response.ProgramAreaId;
				userContext.DegreeEducationLevelId = response.DegreeId;
				userContext.EnrollmentStatus = response.EnrollmentStatus;
				userContext.CampusId = response.CampusId;
			}

			#region set App.UserData values

			var userData = new UserDataModel
			{
				CareerUserDataCheck = true,
				DefaultResumeId = response.DefaultResumeId,
				DefaultResumeCompletionStatus = response.DefaultResumeCompletionStatus,
				Profile = response.Profile
			};

			#endregion

			return new LoggedInUserModel
			{
				UserContext = userContext,
				UserData = userData
			};
		}

		/// <summary>
		/// Validates whether (encrypted) user details exist in an external system
		/// </summary>
		/// <param name="authenticationDetails">A dictionary of name/value pairs needed for validation</param>
		/// <returns>Details of the external user</returns>
		public ValidatedUserView ValidateExternalAuthentication(Dictionary<string, string> authenticationDetails)
		{
			var request = new ExternalAuthenticationRequest
			{
				AuthenticationDetails = authenticationDetails
			}.Prepare(AppContext, Runtime.Settings);

			var response = AuthenticationService.ValidateExternalAuthentication(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.ExternalsDetails;
		}

		/// <summary>
		/// Validates whether user details exist in an external system
		/// </summary>
		/// <param name="socialSecurityNumber">The users Social Security number</param>
		/// <param name="checkFocus"></param>
		/// <returns>Details of the external user</returns>
		public ValidatedUserView ValidateExternalAuthenticationBySsn(string socialSecurityNumber, bool checkFocus = true)
		{
			var request = new ExternalAuthenticationRequest
			{
				SocialSecurityNumber = socialSecurityNumber,
				CheckFocus = checkFocus
			}.Prepare(AppContext, Runtime.Settings);

			var response = AuthenticationService.ValidateExternalAuthentication(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.ExternalsDetails;
		}

		/// <summary>
		/// Validates whether user details exist in an external system
		/// </summary>
		/// <param name="externalId">The user's External Id</param>
		/// <param name="checkFocus"></param>
		/// <returns>Details of the external user</returns>
		public ValidatedUserView ValidateExternalAuthenticationByExternalId(string externalId, bool checkFocus = true)
		{
			var request = new ExternalAuthenticationRequest
			{
				ExternalId = externalId,
				CheckFocus = checkFocus
			}.Prepare(AppContext, Runtime.Settings);

			var response = AuthenticationService.ValidateExternalAuthentication(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.ExternalsDetails;
		}

		/// <summary>
		/// Validates the password reset validation key.
		/// </summary>
		/// <param name="validationKey">The validation key.</param>
		/// <param name="error">The error (should validation fail)</param>
		/// <returns></returns>
		public long? ValidatePasswordResetValidationKey(string validationKey, out ErrorTypes error)
		{
			var request = new ValidatePasswordResetValidationKeyRequest().Prepare(AppContext, Runtime.Settings);
			request.ValidationKey = validationKey;

			var response = AuthenticationService.ValidatePasswordResetValidationKey(request);

			error = response.Error;
			return response.UserId;
		}

		/// <summary>
		/// Validates the pin registration details.
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		public bool ValidatePinRegistration(PinRegistrationModel model)
		{
			var request = new ValidatePinRequest().Prepare(AppContext, Runtime.Settings);
			request.Pin = model.Pin;
			request.EmailAddress = model.EmailAddress;

			var response = AuthenticationService.ValidatePinRegistration(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
			{
				if (response.Error == ErrorTypes.EmailAlreadyInUse)
					model.RegistrationError = response.Error;
			}

			var success = response.Acknowledgement == AcknowledgementType.Success;

			return success;
		}

		/// <summary>
		/// Gets the security questions.
		/// </summary>
		/// <param name="emailAddress">The email address.</param>
		/// <param name="userName">Name of the user.</param>
		/// <param name="module">The module.</param>
		/// <returns></returns>
		public SecurityQuestionResponse GetSecurityQuestions(string emailAddress, string userName, FocusModules module)
		{
			var request = new SecurityQuestionRequest().Prepare(AppContext, Runtime.Settings);
			request.EmailAddress = emailAddress;
			request.UserName = userName;
			request.Module = module;

			var response = AuthenticationService.GetSecurityQuestions(request);

			return response;
		}


		public int? ProcessSecurityQuestions(	string emailAddress, string userName,
																					int securityQuestionIndex1, string securityAnswer1,
																					int securityQuestionIndex2, string securityAnswer2,
																					FocusModules module, string resetPasswordUrl)
		{
			var request = new ProcessSecurityQuestionsRequest().Prepare(AppContext, Runtime.Settings);

			request.SecurityQuestionIndex1 = securityQuestionIndex1;
			request.SecurityQuestionIndex2 = securityQuestionIndex2;
			request.SecurityAnswer1 = securityAnswer1;
			request.SecurityAnswer2 = securityAnswer2;
			request.EmailAddress = emailAddress;
			request.UserName = userName;
			request.Module = module;
			request.ResetPasswordUrl = resetPasswordUrl;

			var response = AuthenticationService.ProcessSecurityQuestions(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.AttemptsFailed
				? response.AttemptsLeft
				: (int?)null;
		}
	}
}
