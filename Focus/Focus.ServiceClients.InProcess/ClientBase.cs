﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core;
using Focus.Core.Messages;

using Focus.Services.ServiceContracts;
using Focus.Services.ServiceImplementations;
using Focus.Web.Core.Models;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.InProcess
{
	/// <summary>
	/// Base class for all Controllers. Manages Service client requests and responses.
	/// Provides common request-response correlation check.
	/// </summary>
	public abstract class ClientBase
	{
		protected ServiceClientRuntime Runtime;
		protected AppContextModel AppContext;

		protected ICoreService CoreService { get { return new CoreService(); } }		
		protected IAuthenticationService AuthenticationService { get { return new AuthenticationService(); } }
		protected IAccountService AccountService { get { return new AccountService(); } }
		protected IJobService JobService { get { return new JobService(); } }
		protected ISearchService SearchService { get { return new SearchService(); } }
		protected ICandidateService CandidateService { get { return new CandidateService(); } }
		protected IEmployerService EmployerService { get { return new EmployerService(); } }
		protected IEmployeeService EmployeeService { get { return new EmployeeService(); } }
    protected IReportService ReportService { get { return new ReportService(); } }
    protected IStaffService StaffService { get { return new StaffService(); } }
		protected IExplorerService ExplorerService { get { return new ExplorerService(); } }
		protected IAnnotationService AnnotationService { get { return new AnnotationService(); } }
		protected IOccupationService OccupationService { get { return new OccupationService(); } }
		protected IOrganizationService OrganizationService { get { return new OrganizationService(); } }
		protected IPostingService PostingService { get { return new PostingService(); } }
		protected IProcessorService ProcessorService { get { return new ProcessorService(); } }
		protected IResumeService ResumeService { get { return new ResumeService(); } }
		protected IUtilityService UtilityService { get { return new UtilityService(); } }


		protected ClientBase(AppContextModel appContext, ServiceClientRuntime runtime)
		{
			AppContext = appContext;
			Runtime = runtime;
		}

		/// <summary>
		/// Correlates the specified request.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="response">The response.</param>
		protected void Correlate(ServiceRequest request, ServiceResponse response)
		{
			if (request.RequestId != response.CorrelationId)
				throw new CorrelationException("There has been a problem processing your request.<br/>Please check the details and try again.");
		}
	}
}