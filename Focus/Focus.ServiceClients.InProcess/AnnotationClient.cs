﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Bookmark;
using Focus.Core.Criteria.Explorer;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Messages.AnnotationService;
using Focus.Core.Models.Career;
using Focus.ServiceClients.InProcess.Extensions;
using Focus.ServiceClients.Interfaces;
using Focus.Web.Core.Models;
using Framework.Core;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.InProcess
{
	public class AnnotationClient : ClientBase, IAnnotationClient
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="AnnotationClient" /> class.
		/// </summary>
		/// <param name="appContext">The application context.</param>
		/// <param name="runtime">The runtime.</param>
		public AnnotationClient(AppContextModel appContext, ServiceClientRuntime runtime) : base(appContext, runtime) { }
		
		#region Bookmarks

    /// <summary>
    /// Gets a bookmark.
    /// </summary>
    /// <param name="bookmarkId">The bookmark id.</param>
    /// <returns></returns>
    public BookmarkDto GetBookmark(long bookmarkId)
    {
      var request = new BookmarkRequest().Prepare(AppContext, Runtime.Settings);
      request.Criteria = new BookmarkCriteria { FetchOption = CriteriaBase.FetchOptions.Single, BookmarkId = bookmarkId };

      var response = AnnotationService.GetBookmark(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.Bookmark;
    }

    /// <summary>
    /// Gets my bookmarks
    /// </summary>
    /// <returns></returns>
    public List<BookmarkDto> GetMyBookmarks(BookmarkTypes? bookmarkType = null, string bookmarkName = null)
    {
      var request = new BookmarkRequest().Prepare(AppContext, Runtime.Settings);
      request.Criteria = new BookmarkCriteria
      {
        FetchOption = CriteriaBase.FetchOptions.List,
        BookmarkType = bookmarkType,
        BookmarkName = bookmarkName
      };

      var response = AnnotationService.GetBookmark(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.Bookmarks;
    }

    /// <summary>
    /// Creates a bookmark.
    /// </summary>
    /// <param name="name">The name</param>
    /// <param name="type">The type</param>
    /// <param name="explorerCriteria">The bookmark criteria for explorer</param>
    public bool CreateBookmark(string name, BookmarkTypes type, ExplorerCriteria explorerCriteria)
    {
      return CreateBookmark(name, type, explorerCriteria.SerializeJson());
    }


    /// <summary>
	  /// Creates a bookmark.
	  /// </summary>
	  /// <param name="name">The name</param>
	  /// <param name="type">The type</param>
	  /// <param name="criteria">The bookmark criteria</param>
	  public bool CreateBookmark(string name, BookmarkTypes type, string criteria = "")
    {
      var request = new CreateBookmarkRequest().Prepare(AppContext, Runtime.Settings);

      UserTypes? userType = null;

      if (Runtime.Settings.CareerExplorerModule == FocusModules.Career || Runtime.Settings.CareerExplorerModule == FocusModules.CareerExplorer)
      {
        userType = UserTypes.Career;
      }
      if (Runtime.Settings.CareerExplorerModule == FocusModules.Explorer || Runtime.Settings.CareerExplorerModule == FocusModules.CareerExplorer)
      {
        userType = userType.IsNull() ? UserTypes.Explorer : userType | UserTypes.Explorer;
      }

      var bookmark = new BookmarkDto
      {
        Name = name,
        Type = type,
        UserId = AppContext.User.UserId,
        UserType = userType.GetValueOrDefault(),
				Criteria = criteria
      };
      request.Bookmark = bookmark;

      var response = AnnotationService.CreateBookmark(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return (response.Acknowledgement == AcknowledgementType.Success);
    }

    /// <summary>
    /// Deletes a bookmark.
    /// </summary>
    /// <param name="bookmarkId">The bookmark id.</param>
    public void DeleteBookmark(long bookmarkId)
    {
      var request = new DeleteBookmarkRequest().Prepare(AppContext, Runtime.Settings);
      request.BookmarkId = bookmarkId;

      var response = AnnotationService.DeleteBookmark(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
    }

    #endregion

    #region Saved search

		/// <summary>
		/// Saves the search.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="name"></param>
		/// <param name="criteria"></param>
		/// <param name="type"></param>
		/// <param name="frequency"></param>
		/// <param name="format"></param>
		/// <param name="toEmailAddress"></param>
		/// <param name="status"></param>
		/// <param name="emailRequired"></param>
		/// <returns></returns>
		/// <exception cref="Framework.Exceptions.ServiceCallException"></exception>
		/// <exception cref="ServiceCallException"></exception>
		/// (searchName, searchCriteria.ToString(), frequency, format,
	  public bool SaveSearch(long id, string name, SearchCriteria criteria, SavedSearchTypes type, EmailAlertFrequencies frequency, EmailFormats format, string toEmailAddress, AlertStatus status, bool emailRequired = false )
		{
			var request = new SaveSearchRequest().Prepare(AppContext, Runtime.Settings);
      
      request.Id = id;
	    request.Name = name;
	    request.SearchType = type;
			request.SearchCriteria = criteria;
	    request.AlertEmailRequired = emailRequired;
	    request.AlertEmailFrequency = frequency;
	    request.AlertEmailFormat = format;
	    request.AlertEmailStatus = status;
	    request.AlertEmailAddress = toEmailAddress;
      
      var response = AnnotationService.SaveSearch(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return (response.Acknowledgement == AcknowledgementType.Success);
		}

		/// <summary>
		/// Gets the saved search.
		/// </summary>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<SearchDetails> GetSavedSearches(SavedSearchTypes type)
		{
			var request = new ListSearchRequest().Prepare(AppContext, Runtime.Settings);
		  request.Type = type;

			var response = AnnotationService.ListSearch(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.SavedSearches;
		}

	  /// <summary>
	  /// Deletes the save search.
	  /// </summary>
	  /// <param name="id"></param>
	  /// <returns></returns>
	  /// <exception cref="ServiceCallException"></exception>
	  public bool DeleteSaveSearch(long id)
		{
			var request = new DeleteSaveSearchRequest().Prepare(AppContext, Runtime.Settings);
	    request.Id = id;
			
			var response = AnnotationService.DeleteSaveSearch(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return (response.Acknowledgement == AcknowledgementType.Success);
    }

    #endregion

  }
}
