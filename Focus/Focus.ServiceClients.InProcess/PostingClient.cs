﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria.SpideredPosting;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Messages.OrganizationService;
using Focus.Core.Messages.PostingService;
using Focus.Core.Models.Assist;
using Focus.Core.Models.Career;
using Focus.ServiceClients.InProcess.Extensions;
using Focus.ServiceClients.Interfaces;
using Focus.Web.Core.Models;
using Framework.Core;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.InProcess
{
	public class PostingClient : ClientBase, IPostingClient
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="PostingClient" /> class.
		/// </summary>
		/// <param name="appContext">The application context.</param>
		/// <param name="runtime">The runtime.</param>
		public PostingClient(AppContextModel appContext, ServiceClientRuntime runtime) : base(appContext, runtime) { }
		
		/// <summary>
		/// Gets the job posting.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="logViewAction"></param>
		/// <returns></returns>
		/// <exception cref="Framework.Exceptions.ServiceCallException"></exception>
		/// <exception cref="ServiceCallException"></exception>
		public PostingEnvelope GetJobPosting(string lensPostingId, bool logViewAction = false)
		{
			var request = new PostingRequest().Prepare(AppContext, Runtime.Settings);
			request.LensPostingId = lensPostingId;
			request.LogViewAction = logViewAction;

			var response = PostingService.FetchPosting(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.PostingDetails;
		}

		/// <summary>
		/// Closes the job posting.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public bool CloseJobPosting(string lensPostingId)
		{
			var request = new ClosePostingRequest().Prepare(AppContext, Runtime.Settings);
			request.LensPostingId = lensPostingId;

			var response = PostingService.ClosePosting(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return (response.Acknowledgement == AcknowledgementType.Success);
		}

		/// <summary>
		/// Gets the typical resumes.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="posting">The posting.</param>
		/// <returns></returns>
		public List<ResumeInfo> GetTypicalResumes(string lensPostingId, PostingEnvelope posting)
		{
			var request = new TypicalResumesRequest().Prepare(AppContext, Runtime.Settings);
			
			if (lensPostingId.IsNotNullOrEmpty())
				request.LensPostingId = lensPostingId;
			else if (posting.IsNotNull())
				request.Posting = posting;

			var response = PostingService.GetTypicalResumes(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.TypicalResumes;
		}

		/// <summary>
		/// Gets the recently viewed postings.
		/// </summary>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<ViewedPostingModel> GetViewedPostings()
		{
			var request = new ViewedPostingRequest().Prepare(AppContext, Runtime.Settings);
			request.UserId = AppContext.User.UserId;
			var response = PostingService.GetViewedPostings(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.ViewedPostings;
		}

		/// <summary>
		/// Adds the viewed posting.
		/// </summary>
		/// <param name="viewedPosting">The viewed posting.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public bool AddViewedPosting(ViewedPostingDto viewedPosting)
		{
			var request = new AddViewedPostingRequest().Prepare(AppContext, Runtime.Settings);

			viewedPosting.UserId = AppContext.User.UserId;
			viewedPosting.ViewedOn = DateTime.Now;
			request.ViewedPosting = viewedPosting;

			var response = PostingService.AddViewedPostings(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return (response.Acknowledgement == AcknowledgementType.Success);
		}

		/// <summary>
		/// Gets the job id from posting.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <returns></returns>
    public long? GetJobIdFromPosting(string lensPostingId)
    {
      var request = new GetJobIdFromPostingRequest {LensPostingId = lensPostingId}.Prepare(AppContext, Runtime.Settings);
      var response = PostingService.GetJobIdFromPosting(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.JobId;
    }

		/// <summary>
		/// Gets the compare posting to resume modal model.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="doLiveSearch">if set to <c>true</c> [do live search].</param>
		/// <returns></returns>
		public ComparePostingToResumeModalModel GetComparePostingToResumeModalModel(string lensPostingId, bool doLiveSearch)
		{
			var request = new GetComparePostingToResumeModalModelRequest { LensPostingId = lensPostingId, DoLiveSearch = doLiveSearch }.Prepare(AppContext, Runtime.Settings);
			var response = PostingService.GetComparePostingToResumeModalModel(request);

			Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Model;
		}

		/// <summary>
		/// Gets the posting employer contact model.
		/// </summary>
		/// <param name="postingId">The posting id.</param>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		public PostingEmployerContactModel GetPostingEmployerContactModel(long postingId, long? personId)
		{
			var request = new GetPostingEmployerContactModelRequest { PostingId = postingId, PersonId = personId}.Prepare(AppContext, Runtime.Settings);
			var response = PostingService.GetPostingEmployerContactModel(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Model;
		}

		/// <summary>
		/// Gets the spidered postings.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<SpideredPostingModel> GetSpideredPostings(SpideredPostingCriteria criteria)
		{
			var request = new SpideredPostingRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = criteria;

			var response = PostingService.GetSpideredPostings(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.SpideredPostingsPaged;
		}

		/// <summary>
		/// Gets the spidered postings with good matches.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<SpideredPostingModel> GetSpideredPostingsWithGoodMatches(SpideredPostingCriteria criteria)
		{
			var request = new SpideredPostingRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = criteria;

			var response = PostingService.GetSpideredPostingsWithGoodMatches(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.SpideredPostingsPaged;
		}

		/// <summary>
		/// Gets the posting id. If posting not in database retrieves it from document store
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <returns></returns>
		public long? GetPostingId(string lensPostingId)
		{
			var request = new PostingRequest().Prepare(AppContext, Runtime.Settings);
			request.LensPostingId = lensPostingId;
			
			var response = PostingService.GetPostingId(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.PostingId; 
		}

		/// <summary>
		/// Gets the posting BGT occ code.
		/// </summary>
		/// <param name="lensPostingId">The lens posting identifier.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
    public string GetPostingBGTOccs(string lensPostingId)
    {
      var request = new PostingRequest().Prepare(AppContext, Runtime.Settings);
      request.LensPostingId = lensPostingId;

      var response = PostingService.GetPostingBGTOccs(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.PostingBGTOcc;
    }
	}
}
