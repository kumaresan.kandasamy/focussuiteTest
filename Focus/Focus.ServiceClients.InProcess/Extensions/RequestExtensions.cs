﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Threading;
using Focus.Core;
using Focus.Core.Messages;
using Focus.Core.Settings.Interfaces;
using Focus.Web.Core.Models;

#endregion

namespace Focus.ServiceClients.InProcess.Extensions
{
	public static class RequestExtensions
	{
		/// <summary>
		/// Helper method that prepares request types.
		/// </summary>
		/// <typeparam name="T">The request type.</typeparam>
		/// <param name="request">The request</param>
		/// <param name="app">The application.</param>
		/// <param name="settings">The settings.</param>
		/// <param name="defaultSessionId">The default session id.</param>
		/// <param name="useAccessToken">Whether to pass an access token in the request</param>
		/// <returns>
		/// Fully prepared request, ready to use.
		/// </returns>
		public static T Prepare<T>(this T request, AppContextModel app, IAppSettings settings, Guid? defaultSessionId = null, bool useAccessToken = true) where T : ServiceRequest
		{
			request.ClientTag = app.ClientTag;
			request.SessionId = defaultSessionId ?? app.SessionId;
			request.RequestId = app.RequestId;

			request.UserContext = app.User;
			request.UserContext.Culture = Thread.CurrentThread.CurrentUICulture.ToString().ToUpperInvariant();
		  
      request.AccessToken = useAccessToken && settings.UsesAccessToken
        ? AccessTokenUtils.GenerateToken(settings.AccessTokenKey, settings.AccessTokenSecret) 
        : null;

	    request.VersionNumber = Constants.SystemVersion;
		 
			return request;
		}
	}
}