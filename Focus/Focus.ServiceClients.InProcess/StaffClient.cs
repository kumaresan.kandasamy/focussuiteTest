﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria.User;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Messages.StaffService;
using Focus.Core.Views;
using Focus.ServiceClients.InProcess.Extensions;
using Focus.ServiceClients.Interfaces;
using Focus.Web.Core.Models;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.InProcess
{
    public class StaffClient : ClientBase, IStaffClient
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StaffClient" /> class.
        /// </summary>
        /// <param name="appContext">The application context.</param>
        /// <param name="runtime">The runtime.</param>
        public StaffClient(AppContextModel appContext, ServiceClientRuntime runtime) : base(appContext, runtime) { }

        /// <summary>
        /// Searches the staff.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public PagedList<StaffSearchViewDto> SearchStaff(StaffCriteria criteria)
        {
            var request = new StaffSearchRequest().Prepare(AppContext, Runtime.Settings);
            request.Criteria = criteria;

            var response = StaffService.SearchStaff(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.StaffSearchViewsPaged;
        }

        /// <summary>
        /// Gets the assist user activities as paged.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public PagedList<AssistUserActivityViewDto> GetAssistUserActivitiesAsPaged(StaffCriteria criteria)
        {
            var request = new AssistUserActivityRequest().Prepare(AppContext, Runtime.Settings);
            request.Criteria = criteria;

            var response = StaffService.GetAssistUserActivityView(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.AssistUserActivitiesPaged;
        }

        /// <summary>
        /// Gets the assist user activities as list.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public List<AssistUserActivityViewDto> GetAssistUserActivitiesAsList(StaffCriteria criteria)
        {
            var request = new AssistUserActivityRequest().Prepare(AppContext, Runtime.Settings);
            request.Criteria = criteria;

            var response = StaffService.GetAssistUserActivityView(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.AssistUserActivitiesList;
        }

        /// <summary>
        /// Gets the assist user employers assisted.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public PagedList<AssistUserEmployersAssistedViewDto> GetAssistUserEmployersAssisted(StaffCriteria criteria)
        {
            var request = new AssistUserEmployersAssistedRequest().Prepare(AppContext, Runtime.Settings);
            request.Criteria = criteria;

            var response = StaffService.GetAssistUserEmployersAssistedView(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.AssistUserEmployersAssistedViewsPaged;
        }

        /// <summary>
        /// Gets the assist user job seekers assisted.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public PagedList<AssistUserJobSeekersAssistedViewDto> GetAssistUserJobSeekersAssisted(StaffCriteria criteria)
        {
            var request = new AssistUserJobSeekersAssistedRequest().Prepare(AppContext, Runtime.Settings);
            request.Criteria = criteria;

            var response = StaffService.GetAssistUserJobSeekersAssistedView(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.AssistUserJobSeekersAssistedViewsPaged;
        }

        /// <summary>
        /// Gets the staff profile.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public StaffProfileModel GetStaffProfile(long userId)
        {
            var request = new StaffInfoRequest().Prepare(AppContext, Runtime.Settings);
            request.Id = userId;

            var response = StaffService.GetStaffProfile(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

            return new StaffProfileModel
            {
                Person = response.Person,
                PersonAddresses = response.PersonAddresses,
                PhoneNumbers = response.PhoneNumbers,
                Logins7Days = response.Logins7Days,
                LastLogin = response.LastLogin,
                ActivityEmployerAccountsAssisted30Days = response.ActivityEmployerAccountsAssisted30Days,
                ActivityEmployerAccountsAssisted7Days = response.ActivityEmployerAccountsAssisted7Days,
                ActivityEmployerAccountsCreated30Days = response.ActivityEmployerAccountsCreated30Days,
                ActivityEmployerAccountsCreated7Days = response.ActivityEmployerAccountsCreated7Days,
                ActivityJobOrdersCreated30Days = response.ActivityJobOrdersCreated30Days,
                ActivityJobOrdersCreated7Days = response.ActivityJobOrdersCreated7Days,
                ActivityJobOrdersEdited30Days = response.ActivityJobOrdersEdited30Days,
                ActivityJobOrdersEdited7Days = response.ActivityJobOrdersEdited7Days,
                ActivityJobSeekersAssisted30Days = response.ActivityJobSeekersAssisted30Days,
                ActivityJobSeekersAssisted7Days = response.ActivityJobSeekersAssisted7Days,
                ActivityJobSeekersUnsubscribed30Days = response.ActivityJobSeekersUnsubscribed30Days,
                ActivityJobSeekersUnsubscribed7Days = response.ActivityJobSeekersUnsubscribed7Days,
                ActivityReferrals30Days = response.ActivityReferrals30Days,
                ActivityReferrals7Days = response.ActivityReferrals7Days,
                ActivityResumesCreated30Days = response.ActivityResumesCreated30Days,
                ActivityResumesCreated7Days = response.ActivityResumesCreated7Days,
                ActivityResumesEdited30Days = response.ActivityResumesEdited30Days,
                ActivityResumesEdited7Days = response.ActivityResumesEdited7Days
            };
        }

        /// <summary>
        ///Gets the Backdate Max Days settings for a Staff.
        /// </summary>
        /// <param name="personId">The Id of person.</param>
        /// <returns></returns>
        public PersonActivityUpdateSettingsDto GetBackdateSettings(long personId)
        {
            var request = new StaffBackdateSettingsRequest { PersonId = personId }.Prepare(AppContext, Runtime.Settings);

            var response = StaffService.GetStaffBackdateSettings(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);

            return response.StaffBackdateSettings;
        }

        /// <summary>
        /// Sets the Staff Backdate activity settings.
        /// </summary>
        /// <param name="staffBackdateSettings">The dto.</param>
        public void UpdateStaffBackdateSettings(PersonActivityUpdateSettingsDto staffBackdateSettings)
        {
            var request = new SaveStaffBackdateSettingRequest { StaffBackdateSettings = staffBackdateSettings }.Prepare(AppContext, Runtime.Settings);

            var response = StaffService.UpdateStaffBackdateSettings(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }

        /// <summary>
        /// Creates the homepage alert.
        /// </summary>
        /// <param name="homepageAlert">The homepage alert.</param>
        /// <exception cref="ServiceCallException"></exception>
        public void CreateHomepageAlert(StaffHomepageAlertView homepageAlert)
        {
            var request = new CreateStaffHomepageAlertRequest().Prepare(AppContext, Runtime.Settings);
            request.HomepageAlert = homepageAlert;

            var response = StaffService.CreateStaffHomepageAlert(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }
    }
}