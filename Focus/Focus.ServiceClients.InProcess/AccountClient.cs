﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Role;
using Focus.Core.Criteria.User;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Messages.AccountService;
using Focus.Core.Models;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Focus.ServiceClients.InProcess.Extensions;
using Focus.ServiceClients.Interfaces;
using Focus.Web.Core.Models;
using Framework.Core;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.InProcess
{
	public class AccountClient : ClientBase, IAccountClient
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="AccountClient" /> class.
		/// </summary>
		/// <param name="appContext">The application context.</param>
		/// <param name="runtime">The runtime.</param>
		public AccountClient(AppContextModel appContext, ServiceClientRuntime runtime) : base(appContext, runtime) {}

		/// <summary>
		/// Blocks a person
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="deactivateJobseeker">if set to <c>true</c> [deactivate jobseeker].</param>
		/// <param name="reasonText">The reason text.</param>
		/// <returns>
		/// A boolean indicating success or failure (Failure means the account was already blocked)
		/// </returns>
		/// <exception cref="ServiceCallException"></exception>
		public bool BlockPerson(long personId, bool deactivateJobseeker = false, string reasonText = "")
		{
			var request = new BlockUnblockPersonRequest
			{
				PersonId = personId,
				Block = true,
				ReasonText = reasonText
			}.Prepare(AppContext, Runtime.Settings);

			var response = AccountService.BlockUnblockPerson(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Success;
		}

		/// <summary>
		/// Unblocks a person
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns>A boolean indicating success or failure (Failure means the account was already unblocked)</returns>
		public bool UnblockPerson(long personId)
		{
			var request = new BlockUnblockPersonRequest
			{
				PersonId = personId,
				Block = false
			}.Prepare(AppContext, Runtime.Settings);

			var response = AccountService.BlockUnblockPerson(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Success;
		}

		/// <summary>
		/// Changes the password.
		/// </summary>
		/// <param name="currentPassword">The current password.</param>
		/// <param name="newPassword">The new password.</param>
		public bool ChangePassword(string currentPassword, string newPassword)
		{
			var request = new ChangePasswordRequest().Prepare(AppContext, Runtime.Settings);
			request.CurrentPassword = currentPassword;
			request.NewPassword = newPassword;

			var response = AccountService.ChangePassword(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return true;
		}

		/// <summary>
		/// Changes the name of the user.
		/// </summary>
		/// <param name="currentUsername">The current username.</param>
		/// <param name="newUsername">The new username.</param>
		public bool ChangeUsername(string currentUsername, string newUsername)
		{
			var request = new ChangeUsernameRequest().Prepare(AppContext, Runtime.Settings);
			request.CurrentUsername = currentUsername;
			request.NewUsername = newUsername;

			var response = AccountService.ChangeUsername(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return true;
		}

		/// <summary>
		/// Checks if an email address is in use by another user
		/// </summary>
		/// <param name="emailAddress">The email address.</param>
		/// <param name="personId">The ID of the person who requires the email address</param>
		public bool CheckEmailAddress(string emailAddress, long? personId = null)
		{
			var request = new CheckEmailAddressRequest
			{
				PersonId = personId,
				EmailAddress = emailAddress
			}.Prepare(AppContext, Runtime.Settings);

			var response = AccountService.CheckEmailAddress(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.EmailInUse;
		}

		/// <summary>
		/// Changes the email address of the user.
		/// </summary>
		/// <param name="currentEmailAddress">The current email address.</param>
		/// <param name="newEmailAddress">The new email address.</param>
		public bool ChangeEmailAddress(string currentEmailAddress, string newEmailAddress)
		{
			var request = new ChangeEmailAddressRequest().Prepare(AppContext, Runtime.Settings);
			request.CurrentEmailAddress = currentEmailAddress;
			request.NewEmailAddress = newEmailAddress;

			var response = AccountService.ChangeEmailAddress(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return true;
		}

		public bool ChangeCareerAccountType(CareerAccountType newAccountType)
		{
			var request = new ChangeCareerAccountTypeRequest().Prepare(AppContext, Runtime.Settings);
			request.NewAccountType = newAccountType;

			var response = AccountService.ChangeCareerAccountType(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return true;
		}

		/// <summary>
		/// Checks the user exists.
		/// </summary>
		/// <param name="registrationModel">The registration model.</param>
		/// <param name="existingUserId">The id of the existing user if amending a user</param>
		/// <param name="checkLocalOnly">Whether to only check the local database, or also any external client</param>
		/// <returns>A boolean indicating whether the user exists or not </returns>
		public Tuple<bool, ExtenalUserPreExistenceReason> CheckUserExists(RegistrationModel registrationModel, long? existingUserId = null, bool checkLocalOnly = false)
		{
			var request = new CheckUserExistsRequest().Prepare(AppContext, Runtime.Settings);
			request.ExistingUserId = existingUserId;
			request.ModuleToCheck = registrationModel.ModuleToCheck ?? Runtime.Settings.Module;
			request.UserName = registrationModel.AccountUserName;
			request.CheckLocalOnly = checkLocalOnly;
			var careerRegistrationModel = registrationModel as CareerRegistrationModel;
			if (careerRegistrationModel != null)
			{
				request.DateOfBirth = careerRegistrationModel.DateOfBirth;
				request.EmailAddress = careerRegistrationModel.EmailAddress;
				request.SSN = careerRegistrationModel.SocialSecurityNumber;
				request.FirstName = careerRegistrationModel.FirstName;
				request.LastName = careerRegistrationModel.LastName;
				request.PrimaryPhone = careerRegistrationModel.PrimaryPhone;
				request.PostalAddress = careerRegistrationModel.PostalAddress;
			}

			var response = AccountService.CheckUserExists(request);
			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return new Tuple<bool, ExtenalUserPreExistenceReason>(response.Exists, response.ExistenceReason);
		}

		public bool CheckSocialSecurityNumberInUse(string socialSecurityNumber)
		{
			CheckSocialSecurityNumberInUseRequest request = new CheckSocialSecurityNumberInUseRequest().Prepare(AppContext,
				Runtime.Settings);
			request.SocialSecurityNumber = socialSecurityNumber;
			CheckSocialSecurityNumberInUseResponse response = AccountService.CheckSocialSecurityNumberInUse(request);
			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Exists;
		}

		/// <summary>
		/// Registers the Talent user.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <param name="initialApprovalStatus">Override the initial approval status.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public TalentRegistrationModel RegisterTalentUser(TalentRegistrationModel model, ApprovalStatuses? initialApprovalStatus = null)
		{
			var request = new RegisterTalentUserRequest().Prepare(AppContext, Runtime.Settings);

			request.AccountUserName = model.AccountUserName;
			request.AccountPassword = model.AccountPassword;
			request.EmployeePerson = model.EmployeePerson;
			request.EmployeeAddress = model.EmployeeAddress;
			request.EmployeePhone = model.EmployeePhone;
			request.EmployeePhoneExtension = model.EmployeePhoneExtension;
			request.EmployeePhoneType = model.EmployeePhoneType;
			request.EmployeeAlternatePhone1 = model.EmployeeAlternatePhone1;
			request.EmployeeAlternatePhone1Type = model.EmployeeAlternatePhone1Type;
			request.EmployeeAlternatePhone2 = model.EmployeeAlternatePhone2;
			request.EmployeeAlternatePhone2Type = model.EmployeeAlternatePhone2Type;
			request.EmployeeEmailAddress = model.EmployeeEmailAddress;
			request.Employer = model.Employer;
			request.EmployerExternalId = model.EmployerExternalId;
			request.EmployerAddress = model.EmployerAddress;
			request.BusinessUnit = model.BusinessUnit;
			request.BusinessUnitDescription = model.BusinessUnitDescription;
			request.BusinessUnitAddress = model.BusinessUnitAddress;
			request.User = model.User;
			request.SecurityQuestions = model.UserSecurityQuestions;
			request.Module = Runtime.Settings.Module;
			request.InitialApprovalStatus = initialApprovalStatus;

			var response = AccountService.RegisterTalentUser(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
			{
				if (response.Error == ErrorTypes.UserNameAlreadyExists)
					model.RegistrationError = ErrorTypes.UserNameAlreadyExists;
				else
					throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
			}

			model.UserId = response.UserId;
			model.EmployeeId = response.EmployeeId;

			var userContext = new UserContext();

			if (Runtime.Settings.OAuthEnabled || Runtime.Settings.SamlEnabled || Runtime.Settings.SamlEnabledForTalent)
			{
				userContext = new UserContext(response.UserId, response.UserId, model.EmployeePerson.FirstName,
					model.EmployeePerson.LastName,
					model.EmployeePerson.EmailAddress, model.EmployeePerson.Id, model.EmployeeId, response.EmployerId,
					AppContext.User.ExternalUserId, AppContext.User.ExternalUserName, AppContext.User.ExternalPassword,
					AppContext.User.ExternalOfficeId, model.User.ScreenName, model.User.UserName,
					response.LastLoggedInOn, response.IsMigrated, response.RegulationsConsent);
			}

			model.UserContext = userContext;
			model.EmployeeApprovalStatus = response.EmployeeApprovalStatus;

			return model;
		}

		/// <summary>
		/// Registers the career user.
		/// </summary>
		/// <param name="model">The career registration model.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void RegisterCareerUser(CareerRegistrationModel model)
		{
			var request = new RegisterCareerUserRequest().Prepare(AppContext, Runtime.Settings);
			request.AccountUserName = model.EmailAddress;
			request.AccountPassword = model.AccountPassword;
			request.DateOfBirth = model.DateOfBirth;
			request.EmailAddress = model.EmailAddress;
			request.SocialSecurityNumber = model.SocialSecurityNumber;
			request.FirstName = model.FirstName;
			request.MiddleInitial = model.MiddleName;
			request.LastName = model.LastName;
			request.SuffixId = model.SuffixId;
			request.PrimaryPhone = model.PrimaryPhone;
			request.PostalAddress = model.PostalAddress;
			request.SecurityQuestion = model.SecurityQuestion;
			request.SecurityQuestionId = model.SecurityQuestionId;
			request.SecurityAnswer = model.SecurityAnswer;
			request.OverrideOfficeId = model.OverrideOfficeId;
			request.ExternalId = model.ExternalId;
			request.AccountType = model.AccountType;
			// Education registration
			request.ProgramOfStudyId = model.ProgramAreaId;
			request.DegreeId = model.DegreeId;
			request.EnrollmentStatus = model.EnrollmentStatus;
			request.CampusId = model.CampusId;
			request.Pin = model.Pin;
			request.PinRegisteredEmailAddress = model.PinRegisteredEmailAddress;
			request.ScreenName = model.ScreenName;
			var response = AccountService.RegisterCareerUser(request);
			Correlate(request, response);
			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			if (response.Acknowledgement == AcknowledgementType.Failure)
			{
				if (response.Error == ErrorTypes.UserNameAlreadyExists)
					model.RegistrationError = ErrorTypes.UserNameAlreadyExists;
				else
					throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
			}

			model.UserId = response.UserId;
		}

		/// <summary>
		/// Registers the explorer user.
		/// </summary>
		/// <param name="model">The model.</param>
		public void RegisterExplorerUser(ExplorerRegistrationModel model)
		{
			var request = new RegisterExplorerUserRequest().Prepare(AppContext, Runtime.Settings);

			request.ScreenName = model.ScreenName;
			request.EmailAddress = model.EmailAddress;
			request.Password = model.AccountPassword;
			request.SocialSecurityNumber = model.SocialSecurityNumber;
			request.SecurityQuestion = model.SecurityQuestion;
			request.SecurityQuestionId = model.SecurityQuestionId;
			request.SecurityAnswer = model.SecurityAnswer;
			request.FirstName = model.FirstName;
			request.MiddleInitial = model.MiddleInitial;
			request.LastName = model.LastName;
			request.DateOfBirth = model.DateOfBirth;
			request.PhoneNumber = model.PhoneNumber;
			request.TandCConsentGiven = model.TandCConsentGiven;
			request.ExternalId = model.ExternalId;

			var response = AccountService.RegisterExplorerUser(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
			{
				if (response.Error == ErrorTypes.UserNameAlreadyExists)
					model.RegistrationError = ErrorTypes.UserNameAlreadyExists;
				else
					throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
			}

			model.ResumeModel = response.ResumeModel;
		}

		/// <summary>
		/// Finds the user. 
		/// </summary>
		/// <param name="findBy">The find by.</param>
		/// <param name="firstName">The first name.</param>
		/// <param name="lastName">The last name.</param>
		/// <param name="emailAddress">The email address.</param>
		/// <param name="externalOffice">The external office.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <returns></returns>
		private PagedList<UserView> FindUsers(FindUserRequest.UserFindBy findBy, string firstName, string lastName, string emailAddress, string externalOffice, int pageIndex, int pageSize)
		{
			var request = new FindUserRequest().Prepare(AppContext, Runtime.Settings);
			request.FindBy = findBy;
			request.SearchFirstName = firstName;
			request.SearchLastName = lastName;
			request.SearchEmailAddress = emailAddress;
			request.SearchExternalOffice = externalOffice;
			request.PageIndex = pageIndex;
			request.PageSize = pageSize;

			var response = AccountService.FindUser(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Users;
		}

		/// <summary>
		/// Finds the name of the users by.
		/// </summary>
		/// <param name="firstName">The first name.</param>
		/// <param name="lastName">The last name.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <returns></returns>
		public PagedList<UserView> FindUsersByName(string firstName, string lastName, int pageIndex, int pageSize)
		{
			return FindUsers(FindUserRequest.UserFindBy.Name, firstName, lastName, null, null, pageIndex, pageSize);
		}

		/// <summary>
		/// Finds the users by email.
		/// </summary>
		/// <param name="emailAddress">The email address.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <returns></returns>
		public PagedList<UserView> FindUsersByEmail(string emailAddress, int pageIndex, int pageSize)
		{
			return FindUsers(FindUserRequest.UserFindBy.Email, null, null, emailAddress, null, pageIndex, pageSize);
		}

		/// <summary>
		/// Finds the users by office.
		/// </summary>
		/// <param name="externalOffice">The external office.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <returns></returns>
		public PagedList<UserView> FindUsersByOffice(string externalOffice, int pageIndex, int pageSize)
		{
			return FindUsers(FindUserRequest.UserFindBy.ExternalOffice, null, null, null, externalOffice, pageIndex, pageSize);
		}

		/// <summary>
		/// Finds the users by external identifier.
		/// </summary>
		/// <param name="externalId">The external identifier.</param>
		/// <returns></returns>
		public UserView FindUserByExternalId(string externalId)
		{
			var request = new FindUserRequest().Prepare(AppContext, Runtime.Settings);
			request.FindBy = FindUserRequest.UserFindBy.ExternalId;
			request.SearchExternalId = externalId;
			request.PageIndex = 0;
			request.PageSize = 2;

			var response = AccountService.FindUserByExternalId(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Users.SingleOrDefault();
		}

		/// <summary>
		/// Gets the user type of a user
		/// </summary>
		/// <param name="userId">The id of the user</param>
		/// <returns>The user type</returns>
		public UserTypes GetUserType(long userId)
		{
			var request = new GetUserTypeRequest().Prepare(AppContext, Runtime.Settings);
			request.UserId = userId;

			var response = AccountService.GetUserType(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.UserType;
		}

		/// <summary>
		/// Gets the user details.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="personId">The person id.</param>
		/// <param name="getSecurityAnswer">Whether to get the decrypted security answer.</param>
		/// <returns></returns>
		public UserDetailsModel GetUserDetails(long userId = 0, long personId = 0, bool getSecurityAnswer = false)
		{
			var request = new GetUserDetailsRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new UserCriteria
			{
				UserId = userId,
				PersonId = personId,
				GetSecurityAnswer = getSecurityAnswer
			};

			var response = AccountService.GetUserDetails(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return new UserDetailsModel
			{
				UserId = Convert.ToInt64(response.UserDetails.Id),
				UserDetails = response.UserDetails,
				PersonDetails = response.PersonDetails,
				PrimaryPhoneNumber = response.PrimaryPhoneNumber,
				AlternatePhoneNumber1 = response.AlternatePhoneNumber1,
				AlternatePhoneNumber2 = response.AlternatePhoneNumber2,
				AddressDetails = response.AddressDetails,
				PersonOffices = response.OfficeRoles,
				SecurityAnswer = getSecurityAnswer ? response.SecurityAnswer : null,
				SecurityQuestion = getSecurityAnswer ? response.SecurityQuestion : null,
				SecurityQuestionId = getSecurityAnswer ? response.SecurityQuestionId : null,
				LockVersion = response.EmployeeLockNumber
			};
		}

		/// <summary>
		/// Get Security Questions for user
		/// </summary>
		/// <param name="questionIndex">The specific security question index - pass 0 to get all of them</param>
		/// <returns></returns>
		public UserSecurityQuestionDto[] GetUserSecurityQuestion(int questionIndex = 0)
		{
			var request = new SecurityQuestionDetailsRequest().Prepare( AppContext, Runtime.Settings );
			request.UserId = request.UserContext.UserId;
			request.QuestionIndex = questionIndex;

			var response = AccountService.GetUserSecurityQuestion(request);

			if( response.Acknowledgement == AcknowledgementType.Failure )
				throw new ServiceCallException( response.Message, (int)response.Error, response.Exception );

			return response.UserSecurityQuestions.ToArray();
		}

		/// <summary>
		/// Whether the logged on user has security questions
		/// </summary>
		/// <returns></returns>
		public bool HasUserSecurityQuestions(long userId)
		{
			var request = new SecurityQuestionDetailsRequest
			{
				UserId = userId
			}.Prepare(AppContext, Runtime.Settings);

			var response = AccountService.HasUserSecurityQuestions(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.HasQuestions;
		}


		/// <summary>
		/// Updates the contact details.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns>User context for updating</returns>
		public UserDto UpdateContactDetails(UserDetailsModel model)
		{
			bool failedCensorshipCheck;
			return UpdateContactDetails(model, false, out failedCensorshipCheck);
		}

		/// <summary>
		/// Updates the contact details.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <param name="checkCensorship">Whether to check the censorship</param>
		/// <param name="failedCensorshipCheck">Whether the update details failed censorship checks</param>
		/// <returns>User context for updating</returns>
		public UserDto UpdateContactDetails(UserDetailsModel model, bool checkCensorship, out bool failedCensorshipCheck)
		{
			var request = new UpdateContactDetailsRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new UserCriteria { UserId = model.UserId, UserName = model.UserDetails.UserName };
			request.PersonDetails = model.PersonDetails;
			request.PrimaryContactDetails = model.PrimaryPhoneNumber;
			request.Alternate1ContactDetails = model.AlternatePhoneNumber1;
			request.Alternate2ContactDetails = model.AlternatePhoneNumber2;
			request.AddressDetails = model.AddressDetails;
			request.UserDetails = model.UserDetails;
			request.CheckCensorship = checkCensorship;
			request.LockVersion = model.LockVersion;

			var response = AccountService.UpdateContactDetails(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			failedCensorshipCheck = response.FailedCensorshipCheck;
			model.LockVersion = response.LockVersion ?? model.LockVersion;

			return response.User;
		}

		/// <summary>
		/// Gets the job seeker's SSN
		/// </summary>
		/// <param name="personId">The Id of the job seeker</param>
		/// <returns>The job seeker's SSN</returns>
		public string GetSSN(long personId)
		{
			var request = new GetSSNRequest { PersonId = personId }.Prepare(AppContext, Runtime.Settings);

			var response = AccountService.GetSSN(request);
			if (response.Acknowledgement == AcknowledgementType.Failure)
			{
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
			}

			return response.SSN;
		}

		/// <summary>
		/// Updates the SSN.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public PersonDto UpdateSsn(UpdateSsnModel model)
		{
			var request = new UpdateSsnRequest { Id = model.PersonId, CurrentSsn = model.CurrentSsn, NewSsn = model.NewSsn }.Prepare(AppContext, Runtime.Settings);

			var response = AccountService.UpdateSsn(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
			{
				if (response.UiError == ErrorTypes.CurrentSsnDoesNotMatch || response.UiError == ErrorTypes.SsnAlreadyExists)
				{
					model.Error = response.UiError;
					model.ErrorText = response.ErrorText;
				}
				else
					throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
			}

			return response.Person;
		}

		/// <summary>
		/// Updates the user.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public long UpdateUser(UserDetailsModel model)
		{
			var request = new RegisterCareerUserRequest().Prepare(AppContext, Runtime.Settings);

			#region Set up request object

			#region Person data

			request.FirstName = model.PersonDetails.FirstName;
			request.MiddleInitial = model.PersonDetails.MiddleInitial;
			request.LastName = model.PersonDetails.LastName;
			request.DateOfBirth = model.PersonDetails.DateOfBirth;
			request.EmailAddress = model.PersonDetails.EmailAddress;
			request.ProgramOfStudyId = model.PersonDetails.ProgramAreaId;
			request.DegreeId = model.PersonDetails.DegreeId;
			request.EnrollmentStatus = model.PersonDetails.EnrollmentStatus;
			request.CampusId = model.PersonDetails.CampusId;

			#endregion

			#region User details

			request.UpdateUserId = model.UserId;
			request.AccountUserName = model.UserDetails.UserName;
			request.AccountPassword = model.Password;
			request.SecurityQuestion = model.SecurityQuestion;
			request.SecurityQuestionId = model.SecurityQuestionId;
			request.SecurityAnswer = model.SecurityAnswer;

			#endregion

			#region Cellphone number

			if (model.PrimaryPhoneNumber.IsNotNull())
			{
				request.UpdatePhoneNumber = new PhoneNumberDto
				{
					IsPrimary = model.PrimaryPhoneNumber.IsPrimary,
					Number = model.PrimaryPhoneNumber.Number,
					Extension = model.PrimaryPhoneNumber.Extension,
					PhoneType = model.PrimaryPhoneNumber.PhoneType,
					ProviderId = model.PrimaryPhoneNumber.ProviderId
				};
			}

			#endregion

			#region Address details

			request.PostalAddress = new Address
			{
				Street1 = model.AddressDetails.Line1,
				City = model.AddressDetails.TownCity,
				StateId = model.AddressDetails.StateId,
				Zip = model.AddressDetails.PostcodeZip
			};

			#endregion

			#endregion

			var response = AccountService.UpdateUser(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.UserId;
		}

		/// <summary>
		/// Validates the employer.
		/// </summary>
		/// <param name="federalEmployerIdentificationNumber">The federal employer identification number.</param>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public ValidatedEmployerView ValidateEmployer(string federalEmployerIdentificationNumber, long? employerId = null)
		{
			var request = new ValidateEmployerRequest().Prepare(AppContext, Runtime.Settings);
			request.FederalEmployerIdentificationNumber = federalEmployerIdentificationNumber;
			request.EmployerId = employerId;

			var response = AccountService.ValidateEmployer(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.ValidatedEmployer;
		}

		/// <summary>
		/// Gets the assist user.
		/// </summary>
		/// <returns></returns>
		public List<UserLookup> GetAssistUsers()
		{
			var request = new UserLookupRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new UserCriteria
			{
				UserRole = Constants.RoleKeys.AssistUser,
				Enabled = true
			};

			var response = AccountService.GetUserLookup(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Users;
		}

		/// <summary>
		/// Gets the users roles.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <returns></returns>
		public List<RoleDto> GetUsersRoles(long userId)
		{
			var request = new RoleRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new RoleCriteria
			{
				UserId = userId,
				FetchOption = CriteriaBase.FetchOptions.List
			};

			var response = AccountService.GetRole(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Roles;
		}

		/// <summary>
		/// Gets the roles for user.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <returns></returns>
		public List<string> GetRolesForUser(long userId)
		{
			var request = new RoleRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new RoleCriteria
			{
				UserId = userId,
				FetchOption = CriteriaBase.FetchOptions.Lookup
			};

			var response = AccountService.GetRole(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return (response.RolesLookup.IsNotNull()) ? response.RolesLookup.Select(x => x.Value).ToList() : new List<string>();
		}

		/// <summary>
		/// Updates the users roles.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="usersRoles">The users roles.</param>
		public void UpdateUsersRoles(long userId, List<RoleDto> usersRoles)
		{
			var request = new UpdateUsersRolesRequest().Prepare(AppContext, Runtime.Settings);
			request.UserId = userId;
			request.Roles = usersRoles;

			var response = AccountService.UpdateUsersRoles(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
		}

		/// <summary>
		/// Resets the users password. Generates new password and sends out reset password email.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="changePasswordUrl">The change password URL.</param>
		public void ResetUsersPassword(long userId, string changePasswordUrl)
		{
			var request = new ResetUsersPasswordRequest().Prepare(AppContext, Runtime.Settings);
			request.ResetUserId = userId;
			request.ResetPasswordUrl = changePasswordUrl;

			var response = AccountService.ResetUsersPassword(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
		}

		/// <summary>
		/// Deactivates the account
		/// THIS METHOD IS CURRENTLY NOT BEING USED ANYWHERE
		/// IF YOU ARE LOOKING TO DEACTIVATE A USER REFER TO THE AccountService.InactivateJobSeeker METHOD FOR A POSSIBLE ALTERNATIVE
		/// </summary>
		/// <param name="userId">The user id.</param>
		public void DeactivateAccount(long userId)
		{
			var request = new DeactivateUserRequest().Prepare(AppContext, Runtime.Settings);
			request.UserId = userId;

			var response = AccountService.DeactivateUser(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
		}

		/// <summary>
		/// Activates the account.
		/// </summary>
		/// <param name="userId">The user id.</param>
		public void ActivateAccount(long userId)
		{
			var request = new ActivateUserRequest().Prepare(AppContext, Runtime.Settings);
			request.UserId = userId;

			var response = AccountService.ActivateUser(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
		}

		/// <summary>
		/// Sends the activation email.
		/// </summary>
		/// <param name="name">The user's name.</param>
		/// <param name="email">The email.</param>
		/// <param name="validationUrl">The validation URL.</param>
		public void SendActivationEmail(string name, string email, string validationUrl)
		{
			// this method is used when registering
			var request = new SendAccountActivationEmailRequest().Prepare(AppContext, Runtime.Settings);
			request.Name = name;
			request.Email = email;
			request.SingleSignOnURL = validationUrl;
			request.Module = Runtime.Settings.Module;

			var response = AccountService.SendAccountActivationEmail(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
		}

		/// <summary>
		/// Sends the activation email.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <param name="validationUrl">The validation URL.</param>
		public void SendActivationEmail(string username, string validationUrl)
		{
			// this method is used when login fails due to user not enabled
			var request = new SendAccountActivationEmailRequest().Prepare(AppContext, Runtime.Settings);
			request.UserName = username;
			request.SingleSignOnURL = validationUrl;
			request.Module = Runtime.Settings.Module;

			var response = AccountService.SendAccountActivationEmail(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
		}

		/// <summary>
		/// Resets the password.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="newPassword">The new password.</param>
		/// <param name="validationKey">The validation key.</param>
		/// <param name="pinUrl">The pin URL.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public AccountModel ResetPassword(long? userId = null, string newPassword = null, string validationKey = null, string pinUrl = null)
		{
			var request = new ResetPasswordRequest().Prepare(AppContext, Runtime.Settings);
			request.Module = Runtime.Settings.Module;
			request.UserId = userId;
			request.NewPassword = newPassword;
			request.ValidationKey = validationKey;
			request.PinUrl = pinUrl;

			var response = AccountService.ResetPassword(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return new AccountModel { AccountUserName = response.AccountUserName, AccountPassword = response.AccountPassword };
		}

		/// <summary>
		/// Updates the screen name and email address.
		/// </summary>
		/// <param name="screenName">Name of the screen.</param>
		/// <param name="emailAddress">The email address.</param>
		public void UpdateScreenNameAndEmailAddress(string screenName, string emailAddress)
		{
			var request = new UpdateScreenNameAndEmailAddressRequest().Prepare(AppContext, Runtime.Settings);
			request.ScreenName = screenName;
			request.EmailAddress = emailAddress;

			var response = AccountService.UpdateScreenNameAndEmailAddress(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
		}

		/// <summary>
		/// Changes the security question.
		/// </summary>
		/// <param name="question">The question.</param>
		/// <param name="questionId">The question Id or null if user defined question</param>
		/// <param name="answer">The answer.</param>
		/// <param name="questionIndex">The security question index</param>
		public bool ChangeSecurityQuestion( string question, long? questionId, string answer, int questionIndex = 1 )
		{
			var userSecurityQuestion = new UserSecurityQuestionDto
			{
				SecurityQuestion = question,
				SecurityQuestionId = questionId,
				SecurityAnswerEncrypted = answer,
				QuestionIndex = questionIndex
			};

			var request = new ChangeSecurityQuestionRequest
			{
				Questions =  new List<UserSecurityQuestionDto> { userSecurityQuestion },
				ChangeSingleQuestion = true
			}.Prepare(AppContext, Runtime.Settings);

			var response = AccountService.ChangeSecurityQuestion(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return true;
		}

		/// <summary>
		/// Changes the security questions.
		/// </summary>
		/// <param name="questions">The question.</param>
		public bool ChangeSecurityQuestions(List<UserSecurityQuestionDto> questions)
		{
			var request = new ChangeSecurityQuestionRequest
			{
				Questions = questions,
				ChangeSingleQuestion = false
			}.Prepare(AppContext, Runtime.Settings);

			var response = AccountService.ChangeSecurityQuestion(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return true;
		}

		/// <summary>
		/// Gets the user alert settings.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <returns></returns>
		public UserAlertDto GetUserAlert(long userId)
		{
			var request = new GetUserAlertRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new UserAlertCriteria { UserId = userId };

			var response = AccountService.GetUserAlert(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.UserAlert;
		}

		/// <summary>
		/// Updates the user's alert settings.
		/// </summary>
		/// <param name="userAlert">The user's alert settings.</param>
		/// <returns>A User Alert record</returns>
		public UserAlertDto UpdateUserAlert(UserAlertDto userAlert)
		{
			var request = new UpdateUserAlertRequest().Prepare(AppContext, Runtime.Settings);
			request.UserAlert = userAlert;

			var response = AccountService.UpdateUserAlert(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.UserAlert;
		}

		/// <summary>
		/// Changes the migrated status.
		/// </summary>
		/// <param name="isMigrated">if set to <c>true</c> [is migrated].</param>
		/// <returns></returns>
		public bool ChangeMigratedStatus(bool isMigrated)
		{
			var request = new ChangeMigratedStatusRequest().Prepare(AppContext, Runtime.Settings);

			request.IsMigrated = isMigrated;

			var response = AccountService.ChangeMigratedStatus(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return true;
		}

		/// <summary>
		/// Changes the consent status.
		/// </summary>
		/// <param name="isConsentGiven">if set to <c>true</c> [is consent given].</param>
		/// <returns></returns>
		public bool ChangeConsentStatus(bool isConsentGiven)
		{
			var request = new ChangeConsentStatusRequest().Prepare(AppContext, Runtime.Settings);

			request.IsConsentGiven = isConsentGiven;

			var response = AccountService.ChangeConsentStatus(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return true;
		}

		/// <summary>
		/// Changes the program of study.
		/// </summary>
		/// <param name="newStudyProgramAreaId">The new study program area id.</param>
		/// <param name="newStudyProgramDegreeId">The new study program degree id.</param>
		/// <returns></returns>
		public bool ChangeProgramOfStudy(long? newStudyProgramAreaId, long? newStudyProgramDegreeId)
		{
			var request = new ChangeProgramOfStudyRequest().Prepare(AppContext, Runtime.Settings);
			request.StudyProgramAreaId = newStudyProgramAreaId;
			request.StudyProgramDegreeId = newStudyProgramDegreeId;

			var response = AccountService.ChangeProgramOfStudy(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return true;
		}

		/// <summary>
		/// Changes the enrollment status.
		/// </summary>
		/// <param name="enrollmentStatus">The enrollment status.</param>
		/// <returns></returns>
		public bool ChangeEnrollmentStatus(SchoolStatus? enrollmentStatus)
		{
			var request = new ChangeEnrollmentStatusRequest().Prepare(AppContext, Runtime.Settings);
			request.EnrollmentStatus = enrollmentStatus;

			var response = AccountService.ChangeEnrollmentStatus(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return true;
		}

		/// <summary>
		/// Changes the campus for a student.
		/// </summary>
		/// <param name="campusId">The campus location.</param>
		/// <returns></returns>
		public bool ChangeCampus(long campusId)
		{
			var request = new ChangeCampusRequest().Prepare(AppContext, Runtime.Settings);
			request.CampusId = campusId;

			var response = AccountService.ChangeCampus(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return true;
		}

		/// <summary>
		/// Changes the phone number of a person
		/// </summary>
		/// <param name="phoneNumber">The phone number.</param>
		/// <returns>A boolean indicating success</returns>
		public bool ChangePhoneNumber(PhoneNumberDto phoneNumber)
		{
			var request = new ChangePhoneNumberRequest().Prepare(AppContext, Runtime.Settings);
			request.PhoneNumber = phoneNumber;

			var response = AccountService.ChangePhoneNumber(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return true;
		}

		/// <summary>
		/// Validates the email.
		/// </summary>
		/// <param name="emails">The emails.</param>
		/// <param name="userType">Type of the user.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public EmailValidationModel ValidateEmail(List<string> emails, UserTypes userType)
		{
			var request = new ValidateEmailRequest().Prepare(AppContext, Runtime.Settings);
			request.Emails = emails;
			request.UserType = userType;

			var response = AccountService.ValidateEmail(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			var model = new EmailValidationModel
			{
				InvalidEmailFormat = response.InvalidEmailFormat,
				MultipleEmailError = response.MultipleEmailError,
				DuplicateEmailError = response.DuplicateEmailError,
				ValidEmail = response.ValidEmail,
				UsernameInUse = response.UsernameInUse
			};

			return model;
		}

		/// <summary>
		/// Sends the registration pin email.
		/// </summary>
		/// <param name="email">The email.</param>
		/// <param name="pinRegistrationUrl">The pin registration URL.</param>
		/// <param name="resetPasswordUrl">The reset password URL.</param>
		/// <param name="targetModule">The target module.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public bool SendRegistrationPinEmail(List<string> email, string pinRegistrationUrl, string resetPasswordUrl, FocusModules targetModule)
		{
			var request = new SendRegistrationPinEmailRequest().Prepare(AppContext, Runtime.Settings);
			request.Email = email;
			request.PinRegistrationUrl = pinRegistrationUrl;
			request.ResetPasswordUrl = resetPasswordUrl;
			request.TargetModule = targetModule;

			var response = AccountService.SendRegistrationPinEmail(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return true;
		}

		/// <summary>
		/// Updates the manager flag.
		/// </summary>
		/// <param name="personId">the person id</param>
		/// <param name="manager">if set to <c>true</c> [manager].</param>
		/// <exception cref="Framework.Exceptions.ServiceCallException"></exception>
		/// <exception cref="ServiceCallException"></exception>
		public void UpdateManagerFlag(long personId, bool manager)
		{
			var request = new UpdateManagerFlagRequest().Prepare(AppContext, Runtime.Settings);
			request.PersonId = personId;
			request.Manager = manager;

			var response = AccountService.UpdateManagerFlag(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
		}

		/// <summary>
		/// Creates the assist user.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void CreateAssistUser(CreateAssistUserModel model)
		{
			var request = new CreateAssistUserRequest().Prepare(AppContext, Runtime.Settings);
			model.AccountUserName = model.UserEmailAddress;
			model.IsClientAuthenticated = false;
			model.IsEnabled = true;
			request.Models = new List<CreateAssistUserModel> { model };
			request.SendEmail = true;
			request.GeneratePassword = true;

			var response = AccountService.CreateAssistUser(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
			{
				if (response.Error == ErrorTypes.UserNameAlreadyExists || response.Error == ErrorTypes.UserAlreadyExists || response.Error == ErrorTypes.BulkRegistrationValidationFailed)
				{
					if (model.RegistrationError.IsNull())
						model.RegistrationError = new List<ErrorTypes>();

					model.RegistrationError.Add(response.Error);
				}
				else
					throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
			}
		}

		/// <summary>
		/// Creates multiple assist users.
		/// </summary>
		/// <param name="models">The models.</param>
		/// <exception cref="ServiceCallException"></exception>
		public List<CreateAssistUserModel> CreateAssistUsers(List<CreateAssistUserModel> models)
		{
			var request = new CreateAssistUserRequest().Prepare(AppContext, Runtime.Settings);

			foreach (var model in models)
			{
				model.IsClientAuthenticated = false;
				model.AccountUserName = model.UserEmailAddress;
				model.IsEnabled = true;
			}

			request.Models = models;
			request.SendEmail = true;
			request.GeneratePassword = true;
			request.BulkUpload = true;

			var response = AccountService.CreateAssistUser(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
			{
				if (response.Models.All(x => x.RegistrationError.Contains(ErrorTypes.Ok)))
					throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
			}

			return response.Models;
		}

		/// <summary>
		/// Updates the last surveyed date.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void UpdateLastSurveyedDate(long personId)
		{
			var request = new UpdateLastSurveyedDateRequest { PersonId = personId }.Prepare(AppContext, Runtime.Settings);

			var response = AccountService.UpdateLastSurveyedDate(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
		}

		/// <summary>
		/// Exports the assist users.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="file">The file.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<CreateAssistUserModel> ExportAssistUsers(string fileName, byte[] file)
		{
			var request = new ExportAssistUsersRequest { FileName = fileName, File = file }.Prepare(AppContext, Runtime.Settings);

			var response = AccountService.ExportAssistUsers(request);

			if (response.Acknowledgement == AcknowledgementType.Failure && !response.ValidationErrors)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.ModelList;
		}

		/// <summary>
		/// Imports the job seeker.
		/// </summary>
		/// <param name="externalId">The external identifier.</param>
		public string ImportJobSeeker(string externalId)
		{
			var request = new ImportJobSeekerAndResumesRequest { JobSeekerExternalId = externalId }.Prepare(AppContext, Runtime.Settings);

			var response = AccountService.ImportJobSeekerAndResumes(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				return request.RequestId.ToString();

			return string.Empty;
		}

		/// <summary>
		/// Imports the employer and artifacts.
		/// </summary>
		public string ImportEmployerAndArtifacts(string externalId)
		{
			var request = new ImportEmployerAndArtifactsRequest { EmployerExternalId = externalId }.Prepare(AppContext, Runtime.Settings);
			var response = AccountService.ImportEmployerAndArtifacts(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				return request.RequestId.ToString();

			return string.Empty;
		}

		/// <summary>
		/// Unsubscribes the user from emails.
		/// </summary>
		/// <param name="encryptedPersonId">The encrypted person id.</param>
		/// <param name="encryptionId">The encryption id.</param>
		/// <param name="urlEncoded">if set to <c>true</c> [URL encoded].</param>
		/// <returns>
		/// ErrorType
		/// </returns>
		public ErrorTypes UnsubscribeJobseekerFromEmails(string encryptedPersonId, long encryptionId, bool urlEncoded = false)
		{
			var request = new UnsubscribeUserFromEmailsRequest
			{
				EncryptedPersonId = encryptedPersonId,
				EncryptionId = encryptionId,
				UrlEncoded = urlEncoded,
				TargetType = TargetTypes.JobSeekerUnsubscribe,
				UserType = EntityTypes.JobSeeker
			}.Prepare(AppContext, Runtime.Settings);

			var response = AccountService.UnsubscribeUserFromEmails(request);

			return response.Error;
		}

		/// <summary>
		/// Unsubscribes the hiring manager from emails.
		/// </summary>
		/// <param name="encryptedPersonId">The encrypted person id.</param>
		/// <param name="encryptionId">The encryption id.</param>
		/// <param name="urlEncoded">if set to <c>true</c> [URL encoded].</param>
		/// <returns></returns>
		public ErrorTypes UnsubscribeHiringManagerFromEmails(string encryptedPersonId, long encryptionId, bool urlEncoded = false)
		{
			var request = new UnsubscribeUserFromEmailsRequest
			{
				EncryptedPersonId = encryptedPersonId,
				EncryptionId = encryptionId,
				UrlEncoded = urlEncoded,
				TargetType = TargetTypes.HiringManagerUnsubscribe,
				UserType = EntityTypes.Employee
			}.Prepare(AppContext, Runtime.Settings);

			var response = AccountService.UnsubscribeUserFromEmails(request);

			return response.Error;
		}

		/// <summary>
		/// Inactivates the job seeker.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="inactivateReason">The inactivate reason.</param>
		/// <exception cref="ServiceCallException"></exception>
		public bool InactivateJobSeeker(long personId, AccountDisabledReason inactivateReason)
		{
			var request = new InactivateJobSeekerRequest { PersonId = personId, AccountDisabledReason = inactivateReason }.Prepare(AppContext, Runtime.Settings);

			var response = AccountService.InactivateJobSeeker(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Success;
		}

		/// <summary>
		/// Updates the assist user.
		/// </summary>
		/// <param name="userDetails">The user details.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public UserDto UpdateAssistUser(UserDetailsModel userDetails)
		{
			var request = new UpdateAssistUserRequest().Prepare(AppContext, Runtime.Settings);

			#region Set up request object

			request.UserId = userDetails.UserId;

			request.Manager = userDetails.PersonDetails.Manager;

			request.DisabledVeteransOutreachProgramSpecialist = userDetails.PersonDetails.DisabledVeteransOutreachProgramSpecialist;

			request.LocalVeteranEmploymentRepresentative = userDetails.PersonDetails.LocalVeteranEmploymentRepresentative;

			#endregion

			var response = AccountService.UpdateAssistUser(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.User;
		}
	}
}
