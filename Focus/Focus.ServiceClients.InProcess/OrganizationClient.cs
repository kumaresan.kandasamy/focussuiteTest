﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core.Messages;
using Focus.Core.Messages.OrganizationService;
using Focus.Core.Models.Career;
using Focus.ServiceClients.InProcess.Extensions;
using Focus.ServiceClients.Interfaces;
using Focus.Web.Core.Models;
using Framework.Core;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.InProcess
{
	public class OrganizationClient : ClientBase, IOrganizationClient
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="OrganizationClient" /> class.
		/// </summary>
		/// <param name="appContext">The application context.</param>
		/// <param name="runtime">The runtime.</param>
		public OrganizationClient(AppContextModel appContext, ServiceClientRuntime runtime) : base(appContext, runtime) { }
		
		/// <summary>
		/// Sets the do not display.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public bool SetDoNotDisplay(string lensPostingId)
		{
			var request = new DontDisplayJobRequest().Prepare(AppContext, Runtime.Settings);
			request.LensPostingId = lensPostingId;

			var response = OrganizationService.DontShowJob(request);
			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return (response.Acknowledgement == AcknowledgementType.Success);
		}

    /// <summary>
    /// Applies for job.
    /// </summary>
    /// <param name="lensPostingId">The lens posting id.</param>
    /// <param name="resumeId">The resume id.</param>
    /// <param name="isEligible">if set to <c>true</c> [is eligible].</param>
    /// <param name="score">The score.</param>
    /// <param name="message">The message.</param>
    /// <param name="reviewApplication">if set to <c>true</c> [review application].</param>
    /// <param name="waivedRequirements">The waived requirements.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public bool ApplyForJob(string lensPostingId, long resumeId, bool isEligible, int score = 0, string message = "", bool reviewApplication = false, List<string> waivedRequirements = null)
		{
			var request = new ApplyForJobRequest().Prepare(AppContext, Runtime.Settings);
			request.ResumeId = resumeId;
			request.LensPostingId = lensPostingId;
			request.IsEligible = isEligible;
      request.WaivedRequirements = waivedRequirements;
			
			if (message.IsNotNullOrEmpty())
				request.Notes = message;

			if (score != 0)
				request.MatchingScore = score;

			request.ReviewApplication = reviewApplication;

			var response = OrganizationService.ApplyForJob(request);
			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return (response.Acknowledgement == AcknowledgementType.Success);
		}

		/// <summary>
		/// Gets the application list.
		/// </summary>
		/// <param name="applicationCount">The application count.</param>
		/// <param name="fromDate">From date.</param>
		/// <param name="toDate">To date.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<ReferralPosting> GetApplicationList(int applicationCount, DateTime fromDate, DateTime toDate)
		{
			var request = new ApplicationListRequest().Prepare(AppContext, Runtime.Settings);
			request.ReferralCount = applicationCount;
			request.FromDate = fromDate;
			request.ToDate = toDate;

			var response = OrganizationService.GetApplicationList(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.ReferralPostings;
		}

		/// <summary>
		/// Gets the match score.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public int GetMatchScore(long resumeId, string lensPostingId)
		{
			var request = new MatchRequest().Prepare(AppContext, Runtime.Settings);
			request.ResumeId = resumeId;
			request.LensPostingId = lensPostingId;

      var response = OrganizationService.MatchResumeToPosting(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Score;
		}

		/// <summary>
		/// Gets the job information.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public PostingEnvelope GetJobInformation(string jobId)
		{
			var request = new JobInformationRequest().Prepare(AppContext, Runtime.Settings);
			request.JobLensId = jobId;

			var response = OrganizationService.JobInformation(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.JobInformation;
		}
	}
}
