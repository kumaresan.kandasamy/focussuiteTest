﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Text;
using Focus.Core;
using Focus.Core.Messages;
using Focus.Core.Messages.ProcessorService;
using Focus.ServiceClients.InProcess.Extensions;
using Focus.ServiceClients.Interfaces;
using Focus.Web.Core.Models;
using Framework.Exceptions;
using Framework.Messaging;

#endregion

namespace Focus.ServiceClients.InProcess
{
	public class ProcessorClient : ClientBase, IProcessorClient
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessorClient" /> class.
		/// </summary>
		/// <param name="appContext">The application context.</param>
		/// <param name="runtime">The runtime.</param>
		public ProcessorClient(AppContextModel appContext, ServiceClientRuntime runtime) : base(appContext, runtime) { }
		
		/// <summary>
		/// Enqueues the batch process.
		/// </summary>
		/// <param name="identifier">The identifier.</param>
		/// <returns></returns>
		public string EnqueueBatchProcess(string identifier)
		{
			var request = new EnqueueBatchRequest { Identifier = identifier }.Prepare(AppContext, Runtime.Settings);
			var response = ProcessorService.EnqueueBatch(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				return response.Message;
			
			return string.Format("Batch {0} enqueued", identifier);
		}


    /// <summary>
    /// Schedules the batch process.
    /// </summary>
    /// <param name="process">The process.</param>
    /// <param name="schedule">The schedule.</param>
    /// <returns></returns>
    public string EnqueueBatchProcess(BatchProcesses process, MessageSchedulePlan schedule)
    {
      var request = new EnqueueBatchRequest { Process = process, Schedule = schedule }.Prepare(AppContext, Runtime.Settings);
      var response = ProcessorService.EnqueueBatch(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        return response.Message;

      if (schedule.Interval == 0)
        return string.Format("Batch {0} scheduled to run immediately", process);

      var message = new StringBuilder();
      message.AppendFormat("Batch {0} scheduled to run every {1} ", process, schedule.Interval);

      switch (schedule.ScheduleType)
      {
        case MessageScheduleType.Minutes:
          message.Append(schedule.Interval == 1 ? "minute" : "minutes");
          break;
        case MessageScheduleType.Hourly:
          message.Append(schedule.Interval == 1 ? "hour" : "hours");
          if (schedule.StartMinutes.GetValueOrDefault(0) > 0)
            message.AppendFormat(" at {0} minutes past the hour", schedule.StartMinutes);
          else
            message.Append(" on the hour");
          break;
        case MessageScheduleType.Daily:
          message.Append(schedule.Interval == 1 ? "day" : "days");
          message.AppendFormat(" at {0}:{1}", schedule.StartHour, schedule.StartMinutes);
          break;
        case MessageScheduleType.Weekly:
          message.Append(schedule.Interval == 1 ? "week" : "weeks");
          message.AppendFormat(" at {0}:{1} on a {2}", schedule.StartHour, schedule.StartMinutes, schedule.StartDay);
          break;
      }

      return message.ToString();
    }

    /// <summary>
    /// Enqueues the import process.
    /// </summary>
    /// <param name="recordType">The type of import to perform</param>
    /// <returns>A message indicating success or failure</returns>
    public string EnqueueImport(IntegrationImportRecordType recordType)
    {
      var request = new EnqueueImportRequest
      {
        ImportType = recordType
      }.Prepare(AppContext, Runtime.Settings);

      var response = ProcessorService.EnqueueImport(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        return response.Message;

      return string.Format("{0} import request enqueued.", recordType.ToString());
    }

    /// <summary>
    /// Enqueues a request to populate the report database for specified entity ids
    /// </summary>
    /// <param name="type">The type of report table</param>
    /// <param name="idList">An optional list of ids if only specific entities are needed</param>
    /// <returns>A success message</returns>
    public string EnqueueReportPopulation(ReportEntityType type, List<long> idList)
    {
      var request = new EnqueueReportPopulationRequest
      {
        EntityType = type,
        EntityIds = idList
      }.Prepare(AppContext, Runtime.Settings);

      var response = ProcessorService.EnqueueReportPopulation(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        return response.Message;

      return string.Format("{0} report population request enqueued.", type.ToString());
    }

    /// <summary>
    /// Enqueues a request to populate the report database for all entities of a given type
    /// </summary>
    /// <param name="type">The type of report table</param>
    /// <param name="resetAll">Whether to delete all the entities first</param>
    /// <returns>A success message</returns>
    public string EnqueueReportPopulation(ReportEntityType type, bool resetAll)
    {
      var request = new EnqueueReportPopulationRequest
      {
        EntityType = type,
        ResetAll = resetAll
      }.Prepare(AppContext, Runtime.Settings);

      var response = ProcessorService.EnqueueReportPopulation(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        return response.Message;

      return string.Format("{0} report population request enqueued.", type.ToString());
    }

    public string ResetCache()
    {
      var request = new ResetCacheRequest().Prepare(AppContext, Runtime.Settings);
      var response = ProcessorService.ResetCache(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        return response.Exception.Message;

      return string.Empty;
    }

    /// <summary>
    /// Sends reset password emails to imported users.
    /// </summary>
    /// <param name="emailsToSend">The number of emails to send.</param>
    /// <param name="emailRecordType">Type of the email record.</param>
    /// <param name="url">The reset password URL.</param>
    /// <returns>The number of emails actually sent</returns>
    public int SendEmailsToImportedUsers(int emailsToSend, MigrationEmailRecordType emailRecordType, string url)
    {
      var request = new ImportedUsersRequest
      {
        EmailsToSend = emailsToSend,
        RecordType = emailRecordType,
        ResetPasswordUrl = url
      }.Prepare(AppContext, Runtime.Settings);
      var response = ProcessorService.SendEmailsToImportedUsers(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.EmailsSent;
    }

    /// <summary>
    /// Encrypts a field in the database
    /// </summary>
    /// <returns>A boolean indicating if the field was already encrypted</returns>
    public bool EncryptField()
    {
      var request = new EncryptionRequest().Prepare(AppContext, Runtime.Settings);
      var response = ProcessorService.EncryptFields(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.AlreadyProcessed;
    }

    /// <summary>
    /// Encrypts a field in the database
    /// </summary>
    /// <returns>A boolean indicating if the field was already encrypted</returns>
    public bool DecryptField()
    {
      var request = new EncryptionRequest().Prepare(AppContext, Runtime.Settings);
      var response = ProcessorService.DecryptFields(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.AlreadyProcessed;
    }
  }
}
