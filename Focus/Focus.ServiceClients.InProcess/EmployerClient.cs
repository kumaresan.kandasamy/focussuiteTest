﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.BusinessUnit;
using Focus.Core.Criteria.BusinessUnitAddress;
using Focus.Core.Criteria.BusinessUnitDescription;
using Focus.Core.Criteria.BusinessUnitLogo;
using Focus.Core.Criteria.Employer;
using Focus.Core.Criteria.EmployerAddress;
using Focus.Core.Criteria.Job;
using Focus.Core.Criteria.SpideredEmployer;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Models;
using Focus.Core.Models.Assist;
using Focus.Core.Views;
using Focus.ServiceClients.InProcess.Extensions;
using Focus.ServiceClients.Interfaces;
using Focus.Web.Core.Models;
using Framework.Core;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.InProcess
{
	public class EmployerClient : ClientBase, IEmployerClient
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmployerClient" /> class.
		/// </summary>
		/// <param name="appContext">The application context.</param>
		/// <param name="runtime">The runtime.</param>
		public EmployerClient(AppContextModel appContext, ServiceClientRuntime runtime) : base(appContext, runtime) { }
		
		/// <summary>
		/// Gets the employer jobs.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <param name="jobTypes">An optional list of job types</param>
		/// <returns></returns>
		public List<SelectView> GetEmployerJobs(long employerId, List<JobTypes> jobTypes = null)
		{
			var request = new EmployerJobRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCriteria
				{
					EmployerId = employerId,
					FetchOption = CriteriaBase.FetchOptions.List,
					JobTypes = jobTypes
				};

			var response = EmployerService.GetEmployerJobs(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.Jobs;
		}

		/// <summary>
		/// Gets the business units.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		public List<BusinessUnitDto> GetBusinessUnits(long employerId)
		{
			var request = new BusinessUnitRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new BusinessUnitCriteria {EmployerId = employerId, FetchOption = CriteriaBase.FetchOptions.List};

			var response = EmployerService.GetBusinessUnits(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.BusinessUnits;
		}

    /// <summary>
    /// Gets the business units.
    /// </summary>
    /// <param name="employerId">The employer id.</param>
    /// <param name="employeeIds">A lookup of employee ids.</param>
    /// <returns></returns>
    public List<BusinessUnitDto> GetBusinessUnitsWithEmployeeIds(long employerId, out ILookup<long, long> employeeIds)
    {
      var request = new BusinessUnitRequest
      {
        IncludeEmployeeIds = true,
        Criteria = new BusinessUnitCriteria
        {
          EmployerId = employerId, 
          FetchOption = CriteriaBase.FetchOptions.List
        }
      }.Prepare(AppContext, Runtime.Settings);

      var response = EmployerService.GetBusinessUnits(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      employeeIds = response.EmployeeIds;

      return response.BusinessUnits;
    }

		/// <summary>
		/// Gets the business unit.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <returns></returns>
		public BusinessUnitDto GetBusinessUnit(long businessUnitId)
		{
			var request = new BusinessUnitRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new BusinessUnitCriteria
				{BusinessUnitId = businessUnitId, FetchOption = CriteriaBase.FetchOptions.Single};

			var response = EmployerService.GetBusinessUnits(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.BusinessUnit;
		}

		/// <summary>
		/// Gets the preferred business unit for an employer.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		public BusinessUnitDto GetPreferredBusinessUnit(long employerId)
		{
			var request = new BusinessUnitRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new BusinessUnitCriteria
			{
				EmployerId = employerId,
				IsPreferred = true,
				FetchOption = CriteriaBase.FetchOptions.Single
			};

			var response = EmployerService.GetBusinessUnits(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.BusinessUnit;
		}

		/// <summary>
		/// Gets the business unit profile.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <returns></returns>
		public BusinessUnitProfileModel GetBusinessUnitProfile(long businessUnitId)
		{
			var request = new BusinessUnitProfileRequest {BusinessUnitId = businessUnitId}.Prepare(AppContext, Runtime.Settings);

			var response = EmployerService.GetBusinessUnitProfile(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return new BusinessUnitProfileModel
				{
					BusinessUnit = response.BusinessUnit,
					BusinessUnitAddresses = response.BusinessUnitAddresses,
					EmployerLockVersion = response.EmployerLockVersion,
					Logins7Days = response.Logins7Days,
					LastLogin = response.LastLogin,
					LastLoginEmail = response.LastLoginEmail,
					NoOfJobOrdersOpen30Days = response.NoOfJobOrdersOpen30Days,
					NoOfJobOrdersOpen7Days = response.NoOfJobOrdersOpen7Days,
					NoOfJobSeekersHired30Days = response.NoOfJobSeekersHired30Days,
					NoOfJobSeekersHired7Days = response.NoOfJobSeekersHired7Days,
					NoOfJobSeekersInterviewed30Days = response.NoOfJobSeekersInterviewed30Days,
					NoOfJobSeekersInterviewed7Days = response.NoOfJobSeekersInterviewed7Days,
					NoOfJobSeekersRejected30Days = response.NoOfJobSeekersRejected30Days,
					NoOfJobSeekersRejected7Days = response.NoOfJobSeekersRejected7Days,
					NoOfOrdersEditedByStaff30Days = response.NoOfOrdersEditedByStaff30Days,
					NoOfOrdersEditedByStaff7Days = response.NoOfOrdersEditedByStaff7Days,
					NoOfResumesReferred30Days = response.NoOfResumesReferred30Days,
					NoOfResumesReferred7Days = response.NoOfResumesReferred7Days,
					OfficeIds = response.OfficeIds
				};
		}

		/// <summary>
		/// Gets the linked companies for a business unit.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public List<BusinessUnitLinkedCompanyDto> GetBusinessUnitLinkedCompanies(BusinessUnitLinkedCompanyCriteria criteria)
		{
			var request = new BusinessUnitLinkedCompaniesRequest { Criteria = criteria }.Prepare(AppContext, Runtime.Settings);
			var response = EmployerService.GetBusinessUnitLinkedCompanies(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.LinkedCompanies;
		}

		/// <summary>
		/// Saves the business unit.
		/// </summary>
		/// <param name="businessUnit">The business unit.</param>
		/// <returns></returns>
		public BusinessUnitDto SaveBusinessUnit(BusinessUnitDto businessUnit)
		{
			var request = new SaveBusinessUnitRequest().Prepare(AppContext, Runtime.Settings);
			request.BusinessUnit = businessUnit;

			var response = EmployerService.SaveBusinessUnit(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.BusinessUnit;
		}


		/// <summary>
		/// Gets the business unit description.
		/// </summary>
		/// <param name="businessUnitDescriptionId">The business unit description id.</param>
		/// <returns></returns>
		public BusinessUnitDescriptionDto GetBusinessUnitDescription(long businessUnitDescriptionId)
		{
			var request = new BusinessUnitDescriptionRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new BusinessUnitDescriptionCriteria
				{BusinessUnitDescriptionId = businessUnitDescriptionId, FetchOption = CriteriaBase.FetchOptions.Single};

			var response = EmployerService.GetBusinessUnitDescriptions(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.BusinessUnitDescription;
		}

		/// <summary>
		/// Gets the primary business unit description.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <returns></returns>
		public BusinessUnitDescriptionDto GetPrimaryBusinessUnitDescription(long businessUnitId)
		{
			var request = new BusinessUnitDescriptionRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new BusinessUnitDescriptionCriteria
				{BusinessUnitId = businessUnitId, IsPrimary = true, FetchOption = CriteriaBase.FetchOptions.Single};

			var response = EmployerService.GetBusinessUnitDescriptions(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.BusinessUnitDescription;
		}

		/// <summary>
		/// Saves the business unit description.
		/// </summary>
		/// <param name="description">The description.</param>
		/// <returns></returns>
		public BusinessUnitDescriptionDto SaveBusinessUnitDescription(BusinessUnitDescriptionDto description)
		{
			var request = new SaveBusinessUnitDescriptionRequest().Prepare(AppContext, Runtime.Settings);
			request.BusinessUnitDescription = description;

			var response = EmployerService.SaveBusinessUnitDescription(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.BusinessUnitDescription;
		}

		/// <summary>
		/// Gets the employer.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		public ApprovalStatuses GetEmployerApprovalStatus(long employerId)
		{
			var request = new GetEmployerDetailsRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new EmployerCriteria { EmployerId = employerId };

			var response = EmployerService.GetEmployerApprovalStatus(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.ApprovalStatus;
		}

		/// <summary>
		/// Gets the employer.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		public EmployerDetailsModel GetEmployer(long employerId)
		{
			var request = new GetEmployerDetailsRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new EmployerCriteria {EmployerId = employerId};

			var response = EmployerService.GetEmployerDetails(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error, response.Exception);

			return new EmployerDetailsModel
				{
					EmployerId = employerId,
					Name = response.Name,
					Description = response.Description,
					FEIN = response.FederalEmployerIdentificationNumber,
					StateEmployerIdentificationNumber = response.StateEmployerIdentificationNumber,
					IndustrialClassification = response.IndustrialClassification,
					OwnershipTypeId = response.OwnershipTypeId,
					AccountTypeId = response.AccountTypeId,
					PrimaryAddress = response.PrimaryAddress,
					PrimaryPhoneNumber = response.PrimaryPhoneNumber,
					AlternatePhoneNumber1 = response.AlternatePhoneNumber1,
					AlternatePhoneNumber2 = response.AlternatePhoneNumber2,
					IsAlreadyApproved = response.IsAlreadyApproved,
					LockVersion = response.LockVersion
				};
		}

		/// <summary>
		/// Gets the employer's legal name.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		public string GetEmployerLegalName(long employerId)
		{
			var request = new GetEmployerDetailsRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new EmployerCriteria { EmployerId = employerId };

			var response = EmployerService.GetEmployerLegalName(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.LegalName;
		}

		/// <summary>
		/// Updates the company details.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns>User context for updating</returns>
		public IUserContext SaveEmployerDetails(EmployerDetailsModel model)
		{
		
				var request = new SaveEmployerDetailsRequest().Prepare(AppContext, Runtime.Settings);
				request.Criteria = new EmployerCriteria { EmployerId = model.EmployerId };

				request.FederalEmployerIdentificationNumber = model.FEIN;
				request.StateEmployerIdentificationNumber = model.StateEmployerIdentificationNumber;
				request.Name = model.Name;
				request.LockVersion = model.LockVersion;
				var response = EmployerService.SaveEmployerDetails(request);
				
				if (response.Acknowledgement == AcknowledgementType.Failure)
					throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

				return request.UserContext;
			
			
		}

		/// <summary>
		/// Gets the employer placements.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<EmployerPlacementsView> GetEmployerPlacements(PlacementsCriteria criteria)
		{
			var request = new EmployerPlacementsViewRequest().Prepare(AppContext, Runtime.Settings);

			criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;

			request.Criteria = criteria;

			var response = EmployerService.GetEmployerPlacements(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error, response.Exception);

			return response.EmployerPlacementsViewsPaged;
		}

		/// <summary>
		/// Gets the business unit descriptions.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <returns></returns>
		public List<BusinessUnitDescriptionDto> GetBusinessUnitDescriptions(long businessUnitId)
		{
			var request = new BusinessUnitDescriptionRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new BusinessUnitDescriptionCriteria
				{BusinessUnitId = businessUnitId, FetchOption = CriteriaBase.FetchOptions.List};

			var response = EmployerService.GetBusinessUnitDescriptions(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.BusinessUnitDescriptions;
		}

		/// <summary>
		/// Gets the business unit descriptions associated with job.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <returns></returns>
		public List<BusinessUnitDescriptionDto> GetBusinessUnitDescriptionsAssociatedWithJob(long businessUnitId)
		{
			var request = new BusinessUnitDescriptionRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new BusinessUnitDescriptionCriteria
				{BusinessUnitId = businessUnitId, AssociatedWithJob = true, FetchOption = CriteriaBase.FetchOptions.List};

			var response = EmployerService.GetBusinessUnitDescriptions(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.BusinessUnitDescriptions;
		}

    /// <summary>
    /// Saves the business unit descriptions.
    /// </summary>
    /// <param name="businessUnitId">The business unit id.</param>
    /// <param name="descriptions">The descriptions.</param>
    /// <param name="checkCensorship">Whether to try and check censorship business unit description</param>
    public bool SaveBusinessUnitDescriptions(long businessUnitId, List<BusinessUnitDescriptionDto> descriptions, bool checkCensorship)
		{
			var request = new SaveBusinessUnitDescriptionsRequest().Prepare(AppContext, Runtime.Settings);
			request.BusinessUnitId = businessUnitId;
			request.Descriptions = descriptions;
      request.CheckCensorship = checkCensorship;

			var response = EmployerService.SaveBusinessUnitDescriptions(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

      return response.FailedCensorshipCheck;
		}

		/// <summary>
		/// Gets the business unit logo.
		/// </summary>
		/// <param name="businessUnitLogoId">The business unit logo id.</param>
		/// <returns></returns>
		public BusinessUnitLogoDto GetBusinessUnitLogo(long businessUnitLogoId)
		{
			var request = new BusinessUnitLogoRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new BusinessUnitLogoCriteria
				{Id = businessUnitLogoId, FetchOption = CriteriaBase.FetchOptions.Single};

			var response = EmployerService.GetBusinessUnitLogos(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.BusinessUnitLogo;
		}

		/// <summary>
		/// Saves the business unit logo.
		/// </summary>
		/// <param name="businessUnitLogo">The business unit logo.</param>
		/// <returns></returns>
		public long SaveBusinessUnitLogo(BusinessUnitLogoDto businessUnitLogo)
		{
			var request = new SaveBusinessUnitLogoRequest().Prepare(AppContext, Runtime.Settings);
			request.BusinessUnitLogo = businessUnitLogo;

			var response = EmployerService.SaveBusinessUnitLogo(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.BusinessUnitLogoId;
		}

		/// <summary>
		/// Gets the business unit logos.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <returns></returns>
		public List<BusinessUnitLogoDto> GetBusinessUnitLogos(long businessUnitId)
		{
			var request = new BusinessUnitLogoRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new BusinessUnitLogoCriteria
				{BusinessUnitId = businessUnitId, FetchOption = CriteriaBase.FetchOptions.List};

			var response = EmployerService.GetBusinessUnitLogos(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.BusinessUnitLogos;
		}

		/// <summary>
		/// Gets the business unit logos associated with job.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <returns></returns>
		public List<BusinessUnitLogoDto> GetBusinessUnitLogosAssociatedWithJob(long businessUnitId)
		{
			var request = new BusinessUnitLogoRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new BusinessUnitLogoCriteria
				{BusinessUnitId = businessUnitId, AssociatedWithJob = true, FetchOption = CriteriaBase.FetchOptions.List};

			var response = EmployerService.GetBusinessUnitLogos(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.BusinessUnitLogos;
		}

		/// <summary>
		/// Saves the business unit logos.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <param name="logos">The logos.</param>
		public List<BusinessUnitLogoDto> SaveBusinessUnitLogos(long businessUnitId, List<BusinessUnitLogoDto> logos)
		{
			var request = new SaveBusinessUnitLogosRequest().Prepare(AppContext, Runtime.Settings);
			request.BusinessUnitId = businessUnitId;
			request.BusinessUnitLogos = logos;

			var response = EmployerService.SaveBusinessUnitLogos(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

		  return response.BusinessUnitLogos;
		}

		/// <summary>
		/// Gets the primary employer address.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		public EmployerAddressDto GetPrimaryEmployerAddress(long employerId)
		{
			var request = new EmployerAddressRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new EmployerAddressCriteria {EmployerId = employerId, IsPrimary = true};

			var response = EmployerService.GetEmployerAddress(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.PrimaryEmployerAddress;
		}

		/// <summary>
		/// Gets the primary business unit address.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <returns></returns>
		public BusinessUnitAddressDto GetPrimaryBusinessUnitAddress(long businessUnitId)
		{
			var request = new BusinessUnitAddressRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new BusinessUnitAddressCriteria
				{BusinessUnitId = businessUnitId, IsPrimary = true, FetchOption = CriteriaBase.FetchOptions.List};

			var response = EmployerService.GetBusinessUnitAddress(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return (response.BusinessUnitAddresses.IsNotNullOrEmpty()) ? response.BusinessUnitAddresses[0] : null;
		}


		/// <summary>
		/// Saves the business unit address.
		/// </summary>
		/// <param name="businessUnitAddress">The business Unit Address.</param>
		/// <returns></returns>
		public BusinessUnitAddressDto SaveBusinessUnitAddress(BusinessUnitAddressDto businessUnitAddress)
		{
			var request = new SaveBusinessUnitAddressRequest().Prepare(AppContext, Runtime.Settings);
			request.BusinessUnitAddress = businessUnitAddress;

			var response = EmployerService.SaveBusinessUnitAddress(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.BusinessUnitAddress;
		}


		/// <summary>
		/// Gets the job titles.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <param name="jobTitle">The job title.</param>
		/// <param name="count">The count.</param>
		/// <returns></returns>
		public List<string> GetJobTitles(long employerId, string jobTitle, int count)
		{
			var request = new EmployerJobTitleRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCriteria {EmployerId = employerId};

			var response = EmployerService.GetEmployerJobTitles(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return
				response.JobTitles.Where(x => x.StartsWith(jobTitle, StringComparison.OrdinalIgnoreCase)).Take(count).OrderBy(x => x)
					.ToList();
		}

		/// <summary>
		/// Assigns the employer to admin. REFERS TO HIRING MANAGER (EmployeeId)
		/// </summary>
		/// <param name="adminId">The admin id.</param>
		/// <param name="employeeId">The employee id.</param>
		public bool AssignEmployerToAdmin(long adminId, long employeeId)
		{
			var request = new AssignEmployerToAdminRequest().Prepare(AppContext, Runtime.Settings);
			request.AdminId = adminId;
			request.EmployeeId = employeeId;

			var response = EmployerService.AssignEmployerToAdmin(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

		  return response.AssignSuccessful;
		}

		/// <summary>
		/// Checks the NAICS exists.
		/// </summary>
		/// <param name="naics">NAICs code.</param>
    /// <param name="childrenOnly">Whether to check "child" NAICS only (i.e. exluce NAICS where ParentId = 0)</param>
    /// <returns></returns>
    public bool CheckNAICSExists(string naics, bool childrenOnly = false)
		{
			var request = new CheckNaicsExistsRequest().Prepare(AppContext, Runtime.Settings);
			request.Naics = naics;
		  request.ChildrenOnly = childrenOnly;

			var response = EmployerService.CheckNaicsExists(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.Exists;
		}

		/// <summary>
		/// Blocks the employees accounts.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void BlockEmployeesAccounts(long businessUnitId)
		{
			var request = new BlockEmployeesAccountsRequest().Prepare(AppContext, Runtime.Settings);
			request.BusinessUnitId = businessUnitId;

			var response = EmployerService.BlockEmployeesAccounts(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Unblocks the employees accounts.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void UnblockEmployeesAccounts(long businessUnitId)
		{
			var request = new UnblockEmployeesAccountsRequest().Prepare(AppContext, Runtime.Settings);
			request.BusinessUnitId = businessUnitId;

			var response = EmployerService.UnblockEmployeesAccounts(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Saves the business unit model.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
		public BusinessUnitModel SaveBusinessUnitModel(BusinessUnitModel model)
		{
		  bool failedCensorshipCheck;
      return SaveBusinessUnitModel(model, false, false, out failedCensorshipCheck);
		}

    /// <summary>
    /// Saves the business unit model.
    /// </summary>
    /// <param name="model">The model.</param>
    /// <param name="checkCensorshipBusinessUnit">Whether to try and check censorship main business unit record</param>
    /// <param name="checkCensorshipDescription">Whether to try and check censorship business unit description</param>
    /// <param name="failedCensorshipCheck">Whether censorship check failed</param>
    /// <returns></returns>
    public BusinessUnitModel SaveBusinessUnitModel(BusinessUnitModel model, bool checkCensorshipBusinessUnit, bool checkCensorshipDescription, out bool failedCensorshipCheck)
    {

			

      var request = new SaveBusinessUnitModelRequest().Prepare(AppContext, Runtime.Settings);
      request.Model = model;
      request.CheckCensorshipBusinessUnit = checkCensorshipBusinessUnit;
      request.CheckCensorshipDescription = checkCensorshipDescription;
			
	    var response = EmployerService.SaveBusinessUnitModel(request);
			
      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      failedCensorshipCheck = response.FailedCensorshipCheck;

      return response.Model;
    }

		/// <summary>
		/// Gets the business unit activities as list.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public List<BusinessUnitActivityViewDto> GetBusinessUnitActivitiesAsList(BusinessUnitCriteria criteria)
		{
			var request = new BusinessUnitActivityRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = criteria;

			var response = EmployerService.GetBusinessUnitActivities(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.BusinessUnitActivitiesList;
		}

		/// <summary>
		/// Gets the business unit activities as paged.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<BusinessUnitActivityViewDto> GetBusinessUnitActivitiesAsPaged(BusinessUnitCriteria criteria)
		{
			var request = new BusinessUnitActivityRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = criteria;

			var response = EmployerService.GetBusinessUnitActivities(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.BusinessUnitActivitiesPaged;
		}

		/// <summary>
		/// Gets the business unit placements.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<BusinessUnitPlacementsViewDto> GetBusinessUnitPlacements(BusinessUnitCriteria criteria)
		{
			var request = new BusinessUnitPlacementsRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = criteria;

			var response = EmployerService.GetBusinessUnitPlacements(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.BusinessUnitPlacementsViewsPaged;
		}

		/// <summary>
		/// Gets the business unit placements.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<EmployerRecentlyPlacedView> GetEmployerRecentlyPlacedMatches(BusinessUnitCriteria criteria)
		{
			var request = new EmployerRecentlyPlacedMatchRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = criteria;

			var response = EmployerService.GetEmployerRecentlyPlacedMatches(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.RecentlyPlacedPaged;
		}

		/// <summary>
		/// Ignores the recommended match.
		/// </summary>
		/// <param name="recentlyPlacedMatchId">The recently placed match id.</param>
		public void IgnoreRecentlyPlacedMatch(long recentlyPlacedMatchId)
		{
			var request = new IgnoreRecentlyPlacedMatchRequest().Prepare(AppContext, Runtime.Settings);
			request.RecentlyPlacedMatchId = recentlyPlacedMatchId;

			var response = EmployerService.IgnoreRecentlyPlacedMatch(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Allows the recommended match.
		/// </summary>
		/// <param name="recentlyPlacedMatchId">The recently placed match id.</param>
		public void AllowRecentlyPlacedMatch(long recentlyPlacedMatchId)
		{
			var request = new AllowRecentlyPlacedMatchRequest().Prepare(AppContext, Runtime.Settings);
			request.RecentlyPlacedMatchId = recentlyPlacedMatchId;

			var response = EmployerService.AllowRecentlyPlacedMatch(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Searches the employers.
		/// </summary>
		/// <param name="postCode">The post code.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<BusinessUnitSearchViewDto> SearchBusinessUnits(string postCode)
		{
			var request =
				new SearchBusinessUnitsRequest {SearchCriteria = new BusinessUnitCriteria {ZipCode = postCode}}.Prepare(AppContext, Runtime.Settings);
			var response = EmployerService.SearchBusinessUnits(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.BusinessUnits;
		}

		/// <summary>
		/// Gets the office.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public OfficeDto GetOffice(long officeId)
		{
			var request = new OfficeRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new OfficeCriteria {OfficeId = officeId, FetchOption = CriteriaBase.FetchOptions.Single};

			var response = EmployerService.GetOffices(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.Office;
		}

    /// <summary>
    /// Gets the default office.
    /// </summary>
    /// <param name="defaultType">The default type.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public OfficeDto GetDefaultOffice(OfficeDefaultType defaultType)
		{
			var request = new OfficeRequest().Prepare(AppContext, Runtime.Settings);
      request.Criteria = new OfficeCriteria
        {
          DefaultOffice = true, 
          DefaultType = defaultType,
          FetchOption = CriteriaBase.FetchOptions.Single
        };

			var response = EmployerService.GetOffices(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.Office;
		}

    /// <summary>
    /// Gets default offices.
    /// </summary>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public List<OfficeDto> GetDefaultOffices(OfficeDefaultType defaultType)
    {
      var request = new OfficeRequest
      {
        Criteria = new OfficeCriteria
        {
          DefaultOffice = true,
          DefaultType = defaultType,
          FetchOption = CriteriaBase.FetchOptions.List
        }
      }.Prepare(AppContext, Runtime.Settings);

      var response = EmployerService.GetOffices(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.Offices;
    }

		/// <summary>
		/// Gets the offices.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public PagedList<OfficeDto> GetOfficesPagedList(OfficeCriteria criteria)
		{
			var request = new OfficeRequest().Prepare(AppContext, Runtime.Settings);

			criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;
			request.Criteria = criteria;

			if (criteria.OfficeGroup.IsNotNull())
			{
				if (criteria.OfficeGroup == OfficeGroup.MyOffices)
					criteria.PersonId = request.UserContext.PersonId;
			}

			var response = EmployerService.GetOffices(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.OfficesPaged;
		}

		/// <summary>
		/// Gets the offices list.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<OfficeDto> GetOfficesList(OfficeCriteria criteria)
		{
			var request = new OfficeRequest().Prepare(AppContext, Runtime.Settings);

			criteria.FetchOption = CriteriaBase.FetchOptions.List;
			request.Criteria = criteria;

      if (criteria.OfficeGroup.IsNotNull())
      {
        if (criteria.OfficeGroup == OfficeGroup.MyOffices)
          criteria.PersonId = request.UserContext.PersonId;
      }

			var response = EmployerService.GetOffices(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.Offices;
		}

		/// <summary>
		/// Gets the offices excluding default list.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<OfficeDto> GetOfficesExcludingDefaultList(OfficeCriteria criteria)
		{
			var request = new OfficeRequest().Prepare(AppContext, Runtime.Settings);

			criteria.FetchOption = CriteriaBase.FetchOptions.List;
			request.Criteria = criteria;
			request.Criteria.DefaultOffice = false;

			var response = EmployerService.GetOffices(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.Offices;
		}

    /// <summary>
    /// Saves the office.
    /// </summary>
    /// <param name="office">The office.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public ErrorTypes SaveOffice(OfficeDto office)
    {
      var request = new SaveOfficeRequest().Prepare(AppContext, Runtime.Settings);
      request.Office = office;

      var response = EmployerService.SaveOffice(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
      {
        if (response.Error == ErrorTypes.DuplicateOfficeExternalId)
          return response.Error;

        throw new ServiceCallException(response.Message, (int)response.Error);
      }

      office.Id = response.Office.Id;

      return ErrorTypes.Ok;
    }

		/// <summary>
		/// Gets the office staff members paged list.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public PagedList<PersonModel> GetOfficeStaffMembersPagedList(StaffMemberCriteria criteria)
		{
			var request = new StaffMemberRequest().Prepare(AppContext, Runtime.Settings);

			criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;
			request.Criteria = criteria;

			var response = EmployerService.GetOfficeStaffMembers(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.StaffMembersPaged;
		}

		/// <summary>
		/// Gets the office staff members list.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<PersonModel> GetOfficeStaffMembersList(StaffMemberCriteria criteria)
		{
			var request = new StaffMemberRequest().Prepare(AppContext, Runtime.Settings);

			criteria.FetchOption = CriteriaBase.FetchOptions.List;
			request.Criteria = criteria;

			var response = EmployerService.GetOfficeStaffMembers(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.StaffMembers;
		}

		/// <summary>
		/// Gets the staff members.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="listSize">Size of the list.</param>
		/// <param name="statewide">The statewide.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
    public List<PersonModel> GetStaffMembers(string name, int listSize, bool? statewide = null)
		{
			var request = new StaffMemberRequest().Prepare(AppContext, Runtime.Settings);
      request.Criteria = new StaffMemberCriteria { Name = name, CheckNames = true, Statewide = statewide, FetchOption = CriteriaBase.FetchOptions.List, ListSize = listSize };

			var response = EmployerService.GetOfficeStaffMembers(request);
			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.StaffMembers;
		}


		/// <summary>
		/// Gets the office managers.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<PersonModel> GetOfficeManagers(long officeId)
		{
			var request = new StaffMemberRequest().Prepare(AppContext, Runtime.Settings);

			request.Criteria = new StaffMemberCriteria {OfficeIds = new List<long?> {officeId}, Manager = true};

			var response = EmployerService.GetOfficeManagers(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.StaffMembers;
		}

		/// <summary>
		/// Gets the staff member count.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public int GetStaffMemberCount(long officeId)
		{
			var request = new StaffMemberRequest().Prepare(AppContext, Runtime.Settings);

			request.Criteria = new StaffMemberCriteria {OfficeIds = new List<long?> {officeId}};

			var response = EmployerService.GetStaffMemberCount(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.StaffMemberCount;
		}

		/// <summary>
		/// Determines whether [has assigned postcodes] [the specified office id].
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns>
		///   <c>true</c> if [has assigned postcodes] [the specified office id]; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="ServiceCallException"></exception>
		public bool HasAssignedPostcodes(long officeId)
		{
			var request = new OfficeRequest().Prepare(AppContext, Runtime.Settings);

			request.Criteria = new OfficeCriteria {OfficeId = officeId};

			var response = EmployerService.HasAssignedPostcodes(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return Convert.ToBoolean(response.HasAssignedPostcodes);
		}

    /// <summary>
    /// Gets the offices person manages.
    /// </summary>
    /// <param name="personId">The person id.</param>
    /// <param name="activeOnly">Whether to get only active offices</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public List<OfficeDto> GetOfficesPersonManages(long personId, bool activeOnly = false)
		{
			var request = new OfficeRequest().Prepare(AppContext, Runtime.Settings);

			request.Criteria = new OfficeCriteria
			{
			  PersonId = personId, 
        Manager = true, 
        InActive = activeOnly ? false : (bool?) null,
        FetchOption = CriteriaBase.FetchOptions.List
			};

			var response = EmployerService.GetOffices(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.Offices;
		}

		/// <summary>
		/// Saves the person offices.
		/// </summary>
		/// <param name="personOffices">The person offices.</param>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<PersonOfficeMapperDto> SavePersonOffices(List<PersonOfficeMapperDto> personOffices, long personId)
		{
			var request = new PersonOfficeMapperRequest().Prepare(AppContext, Runtime.Settings);
			request.PersonOffices = personOffices;
			request.PersonId = personId;

			var response = EmployerService.SavePersonOffices(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.OfficeRoles;
		}

		/// <summary>
		/// Assigns the office staff.
		/// </summary>
		/// <param name="personOffices">The person offices.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<PersonOfficeMapperDto> AssignOfficeStaff(List<PersonOfficeMapperDto> personOffices)
		{
			var request = new PersonOfficeMapperRequest().Prepare(AppContext, Runtime.Settings);
			request.PersonOffices = personOffices;

			var response = EmployerService.AssignOfficeStaff(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.OfficeRoles;
		}

		/// <summary>
		/// Determines whether [is person statewide] [the specified person id].
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns>
		///   <c>true</c> if [is person statewide] [the specified person id]; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="ServiceCallException"></exception>
		public bool IsPersonStatewide(long personId)
		{
			var request = new GetManagerStateRequest().Prepare(AppContext, Runtime.Settings);
			request.PersonId = personId;

			var response = EmployerService.GetManagerState(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.StateId.IsNotNull();
		}

		/// <summary>
		/// Offices the has employers.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public bool OfficeHasEmployers(long officeId)
		{
			var request = new OfficeEmployersRequest().Prepare(AppContext, Runtime.Settings);
			request.OfficeId = officeId;

			var response = EmployerService.GetOfficeEmployers(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return (response.OfficeEmployers.IsNotNullOrEmpty() && response.OfficeEmployers.Count() > 0);
		}

		/// <summary>
		/// Updates the employer assigned office.
		/// </summary>
		/// <param name="employerOffices">The employer offices.</param>
		/// <param name="employerId">The employer id.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void UpdateEmployerAssignedOffice(List<EmployerOfficeMapperDto> employerOffices, long employerId)
		{
			var request =
				new UpdateEmployerAssignedOfficeRequest {EmployerId = employerId, EmployerOffices = employerOffices}.Prepare(AppContext, Runtime.Settings);
			var response = EmployerService.UpdateEmployerAssignedOffice(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Updates the default office.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void UpdateDefaultOffice(long officeId)
		{
			var request = new UpdateDefaultOfficeRequest {DefaultOfficeId = officeId}.Prepare(AppContext, Runtime.Settings);
			var response = EmployerService.UpdateDefaultOffice(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

    /// <summary>
    /// Updates the default office for a specific type
    /// </summary>
    /// <param name="officeId">The office id.</param>
    /// <param name="defaultType">The default type.</param>
    /// <exception cref="ServiceCallException"></exception>
    public void UpdateDefaultOffice(long officeId, OfficeDefaultType defaultType)
    {
      var request = new UpdateDefaultOfficeRequest
      {
        DefaultOfficeId = officeId,
        OfficeDefaultType = defaultType
      }.Prepare(AppContext, Runtime.Settings);

      var response = EmployerService.UpdateDefaultOffice(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);
    }

		/// <summary>
		/// Offices has job seekers.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public bool OfficeHasJobSeekers(long officeId)
		{
			var request = new OfficeJobSeekersRequest().Prepare(AppContext, Runtime.Settings);
			request.OfficeId = officeId;

			var response = EmployerService.GetOfficeJobSeekers(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return (response.OfficeJobSeekers.IsNotNullOrEmpty() && response.OfficeJobSeekers.Count() > 0);
		}

		/// <summary>
		/// Offices the has jobs.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public bool OfficeHasJobs(long officeId)
		{
			var request = new OfficeJobsRequest().Prepare(AppContext, Runtime.Settings);
			request.OfficeId = officeId;

			var response = EmployerService.GetOfficeJobs(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return (response.OfficeJobs.IsNotNullOrEmpty() && response.OfficeJobs.Count() > 0);
		}

		/// <summary>
		/// Gets the state of the offices by.
		/// </summary>
		/// <param name="stateId">The state id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<OfficeDto> GetOfficesByState(long stateId)
		{
			var request = new OfficeRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new OfficeCriteria {StateId = stateId, FetchOption = CriteriaBase.FetchOptions.List};

			var response = EmployerService.GetOffices(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.Offices;
		}

		/// <summary>
		/// Gets the state of the manager.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public long? GetManagerState(long personId)
		{
			var request = new GetManagerStateRequest().Prepare(AppContext, Runtime.Settings);
			request.PersonId = personId;

			var response = EmployerService.GetManagerState(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.StateId;
		}

		/// <summary>
		/// Updates the office assigned zips.
		/// </summary>
		/// <param name="zip">The zip.</param>
		/// <param name="officeIds">The office ids.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void UpdateOfficeAssignedZips(List<string> zip, List<long> officeIds)
		{
			var request = new UpdateOfficeAssignedZipsRequest().Prepare(AppContext, Runtime.Settings);
			request.Zips = zip;
			request.OfficeIds = officeIds;

			var response = EmployerService.UpdateOfficeAssignedZips(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);
		}

		/// <summary>
		/// Gets the spidered employer.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<SpideredEmployerModel> GetSpideredEmployers(SpideredEmployerCriteria criteria)
		{
			var request = new SpideredEmployerRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = criteria;

			var response = EmployerService.GetSpideredEmployers(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error);

			return response.SpideredEmployersPaged;
		}

		/// <summary>
		/// Gets the spidered employers with good matches.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public PagedList<SpideredEmployerModel> GetSpideredEmployersWithGoodMatches(SpideredEmployerCriteria criteria)
		{
			var request = new SpideredEmployerRequest().Prepare(AppContext, Runtime.Settings);

			criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;

			request.Criteria = criteria;

			var response = EmployerService.GetSpideredEmployersWithGoodMatches(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int) response.Error, response.Exception);

			return response.SpideredEmployersPaged;
		}

		/// <summary>
		/// Saves the current office for person.
		/// </summary>
		/// <param name="currentOfficeId">The current office id.</param>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		public bool SaveCurrentOfficeForPerson(long currentOfficeId, long personId)
		{
			var request = new SetCurrentOfficeForPersonRequest().Prepare(AppContext, Runtime.Settings);

			request.CurrentOfficeId = currentOfficeId;
			request.PersonId = personId;

			var response = EmployerService.SetCurrentOfficeForPerson(request);

			return response.Suceeded;
		}

		/// <summary>
		/// Gets the current office for person.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		public CurrentOfficeModel GetCurrentOfficeForPerson(long personId)
		{
			var request = new GetCurrentOfficeForPersonRequest().Prepare(AppContext, Runtime.Settings);

			request.PersonId = personId;

			var response = EmployerService.GetCurrentOfficeForPerson(request);

			return response.CurrentOffice;
		}

		/// <summary>
		/// Determines whether [is office assigned] [the specified office id].
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		public OfficeAssignedModel IsOfficeAssigned(long officeId)
		{
			var request = new IsOfficeAssignedRequest().Prepare(AppContext, Runtime.Settings);

			request.OfficeId = officeId;

			var response = EmployerService.IsOfficeAssigned(request);

			var model = new OfficeAssignedModel {IsAssigned = response.IsAssigned, Errors = response.Errors};

			return model;
		}

		/// <summary>
		/// Activates the deactivate office.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <param name="deactivate">if set to <c>true</c> [deactivate].</param>
		/// <returns></returns>
		public ActivateDeactivateOfficeModel ActivateDeactivateOffice(long officeId, bool deactivate)
		{
			var request = new ActivateDeactivateOfficeRequest().Prepare(AppContext, Runtime.Settings);

			request.OfficeId = officeId;
			request.Deactivate = deactivate;

			var response = EmployerService.ActivateDeactivateOffice(request);

			var model = new ActivateDeactivateOfficeModel {Error = response.ErrorType, Office = response.Office};

			return model;
		}

    /// <summary>
    /// Updates the Preferres status of an employer
    /// </summary>
    /// <param name="businessUnitId">The office id.</param>
    /// <param name="isPreferred">if set to <c>true</c> [deactivate].</param>
    /// <returns></returns>
    public bool SetEmployerPreferredStatus(long businessUnitId, bool isPreferred)
    {
      var request = new EmployerPreferredRequest().Prepare(AppContext, Runtime.Settings);

      request.BusinessUnitId = businessUnitId;
      request.IsPreferred = isPreferred;

      var response = EmployerService.SetEmployerPreferredStatus(request);

      return response.PreferredStatusUpdated;
    }

		/// <summary>
		/// Gets the employer account types.
		/// </summary>
		/// <param name="lensPostingIds">The lens posting ids.</param>
		/// <returns></returns>
		public Dictionary<string, long?> GetEmployerAccountTypes(List<string> lensPostingIds )
		{
			var request = new GetEmployerAccountTypesRequest().Prepare(AppContext, Runtime.Settings);

			request.LensPostingIds = lensPostingIds;

			var response = EmployerService.GetEmployerAccountTypes(request);

			return response.EmployerAccountTypes;
		}

			
		
		/// <summary>
		/// Gets the fein.
		/// </summary>
		/// <param name="businessUnitId">The business unit identifier.</param>
		/// <returns></returns>
		public string GetFein(long businessUnitId)
		{
			var request = new GetFEINRequest().Prepare(AppContext, Runtime.Settings);

			request.BusinessUnitId = businessUnitId;
			
			var response = EmployerService.GetFEIN(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.FEIN;
		}


		/// <summary>
		/// Validates the fein change.
		/// </summary>
		/// <param name="businessUnitId">The business unit identifier.</param>
		/// <param name="newFein">The new fein.</param>
		/// <returns></returns>
		/// <exception cref="Framework.Exceptions.ServiceCallException"></exception>
		public ValidateFEINChangeModel ValidateFEINChange(long businessUnitId, string newFein)
		{
			var request = new ValidateFEINChangeRequest().Prepare(AppContext, Runtime.Settings);

			request.BusinessUnitId = businessUnitId;
			request.NewFEIN = newFein;

			var response = EmployerService.ValidateFEINChange(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.ValidationResult;
		}

		/// <summary>
		/// Saves the fein change.
		/// </summary>
		/// <param name="businessUnitId">The business unit identifier.</param>
		/// <param name="newFein">The new fein.</param>
		/// <param name="lockVersion">The lock version of the employer</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public FEINChangeActions SaveFEINChange(long businessUnitId, string newFein, int? lockVersion = null)
		{
			var request = new SaveFEINChangeRequest
			{
				BusinessUnitId = businessUnitId,
				NewFEIN = newFein,
				LockVersion = lockVersion,
			}.Prepare(AppContext, Runtime.Settings);

			var response = EmployerService.SaveFEINChange(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.ActionCarriedOut;
		}

		public void UpdatePrimaryBusinessUnit(long? currentPrimaryBusinessUnitId, long? newPrimaryBusinessUnitId)
		{
			var request = new UpdatePrimaryBusinessUnitRequest().Prepare(AppContext, Runtime.Settings);

			request.CurrentPrimaryBusinessUnitId = currentPrimaryBusinessUnitId;
			request.NewPrimaryBusinessUnitId = newPrimaryBusinessUnitId;

			var response = EmployerService.UpdatePrimaryBusinessUnit(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

		}

		/// <summary>
		/// Gets the hiring manager office email.
		/// </summary>
		/// <param name="hiringManagerId">The hiring manager id.</param>
		/// <exception cref="ServiceCallException"></exception>
		public string GetHiringManagerOfficeEmail(long hiringManagerId)
		{
			var request = new GetHiringManagerOfficeEmailRequest().Prepare(AppContext, Runtime.Settings);

			request.PersonId = hiringManagerId;

			var response = EmployerService.GetHiringManagerOfficeEmail(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Email;
		}

		/// <summary>
		/// Determines whether [is default administrator] [the specified person id].
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public OfficeDefaultAdministratorModel IsDefaultAdministrator(long personId, long? officeId = null)
		{
			var request = new OfficeRequest().Prepare(AppContext, Runtime.Settings);

			var criteria = new OfficeCriteria {PersonId = personId, OfficeId = officeId};
			request.Criteria = criteria;

			var response = EmployerService.IsOfficeAdministrator(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return new OfficeDefaultAdministratorModel {IsDefaultAdministrator = response.IsDefaultAdministrator, Offices = response.Offices};
		}

		/// <summary>
		/// Creates the homepage alert.
		/// </summary>
		/// <param name="homepageAlerts">The homepage alerts.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void CreateHomepageAlert(List<BusinessUnitHomepageAlertView> homepageAlerts)
		{
			var request = new CreateBusinessUnitHomepageAlertRequest().Prepare(AppContext, Runtime.Settings);
			request.HomepageAlerts = homepageAlerts;

			var response = EmployerService.CreateBusinessUnitHomepageAlert(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);
		}
	}
}
