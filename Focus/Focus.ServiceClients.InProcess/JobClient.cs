﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Core.Criteria.Job;
using Focus.Core.Criteria.JobCertificate;
using Focus.Core.Criteria.JobDrivingLicenceEndorsement;
using Focus.Core.Criteria.JobLanguage;
using Focus.Core.Criteria.JobLicence;
using Focus.Core.Criteria.JobProgramsOfStudy;
using Focus.Core.Criteria.JobSpecialRequirement;
using Focus.Core.Criteria.JobTask;
using Focus.Core.Criteria.NoteReminder;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Messages.JobService;
using Focus.Core.Models;
using Focus.Core.Models.Assist;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Focus.ServiceClients.InProcess.Extensions;
using Focus.ServiceClients.Interfaces;
using Focus.Web.Core.Models;
using Framework.Core;
using Framework.Exceptions;
using JobLocationCriteria = Focus.Core.Criteria.JobLocation.JobLocationCriteria;

#endregion

namespace Focus.ServiceClients.InProcess
{
	public class JobClient : ClientBase, IJobClient
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobClient" /> class.
		/// </summary>
		/// <param name="appContext">The application context.</param>
		/// <param name="runtime">The runtime.</param>
		public JobClient(AppContextModel appContext, ServiceClientRuntime runtime) : base(appContext, runtime) { }
		
		/// <summary>
		/// Gets the job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public JobDto GetJob(long jobId)
		{
			var request = new JobRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCriteria { JobId = jobId };
			
			if (Runtime.Settings.Module == FocusModules.Talent)
				request.Criteria.EmployerId = AppContext.User.EmployerId;

			var response = JobService.GetJob(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);  

			return response.Job;			
		}

		/// <summary>
		/// Flags a job for follow up.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public void FlagJobForFollowUp(long jobId)
		{
			var request = new JobRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCriteria {JobId = jobId};

			var response = JobService.FlagJobForFollowUp(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
		}

		/// <summary>
		/// Saves the job.
		/// </summary>
		/// <param name="job">The job.</param>
		/// <returns></returns>
		public JobDto SaveJob(JobDto job)
		{
			var request = new SaveJobRequest().Prepare(AppContext, Runtime.Settings);
			request.Job = job;
			request.Module = Runtime.Settings.Module;

			var response = JobService.SaveJob(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 

			return response.Job;
		}

		/// <summary>
		/// Gets the job view.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public JobViewDto GetJobView(long jobId)
		{
			var request = new JobViewRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCriteria { JobId = jobId, EmployerId = AppContext.User.EmployerId, FetchOption = CriteriaBase.FetchOptions.Single };
		  request.Module = Runtime.Settings.Module;

			var response = JobService.GetJobView(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);  

			return response.JobView;
		}

		/// <summary>
		/// Saves the job certificates.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobCertificates">The job certificates.</param>
		public void SaveJobCertificates(long jobId, List<JobCertificateDto> jobCertificates)
		{
			var request = new SaveJobCertificatesRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCertificateCriteria { JobId = jobId };
			request.JobCertificates = jobCertificates;

			var response = JobService.SaveJobCertificates(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);  
		}

		/// <summary>
		/// Saves the job languages.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobLanguages">The job languages.</param>
		public void SaveJobLanguages(long jobId, List<JobLanguageDto> jobLanguages)
		{
			var request = new SaveJobLanguagesRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobLanguageCriteria { JobId = jobId };
			request.JobLanguages = jobLanguages;

			var response = JobService.SaveJobLanguages(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);  
		}

		/// <summary>
		/// Saves the job licences.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobLicences">The job licences.</param>
		public void SaveJobLicences(long jobId, List<JobLicenceDto> jobLicences)
		{
			var request = new SaveJobLicencesRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobLicenceCriteria { JobId = jobId };
			request.JobLicences = jobLicences;

			var response = JobService.SaveJobLicences(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);  
		}

		/// <summary>
		/// Saves the job special requirements request.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobSpecialRequirements">The job special requirements.</param>
		public void SaveJobSpecialRequirements(long jobId, List<JobSpecialRequirementDto> jobSpecialRequirements)
		{
			var request = new SaveJobSpecialRequirementsRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobSpecialRequirementCriteria { JobId = jobId };
			request.JobSpecialRequirements = jobSpecialRequirements;

			var response = JobService.SaveJobSpecialRequirements(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);  
		}

		/// <summary>
		/// Saves the job driving licence endorsements.
		/// </summary>
		/// <param name="jobId">The job identifier.</param>
		/// <param name="jobDrivingLicenceEndorsements">The job driving licence endorsements.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void SaveJobDrivingLicenceEndorsements(long jobId, List<JobDrivingLicenceEndorsementDto> jobDrivingLicenceEndorsements)
		{
			var request = new SaveJobDrivingLicenceEndorsementsRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobDrivingLicenceEndorsementCriteria { JobId = jobId };
			request.JobDrivingLicenceEndorsements = jobDrivingLicenceEndorsements;

			var response = JobService.SaveJobDrivingLicenceEndorsements(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
		}

		/// <summary>
		/// Saves the job locations.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobLocations">The job locations.</param>
		public string SaveJobLocations(long jobId, List<JobLocationDto> jobLocations)
		{
			var request = new SaveJobLocationsRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobLocationCriteria { JobId = jobId };
			request.JobLocations = jobLocations;

			var response = JobService.SaveJobLocations(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

		  return response.Location;
		}

		/// <summary>
		/// Saves the job programs of study.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobProgramsOfStudy">The job programs of study.</param>
		public void SaveJobProgramsOfStudy(long jobId, List<JobProgramOfStudyDto> jobProgramsOfStudy)
		{
			var request = new SaveJobProgramsOfStudyRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobProgramsOfStudyCriteria { JobId = jobId };
			request.JobProgramsOfStudy = jobProgramsOfStudy;

			var response = JobService.SaveJobProgramsOfStudy(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
		}

		/// <summary>
		/// Gets the job details.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="getRecommendedCount">if set to <c>true</c> [get recommended count].</param>
    /// <returns></returns>
    public JobDetailsView GetJobDetails(long jobId, bool getRecommendedCount = false)
		{
			var request = new JobDetailsRequest().Prepare(AppContext, Runtime.Settings);
			request.JobId = jobId;
			request.GetRecommendationCount = getRecommendedCount;
		  request.Module = Runtime.Settings.Module;

			var response = JobService.GetJobDetails(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);  
			
			return response.JobDetails;
		}

		/// <summary>
		/// Gets the job details.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public JobPrevDataDto GetJobPrevData( long jobId )
		{
			var request = new JobPrevDataRequest().Prepare( AppContext, Runtime.Settings );
			request.JobId = jobId;
			request.Module = Runtime.Settings.Module;

			var response = JobService.GetJobPrevData( request );

			if( response.Acknowledgement == AcknowledgementType.Failure )
				throw new ServiceCallException( response.Message, (int)response.Error, response.Exception );

			return response.JobPrevData;
		}

		/// <summary>
		/// Gets the job activity summary.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public JobActivityView GetJobActivitySummary(long jobId)
		{
			var request = new JobDetailsRequest().Prepare(AppContext, Runtime.Settings);
			request.JobId = jobId;
			
			var response = JobService.GetJobActivitySummary(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.JobActivitySummary;
		}

		/// <summary>
		/// Gets the jobs as a List.
		/// </summary>
		/// <param name="jobStatus">The job status.</param>
		/// <returns></returns>
		public List<JobViewDto> GetJobs(JobStatuses jobStatus)
		{
			var request = new JobViewRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCriteria
			{
				EmployerId = AppContext.User.EmployerId,
				JobStatus = jobStatus,
				FetchOption = CriteriaBase.FetchOptions.List
			};
      request.Module = Runtime.Settings.Module;
			
			var response = JobService.GetJobView(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 

			return response.JobViews;
		}

		/// <summary>
		/// Gets the jobs by external ID
		/// </summary>
		/// <param name="jobStatus">The job status.</param>
		/// <param name="externalId">The employee id.</param>
		/// <returns></returns>
		public JobDto GetJob( JobStatuses jobStatus, string externalId )
		{
			var request = new JobRequest().Prepare( AppContext, Runtime.Settings );
			request.Criteria = new JobCriteria
			{
				ExternalId = externalId,
				JobStatus = jobStatus,
				FetchOption = CriteriaBase.FetchOptions.List
			};
			request.Module = Runtime.Settings.Module;

			var response = JobService.GetJob( request );

			if( response.Acknowledgement == AcknowledgementType.Failure )
				throw new ServiceCallException( response.Message, (int)response.Error, response.Exception );

			return response.Job;
		}

		/// <summary>
		/// Gets the jobs available to talent user by employee ID
		/// </summary>
		/// <param name="jobStatus">The job status.</param>
		/// <param name="employeeId">The employee id.</param>
		/// <returns></returns>
		public List<JobViewDto> GetJobs(JobStatuses jobStatus, long? employeeId)
		{
			var request = new JobViewRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCriteria
			{
				EmployeeId = employeeId,
				JobStatus = jobStatus,
				FetchOption = CriteriaBase.FetchOptions.List
			};
      request.Module = Runtime.Settings.Module;

			var response = JobService.GetJobView(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.JobViews;
		}
    /// <summary>
    /// Gets the jobs as a PagedList.
    /// </summary>
    /// <param name="jobStatus">The job status.</param>
    /// <param name="pageIndex">Index of the page.</param>
    /// <param name="maximumRows">The maximum rows.</param>
    /// <param name="orderBy">The order by.</param>
    /// <param name="talentDraftJobs">The talent draft jobs flag.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public PagedList<JobViewModel> GetJobs(JobStatuses jobStatus, int pageIndex, int maximumRows, string orderBy, bool talentDraftJobs = false)
    {
      return GetJobs(jobStatus, 0, 0, false, pageIndex, maximumRows, orderBy, talentDraftJobs);
    }

	  /// <summary>
	  /// Gets the jobs for a business unit as a PagedList.
	  /// </summary>
	  /// <param name="jobStatus">The job status.</param>
	  /// <param name="businessUnitId">The business unit identifier.</param>
	  /// <param name="pageIndex">Index of the page.</param>
	  /// <param name="maximumRows">The maximum rows.</param>
	  /// <param name="orderBy">The order by.</param>
	  /// <param name="talentDraftJobs">The talent draft jobs flag.</param>
	  /// <returns></returns>
	  /// <exception cref="ServiceCallException"></exception>
	  public PagedList<JobViewModel> GetJobsForBusinessUnit(JobStatuses jobStatus, long businessUnitId, int pageIndex, int maximumRows, string orderBy, bool talentDraftJobs = false)
	  {
      return GetJobs(jobStatus, businessUnitId, 0, true, pageIndex, maximumRows, orderBy, talentDraftJobs);
	  }

    /// <summary>
    /// Gets the jobs for an employer as a PagedList.
    /// </summary>
    /// <param name="jobStatus">The job status.</param>
    /// <param name="employerId">The employer identifier.</param>
    /// <param name="pageIndex">Index of the page.</param>
    /// <param name="maximumRows">The maximum rows.</param>
    /// <param name="orderBy">The order by.</param>
    /// <param name="talentDraftJobs">The talent draft jobs flag.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public PagedList<JobViewModel> GetJobsForEmployer(JobStatuses jobStatus, long employerId, int pageIndex, int maximumRows, string orderBy, bool talentDraftJobs = false)
    {
      return GetJobs(jobStatus, 0, employerId, true, pageIndex, maximumRows, orderBy, talentDraftJobs);
    }

	  /// <summary>
    /// Gets the jobs as a PagedList.
    /// </summary>
    /// <param name="jobStatus">The job status.</param>
    /// <param name="businessUnitId">The business unit identifier.</param>
    /// <param name="bypassEmployeeId">Whether to get all jobs for the business unit (if specified) rather than just for the employee (Talent only)</param>
    /// <param name="employerId">The employer identifier.</param>
    /// <param name="pageIndex">Index of the page.</param>
    /// <param name="maximumRows">The maximum rows.</param>
    /// <param name="orderBy">The order by.</param>
    /// <param name="talentDraftJobs">The talent draft jobs flag.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public PagedList<JobViewModel> GetJobs(JobStatuses jobStatus, long businessUnitId, long employerId, bool bypassEmployeeId, int pageIndex, int maximumRows, string orderBy, bool talentDraftJobs = false)
    {

      var request = new JobViewRequest().Prepare(AppContext, Runtime.Settings);
      request.Criteria = new JobCriteria
      {
        JobStatus = jobStatus,
        TalentDraftJobs = talentDraftJobs,
        PageIndex = pageIndex,
        PageSize = maximumRows,
        OrderBy = orderBy,
        FetchOption = CriteriaBase.FetchOptions.PagedList
      };

      if (businessUnitId > 0)
        request.Criteria.BusinessUnitId = businessUnitId;

      if (employerId > 0)
        request.Criteria.EmployerId = employerId;

      if (Runtime.Settings.Module == FocusModules.Talent)
      {
        request.Criteria.ValidateEmployeeId = AppContext.User.EmployeeId;

        if (!bypassEmployeeId)
          request.Criteria.EmployeeId = AppContext.User.EmployeeId;

        if (!Runtime.Settings.SharingJobsForSameFEIN)
          request.Criteria.EmployeeIdForBusinessUnit = AppContext.User.EmployeeId;
      }
      request.Module = Runtime.Settings.Module;

      var response = JobService.GetJobView(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.JobViewsPaged;
    }

		/// <summary>
		/// Gets the jobs as a PagedList.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<JobViewModel> GetJobs(JobCriteria criteria)
    {

      var request = new JobViewRequest().Prepare(AppContext, Runtime.Settings);

      criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;
     
      request.Criteria = criteria;
      request.Module = Runtime.Settings.Module;

      var response = JobService.GetJobView(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.JobViewsPaged;
    }


		/// <summary>
		/// Gets the job description.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public string GetJobDescription(long jobId)
		{
			var request = new JobDescriptionRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCriteria { JobId = jobId, FetchOption = CriteriaBase.FetchOptions.Single };

			var response = JobService.GetJobDescription(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);  

			return response.JobDescription;
		}

    /// <summary>
    /// Gets the job tasks.
    /// </summary>
    /// <param name="onetId">The onet id.</param>
    /// <param name="scope">The job task scope (job or resume).</param>
    /// <returns>A list of job tasks matching the criteria</returns>
    public List<JobTaskDetailsView> GetJobTasks(long onetId, JobTaskScopes? scope = null)
    {
      var request = new JobTaskRequest().Prepare(AppContext, Runtime.Settings);
      request.Criteria = new JobTaskCriteria
      {
        OnetId = onetId,
        Scope = scope
      };

      var response = JobService.GetJobTasks(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.JobTasks;
    }

		/// <summary>
		/// Gets all job locations.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		public List<JobLocationDto> GetAllJobLocations(long employerId)
		{
			var request = new JobLocationRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobLocationCriteria { EmployerId = employerId, FetchOption = CriteriaBase.FetchOptions.List };

			var response = JobService.GetJobLocations(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);  

			// Remove duplicates
			var uniqueLocations = new List<JobLocationDto>();

			foreach (var location in response.JobLocations.Where(location => uniqueLocations.Count(x => x.Location == location.Location && x.IsPublicTransitAccessible == location.IsPublicTransitAccessible) == 0))
			{
				uniqueLocations.Add(location);
			}
			
			return uniqueLocations;
		}

		/// <summary>
		/// Gets all job locations for business unit.
		/// </summary>
		/// <param name="businessUnitId">The business unit identifier.</param>
		/// <returns>All the job locations for the specified buiness unit identifer.</returns>
		/// <exception cref="Framework.Exceptions.ServiceCallException"></exception>
		public List<JobLocationDto> GetAllJobLocationsForBusinessUnit(long businessUnitId)
		{
			var request = new JobLocationRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobLocationCriteria { BusinessUnitId = businessUnitId, FetchOption = CriteriaBase.FetchOptions.List };
			var response = JobService.GetJobLocations(request);
			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			// Remove duplicates
			var uniqueLocations = new List<JobLocationDto>();
			foreach (var location in response.JobLocations.Where(location => uniqueLocations.Count(x => x.Location == location.Location && x.IsPublicTransitAccessible == location.IsPublicTransitAccessible) == 0))
			{
				uniqueLocations.Add(location);
			}

			return uniqueLocations;
		}

		/// <summary>
		/// Gets the job locations by job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public List<JobLocationDto> GetJobLocations(long jobId)
		{
			var request = new JobLocationRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobLocationCriteria { JobId = jobId, EmployerId = AppContext.User.EmployerId, FetchOption = CriteriaBase.FetchOptions.List };
			var response = JobService.GetJobLocations(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);  

			return response.JobLocations;
		}

		/// <summary>
		/// Gets the job programs of study.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public List<JobProgramOfStudyDto> GetJobProgramsOfStudy(long jobId)
		{
			var request = new JobProgramOfStudyRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobProgramsOfStudyCriteria { JobId = jobId, EmployerId = AppContext.User.EmployerId, FetchOption = CriteriaBase.FetchOptions.List };

			var response = JobService.GetJobProgramsOfStudy(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.JobProgramsOfStudy;
		}

		/// <summary>
		/// Gets the job certificates.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public List<JobCertificateDto> GetJobCertificates(long jobId)
		{
			var request = new JobCertificateRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCertificateCriteria { JobId = jobId  };

			var response = JobService.GetJobCertificates(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 

			return response.JobCertificates;
		}
		
		/// <summary>
		/// Gets the job languages.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public List<JobLanguageDto> GetJobLanguages(long jobId)
		{
			var request = new JobLanguageRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobLanguageCriteria { JobId = jobId };

			var response = JobService.GetJobLanguages(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 

			return response.JobLanguages;
		}

		/// <summary>
		/// Gets the job licences.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public List<JobLicenceDto> GetJobLicences(long jobId)
		{
			var request = new JobLicenceRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobLicenceCriteria { JobId = jobId };

			var response = JobService.GetJobLicences(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 

			return response.JobLicences;
		}

		/// <summary>
		/// Gets the job special requirements.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public List<JobSpecialRequirementDto> GetJobSpecialRequirements(long jobId)
		{
			var request = new JobSpecialRequirementRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobSpecialRequirementCriteria { JobId = jobId };

			var response = JobService.GetJobSpecialRequirements(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 

			return response.JobSpecialRequirements;
		}

		/// <summary>
		/// Gets the count job special requirements.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns>The count</returns>
		public int GetJobSpecialRequirementsCount(long jobId)
		{
			var request = new JobSpecialRequirementRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobSpecialRequirementCriteria
			{
				FetchOption = CriteriaBase.FetchOptions.Count, 
				JobId = jobId
			};

			var response = JobService.GetJobSpecialRequirements(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.JobRequirementsCount;
		}

		/// <summary>
		/// Gets the job driving licence endorsements.
		/// </summary>
		/// <param name="jobId">The job identifier.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<JobDrivingLicenceEndorsementDto> GetJobDrivingLicenceEndorsements(long jobId)
		{
			var request = new JobDrivingLicenceEndorsementRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobDrivingLicenceEndorsementCriteria { JobId = jobId };

			var response = JobService.GetJobDrivingLicenceEndorsements(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.JobDrivingLicenceEndorsements;
		}

		/// <summary>
		/// Gets the similar jobs.
		/// </summary>
		/// <param name="search">The search.</param>
		/// <param name="jobType"></param>
		/// <returns></returns>
		/// <exception cref="Framework.Exceptions.ServiceCallException"></exception>
		public List<SimilarJobView> GetSimilarJobs(string search, JobTypes jobType)
		{
			if(search.IsNullOrEmpty()) return new List<SimilarJobView>();

			var request = new SimilarJobRequest().Prepare(AppContext, Runtime.Settings);
			request.JobTitle = search;
		  request.JobType = jobType;

			var response = JobService.GetSimilarJobs(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 

			return response.Jobs;
		}

		/// <summary>
		/// Posts the job.
		/// </summary>
		/// <param name="job">The job.</param>
		/// <param name="automaticApproval">Whether to automatically approve the posting</param>
		/// <param name="contentRating">The content rating.</param>
		/// <param name="inappropiateWords">The inappropiate words.</param>
		/// <param name="specialConditions"></param>
		/// <returns></returns>
		/// <exception cref="Framework.Exceptions.ServiceCallException"></exception>
		/// <exception cref="ServiceCallException"></exception>
		public JobDto PostJob(JobDto job, bool automaticApproval, out ContentRatings contentRating, out List<string> inappropiateWords, out bool specialConditions)
		{
			var request = new PostJobRequest().Prepare(AppContext, Runtime.Settings);
			request.Job = job;
		  request.Module = Runtime.Settings.Module;
			if (automaticApproval)
				request.InitialApprovalStatus = ApprovalStatuses.Approved;
			if (AppContext.User.IsNotNull())
			{
				request.ExternalAdminId = AppContext.User.ExternalUserId;
			  request.ExternalOfficeId = AppContext.User.ExternalOfficeId;
				request.ExternalPassword = AppContext.User.ExternalPassword;
				request.ExternalUsername = AppContext.User.ExternalUserName;
			}
			var response = JobService.PostJob(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 

			contentRating = response.ContentRating;
			inappropiateWords = response.InappropiateWords;
			specialConditions = response.SpecialConditions;

			return response.Job;
		}

    /// <summary>
    /// Holds the job.
    /// </summary>
    /// <param name="jobId">The job id.</param>
    /// <param name="approvalStatus">An optional approval status.</param>
		public void HoldJob(long jobId, ApprovalStatuses? approvalStatus = null)
		{
			var request = new JobRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCriteria { JobId = jobId };
		  request.JobStatus = JobStatuses.OnHold;
		  request.ApprovalStatus = approvalStatus;

			var response = JobService.UpdateJobStatus(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 
		}

    /// <summary>
    /// Sets the job back to draft
    /// </summary>
    /// <param name="jobId">The job id.</param>
    /// <param name="approvalStatus">An optional approval status.</param>
    public void SetJobToDraft(long jobId, ApprovalStatuses? approvalStatus = null)
    {
      var request = new JobRequest().Prepare(AppContext, Runtime.Settings);
      request.Criteria = new JobCriteria { JobId = jobId };
      request.JobStatus = JobStatuses.Draft;
      request.ApprovalStatus = approvalStatus;

      var response = JobService.UpdateJobStatus(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
    }

		/// <summary>
		/// Deletes the job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		public void DeleteJob(long jobId)
		{
			var request = new JobRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCriteria { JobId = jobId };

			var response = JobService.DeleteJob(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 
		}

		/// <summary>
		/// Duplicates the job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		public long DuplicateJob(long jobId)
		{
			var request = new JobRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCriteria { JobId = jobId };

			var response = JobService.DuplicateJob(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return (response.Job.IsNotNull() && response.Job.Id.HasValue) ? response.Job.Id.Value : 0;
		}
		/// <summary>
		/// Refreshes the job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="newExpirationDate">The new expiration date.</param>
		/// <param name="postingSurvey">The posting survey.</param>
		/// <param name="newNumberOfOpenings"></param>
		/// <exception cref="Framework.Exceptions.ServiceCallException"></exception>
		public void RefreshJob(long jobId, DateTime newExpirationDate, PostingSurveyDto postingSurvey, int? newNumberOfOpenings = null)
		{
			var request = new JobRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCriteria { JobId = jobId };
			request.NewExpirationDate = newExpirationDate;
			request.PostingSurvey = postingSurvey;
			request.NewNumberOfOpenings = newNumberOfOpenings;
		  request.Module = Runtime.Settings.Module;

			var response = JobService.RefreshJob(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 
		}

		/// <summary>
		/// Reactivates the job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="newExpirationDate">The new expiration date.</param>
		public void ReactivateJob(long jobId, DateTime newExpirationDate)
		{
			var request = new JobRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCriteria { JobId = jobId };
			request.NewExpirationDate = newExpirationDate;

			var response = JobService.ReactivateJob(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 
		}

		/// <summary>
		/// Closes the job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="postingSurvey">The posting survey.</param>
		public void CloseJob(long jobId, PostingSurveyDto postingSurvey)
		{
			var request = new JobRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCriteria { JobId = jobId };
			request.PostingSurvey = postingSurvey;
			request.JobStatus = JobStatuses.Closed;
		  request.Module = Runtime.Settings.Module;

			var response = JobService.CloseJob(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 
		}

		/// <summary>
		/// Gets the job posting referrals.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public PagedList<JobPostingReferralViewDto> GetJobPostingReferrals(JobPostingReferralCriteria criteria)
		{
			var request = new JobPostingReferralViewRequest().Prepare(AppContext, Runtime.Settings);
			criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;
			request.Criteria = criteria;
			
			var response = JobService.GetJobPostingReferralView(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 

			return response.ReferralViewsPaged;
		}

		/// <summary>
		/// Gets the job posting referral.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public JobPostingReferralViewDto GetJobPostingReferral(long jobId)
		{
			var request = new JobPostingReferralViewRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobPostingReferralCriteria
													{
														Id = jobId,
														FetchOption = CriteriaBase.FetchOptions.Single
													};

			var response = JobService.GetJobPostingReferralView(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 

			return response.Referral;
		}

		/// <summary>
		/// Approves the posting referral.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="lockVersion"></param>
		public ErrorTypes ApprovePostingReferral(long jobId, int lockVersion)
		{
			var request = new JobRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCriteria { JobId = jobId };
		  request.Module = Runtime.Settings.Module;
			request.LockVersion = lockVersion;

			var response = JobService.ApproveReferral(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.ResponseType;
		}

		/// <summary>
		/// Denies the posting referral.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="lockVersion">The lock version.</param>
		/// <param name="referralStatusReasons">The referral status reasons.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void DenyPostingReferral(long jobId, int lockVersion, List<KeyValuePair<long, string>> referralStatusReasons = null)
		{
			var request = new JobRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCriteria { JobId = jobId };
			request.LockVersion = lockVersion;
			request.ReferralStatusReasons = referralStatusReasons;

			var response = JobService.DenyReferral(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 
		}

		/// <summary>
		/// Edits the posting referral.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="posting">The posting.</param>
		public void EditPostingReferral(long jobId, string posting)
		{
			var request = new JobRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobCriteria { JobId = jobId };
			request.NewPosting = posting;

			var response = JobService.EditPostingReferral(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 
		}

		/// <summary>
		/// Gets the employees pending postings.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <returns></returns>
		public IEnumerable<JobViewDto> GetEmployeesPendingPostings(long employeeId)
		{
			var request = new JobViewRequest().Prepare(AppContext, Runtime.Settings);

      request.Criteria = new JobCriteria
      {
        EmployeeId = employeeId,
				JobStatus = JobStatuses.AwaitingEmployerApproval,
        FetchOption = CriteriaBase.FetchOptions.List
      };
      request.Module = Runtime.Settings.Module;

			var response = JobService.GetJobView(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 

			return response.JobViews;
		}

		/// <summary>
		/// Looks up a job by job title, employer name and / or from / to dates.
		/// </summary>
		/// <param name="jobTitle">The job title.</param>
		/// <param name="employerName">Name of the employer.</param>
		/// <param name="fromDate">From date.</param>
		/// <param name="toDate">To date.</param>
		/// <param name="jobStatus">The job status.</param>
		/// <returns></returns>
		/// 
		public List<JobLookupView> LookupJob(string jobTitle, string employerName, DateTime? fromDate, DateTime? toDate, JobStatuses? jobStatus)
		{
			var request = new JobLookupRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobLookupCriteria
			                   	{
			                   		JobTitle = jobTitle,
			                   		EmployerName = employerName,
			                   		CreatedFrom = fromDate,
			                   		CreatedTo = toDate,
														JobStatus = jobStatus,
			                   		FetchOption = CriteriaBase.FetchOptions.List
			                   	};

			var response = JobService.GetJobLookup(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 

			return response.JobLookups;
		}

   

		/// <summary>
		/// Looks up a job by job id.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobStatus">The job status.</param>
		/// <returns></returns>
		public List<JobLookupView> LookupJob(long jobId, JobStatuses? jobStatus)
		{
			var request = new JobLookupRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new JobLookupCriteria
			{
				JobId = jobId,
				JobStatus = jobStatus,
				FetchOption = CriteriaBase.FetchOptions.List
			};

			var response = JobService.GetJobLookup(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 

			return response.JobLookups;
		}

		/// <summary>
		/// Gets the lens posting ID for a posted job URL by external Id (this may be an internal id, so this will be checked as well).
		/// </summary>
		/// <param name="checkId">The internal or external job id.</param>
		/// <param name="checkVeteranPriorityDate">Whether to check the VPS date.</param>
		/// <param name="jobStatuses">A list of acceptable job statuses</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public string GetJobLensPostingId(string checkId, bool checkVeteranPriorityDate, List<JobStatuses> jobStatuses = null)
		{
			var request = new JobIdsRequest
			{
				CheckId = checkId, 
				CheckVeteranPriorityDate = checkVeteranPriorityDate,
				JobStatuses = jobStatuses ?? new List<JobStatuses>{ JobStatuses.Active }
			}.Prepare( AppContext, Runtime.Settings );

			var response = JobService.GetJobLensPostingId( request );

			if( response.Acknowledgement == AcknowledgementType.Failure )
				throw new ServiceCallException( response.Message, (int)response.Error, response.Exception );

			return response.LensPostingId;
		}

		/// <summary>
		/// Gets the lens posting ID for a posted job URL by Id.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public string GetJobLensPostingId( long jobId )
		{
		  var request = new JobLensPostingIdRequest {JobId = jobId}.Prepare(AppContext, Runtime.Settings);
			
			var response = JobService.GetJobLensPostingId(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.LensPostingId;
		}

		/// <summary>
		/// Gets the job address.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public JobAddressDto GetJobAddress(long jobId)
		{
			var request = new JobAddressRequest().Prepare(AppContext, Runtime.Settings);
			request.JobId = jobId;

			var response = JobService.GetJobAddress(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 

			return response.JobAddress;
		}

    /// <summary>
    /// Gets the job address.
    /// </summary>
    /// <param name="jobId">The job id.</param>
    /// <returns></returns>
    public JobStatuses GetJobStatus(long jobId)
    {
      var request = new JobStatusRequest().Prepare(AppContext, Runtime.Settings);
      request.JobId = jobId;

      var response = JobService.GetJobStatus(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.JobStatus;
    }

    /// <summary>
    /// Gets the job dates.
    /// </summary>
    /// <param name="jobId">The job id.</param>
    /// <returns></returns>
    public JobDatesView GetJobDates(long jobId)
    {
      var request = new JobDatesRequest
      {
        JobId = jobId
      }.Prepare(AppContext, Runtime.Settings);

      var response = JobService.GetJobDates(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.JobDates;
    }

		/// <summary>
		/// Saves the job address.
		/// </summary>
		/// <param name="jobAddress">The job address.</param>
		public JobAddressDto SaveJobAddress(JobAddressDto jobAddress)
		{
			var request = new SaveJobAddressRequest().Prepare(AppContext, Runtime.Settings);
			request.JobAddress = jobAddress;

			var response = JobService.SaveJobAddress(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception); 

			return response.JobAddress;
		}

		/// <summary>
		/// Gets the application instructions.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public string GetApplicationInstructions(long jobId)
		{
			var request = new ApplicationInstructionsRequest().Prepare(AppContext, Runtime.Settings);
			request.JobId = jobId;

			var response = JobService.GetApplicationInstructions(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.ApplicationInstructions;
		}

    /// <summary>
    /// Gets the job applicants.
    /// </summary>
    /// <param name="jobId">The job id.</param>
    /// <returns></returns>
		public List<ApplicationViewDto> GetJobApplicants(long jobId)
		{
      var request = new JobApplicantsRequest {JobId = jobId}.Prepare(AppContext, Runtime.Settings);

      var response = JobService.GetJobApplicants(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Applicants;
		}

		/// <summary>
		/// Generates the posting.
		/// </summary>
		/// <param name="job">The job.</param>
		/// <param name="jobLocations">The job locations.</param>
		/// <param name="jobLanguages">The job languages.</param>
		/// <param name="jobCertificates">The job certificates.</param>
		/// <param name="jobLicences">The job licences.</param>
		/// <param name="jobSpecialRequirements">The job special requirements.</param>
		/// <param name="jobProgramsOfStudy">The job programs of study.</param>
		/// <param name="jobDrivingLicenceEndorsements">The job driving licence endorsements.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public string GeneratePosting(JobDto job, List<JobLocationDto> jobLocations, List<JobLanguageDto> jobLanguages, List<JobCertificateDto> jobCertificates, List<JobLicenceDto> jobLicences,
																	List<JobSpecialRequirementDto> jobSpecialRequirements, List<JobProgramOfStudyDto> jobProgramsOfStudy, List<JobDrivingLicenceEndorsementDto> jobDrivingLicenceEndorsements)
		{
			var request = new GeneratePostingRequest().Prepare(AppContext, Runtime.Settings);
			request.Job = job;
			request.JobLocations = jobLocations;
			request.JobLanguages = jobLanguages;
			request.JobCertificates = jobCertificates;
			request.JobLicences = jobLicences;
			request.JobSpecialRequirements = jobSpecialRequirements;
			request.JobProgramsOfStudy = jobProgramsOfStudy;
			request.JobDrivingLicenceEndorsements = jobDrivingLicenceEndorsements;

			var response = JobService.GeneratePosting(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Posting;
		}

    /// <summary>
    /// Gets the job activities for a given job
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <returns></returns>
    public PagedList<JobActionEventViewDto> GetJobActivities(JobActivityCriteria criteria)
    {
      var request =
        new JobActivitiesRequest { Criteria = criteria }.Prepare(AppContext, Runtime.Settings);

      var response = JobService.GetJobActivities(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.JobActionEventViewPaged;
    }

    /// <summary>
    /// Prints the job.
    /// </summary>
    /// <param name="jobId">The job id.</param>
    /// <param name="fileName">Name of the file.</param>
    /// <returns></returns>
    public byte[] PrintJob(long jobId, out string fileName)
    {
      var request = new PrintJobRequest { JobId = jobId }.Prepare(AppContext, Runtime.Settings);

      // Get all the job data using various service calls
      request.JobDetails = GetJobDetails(jobId);

      if (request.JobDetails.PostedBy.HasValue)
      {
	      var accountClient = new AccountClient(AppContext, Runtime);
				var userDetails = accountClient.GetUserDetails(request.JobDetails.PostedBy.GetValueOrDefault());
        request.EmployerFirstName = userDetails.PersonDetails.FirstName;
        request.EmployerLastName = userDetails.PersonDetails.LastName;
				request.PhoneNumber = userDetails.PrimaryPhoneNumber.IsNotNull() ? userDetails.PrimaryPhoneNumber.Number : String.Empty;
        request.EmployerLastName = userDetails.PersonDetails.LastName;
        request.Email = userDetails.PersonDetails.EmailAddress;
      }

      request.JobView = GetJobView(jobId);
      request.JobStatus = GetJobStatus(jobId).ToString();
      request.ActionEvents = GetJobActivities(new JobActivityCriteria{JobId = jobId, DaysBack = 180, PageSize = 1000, PageIndex = 0, FetchOption = CriteriaBase.FetchOptions.PagedList});

      var criteria = new NoteReminderCriteria
                    {EntityId = jobId, NoteReminderType = NoteReminderTypes.Note, FetchOption = CriteriaBase.FetchOptions.List};

	    var candidateClient = new CandidateClient(AppContext, Runtime);
			request.Referrals = candidateClient.GetApplicationStatusLogViews(new ApplicationStatusLogViewCriteria { JobId = jobId });

	    var coreClient = new CoreClient(AppContext, Runtime);
			request.Notes = coreClient.GetAllNotesReminders(criteria);

      criteria.NoteReminderType = NoteReminderTypes.Reminder;
			request.Reminders = coreClient.GetAllNotesReminders(criteria);

      // Lastly get the printable document
      var response = JobService.PrintJob(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      fileName = response.DocumentName;
      return response.JobDocument;
    }

    /// <summary>
    /// Gets the job activity users.
    /// </summary>
    /// <param name="jobId">The job id.</param>
    /// <returns></returns>
    public List<JobActionEventViewDto> GetJobActivityUsers (long jobId)
    {
      var request = new JobActivityUsersRequest {JobId = jobId}.Prepare(AppContext, Runtime.Settings);
      var response = JobService.GetJobActivityUsers(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.Actioners;
    }

    /// <summary>
		/// Checks the censorship for the job.
		/// </summary>
		/// <param name="job">The job.</param>
		/// <param name="censorshipType">Type of the censorship.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public List<string> CheckCensorship(JobDto job, CensorshipType censorshipType)
		{
			var request = new CheckCensorshipRequest().Prepare(AppContext, Runtime.Settings);
			request.Job = job;
			request.Type = censorshipType;

			var response = JobService.CheckCensorship(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.CensoredWords;
		}

		/// <summary>
		/// Saves the invite to apply.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="jobId">The job id.</param>
		/// <param name="candidatePersonId">The candidate person id.</param>
		/// <param name="score">The job matching score.</param>
    /// <param name="queueInvitation">Whether the invitation should be queued</param>
		/// <exception cref="ServiceCallException"></exception>
    public void SaveInviteToApply(string lensPostingId, long jobId, long candidatePersonId, int score, bool queueInvitation)
    {
      var request =
        new SaveJobInviteToApplyRequest { LensPostingId = lensPostingId, JobId = jobId, PersonId = candidatePersonId, Score = score, QueueInvitation = queueInvitation }.
          Prepare(AppContext, Runtime.Settings);

      var response = JobService.SaveJobInviteToApply(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
    }

    /// <summary>
    /// Updates the invite to apply.
    /// </summary>
    /// <param name="lensPostingId">The lens posting id.</param>
    /// <param name="candidatePersonId">The candidate person id.</param>
    /// <exception cref="ServiceCallException"></exception>
    public bool UpdateInviteToApply(string lensPostingId, long candidatePersonId)
    {
      var request = new InviteToApplyVisitedRequest {LensPostingId = lensPostingId, PersonId = candidatePersonId}.Prepare(AppContext, Runtime.Settings);
      var response = JobService.UpdateInviteToApply(request);
      
      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.IsInvitee || response.IsRecommended;
    }

	  /// <summary>
	  /// Saves the education internship skills.
	  /// </summary>
	  /// <exception cref="ServiceCallException"></exception>
	  public void SaveEducationInternshipSkills(long jobId, List<JobEducationInternshipSkillDto> skills)
    {
      var request = new SaveJobEducationInternshipSkillsRequest{ JobId = jobId, Skills = skills}.Prepare(AppContext, Runtime.Settings);
      var response = JobService.SaveJobEducationInternshipSkills(request);
      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);
    }

    /// <summary>
    /// Gets the education internship skills.
    /// </summary>
    /// <param name="jobId">The job id.</param>
    /// <returns></returns>
    public List<JobEducationInternshipSkillDto> GetEducationInternshipSkills(long jobId)
    {
      var request = new JobEducationInternshipSkillRequest().Prepare(AppContext, Runtime.Settings);
      request.JobId = jobId;

      var response = JobService.GetJobEducationInternshipSkills(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.EducationInternshipSkills;
    }

		/// <summary>
		/// Updates the job assigned office.
		/// </summary>
		/// <param name="jobOffices">The job offices.</param>
		/// <param name="jobId">The job id.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void UpdateJobAssignedOffice(List<JobOfficeMapperDto> jobOffices, long jobId)
		{
			var request = new UpdateJobAssignedOfficeRequest { JobId = jobId, JobOffices = jobOffices }.Prepare(AppContext, Runtime.Settings);

			var response = JobService.UpdateJobAssignedOffice(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);
		}

		/// <summary>
		/// Assigns the job to staff member.
		/// </summary>
		/// <param name="jobIds">The job ids.</param>
		/// <param name="personId">The person id.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void AssignJobToStaffMember(List<long> jobIds, long personId)
		{
			var request = new UpdateJobAssignedStaffMemberRequest { JobIds = jobIds, PersonId = personId }.Prepare(AppContext, Runtime.Settings);
			var response = JobService.UpdateJobAssignedStaffMember(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);
		}

		/// <summary>
		/// Updates the criminal background exclusion required.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="criminalBackgroundExclusionRequired">if set to <c>true</c> [criminal background exclusion required].</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public JobDto UpdateCriminalBackgroundExclusionRequired(long jobId, bool criminalBackgroundExclusionRequired)
		{
			var request = new UpdateCriminalBackgroundExclusionRequiredRequest { JobId = jobId, CriminalBackgroundExclusionRequired = criminalBackgroundExclusionRequired }.Prepare(AppContext, Runtime.Settings);

			var response = JobService.UpdateCriminalBackgroundExclusionRequired(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Job;
		}

		/// <summary>
		/// Gets the criminal background exclusion required.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public bool GetCriminalBackgroundExclusionRequired(long jobId)
		{
			var request = new CriminalBackgroundExclusionRequiredRequest { JobId = jobId }.Prepare(AppContext, Runtime.Settings);

			var response = JobService.GetCriminalBackgroundExclusionRequired(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.CriminalBackgroundExclusionRequired;
		}

    /// <summary>
    /// Gets the job's issues.
    /// </summary>
    /// <param name="jobId">The job Id.</param>
    /// <returns></returns>
    public JobIssuesModel GetJobIssues(long jobId)
    {
      var request = new JobIssuesRequest {JobId = jobId}.Prepare(AppContext, Runtime.Settings);
      var response = JobService.GetJobIssues(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.JobIssues;
    }

    /// <summary>
    /// Resolves the job's issues.
    /// </summary>
    /// <param name="jobId">The job unique identifier.</param>
    /// <param name="issuesResolved">The issues resolved.</param>
    /// <param name="postHireFollowUpsResolved">The post hire follow ups resolved.</param>
		/// <param name="isAutoResolution">Whether this issue is being automatically resolved</param>
		/// <exception cref="ServiceCallException"></exception>
		public void ResolveJobIssues(long jobId, List<JobIssuesFilter> issuesResolved, List<long> postHireFollowUpsResolved, bool isAutoResolution = false)
    {
      var request = new ResolveJobIssuesRequest
	    {
				JobId = jobId, 
				IssuesResolved = issuesResolved, 
				PostHireFollowUpsResolved = postHireFollowUpsResolved,
				IsAutoResolution = isAutoResolution
	    }.Prepare(AppContext, Runtime.Settings);

      var response = JobService.ResolveJobIssues(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);
    }

		/// <summary>
		/// Invites the jobseekers to apply.
		/// </summary>
		/// <param name="personIdsWithScores">The person ids of the jobseekers to invite, along with matching score.</param>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobLink">The job link.</param>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="senderEmail">The sender email.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void InviteJobseekersToApply(Dictionary<long, int> personIdsWithScores, long jobId, string jobLink, string lensPostingId, string senderEmail)
		{
			var request = new InviteJobseekersToApplyRequest
			{
				PersonIdsWithScores = personIdsWithScores,
				JobId = jobId,
				JobLink = jobLink,
				LensPostingId = lensPostingId,
				SenderEmail = senderEmail
			}.Prepare(AppContext, Runtime.Settings);

			var response = JobService.InviteJobseekersToApply(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);
		}

	  /// <summary>
	  /// Imports the job order.
	  /// </summary>
	  /// <param name="externalJobId">The external job identifier.</param>
	  public void ImportJobOrder(string externalJobId)
	  {
	    var request = new ImportJobRequest
	                    {
	                      ExternalJobIds = new List<Tuple<string, ApprovalStatuses>>
	                                         {
	                                          new Tuple<string, ApprovalStatuses>(externalJobId,ApprovalStatuses.Approved)
	                                         }
	                    }.Prepare(AppContext, Runtime.Settings);
      var response = JobService.ImportJob(request);
      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);
	  }

		/// <summary>
		/// Gets the job credit check required.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public bool GetJobCreditCheckRequired(long jobId)
		{
			var request = new GetJobCreditCheckRequiredRequest
			{JobId = jobId}.Prepare(AppContext, Runtime.Settings);

			var response = JobService.GetJobCreditCheckRequired(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.CreditCheckRequired.GetValueOrDefault();
		}

		/// <summary>
		/// Updates the credit check required.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="creditCheckRequired">if set to <c>true</c> [credit check required].</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public JobDto UpdateCreditCheckRequired(long jobId, bool creditCheckRequired)
		{
			var request = new UpdateCreditCheckRequiredRequest { JobId = jobId, CreditCheckRequired = creditCheckRequired }.Prepare(AppContext, Runtime.Settings);

			var response = JobService.UpdateCreditCheckRequired(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Job;
		}

		/// <summary>
		/// Gets the hiring manager person name email for job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public PersonNameEmail GetHiringManagerPersonNameEmailForJob(long jobId)
		{
			var request = new HiringManagerPersonNameEmailForJobRequest().Prepare(AppContext, Runtime.Settings);
			request.JobId = jobId;

			var response = JobService.GetHiringManagerPersonNameEmailForJob(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Person;
		}
		
		/// <summary>
		/// Gets the job criminal background check required.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public bool GetJobCriminalBackgroundCheckRequired(long jobId)
		{
			var request = new GetJobCreditCheckRequiredRequest { JobId = jobId }.Prepare(AppContext, Runtime.Settings);

			var response = JobService.GetJobCreditCheckRequired(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.CreditCheckRequired.GetValueOrDefault();
		}

		/// <summary>
		/// Save details from the specified job into JobPrevData for later proofing
		/// </summary>
		/// <param name="jobId"></param>
		public void SaveBeforeImageOfJob(long jobId)
		{
			var request = new JobRequest().Prepare( AppContext, Runtime.Settings );
			request.Criteria = new JobCriteria { JobId = jobId };
			request.Module = Runtime.Settings.Module;

			var response = JobService.SaveBeforeImageOfJob( request );

			if( response.Acknowledgement == AcknowledgementType.Failure )
				throw new ServiceCallException( response.Message, (int)response.Error, response.Exception );
		}
	}
}