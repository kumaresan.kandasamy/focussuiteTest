﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria.Document;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Messages.CoreService;
using Focus.Core.Messages.UtilityService;
using Focus.ServiceClients.InProcess.Extensions;
using Focus.ServiceClients.Interfaces;
using Focus.Web.Core.Models;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.InProcess
{
	public class UtilityClient : ClientBase, IUtilityClient
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="UtilityClient" /> class.
		/// </summary>
		/// <param name="appContext">The application context.</param>
		/// <param name="runtime">The runtime.</param>
		public UtilityClient(AppContextModel appContext, ServiceClientRuntime runtime) : base(appContext, runtime) { }
		
		/// <summary>
		/// Gets the plain document.
		/// </summary>
		/// <param name="docType">Type of the doc.</param>
		/// <param name="plainDocument">The plain document.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public string GetPlainDocument(DocumentType docType, string plainDocument)
		{
			var request = new PlainDocumentRequest().Prepare(AppContext, Runtime.Settings);
			request.DocumentType = docType;
			request.PlainDocument = plainDocument;

			var response = UtilityService.TagPlainDocument(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.TaggedDocument;
		}

		/// <summary>
		/// Gets the tagged document.
		/// </summary>
		/// <param name="docType">Type of the doc.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <param name="binaryData">The binary data.</param>
		/// <param name="canonize">if set to <c>true</c> [canonize].</param>
		/// <param name="html">The HTML.</param>
		/// <returns></returns>
		public string GetTaggedDocument(DocumentType docType, string fileExtension, byte[] binaryData, bool canonize, out string html)
		{
			html = string.Empty;

			var request = new BinaryDocumentRequest().Prepare(AppContext, Runtime.Settings);
			request.DocumentType = docType;
			request.FileExtension = fileExtension;
			request.BinaryContent = binaryData;
			request.Canonize = canonize;
			request.WithDocument = MimeDocumentType.Binary;

			var response = UtilityService.TagBinaryDocument(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			html = response.Html;
			return response.TaggedDocument;
		}
	}
}
