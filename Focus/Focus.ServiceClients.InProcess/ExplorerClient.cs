﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.DegreeEducationLevel;
using Focus.Core.Criteria.DegreeEducationLevelReport;
using Focus.Core.Criteria.EmployerReport;
using Focus.Core.Criteria.Explorer;
using Focus.Core.Criteria.InternshipReport;
using Focus.Core.Criteria.Job;
using Focus.Core.Criteria.Skill;
using Focus.Core.Criteria.SkillReport;
using Focus.Core.Criteria.StudyPlace;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Messages;
using Focus.Core.Messages.ExplorerService;
using Focus.Core.Models;
using Focus.Core.Views;
using Focus.ServiceClients.InProcess.Extensions;
using Focus.ServiceClients.Interfaces;
using Focus.Web.Core.Models;
using Framework.Core;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.InProcess
{
	public class ExplorerClient : ClientBase, IExplorerClient
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ExplorerClient" /> class.
		/// </summary>
		/// <param name="appContext">The application context.</param>
		/// <param name="runtime">The runtime.</param>
		public ExplorerClient(AppContextModel appContext, ServiceClientRuntime runtime) : base(appContext, runtime) { }
		
		/// <summary>
		/// Gets the ping.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public PingResponse Ping(string name)
		{
			var request = new PingRequest().Prepare(AppContext, Runtime.Settings);
			request.Name = name;

			var response = ExplorerService.Ping(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response;
		}

		/// <summary>
		/// Gets the state areas.
		/// </summary>
		/// <returns></returns>
		public List<StateAreaViewDto> GetStateAreas()
		{
			var request = new StateAreaRequest().Prepare(AppContext, Runtime.Settings);

			var response = ExplorerService.GetStateAreas(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.StateAreas;
		}

		/// <summary>
		/// Gets a state area.
		/// </summary>
		/// <param name="stateAreaId"></param>
		/// <returns></returns>
		public StateAreaViewDto GetStateArea(long stateAreaId)
		{
			return GetStateAreas().SingleOrDefault(x => x.Id == stateAreaId);
		}

		/// <summary>
		/// Gets the state area name.
		/// </summary>
		/// <param name="stateAreaId">The state area id.</param>
		/// <returns></returns>
		public string GetStateAreaName(long stateAreaId)
		{
			return GetStateAreas().Where(x => x.Id == stateAreaId).Select(x => x.StateAreaName).SingleOrDefault();
		}

    /// <summary>
    /// Gets the state area default.
    /// </summary>
    /// <returns></returns>
    public StateAreaViewDto GetStateAreaDefault()
    {
      var defaultStateArea = GetStateAreas().SingleOrDefault(x => x.IsDefault);
      if (defaultStateArea.IsNull())
      {
        defaultStateArea = GetStateAreas().SingleOrDefault(x => x.StateSortOrder == 0);
      }
      return defaultStateArea;
    }

    /// <summary>
    /// Gets the All States/All Areas entry.
    /// </summary>
    /// <returns>The DTO object for the 'All States' record</returns>
    public StateAreaViewDto GetAllStatesAllAreasEntry()
    {
      return GetStateAreas().FirstOrDefault(x => x.StateSortOrder == 0 && x.AreaSortOrder == 0);
    }

		/// <summary>
		/// Gets the states.
		/// </summary>
		/// <returns></returns>
		public List<StateAreaViewDto> GetStates()
		{
			return (from sa in GetStateAreas()
							where sa.AreaSortOrder == 0
							orderby sa.StateSortOrder, sa.StateAreaName
							select sa).ToList();
		}

		/// <summary>
		/// Gets the areas.
		/// </summary>
		/// <param name="stateAreaId">The state id.</param>
		/// <returns></returns>
		public List<StateAreaViewDto> GetAreas(long stateAreaId)
		{
			var areas = new List<StateAreaViewDto>();

			var stateAreas = GetStateAreas();

			var selectedState = stateAreas.SingleOrDefault(x => x.Id == stateAreaId);
			if (selectedState != null)
			{
				if (selectedState.StateSortOrder == 0)
				{
					areas.Add(selectedState);
					areas[0].StateAreaName = areas[0].AreaName;
				}
				else
				{
					areas = (from sa in stateAreas
									 where sa.StateId == selectedState.StateId
									 orderby sa.AreaSortOrder, sa.StateAreaName
									 select sa).ToList();
				}
			}

			return areas;
		}

		/// <summary>
		/// Gets the jobs.
		/// </summary>
		/// <param name="stateAreaId">The state area id.</param>
		/// <param name="searchTerm">The search term.</param>
		/// <returns></returns>
		public List<JobDto> GetJobs(long stateAreaId, string searchTerm)
		{
			var request = new JobStateAreaRequest().Prepare(AppContext, Runtime.Settings);
			request.StateAreaId = stateAreaId;
			request.SearchTerm = searchTerm;

			var response = ExplorerService.GetJobs(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Jobs;
		}

		/// <summary>
		/// Gets the employers.
		/// </summary>
		/// <param name="stateAreaId">The state area id.</param>
		/// <param name="searchTerm">The search term.</param>
		/// <returns></returns>
		public List<EmployerDto> GetEmployers(long stateAreaId, string searchTerm)
		{
			var request = new EmployerRequest().Prepare(AppContext, Runtime.Settings);
			request.StateAreaId = stateAreaId;
			request.SearchTerm = searchTerm;

			var response = ExplorerService.GetEmployers(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Employers;
		}

		/// <summary>
		/// Gets the degree education levels.
		/// </summary>
		/// <param name="stateAreaId">The state area id.</param>
		/// <param name="searchTerm">The search term.</param>
		/// <param name="searchClientDataOnly">A flag indicating whether to search client data only</param>
		/// <param name="detailedDegreeLevel">The detailed degree level.</param>
		/// <param name="count"></param>
		/// <returns></returns>
		/// <exception cref="Framework.Exceptions.ServiceCallException"></exception>
		public List<DegreeAliasViewDto> GetDegreeAliases(long stateAreaId, string searchTerm, bool? searchClientDataOnly, DetailedDegreeLevels detailedDegreeLevel, int count = -1)
		{
			var request = new DegreeEducationLevelRequest().Prepare(AppContext, Runtime.Settings);
			var searchCriteria = new DegreeEducationLevelCriteria
				{
					StateAreaId = stateAreaId,
					SearchTerm = searchTerm,
					SearchClientDataOnly = searchClientDataOnly,
					DetailedDegreeLevel = detailedDegreeLevel,
					ListSize = count
				};

			request.SearchCriteria = searchCriteria;

			var response = ExplorerService.GetDegreeAliases(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.DegreeAliases;
		}

    /// <summary>
    /// Gets a career area.
    /// </summary>
    /// <param name="careerAreaId">The id of the career area to get.</param>
    /// <returns>The career area dto</returns>
    /// <exception cref="ServiceCallException"></exception>
    public CareerAreaDto GetCareerArea(long careerAreaId)
    {
      var request = new CareerAreaRequest
      {
        CareerAreaId = careerAreaId
      }.Prepare(AppContext, Runtime.Settings);

      var response = ExplorerService.GetCareerArea(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.CareerArea;
    }

    /// <summary>
		/// Gets the career areas.
		/// </summary>
		/// <returns></returns>
		public List<CareerAreaDto> GetCareerAreas()
		{
			var request = new CareerAreaRequest().Prepare(AppContext, Runtime.Settings);

			var response = ExplorerService.GetCareerAreas(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.CareerAreas;
		}

		public List<CareerAreaJobView> GetCareerAreaJobs(long careerAreaId, bool filterJobs = false)
		{
			var request = new CareerAreaJobRequest().Prepare(AppContext, Runtime.Settings);
			request.CareerAreaId = careerAreaId;
		  request.FilterMappedROnets = filterJobs;

			var response = ExplorerService.GetCareerAreaJobs(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.CareerAreaJobs;
		}

    /// <summary>
    /// Gets total jobs by career area
    /// </summary>
    /// <param name="criteria">Criteria to restrict jobs being entered</param>
    /// <param name="listSize">Size of the list.</param>
    /// <returns>A list of career areas and job totals.</returns>
    public List<CareerAreaJobTotalView> GetCareerAreaJobTotals(ExplorerCriteria criteria, int listSize)
    {
      var originalListSize = criteria.ListSize;
      criteria.ListSize = listSize;

      var request = new CareerAreaJobTotalRequest
      {
        Criteria = criteria
      }.Prepare(AppContext, Runtime.Settings);

      var response = ExplorerService.GetCareerAreaJobTotals(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      criteria.ListSize = originalListSize;

      return response.CareerAreaJobTotals;
    }

    /// <summary>
    /// Gets the job career areas.
    /// </summary>
    /// <param name="jobId">The job id.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public List<long> GetJobCareerAreas(long jobId)
    {
      var request = new JobCareerAreaRequest {JobId = jobId}.Prepare(AppContext, Runtime.Settings);
      var response = ExplorerService.GetJobCareerAreas(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int) response.Error);

      return response.CareerAreas;
    }

		/// <summary>
		/// Gets the job title.
		/// </summary>
		/// <param name="jobTitleId">The job title id.</param>
		/// <returns></returns>
		public JobTitleDto GetJobTitle(long jobTitleId)
		{
			var request = new JobTitleRequest().Prepare(AppContext, Runtime.Settings);

			request.JobTitleId = jobTitleId;

			var response = ExplorerService.GetJobTitles(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.JobTitle;
		}

		/// <summary>
		/// Gets the job titles.
		/// </summary>
		/// <param name="searchTerm">The search term.</param>
		/// <returns></returns>
		public List<JobTitleDto> GetJobTitles(string searchTerm)
		{
			var request = new JobTitleRequest().Prepare(AppContext, Runtime.Settings);

			request.SearchTerm = searchTerm;

			var response = ExplorerService.GetJobTitles(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.JobTitles;
		}

		/// <summary>
		/// Gets the skill categories.
		/// </summary>
		/// <param name="parentSkillCategoryId">The parent skill category id.</param>
		/// <returns></returns>
		public List<SkillCategoryDto> GetSkillCategories(long? parentSkillCategoryId)
		{
			var request = new SkillCategoryRequest().Prepare(AppContext, Runtime.Settings);

			var response = ExplorerService.GetSkillCategories(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			if (parentSkillCategoryId.IsNull())
			{
				return response.SkillCategories.Where(x => x.ParentId == null).OrderBy(x => x.Name).ToList();
			}

			return response.SkillCategories.Where(x => x.ParentId == parentSkillCategoryId).OrderBy(x => x.Name).ToList();
		}

		/// <summary>
		/// Gets the skills category skills.
		/// </summary>
		/// <returns></returns>
		public List<SkillCategorySkillViewDto> GetSkillCategorySkills()
		{
			var request = new SkillRequest().Prepare(AppContext, Runtime.Settings);

			var response = ExplorerService.GetSkills(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Skills;
		}

		/// <summary>
		/// Gets the skills.
		/// </summary>
		/// <param name="skillCategoryId">The skill category id.</param>
		/// <returns></returns>
		public List<SkillDto> GetSkills(long? skillCategoryId)
		{
			var request = new SkillRequest().Prepare(AppContext, Runtime.Settings);

			var response = ExplorerService.GetSkills(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			if (skillCategoryId.IsNull())
			{
				return (from s in response.Skills
								group s by new { s.SkillId, s.SkillName, s.SkillType }
									into g
									select new SkillDto
													{
														Id = g.Key.SkillId,
														Name = g.Key.SkillName,
														SkillType = g.Key.SkillType
													}).ToList();
			}

			return (from s in response.Skills
							where s.SkillCategoryId == skillCategoryId
							select new SkillDto
											{
												Id = s.SkillId,
												Name = s.SkillName,
												SkillType = s.SkillType
											}).ToList();
		}

    /// <summary>
    /// Gets skills associated with jobs
    /// </summary>
    /// <returns>A list of skills with jobs</returns>
		public List<SkillsForJobsViewDto> GetSkillsForJobs(int maxRank, string term, int count = 50)
    {
      var request = new SkillRequest().Prepare(AppContext, Runtime.Settings);
	    request.TermSearchCriteria = new TermCriteria
		    {
			    MaxRank = maxRank,
					Term = term,
					ListSize = count
		    };

      var response = ExplorerService.GetSkillsForJobs(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.SkillsForJobs.Where(s => s.Rank <= maxRank).ToList();
    }

		/// <summary>
		/// Gets the site search results.
		/// </summary>
		/// <param name="searchTerm">The search term.</param>
		/// <returns></returns>
		public List<SiteSearchResultModel> GetSiteSearchResults(string searchTerm)
		{
			var request = new SiteSearchRequest().Prepare(AppContext, Runtime.Settings);

			request.SearchTerm = searchTerm;
		  request.StateAreaId = GetStateAreaDefault().Id;

			var response = ExplorerService.GetSiteSearchResults(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.SiteSearchResults;
		}

		/// <summary>
		/// Gets the job report.
		/// </summary>
    /// <param name="criteria">The criteria on which to search.</param>
		/// <returns></returns>
    public ExplorerJobReportView GetJobReport(ExplorerCriteria criteria)
		{
			var request = new JobReportRequest().Prepare(AppContext, Runtime.Settings);
			criteria.FetchOption = CriteriaBase.FetchOptions.Single;
      AppendMilitaryOccupationCodesToCriteria(criteria);

			request.Criteria = criteria;
			//new JobReportCriteria
			//                     {
			//                       FetchOption = CriteriaBase.FetchOptions.Single,
			//                       JobId = jobId,
			//                       StateAreaId = stateAreaId
			//                     };

			var response = ExplorerService.GetJobReport(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.JobReport;
		}

	  /// <summary>
	  /// Gets the job report.
	  /// </summary>
	  /// <param name="criteria">The report criteria.</param>
	  /// <param name="listSize">The list size.</param>
    /// <param name="refreshMilitaryOccupations">Whether to add a list of military occupation codes to the criteria based on the military occupation ID</param>
	  /// <returns></returns>
	  public List<ExplorerJobReportView> GetJobReport(ExplorerCriteria criteria, int listSize, bool refreshMilitaryOccupations = true)
		{
			var request = new JobReportRequest().Prepare(AppContext, Runtime.Settings);

			criteria.FetchOption = CriteriaBase.FetchOptions.List;
			criteria.ListSize = listSize;
      if (refreshMilitaryOccupations || criteria.CareerAreaMilitaryOccupationROnetCodes.IsNullOrEmpty())
        AppendMilitaryOccupationCodesToCriteria(criteria);

			request.Criteria = criteria;
			//new JobReportCriteria
			//                    {FetchOption = CriteriaBase.FetchOptions.List, ListSize = listSize, ReportCriteria = reportCriteria};

			var response = ExplorerService.GetJobReport(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.JobReports;
		}

		/// <summary>
		/// Gets the similar job report.
		/// </summary>
    /// <param name="criteria">The report criteria.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
    public List<ExplorerJobReportView> GetSimilarJobReport(ExplorerCriteria criteria, int listSize)
		{
			var request = new JobReportRequest().Prepare(AppContext, Runtime.Settings);
			criteria.FetchOption = CriteriaBase.FetchOptions.List;
			criteria.ListSize = listSize;

			//request.JobId = jobId;
			request.Criteria = criteria;
			//new JobReportCriteria
			//                  {FetchOption = CriteriaBase.FetchOptions.List, ListSize = listSize, ReportCriteria = reportCriteria};

			var response = ExplorerService.GetSimilarJobReport(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.JobReports;
		}

		/// <summary>
		/// Gets the job employers
		/// </summary>
    /// <param name="criteria">The criteria on which to search.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		public List<EmployerJobViewDto> GetJobEmployers(ExplorerCriteria criteria, int listSize)
		{
			var request = new JobEmployerRequest().Prepare(AppContext, Runtime.Settings);

			criteria.FetchOption = CriteriaBase.FetchOptions.List;
			criteria.ListSize = listSize;

			request.Criteria = criteria;

			var response = ExplorerService.GetJobEmployers(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Employers;
		}

		/// <summary>
		/// Gets the job skills.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public List<JobSkillViewDto> GetJobSkills(long jobId)
		{
			var request = new JobSkillRequest().Prepare(AppContext, Runtime.Settings);

			request.JobId = jobId;

			var response = ExplorerService.GetJobSkills(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Skills;
		}

		/// <summary>
		/// Gets the job degree certifications.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public JobDegreeCertificationModel GetJobDegreeCertifications(ExplorerCriteria criteria)
		{
			var request = new JobDegreeCertificationRequest().Prepare(AppContext, Runtime.Settings);

			request.Criteria = criteria;

			var response = ExplorerService.GetJobDegreeCertifications(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.JobDegreeCertifications;
		}

    /// <summary>
    /// Gets the job degree certifications.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <returns></returns>
    public JobDegreeCertificationModel GetJobDegreeReportCertifications(ExplorerCriteria criteria)
    {
      var request = new JobDegreeCertificationRequest().Prepare(AppContext, Runtime.Settings);

      request.Criteria = criteria;

      var response = ExplorerService.GetJobDegreeReportCertifications(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.JobDegreeCertifications;
    }

		/// <summary>
		/// Gets the career paths.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public JobCareerPathModel GetJobCareerPaths(long jobId)
		{
		  return GetJobCareerPaths(jobId, null);
		}

	  /// <summary>
	  /// Gets the career paths.
	  /// </summary>
	  /// <param name="jobId">The job id.</param>
	  /// <param name="stateAreaId">An id of the state area should job report details (i.e Hiring Demand) be required</param>
	  /// <returns></returns>
	  public JobCareerPathModel GetJobCareerPaths(long jobId, long? stateAreaId)
    {
      var request = new JobCareerPathRequest().Prepare(AppContext, Runtime.Settings);

      request.JobId = jobId;
	    request.HiringDemandStateAreaId = stateAreaId;

      var response = ExplorerService.GetJobCareerPaths(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.CareerPaths;
    }

		/// <summary>
		/// Gets the employer report.
		/// </summary>
		/// <param name="reportCriteria">The report criteria.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		public List<EmployerJobViewDto> GetEmployerReport(ExplorerCriteria reportCriteria, int listSize)
		{
			var request = new EmployerRequest().Prepare(AppContext, Runtime.Settings);

      AppendMilitaryOccupationCodesToCriteria(reportCriteria);
			request.Criteria = new EmployerReportCriteria { FetchOption = CriteriaBase.FetchOptions.List, ListSize = listSize, ReportCriteria = reportCriteria };

			var response = ExplorerService.GetEmployerReport(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.EmployerReports;
		}

		/// <summary>
		/// Gets the employer.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		public EmployerDto GetEmployer(long employerId)
		{
			var request = new EmployerRequest().Prepare(AppContext, Runtime.Settings);

			request.EmployerId = employerId;

			var response = ExplorerService.GetEmployer(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Employer;
		}

		/// <summary>
		/// Gets the employer jobs.
		/// </summary>
    /// <param name="criteria">The criteria on which to search.</param>
		/// <returns></returns>
    public List<ExplorerEmployerJobReportView> GetEmployerJobs(ExplorerCriteria criteria)
		{
			var request = new EmployerJobRequest().Prepare(AppContext, Runtime.Settings);

			criteria.FetchOption = CriteriaBase.FetchOptions.List;
			criteria.ListSize = 15;
      AppendMilitaryOccupationCodesToCriteria(criteria);

			request.Criteria = criteria;

			var response = ExplorerService.GetEmployerJobs(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.EmployerJobs;
		}

		/// <summary>
		/// Gets the employer skills.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		public List<SkillModel> GetEmployerSkills(ExplorerCriteria criteria, int listSize)
		{
			var request = new EmployerSkillRequest().Prepare(AppContext, Runtime.Settings);
			criteria.ListSize = listSize;
			criteria.FetchOption = CriteriaBase.FetchOptions.List;
			request.Criteria = criteria;

			var response = ExplorerService.GetEmployerSkills(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.EmployerSkills;
		}

    /// <summary>
    /// Gets the school's departments.
    /// </summary>
    /// <returns>A list of school departments</returns>
    public List<ProgramAreaDto> GetProgramAreas()
    {
      var request = new ProgramAreaRequest().Prepare(AppContext, Runtime.Settings);

      var response = ExplorerService.GetProgramAreas(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ProgramAreas;
    }

    /// <summary>
    /// Gets the school's departments.
    /// </summary>
    /// <returns>A list of school departments</returns>
    public ProgramAreaDto GetProgramArea(long programAreaId)
    {
      var request = new ProgramAreaRequest().Prepare(AppContext, Runtime.Settings);
      request.ProgramAreaId = programAreaId;

      var response = ExplorerService.GetProgramAreas(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ProgramArea;
    }

    /// <summary>
    /// Gets the degrees for a department
    /// </summary>
    /// <returns>A list of school departments</returns>
    public DegreeDto GetDegree(long? degreeId)
    {
      var request = new DegreeRequest().Prepare(AppContext, Runtime.Settings);
      request.DegreeId = degreeId;

      var response = ExplorerService.GetDegrees(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.Degree;
    }

    /// <summary>
    /// Gets the degrees for a department
    /// </summary>
    /// <param name="programAreaId">Whether to restrict to a specific program area</param>
    /// <returns>A list of school departments</returns>
    public List<ProgramAreaDegreeViewDto> GetProgramAreaDegrees(long? programAreaId)
    {
      var request = new ProgramAreaRequest().Prepare(AppContext, Runtime.Settings);
      if (programAreaId.HasValue)
        request.ProgramAreaId = programAreaId;

      var response = ExplorerService.GetProgramAreaDegrees(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ProgramAreaDegrees;
    }

    /// <summary>
    /// Gets the degrees for a department
    /// </summary>
    /// <returns>A list of school departments</returns>
    public List<ProgramAreaDegreeViewDto> GetProgramAreaDegrees()
    {
      return GetProgramAreaDegrees(null);
    }

    /// <summary>
    /// Gets the degree education levels for a department
    /// </summary>
    /// <param name="programAreaId">Whether to restrict to a specific program area</param>
    /// <param name="criteria">The criteria.</param>
    /// <param name="degreeLevel">The level of degree</param>
    /// <param name="checkDegreeLevelByJob">Whether to check Degree Level criteria by looking at JobDegreeLevel records</param>
    /// <returns>
    /// A list of degree education levels
    /// </returns>
    /// <exception cref="ServiceCallException"></exception>
    public List<ProgramAreaDegreeEducationLevelViewDto> GetProgramAreaDegreeEducationLevels(long? programAreaId, ExplorerCriteria criteria = null, DetailedDegreeLevels? degreeLevel = null, bool? checkDegreeLevelByJob = null)
    {
      var request = new ProgramAreaRequest().Prepare(AppContext, Runtime.Settings);

      request.GetEducationLevels = true;
      if (programAreaId.HasValue)
        request.ProgramAreaId = programAreaId;

      if (degreeLevel.HasValue)
        request.DetailedDegreeLevel = degreeLevel;

      if (checkDegreeLevelByJob.HasValue)
        request.CheckDegreeLevelByJob = checkDegreeLevelByJob;

      request.Criteria = criteria;

      var response = ExplorerService.GetProgramAreaDegrees(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ProgramAreaDegreeEducationLevels;
    }

    public List<ProgramAreaDegreeEducationLevelViewDto> GetROnetProgramAreaDegreeEducationLevels(long ronetId)
    {
      var request = new ProgramAreaRequest().Prepare(AppContext, Runtime.Settings);

      request.Criteria = new ExplorerCriteria {ROnetId = ronetId};

      var response = ExplorerService.GetROnetProgramAreaDegrees(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ProgramAreaDegreeEducationLevels;
    }

    /// <summary>
    /// Gets the degree education levels for a department
    /// </summary>
    /// <returns>A list of degree education levels</returns>
    public List<ProgramAreaDegreeEducationLevelViewDto> GetProgramAreaDegreeEducationLevels()
    {
      return GetProgramAreaDegreeEducationLevels(null);
    }

		/// <summary>
		/// Gets the degree education level report.
		/// </summary>
		/// <param name="reportCriteria">The report criteria.</param>
		/// <param name="degreeLevel">The degree level.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		public List<JobDegreeEducationLevelReportViewDto> GetDegreeEducationLevelReport(ExplorerCriteria reportCriteria, DetailedDegreeLevels degreeLevel, int listSize)
		{
			var request = new DegreeEducationLevelRequest { SearchCriteria = new DegreeEducationLevelCriteria { DetailedDegreeLevel = degreeLevel } }.Prepare(AppContext, Runtime.Settings);

      AppendMilitaryOccupationCodesToCriteria(reportCriteria);
			request.ReportCriteria = new DegreeEducationLevelReportCriteria { FetchOption = CriteriaBase.FetchOptions.List, ListSize = listSize, ReportCriteria = reportCriteria };

			var response = ExplorerService.GetDegreeEducationLevelReport(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

		  return response.DegreeEducationLevelReports;
		}

		/// <summary>
		/// Gets the degree education level.
		/// </summary>
		/// <param name="degreeEducationLevelId">The degree education level id.</param>
		/// <returns></returns>
		public DegreeEducationLevelViewDto GetDegreeEducationLevel(long degreeEducationLevelId)
		{
			var request = new DegreeEducationLevelRequest().Prepare(AppContext, Runtime.Settings);

			request.SearchCriteria = new DegreeEducationLevelCriteria {DegreeEducationLevelId = degreeEducationLevelId };

			var response = ExplorerService.GetDegreeEducationLevel(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.DegreeEducationLevel;
		}

    /// <summary>
    /// Gets the degree education level job report.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="listSize">The list size.</param>
    /// <param name="jobDegreeTierLevel">The job degree tier level.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public List<ExplorerJobReportView> GetDegreeEducationLevelJobReport(ExplorerCriteria criteria, int listSize, int jobDegreeTierLevel)
		{
      var request = new JobReportRequest { JobDegreeTierLevel = jobDegreeTierLevel }.Prepare(AppContext, Runtime.Settings);

		  AppendMilitaryOccupationCodesToCriteria(criteria);

			criteria.FetchOption = CriteriaBase.FetchOptions.List;
			criteria.ListSize = listSize;
			request.Criteria = criteria;

			var response = ExplorerService.GetDegreeEducationLevelJobReport(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.JobReports;
		}

		/// <summary>
		/// Get the degree education level skills.
		/// </summary>
		/// <param name="degreeEducationLevelId">The degree education level id.</param>
		/// <param name="stateAreaId">The state area id.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		public List<SkillModel> GetDegreeEducationLevelSkills(long degreeEducationLevelId, long stateAreaId, int listSize)
		{
			var request = new DegreeEducationLevelSkillRequest().Prepare(AppContext, Runtime.Settings);

			request.Criteria = new DegreeEducationLevelSkillCriteria
			{
				FetchOption = CriteriaBase.FetchOptions.List,
				ListSize = listSize,
				DegreeEducationLevelId = degreeEducationLevelId,
				StateAreaId = stateAreaId
			};

			var response = ExplorerService.GetDegreeEducationLevelSkills(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.DegreeEducationLevelSkills;
		}

		/// <summary>
		/// Gets the degree education level employers.
		/// </summary>
    /// <param name="criteria">The criteria on which to search.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		public List<EmployerJobViewDto> GetDegreeEducationLevelEmployers(ExplorerCriteria criteria, int listSize)
		{
			var request = new DegreeEducationLevelEmployerRequest().Prepare(AppContext, Runtime.Settings);

			criteria.ListSize = listSize;
			criteria.FetchOption = CriteriaBase.FetchOptions.List;
      AppendMilitaryOccupationCodesToCriteria(criteria);

			request.Criteria = criteria;

			var response = ExplorerService.GetDegreeEducationLevelEmployers(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Employers;
		}

		/// <summary>
		/// Gets the degree education level study places.
		/// </summary>
		/// <param name="degreeEducationLevelId">The degree education level id.</param>
		/// <param name="stateAreaId">The state area id.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		public List<StudyPlaceDegreeEducationLevelViewDto> GetDegreeEducationLevelStudyPlaces(long degreeEducationLevelId, long stateAreaId, int listSize)
		{
			var request = new DegreeEducationLevelStudyPlaceRequest().Prepare(AppContext, Runtime.Settings);

			request.Criteria = new DegreeEducationLevelStudyPlaceCriteria
			{
				FetchOption = CriteriaBase.FetchOptions.List,
				ListSize = listSize,
				DegreeEducationLevelId = degreeEducationLevelId,
				StateAreaId = stateAreaId
			};

			var response = ExplorerService.GetDegreeEducationLevelStudyPlaces(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.DegreeEducationLevelStudyPlaces;
		}

		/// <summary>
		/// Gets the degree education level exts.
		/// </summary>
		/// <param name="degreeEducationLevelId">The degree education level id.</param>
		/// <returns></returns>
		public List<DegreeEducationLevelExtDto> GetDegreeEducationLevelExts(long degreeEducationLevelId)
		{
			var request = new DegreeEducationLevelRequest().Prepare(AppContext, Runtime.Settings);

			request.SearchCriteria = new DegreeEducationLevelCriteria {DegreeEducationLevelId = degreeEducationLevelId};

			var response = ExplorerService.GetDegreeEducationLevelExts(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.DegreeEducationLevelExts;
		}

		/// <summary>
		/// Gets the skill report.
		/// </summary>
		/// <param name="reportCriteria">The report criteria.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		public List<SkillModel> GetSkillReport(ExplorerCriteria reportCriteria, int listSize)
		{
			var request = new SkillRequest().Prepare(AppContext, Runtime.Settings);
      AppendMilitaryOccupationCodesToCriteria(reportCriteria);

			request.Criteria = new SkillReportCriteria { FetchOption = CriteriaBase.FetchOptions.List, ListSize = listSize, ReportCriteria = reportCriteria };

			var response = ExplorerService.GetSkillReport(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.SkillReports;
		}

		/// <summary>
		/// Gets the skill.
		/// </summary>
		/// <param name="skillId">The skill id.</param>
		/// <returns></returns>
		public SkillDto GetSkill(long skillId)
		{
			var request = new SkillRequest().Prepare(AppContext, Runtime.Settings);

			request.SkillId = skillId;

			var response = ExplorerService.GetSkill(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Skill;
		}

	  /// <summary>
	  /// Gets the skill job report
	  /// </summary>
	  /// <param name="criteria">The report criteria.</param>
	  /// <param name="listSize">The list size.</param>
	  /// <param name="maximumRank">The maximum rank allowed for the jobskill</param>
	  /// <returns></returns>
	  public List<ExplorerJobReportView> GetSkillJobReport(ExplorerCriteria criteria, int listSize, int? maximumRank = 20)
		{
			var request = new JobReportRequest{ MaximumRank = maximumRank }.Prepare(AppContext, Runtime.Settings);

			criteria.FetchOption = CriteriaBase.FetchOptions.List;
			criteria.ListSize = listSize;
      AppendMilitaryOccupationCodesToCriteria(criteria);

			request.Criteria = criteria;

			var response = ExplorerService.GetSkillJobReport(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.JobReports;
		}

		/// <summary>
		/// Gets the skill employers.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		public List<EmployerJobViewDto> GetSkillEmployers(ExplorerCriteria criteria, int listSize)
		{
			var request = new SkillEmployerRequest().Prepare(AppContext, Runtime.Settings);
			criteria.FetchOption = CriteriaBase.FetchOptions.List;
			criteria.PageSize = listSize;
      AppendMilitaryOccupationCodesToCriteria(criteria);

			request.Criteria = criteria;

			var response = ExplorerService.GetSkillEmployers(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.SkillEmployers;
		}

		/// <summary>
		/// Gets the job job titles.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public List<JobTitleDto> GetJobJobTitles(long jobId)
		{
			var request = new JobTitleRequest().Prepare(AppContext, Runtime.Settings);

			request.JobId = jobId;

			var response = ExplorerService.GetJobJobTitles(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.JobTitles;
		}

		/// <summary>
		/// Gets the job education requirements.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public List<JobEducationRequirementDto> GetJobEducationRequirements(long jobId)
		{
			var request = new JobEducationRequirementRequest().Prepare(AppContext, Runtime.Settings);

			request.JobId = jobId;

			var response = ExplorerService.GetJobEducationRequirements(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.JobEducationRequirements;
		}

		/// <summary>
		/// Gets the job experience levels.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		public List<JobExperienceLevelDto> GetJobExperienceLevels(long jobId)
		{
			var request = new JobExperienceLevelRequest().Prepare(AppContext, Runtime.Settings);

			request.JobId = jobId;

			var response = ExplorerService.GetJobExperienceLevels(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.JobExperienceLevels;
		}

		/// <summary>
		/// Sends an email.
		/// </summary>
		/// <param name="reportCriteria">The report criteria.</param>
		/// <param name="emailAddresses">The email addresses.</param>
		/// <param name="emailSubject">The email subject.</param>
		/// <param name="emailBody">The email body.</param>
		/// <param name="bookmarkUrl">The bookmark url.</param>
		public void SendEmail(ExplorerCriteria reportCriteria, string emailAddresses, string emailSubject, string emailBody, string bookmarkUrl)
		{
			var request = new SendEmailRequest().Prepare(AppContext, Runtime.Settings);

			request.ReportCriteria = reportCriteria;
			request.EmailAddresses = emailAddresses;
			request.EmailSubject = emailSubject;
			request.EmailBody = emailBody;
			request.BookmarkUrl = bookmarkUrl;

			var response = ExplorerService.SendEmail(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);
		}

		/// <summary>
		/// Sends a help email.
		/// </summary>
		/// <param name="emailSubject">The email subject.</param>
		/// <param name="emailBody">The email body.</param>
		public void SendHelpEmail(string emailSubject, string emailBody)
		{
			var request = new SendEmailRequest().Prepare(AppContext, Runtime.Settings);

			request.EmailSubject = emailSubject;
			request.EmailBody = emailBody;

			var response = ExplorerService.SendHelpEmail(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);
		}

		/// <summary>
		/// Gets my account model.
		/// </summary>
		/// <returns></returns>
		public MyAccountModel GetMyAccountModel()
		{
			var request = new MyAccountRequest().Prepare(AppContext, Runtime.Settings);

			var response = ExplorerService.GetMyAccountModel(request);

			if (response.Acknowledgement == AcknowledgementType.Failure || response.Model.IsNull())
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Model;
		}

		/// <summary>
		/// Gets the my resume model.
		/// </summary>
		/// <returns></returns>
		public MyResumeModel GetMyResumeModel()
		{
			var request = new MyResumeRequest().Prepare(AppContext, Runtime.Settings);

			var response = ExplorerService.GetMyResumeModel(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Model;
		}

		/// <summary>
		/// Gets the internship report.
		/// </summary>
		/// <param name="reportCriteria">The report criteria.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		public List<InternshipReportViewDto> GetInternshipReport(ExplorerCriteria reportCriteria, int listSize)
		{
			var request = new InternshipRequest().Prepare(AppContext, Runtime.Settings);

			request.Criteria = new InternshipReportCriteria { FetchOption = CriteriaBase.FetchOptions.List, ListSize = listSize, ReportCriteria = reportCriteria };

			var response = ExplorerService.GetInternshipReport(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.InternshipReports;
		}

    /// <summary>
    /// Gets the employer internships.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="listSize">Size of the list.</param>
    /// <returns></returns>
    public List<InternshipReportViewDto> GetEmployerInternships(ExplorerCriteria criteria, int listSize)
    {
      criteria.ListSize = listSize;
      var request = new EmployerInternshipRequest {Criteria = criteria}.Prepare(AppContext, Runtime.Settings);
      var response = ExplorerService.GetEmployerInternships(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.Internships;
    }

	  /// <summary>
	  /// Gets the employer internships.
	  /// </summary>
	  /// <param name="criteria">The criteria.</param>
	  /// <param name="listSize">The number of records to return</param>
	  /// <returns></returns>
	  public List<InternshipReportViewDto> GetInternshipsBySkill(ExplorerCriteria criteria, int listSize)
    {
      criteria.ListSize = listSize;
      var request = new SkillInternshipsRequest { Criteria = criteria }.Prepare(AppContext, Runtime.Settings);
      var response = ExplorerService.GetInternshipsBySkill(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.Internships;
    }

		/// <summary>
		/// Gets the internship.
		/// </summary>
		/// <param name="internshipCategoryId">The internship category id.</param>
		/// <returns></returns>
		public InternshipCategoryDto GetInternship(long internshipCategoryId)
		{
			var request = new InternshipRequest().Prepare(AppContext, Runtime.Settings);

			request.InternshipCategoryId = internshipCategoryId;

			var response = ExplorerService.GetInternship(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Internship;
		}

		/// <summary>
		/// Gets the internship skills.
		/// </summary>
		/// <param name="internshipCategoryId">The internship category id.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		public List<SkillModel> GetInternshipSkills(long internshipCategoryId, int listSize)
		{
			var request = new InternshipSkillRequest().Prepare(AppContext, Runtime.Settings);

			request.Criteria = new InternshipSkillCriteria
			{
				FetchOption = CriteriaBase.FetchOptions.List,
				ListSize = listSize,
				InternshipCategoryId = internshipCategoryId
			};

			var response = ExplorerService.GetInternshipSkills(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Skills;
		}

		/// <summary>
		/// Gets the internship employers.
		/// </summary>
    /// <param name="criteria">The criteria on which to search.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		public List<EmployerJobViewDto> GetInternshipEmployers(ExplorerCriteria criteria, int listSize)
		{
			var request = new InternshipEmployerRequest().Prepare(AppContext, Runtime.Settings);

			criteria.ListSize = listSize;
			criteria.FetchOption = CriteriaBase.FetchOptions.List;
			request.Criteria = criteria;

			var response = ExplorerService.GetInternshipEmployers(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Employers;
		}

		/// <summary>
		/// Gets the internship jobs.
		/// </summary>
		/// <param name="internshipCategoryId">The internship category id.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		public List<InternshipJobTitleDto> GetInternshipJobs(long internshipCategoryId, int listSize)
		{
			var request = new InternshipJobRequest().Prepare(AppContext, Runtime.Settings);

			request.Criteria = new InternshipJobCriteria
			{
				FetchOption = CriteriaBase.FetchOptions.List,
				ListSize = listSize,
				InternshipCategoryId = internshipCategoryId
			};

			var response = ExplorerService.GetInternshipJobs(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Jobs;
		}

		/// <summary>
		/// Gets the internship categories.
		/// </summary>
		/// <returns></returns>
		public List<InternshipCategoryDto> GetInternshipCategories()
		{
			var request = new InternshipCategoryRequest().Prepare(AppContext, Runtime.Settings);

			var response = ExplorerService.GetInternshipCategories(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.InternshipCategories;
		}

		/// <summary>
		/// Gets the internship categories.
		/// </summary>
		/// <param name="internshipCategoryIds">The internship category ids.</param>
		/// <returns></returns>
		public List<InternshipCategoryDto> GetInternshipCategories(List<long> internshipCategoryIds)
		{
			var request = new InternshipCategoryRequest().Prepare(AppContext, Runtime.Settings);
			request.InternshipCategoryIds = internshipCategoryIds;

			var response = ExplorerService.GetInternshipCategories(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.InternshipCategories;
		}

		/// <summary>
		/// Uploads the resume.
		/// </summary>
		/// <param name="resumeModel">The resume model.</param>
		/// <returns></returns>
		public MyResumeModel UploadResume(ExplorerResumeModel resumeModel)
		{
			var request = new UploadResumeRequest().Prepare(AppContext, Runtime.Settings);

			request.ResumeModel = resumeModel;
			
			var response = ExplorerService.UploadResume(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.ResumeModel;
		}

    /// <summary>
    /// Gets the jobs related to a job title
    /// </summary>
    /// <param name="jobTitleId">The Id of the Job Title</param>
    /// <returns>A list of related jobs</returns>
		public List<JobDto> GetRelatedJobs(long jobTitleId)
		{
			var request = new RelatedJobsRequest { JobTitleId = jobTitleId }.Prepare(AppContext, Runtime.Settings);
			var response = ExplorerService.GetRelatedJobs(request);
			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);
			return response.Jobs;
		}

    /// <summary>
    /// Gets the jobs related to a  military occupation
    /// </summary>
    /// <param name="militaryOccupationId">The Id of the military occupation</param>
    /// <returns> list of related jobs</returns>
    public List<JobDto> GetJobsForMilitaryOccupation(long militaryOccupationId)
    {
      var rOnetCodes = GetROnetCodesFromMilitaryOccupation(militaryOccupationId);

      var request = new JobFromROnetRequest { ROnets = rOnetCodes }.Prepare(AppContext, Runtime.Settings);

      var response = ExplorerService.GetJobFromROnet(request);
      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.Jobs;
    }

		/// <summary>
		/// Gets the job id from R onet.
		/// </summary>
		/// <param name="ronet">The ronet.</param>
		/// <returns></returns>
		public long GetJobIdFromROnet(string ronet)
		{
			var request = new JobFromROnetRequest { ROnet = ronet }.Prepare(AppContext, Runtime.Settings);
			var response = ExplorerService.GetJobFromROnet(request);
			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);
		  return response.Job != null ? response.Job.Id.GetValueOrDefault(0) : 0;
		}

    /// <summary>
    /// Gets the Explorer job from ronet.
    /// </summary>
    /// <param name="rOnet">The ronet.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public JobDto GetJobFromROnet(string rOnet)
    {
      var request = new JobFromROnetRequest { ROnet = rOnet }.Prepare(AppContext, Runtime.Settings);
      var response = ExplorerService.GetJobFromROnet(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.Job;
    }

		/// <summary>
		/// Gets the R onet from resume.
		/// </summary>
		/// <param name="resumexml">The resumexml.</param>
		/// <returns></returns>
		public string GetROnetFromResume(string resumexml)
		{
			var request = new ROnetFromResumeRequest { ResumeXml = resumexml }.Prepare(AppContext, Runtime.Settings);
			var response = ExplorerService.GetROnetFromResume(request);
			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);
			return response.ROnet;
		}

		/// <summary>
		/// Converts the ronet to onet.
		/// </summary>
		/// <param name="rOnet">The R onet.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
    public List<OnetDetailsView> GetOnetFromROnet(string rOnet)
    {
      var request = new ROnetToOnetRequest { ROnet = rOnet }.Prepare(AppContext, Runtime.Settings);
      var response = ExplorerService.GetOnetFromROnet(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.OnetDetails;
    }

		/// <summary>
		/// Gets the R onets from job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
    public string GetROnetsFromJob(long jobId)
    {
      var request = new JobIdToROnetRequest {JobId = jobId}.Prepare(AppContext, Runtime.Settings);
      var response = ExplorerService.GetROnetsFromJob(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.ROnet;
    }

    /// <summary>
    /// Gets the SOCS for an Onet
    /// </summary>
    /// <param name="onetId">The Id of the Onet</param>
    /// <returns>A list of SOCs</returns>
    public List<SOCDto> GetSOCForOnet(long onetId)
    {
      var request = new OnetToSOCRequest
      {
        OnetId = onetId
      }.Prepare(AppContext, Runtime.Settings);

      var response = ExplorerService.GetSOCForOnet(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.SOCs;
    }

    /// <summary>
    /// Gets the job introduction.
    /// </summary>
    /// <param name="jobId">The job id.</param>
    /// <returns></returns>
    public string GetJobIntroduction(long jobId)
    {
      var request = new JobIntroductionRequest {JobId = jobId}.Prepare(AppContext, Runtime.Settings);
      var response = ExplorerService.GetJobIntroduction(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.JobDescription;
    }

    /// <summary>
    /// Users the logged in.
    /// </summary>
    /// <param name="authenticated">if set to <c>true</c> [authenticated].</param>
    /// <returns></returns>
    public bool UserLoggedIn(bool authenticated)
    {
      if (Runtime.Settings.Module == FocusModules.Explorer)
      {
        return authenticated;
      }
			return !AppContext.User.IsNull() && AppContext.User.IsAuthenticated;
    }

    /// <summary>
    /// Appends R Onet codes for a military occupation to the explorer criteria
    /// </summary>
    /// <param name="criteria">The explorer criteria with the military occupation</param>
    private void AppendMilitaryOccupationCodesToCriteria(ExplorerCriteria criteria)
    {
      if (criteria.CareerAreaMilitaryOccupationId.HasValue)
        criteria.CareerAreaMilitaryOccupationROnetCodes = GetROnetCodesFromMilitaryOccupation(criteria.CareerAreaMilitaryOccupationId.Value);
    }

	  /// <summary>
	  /// Gets the R Onet codes for a military occupation
	  /// </summary>
	  /// <param name="militaryOccupationId">The Id of the military occupation</param>
	  /// <param name="minimumStarRating">A minimum star rating for the onet codes</param>
	  /// <returns>A list of R Onet Codes</returns>
	  private List<string> GetROnetCodesFromMilitaryOccupation(long militaryOccupationId, int? minimumStarRating = 1)
    {
      var occupationClient = new OccupationClient(AppContext, Runtime);
			return occupationClient.GetMocOccupationCodes(militaryOccupationId)
                             .Where(o =>  o.StarRating >= minimumStarRating)
                             .Select(o => o.RonetCode)
                             .ToList();
    }
	}
}
