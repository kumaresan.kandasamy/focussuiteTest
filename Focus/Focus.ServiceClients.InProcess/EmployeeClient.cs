﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Employee;
using Focus.Core.Criteria.Employer;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Messages.EmployeeService;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Views;
using Focus.ServiceClients.InProcess.Extensions;
using Focus.ServiceClients.Interfaces;
using Focus.Web.Core.Models;
using Framework.Core;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.InProcess
{
	public class EmployeeClient : ClientBase, IEmployeeClient
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmployeeClient" /> class.
		/// </summary>
		/// <param name="appContext">The application context.</param>
		/// <param name="runtime">The runtime.</param>
		public EmployeeClient(AppContextModel appContext, ServiceClientRuntime runtime) : base(appContext, runtime) { }
		
		/// <summary>
		/// Gets the employee.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <returns></returns>
		public EmployeeDto GetEmployee(long employeeId)
		{
			var request = new EmployeeRequest().Prepare(AppContext, Runtime.Settings);
			request.Id = employeeId;

			var response = EmployeeService.GetEmployee(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Employee;
		}

		/// <summary>
		/// Gets the employee by person.
		/// </summary>
		/// <param name="personId">The employee id.</param>
		/// <returns></returns>
		public EmployeeDto GetEmployeeByPerson(long personId)
		{
			var request = new EmployeeRequest().Prepare(AppContext, Runtime.Settings);
			request.Id = personId;

			var response = EmployeeService.GetEmployeeByPerson(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Employee;
		}

		/// <summary>
		/// Gets the other employees.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <returns></returns>
		public List<long> GetSameEmployerEmployees(long employeeId)
		{
			var request = new EmployeeRequest().Prepare(AppContext, Runtime.Settings);
			request.Id = employeeId;

      var response = EmployeeService.GetEmployeesForSameEmployer(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.SameCompanyEmployeeIds;
		}

		/// <summary>
		/// Gets the hiring areas.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <returns></returns>
		public List<string> GetHiringAreas(long employeeId)
		{
			var request = new EmployeeRequest().Prepare(AppContext, Runtime.Settings);
			request.Id = employeeId;

			var response = EmployeeService.GetHiringAreas(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.HiringAreas;
		}

	  /// <summary>
    /// Gets all employees for a list of employer ids
    /// </summary>
    /// <param name="employerIds">The list of employer ids</param>
    /// <param name="pageIndex">The index of the page required.</param>
    /// <param name="pageSize">The size of each page.</param>
    /// <returns>A list of employees</returns>
    public PagedList<EmployeeSearchResultView> GetEmployeesForEmployers(List<long> employerIds, int pageIndex, int pageSize)
    {
      var request = new EmployeesForEmployerRequest().Prepare(AppContext, Runtime.Settings);
      request.Criteria = new EmployeeCriteria
      {
        EmployerIds = employerIds,
        PageIndex = pageIndex,
        PageSize = pageSize,
        FetchOption = CriteriaBase.FetchOptions.PagedList
      };

      var response = EmployeeService.GetEmployeesForEmployers(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.PagedEmployees;
    }

		/// <summary>
		/// Gets all employees for a list of businuess unit ids
		/// </summary>
		/// <param name="businessUnitIds">The list of business units ids</param>
		/// <param name="pageIndex">The index of the page required.</param>
		/// <param name="pageSize">The size of each page.</param>
		/// <param name="subscribedOnly">if set to <c>true</c> [subscribed only].</param>
		/// <returns>
		/// A list of employees
		/// </returns>
		/// <exception cref="ServiceCallException"></exception>
    public PagedList<EmployeeSearchResultView> GetEmployeesForBusinessUnits(List<long> businessUnitIds, int pageIndex, int pageSize, bool subscribedOnly = false)
    {
      var request = new EmployeesForEmployerRequest().Prepare(AppContext, Runtime.Settings);
      request.Criteria = new EmployeeCriteria
      {
        BusinessUnitIds = businessUnitIds,
        PageIndex = pageIndex,
        PageSize = pageSize,
        FetchOption = CriteriaBase.FetchOptions.PagedList,
				SubscribedOnly = subscribedOnly
      };

      var response = EmployeeService.GetEmployeesForEmployers(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.PagedEmployees;
    }

    /// <summary>
    /// Gets all employees for a list of businuess unit ids
    /// </summary>
    /// <param name="businessUnitIds">The list of business units ids</param>
    /// <returns>A list of employees</returns>
    public List<EmployeeSearchResultView> GetEmployeesForBusinessUnits(List<long> businessUnitIds)
    {
      var request = new EmployeesForEmployerRequest().Prepare(AppContext, Runtime.Settings);
      request.Criteria = new EmployeeCriteria
      {
        BusinessUnitIds = businessUnitIds,
        FetchOption = CriteriaBase.FetchOptions.Count
      };

      var response = EmployeeService.GetEmployeesForEmployers(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.Employees;
    }

    /// <summary>
    /// Gets count of all employees for a list of businuess unit ids
    /// </summary>
    /// <param name="businessUnitIds">The list of business units ids</param>
    /// <returns>A count of employees</returns>
    public long GetEmployeeCountForBusinessUnits(List<long> businessUnitIds)
    {
      var request = new EmployeesForEmployerRequest().Prepare(AppContext, Runtime.Settings);
      request.Criteria = new EmployeeCriteria
      {
        BusinessUnitIds = businessUnitIds,
        FetchOption = CriteriaBase.FetchOptions.Count
      };

      var response = EmployeeService.GetEmployeesForEmployers(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);

      return response.EmployeesCount;
    }

		/// <summary>
		/// Searches the employees.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<EmployeeSearchResultView> SearchEmployees(EmployeeCriteria criteria)
		{
			var request = new EmployeeSearchRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = criteria;

			var response = EmployeeService.Search(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.EmployeeSearchViewsPaged;
		}

		/// <summary>
		/// Emails the employee.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="emailSubject">The email subject.</param>
		/// <param name="emailBody">The email body.</param>
		/// <param name="bccSelf">if set to <c>true</c> [BCC self].</param>
		/// <param name="senderAddress">Sender address</param>
		/// <param name="sendCopy">if set to <c>true</c> [send copy].</param>
		/// <exception cref="ServiceCallException"></exception>
		public void EmailEmployee(long employeeId, string emailSubject, string emailBody, bool bccSelf = false, string senderAddress = null, bool sendCopy = false, EmailFooterTypes footerType = EmailFooterTypes.None, string footerUrl = null)
		{
			var request = new EmailEmployeeRequest().Prepare(AppContext, Runtime.Settings);
			request.EmployeeId = employeeId;
			request.EmailSubject = emailSubject;
			request.EmailBody = emailBody;
			request.BccSelf = bccSelf;
			request.From = senderAddress;
			request.SendCopy = sendCopy;
			request.FooterType = footerType;
			request.FooterUrl = footerUrl;

			var response = EmployeeService.EmailEmployee(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);
		}

        /// <summary>
        /// email to employers
        /// </summary>
        /// <param name="employeeIds">list of ids</param>
        /// <param name="emailSubject">email subject</param>
        /// <param name="emailBody">email body</param>
        /// <param name="bccSelf">BCC list</param>
        /// <param name="senderAddress">Sender Address</param>
        /// <param name="sendCopy">if set to <c>true</c> [send copy].</param>
        /// <param name="footerType">Footer Type</param>
        /// <param name="footerUrl">Footer URL</param>
        public void EmailEmployees(List<long> employeeIds, string emailSubject, string emailBody, bool bccSelf = false, string senderAddress = null, bool sendCopy = false, EmailFooterTypes footerType = EmailFooterTypes.None, string footerUrl = null)
        {
            var request = new EmailEmployeesRequest().Prepare(AppContext, Runtime.Settings);
            request.EmployeeIds = employeeIds;
            request.EmailSubject = emailSubject;
            request.EmailBody = emailBody;
            request.BccSelf = bccSelf;
            request.From = senderAddress;
            request.SendCopy = sendCopy;
            request.FooterType = footerType;
            request.FooterUrl = footerUrl;
            var response = EmployeeService.EmailEmployees(request);

            if (response.Acknowledgement == AcknowledgementType.Failure)
                throw new ServiceCallException(response.Message, (int)response.Error);
        }
    /// <summary>
    /// Emails the employee with an attachment
    /// </summary>
    /// <param name="employeeId">The employee id.</param>
    /// <param name="emailSubject">The email subject.</param>
    /// <param name="emailBody">The email body.</param>
    /// <param name="attachment">The attachment data</param>
    /// <param name="attachmentName">The name of the attachment</param>
    /// <param name="bccSelf">if set to <c>true</c> [BCC self].</param>
    public void EmailEmployeeWithAttachment(long employeeId, string emailSubject, string emailBody, byte[] attachment, string attachmentName, bool bccSelf = false)
    {
      var request = new EmailEmployeeRequest().Prepare(AppContext, Runtime.Settings);
      request.EmployeeId = employeeId;
      request.EmailSubject = emailSubject;
      request.EmailBody = emailBody;
      request.BccSelf = bccSelf;
      request.Attachment = attachment;
      request.AttachmentName = attachmentName;

      var response = EmployeeService.EmailEmployee(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error);
    }

		/// <summary>
		/// Gets the employer account referrals.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<EmployerAccountReferralViewDto> GetEmployerAccountReferrals(EmployerAccountReferralCriteria criteria)
		{
			var request = new EmployerAccountReferralViewRequest().Prepare(AppContext, Runtime.Settings);

			long employerTypeId;

			if (criteria.EmployerType.IsNotNullOrEmpty() && !long.TryParse(criteria.EmployerType, out employerTypeId))
			{
				var coreClient = new CoreClient(AppContext, Runtime);
				var lookup = coreClient.GetLookup(LookupTypes.States).FirstOrDefault(x => x.Key == Runtime.Settings.DefaultStateKey);
				
				if(lookup.IsNotNull())
					criteria.DefaultStateId = lookup.Id;
			}

			request.Criteria = criteria;

			var response = EmployeeService.GetEmployerAccountReferralView(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.ReferralViewsPaged;
		}

		/// <summary>
		/// Gets the employer account referral.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <returns></returns>
		public EmployerAccountReferralViewDto GetEmployerAccountReferral(long employeeId)
		{
			var request = new EmployerAccountReferralViewRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new EmployerAccountReferralCriteria
			{
				Id = employeeId,
				FetchOption = CriteriaBase.FetchOptions.Single
			};
			
			var response = EmployeeService.GetEmployerAccountReferralView(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.Referral;
		}

		/// <summary>
		/// Approves the employer account referral.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="businessUnitId"></param>
		/// <param name="lockVersion">The lock version.</param>
		/// <param name="employerApprovalType">approval type</param>
		public ApproveReferralResponseType ApproveEmployerAccountReferral(long employeeId, long businessUnitId, string lockVersion, ApprovalType employerApprovalType = ApprovalType.Employer)
		{
			var request = new EmployeeRequest().Prepare(AppContext, Runtime.Settings);
			request.EmployeeId = employeeId;
			request.BusinessUnitId = businessUnitId;
			request.EmployerApprovalType = employerApprovalType;
			request.LockVersion = lockVersion;
			
			var response = EmployeeService.ApproveReferral(request);


			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

	    return response.ReferralResponseType;
		}

		/// <summary>
		/// Denies the employer account referral.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <param name="lockVersion">The lock version.</param>
		/// <param name="employerApprovalType">Type of the employer approval.</param>
		/// <param name="approvalDeniedReasons">The approval denied reasons.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void DenyEmployerAccountReferral(long employeeId, long businessUnitId, string lockVersion, ApprovalType employerApprovalType = ApprovalType.Employer, List<KeyValuePair<long, string>> approvalDeniedReasons = null )
		{
			var request = new EmployeeRequest().Prepare(AppContext, Runtime.Settings);
			request.EmployeeId = employeeId;
			request.BusinessUnitId = businessUnitId;
			request.EmployerApprovalType = employerApprovalType;
			request.LockVersion = lockVersion;
			request.ReferralStatusReasons = approvalDeniedReasons;
			
			var response = EmployeeService.DenyReferral(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);
		}

		/// <summary>
		/// Holds the employer account referral.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="businessUnitId"></param>
		/// <param name="lockVersion">The lock version.</param>
		public void HoldEmployerAccountReferral( long employeeId, long businessUnitId, string lockVersion, ApprovalType employerApprovalType = ApprovalType.Employer, List<KeyValuePair<long, string>> approvalHoldReasons = null )
		{
			var request = new EmployeeRequest().Prepare(AppContext, Runtime.Settings);
			request.EmployeeId = employeeId;
			request.BusinessUnitId = businessUnitId;
			request.EmployerApprovalType = employerApprovalType;
			request.LockVersion = lockVersion;
			request.ReferralStatusReasons = approvalHoldReasons;

			var response = EmployeeService.HoldReferral(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);
		}

	  /// <summary>
	  /// Assigns the activity.
	  /// </summary>
	  /// <param name="activityId">The activity id.</param>
	  /// <param name="employeeId">The employee id.</param>
	  /// <param name="actionedDate"></param>
	  public bool AssignActivity(long activityId, long employeeId,DateTime? actionedDate =null)
		{
			var request = new AssignEmployeeActivityRequest().Prepare(AppContext, Runtime.Settings);
			request.ActivityId = activityId;
			request.EmployeeId = employeeId;
	    request.ActionedDate = actionedDate;

			var response = EmployeeService.AssignActivity(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

            if (response.Message == "-1")
                return false;
            else
                return true;
		}

	  /// <summary>
	  /// Assigns the activity to multiple employees.
	  /// </summary>
	  /// <param name="activityId">The activity id.</param>
	  /// <param name="employeeIds">The employee ids.</param>
	  /// <param name="actionedDate"></param>
	  /// <exception cref="ServiceCallException"></exception>
	  public bool AssignActivityToMultipleEmployees(long activityId, List<long> employeeIds,DateTime? actionedDate = null)
		{
			var request = new AssignEmployeeActivityRequest().Prepare(AppContext, Runtime.Settings);
			request.ActivityId = activityId;
			request.EmployeeIds = employeeIds;
		  request.ActionedDate = actionedDate;

			var response = EmployeeService.AssignActivityToMultipleEmployees(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);
            
          if (response.Message == "-1")
                return false;
            else
                return true;
		}

		/// <summary>
		/// Blocks the account.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		public void BlockAccount(long employeeId)
		{
			var request = new EmployeeRequest().Prepare(AppContext, Runtime.Settings);
			request.Id = employeeId;

			var response = EmployeeService.BlockAccount(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);
		}

		/// <summary>
		/// Unblocks the account.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		public void UnblockAccount(long employeeId)
		{
			var request = new EmployeeRequest().Prepare(AppContext, Runtime.Settings);
			request.Id = employeeId;

			var response = EmployeeService.UnblockAccount(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);
		}

		/// <summary>
		/// Gets the user details.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <returns></returns>
		public UserDetailsModel GetUserDetails(long employeeId)
		{
			var request = new EmployeeUserDetailsRequest().Prepare(AppContext, Runtime.Settings);
			request.EmployeeId = employeeId;

			var response = EmployeeService.GetEmployeeUserDetails(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return new UserDetailsModel
			{
				UserId = response.UserId,
				PersonDetails = response.PersonDetails,
				PrimaryPhoneNumber = response.PrimaryPhoneNumber,
				AlternatePhoneNumber1 = response.AlternatePhoneNumber1,
				AlternatePhoneNumber2 = response.AlternatePhoneNumber2,
				AddressDetails = response.AddressDetails
			};
		}

		/// <summary>
		/// Gets the employees business units.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <returns></returns>
		public List<EmployeeBusinessUnitView> GetEmployeesBusinessUnits(long employeeId)
		{
			var request = new EmployeeBusinessUnitViewRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new EmployeeBusinessUnitCriteria { EmployeeId = employeeId, FetchOption = CriteriaBase.FetchOptions.List };

			var response = EmployeeService.GetEmployeeBusinessUnitView(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.EmployeeBusinessUnitViews;
		}

		/// <summary>
		/// Assigns the employee to business unit.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="businessUnitId">The business unit id.</param>
		public void AssignEmployeeToBusinessUnit(long employeeId, long businessUnitId)
		{
			var request = new AssignEmployeeToBusinessUnitRequest().Prepare(AppContext, Runtime.Settings);
			request.EmployeeId = employeeId;
			request.BusinessUnitId = businessUnitId;

			var response = EmployeeService.AssignEmployeeToBusinessUnit(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
		}

		/// <summary>
		/// Sets the business unit as default.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="businessUnitId">The business unit id.</param>
		public void SetBusinessUnitAsDefault(long employeeId, long businessUnitId)
		{
			var request = new SetBusinessUnitAsDefaultRequest().Prepare(AppContext, Runtime.Settings);
			request.EmployeeId = employeeId;
			request.BusinessUnitId = businessUnitId;

			var response = EmployeeService.SetBusinessUnitAsDefault(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
		}

		/// <summary>
		/// Gets the hiring manager profile.
		/// </summary>
		/// <param name="employeebusinessunitId">The employeebusinessunit id.</param>
		/// <returns></returns>
		public HiringManagerProfileModel GetHiringManagerProfile(long employeebusinessunitId)
		{
			var request = new EmployeeBusinessUnitInfoRequest().Prepare(AppContext, Runtime.Settings);
			request.Id = employeebusinessunitId;

			var response = EmployeeService.GetHiringManagerProfile(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return new HiringManagerProfileModel {
																			EmployeeName = response.EmployeeName,
																			EmployeeId = response.EmployeeId,
																			EmployerId = response.EmployerId,
																			EmployeeEmail = response.EmployeeEmail,
																			EmployeePhoneNumber = response.EmployeePhoneNumber,
																			BusinessUnitId = response.BusinessUnitId,
																			BusinessUnitName = response.BusinessUnitName,
																			BusinessUnit = response.BusinessUnit,
                                      BusinessUnitAddresses = response.BusinessUnitAddresses,
																			DaysSinceRegistration = response.DaysSinceRegistration,
																			LoginsSinceRegistration = response.LoginsSinceRegistration,
																			Logins7Days = response.Logins7Days,
																			LastLogin = response.LastLogin,
																			ApplicantsDidNotApply = response.ApplicantsDidNotApply,
																			ApplicantsFailedToShow = response.ApplicantsFailedToShow,
																			ApplicantsHired = response.ApplicantsHired,
																			ApplicantsInterviewDenied = response.ApplicantsInterviewDenied,
																			ApplicantsInterviewed = response.ApplicantsInterviewed,
																			ApplicantsRefused = response.ApplicantsRefused,
																			ApplicantsRejected = response.ApplicantsRejected,
																			ApplicantsFailedToRespondToInvitation = response.ApplicantsFailedToRespondToInvitation,
																			ApplicantsFoundJobFromOtherSource = response.ApplicantsFoundJobFromOtherSource,
																			ApplicantsJobAlreadyFilled = response.ApplicantsJobAlreadyFilled,
																			ApplicantsNewApplicant = response.ApplicantsNewApplicant,
																			ApplicantsNotQualified = response.ApplicantsNotQualified,
																			ApplicantsNotYetPlaced = response.ApplicantsNotYetPlaced,
																			ApplicantsRecommended = response.ApplicantsRecommended,
																			ApplicantsRefusedReferral = response.ApplicantsRefusedReferral,
																			ApplicantsUnderConsideration = response.ApplicantsUnderConsideration,
																			SurveyDidNotHire = response.SurveyDidNotHire,
																			SurveyDidNotInterview = response.SurveyDidNotInterview,
																			SurveyVeryDissatified = response.SurveyVeryDissatified,
																			SurveySomewhatDissatified = response.SurveySomewhatDissatified,
																			SurveyHired = response.SurveyHired,
																			SurveyInterviewed = response.SurveyInterviewed,
																			SurveyVerySatisfied = response.SurveyVerySatisfied,
																			SurveySomewhatSatisfied = response.SurveySomewhatSatisfied,
																			ApplicantsSeven = response.ApplicantsSeven,
																			ApplicantsThirty = response.ApplicantsThirty,
																			InvitationsSentSeven = response.InvitationsSentSeven,
																			InvitationsSentThirty = response.InvitationsSentThirty,
																			InvitedSeekersViewedJobSeven = response.InvitedSeekersViewedJobSeven,
																			InvitedSeekersViewedJobThirty = response.InvitedSeekersViewedJobThirty,
																			InvitedSeekersNotViewJobSeven = response.InvitedSeekersNotViewJobSeven,
																			InvitedSeekersNotViewJobThirty = response.InvitedSeekersNotViewJobThirty,
																			InvitedSeekersClickedHowToApplySeven = response.InvitedSeekersClickedHowToApplySeven,
																			InvitedSeekersClickedHowToApplyThirty = response.InvitedSeekersClickedHowToApplyThirty,
																			InvitedSeekersNotClickHowToApplySeven = response.InvitedSeekersNotClickHowToApplySeven,
																			InvitedSeekersNotClickHowToApplyThirty = response.InvitedSeekersNotClickHowToApplyThirty
																		};

		}

		/// <summary>
		/// Gets the hiring manager activities.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<HiringManagerActivityViewDto> GetHiringManagerActivities(EmployeeBusinessUnitCriteria criteria)
		{
			var request = new EmployeeBusinessUnitActivityRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = criteria;

			var response = EmployeeService.GetHiringManagerActivityView(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);

			return response.EmployeeBusinessUnitActivityPaged;
		}

		/// <summary>
		/// Gets the employee business unit activity users.
		/// </summary>
		/// <param name="employeeBuId">The bu id.</param>
		/// <returns></returns>
		public List<HiringManagerActivityViewDto> GetEmployeeBusinessUnitActivityUsers(long employeeBuId)
		{
			var request = new EmployeeBusinessUnitActivityRequest { EmployeeBuId = employeeBuId }.Prepare(AppContext, Runtime.Settings);
			var response = EmployeeService.GetEmployeeBusinessUnitActivityUsers(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.EmployeeBusinessUnitActivityUsers;
		}

		/// <summary>
		/// Creates the homepage alert.
		/// </summary>
		/// <param name="homepageAlert">The homepage alert.</param>
		/// <exception cref="ServiceCallException"></exception>
		public void CreateHomepageAlert(HiringManagerHomepageAlertView homepageAlert)
		{
			var request = new CreateHiringManagerHomepageAlertRequest().Prepare(AppContext, Runtime.Settings);
			request.HomepageAlert = homepageAlert;

			var response = EmployeeService.CreateHiringManagerHomepageAlert(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error);
		}
	}
}