﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Resume;
using Focus.Core.Criteria.ResumeJob;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Messages.ResumeService;
using Focus.Core.Models.Career;
using Focus.ServiceClients.InProcess.Extensions;
using Focus.ServiceClients.Interfaces;
using Focus.Web.Core.Models;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.InProcess
{
	public class ResumeClient : ClientBase, IResumeClient
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ResumeClient" /> class.
		/// </summary>
		/// <param name="appContext">The application context.</param>
		/// <param name="runtime">The runtime.</param>
		public ResumeClient(AppContextModel appContext, ServiceClientRuntime runtime) : base(appContext, runtime) { }
		
		/// <summary>
		/// Gets the resume.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public ResumeModel GetResume(long? resumeId)
		{
			var request = new GetResumeRequest().Prepare(AppContext, Runtime.Settings);
			if (resumeId.HasValue)
				request.ResumeId = resumeId.Value;

			var response = ResumeService.GetResume(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.SeekerResume;
		}

		/// <summary>
		/// Gets the resume names.
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, string> GetResumeNames()
		{
			var request = new ResumeMetaInfoRequest().Prepare(AppContext, Runtime.Settings);

			if (request.UserContext.PersonId != null)
			{
				request.Criteria = new ResumeCriteria
				{
					PersonId = request.UserContext.PersonId,
					FetchOption = CriteriaBase.FetchOptions.Lookup
				};

				var response = ResumeService.GetResumeInfo(request);

				Correlate(request, response);

				if (response.Acknowledgement == AcknowledgementType.Failure)
					throw new ServiceCallException(response.Message, (int) response.Error, response.Exception);

				return response.ResumeInfoLookup;
			}
			else
			{
				return new Dictionary<string, string>();
			}
		}

    /// <summary>
    /// Gets the number of resumes
    /// </summary>
    /// <returns></returns>
    public int GetResumeCount()
    {
      var request = new ResumeMetaInfoRequest().Prepare(AppContext, Runtime.Settings);
      request.Criteria = new ResumeCriteria { PersonId = request.UserContext.PersonId, FetchOption = CriteriaBase.FetchOptions.Count };

      var response = ResumeService.GetResumeInfo(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.ResumeCount;
    }

    /// <summary>
		/// Gets the resume info.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public ResumeEnvelope GetResumeInfo(long? resumeId)
		{
			var request = new ResumeMetaInfoRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new ResumeCriteria {Id = resumeId, FetchOption = CriteriaBase.FetchOptions.Single};

			var response = ResumeService.GetResumeInfo(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.ResumeInfo;
		}

    /// <summary>
    /// Gets info on all resumes for a user
    /// </summary>
    /// <param name="personId">The resume id.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public List<ResumeEnvelope> GetAllResumeInfo(long? personId = null)
    {
      var request = new ResumeMetaInfoRequest().Prepare(AppContext, Runtime.Settings);
      request.Criteria = new ResumeCriteria
      {
        PersonId = personId ?? request.UserContext.PersonId, 
        FetchOption = CriteriaBase.FetchOptions.List
      };

      var response = ResumeService.GetResumeInfo(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.AllResumeInfo;
    }

    /// <summary>
    /// Gets the original uploaded resume.
    /// </summary>
    /// <param name="resumeId">The resume id.</param>
    /// <param name="fileName">Name of the file.</param>
    /// <param name="mimeType">The mimeType of the original resume.</param>
    /// <param name="htmlResume">The HTML version of the resume.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public byte[] GetOriginalUploadedResume(long resumeId, out string fileName, out string mimeType, out string htmlResume)
    {
      fileName = string.Empty;
      var request = new OriginalResumeRequest().Prepare(AppContext, Runtime.Settings);
      request.ResumeType = DocumentContentType.Binary;
      request.ResumeId = resumeId;

      var response = ResumeService.GetOriginalResume(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      fileName = response.FileName;
		  mimeType = response.MimeType;
      htmlResume = response.HTMLResume;

      return response.ResumeContent;
    }

		/// <summary>
		/// Gets the HTML formatted resume.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <returns></returns>
    public string GetHTMLFormattedResume(long resumeId)
    {
			var request = new OriginalResumeRequest().Prepare(AppContext, Runtime.Settings);
      request.ResumeId = resumeId;
      request.ResumeType = DocumentContentType.HTML;

      var response = ResumeService.GetOriginalResume(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

     return response.HTMLResume;
    }

    /// <summary>
    /// Re-Regisers the default resume for a person.
    /// </summary>
    /// <param name="personId">The id of the person (if set to null, it will assume the id of the currently logged on person.</param>
    public void RegisterDefaultResume(long? personId = null)
    {
      var request = new RegisterResumeRequest
      {
        PersonId = personId
      }.Prepare(AppContext, Runtime.Settings);

      var response = ResumeService.RegisterDefaultResume(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);
    }

	  /// <summary>
	  /// Saves the resume.
	  /// </summary>
	  /// <param name="resume">The resume.</param>
	  /// <param name="resumeNameUpdate">if set to <c>true</c> [resume name update].</param>
	  /// <param name="personId">when set overrides resume personId</param>
	  /// <returns></returns>
	  /// <exception cref="ServiceCallException"></exception>
	  public ResumeModel SaveResume(ResumeModel resume, bool resumeNameUpdate = false,long? personId = null)
		{
			var request = new SaveResumeRequest().Prepare(AppContext, Runtime.Settings);  
			request.SeekerResume = resume;

			if (personId.HasValue)
	      request.PersonId = personId;

			var response = ResumeService.SaveResume(request);


			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.SeekerResume;
		}

		/// <summary>
		/// Saves the resume document.
		/// </summary>
		/// <param name="resumeDocument">The resume document.</param>
		/// <returns></returns>
		public bool SaveResumeDocument(ResumeDocumentDto resumeDocument)
		{
			var request = new SaveResumeDocumentRequest().Prepare(AppContext, Runtime.Settings);
			request.ResumeDocument = resumeDocument;

			var response = ResumeService.SaveResumeDocument(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return (response.Acknowledgement == AcknowledgementType.Success);
    }

    /// <summary>
		/// Deletes the resume.
		/// </summary>
    /// <param name="resumeId">The resume id.</param>
    /// <param name="newDefaultId">The id of the resume to set as the new default.</param>
    /// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
    public bool DeleteResume(long resumeId, long? newDefaultId = null)
		{
			var request = new DeleteResumeRequest().Prepare(AppContext, Runtime.Settings);
			request.ResumeId = resumeId;
      request.NewDefaultResumeId = newDefaultId;

			var response = ResumeService.DeleteResume(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return (response.Acknowledgement == AcknowledgementType.Success);
		}

		/// <summary>
		/// Gets the snap shot.
		/// </summary>
		/// <param name="resume">The resume.</param>
    /// <param name="escapeText">Whether to escape (HTML Encode) the summary</param>
    /// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public string GetSnapShot(ResumeModel resume, bool escapeText)
		{
			var request = new SummaryRequest
			{
        SeekerResume = resume,
			  EscapeText = escapeText
			}.Prepare(AppContext, Runtime.Settings);

			var response = ResumeService.GetAutomatedSummary(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Summary;
		}

		/// <summary>
		/// Gets the tagged resume.
		/// </summary>
		/// <param name="resume">The resume.</param>
		/// <param name="canonize">if set to <c>true</c> [canonize].</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public string GetTaggedResume(ResumeModel resume, bool canonize = false)
		{
			var request = new ResumeConvertionRequest().Prepare(AppContext, Runtime.Settings);
			request.SeekerResume = resume;
			request.Canonize = canonize;

			var response = ResumeService.GetTaggedResume(request);

			Correlate(request, response);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.TaggedResume;
		}

    /// <summary>
    /// Gets the tagged resume.
    /// </summary>
    /// <param name="resumeBytes">The resume bytes.</param>
    /// <param name="fileExtension">The file extension.</param>
    /// <returns></returns>
    public string GetTaggedResume(byte[] resumeBytes, string fileExtension)
    {
      var request = new ResumeConvertionRequest().Prepare(AppContext, Runtime.Settings);
      request.ResumeBytes = resumeBytes;
      request.ResumeFileExtension = fileExtension;

      var response = ResumeService.GetTaggedResume(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.TaggedResume;
    }

		/// <summary>
		/// Gets the HTML resume.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <returns></returns>
    public string GetHTMLResume(long resumeId)
    {
      var request = new HTMLResumeRequest().Prepare(AppContext, Runtime.Settings);
      request.ResumeId = resumeId;

      var response = ResumeService.GetHTMLResume(request);

      Correlate(request, response);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.HTMLResume;
    }

		/// <summary>
		/// Gets the resume object.
		/// </summary>
		/// <param name="taggedResume">The tagged resume.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public ResumeModel GetResumeObject(string taggedResume)
		{
			var request = new ResumeConvertionRequest().Prepare(AppContext, Runtime.Settings);
			request.TaggedResume = taggedResume;

			var response = ResumeService.GetStructuredResume(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.SeekerResume;
		}

		/// <summary>
		/// Gets the distinct jobs.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public Dictionary<string, string> GetDistinctJobs(long resumeId)
		{
			var request = new ResumeJobRequest().Prepare(AppContext, Runtime.Settings);
			request.Criteria = new ResumeJobCriteria {ResumeId = resumeId, FetchOption = CriteriaBase.FetchOptions.Lookup};
			var response = ResumeService.GetResumeJobs(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.ResumeJobLookup ?? new Dictionary<string, string>();
		}

		/// <summary>
		/// Sets the default resume.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public bool SetDefaultResume(long resumeId)
		{
			var request = new DefaultResumeRequest().Prepare(AppContext, Runtime.Settings);
			request.ResumeId = resumeId;
			var response = ResumeService.SetDefaultResume(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return (response.Acknowledgement == AcknowledgementType.Success);
		}

    /// <summary>
    /// Gets the default resume Id.
    /// </summary>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public long GetDefaultResumeId()
    {
      var request = new DefaultResumeRequest().Prepare(AppContext, Runtime.Settings);
      var response = ResumeService.GetDefaultResume(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.ResumeId;
    }

    /// <summary>
    /// Gets the default resume Id for a person
    /// </summary>
    /// <param name="personId">The id of person whose default resume id is required</param>
    /// <param name="completedResumesOnly">Whether to only get completed resumes</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    public long GetDefaultResumeId(long personId, bool completedResumesOnly = false)
		{
			var request = new DefaultResumeRequest
			{
			  PersonId = personId,
        CompletedResumesOnly = completedResumesOnly
			}.Prepare(AppContext, Runtime.Settings);

			var response = ResumeService.GetDefaultResume(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.ResumeId;
		}

		/// <summary>
		/// Gets the default resume.
		/// </summary>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		public ResumeModel GetDefaultResume()
		{
			var request = new DefaultResumeRequest().Prepare(AppContext, Runtime.Settings);
			request.FetchDefaultResume = true;

			var response = ResumeService.GetDefaultResume(request);

			if (response.Acknowledgement == AcknowledgementType.Failure)
				throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

			return response.Resume;
		}

		/// <summary>
		/// Gets the count of resumes recently viewed by Employees.
		/// </summary>
		/// <returns></returns>
		public int GetCountOfResumesRecentlyViewedByEmployees()
		{
			var request = new ResumeViewCountRequest().Prepare(AppContext, Runtime.Settings);
			request.PersonId = AppContext.User.PersonId;
			request.FromDate = DateTime.Now.AddDays(-90);
			request.UserType = UserTypes.Talent;

			var response = ResumeService.GetResumeViewCount(request);

			return response.Acknowledgement == AcknowledgementType.Failure ? 0 : response.ViewCount;
		}

	  /// <summary>
	  /// Gets the resume template.
	  /// </summary>
	  /// <returns></returns>
	  public ResumeModel GetResumeTemplate()
	  {
	    var request = new GetResumeTemplateRequest().Prepare(AppContext, Runtime.Settings);
      request.PersonId = AppContext.User.PersonId;

      var response = ResumeService.GetResumeTemplate(request);

      if (response.Acknowledgement == AcknowledgementType.Failure)
        throw new ServiceCallException(response.Message, (int)response.Error, response.Exception);

      return response.Resume;

	  }
	}
}
