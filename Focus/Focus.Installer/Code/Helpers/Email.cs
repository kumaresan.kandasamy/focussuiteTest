﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Configuration;
using System.Net.Mail;

namespace Focus.Installer.Code.Helpers
{
	public static class Email
	{

		/// <summary>
		/// Sends the email.
		/// </summary>
		/// <param name="subject">The subject.</param>
		/// <param name="templatePath">The template path.</param>
		/// <param name="replacements">The replacements.</param>
		public static bool SendEmail(string subject, string templatePath, Dictionary<string,string> replacements)
		{
			var emailEnabled = Properties.Installer.Default.EmailsEnabled;
			
			if (!emailEnabled) return false;
			var emailTos = Properties.Installer.Default.EmailTo;
			
			var section = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
			var mailFrom = (section != null && section.From.IsNotNullOrEmpty() ? section.From : null);

			if(!File.Exists(templatePath)) return false;
			var htmlBody = File.ReadAllText(templatePath);

			string messagebus, webdeploy, interdeploy,oldVersion,newVersion,description;

			replacements.TryGetValue("Web", out webdeploy);
			replacements.TryGetValue("Integration", out interdeploy);
			replacements.TryGetValue("MessageBus",out messagebus);
			replacements.TryGetValue("OldVersion", out oldVersion);
			replacements.TryGetValue("NewVersion", out newVersion);
			replacements.TryGetValue("InstallerDescription", out description);

			htmlBody = htmlBody.Replace("<%ApplicationName%>", "Focus Installer");
			htmlBody = htmlBody.Replace("<%ServerName%>", Environment.MachineName);
			htmlBody = htmlBody.Replace("<%InstallerDescription%>", description);
			htmlBody = htmlBody.Replace("<%OldVersion%>", oldVersion);
			htmlBody = htmlBody.Replace("<%NewVersion%>", newVersion);
			htmlBody = htmlBody.Replace("<%interdeployments%>", interdeploy);
			htmlBody = htmlBody.Replace("<%Webdeployments%>", webdeploy);
			htmlBody = htmlBody.Replace("<%messdeployments%>", messagebus);

			var recipients = emailTos;
			if (string.IsNullOrWhiteSpace(recipients)) return false;

			var html = AlternateView.CreateAlternateViewFromString(htmlBody, new System.Net.Mime.ContentType("text/html"));
			var m = new MailMessage
			{
				IsBodyHtml = true,
				Subject = subject
			};
			m.To.Add(emailTos);
			m.AlternateViews.Add(html);
			
			using (var smtpClient = new SmtpClient())
				smtpClient.Send(m);
		  return true;
		}

		/// <summary>
		/// Sends the specified subject.
		/// </summary>
		/// <param name="subject">The subject.</param>
		/// <param name="templatePath">The template path.</param>
		/// <param name="replacements">The replacements.</param>
		/// <param name="attachment">The attachment.</param>
		public static void SendEmail(string subject, string templatePath, string replacements,Attachment attachment = null)
		{
			var emailEnabled = Properties.Installer.Default.EmailsEnabled;

			if (!emailEnabled) return;
			var emailTos = Properties.Installer.Default.ErrorEmailsTo;
			
			var section = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
			var mailFrom = (section != null && section.From.IsNotNullOrEmpty() ? section.From : null);

			if (!File.Exists(templatePath)) return;
			var htmlBody = File.ReadAllText(templatePath);

			htmlBody = htmlBody.Replace("<%StackTrace%>", replacements);
			htmlBody = htmlBody.Replace("<%ApplicationName%>", "Focus Installer");
			htmlBody = htmlBody.Replace("<%ServerName%>", Environment.MachineName);
			var recipients = emailTos;
			if (string.IsNullOrWhiteSpace(recipients)) return;

			var html = AlternateView.CreateAlternateViewFromString(htmlBody, new System.Net.Mime.ContentType("text/html"));
			var m = new MailMessage
			{
				IsBodyHtml = true,
				Subject = subject
			};
			m.To.Add(emailTos);
			m.AlternateViews.Add(html);

			if (attachment != null)
				m.Attachments.Add(attachment);

			using (var smtpClient = new SmtpClient())
				smtpClient.Send(m);
		}
	}

}
