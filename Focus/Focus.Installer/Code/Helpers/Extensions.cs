﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

#endregion

namespace Focus.Installer.Code.Helpers
{
	public static class Extensions
	{
		#region Core Extensions

		/// <summary>
		/// Convert the string to a date/time.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns></returns>
		public static DateTime? ToDate(this string valueToConvert)
		{
			DateTime result;
			var success = DateTime.TryParse(valueToConvert, out result);

			return (success) ? (DateTime?)result : null;
		}

		/// <summary>
		/// Converts the string to an int.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns></returns>
		public static int? ToInt(this string valueToConvert)
		{
			int result;
			var success = int.TryParse(valueToConvert, out result);

			return (success) ? (int?)result : null;
		}

		/// <summary>
		/// Converts the string to an long.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns></returns>
		public static long? ToLong(this string valueToConvert)
		{
			long result;
			var success = long.TryParse(valueToConvert, out result);

			return (success) ? (long?)result : null;
		}

		/// <summary>
		/// Converts the string to a bool.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns></returns>
		public static bool? ToBool(this string valueToConvert)
		{
			bool result;
			var success = bool.TryParse(valueToConvert, out result);

			return (success) ? (bool?)result : null;
		}

		/// <summary>
		/// Converts the string to a float.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns></returns>
		public static float? ToFloat(this string valueToConvert)
		{
			float result;
			var success = float.TryParse(valueToConvert, out result);

			return (success) ? (float?)result : null;
		}

		/// <summary>
		/// Converts the string to a decimal.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns></returns>
		public static decimal? ToDecimal(this string valueToConvert)
		{
			decimal result;
			var success = decimal.TryParse(valueToConvert, out result);

			return (success) ? (decimal?)result : null;
		}

		/// <summary>
		/// Converts a string list to a single concatenated string
		/// </summary>
		/// <param name="list">The list to concatenated</param>
		/// <param name="separator">The string to separate each item in the list</param>
		/// <param name="ignoreEmptyElements">Whether to exclude empty elements in the list</param>
		/// <returns>A concatenated string</returns>
		public static string ToConcatenatedString(this List<string> list, string separator, bool ignoreEmptyElements = true)
		{
			return list.IsNull() ? string.Empty : string.Join(separator, list.Select(i => i.Trim()).Where(i => !ignoreEmptyElements || i.Length > 0).ToArray());
		}

		/// <summary>
		/// Indefinites the article.
		/// </summary>
		/// <param name="noun">The noun.</param>
		/// <returns></returns>
		public static string IndefiniteArticle(this string noun)
		{
			if (noun.IsNullOrEmpty()) return string.Empty;

			var lastChar = noun.Length == 1 ? noun : noun.Substring(noun.Length - 1);
			return lastChar.ToLower().IsIn("a", "e", "i", "o", "u") ? "an" : "a";
		}

		/// <summary>
		/// Withes the ownership apostrophe.
		/// </summary>
		/// <param name="noun">The noun.</param>
		/// <returns></returns>
		public static string WithOwnershipApostrophe(this string noun)
		{
			if (noun.IsNullOrEmpty()) return string.Empty;
			return noun.ToLower().EndsWith("s") ? noun + "'" : noun + "'s";
		}

		/// <summary>
		/// Determines whether the string can convert to int.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns>
		/// 	<c>true</c> if the string can convert to int; otherwise, <c>false</c>.
		/// </returns>
		public static bool CanConvertToInt(this string valueToConvert)
		{
			if (valueToConvert.IsNullOrEmpty())
				return false;

			int result;
			return int.TryParse(valueToConvert.Trim(), out result);
		}

		/// <summary>
		/// Determines whether the string can convert to long.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns>
		/// 	<c>true</c> if the string can convert to long; otherwise, <c>false</c>.
		/// </returns>
		public static bool CanConvertToLong(this string valueToConvert)
		{
			if (valueToConvert.IsNullOrEmpty())
				return false;

			long result;
			return long.TryParse(valueToConvert.Trim(), out result);
		}

		/// <summary>
		/// Determines whether the string can convert to double.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns>
		/// 	<c>true</c> if the string can convert to double; otherwise, <c>false</c>.
		/// </returns>
		public static bool CanConvertToDouble(this string valueToConvert)
		{
			if (valueToConvert.IsNullOrEmpty())
				return false;

			double result;
			return double.TryParse(valueToConvert.Trim(), out result);
		}

		/// <summary>
		/// Determines whether the string can convert to date.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns>
		/// 	<c>true</c> if the string can convert to date; otherwise, <c>false</c>.
		/// </returns>
		public static bool CanConvertToDate(this string valueToConvert)
		{
			if (valueToConvert.IsNullOrEmpty())
				return false;

			DateTime result;

			if (DateTime.TryParse(valueToConvert.Trim(), out result))
			{
				// 1st Jan 1753 is the minimum date value in SQL Server.
				// So we must be after that. Let's pick 1900.
				return result >= new DateTime(1900, 1, 1);
			}

			return false;
		}

		/// <summary>
		/// Determines whether the string can convert to boolean.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns>
		/// 	<c>true</c> if the string can convert to boolean; otherwise, <c>false</c>.
		/// </returns>
		public static bool CanConvertToBoolean(this string valueToConvert)
		{
			if (valueToConvert.IsNullOrEmpty())
				return false;

			bool result;
			return bool.TryParse(valueToConvert.Trim(), out result);
		}

		/// <summary>
		/// Checks the int is betweens the min and max values.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <param name="min">The min.</param>
		/// <param name="max">The max.</param>
		/// <returns></returns>
		public static bool Between(this int number, int min, int max)
		{
			return number >= min && number <= max;
		}

		/// <summary>
		/// Checks the double is betweens the min and max values.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <param name="min">The min.</param>
		/// <param name="max">The max.</param>
		/// <returns></returns>
		public static bool Between(this double number, int min, int max)
		{
			return number >= min && number <= max;
		}

		/// <summary>
		/// Satisfieses the requirements that the string is a number.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <param name="condition">The condition.</param>
		/// <returns></returns>
		public static bool SatisfiesRequirements(this string number, Func<int, bool> condition)
		{
			if (number.IsNullOrEmpty())
				return false;

			int result;
			var success = int.TryParse(number.Trim(), out result);

			return success && condition(result);
		}

		/// <summary>
		/// Satisfieses the requirements that the string is a double.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <param name="condition">The condition.</param>
		/// <returns></returns>
		public static bool SatisfiesRequirementsAsDouble(this string number, Func<double, bool> condition)
		{
			if (number.IsNullOrEmpty())
				return false;

			double result;
			var success = double.TryParse(number.Trim(), out result);

			return success && condition(result);
		}

		/// <summary>
		/// Determines whether the specified string is null or empty.
		/// </summary>
		/// <param name="s">The s.</param>
		/// <returns>
		/// 	<c>true</c> if the string is null or empty; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsNullOrEmpty(this string s)
		{
			return string.IsNullOrEmpty(s);
		}

		/// <summary>
		/// Determines whether the string is not null or empty.
		/// </summary>
		/// <param name="s">The s.</param>
		/// <returns>
		/// 	<c>true</c> if the string is not null or empty; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsNotNullOrEmpty(this string s)
		{
			return !string.IsNullOrEmpty(s);
		}

		/// <summary>
		/// Determines whether the specified list is not null or empty.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list">The list.</param>
		/// <returns>
		/// 	<c>true</c> if the list is null or empty; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsNullOrEmpty<T>(this IEnumerable<T> list)
		{
			return list == null || !list.Any();
		}

		/// <summary>
		/// Determines whether the specified list is not null or empty.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list">The list.</param>
		/// <returns>
		/// 	<c>true</c> if the list is not null or empty; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsNotNullOrEmpty<T>(this IEnumerable<T> list)
		{
			return list != null && list.Any();
		}

		/// <summary>
		/// Determines whether the specified object is null.
		/// </summary>
		/// <param name="obj">The obj.</param>
		/// <returns>
		/// 	<c>true</c> if the specified object is null; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsNull(this object obj)
		{
			return obj == null;
		}

		/// <summary>
		/// Determines whether the object is not null.
		/// </summary>
		/// <param name="obj">The obj.</param>
		/// <returns>
		/// 	<c>true</c> if the specified object is not null; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsNotNull(this object obj)
		{
			return obj != null;
		}

		/// <summary>
		/// Determines whether the specified object is a db null.
		/// </summary>
		/// <param name="obj">The obj.</param>
		/// <returns>
		/// 	<c>true</c> if the specified object is a db null; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsDBNull(this object obj)
		{
			return obj == DBNull.Value;
		}

		/// <summary>
		/// Establishes the differences of 2 enumerables.
		/// </summary>
		/// <typeparam name="TSource">The type of the source.</typeparam>
		/// <param name="first">The first.</param>
		/// <param name="second">The second.</param>
		/// <param name="keySelector">The key selector.</param>
		/// <returns></returns>
		public static IEnumerable<TSource> DifferencesBy<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, Func<TSource, double> keySelector)
		{
			var i = -1;

			foreach (var element in first)
			{
				i++;

				if (second.Count() <= i)
				{
					yield return element;
					continue;
				}

				if (second.ElementAt(i).IsNull())
					yield return element;

				if (keySelector(element) == keySelector(second.ElementAt(i)))
					continue;

				yield return element;
			}
		}

		/// <summary>
		/// Returns an IEnumerable of distincts objects. 
		/// </summary>
		/// <typeparam name="TSource">The type of the source.</typeparam>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <param name="source">The source.</param>
		/// <param name="keySelector">The key selector.</param>
		/// <returns></returns>
		public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			var distinctKeys = new HashSet<TKey>();
			return source.Where(element => distinctKeys.Add(keySelector(element)));
		}

		/// <summary>
		/// Trims the specified source.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="numberOfCharacters">The number of characters.</param>
		/// <returns></returns>
		public static string Trim(this string source, int numberOfCharacters)
		{
			if (source.IsNullOrEmpty())
				return string.Empty;

			return (source.Length > numberOfCharacters) ? source.Substring(0, numberOfCharacters) + "..." : source;
		}

		/// <summary>
		/// Equals ignoring case.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="comparison">The comparison.</param>
		/// <returns></returns>
		public static bool EqualsIgnoreCase(this string source, string comparison)
		{
			return source.Equals(comparison, StringComparison.OrdinalIgnoreCase);
		}

		/// <summary>
		/// Determines if a string is contained within
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="comparison">The comparison.</param>
		/// <param name="stringComparison">The string comparison.</param>
		/// <returns>
		/// 	<c>true</c> if [contains] [the specified source]; otherwise, <c>false</c>.
		/// </returns>
		public static bool Contains(this string source, string comparison, StringComparison stringComparison)
		{
			return source.IndexOf(comparison, stringComparison) >= 0;
		}

		/// <summary>
		/// Gets the last moment of month.
		/// </summary>
		/// <param name="dateTime">The date time.</param>
		/// <returns></returns>
		public static DateTime GetLastMomentOfMonth(this DateTime dateTime)
		{
			return new DateTime(dateTime.Year, dateTime.Month, 1).AddMonths(1).AddMilliseconds(-1d);
		}

		/// <summary>
		/// Gets the first moment of month.
		/// </summary>
		/// <param name="dateTime">The date time.</param>
		/// <returns></returns>
		public static DateTime GetFirstMomentOfMonth(this DateTime dateTime)
		{
			return new DateTime(dateTime.Year, dateTime.Month, 1);
		}

		/// <summary>
		/// Converts a bool to a javascript literal.
		/// </summary>
		/// <param name="value">if set to <c>true</c> [value].</param>
		/// <returns></returns>
		public static string ToJavascriptLiteral(this bool value)
		{
			return value.ToString().ToLower();
		}

		/// <summary>
		/// Gets the week commencing date.
		/// </summary>
		/// <param name="startDate">The start date.</param>
		/// <returns></returns>
		public static DateTime GetWeekCommencingDate(this DateTime startDate)
		{
			var dayOfWeek = (int)startDate.DayOfWeek;
			const int startDay = (int)DayOfWeek.Sunday;
			var offSet = startDay - dayOfWeek;

			if (offSet < 0 && startDate == DateTime.MinValue)
				return startDate;

			return startDate.AddDays(offSet);
		}

		/// <summary>
		/// Gets the week end date.
		/// </summary>
		/// <param name="endDate">The end date.</param>
		/// <returns></returns>
		public static DateTime GetWeekEndDate(this DateTime endDate)
		{
			var dayOfWeek = (int)endDate.DayOfWeek;
			const int endDay = (int)DayOfWeek.Saturday;
			var offSet = endDay - dayOfWeek;

			if (offSet > 0 && endDate == DateTime.MinValue)
				return endDate;

			return endDate.AddDays(offSet);
		}

		/// <summary>
		/// Gets the week number.
		/// </summary>
		/// <param name="dtPassed">The dt passed.</param>
		/// <returns></returns>
		public static int GetWeekNumber(this DateTime dtPassed)
		{
			var ciCurr = CultureInfo.CurrentCulture;
			var weekNum = ciCurr.Calendar.GetWeekOfYear(dtPassed, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Saturday);

			return weekNum;
		}

		/// <summary>
		/// Adds the weeks to a date.
		/// </summary>
		/// <param name="dtPassed">The dt passed.</param>
		/// <param name="numberOfWeeks">The number of weeks.</param>
		/// <returns></returns>
		public static DateTime AddWeeks(this DateTime dtPassed, int numberOfWeeks)
		{
			return CultureInfo.CurrentCulture.Calendar.AddWeeks(dtPassed, numberOfWeeks);
		}

		/// <summary>
		/// Gets the fiscal year.
		/// </summary>
		/// <param name="dateTime">The date time.</param>
		/// <returns></returns>
		public static int GetFiscalYear(this DateTime dateTime)
		{
			return dateTime.Month < 4 ? dateTime.Year - 1 : dateTime.Year;
		}

		/// <summary>
		/// Gets the fiscal year start.
		/// </summary>
		/// <param name="dtPassed">The dt passed.</param>
		/// <returns></returns>
		public static DateTime GetFiscalYearStart(this DateTime dtPassed)
		{
			return new DateTime(dtPassed.GetFiscalYear(), 4, 1);
		}

		/// <summary>
		/// Gets the julian days.
		/// </summary>
		/// <param name="dtPassed">The dt passed.</param>
		/// <returns></returns>
		public static long GetJulianDays(this DateTime dtPassed)
		{
			// This code originally comes from Focus Career and this is the original comment

			// Code logic: 1. Get the number of days till the provided year - 1 (365 days).
			//             2. Get the number of leap years in the provided year.
			//             3. Do some screw if the provided year is greater than Gregorain start year.
			//             4. Get the number of days till the providedmonth - 1.
			//             5. Add all the days count and screw the days as per the Gregorian start days.
			// Author: PIS/KNK

			var julianDays = 0;

			const int gregorianStartYear = 1582;
			const long gregorianStartDays = 577737;

			// first, regular days in the elapsed years
			// (-1 so we don't include this year)
			// (use 365 because we're going to handle leaps explicitly)
			var elapsedYears = dtPassed.Year - 1;	// elapsed years
			julianDays += 365 * elapsedYears;

			// add the leap years and remove the non leap years
			// (remember that before 1582 they didn't do 100/400 stuff)
			julianDays += elapsedYears / 4;

			if (elapsedYears > gregorianStartYear)
			{
				julianDays -= elapsedYears / 100;
				julianDays += elapsedYears / 400;
				julianDays += 12;	//100,200,300,500,600,700,900,1000,1100,1300,1400,1500
			}

			var elapsedMonths = dtPassed.Month - 1;

			var elapsedDays = 0;
			for (int month = 0; month < elapsedMonths; month++)
			{
				elapsedDays += DateTime.DaysInMonth(dtPassed.Year, month + 1);
			}

			elapsedDays += dtPassed.Day;
			julianDays += elapsedDays;

			// okay, last little quirk ...
			// (Gregorian calendar jumped from October 4 - October 15,1582)
			if (julianDays > gregorianStartDays)
				julianDays -= 10;

			return julianDays;
		}

		/// <summary>
		/// Formats the date time with default.
		/// </summary>
		/// <param name="dateTime">The date time.</param>
		/// <param name="format">The format.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static string FormatDateTime(this DateTime? dateTime, string format, string defaultValue)
		{
			return (dateTime.HasValue ? String.Format(format, dateTime) : defaultValue);
		}

		/// <summary>
		/// Formats the date time.
		/// </summary>
		/// <param name="dateTime">The date time.</param>
		/// <param name="format">The format.</param>
		/// <returns></returns>
		public static string FormatDateTime(this DateTime dateTime, string format)
		{
			return String.Format(format, dateTime);
		}

		/// <summary>
		/// Generates a hash.
		/// </summary>
		/// <param name="password">The password to hash.</param>
		/// <returns></returns>
		public static string GeneratePasswordHash(this string password)
		{
			if (string.IsNullOrEmpty(password)) throw new ArgumentNullException();

			var buffer = Encoding.UTF8.GetBytes(password);
			buffer = SHA512Managed.Create().ComputeHash(buffer);

			return Convert.ToBase64String(buffer).Substring(0, 86);
		}

		/// <summary>
		/// Captures the number at beginning of string.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="minNumberDigits">The min number digits.</param>
		/// <param name="maxNumberDigits">The max number digits.</param>
		/// <returns></returns>
		public static string CaptureNumberAtBeginningOfString(this string text, int minNumberDigits, int maxNumberDigits)
		{
			var match = Regex.Match(text, string.Concat(@"^\d{", minNumberDigits, ",", maxNumberDigits, "}"));
			var retVal = "";

			if (match.IsNotNull() && match.Success) retVal = match.Value;

			return retVal;
		}

		/// <summary>
		/// Sanitizes the XML string.
		/// </summary>
		/// <param name="xml">The XML.</param>
		/// <returns></returns>
		public static string SanitizeXmlString(this string xml)
		{
			if (xml == null) return "";

			var buffer = new StringBuilder(xml.Length);

			foreach (var character in xml)
				if (IsLegalXmlChar(character))
					buffer.Append(character);

			return buffer.ToString();
		}

		/// <summary>
		/// Determines whether the specified character is a legal XML char.
		/// </summary>
		/// <param name="character">The character.</param>
		/// <returns>
		/// 	<c>true</c> if the specified character is a legal XML char; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsLegalXmlChar(int character)
		{
			return (character == 0x9 /* == '\t' == 9   */||
							character == 0xA /* == '\n' == 10  */||
							character == 0xD /* == '\r' == 13  */||
							(character >= 0x20 && character <= 0xD7FF) ||
							(character >= 0xE000 && character <= 0xFFFD) ||
							(character >= 0x10000 && character <= 0x10FFFF)
						 );
		}

		#endregion

		#region Validation Extensions

		public static void ShouldBeNull(this object actual, string message = null, params object[] args)
		{
			if (actual != null)
				throw new ArgumentException(!String.IsNullOrWhiteSpace(message) ? String.Format(message, args) : "Object should be null");
		}

		public static void ShouldNotBeNull(this object actual, string message = null, params object[] args)
		{
			if (actual == null)
				throw new ArgumentException(!String.IsNullOrWhiteSpace(message) ? String.Format(message, args) : "Object should not be null");
		}

		public static void ShouldBeNullOrEmpty(this string actual, string message = null, params object[] args)
		{
			if (!String.IsNullOrEmpty(actual))
				throw new ArgumentException(!String.IsNullOrWhiteSpace(message) ? String.Format(message, args) : "String should be null or empty");
		}

		public static void ShouldNotBeNullOrEmpty(this string actual, string message = null, params object[] args)
		{
			if (String.IsNullOrEmpty(actual))
				throw new ArgumentException(!String.IsNullOrWhiteSpace(message) ? String.Format(message, args) : "String should not be null or empty");
		}

		public static void ShouldBeTrue(this bool actual, string message = null, params object[] args)
		{
			if (!actual)
				throw new ArgumentException(!String.IsNullOrWhiteSpace(message) ? String.Format(message, args) : "Value should be true");
		}

		public static void ShouldBeFalse(this bool actual, string message = null, params object[] args)
		{
			if (!actual)
				throw new ArgumentException(!String.IsNullOrWhiteSpace(message) ? String.Format(message, args) : "Value should be false");
		}

		public static T ShouldBe<T>(this object actual, string message = null, params object[] args)
		{
			if (!(actual is T))
				throw new ArgumentException(!String.IsNullOrWhiteSpace(message) ? String.Format(message, args) : "Incorrent type");

			return (T)actual;
		}

		#endregion

		#region 'IsIn' extension

		/// <summary>
		/// Checks whether a value is in an array of values
		/// </summary>
		/// <typeparam name="T">The type being used</typeparam>
		/// <param name="value">The value being checked</param>
		/// <param name="args">The array to check</param>
		/// <returns>A boolean indicating if the array contains the value</returns>
		public static bool IsIn<T>(this T value, params T[] args)
		{
			return args.Contains(value);
		}

		/// <summary>
		/// Checks whether a value is in an list of values
		/// </summary>
		/// <typeparam name="T">The type being used</typeparam>
		/// <param name="value">The value being checked</param>
		/// <param name="list">The array to check</param>
		/// <returns>A boolean indicating if the array contains the value</returns>
		public static bool IsIn<T>(this T value, IEnumerable<T> list)
		{
			return list.Contains(value);
		}

		/// <summary>
		/// Checks whether a string value is in an array of values
		/// </summary>
		/// <param name="caseInsenstive">Whether to ignore the case in the check</param>
		/// <param name="value">The value being checked</param>
		/// <param name="args">The array to check</param>
		/// <returns>A boolean indicating if the array contains the value</returns>
		public static bool IsIn(this string value, bool caseInsenstive, params string[] args)
		{
			return caseInsenstive ? args.Contains(value, StringComparer.OrdinalIgnoreCase) : value.IsIn(args);
		}

		/// <summary>
		/// Checks whether a string value is in an array of values
		/// </summary>
		/// <param name="caseInsenstive">Whether to ignore the case in the check</param>
		/// <param name="value">The value being checked</param>
		/// <param name="list">The array to check</param>
		/// <returns>A boolean indicating if the array contains the value</returns>
		public static bool IsIn(this string value, bool caseInsenstive, IEnumerable<string> list)
		{
			return caseInsenstive ? list.Contains(value, StringComparer.OrdinalIgnoreCase) : value.IsIn(list);
		}

		/// <summary>
		/// Checks whether a value is in an array of values
		/// </summary>
		/// <typeparam name="T">The type being used</typeparam>
		/// <param name="value">The value being checked</param>
		/// <param name="args">The array to check</param>
		/// <returns>A boolean indicating if the array contains the value</returns>
		public static bool IsNotIn<T>(this T value, params T[] args)
		{
			return !args.Contains(value);
		}

		/// <summary>
		/// Checks whether a value is in an list of values
		/// </summary>
		/// <typeparam name="T">The type being used</typeparam>
		/// <param name="value">The value being checked</param>
		/// <param name="list">The array to check</param>
		/// <returns>A boolean indicating if the array contains the value</returns>
		public static bool IsNotIn<T>(this T value, IEnumerable<T> list)
		{
			return !list.Contains(value);
		}

		/// <summary>
		/// Checks whether a string value is in an array of values
		/// </summary>
		/// <param name="caseInsenstive">Whether to ignore the case in the check</param>
		/// <param name="value">The value being checked</param>
		/// <param name="args">The array to check</param>
		/// <returns>A boolean indicating if the array contains the value</returns>
		public static bool IsNotIn(this string value, bool caseInsenstive, params string[] args)
		{
			return caseInsenstive ? !args.Contains(value, StringComparer.OrdinalIgnoreCase) : value.IsNotIn(args);
		}

		/// <summary>
		/// Checks whether a string value is in an array of values
		/// </summary>
		/// <param name="caseInsenstive">Whether to ignore the case in the check</param>
		/// <param name="value">The value being checked</param>
		/// <param name="list">The array to check</param>
		/// <returns>A boolean indicating if the array contains the value</returns>
		public static bool IsNotIn(this string value, bool caseInsenstive, IEnumerable<string> list)
		{
			return caseInsenstive ? !list.Contains(value, StringComparer.OrdinalIgnoreCase) : value.IsNotIn(list);
		}
		#endregion

		#region List extensions

		/// <summary>
		/// Adds multiple items to a list
		/// </summary>
		/// <typeparam name="T">The type of the list</typeparam>
		/// <param name="value">The generic list</param>
		/// <param name="args">The items to add</param>
		public static void AddItems<T>(this List<T> value, params T[] args)
		{
			value.AddRange(args);
		}

		#endregion
	}
}
