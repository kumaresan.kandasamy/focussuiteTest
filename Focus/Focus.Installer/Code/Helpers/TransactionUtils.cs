﻿
using System;
using System.Transactions;

namespace Focus.Installer.Code.Helpers
{
	public class TransactionUtils
	{
		public static TransactionScope CreateTransactionScope()
		{
			var transactionOptions = new TransactionOptions
			{
				IsolationLevel = IsolationLevel.ReadCommitted,
				Timeout = TimeSpan.FromHours(24)
			};

			return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
		}
	}
}
