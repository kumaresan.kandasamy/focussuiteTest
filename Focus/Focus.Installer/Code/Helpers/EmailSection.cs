﻿
using System.Configuration;


namespace Focus.Installer.Code.Helpers
{
	public class EmailSection : ConfigurationSection
	{
		[ConfigurationProperty("enabled")]
		public string EmailsEnabled { get; set; }

		[ConfigurationProperty("emailTo")]
		public string EmailToAddress { get; set; }
	}
}
