﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Focus.Installer.Code.Helpers;

namespace Focus.Installer.Code.Commands
{
    internal class DeployCommand : Command
    {
        private readonly Deployment _deployment;
        private readonly SharedPath[] _sharedPath;

        public DeployCommand(Worker worker, Deployment deployment, SharedPath[] sharedPath)
            : base(worker)
        {
            _deployment = deployment;
            _sharedPath = sharedPath;
        }

        public override void Install()
        {
            base.Install();

            #region Ensure From Path exists

            var baseFromPath = _deployment.BaseFromPaths.Path;
            if (_deployment.BaseFromPaths.Sharedpath.IsNotNullOrEmpty())
            {
                var sharedPath = _sharedPath.FirstOrDefault(x => x.Name == _deployment.BaseFromPaths.Sharedpath);
                if (sharedPath != null)
                    baseFromPath = sharedPath.Path + "\\" + _deployment.BaseFromPaths.Path;
            }

            Log("Ensuring source directory exists (" + baseFromPath + ")");
            Ensure.That(Directory.Exists(baseFromPath), "Base from path '" + baseFromPath + "' doesn't exist!");

            #endregion

            #region Check To Path exists and if not then create it

            var baseToPath = _deployment.BaseToPaths.Path;
            if (_deployment.BaseToPaths.Sharedpath.IsNotNullOrEmpty())
            {
                var sharedPath = _sharedPath.FirstOrDefault(x => x.Name == _deployment.BaseToPaths.Sharedpath);
                if (sharedPath.IsNotNull())
                    baseToPath = sharedPath.Path + "\\" + _deployment.BaseToPaths.Path;
            }

            var backUpPath = _deployment.BackupToPaths.Path;
            if (_deployment.BackupToPaths.Sharedpath.IsNotNullOrEmpty())
            {
                var sharedPath = _sharedPath.FirstOrDefault(x => x.Name == _deployment.BackupToPaths.Sharedpath);
                if (sharedPath.IsNotNull())
                    baseToPath = sharedPath.Path + "\\" + _deployment.BackupToPaths.Path;
            }


            // If not create it.
            if (!Directory.Exists(_deployment.BaseToPaths.Path))
            {
                Log("Creating destination directory (" + baseToPath + ")");
                Directory.CreateDirectory(baseToPath);
            }
            else
            {
                Log("Backing up source files from '" + baseToPath + "' to '" + backUpPath + "'");

                var srcPath = new DirectoryInfo(baseToPath);
                var destPath = new DirectoryInfo(backUpPath);

                var srcFiles = srcPath.GetFiles("*.*", SearchOption.AllDirectories);

                var filecount = srcFiles.Length;
                var current = 0;
                foreach (var srcFile in srcFiles)
                {
                    current++;
                    Console.WriteLine("backing up file " + current + " of " + filecount);

                    Console.SetCursorPosition(0, Console.CursorTop - 1);

                    var sourceFile = srcFile.FullName;
                    var destFile = srcFile.FullName.Replace(srcPath.FullName, destPath.FullName);

                    var destDirectory = Path.GetDirectoryName(destFile);

                    Log("   Backing up .." + sourceFile.Replace(srcPath.FullName, ""), true);

                    if (!Directory.Exists(destDirectory))
                        Directory.CreateDirectory(destDirectory);

                    srcFile.CopyTo(destFile, true);
                }
            }

            #endregion

            #region Validate file sets

            Log("Validate file sets");

            foreach (var fileSet in _deployment.FileSets.Where(x => !Directory.Exists(GetFullPath(baseFromPath, x.Path))))
                throw new Exception(String.Format("File set path doesn't exist: {0}", GetFullPath(baseFromPath, fileSet.Path)));

            #endregion

            #region Copy file sets

            Log("Copying file sets");

            var fileSetId = 1;

            foreach (var fileSet in _deployment.FileSets)
            {
                Log("Processing file set " + fileSetId++);

                var srcPath = new DirectoryInfo(GetFullPath(baseFromPath, fileSet.Path));
                var destPath = new DirectoryInfo(baseToPath + "\\" + fileSet.Path);

                var includeFiles = new List<FileInfo>();
                var excludeFiles = new List<FileInfo>();

                foreach (var searchPattern in fileSet.SearchPatterns)
                {
                    if (searchPattern.Include.IsNotNull()) includeFiles = includeFiles.Concat(srcPath.GetFiles(searchPattern.Include, searchPattern.SearchOption)).ToList();
                    if (searchPattern.Exclude.IsNotNull()) excludeFiles = excludeFiles.Concat(srcPath.GetFiles(searchPattern.Exclude, searchPattern.SearchOption)).ToList();
                }

                if (includeFiles.Count == 0)
                    includeFiles = srcPath.GetFiles("*.*", SearchOption.AllDirectories).ToList();

                if (excludeFiles.Count > 0)
                {
                    Log("Excluding the following files");

                    foreach (var excludeFile in excludeFiles)
                        Log("   " + excludeFile.Name);
                }
                var filecount = includeFiles.Count;
                var current = 0;
                foreach (var includeFile in includeFiles)
                {
                    if (excludeFiles.All(x => x.FullName != includeFile.FullName))
                    {
                        current++;
                        Console.WriteLine("   Copying file " + current + " of " + filecount);
                        Console.SetCursorPosition(0, Console.CursorTop - 1);

                        var sourceFile = includeFile.FullName;
                        var destFile = includeFile.FullName.Replace(srcPath.FullName, destPath.FullName);

                        var destDirectory = Path.GetDirectoryName(destFile);

                        Log("   Copying .." + sourceFile.Replace(srcPath.FullName, ""), true);

                        if (!Directory.Exists(destDirectory))
                            Directory.CreateDirectory(destDirectory);

                        includeFile.CopyTo(destFile, true);
                    }
                }
            }


            #endregion
        }

        /// <summary>
        /// Rollbacks this instance.
        /// </summary>
        public override void Rollback()
        {
            base.Rollback();


            var backUpPath = _deployment.BackupToPaths.Path;
            if (_deployment.BackupToPaths.Sharedpath.IsNotNullOrEmpty())
            {
                var sharedPath = _sharedPath.FirstOrDefault(x => x.Name == _deployment.BackupToPaths.Sharedpath);
                if (sharedPath != null)
                    backUpPath = sharedPath.Path + "\\" + _deployment.BackupToPaths.Path;
            }

            var baseToPath = _deployment.BaseToPaths.Path;
            if (_deployment.BaseToPaths.Sharedpath.IsNotNullOrEmpty())
            {
                var sharedPath = _sharedPath.FirstOrDefault(x => x.Name == _deployment.BaseToPaths.Sharedpath);
                if (sharedPath != null)
                    baseToPath = sharedPath.Path + "\\" + _deployment.BaseToPaths.Path;
            }

            #region Restore back all the files from the backup folder

            Log("Restoring files from " + backUpPath);

            var backedupPath = new DirectoryInfo(backUpPath);
            var restorePath = new DirectoryInfo(baseToPath);

            var backedupFiles = backedupPath.GetFiles("*.*", SearchOption.AllDirectories);

            foreach (var backedupFile in backedupFiles)
            {
                var sourceFile = backedupFile.FullName;
                var destFile = backedupFile.FullName.Replace(backedupPath.FullName, restorePath.FullName);

                var destDirectory = Path.GetDirectoryName(destFile);

                Log("   Restoring .." + sourceFile.Replace(backedupPath.FullName, ""));

                if (!Directory.Exists(destDirectory))
                    Directory.CreateDirectory(destDirectory);

                backedupFile.CopyTo(destFile, true);
            }

            #endregion
        }

        /// <summary>
        /// Cleans up the command.
        /// </summary>
        public override void CleanUp()
        {

            var backUpPath = _deployment.BackupToPaths.Path;
            if (_deployment.BackupToPaths.Sharedpath.IsNotNullOrEmpty())
            {
                var sharedPath = _sharedPath.FirstOrDefault(x => x.Name == _deployment.BackupToPaths.Sharedpath);
                if (sharedPath != null)
                    backUpPath = sharedPath.Path + "\\" + _deployment.BackupToPaths.Path;
            }

            if (Directory.Exists(backUpPath))
            {
                Log("Cleaning up backed up files");
                Directory.Delete(backUpPath, true);
            }
        }

        #region Helper Methods

        private static string GetFullPath(string basePath, string subPath)
        {
            Ensure.That(basePath.IsNotNullOrEmpty(), "Base path is null or empty");

            return CleanPath(basePath + "\\" + (subPath ?? ""));
        }

        private static string CleanPath(string path)
        {
            path = path.Replace("\\\\", "\\");
            return (path.IndexOf('.') > 0 ? path.Substring(0, path.IndexOf('.')) + "\\" : path);
        }

        #endregion
    }
}
