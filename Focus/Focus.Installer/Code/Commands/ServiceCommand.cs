﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Diagnostics;
using System.Security;
using Focus.Installer.Code.Helpers;

#endregion

namespace Focus.Installer.Code.Commands
{
	internal class ServiceCommand
	{
		private readonly Worker _worker;
		private readonly Service _service;

		/// <summary>
		/// Initializes a new instance of the <see cref="ServiceCommand"/> class.
		/// </summary>
		/// <param name="worker">The worker.</param>
		/// <param name="service">The service.</param>
		public ServiceCommand(Worker worker, Service service)
		{
			_worker = worker;
			_service = service;
		}

		/// <summary>
		/// Starts the service.
		/// </summary>
		/// <returns></returns>
		public bool Start()
		{
			return ExecuteServiceControllerCommand(ServiceControllerCommandType.Start);
		}

		/// <summary>
		/// Stops the service.
		/// </summary>
		/// <returns></returns>
		public bool Stop()
		{
			return ExecuteServiceControllerCommand(ServiceControllerCommandType.Stop);
		}

		/// <summary>
		/// Executes the service controller command.
		/// </summary>
		/// <param name="commandType">Type of the command.</param>
		/// <returns></returns>
		private bool ExecuteServiceControllerCommand(ServiceControllerCommandType commandType)
		{
			var command = String.Format("sc {0} {1} \"{2}\"", (_service.Server.IsNotNullOrEmpty() ? @"\\" + _service.Server : ""), commandType.ToString().ToLowerInvariant(), _service.Name);

			Log((commandType == ServiceControllerCommandType.Start ? "Starting " : "Stopping") + _service.Description + " with " + command);
			
			var process = new Process();
			var startInfo = new ProcessStartInfo
			{
				WindowStyle = ProcessWindowStyle.Hidden,
				FileName = "cmd.exe",
				Arguments = "/c " + command,
				UseShellExecute = true,
				Verb = "runas"
			};

			process.StartInfo = startInfo;
			process.Start();
			process.WaitForExit();
			return process.ExitCode == 0;
		}

		/// <summary>
		/// Logs the specified details.
		/// </summary>
		/// <param name="details">The details.</param>
		public void Log(string details)
		{
			_worker.Log(details);
		}

		private enum ServiceControllerCommandType
		{
			Start,
			Stop
		}
	}
}
