﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using Focus.Installer.Code.Helpers;

#endregion

namespace Focus.Installer.Code.Commands
{
	internal class DatabaseCommand : Command
	{
		private readonly Database _database;
		private readonly List<SqlScript> _sqlScripts;
		private readonly DbProviderFactory _factory;

		// Time for Script is allowed to run for 24 hours
    private const int commandTimeout = 86400;

		public DatabaseCommand(Worker worker, Database database, List<SqlScript> sqlScripts) : base(worker)
		{
			_database = database;
			_sqlScripts = sqlScripts;
			_factory = DbProviderFactories.GetFactory(_database.ProviderName);
		}

		public override void Install()
		{
			base.Install();
			
			Log("Opening connection");

			using (var conn = CreateConnection())
			{
				var currentVersion = "";
				
				foreach (var sqlScript in _sqlScripts)
				{
					if (currentVersion != sqlScript.Version)
					{
						Log("Upgrading to Version " + sqlScript.Version);
						currentVersion = sqlScript.Version;
					}
					Log("Executing " + sqlScript.Filename);

					var sql = sqlScript.Sql;
					var sqlCommands = Regex.Split(sql, @"\bGO\b").Where(s => s.Trim().IsNotNullOrEmpty()).ToArray();

					Log(" " + sqlCommands.Length + " commands to execute");
					Log(" ");

				
					foreach (var sqlCommand in sqlCommands)
					{
						using (var cmd = CreateCommand(sqlCommand, conn))
						{
					
							try
							{
								cmd.ExecuteNonQuery();
							}
							catch (Exception exception)
							{
								Log("Sql command failed: " + exception.Message);
								throw;
							}
						}
					}
				}
				conn.Close();
			}

		}

		private DbConnection CreateConnection()
		{
			var connection = _factory.CreateConnection();
			connection.ConnectionString = _database.ConnectionString;

			var sqlConnection = connection as SqlConnection;
			if (sqlConnection != null)
				sqlConnection.InfoMessage += SqlServerInfoMessage;

			connection.Open();

			return connection;
		}

		private DbCommand CreateCommand(string sql, DbConnection connection)
		{
			var command = _factory.CreateCommand();
			command.Connection = connection;
			command.CommandText = sql;
			// Local DB does not require command timeout and transactions
			if (_database.ProviderName.Contains("System.Data.SqlServerCE.4.0")) return command;
			command.CommandTimeout =  commandTimeout;
			
			return command;
		}

		private void SqlServerInfoMessage(object sender, SqlInfoMessageEventArgs e)
		{
			Log(e.Message);
		}
	}

	public class SqlScript
	{
		[XmlAttribute("version")]
		public string Version { get; set; }

		[XmlAttribute("filename")]
		public string Filename { get; set; }

		[XmlIgnore]
		public string Sql { get; set; }

		[XmlIgnore]
		public DatabaseScriptType ScriptType { get; set; }

		[XmlAttribute("target")]
		[XmlIgnore]
		public DatabaseTarget ScriptTarget { get; set; }
	}
}
