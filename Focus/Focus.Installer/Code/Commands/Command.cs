﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

namespace Focus.Installer.Code.Commands
{
	/// <summary>
	/// The 'Command' abstract class
	/// </summary>
	internal abstract class Command : ICommand
	{
		protected Command(Worker worker)
		{
			Worker = worker;
		}

		protected Worker Worker { get; private set; }

		/// <summary>
		/// Perform the install
		/// </summary>
		public virtual void Install()
		{ }

		/// <summary>
		/// Perform the roll back
		/// </summary>
		public virtual void Rollback()
		{ }

		/// <summary>
		/// Cleans up the command.
		/// </summary>
		public virtual void CleanUp()
		{ }

	  /// <summary>
	  /// Logs the specified details.
	  /// </summary>
	  /// <param name="details">The details.</param>
	  /// <param name="fileOnly"></param>
	  public void Log(string details,bool fileOnly = false)
		{
			Worker.Log(details,fileOnly);
		}
	}

	public interface ICommand
	{
		void Install();
		void Rollback();
		void CleanUp();
	}
}
