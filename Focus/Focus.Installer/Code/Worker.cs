﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;

using Focus.Installer.Code.Commands;
using Focus.Installer.Code.Helpers;

#endregion

namespace Focus.Installer.Code
{
  public class Worker
  {
    private readonly Action<string,bool> _logMethod;
    private bool _showHelp, _verbose, _forceRelease, _backup;

    private Release[] _installableReleases;
    private Deployment[] _webDeployments;
    private Deployment[] _messageBusDeployments;

    /// <summary>
    /// Initializes a new instance of the <see cref="Worker"/> class.
    /// </summary>
    /// <param name="logMethod">The log method.</param>
    public Worker(Action<string,bool> logMethod)
    {
      _logMethod = logMethod;

      // Everything ok until told otherwise
      Environment.ExitCode = 0x0000;
    }

    public string ConfigPath { get; private set; }
    public string ReleasesPath { get; private set; }
    public Configuration Configuration { get; private set; }
    public Releases Releases { get; private set; }
    public string Version { get; private set; }

    public void Process(string[] args)
    {
      // Let's get cracking!
      Status("Initializing installer...");

      // Read the parameters
      Log("Reading parameters");

      var argsOk = ReadArgs(args);

      // Needs help on the application?
      if (_showHelp || !argsOk)
      {
        // Yes, then go there whatever the other parameters are
        ShowHelp();
        Environment.ExitCode = (!argsOk ? 0x0001 : 0x0000);
      }
      else
      {

        var status = InstallStatus.Starting;
        var successfulInstall = false;

        #region Load Config

        Status("Loading config from: " + ConfigPath);

        if (!File.Exists(ConfigPath))
          throw new Exception("Config path '" + ConfigPath + "' doesn't exist");

        using (var stream = new FileStream(ConfigPath, FileMode.Open, FileAccess.Read, FileShare.Read))
        {
          var serializer = new XmlSerializer(typeof(Configuration));
          Configuration = (Configuration)serializer.Deserialize(stream);

          stream.Close();
        }

        #endregion

        #region Load and validate Versions

        Status("Loading releases from: " + ReleasesPath);

        if (!File.Exists(ReleasesPath))
          throw new Exception("Releases path '" + ReleasesPath + "' doesn't exist");

        using (var stream = new FileStream(ReleasesPath, FileMode.Open, FileAccess.Read, FileShare.Read))
        {
          var serializer = new XmlSerializer(typeof(Releases));
          Releases = (Releases)serializer.Deserialize(stream);

          stream.Close();
        }

        var sqlPathsOk = true;

        foreach (var release in Releases.Items)
        {
          if (!Directory.Exists(release.SqlPath) && release.SqlPath.IsNotNullOrEmpty())
          {
            Log("Sql script path '" + release.SqlPath + "' for version " + release.Version + " doesn't exist.");
            sqlPathsOk = false;
          }
        }

        if (!sqlPathsOk)
        {
          Log("Unable to continue due to Sql script path issues.");
          return;
        }

        #endregion

        #region Establish items to be installed

        Status("Establishing items to be installed");

        _installableReleases = GetInstallableReleases();
        if (_installableReleases.IsNullOrEmpty() && !_forceRelease)
          throw new Exception("No releases found. Use -force argument to install same version");
        _webDeployments = GetDeployments(DeployTarget.Web);
        _messageBusDeployments = GetDeployments(DeployTarget.MessageBus);

        #endregion

        status = InstallStatus.LoadedConfiguration;

        try
        {
          #region Installing...

          Status("Installing");

          #region Stop Web(s)

          foreach (var webDeployment in _webDeployments.Where(x => x.Service.IsNotNull()))
          {
            Status("Stopping web '" + webDeployment.Description + "'");
            StopService(webDeployment.Service);
          }

          status = InstallStatus.StoppedWebs;

          #endregion

          #region Stop Message Bus(es)

          foreach (var messageBusDeployment in _messageBusDeployments.Where(x => x.Service.IsNotNull()))
          {
            Status("Stopping Message Bus '" + messageBusDeployment.Description + "'");
            StopService(messageBusDeployment.Service);
          }

          status = InstallStatus.StoppedMessageBuses;

          #endregion

          #region "Execute any Pre-Deploy scripts"

          foreach (var database in Configuration.Databases)
          {
            var sqlScripts = new List<SqlScript>();

            if (database.DatabaseScripts.IsNotNull() &&
                database.DatabaseScripts.Any(x => x.ScriptType == DatabaseScriptType.PreDeploy))
              sqlScripts.AddRange(
                database.DatabaseScripts.Where(x => x.ScriptType == DatabaseScriptType.PreDeploy)
                  .Select(
                    databaseScript =>
                      new SqlScript
                      {
                        Filename = "Pre-Deploy - " + databaseScript.Name,
                        Version = "",
                        Sql = databaseScript.Script,
                        ScriptType = DatabaseScriptType.PreDeploy
                      }));

            if (sqlScripts.Count > 0)
            {
              Status("Executing Pre-Deploy Scripts '" + database.Target + "'");

              var command = new DatabaseCommand(this, database, sqlScripts);
              command.Install();

              Status("Executed Pre-Deploy Scripts '" + database.Target + "'");
            }
          }

          #endregion

          #region Upgrade the database for all the required versions

          if (Configuration.Databases.IsNotNull() && Configuration.Databases.Length > 0)
          {
            foreach (var database in Configuration.Databases)
            {
              // Check Here for any scripts to be run. seems a waste to open connections with no scripts to execute
              using (var scope = TransactionUtils.CreateTransactionScope())
              {
                var sqlScripts = new List<SqlScript>();

                Status("Upgrading '" + database.Target + "'");

                foreach (var installableRelease in _installableReleases)
                  sqlScripts.AddRange(GetSqlScripts(installableRelease.Version, database.Target));

                var command = new DatabaseCommand(this, database, sqlScripts.Where(x => x.ScriptTarget == database.Target).ToList());
                command.Install();

                var allscripts = Configuration.ScriptsRun.Concat(sqlScripts.Where(x => x.ScriptType == DatabaseScriptType.Version).ToArray()).ToArray();
                Configuration.ScriptsRun = allscripts;
                scope.Complete();
                scope.Dispose();
              }
            }

            // Scripts Executed Successfully Log in Config so next time then dont run.
            using (var stream = new FileStream(ConfigPath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
              var serializer = new XmlSerializer(typeof(Configuration));
              serializer.Serialize(stream, Configuration);
              stream.Close();
            }
          }

          status = InstallStatus.UpdatedDatabase;

          #endregion

          #region Deploy Webs

          foreach (var webDeployment in _webDeployments)
          {
            Status("Deploying web '" + webDeployment.Description + "'");

            webDeployment.Command = new DeployCommand(this, webDeployment, Configuration.SharedPaths);
            webDeployment.Command.Install();

            Status("Deployed web '" + webDeployment.Description + "'");
          }

          status = InstallStatus.DeployedWebs;

          #endregion

          #region  Deploy MessageBus (s)

          foreach (var messageBusDeployment in _messageBusDeployments)
          {
            Status("Deploying message bus '" + messageBusDeployment.Description + "'");

            messageBusDeployment.Command = new DeployCommand(this, messageBusDeployment, Configuration.SharedPaths);
            messageBusDeployment.Command.Install();

            Status("Deployed message bus '" + messageBusDeployment.Description + "'");
          }

          status = InstallStatus.DeployedMessageBuses;

          #endregion

          #region "Execute any Post-Deploy scripts"

          foreach (var database in Configuration.Databases)
          {
            var sqlScripts = new List<SqlScript>();

            if (database.DatabaseScripts.IsNotNull() &&
                database.DatabaseScripts.Any(x => x.ScriptType == DatabaseScriptType.PostDeploy))
              sqlScripts.AddRange(
                database.DatabaseScripts.Where(x => x.ScriptType == DatabaseScriptType.PostDeploy)
                  .Select(
                    databaseScript =>
                      new SqlScript
                      {
                        Filename = "Post-Deploy - " + databaseScript.Name,
                        Version = "",
                        Sql = databaseScript.Script,
                        ScriptType = DatabaseScriptType.PostDeploy
                      }));

            if (sqlScripts.Count > 0)
            {
              Status("Executing Post-Deploy Scripts '" + database.Target + "'");

              var command = new DatabaseCommand(this, database, sqlScripts);
              command.Install();

              Status("Executed Post-Deploy Scripts '" + database.Target + "'");
            }
          }

          #endregion

          successfulInstall = true;

          #endregion
        }
        catch (Exception ex)
        {
          Status("Exception: " + ex.Message);
          Status("InnerException: " + ex.InnerException);
          Status("Stack Trace: " + ex.StackTrace);

          #region Rolling back...

          Log("Rolling back...");
          status = InstallStatus.RollingBack;

          #region Roll back message buses

          if (status >= InstallStatus.DeployedMessageBuses)
            foreach (var messageBusDeployment in _messageBusDeployments.Where(x => x.Command.IsNotNull()))
            {
              Status("Rolling back message bus '" + messageBusDeployment.Description + "'");

              messageBusDeployment.Command.Rollback();

              Status("Rolled back message bus '" + messageBusDeployment.Description + "'");
            }

          #endregion

          #region Roll back webs

          if (status >= InstallStatus.DeployedWebs)
            foreach (var webDeployment in _webDeployments.Where(x => x.Command.IsNotNull()))
            {
              Status("Rolling back web '" + webDeployment.Description + "'");

              webDeployment.Command.Rollback();

              Status("Rolled back web '" + webDeployment.Description + "'");
            }

          #endregion

          if (status >= InstallStatus.UpdatedDatabase)
            Status("Rolled back the DB");

          #endregion
        }
        finally
        {
          #region Restart services, clean up and update current version if applicable

          #region Start Message Bus(es)

          if (_messageBusDeployments.Length > 0)
          {
            Log("Starting Message Bus(es)");
            status = InstallStatus.StartingMessageBuses;

            foreach (var messageBusDeployment in _messageBusDeployments.Where(x => x.Service.IsNotNull()))
            {
              Log("Starting message bus '" + messageBusDeployment.Description + "'");
              StartService(messageBusDeployment.Service);
            }
          }

          #endregion

          #region Start Web(s)

          if (_webDeployments.Length > 0)
          {
            Log("Starting Web(s)");
            status = InstallStatus.StartingWebs;

            foreach (var webDeployment in _webDeployments.Where(x => x.Service.IsNotNull()))
            {
              Log("Starting web '" + webDeployment.Description + "'");
              StartService(webDeployment.Service);
            }
          }

          #endregion

          if (_backup)
          {
            Status("Cleaning up backups");
            status = InstallStatus.CleaningUpBackups;

            #region Clean up message buses

            foreach (var messageBusDeployment in _messageBusDeployments.Where(x => x.Command.IsNotNull()))
            {
              Log("Clean up message bus '" + messageBusDeployment.Description + "'");

              messageBusDeployment.Command.CleanUp();

              Log("Cleaned up message bus '" + messageBusDeployment.Description + "'");
            }

            #endregion

            #region Clean up webs

            foreach (var webDeployment in _webDeployments.Where(x => x.Command.IsNotNull()))
            {
              Log("Clean up back web '" + webDeployment.Description + "'");

              webDeployment.Command.CleanUp();

              Log("Cleaned up back web '" + webDeployment.Description + "'");
            }

            #endregion
          }

          #region Update Version

          if (successfulInstall)
          {
            Status("Updating the current version in the configuration file");

            Configuration.CurrentVersion = Version;

            using (var stream = new FileStream(ConfigPath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
              var serializer = new XmlSerializer(typeof(Configuration));
              serializer.Serialize(stream, Configuration);
              stream.Close();
            }

            Status("Current version has been set to '" + Version + "'");
          }

          #endregion

          #endregion
        }

        status = InstallStatus.Finished;
        Log(" ");
      }

      Status("Installer complete");
    }

    public void SendEmail()
    {
      var subject = "Installed " + Configuration.Description + " " + Version + " On " + Environment.MachineName;

      var replacements = new Dictionary<string, string>
				{
					{"Web", String.Join("<br/> ", _webDeployments.Select(x => x.Description))},
					{"MessageBus",String.Join("<br/> ", _messageBusDeployments.Select(x => x.Description))},
					{"InstallerDescription",Configuration.Description },
					{"OldVersion",Releases.Items[0].Version},
					{"NewVersion",Version}
				};

      if (Email.SendEmail(subject, AppDomain.CurrentDomain.BaseDirectory + "/Templates/Email/Install.html", replacements))
      {
        Log("Log Email Sent");
      }
    }

    public void SendEmail(string log, Exception ex)
    {
      if (!Properties.Installer.Default.EmailsEnabled) return;
      Log("Sending Error Email..");
      // Email out the errors that occurrerd.
      using (var stream = new MemoryStream())
      {
        var subject = "Installer Error " + Configuration.Description + " " + Version + " On " + Environment.MachineName;

        var writer = new StreamWriter(stream);
        writer.Write(log);
        stream.Position = 0;

        var replacements = new StringBuilder();
        replacements.Append("<br/> " + ex.Message);
        replacements.Append("<br/>" + ex.InnerException);
        replacements.Append("<br/> " + ex.StackTrace);


        var errorLogAttachment = new Attachment(stream, MediaTypeNames.Application.Octet) { Name = "Installer log " + Configuration.Description + " " + Version + " " + DateTime.Now.ToString("dd-MMM-yyyy HH:mm") + @".log" };

        Email.SendEmail(subject, AppDomain.CurrentDomain.BaseDirectory + "/Templates/Email/Error.html", replacements.ToString(), errorLogAttachment);
        Log("Error Email Sent");
      }
    }

    /// <summary>
    /// Logs the specified details.
    /// </summary>
    /// <param name="details">The details.</param>
    public void Log(string details,bool fileOnly = false)
    {
      if (_verbose)
        _logMethod(details,fileOnly);
    }

    /// <summary>
    /// Logs the status of the install.
    /// </summary>
    /// <param name="details">The details.</param>
    private void Status(string details, bool fileOnly = false)
    {
      _logMethod(details,fileOnly);
    }

    /// <summary>
    /// Reads the input application parameters
    /// </summary>
    private bool ReadArgs(IList<string> args)
    {
      try
      {
        // Get parameters from the input args
        for (var arg = 0; arg < args.Count; arg++)
        {
          switch (args[arg].ToLower())
          {
            case "-config":
            case "-c":
              {
                arg++;
                ConfigPath = args[arg].Trim();
                break;
              }

            case "-releases":
            case "-r":
              {
                arg++;
                ReleasesPath = args[arg].Trim();
                break;
              }

            case "-version":
            case "-v":
              {
                arg++;
                Version = args[arg].Trim();
                break;
              }

            case "-log":
            case "-l":
              {
                arg++;

                var logLevel = args[arg].Trim().ToLowerInvariant();
                _verbose = (logLevel == "verbose");

                break;
              }
            case "-f":
            case "-force":
              {
                arg++;
                _forceRelease = true;
                break;
              }
            case "-b":
            case "-backup":
              {
                arg++;
                _backup = true;
                break;
              }
            case "-h":
            case "-help":
              {
                _showHelp = true;
                break;
              }

            case "":
            case "\n":
              {
                break;
              }

            default:
              throw new ArgumentOutOfRangeException("parameter", args[arg], "Invalid parameter found in the command line.");
          }
        }

        // Validate required params
        Ensure.NotNullOrEmpty<ArgumentNullException>(ConfigPath, "-config");

        // Default 
        if (ReleasesPath.IsNullOrEmpty())
          ReleasesPath = "Releases.xml";
      }
      catch (Exception ex)
      {
        Log(" ");
        Log("Error retrieving the parameters.");
        Log(ex.Message);
        Log(" ");

        return false;
      }

      return true;
    }

    /// <summary>
    /// Shows the help for the application when parameters are incorrect
    /// </summary>
    private void ShowHelp()
    {
      Log(" ");
      Log("Usage for the Focus Installer:");
      Log("Install.exe -config {config} [-versions {versions}] [-currentversion {currentversion}] [-force {force}] [-log {level}");
      Log("  {config}: configuration file the installer is to use.");
      Log("  {releases}: releases file the installer is to use.  If not specified, 'Releases.xml' is used by default.");
      Log("  {version}: version that is being installing.  If not specified then latest version will be selected.");
      Log("	 {force}: force the current version to be updated.");
      Log("	 {backup}: backups will be deleted.");
      Log("  {level}: verbose or non-verbose.");
      Log(" ");
    }

    /// <summary>
    /// Gets the installable releases.
    /// </summary>
    /// <returns></returns>
    private Release[] GetInstallableReleases()
    {
      // I need to ensure that this is going to return all releases that need upgrading
      // If we are on the current release and "force" is set, then we need to ensure we get that latest release

      var releases = Releases.Items.Where(x => String.Compare(Configuration.CurrentVersion, x.Version, StringComparison.OrdinalIgnoreCase) < 0).ToList();

      if (releases.IsNullOrEmpty() || _forceRelease)
      {
        releases = Releases.Items.Where(x => String.Compare(Configuration.CurrentVersion, x.Version, StringComparison.OrdinalIgnoreCase) == 0).ToList();
        Log("Upgrading Current Version Only: " + Configuration.CurrentVersion);
      }
      else
      {
        Log("Upgrading from version " + releases.OrderBy(x => x.Version).FirstOrDefault().Version + " to " + releases.OrderByDescending(x => x.Version).FirstOrDefault().Version);
      }

      if (Version.IsNullOrEmpty() && releases.IsNotNull() && releases.Count > 0)
        Version = releases.OrderByDescending(x => x.Version).FirstOrDefault().Version;

      if (Version.IsNullOrEmpty())
      {
        Log("Cannot derive the version to upgrade to");
        throw new ArgumentNullException("Version", "Cannot derive the version to upgrade to");
      }

      return releases.Where(x => String.Compare(x.Version, Version, StringComparison.OrdinalIgnoreCase) <= 0).ToArray();
    }

    /// <summary>
    /// Gets the deployments.
    /// </summary>
    /// <param name="deployTarget">The deploy target.</param>
    /// <returns></returns>
    private Deployment[] GetDeployments(DeployTarget deployTarget)
    {
      return Configuration.Deployments.Where(x => x.Target == deployTarget).ToArray();
    }

    /// <summary>
    /// Gets the SQL scripts.
    /// </summary>
    /// <param name="version">The version.</param>
    /// <param name="databaseTartget">The database tartget.</param>
    /// <returns></returns>
    private IEnumerable<SqlScript> GetSqlScripts(string version, DatabaseTarget databaseTartget)
    {
      var sqlScripts = new List<SqlScript>();

      var release = _installableReleases.SingleOrDefault(x => x.Version == version);

      if (release.IsNotNull() || release.SqlPath.IsNotNullOrEmpty())
      {
        var sqlScriptPathInfo = new DirectoryInfo(release.SqlPath + "\\" + databaseTartget);

        if (!sqlScriptPathInfo.Exists)
          return sqlScripts;

        foreach (var sqlScriptFileInfo in sqlScriptPathInfo.GetFiles("*.sql"))
          if (!Configuration.ScriptsRun.Any(x => x.Version == release.Version && x.Filename == sqlScriptFileInfo.Name))
          {
            var sql = File.ReadAllText(sqlScriptFileInfo.FullName);
            sqlScripts.Add(new SqlScript { Version = release.Version, Filename = sqlScriptFileInfo.Name, Sql = sql, ScriptType = DatabaseScriptType.Version, ScriptTarget = databaseTartget });
          }
      }

      return sqlScripts.ToArray();
    }

    /// <summary>
    /// Starts the service.
    /// </summary>
    /// <param name="service">The service.</param>
    /// <exception cref="System.InvalidOperationException">Unable to start ' + service.Description + ' service</exception>
    private void StartService(Service service)
    {
      if (!(new ServiceCommand(this, service)).Start())
        throw new InvalidOperationException("Unable to start '" + service.Description + "' service");
    }

    /// <summary>
    /// Stops the service.
    /// </summary>
    /// <param name="service">The service.</param>
    /// <exception cref="System.InvalidOperationException">Unable to stop ' + service.Description + ' service</exception>
    private void StopService(Service service)
    {
      if (!(new ServiceCommand(this, service)).Stop())
        throw new InvalidOperationException("Unable to stop '" + service.Description + "' service");
    }
  }
}
