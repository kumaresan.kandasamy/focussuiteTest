#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.IO;
using System.Xml.Serialization;

using Focus.Installer.Code.Commands;

#endregion

namespace Focus.Installer.Code
{
	[XmlRootAttribute(Namespace = "", IsNullable = false, ElementName = "configuration")]
	public class Configuration
	{	
		#region Attributes
		
		[XmlAttribute("currentVersion")]
		public string CurrentVersion { get; set; }

		[XmlAttribute("deploymentDescription")]
		public string Description { get; set; }

		#endregion
		
		#region Elements

		[XmlArray("databases"), XmlArrayItem("database", IsNullable = false)]
		public Database[] Databases { get; set; }
		
		[XmlArray("deployments"), XmlArrayItem("deployment", IsNullable = false)]
		public Deployment[] Deployments { get; set; }

		[XmlArray("scriptsRun"), XmlArrayItem("script", IsNullable = true)]
		public SqlScript[] ScriptsRun { get; set; }

		[XmlArray("sharedpaths"), XmlArrayItem("path", IsNullable = true)]
		public SharedPath[] SharedPaths {get; set;}

		#endregion		
	}

	public class SharedPath
	{
		[XmlAttribute("name")]
		public string Name {get; set;}

		[XmlText]
		public string Path { get; set; }
	}

	public class Database 
	{
		#region Attributes

		[XmlAttribute("target")]
		public DatabaseTarget Target { get; set; }

		[XmlAttribute("providerName")]
		public string ProviderName { get; set; }

		[XmlAttribute("connectionString")]
		public string ConnectionString { get; set; }

		[XmlArray("scripts"), XmlArrayItemAttribute("script", IsNullable = true)]
		public DatabaseScript[] DatabaseScripts { get; set; }

		#endregion
	}

	public class DatabaseScript
	{
		#region Attributes

		[XmlAttribute("type")]
		public DatabaseScriptType ScriptType { get; set; }

		[XmlAttribute("name")]
		public string Name { get; set; }

		[XmlText]
		public string Script { get; set; }

		#endregion
	}

	public class Deployment 
	{
		#region Attributes

		[XmlAttribute("target")]
		public DeployTarget Target { get; set; }

		[XmlAttribute("description")]
		public string Description { get; set; }

		#endregion

		#region Elements

		[XmlElement("baseToPath")]
		public BaseToPath BaseToPaths {get; set;}

	  [XmlElement("baseFromPath")]
		public BaseFromPath BaseFromPaths {get; set;}

	  [XmlElement("backupToPath")]
		public BackupToPath BackupToPaths {get; set;}

		[XmlElement("service", IsNullable = true)]
		public Service Service { get; set; }

		[XmlArray("fileSets"), XmlArrayItemAttribute("fileSet", IsNullable = true)]
		public FileSet[] FileSets { get; set; }

		#endregion

		[XmlIgnore]
		internal DeployCommand Command { get; set; }
	}

	public class BaseToPath
	{
		[XmlAttribute("sharedPath")]
		public string Sharedpath { get; set; }
		
		[XmlText]
		public string Path { get; set; }
	}

	public class BaseFromPath
	{
		[XmlAttribute("sharedPath")]
		public string Sharedpath { get; set; }
		
		[XmlText]
		public string Path { get; set; }
	}

		public class BackupToPath
	{
		[XmlAttribute("sharedPath")]
		public string Sharedpath { get; set; }
		
		[XmlText]
		public string Path { get; set; }
	}
	
	public class Service
	{
		#region Attributes

		[XmlAttribute("name")]
		public string Name { get; set; }

		[XmlAttribute("server")]
		public string Server { get; set; }

		[XmlAttribute("description")]
		public string Description { get; set; }
		
		#endregion
	}

	public class Backup
	{
		#region Attributes

		[XmlAttribute("from")]
		public string From { get; set; }

		[XmlAttribute("to")]
		public string To { get; set; }

		[XmlAttribute("include")]
		public string Include { get; set; }

		[XmlAttribute("exclude")]
		public string Exclude { get; set; }

		#endregion
	}

	public class FileSet
	{
		#region Elements

		[XmlElement("path")]
		public string Path { get; set; }

		[XmlArray("searchPatterns"), XmlArrayItemAttribute("searchPattern", IsNullable = false)]
		public SearchPattern[] SearchPatterns { get; set; }

		#endregion
	}

	public class SearchPattern
	{
		#region Attributes

		[XmlAttribute("include")]
		public string Include { get; set; }

		[XmlAttribute("exclude")]
		public string Exclude { get; set; }

		[XmlAttribute("searchOption")]
		public SearchOption SearchOption { get; set; }

		#endregion
	}
}
