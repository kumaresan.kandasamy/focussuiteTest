﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

namespace Focus.Installer.Code
{
	public enum CommandType
	{
		Database,
		Deploy,
		Service
	}

	public enum DatabaseTarget
	{
		Core,
		Report,
		Migration,
        Integration,
		Library,
		Configuration,
		Log,
		MessageBus,
		BddData,
		FocusCareer01,
	}

	public enum DatabaseScriptType
	{
		PreDeploy,
		Version,
		PostDeploy
	}

	public enum DeployTarget
	{
		Web,
        IntegrationLayer,
		MessageBus,
		MigrationServices,
		MigrationV1Database,
	}

	public enum ServiceTarget
	{
		Web,
		MessageBus,
	}

	public enum InstallStatus
	{
		Starting,
		LoadedConfiguration,
		StoppedWebs,
		StoppedIntegrationLayers,
		StoppedMessageBuses,
		UpdatedDatabase,
		DeployedWebs,
		DeployedMessageBuses,
		DeployedIntegrationLayers,
		StartingMessageBuses,
		StartingIntegrationLayers,
		StartingWebs,
		RollingBack,
		CleaningUpBackups,
		Finished
	}
}
