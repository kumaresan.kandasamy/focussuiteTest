using System.Xml.Serialization;

namespace Focus.Installer.Code
{
	[XmlRootAttribute(Namespace = "", IsNullable = false, ElementName = "releases")]
	public class Releases
	{			
		#region Elements

		[XmlElement("release", IsNullable = false)]
		public Release[] Items { get; set; }
		
		#endregion		
	}

	public class Release
	{
		#region Attributes

		[XmlAttribute("version")]
		public string Version { get; set; }

		[XmlAttribute("description")]
		public string Description { get; set; }

		[XmlAttribute("sqlPath")]
		public string SqlPath { get; set; }		

		#endregion
	}
}
