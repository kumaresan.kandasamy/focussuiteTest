﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;

using Focus.Installer.Code;
using Focus.Installer.Code.Helpers;

#endregion

namespace Focus.Installer
{	
	class Program
	{
		private static Worker _worker;
		public static StringBuilder _output;

		static void Main(string[] args)
		{
			// Get a new output log
			_output = new StringBuilder();

			// Create a worker and process the args
			_worker = new Worker(Log);
			try
			{
				_worker.Process(args);
			}
			catch (Exception ex)
			{
				Log("Exception raised: " + ex.Message);
				_worker.SendEmail(_output.ToString(), ex);
				FlushOutput();
				Environment.Exit(0);
			}

			FlushOutput();
			// Send Email Installer Completed Succesfully
			_worker.SendEmail();
		}

	  #region Helper Methods

	  /// <summary>
	  /// Logs the specified s.
	  /// </summary>
	  /// <param name="s">The s.</param>
	  /// <param name="fileOnly"></param>
	  private static void Log(string s,bool fileOnly = false)
		{
      if(!fileOnly)
			Console.WriteLine(s);

			_output.Append(s);
			_output.Append(Environment.NewLine);
		}

		/// <summary>
		/// Flushes the output.
		/// </summary>
		private static void FlushOutput()
		{
			Console.WriteLine();

			if (_worker.ConfigPath.IsNotNullOrEmpty())
			{
				var logPath = AppDomain.CurrentDomain.BaseDirectory;
				if (!logPath.EndsWith("\\")) logPath += "\\";

				logPath += Path.GetFileName(_worker.ConfigPath).Replace(".xml", ".log");

				if (File.Exists(logPath)) File.Delete(logPath);

				using (var sw = File.CreateText(logPath))
				{
					sw.WriteLine(_output.ToString());
					sw.Close();
				}
			}

		}
		#endregion
	}
}
