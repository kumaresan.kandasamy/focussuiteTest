﻿USE [Focus????]
GO

IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'FocusAgent')
DROP USER [FocusAgent]
GO


CREATE USER [FocusAgent] FOR LOGIN [FocusAgent] WITH DEFAULT_SCHEMA=[dbo]
GO


USE [Focus????Log]
GO

IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'FocusAgent')
DROP USER [FocusAgent]
GO


CREATE USER [FocusAgent] FOR LOGIN [FocusAgent] WITH DEFAULT_SCHEMA=[dbo]
GO

USE [Focus????Integration]
GO

IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'FocusAgent')
DROP USER [FocusAgent]
GO


CREATE USER [FocusAgent] FOR LOGIN [FocusAgent] WITH DEFAULT_SCHEMA=[dbo]
GO



USE [Focus????Reporting]
GO

IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'FocusAgent')
DROP USER [FocusAgent]
GO


CREATE USER [FocusAgent] FOR LOGIN [FocusAgent] WITH DEFAULT_SCHEMA=[dbo]
GO