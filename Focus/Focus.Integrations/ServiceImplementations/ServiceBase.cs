﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Mindscape.LightSpeed;

using Framework.Logging;
using Framework.Core;

using Focus.Integrations.Data;
using Focus.Integrations.Messages;

#endregion

namespace Focus.Integrations.ServiceImplementations
{
	public class ServiceBase : IDisposable
	{
		protected const string IntegrationPw = "BGT1ntegrat01n";
		protected IFocusIntegrationRepository Repository;

		/// <summary>
		/// Initializes a new instance of the <see cref="ServiceBase"/> class.
		/// </summary>
		public ServiceBase()
		{
			Repository = new FocusIntegrationRepository();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ServiceBase"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		public ServiceBase(IFocusIntegrationRepository repository)
		{
			Repository = repository;
		}

		#region IDisposable Members

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public virtual void Dispose()
		{
			if (Repository != null) Repository.Dispose();
			Repository = null;

			Logger.Reset();
		}

		#endregion

		/// <summary>
		/// Pings the specified request.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public PingResponse Ping(PingRequest request)
		{
			return new PingResponse(request) { Result = new PingResult { Name = request.Name, Server = Environment.MachineName, Time = DateTime.UtcNow } };
		}

		/// <summary>
		/// Formats an error message.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="error">The error.</param>
		/// <param name="args">The args.</param>
		/// <returns></returns>
		public string FormatErrorMessage(ServiceRequest request, ErrorTypes error, params object[] args)
		{
			var s = "";
			if (args != null)
				foreach (var arg in args)
				{
					if (s.Length > 0) s += ", ";
					s += arg;
				}

			return "ErrorType." + error + (s.Length > 0 ? " - Arguments : " + s : "");
		}

		/// <summary>
		/// Logs an action.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="entity">The entity.</param>
		/// <param name="additionalDetails">The additional details.</param>
		protected void LogAction(ServiceRequest request, ActionTypes actionType, Entity<long> entity, string additionalDetails = null)
		{
			string entityName = null;
			long? entityId = null;

			if (entity.IsNotNull())
			{
				entityName = entity.GetType().Name;
				entityId = entity.Id;
			}

			LogAction(request, actionType, entityName, entityId, additionalDetails);
		}

		/// <summary>
		/// Logs the action.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="entityId">The entity id.</param>
		/// <param name="additionalDetails">The additional details.</param>
		protected void LogAction(ServiceRequest request, ActionTypes actionType, string entityName, long? entityId, string additionalDetails = null)
		{
			var action = Repository.ActionTypes.FirstOrDefault(at => at.Name == actionType.ToString());

			if (action == null)
			{
				action = new ActionType { Name = actionType.ToString()};
				Repository.Add(action);
			}

			EntityType entityType = null;

			if (entityName.IsNotNullOrEmpty())
			{
				entityType = Repository.EntityTypes.FirstOrDefault(et => et.Name == entityName);

				if (entityType == null)
				{
					entityType = new EntityType { Name = entityName };
					Repository.Add(entityType);
				}
			}
			
			var actionEvent = new ActionEvent { ActionedOn = DateTime.Now, ActionType = action, EntityId = entityId, EntityType = entityType, AdditionalDetails = additionalDetails };

			Repository.Add(actionEvent);
			Repository.SaveChanges();
		}

		/// <summary>
		/// Validate 2 security levels for a request: ClientTag
		/// </summary>
		/// <param name="request">The request message.</param>
		/// <param name="response">The response message.</param>
		/// <param name="validate">The validation that needs to take place.</param>
		/// <returns></returns>
		protected bool ValidateRequest(ServiceRequest request, ServiceResponse response, Validate validate)
		{
			// Validate client tag. 
			if ((Validate.ClientTag & validate) == Validate.ClientTag)
			{
				if (!IntegrationSettings.IntegrationClientTags.Contains(request.ClientTag)) 
				{
					response.Acknowledgement = AcknowledgementType.Failure;
					response.Message = FormatErrorMessage(request, ErrorTypes.UnknownClientTag);
					return false;
				}
			}

			return true;
		}

		#region Provider Mapping Helpers

		protected long AddProviderMapping(string providerName, long internalId, string externalId, InstanceType instanceType, long? associatedInternalId, string associatedExternalId, InstanceType? associatedInstanceType)
		{
			var providerType = Repository.Query<ProviderType>().FirstOrDefault(at => at.Name == providerName);

			if (providerType == null)
			{
				providerType = new ProviderType { Name = providerName };
				Repository.Add(providerType);
			}

			var alreadyExists = Repository.ProviderMappings.FirstOrDefault(x => x.InternalId == internalId && x.InstanceType == instanceType);

			if (alreadyExists.IsNull())
			{
				var providerMapping = new ProviderMapping
				                      	{
				                      		ProviderType = providerType,
				                      		InternalId = internalId,
				                      		ExternalId = externalId,
				                      		InstanceType = instanceType
				                      	};

				if (associatedInternalId.HasValue && associatedInstanceType.HasValue)
				{
					providerMapping.AssociatedInternalId = associatedInternalId.Value;
					providerMapping.AssociatedExternalId = associatedExternalId;
					providerMapping.AssociatedInstanceType = associatedInstanceType.Value;
				}

				Repository.Add(providerMapping);
				Repository.SaveChanges();

				return providerMapping.Id;
			}

			return alreadyExists.Id;
		}
		/// <summary>
		/// Adds a provider mapping.
		/// </summary>
		/// <param name="providerName">Name of the provider.</param>
		/// <param name="internalId">The internal id.</param>
		/// <param name="externalId">The external id.</param>
		/// <param name="instanceType">Type of the instance.</param>
		/// <returns></returns>
		protected long AddProviderMapping(string providerName, long internalId, string externalId, InstanceType instanceType)
		{
			return AddProviderMapping(providerName, internalId, externalId, instanceType, null, "", null);
		}

		/// <summary>
		/// Lookups the external mapping.
		/// </summary>
		/// <param name="providerMappingRequests">The provider mapping requests.</param>
		/// <returns></returns>
		protected IEnumerable<ProviderMapping> LookupExternalMapping(IEnumerable<ProviderMappingRequest> providerMappingRequests, string providerName)
		{
			var returnValues = new List<ProviderMapping>();

			foreach (var pmr in providerMappingRequests)
			{
				var providerMapping = (from pm in Repository.ProviderMappings
														 join pt in Repository.ProviderTypes on pm.ProviderTypeId equals pt.Id
														 where pm.InternalId == pmr.InternalId && pm.InstanceType == pmr.InstanceType && pt.Name == providerName
														 select pm).SingleOrDefault();	
				
				if (providerMapping.IsNotNull())
					returnValues.Add(providerMapping);
			}

			return returnValues;
		}

		#endregion 

		#region Enums

		/// <summary>
		/// Validation options enum. Used in validation of messages.
		/// </summary>
		[Flags]
		protected enum Validate
		{
			ClientTag = 0x0001,
			All = ClientTag
		}

		#endregion
	}
}
