﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Configuration;

#endregion

namespace Focus.Integrations
{
	public static class IntegrationSettings
	{
		/// <summary>
		/// Gets the integration client tags.
		/// </summary>
		/// <value>The client tags.</value>
		public static string[] IntegrationClientTags
		{
			get { return GetStringArrayWebConfigValue("Focus-IntegrationClientTags", new string[0]); }
		}

		/// <summary>
		/// Gets the session timeout.
		/// </summary>
		/// <value>The session timeout.</value>
		public static int SessionTimeout
		{
			get { return GetIntWebConfigValue("Focus-SessionTimeout", 20); }
		}

		/// <summary>
		/// Gets the Client provider.
		/// </summary>
		/// <value>The employer provider.</value>
		public static string ClientProvider
		{
			get { return GetStringWebConfigValue("Focus-ClientProvider", "Fake"); }
		}

		/// <summary>
		/// Gets the Client provider.
		/// </summary>
		/// <value>The employer provider.</value>
		public static string CareerReportProvider
		{
			get { return GetStringWebConfigValue("Focus-CareerReportProvider", "Fake"); }
		}

		/// <summary>
		/// Gets the Client provider.
		/// </summary>
		/// <value>The employer provider.</value>
		public static string TalentAssistReportProvider
		{
			get { return GetStringWebConfigValue("Focus-TalentAssistReportProvider", "Fake"); }
		}

		/// <summary>
		/// Gets the Client provider.
		/// </summary>
		/// <value>The employer provider.</value>
		public static string ReportProvider
		{
			get { return GetStringWebConfigValue("Focus-ReportProvider", "Fake"); }
		}

		/// <summary>
		/// Gets the default county.
		/// </summary>
		/// <value>The default county.</value>
		public static string DefaultCounty
		{
			get { return GetStringWebConfigValue("Focus-ClientProvider-Default-County", ""); }
		}
		
		#region WebConfig Helpers Methods

		/// <summary>
		/// Gets the string web config value.
		/// </summary>
		/// <param name="key">A key.</param>
		/// <returns></returns>
		private static string GetStringWebConfigValue(string key)
		{
			return ConfigurationManager.AppSettings[key];
		}

		/// <summary>
		/// Gets the int web config value.
		/// </summary>
		/// <param name="key">A key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		private static int GetIntWebConfigValue(string key, int defaultValue)
		{
			var stringValue = GetStringWebConfigValue(key);
			return string.IsNullOrEmpty(stringValue) ? defaultValue : Convert.ToInt32(stringValue);
		}

		/// <summary>
		/// Gets the bool web config value.
		/// </summary>
		/// <param name="key">A key.</param>
		/// <param name="defaultValue">if set to <c>true</c> [default value].</param>
		/// <returns></returns>
		private static bool GetBoolWebConfigValue(string key, bool defaultValue)
		{
			var stringValue = GetStringWebConfigValue(key);
			return string.IsNullOrEmpty(stringValue) ? defaultValue : Convert.ToBoolean(stringValue);
		}

		/// <summary>
		/// Gets the string web config value.
		/// </summary>
		/// <param name="key">A key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		private static string GetStringWebConfigValue(string key, string defaultValue)
		{
			var stringValue = GetStringWebConfigValue(key);
			return string.IsNullOrEmpty(stringValue) ? defaultValue : stringValue;
		}

		/// <summary>
		/// Gets the string array web config value.
		/// </summary>
		/// <param name="key">A key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		private static string[] GetStringArrayWebConfigValue(string key, string[] defaultValue)
		{
			var stringValue = GetStringWebConfigValue(key);
			return string.IsNullOrEmpty(stringValue) ? defaultValue : stringValue.Split(',');
		}

		#endregion
	}
}