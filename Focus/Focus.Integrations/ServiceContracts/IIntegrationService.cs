﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;

using Focus.Integrations.Messages;

#endregion

namespace Focus.Integrations.ServiceContracts
{
	[ServiceContract(Namespace = Constants.ServiceContractNamespace)]
	public interface IIntegrationService
	{
		[OperationContract]
		PingResponse Ping(PingRequest request);
	}
}
