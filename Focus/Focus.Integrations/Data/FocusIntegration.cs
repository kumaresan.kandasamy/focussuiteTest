using System;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Validation;
using Mindscape.LightSpeed.Linq;

namespace Focus.Integrations.Data
{
  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [System.Runtime.Serialization.DataContract]
  [Table("Data.Integration.ActionEvent")]
  public partial class ActionEvent : Entity<long>
  {
    #region Fields
  
    private System.DateTime _actionedOn;
    private System.Nullable<long> _entityId;
    private string _additionalDetails;
    private long _actionTypeId;
    private long _entityTypeId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ActionedOn entity attribute.</summary>
    public const string ActionedOnField = "ActionedOn";
    /// <summary>Identifies the EntityId entity attribute.</summary>
    public const string EntityIdField = "EntityId";
    /// <summary>Identifies the AdditionalDetails entity attribute.</summary>
    public const string AdditionalDetailsField = "AdditionalDetails";
    /// <summary>Identifies the ActionTypeId entity attribute.</summary>
    public const string ActionTypeIdField = "ActionTypeId";
    /// <summary>Identifies the EntityTypeId entity attribute.</summary>
    public const string EntityTypeIdField = "EntityTypeId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("ActionEvents")]
    private readonly EntityHolder<ActionType> _actionType = new EntityHolder<ActionType>();
    [ReverseAssociation("ActionEvents")]
    private readonly EntityHolder<EntityType> _entityType = new EntityHolder<EntityType>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public ActionType ActionType
    {
      get { return Get(_actionType); }
      set { Set(_actionType, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityType EntityType
    {
      get { return Get(_entityType); }
      set { Set(_entityType, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime ActionedOn
    {
      get { return Get(ref _actionedOn, "ActionedOn"); }
      set { Set(ref _actionedOn, value, "ActionedOn"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EntityId
    {
      get { return Get(ref _entityId, "EntityId"); }
      set { Set(ref _entityId, value, "EntityId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string AdditionalDetails
    {
      get { return Get(ref _additionalDetails, "AdditionalDetails"); }
      set { Set(ref _additionalDetails, value, "AdditionalDetails"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="ActionType" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long ActionTypeId
    {
      get { return Get(ref _actionTypeId, "ActionTypeId"); }
      set { Set(ref _actionTypeId, value, "ActionTypeId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="EntityType" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long EntityTypeId
    {
      get { return Get(ref _entityTypeId, "EntityTypeId"); }
      set { Set(ref _entityTypeId, value, "EntityTypeId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [System.Runtime.Serialization.DataContract]
  [Table("Data.Integration.ActionType")]
  [Cached]
  public partial class ActionType : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _name;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";


    #endregion
    
    #region Relationships

    [ReverseAssociation("ActionType")]
    private EntityCollection<ActionEvent> _actionEvents = new EntityCollection<ActionEvent>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<ActionEvent> ActionEvents
    {
      get { return Get(_actionEvents); }
      set { _actionEvents = value; }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [System.Runtime.Serialization.DataContract]
  [Table("Data.Integration.EntityType")]
  [Cached]
  public partial class EntityType : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 200)]
    private string _name;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";


    #endregion
    
    #region Relationships

    [ReverseAssociation("EntityType")]
    private EntityCollection<ActionEvent> _actionEvents = new EntityCollection<ActionEvent>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<ActionEvent> ActionEvents
    {
      get { return Get(_actionEvents); }
      set { _actionEvents = value; }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [System.Runtime.Serialization.DataContract]
  [Table("Data.Integration.ProviderMapping")]
  public partial class ProviderMapping : Entity<long>
  {
    #region Fields
  
    private long _internalId;
    [ValidateLength(0, 36)]
    private string _externalId;
    [ValueField]
    private Focus.Integrations.InstanceType _instanceType;
    private string _associatedExternalId;
    private System.Nullable<long> _associatedInternalId;
    [ValueField]
    private System.Nullable<Focus.Integrations.InstanceType> _associatedInstanceType;
    private long _providerTypeId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the InternalId entity attribute.</summary>
    public const string InternalIdField = "InternalId";
    /// <summary>Identifies the ExternalId entity attribute.</summary>
    public const string ExternalIdField = "ExternalId";
    /// <summary>Identifies the InstanceType entity attribute.</summary>
    public const string InstanceTypeField = "InstanceType";
    /// <summary>Identifies the AssociatedExternalId entity attribute.</summary>
    public const string AssociatedExternalIdField = "AssociatedExternalId";
    /// <summary>Identifies the AssociatedInternalId entity attribute.</summary>
    public const string AssociatedInternalIdField = "AssociatedInternalId";
    /// <summary>Identifies the AssociatedInstanceType entity attribute.</summary>
    public const string AssociatedInstanceTypeField = "AssociatedInstanceType";
    /// <summary>Identifies the ProviderTypeId entity attribute.</summary>
    public const string ProviderTypeIdField = "ProviderTypeId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("ProviderMappings")]
    private readonly EntityHolder<ProviderType> _providerType = new EntityHolder<ProviderType>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public ProviderType ProviderType
    {
      get { return Get(_providerType); }
      set { Set(_providerType, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long InternalId
    {
      get { return Get(ref _internalId, "InternalId"); }
      set { Set(ref _internalId, value, "InternalId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string ExternalId
    {
      get { return Get(ref _externalId, "ExternalId"); }
      set { Set(ref _externalId, value, "ExternalId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Focus.Integrations.InstanceType InstanceType
    {
      get { return Get(ref _instanceType, "InstanceType"); }
      set { Set(ref _instanceType, value, "InstanceType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string AssociatedExternalId
    {
      get { return Get(ref _associatedExternalId, "AssociatedExternalId"); }
      set { Set(ref _associatedExternalId, value, "AssociatedExternalId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> AssociatedInternalId
    {
      get { return Get(ref _associatedInternalId, "AssociatedInternalId"); }
      set { Set(ref _associatedInternalId, value, "AssociatedInternalId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<Focus.Integrations.InstanceType> AssociatedInstanceType
    {
      get { return Get(ref _associatedInstanceType, "AssociatedInstanceType"); }
      set { Set(ref _associatedInstanceType, value, "AssociatedInstanceType"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="ProviderType" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long ProviderTypeId
    {
      get { return Get(ref _providerTypeId, "ProviderTypeId"); }
      set { Set(ref _providerTypeId, value, "ProviderTypeId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [System.Runtime.Serialization.DataContract]
  [Table("Data.Integration.ProviderType")]
  [Cached]
  public partial class ProviderType : Entity<long>
  {
    #region Fields
  
    [ValidateLength(0, 255)]
    private string _name;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";


    #endregion
    
    #region Relationships

    [ReverseAssociation("ProviderType")]
    private EntityCollection<ProviderMapping> _providerMappings = new EntityCollection<ProviderMapping>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<ProviderMapping> ProviderMappings
    {
      get { return Get(_providerMappings); }
      set { _providerMappings = value; }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    #endregion
  }




  /// <summary>
  /// Provides a strong-typed unit of work for working with the FocusIntegration model.
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  public partial class FocusIntegrationUnitOfWork : Mindscape.LightSpeed.UnitOfWork
  {

    public System.Linq.IQueryable<ActionEvent> ActionEvents
    {
      get { return this.Query<ActionEvent>(); }
    }
    
    public System.Linq.IQueryable<ActionType> ActionTypes
    {
      get { return this.Query<ActionType>(); }
    }
    
    public System.Linq.IQueryable<EntityType> EntityTypes
    {
      get { return this.Query<EntityType>(); }
    }
    
    public System.Linq.IQueryable<ProviderMapping> ProviderMappings
    {
      get { return this.Query<ProviderMapping>(); }
    }
    
    public System.Linq.IQueryable<ProviderType> ProviderTypes
    {
      get { return this.Query<ProviderType>(); }
    }
    
  }

#if LS3_DTOS

  namespace Contracts.Data
  {
    [System.Runtime.Serialization.DataContract(Name="FocusIntegrationDtoBase")]
    [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
    public partial class FocusIntegrationDtoBase
    {
    }

    [System.Runtime.Serialization.DataContract(Name="ActionEvent")]
    [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
    public partial class ActionEventDto : FocusIntegrationDtoBase
    {
      [System.Runtime.Serialization.DataMember]
      public System.DateTime ActionedOn { get; set; }
      [System.Runtime.Serialization.DataMember]
      public System.Nullable<long> EntityId { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string AdditionalDetails { get; set; }
      [System.Runtime.Serialization.DataMember]
      public long ActionTypeId { get; set; }
      [System.Runtime.Serialization.DataMember]
      public long EntityTypeId { get; set; }
    }

    [System.Runtime.Serialization.DataContract(Name="ActionType")]
    [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
    public partial class ActionTypeDto : FocusIntegrationDtoBase
    {
      [System.Runtime.Serialization.DataMember]
      public string Name { get; set; }
    }

    [System.Runtime.Serialization.DataContract(Name="EntityType")]
    [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
    public partial class EntityTypeDto : FocusIntegrationDtoBase
    {
      [System.Runtime.Serialization.DataMember]
      public string Name { get; set; }
    }

    [System.Runtime.Serialization.DataContract(Name="ProviderMapping")]
    [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
    public partial class ProviderMappingDto : FocusIntegrationDtoBase
    {
      [System.Runtime.Serialization.DataMember]
      public long InternalId { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string ExternalId { get; set; }
      [System.Runtime.Serialization.DataMember]
      public Focus.Integrations.InstanceType InstanceType { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string AssociatedExternalId { get; set; }
      [System.Runtime.Serialization.DataMember]
      public System.Nullable<long> AssociatedInternalId { get; set; }
      [System.Runtime.Serialization.DataMember]
      public System.Nullable<Focus.Integrations.InstanceType> AssociatedInstanceType { get; set; }
      [System.Runtime.Serialization.DataMember]
      public long ProviderTypeId { get; set; }
    }

    [System.Runtime.Serialization.DataContract(Name="ProviderType")]
    [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
    public partial class ProviderTypeDto : FocusIntegrationDtoBase
    {
      [System.Runtime.Serialization.DataMember]
      public string Name { get; set; }
    }


    
    [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
    public static partial class FocusIntegrationDtoExtensions
    {
      static partial void CopyFocusIntegrationDtoBase(Entity entity, FocusIntegrationDtoBase dto);
      static partial void CopyFocusIntegrationDtoBase(FocusIntegrationDtoBase dto, Entity entity);

      static partial void BeforeCopyActionEvent(ActionEvent entity, ActionEventDto dto);
      static partial void AfterCopyActionEvent(ActionEvent entity, ActionEventDto dto);
      static partial void BeforeCopyActionEvent(ActionEventDto dto, ActionEvent entity);
      static partial void AfterCopyActionEvent(ActionEventDto dto, ActionEvent entity);
      
      private static void CopyActionEvent(ActionEvent entity, ActionEventDto dto)
      {
        BeforeCopyActionEvent(entity, dto);
        CopyFocusIntegrationDtoBase(entity, dto);
        dto.ActionedOn = entity.ActionedOn;
        dto.EntityId = entity.EntityId;
        dto.AdditionalDetails = entity.AdditionalDetails;
        dto.ActionTypeId = entity.ActionTypeId;
        dto.EntityTypeId = entity.EntityTypeId;
        AfterCopyActionEvent(entity, dto);
      }
      
      private static void CopyActionEvent(ActionEventDto dto, ActionEvent entity)
      {
        BeforeCopyActionEvent(dto, entity);
        CopyFocusIntegrationDtoBase(dto, entity);
        entity.ActionedOn = dto.ActionedOn;
        entity.EntityId = dto.EntityId;
        entity.AdditionalDetails = dto.AdditionalDetails;
        entity.ActionTypeId = dto.ActionTypeId;
        entity.EntityTypeId = dto.EntityTypeId;
        AfterCopyActionEvent(dto, entity);
      }
      
      public static ActionEventDto AsDto(this ActionEvent entity)
      {
        ActionEventDto dto = new ActionEventDto();
        CopyActionEvent(entity, dto);
        return dto;
      }
      
      public static ActionEvent CopyTo(this ActionEventDto source, ActionEvent entity)
      {
        CopyActionEvent(source, entity);
        return entity;
      }

      static partial void BeforeCopyActionType(ActionType entity, ActionTypeDto dto);
      static partial void AfterCopyActionType(ActionType entity, ActionTypeDto dto);
      static partial void BeforeCopyActionType(ActionTypeDto dto, ActionType entity);
      static partial void AfterCopyActionType(ActionTypeDto dto, ActionType entity);
      
      private static void CopyActionType(ActionType entity, ActionTypeDto dto)
      {
        BeforeCopyActionType(entity, dto);
        CopyFocusIntegrationDtoBase(entity, dto);
        dto.Name = entity.Name;
        AfterCopyActionType(entity, dto);
      }
      
      private static void CopyActionType(ActionTypeDto dto, ActionType entity)
      {
        BeforeCopyActionType(dto, entity);
        CopyFocusIntegrationDtoBase(dto, entity);
        entity.Name = dto.Name;
        AfterCopyActionType(dto, entity);
      }
      
      public static ActionTypeDto AsDto(this ActionType entity)
      {
        ActionTypeDto dto = new ActionTypeDto();
        CopyActionType(entity, dto);
        return dto;
      }
      
      public static ActionType CopyTo(this ActionTypeDto source, ActionType entity)
      {
        CopyActionType(source, entity);
        return entity;
      }

      static partial void BeforeCopyEntityType(EntityType entity, EntityTypeDto dto);
      static partial void AfterCopyEntityType(EntityType entity, EntityTypeDto dto);
      static partial void BeforeCopyEntityType(EntityTypeDto dto, EntityType entity);
      static partial void AfterCopyEntityType(EntityTypeDto dto, EntityType entity);
      
      private static void CopyEntityType(EntityType entity, EntityTypeDto dto)
      {
        BeforeCopyEntityType(entity, dto);
        CopyFocusIntegrationDtoBase(entity, dto);
        dto.Name = entity.Name;
        AfterCopyEntityType(entity, dto);
      }
      
      private static void CopyEntityType(EntityTypeDto dto, EntityType entity)
      {
        BeforeCopyEntityType(dto, entity);
        CopyFocusIntegrationDtoBase(dto, entity);
        entity.Name = dto.Name;
        AfterCopyEntityType(dto, entity);
      }
      
      public static EntityTypeDto AsDto(this EntityType entity)
      {
        EntityTypeDto dto = new EntityTypeDto();
        CopyEntityType(entity, dto);
        return dto;
      }
      
      public static EntityType CopyTo(this EntityTypeDto source, EntityType entity)
      {
        CopyEntityType(source, entity);
        return entity;
      }

      static partial void BeforeCopyProviderMapping(ProviderMapping entity, ProviderMappingDto dto);
      static partial void AfterCopyProviderMapping(ProviderMapping entity, ProviderMappingDto dto);
      static partial void BeforeCopyProviderMapping(ProviderMappingDto dto, ProviderMapping entity);
      static partial void AfterCopyProviderMapping(ProviderMappingDto dto, ProviderMapping entity);
      
      private static void CopyProviderMapping(ProviderMapping entity, ProviderMappingDto dto)
      {
        BeforeCopyProviderMapping(entity, dto);
        CopyFocusIntegrationDtoBase(entity, dto);
        dto.InternalId = entity.InternalId;
        dto.ExternalId = entity.ExternalId;
        dto.InstanceType = entity.InstanceType;
        dto.AssociatedExternalId = entity.AssociatedExternalId;
        dto.AssociatedInternalId = entity.AssociatedInternalId;
        dto.AssociatedInstanceType = entity.AssociatedInstanceType;
        dto.ProviderTypeId = entity.ProviderTypeId;
        AfterCopyProviderMapping(entity, dto);
      }
      
      private static void CopyProviderMapping(ProviderMappingDto dto, ProviderMapping entity)
      {
        BeforeCopyProviderMapping(dto, entity);
        CopyFocusIntegrationDtoBase(dto, entity);
        entity.InternalId = dto.InternalId;
        entity.ExternalId = dto.ExternalId;
        entity.InstanceType = dto.InstanceType;
        entity.AssociatedExternalId = dto.AssociatedExternalId;
        entity.AssociatedInternalId = dto.AssociatedInternalId;
        entity.AssociatedInstanceType = dto.AssociatedInstanceType;
        entity.ProviderTypeId = dto.ProviderTypeId;
        AfterCopyProviderMapping(dto, entity);
      }
      
      public static ProviderMappingDto AsDto(this ProviderMapping entity)
      {
        ProviderMappingDto dto = new ProviderMappingDto();
        CopyProviderMapping(entity, dto);
        return dto;
      }
      
      public static ProviderMapping CopyTo(this ProviderMappingDto source, ProviderMapping entity)
      {
        CopyProviderMapping(source, entity);
        return entity;
      }

      static partial void BeforeCopyProviderType(ProviderType entity, ProviderTypeDto dto);
      static partial void AfterCopyProviderType(ProviderType entity, ProviderTypeDto dto);
      static partial void BeforeCopyProviderType(ProviderTypeDto dto, ProviderType entity);
      static partial void AfterCopyProviderType(ProviderTypeDto dto, ProviderType entity);
      
      private static void CopyProviderType(ProviderType entity, ProviderTypeDto dto)
      {
        BeforeCopyProviderType(entity, dto);
        CopyFocusIntegrationDtoBase(entity, dto);
        dto.Name = entity.Name;
        AfterCopyProviderType(entity, dto);
      }
      
      private static void CopyProviderType(ProviderTypeDto dto, ProviderType entity)
      {
        BeforeCopyProviderType(dto, entity);
        CopyFocusIntegrationDtoBase(dto, entity);
        entity.Name = dto.Name;
        AfterCopyProviderType(dto, entity);
      }
      
      public static ProviderTypeDto AsDto(this ProviderType entity)
      {
        ProviderTypeDto dto = new ProviderTypeDto();
        CopyProviderType(entity, dto);
        return dto;
      }
      
      public static ProviderType CopyTo(this ProviderTypeDto source, ProviderType entity)
      {
        CopyProviderType(source, entity);
        return entity;
      }


    }

  }

#endif
}
