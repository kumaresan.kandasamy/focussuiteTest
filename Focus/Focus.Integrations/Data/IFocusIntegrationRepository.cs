﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;

namespace Focus.Integrations.Data
{
	public interface IFocusIntegrationRepository : IDisposable
	{
		#region Core Methods

		/// <summary>
		/// Adds the specified entity to the repository.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="entity">The entity.</param>
		void Add<T>(T entity) where T : Entity;

		/// <summary>
		/// Removes the specified entity from the repository.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="entity">The entity.</param>
		void Remove<T>(T entity) where T : Entity;

		/// <summary>
		/// Removes the entity / entities from the repository from the specified where expression.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="selector">The selector.</param>
		void Remove<T>(Expression<Func<T, bool>> selector) where T : Entity;

		/// <summary>
		/// Removes the entity / entities from the repository using the specified query.
		/// </summary>
		/// <param name="query">The query.</param>
		void Remove(Query query);

		/// <summary>
		/// Updates the entity / entities in the repository using the specified query.
		/// </summary>
		/// <param name="query">The query.</param>
		/// <param name="attributes">The attributes.</param>
		void Update(Query query, object attributes);

		/// <summary>
		/// Counts all the entities in the repository.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		long Count<T>() where T : Entity;

		/// <summary>
		/// Counts the entities in the repository for the specified where expression.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="selector">The selector.</param>
		/// <returns></returns>
		long Count<T>(Expression<Func<T, bool>> selector) where T : Entity;

		/// <summary>
		/// Checks if the entities exists in the repository for the specified where expression.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="selector">The selector.</param>
		/// <returns></returns>
		bool Exists<T>(Expression<Func<T, bool>> selector) where T : Entity;

		/// <summary>
		/// Finds the entity by the id.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		T FindById<T>(object id) where T : Entity;

		/// <summary>
		/// Finds a list of all entities in the repository.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		IList<T> Find<T>() where T : Entity;

		/// <summary>
		/// Finds a list of entities in the repository using the given the specified where expression.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="selector">The selector.</param>
		/// <returns></returns>
		IList<T> Find<T>(Expression<Func<T, bool>> selector) where T : Entity;

		/// <summary>
		/// Queries this repository using a Linq query.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		IQueryable<T> Query<T>() where T : Entity;

		/// <summary>
		/// Saves the changes.
		/// </summary>
		void SaveChanges();

		/// <summary>
		/// Saves the changes to the repository.
		/// </summary>
		/// <param name="reset">if set to <c>true</c> then the repository is reset.</param>
		void SaveChanges(bool reset);

		#endregion

		#region Entity Queryable Helpers

		/// <summary>
		/// Gets the action events.
		/// </summary>
		/// <value>The action events.</value>
		IQueryable<ActionEvent> ActionEvents { get; }

		/// <summary>
		/// Gets the action types.
		/// </summary>
		/// <value>The action types.</value>
		IQueryable<ActionType> ActionTypes { get; }

		/// <summary>
		/// Gets the type of the entity.
		/// </summary>
		/// <value>The type of the entity.</value>
		IQueryable<EntityType> EntityTypes { get; }

		/// <summary>
		/// Gets the type of the entity.
		/// </summary>
		/// <value>The type of the entity.</value>
		IQueryable<ProviderMapping> ProviderMappings { get; }

		/// <summary>
		/// Gets the type of the entity.
		/// </summary>
		/// <value>The type of the entity.</value>
		IQueryable<ProviderType> ProviderTypes { get; }

		#endregion
	}
}