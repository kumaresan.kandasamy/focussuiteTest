﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

#endregion

namespace Focus.Integrations.Data
{
	public class ProviderMappingRequest
	{
		public long InternalId { get; set; }
		public string Key { get; set; }
		public string ExternalId { get; set; }
		public InstanceType InstanceType { get; set; }
	}
}
