﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

#endregion

namespace Focus.Integrations
{
	public class Constants
	{
		public const string ServiceContractNamespace = "http://www.burning-glass.com/at/integration/services/";
		public const string DataContractNamespace = "http://www.burning-glass.com/at/integration/types/";
	}
}
