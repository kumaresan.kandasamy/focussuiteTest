﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

#endregion

namespace Focus.Integrations
{
	#region Action Types

	public enum ActionTypes
	{
		// No Action
		NoAction,

		// Employer 
		ValidateEmployerFein,
	}

	#endregion

	#region Error Types

	/// <summary>
	/// Enumeration of error types.
	/// </summary>
	public enum ErrorTypes
	{
		// Localisation test - dont localise this
		NotUsed = 0,

		// General Errors
		Unknown = 1,
		NotAllowed = 2,
		UnknownClientTag = 3,
		InvalidSessionId = 4,
		LogInRequired = 5,
		ServiceCallFailed = 6,
		CorrelationFailed = 7,

		// Account Errors
		InvalidUserNameOrPassword = 100,
		UnableToChangePassword = 101,
		UserNameAlreadyExists = 102,
		UnableToProcessForgottenPassword = 103,
		MustSupplyUserNameOrEmailAddress = 104,
		UserNotFound = 105,
		UserNameIsNull = 106,
		UserNotEnabled = 107,
		UserEmailAddressNotFound = 108,
		UnableToChangeUserName = 109,

		// Candidate
		ResumeNotFound = 200,
		PostJobFailed = 201,
		CandidateNotFound = 202,
		CandidateSearchFailed = 203,
		AccessCandidateFailed = 204,
		CreateCandidateFailed = 205,
		GetSystemDefaultsFailed = 206,
		UpdateSystemDefaultsFailed = 207,
		SaveJobSeekerFailed = 208
	}


	#endregion

	#region Provider mapping

	public enum InstanceType
	{
		Employer = 1,
		Employee = 2,
		
		JobSeeker = 21, 
 
		Job = 41,

		Admin = 61,

		Office = 81,

		State = 101, 
		County = 102,
		Country =103,

		WorkShifts = 121,

		Titles = 131,

		LookUpJobSeekerActivity = 501,
		LookUpEmployeeActivity = 601

	}

	#endregion 
}
