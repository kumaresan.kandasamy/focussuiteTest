﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

using Framework.Logging;

#endregion

namespace Focus.Integrations
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public abstract class ServiceRequest 
	{
		/// <summary>
		/// A client tag identifies the client application performing the call.
		/// </summary>
		[DataMember]
		public string ClientTag { get; set; }

		/// <summary>
		/// A unique identifier issued by the client representing the instance 
		/// of the request. Avoids rapid-fire processing of the same request over and over 
		/// in denial-of-service type attacks.
		/// 
		/// The RequestId will be passed to the profiling and logging processes
		/// 
		/// </summary>
		[DataMember]
		public Guid RequestId { get; set; }

		/// <summary>
		/// Gets or sets the external user id.
		/// </summary>
		[DataMember]
		public string ExternalUserId { get; set; }

		/// <summary>
		/// Gets or sets the name of the external user.
		/// </summary>
		[DataMember]
		public string ExternalUserName { get; set; }

		/// <summary>
		/// Gets or sets the external password.
		/// </summary>
		[DataMember]
		public string ExternalPassword { get; set; }

		/// <summary>
		/// Gets or sets the external office id.
		/// </summary>
		[DataMember]
		public string ExternalOfficeId { get; set; }

		/// <summary>
		/// Creates a ILogData class from the service request.
		/// </summary>
		/// <returns></returns>
		public ILogData LogData()
		{
			return new LogData { SessionId = Guid.Empty, RequestId = RequestId, UserId = -1 };
		}
	}
}
