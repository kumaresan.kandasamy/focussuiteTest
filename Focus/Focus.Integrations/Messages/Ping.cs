﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

using Focus.Integrations.Data;

#endregion

namespace Focus.Integrations.Messages
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class PingRequest : ServiceRequest
	{
		[DataMember]
		public string Name { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class PingResponse : ServiceResponse
	{
		public PingResponse() { }

		public PingResponse(PingRequest request) : base(request) { }

		[DataMember]
		public PingResult Result { get; set; }
	}
}
