﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

using Framework.Logging;

#endregion

namespace Focus.Integrations
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class LogData : ILogData
	{
		/// <summary>
		/// Gets or sets the session id.
		/// </summary>
		/// <value>The session id.</value>
		[DataMember]
		public Guid SessionId { get; set; }

		/// <summary>
		/// Gets or sets the request id.
		/// </summary>
		/// <value>The request id.</value>
		[DataMember]
		public Guid RequestId { get; set; }

		/// <summary>
		/// Gets or sets the user id.
		/// </summary>
		/// <value>The user id.</value>
		[DataMember]
		public long UserId { get; set; }
	}
}
