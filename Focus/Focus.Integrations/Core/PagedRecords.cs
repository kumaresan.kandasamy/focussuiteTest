﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

#endregion

namespace Focus.Integrations
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class PagedRecords<T> : IPagedRecords<T>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="PagedRecords&lt;T&gt;"/> class.
		/// </summary>
		public PagedRecords()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="PagedRecords&lt;T&gt;"/> class.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="pageSize">Size of the page.</param>
		public PagedRecords(IEnumerable<T> source, int pageIndex, int pageSize) : this(source == null ? new List<T>().AsQueryable() : source.AsQueryable(), pageIndex, pageSize)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="PagedRecords&lt;T&gt;"/> class.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="pageSize">Size of the page.</param>
		private PagedRecords(IQueryable<T> source, int pageIndex, int pageSize)
		{
			TotalRecords = source.Count(); ;
			TotalPages = TotalRecords / pageSize;

			if (TotalRecords % pageSize > 0)
				TotalPages++;

			PageSize = pageSize;
			PageIndex = pageIndex;

			TotalRecords = source.Count();

			Data = (pageIndex == 0 ? source.Take(pageSize).ToList() : source.Skip(pageIndex*pageSize).Take(pageSize).ToList());
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PagedRecords&lt;T&gt;"/> class.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="totalRecords">The total records.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <param name="pageSource">if set to <c>true</c> [page source].</param>
		public PagedRecords(IEnumerable<T> source, int totalRecords, int pageIndex, int pageSize, bool pageSource = true)
		{
			TotalRecords = totalRecords;
			TotalPages = totalRecords / pageSize;

			if (totalRecords % pageSize > 0)
				TotalPages++;

			PageSize = pageSize;
			PageIndex = pageIndex;

			if (pageSource)
				Data = (pageIndex == 0 ? source.Take(pageSize).ToList() : source.Skip(pageIndex * pageSize).Take(pageSize).ToList());
			else
				Data = source.ToList();
		}

		[DataMember]
		public int TotalPages { get; set; }

		[DataMember]
		public int TotalRecords { get; set; }

		[DataMember]
		public int PageIndex { get; set; }

		[DataMember]
		public int PageSize { get; set; }

		[DataMember]
		public IEnumerable<T> Data { get; set; }
	}

	public interface IPagedRecords<T>
	{
		int TotalPages { get; set; }
		int TotalRecords { get; set; }
		int PageIndex { get; set; }
		int PageSize { get; set; }
	}
}
