﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI;

using AjaxControlToolkit;

using Focus.Common.Localisation;
using Focus.Core;
using Framework.Core;

#endregion

namespace Focus.Common.Controls
{
	public abstract class MasterPageBase : MasterPage
	{
		public bool IsAssist { get; private set; }
		public bool IsTalent { get; private set; }
    public bool IsExplorer { get; private set; }
    public bool IsCareer { get; private set; }
    public bool DisableAssistAuthentication { get { return App.Settings.DisableAssistAuthentication; } }
    public bool DisableTalentAuthentication { get { return App.Settings.DisableAssistAuthentication; } }
		
		// AJ Removing the below line of code caused the application to stop working, after speaking to AC he has advised to make it return false
		//public bool DisableChangePassword { get { return App.IsSSO && App.Settings.SSOManageAccountUrl.IsNullOrEmpty(); } }
		public bool DisableChangePassword { get { return false; } }
		
    private int? _resumeCount;

		protected IApp App = new HttpApp();

		/// <summary>
		/// Gets the resume count.
		/// </summary>
    public int ResumeCount
    {
      get
      {
        if (_resumeCount == null)
					_resumeCount = App.User.IsAuthenticated && !App.GetSessionValue<bool>("ReactivateRequestKey") ? ServiceClientLocator.ResumeClient(App).GetResumeCount() : 0;

        return _resumeCount.Value;
      }
    }

		/// <summary>
		/// Gets or sets a value indicating whether [demo shield validated].
		/// </summary>
		protected bool DemoShieldValidated
		{
			get { return App.GetSessionValue("DemoShieldValidated", false); }
			set { App.SetSessionValue("DemoShieldValidated", value); }
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			// Establish if this is the Talent or Assist module
			IsTalent = (Module == FocusModules.Talent);
      IsAssist = (Module == FocusModules.Assist);
      IsExplorer = (Module == FocusModules.Explorer || Module == FocusModules.CareerExplorer);
      IsCareer = (Module == FocusModules.Career || Module == FocusModules.CareerExplorer);
		}

		/// <summary>
		/// Localises the Page (from Html) using the specified key and if not found it will return the default value.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <param name="args">The args.</param>
		/// <returns></returns>
		protected string HtmlLocalise(string key, string defaultValue, params object[] args)
		{
			if (!key.StartsWith("Global"))
				key = (GetType().BaseType ?? GetType()).FullName + "." + key;
			return Localiser.Instance().Localise(key, defaultValue, args);
		}

		/// <summary>
		/// Gets the global script manager.
		/// </summary>
		/// <value>The script manager.</value>
		internal ToolkitScriptManager GlobalScriptManager
		{
			get { return GetToolkitScriptManager(); }
		}
		
		/// <summary>
		/// Gets the debug info.
		/// </summary>
		/// <value>The debug info.</value>
		protected string DebugInfo
		{
			get
			{
				const string debugInfo = "Version: {0}\r\nSession: {1}\r\nRequest: {2}";
				return string.Format(debugInfo, App.VersionInfo, App.SessionId, App.RequestId);
			}
		}

		/// <summary>
		/// Gets the version info.
		/// </summary>
		/// <value>The version info.</value>
		protected string VersionInfo
		{
			get
			{
			  return App.Settings.DisplayServerName
          ? string.Concat(App.VersionInfo, " - ", Server.MachineName)
          : App.VersionInfo;
			}
		}

		/// <summary>
		/// Gets the module.
		/// </summary>
		/// <value>The module.</value>
		public FocusModules Module 
		{
			get { return GetModule(); }
		}

		/// <summary>
		/// Shows the error.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="closeLink">The close link.</param>
		/// <param name="height">The height.</param>
		/// <param name="width">The width.</param>
		/// <param name="top">The top.</param>
		public void ShowError(string message = "", string closeLink = "", int height = 20, int width = 400, int top = 210)
		{
			DisplayError(message, closeLink, height, width, top);
		}

		/// <summary>
		/// Shows the error.
		/// </summary>
		/// <param name="alertType">Type of the alert.</param>
		/// <param name="message">The message.</param>
		public void ShowError(AlertTypes alertType, string message = "")
		{
			DisplayError(alertType, message);
		}

		/// <summary>
		/// Shows the modal alert.
		/// </summary>
		/// <param name="alertType">Type of the alert.</param>
		/// <param name="message">The message.</param>
		/// <param name="title">The title.</param>
		/// <param name="closeLink">The close link.</param>
		/// <param name="width">The width.</param>
		public void ShowModalAlert(AlertTypes alertType, string message, string title = "", string closeLink = "",
		                                       int width = 400)
		{
			DisplayModalAlert(alertType, message, title, closeLink, width);
		}

		#region Abstract Methods

		protected abstract ToolkitScriptManager GetToolkitScriptManager();
		protected abstract FocusModules GetModule();

		protected abstract void DisplayError(string message = "", string closeLink = "", int height = 20, int width = 400, int top = 210);
		protected abstract void DisplayError(AlertTypes alertType, string message = "");

		protected abstract void DisplayModalAlert(AlertTypes alertType, string message, string title = "", string closeLink = "",
		                                       int width = 400);

		protected abstract void ShowChangeOfficeModal(Object sender, EventArgs e);
		
		#endregion
	}
}
