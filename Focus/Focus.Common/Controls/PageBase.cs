﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using AjaxControlToolkit;
using BgtSSO;
using Focus.Common;
using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using Focus.Common.Code.ResultHandlers;
using Focus.Common.Helpers;
using Focus.Common.Models;
using Focus.Web.Core.Models;
using Framework.Core;
using Framework.PageState;

using Focus;
using Focus.Common.Controls;
using Focus.Common.Localisation;
using Focus.Core.Models;
using Focus.Core;
using Constants = Focus.Core.Constants;

#endregion

public class PageBase : Page, IPage
{
	private IApp _app;
	private PageStatePersister _pageStatePersister;
	private DateTime? _inTime;
	private string _declaringType, _method, _parameters;
	private HiddenField _returnUrlHiddenField;

	private IResultHandler _resultHandler;
	private IPageController _pagecontroller;
	// Commented out below as it was raising a compiler warning
	// private IViewModel _viewmodel;

	/// <summary>
	/// Gets the application.
	/// </summary>
	/// <value>
	/// The application.
	/// </value>
	protected IApp App
	{
		get { return _app ?? (_app = new HttpApp()); }
	}

	/// <summary>
	/// Gets the page controller.
	/// </summary>
	/// <value>
	/// The page controller.
	/// </value>
	protected IPageController PageControllerBaseProperty
	{
		get { return _pagecontroller ?? (_pagecontroller = RegisterPageController()); }
	}

	/// <summary>
	/// Gets the result handler.
	/// </summary>
	/// <value>
	/// The result handler.
	/// </value>
	protected IResultHandler ResultHandler
	{
		get { return _resultHandler ?? (_resultHandler = RegisterResultHandler()); }
	}

	/// <summary>
	/// Registers the page controller.
	/// </summary>
	/// <returns></returns>
	protected virtual IPageController RegisterPageController()
	{
		return null;
	}

	/// <summary>
	/// Registers the result handler.
	/// </summary>
	/// <returns></returns>
	protected virtual IResultHandler RegisterResultHandler()
	{
		return new DefaultResultHandler();
	}

	/// <summary>
	/// Gets the application.
	/// </summary>
	/// <value>
	/// The application.
	/// </value>
	IApp IPage.App
	{
		get { return App; }
	}

	/// <summary>
	/// Handles the specified controller result.
	/// </summary>
	/// <param name="controllerResult">The controller result.</param>B
	/// <returns></returns>
	protected bool Handle(IControllerResult controllerResult)
	{
		return ResultHandler.Handle(controllerResult, this);
	}
	
  /// <summary>
	/// Gets the master page.
	/// </summary>
	/// <value>The master page.</value>
	protected MasterPageBase MasterPage
	{
		get { return Master as MasterPageBase; }
	}

	/// <summary>
	/// Gets the script manager.
	/// </summary>
	/// <value>The script manager.</value>
	protected ToolkitScriptManager GlobalScriptManager
	{
		get
		{
			if (MasterPage.IsNotNull())
				return MasterPage.GlobalScriptManager;

			return null;
		}
	}

	/// <summary>
	/// Gets the <see cref="T:System.Web.UI.PageStatePersister"/> object associated with the page.
	/// </summary>
	/// <value></value>
	/// <returns>A <see cref="T:System.Web.UI.PageStatePersister"/> associated with the page.</returns>
	protected override PageStatePersister PageStatePersister
	{
		get
		{
			// Unlike as exemplified in the MSDN docs, we cannot simply return a new PageStatePersister 
			// every call to this property, as it causes problems 
			return _pageStatePersister ?? (_pageStatePersister = PageStateManager.NewPageStatePersister(this));
		}
	}

	/// <summary>
	/// Gets a value indicating whether this instance is single sign on.
	/// </summary>
	protected virtual bool IsSingleSignOn { get { return false; } }

	/// <summary>
	/// Gets or sets a value indicating whether [demo shield validated].
	/// </summary>
	protected bool DemoShieldValidated
	{
		get { return App.GetSessionValue("DemoShieldValidated", false); }
		set { App.SetSessionValue("DemoShieldValidated", value); }
	}

    protected override void OnLoadComplete(EventArgs e)
    {
        foreach (HtmlLink ctrl in Page.Header.Controls.OfType<HtmlLink>())
        {
            string url = Page.ClientScript.GetWebResourceUrl(typeof(ClientCssResourceAttribute), "Calendar.Calendar_resource.css");
            if (url == ctrl.Href)
                Page.Header.Controls.Remove(ctrl);
        }
    }

    /// <summary>
	/// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
	/// </summary>
	/// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
	protected override void OnLoad(EventArgs e)
	{
		if (!Request.Url.AbsolutePath.Contains("/maintenance") && App.Settings.SiteUnderMaintenance)
			Response.Redirect("~/maintenance", true);

		if (Master != null)
			_returnUrlHiddenField = (HiddenField)Master.FindControl("ReturnUrl");

		if (!IsPostBack)
		{
			var returnUrl = GetSessionValue<string>("Page:PageOperator");

      if (!String.IsNullOrWhiteSpace(returnUrl) && _returnUrlHiddenField.IsNotNull())
				_returnUrlHiddenField.Value = returnUrl;			
		}

		if (!Request.Url.AbsolutePath.Contains("/error"))
		{
			if (!String.IsNullOrEmpty(App.Settings.DemoShieldCustomerCode) && !DemoShieldValidated)
			{
				if (IsValidDemoShieldUrl())
				{
					DemoShieldValidated = true;
					Response.Redirect(Request.Url.AbsolutePath, true);
				}
				else
				{
					if (!String.IsNullOrEmpty(App.Settings.DemoShieldCustomerCode) && !String.IsNullOrEmpty(App.Settings.DemoShieldRedirectUrl))
						Response.Redirect(App.Settings.DemoShieldRedirectUrl, true);
					else
					{
						App.Exception = new ExceptionModel(CodeLocalise("ErrorType.DemoAccessNotAuthorised", "You are not authorized to access this demo site."));;
						Response.Redirect("~/error", true);
					}
				}
			}
		}

    if (App.Settings.Module == FocusModules.CareerExplorer)
      if (App.User.IsAuthenticated)
        ScriptManager.RegisterStartupScript(this, GetType(), "FocusScript", "DisplaySessionTimeout();", true);

		base.OnLoad(e);

		ApplyGlobalCssSelector();
	}

	/// <summary>
	/// Determines whether [is valid demo shield URL].
	/// </summary>
	private bool IsValidDemoShieldUrl()
	{
		if (IsSingleSignOn)
			return true;

		var isValidDemoShieldUrl = false;

		var bgInstCode = GetQueryStringParameter("bginstcode");
		var bgAuthKey = GetQueryStringParameter("bgauthkey");
		var bgAppId = GetQueryStringParameter("bgappid");
		var cl = GetQueryStringParameter("cl");

		if (!String.IsNullOrEmpty(bgInstCode) && !String.IsNullOrEmpty(bgAuthKey) && !String.IsNullOrEmpty(bgAppId) && !String.IsNullOrEmpty(cl))
		{
			int customerNameLength;

			if (Int32.TryParse(cl, out customerNameLength))
			{
				var urlParams = new string[4];
				var urlData = Encoding.ASCII.GetString(Convert.FromBase64String(bgInstCode + bgAuthKey + bgAppId));
				var urlParamSequence = urlData.Substring(0, 4);

				urlData = urlData.Substring(4, urlData.Length - 4);

				foreach (char paramSequence in urlParamSequence)
				{
					switch (paramSequence)
					{
						case '1':
							{
								urlParams[0] = urlData.Substring(0, customerNameLength);
								if (urlData.Length > customerNameLength)
									urlData = urlData.Substring(customerNameLength, urlData.Length - customerNameLength);

								break;
							}

						case '2':
							{
								urlParams[1] = urlData.Substring(0, 6);
								if (urlData.Length > 6)
									urlData = urlData.Substring(6, urlData.Length - 6);

								break;
							}

						case '3':
							{
								urlParams[2] = urlData.Substring(0, 3);
								if (urlData.Length > 3)
									urlData = urlData.Substring(3, urlData.Length - 3);

								break;
							}

						case '4':
							{
								urlParams[3] = urlData.Substring(0, 8);
								if (urlData.Length > 8)
									urlData = urlData.Substring(8, urlData.Length - 8);

								break;
							}
					}
				}

				if (urlParams[0] == App.Settings.DemoShieldCustomerCode 
					&& urlParams[1] == DateTime.Today.GetJulianDays().ToString(CultureInfo.InvariantCulture) 
					&& urlParams[2] == "BGT" 
					&& urlParams[3] == DateTime.Today.ToString("yyyyMMdd"))
				{
					isValidDemoShieldUrl = true;
				}
			}
		}

		return isValidDemoShieldUrl;
	}

	/// <summary>
	/// Gets the query string parameter.
	/// </summary>
	/// <param name="key">The key.</param>
	/// <returns></returns>
	private string GetQueryStringParameter(string key)
	{
		return Request.QueryString[key];
	}

	#region Profiling methods

	/// <summary>
	/// Raises the <see cref="E:System.Web.UI.Control.Init" /> event to initialize the page.
	/// </summary>
	/// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
	protected override void OnInit(EventArgs e)
	{
		if (!Page.ClientScript.IsStartupScriptRegistered(GetType(), "MaskedEditFix"))
			Page.ClientScript.RegisterStartupScript(GetType(), "MaskedEditFix", String.Format("<script type='text/javascript' src='{0}'></script>", Page.ResolveUrl("~/Assets/Scripts/MaskedEditFix.js")));

	}

	/// <summary>
	/// Raises the <see cref="E:System.Web.UI.Page.PreInit"/> event at the beginning of page initialization.
	/// </summary>
	/// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
	protected override void OnPreInit(EventArgs e)
	{
		_inTime = DateTime.UtcNow;
		_declaringType = "WebForm : " + AppRelativeVirtualPath.Replace("~/", "");
		_method = (IsPostBack ? "Postback" : "Render");
		_parameters = Request.Url.PathAndQuery;

		base.OnPreInit(e);		
	}

	/// <summary>
	/// Enables a server control to perform final clean up before it is released from memory.
	/// </summary>
	public override void Dispose()
	{
		base.Dispose();

		// Do the profiling work
		if (_inTime != null)
		{
			// How long did it take
			var outTime = DateTime.UtcNow;
			var milliseconds = Convert.ToInt64(outTime.Subtract(_inTime.Value).TotalMilliseconds);

			ServiceClientLocator.CoreClient(App).ProfileEvent(Environment.MachineName, _inTime.Value, outTime, milliseconds, _declaringType, _method, _parameters);
		}
	}

	#endregion

	#region Localisation methods

	/// <summary>
	/// Localises the Page (from Html) using the specified key and if not found it will return the default value.
	/// </summary>
	/// <param name="key">The key.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <param name="args">The args.</param>
	/// <returns></returns>
	protected string HtmlLocalise(string key, string defaultValue, params object[] args)
	{
		return Localiser.Instance().Localise(this, key, defaultValue, args);
	}

	/// <summary>
	/// Localises the Page (from code) using the specified key and if not found it will return the default value.
	/// </summary>
	/// <param name="key">The key.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <param name="args">The args.</param>
	/// <returns></returns>
	protected string CodeLocalise(string key, string defaultValue, params object[] args)
	{
		return Localiser.Instance().Localise(this, key, defaultValue, args);
	}

	/// <summary>
	/// Localises the Enum using the specified key.
	/// </summary>
	/// <param name="enumValue">The enum value.</param>
	/// <returns></returns>
	protected string CodeLocalise(Enum enumValue)
	{
		return Localiser.Instance().Localise(enumValue);
	}

	/// <summary>
	/// Localises the Enum using the specified key and if not found returns the default value
	/// </summary>
	/// <param name="enumValue">The enum value.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <returns></returns>
	protected string CodeLocalise(Enum enumValue, string defaultValue)
	{
		return Localiser.Instance().Localise(enumValue, defaultValue);
	}

	#endregion

	#region View helpers

	/// <summary>
	/// Builds the key using a prefix.
	/// </summary>
	/// <param name="key">The key.</param>
	/// <returns>A simple or prefixed key</returns>
	protected string BuildKey(string key)
	{
		return "Focus:" + key;
	}

	/// <summary>
	/// Gets the viewstate value for the specified key.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="key">The key.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <returns></returns>
	protected T GetViewStateValue<T>(string key, T defaultValue = default(T))
	{
		var obj = ViewState[BuildKey(key)];
		return (obj == null ? defaultValue : (T)obj);
	}

	/// <summary>
	/// Sets the viewstate value for the specified key.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="key">The key.</param>
	/// <param name="value">The value.</param>
	protected void SetViewStateValue<T>(string key, T value)
	{
		ViewState[BuildKey(key)] = value;
	}

	#endregion

	#region Sorting methods

	/// <summary>
	/// Adds an up- or down-arrow image to GridView header.
	/// </summary>
	/// <param name="grid">Gridview.</param>
	/// <param name="row">Header row of gridview.</param>
	protected void AddGlyph(GridView grid, GridViewRow row)
	{
		// Find the column sorted on
		for (var i = 0; i < grid.Columns.Count; i++)
		{
			if (grid.Columns[i].SortExpression == SortColumn)
			{
				// Add a space between header and symbol
				var literal = new Literal {Text = "&nbsp;"};
				row.Cells[i].Controls.Add(literal);

				var image = new Image
				            	{
				            		ImageUrl = (SortDirection == "ASC" ? "~/assets/images/app/sortasc.gif" : "~/assets/images/app/sortdesc.gif"),
				            		Width = 9,
				            		Height = 5
				            	};

				row.Cells[i].Controls.Add(image);

				return;
			}
		}
	}

	/// <summary>
	/// Gets or sets the current column being sorted on.
	/// </summary>
	protected string SortColumn
	{
		get { return GetViewStateValue<string>("SortColumn"); }
		set { SetViewStateValue("SortColumn", value); }
	}

	/// <summary>
	/// Gets or sets the current sort direction (ascending or descending).
	/// </summary>
	protected string SortDirection
	{
		get { return GetViewStateValue<string>("SortDirection"); }
		set { SetViewStateValue("SortDirection", value); }
	}

	/// <summary>
	/// Gets the Sort expression for the current sort settings.
	/// </summary>
	protected string SortExpression
	{
		get { return SortColumn + " " + SortDirection; }
	}

	#endregion

	#region Other methods

  /// <summary>
  /// Gets the edit localisation link HTML.
  /// </summary>
  /// <param name="key">The lookup key.</param>
  /// <returns></returns>
  protected string EditLocalisationLink(string key)
  {

    if (!App.User.IsInRole(Constants.RoleKeys.SystemAdmin))
    {
      return String.Empty;
    }
    key = Localiser.Instance().GenerateKey(this, key);
    return
      string.Format(
				@"<a href=""#"" onkeypress=""javascript:OpenLocalisePopup('{0}','{1}');"" onclick=""javascript:OpenLocalisePopup('{0}','{1}');""><img src=""{2}"" alt=""Edit image"" border=""0"" height=""10px"" width=""10px""></a>",
        key, HttpContext.Current.Request.ApplicationPath, UrlHelper.BrandAssetPath("Images/Edit.gif", _app));

  }


	/// <summary>
	/// Logs the user in.
	/// </summary>
	/// <param name="userName">Name of the user.</param>
	/// <param name="password">The password.</param>
	/// <param name="moduleToLoginTo">The module to login to.</param>
	/// <param name="validationUrl">The validation URL.</param>
	/// <returns></returns>
	protected UserContext LogIn(string userName, string password, FocusModules moduleToLoginTo, string validationUrl)
	{
		var loggedInModel = (App.Settings.SSOEnabled || App.Settings.SamlEnabledForTalent)
			                                  ? ServiceClientLocator.AuthenticationClient(App).LogIn(App.GetSessionValue<SSOProfile>("SSOProfileKey"), moduleToLoginTo)
			                                  : ServiceClientLocator.AuthenticationClient(App).LogIn(userName.Trim(), password.Trim(), moduleToLoginTo, validationUrl); 

		if (loggedInModel != null)
		{
			App.UserData.Load(loggedInModel.UserData);

			var authTicket = new FormsAuthenticationTicket(1, loggedInModel.UserContext.ScreenName ?? loggedInModel.UserContext.FirstName, DateTime.Now, DateTime.Now.AddMinutes(App.Settings.UserSessionTimeout), false, loggedInModel.UserContext.SerializeUserContext());
			var encryptedTicket = FormsAuthentication.Encrypt(authTicket);

			var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
			Response.Cookies.Add(authCookie);

			return loggedInModel.UserContext;
		}

		return null;
	}

	/// <summary>
	/// HTML for a label.
	/// </summary>
	/// <param name="key">The key.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <returns></returns>
	protected string HtmlLabel(string key, string defaultValue)
	{
		return CodeLocalise(key, defaultValue);
	}

    /// <summary>
    /// Generates the HTML for a label element.
    /// </summary>
    /// <param name="forControl">The control to be labelled.</param>
    /// <param name="key">The key.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <param name="isRequired">if set to <c>true</c> [is required].</param>
    /// <returns></returns>
    protected string HtmlLabel(Control forControl, string key, string defaultValue)
	{
        return string.Format("<label for=\"{0}\">{1}</label>", forControl.ClientID, CodeLocalise(key, defaultValue));
	}
        

	/// <summary>
	/// HTML for an enum based label.
	/// </summary>
	/// <param name="enumValue">The enum value.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <returns></returns>
	protected string HtmlLabel(Enum enumValue, string defaultValue)
	{
		return CodeLocalise(enumValue, defaultValue);
	}

	/// <summary>
	/// HTML for an in field label.
	/// </summary>
	/// <param name="forControlId">For control id.</param>
	/// <param name="key">The key.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <param name="width">The width.</param>
	/// <param name="cssClass">The CSS class.</param>
	/// <returns></returns>
	protected string HtmlInFieldLabel(string forControlId, string key, string defaultValue, int width, string cssClass = "inFieldLabel")
	{
		return string.Format("<span class=\"{3}\"><label for=\"{0}\" style=\"width: {1}px\">{2}</label></span>", forControlId, width, CodeLocalise(key, defaultValue), cssClass);
	}

	/// <summary>
	/// HTML for a  required label.
	/// </summary>
	/// <param name="key">The key.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <returns></returns>
	[Obsolete("This method is now deprecated as it does not render an HTML label, leading to accessibilty issues, particularly with Jaws. Use HtmlRequiredLabel(Control, string, string) instead.")]
	protected string HtmlRequiredLabel(string key, string defaultValue)
	{
		return string.Format("{0}&nbsp;<img src='{1}' alt='{2}' />", CodeLocalise(key, defaultValue), UrlHelper.BrandAssetPath("Images/asteriskRequired.gif", App), CodeLocalise("Global.Required.Text", "required"));
	}

	/// <summary>
	/// HTML for a required label.
	/// </summary>
	/// <param name="forControl">The control to associate the label with.</param>
	/// <param name="key">The localisation key.</param>
	/// <param name="defaultValue">The default value for localisation.</param>
	/// <returns></returns>
	protected string HtmlRequiredLabel(Control forControl, string key, string defaultValue)
	{
		return string.Format("<label for=\"{0}\">{1}&nbsp;<img src=\"{2}\" alt=\"{3}\" /></label>", forControl.ClientID,
												 CodeLocalise(key, defaultValue), UrlHelper.BrandAssetPath("Images/asteriskRequired.gif", App),
		                     CodeLocalise("Global.Required.Text", "required"));
	}

	/// <summary>
	/// HTML for a required fields instruction.
	/// </summary>
	/// <returns></returns>
	protected string HtmlRequiredFieldsInstruction()
	{
		var requiredFields = CodeLocalise("Global.RequiredFields.Text", "required fields");
		return string.Format("<img src='{0}' alt='{1}' />&nbsp;<span class='instructions'>{2}</span>", UrlHelper.BrandAssetPath("Images/asteriskRequired.gif", App), requiredFields, requiredFields);
	}

	/// <summary>
	/// HTMLs the tooltip with arrow.
	/// </summary>
	/// <param name="tooltipKey">The tooltip key.</param>
	/// <param name="tooltipDefault">The tooltip default.</param>
	/// <param name="tooltipHeaderKey">The tooltip header key.</param>
	/// <param name="tooltipHeaderDefault">The tooltip header default.</param>
	/// <returns></returns>
	protected string HtmlTooltip(string tooltipKey, string tooltipDefault, string tooltipHeaderKey = null, string tooltipHeaderDefault = null)
	{
		const string html = @"<span class=""tooltip helpMessage"">
	<span class=""tooltipImage""></span>
	<span class=""tooltipPopup tooltipPopupWithArrow"">
		<span class=""arrow""></span>{0}
		<span class=""content"">{1}</span>
	</span>
</span>";

		const string htmlHeader = @"
		<span class=""header"">{0}</span>";

		if (tooltipHeaderKey.IsNotNullOrEmpty())
			return string.Format(html, string.Format(htmlHeader, Localiser.Instance().Localise(tooltipHeaderKey, tooltipHeaderDefault)), Localiser.Instance().Localise(tooltipKey, tooltipDefault));

		return string.Format(html, "", Localiser.Instance().Localise(tooltipKey, tooltipDefault));
	}

	/// <summary>
	/// HTMLs the tooltip with arrow.
	/// </summary>
	/// <param name="tooltipKey">The tooltip key.</param>
	/// <param name="tooltipDefault">The tooltip default.</param>
	/// <returns></returns>
	protected string HtmlTooltipWithArrow(string tooltipKey, string tooltipDefault)
	{
		const string html = @"<div class=""tooltipWithArrow helpMessage"">
	<div class=""tooltipImage""></div> 
	<div class=""tooltipPopup"">
		<div class=""tooltipPopupInner"">
			<div class=""arrow""></div>
			{0} 
			</div>
	</div> 
</div>";

		return string.Format(html, Localiser.Instance().Localise(tooltipKey, tooltipDefault));
	}

	/// <summary>
	/// Renders the HTML for the jQuery Tooltipster plugin.
	/// </summary>
	/// <param name="baseClass">The base tooltip CSS class to apply.</param>
	/// <param name="tooltipKey">The tooltip key.</param>
	/// <param name="tooltipDefault">The tooltip default.</param>
	/// <returns></returns>
	protected string HtmlTooltipster(string baseClass, string tooltipKey, string tooltipDefault)
	{
		const string html = @"<div class=""{0} helpMessage"">
<div class=""tooltipImage toolTipNew"" title=""{1}""></div>
</div>";

		var text = tooltipKey.IsNullOrEmpty() ? tooltipDefault : Localiser.Instance().Localise(tooltipKey, tooltipDefault);
		return string.Format(html, baseClass, HttpUtility.HtmlAttributeEncode(text));
	}

	/// <summary>
	/// Formats the error.
	/// </summary>
	/// <param name="errorType">Type of the error.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <returns></returns>
	protected string FormatError(ErrorTypes errorType, string defaultValue)
	{
		return CodeLocalise(errorType, defaultValue);
	}

	/// <summary>
	/// Shows the error.
	/// </summary>
	/// <param name="message">The message.</param>
	/// <param name="closeLink">The close link.</param>
	/// <param name="height">The height.</param>
	/// <param name="width">The width.</param>
	/// <param name="top">The top.</param>
	protected void ShowError(string message = "", string closeLink = "", int height = 20, int width = 400, int top = 210)
	{
		if(MasterPage.IsNotNull()) MasterPage.ShowError(message, closeLink, height, width, top);
		else
		{
			if(message.IsNullOrEmpty())
			{
				var lastError = App.GetSessionValue<string>(Constants.StateKeys.PageError);
				message = (lastError.IsNullOrEmpty()) ? lastError : "The last handled error could not be found.";
			}
			throw new Exception(message);
		}
	}

	/// <summary>
	/// Uses the IsMobile property value added to Request.Browser by the FiftyOne Degrees http module to 
	/// determine if the requesting device is a mobile device, and add a css class to the global form.
	/// </summary>
	private void ApplyGlobalCssSelector()
	{
		if(Page.Form != null) Page.Form.Attributes.Add("class", (Request.Browser["IsMobile"] == "True") ? "device" : "desktop");
	}

  /// <summary>
  /// Registers a startup script containing a JSON object populated with any values from code required in external javascript files.
  /// </summary>
  /// <param name="scriptKey">The unique script key with which to register the script.</param>
  /// <param name="jsonVariable">The name of the JSON object to populate.</param>
  /// <param name="codeValues">A <see cref="T:System.Collections.Specialized.NameValueCollection"/> containing the code variables to serialize to JSON.</param>
  protected void RegisterCodeValuesJson(string scriptKey, string jsonVariable, NameValueCollection codeValues)
  {
    var dict = codeValues.Keys.Cast<string>().ToDictionary(key => key, key => codeValues[key]);
    var json = new JavaScriptSerializer().Serialize(dict);
    var script = string.Format("{0} = {1};", jsonVariable, json);
    ScriptManager.RegisterStartupScript(this, GetType(), scriptKey, script, true);
  }

	#endregion

	#region Page Operator Methods

	/// <summary>
	/// Gets the return URL.
	/// </summary>
	/// <returns></returns>
	protected string GetReturnUrl()
	{
		var returnUrls = GetReturnUrls();
		
		return returnUrls.Split('|')[0];
	}

	/// <summary>
	/// Gets the return urls.
	/// </summary>
	/// <returns></returns>
	protected string GetReturnUrls()
	{
		return (_returnUrlHiddenField != null && !String.IsNullOrWhiteSpace(_returnUrlHiddenField.Value) ? _returnUrlHiddenField.Value : "");
	}

	/// <summary>
	/// Uses the return URL.
	/// </summary>
	protected string UseReturnUrl()
	{
		var returnUrls = _returnUrlHiddenField != null && !String.IsNullOrWhiteSpace(_returnUrlHiddenField.Value) ? _returnUrlHiddenField.Value : "";
		var returnUrl = returnUrls.Split('|')[0];

		if (returnUrl.IsNullOrEmpty())
			return "";

		SetSessionValue("Page:PageOperator", (returnUrls.Length > returnUrl.Length) ? returnUrls.Substring(returnUrl.Length + 1) : "");

		return returnUrl.Split('|')[0];
	}

	/// <summary>
	/// Sets the return URL.
	/// </summary>
	protected void SetReturnUrl()
	{
		SetReturnUrl(Request.Url.AbsoluteUri);
	}

	/// <summary>
	/// Sets the return URL.
	/// </summary>
	/// <param name="url">The URL.</param>
	protected void SetReturnUrl(string url)
	{
		var currentReturnUrls = GetReturnUrls();

		SetSessionValue("Page:PageOperator", url + '|' + currentReturnUrls);
	}

	/// <summary>
	/// Gets the session value for the specified key.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="key">The key.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <returns></returns>
	private static T GetSessionValue<T>(string key, T defaultValue = default(T))
	{
		var obj = (HttpContext.Current.Session != null ? HttpContext.Current.Session[key] : default(T));
		return (obj == null ? defaultValue : (T)obj);
	}

	/// <summary>
	/// Sets the session value for the specified key.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="key">The key.</param>
	/// <param name="value">The value.</param>
	private static void SetSessionValue<T>(string key, T value)
	{
		if (HttpContext.Current.Session != null)
			HttpContext.Current.Session[key] = value;
	}

	#endregion
	
	#region Career Javascript Registration

	protected void RegisterClientStartupScript(string scriptKey)
	{
		var sManager = ScriptManager.GetCurrent(Page);

		var scriptText = @"function pageLoad() {
	                    $(document).ready(function()
	                    {";

		switch (scriptKey)
		{
			case "HomeScript":
				scriptText += @"ApplyCustomSelectStyling($('.desktop select'));
													$('.fileupload').customFileInput('Browse', 'Browse', 'Browse');
													$('.inFieldLabel > label, .inFieldLabelAlt > label, .inFieldLabelNoBreak > label').inFieldLabels();
													SetupAccordionNew();
													SetupCollapsiblePanel();";
				break;

			case "YourResumeScript":
				scriptText += @"ApplyCustomSelectStyling($('.desktop select'));
													$('.inFieldLabel > label, .inFieldLabelAlt > label, .inFieldLabelNoBreak > label').inFieldLabels();
													$('.fileupload').customFileInput('Browse', 'Browse', 'Browse');";
				break;

			case "ResumeWizardScript":
				scriptText += @"ApplyCustomSelectStyling($('.desktop select'));
													BindTooltips();
													SetupCollapsiblePanel();
													$('.inFieldLabel > label, .inFieldLabelAlt > label, .inFieldLabelNoBreak > label').inFieldLabels();";
				break;

      case "JobMatchingScript":
				scriptText += @"ApplyCustomSelectStyling($('.desktop select'));
													$('.accordionContent').hide();
													$('.multipleAccordionTitle.on').next().show();
													$('.inFieldLabel > label, .inFieldLabelAlt > label, .inFieldLabelNoBreak > label').inFieldLabels();
													if (window.BindTooltips) BindTooltips();";
        break;

      case "JobMatchingScriptAssist":
        scriptText += @"$('.desktop select').customSelect();
													$('.inFieldLabel > label, .inFieldLabelAlt > label, .inFieldLabelNoBreak > label').inFieldLabels();
													if (window.BindTooltips) BindTooltips();";
        break;

      case "AssistToolTip":
				scriptText += @"BindTooltips();";
				break;

			default:
				scriptText += @"if (typeof ApplyCustomSelectStyling !== 'undefined') { ApplyCustomSelectStyling($('.desktop select:not(.form-control)'), 'refresh'); } else { $('.desktop select').customSelect(); }
													$('.inFieldLabel > label, .inFieldLabelAlt > label, .inFieldLabelNoBreak > label').inFieldLabels();
													if (window.BindTooltips) BindTooltips();";
				break;

		}

		scriptText += @"});
                      }";

		if (sManager != null && sManager.IsInAsyncPostBack)
		{
			//if a MS AJAX request, use the Scriptmanager class
			ScriptManager.RegisterStartupScript(Page, Page.GetType(), scriptKey, scriptText, true);
		}
		else
		{
			//if a standard postback, use the standard ClientScript method
			scriptText = string.Concat("Sys.Application.add_load(function(){", scriptText, "});");
			Page.ClientScript.RegisterStartupScript(Page.GetType(), scriptKey, scriptText, true);
		}
	}

	#endregion
}

public interface IPage
{
	IApp App { get; }
}

