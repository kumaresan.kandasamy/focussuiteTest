﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Focus.Common;
using Framework.Core;

using Focus;
using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using Focus.Common.Code.ResultHandlers;
using Focus.Common.Controls;
using Focus.Common.Extensions;
using Focus.Common.Helpers;
using Focus.Common.Localisation;
using Focus.Core;

#endregion

public class UserControlBase : UserControl
{
	private IApp _app;
	private IResultHandler _resultHandler;
	private IPageController _pagecontroller;

  protected const string EmptyDateString = "__/__/____";

	/// <summary>
	/// Gets the page controller.
	/// </summary>
	/// <value>
	/// The page controller.
	/// </value>
	protected IPageController PageControllerBaseProperty
	{
		get { return _pagecontroller ?? (_pagecontroller = RegisterPageController()); }
	}

	/// <summary>
	/// Gets the result handler.
	/// </summary>
	/// <value>
	/// The result handler.
	/// </value>
	protected IResultHandler ResultHandler
	{
		get { return _resultHandler ?? (_resultHandler = RegisterResultHandler()); }
	}

	/// <summary>
	/// Registers the page controller.
	/// </summary>
	/// <returns></returns>
	public virtual IPageController RegisterPageController()
	{
		return null;
	}

	/// <summary>
	/// Registers the result handler.
	/// </summary>
	/// <returns></returns>
	public virtual IResultHandler RegisterResultHandler()
	{
		return new DefaultResultHandler();
	}

	/// <summary>
	/// Handles the specified controller result.
	/// </summary>
	/// <param name="controllerResult">The controller result.</param>B
	/// <returns></returns>
	protected virtual bool Handle(IControllerResult controllerResult)
	{
		return ResultHandler.Handle(controllerResult, Page as PageBase);
	}

	/// <summary>
	/// Gets the master page.
	/// </summary>
	/// <value>The master page.</value>
	protected MasterPageBase MasterPage
	{
		get { return Page.Master is MasterPageBase ? (MasterPageBase) Page.Master : null; }
	}

	/// <summary>
	/// Gets the application.
	/// </summary>
	/// <value>
	/// The application.
	/// </value>
	protected IApp App
	{
		get
		{
			var page = Page as IPage;			
			_app = (page != null) ? page.App : new HttpApp();

			return _app;
		}
	}

	/// <summary>
	/// Gets the script manager.
	/// </summary>
	/// <value>The script manager.</value>
	protected ToolkitScriptManager GlobalScriptManager
	{
		get { return Page.Master is MasterPageBase ? ((MasterPageBase) Page.Master).GlobalScriptManager : null; }
	}

  protected override void OnLoad(EventArgs e)
  {
    if (App.Settings.Module == FocusModules.CareerExplorer)
    {
			if (App.User.IsAuthenticated)
        ScriptManager.RegisterStartupScript(this,
                                            GetType(),
                                            "FocusScript",
                                            "DisplaySessionTimeout();",
                                            true);
    }

    base.OnLoad(e);
  }

	#region Localisation methods

  /// <summary>
  /// Resets the current localiser
  /// </summary>
  protected void ResetLocaliser(bool resetDictionary = false)
  {
    Localiser.Instance().Clear(resetDictionary);
  }

	/// <summary>
	/// Localises the Page (from Html) using the specified key and if not found it will return the default value.
	/// </summary>
	/// <param name="key">The key.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <param name="args">The args.</param>
	/// <returns></returns>
	protected string HtmlLocalise(string key, string defaultValue, params object[] args)
	{
		return Localiser.Instance().Localise(this, key, defaultValue, args);
	}

	/// <summary>
	/// Localises the Page (from code) using the specified key and if not found it will return the default value.
	/// </summary>
	/// <param name="key">The key.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <param name="args">The args.</param>
	/// <returns></returns>
	protected string CodeLocalise(string key, string defaultValue, params object[] args)
	{
		return Localiser.Instance().Localise(this, key, defaultValue, args);
	}

  /// <summary>
  /// Localises the Enum using the specified key.
  /// </summary>
  /// <param name="enumValue">The enum value.</param>
  /// <param name="useAnnotation">Whether to use the annotation for the enum as a default</param>
  /// <returns></returns>
  protected string CodeLocalise(Enum enumValue, bool useAnnotation = false)
	{
    return useAnnotation 
            ? Localiser.Instance().Localise(enumValue) 
            : Localiser.Instance().Localise(enumValue, "** " + enumValue + " **");
	}

	/// <summary>
	/// Localises the Enum using the specified key and if not found returns the default value
	/// </summary>
	/// <param name="enumValue">The enum value.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <returns></returns>
	protected string CodeLocalise(Enum enumValue, string defaultValue)
	{
		return Localiser.Instance().Localise(enumValue, defaultValue);
	}

	/// <summary>
	/// Formats the error.
	/// </summary>
	/// <param name="errorType">Type of the error.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <returns></returns>
	protected string FormatError(ErrorTypes errorType, string defaultValue)
	{
		return CodeLocalise(errorType, defaultValue);
	}

	#endregion

	#region View, Session and Context helpers

	/// <summary>
	/// Builds the key using a prefix.
	/// </summary>
	/// <param name="key">The key.</param>
	/// <returns>A simple or prefixed key</returns>
	protected string BuildKey(string key)
	{
		return Constants.StateKeyPrefix + ":" + key;
	}

	/// <summary>
	/// Gets the viewstate value for the specified key.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="key">The key.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <returns></returns>
	protected T GetViewStateValue<T>(string key, T defaultValue = default(T))
	{
		var obj = ViewState[BuildKey(key)];
		return (obj == null ? defaultValue : (T)obj);
	}

	/// <summary>
	/// Sets the viewstate value for the specified key.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="key">The key.</param>
	/// <param name="value">The value.</param>
	protected void SetViewStateValue<T>(string key, T value)
	{
		ViewState[BuildKey(key)] = value;
	}

	#endregion

	#region Dynamic script helpers

	/// <summary>
	/// Registers a startup script containing a JSON object populated with any values from code required in external javascript files.
	/// </summary>
	/// <param name="scriptKey">The unique script key with which to register the script.</param>
	/// <param name="jsonVariable">The name of the JSON object to populate.</param>
	/// <param name="codeValues">A <see cref="T:System.Collections.Specialized.NameValueCollection"/> containing the code variables to serialize to JSON.</param>
	protected void RegisterCodeValuesJson(string scriptKey, string jsonVariable, NameValueCollection codeValues)
	{
		var dict = codeValues.Keys.Cast<string>().ToDictionary(key => key, key => codeValues[key]);
		var json = new JavaScriptSerializer().Serialize(dict);
		var script = string.Format("{0} = {1};", jsonVariable, json);
		ScriptManager.RegisterStartupScript(this, GetType(), scriptKey, script, true);
	}

	#endregion

	#region Other

  /// <summary>
  /// Gets the edit localisation link HTML.
  /// </summary>
  /// <param name="key">The lookup key.</param>
  /// <returns></returns>
  protected string EditLocalisationLink(string key)
  {
    if (!App.User.IsInRole(Constants.RoleKeys.SystemAdmin))
    {
      return string.Empty;
    }
    
    key = Localiser.Instance().GenerateKey(this, key);
    return
      string.Format(
				@"<a href=""#"" onkeypress=""javascript:OpenLocalisePopup('{0}','{1}');"" onclick=""javascript:OpenLocalisePopup('{0}','{1}');""><img src=""{2}"" alt=""Edit Image"" border=""0"" height=""10px"" width=""10px""></a>",
        key,HttpContext.Current.Request.ApplicationPath, UrlHelper.BrandAssetPath("Images/Edit.gif", _app));
  }

  /// <summary>
	/// Gets the lookup id for a lookup key.
	/// </summary>
	/// <param name="lookupKey">The lookup key.</param>
	/// <returns></returns>
	protected long GetLookupId(string lookupKey)
	{
		try
		{
			if (lookupKey.IsNullOrEmpty())
				throw new ArgumentNullException();

			// Get LookupType for lookup key
			var lookupType = Constants.CodeItemKeys.GetLookupType(lookupKey);

			// Get the Lookups for the lookup type and lookup key
			return (ServiceClientLocator.CoreClient(App).GetLookup(lookupType, key: lookupKey)[0]).Id;
		}
		catch (Exception ex)
		{			
			throw new Exception("GetLookupId failed getting lookup for " + lookupKey, ex);
		}
	}

	/// <summary>
	/// Gets the control render id.
	/// </summary>
	/// <param name="id">The id.</param>
	/// <returns></returns>
	protected string GetControlRenderId(string id)
	{
		var control = Page.FindControlRecursive(id);
		return (control != null ? control.ClientID : "");
	}

	/// <summary>
	/// HTML for a label.
	/// </summary>
	/// <param name="key">The key.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <returns></returns>
	protected string HtmlLabel(string key, string defaultValue)
	{
		return CodeLocalise(key, defaultValue);
	}

	/// <summary>
	/// HTML for an enum based label.
	/// </summary>
	/// <param name="enumValue">The enum value.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <returns></returns>
	protected string HtmlLabel(Enum enumValue, string defaultValue)
	{
		return CodeLocalise(enumValue, defaultValue);
	}

    /// <summary>
    /// HTMLs the label.
    /// </summary>
    /// <param name="enumValue">The enum value.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <returns></returns>
    protected string HtmlLabel(Control forControl, Enum enumValue, string defaultValue)
    {
        return string.Format("<label for=\"{0}\">{1}</label>", forControl.ClientID, CodeLocalise(enumValue, defaultValue));
    }

  /// <summary>
  /// Generates the HTML for a label element.
  /// </summary>
  /// <param name="forControl">The control to be labelled.</param>
  /// <param name="key">The key.</param>
  /// <param name="defaultValue">The default value.</param>
  /// <param name="isRequired">if set to <c>true</c> [is required].</param>
  /// <returns></returns>
	protected string HtmlLabel(Control forControl, string key, string defaultValue, bool isRequired = false)
	{
		return isRequired
      ? HtmlRequiredLabel(forControl, key, defaultValue)
      : string.Format("<label for=\"{0}\">{1}</label>", forControl.ClientID, CodeLocalise(key, defaultValue));
	}

	/// <summary>
	/// HTML for an in field label.
	/// </summary>
	/// <param name="forControlId">For control id.</param>
	/// <param name="key">The key.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <param name="width">The width.</param>
	/// <param name="cssClass">The CSS class.</param>
	/// <returns></returns>
	protected string HtmlInFieldLabel(string forControlId, string key, string defaultValue, int? width, string cssClass = "inFieldLabel")
	{
		return width.HasValue ? string.Format("<span class=\"{3}\"><label for=\"{0}\" style=\"width: {1}px\">{2}</label></span>", forControlId, width, CodeLocalise(key, defaultValue), cssClass) 
			: string.Format("<span class=\"{2}\"><label for=\"{0}\">{1}</label></span>", forControlId, CodeLocalise(key, defaultValue), cssClass);
	}

	/// <summary>
	/// HTML for a required label.
	/// </summary>
	/// <param name="key">The key.</param>
	/// <param name="defaultValue">The default value.</param>
	/// <returns></returns>
	[Obsolete("This method is now deprecated as it does not render an HTML label, leading to accessibilty issues, particularly with Jaws. Use HtmlRequiredLabel(Control, string, string) instead.")]
	protected string HtmlRequiredLabel(string key, string defaultValue)
	{
        return string.Format("{0}&nbsp;<img src='{1}' alt='{2}' style=\"height:5px; width:5px;\" />", CodeLocalise(key, defaultValue), UrlHelper.BrandAssetPath("Images/asteriskRequired.gif", App), CodeLocalise("Global.Required.Text", "required"));
	}

  /// <summary>
  /// HTML for a required label.
  /// </summary>
  /// <param name="forControlId">The id control to associate the label with.</param>
  /// <param name="key">The localisation key.</param>
  /// <param name="defaultValue">The default value for localisation.</param>
  /// <returns></returns>
  protected string HtmlRequiredLabel(string forControlId, string key, string defaultValue)
  {
      return string.Format("<label for=\"{0}\">{1}&nbsp;<img src=\"{2}\" alt=\"{3}\" style=\"height:5px; width:5px;\" /></label>", forControlId,
												 CodeLocalise(key, defaultValue), UrlHelper.BrandAssetPath("Images/asteriskRequired.gif", App),
                         CodeLocalise("Global.Required.Text", "required"));
  }

	/// <summary>
	/// HTML for a required label.
	/// </summary>
	/// <param name="forControl">The control to associate the label with.</param>
	/// <param name="key">The localisation key.</param>
	/// <param name="defaultValue">The default value for localisation.</param>
	/// <returns></returns>
	protected string HtmlRequiredLabel(Control forControl, string key, string defaultValue)
	{
		return string.Format("<label for=\"{0}\">{1}&nbsp;<img src=\"{2}\" alt=\"{3}\" style=\"height:5px; width:5px;\" /></label>", forControl.ClientID,
												 CodeLocalise(key, defaultValue), UrlHelper.BrandAssetPath("Images/asteriskRequired.gif", App),
												 CodeLocalise("Global.Required.Text.NoEdit", "required"));
	}

	/// <summary>
	/// HTML for a required fields instruction.
	/// </summary>
	/// <returns></returns>
	protected string HtmlRequiredFieldsInstruction()
	{
		var requiredFields = CodeLocalise("Global.RequiredFields.Text.NoEdit", "required fields");
        return string.Format("<img src='{0}' alt='{1}' style=\"height:5px; width:5px;\" />&nbsp;<span class='instructions'>{2}</span>", UrlHelper.BrandAssetPath("Images/asteriskRequired.gif", App), requiredFields, requiredFields);
	}

	/// <summary>
	/// HTMLs the tooltip with arrow.
	/// </summary>
	/// <param name="tooltipKey">The tooltip key.</param>
	/// <param name="tooltipDefault">The tooltip default.</param>
	/// <param name="tooltipHeaderKey">The tooltip header key.</param>
	/// <param name="tooltipHeaderDefault">The tooltip header default.</param>
	/// <returns></returns>
	protected string HtmlTooltip(string tooltipKey, string tooltipDefault, string tooltipHeaderKey = null, string tooltipHeaderDefault = null)
	{
		const string html = @"<span class=""tooltip helpMessage"">
	<span class=""tooltipImage""></span>
	<span class=""tooltipPopup tooltipPopupWithArrow"">
		<span class=""arrow""></span>{0}
		<span class=""content"">{1}</span>
	</span>
</span>";

		const string htmlHeader = @"
		<span class=""header"">{0}</span>";

		if (tooltipHeaderKey.IsNotNullOrEmpty())
			return string.Format(html, string.Format(htmlHeader, Localiser.Instance().Localise(tooltipHeaderKey, tooltipHeaderDefault)), Localiser.Instance().Localise(tooltipKey, tooltipDefault));

		return string.Format(html, "", Localiser.Instance().Localise(tooltipKey, tooltipDefault));
	}

	/// <summary>
	/// HTMLs the tooltip with arrow.
	/// </summary>
	/// <param name="tooltipKey">The tooltip key.</param>
	/// <param name="tooltipDefault">The tooltip default.</param>
	/// <returns></returns>
	protected string HtmlTooltipWithArrow(string tooltipKey, string tooltipDefault)
	{
		const string html = @"<div class=""tooltipWithArrow helpMessage"">
	<div class=""tooltipImage""></div> 
	<div class=""tooltipPopup"">
		<div class=""tooltipPopupInner"">
			<div class=""arrow""></div>
			{0} 
			</div>
	</div> 
</div>";

		return string.Format(html, Localiser.Instance().Localise(tooltipKey, tooltipDefault));
	}

  /// <summary>
  /// HTMLs the inline tooltip with arrow.
  /// </summary>
  /// <param name="tooltipKey">The tooltip key.</param>
  /// <param name="tooltipDefault">The tooltip default.</param>
  /// <returns>The HTML string for the tool tip</returns>
  protected string HtmlInlineTooltipWithArrow(string tooltipKey, string tooltipDefault, string optionalStyle = "")
  {
    const string html = @"<div class=""inlineTooltip"" style=""font-weight: normal;{0}"">{1}</div>";

    return string.Format(html, optionalStyle, HtmlTooltipWithArrow(tooltipKey, tooltipDefault));
  }

	/// <summary>
	/// Renders the HTML for the jQuery Tooltipster plugin.
	/// </summary>
	/// <param name="baseClass">The base tooltip CSS class to apply.</param>
	/// <param name="tooltipKey">The tooltip key.</param>
	/// <param name="tooltipDefault">The tooltip default.</param>
	/// <param name="imageClass">The class to use to render the image (if not a "helpMessage").</param>
	/// <returns></returns>
	protected string HtmlTooltipster(string baseClass, string tooltipKey, string tooltipDefault, string imageClass = "helpMessage")
	{
		const string html = @"<div class=""{0} {1}"">
<div class=""tooltipImage toolTipNew"" title=""{2}""></div>
</div>";

		return string.Format(html, baseClass, imageClass, tooltipKey.IsNullOrEmpty() ? tooltipDefault : Localiser.Instance().Localise(tooltipKey, tooltipDefault));
	}

	/// <summary>
	/// Shows the error.
	/// </summary>
	/// <param name="message">The message.</param>
	/// <param name="closeLink">The close link.</param>
	/// <param name="height">The height.</param>
	/// <param name="width">The width.</param>
	/// <param name="top">The top.</param>
	protected void ShowError(string message = "", string closeLink = "", int height = 20, int width = 400, int top = 210)
	{
		if (MasterPage.IsNotNull()) MasterPage.ShowError(message, closeLink, height, width, top);
		else
		{
			if (message.IsNullOrEmpty())
			{
				var lastError = App.GetSessionValue<string>(Constants.StateKeys.PageError);
				message = (lastError.IsNullOrEmpty()) ? lastError : "The last handled error could not be found.";
			}
			throw new Exception(message);
		}
	}

	#endregion

  #region Common Controls

  /// <summary>
  /// Binds the enrollment status drop down.
  /// </summary>
  /// <param name="enrollmentStatusDropDown">The drop down to which to bind the enrollment statuses.</param>
  protected void BindEnrollmentStatusDropDown(DropDownList enrollmentStatusDropDown)
  {
    enrollmentStatusDropDown.Items.Clear();

    if (App.Settings.Theme == FocusThemes.Education)
    {
      if (App.Settings.SchoolType == SchoolTypes.TwoYear)
      {
        if (App.Settings.SupportsProspectiveStudents)
          enrollmentStatusDropDown.Items.AddEnum(SchoolStatus.Prospective);

        enrollmentStatusDropDown.Items.AddEnum(SchoolStatus.FirstYear);
        enrollmentStatusDropDown.Items.AddEnum(SchoolStatus.SophomoreOrAbove);
        enrollmentStatusDropDown.Items.AddEnum(SchoolStatus.NonCreditOther);
        enrollmentStatusDropDown.Items.AddEnum(SchoolStatus.Alumni);
      }
      else if (App.Settings.SchoolType == SchoolTypes.FourYear)
      {
        if (App.Settings.SupportsProspectiveStudents)
          enrollmentStatusDropDown.Items.AddEnum(SchoolStatus.Prospective);

        enrollmentStatusDropDown.Items.AddEnum(SchoolStatus.FirstYear);
        enrollmentStatusDropDown.Items.AddEnum(SchoolStatus.Sophomore);
        enrollmentStatusDropDown.Items.AddEnum(SchoolStatus.Junior);
        enrollmentStatusDropDown.Items.AddEnum(SchoolStatus.Senior);
        enrollmentStatusDropDown.Items.AddEnum(SchoolStatus.Graduate);
        enrollmentStatusDropDown.Items.AddEnum(SchoolStatus.NonCreditOther);
        enrollmentStatusDropDown.Items.AddEnum(SchoolStatus.Alumni);
      }
    }
    else
    {
      enrollmentStatusDropDown.Items.AddEnum(SchoolStatus.Not_Attending_School_HS_Graduate);
      enrollmentStatusDropDown.Items.AddEnum(SchoolStatus.Not_Attending_School_OR_HS_Dropout);
      enrollmentStatusDropDown.Items.AddEnum(SchoolStatus.In_School_Post_HS);
      enrollmentStatusDropDown.Items.AddEnum(SchoolStatus.In_School_Alternative_School);
      enrollmentStatusDropDown.Items.AddEnum(SchoolStatus.In_School_HS_OR_less);
    }

    enrollmentStatusDropDown.Items.AddLocalisedTopDefault("EnrollmentStatus.TopDefault", "- select enrollment status -");
  }
  #endregion
}
