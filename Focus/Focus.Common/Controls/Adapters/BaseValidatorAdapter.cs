﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Web.UI.Adapters;
using System.Web.UI.WebControls;

#endregion

namespace Focus.Common.Controls.Adapters
{
	public class BaseValidatorAdapter : ControlAdapter
	{
		protected BaseValidator Target
		{
			get
			{
				return (this.Control as BaseValidator);
			}
		}

		protected override void OnLoad(System.EventArgs e)
		{
			Target.Attributes.Add("role", "status");
			Target.Attributes.Add("aria-live","assertive");
			Target.Attributes.Add("aria-relevant", "all");
			base.OnLoad(e);
		}
	}
}
