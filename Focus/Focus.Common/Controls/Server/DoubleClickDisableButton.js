﻿function DoubleClickDisableButton_PageLoad(requiresValidation) {
  $("*[data-doubleclickdisable='true']").each(function () {
    var self = $(this);
    if (self.data("clicked"))
      DoubleClickDisableButton_AddOverlay(self, false);
  });

  if (!requiresValidation) {
    if (!($("form").data("doubleclickdisable"))) {
      $("form").submit(function () {
        DoubleClickDisableButton_DisableButtons();
      });
      $("form").data("doubleclickdisable", true);
    }    
  }
}

function DoubleClickDisableButton_DisableButtons() {
  $("input[data-doubleclickdisable='true']").each(function () {
    var self = $(this);
    if (self.data("clicked"))
      DoubleClickDisableButton_AddOverlay(self, true);
  });
}

function DoubleClickDisableButton_AddOverlay(element, state) {
  var overlay = $("body").find("[data-disabledoubleclickoverlay='" + element.attr("id") + "']");
  if (state) {
    if (overlay == null || overlay.length == 0) {
      var offset = element.offset();
      $("<div>&nbsp;</div>").css({
        position: "absolute",
        top: offset.top,
        left: offset.left,
        width: element.outerWidth(),
        height: element.outerHeight(),
        background: "#fffff",
        opacity: 0.1,
        zIndex :99999
      }).attr("data-disabledoubleclickoverlay", element.attr("id")).appendTo("body");
    }
  } else {
    overlay.remove(); 
  }
};

function DoubleClickDisableButton_RemoveOverlay(elementId) {
	var overlay = $("body").find("[data-disabledoubleclickoverlay='" + elementId + "']");
	if (overlay.length > 0) {
		overlay.remove();
	}
};