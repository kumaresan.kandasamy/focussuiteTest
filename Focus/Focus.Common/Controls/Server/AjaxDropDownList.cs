﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Web.UI.WebControls;

#endregion

namespace Focus.Common.Controls.Server
{
	/// <summary>
	/// A simple class that allows us to play nicely with the CascadingDropDown
	/// This newly named control simply inherits from DropDownList but doesn't 
	/// have the SupportsEventValidation attribute attached and therefore doesn't
	/// raise the postback event validation exception that CascadingDropDown raises.
	/// </summary>
	public class AjaxDropDownList : DropDownList 
	{ }
}
