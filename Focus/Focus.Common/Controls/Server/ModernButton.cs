﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;

#endregion

namespace Focus.Common.Controls.Server
{
	/// <summary>
	/// A control derived from <see cref="T:System.Web.UI.WebControls.Button"/> that renders an HTML button element, with support for an optional icon.
	/// </summary>
	[ToolboxData("<{0}:ModernButton runat=server />")]
	public class ModernButton : Button
	{
		#region Properties

		/// <summary>
		/// Gets the <see cref="T:System.Web.UI.HtmlTextWriterTag"/> value that corresponds to this Web server control.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Button;
			}
		}

		/// <summary>
		/// Gets or sets the icon font provider for an optional button icon.
		/// </summary>
		public IconProvider IconProvider
		{
			get { return ViewState.GetValue("IconProvider", IconProvider.None); }
			set { ViewState.SetValue("IconProvider", value); }
		}

		/// <summary>
		/// Gets or sets the icon CSS class(es).
		/// </summary>
		public string IconCssClass
		{
			get { return ViewState.GetValue("IconCssClass", string.Empty); }
			set { ViewState.SetValue("IconCssClass", value); }
		}

		/// <summary>
		/// Gets or sets the placement of the icon within the button.
		/// </summary>
		public IconPlacement IconPlacement
		{
			get { return ViewState.GetValue("IconPlacement", IconPlacement.Left); }
			set { ViewState.SetValue("IconPlacement", value); }
		}

		#endregion

		/// <summary>
		/// Handles the <see cref="E:PreRender"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			Attributes.Remove("value");
		}

		/// <summary>
		/// Renders the contents of the control to the specified writer.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void RenderContents(HtmlTextWriter writer)
		{
			if (writer == null)
				return;

			if (IconProvider != IconProvider.None)
			{
				var iconTagKey = IconProvider == IconProvider.Glyphicons ? "span" : "i";
				var iconBaseCssClass = IconProvider == IconProvider.Glyphicons ? "glyphicons" : "fa";
				var iconHtml = string.Format("<{0} class=\"{1} {2}\" aria-hidden=\"true\"></{0}>", iconTagKey, iconBaseCssClass,
					IconCssClass);

				if (IconPlacement == IconPlacement.Left)
				{
					writer.Write("{0} {1}", iconHtml, Text);
				}
				else
				{
					writer.Write("{0} {1}", Text, iconHtml);
				}
			}
			else
			{
				writer.Write(Text);
			}
		}
	}
}
