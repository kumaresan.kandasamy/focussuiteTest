﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

using AjaxControlToolkit;
using AjaxControlToolkit.Design;

#endregion

[assembly: WebResource("Focus.Common.Controls.Server.ValidatorCallout.js", "text/javascript")]

namespace Focus.Common.Controls.Server
{
	[Designer("Focus.Common.Controls.Server.ValidatorCalloutDesigner, Focus.Web")]
	[RequiredScript(typeof(CommonToolkitScripts))]
	[RequiredScript(typeof(PopupExtender))]
	[TargetControlType(typeof(IValidator))]
	[ClientScriptResource("Focus.Common.Controls.Server.ValidatorCallout", "Focus.Common.Controls.Server.ValidatorCallout.js")]
	public class ValidatorCallout : ExtenderControlBase
	{
		public ValidatorCallout()
		{
			EnableClientState = true;
		}

		[DefaultValue(typeof(Unit), "200")]
		[ExtenderControlProperty]
		[ClientPropertyName("width")]
		public Unit Width
		{
			get { return GetPropertyValue("Width", Unit.Empty); }
			set { SetPropertyValue("Width", value); }
		}

		/// <summary>
		/// Raises the <see cref="E:PreRender"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			// Get the associated BaseValidator and set ClientState accordingly
			var baseValidator = TargetControl as BaseValidator;

			if ((null != baseValidator) && !baseValidator.IsValid)
				ClientState = "INVALID";			
			else			
				ClientState = "";
			
			base.OnPreRender(e);
		}
	}

	class ValidatorCalloutDesigner : ExtenderControlBaseDesigner<ValidatorCallout>
	{ }
}
