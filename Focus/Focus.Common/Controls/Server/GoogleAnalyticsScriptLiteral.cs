﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

using Focus.Core;

#endregion

namespace Focus.Common.Controls.Server
{
	[ToolboxData(@"<{0}:GoogleAnalyticsScriptLiteral runat=server></{0}:GoogleAnalyticsScriptLiteral>")]
	public class GoogleAnalyticsScriptLiteral : Literal
	{
		private const string GoogleAnalyticsSnippet = @"
	<script type=""text/javascript"">

		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', '{0}']);
		_gaq.push(['_trackPageview'{1}]);

		(function() {{
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		}})();

	</script>";

		[Browsable(false)]
		public string PagePreviewUrl
		{
			get { return (string)(ViewState["PagePreviewUrl"] ?? String.Empty); }
			set { ViewState["PagePreviewUrl"] = value; }
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.PreRender"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (OldApp_RefactorIfFound.Settings.Module == FocusModules.Explorer 
				&& !String.IsNullOrEmpty(OldApp_RefactorIfFound.Settings.ExplorerGoogleTrackingId))
			{
				Text = String.Format(GoogleAnalyticsSnippet, OldApp_RefactorIfFound.Settings.ExplorerGoogleTrackingId,
				                     String.IsNullOrEmpty(PagePreviewUrl) ? String.Empty : String.Format(", '{0}'", PagePreviewUrl));
			}
		}
	}
}
