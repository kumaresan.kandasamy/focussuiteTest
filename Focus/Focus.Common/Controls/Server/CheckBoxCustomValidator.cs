﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Web.UI.WebControls;

#endregion

namespace Focus.Common.Controls.Server
{
	public class CheckBoxCustomValidator : CustomValidator
	{
		/// <summary>
		/// Checks the properties of the control for valid values.
		/// </summary>
		/// <returns>
		/// true if the control properties are valid; otherwise, false.
		/// </returns>
		protected override bool ControlPropertiesValid()
		{
			return true;
		}
	}
}