﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Web.UI;

#endregion

namespace Focus.Common.Controls.Server
{
  /// <summary>
  /// Allows controls to be rendered in different order
  /// </summary>
  /// <remarks>See http://weblogs.asp.net/infinitiesloop/rendering-asp-net-controls-out-of-order</remarks>
  [ToolboxData(@"<{0}:OrderedContainer runat=server></{0}:OrderedContainer>")]
  public class OrderedContainer : Control
  {
    public enum RenderOrder
    {
      Normal,
      Reverse
    }

    /// <summary>
    /// The Order to render child controls
    /// </summary>
    public RenderOrder Order
    {
      get
      {
        object o = ViewState["Order"];
        if (o == null)
        {
          return RenderOrder.Normal;
        }
        return (RenderOrder)o;
      }
      set
      {
        ViewState["Order"] = value;
      }
    }

    /// <summary>
    /// Renders the child controls
    /// </summary>
    /// <param name="writer">Output to which HTML is written</param>
    protected override void RenderChildren(HtmlTextWriter writer)
    {
      switch (Order)
      {
        case RenderOrder.Normal:
          base.RenderChildren(writer);
          break;
        case RenderOrder.Reverse:
          RenderReverse(writer);
          break;
      }
    }

    /// <summary>
    /// Renders child controls in reverse order
    /// </summary>
    /// <param name="writer">Output to which HTML is written</param>
    private void RenderReverse(HtmlTextWriter writer)
    {
      for (var i = Controls.Count - 1; i >= 0; i--)
      {
        Controls[i].RenderControl(writer);
      }
    }
  }
}
