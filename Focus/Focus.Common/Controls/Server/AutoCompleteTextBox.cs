﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Focus.Common.Models;

#endregion

[assembly: WebResource("Focus.Common.Controls.Server.AutoCompleteTextBox.js", "text/javascript", PerformSubstitution = true)]

namespace Focus.Common.Controls.Server
{
	[ToolboxData(@"<{0}:AutoCompleteTextBox runat=server></{0}:AutoCompleteTextBox>")]
	public class AutoCompleteTextBox : TextBox
	{
		private const string SelectedValueControlIdSuffix = "_SelectedValue";

		public delegate void SelectedValueChangedHandler(object sender, EventArgs eventArgs);
		public event SelectedValueChangedHandler SelectedValueChanged;

		/// <summary>
		/// Processes the postback data for the <see cref="T:System.Web.UI.WebControls.TextBox"/> control.
		/// </summary>
		/// <param name="postDataKey">The index within the posted collection that references the content to load.</param>
		/// <param name="postCollection">The collection posted to the server.</param>
		/// <returns>
		/// true if the posted content is different from the last posting; otherwise, false.
		/// </returns>
		protected override bool LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			var loadPostData = base.LoadPostData(postDataKey, postCollection);

			if (Page.IsPostBack)
			{
				var currentSelectedValue = Page.Request.Form[SelectedValueClientId];
				SelectedValue = currentSelectedValue ?? String.Empty;
			}

			return loadPostData;
		}

		/// <summary>
		/// Invokes the <see cref="M:System.Web.UI.WebControls.TextBox.OnTextChanged(System.EventArgs)"/> method when the posted data for the <see cref="T:System.Web.UI.WebControls.TextBox"/> control has changed.
		/// </summary>
		protected override void RaisePostDataChangedEvent()
		{
			base.RaisePostDataChangedEvent();

			OnSelectedValueChanged(new EventArgs());
		}

		/// <summary>
		/// Registers client script for generating postback events prior to rendering on the client, if <see cref="P:System.Web.UI.WebControls.TextBox.AutoPostBack"/> is true.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			var sManager = ScriptManager.GetCurrent(Page);

			Page.ClientScript.RegisterClientScriptResource(typeof(AutoCompleteTextBox), "Focus.Common.Controls.Server.AutoCompleteTextBox.js");

			var serviceMethodParameters = String.Empty;
			if (ServiceMethodParameters != null && ServiceMethodParameters.Length > 0)
				serviceMethodParameters = ConvertToJSONString(ServiceMethodParameters);

			var js = String.Format("SetupAutoCompleteTextBox('{0}','{1}','{2}','{3}','{4}',{5},'{6}','{7}',{8}, {9});", ClientID, ServiceUrl, ServiceMethod, serviceMethodParameters, SelectedValueClientId, AutoPostBackOnChange.ToString().ToLower(), OnClientItemSelected, ServiceMethodSearchKey, ResultSize, AllowFreeInput.ToString().ToLowerInvariant());
			if (sManager != null && sManager.IsInAsyncPostBack)
			{
				ScriptManager.RegisterStartupScript(this.Page, GetType(), ClientID + "autoCompleteSetup", js, true);
			}
			else
			{
				Page.ClientScript.RegisterStartupScript(GetType(), ClientID + "autoCompleteSetup", js, true);
			}
		}

		/// <summary>
		/// Renders the <see cref="T:System.Web.UI.WebControls.TextBox"/> control to the specified <see cref="T:System.Web.UI.HtmlTextWriter"/> object.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> that receives the rendered output.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (!String.IsNullOrEmpty(InFieldLabelText))
			{
				writer.Write("<span class=\"inFieldLabel\"><label for=\"{0}\">{1}</label></span>", ClientID, InFieldLabelText);
			}

			base.Render(writer);

			var hiddenSelectedValue = new HtmlInputHidden();
			hiddenSelectedValue.ID = SelectedValueClientId;
			hiddenSelectedValue.Value = SelectedValue;

			if (AutoPostBackOnChange)
			{
				hiddenSelectedValue.Attributes.Add("onchange", Page.ClientScript.GetPostBackClientHyperlink(this, String.Empty));
				//hiddenSelectedValue.Attributes.Add("onchange", "alert(this.value);");
			}

			hiddenSelectedValue.RenderControl(writer);


			//writer.Write("<input type=\"hidden\" id=\"{0}\" name=\"{0}\" value=\"{1}\"{2} />", this.SelectedValueClientId, this.SelectedValue, onChangeJS);
		}

		/// <summary>
		/// Raises the <see cref="E:SelectedValueChanged"/> event.
		/// </summary>
		/// <param name="eventArgs">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void OnSelectedValueChanged(EventArgs eventArgs)
		{
			if (SelectedValueChanged != null)
				SelectedValueChanged(this, eventArgs);
		}

		#region Properties

		/// <summary>
		/// Gets or sets a value indicating whether [auto post back on change].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [auto post back on change]; otherwise, <c>false</c>.
		/// </value>
		public bool AutoPostBackOnChange
		{
			get { return (bool)(ViewState["AutoPostBackOnChange"] ?? false); }
			set { ViewState["AutoPostBackOnChange"] = value; }
		}

		[Browsable(false)]
		public string ServiceUrl
		{
			get { return (string)(ViewState["ServiceUrl"] ?? string.Empty); }
			set { ViewState["ServiceUrl"] = value; }
		}

		[Browsable(false)]
		public string ServiceMethod
		{
			get { return (string)(ViewState["ServiceMethod"] ?? string.Empty); }
			set { ViewState["ServiceMethod"] = value; }
		}

		[Browsable(false)]
		public AutoCompleteMethodParameter[] ServiceMethodParameters
		{
			get { return (AutoCompleteMethodParameter[])(ViewState["ServiceMethodParameters"] ?? null); }
			set { ViewState["ServiceMethodParameters"] = value; }

		}

		[Browsable(false)]
		public string InFieldLabelText
		{
			get { return (string)(ViewState["InFieldLabelText"] ?? string.Empty); }
			set { ViewState["InFieldLabelText"] = value; }
		}

		[Browsable(false)]
		public string SelectedValueClientId
		{
			get { return ClientID + SelectedValueControlIdSuffix; }
		}

		[Browsable(false)]
		public string SelectedValue
		{
			get 
			{
				//var selectedValue = (string)(ViewState["SelectedValue"] ?? string.Empty);
				//if (String.IsNullOrEmpty(selectedValue))
				//{
				//  if (Page.IsPostBack)
				//  {
				//    if (Page.Request.Form[this.SelectedValueClientId] != null)
				//    {
				//      selectedValue = Page.Request.Form[this.SelectedValueClientId].ToString();
				//      ViewState["SelectedValue"] = selectedValue;
				//    }
				//  }
				//}
				//return selectedValue;
				return (string)(ViewState["SelectedValue"] ?? string.Empty);
			}
			set
			{
				ViewState["SelectedValue"] = value;
			}
		}

		[Browsable(false)]
		public long? SelectedValueToNullableLong
		{
			get
			{
				long? defaultValue = null;

				long result;
				var success = long.TryParse((this.SelectedValue ?? string.Empty), out result);

				return (success) ? result : defaultValue;
			}
		}

		[Browsable(false)]
		public string OnClientItemSelected
		{
			get { return (string)(ViewState["OnClientItemSelected"] ?? string.Empty); }
			set { ViewState["OnClientItemSelected"] = value; }
		}

		[Browsable(false)]
		public string ServiceMethodSearchKey
		{
			get { return (string) (ViewState["ServiceMethodSearchString"] ?? string.Empty); }
			set { ViewState["ServiceMethodSearchString"] = value; }
		}

		[Browsable(false)]
		public int ResultSize
		{
			get { return (int) (ViewState["ResultSize"] ?? 0); }
			set { ViewState["ResultSize"] = value; }
		}

		/// <summary>
		/// Gets or sets whether free text input is permitted.
		/// </summary>
		[Browsable(true), Category("Behavior"), Description("Gets or sets whether free text input is permitted.")]
		public bool AllowFreeInput
		{
			get { return (bool) (ViewState["AllowFreeInput"] ?? true); }
			set { ViewState["AllowFreeInput"] = value; }
		}

		#endregion

		#region Helpers

		/// <summary>
		/// Converts to JSON string.
		/// </summary>
		/// <param name="methodParameters">The method parameters.</param>
		/// <returns></returns>
		private string ConvertToJSONString(AutoCompleteMethodParameter[] methodParameters)
		{
			var serialiser = new DataContractJsonSerializer(typeof(AutoCompleteMethodParameter[]));

			var ms = new MemoryStream();
			serialiser.WriteObject(ms, methodParameters);

			var json = Encoding.Default.GetString(ms.ToArray());
			ms.Close();

			return json;
		}

		#endregion
	}
}
