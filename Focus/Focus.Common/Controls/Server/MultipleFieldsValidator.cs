﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Permissions;

#endregion

[assembly: WebResource("Focus.Common.Controls.Server.MultipleFieldsValidator.js", "text/javascript", PerformSubstitution = true)]
[assembly: ScriptResource("Focus.Common.Controls.Server.MultipleFieldsValidator.js")]

namespace Focus.Common.Controls.Server
{
	[AspNetHostingPermission(SecurityAction.LinkDemand, Level = AspNetHostingPermissionLevel.Minimal)]
	[AspNetHostingPermission(SecurityAction.InheritanceDemand, Level = AspNetHostingPermissionLevel.Minimal)]
	[ToolboxData(@"<{0}:MultipleFieldsValidator runat=server></{0}:MultipleFieldsValidator>")]
	public class MultipleFieldsValidator : BaseValidator
	{
		#region Overriden Methods

		/// <summary>
		/// Adds the HTML attributes and styles that need to be rendered for the control to the specified <see cref="T:System.Web.UI.HtmlTextWriter"/> object.
		/// </summary>
		/// <param name="writer">An <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			// Setting SetFocusOnError before calling the base AddAttributesToRender because
			// the AddAttributesToRender is going to check for "SetFocusOnError" value
			base.SetFocusOnError = false; // Because we have many fields to check!
			base.AddAttributesToRender(writer);

			if (RenderUplevel)
			{
				var clientID = ClientID;

				Page.ClientScript.RegisterExpandoAttribute(clientID, "evaluationfunction", "MultipleFieldsValidatorEvaluateIsValid");
				Page.ClientScript.RegisterExpandoAttribute(clientID, "controlstovalidate", GenerateClientSideControlsToValidate());
				Page.ClientScript.RegisterExpandoAttribute(clientID, "condition", PropertyConverter.EnumToString(typeof (Conditions), Condition));
			}
		}

		/// <summary>
		/// Determines whether the control specified by the <see cref="P:System.Web.UI.WebControls.BaseValidator.ControlToValidate"/> property is a valid control.
		/// </summary>
		/// <returns>
		/// true if the control specified by <see cref="P:System.Web.UI.WebControls.BaseValidator.ControlToValidate"/> is a valid control; otherwise, false.
		/// </returns>
		/// <exception cref="T:System.Web.HttpException">No value is specified for the <see cref="P:System.Web.UI.WebControls.BaseValidator.ControlToValidate"/> property.- or -The input control specified by the <see cref="P:System.Web.UI.WebControls.BaseValidator.ControlToValidate"/> property is not found on the page.- or -The input control specified by the <see cref="P:System.Web.UI.WebControls.BaseValidator.ControlToValidate"/> property does not have a <see cref="T:System.Web.UI.ValidationPropertyAttribute"/> attribute associated with it; therefore, it cannot be validated with a validation control.</exception>
		protected override bool ControlPropertiesValid()
		{
			if (ControlsToValidate.Trim().Length == 0)
				throw new HttpException(string.Format("The ControlsToValidate property of {0} cannot be blank.", ID));

			var controlToValidateIDs = GetControlsToValidateIDs();
			if (controlToValidateIDs.Length < 2)
				throw new HttpException(string.Format("The ControlsToValidate property of {0} has less than two IDs.", ID));

			foreach (var controlToValidateID in controlToValidateIDs)
				CheckControlValidationProperty(controlToValidateID, "ControlsToValidate");

			return true;
		}

		/// <summary>
		/// When overridden in a derived class, this method contains the code to determine whether the value in the input control is valid.
		/// </summary>
		/// <returns>
		/// true if the value in the input control is valid; otherwise, false.
		/// </returns>
		protected override bool EvaluateIsValid()
		{
			var controlToValidateIDs = GetControlsToValidateIDs();

			switch (Condition)
			{
				case Conditions.OR:
					foreach (string controlToValidateID in controlToValidateIDs)
					{
						var controlToValidateValue = GetControlValidationValue(controlToValidateID);
						controlToValidateValue = controlToValidateValue == null ? string.Empty : controlToValidateValue.Trim();

						if (controlToValidateValue != string.Empty)
							return true;
					}

					return false;

				case Conditions.XOR:
					// Taking the initial values
					var previousResult = false;
					var passedFirstElement = false;

					// if one of the compared values is not like the other, then they are not all the same,
					// then they satisfy the XOR condition
					foreach (string controlToValidateID in controlToValidateIDs)
					{
						var controlToValidateValue = GetControlValidationValue(controlToValidateID);
						controlToValidateValue = controlToValidateValue == null ? string.Empty : controlToValidateValue.Trim();

						if (!passedFirstElement)
						{
							previousResult = controlToValidateValue != string.Empty;
							passedFirstElement = true;
							continue;
						}

						var currentResult = controlToValidateValue != string.Empty;
						if (previousResult != currentResult)
							return true;

						previousResult = currentResult;
					}

					return false;
				
				case Conditions.AND:
					// Allowing all fields to be blank, so the validation is All or None
					var emptyValueFound = false;
					var valueFound = false;
					foreach (string controlToValidateID in controlToValidateIDs)
					{
						var controlToValidateValue = GetControlValidationValue(controlToValidateID);
						controlToValidateValue = controlToValidateValue == null ? string.Empty : controlToValidateValue.Trim();

						if (controlToValidateValue == string.Empty)
						{
							if (valueFound) return false;
							emptyValueFound = true;
						}
						else
						{
							if (emptyValueFound) return false;
							valueFound = true;
						}
					}

					return true;

				default:
					// This line shouldn't be reached
					throw new Exception("End of validation has been reached without a result!");
			}
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.PreRender"/> event.
		/// </summary>
		/// <param name="e">A <see cref="T:System.EventArgs"/> that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (RenderUplevel)
			 Page.ClientScript.RegisterClientScriptResource(typeof(MultipleFieldsValidator), "Focus.Common.Controls.Server.MultipleFieldsValidator.js");
		}

		#endregion

		#region Helper Methods

		/// <summary>
		/// Gets the controls to validate I ds.
		/// </summary>
		/// <returns></returns>
		private string[] GetControlsToValidateIDs()
		{
			var controlsToValidate = ControlsToValidate.Replace(" ", "");
			string[] controlToValidateIDs;

			try
			{
				controlToValidateIDs = controlsToValidate.Split(',');
			}
			catch (ArgumentOutOfRangeException ex)
			{
				throw new FormatException(string.Format("The ControlsToValidate property of {0} is not well-formatted.", ID), ex);
			}

			return controlToValidateIDs;
		}

		private string GenerateClientSideControlsToValidate()
		{
			var controlToValidateIDs = GetControlsToValidateIDs();
			var controlRenderIDs = string.Empty;

			foreach (var controlToValidateID in controlToValidateIDs)
			{
				var controlToValidateIDTrimmed = controlToValidateID.Trim();

				if (controlToValidateIDTrimmed == string.Empty)
					throw new FormatException(string.Format("The ControlsToValidate property of {0} is not well-formatted.", ID));

				controlRenderIDs += "," + GetControlRenderID(controlToValidateIDTrimmed);
			}

			controlRenderIDs = controlRenderIDs.Remove(0, 1); // Removing the first ","
			
			return controlRenderIDs;
		}

		#endregion

		#region Properties

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public new bool SetFocusOnError
		{
			get { return false; }
			set { throw new NotSupportedException("SetFocusOnError is not supported because you have multiple controls to validate"); }
		}

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public new string ControlToValidate
		{
			get { return string.Empty; }
			set { throw new NotSupportedException("ControlToValidate is not supported because you have multiple controls to validate"); }
		}

		/// <summary>
		/// Comma separated list of control IDs that you want to check
		/// </summary>
		[Browsable(true)]
		[Category("Behavior")]
		[Themeable(false)]
		[DefaultValue("")]
		[Description("Comma separated list of control IDs that you want to check")]
		public string ControlsToValidate
		{
			get { return (string)(ViewState["ControlsToValidate"] ?? string.Empty); }
			set { ViewState["ControlsToValidate"] = value; }
		}

		/// <summary>
		/// The condition used to compare the value of the fields, 
		/// e.g. 'OR', will return true if at least one field is valid
		/// </summary>
		[Browsable(true)]
		[Themeable(false)]
		[Category("Behavior")]
		[DefaultValue(Conditions.OR)]
		[Description("The condition used to compare the value of the fields, e.g. 'OR', will return true if at least one field is valid")]
		public Conditions Condition
		{
			get { return (Conditions)(ViewState["Condition"] ?? Conditions.OR); }
			set { ViewState["Condition"] = value; }
		}

		#endregion

		#region Enum

		public enum Conditions
		{
			/// <summary>
			/// OR condition
			/// </summary>
			OR,
			/// <summary>
			/// XOR Condition
			/// </summary>
			XOR,
			/// <summary>
			/// AND Condition
			/// </summary>
			AND
		}

		#endregion
	}
}
