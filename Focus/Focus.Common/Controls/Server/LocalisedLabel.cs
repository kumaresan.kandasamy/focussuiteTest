﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Focus.Common.Localisation;

#endregion

namespace Focus.Common.Controls.Server
{
	/// <summary>
	/// Represents a label control, which displays localised text from the Focus Localisation store on a Web page.
	/// </summary>
	[DefaultProperty("DefaultText")]
	[ToolboxData("<{0}:LocalisedLabel runat=server></{0}:LocalisedLabel>")]
	public class LocalisedLabel : WebControl
	{
		private bool _viewStateEnabled;

		/// <summary>
		/// Gets or sets the default text to use when no matching localisation entry can be found in the localisation store.
		/// </summary>
		[Bindable(true)]
		[Category("Appearance")]
		[DefaultValue("")]
		[Localizable(false)]
		public string DefaultText
		{
			get
			{
				var s = (string)ViewState["DefaultText"];
				return (s ?? string.Empty);
			}
			set
			{
				if (HasControls())
					Controls.Clear();

				ViewState["DefaultText"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the key to use to select the localisation entry from the localisation store.
		/// </summary>
		[Bindable(true)]
		[Category("Appearance")]
		[DefaultValue("")]
		[Localizable(false)]
		public string LocalisationKey
		{
			get
			{
				var s = (string)ViewState["LocalisationKey"];
				return (s ?? string.Empty);
			}
			set
			{
				ViewState["LocalisationKey"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the identifier for a server control that the <see cref="T:Focus.Common.Controls.Server.LocalisedLabel" /> control is associated with.
		/// </summary>
		/// <returns>A string value corresponding to the <see cref="P:System.Web.UI.Control.ID" /> for a server control contained in the Web form. The default is an empty string (""), indicating that the <see cref="T:Focus.Common.Controls.Server.LocalisedLabel" /> control is not associated with another server control.</returns>
		[DefaultValue("")]
		[IDReferenceProperty]
		[Themeable(false)]
		[TypeConverter(typeof(AssociatedControlConverter))]
		[Category("Accessibility")]
		public virtual string AssociatedControlID
		{
			get
			{
				var item = (string)ViewState["AssociatedControlID"];
				return (item ?? string.Empty);
			}
			set
			{
				ViewState["AssociatedControlID"] = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether the server control persists its view state, and the view state of any child controls it contains, to the requesting client.
		/// </summary>
		/// <returns>true if the server control maintains its view state; otherwise false. The default is true.</returns>
		[DefaultValue(false)]
		[Themeable(false)]
		[Category("Behavior")]
		public override bool EnableViewState
		{
			get { return _viewStateEnabled; }
			set
			{
				base.EnableViewState = _viewStateEnabled = value;
			}
		}

		/// <summary>
		/// Gets or sets whether the <see cref="T:Focus.Common.Controls.Server.LocalisedLabel" /> should render it's content in a span. 
		/// This property is ignored if the control is rendering as a label due to having a valid value assigned to its <see cref="P:Focus.Common.Controls.Server.LocalisedLabel.AssociatedControlID" /> property.
		/// </summary>
		[DefaultValue(true)]
		[Category("Layout")]
		[Localizable(false)]
		public bool RenderOuterSpan
		{
			get
			{
				if (!string.IsNullOrEmpty(AssociatedControlID))
					return true;

				var b = (bool?)ViewState["RenderOuterSpan"];
				return (b ?? true);
			}

			set { ViewState["RenderOuterSpan"] = value; }
		}

		/// <summary>
		/// Gets the <see cref="T:System.Web.UI.HtmlTextWriterTag" /> value that corresponds to this Web server control. This property is used primarily by control developers.
		/// </summary>
		/// <returns>One of the <see cref="T:System.Web.UI.HtmlTextWriterTag" /> enumeration values.</returns>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				if (AssociatedControlID.Length != 0)
					return HtmlTextWriterTag.Label;

				return base.TagKey;
			}
		}

		/// <summary>
		/// Loads the previously saved state for the control.
		/// </summary>
		/// <param name="savedState">An object that contains the saved view state values for the control.</param>
		protected override void LoadViewState(object savedState)
		{
			if (_viewStateEnabled)
			{
				if (savedState != null)
				{
					base.LoadViewState(savedState);
					var item = (string)ViewState["DefaultText"];

					if (item != null && HasControls())
						Controls.Clear();
				}
			}
		}

		/// <summary>
		/// Adds the HTML attributes and styles of a <see cref="T:Focus.Common.Controls.Server.LocalisedLabel" /> control to render to the specified output stream.
		/// </summary>
		/// <param name="writer">An <see cref="T:System.Web.UI.HtmlTextWriter" /> that represents the output stream to render HTML content on the client.</param>
		/// <exception cref="T:System.Web.HttpException">The control specified in the <see cref="P:Focus.Common.Controls.Server.LocalisedLabel.AssociatedControlID" /> property cannot be found.</exception>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			var associatedControlID = AssociatedControlID;

			if (!string.IsNullOrEmpty(associatedControlID))
			{
				var control = FindControl(associatedControlID);

				if (control != null)
					writer.AddAttribute(HtmlTextWriterAttribute.For, control.ClientID);
				else
					if (!DesignMode)
						throw new HttpException(string.Format("Unable to find control with id '{0}' that is associated with the LocalisedLabel '{1}'.", associatedControlID, ID));
			}

			base.AddAttributesToRender(writer);
		}

		/// <summary>
		/// Renders the contents of the <see cref="T:Focus.Common.Controls.Server.LocalisedLabel" /> into the specified writer.
		/// </summary>
		/// <param name="output">The output stream that renders HTML content to the client.</param>
		protected override void RenderContents(HtmlTextWriter output)
		{
			output.Write(GetTextContent());
		}

		/// <summary>
		/// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter" /> object, which writes the content to be rendered on the client.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter" /> object that receives the server control content. </param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (!RenderOuterSpan)
				writer.Write(GetTextContent());
			else
				base.Render(writer);
		}

		/// <summary>
		/// Actually does the work of getting the relevant localised content from the localisation store.
		/// </summary>
		/// <returns></returns>
		private string GetTextContent()
		{
			var parentUserControl = ParentUserControl(this);
			return (parentUserControl == null)
				       ? Localiser.Instance().Localise((PageBase) Page, LocalisationKey, DefaultText, null)
				       : Localiser.Instance().Localise(parentUserControl, LocalisationKey, DefaultText, null);
		}

		/// <summary>
		/// Recursive method to determine if the <see cref="T:Focus.Common.Controls.Server.LocalisedLabel"/> is within a user control or directly within a page.
		/// </summary>
		/// <param name="c"></param>
		/// <returns></returns>
		private UserControlBase ParentUserControl(Control c)
		{
			try
			{
				if (c.GetType().BaseType.BaseType == typeof(UserControlBase))
					return (UserControlBase)c;

				if (c.Parent != null)
					return ParentUserControl(c.Parent);

				return null;
			}
			catch (Exception)
			{
				return null;
			}
		}
	}
}
