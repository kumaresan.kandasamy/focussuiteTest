﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Directives

using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

[assembly: WebResource("Focus.Common.Controls.Server.DependentRequiredFieldValidator.js", "text/javascript", PerformSubstitution = true)]
[assembly: ScriptResource("Focus.Common.Controls.Server.DependentRequiredFieldValidator.js")]

namespace Focus.Common.Controls.Server
{
	/// <summary>
	/// A required field validator that is dependent on the value of another control
	/// </summary>
	/// <remarks>This control differs from the standard <see cref="T:System.Web.UI.WebControls.RequiredFieldValidator"/> in that it is dependent on another field.</remarks>
	[ToolboxData("<{0}:DependentRequiredFieldValidator runat=server></{0}:DependentRequiredFieldValidator>")]
	public class DependentRequiredFieldValidator : BaseValidator
	{
		/// <summary>
		/// Gets or sets the initial value of the associated input control.
		/// </summary>
		[DefaultValue("")]
		[Category("Behavior")]
		[Themeable(false)]
		public string InitialValue
		{
			get
			{
				var item = (string)ViewState["InitialValue"];
				return (item ?? string.Empty);
			}
			set
			{
				ViewState["InitialValue"] = value;
			}
		}


		/// <summary>
		/// Gets or sets the other control on which this control is dependent
		/// </summary>
		[DefaultValue("")]
		[Themeable(false)]
		[TypeConverter(typeof(ValidatedControlConverter))]
		public string DependentControl
		{
			get
			{
				var item = (string)ViewState["DependentControl"];
				return (item ?? string.Empty);
			}
			set
			{
				ViewState["DependentControl"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the initial value of the dependent input control.
		/// </summary>
		[DefaultValue("")]
		[Category("Behavior")]
		[Themeable(false)]
		public string DependentInitialValue
		{
			get
			{
				var item = (string)ViewState["DependentInitialValue"];
				return (item ?? string.Empty);
			}
			set
			{
				ViewState["DependentInitialValue"] = value;
			}
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.PreRender" /> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (RenderUplevel)
				Page.ClientScript.RegisterClientScriptResource(typeof(DependentRequiredFieldValidator), "Focus.Common.Controls.Server.DependentRequiredFieldValidator.js");
		}

		/// <summary>
		/// Called during the validation stage when ASP.NET processes a Web Form.
		/// </summary>
		/// <returns>true if the value in the input control is valid; otherwise, false.</returns>
		protected override bool EvaluateIsValid()
		{
			try
			{
				var dependentValue = GetControlValidationValue(DependentControl).Trim();
				if (dependentValue.Equals(DependentInitialValue.Trim(), StringComparison.OrdinalIgnoreCase))
					return true;

				var controlValidationValue = GetControlValidationValue(ControlToValidate);
				return !(controlValidationValue.Equals(InitialValue.Trim(), StringComparison.OrdinalIgnoreCase));
			}
			catch
			{
				return true;
			}
		}

		/// <summary>
		/// Adds to the specified <see cref="T:System.Web.UI.HtmlTextWriter" /> object the HTML attributes and styles that need to be rendered for the control.
		/// </summary>
		/// <param name="writer">An <see cref="T:System.Web.UI.HtmlTextWriter" /> that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			base.AddAttributesToRender(writer);
			if (RenderUplevel)
			{
				var clientID = ClientID;

				Page.ClientScript.RegisterExpandoAttribute(clientID, "evaluationfunction", "DependentRequiredFieldValidatorEvaluateIsValid");
				Page.ClientScript.RegisterExpandoAttribute(clientID, "initialvalue", InitialValue);

				if (DependentControl.Length > 0)
				{
					Page.ClientScript.RegisterExpandoAttribute(clientID, "dependentcontrol", GetControlRenderID(DependentControl));
					Page.ClientScript.RegisterExpandoAttribute(clientID, "dependentinitialvalue", DependentInitialValue);
				}
			}
		}
	}
}
