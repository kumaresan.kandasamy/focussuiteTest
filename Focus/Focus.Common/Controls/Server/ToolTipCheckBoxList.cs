﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.IO;
using System.Linq;

using System.Web.UI;
using System.Web.UI.WebControls;

#endregion


namespace Focus.Common.Controls.Server
{
  [ToolboxData(@"<{0}:ToolTipCheckBoxList runat=server></{0}:ToolTipCheckBoxList>")]
	public class ToolTipCheckBoxList : CheckBoxList
  {
    /// <summary>
    /// Gets or sets the list of tool tips
    /// </summary>
    public List<string> DataTooltips
    {
      get
      {
        return (List<string>) base.ViewState["DataTooltips"];
      }
      set
      {
        if (value != null)
          base.ViewState["DataTooltips"] = value;
        else
          base.ViewState.Remove("DataTooltips");
      }
    }

    private const string ToolTipTemplate = @"<table role='presentation'>
<tr>
<td style='vertical-align:top;'>
{0}&nbsp;
</td>
<td style='vertical-align:top; width:16px;'>
  <div class='tooltip'>
		<div class=""tooltipWithArrow helpMessage"">
			<div class=""tooltipImage toolTipNew"" title=""{1}""></div>
		</div>
  </div>
</td>
</tr>
</table>";

    /// <summary>
    /// Renders the check box item
    /// </summary>
    /// <param name="itemType">The type of item</param>
    /// <param name="repeatIndex">The index of the item</param>
    /// <param name="repeatInfo">Information on repeated columns?</param>
    /// <param name="writer">The writer to which HTML is written</param>
    protected override void RenderItem(ListItemType itemType, int repeatIndex, RepeatInfo repeatInfo, HtmlTextWriter writer)
    {
      var toolTip = DataTooltips != null && DataTooltips.Any() ? DataTooltips[repeatIndex] : string.Empty;
      if (toolTip.Length > 0)
      {
        Items[repeatIndex].Attributes.Remove("tooltip");

        var stringWriter = new StringWriter();
        var checkBoxWriter = new HtmlTextWriter(stringWriter);
        base.RenderItem(itemType, repeatIndex, repeatInfo, checkBoxWriter);

        writer.Write(ToolTipTemplate, stringWriter, toolTip);
      }
      else
      {
        base.RenderItem(itemType, repeatIndex, repeatInfo, writer);
      }
    }
  }
}
