﻿function MultipleFieldsValidatorEvaluateIsValid(val)
{
	controltovalidateIDs = val.controlstovalidate.split(',');

	switch (val.condition)
	{
		case 'OR':
			for (var controltovalidateIDIndex in controltovalidateIDs)
			{
				var controlID = controltovalidateIDs[controltovalidateIDIndex];
				if (ValidatorTrim(ValidatorGetValue(controlID)) != '')
				{
					return true;
				}
			}
			return false;
			break;

		case 'XOR':
			for (var controltovalidateIDIndex in controltovalidateIDs)
			{
				var controlID = controltovalidateIDs[controltovalidateIDIndex];
				if (controltovalidateIDIndex == '0')
				{
					var previousResult = !(ValidatorTrim(ValidatorGetValue(controlID)) == '');
					continue;
				}
				var currentResult = !(ValidatorTrim(ValidatorGetValue(controlID)) == '');
				if (currentResult != previousResult)
				{
					return true;
				}
				previousResult != currentResult;
			}
			return false;
			break;

		case 'AND':
			var emptyValueFound = false;
			var valueFound = false;
			for (var controltovalidateIDIndex in controltovalidateIDs) 
			{
				var controlID = controltovalidateIDs[controltovalidateIDIndex];
				if (ValidatorTrim(ValidatorGetValue(controlID)) == '') 
				{
					if (valueFound) return false;
					emptyValueFound = true;
				}
				else 
				{
					if (emptyValueFound) return false;
					valueFound = true;
				}
			}
			return true;
			break;
	}

	return false;
}