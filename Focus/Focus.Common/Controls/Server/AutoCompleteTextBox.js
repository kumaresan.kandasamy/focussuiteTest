﻿function SetupAutoCompleteTextBox(textboxClientId, serviceUrl, serviceMethod, serviceMethodParameters, hiddenSelectedClientId, autoPostBackOnChange, onItemSelectedJS, searchKey, resultSize, allowFreeInput) {
	if ($("#" + textboxClientId).data("autocomplete") != null) {
    return;
  }
  $(function () {
		$("#" + textboxClientId).autocomplete({
			delay: 50,
			minLength: 2,
			position: {  collision: "flip"  },
			source: function (request, response) {
				var dataParams = GetAutoCompleteDataParameters(serviceMethodParameters);

				$.ajax({
					type: "POST",
					url: serviceUrl + "/" + serviceMethod,
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					data: '{"' + ((searchKey.length > 0) ? searchKey : 'term') + '": "' + request.term + '"' + dataParams + ((resultSize > 0) ? (',"count":' + resultSize) : '') + '}',
					success: function (result) {
						response(result.d);
					},
					error: function (xhr, status) {
						response(null);
					}
				});
			},
			select: function (event, ui) {
				$("#" + hiddenSelectedClientId).val("");
				if (ui.item && ui.item.id && ui.item.id.length > 0) {
					$("#" + hiddenSelectedClientId).val(ui.item.id);
					if (onItemSelectedJS.length > 0) {
						eval(onItemSelectedJS);
					}
					if (autoPostBackOnChange) {
						$("#" + textboxClientId).val(ui.item.label);
						$("#" + hiddenSelectedClientId).change();
					}
				}
			},
			change: function(event, ui) {
				if (!(allowFreeInput || ui.item)) event.target.value = '';
			}
		});

		$("#" + textboxClientId).blur(function () {
			if ($(this).val().length == 0 && $("#" + hiddenSelectedClientId).val().length > 0) {
				$("#" + hiddenSelectedClientId).val("");
				if (autoPostBackOnChange) {
					$("#" + hiddenSelectedClientId).change();
				}
			}
		});

		$("#" + textboxClientId).keyup(function () {
			if ($("#" + hiddenSelectedClientId).val().length > 0) {
				$("#" + hiddenSelectedClientId).val("");
				if (autoPostBackOnChange) {
					$("#" + hiddenSelectedClientId).change();
				}
			}
		});
	});
}

function GetAutoCompleteDataParameters(serviceMethodParamters) 
{
	var dataParams = "";

	if (serviceMethodParamters && serviceMethodParamters.length > 0) 
	{
		var smpArray = JSON.parse(serviceMethodParamters);

		if (smpArray && smpArray.length > 0) 
		{
			for (var i = 0; i < smpArray.length; i++) 
			{
				dataParams += ", ";

				var dataParamValue = "";
				if (smpArray[i].OverrideValue) {
				    dataParamValue = smpArray[i].OverrideValue;
				}
				else if ($('#' + smpArray[i].ControlId).is("input[type='checkbox']")) 
				{
					if ($('#' + smpArray[i].ControlId).is(":checked")) 
					{
						dataParamValue = "true";
					}
				}
				else {
					dataParamValue = $('#' + smpArray[i].ControlId).val();
				}
				
				if (!dataParamValue || dataParamValue.length == 0) 
				{
					dataParamValue = "null";
				}

				switch (smpArray[i].ParameterType) 
				{
					case "string":
						{
							if (dataParamValue == "null") 
							{
								dataParams += '"' + smpArray[i].ParameterName + '": ' + dataParamValue;
							}
							else 
							{
								dataParams += '"' + smpArray[i].ParameterName + '": "' + dataParamValue + '"';
							}

							break;
						}
					default:
						{
							dataParams += '"' + smpArray[i].ParameterName + '": ' + dataParamValue;
						}
				}
			}
		}
	}

	return dataParams;
}