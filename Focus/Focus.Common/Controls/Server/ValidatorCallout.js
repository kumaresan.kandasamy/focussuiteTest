﻿/// <reference name="MicrosoftAjax.debug.js" />
/// <reference name="MicrosoftAjaxTimer.debug.js" />
/// <reference name="MicrosoftAjaxWebForms.debug.js" />
/// <reference path="../ExtenderBase/BaseScripts.js" />
/// <reference path="../Common/Common.js" />
/// <reference path="../Compat/Timer/Timer.js" />
/// <reference path="../Animation/Animations.js" />
/// <reference path="../Animation/AnimationBehavior.js" />
/// <reference path="../PopupExtender/PopupBehavior.js" />
/// <reference path="../Code/ValidatorCallout.js" />

Type.registerNamespace('Focus.Common.Controls.Server');

Focus.Common.Controls.Server.ValidatorCallout = function ValidatorCallout(element)
{
	Focus.Common.Controls.Server.ValidatorCallout.initializeBase(this, [element]);

	this._width = "200px";
	this._originalValidationMethod = null;
	this._validationMethodOverride = null;
	this._elementToValidate = null;
	this._popupSpan = null;
	this._iconSpan = null;
	this._contentSpan = null;
	this._isBuilt = false;
	this._closeClickHandler = Function.createDelegate(this, this._oncloseClick);
}

Focus.Common.Controls.Server.ValidatorCallout.prototype = {
	initialize: function ()
	{
		Focus.Common.Controls.Server.ValidatorCallout.callBaseMethod(this, 'initialize');
		var elt = this.get_element();

		//
		// Override the evaluation method of the current validator
		//
		if (elt.evaluationfunction)
		{
			this._originalValidationMethod = Function.createDelegate(elt, elt.evaluationfunction);
			this._validationMethodOverride = Function.createDelegate(this, this._onvalidate);
			elt.evaluationfunction = this._validationMethodOverride;
		}

		// Check for failed server-side validation (indicated by non-empty ClientState)
		var clientState = this.get_ClientState();
		if ((null != clientState) && ("" !== clientState))
		{
			this._ensureCallout();
			this.show();
		}
	},

	_ensureCallout: function ()
	{
		if (!this._isBuilt)
		{
			var elt = this.get_element();

			//
			// create the DOM elements
			//
			var elementToValidate = this._elementToValidate = $get(elt.controltovalidate);
			var validatorSpan = document.createElement("span");
			var arrowSpan = document.createElement("span");
			var iconSpan = this._iconSpan = document.createElement("span");
			var contentSpan = this._contentSpan = document.createElement("span");
			var popupSpan = this._popupSpan = document.createElement("span");

			//
			// popupSpan, arrowSpan, iconSpan & contentSpan
			//
			popupSpan.id = this.get_id() + "_popupSpan";
			popupSpan.className = "validatorCallout";
			validatorSpan.className = "validatorCalloutInner";
			arrowSpan.className = "validatorCalloutArrow";
			iconSpan.className = "validatorCalloutClose";
			contentSpan.className = "content";
			contentSpan.innerHTML = this._getErrorMessage();

			//
			// Create the DOM tree
			//
			elt.parentNode.appendChild(popupSpan);
			popupSpan.appendChild(validatorSpan);
			validatorSpan.appendChild(arrowSpan);
			validatorSpan.appendChild(iconSpan);
			validatorSpan.appendChild(contentSpan);

			//
			// initialize behaviors
			//
			this._popupBehavior = $create(
                Sys.Extended.UI.PopupBehavior,
                {
                	parentElement: elementToValidate
                },
                {},
                null,
                this._popupSpan);
			this._popupBehavior.set_positioningMode(Sys.Extended.UI.PositioningMode.Right);

			$addHandler(this._iconSpan, "click", this._closeClickHandler);
			this._isBuilt = true;
		}
	},

	dispose: function ()
	{
		if (this._isBuilt)
		{
			this.hide();

			$removeHandler(this._iconSpan, "click", this._closeClickHandler);

			this._iconSpan = null;
			this._contentSpan = null;

			if (this._popupSpan)
			{
				this._popupSpan.parentNode.removeChild(this._popupSpan);
				this._popupSpan = null;
			}

			this._isBuilt = false;
		}

		Focus.Common.Controls.Server.ValidatorCallout.callBaseMethod(this, 'dispose');
	},

	_getErrorMessage: function ()
	{
		return this.get_element().errormessage || '?????';
	},

	show: function (force)
	{
		if (force || !this.get_isOpen())
		{
			if (force && Focus.Common.Controls.Server.ValidatorCallout._currentCallout)
				Focus.Common.Controls.Server.ValidatorCallout._currentCallout.hide();

			if (Focus.Common.Controls.Server.ValidatorCallout._currentCallout != null)
				return;

			Focus.Common.Controls.Server.ValidatorCallout._currentCallout = this;
			this._contentSpan.innerHTML = this._getErrorMessage();
			this._popupBehavior.show();
		}
	},

	hide: function ()
	{
		if (Focus.Common.Controls.Server.ValidatorCallout._currentCallout == this)
			Focus.Common.Controls.Server.ValidatorCallout._currentCallout = null;

		if (this.get_isOpen())
			this._popupBehavior.hide();
	},

	_onfocus: function (e)
	{
		if (!this._originalValidationMethod(this.get_element()))
		{
			this._ensureCallout();

			this.show(true);
			return false;
		} else
		{
			this.hide();
			return true;
		}
	},

	_oncloseClick: function (e)
	{
		this.hide();
	},

	_onvalidate: function (val)
	{
		if (!this._originalValidationMethod(val))
		{
			this._ensureCallout();

			this.show(false);
			this._invalid = true;
			return false;
		}
		else
		{
			this._invalid = false;
			this.hide();
			return true;
		}
	},

	get_width: function ()
	{
		return this._width;
	},
	set_width: function (value)
	{
		if (this._width != value)
		{
			this._width = value;

			if (this.get_isInitialized())
				this._popupSpan.style.width = _width;

			this.raisePropertyChanged("width");
		}
	},

	get_isOpen: function ()
	{
		return $common.getVisible(this._popupSpan);
	}
}

Focus.Common.Controls.Server.ValidatorCallout.registerClass('Focus.Common.Controls.Server.ValidatorCallout', Sys.Extended.UI.BehaviorBase);
Focus.Common.Controls.Server.FocusValidatorCalloutPosition = function () { throw Error.invalidOperation(); }
