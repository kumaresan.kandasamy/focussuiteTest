﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

[assembly: WebResource("Focus.Common.Controls.Server.DoubleClickDisableButton.js", "text/javascript", PerformSubstitution = true)]

namespace Focus.Common.Controls.Server
{
  public class DoubleClickDisableButton : Button
  {
    /// <summary>
    /// Initialise the control
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e) 
    {
      if (CausesValidation)
        Page.SaveStateComplete += RegisterPostSubmitScript; 

       base.OnInit(e); 
    }

    /// <summary>
    /// Loads the control
    /// </summary>
    /// <param name="e"></param>
    protected override void OnLoad(EventArgs e)
    {
      Page.ClientScript.RegisterClientScriptResource(typeof(DoubleClickDisableButton), "Focus.Common.Controls.Server.DoubleClickDisableButton.js");
      base.OnLoad(e);  
    }

    /// <summary>
    /// Fires when the control is about to be rendered
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
      Attributes.Add("data-doubleclickdisable", "true");
			Attributes.Add("onclick", "$(this).data('clicked', true);");
			Attributes.Add("onkeypress", "$(this).data('clicked', true);");

      base.OnPreRender(e);

      var scriptManager = ScriptManager.GetCurrent(Page);

      var script = string.Format("DoubleClickDisableButton_PageLoad({0});", CausesValidation.ToString().ToLowerInvariant());

      if (scriptManager != null && scriptManager.IsInAsyncPostBack)
        ScriptManager.RegisterStartupScript(Page, typeof(DoubleClickDisableButton), "DoubleClickDisableButton_PageLoad", script, true);
      else
        Page.ClientScript.RegisterStartupScript(typeof(DoubleClickDisableButton), "DoubleClickDisableButton_PageLoad", script, true);
    }

    /// <summary>
    /// Initialises a script to run on form submite
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void RegisterPostSubmitScript(object sender, EventArgs e)
    {
      var scriptManager = ScriptManager.GetCurrent(Page);

      if (scriptManager != null && scriptManager.IsInAsyncPostBack)
        ScriptManager.RegisterOnSubmitStatement(this, typeof(DoubleClickDisableButton), "DoubleClickDisableButton_DisableButtons", "DoubleClickDisableButton_DisableButtons();");
      else
        Page.ClientScript.RegisterOnSubmitStatement(typeof(DoubleClickDisableButton), "DoubleClickDisableButton_DisableButtons", "DoubleClickDisableButton_DisableButtons();");
    }
  }
}
