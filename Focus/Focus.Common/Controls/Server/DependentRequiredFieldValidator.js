﻿function DependentRequiredFieldValidatorEvaluateIsValid(val) {
	if (ValidatorTrim(ValidatorGetValue(val.dependentcontrol)) == ValidatorTrim(val.dependentinitialvalue))
		return true;

	return (ValidatorTrim(ValidatorGetValue(val.controltovalidate)) != ValidatorTrim(val.initialvalue));
}