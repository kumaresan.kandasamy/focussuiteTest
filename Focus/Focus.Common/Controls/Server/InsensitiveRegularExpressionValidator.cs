﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

#endregion

[assembly: WebResource("Focus.Common.Controls.Server.InsensitiveRegularExpressionValidator.js", "text/javascript", PerformSubstitution = true)]
[assembly: ScriptResource("Focus.Common.Controls.Server.InsensitiveRegularExpressionValidator.js")]

namespace Focus.Common.Controls.Server
{
	/// <summary>
	/// Validates whether the value of an associated input control matches the pattern specified by a regular expression.
	/// </summary>
	/// <remarks>This control differs from the standard <see cref="T:System.Web.UI.WebControls.RegularExpressionValidator"/> in that matching is case-insensitive.</remarks>
	[ToolboxData("<{0}:InsensitiveRegularExpressionValidator runat=server></{0}:InsensitiveRegularExpressionValidator>")]
	public class InsensitiveRegularExpressionValidator : BaseValidator
	{
		/// <summary>
		/// Gets or sets the regular expression that determines the pattern used to validate a field.
		/// </summary>
		/// <returns>A string that specifies the regular expression used to validate a field for format. The default is <see cref="F:System.String.Empty" />.</returns>
		/// <exception cref="T:System.Web.HttpException">The regular expression is not properly formed.</exception>
		[DefaultValue("")]
		[Editor("System.Web.UI.Design.WebControls.RegexTypeEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
		[Themeable(false)]
		[Category("Behavior")]
		public string ValidationExpression
		{
			get
			{
				var item = (string)ViewState["ValidationExpression"];
				return (item ?? string.Empty);
			}
			set
			{
				try
				{
					Regex.IsMatch(string.Empty, value);
				}
				catch (Exception ex)
				{
					throw new HttpException(string.Format("'{0}' is not a valid regular expression.", value), ex);
				}
				ViewState["ValidationExpression"] = value;
			}
		}

		/// <summary>
		/// Adds to the specified <see cref="T:System.Web.UI.HtmlTextWriter" /> object the HTML attributes and styles that need to be rendered for the control.
		/// </summary>
		/// <param name="writer">An <see cref="T:System.Web.UI.HtmlTextWriter" /> that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			base.AddAttributesToRender(writer);
			if (RenderUplevel)
			{
				var clientID = ClientID;
				Page.ClientScript.RegisterExpandoAttribute(clientID, "evaluationfunction", "InsensitiveRegularExpressionValidatorEvaluateIsValid");
				if (ValidationExpression.Length > 0)
					Page.ClientScript.RegisterExpandoAttribute(clientID, "validationexpression", ValidationExpression);
			}
		}

		/// <summary>
		/// Indicates whether the value in the input control is valid.
		/// </summary>
		/// <returns>true if the value in the input control is valid; otherwise, false.</returns>
		protected override bool EvaluateIsValid()
		{
			var controlValidationValue = GetControlValidationValue(ControlToValidate);
			if (controlValidationValue == null || controlValidationValue.Trim().Length == 0)
				return true;

			try
			{
				Match match = Regex.Match(controlValidationValue, ValidationExpression, RegexOptions.IgnoreCase);
				return (!match.Success || match.Index != 0 ? false : match.Length == controlValidationValue.Length);
			}
			catch
			{
				return true;
			}
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.PreRender" /> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (RenderUplevel)
				Page.ClientScript.RegisterClientScriptResource(typeof(InsensitiveRegularExpressionValidator), "Focus.Common.Controls.Server.InsensitiveRegularExpressionValidator.js");
		}
	}
}
