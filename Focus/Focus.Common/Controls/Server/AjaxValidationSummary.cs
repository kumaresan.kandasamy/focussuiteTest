﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

namespace Focus.Common.Controls.Server
{
	/// <summary>
	/// Ajax control toolkit compatible validation summary control
	/// </summary>
	[ToolboxData("")]
	public class AjaxValidationSummary : ValidationSummary
	{
		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.PreRender"/> event.
		/// 
		/// This fixes an issue with using the validation summary and the ajax control tookkit 
		/// 
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			ScriptManager.RegisterStartupScript(Page, Page.GetType(), ClientID, ";", true);
		}
	}
}