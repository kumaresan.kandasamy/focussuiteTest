﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Reflection;
using System.Runtime.Caching;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using BgtSSO;
using Focus.Core.Settings.Interfaces;
using Framework.Core;
using Framework.Logging;

#endregion

namespace Focus.Common.Authentication
{
	public class OAuthContext
	{
		private readonly IAppSettings _settings;
		private List<NameValue> _oauthParameters = new List<NameValue>();
		private static readonly Random _random = new Random();
		private static readonly ObjectCache _cache = MemoryCache.Default;

		#region Constants

		private const string ReservedCharacters = "!*'();:@&=+$,/?%#[]";
		private const string MapperCode = @"
using System;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;

using Focus.Common.Authentication;

namespace DynamicCoder 
{
	public class MapperClass 
	{
		public OAuthProfile Map(string data) 
		{			
			try
			{
#CODE#
			}
			catch 
			{
				return null;
			}
		}
	}
}";
		#endregion

		/// <summary>
		/// Initializes a new instance of the <see cref="OAuthContext"/> class.
		/// </summary>
		/// <param name="settings">The settings.</param>
		/// <param name="callbackRoute">The callback route.</param>
		public OAuthContext(IAppSettings settings, string callbackRoute)
		{
			if (settings == null)
				throw new ArgumentNullException("settings");

			if (callbackRoute == null)
				throw new ArgumentNullException("callbackRoute");

			_settings = settings;
			CallbackUrl = GetUrlFromRoute(callbackRoute);
		}

		#region Properties

		public string CallbackUrl { get; private set; }
		public string AccessToken { get; private set; }
		public string AccessTokenSecret { get; private set; }
		public string Token { get; private set; }
		public string TokenSecret { get; private set; }
		public string Verifier { get; private set; }
		public string Code { get; private set; }
		public string ScreenName { get; private set; }
		public string UserId { get; private set; }

		#endregion

		/// <summary>
		/// To get active context object
		/// </summary>
		public static OAuthContext Current
		{
			get { return (OAuthContext)HttpContext.Current.Session["oAuthContext"]; }
			set { HttpContext.Current.Session["oAuthContext"] = value; }
		}

		#region oAuth API

		/// <summary>
		/// function to get request token
		/// </summary>
		public void Authorize()
		{
			if (_settings.OAuthVersion == OAuthVersion.V1)
			{
				HttpWebRequest request = null;

				try
				{
					var requestHeader = GetRequestTokenAuthorizationHeader();

					// Make a request and convert data in Byte[]
					var response = MakeHttpRequest(_settings.OAuthRequestTokenUrl, requestHeader);
					var data = GetResponseByte(response);

					// Read byte[] and get Token and TokenSecret
					using (var reader = new StreamReader(new MemoryStream(data)))
					{
						var resultString = reader.ReadToEnd();
						var queryStrings = HttpUtility.ParseQueryString(resultString);

						Token = queryStrings["oauth_token"];
						TokenSecret = queryStrings["oauth_token_secret"];
					}
				}
				catch (WebException we)
				{
					var errorResponse = we.Response as HttpWebResponse;

					if (errorResponse != null)
					{
						var sr = new StreamReader(errorResponse.GetResponseStream());
						var responseData = sr.ReadToEnd();
						sr.Close();

						throw new OAuthNetworkException("ERROR_NETWORK_PROBLEM", request.RequestUri.ToString(), errorResponse.StatusCode, responseData, request.Headers["Authorization"]);
					}

					throw new Exception("Initializing error! Please check the Customer key and Customer secret key.");
				}
				catch (Exception e)
				{
					throw new OAuthException("ERROR_EXCEPION_OCCURRED", e);
				}
			}
		}

		/// <summary>
		/// Function to get access token
		/// </summary>
		public void Verify()
		{
			HttpWebRequest request = null;

			try
			{
				// Make a request and convert data in Byte[]
				WebResponse response;

				if (_settings.OAuthVersion == OAuthVersion.V1)
				{
					var requestHeader = GetAccessTokenAuthorizationHeader();
					response = MakeHttpRequest(_settings.OAuthRequestAccessTokenUrl, requestHeader);
				}
				else // for version 2.0
					response = MakeHttpRequest(string.Format(_settings.OAuthRequestAccessTokenUrl, _settings.OAuthConsumerKey, CallbackUrl, _settings.OAuthConsumerSecret, Code), string.Empty, "GET");

				var data = GetResponseByte(response);

				// Read byte[] and get Token and TokenSecret
				using (var reader = new StreamReader(new MemoryStream(data)))
				{
					var resultString = reader.ReadToEnd();
					var queryStrings = HttpUtility.ParseQueryString(resultString);

					AccessToken = _settings.OAuthVersion == OAuthVersion.V1 ? queryStrings["oauth_token"] : queryStrings["access_token"];
					AccessTokenSecret = queryStrings["oauth_token_secret"];

					ScreenName = queryStrings["screen_name"];
					UserId = queryStrings["user_id"];
				}

			}
			catch (WebException we)
			{
				var errorResponse = we.Response as HttpWebResponse;

				var sr = new StreamReader(errorResponse.GetResponseStream());
				var responseData = sr.ReadToEnd();
				sr.Close();

				throw new OAuthNetworkException("ERROR_NETWORK_PROBLEM", request.RequestUri.ToString(), errorResponse.StatusCode, responseData, request.Headers["Authorization"]);
			}
			catch (Exception e)
			{
				throw new OAuthException("ERROR_EXCEPION_OCCURRED", e);
			}
		}

		/// <summary>
		/// Function to get user profile response
		/// </summary>
		/// <returns></returns>
		public ISSOProfile GetProfile()
		{
			var requestHeader = GetUserProfileAuthorizationHeader();

			var request = WebRequest.Create(GetProfileUrl());
			request.Headers.Add("Authorization", requestHeader);
			request.Method = HttpMethod.Get;

			try
			{
				var response = request.GetResponse();

				using (var responseStream = response.GetResponseStream())
				{
					var reader = new StreamReader(responseStream);
					var responseText = reader.ReadToEnd();
					reader.Close();

					var profile = MapToProfile(responseText);
					if (profile.IsNull())
						throw new InvalidDataException("A valid profile hasn't been returned by the indent provider");

					profile.CheckIsValid();
					return profile;
				}
			}
			catch (WebException we)
			{
				var errorResponse = we.Response as HttpWebResponse;

				var sr = new StreamReader(errorResponse.GetResponseStream());
				var responseData = sr.ReadToEnd();
				sr.Close();

				throw new OAuthNetworkException("ERROR_NETWORK_PROBLEM", request.RequestUri.ToString(), errorResponse.StatusCode, responseData, request.Headers["Authorization"]);
			}
			catch (Exception e)
			{
				throw new OAuthException("ERROR_EXCEPION_OCCURRED", e);
			}
		}

		/// <summary>
		/// Function to redirect to login site for Verification
		/// </summary>
		public void ObtainVerifier()
		{
			Current = this;

			if (_settings.OAuthVersion == OAuthVersion.V1)
				HttpContext.Current.Response.Redirect(_settings.OAuthVerifierUrl + "?oauth_token=" + UrlEncode(Token) + "&oauth_callback=" + CallbackUrl);
			else
				HttpContext.Current.Response.Redirect(String.Format(_settings.OAuthVerifierUrl, _settings.OAuthConsumerKey, CallbackUrl, _settings.OAuthScope));
		}

		/// <summary>
		/// Last step of request verification. Then we begin with GetAccessToken and then GetProfile
		/// </summary>
		public void EndAuthenticationAndRegisterTokens()
		{
			var request = HttpContext.Current.Request;

			if (_settings.OAuthVersion == OAuthVersion.V1)
			{
				if (!string.IsNullOrEmpty(request.QueryString["oauth_verifier"]))
					Verifier = request.QueryString["oauth_verifier"];

				if (!string.IsNullOrEmpty(request.QueryString["oauth_token"]))
					Token = request.QueryString["oauth_token"];

				if (!string.IsNullOrEmpty(request.QueryString["oauth_token_secret"]))
					TokenSecret = request.QueryString["oauth_token_secret"];
			}
			else
			{
				if (!string.IsNullOrEmpty(request.QueryString["code"]))
					Code = request.QueryString["code"];
			}
		}

		/// <summary>
		/// Function to make HttpRequests
		/// </summary>
		/// <param name="url"></param>
		/// <param name="authorizationHeader"></param>
		/// <param name="requestMethod"></param>
		/// <returns></returns>
		public WebResponse MakeHttpRequest(string url, string authorizationHeader, string requestMethod = "POST")
		{
			var normalizedUrl = NormalizeUrl(url);
			var request = (HttpWebRequest)WebRequest.Create(normalizedUrl);

			request.AllowAutoRedirect = true;
			request.Referer = CallbackUrl;
			request.Method = requestMethod;

			if (authorizationHeader != string.Empty)
			{
				request.Headers.Add("Authorization", authorizationHeader);
				var requestStream = request.GetRequestStream();
			}

			try
			{
				return (request.GetResponse());
			}
			catch (WebException e)
			{
				using (var resp = e.Response)
				{
					using (var sr = new StreamReader(resp.GetResponseStream()))
					{
						var errorMessage = sr.ReadToEnd();

						HttpContext.Current.Response.Write(errorMessage);
						((Page)HttpContext.Current.CurrentHandler).ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Error');</script> ");

						throw new WebException(errorMessage, e);
					}
				}
			}
		}

		/// <summary>
		/// Function to get results in byte[]
		/// </summary>
		/// <param name="response"></param>
		/// <returns></returns>
		public byte[] GetResponseByte(WebResponse response)
		{
			var ms = new MemoryStream();
			var binaryWriter = new BinaryWriter(ms);

			using (var binaryReader = new BinaryReader(response.GetResponseStream()))
			{
				int readedByteCount;
				const int bufferLength = 65536;

				do
				{
					var data = new byte[bufferLength];
					readedByteCount = binaryReader.Read(data, 0, data.Length);

					if (readedByteCount == 0)
						break;

					binaryWriter.Write(data, 0, readedByteCount);
				}
				while (readedByteCount <= bufferLength);
			}

			ms.Position = 0;
			var resultData = ms.ToArray();

			return resultData;
		}

		#endregion

		#region Private methods

		/// <summary>
		/// Generates timestamp
		/// </summary>
		/// <returns></returns>
		private static string GenerateTimeStamp()
		{
			var ts = DateTime.UtcNow - new DateTime(1970, 1, 1);
			return Math.Truncate(ts.TotalSeconds).ToString(CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// Generate url for fetching the profile data
		/// </summary>
		/// <returns></returns>
		private string GetProfileUrl()
		{
			var url = _settings.OAuthRequestProfileUrl;

			url = url.Replace("#SCREENNAME#", ScreenName);
			url = url.Replace("#ACCESSTOKEN#", AccessToken);

			return url;
		}

		/// <summary>
		/// Maps to profile.
		/// </summary>
		/// <param name="profile">The profile.</param>
		/// <returns></returns>
		private OAuthProfile MapToProfile(string profile)
		{
			var key = "ProfileAssemblyCache:" + _settings.Theme + "-" + _settings.Module;

			var assembly = (_cache.Contains(key) ? (Assembly)_cache.Get(key) : null);

			if (assembly == null)
			{
				lock (_cache)
				{
					var compiler = CodeDomProvider.CreateProvider("CSharp");
					var parameters = new CompilerParameters();

					// Start by adding any referenced assemblies
					parameters.ReferencedAssemblies.Add("System.dll");
					parameters.ReferencedAssemblies.Add("System.Web.dll");
					parameters.ReferencedAssemblies.Add("System.Web.Extensions.dll");
					parameters.ReferencedAssemblies.Add(Assembly.GetExecutingAssembly().Location);

					// Load the resulting assembly into memory
					parameters.GenerateInMemory = true;

					var code = MapperCode.Replace("#CODE#", _settings.OAuthProfileMapperCode);

					// Now compile the whole thing
					var compiled = compiler.CompileAssemblyFromSource(parameters, code);

					if (compiled.Errors.HasErrors)
					{
						var errorMsg = "Error Compiling. ";

						// Create Error String
						errorMsg += compiled.Errors.Count + " Errors:";

						for (var x = 0; x < compiled.Errors.Count; x++)
							errorMsg = errorMsg + "\r\nLine: " + compiled.Errors[x].Line + " - " +
												 compiled.Errors[x].ErrorText;

						Logger.Error(String.Format(".....MapToProfile - Compile Error\r\n{0}\r\n\r\nCompiling\r\n\r\n{1}", errorMsg, code));
						throw new ArgumentException(String.Format("MapToProfile - Compile Error\r\n{0}\r\n\r\nCompiling\r\n\r\n{1}", errorMsg, code));
					}

					assembly = compiled.CompiledAssembly;
					_cache.Add(key, assembly, null);
				}
			}

			// Retrieve an object reference - since this object is 'dynamic' we can't explicitly
			// type it so it's of type Object
			var classObject = assembly.CreateInstance("DynamicCoder.MapperClass");
			if (classObject == null)
				throw new ArgumentException(".....GetProfile - Couldn't load dynamic class");

			var methodParams = new object[] { profile };

			try
			{
				var oAuthProfile = classObject.GetType().InvokeMember("Map", BindingFlags.InvokeMethod, null, classObject, methodParams);
				return (OAuthProfile)oAuthProfile;
			}
			catch (Exception error)
			{
				throw new ArgumentException(String.Format(".....GetProfile - Error Invoking dynamic class method.\r\n{0}\r\n\r\n{1}", error.Message, error.InnerException.Message));
			}
		}

		/// <summary>
		/// Generates nonce
		/// </summary>
		/// <param name="timestamp"></param>
		/// <returns></returns>
		private static string GenerateNonce(string timestamp)
		{
			var buffer = new byte[256];
			_random.NextBytes(buffer);

			var hmacsha1 = new HMACSHA1 { Key = Encoding.ASCII.GetBytes(Encoding.ASCII.GetString(buffer)) };

			return ComputeHash(hmacsha1, timestamp);
		}

		/// <summary>
		/// Compte hash. Only for "HMAC-SHA1"
		/// </summary>
		/// <param name="hashAlgorithm"></param>
		/// <param name="data"></param>
		/// <returns></returns>
		private static string ComputeHash(HashAlgorithm hashAlgorithm, string data)
		{
			if (hashAlgorithm == null)
				throw new ArgumentNullException("hashAlgorithm");

			if (string.IsNullOrEmpty(data))
				throw new ArgumentNullException("data");

			var buffer = Encoding.ASCII.GetBytes(data);
			var bytes = hashAlgorithm.ComputeHash(buffer);

			return Convert.ToBase64String(bytes);
		}

		/// <summary>
		/// Encode OAuth params
		/// </summary>
		/// <param name="parameters"></param>
		/// <returns></returns>
		private static string NormalizeOAuthParameters(IList<NameValue> parameters)
		{
			var sb = new StringBuilder();
			NameValue p;

			for (var i = 0; i < parameters.Count; i++)
			{
				p = parameters[i];
				sb.AppendFormat("{0}={1}", p.Name, UrlEncode(p.Value));

				if (i < parameters.Count - 1)
					sb.Append("&");
			}

			return sb.ToString();
		}

		/// <summary>
		/// Function to encode url
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		private static string NormalizeUrl(string url)
		{
			var questionIndex = url.IndexOf('?');

			if (questionIndex == -1)
				return url;

			var parameters = url.Substring(questionIndex + 1);
			var result = new StringBuilder();
			result.Append(url.Substring(0, questionIndex + 1));

			if (!String.IsNullOrEmpty(parameters))
			{
				var parts = parameters.Split('&');
				var hasQueryParameters = parts.Length > 0;

				foreach (var part in parts)
				{
					var nameValue = part.Split('=');
					result.Append(nameValue[0] + "=");

					if (nameValue.Length == 2)
						result.Append(UrlEncode(nameValue[1]));

					result.Append("&");
				}

				if (hasQueryParameters)
					result = result.Remove(result.Length - 1, 1);
			}

			return result.ToString();
		}

		/// <summary>
		/// Fetch query string and create name value pairs. Used in adding url querystring to OAuth header. For eg. "scope" in google
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		private static List<NameValue> ExtractQueryStrings(string url)
		{
			var questionIndex = url.IndexOf('?');

			if (questionIndex == -1)
				return new List<NameValue>();

			var parameters = url.Substring(questionIndex + 1);
			var result = new List<NameValue>();

			if (!String.IsNullOrEmpty(parameters))
			{
				var parts = parameters.Split('&');

				foreach (var part in parts)
					if (!string.IsNullOrEmpty(part) && !part.StartsWith("oauth_"))
					{
						if (part.IndexOf('=') != -1)
						{
							var nameValue = part.Split('=');
							result.Add(new NameValue(nameValue[0], nameValue[1]));
						}
						else
							result.Add(new NameValue(part, String.Empty));
					}
			}

			return result;
		}

		/// <summary>
		/// Generate base string
		/// </summary>
		/// <param name="url"></param>
		/// <param name="httpMethod"></param>
		/// <param name="oAuthParameters"></param>
		/// <returns></returns>
		private static string GenerateSignatureBaseString(string url, string httpMethod, List<NameValue> oAuthParameters)
		{
			oAuthParameters.Sort(new Comparer());

			var uri = new Uri(url);
			var normalizedUrl = string.Format("{0}://{1}", uri.Scheme, uri.Host);

			if (!((uri.Scheme == "http" && uri.Port == 80) || (uri.Scheme == "https" && uri.Port == 443)))
				normalizedUrl += ":" + uri.Port;

			normalizedUrl += uri.AbsolutePath;

			var normalizedRequestParameters = NormalizeOAuthParameters(oAuthParameters);

			var signatureBaseSb = new StringBuilder();
			signatureBaseSb.AppendFormat("{0}&", httpMethod);
			signatureBaseSb.AppendFormat("{0}&", UrlEncode(normalizedUrl));
			signatureBaseSb.AppendFormat("{0}", UrlEncode(normalizedRequestParameters));

			return signatureBaseSb.ToString();
		}

		/// <summary>
		/// Function to generate signature
		/// </summary>
		/// <param name="consumerSecret"></param>
		/// <param name="signatureBaseString"></param>
		/// <param name="tokenSecret"></param>
		/// <returns></returns>
		private static string GenerateSignature(string consumerSecret, string signatureBaseString, string tokenSecret = null)
		{
			var hmacsha1 = new HMACSHA1 { Key = Encoding.ASCII.GetBytes(String.Format("{0}&{1}", UrlEncode(consumerSecret), String.IsNullOrEmpty(tokenSecret) ? "" : UrlEncode(tokenSecret))) };
			return ComputeHash(hmacsha1, signatureBaseString);
		}

		/// <summary>
		/// Url encoding
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		private static string UrlEncode(string value)
		{
			if (String.IsNullOrEmpty(value))
				return String.Empty;

			var sb = new StringBuilder();

			foreach (var @char in value)
			{
				if (ReservedCharacters.IndexOf(@char) == -1)
					sb.Append(@char);
				else
					sb.AppendFormat("%{0:X2}", (int)@char);
			}

			return sb.ToString();
		}

		/// <summary>
		/// Function to get absolute url from route
		/// </summary>
		/// <param name="route">The route.</param>
		/// <returns></returns>
		private static string GetUrlFromRoute(string route)
		{
			var url = HttpContext.Current.Request.Url;
			var port = (url.Port == 80 ? "" : ":" + url.Port);

			return url.Scheme + "://" + url.Host + port + VirtualPathUtility.ToAbsolute("~/" + route);
		}

		#endregion Private methods

		#region Authorization Header functions

		/// <summary>
		/// Resets _oauthParameters var
		/// </summary>
		private void ResetOAuthParameters()
		{
			_oauthParameters.Clear();
		}

		/// <summary>
		/// Register basic oauth parameters
		/// </summary>
		/// <param name="url"></param>
		/// <param name="timestamp"></param>
		/// <param name="nounce"></param>
		private void RegisterBasicOAuthParameters(string url, string timestamp, string nounce)
		{
			_oauthParameters = ExtractQueryStrings(url);
			_oauthParameters.Add(new NameValue(OAuthParameters.ConsumerKey, _settings.OAuthConsumerKey));
			_oauthParameters.Add(new NameValue(OAuthParameters.SignatureMethod, SignatureMethod.HMACSHA1));
			_oauthParameters.Add(new NameValue(OAuthParameters.Timestamp, timestamp));
			_oauthParameters.Add(new NameValue(OAuthParameters.Nounce, nounce));
			_oauthParameters.Add(new NameValue(OAuthParameters.Version, _settings.OAuthVersion));
		}

		/// <summary>
		/// Function to return request token authorization header
		/// </summary>
		/// <returns></returns>
		public string GetRequestTokenAuthorizationHeader()
		{
			var timestamp = GenerateTimeStamp();
			var nounce = GenerateNonce(timestamp);

			ResetOAuthParameters();

			// Register basic oauth parameters
			RegisterBasicOAuthParameters(_settings.OAuthRequestTokenUrl, timestamp, nounce);

			_oauthParameters.Add(new NameValue(OAuthParameters.Callback, CallbackUrl));

			var signatureBaseString = GenerateSignatureBaseString(_settings.OAuthRequestTokenUrl, HttpMethod.Post, _oauthParameters);
			var signature = GenerateSignature(_settings.OAuthConsumerSecret, signatureBaseString);

			// Create header string
			var header = CreateAuthrorzationHeaderString(signature);
			return header;
		}

		/// <summary>
		/// Function to return access token authorization header
		/// </summary>
		/// <returns></returns>
		public string GetAccessTokenAuthorizationHeader()
		{
			var timestamp = GenerateTimeStamp();
			var nounce = GenerateNonce(timestamp);

			ResetOAuthParameters();

			// Register basic oauth parameters
			RegisterBasicOAuthParameters(_settings.OAuthRequestAccessTokenUrl, timestamp, nounce);

			_oauthParameters.Add(new NameValue(OAuthParameters.Token, Token));
			_oauthParameters.Add(new NameValue(OAuthParameters.Verifier, Verifier));

			var signatureBaseString = GenerateSignatureBaseString(_settings.OAuthRequestAccessTokenUrl, HttpMethod.Post, _oauthParameters);
			var signature = GenerateSignature(_settings.OAuthConsumerSecret, signatureBaseString, TokenSecret);

			// Create header string
			var header = CreateAuthrorzationHeaderString(signature);
			return header;
		}

		/// <summary>
		/// Function to return profile authorization header
		/// </summary>
		/// <returns></returns>
		public string GetUserProfileAuthorizationHeader()
		{
			var timestamp = GenerateTimeStamp();
			var nounce = GenerateNonce(timestamp);

			ResetOAuthParameters();

			// Register basic oauth parameters
			RegisterBasicOAuthParameters(GetProfileUrl(), timestamp, nounce);

			_oauthParameters.Add(new NameValue(OAuthParameters.Token, AccessToken));

			var signatureBaseString = GenerateSignatureBaseString(_settings.OAuthRequestProfileUrl, HttpMethod.Get, _oauthParameters);
			var signature = GenerateSignature(_settings.OAuthConsumerSecret, signatureBaseString, AccessTokenSecret);

			// Create header string
			var header = CreateAuthrorzationHeaderString(signature);
			return header;
		}

		/// <summary>
		/// Function to create authorization header string
		/// </summary>
		/// <param name="signature"></param>
		/// <returns></returns>
		public string CreateAuthrorzationHeaderString(string signature)
		{
			var sb = new StringBuilder();
			sb.Append("OAuth ");
			sb.AppendFormat("realm=\"{0}\", ", _settings.OAuthRealm);
			sb.AppendFormat("{0}=\"{1}\", ", OAuthParameters.Signature, UrlEncode(signature));

			foreach (var oauthParameter in _oauthParameters)
			{
				if (oauthParameter.Name.ToLower() == "scope" || oauthParameter.Name.ToLower() == "format") continue;
				sb.AppendFormat("{0}=\"{1}\", ", oauthParameter.Name, UrlEncode(oauthParameter.Value));
			}

			sb = sb.Remove(sb.Length - 2, 2);

			return sb.ToString();
		}

		#endregion
	}

	#region Supporting classes

	/// <summary>
	/// OAuth parameters constants
	/// </summary>
	public static class OAuthParameters
	{
		public const string ConsumerKey = "oauth_consumer_key";
		public const string SignatureMethod = "oauth_signature_method";
		public const string Signature = "oauth_signature";
		public const string Timestamp = "oauth_timestamp";
		public const string Nounce = "oauth_nonce";
		public const string Version = "oauth_version";
		public const string Callback = "oauth_callback";
		public const string Verifier = "oauth_verifier";
		public const string Token = "oauth_token";
		public const string TokenSecret = "oauth_token_secret";
	}

	/// <summary>
	/// Signature methods constants
	/// </summary>
	public static class SignatureMethod
	{
		public const string HMACSHA1 = "HMAC-SHA1";
		public const string RSASHA1 = "RSA-SHA1";
		public const string PLAINTEXT = "PLAINTEXT";
	}

	/// <summary>
	/// Name value collections
	/// </summary>
	public class NameValue
	{
		public string Name { get; set; }
		public string Value { get; set; }

		public NameValue(string name, string value)
		{
			Name = name;
			Value = value;
		}
	}

	/// <summary>
	/// OAuth versions
	/// </summary>
	public class OAuthVersion
	{
		public static string V1 = "1.0";
		public static string V2 = "2.0";
	}

	public class OAuthProfile : ISSOProfile
	{
		private string _screenName;

		public string Id { get; set; }

		public string ScreenName
		{
			get
			{
				if (_screenName.IsNullOrEmpty())
					_screenName = FirstName;

				return _screenName;
			}

			set { _screenName = value; }
		}

		public string EmailAddress { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string CreateRoles { get; set; }
		public string UpdateRoles { get; set; }

		public void CheckIsValid()
		{
			if (Id.IsNullOrEmpty() || Id.Length > 255)
				throw new InvalidDataException("ProfileId is invalid");

			if (EmailAddress.IsNullOrEmpty() || EmailAddress.Length > 200)
				throw new InvalidDataException("EmailAddress is invalid");

			if (FirstName.IsNullOrEmpty() || FirstName.Length > 50)
				throw new InvalidDataException("FirstName is invalid");

			if (LastName.IsNullOrEmpty() || LastName.Length > 50)
				throw new InvalidDataException("LastName is invalid");
		}
	}

	/// <summary>
	/// HttpMethods 
	/// </summary>
	public class HttpMethod
	{
		public static string Get = "GET";
		public static string Post = "POST";
	}

	/// <summary>
	/// Object Comparer
	/// </summary>
	public class Comparer : IComparer<NameValue>
	{
		public int Compare(NameValue x, NameValue y)
		{
			return x.Name == y.Name ? String.CompareOrdinal(x.Value, y.Value) : String.CompareOrdinal(x.Name, y.Name);
		}
	}

	public class OAuthException : Exception
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="OAuthException"/> class.
		/// </summary>
		/// <param name="text">The text.</param>
		public OAuthException(string text)
			: base(text)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="OAuthException"/> class.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="innerException">The inner exception.</param>
		public OAuthException(string text, Exception innerException) : base(text, innerException)
		{ }
	}

	public class OAuthNetworkException : OAuthException
	{
		public string Url { get; set; }
		public HttpStatusCode StatusCode { get; set; }
		public string ResponseDataString { get; set; }
		public string OAuthHeader { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="OAuthNetworkException"/> class.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="url">The URL.</param>
		/// <param name="statusCode">The status code.</param>
		/// <param name="responseDataString">The response data string.</param>
		/// <param name="oAuthHeader">The o auth header.</param>
		public OAuthNetworkException(string text, string url, HttpStatusCode statusCode, string responseDataString, string oAuthHeader) : base(text)
		{
			Url = url;
			StatusCode = statusCode;
			ResponseDataString = responseDataString;
			OAuthHeader = oAuthHeader;
		}
	}

	#endregion
}
