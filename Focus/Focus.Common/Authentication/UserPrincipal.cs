﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Security.Principal;

using Focus.Core.Models;

#endregion

namespace Focus.Common.Authentication
{
	public class UserPrincipal : IPrincipal
	{
		private readonly IIdentity _identity;
		private IUserContext _userContext;

		/// <summary>
		/// Initializes a new instance of the <see cref="UserPrincipal"/> class.
		/// </summary>
		/// <param name="identity">The identity.</param>
		/// <param name="userContext">The user context.</param>
		public UserPrincipal(IIdentity identity, IUserContext userContext)
		{
			_identity = identity;
			_userContext = userContext;
		}

		/// <summary>
		/// Gets the state of the user.
		/// </summary>
		/// <value>The state of the user.</value>
		public IUserContext UserContext
		{
			get { return _userContext; }
			set { _userContext = value;  }
		}

		/// <summary>
		/// Gets the identity of the current principal.
		/// </summary>
		/// <value></value>
		/// <returns>The <see cref="T:System.Security.Principal.IIdentity"/> object associated with the current principal.</returns>
		public IIdentity Identity
		{
			get { return _identity; }
		}

		/// <summary>
		/// Determines whether the current principal belongs to the specified role.
		/// </summary>
		/// <param name="role">The name of the role for which to check membership.</param>
		/// <returns>
		/// true if the current principal is a member of the specified role; otherwise, false.
		/// </returns>
		public bool IsInRole(string role)
		{
			return _userContext.IsInRole(role);
		}
	}
}
