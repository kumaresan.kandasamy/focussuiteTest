﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives



#endregion

namespace Focus.Common
{
	public enum HelpTypes
	{
		PrivacyAndSecurity = 1,
		Help = 2,
		FrequentlyAskedQuestions = 3,
		TermsOfUse = 4
	}

	/// <summary>
	/// An enumeration of icon font providers.
	/// </summary>
	public enum IconProvider
	{
		/// <summary>
		/// None.
		/// </summary>
		None,
		/// <summary>
		/// Glyphicons.
		/// </summary>
		Glyphicons,
		/// <summary>
		/// Font-Awesome.
		/// </summary>
		FontAwesome
	}

	/// <summary>
	/// An enumeration of icon placement within a button.
	/// </summary>
	public enum IconPlacement
	{
		/// <summary>
		/// Icon to the left of the button text.
		/// </summary>
		Left,
		/// <summary>
		/// Icon to the right of the button text.
		/// </summary>
		Right
	}
}
