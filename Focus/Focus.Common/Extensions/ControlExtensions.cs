﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using AjaxControlToolkit;

using Framework.Core;

using Focus.Common.Controls.Server;
using Focus.Core.Views;
using Focus.Common.Localisation;

#endregion

namespace Focus.Common.Extensions
{
	public static class ControlExtensions
	{
		/// <summary>
		/// Determines whether the page is the specified Url.
		/// </summary>
		/// <param name="page">The page.</param>
		/// <param name="url">The URL.</param>
		/// <returns>
		/// 	<c>true</c> if the page is the specified url; otherwise, <c>false</c>.
		/// </returns>
		public static bool Is(this Page page, string url)
		{
			return page.Request.Path.StartsWith(url, StringComparison.OrdinalIgnoreCase);
		}

		/// <summary>
		/// Determines whether the page is not for the specified Url.
		/// </summary>
		/// <param name="page">The page.</param>
		/// <param name="url">The URL.</param>
		/// <returns>
		/// 	<c>true</c> if the page is not the specified url; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsNot(this Page page, string url)
		{
			return !page.Request.Path.StartsWith(url, StringComparison.OrdinalIgnoreCase);
		}

		/// <summary>
		/// Determines whether the page URL is a descendent of the given folder.
		/// </summary>
		/// <param name="page">The page.</param>
		/// <param name="folderUrl">The folder URL.</param>
		/// <returns>
		///		<c>true</c> if the page is a descendant of the given folder, else <c>false</c>.
		/// </returns>
		public static bool IsDescendantOfFolder(this Page page, string folderUrl)
		{
			if (!folderUrl.EndsWith("/", StringComparison.OrdinalIgnoreCase))
			{
				folderUrl += "/";
			}
			return page.Is(folderUrl);
		}

		/// <summary>
		/// Finds the control recursive.
		/// </summary>
		/// <param name="root">The root.</param>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		public static Control FindControlRecursive(this Control root, string id)
		{
			return root.ID == id ? root : (from Control c in root.Controls select FindControlRecursive(c, id)).FirstOrDefault(t => t != null);
		}

		/// <summary>
		/// Trims the specified text box.
		/// </summary>
		/// <param name="textBox">The text box.</param>
		/// <param name="numberOfCharacters">The number of characters.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static string TextTrimmed(this TextBox textBox, int? numberOfCharacters = null, string defaultValue = "")
		{
			textBox.Text = (textBox.Text ?? string.Empty).Trim();
			var text = (numberOfCharacters != null && textBox.Text.Length > numberOfCharacters) ? textBox.Text.Substring(0, numberOfCharacters.Value) + "..." : textBox.Text;
			return (string.IsNullOrEmpty(text) ? defaultValue : text);
		}

		/// <summary>
		/// Clears the text box
		/// </summary>
		/// <param name="textBox">The text box.</param>
		public static void ResetText( this TextBox textBox )
		{
			textBox.Text = string.Empty;
		}

		/// <summary>
		/// Ases the nullable decimal.
		/// </summary>
		/// <param name="textBox">The text box.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static decimal? AsNullableDecimal(this TextBox textBox, decimal? defaultValue = null)
		{
			var text = textBox.TextTrimmed();
			return text.IsNotNullOrEmpty() ? text.ToDecimal() : null;
		}

		/// <summary>
		/// Converts a selected value into an enum value.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="listControl">The list control.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static T SelectedValueToEnum<T>(this ListControl listControl, T defaultValue = default(T)) where T : struct
		{
			var selectedValue = listControl.SelectedValue ?? string.Empty;
			if (!Enum.IsDefined(typeof(T), selectedValue))
				return defaultValue;

			return (T)Enum.Parse(typeof(T), selectedValue);
		}


		/// <summary>
		/// Selecteds the value to flag enum.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="listControl">The list control.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static int SelectedValueToFlagEnum<T>(this ListControl listControl, T defaultValue = default(T)) where T : struct
		{
			try
			{
				var selectedValues = listControl.SelectedValue.Split(',');

				return selectedValues.Sum(value => (int)Enum.Parse(typeof(T), value));
			}
			catch
			{
				return 0;
			}
		}

		/// <summary>
		/// Selecteds the value to flag enum.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="value">The value.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static int SelectedValueToFlagEnum<T>(this string str, T defaultValue = default(T)) where T : struct
		{
			try
			{
				var selectedValues = str.Split(',');

				return selectedValues.Sum(value => (int)Enum.Parse(typeof(T), value));
			}
			catch
			{
				return 0;
			}
		}

		/// <summary>
		/// Values to enum.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="listItem">The list item.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static T ValueToEnum<T>(this ListItem listItem, T defaultValue = default(T)) where T : struct
		{
			try
			{
				var value = (T)Enum.Parse(typeof(T), (listItem.Value ?? string.Empty));
				return !Enum.IsDefined(typeof(T), value) ? defaultValue : value;
			}
			catch (Exception)
			{
				return defaultValue;
			}
		}

		/// <summary>
		/// Values to long.
		/// </summary>
		/// <param name="listItem">The list item.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static long? ValueToLong(this ListItem listItem, long? defaultValue = null)
		{
			try
			{
				return listItem.Value.ToLong();
			}
			catch (Exception)
			{
				return defaultValue;
			}
		}

		/// <summary>
		/// Converts a selected value into a long value.
		/// </summary>
		/// <param name="listControl">The list control.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static long SelectedValueToLong(this ListControl listControl, long defaultValue = default(long))
		{
			long result;
			var success = long.TryParse((listControl.SelectedValue ?? string.Empty), out result);

			return (success) ? result : defaultValue;
		}

		/// <summary>
		/// Converts a selected value into a nullable long value.
		/// </summary>
		/// <param name="listControl">The list control.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static long? SelectedValueToNullableLong(this ListControl listControl, long? defaultValue = null)
		{
			long result;
			var success = long.TryParse((listControl.SelectedValue ?? string.Empty), out result);

			return (success) ? result : defaultValue;
		}

		/// <summary>
		/// Selecteds the value to nullable double.
		/// </summary>
		/// <param name="listControl">The list control.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static double? SelectedValueToNullableDouble(this ListControl listControl, double? defaultValue = null)
		{
			double result;
			var success = double.TryParse((listControl.SelectedValue ?? string.Empty), out result);

			return (success) ? result : defaultValue;
		}

		/// <summary>
		/// Converts a selected value into a int value.
		/// </summary>
		/// <param name="listControl">The list control.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static int SelectedValueToInt(this ListControl listControl, int defaultValue = default(int))
		{
			int result;
			var success = int.TryParse((listControl.SelectedValue ?? string.Empty), out result);

			return (success) ? result : defaultValue;
		}

		/// <summary>
		/// Binds the lookup.
		/// </summary>
		/// <param name="dropDownList">The drop down list.</param>
		/// <param name="lookupItems">The lookup items.</param>
		/// <param name="currentValue">The current value.</param>
		/// <param name="topDefault">The top default.</param>
		public static void BindLookup(this DropDownList dropDownList, IList<LookupItemView> lookupItems, string currentValue, string topDefault)
		{
			BindLookup(dropDownList, lookupItems, currentValue, topDefault, String.Empty);
		}

		/// <summary>
		/// Binds the lookup.
		/// </summary>
		/// <param name="dropDownList">The drop down list.</param>
		/// <param name="lookupItems">The lookup items.</param>
		/// <param name="currentValue">The current value.</param>
		/// <param name="topDefault">The top default.</param>
		/// <param name="topDefaultValue">The top default value.</param>
		/// <param name="excludeKeys">A list of specific keys to exclude</param>
		public static void BindLookup(this DropDownList dropDownList, IList<LookupItemView> lookupItems, string currentValue, string topDefault, string topDefaultValue, List<string> excludeKeys = null)
		{
			dropDownList.DataSource = excludeKeys.IsNull()
				? lookupItems
				: lookupItems.Where(li => !excludeKeys.Contains(li.Key));

			dropDownList.DataValueField = "Id";
			dropDownList.DataTextField = "Text";
			dropDownList.DataBind();

			if (!String.IsNullOrEmpty(currentValue) && dropDownList.ValueInListControl(currentValue))
				dropDownList.SelectedValue = currentValue;

			if (!String.IsNullOrEmpty(topDefault))
				dropDownList.Items.Insert(0, new ListItem(topDefault, topDefaultValue));
		}

		/// <summary>
		/// Binds the lookup.
		/// </summary>
		/// <param name="dropDownList">The drop down list.</param>
		/// <param name="lookupItems">The lookup items.</param>
		/// <param name="currentValue">The current value.</param>
		/// <param name="bindExternalId">if set to <c>true</c> [bind external id].</param>
		public static void BindLookup(this DropDownList dropDownList, IList<LookupItemView> lookupItems, string currentValue = null, bool bindExternalId = false)
		{
			dropDownList.DataSource = lookupItems;

			dropDownList.DataValueField = bindExternalId ? "ExternalId" : "Id";

			dropDownList.DataTextField = "Text";
			dropDownList.DataBind();

			if (!String.IsNullOrEmpty(currentValue) && dropDownList.ValueInListControl(currentValue))
				dropDownList.SelectedValue = currentValue;
		}

		/// <summary>
		/// Binds the dictionary.
		/// </summary>
		/// <param name="dropDownList">The drop down list.</param>
		/// <param name="dictionary">The dictionary.</param>
		/// <param name="currentValue">The current value.</param>
		/// <param name="topDefault">The top default.</param>
		public static void BindDictionary(this DropDownList dropDownList, Dictionary<string, string> dictionary, string currentValue, string topDefault)
		{
			BindDictionary(dropDownList, dictionary, currentValue, topDefault, String.Empty);
		}

		/// <summary>
		/// Binds the enum list automatic control list.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="checkBoxList">The check box list.</param>
		/// <param name="selectedList">The selected list.</param>
		public static void BindEnumListToControlList<T>(this CheckBoxList checkBoxList, List<T> selectedList)
		{
			if (selectedList.IsNullOrEmpty()) return;

			foreach (var item in selectedList)
			{
				var checkBoxItem = checkBoxList.Items.Cast<ListItem>().SingleOrDefault(x => x.Value == item.ToString());
				if (checkBoxItem.IsNotNull()) checkBoxItem.Selected = true;
			}
		}

		/// <summary>
		/// Unbinds a checkbox list to a list of null-able enum representing the selected values
		/// </summary>
		/// <typeparam name="T">The enum type</typeparam>
		/// <param name="list">The check box list</param>
		/// <returns>The list of selected enums</returns>
		public static List<T?> UnbindNullableEnumControlList<T>(this CheckBoxList list) where T : struct
		{
			return list.Items.Cast<ListItem>().Where(x => x.Selected).Select(x => (T?)Enum.Parse(typeof(T), x.Value)).ToList();
		}

		/// <summary>
		/// Unbinds a radio button list to a list of null-able enum representing the selected values
		/// </summary>
		/// <typeparam name="T">The enum type</typeparam>
		/// <param name="list">The check box list</param>
		/// <returns>The list of selected enums</returns>
		public static T? UnbindNullableEnumControlList<T>(this RadioButtonList list) where T : struct
		{
			return list.Items.Cast<ListItem>().Where(x => x.Selected).Select(x => (T?)Enum.Parse(typeof(T), x.Value)).SingleOrDefault();
		}

		/// <summary>
		/// Binds the dictionary.
		/// </summary>
		/// <param name="dropDownList">The drop down list.</param>
		/// <param name="dictionary">The dictionary.</param>
		/// <param name="currentValue">The current value.</param>
		/// <param name="topDefault">The top default.</param>
		/// <param name="topDefaultValue">The top default value.</param>
		public static void BindDictionary(this DropDownList dropDownList, Dictionary<string, string> dictionary, string currentValue, string topDefault, string topDefaultValue)
		{
			dropDownList.DataSource = dictionary;
			dropDownList.DataValueField = "Key";
			dropDownList.DataTextField = "Value";
			dropDownList.DataBind();

			if (!String.IsNullOrEmpty(currentValue) && dropDownList.ValueInListControl(currentValue)) dropDownList.SelectedValue = currentValue;
			if (!String.IsNullOrEmpty(topDefault)) dropDownList.Items.Insert(0, new ListItem(topDefault, topDefaultValue));
		}

		/// <summary>
		/// Binds the list item.
		/// </summary>
		/// <param name="dropDownList">The drop down list.</param>
		/// <param name="itemList">The list items.</param>
		/// <param name="topDefault">The top default.</param>
		/// <param name="topDefaultValue">The top default value.</param>
		public static void BindListItem(this DropDownList dropDownList, IOrderedEnumerable<ListItem> itemList, string topDefault, string topDefaultValue)
		{
			dropDownList.Items.Clear();
			dropDownList.DataSource = itemList;
			dropDownList.DataValueField = "Value";
			dropDownList.DataTextField = "Text";
			dropDownList.DataBind();

			if (!String.IsNullOrEmpty(topDefault)) dropDownList.Items.Insert(0, new ListItem(topDefault, topDefaultValue));
		}

		/// <summary>
		/// Binds the lookup.
		/// </summary>
		/// <param name="comboBox">The combo box.</param>
		/// <param name="lookupItems">The lookup items.</param>
		/// <param name="currentValue">The current value.</param>
		/// <param name="topDefault">The top default.</param>
		public static void BindLookup(this ComboBox comboBox, IList<LookupItemView> lookupItems, string currentValue, string topDefault)
		{
			comboBox.DataSource = lookupItems;
			comboBox.DataValueField = "Id";
			comboBox.DataTextField = "Text";
			comboBox.DataBind();

			if (!String.IsNullOrEmpty(currentValue)) comboBox.Text = currentValue;
			if (!String.IsNullOrEmpty(topDefault)) comboBox.Items.Insert(0, new ListItem(topDefault, string.Empty));
		}

		public static void BindLookup(this CheckBoxList checkBoxList, IList<LookupItemView> lookupItems, string currentValue = null, bool bindExternalId = false, bool bindKey = false)
		{
			checkBoxList.DataSource = lookupItems;

			checkBoxList.DataValueField = !bindExternalId ? "Id" : "ExternalId";

		  if (bindExternalId)
		    checkBoxList.DataValueField = "ExternalId";
      else if (bindKey)
        checkBoxList.DataValueField = "Key";
      else
        checkBoxList.DataValueField = "Id";

			checkBoxList.DataTextField = "Text";
			checkBoxList.DataBind();

			if (!String.IsNullOrEmpty(currentValue) && checkBoxList.ValueInListControl(currentValue))
			{
				foreach (var checkbox in checkBoxList.Items.Cast<ListItem>().Where(checkbox => checkbox.Value == currentValue))
				{
					checkbox.Selected = true;
				}
			}
		}

		public static void BindLookup(this ToolTipCheckBoxList checkBoxList, IList<LookupItemView> lookupItems, List<string> tooltips, string currentValue = null, bool bindExternalId = false)
		{
			checkBoxList.DataSource = lookupItems;
			checkBoxList.DataTooltips = tooltips;

			checkBoxList.DataValueField = !bindExternalId ? "Id" : "ExternalId";
			checkBoxList.DataTextField = "Text";

			checkBoxList.DataBind();

			if (!String.IsNullOrEmpty(currentValue) && checkBoxList.ValueInListControl(currentValue))
			{
				foreach (var checkbox in checkBoxList.Items.Cast<ListItem>().Where(checkbox => checkbox.Value == currentValue))
				{
					checkbox.Selected = true;
				}
			}
		}

		/// <summary>
		/// Binds the lookup.
		/// </summary>
		/// <param name="radioButtonList">The radio button list.</param>
		/// <param name="lookupItems">The lookup items.</param>
		/// <param name="currentValue">The current value.</param>
		public static void BindLookup(this RadioButtonList radioButtonList, IList<LookupItemView> lookupItems, string currentValue)
		{
			radioButtonList.DataSource = lookupItems;
			radioButtonList.DataValueField = "Id";
			radioButtonList.DataTextField = "Text";
			radioButtonList.DataBind();

			//if (!String.IsNullOrEmpty(currentValue) && radioButtonList.ValueInListControl(currentValue)) radioButtonList.SelectedValue = currentValue;
		}

		/// <summary>
		/// Converts a string to list.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="separator">The separator.</param>
		/// <returns></returns>
		public static List<string> ToList(this string value, char[] separator)
		{
			var stringArray = value.Split(separator).ToList();
			var stringList = new List<string>(stringArray.Count);
			stringList.AddRange(stringArray);
			return stringList;
		}

		/// <summary>
		/// Selects the value in the List Control.
		/// </summary>
		/// <param name="listControl">The list control.</param>
		/// <param name="selectedValue">The selected value.</param>
		/// <param name="defaultValue">The default value.</param>
		public static void SelectValue(this ListControl listControl, string selectedValue, string defaultValue = "")
		{
			if (ValueInListControl(listControl, selectedValue))
			{
				listControl.SelectedValue = selectedValue;
				return;
			}

			if (ValueInListControl(listControl, defaultValue))
				listControl.SelectedValue = defaultValue;
		}

		/// <summary>
		/// Values the in drop down list.
		/// </summary>
		/// <param name="listControl">The list control.</param>
		/// <param name="searchValue">The search value.</param>
		/// <returns></returns>
		private static bool ValueInListControl(this ListControl listControl, string searchValue)
		{
			if (listControl != null && !String.IsNullOrEmpty(searchValue))
				return listControl.Items.Cast<ListItem>().Any(listItem => listItem.Value.Equals(searchValue, StringComparison.InvariantCultureIgnoreCase));

			return false;
		}

		/// <summary>
		/// Adds a localised top default to the list.
		/// </summary>
		/// <param name="listItemCollection">The list item collection.</param>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <param name="dropDownValue">The drop down value.</param>
		public static void AddLocalisedTopDefault(this ListItemCollection listItemCollection, string key, string defaultValue, string dropDownValue = "")
		{
			listItemCollection.Insert(0, new ListItem(Localiser.Instance().Localise(key, defaultValue), dropDownValue));
		}

		/// <summary>
		/// Adds the localised list item to either a position or the bottom
		/// </summary>
		/// <param name="listItemCollection">The list item collection.</param>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <param name="itemValue">The item value.</param>
		/// <param name="position">The position.</param>
		public static void AddLocalised(this ListItemCollection listItemCollection, string key, string defaultValue, string itemValue, int? position = null)
		{
			if (position.HasValue)
				listItemCollection.Insert(position.Value, new ListItem(Localiser.Instance().Localise(key, defaultValue), itemValue));
			else
				listItemCollection.Add(new ListItem(Localiser.Instance().Localise(key, defaultValue), itemValue));
		}

		/// <summary>
		/// Adds the enum.
		/// </summary>
		/// <param name="listItemCollection">The list item collection.</param>
		/// <param name="enumValue">The enum value.</param>
		public static void AddEnum(this ListItemCollection listItemCollection, Enum enumValue)
		{
			listItemCollection.Add(new ListItem(Localiser.Instance().Localise(enumValue), enumValue.ToString()));
		}

		/// <summary>
		/// Adds the enum.
		/// </summary>
		/// <param name="listItemCollection">The list item collection.</param>
		/// <param name="enumValue">The enum value.</param>
		/// <param name="defaultText">The default text.</param>
		public static void AddEnum(this ListItemCollection listItemCollection, Enum enumValue, string defaultText)
		{
			listItemCollection.Add(new ListItem(Localiser.Instance().Localise(enumValue, defaultText), enumValue.ToString()));
		}

		/// <summary>
		/// Adds the enum with int value.
		/// </summary>
		/// <param name="listItemCollection">The list item collection.</param>
		/// <param name="enumValue">The enum value.</param>
		/// <param name="defaultText">The default text.</param>
		public static void AddEnumWithIntValue(this ListItemCollection listItemCollection, Enum enumValue, string defaultText)
		{
			listItemCollection.Add(new ListItem(Localiser.Instance().Localise(enumValue, defaultText), Convert.ToInt32(enumValue).ToString()));
		}

		/// <summary>
		/// Formats the text for display as HTML.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <returns></returns>
		public static string FormatForDisplayAsHtml(this string text)
		{
			if (text.IsNullOrEmpty())
				return "";

			return text.Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>");
		}

		/// <summary>
		/// Formats a phone number.
		/// </summary>
		/// <param name="text">The unformatted phone number.</param>
		/// <param name="format">The format to output.</param>
		/// <returns>The formatted phone number.</returns>
		public static string FormatPhoneNumber(this string text, string format)
		{
			if (!Regex.IsMatch(text, "[0-9]+"))
				return text;

			return text.AsLong().ToString(format);
		}

		/// <summary>
		/// Links the disabled.
		/// </summary>
		/// <param name="obj">The obj.</param>
		/// <param name="disable">if set to <c>true</c> [disable].</param>
		public static void LinkDisabled(this HtmlAnchor obj, bool disable)
		{
			if (disable)
			{
				obj.Attributes.Remove("href");
				obj.Style["color"] = "gray";
				obj.Style["text-decoration"] = "none";
			}
		}

		/// <summary>
		/// Convert a list to csv string.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="values">The values.</param>
		/// <param name="separator">The separator.</param>
		/// <returns></returns>
		public static string JoinStrings<T>(this IEnumerable<T> values, string separator)
		{
			var stringValues = values.Select(item => (item.Equals(null) ? String.Empty : item.ToString()));
			return String.Join(separator, stringValues.ToArray());
		}

		/// <summary>
		/// Resets the specified list items.
		/// </summary>
		/// <param name="listItems">The list items.</param>
		public static void Reset(this ListItemCollection listItems)
		{
			foreach (ListItem listItem in listItems)
			{
				listItem.Selected = false;
			}
		}

		/// <summary>
		/// Gets the viewstate value for the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="stateBag">The viewstate.</param>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns>The viewstate value specified by <paramref name="key"/>, or the default value if not found.</returns>
		internal static T GetValue<T>(this IDictionary stateBag, string key, T defaultValue = default(T))
		{
			var obj = stateBag[key];
			return (obj == null ? defaultValue : (T)obj);
		}

		/// <summary>
		/// Sets the viewstate value for the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="stateBag">The viewstate.</param>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		internal static void SetValue<T>(this IDictionary stateBag, string key, T value)
		{
			stateBag[key] = value;
		}
	}
}