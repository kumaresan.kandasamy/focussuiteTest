﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections;
using System.Linq;
using System.Threading;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Linq;
using System.Xml.XPath;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Web.Core.Models;
using Framework.Core;
using Framework.DataAccess;
using HtmlAgilityPack;

#endregion

namespace Focus.Common.Extensions
{
    public static class GeneralExtensions
    {
        /// <summary>
        /// Returns the item as an Enum.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public static T? AsEnum<T>(this object item) where T : struct
        {
            if (item.IsNull())
                return null;

            if (item is string)
            {
                T result;
                if (Enum.TryParse((string)item, true, out result))
                    return result;
            }
            else if (item is int || item is char)
            {
                var val = item is char ? (char)item : (int)item;
                var temp = Enum.GetName(typeof(T), val);

                return (T)Enum.Parse(typeof(T), temp, true);
            }

            return null;
        }

        /// <summary>
        /// Returns the item as an Enum defaulting if required
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <param name="defaultEnum">The default enum.</param>
        /// <returns></returns>
        public static T AsEnum<T>(this object item, T defaultEnum) where T : struct
        {
            if (item.IsNull())
                return defaultEnum;

            if (item is string)
            {
                T result;
                if (Enum.TryParse((string)item, true, out result))
                    return result;
            }
            else if (item is int || item is char)
            {
                var val = item is char ? (char)item : (int)item;
                var temp = Enum.GetName(typeof(T), val);

                return (T)Enum.Parse(typeof(T), temp, true);
            }

            return defaultEnum;
        }

        /// <summary>
        /// Returns null if the item is it's default.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public static T? AsNullIfDefault<T>(this object item) where T : struct
        {
            if (item == null)
                return null;

            if ((item is DateTime && (DateTime)item == default(DateTime))
                || (item is int && (int)item == default(int))
                || (item is long && (long)item == default(long))
                || (item is float && Math.Abs((float)item - default(float)) < 0.000001)
                || (item is double && Math.Abs((double)item - default(double)) < 0.0000000001))
                return null;

            return (T)item;
        }

        /// <summary>
        /// Returns the bool value as "yes" or "no" string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string AsYesNo(this bool? value)
        {
            return (value.HasValue && value == true) ? "yes" : "no";
        }

        /// <summary>
        /// Sets the default string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static string SetDefaultString(this string value, string defaultValue)
        {
            return string.IsNullOrEmpty(value) ? defaultValue : value;
        }

        /// <summary>
        /// Formats to onet pattern.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string FormatToOnetPattern(this string value)
        {
            return value.Length == 8 ? value.Insert(2, "-").Insert(7, ".") : value;
        }

        /// <summary>
        /// Gets the MD5 hash code for the passed value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">The obj.</param>
        /// <returns></returns>
        public static string GetMD5HashCode<T>(this T obj)
        {
            return BitConverter.ToString(System.Security.Cryptography.MD5.Create().ComputeHash(obj.ObjectToByteArray()));
        }

        /// <summary>
        /// Transform object into an integer data type.
        /// </summary>
        /// <param name="item">The object to be transformed.</param>
        /// <returns>The integer value.</returns>
        public static int AsInt32(this object item)
        {
            return AsInt32(item, default(int));
        }

        /// <summary>
        /// Transform object into an integer data type.
        /// </summary>
        /// <param name="item">The object to be transformed.</param>
        /// <param name="defaultInt">The default int.</param>
        /// <returns>The integer value.</returns>
        public static int AsInt32(this object item, int defaultInt)
        {
            if (item == null)
                return defaultInt;

            if (item is int)
                return (int)item;

            int result;
            return !int.TryParse(item.ToString(), out result) ? defaultInt : result;
        }


        /// <summary>
        /// Ases the nullable long.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public static long AsLong(this object item)
        {
            return AsLong(item, default(long));
        }

        /// <summary>
        /// Ases the long.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="defaultLong">The default long.</param>
        /// <returns></returns>
        public static long AsLong(this object item, long defaultLong)
        {
            long result;
            var success = long.TryParse(item.ToString(), out result);

            return success ? result : defaultLong;
        }


        /// <summary>
        /// Converts text titled words to title case.
        /// 
        /// Words it doesn't title are:
        /// 
        /// a, an, are, as, at, be, by, com, de, en, for, from, how, in, is, it, la, of, on, or, 
        /// that, the, this, to, was, what, when, where, who, will, with, und, www
        /// 
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public static string TitleCase(this string text)
        {
            var outText = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(text).Replace("&Amp;", "&amp;").Replace("&Gt;", "&gt;").Replace("&Lt;", "&lt;").Replace("&Apos;", "&apos;").Replace("&Quot;", "&quot;").Replace("&AMP;", "&amp;");

            outText = outText.Replace(" A ", " a ").Replace(" An ", " an ").Replace(" At ", " at ").Replace(" And ", " and ").Replace(" Are ", " are ").Replace(" As ", " as ").Replace(" Be ", " be ");
            outText = outText.Replace(" By ", " by ").Replace(" Com ", " com ").Replace(" De ", " de ").Replace(" En ", " en ").Replace(" For ", " for ").Replace(" From ", " from ");
            outText = outText.Replace(" How ", " how ").Replace(" In ", " in ").Replace(" Is ", " is ").Replace(" It ", " it ").Replace(" La ", " la ").Replace(" Of ", " of ");
            outText = outText.Replace(" On ", " on ").Replace(" Or ", " or ").Replace(" That ", " that ").Replace(" The ", " the ").Replace(" To ", " to ").Replace(" Was ", " was ");
            outText = outText.Replace(" What ", " what ").Replace(" When ", " when ").Replace(" Where ", " where ").Replace(" Who ", " who ").Replace(" Will ", " will ").Replace(" With ", " with ");
            outText = outText.Replace(" Und ", " und ").Replace(" Www ", " www ");

            return outText;
        }

        /// <summary>
        /// Sets the grade point average.
        /// </summary>
        /// <param name="gradePointAverage">The grade point average.</param>
        /// <returns></returns>
        public static string SetGradePointAverage(this GradePointAverage gradePointAverage)
        {
            return gradePointAverage.IsNotNull() ? gradePointAverage.RawGradePointAverage : "";
        }

        /// <summary>
        /// Gets the grade point average.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static GradePointAverage GetGradePointAverage(this string value)
        {
            if (value.IsNotNull())
                return (value.Contains("/") ? new GradePointAverage { Value = value.Split('/')[0].AsFloat(), Max = value.Split('/')[1].AsFloat(), RawGradePointAverage = value } : new GradePointAverage { Value = value.AsFloat(), RawGradePointAverage = value });

            return null;
        }

        /// <summary>
        /// Returns whether a job type is actually an internship or not
        /// </summary>
        /// <param name="jobType">The job type to check</param>
        /// <returns>A boolean indicating if it is an internship</returns>
        public static bool IsInternship(this JobTypes jobType)
        {
            return (OldApp_RefactorIfFound.Settings.Theme != FocusThemes.Workforce && (jobType != JobTypes.Job && jobType != JobTypes.None));
        }

        /// <summary>
        /// Transform object into an boolean data type.
        /// </summary>
        /// <param name="item">The object to be transformed.</param>
        /// <returns>The boolean value.</returns>
        public static bool AsBoolean(this object item)
        {
            return AsBoolean(item, default(bool));
        }

        /// <summary>
        /// Transform object into an boolean data type.
        /// </summary>
        /// <param name="item">The object to be transformed.</param>
        /// <param name="defaultBoolean">if set to <c>true</c> [default boolean].</param>
        /// <returns>The boolean value.</returns>
        public static bool AsBoolean(this object item, bool defaultBoolean)
        {
            if (item == null)
                return defaultBoolean;

            if (item is bool)
                return (bool)item;

            bool result;
            var value = item.ToString();

            return !bool.TryParse(value, out result) ? ((item.AsInt() > 0 || value.ToUpper() == "TRUE" || value.ToUpper() == "T" || value.ToUpper() == "YES") || defaultBoolean) : result;
        }

        /// <summary>
        /// Ases the nullable GUID.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public static Guid? AsNullableGuid(this object item)
        {
            return AsNullableGuid(item, null);
        }

        /// <summary>
        /// Ases the nullable GUID.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static Guid? AsNullableGuid(this object item, Guid? defaultValue)
        {
            if (item.IsNull())
                return defaultValue;

            if (item is Guid)
                return (Guid)item;

            Guid result;
            var value = item.ToString();

            return !Guid.TryParse(value, out result) ? defaultValue : result;
        }


        #region XElement Extension Methods

        /// <summary>
        /// Gets the element value as int32.
        /// </summary>
        /// <param name="ele">The ele.</param>
        /// <param name="elementXPath">The element X path.</param>
        /// <returns></returns>
        public static int GetElementValueAsInt32(this XElement ele, string elementXPath)
        {
            string temp = ele.XPathGetValue(elementXPath);
            return (temp != null ? temp.AsInt32() : default(int));
        }

        /// <summary>
        /// Gets the element value as string.
        /// </summary>
        /// <param name="ele">The ele.</param>
        /// <param name="elementXPath">The element X path.</param>
        /// <returns></returns>
        public static string GetElementValueAsString(this XElement ele, string elementXPath)
        {
            return (ele.XPathGetValue(elementXPath));
        }

        /// <summary>
        /// Gets the element value as date time.
        /// </summary>
        /// <param name="ele">The ele.</param>
        /// <param name="elementXPath">The element X path.</param>
        /// <returns></returns>
        public static DateTime GetElementValueAsDateTime(this XElement ele, string elementXPath)
        {
            var temp = ele.XPathGetValue(elementXPath);
            return (temp != null ? temp.AsDateTime() : default(DateTime));
        }

        /// <summary>
        /// Gets the element value as nullable boolean.
        /// </summary>
        /// <param name="ele">The ele.</param>
        /// <param name="elementXPath">The element X path.</param>
        /// <returns></returns>
        public static bool? GetElementValueAsNullableBoolean(this XElement ele, string elementXPath)
        {
            bool? value = null;
            var temp = ele.XPathGetValue(elementXPath);
            if (temp != null)
                value = temp.AsBoolean();
            return value;
        }

        /// <summary>
        /// Gets the element value as boolean.
        /// </summary>
        /// <param name="ele">The ele.</param>
        /// <param name="elementXPath">The element X path.</param>
        /// <returns></returns>
        public static bool GetElementValueAsBoolean(this XElement ele, string elementXPath)
        {
            var temp = ele.XPathGetValue(elementXPath);
            return (temp != null ? temp.AsBoolean() : default(bool));
        }


        /// <summary>
        /// Toes the XML encoded.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public static string ToXmlEncoded(this string text)
        {
            if (String.IsNullOrEmpty(text))
                return text;

            return HttpContext.Current.Server.HtmlEncode(HttpContext.Current.Server.HtmlDecode(text));
        }

        /// <summary>
        /// Elements the exists.
        /// </summary>
        /// <param name="parentNode">The parent node.</param>
        /// <param name="xpath">The xpath.</param>
        /// <returns></returns>
        public static bool ElementExists(this XElement parentNode, string xpath)
        {
            return parentNode.XPathSelectElement(xpath) != null;
        }

        /// <summary>
        /// Creates the element.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static XElement CreateElement(this XElement parentElement, string name, object value = null)
        {
            if (parentElement == null)
                return null;

            var childElement = (value == null ? new XElement(name) : new XElement(name, value));
            parentElement.Add(childElement);
            return childElement;
        }

        /// <summary>
        /// Creates the attribute.
        /// </summary>
        /// <param name="xmlElement">The XML element.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static XAttribute CreateAttribute(this XElement xmlElement, string name, object value = null)
        {
            var attribute = new XAttribute(name, value ?? "");
            xmlElement.Add(attribute);
            return attribute;
        }

        /// <summary>
        /// Gets the or create element.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <param name="xpath">The xpath.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static XElement GetOrCreateElement(this XElement parentElement, string xpath, string name, object value = null)
        {
            var childElement = parentElement.XPathSelectElement(xpath);

            if (childElement == null)
            {
                parentElement.Add((value == null ? new XElement(name) : new XElement(name, value)));
                childElement = parentElement.XPathSelectElement(xpath);
            }
            else
                childElement.Value = value.ToString();

            return childElement;
        }

        /// <summary>
        /// Gets the or create element.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <param name="xpath">The xpath.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static XElement GetOrCreateElement(this XElement parentElement, string xpath, string name)
        {
            var childElement = parentElement.XPathSelectElement(xpath);

            if (childElement == null)
            {
                parentElement.Add(new XElement(name));
                childElement = parentElement.XPathSelectElement(xpath);
            }

            return childElement;
        }

        /// <summary>
        /// Gets the or create element.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static XElement GetOrCreateElement(this XElement parentElement, string name)
        {
            return GetOrCreateElement(parentElement, name, name);
        }

        /// <summary>
        /// Removes the elements by xpath.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <param name="xpath">The xpath.</param>
        public static void RemoveElementsByXpath(this XElement parentElement, string xpath)
        {
            // Delete all child nodes using the xpath
            var selectedElements = parentElement.XPathSelectElements(xpath);
            if (selectedElements != null && selectedElements.Count() > 0)
                selectedElements.Remove();
            //parentElement.RemoveAllChildElements(xpath);
        }

        /// <summary>
        /// Removes the type of the elements by node.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <param name="nodeType">Type of the node.</param>
        public static void RemoveElementsByNodeType(this XElement parentElement, System.Xml.XmlNodeType nodeType = System.Xml.XmlNodeType.None)
        {
            var selectedNodes = parentElement.Nodes();
            if (nodeType == System.Xml.XmlNodeType.None)
            {
                selectedNodes.Remove();
                return;
            }
            for (int i = 0; i < selectedNodes.Count(); i++)
            {
                var node = selectedNodes.ElementAt(i);
                if (node.NodeType == nodeType)
                    node.Remove();
            }
        }

        /// <summary>
        /// Inners the XML.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <returns></returns>
        public static string InnerXml(this XElement parentElement)
        {
            var xReader = parentElement.CreateReader();
            xReader.MoveToContent();
            return xReader.ReadInnerXml();
        }

        /// <summary>
        /// Outers the XML.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <returns></returns>
        public static string OuterXml(this XElement parentElement)
        {
            var xReader = parentElement.CreateReader();
            xReader.MoveToContent();
            return xReader.ReadOuterXml();
        }
        #endregion

        #region XPath Extension Methods

        /// <summary>
        /// Xs the path select item.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="node">The node.</param>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        public static T XPathSelectItem<T>(this XNode node, string expression)
        {
            return ((IEnumerable)node.XPathEvaluate(expression)).Cast<T>().First();
        }

        public static T XPathSelectItems<T>(this XNode node, string expression)
        {
            return (T)node.XPathEvaluate(expression);
        }

        /// <summary>
        /// Xs the path get value.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <param name="xpath">The xpath.</param>
        /// <returns></returns>
        public static string XPathGetValue(this XElement parentElement, string xpath)
        {
            try
            {
                var targetElement = parentElement.XPathSelectElement(xpath);
                return targetElement != null ? targetElement.Value : null;
            }
            catch
            {
                var targetAttribute = ((IEnumerable)parentElement.XPathEvaluate(xpath)).Cast<XAttribute>().First();
                return targetAttribute != null ? targetAttribute.Value : null;
            }
        }

        /// <summary>
        /// Xs the path get value.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <param name="xpath">The xpath.</param>
        /// <returns></returns>
        public static string XPathGetValue(this XDocument document, string xpath)
        {
            try
            {
                var targetElement = document.XPathSelectElement(xpath);
                return targetElement != null ? targetElement.Value : null;
            }
            catch
            {
                var targetAttribute = ((IEnumerable)document.XPathEvaluate(xpath)).Cast<XAttribute>().First();
                return targetAttribute != null ? targetAttribute.Value : null;
            }
        }

        #endregion

        #region XDocument Extension Methods

        /// <summary>
        /// Gets the element value as int32.
        /// </summary>
        /// <param name="doc">The doc.</param>
        /// <param name="elementXPath">The element X path.</param>
        /// <returns></returns>
        public static int GetElementValueAsInt32(this XDocument doc, string elementXPath)
        {
            var temp = doc.XPathGetValue(elementXPath);
            return (temp != null ? temp.AsInt32() : default(int));
        }

        /// <summary>
        /// Gets the element value as string.
        /// </summary>
        /// <param name="doc">The doc.</param>
        /// <param name="elementXPath">The element X path.</param>
        /// <returns></returns>
        public static string GetElementValueAsString(this XDocument doc, string elementXPath)
        {
            return (doc.XPathGetValue(elementXPath));
        }

        /// <summary>
        /// Gets the element value as date time.
        /// </summary>
        /// <param name="doc">The doc.</param>
        /// <param name="elementXPath">The element X path.</param>
        /// <returns></returns>
        public static DateTime GetElementValueAsDateTime(this XDocument doc, string elementXPath)
        {
            var temp = doc.XPathGetValue(elementXPath);
            return (temp != null ? temp.AsDateTime() : default(DateTime));
        }

        /// <summary>
        /// Gets the element value as boolean.
        /// </summary>
        /// <param name="doc">The doc.</param>
        /// <param name="elementXPath">The element X path.</param>
        /// <returns></returns>
        public static bool GetElementValueAsBoolean(this XDocument doc, string elementXPath)
        {
            var temp = doc.XPathGetValue(elementXPath);
            return (temp != null ? temp.AsBoolean() : default(bool));
        }

        #endregion

        public static string AddAltTagWithValueForImg(this string htmlContent, string value)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);
            var imgNodes = doc.DocumentNode.SelectNodes("//img[not(@alt)]");
            if (imgNodes.IsNotNull())
                foreach (var node in imgNodes)
                    node.SetAttributeValue("alt", value);
            return doc.DocumentNode.WriteTo().Trim();
        }

        #region Service Locator Extension Methods

        public static AppContextModel ToContextModel(this IApp app)
        {
            return new AppContextModel
            {
                ClientTag = app.ClientTag,
                RequestId = app.RequestId,
                SessionId = app.SessionId,
                User = app.User
            };
        }

        #endregion

    }
}
