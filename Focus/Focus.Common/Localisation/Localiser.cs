﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

using Focus.Common.Helpers;
using Focus.Common.Models;
using Focus.Core;

using Framework.Core;
using Framework.Core.Attributes;

#endregion

namespace Focus.Common.Localisation
{
  public class Localiser
  {
    private readonly LocalisationDictionary _localisationDictionary;
    private const string PlaceHolderRegexPattern = ""
                                                   + "(" + Constants.PlaceHolders.FocusTalent
                                                   + "|" + Constants.PlaceHolders.FocusAssist
                                                   + "|" + Constants.PlaceHolders.FocusCareer
                                                   + "|" + Constants.PlaceHolders.FocusReporting
                                                   + "|" + Constants.PlaceHolders.FocusExplorer
                                                   + "|" + Constants.PlaceHolders.SupportEmail
                                                   + "|" + Constants.PlaceHolders.SupportPhone
                                                   + "|" + Constants.PlaceHolders.SupportHoursOfBusiness
                                                   + "|" + Constants.PlaceHolders.SupportHelpTeam
                                                   + "|" + Constants.PlaceHolders.SchoolName
                                                   + "|" + Constants.PlaceHolders.CandidateType
                                                   + "|" + Constants.PlaceHolders.EmploymentType
                                                   + "|" + Constants.PlaceHolders.PostingType
                                                   + "|" + Constants.PlaceHolders.PostingTypes
                                                   + "|" + Constants.PlaceHolders.CandidateTypes
                                                   + "|" + Constants.PlaceHolders.EmploymentTypes
                                                   + "|" + Constants.PlaceHolders.IrsOnlineApplicationHours
                                                   + "|" + Constants.PlaceHolders.IrsOnlineApplicationUrl
																									 + "|" + Constants.PlaceHolders.Business
																									 + "|" + Constants.PlaceHolders.Businesses
                                                   + @")(\:[A-Za-z]+)?";

    private readonly Regex _placeHolderRegex;

    /// <summary>
    /// The _app
    /// </summary>
    private readonly IApp _app;

    protected static readonly object SyncObject = new object();

    /// <summary>
    /// Initializes a new instance of the <see cref="Localiser"/> class.
    /// </summary>
    private Localiser(IApp app = null)
    {
      _app = app ?? new HttpApp();
      var helper = new LocalisationModeHelper(_app);
      _localisationDictionary = helper.LocalisationDictionary;
      _placeHolderRegex = new Regex(PlaceHolderRegexPattern);
    }

    /// <summary>
    /// Instances this instance.
    /// </summary>
    /// <returns></returns>
    public static Localiser Instance(IApp app = null)
    {
      app = app ?? new HttpApp();

      var localiser = app.GetContextValue<Localiser>("Localiser");

      if (localiser == null)
      {
        localiser = new Localiser(app);
        OldApp_RefactorIfFound.SetContextValue("Localiser", localiser);
      }

      return localiser;
    }

    /// <summary>
    /// Clears the current instance
    /// </summary>
    public void Clear(bool resetDictionary = false)
    {
      var localiser = _app.GetContextValue<Localiser>("Localiser");

      if (localiser != null)
        _app.SetContextValue<Localiser>	("Localiser", null);

			if (resetDictionary)
			{
				var helper = new LocalisationModeHelper(_app);
				helper.ResetDictionary();
			}
    }

    /// <summary>
    /// Localises the specified key and if not found it will return the default value.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <param name="args">The args.</param>
    /// <returns></returns>
    public string Localise(string key, string defaultValue, params object[] args)
    {
      #region new localise

      var localisedValue = String.Empty;
      var localisationModeHelper = new LocalisationModeHelper(_app);
      var dict = localisationModeHelper.LocalisationDictionary;

      var themeKey = string.Concat(key, ":", _app.Settings.Theme);

			SaveDefaultLocalisationItem(themeKey, defaultValue);

      var entry = dict.GetValue(themeKey);

      if (entry.IsNull())
        entry = dict.GetValue(key);

      if (entry.IsNull())
      {
        lock (SyncObject)
        {
          entry = dict.GetValue(key);

          if (entry.IsNull())
          {
            entry = new LocalisationDictionaryEntry(key, defaultValue);
            dict.Add(entry.LocaliseKeyUpper, entry);
          }
        }
      }
      else
      {
        if (String.IsNullOrEmpty(entry.DefaultValue))
          entry.DefaultValue = defaultValue;

        localisedValue = entry.LocalisedValue;
      }

      if (_app.User.IsInRole(Constants.RoleKeys.SystemAdmin, _app))
      {
        var pageLocaliseKeys = LocalisationModeHelper.PageLocaliseKeys;

        if (pageLocaliseKeys.IndexOf(key.ToUpper()) == -1)
        {
          pageLocaliseKeys.Add(key.ToUpper());
          LocalisationModeHelper.PageLocaliseKeys = pageLocaliseKeys;
        }
      }

      if (String.IsNullOrEmpty(localisedValue))
        localisedValue = defaultValue;

      if (_app.User.IsInRole(Constants.RoleKeys.SystemAdmin, _app))
      {
        if (!key.Contains(".NoEdit") && !key.Contains("Title")
              && !key.Contains(".TopDefault") && !key.Contains("Button")
              && !key.Contains(".LinkText") && !key.Contains("ToolTip")
              && !key.EndsWith(".Tab"))
          localisedValue = String.Concat(UrlHelper.EditLocalisationLink(key, _app, HttpContext.Current.Request.ApplicationPath), localisedValue);
      }

      #endregion

      localisedValue = (args == null || args.Length == 0 ? localisedValue : string.Format(localisedValue, args));
			return localisedValue.IsNotNullOrEmpty() ? _placeHolderRegex.Replace(localisedValue, PlaceHolderMatch) : defaultValue;
    }

    /// <summary>
    /// Performs basic localisation (without updating the dictionary objects)
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <param name="args">The args.</param>
    /// <returns></returns>
    public string LocaliseBasic(string key, string defaultValue, params object[] args)
    {
      var localisedValue = String.Empty;

      var localisationModeHelper = new LocalisationModeHelper(_app);
      var dict = localisationModeHelper.LocalisationDictionary;

      var themeKey = string.Concat(key, ":", _app.Settings.Theme);

			SaveDefaultLocalisationItem(themeKey, defaultValue);

      var entry = dict.GetValue(themeKey);

      if (entry.IsNull())
        entry = dict.GetValue(key);

      if (entry.IsNotNull())
        localisedValue = entry.LocalisedValue;

      if (String.IsNullOrEmpty(localisedValue))
        localisedValue = defaultValue;

      localisedValue = (args == null || args.Length == 0 ? localisedValue : string.Format(localisedValue, args));
      return _placeHolderRegex.Replace(localisedValue, PlaceHolderMatch);
    }

    /// <summary>
    /// Replaces a place holder matched in the text
    /// </summary>
    /// <param name="m">Details of the matched place holder</param>
    /// <returns>The replaced text</returns>
    private string PlaceHolderMatch(Match m)
    {
      var returnText = "";
      switch (m.Groups[1].Value)
      {
        case Constants.PlaceHolders.FocusTalent:
          returnText = _app.Settings.FocusTalentApplicationName;
          break;

        case Constants.PlaceHolders.FocusAssist:
          returnText = _app.Settings.FocusAssistApplicationName;
          break;

        case Constants.PlaceHolders.FocusCareer:
          returnText = _app.Settings.FocusCareerApplicationName;
          break;

        case Constants.PlaceHolders.FocusReporting:
          returnText = _app.Settings.FocusReportingApplicationName;
          break;

        case Constants.PlaceHolders.FocusExplorer:
          returnText = _app.Settings.FocusExplorerApplicationName;
          break;

        case Constants.PlaceHolders.SupportEmail:
          returnText = _app.Settings.SupportEmail;
          break;

        case Constants.PlaceHolders.SupportPhone:
          returnText = _app.Settings.SupportPhone;
          break;

        case Constants.PlaceHolders.SupportHoursOfBusiness:
          returnText = _app.Settings.SupportHoursOfBusiness;
          break;

        case Constants.PlaceHolders.SupportHelpTeam:
          returnText = _app.Settings.SupportHelpTeam;
          break;

        case Constants.PlaceHolders.SchoolName:
          returnText = _app.Settings.SchoolName;
          break;

        case Constants.PlaceHolders.CandidateType:
          returnText = GetThemeTypes(Constants.PlaceHolders.CandidateType);
          break;

        case Constants.PlaceHolders.EmploymentType:
          returnText = GetThemeTypes(Constants.PlaceHolders.EmploymentType);
          break;

        case Constants.PlaceHolders.PostingType:
          returnText = GetThemeTypes(Constants.PlaceHolders.PostingType);
          break;

        case Constants.PlaceHolders.PostingTypes:
          returnText = GetThemeTypes(Constants.PlaceHolders.PostingTypes);
          break;

        case Constants.PlaceHolders.CandidateTypes:
          returnText = GetThemeTypes(Constants.PlaceHolders.CandidateTypes);
          break;

        case Constants.PlaceHolders.EmploymentTypes:
          returnText = GetThemeTypes(Constants.PlaceHolders.EmploymentTypes);
          break;

        case Constants.PlaceHolders.IrsOnlineApplicationHours:
          returnText = _app.Settings.IrsOnlineApplicationHours;
          break;

        case Constants.PlaceHolders.IrsOnlineApplicationUrl:
          returnText = _app.Settings.IrsOnlineApplicationUrl;
          break;

				case Constants.PlaceHolders.Business:
					returnText = GetThemeTypes(Constants.PlaceHolders.Business);
					break;

				case Constants.PlaceHolders.Businesses:
					returnText = GetThemeTypes(Constants.PlaceHolders.Businesses);
					break;
      }

      if (m.Groups[2].Value.Length > 0 && returnText.IsNotNull())
      {
        switch (m.Groups[2].Value)
        {
          case ":UPPER":
            returnText = returnText.ToUpper();
            break;
          case ":LOWER":
            returnText = returnText.ToLower();
            break;
          case ":TITLE":
            returnText = returnText.ToTitleCase();
            break;
          default:
            returnText = string.Concat(returnText, m.Groups[2].Value);
            break;
        }
      }

      return returnText;
    }

    /// <summary>
    /// Tries the get localisation item.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <returns></returns>
    private string TryGetLocalisationItem(string key, string defaultValue)
    {
      var keyUpper = key.ToUpper();
			var value = _localisationDictionary.ContainsKey(keyUpper) ? _localisationDictionary[keyUpper].LocalisedValue : defaultValue;

	    SaveDefaultLocalisationItem(key, defaultValue);

	    return value;
    }

		/// <summary>
		/// Saves the default localisation item.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
	  private void SaveDefaultLocalisationItem(string key, string defaultValue)
	  {
			if (_app.Settings.SaveDefaultLocalisationItems)
				lock (SyncObject)
				{
					var items = _app.GetContextValue<List<KeyValuePairSerializable<string, string>>>("DefaultLocalisationItems");

					if (items == null)
					{
						items = new List<KeyValuePairSerializable<string, string>>();
						_app.SetContextValue("DefaultLocalisationItems", items);
					}
					
					if (items.All(x => x.Key != key))
						items.Add(new KeyValuePairSerializable<string, string>(key, defaultValue));
				}
	  }

    /// <summary>
    /// Gets the placeholders according to theme.
    /// </summary>
    /// <param name="placeholderType">Type of the placeholder.</param>
    /// <returns></returns>
    private String GetThemeTypes(string placeholderType)
    {
      var localisedItem = String.Empty;

      switch (_app.Settings.Theme)
      {
        case FocusThemes.Workforce:
          switch (placeholderType)
          {
            case Constants.PlaceHolders.CandidateType:
              localisedItem = TryGetLocalisationItem("Global.CandidateType.Workforce", "Job seeker");
              break;
            case Constants.PlaceHolders.EmploymentType:
              localisedItem = TryGetLocalisationItem("Global.EmploymentType.Workforce", "Job posting");
              break;
            case Constants.PlaceHolders.PostingTypes:
              localisedItem = TryGetLocalisationItem("Global.PostingTypes.Workforce", "Jobs");
              break;
            case Constants.PlaceHolders.CandidateTypes:
              localisedItem = TryGetLocalisationItem("Global.CandidateTypes.Workforce", "Job seekers");
              break;
            case Constants.PlaceHolders.EmploymentTypes:
              localisedItem = TryGetLocalisationItem("Global.EmploymentTypes.Workforce", "Job postings");
              break;
						case Constants.PlaceHolders.Business:
							localisedItem = TryGetLocalisationItem("Global.Business.Workforce", "Business");
							break;
						case Constants.PlaceHolders.Businesses:
							localisedItem = TryGetLocalisationItem("Global.Businesses.Workforce", "Businesses");
							break;
            default:
              localisedItem = TryGetLocalisationItem("Global.PostingType.Workforce", "Job");
              break;
          }
          break;

        case FocusThemes.Education:
          switch (placeholderType)
          {
            case Constants.PlaceHolders.CandidateType:
              localisedItem = TryGetLocalisationItem("Global.CandidateType.Education", "Student/Alumni");
              break;
            case Constants.PlaceHolders.EmploymentType:
              localisedItem = TryGetLocalisationItem("Global.EmploymentType.Education", "Job Order/Internship");
              break;
            case Constants.PlaceHolders.PostingTypes:
              localisedItem = TryGetLocalisationItem("Global.PostingTypes.Education", "Jobs/Internships");
              break;
            case Constants.PlaceHolders.CandidateTypes:
              localisedItem = TryGetLocalisationItem("Global.CandidateTypes.Education", "Students");
              break;
						case Constants.PlaceHolders.Business:
							localisedItem = TryGetLocalisationItem("Global.Business.Education", "Employer");
							break;
						case Constants.PlaceHolders.Businesses:
							localisedItem = TryGetLocalisationItem("Global.Businesses.Education", "Employers");
							break;
            default:
              localisedItem = TryGetLocalisationItem("Global.PostingType.Education", "Job/Internship");
              break;
          }
          break;
      }

      return localisedItem;
    }

    /// <summary>
    /// Localises the specified page.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="key">The key.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <param name="args">The args.</param>
    /// <returns></returns>
    public string Localise(PageBase page, string key, string defaultValue, params object[] args)
    {
      if (!key.StartsWith("Global"))
      {
        var keyUpper = key.ToUpper();
        var themeKeyUpper = string.Concat(key, ":", _app.Settings.Theme).ToUpper();

        if (!_localisationDictionary.ContainsKey(keyUpper) && !_localisationDictionary.ContainsKey(themeKeyUpper))
          key = (page.GetType().BaseType ?? GetType()).FullName + "." + key;
      }

      return Localise(key, defaultValue, args);
    }

    /// <summary>
    /// Localises the specified user control.
    /// </summary>
    /// <param name="userControl">The user control.</param>
    /// <param name="key">The key.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <param name="args">The args.</param>
    /// <returns></returns>
    public string Localise(UserControlBase userControl, string key, string defaultValue, params object[] args)
    {
			if (!key.StartsWith("Global"))
			{
				var keyUpper = key.ToUpper();
				var themeKeyUpper = string.Concat(key, ":", _app.Settings.Theme).ToUpper();

				if (!_localisationDictionary.ContainsKey(keyUpper) && !_localisationDictionary.ContainsKey(themeKeyUpper))
					key = (userControl.GetType().BaseType ?? GetType()).FullName + "." + key;
			}

			return Localise(key, defaultValue, args);
    }

    /// <summary>
    /// Localises the specified enum value.
    /// </summary>
    /// <param name="enumValue">The enum value.</param>
    /// <returns></returns>
    public string Localise(Enum enumValue)
    {
      var defaultValue = enumValue.GetAttributeValue<EnumLocalisationDefaultAttribute>();

      if (defaultValue.IsNullOrEmpty())
        defaultValue = enumValue.ToString();

      return Localise(enumValue, defaultValue);
    }

    /// <summary>
    /// Localises the specified enum value.
    /// </summary>
    /// <param name="enumValue">The enum value.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <returns></returns>
    public string Localise(Enum enumValue, string defaultValue, bool localiseMode = false)
    {
      var key = string.Concat(enumValue.GetType().Name, ".", enumValue, ".NoEdit");
      var value = Localise(key, defaultValue);

      if (value == defaultValue)
      {
        key = string.Concat(enumValue.GetType().Name, ".", enumValue);
        value = LocaliseBasic(key, defaultValue);
      }

      return value;
    }

    /// <param name="userControl">The user control.</param>
    /// <param name="key">The key.</param>
    /// <returns></returns>
    public string GenerateKey(UserControlBase userControl, string key)
    {
      if (!key.StartsWith("Global"))
      {
        var keyUpper = key.ToUpper();
        var themeKeyUpper = string.Concat(key, ":", _app.Settings.Theme).ToUpper();

        if (!_localisationDictionary.ContainsKey(keyUpper) && !_localisationDictionary.ContainsKey(themeKeyUpper))
          key = (userControl.GetType().BaseType ?? GetType()).FullName + "." + key;
      }
      return key;
    }

    /// <param name="page">The page control.</param>
    /// <param name="key">The key.</param>
    /// <returns></returns>
    public string GenerateKey(PageBase page, string key)
    {
      if (!key.StartsWith("Global"))
      {
        var keyUpper = key.ToUpper();
        var themeKeyUpper = string.Concat(key, ":", _app.Settings.Theme).ToUpper();

        if (!_localisationDictionary.ContainsKey(keyUpper) && !_localisationDictionary.ContainsKey(themeKeyUpper))
          key = (page.GetType().BaseType ?? GetType()).FullName + "." + key;
      }
      return key;
    }
  }

}