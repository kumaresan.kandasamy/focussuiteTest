﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Web;
using Focus.Core.DataTransferObjects.FocusCore;
using Framework.Caching;
using Framework.Core;

using Focus.Common.Models;
using Focus.Core;

#endregion

namespace Focus.Common.Helpers
{
  public class LocalisationModeHelper
  {
    /// <summary>
    /// The _app
    /// </summary>
    private static IApp _app;

    /// <summary>
    /// Static localisation dictionary variable
    /// </summary>
    private static Dictionary<string, LocalisationDictionary> _dictionaries;

    /// <summary>
    /// Initializes a new instance of the <see cref="LocalisationModeHelper"/> class.
    /// </summary>
    /// <param name="app">The application.</param>
    public LocalisationModeHelper(IApp app)
    {
      _app = app ?? new HttpApp();
    }

    public LocalisationDictionary LocalisationDictionary
    {
      get
      {
        var culture = GetCulture();
        return GetDictionary(culture);
      }

      set
      {
        if (_app == null) 
          return;

        var culture = GetCulture();

        Cacher.Set(string.Format(Constants.CacheKeys.LocalisationDictionary, culture), value);
        SetDictionary(culture, value);
      }
    }

    /// <summary>
    /// Gets the current culture
    /// </summary>
    /// <returns>the current culture</returns>
    private static string GetCulture()
    {
      return _app.IsNotNull() && _app.User.IsNotNull() ? _app.User.Culture : "**-**";
    }

    /// <summary>
    /// Gets the dictionary from either the static variable, or the cache (setting the static variable in this instance)
    /// </summary>
    /// <param name="culture">The current culture</param>
    /// <returns>The localisation dictionary</returns>
    private LocalisationDictionary GetDictionary(string culture)
    {
      var dictionary = _dictionaries.IsNotNull() && _dictionaries.ContainsKey(culture)
       ? _dictionaries[culture]
       : null;

      if (dictionary.IsNull() || dictionary.Count == 0)
      {
        dictionary = Cacher.Get<LocalisationDictionary>(string.Format(Constants.CacheKeys.LocalisationDictionary, culture));
        if (dictionary.IsNotNull() && dictionary.Count > 0)
          SetDictionary(culture, dictionary);
      }

      if ((dictionary.IsNull() || dictionary.Count == 0) && _app.IsNotNull())
        dictionary = ResetDictionary();

      return dictionary;
    }

    /// <summary>
    /// Sets the dictionary in the static variable, and (optionally) the cache
    /// </summary>
    /// <param name="culture">The current culture</param>
    /// <param name="dictionary">The localisation dictionary</param>
    /// <param name="updateCache">Whether to update the cache</param>
    private void SetDictionary(string culture, LocalisationDictionary dictionary, bool updateCache = false)
    {
      if (_dictionaries == null)
        _dictionaries = new Dictionary<string, LocalisationDictionary>();

      if (_dictionaries.ContainsKey(culture))
        _dictionaries[culture] = dictionary;
      else
        _dictionaries.Add(culture, dictionary);

      if (updateCache)
        Cacher.Set(string.Format(Constants.CacheKeys.LocalisationDictionary, _app.User.Culture), dictionary);
    }

    public static List<string> PageLocaliseKeys
    {
      get
      {

        var o = Cacher.Get(String.Concat("PageLocaliseKeys", HttpContext.Current.Request.FilePath.Replace("/", "_")));
        return (o != null) ? (List<string>) o : new List<string>();
      }
      set { Cacher.Set(String.Concat("PageLocaliseKeys", HttpContext.Current.Request.FilePath.Replace("/", "_")), value); }
    }

    public void SavePortalLocalisationItem(string key, string localisedValue, string defaultValue)
    {
      localisedValue = Uri.UnescapeDataString(localisedValue);
      defaultValue = Uri.UnescapeDataString(defaultValue);

			ServiceClientLocator.CoreClient(_app).SaveLocalisationItems(new List<LocalisationItemDto>
                                 {
                                   new LocalisationItemDto
                                     {
                                       Key = key,
                                       Value = localisedValue
                                     }
                                 }, _app.User.Culture);

      var dictionary = ResetDictionary();

      var keyUpper = key.ToUpper();
      var entry = dictionary.GetValue(keyUpper);

      if (entry == null) return;

      entry.LocalisedValue = localisedValue;
      entry.DefaultValue = defaultValue;
    }

    /// <summary>
    /// Resets the dictionary
    /// </summary>
    /// <returns></returns>
    public LocalisationDictionary ResetDictionary()
    {
      var culture = GetCulture();

			var dictionary = ServiceClientLocator.CoreClient(_app).GetLocalisationDictionary();
      SetDictionary(culture, dictionary, true);

      return dictionary;
    }
  }
}
