﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;
using Focus.Core.Messages;
using Focus.Core.Messages.ExplorerService;
using Focus.Core.Models.Career;
using Focus.Services.ServiceImplementations;

#endregion

namespace Focus.Common.Helpers
{
  public static class PingHelper
  {
    public struct PingResults
    {
      public string StatusMessage;
      public double SecondsTaken;
    }

    /// <summary>
    /// Runs various checks on the database (Connect to lens, log-in, search, log-out) to check everything is running as expected)
    /// </summary>
    /// <returns>
    /// Details from the results of the ping
    /// </returns>
    public static PingResults DoPing(IApp app = null)
    {
	    app = app ?? new HttpApp();

			var status = new StringBuilder();
			var pingSuccess = true;

			var startTime = DateTime.UtcNow;
			var explorerService = new ExplorerService();

			var pingRequest = new PingRequest { Name = "Lens/DB check" };
			var pingResponse = explorerService.Ping(pingRequest);

			if (pingResponse.Acknowledgement == AcknowledgementType.Success)
				status.Append("Lens/DB check: OK");
			else
			{
				pingSuccess = false;
				status.Append("Lens/DB check: Down ").Append(pingResponse.Message);
			}

			// Search 
			try
			{
				var seacrhCriteria = new SearchCriteria { RequiredResultCriteria = new RequiredResultCriteria { MaximumDocumentCount = 5 } };
				var seacrhResult = ServiceClientLocator.SearchClient(app).GetSearchResults(seacrhCriteria, 5, false);

				// Check for posting nodes in the search response
				status.Append(Environment.NewLine);

				if (seacrhResult.Count == 0)
				{
					pingSuccess = false;
					status.Append("Get job search results check: Down - No search results");
				}
				else
					status.Append("Get job search results check: OK");
			}
			catch (Exception ex)
			{
				pingSuccess = false;
				status.Append(Environment.NewLine).Append("Get job search results check: Down - ").Append(ex.Message);
			}

			// How long did it take
			var endTime = DateTime.UtcNow;
			var seconds = endTime.Subtract(startTime).TotalSeconds;

			return new PingResults
			{
				StatusMessage = (pingSuccess) ? "OK" : status.ToString(),
				SecondsTaken = seconds
			};
    }

    /// <summary>
    /// Runs various checks on the database (Connect to lens, log-in, search, log-out) to check everything is running as expected
    /// Results are written to a stream
    /// </summary>
    /// <param name="stream">A stream to which the output results should be written</param>
    public static void DoPing(Stream stream)
    {
      var results = DoPing();

      using (var writer = new XmlTextWriter(stream, Encoding.UTF8))
      {
        writer.WriteStartDocument();
        writer.WriteStartElement("pingdom_http_custom_check");
        writer.WriteElementString("status", results.StatusMessage);
        writer.WriteElementString("response_time", results.SecondsTaken.ToString(CultureInfo.InvariantCulture));
        writer.WriteEndElement();
        writer.WriteEndDocument();
        writer.Close();
      }
    }
  }
}
