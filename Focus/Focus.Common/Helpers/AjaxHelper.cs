﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using AjaxControlToolkit;
using Focus.Core.Criteria.Employer;
using Framework.Core;

using Focus.Common.Models;
using Focus.Core;
using Focus.Common.Localisation;

#endregion

namespace Focus.Common.Helpers
{
	public class AjaxHelper
	{
		/// <summary>
		/// The _app
		/// </summary>
		private static IApp _app;

    static AjaxHelper()
    {
      _app = new HttpApp();
    }

		/// <summary>
		/// Initializes a new instance of the <see cref="AjaxHelper"/> class.
		/// </summary>
		/// <param name="app">The application.</param>
		public AjaxHelper(IApp app)
		{
			_app = app ?? new HttpApp();
		}

		/// <summary>
		/// Localises from code using the specified key and if not found it will return the default value.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <param name="args">The args.</param>
		/// <returns></returns>
		public static string CodeLocalise(string key, string defaultValue, params object[] args)
		{
			return Localiser.Instance().Localise(key, defaultValue, args);
		}

    /// <summary>
    /// Localises from code using the specified key and if not found it will return the default value.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <param name="localiseKey"></param>
    /// <param name="args">The args.</param>
    /// <returns></returns>
    public static string CodeLocalise(string key, string defaultValue, string localiseKey, params object[] args)
    {
      return Localiser.Instance().Localise(key, defaultValue, args);
    }

		/// <summary>
		/// Gets the licences and certifications.
		/// </summary>
		/// <param name="prefixText">The prefix text.</param>
		/// <param name="count">The count.</param>
		/// <returns></returns>
		public static string[] GetLicencesAndCertifications(string prefixText, int count)
		{
			return ServiceClientLocator.CoreClient(_app).GetLicencesAndCertifications(prefixText, count).Select(x => x.Name).ToArray();
		}

		/// <summary>
		/// Gets the certifications.
		/// </summary>
		/// <param name="prefixText">The prefix text.</param>
		/// <param name="count">The count.</param>
		/// <returns></returns>
		public static string[] GetLicences(string prefixText, int count)
		{
      return ServiceClientLocator.CoreClient(_app).GetLicenses(prefixText, count).Select(x => x.Name).Distinct().ToArray();
		}

		/// <summary>
		/// Gets the licences.
		/// </summary>
		/// <param name="prefixText">The prefix text.</param>
		/// <param name="count">The count.</param>
		/// <returns></returns>
		public static string[] GetCertifications(string prefixText, int count)
		{
			return ServiceClientLocator.CoreClient(_app).GetCertificates(prefixText, count).Select(x => x.Name).Distinct().ToArray();
		}

		/// <summary>
		/// Gets the languages.
		/// </summary>
		/// <param name="prefixText">The prefix text.</param>
		/// <param name="count">The count.</param>
		/// <returns></returns>
		public static string[] GetLanguages(string prefixText, int count)
		{
			return ServiceClientLocator.CoreClient(_app).GetLanguages(prefixText, count).Select(x => x.Text).ToArray();
		}

		/// <summary>
		/// Gets the job titles.
		/// 
		/// This will be filtered based on the employer id passed in the contextKey
		/// 
		/// </summary>
		/// <param name="prefixText">The prefix text.</param>
		/// <param name="count">The count.</param>
		/// <param name="contextKey">The context key.</param>
		/// <returns></returns>
		public static string[] GetJobTitles(string prefixText, int count, string contextKey)
		{
			return ServiceClientLocator.CoreClient(_app).GetGenericJobTitles(prefixText, count).ToArray();
		}

		/// <summary>
		/// Gets the job titles with MO cs.
		/// </summary>
		/// <param name="prefixText">The prefix text.</param>
		/// <param name="count">The count.</param>
		/// <param name="contextKey">The context key.</param>
		/// <returns></returns>
		public static string[] GetJobTitlesWithMOCs(string prefixText, int count, string contextKey)
		{
			var militaryOccupationJobTitles = ServiceClientLocator.OccupationClient(_app).GetMilitaryOccupationJobTitles(prefixText, count);
			return militaryOccupationJobTitles.IsNotNullOrEmpty() ? militaryOccupationJobTitles.Select(x => x.JobTitle).Distinct().ToArray() : ServiceClientLocator.CoreClient(_app).GetGenericJobTitles(prefixText, count).ToArray();
		}

		/// <summary>
		/// Gets the military job titles.
		/// </summary>
		/// <param name="prefixText">The prefix text.</param>
		/// <param name="count">The count.</param>
		/// <param name="contextKey">The context key.</param>
		/// <returns></returns>
		public static string[] GetMilitaryJobTitles(string prefixText, int count, string contextKey)
		{
			return ServiceClientLocator.OccupationClient(_app).GetMilitaryOccupationJobTitles(prefixText, count).Select(x => x.JobTitle).Distinct().ToArray();
		}

		/// <summary>
		/// Gets the naics.
		/// </summary>
		/// <param name="prefixText">The prefix text.</param>
		/// <param name="count">The count.</param>
    /// <param name="childrenOnly">Whether to get only the child NAICS</param>
    /// <returns></returns>
		public static string[] GetNaics(string prefixText, int count, bool childrenOnly = false)
		{
			var naics = ServiceClientLocator.CoreClient(_app).GetNaics(prefixText, count, childrenOnly).ToArray();
			return naics.Select(naicsItem => string.Concat(naicsItem.Code, " - ", naicsItem.Name)).ToArray();
		}

    /// <summary>
    /// Gets all counties for a typeahead
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    public static string[] GetAllCounties(string prefixText, int count)
    {
			return ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.Counties, prefix: prefixText).Select(county => county.Text).Distinct().ToArray();
    }

		/// <summary>
		/// Gets the drop down occupations.
		/// </summary>
		/// <param name="knownCategoryValues">The known category values.</param>
		/// <param name="category">The category.</param>
		/// <returns></returns>
		public static CascadingDropDownNameValue[] GetOccupations(string knownCategoryValues, string category)
		{
			var nameValues = new List<CascadingDropDownNameValue>();

			long jobFamilyId;
			long.TryParse(GetKnownCategoryValue(knownCategoryValues), out jobFamilyId);

			if (jobFamilyId > 0)
			{
				var onets = ServiceClientLocator.SearchClient(_app).GetOnets(jobFamilyId);

				if (onets != null && onets.Count > 0)
					nameValues.AddRange(onets.Select(t => new CascadingDropDownNameValue(t.Occupation, t.Id.ToString())));
			}
			
			return nameValues.OrderBy(x => x.name).ToArray();
		}

		/// <summary>
		/// Gets the search occupations.
		/// </summary>
		/// <param name="knownCategoryValues">The known category values.</param>
		/// <param name="category">The category.</param>
		/// <returns></returns>
		public static CascadingDropDownNameValue[] GetSearchOccupations(string knownCategoryValues, string category)
		{
			var nameValues = new List<CascadingDropDownNameValue>();

			long jobFamilyId;
			long.TryParse(GetKnownCategoryValue(knownCategoryValues), out jobFamilyId);

			if (jobFamilyId > 0)
			{
				nameValues.Add(new CascadingDropDownNameValue(CodeLocalise("Occupations.AllOccupations.NoEdit", "- all occupations -"), "", true));

				var onets = ServiceClientLocator.SearchClient(_app).GetOnets(jobFamilyId);

				if (onets != null && onets.Count > 0)
					nameValues.AddRange(onets.Select(t => new CascadingDropDownNameValue(t.Occupation, t.Id.ToString())));
			}
			else
				nameValues.Add(new CascadingDropDownNameValue(CodeLocalise("Occupations.SelectOccupation", "- select occupation -"), "", true));

			return nameValues.OrderBy(x => x.name).ToArray();
		}

		/// <summary>
		/// Gets the industry details.
		/// </summary>
		/// <param name="knownCategoryValues">The known category values.</param>
		/// <param name="category">The category.</param>
		/// <returns></returns>
		public static CascadingDropDownNameValue[] GetIndustryDetails(string knownCategoryValues, string category)
		{
			var industryDetailValues = new List<CascadingDropDownNameValue>();

			long parentId;
			long.TryParse(GetKnownCategoryValue(knownCategoryValues), out parentId);

			if (parentId > 0)
			{
				industryDetailValues.Add(new CascadingDropDownNameValue(CodeLocalise("IndustryDetails.AllIndustryDetails.TopDefault", "- all industry details -"), "", true));
				industryDetailValues.AddRange(ServiceClientLocator.CoreClient(_app).GetIndustryClassificationLookup(parentId, 4).Select(industryDetail => new CascadingDropDownNameValue(industryDetail.Value, industryDetail.Key)));
			}
			else
				industryDetailValues.Add(new CascadingDropDownNameValue(CodeLocalise("IndustryDetails.SelectIndustryDetail.TopDefault", "- select industry details -"), "", true));

			return industryDetailValues.ToArray();
		}

		/// <summary>
		/// Gets the counties for a given state
		/// </summary>
		/// <param name="knownCategoryValues">The known category values.</param>
		/// <param name="category">The category.</param>
		/// <returns></returns>
		public static CascadingDropDownNameValue[] GetCounties(string knownCategoryValues, string category)
		{
			var stateValues = new List<CascadingDropDownNameValue>();

			long stateId;
			long.TryParse(GetKnownCategoryValue(knownCategoryValues), out stateId);

			if (stateId > 0)
				stateValues.AddRange(ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.Counties, stateId).Select(county => new CascadingDropDownNameValue(county.Text, county.Id.ToString())));

			// As this is a required field add something for the user to choose if no counties are available
			if (stateValues.Count == 0)
				stateValues.Add(new CascadingDropDownNameValue(CodeLocalise("Not.Applicable", "Not Applicable"), "0", true));

			return stateValues.ToArray();
		}

		/// <summary>
		/// Gets the metropolitan statistical areas.
		/// </summary>
		/// <param name="knownCategoryValues">The known category values.</param>
		/// <param name="category">The category.</param>
		/// <returns></returns>
		public static CascadingDropDownNameValue[] GetMetropolitanStatisticalAreas(string knownCategoryValues, string category)
		{
			var areaValues = new List<CascadingDropDownNameValue>();

			long stateId;
			long.TryParse(GetKnownCategoryValue(knownCategoryValues), out stateId);

			if (stateId > 0)
			{
				areaValues.Add(new CascadingDropDownNameValue(CodeLocalise("MetropolitanStatisticalAreas.AllCities.NoEdit", "- all cities -"), "", true));
				areaValues.AddRange(ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.StateMetropolitanStatisticalAreas, stateId).Select(area => new CascadingDropDownNameValue(area.Text, area.Id.ToString())));
			}
			else
				areaValues.Add(new CascadingDropDownNameValue(CodeLocalise("MetropolitanStatisticalAreas.SelectCity.NoEdit", "- select city -"), "", true));
			
			return areaValues.ToArray();
		}

		/// <summary>
		/// Gets the rank.
		/// </summary>
		/// <param name="knownCategoryValues">The known category values.</param>
		/// <param name="category">The category.</param>
		/// <returns></returns>
		public static CascadingDropDownNameValue[] GetRank(string knownCategoryValues, string category)
		{
			var rankValues = new List<CascadingDropDownNameValue>();

			long branchOfServiceId;
			long.TryParse(GetKnownCategoryValue(knownCategoryValues), out branchOfServiceId);

			rankValues.Add(new CascadingDropDownNameValue(CodeLocalise("Rank.SelectRank", "- select rank -"), "", true));

			if (branchOfServiceId > 0)
			{
				rankValues.AddRange(ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.MilitaryRanks, branchOfServiceId).Select(rank => new CascadingDropDownNameValue(rank.Text, rank.Id.ToString())));
			}
			
			return rankValues.ToArray();
		}

    /// <summary>
    /// Gets the degree education levels by program area.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <param name="includeAllDegrees">if set to <c>true</c> [include all degrees].</param>
    /// <returns>
    /// An array of cascading drop-down items
    /// </returns>
    public static CascadingDropDownNameValue[] GetDegreeEducationLevelsByProgramArea(string knownCategoryValues, string category, bool includeAllDegrees = true)
    {
      long departmentId;
      long.TryParse(GetKnownCategoryValue(knownCategoryValues), out departmentId);

      var degreeValues = new List<CascadingDropDownNameValue>();

      if (departmentId > 0 && includeAllDegrees)
        degreeValues = new List<CascadingDropDownNameValue> { new CascadingDropDownNameValue(CodeLocalise("Global.Degree.AllDegrees", "All Degrees"), string.Concat("-", departmentId)) };

      if (string.Compare(category, "degreeswithundeclared", true, CultureInfo.InvariantCulture) == 0)
      {
        var undeclaredItem = new CascadingDropDownNameValue(CodeLocalise("Global.Degree.Undeclared", "Undeclared"), "0");
        degreeValues.Add(undeclaredItem);
      }

      if (departmentId > 0)
      {
				var degrees = ServiceClientLocator.ExplorerClient(_app).GetProgramAreaDegreeEducationLevels(departmentId);

        if (degrees.IsNotNullOrEmpty())
          degreeValues.AddRange(degrees.Select(d => new CascadingDropDownNameValue(d.DegreeEducationLevelName, d.DegreeEducationLevelId.ToString(CultureInfo.InvariantCulture))));
      }

      return degreeValues.ToArray();
    }

    /// <summary>
    /// Gets the degree education levels by program area.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns>
    /// An array of cascading drop-down items
    /// </returns>
    public static CascadingDropDownNameValue[] GetDegreeEducationLevelsByDegreeLevel(string knownCategoryValues, string category)
    {
      DetailedDegreeLevels detailedDegreeLevel;
      Enum.TryParse(GetKnownCategoryValue(knownCategoryValues), true, out detailedDegreeLevel);

			var degreeEducationLevels = ServiceClientLocator.ExplorerClient(_app).GetProgramAreaDegreeEducationLevels(null, null, detailedDegreeLevel);

      var degreeValues = new List<CascadingDropDownNameValue> { new CascadingDropDownNameValue(CodeLocalise("DetailsDegrees.SelectDegree", "- select degree -"), "") };

      if (degreeEducationLevels.IsNotNullOrEmpty())
        degreeValues.AddRange(degreeEducationLevels.OrderBy(d => d.DegreeName)
                                                   .Select(d => new CascadingDropDownNameValue(d.DegreeName, d.DegreeEducationLevelId.ToString(CultureInfo.InvariantCulture))));

      return degreeValues.ToArray();
    }

		/// <summary>
		/// Gets the jobs by career area id.
		/// </summary>
		/// <param name="knownCategoryValues">The known category values.</param>
		/// <param name="category">The category.</param>
		/// <returns></returns>
		public static CascadingDropDownNameValue[] GetJobsByCareerAreaId(string knownCategoryValues, string category, bool filterJobs = false)
		{
			long careerAreaId;
			long.TryParse(GetKnownCategoryValue(knownCategoryValues), out careerAreaId);

			var jobValues = careerAreaId > 0 
				? new List<CascadingDropDownNameValue> { new CascadingDropDownNameValue(CodeLocalise("Global.Occupations.AllOccupations.NoEdit", "- all occupations -"), string.Concat("-", careerAreaId)) } 
				: new List<CascadingDropDownNameValue> { new CascadingDropDownNameValue(CodeLocalise("Occupations.SelectOccupation", "- select occupation -"), "", true) };

			if (careerAreaId > 0)
			{
				var jobs = ServiceClientLocator.ExplorerClient(_app).GetCareerAreaJobs(careerAreaId, filterJobs);

				if (jobs.IsNotNullOrEmpty())
					jobValues.AddRange(jobs.Select(j => new CascadingDropDownNameValue(j.JobName, j.ROnet.ToString(CultureInfo.InvariantCulture))));
			}

			return jobValues.ToArray();
		}

		/// <summary>
		/// Gets the state county and counties for postal code.
		/// </summary>
		/// <param name="postalCode">The postal code.</param>
		/// <returns></returns>
		public static PostalCodeStateCityCounty GetStateCityCountyAndCountiesForPostalCode(string postalCode)
		{
			long stateId, countyId;
		  string cityName;
			var counties = new CascadingDropDownNameValue[0];

      ServiceClientLocator.CoreClient(_app).GetStateCityAndCountyForPostalCode(postalCode, out stateId, out countyId, out cityName);
			if (stateId > 0)
				counties = GetCounties(stateId.ToString(), "");

			return new PostalCodeStateCityCounty { StateId = stateId, CountyId = countyId, CityName = cityName, Counties = counties };
		}

		/// <summary>
		/// Gets the business unit description.
		/// </summary>
		/// <param name="businessUnitDescriptionId">The business unit description id.</param>
		/// <returns></returns>
		public static string GetBusinessUnitDescription(string businessUnitDescriptionId)
		{
			if (businessUnitDescriptionId.IsNotNullOrEmpty())
			{
				var businessUnitDescription = ServiceClientLocator.EmployerClient(_app).GetBusinessUnitDescription(businessUnitDescriptionId.ToLong().Value);

				if (businessUnitDescription != null)
					return businessUnitDescription.Description;
			}

			return "";
		}

		public static string GetResumeText(long personId)
		{
			if(personId.IsNotNull() && personId > 0)
			{
				return ServiceClientLocator.CandidateClient(_app).GetResumeAsHtml(personId);
			}

			return CodeLocalise("NoResumeAvailable", "No resume available for this candidate");
		}

		/// <summary>
		/// Gets the localisation items.
		/// </summary>
		/// <param name="contextKey">The context key.</param>
		/// <returns></returns>
		public static JsonResponse<LookupItem[]> GetLocalisationItems(string contextKey)
		{
			var lookupItems = new List<LookupItem>();

			foreach (var localisationKey in LocalisationKeys.Keys.Where(x => x.ContextKey == contextKey))
				lookupItems.Add(new LookupItem(localisationKey.Key, localisationKey.Key));

			return new JsonResponse<LookupItem[]> { Success = true, Message = "Loaded items for " + contextKey, Data = lookupItems.ToArray() };
		}

		/// <summary>
		/// Gets the localisation item.
		/// </summary>
		/// <param name="culture">The culture.</param>
		/// <param name="contextKey">The context key.</param>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public static JsonResponse<LocalisationItem> GetLocalisationItem(string culture, string contextKey, string key)
		{
			LocalisationItem localisationItem = null;

			switch (contextKey)
			{
				case "Global.Buttons":
					switch (key)
					{
						case "OkButton.Text":
							localisationItem = new LocalisationItem(2, culture, contextKey, key, "Ok");
							break;

						case "OkButton.ToolTip":
							localisationItem = new LocalisationItem(2, culture, contextKey, key, "Ok");
							break;
					}
					break;

				case "Global.Labels":
					switch (key)
					{
						case "Unknown.Label":
							localisationItem = new LocalisationItem(1, culture, contextKey, key, "Unknown");
							break;
					}
					break;

				case "System.Email":
					switch (key)
					{
						case "ResetPassword.Body":
							localisationItem = new LocalisationItem(3, culture, contextKey, key, "This is the reset password body.");
							break;

						case "ResetPassword.Subject":
							localisationItem = new LocalisationItem(4, culture, contextKey, key, "Password subject.");
							break;
					}
					break;
			}

			return (localisationItem == null ? new JsonResponse<LocalisationItem> { Success = false, Message = "Unable to load item: " + key, Data = localisationItem }
																			 : new JsonResponse<LocalisationItem> { Success = true, Message = "Loaded item for " + key, Data = localisationItem });
		}

		/// <summary>
		/// Gets the staff members.
		/// </summary>
		/// <param name="knownCategoryValues">The known category values.</param>
		/// <param name="category">The category.</param>
		/// <returns></returns>
		public static CascadingDropDownNameValue[] GetStaffMembers(string knownCategoryValues, string category)
		{
			var staffMemberValues = new List<CascadingDropDownNameValue>();

			long officeId;
			long.TryParse(GetKnownCategoryValue(knownCategoryValues), out officeId);

			if (officeId > 0)
				staffMemberValues.AddRange(ServiceClientLocator.EmployerClient(_app).GetOfficeStaffMembersPagedList(new StaffMemberCriteria { OfficeIds = new List<long?> { officeId } }).Select(staff => new CascadingDropDownNameValue(staff.FirstName + " " + staff.LastName, staff.Id.ToString())));

			return staffMemberValues.ToArray();
		}

		/// <summary>
		/// Gets the staff members.
		/// </summary>
		/// <param name="prefixText">The prefix text.</param>
		/// <param name="count">The count.</param>
		/// <returns></returns>
		public static AutoCompleteItem[] GetNonStatewideStaffMemberNames(string prefixText, int count)
		{
		  var staffMembers = ServiceClientLocator.EmployerClient(_app).GetStaffMembers(prefixText, count, false);

		  return staffMembers.Select(staff => new AutoCompleteItem
		  {
        Id = staff.Id.ToString(),
		    Value = string.Concat(staff.FirstName, " ", staff.LastName), 
        Label = string.Concat(staff.FirstName, " ", staff.LastName)
		  }).ToArray();
		}

    /// <summary>
    /// Checks if a NAICS exists
    /// </summary>
    /// <param name="naicsText">The NAICS.</param>
    /// <param name="childrenOnly">Whether to check "child" NAICS only (i.e. exluce NAICS where ParentId = 0)</param>
    /// <returns>
    /// A boolean indicating existence
    /// </returns>
    public static bool CheckNaicsExists(string naicsText, bool childrenOnly = false)
    {
      return ServiceClientLocator.EmployerClient(_app).CheckNAICSExists(naicsText, childrenOnly);
    }

		/// <summary>
		/// Gets the known category value from the known category values.
		/// </summary>
		/// <param name="knownCategoryValues">The known category values.</param>
		/// <returns></returns>
		private static string GetKnownCategoryValue(string knownCategoryValues)
		{
			if (string.IsNullOrEmpty(knownCategoryValues)) return knownCategoryValues;
			if (knownCategoryValues.LastIndexOf(":") > 0) knownCategoryValues = knownCategoryValues.Substring(knownCategoryValues.LastIndexOf(":") + 1);
			if (knownCategoryValues.LastIndexOf(";") > 0) knownCategoryValues = knownCategoryValues.Substring(0, knownCategoryValues.LastIndexOf(";"));

			return knownCategoryValues;
		}

    #region Upload File

    /// <summary>
    /// Gets the processing state of an upload file.
    /// </summary>
    /// <param name="uploadFileId">The id of the upload file</param>
    /// <returns>The processing state</returns>
    public static int GetUploadFileProcessingState(long uploadFileId)
    {
      return (int) ServiceClientLocator.CoreClient(_app).GetUploadFileProcessingState(uploadFileId);
    }

    #endregion
  }
}
