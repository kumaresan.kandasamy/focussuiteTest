﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.IO;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;

using Framework.Caching;

#endregion

namespace Focus.Common.Helpers
{
	public static class UrlHelper
	{
		/// <summary>
		/// Converts a given URL into one containing a dummy folder based on the last write time of the requested file, 
		/// in order to force the browser to load the latest version of the file.
		/// </summary>
		/// <param name="actualUrl">The actual URL of the file.</param>
		/// <returns></returns>
		public static string GetCacheBusterUrl(string actualUrl)
		{
			var qualifiedUrl = VirtualPathUtility.ToAbsolute(actualUrl);
			if (!Cacher.Contains(qualifiedUrl))
			{
				var physicalPath = HostingEnvironment.MapPath(qualifiedUrl);
				var date = File.GetLastWriteTime(physicalPath);
				var index = qualifiedUrl.LastIndexOf(".");
				var result = qualifiedUrl.Insert(index, ".v" + date.Ticks);
				Cacher.Set(qualifiedUrl, result, new CacheDependency(physicalPath));
			}

			return Cacher.Get(qualifiedUrl, true) as string;
		}

	  /// <summary>
	  /// </summary>
	  /// <param name="key">The key.</param>
	  /// <param name="app"></param>
	  /// <param name="applicationPath"></param>
	  /// <returns></returns>
	  public static string EditLocalisationLink(string key, IApp app, string applicationPath)
    {
			return string.Format(@"<a href=""#"" onkeypress=""javascript:OpenLocalisePopup('{0}','{1}'" + @");"" onclick=""javascript:OpenLocalisePopup('{0}','{1}'" + @");""><img src=""{2}"" alt=""Edit Image"" border=""0"" height=""10px"" width=""10px""></a>", key, applicationPath, BrandAssetPath("Images/Edit.gif", app));
    }

		/// <summary>
		/// Builds an absolute path to a brand asset.
		/// </summary>
		/// <param name="assetPathStub">The virtual path stub for the brand asset.</param>
		/// <param name="app">The application.</param>
		/// <returns></returns>
		internal static string BrandAssetPath(string assetPathStub, IApp app)
		{
			return VirtualPathUtility.ToAbsolute(string.Format("~/Branding/{0}/{1}", app.Settings.BrandIdentifier, assetPathStub));
		}

	}
}
