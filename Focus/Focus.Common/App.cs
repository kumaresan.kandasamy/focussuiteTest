﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;
using Focus.Common;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models;
using Focus.Core.Models.Career;
using Focus.Common.Authentication;
using Focus.Common.HttpHandlers;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.Settings.Interfaces;
using Focus.Web.Core.Models;
using Framework.Core;
using Framework.PageState;

#endregion

namespace Focus
{
	public static class OldApp_RefactorIfFound
	{
		public static readonly IApp App = new HttpApp();
		
		/// <summary>
		/// Starts the App.
		/// </summary>
		public static void Start()
		{
			// Initialize the service clients
			ServiceClientLocator.Runtime.Start();

			// Get the Client Tag
			ClientTag = Settings.ClientTag;

			// Session Cookie Names
			VersionedCookiePrefix = string.Format("{0}-{1}-{2}-", Settings.Module, Settings.Theme, Constants.SystemVersion);
			SessionCookieName = string.Concat(VersionedCookiePrefix, "SessionId");

			// Register the routes
			RegisterRoutes(RouteTable.Routes, Settings.Module);

			// Initialise the PageStatePersitor
			PageStateManager.Initialize(Settings.PageStatePersister, Settings.PageStateQueueMaxLength, Settings.PageStateTimeOut, Settings.PageStateConnectionString);
		}

		/// <summary>
		/// Shutdowns the App.
		/// </summary>
		public static void Shutdown()
		{
			ServiceClientLocator.Runtime.Shutdown();
		}

		/// <summary>
		/// Begins the request.
		/// </summary>
		public static void BeginRequest()
		{
			ServiceClientLocator.Runtime.BeginRequest();
		}

		/// <summary>
		/// Performs actions after the user is authenticated
		/// </summary>
		public static void PostAuthenticateRequest()
		{
			if (App.Settings.DiacriticalMarkCheck.IsNullOrEmpty())
				return;

			var context = HttpContext.Current;
			if (context != null)
			{
				if (context.Request.Form.AllKeys.Any(requestKey => Regex.IsMatch(context.Request.Form[requestKey], App.Settings.DiacriticalMarkCheck)))
					throw new HttpRequestValidationException("Invalid request detected");
			}
		}

		/// <summary>
		/// Ends the request.
		/// </summary>
		public static void EndRequest()
		{
			if (HttpContext.Current != null)
			{
				var prefix = BuildKey(HttpContextSessionKeyPrefix);
				var prefixLength = prefix.Length;

				var keysToSave = HttpContext.Current.Items.Keys.Cast<object>().OfType<string>().Where(stringKey => stringKey.StartsWith(prefix)).ToList();
				if (keysToSave.Any())
					keysToSave.ForEach(key => ServiceClientLocator.CoreClient(App).SetSessionValue(key.Substring(prefixLength), HttpContext.Current.Items[key]));

				if (Settings.SaveDefaultLocalisationItems)
				{
					var items = GetContextValue<List<KeyValuePairSerializable<string, string>>>("DefaultLocalisationItems");
					if (items.IsNotNullOrEmpty())
						ServiceClientLocator.CoreClient(App).SaveDefaultLocalisationItems(items);
				}
			}

			ServiceClientLocator.Runtime.EndRequest();
		}

		#region Routes Registry

		/// <summary>
		/// Register routes with routing system.
		/// </summary>
		/// <param name="routes">The routes.</param>
		/// <param name="module">The module.</param>
		private static void RegisterRoutes(RouteCollection routes, FocusModules module)
		{
			// Note: the order in which routes are registered is important.
			routes.Ignore("{resource}.axd/{*pathInfo}");
			routes.Ignore("{*alljs}", new
			{
				alljs = @".*\.js(/.*)?"
			});

			#region Default route

			routes.MapPageRoute("Default", "", "~/Default.aspx");

			#endregion

			#region Site maintenance route

			routes.MapPageRoute("Maintenance", "maintenance", "~/Maintenance.aspx");

			#endregion

			#region Error routes

			routes.MapPageRoute("Error", "error", "~/Error.aspx");
			routes.MapPageRoute("ErrorWithErrorType", "error/{errorType}", "~/Error.aspx");
			routes.MapPageRoute("NoJavascript", "nojavascript", "~/NoJavascript.aspx");

			#endregion

			#region Help routes

			routes.MapPageRoute("JavascriptHelp", "javascript/help", "~/JavascriptHelp.aspx");

			#endregion

			#region Login, Logout, Change password and Forgotten password routes, register with pin

			routes.MapPageRoute("Login", "login", "~/WebAuth/Login.aspx");
			routes.MapPageRoute("Logout", "logout", "~/WebAuth/Logout.aspx");
			routes.MapPageRoute("ResetPassword", "resetpassword", "~/WebAuth/ResetPassword.aspx");
			routes.MapPageRoute("ForgottenPassword", "forgottenpassword", "~/WebAuth/ForgottenPassword.aspx");
			routes.MapPageRoute("ReactivateAccount", "reactivateaccount", "~/WebAuth/ReactivateAccount.aspx");

			#endregion

			routes.MapPageRoute("UnsubscribeJobSeeker", "unsubscribejobseeker", "~/WebCareer/Unsubscribe.aspx");

			routes.MapPageRoute("UnsubscribeHiringManager", "unsubscribehiringmanager", "~/WebAuth/Unsubscribe.aspx");

			#region Admin routes

			routes.MapPageRoute("AdminDashboard", "admin/dashboard", "~/WebAdmin/Dashboard.aspx");
			routes.MapPageRoute("AdminConfiguration", "admin/configuration", "~/WebAdmin/Configuration.aspx");
			routes.MapPageRoute("Diagnostics", "diagnostics", "~/WebAdmin/Diagnostics.aspx");
			routes.MapPageRoute("BatchProcessRunner", "batch/{id}", "~/WebAdmin/BatchProcessRunner.aspx");
			routes.MapPageRoute("BatchProcessRunnerScheduleImmediate", "batch/{process}/now", "~/WebAdmin/BatchProcessRunner.aspx");
			routes.MapPageRoute("BatchProcessRunnerScheduleMinutes", "batch/{process}/{interval}/minutes", "~/WebAdmin/BatchProcessRunner.aspx");
			routes.MapPageRoute("BatchProcessRunnerScheduleHourly", "batch/{process}/{interval}/hours/{minutes}", "~/WebAdmin/BatchProcessRunner.aspx");
			routes.MapPageRoute("BatchProcessRunnerScheduleDaily", "batch/{process}/{interval}/days/{hhmm}", "~/WebAdmin/BatchProcessRunner.aspx");
			routes.MapPageRoute("BatchProcessRunnerScheduleWeekly", "batch/{process}/{interval}/weeks/{day}/{hhmm}", "~/WebAdmin/BatchProcessRunner.aspx");
      routes.MapPageRoute("ReportPopulation", "admin/report/{type}", "~/WebAdmin/ReportPopulation.aspx");
			routes.MapPageRoute("ReportPopulationWithIds", "admin/report/{type}/{idlist}", "~/WebAdmin/ReportPopulation.aspx");
			routes.MapPageRoute("CacheAdministration", "admin/cache", "~/WebAdmin/CacheAdministration.aspx");
			routes.MapPageRoute("Encryption", "admin/fieldencryption/{key}", "~/WebAdmin/FieldEncryption.aspx");
			routes.MapPageRoute("EncryptionOrDecryption", "admin/fieldencryption/{key}/{direction}", "~/WebAdmin/FieldEncryption.aspx");
      routes.MapPageRoute("ImportEmail", "admin/importedusers/{type}", "~/WebAdmin/SendImportedUserEmails.aspx");
      routes.MapPageRoute("ImportEmailWithNumber", "admin/importedusers/{type}/{number}", "~/WebAdmin/SendImportedUserEmails.aspx");
      routes.MapPageRoute("AdminImport", "admin/import", "~/WebAdmin/Import.aspx");
      routes.MapPageRoute("IntegrationImport", "admin/import/{type}", "~/WebAdmin/IntegrationImport.aspx");

			#endregion

			#region Single Sign On route

			routes.MapPageRoute("SingleSignOn", "sso/{key}", "~/WebAuth/SingleSignOn.aspx");
			routes.MapPageRoute("SingleSignOnWithReactivation", "sso/{key}/{reactivate}", "~/WebAuth/SingleSignOn.aspx");
			routes.MapPageRoute("OAuth", "oauth", "~/WebAuth/OAuth.aspx");
			routes.MapPageRoute("OAuthVerifier", "oauth/verify", "~/WebAuth/OAuth.aspx");
			routes.MapPageRoute("OAuthLogin", "oauth/login", "~/WebAuth/OAuth.aspx");

			routes.MapPageRoute("Saml", "saml", "~/WebAuth/Saml.aspx");
			routes.MapPageRoute("SamlAssert", "saml/assert", "~/WebAuth/Saml.aspx");
			routes.MapPageRoute("SamlResolveArtifacts", "saml/resolveartifacts", "~/WebAuth/Saml.aspx");
			routes.MapPageRoute("SamlLogout", "saml/logout", "~/WebAuth/Saml.aspx");

			routes.MapPageRoute("Validate", "validate/{validate}", "~/WebAuth/SingleSignOn.aspx");
			routes.MapPageRoute("ReportingOutputSSO", "reportingoutput/{reportssoid}/{savedreportid}", "~/WebAuth/SingleSignOn.aspx");
			routes.MapPageRoute("AccessJobAsEmployee", "accessjob/{accessjobssoid}/{jobid}", "~/WebAuth/SingleSignOn.aspx");

			routes.MapPageRoute("SSOError", "ssoerror", "~/WebAuth/SSOError.aspx");
			routes.MapPageRoute("SSOClose", "ssoclose", "~/WebAuth/SSOClose.aspx");
			routes.MapPageRoute("SSOComplete", "ssocomplete", "~/WebAuth/SSOComplete.aspx");

			#endregion

			#region Logo routes

			routes.MapHttpHandlerRoute<DatabaseLogoHttpHandler>("LogoDatabase", "logo/{id}");
			routes.MapHttpHandlerRoute<SessionImageHttpHandler>("LogoSession", "sessionlogo/{sessionKey}/{timestamp}");
			routes.MapHttpHandlerRoute<SessionImageHttpHandler>("LogoSessionArray", "sessionlogo/{sessionKey}/{arrayIndex}/{timestamp}");
			routes.MapHttpHandlerRoute<ApplicationImageHttpHandler>("ApplicationImage", "applicationimage/{applicationImageType}");

			#endregion

			#region Services

			routes.MapPageRoute("AjaxCheckNAICSExist", "NAICSExists/{naics}", "~/Services/NAICSExists.aspx");

			#endregion

			#region Module specific routes

			switch (module)
			{
				case FocusModules.Talent:
				{
					#region Talent Routes

					routes.MapPageRoute("TalentRegister", "register", "~/WebTalent/Register.aspx");
					routes.MapPageRoute("TalentChangeLogin", "changelogin", "~/WebTalent/ChangeLogin.aspx");
					routes.MapPageRoute("TalentUpdateContactInformation", "updatecontactinformation", "~/WebTalent/UpdateContactInformation.aspx");
					routes.MapPageRoute("TalentUpdateCompanyInformation", "updatecompanyinformation", "~/WebTalent/UpdateCompanyInformation.aspx");
					routes.MapPageRoute("TalentManageCompany", "managecompany", "~/WebTalent/ManageCompany.aspx");
					routes.MapPageRoute("TalentManageSavedSearches", "managesavedsearches", "~/WebTalent/ManageSearches.aspx");
					routes.MapPageRoute("TalentJobs", "jobs/{status}", "~/WebTalent/Jobs.aspx", true, new RouteValueDictionary
					{
						{"status", "active"}
					});
          routes.MapPageRoute("TalentJobsForBusinessUnit", "jobs/{status}/{id}", "~/WebTalent/Jobs.aspx");
					routes.MapPageRoute("TalentNewJobSearch", "jobsearch/{jobseekerid}/{newsearch}", "~/WebTalent/SearchJobPostings.aspx");
					routes.MapPageRoute("TalentJobSearch", "jobsearch", "~/WebTalent/SearchJobPostings.aspx");
					routes.MapPageRoute("TalentSeekerJobSearch", "jobsearch/{jobseekerid}", "~/WebTalent/SearchJobPostings.aspx");
					routes.MapPageRoute("TalentJobSearchResultsPage", "searchresults/{jobseekerid}/{page}", "~/WebTalent/JobSearchResults.aspx");
					
					routes.MapPageRoute("TalentJobWizardCreate", "job/wizard", "~/WebTalent/JobWizard.aspx");
					routes.MapPageRoute("TalentJobWizardEdit", "job/wizard/{id}", "~/WebTalent/JobWizard.aspx");
					routes.MapPageRoute("TalentJobWizardEditAtStep", "job/wizard/{id}/{step}", "~/WebTalent/JobWizard.aspx");
					routes.MapPageRoute("TalentJobWizardEducationCreate", "job/wizardeducation", "~/WebTalent/JobWizardEducation.aspx");
					routes.MapPageRoute("TalentJobWizardEducationEdit", "job/wizardeducation/{id}", "~/WebTalent/JobWizardEducation.aspx");
          routes.MapPageRoute("TalentJobUpload", "job/upload", "~/WebTalent/JobUpload.aspx");
					routes.MapPageRoute("TalentJobView", "job/view/{id}", "~/WebTalent/Job.aspx");
					routes.MapPageRoute("TalentPool", "pool/{tab}/{searchsession}", "~/WebTalent/Pool.aspx");
					routes.MapPageRoute("TalentPoolForAdvancedSearch", "pool/{tab}/{searchsession}/{advancedsearch}", "~/WebTalent/Pool.aspx");
					routes.MapPageRoute("TalentPoolForJob", "pool/{tab}/job/{jobid}/{searchsession}", "~/WebTalent/Pool.aspx");
					routes.MapPageRoute("TalentPoolForSavedSearch", "pool/{tab}/search/{searchid}/{searchsession}", "~/WebTalent/Pool.aspx");
					routes.MapPageRoute("TalentPrintJob", "printjob/{id}", "~/WebTalent/PrintJob.aspx");
					routes.MapPageRoute("TalentJobPosting", "assist/jobposting/{jobid}/{fromURL}/{jobseekerid}", "~/WebTalent/JobPosting.aspx");
					routes.MapPageRoute("TalentPrivacyAndSecurity", "talent/privacy", "~/WebTalent/Help.aspx");
					routes.MapPageRoute("TalentHelp", "talent/help", "~/WebTalent/Help.aspx");
					routes.MapPageRoute("TalentFrequentlyAskedQuestions", "talent/faqs", "~/WebTalent/Help.aspx");
					routes.MapPageRoute("TalentTermsOfUse", "talent/terms", "~/WebTalent/Help.aspx");
					routes.MapPageRoute("TalentJobDisplay", "talent/jobdisplay", "~/WebTalent/JobDisplay.aspx");
					routes.MapPageRoute("TalentPing", "ping", "~/WebAdmin/Ping.aspx");

					routes.MapPageRoute("NoticeByCategoryModule", "notice/{title}/{category}/{module}", "~/Code/Pages/Notice.aspx");
					routes.MapPageRoute("NoticeByCategoryGroupModule", "notice/{title}/{category}/{group}/{module}", "~/Code/Pages/Notice.aspx");

					#endregion

					break;
				}

				case FocusModules.Assist:
				{
					#region Assist Routes

					routes.MapPageRoute("AssistDashboard", "assist/dashboard", "~/WebAssist/Dashboard.aspx");
					routes.MapPageRoute("AssistDashboardWithAction", "assist/dashbrd/{action}", "~/WebAssist/Dashboard.aspx");
					routes.MapPageRoute("AssistJobSeekers", "assist/jobseekers", "~/WebAssist/JobSeekers.aspx");
					routes.MapPageRoute("AssistMessageJobSeekers", "assist/jobseekers/message", "~/WebAssist/MessageJobSeekers.aspx");
					routes.MapPageRoute("AssistManageJobSeekerLists", "assist/jobseekers/lists", "~/WebAssist/ManageJobSeekerLists.aspx");
					routes.MapPageRoute("AssistEmployers", "assist/employers", "~/WebAssist/Employers.aspx");
					routes.MapPageRoute("AssistMessageEmployers", "assist/employers/message", "~/WebAssist/MessageEmployers.aspx");
					routes.MapPageRoute("AssistJobSeekerReferrals", "assist/jobseeker/referrals", "~/WebAssist/JobSeekerReferrals.aspx");
					routes.MapPageRoute("AssistJobSeekerReferral", "assist/jobseeker/referral/{id}", "~/WebAssist/JobseekerReferral.aspx");
					routes.MapPageRoute("AssistEmployerReferrals", "assist/employer/referrals", "~/WebAssist/EmployerReferrals.aspx");
					routes.MapPageRoute("AssistEmployerReferral", "assist/employer/referral/{id}", "~/WebAssist/EmployerReferral.aspx");
          routes.MapPageRoute("AssistHiringManagerReferral", "assist/hiringmanager/referral/{id}", "~/WebAssist/EmployerReferral.aspx");
          routes.MapPageRoute("AssistBusinessUnitReferral", "assist/businessunit/referral/{id}", "~/WebAssist/EmployerReferral.aspx");
					routes.MapPageRoute("AssistPostingReferrals", "assist/posting/referrals", "~/WebAssist/PostingReferrals.aspx");
					routes.MapPageRoute("AssistPostingReferral", "assist/posting/referral/{id}", "~/WebAssist/PostingReferral.aspx");
                    routes.MapPageRoute("AssistManageActivitesServices", "assist/manageactivitiesservice", "~/WebAssist/ManageAcitivityService.aspx");
					routes.MapPageRoute("AssistManageAssist", "assist/manage/assist", "~/WebAssist/ManageAssist.aspx");
					routes.MapPageRoute("AssistManageStaffAccounts", "assist/manage/staffaccounts", "~/WebAssist/ManageStaffAccounts.aspx");
          routes.MapPageRoute("AssistManageOffices", "assist/manage/offices", "~/WebAssist/ManageOffices.aspx");
          routes.MapPageRoute("AssistCaseManagementErrors", "assist/casemanagement/errors", "~/WebAssist/CaseManagementErrors.aspx");
					routes.MapPageRoute("AssistViewCaseManagementError", "assist/casemanagement/error", "~/WebAssist/ViewCaseManagementError.aspx");
          routes.MapPageRoute("AssistManageStudentAccounts", "assist/manage/studentaccounts", "~/WebAssist/ManageStudentAccounts.aspx");
					routes.MapPageRoute("AssistManageAnnouncements", "assist/manage/announcements", "~/WebAssist/ManageAnnouncements.aspx");
					routes.MapPageRoute("AssistManageDocuments", "assist/manage/documents", "~/WebAssist/ManageDocuments.aspx");
					routes.MapPageRoute("AssistFeedback", "assist/feedback", "~/WebAssist/Feedback.aspx");
					routes.MapPageRoute("AssistChangePassword", "assist/account/password", "~/WebAssist/ChangePassword.aspx");
					routes.MapPageRoute("AssistContactInformation", "assist/account/contact", "~/WebAssist/ContactInformation.aspx");
					routes.MapPageRoute("AssistSavedMessages", "assist/account/messages", "~/WebAssist/SavedMessages.aspx");
					routes.MapPageRoute("AssistSavedSearches", "assist/account/searches", "~/WebAssist/SavedSearches.aspx");
					routes.MapPageRoute("AssistEmailAlerts", "assist/account/emailalerts", "~/WebAssist/EmailAlerts.aspx");
					routes.MapPageRoute("AssistJobOrderDashboard", "assist/joborderdashboard", "~/WebAssist/JobOrderDashboard.aspx");
					routes.MapPageRoute("AssistJobDevelopment", "assist/businessdevelopment", "~/WebAssist/JobDevelopment.aspx");
					routes.MapPageRoute("AssistContactEmployer", "assist/contactemployer/{id}/{personid}/{otherpersonid}", "~/WebAssist/ContactEmployer.aspx");
					routes.MapPageRoute("AssistPostingEmployerContact", "assist/contact/{id}/{personid}", "~/WebAssist/PostingEmployerContact.aspx");
					routes.MapPageRoute("AssistJobView", "assist/jobview/{id}", "~/WebAssist/Job.aspx");
					routes.MapPageRoute("AssistJobWizardCreate", "assist/jobwizard", "~/WebAssist/JobWizard.aspx");
					routes.MapPageRoute("AssistJobWizardEdit", "assist/jobwizard/{id}", "~/WebAssist/JobWizard.aspx");
					routes.MapPageRoute("AssistJobWizardEditWithSource", "assist/jobwizard/{id}/{source}", "~/WebAssist/JobWizard.aspx");
					routes.MapPageRoute("AssistJobWizardEducationCreate", "assist/jobwizardeducation", "~/WebAssist/JobWizardEducation.aspx");
					routes.MapPageRoute("AssistJobWizardEducationEdit", "assist/jobwizardeducation/{id}", "~/WebAssist/JobWizardEducation.aspx");
					routes.MapPageRoute("AssistJobWizardEducationEditWithSource", "assist/jobwizardeducation/{id}/{source}", "~/WebAssist/JobWizardEducation.aspx");
					routes.MapPageRoute("AssistRegisterEmployer", "assist/employers/register", "~/WebAssist/RegisterEmployer.aspx");
					routes.MapPageRoute("AssistPoolForJob", "assist/pool/{tab}/job/{jobid}", "~/WebAssist/Pool.aspx");
					routes.MapPageRoute("AssistPoolForJobWithSearch", "assist/pool/{tab}/job/{jobid}/{searchid}", "~/WebAssist/Pool.aspx");
					routes.MapPageRoute("AssistPrintJob", "assist/printjob/{id}", "~/WebAssist/PrintJob.aspx");
					routes.MapPageRoute("AssistPrintJobPreview", "assist/printjob/{id}/{type}", "~/WebAssist/PrintJob.aspx");

					routes.MapPageRoute("AssistHelp", "assist/help", "~/WebAssist/Help.aspx");
					routes.MapPageRoute("AssistFrequentlyAskedQuestions", "assist/faqs", "~/WebAssist/Help.aspx");
					routes.MapPageRoute("AssistTermsOfUse", "assist/terms", "~/WebAssist/Help.aspx");
					routes.MapPageRoute("AssistPrivacyAndSecurity", "assist/privacy", "~/WebAssist/Help.aspx");

					routes.MapPageRoute("AssistJobSeekerProfile", "assist/jobseekerprofile/{jobseekerid}", "~/WebAssist/JobSeekerProfile.aspx");
					routes.MapPageRoute("AssistEmployerProfile", "assist/employerprofile/{employerid}", "~/WebAssist/EmployerProfile.aspx");
					routes.MapPageRoute("AssistHiringManagerProfile", "assist/hiringmanagerprofile/{employeebusinessunitid}", "~/WebAssist/HiringManagerProfile.aspx");

					// Assist job searching
					routes.MapPageRoute("AssistSearchSeekerJobPostings", "assist/jobseeker/jobs/{jobseekerid}", "~/WebAssist/SearchJobPostings.aspx");
					//routes.MapPageRoute("AssistNewSearchJobPostings", "assist/jobseeker/jobs/{newsearch}", "~/WebAssist/SearchJobPostings.aspx");
					routes.MapPageRoute("AssistNewSearchJobPostings", "assist/jobseeker/jobs/{jobseekerid}/{newsearch}", "~/WebAssist/SearchJobPostings.aspx");
					routes.MapPageRoute("AssistSearchJobPostings", "assist/jobseeker/jobs", "~/WebAssist/SearchJobPostings.aspx");
					routes.MapPageRoute("AssistJobSearchResults", "assist/searchresults/{jobseekerid}", "~/WebAssist/JobSearchResults.aspx");
					routes.MapPageRoute("AssistJobSearchCriteria", "assist/jobsearch/{Control}", Settings.Theme == FocusThemes.Workforce ? "~/WebAssist/JobSearchCriteria.aspx" : "~/WebAssist/JobSearchEducationCriteria.aspx");
					routes.MapPageRoute("AssistJobPosting", "assist/jobposting/{jobid}/{fromURL}/{jobseekerid}", "~/WebAssist/JobPosting.aspx");
					
					routes.MapPageRoute("AssistJobPostingNoJobseeker", "assist/jobposting/{jobid}/{fromURL}", "~/WebAssist/JobPosting.aspx");
					routes.MapPageRoute("AssistJobDisplay", "assist/jobdisplay", "~/WebAssist/JobDisplay.aspx");
					
					routes.MapPageRoute("AssistJobMatching", "assist/jobmatching/{jobid}/{fromURL}/{jobseekerid}", "~/WebAssist/JobMatching.aspx");

					routes.MapPageRoute("AssistJobSearchResultsPage", "assist/searchresults/{jobseekerid}/{page}", "~/WebAssist/JobSearchResults.aspx");

					// Reporting
					routes.MapPageRoute("ReportingHome", "reporting/home", "~/WebReporting/Default.aspx");
					routes.MapPageRoute("ReportingSavedReports", "reporting/savedreports", "~/WebReporting/SavedReports.aspx");
					routes.MapPageRoute("ReportingSavedReport", "reporting/savedreport/{savedreportid}", "~/WebReporting/Default.aspx");
					routes.MapPageRoute("ReportingOutputWindow", "reporting/outputwindow/{ssoId}/{savedreportid}", "~/WebReporting/OutputWindow.aspx");
					routes.MapPageRoute("ReportingOutputReport", "reporting/output/{savedreportid}", "~/WebReporting/PrintableReport.aspx");
					routes.MapPageRoute("ReportingSessionLinked", "reporting/{report}/{session}", "~/WebReporting/Default.aspx");
					routes.MapPageRoute("ReportingSpecific", "reporting/{report}", "~/WebReporting/Default.aspx");
					routes.MapPageRoute("ReportingEmployerProfile", "reporting/employerprofile/{employerid}/{session}", "~/WebAssist/EmployerProfile.aspx");
					routes.MapPageRoute("ReportingStaffProfile", "reporting/staff/{staffid}/{session}", "~/WebReporting/StaffProfile.aspx");
					routes.MapPageRoute("ReportingHiringManagerProfile", "reporting/hiringmanagerprofile/{employeebusinessunitid}/{session}", "~/WebAssist/HiringManagerProfile.aspx");
					routes.MapPageRoute("ReportingJobSeekerProfile", "reporting/jobseekerprofile/{jobseekerid}/{session}", "~/WebAssist/JobSeekerProfile.aspx");
					routes.MapPageRoute("ReportingJobView", "reporting/jobview/{id}/{session}", "~/WebAssist/Job.aspx");

					routes.MapPageRoute("AssistPing", "ping", "~/WebAdmin/Ping.aspx");

					routes.MapPageRoute("NoticeByCategoryModule", "notice/{title}/{category}/{module}", "~/Code/Pages/Notice.aspx");
					routes.MapPageRoute("NoticeByCategoryGroupModule", "notice/{title}/{category}/{group}/{module}", "~/Code/Pages/Notice.aspx");

					#endregion

					break;
				}

				case FocusModules.Explorer:
				case FocusModules.Career:
				case FocusModules.CareerExplorer:
				{
					#region Explorer Routes

					if (module != FocusModules.Career)
					{
						routes.MapPageRoute("ExplorerHome", "explorer", "~/WebExplorer/Home.aspx");
						routes.MapPageRoute("ExplorerExplore", "explorer/explore", "~/WebExplorer/Explore.aspx");
						routes.MapPageRoute("ExplorerExploreOption", "explorer/explore/{option}", "~/WebExplorer/Explore.aspx");
						routes.MapPageRoute("ExplorerExploreOptionBookmark", "explorer/explore/{option}/{bookmarkId}", "~/WebExplorer/Explore.aspx");
						routes.MapPageRoute("ExplorerPrint", "explorer/print/{type}", "~/WebExplorer/Print.aspx");
						routes.MapPageRoute("ExplorerPrintFormat", "explorer/print/{type}/{format}", "~/WebExplorer/Print.aspx");
						routes.MapPageRoute("ExplorerReport", "explorer/report", "~/WebExplorer/Report.aspx");
						routes.MapPageRoute("ExplorerReportBookmark", "explorer/report/bookmark/{bookmarkId}", "~/WebExplorer/Report.aspx");
						routes.MapPageRoute("ExplorerReportForType", "explorer/report/{type}", "~/WebExplorer/Report.aspx");
						routes.MapPageRoute("ExplorerReportForTypeState", "explorer/report/{type}/{stateAreaId}", "~/WebExplorer/Report.aspx");
						routes.MapPageRoute("ExplorerReportForTypeStateAndCareerArea", "explorer/report/{type}/{stateAreaId}/{careerAreaId}", "~/WebExplorer/Report.aspx");
						routes.MapPageRoute("ExplorerReportForTypeStateAndOccupation", "explorer/report/{type}/{stateAreaId}/{careerAreaOccupationType}/{careerAreaOccupationId}", "~/WebExplorer/Report.aspx");
						routes.MapPageRoute("ExplorerReportForTypeStateAndOccupationAndRelated", "explorer/report/{type}/{stateAreaId}/{careerAreaOccupationType}/{careerAreaOccupationId}/{careerAreaJobId}", "~/WebExplorer/Report.aspx");
						routes.MapPageRoute("ExplorerSearchResults", "explorer/search/{searchTerm}", "~/WebExplorer/SearchResults.aspx");
					}

					#endregion

					#region Career Routes

					if (module != FocusModules.Explorer)
					{
						routes.MapPageRoute("JobSearch", "jobsearch", "~/WebCareer/JobSearch.aspx");
						routes.MapPageRoute("JobDisplay", "jobdisplay", "~/WebCareer/JobDisplay.aspx");
						routes.MapPageRoute("ResumeDisplay", "resumedisplay", "~/WebCareer/ResumeDisplay.aspx");
						routes.MapPageRoute("ResumeDisplayWithType", "resumedisplay/{mimeType}", "~/WebCareer/ResumeDisplay.aspx");
						routes.MapPageRoute("JobSearchResults", "jobsearch/searchresults", "~/WebCareer/JobSearchResults.aspx");
						routes.MapPageRoute("SearchPostingsEducation", "jobsearch/searcheducationpostings", "~/WebCareer/SearchPostingsEducation.aspx");
						routes.MapPageRoute("JobPosting", "jobposting/{jobid}/{fromURL}/{pageSection}", "~/WebCareer/JobPosting.aspx");
						routes.MapPageRoute("JobMatching", "jobsearch/jobmatching/{jobid}/{fromURL}", "~/WebCareer/JobMatching.aspx");
						routes.MapPageRoute("JobSearchCriteria", "jobsearch/{Control}", Settings.Theme == FocusThemes.Workforce ? "~/WebCareer/JobSearchCriteria.aspx" : "~/WebCareer/JobSearchEducationCriteria.aspx");

						routes.MapPageRoute("YourResume", "resume/create", "~/WebCareer/YourResume.aspx");
						routes.MapPageRoute("ResumeWizard", "resume", "~/WebCareer/ResumeWizard.aspx");
						routes.MapPageRoute("ResumeWizardID", "resume/id/{Id}", "~/WebCareer/ResumeWizard.aspx");
						routes.MapPageRoute("ResumeWizardTab", "resume/{Tab}", "~/WebCareer/ResumeWizard.aspx");
						routes.MapPageRoute("ResumeWizardSubTab", "resume/{Tab}/{SubTab}", "~/WebCareer/ResumeWizard.aspx");

						// Spoof arriving from an invite to allow a user to go "home"
						routes.MapPageRoute("Invite", "home", "~/WebCareer/Home.aspx");
						routes.MapPageRoute("PinRegistration", "pinregistration/{pin}", "~/WebCareer/Home.aspx");
						routes.MapPageRoute("SSORegistration", "ssoregistration", "~/WebCareer/Home.aspx");
						routes.MapPageRoute("About", "about", "~/WebCareer/About.aspx");

						// Jobseeker unsubscribing from emails
						routes.MapPageRoute("UnsubscribeFromJsEmails", "Unsubscribe", "~/WebCareer/Unsubscribe.aspx");
					}

					#endregion

					#region Career/Explorer Shared Routes

					// Set the correct home route
					routes.MapPageRoute("Home", "home", module == FocusModules.Explorer ? "~/WebExplorer/Home.aspx" : "~/WebCareer/Home.aspx");
					routes.MapPageRoute("MyBookmarks", "mybookmarks", "~/WebAuth/MyBookmarks.aspx");
					routes.MapPageRoute("CareerExplorerPing", "ping", "~/WebAdmin/Ping.aspx");

					routes.MapPageRoute("CareerExplorerPrivacyAndSecurity", "privacy", "~/WebCareer/Help.aspx");
					routes.MapPageRoute("CareerExplorerHelp", "help", "~/WebCareer/Help.aspx");
					routes.MapPageRoute("CareerExplorerFrequentlyAskedQuestions", "faqs", "~/WebCareer/Help.aspx");
					routes.MapPageRoute("CareerExplorerTermsOfUse", "terms", "~/WebCareer/Help.aspx");
					routes.MapPageRoute("CareerTutorial", "tutorial", "~/WebCareer/Info.aspx");

          routes.MapPageRoute("CareerExternalValidation", "externalvalidation", "~/WebCareer/Home.aspx");

					routes.MapPageRoute("NoticeByCategoryModule", "notice/{title}/{category}/{module}", "~/WebCareer/Notice.aspx");
					routes.MapPageRoute("NoticeByCategoryGroupModule", "notice/{title}/{category}/{group}/{module}", "~/WebCareer/Notice.aspx");

					#endregion

					break;
				}
			}

			#endregion
		}

		#endregion

		/// <summary>
		/// The Client Tag.
		/// </summary>
		public static string ClientTag { get; set; }

		public static string VersionedCookiePrefix { get; private set; }
		public static string SessionCookieName { get; private set; }

		/// <summary>
		/// Generates unique session identifier (a Guid)
		/// </summary>
		public static Guid SessionId
		{
			get
			{
				if (App.Settings.UseSqlSessionState)
				{
					Guid sessionIdGuid;
					var sessionIdValue = GetContextValue<string>(SessionCookieName);

					if (sessionIdValue.IsNullOrEmpty())
					{
						if (sessionIdValue.IsNull())
						{
							var sessionInCookie = GetCookieValue(SessionCookieName);
							if (sessionInCookie.IsNotNullOrEmpty())
							{
								var split = sessionInCookie.Split(new[] {':'});
								var sessionUserId = split[0].ToLong();

								if (sessionUserId == 0 || (App.IsNotNull() && sessionUserId == App.User.UserId))
									sessionIdValue = split[1];
							}
						}

						sessionIdGuid = sessionIdValue.IsNullOrEmpty() ? ServiceClientLocator.Runtime.StartSession(new AppContextModel{ClientTag = ClientTag, RequestId = RequestId, User = User}) : new Guid(sessionIdValue);

						SetContextValue(SessionCookieName, sessionIdGuid.ToString());
						SetCookieValue(SessionCookieName, string.Concat(App.User.UserId.ToString(CultureInfo.InvariantCulture), ":", sessionIdGuid.ToString()));
					}
					else
					{
						sessionIdGuid = new Guid(sessionIdValue);
					}

					return sessionIdGuid;
				}

				var sessionId = GetSessionValue<Guid>("SessionId");

				if (sessionId == Guid.Empty)
				{
					sessionId = ServiceClientLocator.Runtime.StartSession(new AppContextModel { ClientTag = ClientTag, RequestId = RequestId, User = User });
					SetSessionValue("SessionId", sessionId);

					return sessionId;
				}

				return sessionId;
			}
		}

    /// <summary>
    /// Reset the session id
    /// </summary>
    /// <param name="newSessionId">An optional session id to use</param>
    /// <returns>The new session id used</returns>
    public static Guid ResetSessionId(Guid? newSessionId = null)
    {
      var sessionIdGuid = newSessionId ?? ServiceClientLocator.Runtime.StartSession(new AppContextModel
      {
        ClientTag = ClientTag,
        RequestId = RequestId,
        User = User
      });

      if (App.Settings.UseSqlSessionState)
      {
        SetContextValue(SessionCookieName, sessionIdGuid.ToString());
        SetCookieValue(SessionCookieName, string.Concat(App.User.UserId.ToString(CultureInfo.InvariantCulture), ":", sessionIdGuid.ToString()));
      }
      else
      {
        SetSessionValue("SessionId", sessionIdGuid);
      }

      return sessionIdGuid;
    }

		/// <summary>
		/// Generates unique request identifier (a Guid)
		/// </summary>
		public static Guid RequestId
		{
			get
			{
				var requestId = GetContextValue<Guid>("RequestId");

				if (requestId == Guid.Empty)
				{
					requestId = Guid.NewGuid();
					SetContextValue("RequestId", requestId);

					return requestId;
				}

				return requestId;
			}
		}

		#region User context

		/// <summary>
		/// Gets the user.
		/// </summary>
		/// <value>The user.</value>
		public static IUserContext User
		{
			get { return HttpContext.Current.User is UserPrincipal ? (HttpContext.Current.User as UserPrincipal).UserContext : new UserContext(); }
			set
			{
				if (HttpContext.Current.User is UserPrincipal)
					(HttpContext.Current.User as UserPrincipal).UserContext = value;
			}
		}

		/// <summary>
		/// Deserializes the user data.
		/// </summary>
		/// <param name="serializedUserData">The serialized user data.</param>
		/// <returns></returns>
		public static IUserContext DeserializeUserContext(this string serializedUserData)
		{
			return new JavaScriptSerializer().Deserialize<UserContext>(serializedUserData);
		}

		/// <summary>
		/// Serializes the user data.
		/// </summary>
		/// <param name="userContext">The user context.</param>
		/// <returns></returns>
		public static string SerializeUserContext(this IUserContext userContext)
		{
			return new JavaScriptSerializer().Serialize(userContext);
		}

		/// <summary>
		/// Saves the specified user context.
		/// </summary>
		/// <param name="userContext">The user context.</param>
		public static void Save(this IUserContext userContext)
		{
			var context = HttpContext.Current;

			// Recreate the authorisation ticket 
			var cookie = FormsAuthentication.FormsCookieName;
			var httpCookie = context.Request.Cookies[cookie];

			if (httpCookie == null) return;

			var ticket = FormsAuthentication.Decrypt(httpCookie.Value);
			if (ticket == null || ticket.Expired) return;

			var newTicket = new FormsAuthenticationTicket(ticket.Version, ticket.Name, ticket.IssueDate, ticket.Expiration, ticket.IsPersistent, userContext.SerializeUserContext(), ticket.CookiePath);

			httpCookie.Value = FormsAuthentication.Encrypt(newTicket);

			context.Response.Cookies.Set(httpCookie);
		}

		/// <summary>
		/// Determines whether the user is in role.
		/// </summary>
		/// <param name="userContext">The user context.</param>
		/// <param name="role">The role.</param>
		/// <param name="app">The current application object.</param>
		/// <returns>
		/// 	<c>true</c> if the user is in a role; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsInRole(this IUserContext userContext, string role, IApp app = null)
		{
			var roles = GetRoles(userContext.UserId, app);
			return roles.IsNotNullOrEmpty() && roles.Contains(role);
		}

    /// <summary>
    /// Determines whether the user is in all of a list of roles.
    /// </summary>
    /// <param name="userContext">The user context.</param>
    /// <param name="rolesToCheck">The roles to check.</param>
    /// <returns>
    /// 	<c>true</c> if the user is in all the roles; otherwise, <c>false</c>.
    /// </returns>
    public static bool IsInAllRoles(this IUserContext userContext, params string[] rolesToCheck)
    {
      return userContext.IsInAllRoles(null, rolesToCheck);
    }

    /// <summary>
    /// Determines whether the user is in all of a list of roles.
    /// </summary>
    /// <param name="userContext">The user context.</param>
    /// <param name="app">The current application object.</param>
    /// <param name="rolesToCheck">The roles to check.</param>
    /// <returns>
    /// 	<c>true</c> if the user is in all the roles; otherwise, <c>false</c>.
    /// </returns>
    public static bool IsInAllRoles(this IUserContext userContext, IApp app, params string[] rolesToCheck)
    {
      var roles = GetRoles(userContext.UserId, app);
      return roles.IsNotNullOrEmpty() && rolesToCheck.All(roles.Contains);
    }

		    /// <summary>
        /// Determines whether the user is in one of a list of role.
        /// </summary>
        /// <param name="userContext">The user context.</param>
        /// <param name="rolesToCheck">The roles to check.</param>
        /// <returns>
        /// 	<c>true</c> if the user is in at least one role; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsInAnyRole(this IUserContext userContext, params string[] rolesToCheck)
        {
          return userContext.IsInAnyRole(null, rolesToCheck);
        }

        /// <summary>
        /// Determines whether the user is in one of a list of role.
        /// </summary>
        /// <param name="userContext">The user context.</param>
        /// <param name="rolesToCheck">The roles to check.</param>
        /// <param name="app">The current application object.</param>
        /// <returns>
        /// 	<c>true</c> if the user is in at least one role; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsInAnyRole(this IUserContext userContext, IApp app, params string[] rolesToCheck)
        {
          var roles = GetRoles(userContext.UserId, app);

          return roles.IsNotNullOrEmpty() && roles.Any(rolesToCheck.Contains);
        }


        /// <summary>
		/// Updates the roles for the current user.
		/// </summary>
		/// <param name="userId">The user id.</param>
		public static void UpdateRoles(long? userId)
		{
			if (!userId.HasValue || userId == 0)
				return;

			var roles = ServiceClientLocator.AccountClient(App).GetRolesForUser(userId.Value);
			SetContextValue("UserContext:Roles", roles);
		}

		/// <summary>
		/// Gets the roles.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="app">The current application object.</param>
		/// <returns></returns>
		private static List<string> GetRoles(long? userId, IApp app = null)
		{
			if (!userId.HasValue || userId == 0)
				return new List<string>();

			var roles = GetContextValue<List<string>>("UserContext:Roles");

			if (roles.IsNullOrEmpty())
			{
				app = app ?? App;
				roles = ServiceClientLocator.AccountClient(app).GetRolesForUser(userId.Value);
				SetContextValue("UserContext:Roles", roles);
			}

			return roles;
		}

		/// <summary>
		/// Gets the offices.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="getFromDB">Switch to decide whether to get get offices from database.</param>
		/// <returns></returns>
		public static List<OfficeDto> GetOfficesForUser(long? personId, bool getFromDB = false)
		{
			if (!personId.HasValue)
				return new List<OfficeDto>();

			var offices = GetSessionValue("UserContext:Offices", new List<OfficeDto>());

			if (offices.IsNullOrEmpty() || getFromDB)
			{
				offices = ServiceClientLocator.EmployerClient(App).GetOfficesPersonManages(personId.Value);
				SetSessionValue("UserContext:Offices", offices);
			}

			return offices;
		}

		/// <summary>
		/// Determines whether this instance [can view queues pages].
		/// </summary>
		/// <param name="userContext">The user context.</param>
		/// <returns>
		/// 	<c>true</c> if this instance [can view queues pages]; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsAssistUserAndCanViewQueues(this IUserContext userContext)
		{
			var roles = GetRoles(userContext.UserId);

			return (roles.IsNotNullOrEmpty() && (roles.Contains(Constants.RoleKeys.AssistJobSeekerReferralApprover) ||
			                                     roles.Contains(Constants.RoleKeys.AssistEmployerAccountApprover) ||
			                                     roles.Contains(Constants.RoleKeys.AssistPostingApprover) ||
			                                     roles.Contains(Constants.RoleKeys.AssistJobSeekerReferralApprovalViewer) ||
			                                     roles.Contains(Constants.RoleKeys.AssistEmployerAccountApprovalViewer) ||
			                                     roles.Contains(Constants.RoleKeys.AssistPostingApprovalViewer)));
		}

		/// <summary>
		/// Determines whether this instance [can view assist job seekers pages].
		/// </summary>
		/// <param name="userContext">The user context.</param>
		/// <returns>
		/// 	<c>true</c> if this instance [can view assist job seekers pages]; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsAssistUserAndCanViewJobSeekers(this IUserContext userContext)
		{
			var roles = GetRoles(userContext.UserId);

			return (roles.IsNotNullOrEmpty() && (roles.Contains(Constants.RoleKeys.AssistJobSeekersAdministrator) ||
			                                     roles.Contains(Constants.RoleKeys.AssistJobSeekersReadOnly)));
		}

		/// <summary>
		/// Determines whether this instance [can view assist employers pages].
		/// </summary>
		/// <param name="userContext">The user context.</param>
		/// <returns>
		/// 	<c>true</c> if this instance [can view assist employers pages]; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsAssistUserAndCanViewEmployers(this IUserContext userContext)
		{
			var roles = GetRoles(userContext.UserId);

			return (roles.IsNotNullOrEmpty() && (roles.Contains(Constants.RoleKeys.AssistEmployersAdministrator) ||
			                                     roles.Contains(Constants.RoleKeys.AssistEmployersReadOnly)));
		}

		/// <summary>
		/// Determines whether [is assist user and can view reports] [the specified user context].
		/// </summary>
		/// <param name="userContext">The user context.</param>
		/// <returns>
		///   <c>true</c> if [is assist user and can view reports] [the specified user context]; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsAssistUserAndCanViewReports(this IUserContext userContext)
		{
          var roles = GetRoles(userContext.UserId);

          if (roles.IsNullOrEmpty())
            return false;

          var validRoles = new List<string>
          {
            Constants.RoleKeys.AssistJobSeekerReports,
            Constants.RoleKeys.AssistJobOrderReports,
            Constants.RoleKeys.AssistEmployerReports,
            Constants.RoleKeys.AssistJobSeekerReportsViewOnly,
            Constants.RoleKeys.AssistJobOrderReportsViewOnly,
            Constants.RoleKeys.AssistEmployerReportsViewOnly
          };

          return roles.Any(validRoles.Contains);
		}

		#endregion

		#region Settings, Version Info and Exceptions

		/// <summary>
		/// Gets the settings.
		/// </summary>
		/// <value>The settings.</value>
		public static IAppSettings Settings
		{
			get
			{
				var appSettings = GetContextValue<IAppSettings>("AppSettings");

				if (appSettings == null)
				{
					appSettings = ServiceClientLocator.Runtime.Settings;
					SetContextValue("AppSettings", appSettings);

					return appSettings;
				}

				return appSettings;
			}
		}

		/// <summary>
		/// Refreshes the settings.
		/// </summary>
		public static void RefreshSettings()
		{
			ServiceClientLocator.Runtime.RefreshSettings();
			SetContextValue("AppSettings", ServiceClientLocator.Runtime.Settings);
		}

		/// <summary>
		/// Gets the version info.
		/// </summary>
		/// <value>The version info.</value>
		public static string VersionInfo
		{
			get
			{
				var versionInfo = GetCacheValue("VersionInfo", string.Empty);

				if (versionInfo.IsNullOrEmpty())
				{
					versionInfo = (Settings.AppVersion.IsNotNullOrEmpty() ? Settings.AppVersion + "&nbsp;&nbsp;" : "") + ServiceClientLocator.CoreClient(App).GetVersionInfo();
					SetCacheValue("VersionInfo", versionInfo);
				}

				return versionInfo;
			}
		}

		/// <summary>
		/// Gets the current app place holder.
		/// </summary>
		/// <value>The current app place holder.</value>
		public static string CurrentModuleNamePlaceHolder
		{
			get
			{
				switch (Settings.Module)
				{
					case FocusModules.Assist:
						return Constants.PlaceHolders.FocusAssist;
					case FocusModules.Career:
					case FocusModules.CareerExplorer:
						return Constants.PlaceHolders.FocusCareer;
					case FocusModules.Explorer:
						return Constants.PlaceHolders.FocusExplorer;
					case FocusModules.Talent:
						return Constants.PlaceHolders.FocusTalent;
					default:
						return string.Empty;
				}
			}
		}

		/// <summary>
		/// Gets or sets the exception.
		/// </summary>
		/// <value>The exception.</value>
		public static ExceptionModel Exception
		{
			get
			{
				var exception = GetSessionValue<ExceptionModel>("Exception");
				return exception;
			}
			set { SetSessionValue("Exception", value); }
		}

		#endregion

		#region Cache, Session and Context helpers

		private const string HttpContextSessionKeyPrefix = "HttpContextSession:";
		private const string KeyPrefix = "Focus:";

		/// <summary>
		/// Builds the key using a prefix.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>A simple or prefixed key</returns>
		public static string BuildHttpContextSessionKey(string key)
		{
			return string.Concat(HttpContextSessionKeyPrefix, key);
		}

		/// <summary>
		/// Builds the key using a prefix.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>A simple or prefixed key</returns>
		public static string BuildKey(string key)
		{
			return string.Concat(KeyPrefix, key);
		}

		/// <summary>
		/// Gets the cache value for the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static T GetCacheValue<T>(string key, T defaultValue = default(T))
		{
			var obj = (HttpContext.Current.Cache != null ? HttpContext.Current.Cache[BuildKey(key)] : default(T));
			return (obj == null ? defaultValue : (T) obj);
		}

		/// <summary>
		/// Sets the cache value for the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		public static void SetCacheValue<T>(string key, T value)
		{
			if (HttpContext.Current.Cache != null)
				HttpContext.Current.Cache[BuildKey(key)] = value;
		}

		/// <summary>
		/// Gets the session value for the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <param name="knownTypes">The known types.</param>
		/// <returns></returns>
		public static T GetSessionValue<T>(string key, T defaultValue = default(T), IEnumerable<Type> knownTypes = null)
		{
			if (App.Settings.UseSqlSessionState)
			{
				var sessionContextKey = BuildHttpContextSessionKey(key);
				if (HttpContext.Current != null)
				{
					var contextValue = HttpContext.Current.Items[BuildKey(sessionContextKey)];
					if (contextValue != null)
						return (T) contextValue;
				}

				var value = ServiceClientLocator.CoreClient(App).GetSessionValue(key, defaultValue, knownTypes);
				if (knownTypes.IsNull())
					SetContextValue(sessionContextKey, value);

				return value;
			}

			var obj = (HttpContext.Current.Session != null ? HttpContext.Current.Session[BuildKey(key)] : default(T));
			return (obj == null ? defaultValue : (T) obj);
		}

		/// <summary>
		/// Sets the session value for the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		/// <param name="knownTypes">The known types.</param>
		public static void SetSessionValue<T>(string key, T value, IEnumerable<Type> knownTypes = null)
		{
			if (App.Settings.UseSqlSessionState)
			{
				if (value == null)
				{
					RemoveSessionValue(key);
				}
				else
				{
					if (HttpContext.Current == null || knownTypes.IsNotNull())
						ServiceClientLocator.CoreClient(App).SetSessionValue(key, value, knownTypes);

					if (knownTypes.IsNull())
						SetContextValue(BuildHttpContextSessionKey(key), value);
				}
			}
			else
			{
				if (HttpContext.Current.Session != null)
					HttpContext.Current.Session[BuildKey(key)] = value;
			}
		}

		/// <summary>
		/// Removes the session value.
		/// </summary>
		/// <param name="key">The key.</param>
		public static void RemoveSessionValue(string key)
		{
			if (App.Settings.UseSqlSessionState)
			{
				ServiceClientLocator.CoreClient(App).RemoveSessionValue(key);
				RemoveContextValue(BuildHttpContextSessionKey(key));
			}
			else
			{
				if (HttpContext.Current.Session != null)
					HttpContext.Current.Session.Remove(BuildKey(key));
			}
		}

		/// <summary>
		/// Clears all the session values.
		/// </summary>
		public static void ClearSession()
		{
			if (App.Settings.UseSqlSessionState)
			{
				ServiceClientLocator.CoreClient(App).ClearSession();
				if (HttpContext.Current != null)
				{
					var prefix = BuildKey(HttpContextSessionKeyPrefix);
					var keysToRemove = HttpContext.Current.Items.Keys.Cast<object>().OfType<string>().Where(stringKey => stringKey.StartsWith(prefix)).ToList();
					keysToRemove.ForEach(HttpContext.Current.Items.Remove);
				}

				App.SetContextValue(SessionCookieName, string.Empty);
				App.RemoveCookieValue(SessionCookieName);
			}
			else
			{
				if (HttpContext.Current.Session != null)
					HttpContext.Current.Session.Clear();
			}
		}

		/// <summary>
		/// Sets the cookie value for the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		/// <param name="versionCookie">Whether cookie should be named to be specific to system version</param>
		public static void SetCookieValue(string key, string value, bool versionCookie = false)
		{
			var current = HttpContext.Current;
			if (current == null)
				return;

			if (versionCookie)
				key = string.Concat(VersionedCookiePrefix, key);

			current.Response.Cookies.Remove(key);

			var cookie = new HttpCookie(key)
			{
				Value = value
			};
			current.Response.SetCookie(cookie);
		}

		/// <summary>
		/// Gets the cookie value for the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">A default value to return if no cookie present</param>
		/// <param name="versionCookie">Whether cookie should be named to be specific to system version</param>
		/// <returns>The value of the cookie</returns>
		public static string GetCookieValue(string key, string defaultValue = null, bool versionCookie = false)
		{
			var current = HttpContext.Current;
			if (current == null)
				return defaultValue;

			if (versionCookie)
				key = string.Concat(VersionedCookiePrefix, key);

			var cookieValue = defaultValue;

			var cookie = HttpContext.Current.Request.Cookies[key];
			if (cookie.IsNotNull())
				cookieValue = cookie.Value;

			return cookieValue;
		}

		/// <summary>
		/// Removes the cookie for the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="versionCookie">Whether cookie should be named to be specific to system version</param>
		public static void RemoveCookieValue(string key, bool versionCookie = false)
		{
			var current = HttpContext.Current;
			if (current == null)
				return;

			if (versionCookie)
				key = string.Concat(VersionedCookiePrefix, key);

			var cookie = new HttpCookie(key)
			{
				Expires = DateTime.Now.AddDays(-365d)
			};
			current.Response.Cookies.Add(cookie);
		}

		/// <summary>
		/// Gets the context value for the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public static T GetContextValue<T>(string key)
		{
			var obj = (HttpContext.Current != null ? HttpContext.Current.Items[BuildKey(key)] : default(T));
			return (obj == null ? default(T) : (T) obj);
		}

		/// <summary>
		/// Sets the context value for the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		public static void SetContextValue<T>(string key, T value)
		{
			if (HttpContext.Current != null)
				HttpContext.Current.Items[BuildKey(key)] = value;
		}

		/// <summary>
		/// Removes the context value for the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		public static void RemoveContextValue(string key)
		{
			if (HttpContext.Current != null)
				HttpContext.Current.Items.Remove(BuildKey(key));
		}

		#endregion

		#region Other Methods

		/// <summary>
		/// Returns content url for the specified virtual path.
		/// </summary>
		/// <param name="virtualPath">The virtual path.</param>
		/// <returns></returns>
		public static string Content(string virtualPath)
		{
			return VirtualPathUtility.ToAbsolute(virtualPath);
		}

		/// <summary>
		/// Clears the page errors.
		/// </summary>
		/// <returns></returns>
		public static void ClearPageError()
		{
			SetSessionValue(Constants.StateKeys.PageError, "");
		}

		/// <summary>
		/// Sets the page error.
		/// </summary>
		/// <param name="error">The error.</param>
		public static void SetPageError(string error)
		{
			SetSessionValue(Constants.StateKeys.PageError, error);
		}

		#endregion

		#region Module Specific Items

		public static IUserData UserData
		{
			get { return new UserData(App); }
		}

		#endregion

	}

	public interface IUserData
	{
		/// <summary>
		/// Gets a value indicating whether this user has a default resume.
		/// </summary>
		bool HasDefaultResume { get; }

		/// <summary>
		/// Gets or sets the default resume id.
		/// </summary>
		long? DefaultResumeId { get; set; }

		/// <summary>
		/// Gets or sets the default resume completion status.
		/// </summary>
		/// <value>The default resume completion status.</value>
		ResumeCompletionStatuses DefaultResumeCompletionStatus { get; set; }

		/// <summary>
		/// Gets a value indicating whether this instance has user profile.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance has user profile; otherwise, <c>false</c>.
		/// </value>
		bool HasProfile { get; }

		/// <summary>
		/// Gets or sets the user profile.
		/// </summary>
		UserProfile Profile { get; set; }

		/// <summary>
		/// Gets a value indicating whether this user has a default resume.
		/// </summary>
		bool? CareerUserDataCheck { get; set; }

		/// <summary>
		/// Loads the specified model.
		/// </summary>
		/// <param name="model">The model.</param>
		void Load(UserDataModel model);
	}

	public class UserData : IUserData
	{
		private IApp _app;

		public UserData(IApp app)
		{
			_app = app;
		}

		#region Assist

		#endregion

		#region Talent

		#endregion

		#region Career

		/// <summary>
		/// Gets a value indicating whether this user has a default resume.
		/// </summary>
		public bool HasDefaultResume
		{
			get { return DefaultResumeId.HasValue && DefaultResumeId > 0; }
		}

		/// <summary>
		/// Gets or sets the default resume.
		/// </summary>
		public long? DefaultResumeId
		{
			get
			{
				CheckSession();
				return _app.GetSessionValue<long?>("UserData:DefaultResumeId");
			}
			set { _app.SetSessionValue("UserData:DefaultResumeId", value); }
		}

		/// <summary>
		/// Gets or sets the default resume completion status.
		/// </summary>
		/// <value>The default resume completion status.</value>
		public ResumeCompletionStatuses DefaultResumeCompletionStatus
		{
			get
			{
				CheckSession();
				return _app.GetSessionValue<ResumeCompletionStatuses>("UserData:DefaultResumeCompletionStatus");
			}
			set { _app.SetSessionValue("UserData:DefaultResumeCompletionStatus", value); }
		}

		/// <summary>
		/// Gets a value indicating whether this instance has user profile.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance has user profile; otherwise, <c>false</c>.
		/// </value>
		public bool HasProfile
		{
			get { return Profile.IsNotNull(); }
		}

		/// <summary>
		/// Gets or sets the user profile.
		/// </summary>
		public UserProfile Profile
		{
			get
			{
				CheckSession();
				return _app.GetSessionValue<UserProfile>("UserData:Profile");
			}
			set { _app.SetSessionValue("UserData:Profile", value); }
		}

		/// <summary>
		/// Gets a value indicating whether this user has a default resume.
		/// </summary>
		public bool? CareerUserDataCheck
		{
			get
			{
			  return _app.GetSessionValue<bool?>("UserData:CareerUserDataCheck");
			}
			set
			{
			  _app.SetSessionValue("UserData:CareerUserDataCheck", value);
			}
		}

		/// <summary>
		/// Checks whether user data still exists in the session
		/// </summary>
		private void CheckSession()
		{
			if (!_app.Settings.UseSqlSessionState && !CareerUserDataCheck.GetValueOrDefault(false))
			{
				var userData = ServiceClientLocator.AuthenticationClient(_app).GetCareerUserData();
				if (userData.IsNotNull())
				{
					_app.UserData.CareerUserDataCheck = userData.CareerUserDataCheck;
					_app.UserData.DefaultResumeId = userData.DefaultResumeId;
					_app.UserData.DefaultResumeCompletionStatus = userData.DefaultResumeCompletionStatus;
					_app.UserData.Profile = userData.Profile;
				}
				CareerUserDataCheck = true;
			}
		}

		#endregion

		#region Explorer

		#endregion

		/// <summary>
		/// Loads the specified model.
		/// </summary>
		/// <param name="model">The model.</param>
		public void Load(UserDataModel model)
		{
      if (!_app.Settings.UseSqlSessionState)
			  CareerUserDataCheck = model.CareerUserDataCheck;

			DefaultResumeId = model.DefaultResumeId;
			DefaultResumeCompletionStatus = model.DefaultResumeCompletionStatus;
			Profile = model.Profile;
		}
	}

	public interface IApp
	{
		/// <summary>
		/// Starts this instance.
		/// </summary>
		void Start();

		/// <summary>
		/// Shutdowns the App.
		/// </summary>
		void Shutdown();

		/// <summary>
		/// Begins the request.
		/// </summary>
		void BeginRequest();

	  /// <summary>
	  /// Performs actions after the user is authenticated
	  /// </summary>
	  void PostAuthenticateRequest();

		/// <summary>
		/// Ends the request.
		/// </summary>
		void EndRequest();

		/// <summary>
		/// The Client Tag.
		/// </summary>
		string ClientTag { get; }

		/// <summary>
		/// Resets the unique session identifier (a Guid)
		/// </summary>
		Guid SessionId { get; }

		/// <summary>
		/// Generates unique request identifier (a Guid)
		/// </summary>
		Guid RequestId { get; }

		/// <summary>
		/// Gets the user.
		/// </summary>
		/// <value>The user.</value>
		IUserContext User { get; set; }

		/// <summary>
		/// Gets the settings.
		/// </summary>
		/// <value>The settings.</value>
		IAppSettings Settings { get; }

		/// <summary>
		/// Refreshes the settings.
		/// </summary>
		void RefreshSettings();

		/// <summary>
		/// Gets the version info.
		/// </summary>
		/// <value>The version info.</value>
		string VersionInfo { get; }

		/// <summary>
		/// Gets the current app place holder.
		/// </summary>
		/// <value>The current app place holder.</value>
		string CurrentModuleNamePlaceHolder { get; }

		/// <summary>
		/// Gets or sets the exception.
		/// </summary>
		/// <value>The exception.</value>
    ExceptionModel Exception { get; set; }

	  /// <summary>
	  /// Generates unique session identifier (a Guid)
	  /// </summary>
	  Guid ResetSessionId(Guid? newSessionId = null);

		/// <summary>
		/// Deserializes the user data.
		/// </summary>
		/// <param name="serializedUserData">The serialized user data.</param>
		/// <returns></returns>
		IUserContext DeserializeUserContext(string serializedUserData);

		/// <summary>
		/// Serializes the user data.
		/// </summary>
		/// <param name="userContext">The user context.</param>
		/// <returns></returns>
		string SerializeUserContext(IUserContext userContext);

		/// <summary>
		/// Saves the specified user context.
		/// </summary>
		/// <param name="userContext">The user context.</param>
		void Save(IUserContext userContext);

	  void ResetUserContext(IUserContext userContext);

		/// <summary>
		/// Determines whether the user is in role.
		/// </summary>
		/// <param name="userContext">The user context.</param>
		/// <param name="role">The role.</param>
		/// <returns>
		/// 	<c>true</c> if the user is in a role; otherwise, <c>false</c>.
		/// </returns>
		bool IsInRole(IUserContext userContext, string role);

		/// <summary>
		/// Updates the roles for the current user.
		/// </summary>
		/// <param name="userId">The user id.</param>
		void UpdateRoles(long? userId);

		List<OfficeDto> GetOfficesForUser(long? personId, bool getFromDB = false);

		/// <summary>
		/// Determines whether this instance [can view queues pages].
		/// </summary>
		/// <param name="userContext">The user context.</param>
		/// <returns>
		/// 	<c>true</c> if this instance [can view queues pages]; otherwise, <c>false</c>.
		/// </returns>
		bool IsAssistUserAndCanViewQueues(IUserContext userContext);

		/// <summary>
		/// Determines whether this instance [can view assist job seekers pages].
		/// </summary>
		/// <param name="userContext">The user context.</param>
		/// <returns>
		/// 	<c>true</c> if this instance [can view assist job seekers pages]; otherwise, <c>false</c>.
		/// </returns>
		bool IsAssistUserAndCanViewJobSeekers(IUserContext userContext);

		/// <summary>
		/// Determines whether this instance [can view assist employers pages].
		/// </summary>
		/// <param name="userContext">The user context.</param>
		/// <returns>
		/// 	<c>true</c> if this instance [can view assist employers pages]; otherwise, <c>false</c>.
		/// </returns>
		bool IsAssistUserAndCanViewEmployers(IUserContext userContext);

		/// <summary>
		/// Builds the key using a prefix.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>A simple or prefixed key</returns>
		string BuildKey(string key);

		/// <summary>
		/// Gets the cache value for the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		T GetCacheValue<T>(string key, T defaultValue = default(T));

		/// <summary>
		/// Sets the cache value for the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		void SetCacheValue<T>(string key, T value);

    /// <summary>
    /// Gets the session value for the specified key.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="key">The key.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <param name="knownTypes">The known types.</param>
    /// <returns></returns>
		T GetSessionValue<T>(string key, T defaultValue = default(T), IEnumerable<Type> knownTypes = null);

    /// <summary>
    /// Sets the session value for the specified key.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    /// <param name="knownTypes">The known types.</param>
    void SetSessionValue<T>(string key, T value, IEnumerable<Type> knownTypes = null);

		/// <summary>
		/// Removes the session value.
		/// </summary>
		/// <param name="key">The key.</param>
		void RemoveSessionValue(string key);

    /// <summary>
    /// Clears all session value.
    /// </summary>
    void ClearSession();

		/// <summary>
		/// Sets the cookie value for the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		/// <param name="versionCookie">Whether cookie should be named to be specific to system version</param>
		void SetCookieValue(string key, string value, bool versionCookie = false);

    /// <summary>
    /// Gets the cookie value for the specified key.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="defaultValue">A default value to return if no cookie present</param>
    /// <param name="versionCookie">Whether cookie is specific to system version</param>
    /// <returns>The value of the cookie</returns>
    string GetCookieValue(string key, string defaultValue = null, bool versionCookie = false);

		/// <summary>
		/// Removes the cookie for the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
    /// <param name="versionCookie">Whether cookie is specific to system version</param>
    void RemoveCookieValue(string key, bool versionCookie = false);

		/// <summary>
		/// Gets the context value for the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		T GetContextValue<T>(string key);

		/// <summary>
		/// Sets the context value for the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		void SetContextValue<T>(string key, T value);

	  /// <summary>
	  /// Removes the context value for the specified key.
	  /// </summary>
	  /// <param name="key">The key.</param>
	  void RemoveContextValue(string key);

		/// <summary>
		/// Returns content url for the specified virtual path.
		/// </summary>
		/// <param name="virtualPath">The virtual path.</param>
		/// <returns></returns>
		string Content(string virtualPath);

		/// <summary>
		/// Clears the page error.
		/// </summary>
		void ClearPageError();

		/// <summary>
		/// Sets the page error.
		/// </summary>
		/// <param name="error">The error.</param>
		void SetPageError(string error);

		/// <summary>
		/// Gets the user data.
		/// </summary>
		/// <value>
		/// The user data.
		/// </value>
		IUserData UserData { get; }
    
	  /// <summary>
	  /// Checks whether the site is in anoymous only mode
	  /// </summary>
	  bool IsAnonymousAccess();

    /// <summary>
    /// Checks whether the site is in anoymous only mode
    /// </summary>
    bool IsAnonymousDomain();

    /// <summary>
    /// Checks whether the user must be guided to the resume page
    /// </summary>
    /// <param name="personId">Optional id of the person (in the case where the user has not yet been authenticated</param>
    /// <returns></returns>
    bool GuideToResume(long? personId = null);
	}

	public class HttpApp : IApp
	{
		public void Start()
		{
			OldApp_RefactorIfFound.Start();
		}

		public void Shutdown()
		{
			OldApp_RefactorIfFound.Shutdown();
		}

		public void BeginRequest()
		{
			OldApp_RefactorIfFound.BeginRequest();
		}

    public void PostAuthenticateRequest()
    {
      OldApp_RefactorIfFound.PostAuthenticateRequest();
    }

		public void EndRequest()
		{
			OldApp_RefactorIfFound.EndRequest();
		}

		public string ClientTag { get { return OldApp_RefactorIfFound.ClientTag; } }
		public Guid SessionId { get { return OldApp_RefactorIfFound.SessionId; } }
		public Guid RequestId { get { return OldApp_RefactorIfFound.RequestId; } }
		public IUserContext User
		{
			get { return OldApp_RefactorIfFound.User; }
			set { OldApp_RefactorIfFound.User = value; }
		}

		public IAppSettings Settings { get { return OldApp_RefactorIfFound.Settings; } }

		public void RefreshSettings()
		{
			OldApp_RefactorIfFound.RefreshSettings();
		}

		public string VersionInfo { get { return OldApp_RefactorIfFound.VersionInfo; } }
		public string CurrentModuleNamePlaceHolder { get { return OldApp_RefactorIfFound.CurrentModuleNamePlaceHolder; } }
    public ExceptionModel Exception
		{
			get { return OldApp_RefactorIfFound.Exception; }
			set { OldApp_RefactorIfFound.Exception = value; }
		}

    public Guid ResetSessionId(Guid? newSessionId = null)
    {
      return OldApp_RefactorIfFound.ResetSessionId(newSessionId);
    }

		public IUserContext DeserializeUserContext(string serializedUserData)
		{
			return serializedUserData.DeserializeUserContext();
		}

		public string SerializeUserContext(IUserContext userContext)
		{
			return userContext.SerializeUserContext();
		}

		public void Save(IUserContext userContext)
		{
			userContext.Save();
		}

    public void ResetUserContext(IUserContext userContext)
    {
      var authTicket = new FormsAuthenticationTicket(1, userContext.FirstName, DateTime.Now, DateTime.Now.AddMinutes(Settings.UserSessionTimeout), false, userContext.SerializeUserContext());
      var encryptedTicket = FormsAuthentication.Encrypt(authTicket);
      var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

      HttpContext.Current.Response.Cookies.Add(authCookie);

      HttpContext.Current.User = new UserPrincipal(new FormsIdentity(authTicket), userContext);
    }

		public bool IsInRole(IUserContext userContext, string role)
		{
			return userContext.IsInRole(role);
		}

		public void UpdateRoles(long? userId)
		{
			OldApp_RefactorIfFound.UpdateRoles(userId);
		}

		public List<OfficeDto> GetOfficesForUser(long? personId, bool getFromDB = false)
		{
			return OldApp_RefactorIfFound.GetOfficesForUser(personId, getFromDB);
		}

		public bool IsAssistUserAndCanViewQueues(IUserContext userContext)
		{
			return userContext.IsAssistUserAndCanViewQueues();
		}

		public bool IsAssistUserAndCanViewJobSeekers(IUserContext userContext)
		{
			return userContext.IsAssistUserAndCanViewJobSeekers();
		}

		public bool IsAssistUserAndCanViewEmployers(IUserContext userContext)
		{
			return userContext.IsAssistUserAndCanViewEmployers();
		}

		public string BuildKey(string key)
		{
			return OldApp_RefactorIfFound.BuildKey(key);
		}

		public T GetCacheValue<T>(string key, T defaultValue = default(T))
		{
			return OldApp_RefactorIfFound.GetCacheValue(key,defaultValue);
		}

		public void SetCacheValue<T>(string key, T value)
		{
			OldApp_RefactorIfFound.SetCacheValue(key, value);
		}

		public T GetSessionValue<T>(string key, T defaultValue = default(T), IEnumerable<Type> knownTypes = null)
		{
      return OldApp_RefactorIfFound.GetSessionValue(key, defaultValue, knownTypes);
		}

    public void SetSessionValue<T>(string key, T value, IEnumerable<Type> knownTypes = null)
		{
			OldApp_RefactorIfFound.SetSessionValue(key, value, knownTypes);
		}

    public void RemoveSessionValue(string key)
		{
			OldApp_RefactorIfFound.RemoveSessionValue(key);
		}

    public void ClearSession()
    {
      OldApp_RefactorIfFound.ClearSession();
    }

    public void SetCookieValue(string key, string value, bool versionCookie = false)
		{
      OldApp_RefactorIfFound.SetCookieValue(key, value, versionCookie);
		}

    public string GetCookieValue(string key, string defaultValue = null, bool versionCookie = false)
		{
      return OldApp_RefactorIfFound.GetCookieValue(key, defaultValue, versionCookie);
		}

    public void RemoveCookieValue(string key, bool versionCookie = false)
		{
      OldApp_RefactorIfFound.RemoveCookieValue(key, versionCookie);
		}

		public T GetContextValue<T>(string key)
		{
			return OldApp_RefactorIfFound.GetContextValue<T>(key);
		}

		public void SetContextValue<T>(string key, T value)
		{
			OldApp_RefactorIfFound.SetContextValue(key, value);
		}

    public void RemoveContextValue(string key)
    {
      OldApp_RefactorIfFound.RemoveContextValue(key);
    }

		public string Content(string virtualPath)
		{
			return OldApp_RefactorIfFound.Content(virtualPath);
		}

		public IUserData UserData { get { return new UserData(this);}}

		public void ClearPageError()
		{
			OldApp_RefactorIfFound.ClearPageError();
		}

		public void SetPageError(string error)
		{
			OldApp_RefactorIfFound.SetPageError(error);
		}

	  /// <summary>
	  /// Checks whether the site is being accessed in anonymous only mode
	  /// </summary>
	  public bool IsAnonymousAccess()
	  {
	    if (User.IsNotNull())
	    {
	      if (User.IsAuthenticated)
	        return false;

	      if (User.IsAnonymous)
	        return true;
	    }

	    return IsAnonymousDomain();
	  }

	  /// <summary>
	  /// Checks whether the domain is intended for anonymous access
	  /// </summary>
	  public bool IsAnonymousDomain()
    {
      bool? isAnonymous = false;
      if (HttpContext.Current.IsNotNull())
      {
        isAnonymous = GetContextValue<bool?>("IsAnonymous");
        if (isAnonymous.IsNull())
        {
          isAnonymous = (Settings.IsCareerExplorerAnonymousOnly == AnonymousAccess.All)
            || (Settings.IsCareerExplorerAnonymousOnly == AnonymousAccess.Domain && Settings.IsCareerExplorerAnonymousOnlyDomainNames.Contains(HttpContext.Current.Request.Url.Host, StringComparer.OrdinalIgnoreCase));

          SetContextValue("IsAnonymous", isAnonymous);
        }
      }
	    return isAnonymous.GetValueOrDefault(false);
	  }

    /// <summary>
    /// Checks whether the user must be guided to the resume page
    /// </summary>
    /// <param name="personId">Optional id of the person (in the case where the user has not yet been authenticated</param>
    public bool GuideToResume(long? personId = null)
    {
      bool? guideToResume = false;

      if (Settings.ResumeGuidedPath && (personId.IsNotNull() || User.IsAuthenticated))
      {
        if (HttpContext.Current.IsNotNull())
          guideToResume = GetContextValue<bool?>("GuideToResume");

        if (guideToResume.IsNull())
        {
          if (User.IsAuthenticated && UserData.IsNotNull())
            guideToResume = !(UserData.HasDefaultResume && UserData.DefaultResumeCompletionStatus == ResumeCompletionStatuses.Completed);
          else
            guideToResume = (ServiceClientLocator.ResumeClient(this).GetDefaultResumeId(personId.GetValueOrDefault(0), true) <= 0);

          if (HttpContext.Current.IsNotNull())
            SetContextValue("GuideToResume", guideToResume); 
        }
      }

      return guideToResume.GetValueOrDefault(false);
    }
	}
}
