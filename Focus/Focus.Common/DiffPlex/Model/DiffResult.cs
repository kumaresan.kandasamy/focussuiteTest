﻿using System.Collections.Generic;
using System.Text;
using Framework.Core;

namespace Focus.Common.DiffPlex.Model
{
	/// <summary>
	/// The result of diffing two peices of text
	/// </summary>
	public class DiffResult
	{
		/// <summary>
		/// The chunked peices of the old text
		/// </summary>
		public string[] PiecesOld { get; private set; }

		/// <summary>
		/// The chunked peices of the new text
		/// </summary>
		public string[] PiecesNew { get; private set; }


		/// <summary>
		/// A collection of DiffBlocks which details deletions and insertions
		/// </summary>
		public IList<DiffBlock> DiffBlocks { get; private set; }

		public DiffResult(string[] peicesOld, string[] piecesNew, IList<DiffBlock> blocks)
		{
			PiecesOld = peicesOld;
			PiecesNew = piecesNew;
			DiffBlocks = blocks;
		}

		private static string ProcessDiffSection( int blockCount, string[] diffBlock, ref int blockPos, StringBuilder titleText, bool isInsertedText, bool hasDeletedText )
		{
			string displayText;
			if (isInsertedText)
			{
				displayText = hasDeletedText ? "<span class='changedText'>" : "<span class='insertedText'>";
			}
			else
			{
				displayText = "<span class='deletedText'>";				
			}
			titleText.Append( isInsertedText ? "<span class='insertedText'>" : "<span class='deletedText'>" );
			for( var j = 0; j < blockCount; j++ )
			{
				var wordText = diffBlock[blockPos].Replace("\"", "&quot;");
				titleText.Append( wordText );
				displayText += wordText;
				blockPos++;
			}
			titleText.Append( "</span>" );
			displayText += "</span>";
			return displayText;
		}

		/// <summary>
		/// Builds an HTML output representation of the differences between the two texts compared
		/// </summary>
		/// <returns></returns>
		public string BuildDiffText()
		{
			int newPiecePos = 0, oldPiecePos = 0;
			var text = new StringBuilder();
			var diffBlockPos = 0;
			var currentDiffBlock = DiffBlocks.Count == 0 ? null : DiffBlocks[0];
			while( newPiecePos < PiecesNew.Length || oldPiecePos < PiecesOld.Length )
			{
				if (currentDiffBlock != null && (currentDiffBlock.DeleteStartA == oldPiecePos || currentDiffBlock.InsertStartB == newPiecePos))
				{
					int j;
					var textToDisplay = "";
					var titleText = new StringBuilder();
					for( j = 0; j < currentDiffBlock.InsertCountB; j++ )
					{
						textToDisplay += PiecesNew[newPiecePos + j];
					}
					var displayText = new StringBuilder();
					if( currentDiffBlock.DeleteStartA == oldPiecePos && currentDiffBlock.DeleteCountA > 0 )
					{
						titleText.Append( currentDiffBlock.InsertCountB > 0 && !textToDisplay.IsNullOrWhitespace() ? "<center><h4>CHANGED TEXT</h4></center>" : "<center><h4>DELETED TEXT</h4></center>" );
						if (newPiecePos == PiecesNew.Length)
						{
							titleText.Append( " " );
						}
						displayText.Append( ProcessDiffSection( currentDiffBlock.DeleteCountA, PiecesOld, ref oldPiecePos, titleText, false, true ) );
						displayText.Append( "</span>" );
					}
					if (currentDiffBlock.InsertStartB == newPiecePos && currentDiffBlock.InsertCountB > 0)
					{
						if( textToDisplay.IsNullOrWhitespace() && displayText.Length > 0 )
						{
							newPiecePos += currentDiffBlock.InsertCountB;
							displayText.Append( textToDisplay );
						}
						else
						{
							if( currentDiffBlock.DeleteCountA == 0 )
							{
								titleText.Append( "<center><h4>INSERTED TEXT</h4></center>" );
							}
							displayText.Clear();
							displayText.Append( ProcessDiffSection( currentDiffBlock.InsertCountB, PiecesNew, ref newPiecePos, titleText, true, currentDiffBlock.DeleteCountA > 0 ) );
							displayText.Append("</span>");
						}
					}
					if (titleText.Length > 0)
					{
						text.Append("<span class=\"toolTipNew\" title=\"");
						text.Append(titleText);
						text.Append("\">");
					}
					text.Append(displayText);
					diffBlockPos++;
					currentDiffBlock = diffBlockPos < DiffBlocks.Count ? DiffBlocks[diffBlockPos] : null;
				}
				else
				{
					text.Append(PiecesNew[newPiecePos++]);
					oldPiecePos++;
				}
			}
			return text.ToString();
		}
	}
}