﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion	

namespace Focus.Common.Models
{
	[DataContract]
	public class AutoCompleteItem
	{
		[DataMember(Name="id")]
		public string Id { get; set; }

		[DataMember(Name="label")]
		public string Label { get; set; }

		[DataMember(Name="value")]
		public string Value { get; set; }
	}

	[Serializable]
	[DataContract]
	public class AutoCompleteMethodParameter
	{
		[DataMember]
		public string ControlId { get; set; }

		[DataMember]
		public string ParameterName { get; set; }

		[DataMember]
		public string ParameterType { get; set; }

    [DataMember]
    public string OverrideValue { get; set; }
	}
}
