﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;

#endregion 

namespace Focus.Common.Models
{
  [Serializable]
  public class AssistTalentPoolModel
  {
    private long _jobId;

		/// <summary>
		/// Initializes a new instance of the <see cref="AssistTalentPoolModel" /> class.
		/// </summary>
		public AssistTalentPoolModel()
		{
			SearchId = Guid.Empty;
			CurrentTab = TalentPoolListTypes.AllResumes;
			SearchCriteria = new CandidateSearchCriteria { JobDetailsCriteria = new CandidateSearchJobDetailsCriteria() };
		}

		public long JobId
    {
      get { return _jobId; }
      set
      {
        _jobId = value;
        SearchCriteria.JobDetailsCriteria.JobId = value;
      }
    }

    public string JobTitle { get; set; }
    public string EmployerName { get; set; }
		public JobStatuses JobStatus { get; set; }

    public Guid SearchId { get; set; }
    public Guid SearchCriteriaId { get; set; }
    public TalentPoolListTypes CurrentTab { get; set; }
    public CandidateSearchCriteria SearchCriteria { get; set; }

    public DateTime? VeteranPriorityEndDate { get; set; }
    public ExtendVeteranPriorityOfServiceTypes? ExtendVeteranPriority { get; set; }
  }
}
