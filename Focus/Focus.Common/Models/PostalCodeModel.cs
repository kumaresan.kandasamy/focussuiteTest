﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

using AjaxControlToolkit;

#endregion

namespace Focus.Common.Models
{
	[DataContract]
	public class PostalCodeStateCityCounty
	{
		/// <summary>
		/// Gets or sets the state id.
		/// </summary>
		[DataMember]
		public long StateId { get; set; }

		/// <summary>
		/// Gets or sets the county id.
		/// </summary>
		[DataMember]
		public long CountyId { get; set; }

    /// <summary>
    /// Gets or sets thecity name.
    /// </summary>
    [DataMember]
    public string CityName{ get; set; }

		/// <summary>
		/// Gets or sets the counties.
		/// </summary>
		[DataMember]
		public CascadingDropDownNameValue[] Counties { get; set; }
	}
}
