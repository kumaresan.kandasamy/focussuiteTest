﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

#endregion

namespace Focus.Common.Models
{
  [Serializable]
  public class JobDegreeEducationLevelModel
  {
    public long DegreeEducationLevelId { get; set; }
    public int HiringDemand { get; set; }
    public string DegreeEducationLevelName { get; set; }
    public long DegreeId { get; set; }
    public string DegreeName { get; set; }
    public long ProgramAreaId { get; set; }
    public string ProgramAreaName { get; set; }
  }
}
