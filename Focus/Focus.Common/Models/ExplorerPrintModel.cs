﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Models;
using Focus.Core.Views;

#endregion

namespace Focus.Common.Models
{
  [Serializable]
  public class ExplorerPrintModel
  {
    public string Name { get; set; }
    public string Location { get; set; }
    public ExplorerCriteria ReportCriteria { get; set; }
  }

  [Serializable]
  public class ExplorerJobPrintModel : ExplorerPrintModel
  {
    public string JobTitles { get; set; }
    public string JobIntroduction { get; set; }
    public bool JobRelatedToSchoolDegree { get; set; }
    public bool ShowOtherFoundationSkills { get; set; }
    public StarterJobLevel? StarterJobLevel { get; set; }

    public string HiringTrend { get; set; }
    public string HiringTrendArrowSpan { get; set; }
    public string HiringDemand { get; set; }
    public string HiringDemandImageUrl { get; set; }
    public string SalaryRange { get; set; }
    public string SalaryRangeImageUrl { get; set; }
    public string SalaryNationwideLabel { get; set; }
    public string SalaryNationwide { get; set; }
    public string SalaryNationwideImageUrl { get; set; }

    public List<EmployerJobViewDto> Employers { get; set; }

    public List<JobSkillViewDto> SpecializedSkills { get; set; }
    public List<JobSkillViewDto> SoftwareSkills { get; set; }
    public List<JobSkillViewDto> FoundationSkills { get; set; }
    public List<JobSkillViewDto> HeldSkills { get; set; }

    public string DegreeIntro { get; set; }
    public JobDegreeCertificationModel DegreeCertifications { get; set; }
    public List<ProgramAreaDegreeViewDto> ProgramAreaDegrees;

    public bool EducationRequirementsVisible { get; set; }
    public string HighSchoolTechnicalTrainingSpan { get; set; }
    public string AssociatesDegreeSpan { get; set; }
    public string BachelorsDegreeSpan { get; set; }
    public string GraduateProfessionalDegreeSpan { get; set; }

    public string LessThanTwoSpan { get; set; }
    public string TwoToFiveSpan { get; set; }
    public string FiveToEightSpan { get; set; }
    public string EightPlusSpan { get; set; }

    public List<ExplorerJobReportView> SimilarJobs { get; set; }

    public List<JobCareerPathModel> CareerPathFrom { get; set; }
    public List<JobCareerPathModel> CareerPathTo { get; set; }
  }

  [Serializable]
  public class ExplorerEmployerPrintModel : ExplorerPrintModel
  {
    public List<ExplorerEmployerJobReportView> Jobs { get; set; }
    public List<InternshipReportViewDto> Internships { get; set; }

    public List<SkillModel> SpecializedSkills { get; set; }
    public List<SkillModel> SoftwareSkills { get; set; }
    public List<SkillModel> FoundationSkills { get; set; }
  }

  [Serializable]
  public class ExplorerDegreeCertificationPrintModel : ExplorerPrintModel
  {
    public int Tier { get; set; }

    public List<ExplorerJobReportView> Jobs { get; set; }
    public List<ExplorerJobReportView> JobsTier2 { get; set; }

    public List<SkillModel> SpecializedSkills { get; set; }
    public List<SkillModel> SoftwareSkills { get; set; }
    public List<SkillModel> FoundationSkills { get; set; }

    public List<EmployerJobViewDto> Employers { get; set; }

    public List<StudyPlaceDegreeEducationLevelViewDto> StudyPlaces { get; set; }
  }

  [Serializable]
  public class ExplorerSkillPrintModel : ExplorerPrintModel
  {
    public List<ExplorerJobReportView> Jobs { get; set; }
    public List<InternshipReportViewDto> Internships { get; set; }

    public List<EmployerJobViewDto> Employers { get; set; }
    public JobDegreeCertificationModel DegreeCertifications { get; set; }

    public List<JobDegreeEducationLevelModel> JobDegreesWithProgramAreas;
    public List<JobDegreeEducationLevelModel> JobDegreesWithoutProgramAreas;
  }

  [Serializable]
  public class ExplorerInternshipPrintModel : ExplorerPrintModel
  {
    public List<SkillModel> SpecializedSkills { get; set; }
    public List<SkillModel> SoftwareSkills { get; set; }
    public List<SkillModel> FoundationSkills { get; set; }

    public List<EmployerJobViewDto> Employers { get; set; }

    public List<InternshipJobTitleDto> Jobs { get; set; }
  }

  [Serializable]
  public class ExplorerProgramAreaDegreePrintModel : ExplorerPrintModel
  {
    public string Description { get; set; }

    public List<ExplorerJobReportView> Jobs { get; set; }
    public List<ProgramAreaDegreeEducationLevelViewDto> Degrees { get; set; }
  }

  [Serializable]
  public class ExplorerCareerAreaPrintModel : ExplorerPrintModel
  {
    public List<ExplorerJobReportView> Jobs { get; set; }

    public List<SkillModel> SpecializedSkills { get; set; }
    public List<SkillModel> SoftwareSkills { get; set; }
    public List<SkillModel> FoundationSkills { get; set; }
  }
}
