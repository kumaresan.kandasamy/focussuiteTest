﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Common.Models
{
	/// <summary>
	/// JSON response.
	/// </summary>
	[DataContract]
	public class JsonResponse
	{
		/// <summary>
		///	Gets or sets Data.
		/// </summary>
		[DataMember]
		public string Data { get; set; }

		/// <summary>
		///	Gets or sets Message.
		/// </summary>
		[DataMember]
		public string Message { get; set; }

		/// <summary>
		///	Gets or sets a value indicating whether Success.
		/// </summary>
		[DataMember]
		public bool Success { get; set; }
	}

	/// <summary>
	/// JSON response with a strongly typed data.
	/// </summary>
	[DataContract]
	public class JsonResponse<T>
	{
		/// <summary>
		///	Gets or sets Data.
		/// </summary>
		[DataMember]
		public T Data { get; set; }

		/// <summary>
		///	Gets or sets Message.
		/// </summary>
		[DataMember]
		public string Message { get; set; }

		/// <summary>
		///	Gets or sets a value indicating whether Success.
		/// </summary>
		[DataMember]
		public bool Success { get; set; }
	}
}
