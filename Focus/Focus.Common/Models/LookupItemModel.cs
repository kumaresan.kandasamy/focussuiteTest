﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Common.Models
{
	[DataContract]
	public class LookupItem
	{
		[DataMember]
		public string Name { get; set; }

		[DataMember]
		public string Value { get; set; }

		[DataMember]
		public bool IsDefaultValue { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="LookupItem"/> class.
		/// </summary>
    public LookupItem()
    { }

		/// <summary>
		/// Initializes a new instance of the <see cref="LookupItem"/> class.
		/// </summary>
		/// <param name="name">The name.</param>
		public LookupItem(string name)
		{
			Name = Value = name;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="LookupItem"/> class.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
    public LookupItem(string name, string value)
    {
      Name = name;
      Value = value;
    }

		/// <summary>
		/// Initializes a new instance of the <see cref="LookupItem"/> class.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		/// <param name="defaultValue">if set to <c>true</c> [default value].</param>
		public LookupItem(string name, string value, bool defaultValue)
    {
      Name = name;
      Value = value;
      IsDefaultValue = defaultValue;
    }
	}
}
