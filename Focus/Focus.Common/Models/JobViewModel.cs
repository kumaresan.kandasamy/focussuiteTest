﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core;

#endregion

namespace Focus.Common.Models
{
  [Serializable]
  public class JobViewModel
  {
    public long JobId { get; set; }
    public long? EmployeeId { get; set; }
    public long EmployerId { get; set; }
    public JobStatuses JobStatus { get; set; }
    public string JobTitle { get; set; }
    public int JobOrderActivityPageSize { get; set; }
    public int JobOrderActivityDaysBack { get; set; }
    public string JobOrderActivitySortColumn { get; set; }
    public string JobOrderActivitySortDirection { get; set; }
    public int JobOrderActivityPageNumber { get; set; }
    public NoteReminderTypes NotesOrReminders { get; set; }
    public int NotesAndRemindersPageSize { get; set; }
    public int NotesAndRemindersPageNumber { get; set; }
    public bool IsConfidential { get; set; }
		public JobTypes? JobType { get; set; }
		public DateTime? JobClosedOn { get; set; }
    public DateTime? JobClosingOn { get; set; }
    public bool AutoClosed { get; set; }
  }
}
