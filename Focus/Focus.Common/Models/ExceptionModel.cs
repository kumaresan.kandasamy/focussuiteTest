﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Common.Models
{
  /// <summary>
  /// Class used to store details on an Exception in the session
  /// </summary>
  [DataContract]
  public class ExceptionModel
  {
    [DataMember]
    public string Message { get; set; }

    [DataMember]
    public string StackTrace { get; set; }

    public ExceptionModel()
    {
      
    }

    public ExceptionModel(string message)
    {
      Message = message;
    }
  }
}
