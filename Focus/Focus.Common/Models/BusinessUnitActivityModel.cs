﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Common.Models
{
	[Serializable]
	public class BusinessUnitActivityModel
	{
		public BusinessUnitDto BusinessUnit { get; set; }
		public int BusinessUnitId { get; set; }
		public DateTime? ActionDateTime { get; set; }
		public string Username { get; set; }
		public string ActionName { get; set; }
	}
}
