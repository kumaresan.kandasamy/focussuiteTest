﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Common.Models
{
	[DataContract]
	public class LocalisationItem
	{
		[DataMember]
		public long Id { get; set; }

		[DataMember]
		public string Culture { get; set; }

		[DataMember]
		public string ContextKey { get; set; }

		[DataMember]
		public string Key { get; set; }

		[DataMember]
		public string Value { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="LocalisationItem"/> class.
		/// </summary>
		public LocalisationItem()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="LocalisationItem"/> class.
		/// </summary>
		/// <param name="id">The id.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="contextKey">The context key.</param>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		public LocalisationItem(long id, string culture, string contextKey, string key, string value)
		{
			Id = id;
			Culture = culture;
			ContextKey = contextKey;
			Key = key;
			Value = value;
		}
	}
}
