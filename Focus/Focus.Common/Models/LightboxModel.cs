﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;

using Focus.Core;
using Focus.Core.Criteria.Explorer;

#endregion

namespace Focus.Common.Models
{
	[Serializable]
	public class LightboxBreadcrumb
	{
		public ReportTypes LinkType { get; set; }
		public long LinkId { get; set; }
		public string LinkText { get; set; }
    public ExplorerCriteria Criteria { get; set; }
	}

	[Serializable]
	public class LightboxEventCommandArguement
	{
		public long Id { get; set; }
		public List<LightboxBreadcrumb> Breadcrumbs { get; set; }
    public ModalPrintFormat PrintFormat { get; set; }
		public string LightboxName { get; set; }
		public string LightboxLocation { get; set; }
    public bool MovingBack { get; set; }
    public ExplorerCriteria CriteriaHolder { get; set; }
    public ExplorerCriteria MainPageCriteria { get; set; }
  }

	[Serializable]
	public class LightboxEventArgs
	{
		public string CommandName { get; set; }
		public LightboxEventCommandArguement CommandArgument { get; set; }
	}

  [Serializable]
  public class FilterCommandArgument
  {
    public ExplorerCriteria NewCriteria { get; set; }
    public ExplorerCriteria OldCriteria { get; set; }
  }

  [Serializable]
  public class FilterEventArgs
  {
    public string CommandName { get; set; }
    public FilterCommandArgument CommandArgument { get; set; }
  }
}
