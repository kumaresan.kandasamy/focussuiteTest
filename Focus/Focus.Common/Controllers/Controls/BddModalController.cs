﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common.Code.ControllerResults;

#endregion

namespace Focus.Common.Code.Controllers.Controls
{
	public class BddModalController : PageControllerBase
	{
		public BddModalController(IApp app)
			: base(app)
		{ }

		/// <summary>
		/// Clicks the ok.
		/// </summary>
		/// <param name="controller">The controller.</param>
		/// <param name="actionName">Name of the action.</param>
		/// <param name="actionParameters">The action parameters.</param>
		/// <param name="okLink">The ok link.</param>
		/// <returns></returns>
		public IControllerResult ClickOk(Type controller, string actionName, object[] actionParameters, string okLink)
		{
			if (!String.IsNullOrEmpty(okLink))
			{
				return new RedirectResult {EndResponse = false, Url = okLink};
			}
			if (controller != null && !String.IsNullOrEmpty(actionName))
			{
				return new RedirectToActionResult { Controller = controller, ActionName = actionName, ActionParameters = actionParameters};
			}
			return null;
		}

		/// <summary>
		/// Clicks the cancel.
		/// </summary>
		/// <param name="controller">The controller.</param>
		/// <param name="actionName">Name of the action.</param>
		/// <param name="actionParameters">The action parameters.</param>
		/// <param name="cancelLink">The cancel link.</param>
		/// <returns></returns>
		public IControllerResult ClickCancel(Type controller, string actionName, object[] actionParameters, string cancelLink)
		{
			if (!String.IsNullOrEmpty(cancelLink))
			{
				return new RedirectResult { EndResponse = false, Url = cancelLink };
			}
			if (controller != null && !String.IsNullOrEmpty(actionName))
			{
				return new RedirectToActionResult { Controller = controller, ActionName = actionName, ActionParameters = actionParameters};
			}
			return null;
		}
	}
}