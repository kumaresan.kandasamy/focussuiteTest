﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

using Focus.Common.Code;
using Focus.Core;
using Focus.Core.Views;

namespace Focus.Common.Controllers.Base
{
	public abstract class SettingsController : PageControllerBase
	{
		protected SettingsController(IApp app) : base(app){}


		/// <summary>
		/// Determines whether [is hiring manager pending approval] [the specified person id].
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns>
		///   <c>true</c> if [is hiring manager pending approval] [the specified person id]; otherwise, <c>false</c>.
		/// </returns>
		public bool IsHiringManagerPendingApproval(long? personId)
		{
			if (!personId.HasValue)
				return false;

			var status = ServiceClientLocator.EmployeeClient(App).GetEmployeeByPerson(personId.Value).ApprovalStatus;

			return status == ApprovalStatuses.WaitingApproval;
		}

		/// <summary>
		/// Gets the hiring manager approval status.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		public ApprovalStatuses? GetHiringManagerApprovalStatus(long? personId)
		{
			if (!personId.HasValue)
				return null;

			return ServiceClientLocator.EmployeeClient(App).GetEmployeeByPerson(personId.Value).ApprovalStatus;
		}
	}
}