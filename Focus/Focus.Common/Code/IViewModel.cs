﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

using Focus.Common.Code.ControllerResults;

#endregion

namespace Focus.Common.Code
{
	public interface IViewModel
	{
		ValidationControllerResult Validate();
	}

	[DataContract]
	[Serializable]
	public abstract class ViewModelBase : IViewModel
	{
		public ValidationControllerResult Validate()
		{
			var result = new ValidationControllerResult();
			result.Valid = Validator.TryValidateObject(this, new ValidationContext(this, null, null), result.ValidationResults, true);
			return result;
		}
	}
}
