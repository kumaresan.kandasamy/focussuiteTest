﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Common.Code.ControllerResults;

#endregion

namespace Focus.Common.Code.ResultHandlers
{
	public interface IResultHandler
	{
		bool Handle(IControllerResult controllerResult, PageBase page);
	}

	public abstract class ResultHandlerBase : IResultHandler
	{
		public abstract bool Handle(IControllerResult controllerResult, PageBase page);
	}
}
