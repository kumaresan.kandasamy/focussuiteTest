﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Web;
using System.Web.Security;

using Focus.Common.Code.ControllerResults;

using Framework.Core;

#endregion

namespace Focus.Common.Code.ResultHandlers
{
	public class DefaultResultHandler : ResultHandlerBase
	{
		private readonly IApp _app = new HttpApp();

		public override bool Handle(IControllerResult controllerResult, PageBase page)
		{
			if (controllerResult.IsNull()) return false;

			var viewModelResult = controllerResult as ViewModelResult;
			if (viewModelResult != null)
			{
				return false;
			}

			var redirectResult = controllerResult as RedirectResult;
			if (redirectResult.IsNotNull())
			{
				HttpContext.Current.Response.Redirect(redirectResult.Url, redirectResult.EndResponse);
				return !controllerResult.ContinueAfterHandle;
			}

			var loginResult = controllerResult as LoginResult;
			if (loginResult.IsNotNull() && loginResult.Successful)
			{
				var authTicket = new FormsAuthenticationTicket(1, loginResult.UserData.FirstName, DateTime.Now, DateTime.Now.AddMinutes(_app.Settings.UserSessionTimeout), false, loginResult.UserData.SerializeUserContext());
				var encryptedTicket = FormsAuthentication.Encrypt(authTicket);

				var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
				HttpContext.Current.Response.Cookies.Add(authCookie);

				if (loginResult.RedirectUrl != null) HttpContext.Current.Response.Redirect(loginResult.RedirectUrl, false);
				return !controllerResult.ContinueAfterHandle;
			}

			var confirmationModalResult = controllerResult as ConfirmationModalResult;
			if (confirmationModalResult != null && page.Master != null)
			{
				var modal = page.Master.FindControl("MasterBddModal") as BddModalBase;
				if (modal != null)
				{
					modal.Show
						(
							new Dictionary<string, object>
								{
									{"Width", (confirmationModalResult.Width ?? 400).ToString(CultureInfo.InvariantCulture)},
									{"Height", (confirmationModalResult.Height ?? 200).ToString(CultureInfo.InvariantCulture)},
									{"OkText", confirmationModalResult.OkText ?? "OK"},
									{"CancelText", confirmationModalResult.CancelText ?? "Cancel"},
									{"Title", confirmationModalResult.Title},
									{"Details", confirmationModalResult.Details}
								},
							new Dictionary<string, IControllerResult>
								{
									{"OkButtonClick", confirmationModalResult.OkResult},
									{"CancelButtonClick", confirmationModalResult.CancelResult}
								},
							"~/Code/Controls/ModalContents/ConfirmationModalContent.ascx",
							confirmationModalResult.Height ?? 200,
							confirmationModalResult.Width ?? 400,
							confirmationModalResult.Y ?? -1,
              null,
              confirmationModalResult.CancelResult.IsNull()
						);
					return !controllerResult.ContinueAfterHandle;
				}
			}

			var informationModalResult = controllerResult as InformationModalResult;
			if (informationModalResult != null && page.Master != null)
			{
				var modal = page.Master.FindControl("MasterBddModal") as BddModalBase;
				if (modal != null)
				{
					modal.Show
						(
							new Dictionary<string, object>
								{
									{"Width", (informationModalResult.Width ?? 400).ToString(CultureInfo.InvariantCulture)},
									{"Height", (informationModalResult.Height ?? 200).ToString(CultureInfo.InvariantCulture)},
									{"OkText", informationModalResult.OkText ?? "OK"},
									{"Title", informationModalResult.Title},
									{"Details", informationModalResult.Details},
									{"DisplayCloseIcon", informationModalResult.DisplayCloseIcon.ToString(CultureInfo.InvariantCulture)}
								},
							new Dictionary<string, IControllerResult>
								{
									{"OkButtonClick", informationModalResult.OkResult}
								},
							"~/Code/Controls/ModalContents/InformationModalContent.ascx",
							informationModalResult.Height ?? 200,
							informationModalResult.Width ?? 400,
							informationModalResult.Y ?? -1,
							null,
              informationModalResult.OkResult.IsNull()
						);
					return !controllerResult.ContinueAfterHandle;
				}
			}

			var redirectToActionResult = controllerResult as RedirectToActionResult;
			if (redirectToActionResult != null)
			{
				var controller = Activator.CreateInstance(redirectToActionResult.Controller, _app) as IPageController;
				if (controller == null) return false;
				var action = redirectToActionResult.Controller.GetMethod(redirectToActionResult.ActionName);
				var result = action.Invoke(controller, redirectToActionResult.ActionParameters) as IControllerResult;
				if (result != null) return Handle(result, page) && !controllerResult.ContinueAfterHandle;
			}

			var customModalResult = controllerResult as CustomModalResult;
      if (customModalResult != null && page.Master != null)
			{
				if (page.Master != null)
				{
					var modal = page.Master.FindControl("MasterBddModal") as BddModalBase;
					if (modal != null)
					{
						modal.Show
							(
								customModalResult.Properties,
								customModalResult.ButtonActions,
								customModalResult.ModalContentUrl,
								customModalResult.Height ?? 200,
								customModalResult.Width ?? 400,
								customModalResult.Y ?? -1,
								customModalResult.PopupDragHandleControlId
							);
					return !controllerResult.ContinueAfterHandle;
					}
				}
			}

			var validationControllerResult = controllerResult as ValidationControllerResult;
			Debug.Assert(validationControllerResult == null, "An invalid model should not get to this point.  The user should gave been forced to enter a correct value before subitting.");

			var confirmationResult = controllerResult as ConfirmationResult;
			if (confirmationResult != null) return true;

			return false;
		}
	}
}
