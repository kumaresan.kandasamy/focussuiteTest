﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Common.Code.ControllerResults;
using Framework.Core;

#endregion

namespace Focus.Common.Code
{
	public abstract class BddModalBase : UserControlBase
	{
    private const string PropertiesKey = "PropertiesKey";
    private const string ButtonActionsKey = "ButtonActionsKey";
    private const string PropertiesTypesKey = "PropertiesKey:Types";
    private const string ButtonActionsTypesKey = "ButtonActionsKey:Types";

		private Dictionary<string, object> _properties;
		private Dictionary<string, IControllerResult> _buttonActions;

    /// <summary>
    /// Gets or sets the properties.
    /// </summary>
    /// <value>
    /// The properties.
    /// </value>
    public Dictionary<string, object> Properties
    {
      get
      {
        if (_properties.IsNull())
        {
            var knownTypesList = App.GetSessionValue<List<string>>(PropertiesTypesKey);
            /** KRP 15.May.2017 - FVN-4623
            * check if knownTypesList is null
            */
            if (knownTypesList.IsNotNull())
            {
                var knownTypes = knownTypesList.Select(Type.GetType);
                _properties = App.GetSessionValue<Dictionary<string, object>>(PropertiesKey, knownTypes: knownTypes);
            }
        }
        return _properties;
      }
      protected set
      {
        _properties = value;

        if (_properties.IsNull())
        {
          App.SetSessionValue<List<string>>(PropertiesTypesKey, null);
          App.SetSessionValue(PropertiesKey, _properties);
        }
        else
        {
          var knownTypes = _properties.Values.Where(v => v.IsNotNull()).Select(v => v.GetType()).Distinct().ToList();
          var knownTypeNames = knownTypes.Select(t => t.AssemblyQualifiedName).ToList();

          App.SetSessionValue(PropertiesTypesKey, knownTypeNames);
          App.SetSessionValue(PropertiesKey, _properties, knownTypes);
        }
      }
    }

    /// <summary>
    /// Gets or sets the button actions.
    /// </summary>
    /// <value>
    /// The button actions.
    /// </value>
    public Dictionary<string, IControllerResult> ButtonActions
    {
      get
      {
        if (_buttonActions.IsNull())
        {
          var knownTypes = App.GetSessionValue<List<string>>(ButtonActionsTypesKey).Select(Type.GetType);
          _buttonActions = App.GetSessionValue<Dictionary<string, IControllerResult>>(ButtonActionsKey, knownTypes: knownTypes);
        }
        return _buttonActions;
      }
      protected set
      {
        _buttonActions = value;

        if (_buttonActions == null)
        {
          App.SetSessionValue<List<string>>(ButtonActionsTypesKey, null);
          App.SetSessionValue(ButtonActionsKey, _buttonActions);
        }
        else
        {
          var knownTypes = _buttonActions.Values.Where(v => v.IsNotNull()).Select(v => v.GetType()).Distinct().ToList();
          var knownTypeNames = knownTypes.Select(t => t.AssemblyQualifiedName).ToList();

          App.SetSessionValue(ButtonActionsTypesKey, knownTypeNames);
          App.SetSessionValue(ButtonActionsKey, _buttonActions, knownTypes);
        }
      }
    }

		/// <summary>
		/// Shows the specified properties.
		/// </summary>
		/// <param name="properties">The properties.</param>
		/// <param name="buttonActions">The button actions.</param>
		/// <param name="contentUrl">The content URL.</param>
		/// <param name="height">The height.</param>
		/// <param name="width">The width.</param>
		/// <param name="y">The y.</param>
		/// <param name="popupDragHandleControlId">The element that serves as the drag handle element for the modal.</param>
    /// <param name="clientSideCancel">Whether to allow the modal to be closed client side</param>
    public abstract void Show(Dictionary<string, object> properties, Dictionary<string, IControllerResult> buttonActions, string contentUrl, int height, int width, int y, string popupDragHandleControlId, bool clientSideCancel = false);

		/// <summary>
		/// Hides this instance.
		/// </summary>
		public abstract void Hide();
	}
}
