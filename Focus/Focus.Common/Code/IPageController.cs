﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

using System;
using Focus.Common.Localisation;
using Focus.Core;

namespace Focus.Common.Code
{
	public interface IPageController
	{}

	public abstract class PageControllerBase : IPageController
	{
		protected readonly IApp App;

		protected PageControllerBase(IApp app)
		{
			App = app;
		}

		protected string CodeLocalise(string key, string defaultValue, params object[] args)
		{
			return Localiser.Instance(App).Localise(key, defaultValue, args);
		}

		protected string CodeLocalise(Enum enumValue, bool useAnnotation = false)
		{
			return useAnnotation
							? Localiser.Instance().Localise(enumValue)
							: Localiser.Instance().Localise(enumValue, "** " + enumValue + " **");
		}

		protected string FormatError(ErrorTypes errorType, string defaultValue)
		{
			return Localiser.Instance().Localise(errorType, defaultValue);
		}
	}
}
