﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Common.Code.ControllerResults;
using Framework.Core;

#endregion

namespace Focus.Common.Code
{
	interface IBddModalContent
	{
		/// <summary>
		/// Gets the properties.
		/// </summary>
		/// <value>
		/// The properties.
		/// </value>
		Dictionary<string, object> Properties { get; }

		/// <summary>
		/// Gets the button actions.
		/// </summary>
		/// <value>
		/// The button actions.
		/// </value>
		Dictionary<string, IControllerResult> ButtonActions { get; }
    string CancelButtonId { get; set; }

		/// <summary>
		/// Gets the parent modal.
		/// </summary>
		/// <value>
		/// The parent modal.
		/// </value>
		BddModalBase ParentModal { get; }
	}

	public abstract class BddModalContentBase : UserControlBase, IBddModalContent
	{
		private BddModalBase _parentModal;

		/// <summary>
		/// Gets the properties.
		/// </summary>
		/// <value>
		/// The properties.
		/// </value>
		public Dictionary<string, object> Properties { get { return ParentModal.Properties; } }

		/// <summary>
		/// Gets the button actions.
		/// </summary>
		/// <value>
		/// The button actions.
		/// </value>
		public Dictionary<string, IControllerResult> ButtonActions { get { return ParentModal.ButtonActions; } }
    public string CancelButtonId { get; set; }

		/// <summary>
		/// Gets the parent modal.
		/// </summary>
		/// <value>
		/// The parent modal.
		/// </value>
		public BddModalBase ParentModal{ get { return _parentModal ?? Parent.Parent.Parent as BddModalBase; } }

		/// <summary>
		/// Gets the property.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public T GetProperty<T>(string key)
		{
            /** KRP 15.May.2017 - FVN-4623
            * check if Properties is null
            */
		    if (Properties.IsNotNull())
		    {
                object value; 
                if (Properties.TryGetValue(key, out value)) return (T)value;
		    }
		    return default(T);
		}

		/// <summary>
		/// Handles the specified controller result.
		/// </summary>
		/// <param name="controllerResult">The controller result.</param>
		/// <returns></returns>
		/// B
		protected override bool Handle(IControllerResult controllerResult)
		{
			// Get a local copy of the parent modal before handling a result.
			_parentModal = ParentModal;
			return base.Handle(controllerResult);
		}
	}
}
