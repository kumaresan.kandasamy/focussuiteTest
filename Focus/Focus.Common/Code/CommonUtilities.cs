﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Common.Localisation;
using Focus.Core;
using Focus.Core.Models.Career;
using Framework.Core;

#endregion

namespace Focus.Common.Code
{
	public class CommonUtilities
	{
		private static IApp _app;

		static CommonUtilities()
		{
			if (_app.IsNull())
				_app = new HttpApp();
		}

		public CommonUtilities(IApp app = null)
		{
			_app = app ?? new HttpApp();
		}

		/// <summary>
		/// Gets the criteria.
		/// </summary>
		/// <param name="searchcriteria">The searchcriteria.</param>
		/// <returns></returns>
		public static string GetCriteria(SearchCriteria searchcriteria)
		{
			var localiser = Localiser.Instance();

			var outCriteria = new StringBuilder();

			if (searchcriteria.IsNotNull())
			{
				var criteria = searchcriteria;

				if (criteria.JobIdCriteria.IsNotNull())
				{

					#region Job Id

					outCriteria.Append("<b>Job Id:</b> " + criteria.JobIdCriteria + " <br/>");

					#endregion

				}
				else
				{

					#region Required Result  Criteria

					if (criteria.RequiredResultCriteria.IsNotNull())
					{
						var tempCriterion = criteria.RequiredResultCriteria;

						if (tempCriterion.MinimumStarMatch.IsNotNull())
						{
							string starMatch;

							switch (tempCriterion.MinimumStarMatch)
							{
								case StarMatching.FiveStar:
									starMatch = "5 Star";
									break;
								case StarMatching.FourStar:
									starMatch = "4 Star";
									break;
								case StarMatching.ThreeStar:
									starMatch = "3 Star";
									break;
								case StarMatching.TwoStar:
									starMatch = "2 Star";
									break;
								case StarMatching.OneStar:
									starMatch = "1 Star";
									break;
								default:
									starMatch = "0 Star";
									break;
							}

							if (!_app.Settings.CareerHideSearchJobsByResume)
							{
								if (!string.IsNullOrEmpty(starMatch))
									outCriteria.Append("Matching your resume with at least a: " + starMatch + "<br/>");
								else
									outCriteria.Append("All jobs, without matching your resume <br/>");
							}
						}
					}

					#endregion

					#region Job Location Criteria

					if (criteria.JobLocationCriteria.IsNotNull())
					{
						var area = criteria.JobLocationCriteria.Area;
						var radius = criteria.JobLocationCriteria.Radius;

						if (radius.IsNotNull())
							outCriteria.Append(string.Format("<b>Radius:</b> {0} {1}, ZIP code: {2}", radius.Distance, radius.DistanceUnits, radius.PostalCode) + "<br/>");
						else if (area.IsNotNull())
						{
							var msaLookup = (area.MsaId.HasValue) ? ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.StateMetropolitanStatisticalAreas).FirstOrDefault(x => x.Id == area.MsaId) : null;
							var stateLookup = (area.StateId.HasValue) ? ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.States).FirstOrDefault(x => x.Id == area.StateId) : null;

							if (msaLookup.IsNotNull() && stateLookup.IsNotNull())
								outCriteria.AppendFormat("<b>Location:</b> {0}, {1}<br/>", msaLookup.Text, stateLookup.Text);
							else if (msaLookup.IsNotNull())
								outCriteria.AppendFormat("<b>Location:</b> {0}<br/>", msaLookup.Text);
							else if (stateLookup.IsNotNull())
								outCriteria.AppendFormat("<b>Location:</b> {0}<br/>", stateLookup.Text);
						}

						if (criteria.JobLocationCriteria.OnlyShowJobInMyState && area.IsNotNull() && area.StateId.IsNotNull() && area.StateId > 0)
							outCriteria.Append("Only show in-state (" + ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.States).Where(x => x.Id == area.StateId).Select(x => x.Text).FirstOrDefault() + ") jobs<br/>");

					}

					#endregion

					#region Keyword Criteria

					if (criteria.KeywordCriteria.IsNotNull())
					{
						var kCriteria = criteria.KeywordCriteria;
						outCriteria.Append(string.Format("<b>Key words:</b> {0}", kCriteria.KeywordText));

						outCriteria.Append(" (");
						outCriteria.Append(kCriteria.Operator == LogicalOperators.Or
							? localiser.Localise("IncludeAny.Text", "Include any words")
							: Localiser.Instance().Localise("IncludeAll.Text", "Include all words"));

						outCriteria.Append(")<br />");
					}

					#endregion

					#region Posting Age Criteria

					if (criteria.PostingAgeCriteria.IsNotNull())
					{
						if (criteria.PostingAgeCriteria.PostingAgeInDays.IsNotNull())
						{
							if (criteria.PostingAgeCriteria.PostingAgeInDays == 1)
								outCriteria.Append("<b>Job posted:</b> Most recent a day " + "<br/>");
							else if (criteria.PostingAgeCriteria.PostingAgeInDays == 0)
								outCriteria.Append("<b>Job posted:</b> More than 30 days " + "<br/>");
							else
								outCriteria.Append("<b>Job posted:</b> Most recent " + criteria.PostingAgeCriteria.PostingAgeInDays + " days " + "<br/>");
						}
					}

					#endregion

					#region Education Level Criteria

					if (criteria.EducationLevelCriteria.IsNotNull())
					{
						var educationList = new List<string>();

						foreach (var educationLevelId in criteria.EducationLevelCriteria.RequiredEducationIds)
						{
							var lookup = ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.RequiredEducationLevels).FirstOrDefault(x => x.Id == educationLevelId);
							if (lookup.IsNotNull())
								educationList.Add(lookup.Text);
						}

						if (educationList.IsNotNullOrEmpty())
							outCriteria.Append("<b>Education level</b>: " + string.Join(", ", educationList.ToArray()) + "<br/>");

						if (criteria.EducationLevelCriteria.IncludeJobsWithoutEducationRequirement)
							outCriteria.Append("Show jobs without education information" + "<br/>");
					}

					#endregion

					#region Salary Level Criteria

					if (criteria.SalaryCriteria.IsNotNull())
					{
						var salCriteria = criteria.SalaryCriteria;

						if (salCriteria.IncludeJobsWithoutSalaryInformation)
							outCriteria.Append("Show jobs without salary information" + "<br/>");

						if (salCriteria.MinimumSalary > 0)
							outCriteria.Append(string.Format("<b>Minimum salary:</b> {0:C}", salCriteria.MinimumSalary) + "<br/>");
					}

					#endregion

					#region Internship Criteria

					if (criteria.InternshipCriteria.IsNotNull())
					{
						var tempIntenCriterion = criteria.InternshipCriteria;

						if (tempIntenCriterion.Internship.IsNotNull())
						{
							if (OldApp_RefactorIfFound.Settings.Theme == FocusThemes.Workforce)
							{
								switch (tempIntenCriterion.Internship)
								{
									case FilterTypes.ShowOnly:
										outCriteria.Append(localiser.Localise("InternshipFilter.ShowOnlyText", "<b>Internships</b>: Search only for internships<br/>"));
										break;
									case FilterTypes.Exclude:
										outCriteria.Append(localiser.Localise("InternshipFilter.ExcludeText", "<b>Internships</b>: Exclude internships<br/>"));
										break;
									case FilterTypes.NoFilter:
										outCriteria.Append(localiser.Localise("InternshipFilter.NoFilter", "<b>Internships</b>: Search for all jobs (including internships)<br/>"));
										break;
								}
							}
							else
							{
								outCriteria.Append(localiser.Localise("JobTypes.Text", "<b>Job types</b>: "));

								switch (tempIntenCriterion.Internship)
								{
									case FilterTypes.Exclude:
										outCriteria.Append(localiser.Localise("Jobs.Text", "Jobs<br/>"));
										break;

									case FilterTypes.ShowOnly:
										outCriteria.Append(localiser.Localise("Internships.Text", "Internships<br/>"));
										break;

									case FilterTypes.NoFilter:
										outCriteria.Append(localiser.Localise("JobsAndInternships.Text", "Jobs and Internships<br/>"));
										break;
								}
							}
						}

						if (tempIntenCriterion.InternshipCategories.IsNotNullOrEmpty())
						{
							var internshipCategories = ServiceClientLocator.ExplorerClient(_app).GetInternshipCategories(tempIntenCriterion.InternshipCategories);

							var internshipCategoryNames = new StringBuilder("");

							foreach (var internshipCategory in internshipCategories)
							{
								internshipCategoryNames.AppendFormat("{0}, ", internshipCategory.Name);
							}

							if (internshipCategoryNames.Length > 2)
								outCriteria.Append(localiser.Localise("InternshipCategories.Text", "<b>Internship Categories</b>: {0} <br/>", internshipCategoryNames.ToString().Substring(0, internshipCategoryNames.Length - 2)));
						}
					}

					#endregion

					#region Home Based Jobs Criteria

					if (criteria.HomeBasedJobsCriteria.IsNotNull())
					{
						var tempHomeBasedCriterion = criteria.HomeBasedJobsCriteria;

						if (tempHomeBasedCriterion.HomeBasedJobs.IsNotNull())
						{
							if (OldApp_RefactorIfFound.Settings.Theme == FocusThemes.Workforce)
							{
								switch (tempHomeBasedCriterion.HomeBasedJobs)
								{
									case FilterTypes.ShowOnly:
										outCriteria.Append(localiser.Localise("HomeBasedJobsFilter.ShowOnlyText", "<b>Home Based Jobs</b>: Search only for Home-based jobs<br/>"));
										break;
									case FilterTypes.Exclude:
										outCriteria.Append(localiser.Localise("HomeBasedJobsFilter.ExcludeText", "<b>Home Based Jobs</b>: Exclude Home-based jobs<br/>"));
										break;
									case FilterTypes.NoFilter:
										outCriteria.Append(localiser.Localise("HomeBasedJobsFilter.NoFilter", "<b>Home Based Jobs</b>: Search for all jobs (including home-based jobs)<br/>"));
										break;
								}
							}
						}
					}

					#endregion

					#region Commission Based Jobs Criteria

					if (criteria.CommissionBasedJobsCriteria.IsNotNull())
					{
						var tempCommissionBasedCriterion = criteria.CommissionBasedJobsCriteria;

						if (tempCommissionBasedCriterion.CommissionBasedJobs.IsNotNull())
						{
							if (OldApp_RefactorIfFound.Settings.Theme == FocusThemes.Workforce)
							{
								switch (tempCommissionBasedCriterion.CommissionBasedJobs)
								{
									case FilterTypes.ShowOnly:
										outCriteria.Append(localiser.Localise("CommissionBasedJobsFilter.ShowOnlyText", "<b>Commission-only jobs</b>: Search only for Commission-only jobs<br/>"));
										break;
									case FilterTypes.Exclude:
										outCriteria.Append(localiser.Localise("CommissionBasedJobsFilter.ExcludeText", "<b>Commission-only jobs</b>: Exclude Commission-only jobs<br/>"));
										break;
									case FilterTypes.NoFilter:
										outCriteria.Append(localiser.Localise("CommissionBasedJobsFilter.NoFilter", "<b>Commission-only jobs</b>: Search for all jobs (including Commission-only jobs)<br/>"));
										break;
								}
							}
						}
					}

					#endregion

					#region Salary And Commission Based Jobs Criteria

					if (criteria.SalaryAndCommissionBasedJobsCriteria.IsNotNull())
					{
						var tempSalaryAndCommissionBasedCriterion = criteria.SalaryAndCommissionBasedJobsCriteria;

						if (tempSalaryAndCommissionBasedCriterion.SalaryAndCommissionBasedJobs.IsNotNull())
						{
							if (OldApp_RefactorIfFound.Settings.Theme == FocusThemes.Workforce)
							{
								switch (tempSalaryAndCommissionBasedCriterion.SalaryAndCommissionBasedJobs)
								{
									case FilterTypes.ShowOnly:
										outCriteria.Append(localiser.Localise("SalaryAndCommissionBasedJobsFilter.ShowOnlyText", "<b>Commission + salary jobs</b>: Search only for Commission + salary jobs jobs<br/>"));
										break;
									case FilterTypes.Exclude:
										outCriteria.Append(localiser.Localise("SalaryAndCommissionBasedJobsFilter.ExcludeText", "<b>Commission + salary jobs</b>: Exclude Commission + salary jobs jobs<br/>"));
										break;
									case FilterTypes.NoFilter:
										outCriteria.Append(localiser.Localise("SalaryAndCommissionBasedJobsFilter.NoFilter", "<b>Commission + salary jobs</b>: Search for all jobs (including Commission + salary jobs jobs)<br/>"));
										break;
								}
							}
						}
					}

					#endregion

					#region Program Of Study Criteria

					if (criteria.EducationProgramOfStudyCriteria.IsNotNull())
					{
						string programOfStudy;

						if (criteria.EducationProgramOfStudyCriteria.DegreeEducationLevelId.HasValue && criteria.EducationProgramOfStudyCriteria.DegreeEducationLevelId > 0)
							programOfStudy = ServiceClientLocator.ExplorerClient(_app).GetDegreeEducationLevel(criteria.EducationProgramOfStudyCriteria.DegreeEducationLevelId.Value).DegreeEducationLevelName;
						else if (criteria.EducationProgramOfStudyCriteria.ProgramAreaId.HasValue)
						{
							programOfStudy = localiser.Localise("AllDegreesForProgramAre.Text", "{0} - All degrees", ServiceClientLocator.ExplorerClient(_app).GetProgramArea(criteria.EducationProgramOfStudyCriteria.ProgramAreaId.Value).Name);
						}
						else
							programOfStudy = localiser.Localise("AllDegrees.Text", "All degrees");

						outCriteria.Append(string.Format("<b>{0}</b>: {1}<br/>", localiser.Localise("ProgramOfStudy.Text", "Program of study"), programOfStudy));
					}

					#endregion

					#region Occupation and Industry Criteria

					if (criteria.OccupationCriteria.IsNotNull())
					{
						if (criteria.OccupationCriteria.JobFamilyId != null && criteria.OccupationCriteria.JobFamilyId > 0)
						{
							var jobFamilyId = criteria.OccupationCriteria.JobFamilyId;
							var jobFamilyLookup = ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.JobFamily).FirstOrDefault(x => x.Id == jobFamilyId);
							if (jobFamilyLookup.IsNotNull())
								outCriteria.Append("<b>Job family</b>: " + jobFamilyLookup.Text + "<br/>");
						}

						if (criteria.OccupationCriteria.OccupationIds.IsNotNullOrEmpty())
						{
							var occupationId = criteria.OccupationCriteria.OccupationIds[0];
							if (occupationId.IsNotNull() && occupationId > 0)
							{
								var occupation = ServiceClientLocator.SearchClient(_app).GetOnets(occupationId, null, 1);
								if (occupation.IsNotNullOrEmpty())
								{
									outCriteria.Append("<b>Occupation</b>: " + occupation[0].Occupation + "<br/>");
								}
							}
						}
					}

					if (criteria.IndustryCriteria.IsNotNull())
					{
						if (criteria.IndustryCriteria.IndustryId.HasValue && criteria.IndustryCriteria.IndustryId > 0)
						{
							var industry = ServiceClientLocator.CoreClient(_app).GetIndustryClassification(criteria.IndustryCriteria.IndustryId.Value);
							outCriteria.Append("<b>Industry</b>: " + industry.Name + "<br/>");
						}

						if (criteria.IndustryCriteria.IndustryDetailIds.IsNotNull() && criteria.IndustryCriteria.IndustryDetailIds.Count() == 1 && criteria.IndustryCriteria.IndustryDetailIds[0] > 0)
						{
							var industryDetails = ServiceClientLocator.CoreClient(_app).GetIndustryClassification(criteria.IndustryCriteria.IndustryDetailIds[0]);
							outCriteria.Append("<b>Industry details</b>: " + industryDetails.Name + "<br/>");
						}
					}

					#endregion

					#region Emerging Criteria

					if (criteria.JobSectorCriteria.IsNotNull())
					{
						if (criteria.JobSectorCriteria.RequiredJobSectors.IsNotNullOrEmpty())
							outCriteria.Append("<b>Emerging sectors</b>: " + criteria.JobSectorCriteria.RequiredJobSectors + "<br/>");
					}

					#endregion

					#region Physical abilities Criteria

					if (criteria.PhysicalAbilityCriteria.IsNotNull())
					{
						if (criteria.PhysicalAbilityCriteria.PhysicalAbilities.IsNotNullOrEmpty())
							outCriteria.Append("<b>Physical ability</b>: " + criteria.PhysicalAbilityCriteria.PhysicalAbilities + "<br/>");
					}

					#endregion

					#region Reference Document

					if (criteria.ReferenceDocumentCriteria.IsNotNull())
					{
						var refDocCriteria = criteria.ReferenceDocumentCriteria;

						if (refDocCriteria.IsNotNull())
						{
							if (refDocCriteria.DocumentType == DocumentType.Posting)
								outCriteria.Append(string.Format("All Jobs, matching the {0} ({1})", refDocCriteria.DocumentType.ToString().ToLower(), refDocCriteria.DocumentId) + "<br/>");
							if (refDocCriteria.DocumentType == DocumentType.Resume && OldApp_RefactorIfFound.Settings.Theme == FocusThemes.Education)
								outCriteria.Append(string.Format("All Jobs, matching the {0}", refDocCriteria.DocumentType.ToString().ToLower()) + "<br/>");
						}

						var excludejob = refDocCriteria.ExcludedJobs;
						if (excludejob.IsNotNull() && excludejob.Count > 0)
						{
							outCriteria.Append("<b>Exculded job(s): </b><br/>");
							foreach (Job job in excludejob)
								outCriteria.Append(job.Description + "<br/>");
						}
					}

					#endregion

					#region Exclude Job Criteria

					else if (criteria.ReferenceDocumentCriteria.IsNotNull())
					{
						var refDocCriteria = criteria.ReferenceDocumentCriteria;

						if (refDocCriteria.IsNotNull())
						{
							if (refDocCriteria.DocumentType == DocumentType.Posting)
								outCriteria.Append(string.Format("All Jobs, matching the {0} ({1})", refDocCriteria.DocumentType.ToString().ToLower(), refDocCriteria.DocumentId) + "<br/>");
							if (refDocCriteria.DocumentType == DocumentType.Resume)
								outCriteria.Append(string.Format("All Jobs, matching the {0}", refDocCriteria.DocumentType.ToString().ToLower()) + "<br/>");
						}
					}

					#endregion

					#region ROnet Criteria

					if (criteria.ROnetCriteria.IsNotNull())
					{
						if (criteria.ROnetCriteria.CareerAreaId.IsNotNull())
						{
							var careerAreaName = ServiceClientLocator.CoreClient(_app).GetCareerAreaName(Convert.ToInt64(criteria.ROnetCriteria.CareerAreaId));

							outCriteria.Append("<b>Job family</b>: " + careerAreaName + "<br/>");
						}

						if (criteria.ROnetCriteria.ROnets.IsNotNullOrEmpty() && !criteria.ROnetCriteria.IsAllROnets)
						{
							var jobTitles = new StringBuilder();

							foreach (var roNet in criteria.ROnetCriteria.ROnets)
							{
								if (jobTitles.Length > 0)
									jobTitles.Append(", ");

								jobTitles.Append(ServiceClientLocator.CoreClient(_app).GetJobNameFromROnet(roNet));
							}

							outCriteria.Append(string.Format("<b>{0}</b>: {1}<br/>", localiser.Localise("Occupation.Text", "Occupation"), jobTitles.ToString()));
						}
					}

					#endregion

					#region Work Availability

					var workDaysCriteria = criteria.WorkDaysCriteria;
					if (workDaysCriteria.IsNotNull() && (workDaysCriteria.WorkDays.GetValueOrDefault(DaysOfWeek.None) != DaysOfWeek.None || workDaysCriteria.WeekDayVaries))
					{
						outCriteria.Append("<b>").Append(localiser.Localise("NormalWorkDay.Text", "Normal work days")).Append("</b>:");

						var workDays = workDaysCriteria.WorkDays.GetValueOrDefault(DaysOfWeek.None);
						var separator = " ";

						if ((workDays & DaysOfWeek.Weekdays) == DaysOfWeek.Weekdays)
						{
							outCriteria.Append(separator).Append(localiser.Localise("Weekdays.All.Text", "Weekday"));
							separator = ", ";
						}
						else
						{
							var days = new List<DaysOfWeek> { DaysOfWeek.Monday, DaysOfWeek.Tuesday, DaysOfWeek.Wednesday, DaysOfWeek.Thursday, DaysOfWeek.Friday };
							days.ForEach(workDay =>
							{
								if ((workDays & workDay) == workDay)
								{
									outCriteria.Append(separator)
										.Append(localiser.Localise(string.Format("Weekdays.{0}.ShortText", workDay.ToString()), workDay.ToString().Substring(0, 3)));
									separator = ", ";
								}
							});
						}


						if ((workDays & DaysOfWeek.Weekends) == DaysOfWeek.Weekends)
						{
							outCriteria.Append(separator).Append(localiser.Localise("Weekends.Text", "Weekend"));
							separator = ", ";
						}
						else
						{
							var days = new List<DaysOfWeek> { DaysOfWeek.Saturday, DaysOfWeek.Sunday };
							days.ForEach(workDay =>
							{
								if ((workDays & workDay) == workDay)
								{
									outCriteria.Append(separator)
										.Append(localiser.Localise(string.Format("Weekdays.{0}.ShortText", workDay.ToString()), workDay.ToString().Substring(0, 3)));
									separator = ", ";
								}
							});
						}

						if (workDaysCriteria.WeekDayVaries)
						{
							outCriteria.Append(separator)
								.Append(localiser.Localise("Varies.Text", "Varies"));
						}

						outCriteria.Append("<br/>");
					}

					if (workDaysCriteria.IsNotNull() && workDaysCriteria.WorkWeek.IsNotNullOrZero())
					{
						var workWeekId = workDaysCriteria.WorkWeek.AsLong();
						var workWeekLookup = ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.EmploymentStatuses).FirstOrDefault(x => x.Id == workWeekId);
						if (workWeekLookup.IsNotNull())
						{
							outCriteria.Append("<b>").Append(localiser.Localise("NormalWorkWeek.Text", "Hours")).Append("</b>: ");
							outCriteria.Append(workWeekLookup.Text);
							outCriteria.Append("<br />");
						}
					}

					#endregion

					#region Language Proficiencies

					if (criteria.LanguageCriteria.IsNotNull() && criteria.LanguageCriteria.LanguagesWithProficiencies.Count > 0)
					{
						var languageProficiencies = ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.LanguageProficiencies).ToDictionary(l => l.Id, l => l.Text);

						outCriteria.AppendFormat("<b>{0}:</b> {1}", localiser.Localise("SearchCriteriaLanguages.Label", "Languages"),
							string.Join(
								string.Format(" {0} ",
									criteria.LanguageCriteria.LanguageSearchType
										? localiser.Localise("Global.And.Text", "and")
										: localiser.Localise("Global.Or.Text", "or")),
								(criteria.LanguageCriteria.LanguagesWithProficiencies.Select(
									l => l.Language.ToString() +
											 (l.Proficiency != 0
												 ? l.Proficiency != languageProficiencies.Keys.Min()
													 ? localiser.Localise("OrBelow.Text", " ({0} or below)",
														 languageProficiencies[l.Proficiency].ToString())
													 : string.Format(" ({0})", languageProficiencies[l.Proficiency].ToString())
												 : localiser.Localise("ResumeSearchLanguageProficiencies.TopDefault", " ({0})",
													 "any proficiency"))).ToArray()))
							);

						outCriteria.Append("<br/>");
					}

					#endregion

				}
			}

			return outCriteria.ToString();
		}

		/// <summary>
		/// Popuates an education level drop down
		/// </summary>
		/// <param name="educationLevelDropDown">The drop-down to populate</param>
		public static void GetEducationLevelDropDownList(DropDownList educationLevelDropDown)
		{
			educationLevelDropDown.Items.Clear();

            if (_app.Settings.IsComptiaSpecific)
            {
                educationLevelDropDown.Items.AddEnum(EducationLevel.Doctorate_Degree_26, "Doctorate Degree");
                educationLevelDropDown.Items.AddEnum(EducationLevel.Masters_Degree_25, "Master's Degree");
                educationLevelDropDown.Items.AddEnum(EducationLevel.Bachelors_OR_Equivalent_24, "Bachelor’s Degree");
                educationLevelDropDown.Items.AddEnum(EducationLevel.Associates_Degree_32, "Associate’s Degree ");
                educationLevelDropDown.Items.AddEnum(EducationLevel.HS_3_Year_Vocational_Degree_20, "Vocational Degree");
                educationLevelDropDown.Items.AddEnum(EducationLevel.College_Courses_Completed_33, "Some College – no degree");
                educationLevelDropDown.Items.AddEnum(EducationLevels.HighSchoolDiploma, "High School Diploma");
                educationLevelDropDown.Items.AddEnum(EducationLevels.HighSchoolDiplomaOrEquivalent, "High School Equivalency Diploma");

                educationLevelDropDown.Items.AddLocalisedTopDefault("EducationLevel.TopDefault", "- select education level -");

            }
            else
            {
                if (!_app.Settings.ShowLocalisedEduLevels)
                {
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Doctorate_Degree_26);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Masters_Degree_25);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Bachelors_OR_Equivalent_24);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.HS_3_Year_Associates_Degree_23);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.HS_2_Year_Associates_Degree_22);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.HS_1_Year_Associates_Degree_21);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.HS_3_Year_Vocational_Degree_20);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.HS_2_Year_Vocational_Degree_19);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.HS_1_Year_Vocational_Degree_18);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.HS_3_Year_College_OR_VOC_Tech_No_Degree_17);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.HS_2_Year_College_OR_VOC_Tech_No_Degree_16);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.HS_1_Year_College_OR_VOC_Tech_No_Degree_15);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Disabled_w_Cert_IEP_14);
                    if (_app.Settings.Theme == FocusThemes.Education)
                        educationLevelDropDown.Items.AddEnum(EducationLevel.GED_88, "General Education Diploma (GED)");
                    else
                        educationLevelDropDown.Items.AddEnum(EducationLevel.GED_88);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Grade_12_HS_Graduate_13);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Grade_12_No_Diploma_12);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Grade_11_11);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Grade_10_10);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Grade_9_09);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Grade_8_08);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Grade_7_07);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Grade_6_06);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Grade_5_05);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Grade_4_04);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Grade_3_03);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Grade_2_02);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Grade_1_01);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.No_Grade_00);

                    educationLevelDropDown.Items.AddLocalisedTopDefault("EducationLevel.TopDefault", "- select education level -");
                }
                else
                {
                    educationLevelDropDown.Items.AddEnum(EducationLevel.High_School_Diploma_OR_Equivalent_30);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Certificate_Vocational_Program_31);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.College_Courses_Completed_33);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Associates_Degree_32);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Bachelors_OR_Equivalent_24, "Bachelor's Degree");
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Masters_Degree_25, "Master's Degree");
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Doctorate_Degree_26, "Doctorate");
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Professional_Doctorate_34);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.College_Courses_Completed_Beyond_Highest_Degree_35);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Professional_Development_36);
                    educationLevelDropDown.Items.AddEnum(EducationLevel.Other_37);

                    educationLevelDropDown.Items.AddLocalisedTopDefault("EducationLevel.TopDefault", "- select -");
                }
            }
		}

		/// <summary>
		/// Popuates an employment status drop down
		/// </summary>
		/// <param name="employmentStatuslDropDown">The drop-down to populate</param>
		public static void GetEmploymentStatusDropDownList(DropDownList employmentStatuslDropDown)
		{
			employmentStatuslDropDown.Items.Clear();

			employmentStatuslDropDown.Items.AddEnum(EmploymentStatus.Employed, "Employed");
			employmentStatuslDropDown.Items.AddEnum(EmploymentStatus.EmployedTerminationReceived, "Employed - Rcvd Notice of Termination");
			employmentStatuslDropDown.Items.AddEnum(EmploymentStatus.UnEmployed, "Not Employed");

			employmentStatuslDropDown.Items.AddLocalisedTopDefault("EmploymentStatus.TopDefault", "- select status -");
		}

		public static string WordReplace(string text, IEnumerable<string> words, string prefix, string suffix)
		{
            //FVN-6443
            text = text.Replace("<", " <").Replace(">", "> ");

			// This regex handles any symbols in the profanity word as they need to be matched literally.
			var symbolRegex = new Regex(@"(\W)");
			var formatTextWithPlaceHolders = string.Concat("$1", prefix, "$2", suffix, "$3");

			var replaceWords = (from word in words
													select symbolRegex.Replace(word, @"\$1") into w
													select w.Replace(@"\*", @"\S*") into w
													select string.Concat(@"(\W)(", w, @")(\W)") into w
													select new Regex(w, RegexOptions.IgnoreCase));

			return replaceWords.Aggregate(text, (current, wordRegex) => wordRegex.Replace(current, formatTextWithPlaceHolders));
		}
	}
}
