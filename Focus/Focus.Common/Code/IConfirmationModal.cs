﻿namespace Focus.Common.Code
{
	public interface IConfirmationModal
	{
    void Show(string title, string details, string closeText, string closeCommandName = "", string closeLink = "", string okText = null, string okCommandName = "", string okCommandArgument = "", int height = 200, int width = 400, int y = -1);
	}
}
