﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

namespace Focus.Common.Code.ControllerResults
{
	public interface IControllerResult
	{
		string ErrorMessage { get; set; }
		bool ContinueAfterHandle { get; set; }
	}

	public abstract class ControllerResultBase : IControllerResult
	{
		public string ErrorMessage { get; set; }
		public bool ContinueAfterHandle { get; set; }
	}
}
