﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using directives

using System.Collections.Generic;

#endregion

namespace Focus.Common.Code.ControllerResults
{
	public class CustomModalResult : ControllerResultBase
	{
		public Dictionary<string, object> Properties { get; set; }
		public Dictionary<string, IControllerResult> ButtonActions { get; set; }
		public string ModalContentUrl { get; set; }
		public int? Height { get; set; }
		public int? Width { get; set; }
		public int? Y { get; set; }
		public string PopupDragHandleControlId { get; set; }
	}
}
