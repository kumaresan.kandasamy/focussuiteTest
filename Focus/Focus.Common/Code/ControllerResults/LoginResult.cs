﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

using Focus.Core.Models;

namespace Focus.Common.Code.ControllerResults
{
	public class LoginResult : ControllerResultBase
	{
		public string RedirectUrl { get; set; }
		public bool Successful { get; set; }
		public UserContext UserData { get; set; }
	}
}
