﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

namespace Focus.Common.Code.ControllerResults
{
	public class InformationModalResult : ControllerResultBase
	{
		public string Title { get; set; }
		public string Details { get; set; }
		public string OkText { get; set; }
		public IControllerResult OkResult { get; set; }
		public int? Height { get; set; }
		public int? Width { get; set; }
		public int? Y { get; set; }
		public bool DisplayCloseIcon { get; set; }
	}
}
