﻿/*! Copyright © 2000-2014 Burning Glass International Inc.
*
* Proprietary and Confidential
*
* All rights are reserved. Reproduction or transmission in whole or in part, in
* any form or by any means, electronic, mechanical or otherwise, is prohibited
* without the prior written consent of Burning Glass International Inc. */

// ReSharper disable UnusedParameter
(function (focusSuite, undefined) {
// ReSharper restore UnusedParameter

    /* Initialises the focusSuite global object
    *
    * Note: must be included BEFORE anything that references it!
    */

    //##GLOBALS##

} (window.focusSuite = window.focusSuite || {}));