﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using Microsoft.Ajax.Utilities;

#endregion

namespace Focus.Common.HttpHandlers
{
	public class ClientScriptInitialisationHandler : IHttpHandler
	{
		#region IHttpHandler Members

		public bool IsReusable
		{
			// return true as no state information is being used by this handler
			get { return true; }
		}

		public void ProcessRequest(HttpContext context)
		{
			// cache keys that we will be using
			// the key for the cache item that holds the actual javascript to output
			const string scriptCacheKey = "globalsScript";
			// the key for the cache item that holds the timestamp for when the javascript was created - used to create a Last-Modified header for the purposes of browser caching
			const string scriptLastModifiedCacheKey = "globalsScriptLastModified";
			// specify how many days the generated javascript should remain in server and browser cache
			const int cacheExpireDays = 30;

			// attempt to get the javascript from server cache
			var globalsScript = context.Cache[scriptCacheKey];

			if (globalsScript == null) // built JS not in cache so need to build
			{
				// get the "template" js file, which is an embedded resource within this dll
				var assembly = Assembly.GetExecutingAssembly();
				const string resourceName = "Focus.Common.HttpHandlers.ClientScriptGlobalsTemplate.js";
				string globalsScriptTemplate;
				using (var stream = assembly.GetManifestResourceStream(resourceName))
				using (var reader = new StreamReader(stream))
				{
					globalsScriptTemplate = reader.ReadToEnd();
				}

				// use the service locator to get the app settings we're going to use to create the global variables
				var app = new HttpApp();
				var appSettings = app.Settings;

				// create a dictionary of the values we're going to make available clientside
				var globalVariables = new Dictionary<string, Tuple<object, Type>>
					                      {
						                      {
							                      "brandIdentifier",
							                      new Tuple<object, Type>(appSettings.BrandIdentifier, typeof (string))
						                      },
						                      {
							                      "applicationBasePath",
							                      new Tuple<object, Type>(context.Request.ApplicationPath.EndsWith("/") ? context.Request.ApplicationPath : context.Request.ApplicationPath + "/", typeof (string))
						                      },
						                      {
							                      "theme",
																		new Tuple<object, Type>(app.Settings.Theme.ToString().ToLower(), typeof(string))
						                      }
					                      };

				// create a string containing each of the above values in the format prudence.<variable name> = <value>;
				var globalVariableString = "";
				// ReSharper disable ReturnValueOfPureMethodIsNotUsed
				globalVariables.Keys.All(
					a => { globalVariableString += GetVariableString(a, globalVariables[a].Item1, globalVariables[a].Item2); return true; });
				// ReSharper restore ReturnValueOfPureMethodIsNotUsed

				// merge the variables into the template by simply replacing the //##GLOBALS## placeholder
				var output = globalsScriptTemplate.Replace("//##GLOBALS##", globalVariableString);

				if (!context.IsDebuggingEnabled)
				{
					/*
					 * Debugging is not enabled so minify the generated javascript for speed of transfer over the wire.
					 * Using the minifier in Microsoft.Ajax.Utilities, which is the same as that used under the hood by the default MVC bundling and minification functionality.
					 */
					var minnie = new Minifier();
					var codeSettings = new CodeSettings { EvalTreatment = EvalTreatment.MakeImmediateSafe, PreserveImportantComments = true, PreserveFunctionNames = true, LocalRenaming = LocalRenaming.KeepAll };
					output = minnie.MinifyJavaScript(output, codeSettings);
				}

				/* 
				 * Lump the generated javascript into server cache, together with a separate cache item containing a timestamp of when it was generated.
				 * The timestamp is used for the purposes of setting a Last-Modified header on the output to facilitate browser caching.
				 * Setting an arbitrary 30 day expiration on the cached items; the ultimate cache dependency is web.config (as referenced by the app settings) 
				 * so the items will be removed from cache when web.config changes or the application restarts eg. app pool recycles.
				 */
				context.Cache.Add(scriptCacheKey, output, null, DateTime.Now.AddDays(cacheExpireDays), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
				context.Cache.Add(scriptLastModifiedCacheKey, DateTime.Now, null, DateTime.Now.AddDays(cacheExpireDays), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
			}

			// set the caching headers in the response
			context.Response.Cache.SetCacheability(HttpCacheability.Public);
			context.Response.Cache.SetMaxAge(new TimeSpan(cacheExpireDays, 0, 0, 0));
			string rawIfModifiedSince = context.Request.Headers.Get("If-Modified-Since");
			var lastModifiedDateTime = (DateTime)context.Cache[scriptLastModifiedCacheKey];
			if (string.IsNullOrEmpty(rawIfModifiedSince))
			{
				// set Last Modified time
				context.Response.Cache.SetLastModified(lastModifiedDateTime);
			}
			else
			{
				var ifModifiedSince = DateTime.Parse(rawIfModifiedSince);

				// HTTP does not provide milliseconds, so remove it from the comparison
				if (lastModifiedDateTime.AddMilliseconds(-lastModifiedDateTime.Millisecond) == ifModifiedSince)
				{
					// the generated javascript has not changed
					context.Response.StatusCode = 304;
					return;
				}
			}

			// output the generated javascript to the response
			context.Response.ContentType = "text/javascript";
			context.Response.Write((string)context.Cache[scriptCacheKey]);
			context.Response.Flush();
			context.Response.End();
		}

		/// <summary>
		/// Returns a variable in the format prudence.variable_name = value, wrapping the value as appropriate based on its type.
		/// </summary>
		/// <param name="name">The name of the variable.</param>
		/// <param name="value">The value of the variable.</param>
		/// <param name="type">The type of the variable.</param>
		/// <returns></returns>
		private static string GetVariableString(string name, object value, Type type)
		{
			string variableString;
			if (type == typeof(Regex))
			{
				// regular expressions in javascript are expressed as /regex/ with no quotes
				variableString = string.Format("/{0}/", value);
			}
			else if (type == typeof(string))
			{
				// strings are expressed in javascript wrapped in single-quotes (or at least that's the accepted best practice!)
				variableString = string.Format("'{0}'", value);
			}
			else if (type == typeof(bool))
			{
				variableString = value.ToString().ToLowerInvariant();
			}
			else
			{
				variableString = value.ToString();
			}
			return string.Format("focusSuite.{0} = {1};{2}", name, variableString, Environment.NewLine);
		}

		#endregion
	}
}
