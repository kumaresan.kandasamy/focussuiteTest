﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Web;

using Framework.Core;

#endregion

namespace Focus.Common.HttpHandlers
{
	public class DatabaseLogoHttpHandler : IHttpHandler
	{
		public bool IsReusable
		{
			get { return false; }
		}

		/// <summary>
		/// Enables processing of HTTP Web requests by a custom HttpHandler that implements the <see cref="T:System.Web.IHttpHandler" /> interface.
		/// </summary>
		/// <param name="context">An <see cref="T:System.Web.HttpContext" /> object that provides references to the intrinsic server objects (for example, Request, Response, Session, and Server) used to service HTTP requests.</param>
		public void ProcessRequest(HttpContext context)
		{
			long logoId = 0;
			var requestContext = context.Request.RequestContext;

			if(requestContext.IsNotNull() && requestContext.RouteData.IsNotNull())
				long.TryParse(requestContext.RouteData.Values["id"].ToString(), out logoId);
			
			if (logoId != 0)
			{
				var app = new HttpApp();
				context.Response.ContentType = "image/gif";
				context.Response.BinaryWrite(ServiceClientLocator.EmployerClient(app).GetBusinessUnitLogo(logoId).Logo);
			}
		}
	}
}
