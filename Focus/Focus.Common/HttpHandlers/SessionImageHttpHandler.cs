﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Web;
using System.Web.SessionState;
using Focus.Core.Models;
using Framework.Core;

using Focus.Core;

#endregion

namespace Focus.Common.HttpHandlers
{
	public class SessionImageHttpHandler : IHttpHandler, IReadOnlySessionState
	{

		public bool IsReusable
		{
			get { return false; }
		}

		/// <summary>
		/// Enables processing of HTTP Web requests by a custom HttpHandler that implements the <see cref="T:System.Web.IHttpHandler"/> interface.
		/// </summary>
		/// <param name="context">An <see cref="T:System.Web.HttpContext"/> object that provides references to the intrinsic server objects (for example, Request, Response, Session, and Server) used to service HTTP requests.</param>
		public void ProcessRequest(HttpContext context)
		{
			var sessionKey = "";
			var arrayIndex = 0;

			var requestContext = context.Request.RequestContext;

			if(requestContext.IsNotNull() && requestContext.RouteData.IsNotNull())
			{
				sessionKey = requestContext.RouteData.Values["sessionKey"].ToString();
				if (requestContext.RouteData.Values["arrayIndex"].IsNotNull())
					int.TryParse(requestContext.RouteData.Values["arrayIndex"].ToString(), out arrayIndex);
			}

			byte[] binaryData = null;

			if (sessionKey == Constants.StateKeys.ManageBusinessUnitModel)
			{
				var model = OldApp_RefactorIfFound.GetSessionValue<ManageBusinessUnitModel>(sessionKey);
				if(model.IsNotNull()) binaryData = model.BusinessUnitLogos[arrayIndex].Logo;
			}
			else
			{
				binaryData = OldApp_RefactorIfFound.GetSessionValue<byte[]>(sessionKey);
			}

			if (binaryData.IsNotNullOrEmpty())
			{
				context.Response.ContentType = "image/gif";
				context.Response.BinaryWrite(binaryData);
			}
		}
	}
}
