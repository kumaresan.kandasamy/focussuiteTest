﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web;

#endregion

namespace Focus.Common.HttpHandlers
{
	public class SimpleHttpHandler : IHttpHandler
	{
		public bool IsReusable
		{
			get { return false; }
		}

		/// <summary>
		/// Enables processing of HTTP Web requests by a custom HttpHandler that implements the <see cref="T:System.Web.IHttpHandler"/> interface.
		/// </summary>
		/// <param name="context">An <see cref="T:System.Web.HttpContext"/> object that provides references to the intrinsic server objects (for example, Request, Response, Session, and Server) used to service HTTP requests.</param>
		public void ProcessRequest(HttpContext context)
		{
			context.Response.Write("<html><body><h1>The current time is ");
			context.Response.Write(DateTime.Now.ToLongTimeString());
			context.Response.Write("</h1><p><b>Request Details:</b><br /><ul>");
			context.Response.Write("<li>Requested URL: ");
			context.Response.Write(context.Request.Url.ToString());
			context.Response.Write("</li><li>HTTP Verb: ");
			context.Response.Write(context.Request.HttpMethod);
			context.Response.Write("</li><li>Browser Information: ");
			context.Response.Write(context.Request.Browser.ToString());
			context.Response.Write("</li></ul></body></html>");
		}
	}
}
