﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web;
using Focus.Core;
using Framework.Core;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

#endregion

namespace Focus.Common.HttpHandlers
{
	public class ApplicationImageHttpHandler : IHttpHandler
	{
		public bool IsReusable
		{
			get { return false; }
		}

		/// <summary>
		/// Enables processing of HTTP Web requests by a custom HttpHandler that implements the <see cref="T:System.Web.IHttpHandler"/> interface.
		/// </summary>
		/// <param name="context">An <see cref="T:System.Web.HttpContext"/> object that provides references to the intrinsic server objects (for example, Request, Response, Session, and Server) used to service HTTP requests.</param>
		public void ProcessRequest(HttpContext context)
		{
			var applicationImage = "";
			var requestContext = context.Request.RequestContext;
			var app = new HttpApp();

			if (requestContext.IsNotNull() && requestContext.RouteData.IsNotNull())
				applicationImage = requestContext.RouteData.Values["applicationImageType"].ToString();

			var applicationImageType = ApplicationImageTypes.FocusAssistHeader;
			
			if (applicationImage.IsNotNullOrEmpty())
				Enum.TryParse(applicationImage, out applicationImageType);

            var imgData = ServiceClientLocator.CoreClient(app).GetApplicationImage(applicationImageType).Image;

            var image = byteArrayToImage(imgData);

            if (ImageFormat.Jpeg.Equals(image.RawFormat))
                context.Response.ContentType = "image/jpg";
            else if (ImageFormat.Png.Equals(image.RawFormat))
                context.Response.ContentType = "image/png";
            else
                context.Response.ContentType = "image/gif";

			
			context.Response.BinaryWrite(imgData);
		}

        public static Image byteArrayToImage(byte[] bmpBytes)
        {
            Image image = null;
            using (MemoryStream stream = new MemoryStream(bmpBytes))
            {
                image = Image.FromStream(stream);
            }

            return image;
        }
	}
}
