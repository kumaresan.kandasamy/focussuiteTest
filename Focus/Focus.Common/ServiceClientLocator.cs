﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Configuration;
using Focus.Common.Extensions;
using Focus.ServiceClients.Interfaces;
using Framework.Core;

#endregion

namespace Focus.Common
{
	public static class ServiceClientLocator
	{
		private static IServiceClientRuntime _serviceClientRuntime;

		public static IServiceClientRuntime Runtime
		{
			get
			{
				if (_serviceClientRuntime.IsNull())
				{
						_serviceClientRuntime = new ServiceClients.InProcess.ServiceClientRuntime();
				}

				return _serviceClientRuntime;
			}
		}


		/// <summary>
		/// Accounts the client.
		/// </summary>
		/// <param name="app">The app.</param>
		/// <returns></returns>
		public static IAccountClient AccountClient(IApp app)
		{
			return _serviceClientRuntime.AccountClient(app.ToContextModel());
		}

		/// <summary>
		/// Gets an annotation client.
		/// </summary>
		/// <param name="app">The app.</param>
		/// <returns></returns>
		public static IAnnotationClient AnnotationClient(IApp app)
		{
			return _serviceClientRuntime.AnnotationClient(app.ToContextModel());
		}

		/// <summary>
		/// Gets an authentication client.
		/// </summary>
		/// <param name="app">The app.</param>
		public static IAuthenticationClient AuthenticationClient(IApp app)
		{
			return _serviceClientRuntime.AuthenticationClient(app.ToContextModel());
		}

		/// <summary>
		/// Gets a candidate client.
		/// </summary>
		/// <param name="app">The app.</param>
		/// <returns></returns>
		public static ICandidateClient CandidateClient(IApp app)
		{
			return _serviceClientRuntime.CandidateClient(app.ToContextModel());
		}

		/// <summary>
		/// Gets a core client.
		/// </summary>
		/// <param name="app">The app.</param>
		/// <returns></returns>
		public static ICoreClient CoreClient(IApp app)
		{
			return _serviceClientRuntime.CoreClient(app.ToContextModel());
		}

		/// <summary>
		/// Gets an employee client.
		/// </summary>
		/// <param name="app">The app.</param>
		/// <returns></returns>
		public static IEmployeeClient EmployeeClient(IApp app)
		{
			return _serviceClientRuntime.EmployeeClient(app.ToContextModel());
		}

		/// <summary>
		/// Gets an employer client.
		/// </summary>
		/// <param name="app">The app.</param>
		/// <returns></returns>
		public static IEmployerClient EmployerClient(IApp app)
		{
			return _serviceClientRuntime.EmployerClient(app.ToContextModel());
		}

		/// <summary>
		/// Gets an explorer client.
		/// </summary>
		/// <param name="app">The app.</param>
		/// <returns></returns>
		public static IExplorerClient ExplorerClient(IApp app)
		{
			return _serviceClientRuntime.ExplorerClient(app.ToContextModel());
		}

		/// <summary>
		/// Gets a job client.
		/// </summary>
		/// <param name="app">The app.</param>
		/// <returns></returns>
		public static IJobClient JobClient(IApp app)
		{
			return _serviceClientRuntime.JobClient(app.ToContextModel());
		}

		/// <summary>
		/// Gets an occupation client.
		/// </summary>
		/// <param name="app">The app.</param>
		/// <returns></returns>
		public static IOccupationClient OccupationClient(IApp app)
		{
			return _serviceClientRuntime.OccupationClient(app.ToContextModel());
		}

		/// <summary>
		/// Gets an organization client.
		/// </summary>
		/// <param name="app">The app.</param>
		/// <returns></returns>
		public static IOrganizationClient OrganizationClient(IApp app)
		{
			return _serviceClientRuntime.OrganizationClient(app.ToContextModel());
		}

		/// <summary>
		/// Gets a posting client.
		/// </summary>
		/// <param name="app">The app.</param>
		/// <returns></returns>
		public static IPostingClient PostingClient(IApp app)
		{
			return _serviceClientRuntime.PostingClient(app.ToContextModel());
		}

		/// <summary>
		/// Gets a processor client.
		/// </summary>
		/// <param name="app">The app.</param>
		/// <returns></returns>
		public static IProcessorClient ProcessorClient(IApp app)
		{
			return _serviceClientRuntime.ProcessorClient(app.ToContextModel());
		}

		/// <summary>
		/// Gets a report client.
		/// </summary>
		/// <param name="app">The app.</param>
		/// <returns></returns>
		public static IReportClient ReportClient(IApp app)
		{
			return _serviceClientRuntime.ReportClient(app.ToContextModel());
		}

		/// <summary>
		/// Gets a resume client.
		/// </summary>
		/// <param name="app">The app.</param>
		/// <returns></returns>
		public static IResumeClient ResumeClient(IApp app)
		{
			return _serviceClientRuntime.ResumeClient(app.ToContextModel());
		}

		/// <summary>
		/// Gets a search client.
		/// </summary>
		/// <param name="app">The app.</param>
		/// <returns></returns>
		public static ISearchClient SearchClient(IApp app)
		{
			return _serviceClientRuntime.SearchClient(app.ToContextModel());
		}

		/// <summary>
		/// Gets a staff client.
		/// </summary>
		/// <param name="app">The app.</param>
		/// <returns></returns>
		public static IStaffClient StaffClient(IApp app)
		{
			return _serviceClientRuntime.StaffClient(app.ToContextModel());
		}

		/// <summary>
		/// Gets an utility client.
		/// </summary>
		/// <param name="app">The app.</param>
		/// <returns></returns>
		public static IUtilityClient UtilityClient(IApp app)
		{
			return _serviceClientRuntime.UtilityClient(app.ToContextModel());
		}
	}
}
