--When setting up new installation these are the settings that need to be added to the database
--The values will need to be configured to match the environment

IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'SharedEncryptionKey')
BEGIN
 INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
 VALUES ('SharedEncryptionKey', LEFT(NEWID(), 32), 1)
END

go

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES
('AppVersion','<span style="color:red;font-weight:bold;">DEV-BETA</span>',1),
('ResumeFormat_Standard','~\Assets\Xslts\FocusCareerFormat.xsl',1),
('ResumeFormat_Professional','~\Assets\Xslts\ProfessionalFormat.xsl',1),
('ResumeFormat_Modern','~\Assets\Xslts\ModernFormat.xsl',1),
('ResumeFormat_Contemporary','~\Assets\Xslts\ContemporaryFormat.xsl',1),
('ResumeFormat_Classic','~\Assets\Xslts\ClassicFormat.xsl',1),
('ResumeFormat_Chronological','~\Assets\Xslts\ChronologicalFormat.xsl',1),
('CareerApplicationPath','http://bosdemsvr008.usdev.burninglass.com/careerexplorer',1),
('ValidClientTags','ABC123,EFG456',1),
('TalentApplicationPath','http://bosdemsvr008.usdev.burninglass.com/talent',1),
('AssistApplicationPath','http://bosdemsvr008.usdev.burninglass.com/assist',1),
('LensService','{"host":"BOSAPPSVR005.usdev.burninglass.com","portNumber":2003,"timeout":600,"vendor":"careerjobs","characterSet":"iso-8859-1","serviceType":"Posting","description":"Career posting repository","readonly":false,"anonymousaccess":true,"customFilters":[{"key":"Status","tag":"cf001"},{"key":"OpenDate","tag":"cf002"},{"key":"ClosingOn","tag":"cf003"},{"key":"MinimumSalary","tag":"cf004"},{"key":"MaximumSalary","tag":"cf005"},{"key":"ExternalPostingId","tag":"cf007"},{"key":"Occupation","tag":"cf008"},{"key":"StateCode","tag":"cf009"},{"key":"PostingId","tag":"cf010"},{"key":"Origin","tag":"cf012"},{"key":"Sic2","tag":"cf013"},{"key":"Industry","tag":"cf014"},{"key":"EducationLevel","tag":"cf016"},{"key":"GreenJob","tag":"cf022"},{"key":"Sector","tag":"cf024"},{"key":"CountyCode","tag":"cf025"},{"key":"Internship","tag":"cf026"},{"key":"ProgramsOfStudy","tag":"cf027"},{"key":"AnnualPay","tag":"cf028"},{"key":"Salary","tag":"cf029"},{"key":"Msa","tag":"cf030"},{"key":"ROnet","tag":"cf031"},{"key":"UnpaidPosition","tag":"cf033"},{"key":"InternshipCategories","tag":"cf034"},{"key":"RequiredProgramOfStudyId","tag":"cf036"},{"key":"HomeBased","tag":"cf037"}]}',1),
('LensService','{"host":"BOSAPPSVR005.usdev.burninglass.com","portNumber":2002,"timeout":600,"vendor":"careermartha","characterSet":"iso-8859-1","serviceType":"Resume","description":"Talent/Assist resume repository","readonly":false,"customFilters":[{"key":"Status","tag":"cf001"},{"key":"RegisteredOn","tag":"cf002"},{"key":"ModifiedOn","tag":"cf003"},{"key":"DateOfBirth","tag":"cf004"},{"key":"CitizenFlag","tag":"cf005"},{"key":"VeteranStatus","tag":"cf007"},{"key":"Languages","tag":"cf008"},{"key":"DrivingLicenceClass","tag":"cf009"},{"key":"DrivingLicenceEndorsement","tag":"cf010"},{"key":"Certificates","tag":"cf011"},{"key":"WorkOvertime","tag":"cf012"},{"key":"ShiftType","tag":"cf013"},{"key":"WorkType","tag":"cf014"},{"key":"WorkWeek","tag":"cf015"},{"key":"EducationLevel","tag":"cf016"},{"key":"JobseekerId","tag":"cf017"},{"key":"Relocation","tag":"cf019"},{"key":"JobTypes","tag":"cf020"},{"key":"GPA","tag":"cf021"},{"key":"MonthsExperience","tag":"cf022"},{"key":"EnrollmentStatus","tag":"cf023"},{"key":"ProgramsOfStudy","tag":"cf024"},{"key":"DegreeCompletionDate","tag":"cf025"}]}',1),
('LensService','{"host":"BOSAPPSVR005.usdev.burninglass.com","portNumber":2002,"timeout":600,"vendor":"careermartha","characterSet":"iso-8859-1","serviceType":"Generic","description":"Generic Lens","readonly":true}',1),
('LensService','{"host":"BOSAPPSVR006.usdev.burninglass.com","portNumber":3102,"timeout":600,"vendor":"careermartha","characterSet":"iso-8859-1","serviceType":"JobMine","description":"Job Mine","readonly":true}',1),
('LensService','{"host":"BOSAPPSVR005.usdev.burninglass.com","portNumber":2003,"timeout":600,"vendor":"spideredjobs","characterSet":"iso-8859-1","serviceType":"Posting","description":"Spidered posting repository","readonly":true,"customFilters":[{"key":"Status","tag":"cf001"},{"key":"OpenDate","tag":"cf002"},{"key":"ClosingOn","tag":"cf003"},{"key":"MinimumSalary","tag":"cf004"},{"key":"MaximumSalary","tag":"cf005"},{"key":"ExternalPostingId","tag":"cf007"},{"key":"Occupation","tag":"cf008"},{"key":"StateCode","tag":"cf009"},{"key":"PostingId","tag":"cf010"},{"key":"Origin","tag":"cf012"},{"key":"Sic2","tag":"cf013"},{"key":"Industry","tag":"cf014"},{"key":"EducationLevel","tag":"cf016"},{"key":"GreenJob","tag":"cf022"},{"key":"Sector","tag":"cf024"},{"key":"CountyCode","tag":"cf025"},{"key":"Internship","tag":"cf026"},{"key":"ProgramsOfStudy","tag":"cf027"},{"key":"AnnualPay","tag":"cf028"},{"key":"Salary","tag":"cf029"},{"key":"Msa","tag":"cf030"},{"key":"ROnet","tag":"cf031"},{"key":"UnpaidPosition","tag":"cf033"},{"key":"InternshipCategories","tag":"cf034"},{"key":"RequiredProgramOfStudyId","tag":"cf036"},{"key":"HomeBased","tag":"cf037"}]}',1),
('BatchProcessSettings','[{"Process":1,"Identifier":"75965dec0c6f4cbba3eaedbbd2ff3262"},{"Process":2,"Identifier":"c1144ccbed7749359aaa964512a7e900"},{"Process":3,"Identifier":"97d8c47be2cb45deb7d93e08c2864702"},{"Process":4,"Identifier":"35cbf2025ffb44899528a2adb31a198f"},{"Process":5,"Identifier":"58481ee4f600419c9cf4960aa2309514"},{"Process":6,"Identifier":"6d4c26985c7245c096999cd42bab4677"},{"Process":7,"Identifier":"506c2147a60941934539a6bc5170e2"}]',1),
('DefaultStateKey','State.TX',1),
('DocumentStoreServiceUrl','http://USHtmlDocStore/CareerDocStoreSvc/DocStoreSvc.asmx',1),
('LiveJobsApiRootUrl','http://10.0.2.86/LiveJobsApi/V110_Stag',1),
('LiveJobsApiKey','FocusStag',1),
('LiveJobsApiSecret','AEAA00C48697417A8A236BA09567E649',1),
('LiveJobsApiToken','Core',1),
('LiveJobsApiTokenSecret','7A07362AF1EE11E192DF85E56188709B',1)


-- Talent Additions
('NearbyStateKeys','State.AL,State.AK,State.AZ,State.AR,State.CA',1)

-- Education Additions
('ExplorerDegreeFilteringType','4',1),
('ExplorerDegreeClientDataTag','LSCS',1),
('Theme','Education',1)