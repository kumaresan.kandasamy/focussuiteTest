Andy - 20130125 - Added ParentId to NAICS and populated it with AddNAICSParentId.sql script

One to One relationship between Candidate and Person (Candidate to include a PersonId and FK to Person)
Remove Candidate.ExternalId
Remove ApplicationView.CandidateExternalId
Remove PersonListCandidateView.CandidateExternalId
Remove CandidateNoteView.CandidateExternalId
Remove JobSeekerReferralView.CandidateExternalId
Add EntityId to Bookmark table

Add ViewedPosting entity
Add Posting entity

Andy - 20130201 - Added Scope and Certificate to JobTask Entity

Andy - 20130206 - Renamed PersonResume To Resume
									Added ResumeDocument
									Moved Resume File Properties From Resume To ResumeDocument
									Renamed CandidateNote to PersonNote
									Linked PersonNote to Person record not candidate
									Renamed CandidateWorkHistory To ResumeJob
									Linked ResumeJob To Resume
									RemovedEmployeeCandidate
									Renamed PersonListCandidate To PersonListPerson
									Linked PersonListPerson To Person
									
Andy - 20130207 - Removed Candidate
									Updated SQL views to work with new data structure

Andy - 20130213 - Added StandardIndustrialClassification entity and populated with data from the API database.
									Added Skills entity and populated with data from the API database.

Andy - 20130215 - Applications linked to posting
									Renamed Job Posting property to PostingHtml
									Made link from job to posting 1-to-1

Add ExcludedLensPosting entity