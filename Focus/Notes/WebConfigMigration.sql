-- To set Theme to Education

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'Theme'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES
('Theme','Education',1)

-- To set the Log Severity to Error

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'LogSeverity'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES
('LogSeverity','Error',1)

-- To enable Profiling

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ProfilingEnabled'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES
('ProfilingEnabled','True',1)

-- To change Cache Prefix

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'CachePrefix'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES
('CachePrefix','Focus',1)

-- To change On Error Email

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'OnErrorEmail'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES
('OnErrorEmail','ukdevelopers@burning-glass.com;jim@burning-glass.com',1)

-- To disable Emails

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'EmailsEnabled'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES
('EmailsEnabled','False',1)

-- To turn on Email Rediect

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'EmailRedirect'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES
('EmailRedirect','focusmail@ymail.com',1)

-- To log suppressed Emails to file

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'EmailLogFileLocation'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES
('EmailLogFileLocation','C:\Data\Logs',1)

-- To enable Demo Shield 

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'DemoShieldCustomerCode'
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'DemoShieldRedirectUrl'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES
('DemoShieldCustomerCode','FWO',1),
('DemoShieldRedirectUrl','http://demo.burning-glass.com/career/',1)

-- To change Page State Persistor 

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'PageStatePersister'
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'PageStateQueueMaxLength'
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'PageStateTimeOut'
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'PageStateConnectionString'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES
('PageStatePersister','Page',1),
('PageStateQueueMaxLength','20',1),
('PageStateTimeOut','1440',1),
('PageStateConnectionString','',1)

