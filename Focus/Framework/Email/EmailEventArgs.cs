﻿using System;
using System.Net.Mail;

namespace Framework.Email
{
	public class EmailEventArgs : EventArgs
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmailEventArgs" /> class.
		/// </summary>
		/// <param name="from">From.</param>
		/// <param name="to">To.</param>
		/// <param name="cc">The cc.</param>
		/// <param name="bcc">The BCC.</param>
		/// <param name="subject">The subject.</param>
		/// <param name="body">The body.</param>
		/// <param name="sendAsync">if set to <c>true</c> [send async].</param>
		/// <param name="isBodyHtml">if set to <c>true</c> [is body HTML].</param>
		/// <param name="attachment">The attachment.</param>
		/// <param name="attachmentName">Name of the attachment.</param>
    public EmailEventArgs(string from, string to, string cc, string bcc, string subject, string body, bool sendAsync, bool isBodyHtml = true, byte[] attachment = null, string attachmentName = null)
		{
			From = from;
			To = to;
			Cc = cc;
			Bcc = bcc;
			Subject = subject;
			Body = body;
			SendAsync = sendAsync;
			IsBodyHtml = isBodyHtml;
      Attachment = attachment;
      AttachmentName = attachmentName;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="EmailEventArgs"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="sendAsync">if set to <c>true</c> [send async].</param>
		/// <param name="isBodyHtml">if set to <c>true</c> [is body HTML].</param>
		public EmailEventArgs(MailMessage message, bool sendAsync, bool isBodyHtml = true)
		{
			Message = message;
			SendAsync = sendAsync;
			IsBodyHtml = isBodyHtml;
		}

		/// <summary>
		/// Gets and sets the email from.
		/// </summary>   
		public string From { get; private set; }

		/// <summary>
		/// Gets and sets the email to.
		/// </summary>   
		public string To { get; private set; }

		/// <summary>
		/// Gets and sets the email cc.
		/// </summary>   
		public string Cc { get; private set; }

		/// <summary>
		/// Gets and sets the email bcc.
		/// </summary>   
		public string Bcc { get; private set; }

		/// <summary>
		/// Gets and sets the email subject.
		/// </summary>   
		public string Subject { get; private set; }

		/// <summary>
		/// Gets and sets the email body.
		/// </summary>   
		public string Body { get; private set; }

		/// <summary>
		/// Gets and sets a complete message.
		/// </summary>   
		public MailMessage Message { get; private set; }

		/// <summary>
		/// Gets and sets the send asynchronous setting.
		/// </summary>   
		public bool SendAsync { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is body HTML.
		/// </summary>
		public bool IsBodyHtml { get; set; }

    /// <summary>
    /// Gets or sets the attachment.
    /// </summary>
    public byte[] Attachment { get; set; }

    /// <summary>
    /// Gets or sets the name of the attachment.
    /// </summary>
    public string AttachmentName { get; set; }
	}
}
