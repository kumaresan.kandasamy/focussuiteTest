﻿using System.Configuration;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Framework.Email
{
	public sealed class Emailer
	{
		private readonly bool _sendAsync;

		public Emailer()
		{
			// Send async overide
			if (!bool.TryParse(ConfigurationManager.AppSettings.Get("Focus-Emailer-SendAsync") ?? "false", out _sendAsync))
				_sendAsync = false;
		}

		/// <summary>
		/// Delegate event handler that hooks up requests.
		/// </summary>
		/// <param name="sender">Sender of the event.</param>
		/// <param name="e">Event arguments.</param>
		public delegate void EmailEventHandler(object sender, EmailEventArgs e);

		/// <summary>
		/// The Log event.
		/// </summary>
		public event EmailEventHandler EmailEvent;
		
		#region The singleton definition

		/// <summary>
		/// Private constructor.
		/// </summary>
		static Emailer()
		{
			Instance = new Emailer();
		}

		/// <summary>
		/// Gets the instance of the singleton emailer object.
		/// </summary>
		public static Emailer Instance { get; private set; }

		#endregion

		/// <summary>
		/// Sends the email.
		/// </summary>
		/// <param name="from">From.</param>
		/// <param name="to">To.</param>
		/// <param name="cc">The cc.</param>
		/// <param name="bcc">The bcc.</param>
		/// <param name="subject">The subject.</param>
		/// <param name="body">The body.</param>
		/// <param name="sendAsync">if set to <c>true</c> then the mail will be send asyncronously.</param>
		/// <param name="isBodyHtml">if set to <c>true</c> [is body HTML].</param>
		/// <param name="attachment">The attachment.</param>
		/// <param name="attachmentName">Name of the attachment.</param>
    public static void SendEmail(string from, string to, string cc, string bcc, string subject, string body, bool sendAsync = false, bool isBodyHtml = true, byte[] attachment = null, string attachmentName = null)
		{
			Instance.OnEmail(new EmailEventArgs(from, to, cc, bcc, subject, body, sendAsync, isBodyHtml, attachment, attachmentName));
		}

		/// <summary>
		/// Sends the email.
		/// </summary>
		/// <param name="mailMessage">The mail message.</param>
		/// <param name="sendAsync">if set to <c>true</c> [send async].</param>
		/// <param name="isBodyHtml">if set to <c>true</c> [is body HTML].</param>
		public static void SendEmail(MailMessage mailMessage, bool sendAsync = false, bool isBodyHtml = true)
		{
			Instance.OnEmail(new EmailEventArgs(mailMessage, sendAsync, isBodyHtml));
		}

		#region Email Event Methods

		/// <summary>
		/// Invoke the Email event.
		/// </summary>
		/// <param name="e">Log event parameters.</param>
		public void OnEmail(EmailEventArgs e)
		{
			if (EmailEvent != null)
				if (_sendAsync || e.SendAsync)
				{
					Task.Factory.StartNew(() => EmailEvent(this, e));
				}
				else
					EmailEvent(this, e);				
		}

		/// <summary>
		/// Attach a listening provider emailing device to emailer.
		/// </summary>
		/// <param name="provider">Provider (listening device).</param>
		public void Attach(IEmailProvider provider)
		{
			EmailEvent += provider.SendEmail;
		}

		/// <summary>
		/// Detach a listening provider emailing device from emailer.
		/// </summary>
		/// <param name="provider">Provider (listening device).</param>
		public void Detach(IEmailProvider provider)
		{
			EmailEvent -= provider.SendEmail;
		}

		/// <summary>
		/// Detach all listening provider emailing devices from emailer.
		/// </summary>
		public void Detach()
		{
			EmailEvent = null;
		}

		#endregion
	}
}
