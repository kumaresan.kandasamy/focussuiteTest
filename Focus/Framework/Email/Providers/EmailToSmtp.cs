﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;

using Framework.Core;
using Framework.Logging;

#endregion

namespace Framework.Email.Providers
{
	/// <summary>
	/// Sends a email using an smtp client.
	/// </summary>
	public class EmailToSmtp : IEmailProvider
	{		
		private readonly string _emailRedirect;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="EmailToSmtp"/> class.
		/// </summary>
		public EmailToSmtp(string emailRedirect = "")
		{
			_emailRedirect = emailRedirect;			
		}

		/// <summary>
		/// Sends the email.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="Framework.Email.EmailEventArgs"/> instance containing the event data.</param>
		public void SendEmail(object sender, EmailEventArgs e)
		{
			var message = e.Message;

			if (e.Message == null)
			{
				message = new MailMessage();

				if (!String.IsNullOrEmpty(e.From))
					message.From = new MailAddress(e.From);

				if (String.IsNullOrEmpty(e.To))
					return;

				var toAddresses = e.To.Split(',', ';');

				foreach (var toAddress in toAddresses.Where(x => x.IsNotNullOrEmpty()))
					message.To.Add(new MailAddress(toAddress));

				if (!String.IsNullOrEmpty(e.Cc))
				{
					var ccAddresses = e.Cc.Split(',', ';');

					foreach (var ccAddress in ccAddresses.Where(x => x.IsNotNullOrEmpty()))
						message.CC.Add(new MailAddress(ccAddress));
				}

				if (!String.IsNullOrEmpty(e.Bcc))
				{
					var bccAddresses = e.Bcc.Split(',', ';');

					foreach (var bccAddress in bccAddresses.Where(x => x.IsNotNullOrEmpty()))
						message.Bcc.Add(new MailAddress(bccAddress));
				}

				message.Subject = e.Subject;
				message.Body = e.Body;

				if (e.Attachment.IsNotNullOrEmpty())
					message.Attachments.Add(new Attachment(new MemoryStream(e.Attachment), e.AttachmentName));
			}

			// Check to see if we need to redirect the mails
			RedirectMails(message);

			// Set IsBodyHtml and replace CR's with <br/>
			message.IsBodyHtml = e.IsBodyHtml;

			if (message.IsBodyHtml && message.Body.IsNotNullOrEmpty())
				message.Body = message.Body.Replace(Environment.NewLine, "<br/>");

			using (var smtpClient = new SmtpClient())
			{
				try
				{
					smtpClient.Send(message);
				}
				catch (SmtpException sex)
				{
					var statusCode = sex.StatusCode;
					var exceptionMessage = sex.Message;

					Logger.Error("EmailToSmtp failed to send email: ", sex, statusCode, exceptionMessage);

					throw;
				}
				catch (Exception ex)
				{
					Logger.Error("EmailToSmtp failed to send email: ", ex);

					throw;
				}
				finally
				{
					message.Dispose();
				}
			}
		}

		/// <summary>
		/// Redirects the mails.
		/// </summary>
		/// <param name="message">The message.</param>
		private void RedirectMails(MailMessage message)
		{
			// If we don't have a redirect address then send to original sender
			if (_emailRedirect.IsNullOrEmpty())
				return;

			// Add all recipients to the  beginning Message body
			var messagePrefix = new StringBuilder();
			BuildMessagePrefix(message.To, messagePrefix, "to");
			BuildMessagePrefix(message.CC, messagePrefix, "cc");
			BuildMessagePrefix(message.Bcc, messagePrefix, "bcc");

			messagePrefix.Append(Environment.NewLine);

			// Clear all recipients
			message.To.Clear();
			message.CC.Clear();
			message.Bcc.Clear();

			// Build new recipient list
			var redirectCollection = _emailRedirect.Split(',', ';');
			foreach (var address in redirectCollection)
				message.To.Add(address);

			message.Body = messagePrefix + message.Body;
		}

		/// <summary>
		/// Builds the message prefix.
		/// </summary>
		/// <param name="addresses">The addresses.</param>
		/// <param name="messagePrefix">The message prefix.</param>
		/// <param name="recipientType">Type of the recipient.</param>
		private void BuildMessagePrefix(IEnumerable<MailAddress> addresses, StringBuilder messagePrefix, string recipientType)
		{
			foreach (var address in addresses)
				BuildMessagePrefix(address, messagePrefix, recipientType);
		}

		/// <summary>
		/// Builds the message prefix.
		/// </summary>
		/// <param name="address">The address.</param>
		/// <param name="messagePrefix">The message prefix.</param>
		/// <param name="recipientType">Type of the recipient.</param>
		private void BuildMessagePrefix(MailAddress address, StringBuilder messagePrefix, string recipientType)
		{
			if (address == null)
				return;

			messagePrefix.Append("Mail ");
			messagePrefix.Append(recipientType);
			messagePrefix.Append(": ");
			messagePrefix.Append(address);
			messagePrefix.Append(Environment.NewLine);
		}
	}
}
