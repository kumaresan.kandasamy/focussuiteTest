﻿using System;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Text;

using Framework.Core;

namespace Framework.Email.Providers
{
	/// <summary>
	/// Logs email details to file
	/// </summary>
	public class EmailToFile : IEmailProvider
	{
		private readonly string _logFileLocation;
		private static readonly object _sync = new object();

		public EmailToFile(string logFileLocation)
		{
			_logFileLocation = logFileLocation;
		}

		/// <summary>
		/// Sends the email.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="Framework.Email.EmailEventArgs"/> instance containing the event data.</param>
		public void SendEmail(object sender, EmailEventArgs e)
		{
			if (_logFileLocation.IsNullOrEmpty())
				return;

			lock (_sync)
			{
				if (!Directory.Exists(_logFileLocation)) 
					Directory.CreateDirectory(_logFileLocation);

				// Going to use daily log files
				var logFileName = GenerateLogFileName();
				var logFilePath = String.Format(@"{0}\{1}", _logFileLocation, logFileName);
				
				using (TextWriter logFile = new StreamWriter(logFilePath, true))
				{
					logFile.WriteLine("EMAIL MESSAGE " + DateTime.Now.ToString("HH:mm:ss"));

					if (e.Message == null)
					{
						logFile.WriteLine("FROM : " + e.From);
						logFile.WriteLine("TO : " + e.To);
						logFile.WriteLine("CC : " + e.Cc);
						logFile.WriteLine("BCC : " + e.Bcc);
						logFile.WriteLine("SUBJECT : " + e.Subject);
						logFile.WriteLine("BODY : " + e.Body);
						logFile.WriteLine("SENDASYNC : " + e.SendAsync);
					}
					else
					{
						var fromAddress = (e.Message.From.IsNotNull()) ? e.Message.From.Address : "";
						var toAddresses = (e.Message.To.IsNotNull()) ? EmailAddressesToString(e.Message.To) : "";
						var ccAddresses = (e.Message.CC.IsNotNull()) ? EmailAddressesToString(e.Message.CC) : "";
						var bccAddresses = (e.Message.Bcc.IsNotNull()) ? EmailAddressesToString(e.Message.Bcc) : "";
						var attachmentNames = (e.Message.Attachments.IsNotNull()) ? AttachmentNamesToString(e.Message.Attachments) : "";

						logFile.WriteLine("FROM : " + fromAddress);
						logFile.WriteLine("TO : " + toAddresses);
						logFile.WriteLine("CC : " + ccAddresses);
						logFile.WriteLine("BCC : " + bccAddresses);
						logFile.WriteLine("SUBJECT : " + e.Message.Subject);
						logFile.WriteLine("BODY : " + e.Message.Body);
						if (attachmentNames.Length > 0) logFile.WriteLine("ATTACHMENTS : " + attachmentNames);
						logFile.WriteLine("SENDASYNC : " + e.SendAsync);
					}

					// Write a couple of blank lines
					logFile.WriteLine("");
					logFile.WriteLine("");
				}				
			}
		}

		/// <summary>
		/// Generates the name of the log file.
		/// </summary>
		/// <returns></returns>
		private static string GenerateLogFileName()
		{
			return String.Format("EmailLogs-{0}.txt", DateTime.Now.ToString("yyyyMMdd"));
		}

		/// <summary>
		/// Emails the addresses to string.
		/// </summary>
		/// <param name="emailAddresses">The email addresses.</param>
		/// <returns></returns>
		private static string EmailAddressesToString(MailAddressCollection emailAddresses)
		{
			if (emailAddresses.Count == 0) return "";
			if (emailAddresses.Count == 1) return emailAddresses[0].Address;

			var addressStringBuilder = new StringBuilder();

			foreach (var address in emailAddresses)
			{
				addressStringBuilder.AppendFormat("{0},", address.Address);
			}

			var addressString = addressStringBuilder.ToString();
			return addressString.Substring(0, addressString.Length - 1);
		}

		/// <summary>
		/// Attachments the names to string.
		/// </summary>
		/// <param name="attachments">The attachments.</param>
		/// <returns></returns>
		private static string AttachmentNamesToString(AttachmentCollection attachments)
		{
			if (attachments.Count == 0) return "";
			if (attachments.Count == 1) return attachments[0].Name;
			
			var attachmentNamesStringBuilder = new StringBuilder();

			foreach (var attachment in attachments)
			{
				attachmentNamesStringBuilder.AppendFormat("{0},", attachment.Name);
			}

			var attachmentString = attachmentNamesStringBuilder.ToString();
			return attachmentString.Substring(0, attachmentString.Length - 1);
		}
	}
}
