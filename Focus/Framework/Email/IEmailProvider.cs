﻿
namespace Framework.Email
{
	public interface IEmailProvider
	{
		void SendEmail(object sender, EmailEventArgs e);
	}
}
