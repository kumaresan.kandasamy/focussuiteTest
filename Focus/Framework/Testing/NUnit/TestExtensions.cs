﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Xml.Linq;
using NUnit.Framework;

#endregion

namespace Framework.Testing.NUnit
{
	public static class TestExtensions
	{
		[System.Diagnostics.DebuggerNonUserCode]
		public static void ShouldEqual(this object actual, object expected, string message = null)
		{
			Assert.AreEqual(expected, actual, message);
		}

		[System.Diagnostics.DebuggerNonUserCode]
		public static void ShouldNotEqual(this object actual, object expected)
		{
			Assert.AreNotEqual(expected, actual);
		}

		[System.Diagnostics.DebuggerNonUserCode]
		public static void ShouldBeTheSameAs(this object actual, object expected)
		{
			Assert.AreSame(expected, actual);
		}

		[System.Diagnostics.DebuggerNonUserCode]
		public static void ShouldNotBeTheSameAs(this object actual, object expected)
		{
			Assert.AreNotSame(expected, actual);
		}

		[System.Diagnostics.DebuggerNonUserCode]
		public static void ShouldBeGreater(this int actual, int expected)
		{
			Assert.Greater(actual, expected);
		}

		[System.Diagnostics.DebuggerNonUserCode]
		public static void ShouldBeGreaterOrEqual(this int actual, long expected)
		{
			Assert.GreaterOrEqual(actual, expected);
		}

		[System.Diagnostics.DebuggerNonUserCode]
		public static void ShouldBeLess(this int actual, int expected)
		{
			Assert.Less(actual, expected);
		}

		[System.Diagnostics.DebuggerNonUserCode]
		public static void ShouldBeLessOrEqual(this int actual, int expected)
		{
			Assert.LessOrEqual(actual, expected);
		}

		[System.Diagnostics.DebuggerNonUserCode]
		public static void ShouldBeGreater(this long actual, long expected)
		{
			Assert.Greater(actual, expected);
		}

		[System.Diagnostics.DebuggerNonUserCode]
		public static void ShouldBeNull(this object actual)
		{
			Assert.IsNull(actual);
		}

		[System.Diagnostics.DebuggerNonUserCode]
		public static void ShouldNotBeNull(this object actual)
		{
			Assert.IsNotNull(actual);
		}

		[System.Diagnostics.DebuggerNonUserCode]
		public static void ShouldBeNullOrEmpty(this string actual)
		{
			Assert.IsNullOrEmpty(actual);
		}

		[System.Diagnostics.DebuggerNonUserCode]
		public static void ShouldNotBeNullOrEmpty(this string actual)
		{
			Assert.IsNotNullOrEmpty(actual);
		}

		[System.Diagnostics.DebuggerNonUserCode]
		public static void ShouldBeTrue(this bool b)
		{
			Assert.IsTrue(b);
		}

		[System.Diagnostics.DebuggerNonUserCode]
		public static void ShouldBeFalse(this bool b)
		{
			Assert.IsFalse(b);
		}

		[System.Diagnostics.DebuggerNonUserCode]
		public static Exception ShouldBeThrownBy(this Type exceptionType, TestDelegate code)
		{
			return Assert.Throws(exceptionType, code);
		}

		[System.Diagnostics.DebuggerNonUserCode]
		public static T ShouldBe<T>(this object actual)
		{
			Assert.IsInstanceOf<T>(actual);
			return (T)actual;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		public static void ShouldBeValidXml(this string actual)
		{
			bool isXml;
			try
			{
				XDocument.Parse(actual);
				isXml = true;
			}
			catch (Exception)
			{
				isXml = false;
			}
			isXml.ShouldBeTrue();
		}
	}
}
