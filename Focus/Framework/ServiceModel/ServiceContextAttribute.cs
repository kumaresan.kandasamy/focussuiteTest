﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

#endregion

namespace Framework.ServiceModel
{
	/// <summary>
	/// WCF Service Context Attribute
	/// </summary>
	public class ServiceContextAttribute : Attribute, IServiceBehavior, IDispatchMessageInspector
	{
		/// <summary>
		/// Called after an inbound message has been received but before the message is dispatched to the intended operation.
		/// </summary>
		/// <param name="request">The request message.</param>
		/// <param name="channel">The incoming channel.</param>
		/// <param name="instanceContext">The current service instance.</param>
		/// <returns>
		/// The object used to correlate state. This object is passed back in the <see cref="M:System.ServiceModel.Dispatcher.IDispatchMessageInspector.BeforeSendReply(System.ServiceModel.Channels.Message@,System.Object)" /> method.
		/// </returns>
		public object AfterReceiveRequest(ref System.ServiceModel.Channels.Message request, IClientChannel channel, InstanceContext instanceContext)
		{
			ServiceContext.AddToOperationContext();
			return null;
		}

		/// <summary>
		/// Called after the operation has returned but before the reply message is sent.
		/// </summary>
		/// <param name="reply">The reply message. This value is null if the operation is one way.</param>
		/// <param name="correlationState">The correlation object returned from the <see cref="M:System.ServiceModel.Dispatcher.IDispatchMessageInspector.AfterReceiveRequest(System.ServiceModel.Channels.Message@,System.ServiceModel.IClientChannel,System.ServiceModel.InstanceContext)" /> method.</param>
		public void BeforeSendReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
		{
			ServiceContext.RemoveFromOperationContext();
		}

		/// <summary>
		/// Provides the ability to pass custom data to binding elements to support the contract implementation.
		/// </summary>
		/// <param name="serviceDescription">The service description of the service.</param>
		/// <param name="serviceHostBase">The host of the service.</param>
		/// <param name="endpoints">The service endpoints.</param>
		/// <param name="bindingParameters">Custom objects to which binding elements have access.</param>
		public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
		{ }

		/// <summary>
		/// Provides the ability to change run-time property values or insert custom extension objects such as error handlers, message or parameter interceptors, security extensions, and other custom extension objects.
		/// </summary>
		/// <param name="serviceDescription">The service description.</param>
		/// <param name="serviceHostBase">The host that is currently being built.</param>
		public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
		{
			foreach (ChannelDispatcher channelDispatcher in serviceHostBase.ChannelDispatchers)
				foreach (EndpointDispatcher endpointDispatcher in channelDispatcher.Endpoints)
					endpointDispatcher.DispatchRuntime.MessageInspectors.Add(this);
		}

		/// <summary>
		/// Provides the ability to inspect the service host and the service description to confirm that the service can run successfully.
		/// </summary>
		/// <param name="serviceDescription">The service description.</param>
		/// <param name="serviceHostBase">The service host that is currently being constructed.</param>
		public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
		{ }
	}
}
