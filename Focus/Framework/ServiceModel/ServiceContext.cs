﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections;
using System.ServiceModel;

#endregion

namespace Framework.ServiceModel
{
	/// <summary>
	/// WCF Service Context
	/// </summary>
	public class ServiceContext : IExtension<OperationContext>
	{
		private IDictionary _items;

		/// <summary>
		/// Gets the current service context.
		/// </summary>
		public static ServiceContext Current
		{
			get { return OperationContext.Current.Extensions.Find<ServiceContext>(); }
		}

		/// <summary>
		/// Gets the items.
		/// </summary>
		public IDictionary Items
		{
			get { return _items ?? (_items = new Hashtable()); }
		}

		/// <summary>
		/// Adds a new service context to operation context.
		/// </summary>
		public static void AddToOperationContext()
		{
			OperationContext.Current.Extensions.Add(new ServiceContext());
		}

		/// <summary>
		/// Removes the service context from operation context.
		/// </summary>
		public static void RemoveFromOperationContext()
		{
			// Try and auto dispose of all items
			foreach (var item in Current.Items)
			{
				var disposable = ((DictionaryEntry)item).Value as IDisposable;

				if (disposable != null)
					try { disposable.Dispose(); }
					catch { }
			}

			OperationContext.Current.Extensions.Remove(Current);
		}

		/// <summary>
		/// Attaches the specified owner.
		/// </summary>
		/// <param name="owner">The owner.</param>
		public void Attach(OperationContext owner)
		{ }

		/// <summary>
		/// Detaches the specified owner.
		/// </summary>
		/// <param name="owner">The owner.</param>
		public void Detach(OperationContext owner)
		{ }
	}
}
