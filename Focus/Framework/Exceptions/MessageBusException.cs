﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Framework.Exceptions
{
	[Serializable]
	public class MessageBusException : ApplicationException
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="MessageBusException" /> class.
		/// </summary>
		public MessageBusException() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="MessageBusException" /> class.
		/// </summary>
		/// <param name="message">The message.</param>
		public MessageBusException(string message) : base(message) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="MessageBusException" /> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="inner">The inner.</param>
		public MessageBusException(string message, Exception inner) : base(message, inner) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="MessageBusException" /> class.
		/// </summary>
		/// <param name="info">The info.</param>
		/// <param name="context">The context.</param>
		public MessageBusException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}
