﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;



#endregion

namespace Framework.Exceptions
{
	[Serializable]
	public class ServiceCallException : ApplicationException
	{
		public int ErrorCode { get; set; }

		public ServiceCallException() { }

		public ServiceCallException(string message, int errorCode) : base(message)
		{
			ErrorCode = errorCode;
		}

		public ServiceCallException(string message, int errorCode, Exception inner): base(message, inner)
		{
			ErrorCode = errorCode;
		}

		public ServiceCallException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}