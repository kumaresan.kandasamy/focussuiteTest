﻿using System;
using System.Runtime.Serialization;

namespace Framework.Exceptions
{
	[Serializable]
	public class CorrelationException : ApplicationException
	{
		public CorrelationException() { }

		public CorrelationException(string message) : base(message) { }
		
		public CorrelationException(string message, Exception inner) : base(message, inner) { }

		public CorrelationException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}