﻿using System;
using System.Runtime.Serialization;


namespace Framework.Exceptions
{
	[Serializable]
	public class SessionException : ApplicationException
	{
		public SessionException() { }

		public SessionException(string message) : base(message) { }
		
		public SessionException(string message, Exception inner) : base(message, inner) { }

		public SessionException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}
