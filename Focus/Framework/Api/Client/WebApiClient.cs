﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json;

#endregion

namespace Framework.Api
{
	public class WebApiClient
	{
		private readonly string _endpoint;
		private readonly string _consumerKey, _consumerSecret, _token, _tokenSecret;
		private readonly OAuth.SignatureMethods _signatureMethod = OAuth.SignatureMethods.HMACSHA1;
		private readonly bool _useOAuth;

		/// <summary>
		/// Initializes a new instance of the <see cref="WebApiClient"/> class.
		/// </summary>
		/// <param name="endpoint">The endpoint.</param>
		/// <param name="consumerKey">The consumer key.</param>
		/// <param name="consumerSecret">The consumer secret.</param>
		/// <param name="token">The token.</param>
		/// <param name="tokenSecret">The token secret.</param>
		/// <param name="signatureMethod">The signature method.</param>
		public WebApiClient(string endpoint, string consumerKey = null, string consumerSecret = null, string token = null, string tokenSecret = null, OAuth.SignatureMethods signatureMethod = OAuth.SignatureMethods.HMACSHA1)
		{
			_endpoint = endpoint;

			_consumerKey = consumerKey;
			_consumerSecret = consumerSecret;
			_token = token;
			_tokenSecret = tokenSecret;
			_signatureMethod = signatureMethod;

			_useOAuth = (!string.IsNullOrEmpty(_consumerKey) && !string.IsNullOrEmpty(_consumerSecret) && !string.IsNullOrEmpty(_token) && 
									 !string.IsNullOrEmpty(_tokenSecret) && _signatureMethod != OAuth.SignatureMethods.UNKNOWN);
		}

		/// <summary>
		/// Gets all.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="query">The query.</param>
		/// <returns></returns>
		public ApiResponse<T> GetAll<T>(string query = null)
		{
			using (var httpClient = GetHttpClient())
			{
				var endpoint = _endpoint + (!string.IsNullOrEmpty(query) ? "?" + query : "");
				var response = httpClient.GetAsync(endpoint).Result;

				var apiResponse = JsonConvert.DeserializeObject<ApiResponse<T>>(response.Content.ReadAsStringAsync().Result);
				return apiResponse;
			}
		}

    /// <summary>
    /// Gets all.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="resource">The resource.</param>
    /// <param name="query">The query.</param>
    /// <returns></returns>
    public ApiResponse<T> GetAll<T>(string resource, string query)
    {
      using (var httpClient = GetHttpClient())
      {
        var endpoint = _endpoint + "/" + resource + (!string.IsNullOrEmpty(query) ? "?" + query : "");
        var response = httpClient.GetAsync(endpoint).Result;

        var apiResponse = JsonConvert.DeserializeObject<ApiResponse<T>>(response.Content.ReadAsStringAsync().Result);
        return apiResponse;
      }
    }

		/// <summary>
		/// Gets all.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="id">The id.</param>
		/// <param name="resource">The resource.</param>
		/// <param name="query">The query.</param>
		/// <returns></returns>
		public ApiResponse<T> GetAll<T>(int id, string resource, string query = null)
		{
			using (var httpClient = GetHttpClient())
			{
				var endpoint = _endpoint + "/" + id + "/" + resource + (!string.IsNullOrEmpty(query) ? "?" + query : "");
				var response = httpClient.GetAsync(endpoint).Result;

				var apiResponse = JsonConvert.DeserializeObject<ApiResponse<T>>(response.Content.ReadAsStringAsync().Result);
				return apiResponse;
			}
		}

		/// <summary>
		/// Gets the specified id.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="id">The id.</param>
		/// <param name="query">The query.</param>
		/// <returns></returns>
		public ApiResponse<T> Get<T>(object id, string query = null)
		{
			using (var httpClient = GetHttpClient())
			{
				var endpoint = _endpoint + "/" + id + (!string.IsNullOrEmpty(query) ? "?" + query : "");
				var response = httpClient.GetAsync(endpoint).Result;
				
				var apiResponse = JsonConvert.DeserializeObject<ApiResponse<T>>(response.Content.ReadAsStringAsync().Result);
				return apiResponse;
			}
		}

		/// <summary>
		/// Posts the specified resource.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="data">The data.</param>
		/// <returns></returns>
		public ApiResponse Post<T>(T data)
		{
			using (var httpClient = GetHttpClient())
			{
				var content = GetHttpContent(data);

				var response = httpClient.PostAsync(_endpoint, content).Result;

				var apiResponse = JsonConvert.DeserializeObject<ApiResponse<T>>(response.Content.ReadAsStringAsync().Result);
				return apiResponse;
			}
		}

		/// <summary>
		/// Posts the specified criteria.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="criteria">The criteria.</param>
		/// <param name="dateFormatHandling">The date format handling.</param>
		/// <param name="dateTimeZoneHandling">The date time zone handling.</param>
		/// <returns></returns>
		public ApiResponse<T> Post<T>(object criteria, DateFormatHandling dateFormatHandling = DateFormatHandling.IsoDateFormat, DateTimeZoneHandling dateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind)
		{
			using (var httpClient = GetHttpClient())
			{
				var content = GetHttpContent(criteria, dateFormatHandling, dateTimeZoneHandling);

				httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
				var response = httpClient.PostAsync(_endpoint, content).Result;

				var output = response.Content.ReadAsStringAsync().Result;
				Console.WriteLine(output);

				var result = JsonConvert.DeserializeObject<T>(output);
				var apiResponse = new ApiResponse<T>();
				apiResponse.Result = result;

				return apiResponse;
			}
		}

		/// <summary>
		/// Puts the specified resource by id.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="id">The id.</param>
		/// <param name="data">The data.</param>
		/// <returns></returns>
		public ApiResponse Put<T>(object id, T data)
		{
			using (var httpClient = GetHttpClient())
			{
				var content = GetHttpContent(data);

				var response = httpClient.PutAsync(_endpoint + "/" + id, content).Result;

				var apiResponse = JsonConvert.DeserializeObject<ApiResponse>(response.Content.ReadAsStringAsync().Result);
				return apiResponse;
			}
		}

		/// <summary>
		/// Deletes the specified resource by id.
		/// </summary>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		public ApiResponse Delete(object id)
		{
			using (var httpClient = GetHttpClient())
			{
				var response = httpClient.DeleteAsync(_endpoint + "/" + id).Result;

				var apiResponse = JsonConvert.DeserializeObject<ApiResponse>(response.Content.ReadAsStringAsync().Result);
				return apiResponse;
			}
		}

		/// <summary>
		/// Gets the Http content of the data.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="data">The data.</param>
		/// <param name="dateFormatHandling">The date format handling.</param>
		/// <param name="dateTimeZoneHandling">The date time zone handling.</param>
		/// <returns></returns>
		protected HttpContent GetHttpContent<T>(T data, DateFormatHandling dateFormatHandling = DateFormatHandling.IsoDateFormat, DateTimeZoneHandling dateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind)
		{
			if (data is string)
				return new StringContent(data.ToString(), Encoding.UTF8, "application/json");
			// Else serialize
			return new StringContent(JsonConvert.SerializeObject(data, new JsonSerializerSettings { DateFormatHandling = dateFormatHandling, DateTimeZoneHandling = dateTimeZoneHandling }), Encoding.UTF8, "application/json");
		}

		/// <summary>
		/// Gets the HTTP client.
		/// </summary>
		/// <returns></returns>
		protected HttpClient GetHttpClient()
		{
			return _useOAuth ? new HttpClient(new OAuthMessageHandler(new HttpClientHandler(), _consumerKey, _consumerSecret, _token, _tokenSecret, _signatureMethod)) : new HttpClient();
		}

		private class OAuthMessageHandler : DelegatingHandler
		{
			private readonly OAuth _oAuth = new OAuth();
			private readonly string _consumerKey, _consumerSecret, _token, _tokenSecret;
			private readonly OAuth.SignatureMethods _signatureMethod;

			/// <summary>
			/// Initializes a new instance of the <see cref="OAuthMessageHandler"/> class.
			/// </summary>
			/// <param name="innerHandler">The inner handler.</param>
			/// <param name="consumerKey">The consumer key.</param>
			/// <param name="consumerSecret">The consumer secret.</param>
			/// <param name="token">The token.</param>
			/// <param name="tokenSecret">The token secret.</param>
			/// <param name="signatureMethod">The signature method.</param>
			public OAuthMessageHandler(HttpMessageHandler innerHandler, string consumerKey, string consumerSecret, string token, string tokenSecret, OAuth.SignatureMethods signatureMethod) : base(innerHandler)
			{
				_consumerKey = consumerKey;
				_consumerSecret = consumerSecret;
				_token = token;
				_tokenSecret = tokenSecret;
				_signatureMethod = signatureMethod;				
			}

			/// <summary>
			/// Sends an HTTP request to the inner handler to send to the server as an asynchronous operation.
			/// </summary>
			/// <param name="request">The HTTP request message to send to the server.</param>
			/// <param name="cancellationToken">A cancellation token to cancel operation.</param>
			/// <returns>
			/// Returns <see cref="T:System.Threading.Tasks.Task`1"/>. The task object representing the asynchronous operation.
			/// </returns>
			/// <exception cref="T:System.ArgumentNullException">The <paramref name="request"/> was null.</exception>
			protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
			{
				// Compute OAuth header
				string normalizedUri, normalizedParameters, authHeader;

				_oAuth.GenerateSignature(request.RequestUri, _consumerKey, _consumerSecret, _token, _tokenSecret, request.Method.Method, 
																 OAuth.GenerateTimeStamp(), _oAuth.GenerateNonce(), _signatureMethod, out normalizedUri, out normalizedParameters, out authHeader);

				request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("OAuth", authHeader);
				return base.SendAsync(request, cancellationToken);
			}
		}
	}
}
