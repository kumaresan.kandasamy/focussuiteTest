﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

#endregion

namespace Framework.Api
{
	public class OAuthHeader
	{
		public string ConsumerKey  { get; set; }
		public string Callback { get; set; }
		public string Version { get; set; }
		public OAuth.SignatureMethods SignatureMethod { get; set; }
		public string Signature { get; set; }
		public long Timestamp { get; set; }
		public string Nonce { get; set; }
		public string Token { get; set; }
	}
}
