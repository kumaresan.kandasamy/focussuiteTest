﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Text;
using System.Web;

#endregion

namespace Framework.Api
{
	public class OAuth
	{
		#region Constants

		protected const string OAuthVersion = "1.0";
		protected const string OAuthParameterPrefix = "oauth_";

		//
		// List of know and used oauth parameters' names
		//        
		public const string OAuthConsumerKeyKey = "oauth_consumer_key";
		public const string OAuthCallbackKey = "oauth_callback";
		public const string OAuthVersionKey = "oauth_version";
		public const string OAuthSignatureMethodKey = "oauth_signature_method";
		public const string OAuthSignatureKey = "oauth_signature";
		public const string OAuthTimestampKey = "oauth_timestamp";
		public const string OAuthNonceKey = "oauth_nonce";
		public const string OAuthTokenKey = "oauth_token";
		public const string OAuthTokenSecretKey = "oauth_token_secret";

		protected const string HMACSHA1SignatureType = "HMAC-SHA1";
		protected const string PlainTextSignatureType = "PLAINTEXT";
		protected const string RSASHA1SignatureType = "RSA-SHA1";

		#endregion

		protected Random random = new Random();
		protected string reservedChars = @"`!@#$%^&*()_-+=.~,:;'?/|\[] ";
		protected string unreservedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~";

		/// <summary>
		/// Provides a predefined set of algorithms that are supported officially by the protocol
		/// </summary>
		public enum SignatureMethods
		{
			UNKNOWN,
			PLAINTEXT,
			HMACSHA1,
			RSASHA1
		}

		/// <summary>
		/// Provides an internal structure to sort the query parameter
		/// </summary>
		protected class QueryParameter
		{
			public QueryParameter(string name, string value)
			{
				Name = name;
				Value = value;
			}

			public string Name { get; private set; }
			public string Value { get; private set; }
		}

		/// <summary>
		/// Helper function to compute a hash value
		/// </summary>
		/// <param name="hashAlgorithm">The hashing algorithm used. If that algorithm needs some initialization, like HMAC and its derivatives, they should be initialized prior to passing it to this function</param>
		/// <param name="data">The data to hash</param>
		/// <returns>a Base64 string of the hash value</returns>
		private string ComputeHash(HashAlgorithm hashAlgorithm, string data)
		{
			if (hashAlgorithm == null)
				throw new ArgumentNullException("hashAlgorithm");

			if (string.IsNullOrEmpty(data))
				throw new ArgumentNullException("data");

			var dataBuffer = Encoding.UTF8.GetBytes(data);
			var hashBytes = hashAlgorithm.ComputeHash(dataBuffer);

			return Convert.ToBase64String(hashBytes);
		}

		/// <summary>
		/// Internal function to cut out all non oauth query string parameters (all parameters not beginning with "oauth_")
		/// </summary>
		/// <param name="parameters">The query string part of the Url</param>
		/// <returns>A list of QueryParameter each containing the parameter name and value</returns>
		private List<QueryParameter> GetQueryParameters(string parameters)
		{
			if (parameters.StartsWith("?"))
				parameters = parameters.Remove(0, 1);

			var result = new List<QueryParameter>();

			if (!string.IsNullOrEmpty(parameters))
			{
				var p = parameters.Split('&');

				foreach (var s in p)
				{
					if (!string.IsNullOrEmpty(s) && !s.StartsWith(OAuthParameterPrefix))
					{
						if (s.IndexOf('=') > -1)
						{
							var temp = s.Split('=');
							result.Add(new QueryParameter(temp[0], temp[1]));
						}
						else
							result.Add(new QueryParameter(s, string.Empty));
					}
				}
			}

			return result;
		}

		/// <summary>
		/// This is a different Url Encode implementation since the default .NET one outputs the percent encoding in lower case.
		/// While this is not a problem with the percent encoding spec, it is used in upper case throughout OAuth
		/// </summary>
		/// <param name="value">The value to Url encode</param>
		/// <returns>Returns a Url encoded string</returns>
		protected string UrlEncode(string value)
		{
			var result = new StringBuilder();

			if (string.IsNullOrEmpty(value))
				return string.Empty;

			foreach (var symbol in value)
			{
				if (unreservedChars.IndexOf(symbol) != -1)
					result.Append(symbol);
				else if (reservedChars.IndexOf(symbol) != -1)
					result.Append('%' + String.Format("{0:X2}", (int)symbol).ToUpper());
				else
				{
					var encoded = HttpUtility.UrlEncode(symbol.ToString()).ToUpper();

					if (!string.IsNullOrEmpty(encoded))
						result.Append(encoded);
				}
			}

			return result.ToString();
		}

		/// <summary>
		/// Normalizes the request parameters according to the spec
		/// </summary>
		/// <param name="parameters">The list of parameters already sorted</param>
		/// <returns>a string representing the normalized parameters</returns>
		protected string NormalizeRequestParameters(IList<QueryParameter> parameters)
		{
			var sb = new StringBuilder();
			QueryParameter p = null;

			for (var i = 0; i < parameters.Count; i++)
			{
				p = parameters[i];
				sb.AppendFormat("{0}={1}", p.Name, p.Value);

				if (i < parameters.Count - 1)
					sb.Append("&");
			}

			return sb.ToString();
		}

		/// <summary>
		/// Generate the signature base that is used to produce the signature.
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <param name="consumerKey">The consumer key.</param>
		/// <param name="token">The token.</param>
		/// <param name="tokenSecret">The token secret.</param>
		/// <param name="httpMethod">The HTTP method.</param>
		/// <param name="timeStamp">The time stamp.</param>
		/// <param name="nonce">The nonce.</param>
		/// <param name="signatureType">Type of the signature.</param>
		/// <param name="normalizedUrl">The normalized URL.</param>
		/// <param name="normalizedRequestParameters">The normalized request parameters.</param>
		/// <returns></returns>
		public string GenerateSignatureBase(Uri url, string consumerKey, string token, string tokenSecret, string httpMethod, long timeStamp, string nonce, string signatureType, out string normalizedUrl, out string normalizedRequestParameters)
		{
			if (token == null)
				token = string.Empty;

			if (tokenSecret == null)
				tokenSecret = string.Empty;

			if (string.IsNullOrEmpty(consumerKey))
				throw new ArgumentNullException("consumerKey");

			if (string.IsNullOrEmpty(httpMethod))
				throw new ArgumentNullException("httpMethod");

			if (string.IsNullOrEmpty(signatureType))
				throw new ArgumentNullException("signatureType");

			var parameters = GetQueryParameters(url.Query);
			parameters.Add(new QueryParameter(OAuthVersionKey, OAuthVersion));
			parameters.Add(new QueryParameter(OAuthNonceKey, nonce));
			parameters.Add(new QueryParameter(OAuthTimestampKey, timeStamp.ToString()));
			parameters.Add(new QueryParameter(OAuthSignatureMethodKey, signatureType));
			parameters.Add(new QueryParameter(OAuthConsumerKeyKey, consumerKey));

			if (!string.IsNullOrEmpty(token))
				parameters.Add(new QueryParameter(OAuthTokenKey, token));

			parameters.Sort(new QueryParameterComparer());

			normalizedUrl = string.Format("{0}://{1}", url.Scheme, url.Host);

			if (!((url.Scheme == "http" && url.Port == 80) || (url.Scheme == "https" && url.Port == 443)))
				normalizedUrl += ":" + url.Port;
			
			normalizedUrl += url.AbsolutePath;
			normalizedRequestParameters = NormalizeRequestParameters(parameters);

			var signatureBase = new StringBuilder();
			signatureBase.AppendFormat("{0}&", httpMethod.ToUpper());
			signatureBase.AppendFormat("{0}&", UrlEncode(normalizedUrl));
			signatureBase.AppendFormat("{0}", UrlEncode(normalizedRequestParameters));

			return signatureBase.ToString();
		}

		/// <summary>
		/// Generate the signature value based on the given signature base and hash algorithm
		/// </summary>
		/// <param name="signatureBase">The signature based as produced by the GenerateSignatureBase method or by any other means</param>
		/// <param name="hash">The hash algorithm used to perform the hashing. If the hashing algorithm requires initialization or a key it should be set prior to calling this method</param>
		/// <returns>A base64 string of the hash value</returns>
		public string GenerateSignatureUsingHash(string signatureBase, HashAlgorithm hash)
		{
			return ComputeHash(hash, signatureBase);
		}

		/// <summary>
		/// Generates a signature using the HMAC-SHA1 algorithm
		/// </summary>
		/// <param name="url">The full url that needs to be signed including its non OAuth url parameters</param>
		/// <param name="consumerKey">The consumer key</param>
		/// <param name="consumerSecret">The consumer seceret</param>
		/// <param name="token">The token, if available. If not available pass null or an empty string</param>
		/// <param name="tokenSecret">The token secret, if available. If not available pass null or an empty string</param>
		/// <param name="httpMethod">The http method used. Must be a valid HTTP method verb (POST,GET,PUT, etc)</param>
		/// <param name="timeStamp">The time stamp.</param>
		/// <param name="nonce">The nonce.</param>
		/// <param name="normalizedUrl">The normalized URL.</param>
		/// <param name="normalizedRequestParameters">The normalized request parameters.</param>
		/// <param name="authHeader">The auth header.</param>
		/// <returns>A base64 string of the hash value</returns>
		public string GenerateSignature(Uri url, string consumerKey, string consumerSecret, string token, string tokenSecret, string httpMethod, long timeStamp, string nonce, out string normalizedUrl, out string normalizedRequestParameters, out string authHeader)
		{
			return GenerateSignature(url, consumerKey, consumerSecret, token, tokenSecret, httpMethod, timeStamp, nonce, SignatureMethods.HMACSHA1, out normalizedUrl, out normalizedRequestParameters, out authHeader);
		}

		/// <summary>
		/// Generates a signature using the specified signatureType
		/// </summary>
		/// <param name="url">The full url that needs to be signed including its non OAuth url parameters</param>
		/// <param name="consumerKey">The consumer key</param>
		/// <param name="consumerSecret">The consumer seceret</param>
		/// <param name="token">The token, if available. If not available pass null or an empty string</param>
		/// <param name="tokenSecret">The token secret, if available. If not available pass null or an empty string</param>
		/// <param name="httpMethod">The http method used. Must be a valid HTTP method verb (POST,GET,PUT, etc)</param>
		/// <param name="timeStamp">The time stamp.</param>
		/// <param name="nonce">The nonce.</param>
		/// <param name="signatureType">The type of signature to use</param>
		/// <param name="normalizedUrl">The normalized URL.</param>
		/// <param name="normalizedRequestParameters">The normalized request parameters.</param>
		/// <param name="authHeader">The auth header.</param>
		/// <returns>A base64 string of the hash value</returns>
		public string GenerateSignature(Uri url, string consumerKey, string consumerSecret, string token, string tokenSecret, string httpMethod, long timeStamp, string nonce, SignatureMethods signatureType, out string normalizedUrl, out string normalizedRequestParameters, out string authHeader)
		{
			normalizedUrl = null;
			normalizedRequestParameters = null;
			authHeader = null;

			switch (signatureType)
			{
				case SignatureMethods.PLAINTEXT:
					return string.Format("{0}&{1}", consumerSecret, tokenSecret);

				case SignatureMethods.HMACSHA1:
					var signatureBase = GenerateSignatureBase(url, consumerKey, token, tokenSecret, httpMethod, timeStamp, nonce, HMACSHA1SignatureType, out normalizedUrl, out normalizedRequestParameters);

					var hmacsha1 = new HMACSHA1();
					hmacsha1.Key = Encoding.ASCII.GetBytes(string.Format("{0}&{1}", UrlEncode(consumerSecret), string.IsNullOrEmpty(tokenSecret) ? "" : UrlEncode(tokenSecret)));

					var signature = GenerateSignatureUsingHash(signatureBase, hmacsha1);

					var auth = new StringBuilder();
					auth.AppendFormat("{0}=\"{1}\", ", OAuthConsumerKeyKey, UrlEncode(consumerKey));
					auth.AppendFormat("{0}=\"{1}\", ", OAuthNonceKey, UrlEncode(nonce));
					auth.AppendFormat("{0}=\"{1}\", ", OAuthSignatureKey, UrlEncode(signature));
					auth.AppendFormat("{0}=\"{1}\", ", OAuthSignatureMethodKey, "HMAC-SHA1");
					auth.AppendFormat("{0}=\"{1}\", ", OAuthTimestampKey, timeStamp);
					auth.AppendFormat("{0}=\"{1}\", ", OAuthTokenKey, UrlEncode(token));
					auth.AppendFormat("{0}=\"{1}\"", OAuthVersionKey, "1.0");
					authHeader = auth.ToString();

					return signature;

				case SignatureMethods.RSASHA1:
					throw new NotImplementedException();
				default:
					throw new ArgumentException("Unknown signature type", "signatureType");
			}
		}

		/// <summary>
		/// Generate the timestamp for the signature        
		/// </summary>
		/// <returns></returns>
		public static long GenerateTimeStamp()
		{
			return GenerateTimeStamp(DateTime.UtcNow);
		}

		/// <summary>
		/// Generates the time stamp.
		/// </summary>
		/// <param name="dateTime">The date time to generate the timestamp from.</param>
		/// <returns></returns>
		public static long GenerateTimeStamp(DateTime dateTime)
		{
			// Default implementation of UNIX time of the current UTC time
			var ts = dateTime - new DateTime(1970, 1, 1, 0, 0, 0, 0);
			return Convert.ToInt64(ts.TotalSeconds);
		}

		/// <summary>
		/// Generate a nonce
		/// </summary>
		/// <returns></returns>
		public virtual string GenerateNonce()
		{
			var nonceData = new StringBuilder();
			var data = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(String.Concat(DateTime.UtcNow.Ticks, random.Next(123400, 999999))));

			foreach (var d in data)
				nonceData.Append(d.ToString("x2").ToLower());

			return nonceData.ToString();
		}

		/// <summary>
		/// Parses the signature method.
		/// </summary>
		/// <param name="signatureMethod">The signature method.</param>
		/// <returns></returns>
		public static SignatureMethods ParseSignatureMethod(string signatureMethod)
		{
			switch (signatureMethod)
			{
				case PlainTextSignatureType:
					return SignatureMethods.PLAINTEXT;

				case HMACSHA1SignatureType:
					return SignatureMethods.HMACSHA1;

				case RSASHA1SignatureType:
					return SignatureMethods.RSASHA1;

				default:
					return SignatureMethods.UNKNOWN;
			}			
		}

		/// <summary>
		/// Comparer class used to perform the sorting of the query parameters
		/// </summary>
		protected class QueryParameterComparer : IComparer<QueryParameter>
		{
			public int Compare(QueryParameter x, QueryParameter y)
			{
				if (x.Name == y.Name)
					return string.Compare(x.Value, y.Value);

				return string.Compare(x.Name, y.Name);
			}
		}
	}
}
