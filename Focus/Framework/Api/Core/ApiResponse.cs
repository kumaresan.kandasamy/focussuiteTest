﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Net;
using System.Runtime.Serialization;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

#endregion

namespace Framework.Api
{
	[DataContract(Name = "response", Namespace = "")]
	public class ApiResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ApiResponse"/> class.
		/// </summary>
		public ApiResponse()
		{
			Status = ApiResponseStatus.Success;
			StatusCode = HttpStatusCode.OK;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ApiResponse"/> class.
		/// </summary>
		/// <param name="requestId">The request id.</param>
		/// <param name="timestamp">The timestamp.</param>
		public ApiResponse(Guid requestId, long timestamp) : this()
		{
			RequestId = requestId;
			Timestamp = timestamp;
		}

		[DataMember(Name = "requestId", Order = 0)]
		public Guid RequestId { get; set; }

		[DataMember(Name = "timestamp", Order = 1)]
		public long Timestamp { get; set; }

		[DataMember(Name = "status", Order = 2)]
		[JsonConverter(typeof(StringEnumConverter))]
		public ApiResponseStatus Status { get; set; }

		[DataMember(Name = "statusCode", Order = 3)]
		[JsonConverter(typeof(StringEnumConverter))]
		public HttpStatusCode StatusCode { get; set; }

		[DataMember(Name = "baseUri", Order = 4, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public string BaseUri { get; set; }

		[DataMember(Name = "errorCode", Order = 5, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public string ErrorCode { get; set; }

		[DataMember(Name = "developerMessage", Order = 6, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public string DeveloperMessage { get; set; }

		[DataMember(Name = "userMessage", Order = 7, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public string UserMessage { get; set; }

		[DataMember(Name = "moreInfo", Order = 8, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public string MoreInfo { get; set; }
	}

	[DataContract(Name = "response", Namespace = "")]
	public class ApiResponse<T> : ApiResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ApiResponse"/> class.
		/// </summary>
		public ApiResponse()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="ApiResponse"/> class.
		/// </summary>
		/// <param name="requestId">The request id.</param>
		/// <param name="timestamp">The timestamp.</param>
		/// <param name="result">The result.</param>
		public ApiResponse(Guid requestId, long timestamp, T result): base(requestId, timestamp)
		{
			Result = result;
		}

		[DataMember(Name = "result", Order = 10, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public T Result { get; set; }
	}

	public enum ApiResponseStatus
	{
		[EnumMember(Value = "success")]
		Success,

		[EnumMember(Value = "failure")]
		Failure
	}
}
