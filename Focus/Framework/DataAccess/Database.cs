﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace Framework.DataAccess
{
	public class Database : IDisposable
	{
		// DatabaseType, Connection & Transaction fields
		private DbConnection _connection;
		private DbTransaction _transaction;

		public Database(string connectionString, DatabaseType databaseType) : this(connectionString, databaseType, false) { }
		public Database(string connectionString, DatabaseType databaseType, bool beginTransaction)
		{
			ConnectionString = connectionString;
			DatabaseType = databaseType;

			switch (databaseType)
			{
				case DatabaseType.MySql:
					_connection = new MySqlConnection(connectionString);
					break;

				case DatabaseType.MsSql:
					_connection = new SqlConnection(connectionString);
					break;

				default:
					throw new Exception("Database type not recognised");
			}

			if (beginTransaction) BeginTransaction();
		}

		public string ConnectionString { get; private set; }
		public DatabaseType DatabaseType { get; private set; }
		public Exception ResultException { get; private set; }

		#region IDisposable Implementation

		/// <summary>
		/// Rolls back any pending transactions and closes the DB connection.
		/// </summary>
		public virtual void Dispose()
		{
			Close();

			if (_transaction != null) _transaction.Dispose();
			if (_connection != null) _connection.Dispose();

			_transaction = null;
			_connection = null;
		}

		#endregion

		#region Transaction Methods

		/// <summary>
		/// Begins a new database transaction.
		/// </summary>
		/// <returns>
		/// An object representing the new transaction.
		/// </returns>
		/// <seealso cref="Commit"/>
		/// <seealso cref="Rollback"/>
		public IDbTransaction BeginTransaction()
		{
			CheckTransactionState(false);
			Open();
			_transaction = _connection.BeginTransaction();
			return _transaction;
		}

		/// <summary>
		/// Begins a new database transaction with the specified 
		/// transaction isolation level.
		/// <seealso cref="Commit"/>
		/// <seealso cref="Rollback"/>
		/// </summary>
		/// <param name="isolationLevel">The transaction isolation level.</param>
		/// <returns>An object representing the new transaction.</returns>
		public IDbTransaction BeginTransaction(IsolationLevel isolationLevel)
		{
			CheckTransactionState(false);
			Open();
			_transaction = _connection.BeginTransaction(isolationLevel);
			return _transaction;
		}

		/// <summary>
		/// Commits the current database transaction.
		/// <seealso cref="BeginTransaction"/>
		/// <seealso cref="Rollback"/>
		/// </summary>
		public void Commit()
		{
			CheckTransactionState(true);

			try { _transaction.Commit(); }
			finally
			{
				_transaction.Dispose();
				_transaction = null;
			}
		}

		/// <summary>
		/// Rolls back the current transaction from a pending state.
		/// <seealso cref="BeginTransaction"/>
		/// <seealso cref="Commit"/>
		/// </summary>
		public void Rollback()
		{
			CheckTransactionState(true);

			try { _transaction.Rollback(); }
			finally
			{
				_transaction.Dispose();
				_transaction = null;
			}
		}

		// Checks the state of the current transaction
		private void CheckTransactionState(bool mustBeOpen)
		{
			if (mustBeOpen)
			{
				if (_transaction == null)
					throw new InvalidOperationException("Transaction is not open.");
			}
			else
			{
				if (_transaction != null)
					throw new InvalidOperationException("Transaction is already open.");
			}
		}

		#endregion

		#region Open / Close Methods

		/// <summary>
		/// Opens DB connection.
		/// </summary>
		public virtual void Open()
		{
			if (_connection == null) return;

			if (_connection.State == ConnectionState.Broken)
				_connection.Close();

			if (_connection.State == ConnectionState.Closed)
				_connection.Open();
		}

		/// <summary>
		/// Rolls back any pending transactions and closes the DB connection.
		/// An application can call the <c>Close</c> method more than
		/// one time without generating an exception.
		/// </summary>
		public virtual void Close()
		{
			try
			{
				if (_connection != null && _connection.State != ConnectionState.Closed)
					_connection.Close();
			}
			catch
			{ }
		}

		#endregion

		#region Parameter Methods

		/// <summary>
		/// Creates a Param.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public DatabaseParam Param(string name, object value)
		{
			return new DatabaseParam(name, value);
		}

		/// <summary>
		/// Creates a string param and trims to size.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		/// <param name="size">The size to trim.</param>
		/// <returns></returns>
		public DatabaseParam Param(string name, string value, int size)
		{
			if (String.IsNullOrEmpty(value) || value.Length <= size)
				return new DatabaseParam(name, value);

			return new DatabaseParam(name, value.Substring(0, size));
		}

		#endregion

		#region Data read section

		/// <summary>
		/// Read of individual object.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sql">The SQL.</param>
		/// <param name="create">The create.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns></returns>
		public T ExecuteObject<T>(string sql, Func<IDataReader, T> create, params DatabaseParam[] parameters)
		{
			try
			{
				using (var command = _connection.CreateCommand())
				{
					command.Connection = _connection;
					if (_transaction != null) command.Transaction = _transaction;
					command.SetCommand(sql, DatabaseType, parameters);

					Open();

					var t = default(T);
					var reader = command.ExecuteReader();
					if (reader.Read()) t = create(reader);

					return t;
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Execute Object failure.", ex);
			}
		}

		/// <summary>
		/// Read of list of objects.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sql">The SQL.</param>
		/// <param name="create">The create.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns></returns>
		public List<T> ExecuteList<T>(string sql, Func<IDataReader, T> create, params DatabaseParam[] parameters)
		{
			try
			{
				using (var command = _connection.CreateCommand())
				{
					command.Connection = _connection;
					if (_transaction != null) command.Transaction = _transaction;
					command.SetCommand(sql, DatabaseType, parameters);

					Open();

					var list = new List<T>();
					var reader = command.ExecuteReader();

					while (reader.Read())
						list.Add(create(reader));

					list.TrimExcess();

					return list;
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Execute List failure.", ex);
			}
		}

    /// <summary>
    /// Gets a data reader.
    /// </summary>
    /// <param name="sql">The SQL.</param>
    /// <param name="commandTimeout">The command timeout.</param>
    /// <param name="parameters">The parameters.</param>
    /// <returns></returns>
    /// <exception cref="System.Exception">Execute Reader failure.</exception>
    public DbDataReader ExecuteReader(string sql, int commandTimeout, params DatabaseParam[] parameters)
    {
      try
      {
        using (var command = _connection.CreateCommand())
        {
          command.Connection = _connection;
          if (_transaction != null) command.Transaction = _transaction;
          if (commandTimeout >= 0) command.CommandTimeout = commandTimeout;
          command.SetCommand(sql, DatabaseType, parameters);

          Open();

          return command.ExecuteReader();
        }
      }
      catch (Exception ex)
      {
        throw new Exception("Execute Reader failure.", ex);
      }
    }

		/// <summary>
		/// Gets a data reader.
		/// </summary>
		/// <param name="sql">The SQL.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns></returns>
		public DbDataReader ExecuteReader(string sql, params DatabaseParam[] parameters)
		{
		  return ExecuteReader(sql, -1, parameters);
		}

		/// <summary>
		/// Gets a record count.
		/// </summary>
		/// <param name="sql">The SQL.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns></returns>
		public int ExecuteCount(string sql, params DatabaseParam[] parameters)
		{
			return ExecuteScalar(sql, parameters).AsInt();
		}

		/// <summary>
		/// Gets any scalar value from the database.
		/// </summary>
		/// <param name="sql">The SQL.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns></returns>
		public object ExecuteScalar(string sql, params DatabaseParam[] parameters)
		{
			try
			{
				using (var command = _connection.CreateCommand())
				{
					command.Connection = _connection;
					if (_transaction != null) command.Transaction = _transaction;
					command.SetCommand(sql, DatabaseType, parameters);

					Open();

					return command.ExecuteScalar();
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Execute Scalar failure.", ex);
			}
		}

		#endregion

		#region Data update section

		/// <summary>
		/// Inserts an item into the database
		/// </summary>
		/// <param name="sql">The SQL.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns>The number of rows affected.</returns>
		public int Insert(string sql, params DatabaseParam[] parameters)
		{
			try
			{
				using (var command = _connection.CreateCommand())
				{
					command.Connection = _connection;
					if (_transaction != null) command.Transaction = _transaction;
					command.SetCommand(sql, DatabaseType, parameters);

					Open();

					return command.ExecuteNonQuery();
				}
			}
			catch (Exception ex)
			{
				if (_connection != null && _connection.State == ConnectionState.Open &&
						DatabaseType == DatabaseType.MySql && sql.ToLowerInvariant().Contains("lock tables"))
				{
					using (var command = _connection.CreateCommand())
					{
						command.Connection = _connection;
						if (_transaction != null) command.Transaction = _transaction;
						command.CommandText = "UNLOCK TABLES;";
						command.ExecuteNonQuery();
					}
				}

				throw new Exception("Insert failure.", ex);
			}
		}

		/// <summary>
		/// Inserts an item into the database and get the identity column value
		/// </summary>
		/// <param name="sql">The SQL.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns>The identity of the inserted record.</returns>
		public object InsertAndGetId(string sql, params DatabaseParam[] parameters)
		{
			try
			{
				using (var command = _connection.CreateCommand())
				{
					command.Connection = _connection;
					if (_transaction != null) command.Transaction = _transaction;
					command.SetCommand(sql.AppendIdentitySelect(DatabaseType), DatabaseType, parameters);

					Open();

					return command.ExecuteScalar();
				}
			}
			catch (Exception ex)
			{
				if (_connection != null && _connection.State == ConnectionState.Open &&
						DatabaseType == DatabaseType.MySql && sql.ToLowerInvariant().Contains("lock tables"))
				{
					using (var command = _connection.CreateCommand())
					{
						command.Connection = _connection;
						if (_transaction != null) command.Transaction = _transaction;
						command.CommandText = "UNLOCK TABLES;";
						command.ExecuteNonQuery();
					}
				}

				throw new Exception("Insert and get Id failure.", ex);
			}
		}

		/// <summary>
		/// Updates an item in the database
		/// </summary>
		/// <param name="sql">The SQL.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns>The number of rows affected.</returns>
		public int Update(string sql, params DatabaseParam[] parameters)
		{
			try
			{
				using (var command = _connection.CreateCommand())
				{
					command.Connection = _connection;
					if (_transaction != null) command.Transaction = _transaction;
					command.SetCommand(sql, DatabaseType, parameters);

					Open();

					return command.ExecuteNonQuery();
				}
			}
			catch (Exception ex)
			{
				if (_connection != null && _connection.State == ConnectionState.Open &&
						DatabaseType == DatabaseType.MySql && sql.ToLowerInvariant().Contains("lock tables"))
				{
					using (var command = _connection.CreateCommand())
					{
						command.Connection = _connection;
						if (_transaction != null) command.Transaction = _transaction;
						command.CommandText = "UNLOCK TABLES;";
						command.ExecuteNonQuery();
					}
				}

				throw new Exception("Update failure.", ex);
			}
		}

		/// <summary>
		/// Deletes an item from the database.
		/// </summary>
		/// <param name="sql">The SQL.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns>The number of rows affected.</returns>
		public int Delete(string sql, params DatabaseParam[] parameters)
		{
			return Update(sql, parameters);
		}

		#endregion


		/// <summary>
		/// Read of individual object.
		/// </summary>
		/// <param name="sql">The SQL.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns></returns>
		public int ExecuteBulkStatement(string sql, params DatabaseParam[] parameters)
		{
			try
			{
				using (var command = _connection.CreateCommand())
				{
					command.Connection = _connection;
					if (_transaction != null) command.Transaction = _transaction;
					command.SetCommand(sql, DatabaseType, parameters);

					Open();

					var retVal = command.ExecuteNonQuery();

					return retVal;
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Execute Object failure.", ex);
			}
		}

		public DbDataReader ExecuteBulkStatement(string sql, int commandTimeout, params DatabaseParam[] parameters)
		{
			try
			{
				using (var command = _connection.CreateCommand())
				{
					command.Connection = _connection;
					if (_transaction != null) command.Transaction = _transaction;
					if (commandTimeout >= 0) command.CommandTimeout = commandTimeout;
					command.SetCommand(sql, DatabaseType, parameters);

					Open();

					return command.ExecuteReader();
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Execute bulk statement failure.", ex);
			}
		}

	}
}
