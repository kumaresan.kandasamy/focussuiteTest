﻿namespace Framework.DataAccess
{
	public class DatabaseParam
	{
		public string Name { get; private set; }
		public object Value { get; internal set; }

		public DatabaseParam(string name, object value)
		{
			Name = name;
			Value = value;
		}
	}
}