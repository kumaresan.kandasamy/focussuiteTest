﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text.RegularExpressions;

namespace Framework.DataAccess
{
	public static class DbExtensions
	{
		/// <summary>
		/// Transform object into Identity data type (integer).
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <returns>Identity value.</returns>
		public static int AsId(this object item)
		{
			return AsInt(item);
		}

		/// <summary>
		/// Transform object into Identity data type (integer).
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <param name="defaultId">Optional default value is -1.</param>
		/// <returns>Identity value.</returns>
		public static int AsId(this object item, int defaultId)
		{
			return AsInt(item, defaultId);
		}

		/// <summary>
		/// Transform object into Identity data type (long).
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <returns>Identity value.</returns>
		public static long AsId64(this object item)
		{
			return AsInt64(item);
		}

		/// <summary>
		/// Transform object into Identity data type (long).
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <param name="defaultId">Optional default value is -1.</param>
		/// <returns>Identity value.</returns>
		public static long AsId64(this object item, long defaultId)
		{
			return AsInt64(item, defaultId);
		}

		/// <summary>
		/// Transform object into an integer data type (integer).
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <returns>Identity value.</returns>
		public static int AsInt(this object item)
		{
			return AsInt(item, default(int));
		}

		/// <summary>
		/// Transform object into an integer data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <param name="defaultInt">The default int.</param>
		/// <returns>The integer value.</returns>
		public static int AsInt(this object item, int defaultInt)
		{
			if (item == null)
				return defaultInt;

			if (item is int)
				return (int)item;

			int result;
			return !int.TryParse(item.ToString(), out result) ? defaultInt : result;
		}

		/// <summary>
		/// Transform object into an integer data type (integer).
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <returns>Identity value.</returns>
		public static long AsInt64(this object item)
		{
			return AsInt64(item, default(long));
		}

		/// <summary>
		/// Transform object into an integer data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <param name="defaultInt">The default int.</param>
		/// <returns>The integer value.</returns>
		public static long AsInt64(this object item, long defaultInt)
		{
			if (item == null)
				return defaultInt;

			if (item is long)
				return (long)item;

			long result;
			return !long.TryParse(item.ToString(), out result) ? defaultInt : result;
		}

		/// <summary>
		/// Transform object into an unsigned integer data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <returns>Identity value.</returns>
		public static uint AsUInt(this object item)
		{
			return AsUInt(item, default(uint));
		}

		/// <summary>
		/// Transform object into an unsigned integer data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <param name="defaultUInt">The default U int.</param>
		/// <returns>The integer value.</returns>
		public static uint AsUInt(this object item, uint defaultUInt)
		{
			if (item == null)
				return defaultUInt;

			if (item is uint)
				return (uint)item;

			uint result;
			return !uint.TryParse(item.ToString(), out result) ? defaultUInt : result;
		}

		/// <summary>
		/// Transform object into double data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <returns>The double value.</returns>
		public static double AsDouble(this object item)
		{
			return AsDouble(item, default(double));
		}

		/// <summary>
		/// Transform object into double data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <param name="defaultDouble">The default double.</param>
		/// <returns>The double value.</returns>
		public static double AsDouble(this object item, double defaultDouble)
		{
			if (item == null)
				return defaultDouble;

			if (item is double)
				return (double)item;

			double result;
			return !double.TryParse(item.ToString(), out result) ? defaultDouble : result;
		}

		/// <summary>
		/// Transform object into decimal data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <returns>The decimal value.</returns>
		public static decimal AsDecimal(this object item)
		{
			return AsDecimal(item, default(decimal));
		}

		/// <summary>
		/// Transform object into decimal data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <param name="defaultDecimal">The default decimal.</param>
		/// <returns>The decimal value.</returns>
		public static decimal AsDecimal(this object item, decimal defaultDecimal)
		{
			if (item == null)
				return defaultDecimal;

			if (item is decimal)
				return (decimal)item;

			decimal result;
			return !decimal.TryParse(item.ToString(), out result) ? defaultDecimal : result;
		}

		/// <summary>
		/// Transform object into float data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <returns>The float value.</returns>
		public static float AsFloat(this object item)
		{
			return AsFloat(item, default(float));
		}

		/// <summary>
		/// Transform object into float data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <param name="defaultFloat">The default float.</param>
		/// <returns>The float value.</returns>
		public static float AsFloat(this object item, float defaultFloat)
		{
			if (item == null)
				return defaultFloat;

			if (item is float)
				return (float)item;

			float result;
			return !float.TryParse(item.ToString(), out result) ? defaultFloat : result;
		}


		/// <summary>
		/// Transform object into string data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <returns>The string value.</returns>
		public static string AsString(this object item)
		{
			return AsString(item, default(string));
		}

		/// <summary>
		/// Transform object into string data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <param name="defaultString">The default string.</param>
		/// <returns>The string value.</returns>
		public static string AsString(this object item, string defaultString)
		{
			if (item == null || item.Equals(DBNull.Value))
				return defaultString;

			return item.ToString().Trim();
		}

		public static string AsSqlSafeString(this string text)
		{
			return string.IsNullOrEmpty(text) ? "" : text.Replace("'", "''");
		}

		/// <summary>
		/// Transform object into DateTime data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <returns>The DateTime value.</returns>
		public static DateTime AsDateTime(this object item)
		{
			return AsDateTime(item, default(DateTime));
		}

		/// <summary>
		/// Transform object into DateTime data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <param name="defaultDateTime">The default date time.</param>
		/// <returns>The DateTime value.</returns>
		public static DateTime AsDateTime(this object item, DateTime defaultDateTime)
		{
			if (item == null || string.IsNullOrEmpty(item.ToString()))
				return defaultDateTime;

			if (item is DateTime)
				return (DateTime)item;

			DateTime result;

			return !DateTime.TryParse(item.ToString(), out result) ? defaultDateTime : result;
		}

		/// <summary>
		/// Transform object into bool data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <returns>The bool value.</returns>
		public static bool AsBool(this object item)
		{
			return AsBool(item, default(bool));
		}

		/// <summary>
		/// Transform object into bool data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <param name="defaultBool">if set to <c>true</c> [default bool].</param>
		/// <returns>The bool value.</returns>
		public static bool AsBool(this object item, bool defaultBool)
		{
			return item == null ? defaultBool : new List<string> { "yes", "y", "true" }.Contains(item.ToString().ToLower());
		}

		/// <summary>
		/// Transform string into byte array.
		/// </summary>
		/// <param name="s">The object to be transformed.</param>
		/// <returns>The transformed byte array.</returns>
		public static byte[] AsByteArray(this string s)
		{
			return (string.IsNullOrEmpty(s) ? null : Convert.FromBase64String(s));
		}

		/// <summary>
		/// Transform object into base64 string.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <returns>The base64 string value.</returns>
		public static string AsBase64String(this object item)
		{
			return (item == null ? null : Convert.ToBase64String((byte[])item));
		}

		/// <summary>
		/// Transform object into Guid data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <returns>The Guid value.</returns>
		public static Guid AsGuid(this object item)
		{
			try { return new Guid(item.ToString()); }
			catch { return Guid.Empty; }
		}

		/// <summary>
		/// Concatenates SQL and ORDER BY clauses into a single string. 
		/// </summary>
		/// <param name="sql">The SQL string</param>
		/// <param name="sortExpression">The Sort Expression.</param>
		/// <returns>Contatenated SQL Statement.</returns>
		public static string OrderBy(this string sql, string sortExpression)
		{
			if (string.IsNullOrEmpty(sortExpression))
				return sql;

			return sql + " ORDER BY " + sortExpression;
		}

		/// <summary>
		/// Takes an enumerable source and returns a comma separate string.
		/// Handy to build SQL Statements (for example with IN () statements) from object collections.
		/// </summary>
		/// <typeparam name="T">The Enumerable type</typeparam>
		/// <typeparam name="TU">The original data type (typically identities - int).</typeparam>
		/// <param name="source">The enumerable input collection.</param>
		/// <param name="func">The function that extracts property value in object.</param>
		/// <returns>The comma separated string.</returns>
		public static string CommaSeparate<T, TU>(this IEnumerable<T> source, Func<T, TU> func)
		{
			return string.Join(",", source.Select(s => func(s).ToString()).ToArray());
		}

		/// <summary>
		/// Extension method: Appends the db specific syntax to sql string 
		/// for retrieving newly generated identity (autonumber) value.
		/// </summary>
		/// <param name="sql">The sql string to which to append syntax.</param>
		/// <param name="databaseType">Type of the database.</param>
		/// <returns>Sql string with identity select.</returns>
		internal static string AppendIdentitySelect(this string sql, DatabaseType databaseType)
		{
			switch (databaseType)
			{
				case DatabaseType.MsSql: return sql + ";SELECT SCOPE_IDENTITY();";
				case DatabaseType.MySql: return sql + ";SELECT LAST_INSERT_ID();";
				default: return sql + ";SELECT @@IDENTITY;";
			}
		}

		/// <summary>
		/// Extention method. Updates SQL and adds query parameters to command object.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="sql">The SQL.</param>
		/// <param name="databaseType">Type of the database.</param>
		/// <param name="parameters">The parameters.</param>
		internal static void SetCommand(this DbCommand command, string sql, DatabaseType databaseType, params DatabaseParam[] parameters)
		{
			if (parameters != null && parameters.Length > 0)
			{
				string paramFixer;

				switch (databaseType)
				{
					case DatabaseType.MsSql:
						paramFixer = "@$1";
						break;

					case DatabaseType.MySql:
						paramFixer = "?$1";
						break;

					default:
						paramFixer = "?";
						break;
				}

				sql = Regex.Replace(sql, @"\{([a-zA-Z0-9]{1,})\}", paramFixer);
			}

			if (parameters != null && parameters.Length > 0)
			{
				foreach (var t in parameters)
				{
					// No empty strings to the database
					if (t.Value is string && ((string)t.Value) == String.Empty)
						t.Value = null;

					var dbParameter = command.CreateParameter();
					dbParameter.ParameterName = t.Name;
					dbParameter.Value = t.Value ?? DBNull.Value;

					command.Parameters.Add(dbParameter);
				}
			}

			command.CommandText = sql;
		}
	}
}