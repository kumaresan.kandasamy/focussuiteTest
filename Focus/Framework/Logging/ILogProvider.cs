﻿
namespace Framework.Logging
{
	/// <summary>
	/// Defines a single method to write requested log events to an output media.
	/// </summary>
	public interface ILogProvider
	{
		/// <summary>
		/// Write a log request to a given output media.
		/// </summary>
		/// <param name="sender">Sender of the log request.</param>
		/// <param name="e">Parameters of the log request.</param>
		void Log(object sender, LogEventArgs e);

		/// <summary>
		/// Write a profile request to a given output media.
		/// </summary>
		/// <param name="sender">Sender of the profile request.</param>
		/// <param name="e">Parameters of the profile request.</param>
		void Profile(object sender, ProfileEventArgs e);
	}
}
