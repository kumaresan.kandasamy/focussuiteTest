﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

#endregion

namespace Framework.Logging
{
	[Serializable]
	public class WebRequestLog
	{
		public Uri Url { get; set; }

		public Dictionary<string, string> FormVars { get; set; }

		public Dictionary<string, string> Cookies { get; set; }

		/// <summary>
		/// Constructor from a http request
		/// </summary>
		/// <param name="request">The http request</param>
		public WebRequestLog(HttpRequest request)
		{
			var redactValue = new Func<string, bool>(key => key.IndexOf("password", StringComparison.OrdinalIgnoreCase) != -1);

			Url = request.Url;
			FormVars = ToDictionary(request.Form, redactValue);
			Cookies = ToDictionary(request.Cookies);
		}

		/// <summary>
		/// Manual constructor
		/// </summary>
		/// <param name="url">The full url</param>
		/// <param name="formVariables">A dictionary of form variables</param>
		/// <param name="cookies">A dictionary of cookies</param>
		public WebRequestLog(Uri url, Dictionary<string, string> formVariables, Dictionary<string, string> cookies)
		{
			Url = url;
			FormVars = formVariables;
			Cookies = cookies;
		}

		/// <summary>
		/// Convests a cookie collection to a dictionary
		/// </summary>
		/// <param name="cookies">The cookie collection</param>
		/// <returns>A dictionary of name / value strings</returns>
		private static Dictionary<string, string> ToDictionary(HttpCookieCollection cookies)
		{
			try
			{
				var count = cookies.Count;
				return count > 0
					? Enumerable.Range(0, cookies.Count).Select(i => cookies[i]).ToDictionary(i => i.Name, i => i.Value)
					: new Dictionary<string, string>();
			}
			catch (Exception e)
			{
				return new Dictionary<string, string> { { "Failed to retrieve", e.Message } };
			}
		}

		/// <summary>
		/// Convests a name value collection to a dictionary
		/// </summary>
		/// <param name="nameValueCollection">The name value collection</param>
		/// <returns>A dictionary of name / value strings</returns>
		private static Dictionary<string, string> ToDictionary(NameValueCollection nameValueCollection, Func<string, bool> redactValue = null)
		{
			IEnumerable<string> keys;

			try
			{
				keys = nameValueCollection.AllKeys.Where(k => k != null);
			}
			catch (Exception e)
			{
				return new Dictionary<string, string> { { "Failed to retrieve", e.Message } };
			}

			var dictionary = new Dictionary<string, string>();
			foreach (var key in keys)
			{
				try
				{
					var value = nameValueCollection[key];
					if (redactValue != null && redactValue(value))
					{
						value = Regex.Replace(value, ".", "*");
					}
					dictionary.Add(key, value);
				}
				catch (Exception e)
				{
					dictionary.Add(key, e.Message);
				}
			}

			return dictionary;
		}
	}
}
