using System;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Validation;
using Mindscape.LightSpeed.Linq;

namespace Framework.Logging.Providers
{
  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [System.Runtime.Serialization.DataContract]
  [Table("Log.LogEvent")]
  internal partial class LogEvent : Entity<long>
  {
    #region Fields
  
    private System.Guid _sessionId;
    private System.Guid _requestId;
    private long _userId;
    [ValidateLength(0, 50)]
    private string _server;
    [ValidateLength(0, 30)]
    private string _application;
    private System.DateTime _eventDate;
    [ValueField]
    private Framework.Logging.LogSeverity _severity;
    [ValidateLength(0, 1024)]
    private string _message;
    private string _exception;
    [ValidateLength(0, 200)]
    private string _declaringType;
    [ValidateLength(0, 200)]
    private string _method;
    private string _parameters;
    private string _logItems;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SessionId entity attribute.</summary>
    public const string SessionIdField = "SessionId";
    /// <summary>Identifies the RequestId entity attribute.</summary>
    public const string RequestIdField = "RequestId";
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the Server entity attribute.</summary>
    public const string ServerField = "Server";
    /// <summary>Identifies the Application entity attribute.</summary>
    public const string ApplicationField = "Application";
    /// <summary>Identifies the EventDate entity attribute.</summary>
    public const string EventDateField = "EventDate";
    /// <summary>Identifies the Severity entity attribute.</summary>
    public const string SeverityField = "Severity";
    /// <summary>Identifies the Message entity attribute.</summary>
    public const string MessageField = "Message";
    /// <summary>Identifies the Exception entity attribute.</summary>
    public const string ExceptionField = "Exception";
    /// <summary>Identifies the DeclaringType entity attribute.</summary>
    public const string DeclaringTypeField = "DeclaringType";
    /// <summary>Identifies the Method entity attribute.</summary>
    public const string MethodField = "Method";
    /// <summary>Identifies the Parameters entity attribute.</summary>
    public const string ParametersField = "Parameters";
    /// <summary>Identifies the LogItems entity attribute.</summary>
    public const string LogItemsField = "LogItems";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Guid SessionId
    {
      get { return Get(ref _sessionId, "SessionId"); }
      set { Set(ref _sessionId, value, "SessionId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Guid RequestId
    {
      get { return Get(ref _requestId, "RequestId"); }
      set { Set(ref _requestId, value, "RequestId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Server
    {
      get { return Get(ref _server, "Server"); }
      set { Set(ref _server, value, "Server"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Application
    {
      get { return Get(ref _application, "Application"); }
      set { Set(ref _application, value, "Application"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime EventDate
    {
      get { return Get(ref _eventDate, "EventDate"); }
      set { Set(ref _eventDate, value, "EventDate"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Framework.Logging.LogSeverity Severity
    {
      get { return Get(ref _severity, "Severity"); }
      set { Set(ref _severity, value, "Severity"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Message
    {
      get { return Get(ref _message, "Message"); }
      set { Set(ref _message, value, "Message"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Exception
    {
      get { return Get(ref _exception, "Exception"); }
      set { Set(ref _exception, value, "Exception"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string DeclaringType
    {
      get { return Get(ref _declaringType, "DeclaringType"); }
      set { Set(ref _declaringType, value, "DeclaringType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Method
    {
      get { return Get(ref _method, "Method"); }
      set { Set(ref _method, value, "Method"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Parameters
    {
      get { return Get(ref _parameters, "Parameters"); }
      set { Set(ref _parameters, value, "Parameters"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string LogItems
    {
      get { return Get(ref _logItems, "LogItems"); }
      set { Set(ref _logItems, value, "LogItems"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [System.Runtime.Serialization.DataContract]
  [Table("Log.ProfileEvent")]
  internal partial class ProfileEvent : Entity<long>
  {
    #region Fields
  
    private System.Guid _sessionId;
    private System.Guid _requestId;
    private long _userId;
    [ValidateLength(0, 50)]
    private string _server;
    [ValidateLength(0, 30)]
    private string _application;
    private System.DateTime _inTime;
    private System.DateTime _outTime;
    private long _milliseconds;
    [ValidateLength(0, 200)]
    private string _declaringType;
    [ValidateLength(0, 200)]
    private string _method;
    private string _parameters;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SessionId entity attribute.</summary>
    public const string SessionIdField = "SessionId";
    /// <summary>Identifies the RequestId entity attribute.</summary>
    public const string RequestIdField = "RequestId";
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the Server entity attribute.</summary>
    public const string ServerField = "Server";
    /// <summary>Identifies the Application entity attribute.</summary>
    public const string ApplicationField = "Application";
    /// <summary>Identifies the InTime entity attribute.</summary>
    public const string InTimeField = "InTime";
    /// <summary>Identifies the OutTime entity attribute.</summary>
    public const string OutTimeField = "OutTime";
    /// <summary>Identifies the Milliseconds entity attribute.</summary>
    public const string MillisecondsField = "Milliseconds";
    /// <summary>Identifies the DeclaringType entity attribute.</summary>
    public const string DeclaringTypeField = "DeclaringType";
    /// <summary>Identifies the Method entity attribute.</summary>
    public const string MethodField = "Method";
    /// <summary>Identifies the Parameters entity attribute.</summary>
    public const string ParametersField = "Parameters";


    #endregion
    
    #region Properties



    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Guid SessionId
    {
      get { return Get(ref _sessionId, "SessionId"); }
      set { Set(ref _sessionId, value, "SessionId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Guid RequestId
    {
      get { return Get(ref _requestId, "RequestId"); }
      set { Set(ref _requestId, value, "RequestId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Server
    {
      get { return Get(ref _server, "Server"); }
      set { Set(ref _server, value, "Server"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Application
    {
      get { return Get(ref _application, "Application"); }
      set { Set(ref _application, value, "Application"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime InTime
    {
      get { return Get(ref _inTime, "InTime"); }
      set { Set(ref _inTime, value, "InTime"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime OutTime
    {
      get { return Get(ref _outTime, "OutTime"); }
      set { Set(ref _outTime, value, "OutTime"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long Milliseconds
    {
      get { return Get(ref _milliseconds, "Milliseconds"); }
      set { Set(ref _milliseconds, value, "Milliseconds"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string DeclaringType
    {
      get { return Get(ref _declaringType, "DeclaringType"); }
      set { Set(ref _declaringType, value, "DeclaringType"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Method
    {
      get { return Get(ref _method, "Method"); }
      set { Set(ref _method, value, "Method"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Parameters
    {
      get { return Get(ref _parameters, "Parameters"); }
      set { Set(ref _parameters, value, "Parameters"); }
    }

    #endregion
  }




  /// <summary>
  /// Provides a strong-typed unit of work for working with the Log model.
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  internal partial class LogUnitOfWork : Mindscape.LightSpeed.UnitOfWork
  {

    internal System.Linq.IQueryable<LogEvent> LogEvents
    {
      get { return this.Query<LogEvent>(); }
    }
    
    internal System.Linq.IQueryable<ProfileEvent> ProfileEvents
    {
      get { return this.Query<ProfileEvent>(); }
    }
    
  }

#if LS3_DTOS

  namespace Contracts.Data
  {
    [System.Runtime.Serialization.DataContract(Name="LogDtoBase")]
    [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
    public partial class LogDtoBase
    {
    }

    [System.Runtime.Serialization.DataContract(Name="LogEvent")]
    [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
    public partial class LogEventDto : LogDtoBase
    {
      [System.Runtime.Serialization.DataMember]
      public System.Guid SessionId { get; set; }
      [System.Runtime.Serialization.DataMember]
      public System.Guid RequestId { get; set; }
      [System.Runtime.Serialization.DataMember]
      public long UserId { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string Server { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string Application { get; set; }
      [System.Runtime.Serialization.DataMember]
      public System.DateTime EventDate { get; set; }
      [System.Runtime.Serialization.DataMember]
      public Framework.Logging.LogSeverity Severity { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string Message { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string Exception { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string DeclaringType { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string Method { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string Parameters { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string LogItems { get; set; }
    }

    [System.Runtime.Serialization.DataContract(Name="ProfileEvent")]
    [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
    public partial class ProfileEventDto : LogDtoBase
    {
      [System.Runtime.Serialization.DataMember]
      public System.Guid SessionId { get; set; }
      [System.Runtime.Serialization.DataMember]
      public System.Guid RequestId { get; set; }
      [System.Runtime.Serialization.DataMember]
      public long UserId { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string Server { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string Application { get; set; }
      [System.Runtime.Serialization.DataMember]
      public System.DateTime InTime { get; set; }
      [System.Runtime.Serialization.DataMember]
      public System.DateTime OutTime { get; set; }
      [System.Runtime.Serialization.DataMember]
      public long Milliseconds { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string DeclaringType { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string Method { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string Parameters { get; set; }
    }


    
    [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
    public static partial class LogDtoExtensions
    {
      static partial void CopyLogDtoBase(Entity entity, LogDtoBase dto);
      static partial void CopyLogDtoBase(LogDtoBase dto, Entity entity);

      static partial void BeforeCopyLogEvent(LogEvent entity, LogEventDto dto);
      static partial void AfterCopyLogEvent(LogEvent entity, LogEventDto dto);
      static partial void BeforeCopyLogEvent(LogEventDto dto, LogEvent entity);
      static partial void AfterCopyLogEvent(LogEventDto dto, LogEvent entity);
      
      private static void CopyLogEvent(LogEvent entity, LogEventDto dto)
      {
        BeforeCopyLogEvent(entity, dto);
        CopyLogDtoBase(entity, dto);
        dto.SessionId = entity.SessionId;
        dto.RequestId = entity.RequestId;
        dto.UserId = entity.UserId;
        dto.Server = entity.Server;
        dto.Application = entity.Application;
        dto.EventDate = entity.EventDate;
        dto.Severity = entity.Severity;
        dto.Message = entity.Message;
        dto.Exception = entity.Exception;
        dto.DeclaringType = entity.DeclaringType;
        dto.Method = entity.Method;
        dto.Parameters = entity.Parameters;
        dto.LogItems = entity.LogItems;
        AfterCopyLogEvent(entity, dto);
      }
      
      private static void CopyLogEvent(LogEventDto dto, LogEvent entity)
      {
        BeforeCopyLogEvent(dto, entity);
        CopyLogDtoBase(dto, entity);
        entity.SessionId = dto.SessionId;
        entity.RequestId = dto.RequestId;
        entity.UserId = dto.UserId;
        entity.Server = dto.Server;
        entity.Application = dto.Application;
        entity.EventDate = dto.EventDate;
        entity.Severity = dto.Severity;
        entity.Message = dto.Message;
        entity.Exception = dto.Exception;
        entity.DeclaringType = dto.DeclaringType;
        entity.Method = dto.Method;
        entity.Parameters = dto.Parameters;
        entity.LogItems = dto.LogItems;
        AfterCopyLogEvent(dto, entity);
      }
      
      internal static LogEventDto AsDto(this LogEvent entity)
      {
        LogEventDto dto = new LogEventDto();
        CopyLogEvent(entity, dto);
        return dto;
      }
      
      internal static LogEvent CopyTo(this LogEventDto source, LogEvent entity)
      {
        CopyLogEvent(source, entity);
        return entity;
      }

      static partial void BeforeCopyProfileEvent(ProfileEvent entity, ProfileEventDto dto);
      static partial void AfterCopyProfileEvent(ProfileEvent entity, ProfileEventDto dto);
      static partial void BeforeCopyProfileEvent(ProfileEventDto dto, ProfileEvent entity);
      static partial void AfterCopyProfileEvent(ProfileEventDto dto, ProfileEvent entity);
      
      private static void CopyProfileEvent(ProfileEvent entity, ProfileEventDto dto)
      {
        BeforeCopyProfileEvent(entity, dto);
        CopyLogDtoBase(entity, dto);
        dto.SessionId = entity.SessionId;
        dto.RequestId = entity.RequestId;
        dto.UserId = entity.UserId;
        dto.Server = entity.Server;
        dto.Application = entity.Application;
        dto.InTime = entity.InTime;
        dto.OutTime = entity.OutTime;
        dto.Milliseconds = entity.Milliseconds;
        dto.DeclaringType = entity.DeclaringType;
        dto.Method = entity.Method;
        dto.Parameters = entity.Parameters;
        AfterCopyProfileEvent(entity, dto);
      }
      
      private static void CopyProfileEvent(ProfileEventDto dto, ProfileEvent entity)
      {
        BeforeCopyProfileEvent(dto, entity);
        CopyLogDtoBase(dto, entity);
        entity.SessionId = dto.SessionId;
        entity.RequestId = dto.RequestId;
        entity.UserId = dto.UserId;
        entity.Server = dto.Server;
        entity.Application = dto.Application;
        entity.InTime = dto.InTime;
        entity.OutTime = dto.OutTime;
        entity.Milliseconds = dto.Milliseconds;
        entity.DeclaringType = dto.DeclaringType;
        entity.Method = dto.Method;
        entity.Parameters = dto.Parameters;
        AfterCopyProfileEvent(dto, entity);
      }
      
      internal static ProfileEventDto AsDto(this ProfileEvent entity)
      {
        ProfileEventDto dto = new ProfileEventDto();
        CopyProfileEvent(entity, dto);
        return dto;
      }
      
      internal static ProfileEvent CopyTo(this ProfileEventDto source, ProfileEvent entity)
      {
        CopyProfileEvent(source, entity);
        return entity;
      }


    }

  }

#endif
}
