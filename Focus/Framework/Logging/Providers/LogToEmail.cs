﻿using System;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace Framework.Logging.Providers
{
	/// <summary>
	/// Logs a log event by sending it as an email.
	/// 
	/// Note: This will only send an email on Error or Fatal log events.
	///				No profiling is sent via email.
	/// </summary>
	public class LogToEmail : ILogProvider
	{
		private readonly string _application;
		private readonly string _to;

		/// <summary>
		/// Constructor for the EmailLogProvider class
		/// </summary>
		/// <param name="application">The application.</param>
		/// <param name="to">To email address.</param>
		public LogToEmail(string application, string to)
		{
			_application = application;
			_to = to;
		}

		/// <summary>
		/// Sends a log request via email.
		/// </summary>
		/// <remarks>
		/// Actual email 'Send' calls are commented out.
		/// Uncomment if you have the proper email privileges.
		/// </remarks>
		/// <param name="sender">Sender of the log request.</param>
		/// <param name="e">Parameters of the log request.</param>
		public void Log(object sender, LogEventArgs e)
		{
			if (e.Severity == LogSeverity.Error || e.Severity == LogSeverity.Fatal)
			{
				var exceptionDetails = FormatException(e, _application);

				try
				{
					SendEmail(_to, _application + " [" + e.Server + "] " + DateTime.Now.ToString("yyyyMMdd-HH:mm"), exceptionDetails, true);
				}
				catch (Exception loggingException)
				{
					var body = "There has been a critical exception in the " + _application + " application, more than likely the SQL Server is unavailable." + Environment.NewLine +
										 "Critical exception: " + loggingException + Environment.NewLine + Environment.NewLine +
										 "Initial exception: " + Environment.NewLine + Environment.NewLine + exceptionDetails;

					SendEmail(_to, _application + " [" + e.Server + "] " + DateTime.Now.ToString("yyyyMMdd-HH:mm") + " - Critical Exception", body);
				}				
			}
		}

		/// <summary>
		/// Write a profile request to a given output media.
		/// </summary>
		/// <param name="sender">Sender of the profile request.</param>
		/// <param name="e">Parameters of the profile request.</param>
		public void Profile(object sender, ProfileEventArgs e)
		{
			// Do Nothing
		}

		#region Helper Methods

		/// <summary>
		/// Sends the email.
		/// </summary>
		/// <param name="to">To.</param>
		/// <param name="subject">The subject.</param>
		/// <param name="body">The body.</param>
		/// <param name="isBodyHtml">if set to <c>true</c> [is body HTML].</param>
		private static void SendEmail(string to, string subject, string body, bool isBodyHtml = false)
		{
			try
			{
				var message = new MailMessage();

				if (String.IsNullOrEmpty(to))
					return;

				var toAddresses = to.Split(',',';');

				foreach (var toAddress in toAddresses)
					message.To.Add(new MailAddress(toAddress));

				message.Subject = subject;
				message.Body = body;

				message.IsBodyHtml = isBodyHtml;

				var smtpClient = new SmtpClient();
				smtpClient.Send(message);
			}
			catch
			{ }
		}

		/// <summary>
		/// Formats the exception.
		/// </summary>
		/// <param name="e">The <see cref="Framework.Logging.LogEventArgs"/> instance containing the event data.</param>
		/// <param name="application">Name of the application.</param>
		/// <returns></returns>
		private static string FormatException(LogEventArgs e, string application)
		{
			var sb = new StringBuilder();
			sb.Append(@"<html><head><style type=""text/css"">.label { width: 150px; vertical-align: top; }</style></head><body>");
			sb.Append(@"<h3>Exception</h3>");
			sb.Append(@"<table>");
			sb.Append(FormatExceptionProperty("Date time", e.EventDate.ToString("yyyyMMdd-HHmmss")));
			sb.Append(FormatExceptionProperty("Server", e.Server));
			sb.Append(FormatExceptionProperty("Application", application));
			sb.Append(FormatExceptionProperty("Severity", e.SeverityString));
			
			if (e.SessionId != Guid.Empty)
				sb.Append(FormatExceptionProperty("Session Id", e.SessionId.ToString()));
			
			if (e.RequestId != Guid.Empty)	
				sb.Append(FormatExceptionProperty("Request Id", e.RequestId.ToString()));
			
			if (e.UserId > 0)
				sb.Append(FormatExceptionProperty("User Id", e.UserId.ToString()));
			
			if (!String.IsNullOrEmpty(e.DeclaringType))
				sb.Append(FormatExceptionProperty("Declaring Type", e.DeclaringType));
			
			if (!String.IsNullOrEmpty(e.Method))
				sb.Append(FormatExceptionProperty("Method", e.Method));
			
			if (!String.IsNullOrEmpty(e.Parameters))
				sb.Append(FormatExceptionProperty("Parameters", e.Parameters));
			
			if (!String.IsNullOrEmpty(e.Message))
				sb.Append(FormatExceptionProperty("Message", e.Message));
			
			if (e.Exception != null)
			{
				if (!String.IsNullOrEmpty(e.Exception.Source))
					sb.Append(FormatExceptionProperty("Exception source", e.Exception.Source));
				
				if (!String.IsNullOrEmpty(e.Exception.Message))
					sb.Append(FormatExceptionProperty("Exception message", e.Exception.Message));
				
				if (e.Exception.InnerException != null && !String.IsNullOrEmpty(e.Exception.InnerException.Message))
					sb.Append(FormatExceptionProperty("Inner exception", e.Exception.InnerException.Message));
				
				if (!String.IsNullOrEmpty(e.Exception.StackTrace))
					sb.Append(FormatExceptionProperty("Stack trace", e.Exception.StackTrace));
			}

			sb.Append(@"</table>");

			if (e.LogItems != null && e.LogItems.Count > 0)
			{
				sb.Append(@"<h3>Log Items</h3>");
				
				foreach (var logItem in e.LogItems)
					sb.AppendLine(logItem.ToHtmlTable() + @"<br/><br/>");	
			}

			sb.Append(@"</body></html>");

			return sb.ToString();
		}

		private static string FormatExceptionProperty(string name, string value)
		{
			// HACK - to get text to wrap in the email body
			value = value.Replace(",", ", ").Replace("  ", " ");
			return String.Format(@"<tr><td class=""label"">{0}</td><td class=""value"">{1}</td></tr>", name, HttpUtility.HtmlEncode(value));
		}

		#endregion
	}
}
