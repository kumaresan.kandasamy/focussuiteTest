﻿using System;
using System.Xml.Linq;
using Framework.Core;
using Mindscape.LightSpeed;

namespace Framework.Logging.Providers
{
	public class LogToDatabase : ILogProvider
	{
		private readonly string _application;

		/// <summary>
		/// Initializes a new instance of the <see cref="LogToDatabase"/> class.
		/// </summary>
		/// <param name="application">The application.</param>
		/// <param name="configuration">Name of the configuration.</param>
		public LogToDatabase(string application, string configuration)
		{
			// Assign application and make sure it's not greater than 30 chars
			_application = application;
			if (!string.IsNullOrEmpty(_application) && _application.Length > 30)
				_application = _application.Substring(0, 27) + "...";
			
			DataContext.Initialise(configuration);
		}

		/// <summary>
		/// Write a log request to a given output media.
		/// </summary>
		/// <param name="sender">Sender of the log request.</param>
		/// <param name="e">Parameters of the log request.</param>
		public void Log(object sender, LogEventArgs e)
		{
			var xml = String.Empty;

			#region Prepare the log items xml

			if (e.Severity == LogSeverity.Error || e.Severity == LogSeverity.Fatal)
			{
				var xmlElement = new XElement("LogItems");

				if (e.LogItems != null)
					foreach (var logItem in e.LogItems)
					{
						// Sanitize mindscape lightspeed exceptions
						var exception = (logItem.Exception == null) ? "" : logItem.Exception.ToString().SanitizeXmlString();

						xmlElement.Add(new XElement("LogItem",
														new XAttribute("Severity", logItem.SeverityString),
														new XAttribute("SessionId", logItem.SessionId),
														new XAttribute("RequestId", logItem.RequestId),
														new XAttribute("UserId", logItem.UserId),
														new XAttribute("Server", logItem.Server),
														new XAttribute("EventDate", logItem.EventDate),
														new XElement("DeclaringType", logItem.DeclaringType),
														new XElement("Method", logItem.Method),
														new XElement("Message", new XCData(logItem.Message)),
														new XElement("Exception", new XCData(exception)),
														new XElement("Parameters", new XCData(logItem.Parameters))));
					}

				xml = xmlElement.ToString();
			}

			#endregion

			#region Create a Log Event

			using (var unitOfWork = DataContext.Current.CreateUnitOfWork())
			{				
				var logEvent = new LogEvent
				               	{
				               		SessionId = e.SessionId,
				               		RequestId = e.RequestId,
				               		UserId = e.UserId,
				               		Server = e.Server,
				               		Application = _application,
				               		EventDate = e.EventDate,
				               		Severity = e.Severity,
				               		Message = e.Message,
				               		Exception = (e.Exception != null ? e.Exception.ToString() : string.Empty),
				               		DeclaringType = e.DeclaringType,
				               		Method = e.Method,
				               		Parameters = e.Parameters,
													LogItems = xml
				               	};

				unitOfWork.Add(logEvent);
				unitOfWork.SaveChanges();
			}

			#endregion
		}

		/// <summary>
		/// Write a profile request to a given output media.
		/// </summary>
		/// <param name="sender">Sender of the profile request.</param>
		/// <param name="e">Parameters of the profile request.</param>
		public void Profile(object sender, ProfileEventArgs e)
		{
			#region Create a Profile Event 

			using (var unitOfWork = DataContext.Current.CreateUnitOfWork())
			{				
				var profileEvent = new ProfileEvent
				{
					SessionId = e.SessionId,
					RequestId = e.RequestId,
					UserId = e.UserId,
					Server = e.Server,
					Application = _application,
					InTime = e.InTime,
					OutTime = e.OutTime,
					Milliseconds = e.Milliseconds,
					DeclaringType = e.DeclaringType,
					Method = e.Method,
					Parameters = e.Parameters
				};

				unitOfWork.Add(profileEvent);
				unitOfWork.SaveChanges();
			}

			#endregion
		}

		/// <summary>
		/// DataContext static class to manage the context
		/// </summary>
		private static class DataContext
		{
			/// <summary>
			/// Initialises with the specified configuration name.
			/// </summary>
			/// <param name="configurationName">Name of the configuration.</param>
			public static void Initialise(string configurationName)
			{
				Current = new LightSpeedContext<LogUnitOfWork>(configurationName);
			}

			/// <summary>
			/// Gets or sets the current context.
			/// </summary>
			/// <value>The current.</value>
			public static LightSpeedContext<LogUnitOfWork> Current { get; private set; }
		}
	}
}
