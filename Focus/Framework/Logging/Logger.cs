﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

using Framework.Core;

namespace Framework.Logging
{
	/// <summary>
	/// Log class through which all log events are processed.
	/// </summary>
	public sealed class Logger
	{
		private const string LogKey = "Logger.LogItems";

		private LogSeverity _severity;
		private int _flushCount = 1000;
		
		/// <summary>
		/// Delegate event handler that hooks up requests.
		/// </summary>
		/// <param name="sender">Sender of the event.</param>
		/// <param name="e">Event arguments.</param>
		public delegate void LogEventHandler(object sender, LogEventArgs e);

		/// <summary>
		/// The Log event.
		/// </summary>
		public event LogEventHandler LogEvent;

		#region The singleton definition

		/// <summary>
		/// Private constructor. Initializes default severity to "Error".
		/// </summary>
		static Logger()
		{
			Instance = new Logger();
			Severity = LogSeverity.Error;
		}

		/// <summary>
		/// Gets the instance of the singleton logger object.
		/// </summary>
		public static Logger Instance { get; private set; }

		#endregion

		#region Settings

		/// <summary>
		/// Gets and sets the severity level of logging activity.
		/// </summary>
		public static LogSeverity Severity
		{
			get { return Instance._severity; }
			set { Instance._severity = value; }
		}

		/// <summary>
		/// Gets or sets the flush count.
		/// </summary>
		/// <value>The flush count.</value>
		public static int FlushCount
		{
			get { return Instance._flushCount; }
			set { Instance._flushCount = value; }
		}

		#endregion

		#region Log Start / End

		/// <summary>
		/// Infoes the start end.
		/// </summary>
		/// <param name="step">The step.</param>
		/// <param name="action">The action.</param>
    public static void TraceStartEnd(string step, Action action, Guid? SessionId = null, Guid? RequestId = null, long UserId = 0)
		{
			var stopwatch = Stopwatch.StartNew();
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();

      Info(SessionId ?? Guid.Empty, RequestId ?? Guid.Empty, UserId, methodBase, "START | " + step);

			try
			{
				action();

				stopwatch.Stop();
				Info(SessionId ?? Guid.Empty, RequestId ?? Guid.Empty, UserId, methodBase, "STOP  | " + step + " took " + stopwatch.ElapsedMilliseconds + " ms");
			}
			catch (Exception exception)
			{
				stopwatch.Stop();
				Info(methodBase, "STOP  | " + step + " took " + stopwatch.ElapsedMilliseconds + " ms", exception);

				throw;
			}
		}

		#endregion

		#region Debug Methods

		/// <summary>
		/// Log a message when severity level is "Debug" or higher.
		/// </summary>
		/// <param name="message">Log message</param>
		public static void Debug(string message)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Debug, Guid.Empty, Guid.Empty, 0, message, null, methodBase);
		}

		/// <summary>
		/// Log a message when severity level is "Debug" or higher.
		/// </summary>
		/// <param name="message">Log message.</param>
		/// <param name="exception">Log exception.</param>
		/// <param name="parameters">Log parameters.</param>
		public static void Debug(string message, Exception exception, params object[] parameters)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Debug, Guid.Empty, Guid.Empty, 0, message, exception, methodBase, parameters);
		}

		/// <summary>
		/// Log a message when severity level is "Debug" or higher.
		/// </summary>
		/// <param name="sessionId">Log session id.</param>
		/// <param name="requestId">Log request id.</param>
		/// <param name="userId">Log user id.</param>
		/// <param name="message">Log message.</param>
		/// <param name="exception">Log exception.</param>
		/// <param name="parameters">Log parameters.</param>
		public static void Debug(Guid sessionId, Guid requestId, long userId, string message, Exception exception, params object[] parameters)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Debug, sessionId, requestId, userId, message, exception, methodBase, parameters);
		}

		/// <summary>
		/// Log a message when severity level is "Debug" or higher. 
		/// ** Used internally by the Profiler **
		/// </summary>
		/// <param name="sessionId">The session id.</param>
		/// <param name="requestId">The request id.</param>
		/// <param name="userId">The user id.</param>
		/// <param name="message">The message.</param>
		/// <param name="declaringType">Type of the declaring.</param>
		/// <param name="method">The method.</param>
		/// <param name="exception">The exception.</param>
		/// <param name="parameters">The parameters.</param>
		internal static void Debug(Guid sessionId, Guid requestId, long userId, string message, string declaringType, string method, Exception exception, params object[] parameters)
		{
			Log(LogSeverity.Debug, sessionId, requestId, userId, message, exception, declaringType, method, parameters);
		}

		/// <summary>
		/// Log a message when severity level is "Debug" or higher.
		/// </summary>
		/// <param name="logData">Log data.</param>
		/// <param name="message">Log message.</param>
		public static void Debug(ILogData logData, string message)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Debug, logData.SessionId, logData.RequestId, logData.UserId, message, null, methodBase, (new object[] { logData }));
		}

		/// <summary>
		/// Log a message when severity level is "Debug" or higher.
		/// </summary>
		/// <param name="logData">Log data.</param>
		/// <param name="message">Log message.</param>
		/// <param name="exception">Log exception.</param>
		public static void Debug(ILogData logData, string message, Exception exception)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Debug, logData.SessionId, logData.RequestId, logData.UserId, message, exception, methodBase, (new object[] { logData }));
		}

		#endregion

		#region Info Methods

		/// <summary>
		/// Log a message when severity level is "Info" or higher.
		/// </summary>
		/// <param name="message">Log message</param>
		public static void Info(string message)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Info, Guid.Empty, Guid.Empty, 0, message, null, methodBase);
		}

		/// <summary>
		/// Infoes the specified type.
		/// </summary>
		/// <param name="methodBase">The method base.</param>
		/// <param name="message">The message.</param>
		/// <param name="exception">The exception.</param>
		public static void Info(MethodBase methodBase, string message, Exception exception = null)
		{
			Log(LogSeverity.Info, Guid.Empty, Guid.Empty, 0, message, exception, methodBase);
		}

    /// <summary>
    /// Infoes the specified type.
    /// </summary>
    /// <param name="methodBase">The method base.</param>
    /// <param name="message">The message.</param>
    /// <param name="exception">The exception.</param>
    public static void Info(Guid SessionId, Guid RequestId, long UserId, MethodBase methodBase, string message, Exception exception = null)
    {
      Log(LogSeverity.Info, SessionId, RequestId, UserId, message, exception, methodBase);
    }

		/// <summary>
		/// Log a message when severity level is "Info" or higher.
		/// </summary>
		/// <param name="message">Log message.</param>
		/// <param name="exception">Log exception.</param>
		/// <param name="parameters">Log parameters.</param>
		public static void Info(string message, Exception exception, params object[] parameters)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Info, Guid.Empty, Guid.Empty, 0, message, exception, methodBase, parameters);
		}

		/// <summary>
		/// Log a message when severity level is "Info" or higher.
		/// </summary>
		/// <param name="sessionId">Log session id.</param>
		/// <param name="requestId">Log request id.</param>
		/// <param name="userId">Log user id.</param>
		/// <param name="message">Log message.</param>
		/// <param name="exception">Log exception.</param>
		/// <param name="parameters">Log parameters.</param>
		public static void Info(Guid sessionId, Guid requestId, long userId, string message, Exception exception, params object[] parameters)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Info, sessionId, requestId, userId, message, exception, methodBase, parameters);
		}

		/// <summary>
		/// Log a message when severity level is "Info" or higher.
		/// </summary>
		/// <param name="logData">Log request.</param>
		/// <param name="message">Log message.</param>
		public static void Info(ILogData logData, string message)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Info, logData.SessionId, logData.RequestId, logData.UserId, message, null, methodBase, (new object[] { logData }));
		}

		/// <summary>
		/// Log a message when severity level is "Info" or higher.
		/// </summary>
		/// <param name="logData">Log request.</param>
		/// <param name="message">Log message.</param>
		/// <param name="exception">Log exception.</param>
		public static void Info(ILogData logData, string message, Exception exception)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Info, logData.SessionId, logData.RequestId, logData.UserId, message, exception, methodBase, (new object[] { logData }));
		}

		#endregion

		#region Warning Methods

		/// <summary>
		/// Log a message when severity level is "Warning" or higher.
		/// </summary>
		/// <param name="message">Log message</param>
		public static void Warning(string message)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Warning, Guid.Empty, Guid.Empty, 0, message, null, methodBase);
		}

		/// <summary>
		/// Log a message when severity level is "Warning" or higher.
		/// </summary>
		/// <param name="message">Log message.</param>
		/// <param name="exception">Log exception.</param>
		/// <param name="parameters">Log parameters.</param>
		public static void Warning(string message, Exception exception, params object[] parameters)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Warning, Guid.Empty, Guid.Empty, 0, message, exception, methodBase, parameters);
		}

		/// <summary>
		/// Log a message when severity level is "Warning" or higher.
		/// </summary>
		/// <param name="sessionId">Log session id.</param>
		/// <param name="requestId">Log request id.</param>
		/// <param name="userId">Log user id.</param>
		/// <param name="message">Log message.</param>
		/// <param name="exception">Log exception.</param>
		/// <param name="parameters">Log parameters.</param>
		public static void Warning(Guid sessionId, Guid requestId, long userId, string message, Exception exception, params object[] parameters)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Warning, sessionId, requestId, userId, message, exception, methodBase, parameters);
		}

		/// <summary>
		/// Log a message when severity level is "Warning" or higher.
		/// </summary>
		/// <param name="logData">Log request.</param>
		/// <param name="message">Log message.</param>
		public static void Warning(ILogData logData, string message)
		{			
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Warning, logData.SessionId, logData.RequestId, logData.UserId, message, null, methodBase, (new object[] { logData }));
		}

		/// <summary>
		/// Log a message when severity level is "Warning" or higher.
		/// </summary>
		/// <param name="logData">Log request.</param>
		/// <param name="message">Log message.</param>
		/// <param name="exception">Log exception.</param>
		public static void Warning(ILogData logData, string message, Exception exception)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Warning, logData.SessionId, logData.RequestId, logData.UserId, message, exception, methodBase, (new object[] { logData }));
		}

		#endregion

		#region Error Methods

		/// <summary>
		/// Log a message when severity level is "Error" or higher.
		/// </summary>
		/// <param name="message">Log message</param>
		public static void Error(string message)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Error, Guid.Empty, Guid.Empty, 0, message, null, methodBase);
		}

		/// <summary>
		/// Log a message when severity level is "Error" or higher.
		/// </summary>
		/// <param name="message">Log message.</param>
		/// <param name="exception">Log exception.</param>
		/// <param name="parameters">Log parameters.</param>
		public static void Error(string message, Exception exception, params object[] parameters)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Error, Guid.Empty, Guid.Empty, 0, message, exception, methodBase, parameters);
		}

		/// <summary>
		/// Log a message when severity level is "Error" or higher.
		/// </summary>
		/// <param name="sessionId">Log session id.</param>
		/// <param name="requestId">Log request id.</param>
		/// <param name="userId">Log user id.</param>
		/// <param name="message">Log message.</param>
		/// <param name="exception">Log exception.</param>
		/// <param name="parameters">Log parameters.</param>
		public static void Error(Guid sessionId, Guid requestId, long userId, string message, Exception exception, params object[] parameters)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Error, sessionId, requestId, userId, message, exception, methodBase, parameters);
		}

		/// <summary>
		/// Log a message when severity level is "Error" or higher.
		/// </summary>
		/// <param name="logData">Log request.</param>
		/// <param name="message">Log message.</param>
		public static void Error(ILogData logData, string message)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Error, logData.SessionId, logData.RequestId, logData.UserId, message, null, methodBase, (new object[] { logData }));
		}

		/// <summary>
		/// Log a message when severity level is "Error" or higher.
		/// </summary>
		/// <param name="logData">The log data.</param>
		/// <param name="message">Log message.</param>
		/// <param name="exception">Log exception.</param>
		public static void Error(ILogData logData, string message, Exception exception)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Error, logData.SessionId, logData.RequestId, logData.UserId, message, exception, methodBase, (new object[] { logData }));
		}

		#endregion

		#region Fatal Methods

		/// <summary>
		/// Log a message when severity level is "Fatal" or higher.
		/// </summary>
		/// <param name="message">Log message</param>
		public static void Fatal(string message)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Fatal, Guid.Empty, Guid.Empty, 0, message, null, methodBase);
		}

		/// <summary>
		/// Log a message when severity level is "Fatal" or higher.
		/// </summary>
		/// <param name="message">Log message.</param>
		/// <param name="exception">Log exception.</param>
		/// <param name="parameters">Log parameters.</param>
		public static void Fatal(string message, Exception exception, params object[] parameters)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Fatal, Guid.Empty, Guid.Empty, 0, message, exception, methodBase, parameters);
		}

		/// <summary>
		/// Log a message when severity level is "Fatal" or higher.
		/// </summary>
		/// <param name="sessionId">Log session id.</param>
		/// <param name="requestId">Log request id.</param>
		/// <param name="userId">Log user id.</param>
		/// <param name="message">Log message.</param>
		/// <param name="exception">Log exception.</param>
		/// <param name="parameters">Log parameters.</param>
		public static void Fatal(Guid sessionId, Guid requestId, long userId, string message, Exception exception, params object[] parameters)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Fatal, sessionId, requestId, userId, message, exception, methodBase, parameters);
		}

		/// <summary>
		/// Log a message when severity level is "Fatal" or higher.
		/// </summary>
		/// <param name="logData">Log request.</param>
		/// <param name="message">Log message.</param>
		public static void Fatal(ILogData logData, string message)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Fatal, logData.SessionId, logData.RequestId, logData.UserId, message, null, methodBase, (new object[] { logData }));
		}

		/// <summary>
		/// Log a message when severity level is "Fatal" or higher.
		/// </summary>
		/// <param name="logData">Log request.</param>
		/// <param name="message">Log message.</param>
		/// <param name="exception">Log exception.</param>
		public static void Fatal(ILogData logData, string message, Exception exception)
		{
			var methodBase = new StackTrace(true).GetFrame(1).GetMethod();
			Log(LogSeverity.Fatal, logData.SessionId, logData.RequestId, logData.UserId, message, exception, methodBase, (new object[] { logData }));
		}

		#endregion

		#region Flush Methods

		/// <summary>
		/// Flushes the log.
		/// </summary>
		public static void Flush(bool force = false)
		{
			var logItems = (List<LogItem>)ObjectManager.GetObject(LogKey);

			if (logItems != null && logItems.Count > 0)
			{
				var lastLogItem = logItems[logItems.Count - 1];
				Instance.OnLog(new LogEventArgs(lastLogItem, logItems));

				// Only clear out log items if greater than count or explicitly forced to
				if (force || logItems.Count > FlushCount)
					ObjectManager.ClearObject(LogKey);
			}
		}

		#endregion

		#region Reset

		/// <summary>
		/// Resets this logs.
		/// </summary>
		public static void Reset()
		{
			ObjectManager.ClearObject(LogKey);
		}

		#endregion

		#region Helper Methods
		
		/// <summary>
		/// Logs the specified severity.
		/// </summary>
		/// <param name="severity">The severity.</param>
		/// <param name="sessionId">The session id.</param>
		/// <param name="requestId">The request id.</param>
		/// <param name="userId">The user id.</param>
		/// <param name="message">The message.</param>
		/// <param name="exception">The exception.</param>
		/// <param name="methodBase">The method base.</param>
		/// <param name="parameters">The parameters.</param>
		private static void Log(LogSeverity severity, Guid sessionId, Guid requestId, long userId, string message,
														Exception exception, MethodBase methodBase, params object[] parameters)
		{
			Log(severity, sessionId, requestId, userId, message, exception, methodBase.DeclaringType.ToString(), methodBase.Name, parameters);
		}

		/// <summary>
		/// Logs the specified severity.
		/// </summary>
		/// <param name="severity">The severity.</param>
		/// <param name="sessionId">The session id.</param>
		/// <param name="requestId">The request id.</param>
		/// <param name="userId">The user id.</param>
		/// <param name="message">The message.</param>
		/// <param name="exception">The exception.</param>
		/// <param name="declaringType">Type of the declaring.</param>
		/// <param name="method">The method.</param>
		/// <param name="parameters">The parameters.</param>
		private static void Log(LogSeverity severity, Guid sessionId, Guid requestId, long userId, string message,
														Exception exception, string declaringType, string method, params object[] parameters)
		{
			if (severity >= Severity)
			{
				var logItem = new LogItem(severity, sessionId, requestId, userId, Environment.MachineName, DateTime.Now,
				                          message, exception, declaringType, method, parameters.AsString());

				var logItems = (List<LogItem>) ObjectManager.GetObject(LogKey);

				if (logItems == null)
				{
					logItems = new List<LogItem>();
					ObjectManager.SetObject(LogKey, logItems);
				}

				logItems.Add(logItem);
				
				Flush();
			}
		}

		#endregion

		#region Log Event Methods

		/// <summary>
		/// Invoke the Log event.
		/// </summary>
		/// <param name="e">Log event parameters.</param>
		public void OnLog(LogEventArgs e)
		{
			if (LogEvent != null)
				LogEvent(this, e);
		}

		/// <summary>
		/// Attach a listening provider logging device to logger.
		/// </summary>
		/// <param name="provider">Provider (listening device).</param>
		public void Attach(ILogProvider provider)
		{
			LogEvent += provider.Log;
		}

		/// <summary>
		/// Detach a listening provider logging device from logger.
		/// </summary>
		/// <param name="provider">Provider (listening device).</param>
		public void Detach(ILogProvider provider)
		{
			LogEvent -= provider.Log;
		}

		/// <summary>
		/// Detach a listening provider logging devices from logger.
		/// </summary>
		public void Detach()
		{
			LogEvent = null;
		}

		#endregion
	}
}
