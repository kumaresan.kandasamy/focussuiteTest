﻿using System;
using System.Text;
using System.Web;
using Framework.Core;

namespace Framework.Logging
{
	public class LogItem
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="LogItem"/> class.
		/// </summary>
		/// <param name="severity">The severity.</param>
		/// <param name="sessionId">The session id.</param>
		/// <param name="requestId">The request id.</param>
		/// <param name="userId">The user id.</param>
		/// <param name="server">The server.</param>
		/// <param name="eventDate">The event date.</param>
		/// <param name="message">The message.</param>
		/// <param name="exception">The exception.</param>
		/// <param name="declaringType">Type of the declaring.</param>
		/// <param name="method">The method.</param>
		/// <param name="parameters">The parameters.</param>
		public LogItem(LogSeverity severity, Guid sessionId, Guid requestId, long userId, string server, DateTime eventDate,
									 string message, Exception exception, string declaringType, string method, string parameters)
		{
			Severity = severity;
			SessionId = sessionId;
			RequestId = requestId;
			UserId = userId;
			Server = server;
			EventDate = eventDate;
			Message = message;
			Exception = exception;
			DeclaringType = declaringType;
			Method = method;
			Parameters = parameters;
		}

		#region Properties

		/// <summary>
		/// Gets and sets the log severity.
		/// </summary>       
		public LogSeverity Severity { get; private set; }

		/// <summary>
		/// Gets and sets the session id.
		/// </summary> 
		public Guid SessionId { get; private set; }

		/// <summary>
		/// Gets and sets the request id.
		/// </summary> 
		public Guid RequestId { get; private set; }

		/// <summary>
		/// Gets and sets the user id.
		/// </summary> 
		public long UserId { get; private set; }

		/// <summary>
		/// Gets and sets the Server.
		/// </summary> 
		public string Server { get; private set; }

		/// <summary>
		/// Gets and sets the log date and time.
		/// </summary> 
		public DateTime EventDate { get; private set; }

		/// <summary>
		/// Gets and sets the log message.
		/// </summary> 
		public string Message { get; internal set; }

		/// <summary>
		/// Gets and sets the optional inner exception.
		/// </summary>     
		public Exception Exception { get; private set; }

		/// <summary>
		/// Gets and sets the declaring type.
		/// </summary>   
		public string DeclaringType { get; private set; }

		/// <summary>
		/// Gets and sets the method.
		/// </summary>   
		public string Method { get; private set; }

		/// <summary>
		/// Gets and sets the parameters.
		/// </summary>   
		public string Parameters { get; private set; }

		/// <summary>
		/// Friendly string that represents the severity.
		/// </summary>
		public string SeverityString
		{
			get { return Severity.ToString("G"); }
		}

		#endregion

		/// <summary>
		/// LogItem as a string representation.
		/// </summary>
		/// <returns>String representation of the LogItem.</returns>
		public override string ToString()
		{
			return String.Format("Log Item: {0} - {1} - {2} - {3} - {4} - {5} - {6} - {7} - {8} - {9} - {10}", SeverityString, SessionId, RequestId, UserId,
																																																				 Server, EventDate, Message, Exception,
																																																				 DeclaringType, Method, Parameters);
		}

		/// <summary>
		/// LogItem as a string representation.
		/// </summary>
		/// <returns>String representation of the LogItem.</returns>
		public string ToShortString()
		{
			return String.Format("Log Item: {0} - {1} - {2} - {3} - {4} - {5}", EventDate, Message, Exception, DeclaringType, Method, Parameters);
		}

		/// <summary>
		/// LogItem as a HTML table.
		/// </summary>
		/// <returns></returns>
		public string ToHtmlTable()
		{
			var sb = new StringBuilder();
			sb.Append(@"<table>");
			
			if(EventDate.IsNotNull())
				sb.Append(FormatLogProperty("Date time", EventDate.ToString("yyyyMMdd-HHmmss")));

			if(Message.IsNotNullOrEmpty())
				sb.Append(FormatLogProperty("Message", Message ));
			
			if(Exception.IsNotNull())
				sb.Append(FormatLogProperty("Exception", Exception.ToString()));
			
			if(DeclaringType.IsNotNullOrEmpty())
				sb.Append(FormatLogProperty("Declaring type", DeclaringType));
			
			if(Method.IsNotNullOrEmpty())
				sb.Append(FormatLogProperty("Method", Method));
			
			if(Parameters.IsNotNullOrEmpty())
				sb.Append(FormatLogProperty("Parameters", Parameters));
			
			sb.Append(@"</table>");

			return sb.ToString();
		}

		/// <summary>
		/// Formats the log property.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		private static string FormatLogProperty(string name, string value)
		{
			// HACK - to get text to wrap in the email body
			value = value.Replace(",", ", ").Replace("  ", " ");
			return String.Format(@"<tr><td class=""label"">{0}</td><td class=""value"">{1}</td></tr>", name, HttpUtility.HtmlEncode(value));
		}
	}
}
