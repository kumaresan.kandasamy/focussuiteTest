﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.Logging
{
	/// <summary>
	/// Contains log specific event data for log events.
	/// </summary>
	public class LogEventArgs : EventArgs
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="LogEventArgs"/> class.
		/// </summary>
		/// <param name="logItem">The log item.</param>
		/// <param name="logItems">The log items.</param>
		internal LogEventArgs(LogItem logItem, List<LogItem> logItems)
		{
			Severity = logItem.Severity;
			SessionId = logItem.SessionId;
			RequestId = logItem.RequestId;
			UserId = logItem.UserId;
			Server = logItem.Server;
			EventDate = logItem.EventDate;
			Message = logItem.Message;
			Exception = logItem.Exception;
			DeclaringType = logItem.DeclaringType;
			Method = logItem.Method;
			Parameters = logItem.Parameters;
			LogItems = logItems;
		}

		#region Properties

		/// <summary>
		/// Gets and sets the log severity.
		/// </summary>       
		public LogSeverity Severity { get; private set; }

		/// <summary>
		/// Gets and sets the session id.
		/// </summary> 
		public Guid SessionId { get; private set; }

		/// <summary>
		/// Gets and sets the request id.
		/// </summary> 
		public Guid RequestId { get; private set; }

		/// <summary>
		/// Gets and sets the user id.
		/// </summary> 
		public long UserId { get; private set; }

		/// <summary>
		/// Gets and sets the Server.
		/// </summary> 
		public string Server { get; private set; }

		/// <summary>
		/// Gets and sets the log date and time.
		/// </summary> 
		public DateTime EventDate { get; private set; }

		/// <summary>
		/// Gets and sets the log message.
		/// </summary> 
		public string Message { get; internal set; }

		/// <summary>
		/// Gets and sets the optional inner exception.
		/// </summary>     
		public Exception Exception { get; private set; }

		/// <summary>
		/// Gets and sets the declaring type.
		/// </summary>   
		public string DeclaringType { get; private set; }

		/// <summary>
		/// Gets and sets the method.
		/// </summary>   
		public string Method { get; private set; }

		/// <summary>
		/// Gets and sets the parameters.
		/// </summary>   
		public string Parameters { get; private set; }

		/// <summary>
		/// Friendly string that represents the severity.
		/// </summary>
		public string SeverityString
		{
			get { return Severity.ToString("G"); }
		}

		/// <summary>
		/// Gets or sets the log items.
		/// </summary>
		/// <value>The log items.</value>
		public List<LogItem> LogItems { get; private set; }

		#endregion

		/// <summary>
		/// LogEventArgs as a string representation.
		/// </summary>
		/// <returns>String representation of the LogEventArgs.</returns>
		public override string ToString()
		{
			var logItems = "";

			if (LogItems != null)
			{
				var sb = new StringBuilder();

				foreach (var logItem in LogItems)
					sb.AppendLine(logItem.ToShortString());

				logItems = Environment.NewLine + Environment.NewLine + "Log Items:" + Environment.NewLine + Environment.NewLine + sb;
			}

			return String.Format("{0} - {1} - {2} - {3} - {4} - {5} - {6} - {7} - {8} - {9} - {10}{11}", SeverityString, SessionId, RequestId, UserId,
																																																	 Server, EventDate, Message, Exception,
																																																	 DeclaringType, Method, Parameters, logItems);
		}
	}
}
