﻿using System;

namespace Framework.Logging
{
	/// <summary>
	/// Contains profile specific event data for profile events.
	/// </summary>
	public class ProfileEventArgs : EventArgs
	{
		/// <summary>
		/// Constructor of ProfileEventArgs.
		/// </summary>
		/// <param name="sessionId">Profile session id.</param>
		/// <param name="requestId">Profile request id.</param>
		/// <param name="userId">Profile user id.</param>
		/// <param name="server">Profile server.</param>
		/// <param name="inTime">Profile in time.</param>
		/// <param name="outTime">Profile out time.</param>
		/// <param name="milliseconds">Profile milliseconds.</param>
		/// <param name="declaringType">Profile declaring type.</param>
		/// <param name="method">Profile method.</param>
		/// <param name="parameters">Profile parameters.</param>
		public ProfileEventArgs(Guid sessionId, Guid requestId, long userId, string server, DateTime inTime, DateTime outTime, long milliseconds, string declaringType, string method, string parameters)
		{
			SessionId = sessionId;
			RequestId = requestId;
			UserId = userId;
			Server = server;
			InTime = inTime;
			OutTime = outTime;
			Milliseconds = milliseconds;
			DeclaringType = declaringType;
			Method = method;
			Parameters = parameters;
		}

		#region Properties

		/// <summary>
		/// Gets and sets the session id.
		/// </summary> 
		public Guid SessionId { get; private set; }

		/// <summary>
		/// Gets and sets the request id.
		/// </summary> 
		public Guid RequestId { get; private set; }

		/// <summary>
		/// Gets and sets the user id.
		/// </summary> 
		public long UserId { get; private set; }

		/// <summary>
		/// Gets and sets the Server.
		/// </summary> 
		public string Server { get; private set; }

		/// <summary>
		/// Gets and sets the in time.
		/// </summary> 
		public DateTime InTime { get; private set; }

		/// <summary>
		/// Gets and sets the out time.
		/// </summary> 
		public DateTime OutTime { get; private set; }

		/// <summary>
		/// Gets and sets the milliseconds.
		/// </summary>     
		public long Milliseconds { get; private set; }

		/// <summary>
		/// Gets and sets the declaring type.
		/// </summary>   
		public string DeclaringType { get; private set; }

		/// <summary>
		/// Gets and sets the method.
		/// </summary>   
		public string Method { get; private set; }

		/// <summary>
		/// Gets and sets the parameters.
		/// </summary>   
		public string Parameters { get; private set; }

		#endregion

		/// <summary>
		/// ProfileEventArgs as a string representation.
		/// </summary>
		/// <returns>String representation of the ProfileEventArgs.</returns>
		public override string ToString()
		{
			return String.Format("{0} - {1} - {2} - {3} - {4} - {5} - {6} - {7} - {8} - {9}", SessionId, RequestId, UserId, Server, InTime, OutTime, 
																																												Milliseconds, DeclaringType, Method, Parameters);
		}
	}
}
