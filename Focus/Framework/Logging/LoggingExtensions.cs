﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.Script.Serialization;

namespace Framework.Logging
{
	internal static class LoggingExtensions
	{
		/// <summary>
		/// Ases the string.
		/// </summary>
		/// <param name="parameters">The parameters.</param>
		/// <returns></returns>
		public static string AsString(this object[] parameters)
		{
			// What were the parameters of the service request			
			if (parameters != null && parameters.Length > 0)
			{
				var sb = new StringBuilder();
				var i = 1;

				foreach (var parameter in parameters)
				{
					try
					{
						if (parameter != null)
						{
							if (parameter is Page)
								sb.AppendLine(i.ToString("#00 : ") + parameter.GetType().Name + " " + ((Page)parameter).ID);
							else
								sb.AppendLine(i.ToString("#00 : ") + parameter.GetType().Name + " " + (new JavaScriptSerializer()).Serialize(parameter));
						}
						else
							sb.AppendLine(i.ToString("#00 : ") + "Unknown parameter (NULL)");
					}
					catch (Exception)
					{
						sb.AppendLine(i.ToString("#00 : ") + "Unknown parameter (Unable to Serialize)");
					}

					i++;
				}

				return sb.ToString();
			}

			return string.Empty;
		}
	}
}
