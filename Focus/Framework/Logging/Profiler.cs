﻿using System.Collections.Generic;
using System.Web.UI;

namespace Framework.Logging
{
	/// <summary>
	/// Profiler class through which all profile events are processed.
	/// </summary>
	public sealed class Profiler
	{
		private int _handlerCount;

		/// <summary>
		/// Delegate event handler that hooks up requests.
		/// </summary>
		/// <param name="sender">Sender of the event.</param>
		/// <param name="e">Event arguments.</param>
		public delegate void ProfileEventHandler(object sender, ProfileEventArgs e);

		/// <summary>
		/// The Profile event.
		/// </summary>
		public event ProfileEventHandler ProfileEvent;

		#region The singleton definition

		/// <summary>
		/// Private constructor.
		/// </summary>
		static Profiler()
		{
			Instance = new Profiler();
		}

		/// <summary>
		/// Gets the instance of the singleton profiler object.
		/// </summary>
		public static Profiler Instance { get; private set; }

		#endregion

		#region Profile Methods

		/// <summary>
		/// Gets a value indicating whether profiling is enabled.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this is enabled; otherwise, <c>false</c>.
		/// </value>
		internal bool IsEnabled
		{
			get { return _handlerCount > 0; }
		}

		/// <summary>
		/// Profiles the specified user state.
		/// </summary>
		/// <param name="logData">The log data.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns></returns>
		public static Profile Profile(ILogData logData, params object[] parameters)
		{
			if (parameters.Length == 0)
				return new Profile(logData.SessionId, logData.RequestId, logData.UserId);

			var allParameters = new List<object> ();
			allParameters.AddRange(parameters);

			return new Profile(logData.SessionId, logData.RequestId, logData.UserId, allParameters.ToArray());
		}

		/// <summary>
		/// Profiles the specified user state.
		/// </summary>
		/// <param name="logData">The log data.</param>
		/// <param name="page">The web page.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns></returns>
		public static Profile Profile(ILogData logData, Page page, params object[] parameters)
		{
			if (parameters.Length == 0)
				return new Profile(logData.SessionId, logData.RequestId, logData.UserId, page, (new object[] { page.Request.RawUrl }));

			var allParameters = new List<object> { page.Request.RawUrl };
			allParameters.AddRange(parameters);

			return new Profile(logData.SessionId, logData.RequestId, logData.UserId, page, allParameters.ToArray());
		}

		#endregion

		#region Profiler Event Methods

		/// <summary>
		/// Invoke the Profile event.
		/// </summary>
		/// <param name="e">Log event parameters.</param>
		public void OnProfile(ProfileEventArgs e)
		{
			if (ProfileEvent != null)
				ProfileEvent(this, e);
		}

		/// <summary>
		/// Attach a listening provider logging device to logger.
		/// </summary>
		/// <param name="provider">Provider (listening device).</param>
		public void Attach(ILogProvider provider)
		{
			ProfileEvent += provider.Profile;
			_handlerCount++;
		}

		/// <summary>
		/// Detach a listening provider logging device from logger.
		/// </summary>
		/// <param name="provider">Provider (listening device).</param>
		public void Detach(ILogProvider provider)
		{
			ProfileEvent -= provider.Profile;
			_handlerCount--;
		}

		/// <summary>
		/// Detaches all listening provider logging devices from logger.
		/// </summary>
		public void Detach()
		{
			ProfileEvent = null;
		}

		#endregion
	}
}
