﻿using System;
using System.Diagnostics;
using System.Web.UI;

namespace Framework.Logging
{
	/// <summary>
	/// Log class using Profile method to time a block of code.
	/// Should be used as follows:
	/// 
	/// using (Profiler.Profile(serviceRequest))
	/// {
	///		... Code to Time ...
	/// }
	/// 
	/// </summary>
	public sealed class Profile : IDisposable
	{
		private readonly bool _enabled;
		private readonly DateTime _inTime;

		private readonly Guid _sessionId, _requestId;
		private readonly long _userId;
		private readonly Page _page;
		private readonly object[] _parameters;

		/// <summary>
		/// Initializes a new instance of the <see cref="Profile"/> class.
		/// </summary>
		/// <param name="parameters">The parameters.</param>
		public Profile(params object[] parameters) : this(Guid.Empty, Guid.Empty, 0, parameters)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="Profile"/> class.
		/// </summary>
		/// <param name="sessionId">The session id.</param>
		/// <param name="requestId">The request id.</param>
		/// <param name="userId">The user id.</param>
		/// <param name="parameters">The parameters.</param>
		public Profile(Guid sessionId, Guid requestId, long userId, params object[] parameters)
		{
			_enabled = Profiler.Instance.IsEnabled;
			_sessionId = sessionId;
			_requestId = requestId;
			_userId = userId;
			_parameters = parameters;
			
			_inTime = DateTime.UtcNow;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Profile"/> class.
		/// </summary>
		/// <param name="sessionId">The session id.</param>
		/// <param name="requestId">The request id.</param>
		/// <param name="userId">The user id.</param>
		/// <param name="page">The page.</param>
		/// <param name="parameters">The parameters.</param>
		public Profile(Guid sessionId, Guid requestId, long userId, Page page, params object[] parameters)
		{
			_enabled = Profiler.Instance.IsEnabled;
			_sessionId = sessionId;
			_requestId = requestId;
			_userId = userId;
			_page = page;
			_parameters = parameters;

			_inTime = DateTime.UtcNow;
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			// How long did it take
			var outTime = DateTime.UtcNow;
			var milliseconds = Convert.ToInt64(outTime.Subtract(_inTime).TotalMilliseconds);

			// What were the parameters of the service request			
			var parameters = _parameters.AsString();

			string declaringType, method;

			if (_page == null)
			{
				// What was the Declaring Type and Method 
				var methodBase = new StackTrace(true).GetFrame(1).GetMethod();

				declaringType = methodBase.DeclaringType.ToString();
				method = methodBase.Name;
			}
			else
			{
				declaringType = "WebForm : " + _page.AppRelativeVirtualPath.Replace("~/", "");
				method = (_page.IsPostBack ? "Postback" : "Render");
			}

			Logger.Debug(_sessionId, _requestId, _userId, "Effort: " + milliseconds + " ms", declaringType, method, null, parameters);

			if (_enabled)			
				Profiler.Instance.OnProfile(new ProfileEventArgs(_sessionId, _requestId, _userId, Environment.MachineName, _inTime, outTime, milliseconds, declaringType, method, parameters));

			GC.SuppressFinalize(this);
		}
	}
}