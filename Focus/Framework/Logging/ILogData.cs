﻿using System;

namespace Framework.Logging
{
	public interface ILogData
	{
		Guid SessionId { get; set; }
		Guid RequestId { get; set; }
		long UserId { get; set; }
	}
}
