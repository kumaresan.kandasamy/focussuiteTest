﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Text;

using Framework.Core;

using Newtonsoft.Json;

#endregion

namespace Framework.Messaging
{
	internal static class Extensions
	{
		#region Exception Management

		/// <summary>
		/// Formats the specified exception as a string.
		/// </summary>
		/// <param name="exception">The exception.</param>
		/// <returns></returns>
		public static string FormatException(this Exception exception)
		{
			var sb = new StringBuilder();

			sb.AppendLine("Exception");
			sb.AppendLine(" ");

			if (exception.IsNotNull())
				FormatException(sb, exception, string.Empty);

			return sb.ToString();
		}

		/// <summary>
		/// Formats the exception as a string.
		/// </summary>
		/// <param name="builder">The builder.</param>
		/// <param name="exception">The exception.</param>
		/// <param name="indent">The indent.</param>
		private static void FormatException(StringBuilder builder, Exception exception, string indent)
		{
			if (indent.IsNull())
				indent = string.Empty;
			else if (indent.Length > 0)
				builder.AppendFormat("{0}Inner Exception:", indent);

			builder.AppendFormat("\n{0}Type: {1}", indent, exception.GetType().FullName);
			builder.AppendFormat("\n{0}Message: {1}", indent, exception.Message);
			builder.AppendFormat("\n{0}Source: {1}", indent, exception.Source);
			builder.AppendFormat("\n{0}Stacktrace: {1}", indent, exception.StackTrace);

			if (exception.InnerException.IsNotNull())
			{
				builder.Append("\n");
				FormatException(builder, exception.InnerException, indent + "\t");
			}
		}

		#endregion

		#region Serialisation

		/// <summary>
		/// Serializes the specified object to json
		/// </summary>
		/// <returns></returns>
		public static string SerializeJson(this object value, Formatting formatting = Formatting.None)
		{
			return JsonConvert.SerializeObject(value, formatting);
		}

		/// <summary>
		/// Deserializes the specified serialized data into an object
		/// </summary>
		/// <returns></returns>
		public static object DeserializeJson(this string serializedData, Type type)
		{
			return JsonConvert.DeserializeObject(serializedData, type);
		}

		#endregion
	}
}
