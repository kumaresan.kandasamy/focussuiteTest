﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Framework.Messaging.Entities;

#endregion

namespace Framework.Messaging
{
	public interface IMessageBus
	{
		/// <summary>
		/// Publishes the message.
		/// </summary>
		/// <typeparam name="TMessage">The type of the message.</typeparam>
		/// <param name="message">The message.</param>
		/// <param name="messagePriority">The message priority.</param>
    /// <param name="updateIfExists">Update any existing message with matching entity key and type if not yet processed</param>
    /// <param name="processOn">An optional future date to process the message.</param>
    /// <param name="entityName">An optional name for the entity being processed in the message.</param>
    /// <param name="entityKey">The optional key for the entity being processed in the message.</param>
    void Publish<TMessage>(TMessage message, MessagePriority? messagePriority = null, bool updateIfExists = false, DateTime? processOn = null, string entityName = null, long? entityKey = null) where TMessage : class, IMessage;

		/// <summary>
		/// Publishes multiple messages.
		/// </summary>
		/// <typeparam name="TMessage">The type of the message.</typeparam>
		/// <param name="messages">The messages.</param>
		/// <param name="messagePriority">The message priority.</param>
    /// <param name="updateIfExists">Update any existing message with matching entity key and type if not yet processed</param>
    /// <param name="processOn">An optional future date to process the message.</param>
    /// <param name="entityName">An optional name for the entity being processed in the message.</param>
    /// <param name="entityKey">The optional key for the entity being processed in the message.</param>
    void PublishMultiple<TMessage>(IEnumerable<TMessage> messages, MessagePriority? messagePriority = null, bool updateIfExists = false, DateTime? processOn = null, string entityName = null, long? entityKey = null) where TMessage : class, IMessage;

	  /// <summary>
	  /// Schedules a message to run at a set interval
	  /// </summary>
	  /// <param name="message">The message to schedule</param>
	  /// <param name="schedulePlan">The planned schedule to run</param>
	  /// <param name="messagePriority">The message priority.</param>
	  void ScheduleMessage<TMessage>(TMessage message, MessageSchedulePlan schedulePlan, MessagePriority? messagePriority = MessagePriority.Medium) where TMessage : class, IMessage;

	  /// <summary>
	  /// Gets a MessageType record for a message
	  /// </summary>
	  /// <param name="message">The message whose type is required</param>
	  /// <returns>The MessageType record</returns>
	  MessageTypeEntity GetMessageType<TMessage>(TMessage message) where TMessage : class, IMessage;

	  /// <summary>
	  /// Updates any scheduled messages to ensure they are on the correct version
	  /// </summary>
    /// <param name="version">The current version.</param>
    void UpdateScheduledMessages(string version);

    /// <summary>
    /// Peeks at messages in the message bus without processing them
    /// </summary>
    /// <typeparam name="TMessage">The type of the message.</typeparam>
    /// <param name="version">The version.</param>
    /// <param name="number">The number of messages to return.</param>
    /// <returns>A list of messages of the specified type and version</returns>
	  List<TMessage> PeekMessages<TMessage>(string version = null, int number = 1) where TMessage : class, IMessage;
	}
}
