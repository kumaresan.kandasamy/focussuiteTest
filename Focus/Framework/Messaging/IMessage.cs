﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

#endregion

namespace Framework.Messaging
{
	public interface IMessage
	{
		/// <summary>
		/// Gets the message id.
		/// </summary>
    Guid Id { get; set; }

		/// <summary>
		/// Gets the version of the message.
		/// </summary>
		string Version { get; set; }
	}
}
