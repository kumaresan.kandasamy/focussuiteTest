﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using Framework.Core;
using Framework.Messaging.Entities;
using Framework.ServiceLocation;

#endregion

namespace Framework.Messaging
{
	public class MessageBus : IMessageBus
	{
		private readonly IMessageStore _messageStore;
		private readonly List<string> _directMessageTypes = new List<string>();
		private static readonly Queue<string> _directWebApiUrlBaseAddresses = new Queue<string>();
		private readonly string _apiKey;
		private readonly int _apiTimeOut = 10000;
		private readonly bool _isMessageBusService = false;
		private readonly bool _directWebApiAvailable;

		private const string PostEventRequest = "{0}/api/events";

		private readonly object _sync = new object();

		/// <summary>
		/// Initializes a new instance of the <see cref="MessageBus" /> class.
		/// </summary>
		public MessageBus() : this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="MessageBus" /> class.
		/// </summary>
		/// <param name="messageStore">The message store.</param>
		public MessageBus(IMessageStore messageStore)
		{
			_messageStore = messageStore ?? ServiceLocator.Current.Resolve<IMessageStore>();

			// Get the messages we want to allow to be handled in process
			foreach (var directMessageType in (ConfigurationManager.AppSettings.Get("Focus-MessageBus-DirectMessageTypes") ?? "").Split(new[] { ',', '|' }, StringSplitOptions.RemoveEmptyEntries))
				_directMessageTypes.Add(directMessageType);

			// Get all the base address of the Message Bus Web Apis that are available to us to process direct messages
			if (!_directWebApiUrlBaseAddresses.Any())
			{
				lock (_sync)
				{
					// check again!
					if (!_directWebApiUrlBaseAddresses.Any())
					{
						foreach (var baseAddress in (ConfigurationManager.AppSettings.Get("Focus-MessageBus-DirectWebApiUrlBaseAddresses") ?? "").Split(new[] { ',', '|' }, StringSplitOptions.RemoveEmptyEntries))
							_directWebApiUrlBaseAddresses.Enqueue(baseAddress);
					}
				}
			}

			// Set whether we have an endpoint available for direct messages
			_directWebApiAvailable = _directWebApiUrlBaseAddresses.Any();

			_apiKey = ConfigurationManager.AppSettings.Get("Focus-MessageBus-ApiKey") ?? "";
			
			if (!int.TryParse(ConfigurationManager.AppSettings.Get("Focus-MessageBus-ApiTimeOut"), out _apiTimeOut))
				_apiTimeOut = 10000;

			// We need to specify if this is a message bus service issuing the call and therefore a child event
			// We can establish this by seeing if we have a the "Focus-MessageBus-WebApiUrlBaseAddress" web api config setting
			_isMessageBusService = (ConfigurationManager.AppSettings.Get("Focus-MessageBus-WebApiUrlBaseAddress") ?? "").IsNotNullOrEmpty();
		}

		/// <summary>
		/// Publishes the message.
		/// </summary>
		/// <typeparam name="TMessage">The type of the message.</typeparam>
		/// <param name="message">The message.</param>
		/// <param name="messagePriority">The message priority.</param>
		/// <param name="updateIfExists">Update any existing message with matching entity key and type if not yet processed</param>
		/// <param name="processOn">An optional future date to process the message.</param>
		/// <param name="entityName">An optional name for the entity being processed in the message.</param>
		/// <param name="entityKey">The optional key for the entity being processed in the message.</param>
		public void Publish<TMessage>(TMessage message, MessagePriority? messagePriority = null, bool updateIfExists = false, DateTime? processOn = null, string entityName = null, long? entityKey = null) where TMessage : class, IMessage
		{
			PublishToMessageBus(message, UpgradeToDirectIfApplicable(message, messagePriority), updateIfExists, processOn, entityName, entityKey);    
		}

		/// <summary>
		/// Publishes multiple messages.
		/// </summary>
		/// <typeparam name="TMessage">The type of the message.</typeparam>
		/// <param name="messages">The messages.</param>
		/// <param name="messagePriority">The message priority.</param>
		/// <param name="updateIfExists">Update any existing message with matching entity key and type if not yet processed</param>
		/// <param name="processOn">An optional future date to process the message.</param>
		/// <param name="entityName">An optional name for the entity being processed in the message.</param>
		/// <param name="entityKey">The optional key for the entity being processed in the message.</param>
		public void PublishMultiple<TMessage>(IEnumerable<TMessage> messages, MessagePriority? messagePriority = null, bool updateIfExists = false, DateTime? processOn = null, string entityName = null, long? entityKey = null) where TMessage : class, IMessage
		{
			foreach (var message in messages)
				PublishToMessageBus(message, UpgradeToDirectIfApplicable(message, messagePriority), updateIfExists, processOn, entityName, entityKey);
		}

		/// <summary>
		/// Schedules a message to run at a set interval
		/// </summary>
		/// <param name="message">The message to schedule</param>
		/// <param name="schedulePlan">The planned schedule to run</param>
		/// <param name="messagePriority">The message priority.</param>
		public void ScheduleMessage<TMessage>(TMessage message, MessageSchedulePlan schedulePlan, MessagePriority? messagePriority = MessagePriority.Medium) where TMessage : class, IMessage
		{
			_messageStore.ScheduleMessage(message, schedulePlan, messagePriority);
		}

		/// <summary>
		/// Gets a MessageType record for a message
		/// </summary>
		/// <param name="message">The message whose type is required</param>
		/// <returns>The MessageType record</returns>
		public MessageTypeEntity GetMessageType<TMessage>(TMessage message) where TMessage : class, IMessage
		{
			return _messageStore.GetMessageType(message);
		}

		/// <summary>
		/// Updates any scheduled messages to ensure they are on the correct version
		/// </summary>
		/// <param name="version">The current version.</param>
		public void UpdateScheduledMessages(string version)
		{
			_messageStore.UpdateScheduledMessages(version);
		}

		/// <summary>
		/// Peeks at messages in the message bus without processing them
		/// </summary>
		/// <typeparam name="TMessage">The type of the message.</typeparam>
		/// <param name="version">The version.</param>
		/// <param name="number">The number of messages to return.</param>
		/// <returns>A list of messages of the specified type and version</returns>
		public List<TMessage> PeekMessages<TMessage>(string version = null, int number = 1) where TMessage : class, IMessage
		{
			return _messageStore.PeekMessages<TMessage>(version, number);
		}

		/// <summary>
		/// Upgrades the message priority to direct if applicable.
		/// </summary>
		/// <typeparam name="TMessage">The type of the message.</typeparam>
		/// <param name="message">The message.</param>
		/// <param name="messagePriority">The message priority.</param>
		/// <returns></returns>
		private MessagePriority? UpgradeToDirectIfApplicable<TMessage>(TMessage message, MessagePriority? messagePriority = null) where TMessage : class, IMessage
		{
			messagePriority = messagePriority ?? MessagePriority.Medium;

			if (messagePriority != MessagePriority.Direct && _directWebApiUrlBaseAddresses.IsNotNullOrEmpty())
			{
				var messageType = message.GetType().Name;

				if (_directMessageTypes.Any(x => x == messageType))
					messagePriority = MessagePriority.Direct;
			}

			return messagePriority;
		}

		private void PublishToMessageBus<TMessage>(TMessage message, MessagePriority? messagePriority = null, bool updateIfExists = false, DateTime? processOn = null, string entityName = null, long? entityKey = null) where TMessage : class, IMessage
		{
			if (messagePriority != MessagePriority.Direct || !_directWebApiAvailable)
			{
				#region Publish

				_messageStore.Enqueue(message, messagePriority ?? MessagePriority.Medium, updateIfExists, processOn, entityName, entityKey);  

				#endregion
			}
			else
			{
				#region Publish Direct


					var numRetries = 5;
					const int retryTimeout = 50;

					do
					{
						try
						{
							using (var httpClient = new HttpClient())
							{
								var apiUrl = GetDirectWebApiUrlBaseAddresses();
								apiUrl = String.Format(PostEventRequest, apiUrl);

								httpClient.Timeout = new TimeSpan(0, 0, 0, 0, (_apiTimeOut));
								httpClient.DefaultRequestHeaders.Accept.Clear();
								httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
								httpClient.DefaultRequestHeaders.Add("X-ApiKey", _apiKey);

								// If this is the message bus service then we are going to ensure we wait for completion so 
								// add a "X-IsBlocking" header to ensure it instructs the web api that it is a blocking request
								if (_isMessageBusService)
									httpClient.DefaultRequestHeaders.Add("X-IsBlocking", "true");

								var publishEventRequest = new PublishEventRequest
								{
									Priority = MessagePriority.Direct,
									MessageType = message.GetType().AssemblyQualifiedName,
									ProcessOn = processOn,
									EntityName = entityName,
									EntityKey = entityKey,
									UpdateIfExists = updateIfExists,
									Message = message.SerializeJson()
								};

								var apiResponse = httpClient.PostAsJsonAsync(apiUrl, publishEventRequest).Result;

								if (apiResponse.StatusCode != HttpStatusCode.Accepted)
									throw new Exception("Unable to publish message '" + message.Id + "' direct to " + apiUrl + ".  Returned status code: " + apiResponse.StatusCode);

								return;
							}
						}
						catch
						{
							if (numRetries <= 0)
								throw;

							if (retryTimeout > 0)
								Thread.Sleep(retryTimeout);
						}
					} while (numRetries-- > 0);

				#endregion
			}
		}

		/// <summary>
		/// Gets the Api Url.
		/// </summary>
		/// <returns></returns>
		private string GetDirectWebApiUrlBaseAddresses()
		{
			lock (_sync)
			{
				if (_directWebApiUrlBaseAddresses.Count == 0)
					return null;

				var directWebApiUrlBaseAddresses = _directWebApiUrlBaseAddresses.Dequeue();
				_directWebApiUrlBaseAddresses.Enqueue(directWebApiUrlBaseAddresses);

				return directWebApiUrlBaseAddresses;
			}
		}

		private class PublishEventRequest
		{
			public MessagePriority Priority { get; set; }
			public string MessageType { get; set; }
			public DateTime? ProcessOn { get; set; }
			public string EntityName { get; set; }
			public long? EntityKey { get; set; }
			public bool UpdateIfExists { get; set; }
			public string Message { get; set; }
		}
	}
}
