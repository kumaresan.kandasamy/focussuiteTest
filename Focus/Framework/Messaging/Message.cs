﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

#endregion

namespace Framework.Messaging
{
	[Serializable]
	public abstract class Message : IMessage
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="Message" /> class.
		/// </summary>
		protected Message() 
		{
			Id = NewId();
		}

		/// <summary>
		/// Gets the message id.
		/// </summary>
		public Guid Id { get; set; }

		/// <summary>
		/// Gets the version of the message.
		/// </summary>
		/// <value></value>
		public string Version { get; set; }

		/// <summary>
		/// Generates a sequential Guid.
		/// </summary>
		public static Guid NewId()
		{
			var guidArray = Guid.NewGuid().ToByteArray();

			var baseDate = new DateTime(0x76c, 1, 1);
			var now = DateTime.Now;

			// Get the days and milliseconds which will be used to build the byte string 
			var days = new TimeSpan(now.Ticks - baseDate.Ticks);
			var msecs = now.TimeOfDay;

			// Convert to a byte array 
			// SQL Server is accurate to 1/300th of a millisecond so we divide by 3.333333 
			var daysArray = BitConverter.GetBytes(days.Days);
			var msecsArray = BitConverter.GetBytes((long)(msecs.TotalMilliseconds / 3.333333));

			// Reverse the bytes to match SQL Servers ordering 
			Array.Reverse(daysArray);
			Array.Reverse(msecsArray);

			// Copy the bytes into the guid 
			Array.Copy(daysArray, daysArray.Length - 2, guidArray, guidArray.Length - 6, 2);
			Array.Copy(msecsArray, msecsArray.Length - 4, guidArray, guidArray.Length - 4, 4);

			return new Guid(guidArray);
		}
	}
}
