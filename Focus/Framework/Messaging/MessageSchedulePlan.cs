﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Framework.Core;

#endregion

namespace Framework.Messaging
{
  [Serializable]
  public class MessageSchedulePlan
  {
    /// <summary>
    /// The type of schedule (e.g. hourly, daily)
    /// </summary>
    public MessageScheduleType ScheduleType { get; set; }

    /// <summary>
    /// How often it should run based on the type
    /// </summary>
    public int Interval { get; set; }

    /// <summary>
    /// Whether to run at a fixed time past the hour (hourly/daily/weekly only)
    /// </summary>
    public int? StartMinutes { get; set; }

    /// <summary>
    /// Whether to run at a fixed hour (daily/weekly only)
    /// </summary>
    public int? StartHour { get; set; }

    /// <summary>
    /// Whether to run at a fixed day (weekly only)
    /// </summary>
    public DayOfWeek? StartDay { get; set; }

    public DateTime GetNextProcessDate(DateTime currentProcessedDate)
    {
      var nextDate = (ScheduleType == MessageScheduleType.Minutes)
        ? new DateTime(currentProcessedDate.Year, currentProcessedDate.Month, currentProcessedDate.Day, currentProcessedDate.Hour, currentProcessedDate.Minute, 0)
        : currentProcessedDate.Date;

      var now = DateTime.Now;

      switch (ScheduleType)
      {
        case MessageScheduleType.Minutes:
          while (nextDate <= now)
          {
            nextDate = nextDate.AddMinutes(Interval);
          }
          break;

        case MessageScheduleType.Hourly:
          if (StartMinutes.HasValue)
            nextDate = nextDate.AddMinutes(StartMinutes.Value);

          while (nextDate <= now)
          {
            nextDate = nextDate.AddHours(Interval);
          }
          break;

        default:
          if (StartHour.IsNull())
            throw new ArgumentException("Start Hour must be set for Daily and Weekly schedules");

          nextDate = nextDate.AddHours(StartHour.Value);
          if (StartMinutes.HasValue)
            nextDate = nextDate.AddMinutes(StartMinutes.Value);

          var multiplier = 1;
          if (ScheduleType == MessageScheduleType.Weekly)
          {
            if (StartDay.IsNull())
              throw new ArgumentException("Start Day must be set for Weekly schedules");

            while (nextDate.DayOfWeek != StartDay)
            {
              nextDate = nextDate.AddDays(1);
            }
            multiplier = 7;
          }

          while (nextDate <= now)
          {
            nextDate = nextDate.AddDays(Interval * multiplier);
          }
          break;
      }

      return nextDate;
    }
  }

  public enum MessageScheduleType
  {
    Minutes,
    Hourly,
    Daily,
    Weekly
  }
}
