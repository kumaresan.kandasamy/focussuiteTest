﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

using Framework.Core;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Linq;
using Mindscape.LightSpeed.Querying;
using Mindscape.LightSpeed.Validation;

#endregion

namespace Framework.Messaging
{
	using Entities;

	public class MessageStore : IMessageStore
	{
		private readonly LightSpeedContext<MessagingUnitOfWork> _messagingContext;
		private const string DefaultMessagingContextName = "MessageBus";

		public MessageStore(): this(DefaultMessagingContextName)
		{ }

		/// <summary>
		/// Prevents a default instance of the <see cref="MessageStore" /> class from being created.
		/// </summary>
		/// <param name="contextName">Name of the context.</param>
		public MessageStore(string contextName)
		{
			_messagingContext = new LightSpeedContext<MessagingUnitOfWork>(contextName);
		}

    /// <summary>
    /// Updates any scheduled messages to ensure they are on the correct version
    /// </summary>
    /// <param name="version">The current version.</param>
    public void UpdateScheduledMessages(string version)
    {
      using (var uow = _messagingContext.CreateUnitOfWork())
      {
        var scheduledMessageTypes = uow.MessageTypes.Where(x => x.SchedulePlan != null).Select(msg => msg.Id).ToList();
        var messagesToUpdate = uow.Messages.WithTableHint("NOLOCK").Where(msg => msg.Version != version && msg.Status == MessageStatus.Enqueued && scheduledMessageTypes.Contains(msg.MessageTypeId)).ToList();

        messagesToUpdate.ForEach(msg => msg.Version = version);
        uow.SaveChanges();
      }
    }

		/// <summary>
		/// Gets the count of messages in the store
		/// </summary>
		/// <param name="status">The status.</param>
		/// <param name="priority">The priority.</param>
		/// <param name="version">The message version.</param>
    /// <param name="messageTypes">Optional message types to target.</param>
    /// <param name="excludeMessageTypes">Optional message types to exclude.</param>
    /// <returns>The count of the number of messages that exist</returns>
    public int Count(MessageStatus? status = null, MessagePriority? priority = null, string version = null, List<Guid> messageTypes = null, List<Guid> excludeMessageTypes = null)
		{
			using (var uow = _messagingContext.CreateUnitOfWork())
			{
        var query = GetMessagesQuery(uow, "NOLOCK", status, priority, version, messageTypes, excludeMessageTypes);
				
				return query.Count();
			}
		}

    /// <summary>
    /// Gets if there are any messages in the store
    /// </summary>
    /// <param name="status">The status.</param>
    /// <param name="priority">The priority.</param>
    /// <param name="version">The message version.</param>
    /// <param name="messageTypes">Optional message types to target.</param>
    /// <param name="excludeMessageTypes">Optional message types to exclude.</param>
    /// <returns>A boolean indicating if any matching messages exist</returns>
    public bool Any(MessageStatus? status = null, MessagePriority? priority = null, string version = null, List<Guid> messageTypes = null, List<Guid> excludeMessageTypes = null)
    {
      using (var uow = _messagingContext.CreateUnitOfWork())
      {
				var query = GetMessagesQuery(uow, "NOLOCK", status, priority, version, messageTypes, excludeMessageTypes);
				var guid = query.Select(m => m.Id).FirstOrDefault();

        return guid.IsNotNullOrEmpty();
      }
    }

    /// <summary>
    /// Enqueues the message.
    /// </summary>
    /// <typeparam name="TMessage">The type of the message.</typeparam>
    /// <param name="message">The message.</param>
    /// <param name="priority">The priority.</param>
    /// <param name="updateIfExists">Update any existing message with matching entity key and type if not yet processed</param>
    /// <param name="processOn">An optional future date to process the message.</param>
    /// <param name="entityName">An optional name for the entity being processed in the message.</param>
    /// <param name="entityKey">The optional key for the entity being processed in the message.</param>
		/// <param name="markDequeued">if set to <c>true</c> then mark the message on the bus as dequeued.</param>
		public void Enqueue<TMessage>(TMessage message, MessagePriority? priority = null, bool updateIfExists = false, DateTime? processOn = null, string entityName = null, long? entityKey = null, bool markDequeued = false) where TMessage : class, IMessage
		{
			using (var uow = _messagingContext.CreateUnitOfWork())
			{
				var messageTypeId = GetMessageType(uow, message).Id;

        MessageEntity messageEntity = null;
        var isNew = false;
			  var now = DateTime.UtcNow;

        if (updateIfExists)
        {
					messageEntity = uow.Messages.WithTableHint("NOLOCK").FirstOrDefault(msg => msg.Version == message.Version
                                                               && msg.MessageTypeId == messageTypeId
                                                               && msg.EntityName == entityName
                                                               && msg.EntityKey == entityKey 
                                                               && msg.Status == MessageStatus.Enqueued);
        }

			  if (messageEntity.IsNotNull())
			  {
          message.Id = messageEntity.Id;
			    messageEntity.MessagePayload.Data = message.SerializeJson();
			  }
        else
			  {
			    messageEntity = new MessageEntity
			    {
			      Id = message.Id,
			      MessageTypeId = messageTypeId,
			      Status = (markDequeued ? MessageStatus.Dequeued : MessageStatus.Enqueued),
			      Version = message.Version,
            MessagePayload = new MessagePayloadEntity { Data = message.SerializeJson() }
			    };

			    isNew = true;
			  }

  		  messageEntity.Priority = (priority ?? MessagePriority.Medium);
        messageEntity.EnqueuedOn = now;						        
				messageEntity.EntityKey = entityKey;
        messageEntity.EntityName = entityName;
				messageEntity.ProcessOn = processOn ?? new DateTime(1900, 1, 1);

			  messageEntity.MessageLogs.Add(new MessageLogEntity { Timestamp = now, Details = "Enqueued @ " + now.ToString("yyyy-MM-dd HH:mm.ss ") });

				if (markDequeued)
				{
					messageEntity.DequeuedOn = now;
					messageEntity.MessageLogs.Add(new MessageLogEntity { Timestamp = now, Details = "Dequeued @ " + now.ToString("yyyy-MM-dd HH:mm.ss ") });
				}

        if (isNew)
				  uow.Add(messageEntity);

				uow.SaveChanges();
			}
		}

	  /// <summary>
	  /// Dequeues the specified priority.
	  /// </summary>
	  /// <param name="message">Will pass back the actual message</param>
	  /// <param name="priority">The priority.</param>
	  /// <param name="version">The message version.</param>
	  /// <param name="messageTypes">Optional message types to target.</param>
	  /// <param name="excludeMessageTypes">Optional message types to exclude.</param>
	  /// <returns></returns>
	  public IMessage Dequeue(out MessageEntity message, MessagePriority? priority = null, string version = null, List<Guid> messageTypes = null, List<Guid> excludeMessageTypes = null)
		{
			IMessage dequeuedMessage;

			using (var uow = _messagingContext.CreateUnitOfWork())
			{
				var now = DateTime.UtcNow;
				var messageId = DequeueMessage(uow, version, messageTypes, excludeMessageTypes);
				if (messageId.IsNotNullOrEmpty())
				{
					message = uow.Messages.WithTableHint("NOLOCK").FirstOrDefault(m => m.Id == messageId);
					if (message.IsNull())
					{
						return null;
					}

					message.MessageLogs.Add(new MessageLogEntity { Timestamp = now, Details = "Dequeued @ " + now.ToString("yyyy-MM-dd HH:mm.ss ") });

					uow.SaveChanges();
				}
				else
				{
					message = null;
					return null;
				}

				var dequeuedMessageType = Type.GetType(message.MessageType.TypeName);

				var messageData = message.MessagePayload;
				dequeuedMessage = (IMessage)messageData.Data.DeserializeJson(dequeuedMessageType);
			}

			return dequeuedMessage;
		}

		/// <summary>
		/// Dequeues a message
		/// </summary>
		/// <param name="unitOfWork">The unit of work.</param>
		/// <param name="version">The version.</param>
		/// <param name="messageTypes">The message types.</param>
		/// <param name="excludeMessageTypes">The exclude message types.</param>
		/// <returns></returns>
		private static Guid DequeueMessage(IUnitOfWork unitOfWork, string version, List<Guid> messageTypes, List<Guid> excludeMessageTypes)
		{
			var commandText = "[Messaging.DequeueMessage]";
			SqlParameter messageTypesParameter = null;
			
			if (messageTypes.IsNotNullOrEmpty() || excludeMessageTypes.IsNotNullOrEmpty())
			{
				var messageTypeTable = new DataTable("messageTypeTable");
				messageTypeTable.Columns.Add("Guid");

				if (messageTypes.IsNotNullOrEmpty())
				{
					commandText = "[Messaging.DequeueMessageIncludeTypes]";
					foreach (var messageType in messageTypes)
					{
						messageTypeTable.Rows.Add(messageType);
					}
				}
				else
				{
					commandText = "[Messaging.DequeueMessageExcludeTypes]";
					foreach (var messageType in excludeMessageTypes)
					{
						messageTypeTable.Rows.Add(messageType);
					}
				}
				messageTypesParameter = new SqlParameter("MessageTypeIds", messageTypeTable)
				{
					SqlDbType = SqlDbType.Structured,
					TypeName = "GUIDTableType"
				};
			}

			var versionParameter = new SqlParameter("Version", version);
			var dequeuedOnParameter = new SqlParameter("DequeuedOn", DateTime.UtcNow);
			var messageIdParameter = new SqlParameter("MessageId", Guid.Empty)
			{
				SqlDbType = SqlDbType.UniqueIdentifier,
				Direction = ParameterDirection.InputOutput
			};

			var result = Guid.Empty;

			using (var command = unitOfWork.Context.DataProviderObjectFactory.CreateCommand())
			{
				command.CommandText = commandText;
				command.CommandType = CommandType.StoredProcedure; 
				command.Parameters.Add(versionParameter);

				if (messageTypesParameter.IsNotNull())
					command.Parameters.Add(messageTypesParameter);
					
				command.Parameters.Add(dequeuedOnParameter);
				command.Parameters.Add(messageIdParameter);

				unitOfWork.PrepareCommand(command);
					
				command.ExecuteNonQuery();

				if (!messageIdParameter.Value.IsDBNull())
					result = (Guid)(messageIdParameter.Value);
			}

			return result;
		}
		
    /// <summary>
    /// Peeks at messages in the message bus without processing them
    /// </summary>
    /// <typeparam name="TMessage">The type of the message.</typeparam>
    /// <param name="version">The version.</param>
    /// <param name="number">The number of messages to return.</param>
    /// <returns>A list of messages of the specified type and version</returns>
	  public List<TMessage> PeekMessages<TMessage>(string version = null, int number = 1) where TMessage : class, IMessage
	  {
      var results = new List<TMessage>();

	    var messageType = typeof (TMessage);
      var messageTypeTypeName = messageType.AssemblyQualifiedName;

	    using (var uow = _messagingContext.CreateUnitOfWork())
	    {
				var messageTypeEntity = uow.MessageTypes.WithTableHint("NOLOCK").FirstOrDefault(x => x.TypeName == messageTypeTypeName);
        if (messageTypeEntity != null)
        {
          var payloads = (from msg in uow.Messages
                          join payload in uow.MessagePayloads
                            on msg.MessagePayloadId equals payload.Id
                          where (version == null || msg.Version == version)
                                && msg.MessageTypeId == messageTypeEntity.Id
                          orderby msg.EnqueuedOn descending
                          select payload).Take(number).ToList();

          results.AddRange(payloads.Select(payload => (TMessage)payload.Data.DeserializeJson(messageType)));
        }
	    }

      return results;
	  }

		/// <summary>
		/// Peeks at a messages in the message bus without processing it
		/// </summary>
		/// <param name="messageId">The message identifier.</param>
		/// <returns>A Tuple of IMessage and MessageEntity</returns>
		public Tuple<IMessage, MessageEntity> PeekMessage(Guid messageId)
		{
			using (var uow = _messagingContext.CreateUnitOfWork())
			{
				var peekedMessageEntity = uow.Messages.WithTableHint("NOLOCK").FirstOrDefault(m => m.Id == messageId);
				if (peekedMessageEntity.IsNull())
				{
					return null;
				}

				var peekedMessageType = Type.GetType(peekedMessageEntity.MessageType.TypeName);

				var messageData = peekedMessageEntity.MessagePayload;
				var peekedMessage = (IMessage)messageData.Data.DeserializeJson(peekedMessageType);

				return new Tuple<IMessage, MessageEntity>(peekedMessage, peekedMessageEntity);
			}
		}

		/// <summary>
		/// Registers the success.
		/// </summary>
		/// <typeparam name="TMessage">The type of the message.</typeparam>
		/// <param name="message">The message.</param>
		/// <param name="executionMilliseconds">The execution time milliseconds.</param>
		/// <param name="log">The log.</param>
		/// <exception cref="System.NotImplementedException"></exception>
		public void RegisterSuccess<TMessage>(TMessage message, long executionMilliseconds, string log) where TMessage : class, IMessage
		{
			using (var uow = _messagingContext.CreateUnitOfWork())
			{
				var now = DateTime.UtcNow;

				var messageEntity = uow.Messages.WithTableHint("NOLOCK").FirstOrDefault(x => x.Id == message.Id);

				if (messageEntity.IsNull()) return;

				messageEntity.Status = MessageStatus.Handled;
				messageEntity.HandledOn = now;
				messageEntity.ExecutionMilliseconds = executionMilliseconds;
				messageEntity.MessageLogs.Add(new MessageLogEntity { Timestamp = now, Details = "Handled @ " + now.ToString("yyyy-MM-dd HH:mm.ss ") + Environment.NewLine + "Log: " + Environment.NewLine + log });

				uow.SaveChanges();
			}
		}

		/// <summary>
		/// Registers the failure.
		/// </summary>
		/// <typeparam name="TMessage">The type of the message.</typeparam>
		/// <param name="message">The message.</param>
		/// <param name="executionMilliseconds">The execution time milliseconds.</param>
		/// <param name="log">The log.</param>
		/// <exception cref="System.NotImplementedException"></exception>
		public void RegisterFailure<TMessage>(TMessage message, long executionMilliseconds, string log) where TMessage : class, IMessage
		{
			using (var uow = _messagingContext.CreateUnitOfWork())
			{
				var now = DateTime.UtcNow;

				var messageEntity = uow.Messages.WithTableHint("NOLOCK").FirstOrDefault(x => x.Id == message.Id);

				if (messageEntity.IsNull()) return;

				messageEntity.Status = MessageStatus.Handled;
				messageEntity.Failed = true;
				messageEntity.HandledOn = now;
				messageEntity.ExecutionMilliseconds = executionMilliseconds;
				messageEntity.MessageLogs.Add(new MessageLogEntity { Timestamp = now, Details = "Handled and failed @ " + now.ToString("yyyy-MM-dd HH:mm.ss ") + Environment.NewLine + "Log: " + Environment.NewLine + log });

				uow.SaveChanges();
			}
		}

    /// <summary>
    /// Gets a MessageType record for a message
    /// </summary>
    /// <param name="message">The message whose type is required</param>
    /// <returns>The MessageType record</returns>
    public MessageTypeEntity GetMessageType<TMessage>(TMessage message) where TMessage : class, IMessage
	  {
	    using (var uow = _messagingContext.CreateUnitOfWork())
	    {
		    return GetMessageType(uow, message);
	    }	    
	  }

    /// <summary>
    /// Schedules a message to run at a set interval
    /// </summary>
    /// <param name="message">The message to schedule</param>
    /// <param name="schedulePlan">The planned schedule to run</param>
    /// <param name="messagePriority">The message priority.</param>
    public void ScheduleMessage<TMessage>(TMessage message, MessageSchedulePlan schedulePlan, MessagePriority? messagePriority = MessagePriority.Medium) where TMessage : class, IMessage
    {
      using (var uow = _messagingContext.CreateUnitOfWork())
      {
        var messageTypeTypeName = message.GetType().AssemblyQualifiedName;
				var messageType = uow.MessageTypes.WithTableHint("NOLOCK").FirstOrDefault(x => x.TypeName == messageTypeTypeName);

        if (messageType.IsNull())
        {
            messageType = new MessageTypeEntity { TypeName = messageTypeTypeName };
            uow.Add(messageType);
        }

        messageType.SchedulePlan = schedulePlan.SerializeJson();
        uow.SaveChanges();
      }

      var processOn = schedulePlan.GetNextProcessDate(DateTime.Now);
      Enqueue(message, messagePriority, true, processOn);
    }

	  /// <summary>
    /// Gets a query for accessing messages in the store
    /// </summary>
    /// <param name="uow">The current unit of work</param>
    /// <param name="tableHint">The table hint.</param>
    /// <param name="status">The status.</param>
    /// <param name="priority">The priority.</param>
    /// <param name="version">The message version.</param>
    /// <param name="messageTypes">Optional message types to target.</param>
    /// <param name="excludeMessageTypes">Optional message types to exclude.</param>
    /// <returns>
    /// A boolean indicating if any matching messages exist
    /// </returns>
    private static IQueryable<MessageEntity> GetMessagesQuery(MessagingUnitOfWork uow, string tableHint = "", MessageStatus? status = null, MessagePriority? priority = null, string version = null, List<Guid> messageTypes = null, List<Guid> excludeMessageTypes = null)
    {
      var query = tableHint.IsNotNullOrEmpty()
        ? uow.Messages.WithTableHint(tableHint)
        : uow.Messages;

      if (status.IsNotNull())
        query = query.Where(x => x.Status == status);

      if (priority.IsNotNull())
        query = query.Where(x => x.Priority == priority);

      if (version.IsNotNullOrEmpty())
        query = query.Where(x => x.Version == version);

      if (messageTypes.IsNotNullOrEmpty())
        query = query.Where(x => messageTypes.Contains(x.MessageTypeId));

      if (excludeMessageTypes.IsNotNullOrEmpty())
        query = query.Where(x => !excludeMessageTypes.Contains(x.MessageTypeId));

      query = query.Where(x => x.ProcessOn == null || x.ProcessOn <= DateTime.Now);

      return query;
    }

		/// <summary>
		/// Gets a MessageType record for a message
		/// </summary>
		/// <typeparam name="TMessage">The type of the message.</typeparam>
		/// <param name="uow">The uow.</param>
		/// <param name="message">The message whose type is required</param>
		/// <returns>
		/// The MessageType record
		/// </returns>
		private static MessageTypeEntity GetMessageType<TMessage>(MessagingUnitOfWork uow, TMessage message) where TMessage : class, IMessage
		{
			var messageTypeTypeName = message.GetType().AssemblyQualifiedName;
			var messageType = uow.MessageTypes.WithTableHint("NOLOCK").FirstOrDefault(x => x.TypeName == messageTypeTypeName);

			if (messageType == null)
			{
				messageType = new MessageTypeEntity { TypeName = messageTypeTypeName };

				try
				{
					uow.Add(messageType);
					uow.SaveChanges();
				}
				catch (ValidationException)
				{
					// Do Nothing
				}
			}

			return messageType;
		}
	}
}
