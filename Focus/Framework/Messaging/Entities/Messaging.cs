using System;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Validation;
using Mindscape.LightSpeed.Linq;

namespace Framework.Messaging.Entities
{
  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [System.Runtime.Serialization.DataContract]
  [Table("Messaging.Message", IdentityMethod=IdentityMethod.GuidComb)]
  public partial class MessageEntity : Entity<System.Guid>
  {
    #region Fields
  
    [ValueField]
    [ValidatePresence]
    private Framework.Messaging.MessageStatus _status;
    [ValueField]
    [ValidatePresence]
    private Framework.Messaging.MessagePriority _priority;
    [ValidateLength(0, 10)]
    private string _version;
    [ValidatePresence]
    private System.DateTime _enqueuedOn;
    private System.Nullable<System.DateTime> _dequeuedOn;
    private System.Nullable<System.DateTime> _handledOn;
    private long _executionMilliseconds;
    private bool _failed;
    private System.Nullable<System.DateTime> _processOn;
    [ValidateLength(0, 200)]
    private string _entityName;
    private System.Nullable<long> _entityKey;
    private System.Guid _messageTypeId;
    private System.Guid _messagePayloadId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Status entity attribute.</summary>
    public const string StatusField = "Status";
    /// <summary>Identifies the Priority entity attribute.</summary>
    public const string PriorityField = "Priority";
    /// <summary>Identifies the Version entity attribute.</summary>
    public const string VersionField = "Version";
    /// <summary>Identifies the EnqueuedOn entity attribute.</summary>
    public const string EnqueuedOnField = "EnqueuedOn";
    /// <summary>Identifies the DequeuedOn entity attribute.</summary>
    public const string DequeuedOnField = "DequeuedOn";
    /// <summary>Identifies the HandledOn entity attribute.</summary>
    public const string HandledOnField = "HandledOn";
    /// <summary>Identifies the ExecutionMilliseconds entity attribute.</summary>
    public const string ExecutionMillisecondsField = "ExecutionMilliseconds";
    /// <summary>Identifies the Failed entity attribute.</summary>
    public const string FailedField = "Failed";
    /// <summary>Identifies the ProcessOn entity attribute.</summary>
    public const string ProcessOnField = "ProcessOn";
    /// <summary>Identifies the EntityName entity attribute.</summary>
    public const string EntityNameField = "EntityName";
    /// <summary>Identifies the EntityKey entity attribute.</summary>
    public const string EntityKeyField = "EntityKey";
    /// <summary>Identifies the MessageTypeId entity attribute.</summary>
    public const string MessageTypeIdField = "MessageTypeId";
    /// <summary>Identifies the MessagePayloadId entity attribute.</summary>
    public const string MessagePayloadIdField = "MessagePayloadId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Message")]
    private EntityCollection<MessageLogEntity> _messageLogs = new EntityCollection<MessageLogEntity>();
    [ReverseAssociation("Messages")]
    private readonly EntityHolder<MessageTypeEntity> _messageType = new EntityHolder<MessageTypeEntity>();
    [ReverseAssociation("Message")]
    private readonly EntityHolder<MessagePayloadEntity> _messagePayload = new EntityHolder<MessagePayloadEntity>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<MessageLogEntity> MessageLogs
    {
      get { return Get(_messageLogs); }
      set { _messageLogs = value; }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public MessageTypeEntity MessageType
    {
      get { return Get(_messageType); }
      set { Set(_messageType, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public MessagePayloadEntity MessagePayload
    {
      get { return Get(_messagePayload); }
      set { Set(_messagePayload, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Framework.Messaging.MessageStatus Status
    {
      get { return Get(ref _status, "Status"); }
      set { Set(ref _status, value, "Status"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public Framework.Messaging.MessagePriority Priority
    {
      get { return Get(ref _priority, "Priority"); }
      set { Set(ref _priority, value, "Priority"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Version
    {
      get { return Get(ref _version, "Version"); }
      set { Set(ref _version, value, "Version"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime EnqueuedOn
    {
      get { return Get(ref _enqueuedOn, "EnqueuedOn"); }
      set { Set(ref _enqueuedOn, value, "EnqueuedOn"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> DequeuedOn
    {
      get { return Get(ref _dequeuedOn, "DequeuedOn"); }
      set { Set(ref _dequeuedOn, value, "DequeuedOn"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> HandledOn
    {
      get { return Get(ref _handledOn, "HandledOn"); }
      set { Set(ref _handledOn, value, "HandledOn"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public long ExecutionMilliseconds
    {
      get { return Get(ref _executionMilliseconds, "ExecutionMilliseconds"); }
      set { Set(ref _executionMilliseconds, value, "ExecutionMilliseconds"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public bool Failed
    {
      get { return Get(ref _failed, "Failed"); }
      set { Set(ref _failed, value, "Failed"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ProcessOn
    {
      get { return Get(ref _processOn, "ProcessOn"); }
      set { Set(ref _processOn, value, "ProcessOn"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string EntityName
    {
      get { return Get(ref _entityName, "EntityName"); }
      set { Set(ref _entityName, value, "EntityName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> EntityKey
    {
      get { return Get(ref _entityKey, "EntityKey"); }
      set { Set(ref _entityKey, value, "EntityKey"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="MessageType" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Guid MessageTypeId
    {
      get { return Get(ref _messageTypeId, "MessageTypeId"); }
      set { Set(ref _messageTypeId, value, "MessageTypeId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="MessagePayload" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Guid MessagePayloadId
    {
      get { return Get(ref _messagePayloadId, "MessagePayloadId"); }
      set { Set(ref _messagePayloadId, value, "MessagePayloadId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [System.Runtime.Serialization.DataContract]
  [Table("Messaging.MessagePayload", IdentityMethod=IdentityMethod.GuidComb)]
  public partial class MessagePayloadEntity : Entity<System.Guid>
  {
    #region Fields
  
    private string _data;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Data entity attribute.</summary>
    public const string DataField = "Data";


    #endregion
    
    #region Relationships

    [ReverseAssociation("MessagePayload")]
    private readonly EntityHolder<MessageEntity> _message = new EntityHolder<MessageEntity>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public MessageEntity Message
    {
      get { return Get(_message); }
      set { Set(_message, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Data
    {
      get { return Get(ref _data, "Data"); }
      set { Set(ref _data, value, "Data"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [System.Runtime.Serialization.DataContract]
  [Table("Messaging.MessageType", IdentityMethod=IdentityMethod.GuidComb)]
  [Cached(ExpiryMinutes=60)]
  public partial class MessageTypeEntity : Entity<System.Guid>
  {
    #region Fields
  
    [ValidateLength(0, 450)]
    [ValidatePresence]
    [ValidateUnique]
    private string _typeName;
    [ValidateLength(0, 500)]
    private string _schedulePlan;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the TypeName entity attribute.</summary>
    public const string TypeNameField = "TypeName";
    /// <summary>Identifies the SchedulePlan entity attribute.</summary>
    public const string SchedulePlanField = "SchedulePlan";


    #endregion
    
    #region Relationships

    [ReverseAssociation("MessageType")]
    private EntityCollection<MessageEntity> _messages = new EntityCollection<MessageEntity>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<MessageEntity> Messages
    {
      get { return Get(_messages); }
      set { _messages = value; }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string TypeName
    {
      get { return Get(ref _typeName, "TypeName"); }
      set { Set(ref _typeName, value, "TypeName"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string SchedulePlan
    {
      get { return Get(ref _schedulePlan, "SchedulePlan"); }
      set { Set(ref _schedulePlan, value, "SchedulePlan"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [System.Runtime.Serialization.DataContract]
  [Table("Messaging.MessageLog", IdentityMethod=IdentityMethod.GuidComb)]
  public partial class MessageLogEntity : Entity<System.Guid>
  {
    #region Fields
  
    private System.DateTime _timestamp;
    private string _details;
    private System.Guid _messageId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Timestamp entity attribute.</summary>
    public const string TimestampField = "Timestamp";
    /// <summary>Identifies the Details entity attribute.</summary>
    public const string DetailsField = "Details";
    /// <summary>Identifies the MessageId entity attribute.</summary>
    public const string MessageIdField = "MessageId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("MessageLogs")]
    private readonly EntityHolder<MessageEntity> _message = new EntityHolder<MessageEntity>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public MessageEntity Message
    {
      get { return Get(_message); }
      set { Set(_message, value); }
    }


    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime Timestamp
    {
      get { return Get(ref _timestamp, "Timestamp"); }
      set { Set(ref _timestamp, value, "Timestamp"); }
    }

    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public string Details
    {
      get { return Get(ref _details, "Details"); }
      set { Set(ref _details, value, "Details"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Message" /> property.</summary>
    [System.Runtime.Serialization.DataMember]
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Guid MessageId
    {
      get { return Get(ref _messageId, "MessageId"); }
      set { Set(ref _messageId, value, "MessageId"); }
    }

    #endregion
  }




  /// <summary>
  /// Provides a strong-typed unit of work for working with the Messaging model.
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  public partial class MessagingUnitOfWork : Mindscape.LightSpeed.UnitOfWork
  {

    public System.Linq.IQueryable<MessageEntity> Messages
    {
      get { return this.Query<MessageEntity>(); }
    }
    
    public System.Linq.IQueryable<MessagePayloadEntity> MessagePayloads
    {
      get { return this.Query<MessagePayloadEntity>(); }
    }
    
    public System.Linq.IQueryable<MessageTypeEntity> MessageTypes
    {
      get { return this.Query<MessageTypeEntity>(); }
    }
    
    public System.Linq.IQueryable<MessageLogEntity> MessageLogs
    {
      get { return this.Query<MessageLogEntity>(); }
    }
    
  }

#if LS3_DTOS

  namespace Contracts.Data
  {
    [System.Runtime.Serialization.DataContract(Name="MessagingDtoBase")]
    [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
    public partial class MessagingDtoBase
    {
    }

    [System.Runtime.Serialization.DataContract(Name="MessageEntity")]
    [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
    public partial class MessageEntityDto : MessagingDtoBase
    {
      [System.Runtime.Serialization.DataMember]
      public Framework.Messaging.MessageStatus Status { get; set; }
      [System.Runtime.Serialization.DataMember]
      public Framework.Messaging.MessagePriority Priority { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string Version { get; set; }
      [System.Runtime.Serialization.DataMember]
      public System.DateTime EnqueuedOn { get; set; }
      [System.Runtime.Serialization.DataMember]
      public System.Nullable<System.DateTime> DequeuedOn { get; set; }
      [System.Runtime.Serialization.DataMember]
      public System.Nullable<System.DateTime> HandledOn { get; set; }
      [System.Runtime.Serialization.DataMember]
      public long ExecutionMilliseconds { get; set; }
      [System.Runtime.Serialization.DataMember]
      public bool Failed { get; set; }
      [System.Runtime.Serialization.DataMember]
      public System.Nullable<System.DateTime> ProcessOn { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string EntityName { get; set; }
      [System.Runtime.Serialization.DataMember]
      public System.Nullable<long> EntityKey { get; set; }
      [System.Runtime.Serialization.DataMember]
      public System.Guid MessageTypeId { get; set; }
      [System.Runtime.Serialization.DataMember]
      public System.Guid MessagePayloadId { get; set; }
    }

    [System.Runtime.Serialization.DataContract(Name="MessagePayloadEntity")]
    [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
    public partial class MessagePayloadEntityDto : MessagingDtoBase
    {
      [System.Runtime.Serialization.DataMember]
      public string Data { get; set; }
    }

    [System.Runtime.Serialization.DataContract(Name="MessageTypeEntity")]
    [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
    public partial class MessageTypeEntityDto : MessagingDtoBase
    {
      [System.Runtime.Serialization.DataMember]
      public string TypeName { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string SchedulePlan { get; set; }
    }

    [System.Runtime.Serialization.DataContract(Name="MessageLogEntity")]
    [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
    public partial class MessageLogEntityDto : MessagingDtoBase
    {
      [System.Runtime.Serialization.DataMember]
      public System.DateTime Timestamp { get; set; }
      [System.Runtime.Serialization.DataMember]
      public string Details { get; set; }
      [System.Runtime.Serialization.DataMember]
      public System.Guid MessageId { get; set; }
    }


    
    [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
    public static partial class MessagingDtoExtensions
    {
      static partial void CopyMessagingDtoBase(Entity entity, MessagingDtoBase dto);
      static partial void CopyMessagingDtoBase(MessagingDtoBase dto, Entity entity);

      static partial void BeforeCopyMessageEntity(MessageEntity entity, MessageEntityDto dto);
      static partial void AfterCopyMessageEntity(MessageEntity entity, MessageEntityDto dto);
      static partial void BeforeCopyMessageEntity(MessageEntityDto dto, MessageEntity entity);
      static partial void AfterCopyMessageEntity(MessageEntityDto dto, MessageEntity entity);
      
      private static void CopyMessageEntity(MessageEntity entity, MessageEntityDto dto)
      {
        BeforeCopyMessageEntity(entity, dto);
        CopyMessagingDtoBase(entity, dto);
        dto.Status = entity.Status;
        dto.Priority = entity.Priority;
        dto.Version = entity.Version;
        dto.EnqueuedOn = entity.EnqueuedOn;
        dto.DequeuedOn = entity.DequeuedOn;
        dto.HandledOn = entity.HandledOn;
        dto.ExecutionMilliseconds = entity.ExecutionMilliseconds;
        dto.Failed = entity.Failed;
        dto.ProcessOn = entity.ProcessOn;
        dto.EntityName = entity.EntityName;
        dto.EntityKey = entity.EntityKey;
        dto.MessageTypeId = entity.MessageTypeId;
        dto.MessagePayloadId = entity.MessagePayloadId;
        AfterCopyMessageEntity(entity, dto);
      }
      
      private static void CopyMessageEntity(MessageEntityDto dto, MessageEntity entity)
      {
        BeforeCopyMessageEntity(dto, entity);
        CopyMessagingDtoBase(dto, entity);
        entity.Status = dto.Status;
        entity.Priority = dto.Priority;
        entity.Version = dto.Version;
        entity.EnqueuedOn = dto.EnqueuedOn;
        entity.DequeuedOn = dto.DequeuedOn;
        entity.HandledOn = dto.HandledOn;
        entity.ExecutionMilliseconds = dto.ExecutionMilliseconds;
        entity.Failed = dto.Failed;
        entity.ProcessOn = dto.ProcessOn;
        entity.EntityName = dto.EntityName;
        entity.EntityKey = dto.EntityKey;
        entity.MessageTypeId = dto.MessageTypeId;
        entity.MessagePayloadId = dto.MessagePayloadId;
        AfterCopyMessageEntity(dto, entity);
      }
      
      public static MessageEntityDto AsDto(this MessageEntity entity)
      {
        MessageEntityDto dto = new MessageEntityDto();
        CopyMessageEntity(entity, dto);
        return dto;
      }
      
      public static MessageEntity CopyTo(this MessageEntityDto source, MessageEntity entity)
      {
        CopyMessageEntity(source, entity);
        return entity;
      }

      static partial void BeforeCopyMessagePayloadEntity(MessagePayloadEntity entity, MessagePayloadEntityDto dto);
      static partial void AfterCopyMessagePayloadEntity(MessagePayloadEntity entity, MessagePayloadEntityDto dto);
      static partial void BeforeCopyMessagePayloadEntity(MessagePayloadEntityDto dto, MessagePayloadEntity entity);
      static partial void AfterCopyMessagePayloadEntity(MessagePayloadEntityDto dto, MessagePayloadEntity entity);
      
      private static void CopyMessagePayloadEntity(MessagePayloadEntity entity, MessagePayloadEntityDto dto)
      {
        BeforeCopyMessagePayloadEntity(entity, dto);
        CopyMessagingDtoBase(entity, dto);
        dto.Data = entity.Data;
        AfterCopyMessagePayloadEntity(entity, dto);
      }
      
      private static void CopyMessagePayloadEntity(MessagePayloadEntityDto dto, MessagePayloadEntity entity)
      {
        BeforeCopyMessagePayloadEntity(dto, entity);
        CopyMessagingDtoBase(dto, entity);
        entity.Data = dto.Data;
        AfterCopyMessagePayloadEntity(dto, entity);
      }
      
      public static MessagePayloadEntityDto AsDto(this MessagePayloadEntity entity)
      {
        MessagePayloadEntityDto dto = new MessagePayloadEntityDto();
        CopyMessagePayloadEntity(entity, dto);
        return dto;
      }
      
      public static MessagePayloadEntity CopyTo(this MessagePayloadEntityDto source, MessagePayloadEntity entity)
      {
        CopyMessagePayloadEntity(source, entity);
        return entity;
      }

      static partial void BeforeCopyMessageTypeEntity(MessageTypeEntity entity, MessageTypeEntityDto dto);
      static partial void AfterCopyMessageTypeEntity(MessageTypeEntity entity, MessageTypeEntityDto dto);
      static partial void BeforeCopyMessageTypeEntity(MessageTypeEntityDto dto, MessageTypeEntity entity);
      static partial void AfterCopyMessageTypeEntity(MessageTypeEntityDto dto, MessageTypeEntity entity);
      
      private static void CopyMessageTypeEntity(MessageTypeEntity entity, MessageTypeEntityDto dto)
      {
        BeforeCopyMessageTypeEntity(entity, dto);
        CopyMessagingDtoBase(entity, dto);
        dto.TypeName = entity.TypeName;
        dto.SchedulePlan = entity.SchedulePlan;
        AfterCopyMessageTypeEntity(entity, dto);
      }
      
      private static void CopyMessageTypeEntity(MessageTypeEntityDto dto, MessageTypeEntity entity)
      {
        BeforeCopyMessageTypeEntity(dto, entity);
        CopyMessagingDtoBase(dto, entity);
        entity.TypeName = dto.TypeName;
        entity.SchedulePlan = dto.SchedulePlan;
        AfterCopyMessageTypeEntity(dto, entity);
      }
      
      public static MessageTypeEntityDto AsDto(this MessageTypeEntity entity)
      {
        MessageTypeEntityDto dto = new MessageTypeEntityDto();
        CopyMessageTypeEntity(entity, dto);
        return dto;
      }
      
      public static MessageTypeEntity CopyTo(this MessageTypeEntityDto source, MessageTypeEntity entity)
      {
        CopyMessageTypeEntity(source, entity);
        return entity;
      }

      static partial void BeforeCopyMessageLogEntity(MessageLogEntity entity, MessageLogEntityDto dto);
      static partial void AfterCopyMessageLogEntity(MessageLogEntity entity, MessageLogEntityDto dto);
      static partial void BeforeCopyMessageLogEntity(MessageLogEntityDto dto, MessageLogEntity entity);
      static partial void AfterCopyMessageLogEntity(MessageLogEntityDto dto, MessageLogEntity entity);
      
      private static void CopyMessageLogEntity(MessageLogEntity entity, MessageLogEntityDto dto)
      {
        BeforeCopyMessageLogEntity(entity, dto);
        CopyMessagingDtoBase(entity, dto);
        dto.Timestamp = entity.Timestamp;
        dto.Details = entity.Details;
        dto.MessageId = entity.MessageId;
        AfterCopyMessageLogEntity(entity, dto);
      }
      
      private static void CopyMessageLogEntity(MessageLogEntityDto dto, MessageLogEntity entity)
      {
        BeforeCopyMessageLogEntity(dto, entity);
        CopyMessagingDtoBase(dto, entity);
        entity.Timestamp = dto.Timestamp;
        entity.Details = dto.Details;
        entity.MessageId = dto.MessageId;
        AfterCopyMessageLogEntity(dto, entity);
      }
      
      public static MessageLogEntityDto AsDto(this MessageLogEntity entity)
      {
        MessageLogEntityDto dto = new MessageLogEntityDto();
        CopyMessageLogEntity(entity, dto);
        return dto;
      }
      
      public static MessageLogEntity CopyTo(this MessageLogEntityDto source, MessageLogEntity entity)
      {
        CopyMessageLogEntity(source, entity);
        return entity;
      }


    }

  }

#endif
}
