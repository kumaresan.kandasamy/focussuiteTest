﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives



#endregion

namespace Framework.Messaging
{
	public enum MessageStatus
	{
		Enqueued = 1,
		Dequeued = 2,
		Handled = 3,
		Archived = 9
	}

	public enum MessagePriority
	{
		Low = 1,
		Medium = 2,
		High = 3,
		Direct = 4
	}
}
