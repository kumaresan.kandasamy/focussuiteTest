﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Framework.Core;
using Framework.Exceptions;
using Framework.Logging;
using Framework.Messaging.Entities;
using Framework.ServiceLocation;

#endregion

namespace Framework.Messaging
{
	public class MessageBusHandler
	{
		private readonly IMessageStore _messageStore;

		/// <summary>
		/// Initializes a new instance of the <see cref="MessageBusHandler" /> class.
		/// </summary>
		public MessageBusHandler() : this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="MessageBusHandler" /> class.
		/// </summary>
		/// <param name="messageStore">The message store.</param>
		public MessageBusHandler(IMessageStore messageStore)
		{
			_messageStore = messageStore ?? ServiceLocator.Current.Resolve<IMessageStore>();
		}

		/// <summary>
		/// Processes this instance.
		/// </summary>
		/// <param name="priority">The priority.</param>
		/// <param name="version">The version.</param>
		/// <param name="messageTypes">Optional message types to target.</param>
		/// <param name="excludeMessageTypes">Optional message types to exclude.</param>
		/// <param name="iterations">The number handle iterations.</param>
		public void Process(MessagePriority? priority = null, string version = null, List<Guid> messageTypes = null, List<Guid> excludeMessageTypes = null, int iterations = 1)
		{
			for (var i=0; i<iterations; i++)
			{
				MessageEntity messageEntity;
				var message = _messageStore.Dequeue(out messageEntity, priority, version, messageTypes, excludeMessageTypes);

				if (message.IsNotNull() && messageEntity.IsNotNull())
					Handle(messageEntity, message as dynamic, version);
			}
		}

		/// <summary>
		/// Processes this instance.
		/// </summary>
		/// <param name="priority">The priority.</param>
		/// <param name="version">The version.</param>
		/// <param name="messageTypes">Optional message types to target.</param>
		/// <param name="excludeMessageTypes">Optional message types to exclude.</param>
		public Tuple<IMessage, MessageEntity> Dequeue(MessagePriority? priority = null, string version = null, List<Guid> messageTypes = null, List<Guid> excludeMessageTypes = null)
		{
			MessageEntity messageEntity;
			var message = _messageStore.Dequeue(out messageEntity, priority, version, messageTypes, excludeMessageTypes);

			if (message.IsNotNull() && messageEntity.IsNotNull())
				return new Tuple<IMessage, MessageEntity>(message, messageEntity);

			return null;
		}

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <typeparam name="TMessage">The type of the message.</typeparam>
		/// <param name="messageEntity">The base message</param>
		/// <param name="message">The message.</param>
		/// <param name="version">The version.</param>
		/// <param name="isHandledInProcess">if set to <c>true</c> then this is being handled in process.</param>
		/// <exception cref="MessageBusException">
		/// </exception>
		public void Handle<TMessage>(MessageEntity messageEntity, TMessage message, string version, bool isHandledInProcess = false) where TMessage : class, IMessage
		{
			var log = new StringBuilder();
			var handleStopwatch = Stopwatch.StartNew();

			var messageHandlers = ServiceLocator.Current.ResolveServices<IMessageHandler<TMessage>>();

			log.AppendLine("Handling of message starting on thread#" + Thread.CurrentThread.ManagedThreadId + (isHandledInProcess ? " - InProcess" : ""));
			log.AppendLine(" ");

			// Hopefully we have the message handlers registered
			if (messageHandlers.IsNullOrEmpty())
			{
				handleStopwatch.Stop();

				log.AppendLine("No handlers have been registered for message type " + message.GetType().AssemblyQualifiedName);
				log.AppendLine(" ");
				log.AppendLine("Handling of message complete");

				_messageStore.RegisterFailure(message, handleStopwatch.ElapsedMilliseconds, log.ToString());

				throw new MessageBusException(string.Format("No handlers have been registered for message type {0}", message.GetType().AssemblyQualifiedName));
			}

			// All should be ok as we have at least 1 handler for this message type
			// So lets crack on and call each handler

			try
			{
				foreach (var messageHandler in messageHandlers)
				{
					log.AppendLine("Passing message to be handled by: " + messageHandler.GetType().AssemblyQualifiedName);

					// Call the message handler
					var handlerStopwatch = Stopwatch.StartNew();

					try
					{
						messageHandler.Handle(message);

						handlerStopwatch.Stop();

						log.AppendLine("Message handled successfully and it took " + handlerStopwatch.ElapsedMilliseconds + " ms.");
					}
					catch (Exception exception)
					{
						handlerStopwatch.Stop();

						log.AppendLine("Message handled unsuccessfully and it took " + handlerStopwatch.ElapsedMilliseconds + " ms.");
						log.AppendLine(" ");
						log.AppendLine("Exception:");
						log.AppendLine(exception.FormatException());

						// Don't process any other handlers that maybe registered for this message as it maybe goosed.
						// Normally we would only have 1 handler per message type so this should be ok)
						throw;
					}

					// Add a nice blank like
					log.AppendLine(" ");
				}

				handleStopwatch.Stop();

				log.AppendLine("Handling of message complete");

				_messageStore.RegisterSuccess(message, handleStopwatch.ElapsedMilliseconds, log.ToString());
			}
			catch (Exception exception)
			{
				_messageStore.RegisterFailure(message, handleStopwatch.ElapsedMilliseconds, log.ToString());
				throw new MessageBusException(string.Format("Message Bus Handler Exception"), exception);
			}
			finally
			{
				// Check if this was a scheduled message
				if (messageEntity != null && messageEntity.ProcessOn.IsNotNull() && messageEntity.ProcessOn.Value.Year > 1900 && messageEntity.EntityKey.IsNull())
				{
					var messageType = _messageStore.GetMessageType(message);
					if (messageType.SchedulePlan.IsNotNullOrEmpty())
					{
						var plan = messageType.SchedulePlan.DeserializeJson<MessageSchedulePlan>();
						var processOn = plan.GetNextProcessDate(messageEntity.ProcessOn.Value);

						message.Id = Message.NewId();
						message.Version = version;
						_messageStore.Enqueue(message, messageEntity.Priority, false, processOn);
					}
				}
			}
		}		
	}
}
