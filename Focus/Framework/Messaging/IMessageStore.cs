﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.ServiceModel.Channels;
using Framework.Messaging.Entities;

#endregion

namespace Framework.Messaging
{
	public interface IMessageStore
	{
	  /// <summary>
	  /// Updates any scheduled messages to ensure they are on the correct version
	  /// </summary>
	  /// <param name="version">The current version.</param>
	  void UpdateScheduledMessages(string version);

		/// <summary>
		/// Gets the count of messages in the store
		/// </summary>
		/// <param name="status">The status.</param>
		/// <param name="priority">The priority.</param>
		/// <param name="version">The version.</param>
    /// <param name="messageTypes">Optional message types to target.</param>
    /// <param name="excludeMessageTypes">Optional message types to exclude.</param>
    /// <returns></returns>
    int Count(MessageStatus? status = null, MessagePriority? priority = null, string version = null, List<Guid> messageTypes = null, List<Guid> excludeMessageTypes = null);

    /// <summary>
    /// Gets if there are any messages in the store
    /// </summary>
    /// <param name="status">The status.</param>
    /// <param name="priority">The priority.</param>
    /// <param name="version">The version.</param>
    /// <param name="messageTypes">Optional message types to target.</param>
    /// <param name="excludeMessageTypes">Optional message types to exclude.</param>
    /// <returns></returns>
    bool Any(MessageStatus? status = null, MessagePriority? priority = null, string version = null, List<Guid> messageTypes = null, List<Guid> excludeMessageTypes = null);

		/// <summary>
		/// Enqueues the message.
		/// </summary>
		/// <typeparam name="TMessage">The type of the message.</typeparam>
		/// <param name="message">The message.</param>
		/// <param name="priority">The priority.</param>
		/// <param name="updateIfExists">Update any existing message with matching entity key and type if not yet processed</param>
		/// <param name="processOn">An optional future date to process the message.</param>
		/// <param name="entityName">An optional name for the entity being processed in the message.</param>
		/// <param name="entityKey">The optional key for the entity being processed in the message.</param>
		/// <param name="markDequeued">if set to <c>true</c> then mark the message on the bus as dequeued.</param>
		void Enqueue<TMessage>(TMessage message, MessagePriority? priority = null, bool updateIfExists = false, DateTime? processOn = null, string entityName = null, long? entityKey = null, bool markDequeued = false) where TMessage : class, IMessage;

		/// <summary>
		/// Dequeues the specified priority.
		/// </summary>
    /// <param name="message">Will pass back the actual message</param>
		/// <param name="priority">The priority.</param>
		/// <param name="version">The version.</param>
    /// <param name="messageTypes">Optional message types to target.</param>
    /// <param name="excludeMessageTypes">Optional message types to exclude.</param>
    /// <returns></returns>
    IMessage Dequeue(out MessageEntity message, MessagePriority? priority = null, string version = null, List<Guid> messageTypes = null, List<Guid> excludeMessageTypes = null);

		/// <summary>
		/// Registers the success.
		/// </summary>
		/// <typeparam name="TMessage">The type of the message.</typeparam>
		/// <param name="message">The message.</param>
		/// <param name="executionMilliseconds">The execution time milliseconds.</param>
		/// <param name="log">The log.</param>
		void RegisterSuccess<TMessage>(TMessage message, long executionMilliseconds, string log) where TMessage : class, IMessage;

		/// <summary>
		/// Registers the failure.
		/// </summary>
		/// <typeparam name="TMessage">The type of the message.</typeparam>
		/// <param name="message">The message.</param>
		/// <param name="executionMilliseconds">The execution time milliseconds.</param>
		/// <param name="log">The log.</param>
		void RegisterFailure<TMessage>(TMessage message, long executionMilliseconds, string log) where TMessage : class, IMessage;

	  /// <summary>
	  /// Gets a MessageType record for a message
	  /// </summary>
	  /// <param name="message">The message whose type is required</param>
	  /// <returns>The MessageType record</returns>
    MessageTypeEntity GetMessageType<TMessage>(TMessage message) where TMessage : class, IMessage;

	  /// <summary>
	  /// Schedules a message to run at a set interval
	  /// </summary>
	  /// <param name="message">The message to schedule</param>
	  /// <param name="schedulePlan">The planned schedule to run</param>
    /// <param name="messagePriority">The message priority.</param>
    void ScheduleMessage<TMessage>(TMessage message, MessageSchedulePlan schedulePlan, MessagePriority? messagePriority = MessagePriority.Medium) where TMessage : class, IMessage;

    /// <summary>
    /// Peeks at messages in the message bus without processing them
    /// </summary>
    /// <typeparam name="TMessage">The type of the message.</typeparam>
    /// <param name="version">The version.</param>
    /// <param name="number">The number of messages to return.</param>
    /// <returns>A list of messages of the specified type and version</returns>
    List<TMessage> PeekMessages<TMessage>(string version = null, int number = 1) where TMessage : class, IMessage;

		/// <summary>
		/// Peeks at a messages in the message bus without processing it
		/// </summary>
		/// <param name="messageId">The message identifier.</param>
		/// <returns>A Tuple of IMessage and MessageEntity</returns>
		Tuple<IMessage, MessageEntity> PeekMessage(Guid messageId);
	}
}
