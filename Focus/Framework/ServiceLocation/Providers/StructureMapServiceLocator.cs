﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;

using StructureMap;
using StructureMap.Configuration.DSL;

#endregion

namespace Framework.ServiceLocation
{
	public class StructureMapServiceLocator : IServiceLocator
	{
		private static bool _isDisposing;

		/// <summary>
		/// Initializes a new instance of the <see cref="StructureMapServiceLocator" /> class.
		/// </summary>
		public StructureMapServiceLocator() : this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="StructureMapServiceLocator" /> class.
		/// </summary>
		/// <param name="container">The container.</param>
		public StructureMapServiceLocator(IContainer container)
		{
			Container = container ?? CreateNewContainer();
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			if (_isDisposing) return;
			if (Container == null) return;

			_isDisposing = true;

			Container.Dispose();
			Container = null;
		}

		protected IContainer Container { get; private set; }

		/// <summary>
		/// Creates the new structure map container.
		/// </summary>
		/// <returns></returns>
		private static IContainer CreateNewContainer()
		{
			ObjectFactory.Initialize(x => x.AddRegistry(new Registry()));			
			return ObjectFactory.Container;
		}

		#region Registration, resolution, release, reset, inject and teardown methods

		/// <summary>
		/// Resolves this instance.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public T Resolve<T>() where T : class
		{
			try
			{
				return Container.GetInstance<T>();
			}
			catch (Exception ex)
			{
				throw new ServiceResolutionException(typeof(T), ex);
			}
		}

		/// <summary>
		/// Resolves the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public T Resolve<T>(string key) where T : class
		{
			try
			{
				return Container.GetInstance<T>(key);
			}
			catch (Exception ex)
			{
				throw new ServiceResolutionException(typeof(T), ex);
			}
		}

		/// <summary>
		/// Resolves the specified type.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		public object Resolve(Type type)
		{
			try
			{
				return Container.GetInstance(type);
			}
			catch (Exception ex)
			{
				throw new ServiceResolutionException(type, ex);
			}
		}

		/// <summary>
		/// Resolves the services.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public IList<T> ResolveServices<T>() where T : class
		{
			return Container.GetAllInstances<T>();
		}

		/// <summary>
		/// Registers the specified impl type.
		/// </summary>
		/// <typeparam name="TInterface">The type of the interface.</typeparam>
		/// <param name="implType">Type of the impl.</param>
		public void Register<TInterface>(Type implType) where TInterface : class
		{
			var key = string.Format("{0}-{1}", typeof(TInterface).Name, implType.FullName);
			Container.Configure(x => x.For(implType).Use(implType).Named(key));

			// Work-around, also register this implementation to service mapping
			// without the generated key above.
			Container.Configure(x => x.For(implType).Use(implType));
		}

		/// <summary>
		/// Registers this instance.
		/// </summary>
		/// <typeparam name="TInterface">The type of the interface.</typeparam>
		/// <typeparam name="TImplementation">The type of the implementation.</typeparam>
		public void Register<TInterface, TImplementation>() where TImplementation : class, TInterface
		{
			Container.Configure(x => x.For<TInterface>().Use<TImplementation>());
		}

		/// <summary>
		/// Registers the specified key.
		/// </summary>
		/// <typeparam name="TInterface">The type of the interface.</typeparam>
		/// <typeparam name="TImplementation">The type of the implementation.</typeparam>
		/// <param name="key">The key.</param>
		public void Register<TInterface, TImplementation>(string key) where TImplementation : class, TInterface
		{
			Container.Configure(x => x.For<TInterface>().Use<TImplementation>().Named(key));
		}

		/// <summary>
		/// Registers the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="type">The type.</param>
		public void Register(string key, Type type)
		{
			Container.Configure(x => x.For(type).Use(type).Named(key));
		}

		/// <summary>
		/// Registers the specified service type.
		/// </summary>
		/// <param name="serviceType">Type of the service.</param>
		/// <param name="implType">Type of the impl.</param>
		public void Register(Type serviceType, Type implType)
		{
			Container.Configure(x => x.For(serviceType).Use(implType));
		}

		/// <summary>
		/// Registers the specified instance.
		/// </summary>
		/// <typeparam name="TInterface">The type of the interface.</typeparam>
		/// <param name="instance">The instance.</param>
		public void Register<TInterface>(TInterface instance) where TInterface : class
		{
			Container.Configure(x => x.For<TInterface>().Use(instance));
		}

		/// <summary>
		/// Registers the specified factory method.
		/// </summary>
		/// <typeparam name="TInterface">The type of the interface.</typeparam>
		/// <param name="factoryMethod">The factory method.</param>
		public void Register<TInterface>(Func<TInterface> factoryMethod) where TInterface : class
		{
			Container.Configure(x => x.For<TInterface>().Use(factoryMethod));
		}

		/// <summary>
		/// Releases the specified instance.
		/// </summary>
		/// <param name="instance">The instance.</param>
		public void Release(object instance)
		{
			// Not needed for StructureMap it doesn't keep references beyond the life cycle that was configured.
		}

		/// <summary>
		/// Resets this instance.
		/// </summary>
		public void Reset()
		{
			throw new NotSupportedException("StructureMap does not support reset");
		}

		/// <summary>
		/// Injects the specified instance.
		/// </summary>
		/// <typeparam name="TService">The type of the service.</typeparam>
		/// <param name="instance">The instance.</param>
		/// <returns></returns>
		public TService Inject<TService>(TService instance) where TService : class
		{
			if (instance == null)
				return null;

			Container.BuildUp(instance);

			return instance;
		}

		/// <summary>
		/// Tears down.
		/// </summary>
		/// <typeparam name="TService">The type of the service.</typeparam>
		/// <param name="instance">The instance.</param>
		public void TearDown<TService>(TService instance) where TService : class
		{
			// Not needed for StructureMap it doesn't keep references beyond the life cycle that was configured.
		}

		#endregion
	}
}
