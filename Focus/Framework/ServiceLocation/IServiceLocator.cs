#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;

#endregion

namespace Framework.ServiceLocation
{
	public interface IServiceLocator : IDisposable
	{
		T Resolve<T>() where T : class;
		T Resolve<T>(string key) where T : class;
		object Resolve(Type type);
		IList<T> ResolveServices<T>() where T : class;
		void Register<TInterface>(Type implType) where TInterface : class;
		void Register<TInterface, TImplementation>() where TImplementation : class, TInterface;
		void Register<TInterface, TImplementation>(string key) where TImplementation : class, TInterface;
		void Register(string key, Type type);
		void Register(Type serviceType, Type implType);
		void Register<TInterface>(TInterface instance) where TInterface : class;
		void Release(object instance);
		void Reset();
		TService Inject<TService>(TService instance) where TService : class;
		void TearDown<TService>(TService instance) where TService : class;
		void Register<TInterface>(Func<TInterface> factoryMethod) where TInterface : class;
	}
}
