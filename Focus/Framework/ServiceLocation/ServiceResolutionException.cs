﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Framework.ServiceLocation
{
	[Serializable]
	public class ServiceResolutionException : Exception
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ServiceResolutionException" /> class.
		/// </summary>
		/// <param name="service">The service.</param>
		public ServiceResolutionException(Type service) : base(string.Format("Could not resolve serviceType '{0}'", service))
		{
			ServiceType = service;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ServiceResolutionException" /> class.
		/// </summary>
		/// <param name="service">The service.</param>
		/// <param name="innerException">The inner exception.</param>
		public ServiceResolutionException(Type service, Exception innerException) : base(string.Format("Could not resolve serviceType '{0}'", service), innerException)
		{
			ServiceType = service;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ServiceResolutionException" /> class.
		/// </summary>
		/// <param name="info">The info.</param>
		/// <param name="context">The context.</param>
		public ServiceResolutionException(SerializationInfo info, StreamingContext context) : base(info, context)
		{ }

		/// <summary>
		/// Gets or sets the type of the service.
		/// </summary>
		/// <value>
		/// The type of the service.
		/// </value>
		public Type ServiceType { get; set; }
	}
}
