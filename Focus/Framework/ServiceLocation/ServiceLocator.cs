#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

#endregion

namespace Framework.ServiceLocation
{
	/// <summary>
	/// This class provides the ambient container for this application. If your
	/// framework defines such an ambient container, use ServiceLocator.Current
	/// to get it.
	/// </summary>
	public static class ServiceLocator
	{
		/// <summary>
		/// The current ambient container.
		/// </summary>
		public static IServiceLocator Current { get; private set; }

		/// <summary>
		/// Sets the locator provider.
		/// </summary>
		/// <param name="serviceLocator">The service locator.</param>
		public static void SetServiceLocator(IServiceLocator serviceLocator)
		{
			Current = serviceLocator;
		}
	}
}
