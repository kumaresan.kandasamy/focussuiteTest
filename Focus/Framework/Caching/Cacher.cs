﻿using System;
using System.Collections;

using Framework.Caching.Providers;
using System.Web.Caching;

namespace Framework.Caching
{
	/// <summary>
	/// Cacher class through which all caching is handled.
	/// </summary>
	public sealed class Cacher
	{
		/// <summary>
		/// Initializes the <see cref="Cacher"/> class.
		/// </summary>
		static Cacher()
		{
			Provider = new CacheToHttpRuntime(null);
		}

		/// <summary>
		/// Get the current cache provider being used.
		/// </summary>
		public static ICacheProvider Provider { get; private set; }

		/// <summary>
		/// Initialize the cache provider.
		/// </summary>
		/// <param name="cacheProvider"></param>
		public static void Initialize(ICacheProvider cacheProvider)
		{
			Provider = cacheProvider;
		}

		/// <summary>
		/// Gets the number of items in the cache.
		/// </summary>
		public static int Count
		{
			get { return Provider.Count; }
		}

		/// <summary>
		/// Gets a collection of all cached item keys.
		/// </summary>
		public static ICollection Keys
		{
			get { return Provider.Keys; }
		}

		/// <summary>
		/// Determines whether the cache contains the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>
		/// 	<c>true</c> if the cache contains the specified key; otherwise, <c>false</c>.
		/// </returns>
		public static bool Contains(string key)
		{
			return Provider.Contains(key);
		}

    /// <summary>
    /// Gets the cached item for the specified key.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="overrideWithHTTPCaching">if set to <c>true</c> [override with HTTP caching].</param>
    /// <returns>
    ///   <c>cached value</c> if found in the cache using the specified key; otherwise, <c>null</c>
    /// </returns>
		public static object Get(string key, bool overrideWithHTTPCaching = false)
		{
      return Provider.Get(key, overrideWithHTTPCaching);
		}

		/// <summary>
		/// Gets the cached item for the specified key.
		/// </summary>
		/// <typeparam name="T">The type of value to return.</typeparam>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public static T Get<T>(string key)
		{
			return Provider.Get<T>(key);
		}

		/// <summary>
		/// Sets an item in the cache for the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="data">The data.</param>
		public static void Set(string key, object data)
		{
			Provider.Set(key, data);
		}

		/// <summary>
		/// Sets the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		/// <param name="dependency">The cache dependency.</param>
		public static void Set(string key, object value, CacheDependency dependency)
		{
			Provider.Set(key, value, dependency);
		}

		/// <summary>
		/// Sets the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		/// <param name="absoluteExpiration">The absolute expiration.</param>
		public static void Set(string key, object value, DateTime absoluteExpiration)
		{
			Provider.Set(key, value, absoluteExpiration);
		}

		/// <summary>
		/// Removes an item from the cache for the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		public static void Remove(string key)
		{
			Provider.Remove(key);
		}

		/// <summary>
		/// Removes all items from the cache for the collection of specified keys.
		/// </summary>
		/// <param name="keys">The keys.</param>
		public static void RemoveAll(ICollection keys)
		{
			Provider.RemoveAll(keys);
		}

		/// <summary>
		/// Clear all the items in the cache.
		/// </summary>
		public static void Clear()
		{
			Provider.Clear();
		}
	}
}
