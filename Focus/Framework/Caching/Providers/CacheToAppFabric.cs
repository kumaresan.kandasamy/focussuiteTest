﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

using Microsoft.ApplicationServer.Caching;

#endregion

namespace Framework.Caching.Providers
{
  public class CacheToAppFabric : ICacheProvider
  {
    private static DataCacheFactory _cacheFactory;
    private static DataCache _defaultCache;
    private static Cache _cache;
    private const string DefaultCacheRegion = "FocusSuiteRegion";

    private readonly object _syncObject = new object();

    public CacheToAppFabric(IEnumerable<Tuple<string, int>> hosts, string cacheIdentifier)
    {
      if (_cacheFactory == null)
      {
        // Set server properties
        var servers = hosts.Select(host => new DataCacheServerEndpoint(host.Item1, host.Item2)).ToList();
        //Create cache configuration
        var configuration = new DataCacheFactoryConfiguration
        {
          LocalCacheProperties = new DataCacheLocalCacheProperties(),
          Servers = servers
        };
        _cacheFactory = new DataCacheFactory(configuration);
      }

      //Disable tracing to avoid informational/verbose messages on the web page
      //DataCacheClientLogManager.ChangeLogLevel(System.Diagnostics.TraceLevel.Off);

      // Set default cache
      if (_defaultCache == null)
      {
        _defaultCache = _cacheFactory.GetCache(cacheIdentifier);
        _defaultCache.CreateRegion(DefaultCacheRegion);
      }

      if (_cache == null)
        _cache = HttpRuntime.Cache;
    }


    #region Implementation of ICacheProvider

    public int Count
    {
      get { return (from region in _defaultCache.GetSystemRegions() from kvp in _defaultCache.GetObjectsInRegion(region) select kvp.Key).Count(); }
    }

    public ICollection Keys
    {
      get
      {
        return (from region in _defaultCache.GetSystemRegions() from kvp in _defaultCache.GetObjectsInRegion(region) select kvp.Key).ToList();
      }
    }

    public bool Contains(string key)
    {
      var entry = _cache.Get(key);
      return entry != null;
    }

    public object Get(string key, bool overrideWithHttpCaching)
    {
      return overrideWithHttpCaching ? _cache.Get(key) : _defaultCache.Get(key, DefaultCacheRegion);
    }

    public T Get<T>(string key)
    {
      var obj = _defaultCache.Get(key, DefaultCacheRegion);
       return (obj == null ? default(T) : (T)obj);
    }

    public void Set(string key, object value)
    {
      lock (_syncObject)
        _defaultCache.Put(key, value, DefaultCacheRegion);
    }

    public void Set(string key, object value, DateTime absoluteExpiration)
    {
      var span = absoluteExpiration - DateTime.Now;
      lock (_syncObject)
        _defaultCache.Put(key, value, span, DefaultCacheRegion);
    }

    public void Set(string key, object value, CacheDependency dependency)
    {
      // Use IIS caching for file dependency
      lock (_syncObject)
        _cache.Insert(key, value, dependency);
    }

    public void Remove(string key)
    {
      lock (_syncObject)
        _defaultCache.Remove(key, DefaultCacheRegion);
    }

    public void RemoveAll(ICollection keys)
    {
      lock (_syncObject)
      foreach (var key in keys)
      {
        _defaultCache.Remove((string)key, DefaultCacheRegion);
      }
    }

    public void Clear()
    {
	    lock (_syncObject)
	    {
		    foreach (var regionName in _defaultCache.GetSystemRegions())
		    {
			    _defaultCache.ClearRegion(regionName);
		    }

		    _defaultCache.ClearRegion(DefaultCacheRegion);
	    }
    }

    #endregion
  }
}
