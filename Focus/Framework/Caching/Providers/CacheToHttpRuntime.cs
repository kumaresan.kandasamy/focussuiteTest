﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace Framework.Caching.Providers
{
	public class CacheToHttpRuntime : ICacheProvider
	{
		private readonly Cache _cache;
		private readonly string _keyPrefix;

		private readonly object _syncObject = new object();

		/// <summary>
		/// Initializes a new instance of the <see cref="CacheToHttpRuntime"/> class.
		/// </summary>
		/// <param name="keyPrefix">The key prefix.</param>
		public CacheToHttpRuntime(string keyPrefix)
		{
			_cache = HttpRuntime.Cache;
			_keyPrefix = (string.IsNullOrEmpty(keyPrefix) ? "BurningGlass" : keyPrefix) + ".";
		}

		/// <summary>
		/// Builds the key using the prefix.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>A simple or prefixed key</returns>
		private string BuildKey(string key)
		{
			return _keyPrefix + key;
		}

		/// <summary>
		/// Gets the number of items in the cache.
		/// </summary>
		public int Count
		{
			get { return (from DictionaryEntry entry in _cache select (string)entry.Key).Count(key => key.StartsWith(_keyPrefix)); }
		}

		/// <summary>
		/// Gets a collection of all cached item keys.
		/// </summary>
		public ICollection Keys
		{
			get
			{
				return (from DictionaryEntry entry in _cache
								select (string)entry.Key
									into key
									where key.StartsWith(_keyPrefix)
									select key.Substring(_keyPrefix.Length + 1)).ToList();
			}
		}

		/// <summary>
		/// Determines whether the cache contains the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>
		/// 	<c>true</c> if the cache contains the specified key; otherwise, <c>false</c>.
		/// </returns>
		public bool Contains(string key)
		{
			var exactKey = BuildKey(key);
			var entry = _cache.Get(exactKey);
			return entry != null;
		}

    /// <summary>
    /// Gets the cached item for the specified key.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="overrideWithHttpCaching">if set to <c>true</c> [override with HTTP caching].</param>
    /// <returns>
    ///   <c>cached value</c> if found in the cache using the specified key; otherwise, <c>null</c>
    /// </returns>
		public object Get(string key, bool overrideWithHttpCaching)
		{
			return _cache.Get(BuildKey(key));
		}

		/// <summary>
		/// Gets a cached item using the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public T Get<T>(string key)
		{
			var obj = _cache.Get(BuildKey(key));
			return (obj == null ? default(T) : (T)obj);
		}

		/// <summary>
		/// Sets a cached item using the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		public void Set(string key, object value)
		{
			lock (_syncObject)
				_cache.Insert(BuildKey(key), value);
		}

		/// <summary>
		/// Sets the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		/// <param name="absoluteExpiration">The absolute expiration.</param>
		public void Set(string key, object value, DateTime absoluteExpiration)
		{
			lock (_syncObject)
				_cache.Insert(BuildKey(key), value, null, absoluteExpiration, Cache.NoSlidingExpiration);
		}

		/// <summary>
		/// Sets the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		/// <param name="dependency">The cache dependency.</param>
		public void Set(string key, object value, CacheDependency dependency)
		{
			lock (_syncObject)
				_cache.Insert(BuildKey(key), value, dependency);
		}

		/// <summary>
		/// Removes a cached item using the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		public void Remove(string key)
		{
			lock (_syncObject)
				_cache.Remove(BuildKey(key));
		}

		/// <summary>
		/// Removes all items from the cache for the collection of specified keys.
		/// </summary>
		/// <param name="keys">The keys.</param>
		public void RemoveAll(ICollection keys)
		{
			lock (_syncObject)
				foreach (var keyItem in keys)
					_cache.Remove(BuildKey((string)keyItem));

			GC.Collect();
		}

		/// <summary>
		/// Clears the cache of all items.
		/// </summary>
		public void Clear()
		{
			lock (_syncObject)
				foreach (string key in Keys)
					_cache.Remove(BuildKey(key));

			GC.Collect();
		}
	}
}
