﻿using System;
using System.Collections;
using System.Web.Caching;

namespace Framework.Caching
{
	public interface ICacheProvider
	{
		int Count { get; }
		ICollection Keys { get; }
		bool Contains(string key);
    object Get(string key, bool overrideWithHttpCaching);
		T Get<T>(string key);
		void Set(string key, object value);
		void Set(string key, object value, DateTime absoluteExpiration);
		void Set(string key, object value, CacheDependency dependency);
		void Remove(string key);
		void RemoveAll(ICollection keys);
		void Clear();
	}
}
