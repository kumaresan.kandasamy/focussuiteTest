﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.ServiceModel;

using Mindscape.LightSpeed;

#endregion

namespace Framework.Lightspeed
{
	public sealed class PerCallUnitOfWorkScope<TUnitOfWork> : UnitOfWorkScopeBase<TUnitOfWork> where TUnitOfWork : class, IUnitOfWork, new()
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="PerCallUnitOfWorkScope{TUnitOfWork}" /> class.
		/// </summary>
		/// <param name="lightSpeedContext">The light speed context.</param>
		public PerCallUnitOfWorkScope(LightSpeedContext<TUnitOfWork> lightSpeedContext) : base(lightSpeedContext)
		{ }

		public override TUnitOfWork Current
		{
			get
			{
				if (OperationContext.Current == null)
					return null;

				var extension = OperationContext.Current.Extensions.Find<UnitOfWorkScopeExtension<TUnitOfWork>>();

				if (extension == null)
				{
					extension = new UnitOfWorkScopeExtension<TUnitOfWork>(LightSpeedContext.CreateUnitOfWork());
					OperationContext.Current.Extensions.Add(extension);
					OperationContext.Current.OperationCompleted += OperationCompleted;
				}

				return extension.UnitOfWork;
			}
		}

		private void OperationCompleted(object sender, EventArgs e)
		{
			Current.Dispose();
		}

		public override bool HasCurrent
		{
			get { return (OperationContext.Current.Extensions.Find<UnitOfWorkScopeExtension<TUnitOfWork>>() != null); }
		}

		private class UnitOfWorkScopeExtension<TScopeExtensionUnitOfWork> : IExtension<OperationContext> where TScopeExtensionUnitOfWork : class, IUnitOfWork, new()
		{
			public TScopeExtensionUnitOfWork UnitOfWork { get; private set; }

			public UnitOfWorkScopeExtension(TScopeExtensionUnitOfWork unitOfWork)
			{
				if (unitOfWork == null)
					throw new ArgumentNullException("unitOfWork");

				UnitOfWork = unitOfWork;
			}

			void IExtension<OperationContext>.Attach(OperationContext owner)
			{ }

			void IExtension<OperationContext>.Detach(OperationContext owner)
			{ }
		}
	}
}
