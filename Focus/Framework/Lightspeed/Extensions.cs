﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Mindscape.LightSpeed;

using Framework.ServiceLocation;

#endregion

namespace Framework.Lightspeed
{
	public static class Extensions
	{
		/// <summary>
		/// Creates a new context registry that is scoped to the length of a request
		/// </summary>
		/// <typeparam name="TUnitOfWork">The type of the unit of work.</typeparam>
		/// <param name="serviceLocator">The service locator.</param>
		/// <param name="unitOfWorkScope">The unit of work scope.</param>
		/// <param name="contextName">The name of the context to load configuration from the .config file</param>
		public static void AddLightspeedContextFor<TUnitOfWork>(this IServiceLocator serviceLocator, LightspeedUnitOfWorkScope unitOfWorkScope, string contextName) where TUnitOfWork : class, IUnitOfWork, new()
		{
			// Register the creation of the store lightspeed context. This generally should not
			// be referenced directly in our web application - use the unit of work scope below instead.
			serviceLocator.Register(() => new LightSpeedContext<TUnitOfWork>(contextName));

			switch (unitOfWorkScope)
			{
				case LightspeedUnitOfWorkScope.Web:
					// A generic registration for a unit of work scope
					serviceLocator.Register<UnitOfWorkScopeBase<TUnitOfWork>>(serviceLocator.Resolve<PerRequestUnitOfWorkScope<TUnitOfWork>>);

					// Instruct Structure Map on how to create the unit of work
					serviceLocator.Register(() => new PerRequestUnitOfWorkScope<TUnitOfWork>(serviceLocator.Resolve<LightSpeedContext<TUnitOfWork>>()));
					break;

				case LightspeedUnitOfWorkScope.Wcf:
					// A generic registration for a unit of work scope
					serviceLocator.Register<UnitOfWorkScopeBase<TUnitOfWork>>(serviceLocator.Resolve<PerCallUnitOfWorkScope<TUnitOfWork>>);

					// Instruct Structure Map on how to create the unit of work
					serviceLocator.Register(() => new PerCallUnitOfWorkScope<TUnitOfWork>(serviceLocator.Resolve<LightSpeedContext<TUnitOfWork>>()));
					break;

				default:
					// A generic registration for a unit of work scope
					serviceLocator.Register<UnitOfWorkScopeBase<TUnitOfWork>>(serviceLocator.Resolve<SimpleUnitOfWorkScope<TUnitOfWork>>);

					// Instruct Structure Map on how to create the unit of work
					serviceLocator.Register(() => new SimpleUnitOfWorkScope<TUnitOfWork>(serviceLocator.Resolve<LightSpeedContext<TUnitOfWork>>()));
					break;
			}
		}

		/// <summary>
		/// Frees the lightspeed unit of work scope.
		/// </summary>
		/// <typeparam name="TUnitOfWork">The type of the unit of work.</typeparam>
		/// <param name="serviceLocator">The service locator.</param>
		public static void FreeLightspeedUnitOfWorkScope<TUnitOfWork>(this IServiceLocator serviceLocator) where TUnitOfWork : class, IUnitOfWork, new()
		{
			var scope = serviceLocator.Resolve<UnitOfWorkScopeBase<TUnitOfWork>>();
			
			if (scope != null && scope.HasCurrent)
				scope.Current.Dispose();
		}
	}

	public enum LightspeedUnitOfWorkScope
	{
		Simple,
		Web,
		Wcf
	}
}
