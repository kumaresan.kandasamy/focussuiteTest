﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Framework.Core;

using Mindscape.LightSpeed;

#endregion

namespace Framework.Lightspeed
{
  public class EncryptedStringConverter : EncryptionConvetor, IFieldConverter
  {
    /// <summary>
    /// Converts an encrypted value in a database to a string
    /// </summary>
    /// <param name="databaseValue">The encrypted value in the database</param>
    /// <returns>The string value</returns>
    public object ConvertFromDatabase(object databaseValue)
    {
      if (databaseValue.IsDBNull())
        return null;

      var value = (string)databaseValue;
      if (value.Length == 0)
        return null;
        
      return EncryptionEnabled ? Encryptor.DecryptString(value) : value;
    }

    /// <summary>
    /// Encrypts a string value to be saved in the database
    /// </summary>
    /// <param name="value">The value to encrypt and save in the database</param>
    /// <returns>The encrypted value</returns>
    public object ConvertToDatabase(object value)
    {
      var stringValue = (string) value;
      if (stringValue.IsNullOrEmpty())
        return stringValue;

      return EncryptionEnabled ? Encryptor.EncryptToString(stringValue) : stringValue;
    }
  }
}
