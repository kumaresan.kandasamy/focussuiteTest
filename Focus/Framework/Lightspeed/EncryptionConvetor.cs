﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Configuration;

using Framework.Core;
using Framework.Encryption;

#endregion

namespace Framework.Lightspeed
{
  public abstract class EncryptionConvetor
  {
    private static bool? _encryptionEnabled;
    private static AESEncryption _encryptor;

    protected static bool EncryptionEnabled
    {
      get
      {
        if (!_encryptionEnabled.HasValue)
          _encryptionEnabled = Boolean.Parse(ConfigurationManager.AppSettings.Get("Focus-Encryption-Enabled") ?? "false");

        return _encryptionEnabled.Value;
      }
    }

    protected static AESEncryption Encryptor
    {
      get
      {
        if (_encryptor.IsNull())
          _encryptor = new AESEncryption(ConfigurationManager.AppSettings.Get("Focus-Encryption-Key"), ConfigurationManager.AppSettings.Get("Focus-Encryption-Vector"));

        return _encryptor;
      }
    }
  }
}
