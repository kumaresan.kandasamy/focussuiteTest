﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Framework.Core;

using Mindscape.LightSpeed;

#endregion

namespace Framework.Lightspeed
{
  public class EncryptedDateConverter : EncryptionConvetor, IFieldConverter
  {
    /// <summary>
    /// Converts an encrypted value in a database to a datetime
    /// </summary>
    /// <param name="databaseValue">The encrypted value in the database</param>
    /// <returns>The datetime value</returns>
    public object ConvertFromDatabase(object databaseValue)
    {
      if (databaseValue.IsDBNull())
        return null;

      var value = (string)databaseValue;
      if (value.IsNullOrEmpty())
        return null;

      return EncryptionEnabled ? DateTime.Parse(Encryptor.DecryptString(value)) : DateTime.Parse(value);
    }

    /// <summary>
    /// Encrypts a datetime value to be saved in the database
    /// </summary>
    /// <param name="value">The value to encrypt and save in the database</param>
    /// <returns>The encrypted value</returns>
    public object ConvertToDatabase(object value)
    {
      var dateValue = (DateTime?)value;
      if (dateValue.IsNull())
        return dateValue;

      return EncryptionEnabled ? Encryptor.EncryptToString(dateValue.Value) : dateValue.Value.ToString(Constants.UnencyptedDateFormat);
    }
  }
}
