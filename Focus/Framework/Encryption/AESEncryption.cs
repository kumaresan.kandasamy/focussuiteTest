﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security.Cryptography;
using System.IO;
using System.Text;

using Framework.Lightspeed;

#endregion

namespace Framework.Encryption
{
  /*
   * Code take from http://msdn.microsoft.com/en-us/library/system.security.cryptography.rijndaelmanaged.generateiv(v=vs.110).aspx
   */

  /// <summary>
  /// Class to perform simple AES encryption
  /// </summary>
  public class AESEncryption
  {
    // Change these keys
    public byte[] Key { get; set; }
    public byte[] InitialisationVector { get; set; }

    private readonly ASCIIEncoding _encoder;

    #region Constructors

    /// <summary>
    /// Constructor using specified key
    /// </summary>
    /// <param name="key">The key</param>
    public AESEncryption(string key)
    {
      _encoder = new ASCIIEncoding();

      Key = _encoder.GetBytes(key);
    }

    /// <summary>
    /// Constructor using key and specified initialisation vector
    /// </summary>
    /// <param name="key">The specified initialisation vector</param>
    /// <param name="initialisationVector"></param>
    public AESEncryption(string key, string initialisationVector)
    {
      _encoder = new ASCIIEncoding();

      Key = _encoder.GetBytes(key);
      InitialisationVector = _encoder.GetBytes(initialisationVector);
    }

    #endregion

    #region Encryption

    /// <summary>
    /// Encrypts a value to a (Base64 encoded) string using the key and vector specfied for the class
    /// </summary>
    /// <param name="textValue">The value to encrypt</param>
    /// <returns>The encrypted value in Base64 format</returns>
    public string EncryptToString(string textValue)
    {
      return Convert.ToBase64String(Encrypt(textValue, InitialisationVector));
    }

    /// <summary>
    /// Encrypts a date value to a (Base64 encoded) string using the key and vector specfied for the class
    /// </summary>
    /// <param name="dateValue">The value to encrypt</param>
    /// <returns>The encrypted value in Base64 format</returns>
    public string EncryptToString(DateTime dateValue)
    {
      return Convert.ToBase64String(Encrypt(dateValue.ToString(Constants.UnencyptedDateFormat), InitialisationVector));
    }

    /// <summary>
    /// Encrypts a value to (Base64 encoded) string using the key specified for the class and a random initialisation vector
    /// The initalisation vector is pre-pended onto the encrypted value
    /// </summary>
    /// <param name="textValue">The value to encrypt</param>
    /// <returns>A concatenated string of the vector, plus the encrypted value in Base64 format</returns>
    /// <remarks>Initialisation vector will always be 16 bytes long</remarks>
    public string EncryptToStringWithGeneratedVector(string textValue)
    {
      var initialisationVector = Guid.NewGuid().ToString().Substring(0, 16);
      var vectorBytes = _encoder.GetBytes(initialisationVector);

      var encrypted = Convert.ToBase64String(Encrypt(textValue, vectorBytes));

      return string.Concat(initialisationVector, encrypted);
    }

    /// <summary>
    /// Encrypts a value to a byte array using the key and vector specfied for the class
    /// </summary>
    /// <param name="textValue">The value to encrypt</param>
    /// <returns>The encrypted value as a byte array</returns>
    public byte[] Encrypt(string textValue)
    {
      return Encrypt(textValue, InitialisationVector);
    }

    /// <summary>
    /// Encrypts a value to a byte array using the key and a specified initialisation vector
    /// </summary>
    /// <param name="textValue">The value to encrypt</param>
    /// <param name="initialisationVector">The initialisation vector</param>
    /// <returns>The encrypted value as a byte array</returns>
    public byte[] Encrypt(string textValue, byte[] initialisationVector)
    {
      byte[] encrypted;

      using (var rijAlg = new RijndaelManaged())
      {
        rijAlg.Key = Key;
        rijAlg.IV = initialisationVector;

        var encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

        using (var memoryStream = new MemoryStream())
        {
          using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
          {
            using (var streamWriter = new StreamWriter(cryptoStream))
            {
              streamWriter.Write(textValue);
            }
            encrypted = memoryStream.ToArray();
          }
        }
      }

      return encrypted;
    }

    #endregion

    #region Decryption

    /// <summary>
    /// Decrypts a value from a (Base64 encoded) string using the key and vector specfied for the class
    /// </summary>
    /// <param name="encryptedString">The value to decrypt in Base64 format</param>
    /// <returns>The decrypted value</returns>
    public string DecryptString(string encryptedString)
    {
      return Decrypt(Convert.FromBase64String(encryptedString), InitialisationVector);
    }

    /// <summary>
    /// Decrypts a value from a (Base64 encoded) string using the key specified for the class and a initialisation vector specified in the string
    /// </summary>
    /// <param name="encryptedString">The value to decrypt</param>
    /// <returns>The decrypted value</returns>
    /// <remarks>It is assumed the initialisation vector is the first 16 characters of the string</remarks>
    public string DecryptStringWithGeneratedVector(string encryptedString)
    {
      var initialisationVector = encryptedString.Substring(0, 16);
      var vectorBytes = _encoder.GetBytes(initialisationVector);

      var encrypted = Convert.FromBase64String(encryptedString.Substring(16));

      return Decrypt(encrypted, vectorBytes);
    }

    /// <summary>
    /// Decrypts a value from a byte array using the key and vector specfied for the class
    /// </summary>
    /// <param name="encryptedValue">The value to decrypt</param>
    /// <returns>The decrypted value</returns>
    public string Decrypt(byte[] encryptedValue)
    {
      return Decrypt(encryptedValue, InitialisationVector);
    }

    /// <summary>
    /// Encrypts a value from a byte array using the key and a specified initialisation vector
    /// </summary>
    /// <param name="encryptedValue">The value to decrypt</param>
    /// <param name="initialisationVector">The initialisation vector</param>
    /// <returns>The decrypted value as a byte array</returns>
    public string Decrypt(byte[] encryptedValue, byte[] initialisationVector)
    {
      string plaintext;

      using (var rijAlg = new RijndaelManaged())
      {
        rijAlg.Key = Key;
        rijAlg.IV = initialisationVector;

        var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);
 
        using (var memoryStream = new MemoryStream(encryptedValue))
        {
          using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
          {
            using (var streamReader = new StreamReader(cryptoStream))
            {
              plaintext = streamReader.ReadToEnd();
            }
          }
        }
      }

      return plaintext;
    }

    #endregion
  }
}