﻿namespace Framework.Authentication
{
	public interface IPasswordSettings
	{
		string PasswordRegExPattern { get; }
	}
}