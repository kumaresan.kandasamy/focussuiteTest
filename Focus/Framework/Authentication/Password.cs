﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Framework.Authentication
{
	public class Password
	{
		private readonly IPasswordSettings _passwordRegExPattern;

		/// <summary>
		/// Initializes a new instance of the <see cref="Password"/> class.
		/// </summary>
		/// <param name="passwordRegExPattern">The password reg ex pattern.</param>
		/// <param name="password">The password.</param>
		/// <param name="checkStrength">if set to <c>true</c> check the strength of the password.</param>
		public Password(IPasswordSettings passwordRegExPattern, string password, bool checkStrength = true)
		{
			_passwordRegExPattern = passwordRegExPattern;
			var newSalt = Guid.NewGuid().ToString("N").Replace("-", "").Substring(0, 8);
			Initialise(password, newSalt, checkStrength);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Password"/> class.
		/// </summary>
		/// <param name="passwordRegExPattern">The password reg ex pattern.</param>
		/// <param name="password">The password.</param>
		/// <param name="salt">The salt.</param>
		/// <param name="checkStrength">if set to <c>true</c> check the strength of the password.</param>
		public Password(IPasswordSettings passwordRegExPattern, string password, string salt, bool checkStrength = true)
		{
			_passwordRegExPattern = passwordRegExPattern;
			Initialise(password, salt, checkStrength);
		}

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The value.</value>
		public string Value { get; private set; }

		/// <summary>
		/// Gets or sets the salt.
		/// </summary>
		/// <value>The salt.</value>
		public string Salt { get; private set; }

		/// <summary>
		/// Gets or sets the hash.
		/// </summary>
		/// <value>The hash.</value>
		public string Hash { get; private set; }


		/// <summary>
		/// Generates the random password.
		/// </summary>
		/// <param name="passwordLength">Length of the password.</param>
		/// <returns></returns>
		public static Password GenerateRandomPassword(int passwordLength = 10)
		{
			var passwordCharacters = new List<string> {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
																									"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
																									"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"};

			var passwordBuilder = new StringBuilder("");
			var random = new Random();

			for(var i=0; i<passwordLength; i++)
			{
				var passwordCharacterIndex = random.Next(0, passwordCharacters.Count - 1);
				passwordBuilder.Append(passwordCharacters[passwordCharacterIndex]);
			}

			return new Password(null, passwordBuilder.ToString(), false);
		}

    public static String GetMD5Hash(String inPassword)
    {
      return BitConverter.ToString(MD5.Create().ComputeHash((new ASCIIEncoding()).GetBytes(inPassword))).Replace("-", "").ToLower();
    }

	  /// <summary>
		/// Initialises the specified password.
		/// </summary>
		/// <param name="password">The password.</param>
		/// <param name="salt">The salt.</param>
		/// <param name="checkStrength">if set to <c>true</c> [check strength].</param>
		private void Initialise(string password, string salt, bool checkStrength)
		{
			if (string.IsNullOrEmpty(password) || string.IsNullOrEmpty(salt)) throw new ArgumentNullException();

			Value = password;
			Salt = salt;
			Hash = GetHash();

			if (checkStrength && !Regex.IsMatch(Value, _passwordRegExPattern.PasswordRegExPattern)) throw new Exception("Invalid Password");
		}

		/// <summary>
		/// Gets the hash.
		/// </summary>
		/// <returns></returns>
		private string GetHash()
		{
			var buffer = Encoding.UTF8.GetBytes(Value + Salt);
			buffer = SHA512.Create().ComputeHash(buffer);

			return Convert.ToBase64String(buffer).Substring(0, 86); 
		}
	}
}