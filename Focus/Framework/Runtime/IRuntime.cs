﻿using System;

using Framework.ServiceLocation;

namespace Framework.Runtime
{
	/// <summary>
	/// Defines a runtime environment for a implemented applications.
	/// </summary>
	public interface IRuntime : IDisposable
	{
		/// <summary>
		/// Gets the service locator associated with the runtime.
		/// </summary>
		IServiceLocator ServiceLocator { get; }

		/// <summary>
		/// Starts the runtime environment.
		/// </summary>
		void Start();

		/// <summary>
		/// Shutdowns the runtime environment and release all the resouces held by the runtime environment.
		/// </summary>
		void Shutdown();

		/// <summary>
		/// Begins the request.
		/// </summary>
		void BeginRequest();

		/// <summary>
		/// Ends the request.
		/// </summary>
		void EndRequest();
	}
}
