﻿#region Copyright © 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace Framework.Core
{
	[Serializable]
	public class PagedList<T> : List<T>, IPagedList
	{
    /// <summary>
    /// Default constructor. Required to be present by serialiser (for WCF services)
    /// </summary>
    public PagedList() {}

		/// <summary>
		/// Initializes a new instance of the <see cref="PagedList&lt;T&gt;"/> class.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="pageSize">Size of the page.</param>
		public PagedList(IQueryable<T> source, int pageIndex, int pageSize) : this(source, source.Count(), pageIndex, pageSize)
		{
		}

    /// <summary>
    /// Initializes a new instance of the <see cref="PagedList&lt;T&gt;"/> class.
    /// </summary>
    /// <param name="source">The source.</param>
    /// <param name="recordCount">The record count.</param>
    /// <param name="pageIndex">Index of the page.</param>
    /// <param name="pageSize">Size of the page.</param>
    public PagedList(IQueryable<T> source, int recordCount, int pageIndex, int pageSize)
    {
      TotalCount = recordCount;
      TotalPages = recordCount / pageSize;

      if (recordCount % pageSize > 0)
        TotalPages++;

      PageSize = pageSize;
      PageIndex = pageIndex;

      AddRange(pageIndex == 0 ? source.Take(pageSize).ToList() : source.Skip(pageIndex * pageSize).Take(pageSize).ToList());
    }

		/// <summary>
		/// Initializes a new instance of the <see cref="PagedList&lt;T&gt;"/> class.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="totalRecords">The total records.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <param name="pageSource">if set to <c>true</c> [page source].</param>
		public PagedList(IEnumerable<T> source, int totalRecords, int pageIndex, int pageSize, bool pageSource = true)
		{
			TotalCount = totalRecords;
			TotalPages = totalRecords / pageSize;

			if (totalRecords % pageSize > 0)
				TotalPages++;

			PageSize = pageSize;
			PageIndex = pageIndex;

			if (pageSource)
				AddRange(pageIndex == 0 ? source.Take(pageSize).ToList() : source.Skip(pageIndex * pageSize).Take(pageSize).ToList());
			else
				AddRange(source.ToList());
		}
		
		public int TotalPages { get; set; }
		public int TotalCount { get; set; }
		public int PageIndex { get; set; }
		public int PageSize { get; set; }

		/// <summary>
		/// Gets a value indicating whether this instance has previous page.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance has previous page; otherwise, <c>false</c>.
		/// </value>
		public bool HasPreviousPage
		{
			get { return (PageIndex > 0); }
		}
		
		/// <summary>
		/// Gets a value indicating whether this instance has next page.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance has next page; otherwise, <c>false</c>.
		/// </value>
		public bool HasNextPage
		{
			get { return ((PageIndex + 1) * PageSize) <= TotalCount; }
		}
	}

	public interface IPagedList
	{
		int TotalCount { get; set; }
		int TotalPages { get; set; }
		int PageIndex { get; set; }
		int PageSize { get; set; }
		bool HasPreviousPage { get; }
		bool HasNextPage { get; }
	}
}
