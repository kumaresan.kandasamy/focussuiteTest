﻿using System;
using System.Threading;
using System.Web;

namespace Framework.Core
{
	public static class ObjectManager
	{
		/// <summary>
		/// Gets an object from context, either http if there is one or the current thread
		/// </summary>
		/// <param name="keyName">Name of the key.</param>
		/// <returns></returns>
		public static object GetObject(string keyName)
		{
			keyName = GetKeyName(keyName);

			if (HttpContext.Current != null)
				return HttpContext.Current.Items[keyName];

			return Thread.GetData(Thread.GetNamedDataSlot(keyName));
		}

		/// <summary>
		/// Sets an object in context, either http if there is one or the current thread
		/// </summary>
		/// <param name="keyName">Name of the key.</param>
		/// <param name="value">The value.</param>
		public static void SetObject(string keyName, object value)
		{
			keyName = GetKeyName(keyName);

			if (HttpContext.Current != null)
			{
				if (HttpContext.Current.Items.Contains(keyName))
					HttpContext.Current.Items[keyName] = value;
				else
					HttpContext.Current.Items.Add(keyName, value);

				return;
			}

			Thread.SetData(Thread.GetNamedDataSlot(keyName), value);
		}

		/// <summary>
		/// Clears an object from context, either http if there is one or the current thread
		/// </summary>
		/// <param name="keyName">Name of the key.</param>
		public static void ClearObject(string keyName)
		{
			keyName = GetKeyName(keyName);

			if (HttpContext.Current != null)
			{
				HttpContext.Current.Items.Remove(keyName);
				return;
			}

			Thread.FreeNamedDataSlot(keyName);
		}

		/// <summary>
		/// Gets the name of the key.
		/// </summary>
		/// <param name="keyName">Name of the key.</param>
		/// <returns></returns>
		private static string GetKeyName(string keyName)
		{
			// Are we serving a HTTP application, or Windows-forms application??

			if (HttpContext.Current != null)
			{
				// Dealing with an HTTP application, maybe the thread currently serving 
				// a ASP.Net request will be paused by the CLR to be reused for another 
				// request.... so bundle the keyName with something unique from the 
				// incoming request

				keyName = String.Concat(keyName, "_", HttpContext.Current.Timestamp.Ticks.ToString(), "_", Thread.CurrentThread.ManagedThreadId.ToString());
				return (keyName);
			}

			// no HTTP context, that's a Windows-forms application, we can safely think
			// that the thread will be unique to the application request

			keyName = String.Concat(keyName, "_", Thread.CurrentThread.ManagedThreadId.ToString());
			return (keyName);
		}
	}
}
