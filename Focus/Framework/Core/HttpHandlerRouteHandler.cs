﻿namespace System.Web.Routing
{
	public class HttpHandlerRoute<T> : IRouteHandler where T : IHttpHandler
	{
		private string _virtualPath;

		/// <summary>
		/// Initializes a new instance of the <see cref="HttpHandlerRoute&lt;T&gt;"/> class.
		/// </summary>
		/// <param name="virtualPath">The virtual path.</param>
		public HttpHandlerRoute(string virtualPath)
		{
			_virtualPath = virtualPath;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="HttpHandlerRoute&lt;T&gt;"/> class.
		/// </summary>
		public HttpHandlerRoute() { }

		/// <summary>
		/// Provides the object that processes the request.
		/// </summary>
		/// <param name="requestContext">An object that encapsulates information about the request.</param>
		/// <returns>An object that processes the request.</returns>
		public IHttpHandler GetHttpHandler(RequestContext requestContext)
		{
			return Activator.CreateInstance<T>();
		}
	}

	public class HttpHandlerRoute : IRouteHandler
	{
		private string _virtualPath;

		/// <summary>
		/// Initializes a new instance of the <see cref="HttpHandlerRoute"/> class.
		/// </summary>
		/// <param name="virtualPath">The virtual path.</param>
		public HttpHandlerRoute(string virtualPath)
		{
			_virtualPath = virtualPath;
		}

		/// <summary>
		/// Provides the object that processes the request.
		/// </summary>
		/// <param name="requestContext">An object that encapsulates information about the request.</param>
		/// <returns>An object that processes the request.</returns>
		public IHttpHandler GetHttpHandler(RequestContext requestContext)
		{
			if (!string.IsNullOrEmpty(_virtualPath))
				return (IHttpHandler)Compilation.BuildManager.CreateInstanceFromVirtualPath(_virtualPath, typeof(IHttpHandler));

			throw new InvalidOperationException("HttpHandlerRoute threw an error because the virtual path to the HttpHandler is null or empty.");
		}
	}

	public static class RoutingExtension
	{
		/// <summary>
		/// Maps the HTTP handler route.
		/// </summary>
		/// <param name="routes">The routes.</param>
		/// <param name="routeName">Name of the route.</param>
		/// <param name="routeUrl">The route URL.</param>
		/// <param name="physicalFile">The physical file.</param>
		/// <param name="defaults">The defaults.</param>
		/// <param name="constraints">The constraints.</param>
		public static void MapHttpHandlerRoute(this RouteCollection routes, string routeName, string routeUrl, string physicalFile, RouteValueDictionary defaults = null, RouteValueDictionary constraints = null)
		{
			var route = new Route(routeUrl, defaults, constraints, new HttpHandlerRoute(physicalFile));
			routes.Add(routeName, route);
		}

		/// <summary>
		/// Maps the HTTP handler route.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="routes">The routes.</param>
		/// <param name="routeName">Name of the route.</param>
		/// <param name="routeUrl">The route URL.</param>
		/// <param name="defaults">The defaults.</param>
		/// <param name="constraints">The constraints.</param>
		public static void MapHttpHandlerRoute<T>(this RouteCollection routes, string routeName, string routeUrl, RouteValueDictionary defaults = null, RouteValueDictionary constraints = null) where T : IHttpHandler
		{
			var route = new Route(routeUrl, defaults, constraints, new HttpHandlerRoute<T>());
			routes.Add(routeName, route);
		}
	}
}

