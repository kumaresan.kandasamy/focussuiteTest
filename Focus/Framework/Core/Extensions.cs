﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;

using System.IO;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

using Framework.Core.Attributes;

using JetBrains.Annotations;
using Newtonsoft.Json;

#endregion

namespace Framework.Core
{
	public static class Extensions
	{
		private static readonly object Locker = new object();
		/// <summary>
		/// Clones the object.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="obj">The obj.</param>
		/// <returns></returns>
		public static T CloneObject<T>(this T obj)
		{
			return obj.ObjectToByteArray().ByteArrayToObject<T>();
		}


		/// <summary>
		/// Objects to byte array.
		/// </summary>
		/// <param name="objectToSerialize">The object to serialize.</param>
		/// <returns></returns>
		public static byte[] ObjectToByteArray(this object objectToSerialize)
		{
			using (var fs = new MemoryStream())
			{
				var formatter = new BinaryFormatter();

				try
				{

					// Here's the core functionality! One Line!
					// To be thread-safe we lock the object
					lock (Locker)
					{
						formatter.Serialize(fs, objectToSerialize);
					}


					return fs.ToArray();
				}
				catch (SerializationException ex)
				{
					return null;
				}
				finally
				{
					fs.Close();
				}
			}
		}

		/// <summary>
		/// Bytes the array to object.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="objectToDeSerialize">The object to de serialize.</param>
		/// <returns></returns>
		private static T ByteArrayToObject<T>(this byte[] objectToDeSerialize)
		{
			using (var fs = new MemoryStream())
			{
				var formatter = new BinaryFormatter();

				try
				{
					fs.Write(objectToDeSerialize, 0, objectToDeSerialize.Length);
					fs.Seek(0, SeekOrigin.Begin);

					//To be thread-safe we lock the object
					object obj;


					lock (Locker)
					{
						obj = formatter.Deserialize(fs);
					}


					return (T)obj;
				}
				catch (SerializationException ex)
				{
					return default(T);
				}
				finally
				{
					fs.Close();
				}
			}
		}

		/// <summary>
		/// Convert the string to a date/time.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns></returns>
		public static DateTime? ToDate(this string valueToConvert)
		{
			DateTime result;
			var success = DateTime.TryParse(valueToConvert, out result);

			return (success) ? (DateTime?)result : null;
		}

		/// <summary>
		/// Converts the string to an int.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns></returns>
		public static int? ToInt(this string valueToConvert)
		{
			int result;
			var success = int.TryParse(valueToConvert, out result);	
			
			return (success) ? (int?)result : null;
		}

		/// <summary>
		/// Converts the string to an long.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns></returns>
		public static long? ToLong(this string valueToConvert)
		{
			long result;
			var success = long.TryParse(valueToConvert, out result);

			return (success) ? (long?)result : null;
		}

		/// <summary>
		/// Converts the string to a bool.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns></returns>
		public static bool? ToBool(this string valueToConvert)
		{
			bool result;
			var success = bool.TryParse(valueToConvert, out result);

			return (success) ? (bool?)result : null;
		}

		/// <summary>
		/// Converts the string to a float.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns></returns>
		public static float? ToFloat(this string valueToConvert)
		{
			float result;
			var success = float.TryParse(valueToConvert, out result);
			
			return (success) ? (float?)result : null;
		}

		/// <summary>
		/// Converts the string to a decimal.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns></returns>
		public static decimal? ToDecimal(this string valueToConvert)
		{
			decimal result;
			var success = decimal.TryParse(valueToConvert, out result);

			return (success) ? (decimal?)result : null;
		}

	  /// <summary>
	  /// Converts a string list to a single concatenated string
	  /// </summary>
	  /// <param name="list">The list to concatenated</param>
	  /// <param name="separator">The string to separate each item in the list</param>
	  /// <param name="ignoreEmptyElements">Whether to exclude empty elements in the list</param>
	  /// <returns>A concatenated string</returns>
	  public static string ToConcatenatedString(this List<string> list, string separator, bool ignoreEmptyElements = true)
    {
      return list.IsNull() ? string.Empty : string.Join(separator, list.Select(i => i.Trim()).Where(i => !ignoreEmptyElements || i.Length > 0).ToArray());
    }

    /// <summary>
    /// Indefinites the article.
    /// </summary>
    /// <param name="noun">The noun.</param>
    /// <returns></returns>
    public static string IndefiniteArticle(this string noun)
    {
      if (noun.IsNullOrEmpty()) return string.Empty;
      
      var lastChar = noun.Length == 1 ? noun : noun.Substring(noun.Length - 1);
      return lastChar.ToLower().IsIn("a", "e", "i", "o", "u") ? "an" : "a";
    }

    /// <summary>
    /// Withes the ownership apostrophe.
    /// </summary>
    /// <param name="noun">The noun.</param>
    /// <returns></returns>
    public static string WithOwnershipApostrophe(this string noun)
    {
      if (noun.IsNullOrEmpty()) return string.Empty;
      return noun.ToLower().EndsWith("s") ? noun + "'" : noun + "'s";
    }

	  /// <summary>
		/// Determines whether the string can convert to int.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns>
		/// 	<c>true</c> if the string can convert to int; otherwise, <c>false</c>.
		/// </returns>
		public static bool CanConvertToInt(this string valueToConvert)
		{
			if (valueToConvert.IsNullOrEmpty())
				return false;

			int result;
			return int.TryParse(valueToConvert.Trim(), out result);
		}

		/// <summary>
		/// Determines whether the string can convert to long.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns>
		/// 	<c>true</c> if the string can convert to long; otherwise, <c>false</c>.
		/// </returns>
		public static bool CanConvertToLong(this string valueToConvert)
		{
			if (valueToConvert.IsNullOrEmpty())
				return false;

			long result;
			return long.TryParse(valueToConvert.Trim(), out result);
		}

		/// <summary>
		/// Determines whether the string can convert to double.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns>
		/// 	<c>true</c> if the string can convert to double; otherwise, <c>false</c>.
		/// </returns>
		public static bool CanConvertToDouble(this string valueToConvert)
		{
			if (valueToConvert.IsNullOrEmpty())
				return false;

			double result;	
			return double.TryParse(valueToConvert.Trim(), out result);
		}

		/// <summary>
		/// Determines whether the string can convert to date.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns>
		/// 	<c>true</c> if the string can convert to date; otherwise, <c>false</c>.
		/// </returns>
		public static bool CanConvertToDate(this string valueToConvert)
		{
			if (valueToConvert.IsNullOrEmpty())
				return false;

			DateTime result;

			if (DateTime.TryParse(valueToConvert.Trim(), out result))
			{
				// 1st Jan 1753 is the minimum date value in SQL Server.
				// So we must be after that. Let's pick 1900.
				return result >= new DateTime(1900, 1, 1);
			}

			return false;
		}

		/// <summary>
		/// Determines whether the string can convert to boolean.
		/// </summary>
		/// <param name="valueToConvert">The value to convert.</param>
		/// <returns>
		/// 	<c>true</c> if the string can convert to boolean; otherwise, <c>false</c>.
		/// </returns>
		public static bool CanConvertToBoolean(this string valueToConvert)
		{
			if (valueToConvert.IsNullOrEmpty())
				return false;

			bool result;
			return bool.TryParse(valueToConvert.Trim(), out result);
		}

		/// <summary>
		/// Checks the int is betweens the min and max values.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <param name="min">The min.</param>
		/// <param name="max">The max.</param>
		/// <returns></returns>
		public static bool Between(this int number, int min, int max)
		{
			return number >= min && number <= max;
		}

		/// <summary>
		/// Checks the double is betweens the min and max values.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <param name="min">The min.</param>
		/// <param name="max">The max.</param>
		/// <returns></returns>
		public static bool Between(this double number, int min, int max)
		{
			return number >= min && number <= max;
		}

		/// <summary>
		/// Satisfieses the requirements that the string is a number.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <param name="condition">The condition.</param>
		/// <returns></returns>
		public static bool SatisfiesRequirements(this string number, Func<int, bool> condition)
		{
			if (number.IsNullOrEmpty())
				return false;

			int result;
			var success = int.TryParse(number.Trim(), out result);

			return success && condition(result);
		}

		/// <summary>
		/// Satisfieses the requirements that the string is a double.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <param name="condition">The condition.</param>
		/// <returns></returns>
		public static bool SatisfiesRequirementsAsDouble(this string number, Func<double, bool> condition)
		{
			if (number.IsNullOrEmpty())
				return false;

			double result;
			var success = double.TryParse(number.Trim(), out result);

			return success && condition(result);
		}

		/// <summary>
		/// Determines whether the specified string is null or empty.
		/// </summary>
		/// <param name="s">The s.</param>
		/// <returns>
		/// 	<c>true</c> if the string is null or empty; otherwise, <c>false</c>.
		/// </returns>
    [ContractAnnotation("null => true")]
    public static bool IsNullOrEmpty(this string s)
		{
			return string.IsNullOrEmpty(s);
		}

		/// <summary>
		/// Determines whether the specified string is null or whitespace.
		/// </summary>
		/// <param name="s">The string.</param>
		/// <returns>
		/// 	<c>true</c> if the string is null or whitespace; otherwise, <c>false</c>.
		/// </returns>
		[ContractAnnotation( "null => true" )]
		public static bool IsNullOrWhitespace( this string s )
		{
			return string.IsNullOrWhiteSpace( s );
		}

    /// <summary>
    /// Determines whether the specified guid is null or empty.
    /// </summary>
    /// <param name="guid">The Guid.</param>
    /// <returns>
    /// 	<c>true</c> if the guid is null or empty; otherwise, <c>false</c>.
    /// </returns>
    [ContractAnnotation("null => true")]
    public static bool IsNullOrEmpty(this Guid guid)
    {
      return guid.IsNull() || guid == Guid.Empty;
    }

		/// <summary>
		/// Determines whether the string is not null or empty.
		/// </summary>
		/// <param name="s">The s.</param>
		/// <returns>
		/// 	<c>true</c> if the string is not null or empty; otherwise, <c>false</c>.
		/// </returns>
    [ContractAnnotation("null => false")]
    public static bool IsNotNullOrEmpty(this string s)
		{
			return !string.IsNullOrEmpty(s);
		}

    /// <summary>
    /// Determines whether a Guid is not null or empty.
    /// </summary>
    /// <param name="guid">The Guid.</param>
    /// <returns>
    /// 	<c>true</c> if the Guid is not null or empty; otherwise, <c>false</c>.
    /// </returns>
    [ContractAnnotation("null => false")]
    public static bool IsNotNullOrEmpty(this Guid guid)
    {
      return guid.IsNotNull() && guid != Guid.Empty;
    }

		/// <summary>
		/// Determines whether the specified list is not null or empty.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list">The list.</param>
		/// <returns>
		/// 	<c>true</c> if the list is null or empty; otherwise, <c>false</c>.
		/// </returns>
    [ContractAnnotation("null => true")]
    public static bool IsNullOrEmpty<T>(this IEnumerable<T> list)
		{
			return list == null || !list.Any();
		}

		/// <summary>
		/// Determines whether the specified list is not null or empty.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list">The list.</param>
		/// <returns>
		/// 	<c>true</c> if the list is not null or empty; otherwise, <c>false</c>.
		/// </returns>
    [ContractAnnotation("null => false")]
    public static bool IsNotNullOrEmpty<T>(this IEnumerable<T> list)
		{
			return list != null && list.Any();
		}

		/// <summary>
		/// Determines whether the specified object is null.
		/// </summary>
		/// <param name="obj">The obj.</param>
		/// <returns>
		/// 	<c>true</c> if the specified object is null; otherwise, <c>false</c>.
		/// </returns>
    [ContractAnnotation("null => true")]
    public static bool IsNull(this object obj)
		{
			return obj == null;
		}

		/// <summary>
		/// Determines whether the object is not null.
		/// </summary>
		/// <param name="obj">The obj.</param>
		/// <returns>
		/// 	<c>true</c> if the specified object is not null; otherwise, <c>false</c>.
		/// </returns>
    [ContractAnnotation("null => false")]
    public static bool IsNotNull(this object obj)
		{
			return obj != null;
		}

    /// <summary>
    /// Determines whether the specified number is null or zero.
    /// </summary>
    /// <param name="number">The number.</param>
    /// <returns>
    /// 	<c>true</c> if the string is null or zero; otherwise, <c>false</c>.
    /// </returns>
    [ContractAnnotation("null => true")]
    public static bool IsNullOrZero(this int? number)
    {
      return number.GetValueOrDefault(0) == 0;
    }

    /// <summary>
    /// Determines whether the specified number is not null and not zero.
    /// </summary>
    /// <param name="number">The number.</param>
    /// <returns>
    /// 	<c>true</c> if the string is not null and not zero; otherwise, <c>false</c>.
    /// </returns>
    [ContractAnnotation("null => false")]
    public static bool IsNotNullOrZero(this int? number)
    {
      return number.GetValueOrDefault(0) != 0;
    }

		/// <summary>
		/// Determines whether the specified datetime is not null and not zero.
		/// </summary>
		/// <param name="dateTime">The number.</param>
		/// <returns>
		/// 	<c>true</c> if the string is not null and not zero; otherwise, <c>false</c>.
		/// </returns>
		[ContractAnnotation("null => false")]
		public static bool IsNotNullOrDefault(this DateTime? dateTime)
		{
			return dateTime != null && (DateTime)dateTime != DateTime.MinValue;
		}

    /// <summary>
    /// Determines whether the specified number is null or zero.
    /// </summary>
    /// <param name="number">The number.</param>
    /// <returns>
    /// 	<c>true</c> if the string is null or zero; otherwise, <c>false</c>.
    /// </returns>
    [ContractAnnotation("null => true")]
    public static bool IsNullOrZero(this long? number)
    {
      return number.GetValueOrDefault(0) == 0;
    }

    /// <summary>
    /// Determines whether the specified number is not null and not zero.
    /// </summary>
    /// <param name="number">The number.</param>
    /// <returns>
    /// 	<c>true</c> if the string is not null and not zero; otherwise, <c>false</c>.
    /// </returns>
    [ContractAnnotation("null => false")]
    public static bool IsNotNullOrZero(this long? number)
    {
      return number.GetValueOrDefault(0) != 0;
    }

    /// <summary>
    /// Determines whether the specified number is null or zero.
    /// </summary>
    /// <param name="number">The number.</param>
    /// <returns>
    /// 	<c>true</c> if the string is null or zero; otherwise, <c>false</c>.
    /// </returns>
    [ContractAnnotation("null => true")]
    public static bool IsNullOrZero(this decimal? number)
    {
      return number.GetValueOrDefault(0) == 0;
    }

    /// <summary>
    /// Determines whether the specified number is not null and not zero.
    /// </summary>
    /// <param name="number">The number.</param>
    /// <returns>
    /// 	<c>true</c> if the string is not null and not zero; otherwise, <c>false</c>.
    /// </returns>
    [ContractAnnotation("null => false")]
    public static bool IsNotNullOrZero(this decimal? number)
    {
      return number.GetValueOrDefault(0) != 0;
    }

    /// <summary>
    /// Determines whether the specified object is a db null.
    /// </summary>
    /// <param name="obj">The obj.</param>
    /// <returns>
    /// 	<c>true</c> if the specified object is a db null; otherwise, <c>false</c>.
    /// </returns>
    public static bool IsDBNull(this object obj)
    {
      return obj == DBNull.Value;
    }

    /// <summary>
    /// Returns null if a string is empty, otherwise keep the string the same
    /// </summary>
    /// <param name="text">The text to check</param>
    /// <returns>null if a string is empty, or the string itself</returns>
    public static string NullIfEmpty(this string text)
    {
      return text.IsNullOrEmpty() ? null : text;
    }

		/// <summary>
		/// Establishes the differences of 2 enumerables.
		/// </summary>
		/// <typeparam name="TSource">The type of the source.</typeparam>
		/// <param name="first">The first.</param>
		/// <param name="second">The second.</param>
		/// <param name="keySelector">The key selector.</param>
		/// <returns></returns>
		public static IEnumerable<TSource> DifferencesBy<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, Func<TSource, double> keySelector)
		{
			var i = -1;

			foreach (var element in first)
			{
				i++;

				if (second.Count() <= i)
				{
					yield return element;
					continue;
				}

				if (second.ElementAt(i).IsNull())
					yield return element;

				if (keySelector(element) == keySelector(second.ElementAt(i)))
					continue;

				yield return element;
			}
		}

		/// <summary>
		/// Returns an IEnumerable of distincts objects. 
		/// </summary>
		/// <typeparam name="TSource">The type of the source.</typeparam>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <param name="source">The source.</param>
		/// <param name="keySelector">The key selector.</param>
		/// <returns></returns>
		public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			var distinctKeys = new HashSet<TKey>();
			return source.Where(element => distinctKeys.Add(keySelector(element)));
		}

    /// <summary>
    /// Trims the specified source.
    /// </summary>
    /// <param name="source">The source.</param>
    /// <param name="numberOfCharacters">The number of characters.</param>
    /// <param name="addEllipsis">if set to <c>true</c> add ellipsis.</param>
    /// <returns></returns>
    public static string Trim(this string source, int numberOfCharacters, bool addEllipsis = true)
		{
			if (source.IsNullOrEmpty())
				return string.Empty;

			return (source.Length > numberOfCharacters) ? source.Substring(0, numberOfCharacters) + (addEllipsis?  "...": "") : source;
		}


		/// <summary>
		/// Equals ignoring case.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="comparison">The comparison.</param>
		/// <returns></returns>
		public static bool EqualsIgnoreCase(this string source, string comparison)
		{
			return source.Equals(comparison, StringComparison.OrdinalIgnoreCase);
		}

		/// <summary>
		/// Determines if a string is contained within
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="comparison">The comparison.</param>
		/// <param name="stringComparison">The string comparison.</param>
		/// <returns>
		/// 	<c>true</c> if [contains] [the specified source]; otherwise, <c>false</c>.
		/// </returns>
		public static bool Contains (this string source, string comparison, StringComparison stringComparison)
		{
			return source.IndexOf(comparison, stringComparison) >= 0;
		}

    /// <summary>
    /// Returns a portion of a string after a given substring
    /// </summary>
    /// <param name="mainString">The main string to search</param>
    /// <param name="substring">The substring to check</param>
    /// <param name="stringComparison">String comparison</param>
    /// <returns></returns>
    public static string SubstringAfter(this string mainString, string substring, StringComparison stringComparison = StringComparison.OrdinalIgnoreCase)
    {
      var index = mainString.IndexOf(substring, stringComparison);

      return index >= 0 ? mainString.Substring(index + substring.Length) : null;
    }

    /// <summary>
    /// Returns a portion of a string before a given substring
    /// </summary>
    /// <param name="mainString">The main string to search</param>
    /// <param name="substring">The substring to check</param>
    /// <param name="stringComparison">String comparison</param>
    /// <returns></returns>
    public static string SubstringBefore(this string mainString, string substring, StringComparison stringComparison = StringComparison.OrdinalIgnoreCase)
    {
      var index = mainString.IndexOf(substring, stringComparison);

      return index >= 0 ? mainString.Substring(0, index) : null;
    }

    /// <summary>
    /// Truncates a string to a given length
    /// </summary>
    /// <param name="originalString">The string to truncate if it exceeds the given length</param>
    /// <param name="maximumLength">The length to which to truncate</param>
    /// <returns>The truncated string. If the original string does not exceed the length, it is returned unchanged</returns>
		public static string TruncateString(this string originalString, int maximumLength, bool addEllipsis = false)
    {
      if (originalString.IsNull())
        return null;

	    if (originalString.Length > maximumLength)
	    {
		    return addEllipsis
			           ? string.Concat(originalString.Substring(0, maximumLength - 3), "...")
			           : originalString.Substring(0, maximumLength);
	    }

	    return originalString;
    }

    /// <summary>
    /// Trims any whitespace and squases multiple sequential whitespace characters into a single space
    /// </summary>
    /// <param name="inputString"></param>
    /// <returns></returns>
    public static string NormaliseSpace(this string inputString)
    {
      return Regex.Replace(inputString, @"\s+", " ").Trim();
    }

		/// <summary>
		/// Remove some rogue characters from text that can cause problems elsewhere
		/// </summary>
		/// <param name="inputString"></param>
		/// <returns></returns>
		public static string RemoveRogueCharacters(this string inputString)
		{
			if (inputString == null)
				return "";
            if (inputString != null)
            {
                inputString = Regex.Replace(inputString, "\\a", "");
                inputString = Regex.Replace(inputString, "\\v", "");
            }
			return Regex.Replace(inputString, @"[\x02\x03]", "");
		}

		/// <summary>
		/// Gets the last moment of month.
		/// </summary>
		/// <param name="dateTime">The date time.</param>
		/// <returns></returns>
		public static DateTime GetLastMomentOfMonth(this DateTime dateTime)
		{
			return new DateTime(dateTime.Year, dateTime.Month, 1).AddMonths(1).AddMilliseconds(-1d);
		}

		/// <summary>
		/// Gets the first moment of month.
		/// </summary>
		/// <param name="dateTime">The date time.</param>
		/// <returns></returns>
		public static DateTime GetFirstMomentOfMonth(this DateTime dateTime)
		{
			return new DateTime(dateTime.Year, dateTime.Month, 1);
		}

		/// <summary>
		/// Converts a bool to a javascript literal.
		/// </summary>
		/// <param name="value">if set to <c>true</c> [value].</param>
		/// <returns></returns>
		public static string ToJavascriptLiteral(this bool value)
		{
			return value.ToString().ToLower();
		}

		/// <summary>
		/// Add to a string to the end
		/// </summary>
		/// <param name="mainString">First String</param>
		/// <param name="addtostring">String to add to end</param>
		/// <returns></returns>
		public static string AddToEndOfString(this string mainString, string addtostring)
		{
			return string.Concat(mainString, addtostring);
		}

		/// <summary>
		/// Gets the week commencing date.
		/// </summary>
		/// <param name="startDate">The start date.</param>
		/// <returns></returns>
		public static DateTime GetWeekCommencingDate(this DateTime startDate)
		{
			var dayOfWeek = (int)startDate.DayOfWeek;
			const int startDay = (int)DayOfWeek.Sunday;
			var offSet = startDay - dayOfWeek;
			
			if (offSet < 0 && startDate == DateTime.MinValue)
				return startDate;
			
			return startDate.AddDays(offSet);
		}

		/// <summary>
		/// Gets the week end date.
		/// </summary>
		/// <param name="endDate">The end date.</param>
		/// <returns></returns>
		public static DateTime GetWeekEndDate(this DateTime endDate)
		{
			var dayOfWeek = (int)endDate.DayOfWeek;
			const int endDay = (int)DayOfWeek.Saturday;
			var offSet = endDay - dayOfWeek;
			
			if (offSet > 0 && endDate == DateTime.MinValue)
				return endDate;
			
			return endDate.AddDays(offSet);
		}

		/// <summary>
		/// Gets the week number.
		/// </summary>
		/// <param name="dtPassed">The dt passed.</param>
		/// <returns></returns>
		public static int GetWeekNumber(this DateTime dtPassed)
		{
			var ciCurr = CultureInfo.CurrentCulture;
			var weekNum = ciCurr.Calendar.GetWeekOfYear(dtPassed, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Saturday);
			
			return weekNum;
		}

		/// <summary>
		/// Adds the weeks to a date.
		/// </summary>
		/// <param name="dtPassed">The dt passed.</param>
		/// <param name="numberOfWeeks">The number of weeks.</param>
		/// <returns></returns>
		public static DateTime AddWeeks(this DateTime dtPassed, int numberOfWeeks)
		{
			return CultureInfo.CurrentCulture.Calendar.AddWeeks(dtPassed, numberOfWeeks);
		}

    /// <summary>
    /// Adds a number of working days (days excluding weekends) to a given date
    /// </summary>
    /// <param name="specificDate">The starting date</param>
    /// <param name="workingDaysToAdd">The number of working days to add</param>
    /// <returns>The adjusted date</returns>
    /// <remarks>
    /// See http://stackoverflow.com/questions/3717791/c-adding-working-days-from-a-cetain-date as a basic for this method
    /// </remarks>
    public static DateTime AddWorkingDays(this DateTime specificDate, int workingDaysToAdd)
    {
      if (workingDaysToAdd == 0)
        return specificDate;

      var direction = Math.Sign(workingDaysToAdd);

      // If the start date is a weekend, skip to the next business day to start with
      if (specificDate.DayOfWeek == DayOfWeek.Saturday)
        specificDate = specificDate.AddDays(direction == 1 ? 2 : -1);
      else if (specificDate.DayOfWeek == DayOfWeek.Sunday)
        specificDate = specificDate.AddDays(direction == 1 ? 1 : -2);

      var completeWeeks = Math.Abs(workingDaysToAdd) / 5;
      var date = specificDate.AddDays(direction * completeWeeks * 7);
      workingDaysToAdd = Math.Abs(workingDaysToAdd) % 5;

      // This loops only up to a maximum of 4 iterations
      for (var i = 0; i < workingDaysToAdd; i++)
      {
        date = date.AddDays(direction);
        while (!IsWeekDay(date))
        {
          date = date.AddDays(direction);
        }
      }
      return date;
    }

		/// <summary>
		/// Gets the business days between two dates.
		/// </summary>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <returns></returns>
		public static int GetBusinessDays(this DateTime startDate, DateTime endDate)
		{
			var calcBusinessDays = 1 + ((endDate - startDate).TotalDays * 5 - (startDate.DayOfWeek - endDate.DayOfWeek) * 2) / 7;

			if (endDate.DayOfWeek == DayOfWeek.Saturday) calcBusinessDays--;
			if (startDate.DayOfWeek == DayOfWeek.Sunday) calcBusinessDays--;

			return Convert.ToInt32(calcBusinessDays);
		}

    /// <summary>
    /// Checks whether a date is a week date
    /// </summary>
    /// <param name="date"></param>
    /// <returns></returns>
    public static bool IsWeekDay(this DateTime date)
    {
      return date.DayOfWeek.IsNotIn(DayOfWeek.Saturday, DayOfWeek.Sunday);
    }

		/// <summary>
		/// Gets the fiscal year.
		/// </summary>
		/// <param name="dateTime">The date time.</param>
		/// <returns></returns>
		public static int GetFiscalYear(this DateTime dateTime)
		{
			return dateTime.Month < 4 ? dateTime.Year - 1 : dateTime.Year;
		}

		/// <summary>
		/// Gets the fiscal year start.
		/// </summary>
		/// <param name="dtPassed">The dt passed.</param>
		/// <returns></returns>
		public static DateTime GetFiscalYearStart(this DateTime dtPassed)
		{
			return new DateTime(dtPassed.GetFiscalYear(), 4, 1);
		}

    /// <summary>
    /// Gets the age of a person
    /// </summary>
    /// <param name="dateOfBirth">The person's date of birth</param>
    /// <param name="currentDate">(Optional) The current date for which the age is required</param>
    /// <returns>The person's age</returns>
    public static short GetAge(this DateTime dateOfBirth, DateTime? currentDate = null)
    {
      if (currentDate.IsNull())
        currentDate = DateTime.Now;

      var age = currentDate.Value.Year - dateOfBirth.Year;
      if (currentDate < dateOfBirth.AddYears(age))
        age--;

      return (short)age;
    }

		/// <summary>
		/// Gets the julian days.
		/// </summary>
		/// <param name="dtPassed">The dt passed.</param>
		/// <returns></returns>
		public static long GetJulianDays(this DateTime dtPassed)
		{
			// This code originally comes from Focus Career and this is the original comment

			// Code logic: 1. Get the number of days till the provided year - 1 (365 days).
			//             2. Get the number of leap years in the provided year.
			//             3. Do some screw if the provided year is greater than Gregorain start year.
			//             4. Get the number of days till the providedmonth - 1.
			//             5. Add all the days count and screw the days as per the Gregorian start days.
			// Author: PIS/KNK

			var julianDays = 0;

			const int gregorianStartYear = 1582;
			const long gregorianStartDays = 577737;

			// first, regular days in the elapsed years
			// (-1 so we don't include this year)
			// (use 365 because we're going to handle leaps explicitly)
			var elapsedYears = dtPassed.Year - 1;	// elapsed years
			julianDays += 365 * elapsedYears;

			// add the leap years and remove the non leap years
			// (remember that before 1582 they didn't do 100/400 stuff)
			julianDays += elapsedYears / 4;

			if (elapsedYears > gregorianStartYear)
			{
				julianDays -= elapsedYears / 100;
				julianDays += elapsedYears / 400;
				julianDays += 12;	//100,200,300,500,600,700,900,1000,1100,1300,1400,1500
			}

			var elapsedMonths = dtPassed.Month - 1;

			var elapsedDays = 0; 
			for (int month = 0; month < elapsedMonths; month++)
			{
				elapsedDays += DateTime.DaysInMonth(dtPassed.Year, month + 1);
			}

			elapsedDays += dtPassed.Day;
			julianDays += elapsedDays;

			// okay, last little quirk ...
			// (Gregorian calendar jumped from October 4 - October 15,1582)
			if (julianDays > gregorianStartDays)
				julianDays -= 10;

			return julianDays;
		}

		/// <summary>
		/// Formats the date time with default.
		/// </summary>
		/// <param name="dateTime">The date time.</param>
		/// <param name="format">The format.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static string FormatDateTime(this DateTime? dateTime, string format, string defaultValue)
		{
			return (dateTime.HasValue ? String.Format(format, dateTime) : defaultValue);
		}

		/// <summary>
		/// Formats the date time.
		/// </summary>
		/// <param name="dateTime">The date time.</param>
		/// <param name="format">The format.</param>
		/// <returns></returns>
		public static string FormatDateTime(this DateTime dateTime, string format)
		{
			return String.Format(format, dateTime);
		}

		/// <summary>
		/// Generates a hash.
		/// </summary>
		/// <param name="password">The password to hash.</param>
		/// <returns></returns>
		public static string GeneratePasswordHash(this string password)
		{
			if (string.IsNullOrEmpty(password)) throw new ArgumentNullException();

			var buffer = Encoding.UTF8.GetBytes(password);
			buffer = SHA512Managed.Create().ComputeHash(buffer);
			
			return Convert.ToBase64String(buffer).Substring(0, 86); 
		}

		/// <summary>
		/// Serializes the specified object to json
		/// </summary>
		/// <param name="theObject"></param>
		/// <returns></returns>
		public static string SerializeJson (this object theObject, Formatting formatting = Formatting.None)
		{
			return JsonConvert.SerializeObject(theObject, formatting);
		}

		/// <summary>
		/// Deserializes the specified serialized data into an object
		/// </summary>
		/// <param name="serializedData"></param>
		/// <param name="theType"></param>
		/// <returns></returns>
		public static object DeserializeJson(this string serializedData, Type theType)
		{
			return JsonConvert.DeserializeObject(serializedData, theType);
		}

    /// <summary>
    /// Deserializes the specified serialized data into an object the specified tpye
    /// </summary>
    /// <param name="serializedData">The serialised date</param>
    /// <returns>The deserialised object</returns>
    public static T DeserializeJson<T>(this string serializedData)
    {
      return (T)JsonConvert.DeserializeObject(serializedData, typeof(T));
    }
		
		/// <summary>
		/// Captures the number at beginning of string.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="minNumberDigits">The min number digits.</param>
		/// <param name="maxNumberDigits">The max number digits.</param>
		/// <returns></returns>
		public static string CaptureNumberAtBeginningOfString(this string text, int minNumberDigits, int maxNumberDigits)
		{
			var match  = Regex.Match(text, string.Concat(@"^\d{", minNumberDigits, ",", maxNumberDigits, "}"));
			var retVal = "";

			if (match.IsNotNull() && match.Success) retVal = match.Value;

			return retVal;
		}

		/// <summary>
		/// HTML encodes a string.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <returns></returns>
		public static string HtmlEncode(this string text)
		{
			return HttpUtility.HtmlEncode(text);
		}

    /// <summary>
    /// XML encodes a string
    /// </summary>
    /// <param name="text">The text to encode.</param>
    /// <returns>The Xml encoded text</returns>
    public static string XmlEncode(this string text)
    {
      return new XText(text).ToString();
    }

		/// <summary>
		/// Sanitizes the XML string.
		/// </summary>
		/// <param name="xml">The XML.</param>
		/// <returns></returns>
		public static string SanitizeXmlString(this string xml)
		{
			if (xml == null) return "";

			var buffer = new StringBuilder(xml.Length);

			foreach(var character in xml)
				if (IsLegalXmlChar(character))
					buffer.Append(character);

			return buffer.ToString();
		}

		/// <summary>
		/// Remove some rogue entities from text that can cause problems elsewhere
		/// </summary>
		/// <param name="xml"></param>
		/// <returns></returns>
		public static string RemoveRogueXmlEntities(this string xml)
		{
			if (xml == null) 
				return "";

			return Regex.Replace(xml, @"&#x2;|&#x3;", "", RegexOptions.IgnoreCase);
		}

    /// <summary>
    /// Fix double-encoding (which can occur in migrated resumes)
    /// </summary>
    /// <param name="text">The text to check</param>
    /// <returns>The adjusted string</returns>
    public static string FixDoubleEncoding(this string text)
    {
      if (text.IsNullOrEmpty())
        return text;

      return text.Replace(@"&amp;", @"&");
    }

		/// <summary>
		/// Determines whether the specified character is a legal XML char.
		/// </summary>
		/// <param name="character">The character.</param>
		/// <returns>
		/// 	<c>true</c> if the specified character is a legal XML char; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsLegalXmlChar(int character)
		{
			return (character == 0x9 /* == '\t' == 9   */||
			        character == 0xA /* == '\n' == 10  */||
			        character == 0xD /* == '\r' == 13  */||
			        (character >= 0x20 && character <= 0xD7FF) ||
			        (character >= 0xE000 && character <= 0xFFFD) ||
			        (character >= 0x10000 && character <= 0x10FFFF)
			       );
		}

		/// <summary>
		/// Removes all non numeric characters from the text.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <returns></returns>
		public static string RemoveNonNumericCharacters(this string text)
		{
			var digitsOnly = new Regex(@"[^\d]");
			return digitsOnly.Replace(text, "");
		}

		#region Enum Extensions

		/// <summary>
		/// Gets the attribute value.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public static string GetAttributeValue<T>(this Enum value) where T : IEnumAttribute
		{
			// Get the type
			var type = value.GetType();

			// Get fieldinfo for this type
			var fieldInfo = type.GetField(value.ToString());

			// Get the value attributes
			var attribs = fieldInfo.GetCustomAttributes(typeof(T), false) as T[];

			// Return the first if there was a match.
			return attribs.IsNotNullOrEmpty() ? attribs[0].Value : "";
		}

		#endregion

		#region Validation Extensions

		public static void ShouldBeNull(this object actual, string message = null, params object[] args)
		{
			if (actual != null)
				throw new ArgumentException(!String.IsNullOrWhiteSpace(message) ? String.Format(message, args) : "Object should be null");
		}

		public static void ShouldNotBeNull(this object actual, string message = null, params object[] args)
		{
			if (actual == null)
				throw new ArgumentException(!String.IsNullOrWhiteSpace(message) ? String.Format(message, args) : "Object should not be null");
		}

		public static void ShouldBeNullOrEmpty(this string actual, string message = null, params object[] args)
		{
			if (!String.IsNullOrEmpty(actual))
				throw new ArgumentException(!String.IsNullOrWhiteSpace(message) ? String.Format(message, args) : "String should be null or empty");
		}

		public static void ShouldNotBeNullOrEmpty(this string actual, string message = null, params object[] args)
		{
			if (String.IsNullOrEmpty(actual))
				throw new ArgumentException(!String.IsNullOrWhiteSpace(message) ? String.Format(message, args) : "String should not be null or empty");
		}

		public static void ShouldBeTrue(this bool actual, string message = null, params object[] args)
		{
			if (!actual)
				throw new ArgumentException(!String.IsNullOrWhiteSpace(message) ? String.Format(message, args) : "Value should be true");
		}

		public static void ShouldBeFalse(this bool actual, string message = null, params object[] args)
		{
			if (!actual)
				throw new ArgumentException(!String.IsNullOrWhiteSpace(message) ? String.Format(message, args) : "Value should be false");
		}

		public static T ShouldBe<T>(this object actual, string message = null, params object[] args)
		{
			if (!(actual is T))
				throw new ArgumentException(!String.IsNullOrWhiteSpace(message) ? String.Format(message, args) : "Incorrent type");

			return (T)actual;
		}

    public static bool IsValidEmail(this string strIn)
    {
      if (strIn.IsNullOrEmpty())
        return false;

      return Regex.IsMatch(strIn, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
    }

		#endregion

    #region 'IsIn' extension

    /// <summary>
    /// Checks whether a value is in an array of values
    /// </summary>
    /// <typeparam name="T">The type being used</typeparam>
    /// <param name="value">The value being checked</param>
    /// <param name="args">The array to check</param>
    /// <returns>A boolean indicating if the array contains the value</returns>
    public static bool IsIn<T>(this T value, params T[] args)
    {
      return args.Contains(value);
    }

    /// <summary>
    /// Checks whether a value is in an list of values
    /// </summary>
    /// <typeparam name="T">The type being used</typeparam>
    /// <param name="value">The value being checked</param>
    /// <param name="list">The array to check</param>
    /// <returns>A boolean indicating if the array contains the value</returns>
    public static bool IsIn<T>(this T value, IEnumerable<T> list)
    {
      return list.Contains(value);
    }

    /// <summary>
    /// Checks whether a string value is in an array of values
    /// </summary>
    /// <param name="caseInsenstive">Whether to ignore the case in the check</param>
    /// <param name="value">The value being checked</param>
    /// <param name="args">The array to check</param>
    /// <returns>A boolean indicating if the array contains the value</returns>
    public static bool IsIn(this string value, bool caseInsenstive, params string[] args)
    {
      return caseInsenstive ? args.Contains(value, StringComparer.OrdinalIgnoreCase) : value.IsIn(args);
    }

    /// <summary>
    /// Checks whether a string value is in an array of values
    /// </summary>
    /// <param name="caseInsenstive">Whether to ignore the case in the check</param>
    /// <param name="value">The value being checked</param>
    /// <param name="list">The array to check</param>
    /// <returns>A boolean indicating if the array contains the value</returns>
    public static bool IsIn(this string value, bool caseInsenstive, IEnumerable<string> list)
    {
      return caseInsenstive ? list.Contains(value, StringComparer.OrdinalIgnoreCase) : value.IsIn(list);
    }

    /// <summary>
    /// Checks whether a value is in an array of values
    /// </summary>
    /// <typeparam name="T">The type being used</typeparam>
    /// <param name="value">The value being checked</param>
    /// <param name="args">The array to check</param>
    /// <returns>A boolean indicating if the array contains the value</returns>
    public static bool IsNotIn<T>(this T value, params T[] args)
    {
      return !args.Contains(value);
    }

    /// <summary>
    /// Checks whether a value is in an list of values
    /// </summary>
    /// <typeparam name="T">The type being used</typeparam>
    /// <param name="value">The value being checked</param>
    /// <param name="list">The array to check</param>
    /// <returns>A boolean indicating if the array contains the value</returns>
    public static bool IsNotIn<T>(this T value, IEnumerable<T> list)
    {
      return !list.Contains(value);
    }

    /// <summary>
    /// Checks whether a string value is in an array of values
    /// </summary>
    /// <param name="caseInsenstive">Whether to ignore the case in the check</param>
    /// <param name="value">The value being checked</param>
    /// <param name="args">The array to check</param>
    /// <returns>A boolean indicating if the array contains the value</returns>
    public static bool IsNotIn(this string value, bool caseInsenstive, params string[] args)
    {
      return caseInsenstive ? !args.Contains(value, StringComparer.OrdinalIgnoreCase) : value.IsNotIn(args);
    }

    /// <summary>
    /// Checks whether a string value is in an array of values
    /// </summary>
    /// <param name="caseInsenstive">Whether to ignore the case in the check</param>
    /// <param name="value">The value being checked</param>
    /// <param name="list">The array to check</param>
    /// <returns>A boolean indicating if the array contains the value</returns>
    public static bool IsNotIn(this string value, bool caseInsenstive, IEnumerable<string> list)
    {
      return caseInsenstive ? !list.Contains(value, StringComparer.OrdinalIgnoreCase) : value.IsNotIn(list);
    }
	  #endregion

    #region List extensions

    /// <summary>
    /// Adds multiple items to a list
    /// </summary>
    /// <typeparam name="T">The type of the list</typeparam>
    /// <param name="value">The generic list</param>
    /// <param name="args">The items to add</param>
    public static void AddItems<T>(this List<T> value, params T[] args)
    {
      value.AddRange(args);
    }

    /// <summary>
    /// Compares to see if two lists contain the same elements (in any order)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list">The first list</param>
    /// <param name="listToCompare">The list to which to compare</param>
    /// <param name="treatNullAsEmpty">Whether a null list should match a list with no entries</param>
    /// <returns>A boolean indicating whether the lists match</returns>
    public static bool EqualsList<T>(this List<T> list, List<T> listToCompare, bool treatNullAsEmpty = true) where T : struct
    {
      // If a list is null, and the other isn't, this counts as a mismatch
      if (!treatNullAsEmpty)
      {
        if (list.IsNull() && listToCompare.IsNotNull())
          return false;

        if (list.IsNotNull() && listToCompare.IsNull())
          return false;
      }

      // Lists must be of the same size
      var listCount = list.IsNull() ? 0 : list.Count;
      var listToCompareCount = listToCompare.IsNull() ? 0 : listToCompare.Count;

      if (listCount != listToCompareCount)
        return false;

      // Build a dictionary containing a look up of the items in the first list, and the number of times they occur
      var checkDict = new Dictionary<T, int>();

      if (list.IsNotNull())
      {
        foreach (var item in list)
        {
          if (checkDict.ContainsKey(item))
            checkDict[item] = checkDict[item] + 1;
          else
            checkDict.Add(item, 1);
        }
      }

      // Iterate through the second list, checking against the dictionary to ensure number of occurrences matches
      var match = true;
      if (listToCompare.IsNotNull())
      {
        for (var index = 0; index < listCount && match; index++)
        {
          var item = listToCompare[index];
          if (!checkDict.ContainsKey(item) || checkDict[item] == 0)
            match = false;
          else
            checkDict[item] -= 1;
        }
      }

      return match;
    }

    #endregion

    #region Sql Extensions

    public static string AsCommaListForSql(this List<string> values)
    {
      return string.Join(",", values.Select(value => string.Concat("'", value.EscapeSql(), "'")));
    }

    public static string AsCommaListForSql(this string[] values)
    {
      return string.Join(",", values.Select(value => string.Concat("'", value.EscapeSql(), "'")));
    }

    public static string AsCommaListForSql<T>(this List<T?> values) where T : struct
    {
      return string.Join(",", values.Select(value => Convert.ToInt32(value)));
    }

    public static string AsCommaListForSql<T>(this List<T> values)
    {
      return string.Join(",", values);
    }

    public static string AsOneOrZero(this bool value)
    {
      return value ? "1" : "0";
    }

    public static string EscapeSql(this string sql)
    {
      return sql.Replace("'", "''");
    }

    #endregion
  }
}
