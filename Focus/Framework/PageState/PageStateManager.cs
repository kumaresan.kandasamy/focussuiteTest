﻿using System.Configuration;
using System.Web.UI;

using Framework.PageState.Persisters;

namespace Framework.PageState
{
	public sealed class PageStateManager
	{		
		/// <summary>
		/// Initializes the <see cref="PageStateManager"/> class.
		/// </summary>
		static PageStateManager()
		{
			Persister = PageStatePersisters.Page;
			PageStateQueueMaxLength = 10;
			PageStateTimeOut = 2880;
			PageStateConnectionString = "";
		}

		/// <summary>
		/// Get the current viewstate provider being used.
		/// </summary>
		public static PageStatePersisters Persister { get; private set; } 

		public static int PageStateQueueMaxLength { get; private set; }
		public static int PageStateTimeOut { get; private set; }
		public static string PageStateConnectionString { get; private set; }

		/// <summary>
		/// Initialize the cache provider.
		/// </summary>
		/// <param name="persister">The persister.</param>
		/// <param name="pageStateQueueMaxLength">Length of the page state queue max.</param>
		/// <param name="pageStateTimeOut">The page state time out.</param>
		/// <param name="pageStateConnectionString">The page state connection string.</param>
		public static void Initialize(PageStatePersisters persister, int pageStateQueueMaxLength = 10, int pageStateTimeOut = 2880, string pageStateConnectionString = "")
		{
			Persister = persister;
			PageStateQueueMaxLength = pageStateQueueMaxLength;
			PageStateTimeOut = pageStateTimeOut;

			if (persister == PageStatePersisters.MsSql && string.IsNullOrEmpty(pageStateConnectionString))
				throw new ConfigurationErrorsException("Connection String 'PageStateConnectionString' not defined and is required when using Ms Sql page state provider.");

			if (!string.IsNullOrEmpty(pageStateConnectionString))
				PageStateConnectionString = ConfigurationManager.ConnectionStrings[pageStateConnectionString].ConnectionString;
		}

		/// <summary>
		/// News the page state persister.
		/// </summary>
		/// <param name="page">The page.</param>
		/// <returns></returns>
		public static PageStatePersister NewPageStatePersister(Page page)
		{
			switch (Persister)
			{		
				case PageStatePersisters.Session:
					return new SessionExPageStatePersister(page);

				case PageStatePersisters.MsSql:
					return new MsSqlPageStatePersister(page);

				default:
					return new HiddenFieldPageStatePersister(page);
			}			
		}
	}

	/// <summary>
	/// Enumeration of Page State Persistors 
	/// </summary>
	public enum PageStatePersisters
	{
		Page,
		Session,
		MsSql
	}
}
