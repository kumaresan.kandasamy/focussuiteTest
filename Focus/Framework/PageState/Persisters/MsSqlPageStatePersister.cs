﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace Framework.PageState.Persisters
{
	/// <summary>
	/// MsSql based Page State Persister that saves both ViewState and ControlState
	/// </summary>
	public class MsSqlPageStatePersister : PageStatePersister
	{
		private const string PageStateFieldName = "__PAGESTATEKEY";
	
		/// <summary>
		/// Initializes a new instance of the <see cref="SessionExPageStatePersister"/> class.
		/// </summary>
		/// <param name="page">The <see cref="T:System.Web.UI.Page"/> that the view state persistence mechanism is created for.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="page"/> parameter is null.</exception>
		public MsSqlPageStatePersister(Page page) : base(page)
		{ } 
 
		/// <summary>
		/// Overridden by derived classes to deserialize and load persisted state information when a <see cref="T:System.Web.UI.Page"/> object initializes its control hierarchy.
		/// </summary>
		public override void Load() 
		{ 
			// The cache key for this viewstate is stored in a hidden field, so grab it 
			var pageStateKey = GetPageStateKey();

			byte[] rawData = null;

			using (var conn = new SqlConnection(PageStateManager.PageStateConnectionString))
			using (var cmd = new SqlCommand("PageState_LoadPageState", conn))
			{
				cmd.CommandType = CommandType.StoredProcedure;

				// params
				cmd.Parameters.Add("@returnValue", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
				cmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = pageStateKey;

				// Get the PageState
				conn.Open();
				using (var reader = cmd.ExecuteReader())
				{
					if (reader.Read())
					{
						rawData = (byte[])Array.CreateInstance(typeof(byte), reader.GetInt32(0));

						if (reader.NextResult() && reader.Read())
							reader.GetBytes(0, 0, rawData, 0, rawData.Length);
					}
				}
			}

			//	Deserialize the view state raw data into object
			if (rawData != null && rawData.Length > 0)
			{
				using (var stream = new MemoryStream(rawData))
				{
					var los = new LosFormatter();

					var p = (Pair)los.Deserialize(stream);
					ViewState = p.First;
					ControlState = p.Second;
				}
			}
		}

		/// <summary>
		/// Overridden by derived classes to serialize persisted state information when a <see cref="T:System.Web.UI.Page"/> object is unloaded from memory.
		/// </summary>
		public override void Save()
		{
			// If Viewstate disabled do nothing
			if (!Page.EnableViewState) return;

			// if no state then do nothing
			if (ViewState == null && ControlState == null) return;
			
			using (var stream = new MemoryStream())
			{
				try
				{
					// Serialize the ViewState into String
					var los = new LosFormatter();
					los.Serialize(stream, new Pair(ViewState, ControlState));

					var pageStateKey = GetPageStateKey();

					using (var conn = new SqlConnection(PageStateManager.PageStateConnectionString))
					using (var cmd = new SqlCommand("PageState_SavePageState", conn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						// Params
						cmd.Parameters.Add("@returnValue", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
						cmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = pageStateKey;
						cmd.Parameters.Add("@value", SqlDbType.Image).Value = stream.ToArray();
						cmd.Parameters.Add("@timeout", SqlDbType.Int).Value = PageStateManager.PageStateTimeOut;

						conn.Open();
						cmd.ExecuteNonQuery();
					}

					var control = Page.FindControl(PageStateFieldName) as HtmlInputHidden; 
					
					if (control == null)
						Page.ClientScript.RegisterHiddenField(PageStateFieldName, pageStateKey.ToString());
					else
						control.Value = pageStateKey.ToString();
				}
				finally
				{
					stream.Close();
				}
			}
		}

		/// <summary>
		/// Gets the page state key.
		/// </summary>
		/// <returns></returns>
		private Guid GetPageStateKey()
		{
			var pageStateKey = Page.Request.Form[PageStateFieldName];

			if (string.IsNullOrEmpty(pageStateKey))
				return Guid.NewGuid();

			try { return new Guid(pageStateKey); } 
			catch (FormatException) { return Guid.NewGuid(); }
		}
	}
}
