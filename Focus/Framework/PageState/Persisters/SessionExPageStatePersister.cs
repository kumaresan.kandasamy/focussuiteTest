﻿using System;
using System.Collections.Generic;
using System.Web.UI;

namespace Framework.PageState.Persisters
{
	/// <summary>
	/// Session based Page State Persister that saves both ViewState and ControlState
	/// </summary>
	public class SessionExPageStatePersister : PageStatePersister
	{
		private const string ViewStateFieldName = "__VIEWSTATEKEY";
		private const string ViewStateKeyPrefix = "ViewState_";
		private const string RecentViewStateQueue = "ViewStateQueue";
		
		/// <summary>
		/// Initializes a new instance of the <see cref="SessionExPageStatePersister"/> class.
		/// </summary>
		/// <param name="page">The <see cref="T:System.Web.UI.Page"/> that the view state persistence mechanism is created for.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="page"/> parameter is null.</exception>
		public SessionExPageStatePersister(Page page) : base(page) 
		{ } 
 
		/// <summary>
		/// Overridden by derived classes to deserialize and load persisted state information when a <see cref="T:System.Web.UI.Page"/> object initializes its control hierarchy.
		/// </summary>
		public override void Load() 
		{ 
			// The cache key for this viewstate is stored in a hidden field, so grab it 
			var viewStateKey = Page.Request.Form[ViewStateFieldName] as string;

			// Grab the viewstate data using the key to look it up 
			if (viewStateKey != null)
			{
				var p = (Pair)Page.Session[viewStateKey];
				ViewState = p.First;
				ControlState = p.Second;
			}
		}

		/// <summary>
		/// Overridden by derived classes to serialize persisted state information when a <see cref="T:System.Web.UI.Page"/> object is unloaded from memory.
		/// </summary>
		public override void Save()
		{
			// If Viewstate disabled do nothing
			if (!Page.EnableViewState) return;

			// Give this viewstate a random key 
			var viewStateKey = ViewStateKeyPrefix + Guid.NewGuid();

			// Store the view and control state 
			Page.Session[viewStateKey] = new Pair(ViewState, ControlState);

			// Store the viewstate's key in a hidden field, so on postback we can grab it from the cache 
			Page.ClientScript.RegisterHiddenField(ViewStateFieldName, viewStateKey);

			// Some tidying up: keep track of the X most recent viewstates for this user, and remove old ones 
			var recent = Page.Session[RecentViewStateQueue] as Queue<string>;
			if (recent == null) Page.Session[RecentViewStateQueue] = recent = new Queue<string>();

			// Add this new one so it'll get removed later
			recent.Enqueue(viewStateKey);

			// If we've got lots in the queue, remove the old ones 
			while (recent.Count > PageStateManager.PageStateQueueMaxLength) 
				Page.Session.Remove(recent.Dequeue());
		}
	}
}
