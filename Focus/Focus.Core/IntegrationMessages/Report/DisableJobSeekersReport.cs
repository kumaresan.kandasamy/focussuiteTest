﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.Models.Integration;

#endregion

namespace Focus.Core.IntegrationMessages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class DisableJobSeekersReportRequest : IntegrationRequest
  {
    public DisableJobSeekersReportRequest() : base(IntegrationPoint.DisableJobSeekersReport) { }

    [DataMember]
    public List<DisableJobSeekersReportModel> JobSeekerIdList { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class DisableJobSeekersReportResponse : IntegrationResponse
  {
    public DisableJobSeekersReportResponse(DisableJobSeekersReportRequest request) : base(request) { }
  }
}
