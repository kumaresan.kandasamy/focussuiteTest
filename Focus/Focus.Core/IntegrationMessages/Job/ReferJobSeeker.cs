﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Focus.Core.Models.Integration;

namespace Focus.Core.IntegrationMessages
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ReferJobSeekerRequest : IntegrationRequest
	{
		public ReferJobSeekerRequest() : base(IntegrationPoint.ReferJobSeeker) { }

		[DataMember]
		public long JobId { get; set; }

		[DataMember]
		public long PostingId { get; set; }

		[DataMember]
		public long? ApplicationId { get; set; }

		[DataMember]
		public long? PersonId { get; set; }

		[DataMember]
		public string JobSeekerId { get; set; }

		[DataMember]
		public string ExternalJobId { get; set; }

		[DataMember]
		public string OfficeId { get; set; }

		[DataMember]
		public JobModel Job { get; set; }
	}


	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ReferJobSeekerResponse : IntegrationResponse
	{
		public ReferJobSeekerResponse(ReferJobSeekerRequest request) : base(request) { }

		[DataMember]
		public bool ReferralLimitExceeded { get; set; }

		[DataMember]
		public string ExternalJobId { get; set; }
	}

}
