﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.IntegrationMessages
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateJobStatusRequest : IntegrationRequest
	{
		public UpdateJobStatusRequest() : base(IntegrationPoint.UpdateJobStatus) { }

		[DataMember]
		public string JobId { get; set; }

		[DataMember]
		public JobStatuses JobStatus { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateJobStatusResponse : IntegrationResponse
	{
		public UpdateJobStatusResponse(UpdateJobStatusRequest request) : base(request) { }
	}
}
