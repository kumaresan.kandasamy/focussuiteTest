﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.IntegrationMessages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SetApplicationStatusRequest : IntegrationRequest
  {
    public SetApplicationStatusRequest() : base(IntegrationPoint.SetApplicationStatus) { }

    [DataMember]
    public long? ApplicationId { get; set; }

    [DataMember]
    public string JobSeekerId { get; set; }

    [DataMember]
    public string JobId { get; set; }

    [DataMember]
    public ApplicationStatusTypes ApplicationStatus { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SetApplicationStatusResponse : IntegrationResponse
  {
    public SetApplicationStatusResponse(SetApplicationStatusRequest request) : base(request) { }

    [DataMember]
    public bool ReferralLimitExceeded { get; set; }
  }
}
