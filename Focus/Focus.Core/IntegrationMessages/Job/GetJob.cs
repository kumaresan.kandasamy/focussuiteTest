﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

using Focus.Core.Models.Integration;

#endregion

namespace Focus.Core.IntegrationMessages
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetJobRequest : IntegrationRequest
	{
		public GetJobRequest() : base(IntegrationPoint.GetJob) { }

		[DataMember]
		public string JobId { get; set; }

		[DataMember]
		public string ExternalEmployerId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetJobResponse : IntegrationResponse
	{
		public GetJobResponse(GetJobRequest request) : base(request) { }

		[DataMember]
		public JobModel Job { get; set; }
	}
}
