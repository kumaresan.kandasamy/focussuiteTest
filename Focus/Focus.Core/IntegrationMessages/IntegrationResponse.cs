﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using Focus.Core;
using Framework.Exceptions;

#endregion

namespace Focus.Core.IntegrationMessages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public abstract class IntegrationResponse : IIntegrationResponse
  {
    protected IntegrationResponse(IIntegrationRequest request)
    {
      IntegrationBreadcrumbs = new List<RequestResponse>();
      Request = request;
      Begin = DateTime.Now;
    }

    [DataMember]
    public IntegrationOutcome Outcome { get; set; }

    [DataMember]
    public IIntegrationRequest Request { get; set; }

    [DataMember]
    public Exception Exception { get; set; }

    [DataMember]
    public List<RequestResponse> IntegrationBreadcrumbs { get; set; }

    [DataMember]
    public DateTime Begin { get; set; }

  }

  public class RequestResponse
  {
		/// <summary>
		/// Initializes a new instance of the <see cref="RequestResponse"/> class.
		/// </summary>
	  public RequestResponse()
	  {
		  StatusCode = HttpStatusCode.OK;
	  }

    [DataMember]
    public string Url { get; set; }

    [DataMember]
    public DateTime StartTime { get; set; }

    [DataMember]
    public DateTime EndTime { get; set; }

		[DataMember]
		public HttpStatusCode? StatusCode { get; set; }

		[DataMember]
		public string StatusDescription { get; set; }

    [DataMember]
    public string OutgoingRequest { get; set; }

    [DataMember]
    public string IncomingResponse { get; set; }
  }

  #region Interface

  public interface IIntegrationResponse
  {
    IntegrationOutcome Outcome { get; set; }
    Exception Exception { get; set; }
    List<RequestResponse> IntegrationBreadcrumbs { get; set; }
    DateTime Begin { get; set; }
  }

  #endregion

}
