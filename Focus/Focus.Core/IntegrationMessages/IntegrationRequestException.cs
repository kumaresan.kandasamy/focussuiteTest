﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

#endregion

namespace Focus.Core.IntegrationMessages
{
	public class IntegrationRequestException : Exception
	{
		public RequestResponse RequestResponse { get; set; }

		public IntegrationRequestException(string message) : base(message)
		{
			
		}

		public IntegrationRequestException(string message, RequestResponse response) : base(message)
		{
			RequestResponse = response;
		}
	}
}
