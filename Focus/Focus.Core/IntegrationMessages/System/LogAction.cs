﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.IntegrationMessages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class LogActionRequest : IntegrationRequest
  {
		public LogActionRequest() : base(IntegrationPoint.LogAction) { }

    [DataMember]
    public string ActionerExternalId { get; set; }

		[DataMember]
		public DateTime ActionedOn { get; set; }

		[DataMember]
		public ActionTypes ActionType { get; set; }

		[DataMember]
		public string ActionData { get; set; }

		[DataMember]
		public bool CheckForOneADay { get; set; }

		[DataMember]
		public long ActionerId { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class LogActionResponse : IntegrationResponse
  {
		public LogActionResponse(LogActionRequest request) : base(request) { }
  }
}
