﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Focus.Core.Models.Integration;

namespace Focus.Core.IntegrationMessages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class AuthenticateStaffRequest : IntegrationRequest
  {
    public AuthenticateStaffRequest() : base(IntegrationPoint.GetStaff) { }
    
    [DataMember]
    public string Username { get; set; }

    [DataMember]
    public string Password { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class AuthenticateStaffResponse : IntegrationResponse
  {
    public AuthenticateStaffResponse(AuthenticateStaffRequest request) : base(request) { }

    public StaffModel StaffUserInfo { get; set; }
  }
}
