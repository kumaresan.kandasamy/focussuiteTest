﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Focus.Core.IntegrationMessages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class AssignEmployerActivityRequest: IntegrationRequest
  {
    public AssignEmployerActivityRequest():base(IntegrationPoint.AssignEmployerActivity){}

    [DataMember]
    public string EmployerId { get; set; }

    [DataMember]
    public string EmployeeId { get; set; }

    [DataMember]
    public string AdminId { get; set; }

    [DataMember]
    public string OfficeId { get; set; }

    [DataMember]
    public string ActivityId { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class AssignEmployerActivityResponse : IntegrationResponse
  {
    public AssignEmployerActivityResponse(AssignEmployerActivityRequest request) : base(request) { }
    public string ActivityId { get; set; }
  }
}
