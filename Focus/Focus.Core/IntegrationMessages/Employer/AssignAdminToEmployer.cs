﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.IntegrationMessages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class AssignAdminToEmployerRequest : IntegrationRequest
  {
    public AssignAdminToEmployerRequest() : base(IntegrationPoint.AssignAdminToEmployer){}

    [DataMember]
    public string EmployerId { get; set; }

    [DataMember]
    public string AdminId { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class AssignAdminToEmployerResponse : IntegrationResponse
  {
    public AssignAdminToEmployerResponse(AssignAdminToEmployerRequest request) : base(request) { }
  }
}
