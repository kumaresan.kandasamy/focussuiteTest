﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Focus.Core.Models.Integration;

#endregion

namespace Focus.Core.IntegrationMessages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveEmployeeRequest : IntegrationRequest
  {
    public SaveEmployeeRequest():base(IntegrationPoint.SaveEmployee){}

		[DataMember]
    public long PersonId { get; set; }

		[DataMember]
    public EmployeeModel Employee { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveEmployeeResponse : IntegrationResponse
  {
    public SaveEmployeeResponse(SaveEmployeeRequest request) : base(request) { }
    public string ExternalEmployeeId { get; set; }
  }
}
