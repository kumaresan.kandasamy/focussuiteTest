﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.Models.Integration;

#endregion

namespace Focus.Core.IntegrationMessages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveEmployerRequest : IntegrationRequest
  {
    public SaveEmployerRequest():base(IntegrationPoint.SaveEmployer){}

    [DataMember]
    public long EmployerId { get; set; }

    [DataMember]
    public EmployerModel Employer { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveEmployerResponse : IntegrationResponse
  {
    public SaveEmployerResponse(SaveEmployerRequest request) : base(request) { }

    [DataMember]
    public string ExternalEmployerId { get; set; }

    [DataMember]
    public List<Tuple<long, string>>  Employees { get; set; }
  }
}
