﻿#region Copyright © 2000 - 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion


namespace Focus.Core.IntegrationMessages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class UpdateEmployeeStatusRequest : IntegrationRequest
  {
    public UpdateEmployeeStatusRequest() : base(IntegrationPoint.UpdateEmployeeStatus) { }

    [DataMember]
    public long StaffUserId { get; set; }

    [DataMember]
    public long EmployeeUserId { get; set; }

    [DataMember]
    public string ExternalStaffUserId { get; set; }

    [DataMember]
    public string ExternalEmployeeUserId { get; set; }

    [DataMember]
    public bool Blocked { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class UpdateEmployeeStatusResponse : IntegrationResponse
  {
    public UpdateEmployeeStatusResponse(UpdateEmployeeStatusRequest request) : base(request) { }
  }
}
