﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Focus.Core.Models.Integration;

namespace Focus.Core.IntegrationMessages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetEmployerRequest : IntegrationRequest
  {
    public GetEmployerRequest():base(IntegrationPoint.GetEmployer){}

    [DataMember]
    public string EmployerId { get; set; }

    [DataMember]
    public string FEIN { get; set; }

    [DataMember]
    public string CompanyName { get; set; }

    [DataMember]
    public string LegalName { get; set; }

    [DataMember]
    public bool ForImport { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetEmployerResponse : IntegrationResponse
  {
    public GetEmployerResponse(GetEmployerRequest request) : base(request) { }

    [DataMember]
    public EmployerModel Employer { get; set; }

    [DataMember]
    public List<Tuple<string, ApprovalStatuses>> JobsToImport { get; set; }

    [DataMember]
    public string Version { get; set; }
  }
}
