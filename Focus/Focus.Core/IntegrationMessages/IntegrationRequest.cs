﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Focus.Core.Models.Integration;

#endregion

namespace Focus.Core.IntegrationMessages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public abstract class IntegrationRequest : IIntegrationRequest
  {
    protected IntegrationRequest(IntegrationPoint integrationPoint)
    {
      RequestMade = DateTime.Now;
      IntegrationPoint = integrationPoint;
      RequestId = Guid.NewGuid();
    }

    [DataMember]
    public long UserId { get; set; }
    
    [DataMember]
    public DateTime RequestMade { get; set; }

    [DataMember]
    public IntegrationPoint IntegrationPoint { get; set; }

		
    [DataMember]
    public Guid? RequestId { get; set; }
    
    /// <summary>
		/// Gets or sets the admin identifier used when authenticating an AOSOS API call.
		/// </summary>
		/// <value>
		/// The admin identifier.
		/// </value>
		[DataMember]
		public string ExternalAdminId { get; set; }

		/// <summary>
		/// Gets or sets the office identifier used when authenticating an AOSOS API call.
		/// </summary>
		/// <value>
		/// The office identifier.
		/// </value>
		[DataMember]
		public string ExternalOfficeId { get; set; }

		/// <summary>
		/// Gets or sets the username used when authenticating an AOSOS API call.
		/// </summary>
		/// <value>
		/// The username.
		/// </value>
		[DataMember]
		public string ExternalUsername { get; set; }

		/// <summary>
		/// Gets or sets the password used when authenticating an AOSOS API call.
		/// </summary>
		/// <value>
		/// The password.
		/// </value>
		[DataMember]
		public string ExternalPassword { get; set; }
  }

  public interface IIntegrationRequest
  {
    long UserId { get; set; }
    DateTime RequestMade { get; set; }
    IntegrationPoint IntegrationPoint { get; set; }
    Guid? RequestId { get; set; }
/// <summary>
		/// Gets or sets the admin identifier used when authenticating an AOSOS API call.
		/// <summary>
		/// Gets or sets the admin identifier used when authenticating an AOSOS API call.
		/// </summary>
		/// <value>
		/// The admin identifier.
		/// </value>
		string ExternalAdminId { get; set; }

		/// <summary>
		/// Gets or sets the office identifier used when authenticating an AOSOS API call.
		/// </summary>
		/// <value>
		/// The office identifier.
		/// </value>
		string ExternalOfficeId { get; set; }

		/// <summary>
		/// Gets or sets the username used when authenticating an AOSOS API call.
		/// </summary>
		/// <value>
		/// The username.
		/// </value>
		string ExternalUsername { get; set; }

		/// <summary>
		/// Gets or sets the password used when authenticating an AOSOS API call.
		/// </summary>
		/// <value>
		/// The password.
		/// </value>
		string ExternalPassword { get; set; }
  }
}
