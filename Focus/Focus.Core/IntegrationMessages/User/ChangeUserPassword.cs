﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.IntegrationMessages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ChangeUserPasswordRequest : IntegrationRequest
  {
    public ChangeUserPasswordRequest():base(IntegrationPoint.ChangePassword){}
    [DataMember]
    public string UserName { get; set; }
    [DataMember]
    public string NewPassword { get; set; }
    [DataMember]
    public string OldPassword { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ChangeUserPasswordResponse : IntegrationResponse
  {
    public ChangeUserPasswordResponse(ChangeUserPasswordRequest request) : base(request) { }
  }
}
