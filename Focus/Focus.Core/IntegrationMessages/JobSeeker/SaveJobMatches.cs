﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Focus.Core.IntegrationMessages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveJobMatchesRequest : IntegrationRequest
  {
    public SaveJobMatchesRequest() : base(IntegrationPoint.SaveJobMatches) { }
    [DataMember]
    public long JobSeekerId { get; set; }
    [DataMember]
    public string ExternalJobSeekerId { get; set; }
    [DataMember]
    public List<string> Jobs { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveJobMatchesResponse : IntegrationResponse
  {
    public SaveJobMatchesResponse(SaveJobMatchesRequest request) : base(request) { }
  }

}
