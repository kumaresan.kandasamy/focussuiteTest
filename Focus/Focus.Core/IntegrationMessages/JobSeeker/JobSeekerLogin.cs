﻿#region Copyright © 2000 - 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Models.Career;
using Focus.Core.Models.Integration;

#endregion

namespace Focus.Core.IntegrationMessages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobSeekerLoginRequest : IntegrationRequest
  {
    public JobSeekerLoginRequest() : base(IntegrationPoint.SaveJobSeeker) { }

    [DataMember]
    public JobSeekerModel JobSeeker { get; set; }

    [DataMember]
    public long PersonId { get; set; }

    [DataMember]
    public bool Active { get; set; }

    [DataMember]
    public List<AssignJobSeekerActivityRequest> Activities { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobSeekerLoginResponse : IntegrationResponse
  {
    public JobSeekerLoginResponse(JobSeekerLoginRequest request) : base(request)
    {
      PersonId = request.PersonId;
    }

    #region WI required properties

    [DataMember]
    public long PersonId { get; set; }

    [DataMember]
    public bool UiClaimant { get; set; }

    [DataMember]
    public ResumeModel Resume { get; set; }

    #endregion
  }
}
