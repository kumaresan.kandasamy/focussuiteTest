﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Focus.Core.IntegrationMessages
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class AssignAdminToJobSeekerRequest : IntegrationRequest
	{
		public AssignAdminToJobSeekerRequest() : base(IntegrationPoint.AssignAdminToJobSeeker) { }

		[DataMember]
		public string JobSeekerId { get; set; }

		[DataMember]
		public string AdminId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class AssignAdminToJobSeekerResponse : IntegrationResponse
	{
		public AssignAdminToJobSeekerResponse(AssignAdminToJobSeekerRequest request) : base(request) { }
	}
}
