﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.Models.Integration;

#endregion

namespace Focus.Core.IntegrationMessages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class AuthenticateJobSeekerRequest : IntegrationRequest
  {
    public AuthenticateJobSeekerRequest():base(IntegrationPoint.AuthenticateJobSeeker){}

    [DataMember]
    public string UserName { get; set; }

    [DataMember]
    public string Password { get; set; }

    [DataMember]
    public Dictionary<string, string> AuthenticationDetails { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class AuthenticateJobSeekerResponse : IntegrationResponse
  {
    public AuthenticateJobSeekerResponse(AuthenticateJobSeekerRequest request) : base(request) { }
    [DataMember]
    public JobSeekerAuthenticationResult AuthenticationResult { get; set; }
    [DataMember]
    public JobSeekerModel JobSeeker { get; set; }
  }

}
