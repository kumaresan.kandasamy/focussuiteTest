﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.Models.Integration;

#endregion

namespace Focus.Core.IntegrationMessages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveJobSeekerRequest : IntegrationRequest
  {
    public SaveJobSeekerRequest() : base(IntegrationPoint.SaveJobSeeker) { }

    [DataMember]
    public long PersonId { get; set; }
    [DataMember]
    public JobSeekerModel JobSeeker { get; set; }

    [DataMember]
    public bool IncludeSelectiveService { get; set; }

		[DataMember]
		public List<AssignJobSeekerActivityRequest> Activities { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveJobSeekerResponse : IntegrationResponse
  {
    public SaveJobSeekerResponse(SaveJobSeekerRequest request) : base(request) { }
    public string ExternalJobSeekerId { get; set; }
		public bool IsNewJobSeeker { get; set; }
  }
}
