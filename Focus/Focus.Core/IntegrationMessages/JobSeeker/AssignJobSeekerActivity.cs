﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Focus.Core.DataTransferObjects.FocusCore;
using Framework.Core;

namespace Focus.Core.IntegrationMessages
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class AssignJobSeekerActivityRequest : IntegrationRequest
	{
		public AssignJobSeekerActivityRequest() : base(IntegrationPoint.AssignJobSeekerActivity) { }

		[DataMember]
		public long PersonId { get; set; }

		[DataMember]
		public string JobSeekerId { get; set; }

		[DataMember]
		public string AdminId { get; set; }

		[DataMember]
		public string ActivityId { get; set; }

		[DataMember]
		public string OfficeId { get; set; }

		[DataMember]
		public ActionTypes ActionType { get; set; }

		[DataMember]
		public string ActionTypeName { get; set; }

		[DataMember]
		public bool SelfServicedActivity { get; set; }

    [DataMember]
    public long ActivityUserId { get; set; }

		[DataMember]
		public long ActionerId { get; set; }

		[DataMember]
    public DateTime? LastActivityDate { get; set; }

		[DataMember]
		public bool IsSelfAssigned { get; set; }

		[DataMember]
		public bool IsManual { get; set; }

		/// <summary>
		/// This was added because ActionType gets serialised to a number, which may caused problems if an ActionType is added not at the end of the list
		/// Because of this is is better to user ActionTypeName. However, ActionType is still present for backward compatibility
		/// </summary>
		[IgnoreDataMember]
		public ActionTypes ActivityActionType
		{
			get
			{
				return ActionTypeName.IsNotNullOrEmpty() 
							   ? (ActionTypes)Enum.Parse(typeof (ActionTypes), ActionTypeName, true) 
								 : ActionType;
			}
		}
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class AssignJobSeekerActivityResponse : IntegrationResponse
	{
		public AssignJobSeekerActivityResponse(AssignJobSeekerActivityRequest request) : base(request) { }

		[DataMember]
		public string ActivityId { get; set; }

    [DataMember]
    public bool WasRestricted { get; set; }
	}
}
