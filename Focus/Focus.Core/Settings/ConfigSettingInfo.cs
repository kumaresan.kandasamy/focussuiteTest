﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives



#endregion

namespace Focus.Core.Settings
{
	public class ConfigSettingInfo
	{
		public ConfigSettingInfo(string name, ConfigSettingType type, ConfigSettingCategory category, string subCategory, string key, string displayName = null, string description = null, int displayOrder = 0, object defaultValue = null, object currentValue = null)
		{
			Name = name;
			Type = type;
			Category = category;
			SubCategory = subCategory;
			Key = key ?? name;
			DisplayName = displayName;
			Description = description ?? displayName;
			DisplayOrder = displayOrder;
			DefaultValue = defaultValue;
			CurrentValue = currentValue;
		}

		public string Name { get; private set; }
		public ConfigSettingType Type { get; private set; }
		public ConfigSettingCategory Category { get; set; }
		public string SubCategory { get; private set; }
		public string Key { get; set; }
		public string DisplayName { get; private set; }
		public string Description { get; set; }
		public int DisplayOrder { get; private set; }
		public object DefaultValue { get; set; }
		public object CurrentValue { get; set; }
	}
}
