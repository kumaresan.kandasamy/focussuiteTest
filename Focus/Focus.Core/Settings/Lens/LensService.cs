﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Framework.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

#endregion



namespace Focus.Core.Settings.Lens
{
	[DataContract(Name = "lensService", Namespace = "")]
	public class LensService
	{
		[DataMember(Name = "host", Order = 1, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public string Host { get; set; }

		[DataMember(Name = "portNumber", Order = 2, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public int PortNumber { get; set; }

		[DataMember(Name = "timeout", Order = 3, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public int Timeout { get; set; }

		[DataMember(Name = "vendor", Order = 4, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public string Vendor { get; set; }

		[DataMember(Name = "characterSet", Order = 5, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public string CharacterSet { get; set; }

		[DataMember(Name = "serviceType", Order = 6, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		[JsonConverter(typeof(StringEnumConverter))]
		public LensServiceTypes ServiceType { get; set; }

		[DataMember(Name = "description", Order = 7, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public string Description { get; set; }

		[DataMember(Name = "readonly", Order = 8, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public bool ReadOnly { get; set; }

    [DataMember(Name = "customFilters", Order = 9, EmitDefaultValue = false)]
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
    public CustomFilterList CustomFilters { get; set; }

    [DataMember(Name = "isspideredjobslens", Order = 10, EmitDefaultValue = false)]
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
    public bool? IsSpideredJobsLens { get; set; }

    [DataMember(Name = "maximumdayssearchableon", Order = 11, EmitDefaultValue = false)]
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
    public int? MaximumDaysSearchableOn { get; set; }

    [DataMember(Name = "newjobspostingdelaydays", Order = 12, EmitDefaultValue = false)]
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
    public int? NewJobsPostingDelayDays { get; set; }

		[DataMember(Name = "searchAllVendors", Order = 13, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public bool SearchAllVendors { get; set; }

		[DataMember(Name = "url", Order = 14, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public string Url { get; set; }

		[DataMember(Name = "consumerKey", Order = 15, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public string ConsumerKey { get; set; }

    [DataMember(Name = "anonymousaccess", Order = 16, EmitDefaultValue = false)]
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
    public bool AnonymousAccess { get; set; }
    
		public string VeteranStatusCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.VeteranStatus); } 
		}

		public string WorkOvertimeCustomFilterTag
 		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.WorkOvertime); } 
		}

		public string ShiftTypeCustomFilterTag
 		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.ShiftType); } 
		}

		public string WorkTypeCustomFilterTag 
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.WorkType); } 
		}

		public string WorkWeekCustomFilterTag 
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.WorkWeek); } 
		}

		public string DrivingLicenceClassCustomFilterTag 
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.DrivingLicenceClass); } 
		}

		public string DrivingLicenceEndorsementCustomFilterTag 
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.DrivingLicenceEndorsement); } 
		}

		public string JobseekerIdCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.JobseekerId); }
		}

		public string MinimumSalaryCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.MinimumSalary); }
		}

		public string MaximumSalaryCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.MaximumSalary); }
		}

		public string OccupationCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.Occupation); }
		}

		public string EducationLevelCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.EducationLevel); }
		}

		public string IndustryCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.Industry); }
		}

		public string SectorCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.Sector); }
		}

		public string PostingDateCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.OpenDate); }
		}

		public string ClosingOnCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.ClosingOn); }
		}

		public string StatusCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.Status); }
		}

		public string DateOfBirthCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.DateOfBirth); }
		}

		public string CitizenFlagCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.CitizenFlag); }
		}

		public string RegisteredOnCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.RegisteredOn); }
		}

		public string ModifiedOnCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.ModifiedOn); }
		}

		public string LanguagesCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.Languages); }
		}

		public string CertificatesCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.Certificates); }
		}

		public string RelocationCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.Relocation); }
		}

		public string StateCodeCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.StateCode); }
		}

		public string CountyCodeCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.CountyCode); }
		}

		public string MsaCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.Msa); }
		}

		public string OpenDateCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.OpenDate); }
		}

		public string ExternalPostingIdCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.ExternalPostingId); }
		}

		public string PostingIdCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.PostingId); }
		}

		public string OriginCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.Origin); }
		}

		public string Sic2CustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.Sic2); }
		}

		public string GreenJobCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.GreenJob); }
		}

		public string InternshipCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.Internship); }
		}

		public string SalaryCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.Salary); }
		}

		public string JobTypesCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.JobTypes); }
		}

		public string IncludeUnpaidCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.UnpaidPosition); }
		}

		public string GPACustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.GPA); }
		}

		public string MonthsExperienceCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.MonthsExperience); }
		}

		public string EnrollmentStatusCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.EnrollmentStatus); }
		}

		public string ProgramOfStudyCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.ProgramsOfStudy); }
		}

    public string ROnetCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.ROnet); }
		}

		public string InternshipCategoriesFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.InternshipCategories); }
		}

		public string RequiredProgramOfStudyIdFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.RequiredProgramOfStudyId); }
		}

		public string DegreeCompletionDateFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.DegreeCompletionDate); }
		}

		public string ResumeIndustriesCustomFilterTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.ResumeIndustries); }
		}

    public string HomeBasedTag
    {
      get { return GetCustomFilterTag(Constants.CustomFilterKeys.HomeBasedTag); }
    }

		public string CommissionBasedTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.CommissionBasedTag); }
		}

		public string SalaryAndCommissionBasedTag
		{
			get { return GetCustomFilterTag(Constants.CustomFilterKeys.SalaryAndCommissionBasedTag); }
		}

    public string NCRCLevelTag
    {
      get { return GetCustomFilterTag(Constants.CustomFilterKeys.NCRCLevel); }
    }

		private string GetCustomFilterTag(string customFilterName)
		{
			return CustomFilters.IsNullOrEmpty() ? null : CustomFilters.Where(x => x.Key == customFilterName).Select(x => x.Tag).FirstOrDefault();
		}	
	}

	[DataContract(Name = "customFilter", Namespace = "")]
	public class CustomFilter
	{
		[DataMember(Name = "key", Order = 1, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public string Key { get; set; }

		[DataMember(Name = "tag", Order = 2, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public string Tag { get; set; }
	}

	[CollectionDataContract(Name = "customFilters", ItemName = "customFilter", Namespace = "")]
	public class CustomFilterList : List<CustomFilter>
	{ }
}
