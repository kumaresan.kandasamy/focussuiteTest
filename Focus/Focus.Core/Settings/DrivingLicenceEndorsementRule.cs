﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

#endregion

namespace Focus.Core.Settings
{
	public class DrivingLicenceEndorsementRule
	{
		[DataMember(Name = "licenceKey", Order = 1, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public string LicenceKey { get; set; }

		[DataMember(Name = "endorsementKeys", Order = 2, EmitDefaultValue = false)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
		public EndorsementKeyList EndorsementKeys { get; set; }
	}

	[CollectionDataContract(Name = "drivingLicenceEndorsementRules", ItemName = "drivingLicenceEndorsementRule", Namespace = "")]
	public class DrivingLicenceEndorsementRuleList : List<DrivingLicenceEndorsementRule>
	{
	}

	[CollectionDataContract(Name = "endorsementKeys", ItemName = "endorsementKey", Namespace = "")]
	public class EndorsementKeyList : List<string>
	{
	}
}
