﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core.Settings.Lens;
using Focus.Core.Settings.OAuth;
using Framework.Authentication;
using Framework.Logging;
using Framework.PageState;

#endregion


namespace Focus.Core.Settings.Interfaces
{
    public interface IAppSettings : IPasswordSettings
    {
        //TO DO ------------------------------------------------------------------------------------------------------------------
        List<BatchProcessSetting> BatchProcessSettings { get; }

        //TO DO list ------------------------------------------------------------------------------------------------------------------
        List<LensService> LensServices { get; }

        DrivingLicenceEndorsementRuleList DrivingLicenceEndorsementRules { get; }

        // MessageBusSettings
        string MessageBusUsername { get; } //not in db - ukdev@aperture-control one from web.config

        // this needs looking at as the default is actually returned by a method which combines the nearby states with the default state.
        string[] SearchableStateKeys { get; }

        #region GeneralSettings

        #region Application Settings

        [WebConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "Focus-Application", DisplayName = "Application", Description = "Application", DisplayOrder = 2)]
        string Application { get; }

        [WebConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "Focus-ClientTag", DisplayName = "Client tag", Description = "The client tag", DisplayOrder = 3)]
        string ClientTag { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "AppVersion", DisplayName = "Application version", Default = "Version ??.??", Description = "The current version of the application", DisplayOrder = 4)]
        string AppVersion { get; }

        [WebConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", DisplayName = "Module", Description = "The module (Talent/Assist/etc)", DisplayOrder = 5)]
        FocusModules Module { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "Theme", DisplayName = "Theme", Description = "The theme (Workforce/Education)", DisplayOrder = 6)]
        FocusThemes Theme { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "CareerExplorerModule", DisplayName = "Career Explorer module", Default = FocusModules.CareerExplorer, DisplayOrder = 7)]
        FocusModules CareerExplorerModule { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "CachePrefix", DisplayName = "Cache prefix", DisplayOrder = 8)]
        string CachePrefix { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "ValidClientTags", DisplayName = "Valid client tags", Default = new string[0], DisplayOrder = 9)]
        string[] ValidClientTags { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "LogSeverity", DisplayName = "Log severity", DisplayOrder = 10)]
        LogSeverity LogSeverity { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "DistanceUnits", DisplayName = "Distance units", Default = DistanceUnits.Miles, DisplayOrder = 12)]
        DistanceUnits DistanceUnits { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "NewEmployerApproval", DisplayName = "New employer approval", Default = TalentApprovalOptions.ApprovalSystemAvailable, DisplayOrder = 13)]
        TalentApprovalOptions NewEmployerApproval { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "RegistrationCompleteAction", DisplayName = "Registration complete action", Default = RegistrationCompleteAction.EmailLink, DisplayOrder = 14)]
        RegistrationCompleteAction RegistrationCompleteAction { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "RegistrationRoute", DisplayName = "Registration route", Default = RegistrationRoute.Default, DisplayOrder = 15)]
        RegistrationRoute RegistrationRoute { get; }

        // Should this SMTP server be kept? only used in unit tests...
        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "SmtpServer", DisplayName = "SMTP server", Default = "NOTSET", DisplayOrder = 16)]
        string SmtpServer { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "OnErrorEmail", DisplayName = "On error email", DisplayOrder = 17)]
        string OnErrorEmail { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "FeedbackEmailAddress", DisplayName = "Feedback email address", Default = "focus-dev@burning-glass.com", DisplayOrder = 18)]
        string FeedbackEmailAddress { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "JobPostingStylesheet", DisplayName = "Job posting stylesheet", Default = "DefaultPosting.xslt", DisplayOrder = 19)]
        string JobPostingStylesheet { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "FullResumeStylesheet", DisplayName = "Full resume stylesheet", Default = "DefaultFullResume.xslt", DisplayOrder = 20)]
        string FullResumeStylesheet { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "PartialResumeStylesheet", DisplayName = "Partial resume stylesheet", Default = "DefaultPartialResume.xslt", DisplayOrder = 21)]
        string PartialResumeStylesheet { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "DefaultDrivingLicenseType", DisplayName = "Default driving license type", Default = "DrivingLicenceClasses.ClassD", DisplayOrder = 22)]
        string DefaultDrivingLicenseType { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "DefaultSearchRadiusKey", DisplayName = "Default search radius key", Default = "Radius.TwentyFiveMiles", DisplayOrder = 23)]
        string DefaultSearchRadiusKey { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "DefaultStateKey", DisplayName = "Default state key", Default = "State.MA", DisplayOrder = 24)]
        string DefaultStateKey { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = Constants.ConfigurationItemKeys.DummyUsernameFormat, DisplayName = "Dummy username format", Default = "{0}", DisplayOrder = 25)]
        string DummyUsernameFormat { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = Constants.ConfigurationItemKeys.DummyEmailFormat, DisplayName = "Dummy email format", Default = "{0}@ci.bgt.com", DisplayOrder = 25)]
        string DummyEmailFormat { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "LogoutRedirectUrl", DisplayName = "Logout redirect URL", Default = null, DisplayOrder = 26)]
        string LogoutRedirectUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "TalentModulePresent", DisplayName = "Talent module present in the instance", Description = "This decides whether Talent related features are displayed within Career and Assist", Default = true, DisplayOrder = 27)]
        bool TalentModulePresent { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "DisplayServerName", DisplayName = "Display server name", Description = "Whether to display the server name in the footer", Default = false, DisplayOrder = 28)]
        bool DisplayServerName { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "UseSqlSessionState", DisplayName = "Use SQL session state handler", Description = "This determines whether to use SQL Server to store the session data, rather than in-process", Default = false, DisplayOrder = 29)]
        bool UseSqlSessionState { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "BrandIdentifier", DisplayName = "The client brand identifier", Description = "Specifies a unique string that identifies the client brand to apply to the application", Default = "Default", DisplayOrder = 30)]
        string BrandIdentifier { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "PagerDropdownLimit", DisplayName = "Limit of number of pages for pager drop-down", Description = "This determines the maximum number of pages supported by the page number drop-down on pager controls before switching to a text box", Default = 9999, DisplayOrder = 31)]
        int PagerDropdownLimit { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "ClientApplyToPostingUrl", DisplayName = "Client apply to posting URL", DisplayOrder = 32)]
        string ClientApplyToPostingUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = "HideDrivingLicence", DisplayName = "Hide driving licence", Default = false, DisplayOrder = 33)]
        bool HideDrivingLicence { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = "HidePersonalInformation", DisplayName = "Hide personal information", Default = false, DisplayOrder = 34)]
        bool HidePersonalInformation { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = "HideReferences", DisplayName = "Hide references", Default = false, DisplayOrder = 35)]
        bool HideReferences { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = "HideEnrollmentStatus", DisplayName = "Hide enrollment status", Default = false, DisplayOrder = 36)]
        bool HideEnrollmentStatus { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = "ShowBrandingStatement", DisplayName = "Show branding statement", Default = false, DisplayOrder = 37)]
        bool ShowBrandingStatement { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = "JobTitleRequired", DisplayName = "Job title required", Default = false, DisplayOrder = 38)]
        bool JobTitleRequired { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = "HideSeasonalDuration", DisplayName = "Hide seasonal duration", Default = false, DisplayOrder = 39)]
        bool HideSeasonalDuration { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = "ShowLocalisedEduLevels", DisplayName = "Show localised education levels", Default = false, DisplayOrder = 40)]
        bool ShowLocalisedEduLevels { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.AutoDenyJobseekerTimePeriod, DisplayName = "The number of days since a job has been closed before auto denying any applications that have been made to that job", Default = 30, DisplayOrder = 41)]
        int AutoDenyJobseekerTimePeriod { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.ShowWorkNetLink, DisplayName = "Show a link to WorkNet", Default = false, DisplayOrder = 42)]
        bool ShowWorkNetLink { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.SelectiveServiceRegistration, DisplayName = "Selective service registration", Default = false, DisplayOrder = 43)]
        bool SelectiveServiceRegistration { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = "AccountTypeDisplayType", DisplayName = "Account type display type", Default = ControlDisplayType.Optional, DisplayOrder = 44)]
        ControlDisplayType AccountTypeDisplayType { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = "JobDescriptionCharsShown", DisplayName = "Number of job description characters shown", Default = 0, DisplayOrder = 45)]
        int JobDescriptionCharsShown { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.HideYearsOfExpInSearchResults, DisplayName = "Hide years of experience in search results", Default = false, DisplayOrder = 46)]
        bool HideYearsOfExpInSearchResults { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.ShowYearsOfExperienceInPostingResults, DisplayName = "Show years of experience in posting search results", Default = false, DisplayOrder = 47)]
        bool ShowYearsOfExperienceInPostingResults { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.ShowHomeLocationInSearchResults, DisplayName = "Show home location in search results", Default = false, DisplayOrder = 48)]
        bool ShowHomeLocationInSearchResults { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.ShowPersonalEmail, DisplayName = "Show personal email", Default = false, DisplayOrder = 49)]
        bool ShowPersonalEmail { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.WorkNetLink, DisplayName = "The link to WorkNet", Default = "", DisplayOrder = 50)]
        string WorkNetLink { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.SystemDefaultSenderEmailAddress, DisplayName = "System default sender email address", Default = "", DisplayOrder = 51)]
        string SystemDefaultSenderEmailAddress { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.EnableDefaultOfficePerRecordType, DisplayName = "Enable default office per record type", Default = false, DisplayOrder = 52)]
        bool EnableDefaultOfficePerRecordType { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "NewBusinessUnitApproval", DisplayName = "New Business Unit Approval", Default = TalentApprovalOptions.NoApproval, DisplayOrder = 53)]
        TalentApprovalOptions NewBusinessUnitApproval { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.ExtendedVeteranPriorityofService, DisplayName = "Extended veteran priority of service", Default = false, DisplayOrder = 54)]
        bool ExtendedVeteranPriorityofService { get; set; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.ShowWorkLocationFullAddress, DisplayName = "Show work location full address", Default = false, DisplayOrder = 55)]
        bool ShowWorkLocationFullAddress { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = Constants.ConfigurationItemKeys.ReferralRequestApprovalQueueOfficeFilterType, DisplayName = "Referral Request Approval Queue - Office Filter Type option", Description = "Whether the Referral Request Approval office filter acts on the related Job offices instead of the job seeker offices", Default = false, DisplayOrder = 75)]
        bool ReferralRequestApprovalQueueOfficeFilterType { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "NewHiringManagerApproval", DisplayName = "New Hiring Manager Approval", Default = TalentApprovalOptions.NoApproval, DisplayOrder = 56)]
        TalentApprovalOptions NewHiringManagerApproval { get; }

        /// <summary>
        /// Gets the under age job seeker restriction threshold. A value of zero indicates there are no restrictions otherwise restrictions should apply to job seekers under the specified value.
        /// </summary>
        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.UnderAgeJobSeekerRestrictionThreshold, DisplayName = "Under-age job seeker restriction threshold", Default = 0, DisplayOrder = 60)]
        int UnderAgeJobSeekerRestrictionThreshold { get; }

        /// <summary>
        /// Gets the minimum career age threshold.
        /// </summary>
        /// <value>
        /// The minimum career age threshold.
        /// </value>
        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.MinimumCareerAgeThreshold, DisplayName = "Minimum career age threshold", Default = 14, DisplayOrder = 61)]
        int MinimumCareerAgeThreshold { get; }

        /// <summary>
        /// Gets a value indicating whether [use office address to send messages].
        /// </summary>
        /// <value>
        /// <c>true</c> if [use office address to send messages]; otherwise, <c>false</c>.
        /// </value>
        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.UseOfficeAddressToSendMessages, DisplayName = "Use office address to send messages", Default = false, DisplayOrder = 62)]
        bool UseOfficeAddressToSendMessages { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [show asset access].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [show asset access]; otherwise, <c>false</c>.
        /// </value>
        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.ShowAssetAccess, DisplayName = "Show Asset access", Default = false, DisplayOrder = 63)]
        bool ShowAssetAccess { get; }

        /// <summary>
        /// Gets the asset URL.
        /// </summary>
        /// <value>
        /// The asset URL.
        /// </value>
        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.AssetUrl, DisplayName = "Asset URL", Default = "", DisplayOrder = 64)]
        string AssetUrl { get; }

        /// <summary>
        /// Gets a value indicating whether [site under maintenance].
        /// </summary>
        /// <value>
        /// <c>true</c> if [site under maintenance]; otherwise, <c>false</c>.
        /// </value>
        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.SiteUnderMaintenance, DisplayName = "Site under maintenance", Default = false, DisplayOrder = 65)]
        bool SiteUnderMaintenance { get; }

        /// <summary>
        /// Gets the statistics archive in days.
        /// </summary>
        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.StatisticsArchiveInDays, DisplayName = "Statistics archive in days", Default = 1, DisplayOrder = 66)]
        int StatisticsArchiveInDays { get; }

        /// <summary>
        /// Gets whether to user the current user's name as the text for the "My Account" link.
        /// </summary>
        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.UseUsernameAsAccountLink, DisplayName = "Use user's name as account link", Default = false, DisplayOrder = 67)]
        bool UseUsernameAsAccountLink { get; }

        /// <summary>
        /// Gets a value indicating whether to save default localisation keys.
        /// </summary>
        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.SaveDefaultLocalisationItems, DisplayName = "Save Default Localisation Keys", Default = false, DisplayOrder = 68)]
        bool SaveDefaultLocalisationItems { get; }

        /// <summary>
        /// Gets a value indicating whether [no fixed location].
        /// </summary>
        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.NoFixedLocation, DisplayName = "Create postings with no fixed location", Default = false, DisplayOrder = 69)]
        bool NoFixedLocation { get; }

        /// <summary>
        /// Gets the job distribution feeds.
        /// </summary>
        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.JobDistributionFeeds, DisplayName = "The feeds to be included in the spidered postings statistics", Default = "", DisplayOrder = 70)]
        string JobDistributionFeeds { get; }

        /// <summary>
        /// Gets the business terminology.
        /// </summary>
        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.BusinessTerminology, DisplayName = "The singular terminology to be used to reference business/employer/company", Default = "Business", DisplayOrder = 71)]
        string BusinessTerminology { get; }

        /// <summary>
        /// Gets the businesses terminology.
        /// </summary>
        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.BusinessesTerminology, DisplayName = "The plural terminology to be used to reference businesses/employers/companies", Default = "Businesses", DisplayOrder = 72)]
        string BusinessesTerminology { get; }

        /// <summary>
        /// Gets the businesses terminology.
        /// </summary>
        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.EnableCDN, DisplayName = "Whether to enable CDN in the Ajax Control Toolkit", Default = false, DisplayOrder = 73)]
        bool EnableCDN { get; }

        #endregion

        #region Regular expression settings

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Regular Expression Settings", Key = "ResumeUploadAllowedFileRegExPattern", DisplayName = "Resume upload allowed File RegEx Pattern", DisplayOrder = 1)]
        string ResumeUploadAllowedFileRegExPattern { get; }

        //string ResumeUploadAllowedFileRegExPattern { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Regular Expression Settings", Key = "EmailAddressRegExPattern", DisplayName = "Email address regular expression", Default = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", DisplayOrder = 2)]
        string EmailAddressRegExPattern { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Regular Expression Settings", Key = "UserNameRegExPattern", DisplayName = "Username regular expression", Default = null, DisplayOrder = 3)]
        string UserNameRegExPattern { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Regular Expression Settings", Key = "UrlRegExPattern", DisplayName = "URL regular expression", Default = @"^(http|https)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]$", DisplayOrder = 4)]
        string UrlRegExPattern { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Regular Expression Settings", Key = "FEINRegExPattern", DisplayName = "FEIN regular expression", Default = @"^[0-9]{2}-[0-9]{7}$", DisplayOrder = 5)]
        string FEINRegExPattern { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Regular Expression Settings", Key = "PostalCodeRegExPattern", DisplayName = "Postal code regular expression", Default = @"(\d{5,})", DisplayOrder = 6)]
        string PostalCodeRegExPattern { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Regular Expression Settings", Key = "ExtendedPostalCodeRegExPattern", DisplayName = "Extended postal code regular expression", Default = @"(\d{5}|\d{9})", DisplayOrder = 7)]
        string ExtendedPostalCodeRegExPattern { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Regular Expression Settings", Key = "PostalCodeMaskPattern", DisplayName = "Postal code mask pattern", Default = "99999", DisplayOrder = 8)]
        string PostalCodeMaskPattern { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Regular Expression Settings", Key = "ExtendedPostalCodeMaskPattern", DisplayName = "Extended postal code mask pattern", Default = "999999999", DisplayOrder = 9)]
        string ExtendedPostalCodeMaskPattern { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Regular Expression Settings", Key = "PhoneNumberRegExPattern", DisplayName = "Phone number regular expression", Default = @"^(([0-9]{3}+|(\([0-9]{3}+\)))((-| )(?!$))?)*$", DisplayOrder = 10)]
        string PhoneNumberRegExPattern { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Regular Expression Settings", Key = "PhoneNumberMaskPattern", DisplayName = "Phone number mask pattern", Default = "(999) 999-9999", DisplayOrder = 11)]
        string PhoneNumberMaskPattern { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Regular Expression Settings", Key = "NonUsPhoneNumberMaskPattern", DisplayName = "Non-US phone number mask pattern", Default = "99999999999999999999", DisplayOrder = 12)]
        string NonUsPhoneNumberMaskPattern { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Regular Expression Settings", Key = "PhoneNumberStrictRegExPattern", DisplayName = "Phone number strict regular expression", Default = @"(\d{3})(\d{3})(\d{4})", DisplayOrder = 13)]
        string PhoneNumberStrictRegExPattern { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Regular Expression Settings", Key = "PhoneNumberFormat", DisplayName = "Phone number display format", Default = "($1) $2-$3", DisplayOrder = 14)]
        string PhoneNumberFormat { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Regular Expression Settings", Key = "DiacriticalMarkCheck", DisplayName = "Regular expression for checking diacritical marks", Default = @"[\u0300-\u036F]{3,}", DisplayOrder = 15)]
        string DiacriticalMarkCheck { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Regular Expression Settings", Key = Constants.ConfigurationItemKeys.SEINRegExPattern, DisplayName = "SEIN regular expression", Default = @"^.*$", DisplayOrder = 16)]
        string SEINRegExPattern { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Regular Expression Settings", Key = Constants.ConfigurationItemKeys.SEINMaskPattern, DisplayName = "SEIN mask pattern", Default = "", DisplayOrder = 17)]
        string SEINMaskPattern { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Regular Expression Settings", Key = Constants.ConfigurationItemKeys.UserNameMinLengthRegExPattern, DisplayName = "Username min length pattern", Default = "6", DisplayOrder = 18)]
        string UserNameMinLengthPattern { get; }

        #endregion

        #region Limits Settings

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = "StarRatingMinimumScores", DisplayName = "Star rating Minimum scores", DisplayOrder = 1)]
        int[] StarRatingMinimumScores { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = "JobExpiryCountdownDays", DisplayName = "Job expiry countdown (Days)", Default = new int[] { 1, 8, 15 }, DisplayOrder = 2)]
        int[] JobExpiryCountdownDays { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = "DefaultJobExpiry", DisplayName = "Default job expiry", Default = 14, DisplayOrder = 3)]
        int DefaultJobExpiry { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = "SimilarJobsCount", DisplayName = "Similar jobs count", Default = 20, DisplayOrder = 4)]
        int SimilarJobsCount { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = "SessionTimeout", DisplayName = "Session timeout (minutes)", Default = (24 * 60), DisplayOrder = 5)]
        int SessionTimeout { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = "UserSessionTimeout", DisplayName = "User session timeout", Default = 60, DisplayOrder = 6)]
        int UserSessionTimeout { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = "LE", DisplayName = "License expiration", Default = 120, DisplayOrder = 7)]
        int LicenseExpiration { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = Constants.ConfigurationItemKeys.MaximumSalary, DisplayName = "Maximum Salary", Default = 350000, DisplayOrder = 9)]
        int MaximumSalary { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = "MaximumSavedSearches", DisplayName = "Maximum saved searches", Default = 20, DisplayOrder = 10)]
        int MaximumSavedSearches { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = "MaximumRecentlyViewedPostings", DisplayName = "Maximum recently viewed postings", Default = 5, DisplayOrder = 11)]
        int MaximumRecentlyViewedPostings { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = "EmployeeSearchMaximumRecordsToReturn", DisplayName = "Employee search maximum records to return", Default = 100, DisplayOrder = 12)]
        int EmployeeSearchMaximumRecordsToReturn { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = "TalentPoolSearchMaximumRecordsToReturn", DisplayName = "Talent pool search maximum records to return", Default = 100, DisplayOrder = 13)]
        int TalentPoolSearchMaximumRecordsToReturn { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = "ResumeReferralApprovalsMinimumStars", DisplayName = "Resume referral approval minimum stars", Default = 3, DisplayOrder = 14)]
        int ResumeReferralApprovalsMinimumStars { get; }

        int ResumeUploadMaximumAllowedFileSize { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = "JobDevelopmentScoreThreshold", DisplayName = "Job development score threshold", Default = 700, DisplayOrder = 15)]
        int JobDevelopmentScoreThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = "RecommendedMatchesMinimumStars", DisplayName = "Recommended matches minimum stars", Default = 0, DisplayOrder = 16)]
        int RecommendedMatchesMinimumStars { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = "RecentPlacementTimeframe", DisplayName = "Recent placement timeframe", Default = 90, DisplayOrder = 17)]
        int RecentPlacementTimeframe { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = "SurveyFrequency", DisplayName = "Survey frequency", Default = 30, Description = "This specifies the frequency with which the survey is displayed", DisplayOrder = 18)]
        int SurveyFrequency { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = "PasswordValidationKeyExpiryHours", DisplayName = "Password validation key expiry (hours)", Default = 24, Description = "This specifies how long password validation keys are active for", DisplayOrder = 19)]
        int PasswordValidationKeyExpiryHours { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = Constants.ConfigurationItemKeys.MinimumSalary, DisplayName = "Minimum Salary", Default = 0, DisplayOrder = 20)]
        int MinimumSalary { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Limit Settings", Key = Constants.ConfigurationItemKeys.AdjustSalaryRangeByType, DisplayName = "Adjust the minimum/maximum salary limits based on salary frequency", Default = false, DisplayOrder = 21)]
        bool AdjustSalaryRangeByType { get; }

        #endregion

        #region Switch Settings

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "AllJobPostingsRequireApproval", DisplayName = "All Job Postings Require Approval", Default = false, DisplayOrder = 1)]
        bool AllJobPostingsRequireApproval { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "ActivationYellowWordFiltering", DisplayName = "Activation yellow word filtering", Default = false, DisplayOrder = 2)]
        bool ActivationYellowWordFiltering { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "EmployerPreferencesOverideSystemDefaults", DisplayName = "Employer preferences override system defaults", Default = false, DisplayOrder = 3)]
        bool EmployerPreferencesOverideSystemDefaults { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "DisplayResumeBuilderDuration", DisplayName = "Display resume builder duration", Default = false, DisplayOrder = 4)]
        bool DisplayResumeBuilderDuration { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "ShowLicenseRequirement", DisplayName = "Show license requirement", Default = true, DisplayOrder = 5)]
        bool ShowLicenseRequirement { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "AuthenticateAssistUserViaClient", DisplayName = "Authenticate Assist user via client", Default = false, DisplayOrder = 6)]
        bool AuthenticateAssistUserViaClient { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "ClientAllowsPasswordReset", DisplayName = "Client allows password reset", Default = false, DisplayOrder = 7)]
        bool ClientAllowsPasswordReset { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "EmailsEnabled", DisplayName = "Emails enabled", DisplayOrder = 8)]
        bool EmailsEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "EmailRedirect", DisplayName = "Email Redirect Address", DisplayOrder = 9)]
        string EmailRedirect { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "EmailLogFileLocation", DisplayName = "Suppressed Email Log File Location", DisplayOrder = 10)]
        string EmailLogFileLocation { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "MyAccountEnabled", DisplayName = "My account enabled", Default = true, DisplayOrder = 11)]
        bool MyAccountEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "ProfilingEnabled", DisplayName = "Profiling enabled", DisplayOrder = 12)]
        bool ProfilingEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "JobProofingEnabled", DisplayName = "Job Proofing enabled", DisplayOrder = 12)]
        bool JobProofingEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "SendOtherJobsEmail", DisplayName = "Send other jobs email", Default = false, DisplayOrder = 13)]
        bool SendOtherJobsEmail { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "HideSalary", DisplayName = "Hide salary", Default = false, DisplayOrder = 14)]
        bool HideSalary { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "JobSeekerReferralsEnabled", DisplayName = "Job seeker referrals enabled", Default = true, DisplayOrder = 15)]
        bool JobSeekerReferralsEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = Constants.ConfigurationItemKeys.ConfidentialEmployersEnabled, DisplayName = "Confidential employers enabled", Default = true, DisplayOrder = 16)]
        bool ConfidentialEmployersEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "SendHiringFromTaxCreditProgramNotification", DisplayName = "Send hiring from tax credit program notification", Default = true, DisplayOrder = 17)]
        bool SendHiringFromTaxCreditProgramNotification { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "SharingJobsForSameFEIN", DisplayName = "Sharing jobs for same FEIN", Default = true, DisplayOrder = 18)]
        bool SharingJobsForSameFEIN { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "NationWideInstance", DisplayName = "Nation wide instance", Default = true, DisplayOrder = 19)]
        bool NationWideInstance { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "CountyEnabled", DisplayName = "County enabled", Default = false, DisplayOrder = 20)]
        bool CountyEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "ShowStudentExternalId", DisplayName = "Show student external ID", Default = true, DisplayOrder = 21)]
        bool ShowStudentExternalId { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "NewEmployerCensorshipFiltering", DisplayName = "New employer censorship filtering", Default = true, DisplayOrder = 22)]
        bool NewEmployerCensorshipFiltering { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = "DisplaySurveyFrequency", DisplayName = "Dsiplay survey frequency", Default = false, Description = "Sets whether to display survey frequency", DisplayOrder = 23)]
        bool DisplaySurveyFrequency { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = Constants.ConfigurationItemKeys.ConfidentialEmployersDefault, DisplayName = "Default value for confidential employers value", Default = true, DisplayOrder = 24)]
        bool ConfidentialEmployersDefault { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = Constants.ConfigurationItemKeys.DirectEmails, DisplayName = "Direct emails, rather than by message bus", Default = false, DisplayOrder = 25)]
        bool DirectEmails { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = Constants.ConfigurationItemKeys.ASyncEmails, DisplayName = "Send emails asynchronously", Default = false, DisplayOrder = 26)]
        bool ASyncEmails { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = Constants.ConfigurationItemKeys.MaximumEmailLength, DisplayName = "Maximum Email Length", DisplayOrder = 27)]
        int MaximumEmailLength { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Switch Settings", Key = Constants.ConfigurationItemKeys.MaximumUserNameLength, DisplayName = "Maximum Username Length", DisplayOrder = 28)]
        int MaximumUserNameLength { get; }

        #endregion

        #region Recaptcha settings

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Recaptcha Settings", Key = "RecaptchaEnabled", DisplayName = "Recaptcha enabled", Default = false, DisplayOrder = 1)]
        bool RecaptchaEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Recaptcha Settings", Key = "RecaptchaPublicKey", DisplayName = "Recaptcha public key", Default = "NOTSET", DisplayOrder = 2)]
        string RecaptchaPublicKey { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Recaptcha Settings", Key = "RecaptchaPrivateKey", DisplayName = "Recaptcha private key", Default = "NOTSET", DisplayOrder = 3)]
        string RecaptchaPrivateKey { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Recaptcha Settings", Key = "RecaptchaAuthorizeAfterFirstCheck", DisplayName = "Recaptcha authorize if previously checked", Default = false, DisplayOrder = 4)]
        bool RecaptchaAuthorizeAfterFirstCheck { get; }

        #endregion

        #region Demoshield settings

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "DemoShield Settings", Key = "DemoShieldCustomerCode", DisplayName = "Demo Shield customer code", DisplayOrder = 1)]
        string DemoShieldCustomerCode { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "DemoShield Settings", Key = "DemoShieldRedirectUrl", DisplayName = "Demo Shield redirect URL", DisplayOrder = 2)]
        string DemoShieldRedirectUrl { get; }

        #endregion

        #region Migration settings

        [WebConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Migration Settings", Key = "Focus-MigrationProvider", DisplayName = "Migration Provider", DisplayOrder = 1)]
        string MigrationProvider { get; }

        [WebConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Migration Settings", Key = "Focus-MigrationBatchSize", DisplayName = "Migration batch size", DisplayOrder = 2)]
        int MigrationBatchSize { get; }

        [WebConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Migration Settings", Key = "Focus-ImportBatchSize", DisplayName = "Import batch size", DisplayOrder = 3)]
        int ImportBatchSize { get; }

        [WebConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Migration Settings", Key = "EnableFrontEndMigration", DisplayName = "Enable front end migration", DisplayOrder = 4)]
        bool EnableFrontEndMigration { get; }

        [WebConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Migration Settings", Key = "IntegrationImportBatchSize", DisplayName = "Front end migration - Batch size", DisplayOrder = 5)]
        int IntegrationImportBatchSize { get; }

        #endregion

        #region PageState settings

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "PageState Settings", Key = "PageStateConnectionString", DisplayName = "PageState Connection String", DisplayOrder = 1)]
        string PageStateConnectionString { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "PageState Settings", Key = "PageStatePersister", DisplayName = "Page state persistor", DisplayOrder = 2)]
        PageStatePersisters PageStatePersister { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "PageState Settings", Key = "PageStateQueueMaxLength", DisplayName = "Page state queue max length", DisplayOrder = 3)]
        int PageStateQueueMaxLength { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "PageState Settings", Key = "PageStateTimeOut", DisplayName = "Page state timeout", DisplayOrder = 4)]
        int PageStateTimeOut { get; }

        #endregion

        #region Support settings

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Support Settings", Key = "SupportEmail", DisplayName = "Support email", Default = "support@burning-glass.com", DisplayOrder = 1)]
        string SupportEmail { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Support Settings", Key = "SupportPhone", DisplayName = "Support phone number", Default = "+1 (617) 227 4875", DisplayOrder = 2)]
        string SupportPhone { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Support Settings", Key = "SupportHoursOfBusiness", DisplayName = "Support business hours", Default = "Business hours are 8:00-4:30 EST.", DisplayOrder = 3)]
        string SupportHoursOfBusiness { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Support Settings", Key = "SupportHelpTeam", DisplayName = "Support help team", Default = "Support Help Team", DisplayOrder = 4)]
        string SupportHelpTeam { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Support Settings", Key = "SupportDaysResponse", DisplayName = "Number of days response from support", Default = 2, DisplayOrder = 5)]
        int SupportDaysResponse { get; }

        #endregion

        #region Labor Insight

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Labour Insight Settings", Key = "LaborInsightAccess", DisplayName = "Labour Insight Access", Default = true, DisplayOrder = 1)]
        bool LaborInsightAccess { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Labour Insight Settings", Key = "LaborInsightLinkUrl", DisplayName = "Labour insight link URL", Default = "http://www.burning-glass.com/products/labor.html", DisplayOrder = 2)]
        string LaborInsightLinkUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Labour Insight Settings", Key = "LaborRegulationsUrl", DisplayName = "Labour regulations URL", Default = null, DisplayOrder = 3)]
        string LaborRegulationsUrl { get; }

        #endregion

        #region Alert Defaults

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Alert Defaults", Key = "JobAlertsOption", DisplayName = "Job alerts Option", Default = JobAlertsOptions.UserConfiguration, DisplayOrder = 1)]
        JobAlertsOptions JobAlertsOption { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Alert Defaults", Key = "JobAlertFrequency", DisplayName = "Job alert frequency", Default = AlertFrequencies.Weekly, DisplayOrder = 2)]
        AlertFrequencies JobAlertFrequency { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Alert Defaults", Key = "JobAlertTruncateWords", DisplayName = "Job alert truncated description", Description = "Truncate job description to this number of words", Default = 50, DisplayOrder = 3)]
        int JobAlertTruncateWords { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Alert Defaults", Key = "StaffEmailAlerts", DisplayName = "Staff email alerts", Default = AlertFrequencies.Never, DisplayOrder = 4)]
        AlertFrequencies StaffEmailAlerts { get; }

        #endregion

        #region Service Method Validation

        [WebConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Service method validation", Key = "ValidateServiceVersion", DisplayName = "Validate service version", Description = "Sets whether to check the version number in service method calls", DisplayOrder = 1)]
        bool ValidateServiceVersion { get; }

        [WebConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Service method validation", Key = "UsesAccessToken", DisplayName = "Uses access token", Description = "Sets whether AccessToken validation of service methods is enabled", DisplayOrder = 2)]
        bool UsesAccessToken { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Service method validation", Key = "AccessTokenKey", DisplayName = "Access Token Key", Default = "", Description = "The key for AccessToken validation", DisplayOrder = 3)]
        string AccessTokenKey { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Service method validation", Key = "AccessTokenSecret", DisplayName = "Access Token Secret", Default = "", Description = "The secret code for AccessToken validation", DisplayOrder = 4)]
        string AccessTokenSecret { get; }

        #endregion

        #region Live Jobs Repository Settings

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Live Jobs Repository Settings", Key = "LiveJobsApiRootUrl", DisplayName = "Live jobs API Root URL", Default = "http://10.0.1.223/LiveJobsApi_v1127", Description = "This specifies the live jobs API root URL", DisplayOrder = 1)]
        string LiveJobsApiRootUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Live Jobs Repository Settings", Key = "LiveJobsApiKey", DisplayName = "Live jobs API Key", Default = "BGT", Description = "This specifies the live jobs API key", DisplayOrder = 2)]
        string LiveJobsApiKey { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Live Jobs Repository Settings", Key = "LiveJobsApiSecret", DisplayName = "Live jobs API Secret", Default = "086718ED10AC48B685F93535208AE903", Description = "This specifies the live jobs API secret", DisplayOrder = 3)]
        string LiveJobsApiSecret { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Live Jobs Repository Settings", Key = "LiveJobsApiToken", DisplayName = "Live jobs API Token", Default = "Core", Description = "This specifies the live jobs API token", DisplayOrder = 4)]
        string LiveJobsApiToken { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Live Jobs Repository Settings", Key = "LiveJobsApiTokenSecret", DisplayName = "Live jobs API Token Secret", Default = "3250A588F0FD11E1A0EA0CF26088709B", Description = "This specifies the live jobs API token secret", DisplayOrder = 5)]
        string LiveJobsApiTokenSecret { get; }

        #endregion

        #region IRS Settings

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "IRS Settings", Key = "IrsOnlineApplicationHours", DisplayName = "IRS online application hours", Default = "7:00 to 10:00 am (EST), Monday-Friday", DisplayOrder = 1)]
        string IrsOnlineApplicationHours { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "IRS Settings", Key = "IrsOnlineApplicationUrl", DisplayName = "IRS online application URL", Default = "https://sa1.www4.irs.gov/modiein/individual/index.jsp", DisplayOrder = 2)]
        string IrsOnlineApplicationUrl { get; }

        #endregion

        #region Posting footer

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "PostingFooter", Key = Constants.ConfigurationItemKeys.PostingFooterTitle, DisplayName = "Posting Footer Title", Default = "Required notice about this job", DisplayOrder = 1)]
        string PostingFooterTitle { get; }

        #endregion

        #region Credit Check

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Credit Check Settings", Key = Constants.ConfigurationItemKeys.ShowCreditCheck, DisplayName = "Show Credit Check", Default = false, DisplayOrder = 1)]
        bool ShowCreditCheck { get; set; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Credit Check Settings", Key = "TalentCreditCheckJobPostingWarning", DisplayName = "Talent Credit Check Job Posting Warning", Default = "The attached job announcement contains a hiring restriction based on credit information. You are not prohibited from applying for this job, even though you may have “bad” credit or a poor credit history. Further, rejecting you for reasons related to credit may violate federal civil rights laws, depending on the circumstances.<br/><br/><a href= '{0}' target='_blank'>Please click for more information</a>", DisplayOrder = 2)]
        string JobSeekerCreditCheckJobPostingWarning { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Credit Check Settings", Key = Constants.ConfigurationItemKeys.TEGLNoticeToEmployersURL, DisplayName = "TEGL Notice To Employers URL", Default = "http://wdr.doleta.gov/directives/attach/TEGL/TEGL-11-14_Attachment1_Acc.pdf", DisplayOrder = 2)]
        string TEGLNoticeToEmployersURL { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Credit Check Settings", Key = Constants.ConfigurationItemKeys.TEGLNoticeToJobSeekersURL, DisplayName = "TEGL Notice To Job Seekers URL", Default = "http://wdr.doleta.gov/directives/attach/TEGL/TEGL-11-14_Attachment3_Acc.pdf", DisplayOrder = 3)]
        string TEGLNoticeToJobSeekersURL { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Credit Check Settings", Key = "TalentCreditCheckJobPostingWarning", DisplayName = "Talent Credit Check Job Posting Warning", Default = "The attached job announcement contains a hiring restriction based on credit information. You are not prohibited from applying for this job, even though you may have “bad” credit or a poor credit history. Further, rejecting you for reasons related to credit may violate federal civil rights laws, depending on the circumstances.<br/><br/><a href= '{0}' target='_blank'>Please click for more information</a>", DisplayOrder = 4)]
        string TalentCreditCheckJobPostingWarning { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Credit Check Settings", Key = "AssistCreditCheckJobReferralWarning", DisplayName = "Assist Credit Check Job Referral Warning", Default = @"<br/><br/><font size=""8""><b>Required notice about this job<b/></font><br/><br/>The public workforce system has identified hiring restrictions based on credit history in the job announcement submitted for posting by this employer or in a job announcement referenced in a job bank.<br/><br/>We advise employers not to automatically exclude job seekers based on their credit history unless the employer can show that a credit history restriction is related to the job posted and consistent with the employer’s business needs. While employers are permitted to use credit reports in hiring and other decisions, this type of screening requirement may unjustifiably limit the employment opportunities of applicants in protected groups and may therefore violate federal civil rights laws. Any employer that submits a job announcement containing restrictions or exclusions based on an applicant’s credit information has an opportunity to edit or remove the announcement.<br/><br/>Please take this opportunity to remove or edit the announcement as needed. If you continue to post the announcement as is, the announcement will be posted along with information for job seekers about the civil rights laws that may apply to restrictions based on credit history.<br/><br/><a href= '{0}' target='_blank'>Please click for more information</a>", DisplayOrder = 5)]
        string AssistCreditCheckJobReferralWarning { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Credit Check Settings", Key = Constants.ConfigurationItemKeys.TEGLNoticeToStaffMembersURL, DisplayName = "TEGL Notice To Staff Members URL", Default = "http://wdr.doleta.gov/directives/attach/TEGL/TEGL-11-14_Attachment2_Acc.pdf", DisplayOrder = 6)]
        string TEGLNoticeToStaffMembersURL { get; }


        #endregion

        #region Criminal Background Check

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Criminal Background Check Settings", Key = Constants.ConfigurationItemKeys.ShowCriminalBackgroundCheck, DisplayName = "Show Criminal Background Check", Default = false, DisplayOrder = 1)]
        bool ShowCriminalBackgroundCheck { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Criminal Background Check Settings", Key = Constants.ConfigurationItemKeys.JobSeekerCriminalBackgroundJobPostingWarning, DisplayName = "Job Seeker Criminal Background Job Posting Warning", Default = "Individuals with conviction or arrest histories are not prohibited from applying for this job. The public workforce system has identified criminal record exclusions or restrictions in the attached job announcement. These exclusions or restrictions may be unlawful under certain circumstances. Therefore, the system is providing this notice to job seekers.<br/><br/><a href= '#CRIMINALBACKGROUNDURL#' target='_blank'>Please click for more information</a>", DisplayOrder = 4)]
        string JobSeekerCriminalBackgroundJobPostingWarning { get; }

        #endregion

        #endregion

        #region TalentSettings

        #region Visual settings

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Visual Settings", Key = "TalentLightColour", DisplayName = "Talent light colour", Default = "cccccc", DisplayOrder = 1)]
        string TalentLightColour { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Visual Settings", Key = "TalentDarkColour", DisplayName = "Talent dark colour", Default = "ececec", DisplayOrder = 2)]
        string TalentDarkColour { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Visual Settings", Key = "ShowBurningGlassLogoInTalent", DisplayName = "Show Burning Glass logo in Talent", Default = true, DisplayOrder = 3)]
        bool ShowBurningGlassLogoInTalent { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Visual settings", Key = Constants.ConfigurationItemKeys.SearchByIndustries, DisplayName = "Search by industries", Default = false, DisplayOrder = 4)]
        bool SearchByIndustries { get; }

        #endregion

        #region Talent Application Settings

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Talent Application Settings", Key = "TalentApplicationPath", DisplayName = "Talent Application Path", Default = @"http://talentdev.burning-glass.com", DisplayOrder = 1)]
        string TalentApplicationPath { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Talent Application Settings", Key = "FocusTalentApplicationName", DisplayName = "Focus Talent application name", Default = "Focus/Talent", DisplayOrder = 2)]
        string FocusTalentApplicationName { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Talent Application Settings", Key = "AllowTalentCompanyInformationUpdate", DisplayName = "Allow Talent company information update", Default = false, DisplayOrder = 3)]
        bool AllowTalentCompanyInformationUpdate { get; }

        //bool AllowTalentCompanyInformationUpdate { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Talent Application Settings", Key = "DisableTalentAuthentication", DisplayName = "Disable Talent authentication", Default = false, DisplayOrder = 4)]
        bool DisableTalentAuthentication { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Talent Application Settings", Key = "TalentPoolSearch", DisplayName = "Talent Pool Search", Default = false, DisplayOrder = 5)]
        bool TalentPoolSearch { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Talent Application Settings", Key = "BusinessUnitTalentPoolSearch", DisplayName = "BusinessUnit Talent Pool Search", Default = false, DisplayOrder = 6)]
        bool BusinessUnitTalentPoolSearch { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Talent Application Settings", Key = "HiringManagerTalentPoolSearch", DisplayName = "Hiring Manager Talent Pool Search", Default = false, DisplayOrder = 7)]
        bool HiringManagerTalentPoolSearch { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Talent Application Settings", Key = "MultipleJobseekerInvitesEnabled", DisplayName = "Multiple Jobseeker Invites Enabled", Default = false, DisplayOrder = 8)]
        bool MultipleJobseekerInvitesEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Talent Application Settings", Key = Constants.ConfigurationItemKeys.DisplayFEIN, DisplayName = "FEIN display options", Description = "Whether the FEIN is hidden, optional or mandatory", Default = ControlDisplayType.Mandatory, DisplayOrder = 9)]
        ControlDisplayType DisplayFEIN { get; }


        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Talent Application Settings", Key = "ShowEmployeeNumbers", DisplayName = "Show Employee Numbers", Default = false, DisplayOrder = 10)]
        bool ShowEmployeeNumbers { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Talent Application Settings", Key = "PreScreeningServiceRequest", DisplayName = "Pre Screening Service Request", Default = false, DisplayOrder = 11)]
        bool PreScreeningServiceRequest { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Talent Application Settings", Key = Constants.ConfigurationItemKeys.HasJobUpload, DisplayName = "Has Job Upload functionality", Default = true, DisplayOrder = 12)]
        bool HasJobUpload { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Talent Application Settings", Key = Constants.ConfigurationItemKeys.DisplayJobActivityLogInTalent, DisplayName = "Display Job Activity Log In Talent", Default = false, DisplayOrder = 13)]
        bool DisplayJobActivityLogInTalent { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Talent Application Settings", Key = Constants.ConfigurationItemKeys.EnableTalentPasswordSecurity, DisplayName = "Enable Talent Password Security", Default = false, DisplayOrder = 14)]
        bool EnableTalentPasswordSecurity { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Talent Application Settings", Key = Constants.ConfigurationItemKeys.ShowCustomMessageTemplate, DisplayName = "Show Custom Message Template on Talent Applicant's tab", Default = false, DisplayOrder = 14)]
        bool ShowCustomMessageTemplate { get; }

        #endregion

        #region Job Settings

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job Settings", Key = "SplitJobsByLocation", DisplayName = "Split jobs by location", Default = false, DisplayOrder = 1)]
        bool SplitJobsByLocation { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job Settings", Key = "HideScreeningPreferences", DisplayName = "Hide Screening Preferences", Default = false, DisplayOrder = 2)]
        bool HideScreeningPreferences { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job Settings", Key = "HideInterviewContactPreferences", DisplayName = "Hide Interview Contact Preferences", Default = false, DisplayOrder = 3)]
        bool HideInterviewContactPreferences { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job Settings", Key = "MeetsMinimumWageRequirementCheckBox", DisplayName = "Show the 'Meets minimum wage requirement' checkbox", Default = false, DisplayOrder = 4)]
        bool MeetsMinimumWageRequirementCheckBox { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job Settings", Key = Constants.ConfigurationItemKeys.DaysForDefaultJobClosingDate, DisplayName = "Number of days in future to default the job closing date", Default = 30, DisplayOrder = 5)]
        int DaysForDefaultJobClosingDate { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job Settings", Key = "DaysForJobPostingLifetime", DisplayName = "Number of days a job posting can be active in the system", Default = 0, DisplayOrder = 6)]
        int DaysForJobPostingLifetime { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job Settings", Key = "DaysForFederalContractorLifetime", DisplayName = "Number of days a federal contractor job posting can be active in the system", Default = 0, DisplayOrder = 7)]
        int DaysForFederalContractorLifetime { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job Settings", Key = "DaysForForeignLaborH2ALifetime", DisplayName = "Number of days a Foreign Labor H2A job posting can be active in the system", Default = 365, DisplayOrder = 8)]
        int DaysForForeignLaborH2ALifetime { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job Settings", Key = "DaysForForeignLaborH2BLifetime", DisplayName = "Number of days a Foreign Labor H2B job posting can be active in the system", Default = 365, DisplayOrder = 9)]
        int DaysForForeignLaborH2BLifetime { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job Settings", Key = "DaysForForeignLaborOtherLifetime", DisplayName = "Number of days a Foreign Labor Other job posting can be active in the system", Default = 365, DisplayOrder = 10)]
        int DaysForForeignLaborOtherLifetime { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job settings", Key = "HideWOTC", DisplayName = "Hide WOTC panel in job wizard", Default = false, DisplayOrder = 11)]
        bool HideWOTC { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job settings", Key = "HideMinimumAge", DisplayName = "Hide minimum age panel in job wizard", Default = false, DisplayOrder = 12)]
        bool HideMinimumAge { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job settings", Key = "HideUploadMultipleJobs", DisplayName = "Hide upload multiple jobs option", Default = false, DisplayOrder = 99)]
        bool HideUploadMultipleJobs { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job settings", Key = Constants.ConfigurationItemKeys.HideJobConditions, DisplayName = "Hide job conditions panel in job wizard", Default = false, DisplayOrder = 13)]
        bool HideJobConditions { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job settings", Key = Constants.ConfigurationItemKeys.HasMinimumHoursPerWeek, DisplayName = "Job wizard has minimum hours per week field", Default = false, DisplayOrder = 14)]
        bool HasMinimumHoursPerWeek { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job settings", Key = Constants.ConfigurationItemKeys.ScreeningPrefExclusiveToMinStarMatch, DisplayName = "Only job seekers with a match of x stars or above can apply", Default = false, DisplayOrder = 15)]
        bool ScreeningPrefExclusiveToMinStarMatch { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job settings", Key = Constants.ConfigurationItemKeys.ScreeningPrefBelowStarMatchApproval, DisplayName = "Only job seekers with a match score of x stars may apply without staff permission", Default = true, DisplayOrder = 16)]
        bool ScreeningPrefBelowStarMatchApproval { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job settings", Key = "ScreeningPrefExclusiveToMinStarMatchDefault", DisplayName = "Only job seekers with a match of x stars or above can apply default value", Default = ScreeningPreferences.JobSeekersMustHave3StarMatchToApply, DisplayOrder = 17)]
        ScreeningPreferences ScreeningPrefExclusiveToMinStarMatchDefault { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job Settings", Key = Constants.ConfigurationItemKeys.ShowPostingHowToApply, DisplayName = "Show How To Apply secion on postings", Default = false, DisplayOrder = 18)]
        bool ShowPostingHowToApply { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job Settings", Key = Constants.ConfigurationItemKeys.PartTimeEmploymentLimit, DisplayName = "Maximum number of hours for part-time jobs", Default = 0, DisplayOrder = 19)]
        int PartTimeEmploymentLimit { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job Settings", Key = Constants.ConfigurationItemKeys.DaysForMinimumJobClosingDate, DisplayName = "Minumum number of days in future to which the job closing date can be set", Default = 0, DisplayOrder = 20)]
        int DaysForMinimumJobClosingDate { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job Settings", Key = Constants.ConfigurationItemKeys.MandatoryHoursPerWeek, DisplayName = "Hours per week mandatory for jobs", Default = false, DisplayOrder = 21)]
        bool MandatoryHoursPerWeek { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job Settings", Key = Constants.ConfigurationItemKeys.MandatoryHours, DisplayName = "Hours (part-time/full-time) mandatory for jobs", Default = false, DisplayOrder = 22)]
        bool MandatoryHours { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job Settings", Key = Constants.ConfigurationItemKeys.MandatoryJobType, DisplayName = "Job Type mandatory for jobs", Default = false, DisplayOrder = 23)]
        bool MandatoryJobType { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job Settings", Key = Constants.ConfigurationItemKeys.FederalContratorExpiryDateMandatory, DisplayName = "Whether a federal contractor job posting should have a mandatory expiry date", Default = true, DisplayOrder = 24)]
        bool FederalContratorExpiryDateMandatory { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Job Settings", Key = Constants.ConfigurationItemKeys.RequirementsPreferenceDefault, DisplayName = "Requirements preference default", Default = RequirementsPreference.AllowAll, DisplayOrder = 25)]
        RequirementsPreference RequirementsPreferenceDefault { get; }

        #endregion

        #region Approval Settings

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Approval Settings", Key = Constants.ConfigurationItemKeys.ApprovalDefaultForCustomerCreatedJobs, DisplayName = "Approval default for Customer-created job posting", Default = JobApprovalOptions.SpecifiedApproval, DisplayOrder = 1)]
        JobApprovalOptions ApprovalDefaultForCustomerCreatedJobs { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Approval Settings", Key = Constants.ConfigurationItemKeys.ApprovalChecksForCustomerCreatedJobs, DisplayName = "Checks needed on jobs for approval", Default = new[] { JobApprovalCheck.SpecialRequirements }, DisplayOrder = 2)]
        JobApprovalCheck[] ApprovalChecksForCustomerCreatedJobs { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Approval Settings", Key = Constants.ConfigurationItemKeys.ApprovalDefaultForStaffCreatedJobs, DisplayName = "Approval default for staff-created job postings", Default = JobApprovalOptions.NoApproval, DisplayOrder = 3)]
        JobApprovalOptions ApprovalDefaultForStaffCreatedJobs { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Approval Settings", Key = Constants.ConfigurationItemKeys.ApprovalChecksForStaffCreatedJobs, DisplayName = "Checks needed on jobs for approval", Default = new[] { JobApprovalCheck.SpecialRequirements }, DisplayOrder = 4)]
        JobApprovalCheck[] ApprovalChecksForStaffCreatedJobs { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Talent, SubCategory = "Approval Settings", Key = Constants.ConfigurationItemKeys.ActivateYellowWordFilteringForStaffCreatedJobs, DisplayName = "Activate yellow word filtering For staff-created jobs", Default = false, DisplayOrder = 5)]
        bool ActivateYellowWordFilteringForStaffCreatedJobs { get; }
        
        #endregion

        #endregion

        #region AssistSettings

        #region Application settings

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = "AssistApplicationPath", DisplayName = "Assist application path", Default = @"http://assistdev.burning-glass.com", DisplayOrder = 1)]
        string AssistApplicationPath { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = "FocusAssistApplicationName", DisplayName = "Assist application name", Default = "Focus/Assist", DisplayOrder = 2)]
        string FocusAssistApplicationName { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = "DisableAssistAuthentication", DisplayName = "Disable Assist authentication", Default = false, DisplayOrder = 3)]
        bool DisableAssistAuthentication { get; }

        // Should this be in candidate issues?
        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = "InappropriateEmailAddressCheckEnabled", DisplayName = "Flag inappropriate email addresses", Default = false, DisplayOrder = 4)]
        bool InappropriateEmailAddressCheckEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = "OfficeExternalIdMandatory", DisplayName = "Office external ID mandatory", Default = true, DisplayOrder = 5)]
        bool OfficeExternalIdMandatory { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = "ShowAssistMultipleJSSearchButtons", DisplayName = "Display Add another button for searching for multiple Jobseekers", Default = false, DisplayOrder = 6)]
        bool ShowAssistMultipleJSSearchButtons { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = "ClientSpecificExternalJobseekerIDRegex", DisplayName = "Client specific external jobseeker ID regex", Default = "", DisplayOrder = 7)]
        string ClientSpecificExternalJobseekerIDRegex { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = "MaxSearchableIDs", DisplayName = "Maximum number of IDs that User can filter on", Default = 5, DisplayOrder = 8)]
        int MaxSearchableIDs { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = "AssistShowChangeSSNActivityInWF", DisplayName = "Show change SSN activity in action dropdowns", Default = false, DisplayOrder = 9)]
        bool AssistShowChangeSSNActivityInWF { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = "NumberOfHiringManagersOnEmployerProfile", DisplayName = "Number of hiring managers to show for each linked company when viewing an employer profile", Default = 3, DisplayOrder = 10)]
        int NumberOfHiringManagersOnEmployerProfile { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = Constants.ConfigurationItemKeys.CheckExternalSystemOnSeekerSearch, DisplayName = "Check external system when searching for job seekers", Default = false, DisplayOrder = 12)]
        bool CheckExternalSystemOnSeekerSearch { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = "UnsubscribeFromJSMessages", DisplayName = "Unsubscribe from job seeker email messages", Default = false, DisplayOrder = 13)]
        bool UnsubscribeFromJSMessages { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = "UnsubscribeEmailFooter", DisplayName = "Unsubscribe email footer", Default = "<br><br>This email has been sent by #FOCUSASSIST#. To unsubscribe from future messages of this nature, please <a href=\"{0}\" target=\"_blank\">click here</a>", DisplayOrder = 14)]
        string UnsubscribeEmailFooter { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = "UnsubscribeFromHMMessages", DisplayName = "Unsubscribe from hiring manager email messages", Default = false, DisplayOrder = 15)]
        bool UnsubscribeFromHMMessages { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = Constants.ConfigurationItemKeys.ShowTalentActivityReport, DisplayName = "Show Talent activity report", Default = false, DisplayOrder = 16)]
        bool ShowTalentActivityReport { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = "ReadOnlyContactInfo", DisplayName = "Read Only Contact Information", Default = false, DisplayOrder = 17)]
        bool ReadOnlyContactInfo { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = Constants.ConfigurationItemKeys.EnableAssistErrorHandling, DisplayName = "Enable Case Management Error Handling", Default = false, DisplayOrder = 18)]
        bool EnableAssistErrorHandling { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist Application Settings", Key = Constants.ConfigurationItemKeys.EditFEIN, DisplayName = "Edit FEIN option", Description = "Whether the Edit FEIN is visible", Default = true, DisplayOrder = 19)]
        bool CanEditFEIN { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = Constants.ConfigurationItemKeys.DocumentFileTypes, DisplayName = "Supported document file types", Default = "pdf,xls,xlsx", DisplayOrder = 20)]
        string DocumentFileTypes { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = Constants.ConfigurationItemKeys.ShowStaffAssistedLMI, DisplayName = "Show StaffAssistedLMI", Default = false, DisplayOrder = 19)]
        bool ShowStaffAssistedLMI { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = Constants.ConfigurationItemKeys.MaxStaffForOffices, DisplayName = "Maximum number of staff that can be assigned to an office", Default = 50, DisplayOrder = 20)]
        int MaxStaffForOffices { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = Constants.ConfigurationItemKeys.EnablePreferredEmployers, DisplayName = "Enable Preferred Employers", Default = 50, DisplayOrder = 20)]
        bool EnablePreferredEmployers { get; }

        #endregion

        #region Visual settings

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist visual settings", Key = "ShowBurningGlassLogoInAssist", DisplayName = "Show Burning Glass logo in Assist", Default = true, DisplayOrder = 1)]
        bool ShowBurningGlassLogoInAssist { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist visual settings", Key = "AssistLightColour", DisplayName = "Assist light colour", Default = "cccccc", DisplayOrder = 2)]
        string AssistLightColour { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist visual settings", Key = "AssistDarkColour", DisplayName = "Assist dark colour", Default = "ececec", DisplayOrder = 3)]
        string AssistDarkColour { get; }

        #endregion

        #region Candidate issue settings

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue settings", Key = "NotLoggingInEnabled", DisplayName = "Not logging in", Description = "Tick to enable flagging for not logging in", Default = true, DisplayOrder = 1)]
        bool NotLoggingInEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue settings", Key = "NotClickingLeadsEnabled", DisplayName = "Not clicking leads", Description = "Tick to enable flagging for not clicking leads", Default = true, DisplayOrder = 2)]
        bool NotClickingLeadsEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue settings", Key = "RejectingJobOffersEnabled", DisplayName = "Rejecting job offers", Description = "Tick to enable flagging for rejecting job offers", Default = true, DisplayOrder = 3)]
        bool RejectingJobOffersEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue settings", Key = "NotReportingForInterviewEnabled", DisplayName = "Not reporting for interview", Description = "Tick to enable flagging for not reporting for interview", Default = true, DisplayOrder = 4)]
        bool NotReportingForInterviewEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue settings", Key = "NotRespondingToEmployerInvitesEnabled", DisplayName = "Not responding to employer invites", Description = "Tick to enable flagging for not responding to employer invites", Default = true, DisplayOrder = 5)]
        bool NotRespondingToEmployerInvitesEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue settings", Key = "NotRespondingToReferralSuggestionsEnabled", DisplayName = "Not respnding to referral suggestions", Description = "Tick to enable flagging for not responding to referral suggestions", Default = true, DisplayOrder = 6)]
        bool NotRespondingToReferralSuggestionsEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue settings", Key = "SelfReferringToUnqualifiedJobsEnabled", DisplayName = "Self referring to unqualified jobs", Description = "Tick to enable flagging for self referring to unqualified jobs", Default = true, DisplayOrder = 7)]
        bool SelfReferringToUnqualifiedJobsEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue settings", Key = "ShowingLowQualityMatchesEnabled", DisplayName = "Show low quality matches", Description = "Tick to enable showing of low quality matches", Default = true, DisplayOrder = 8)]
        bool ShowingLowQualityMatchesEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue settings", Key = "PostingLowQualityResumeEnabled", DisplayName = "Post low quality resume", Description = "Tick to enable posting of low quality resumes", Default = true, DisplayOrder = 9)]
        bool PostingLowQualityResumeEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue settings", Key = "PostHireFollowUpEnabled", DisplayName = "Post-hire follow up", Description = "Tick to enable post hire follow up", Default = true, DisplayOrder = 10)]
        bool PostHireFollowUpEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue settings", Key = "FollowUpEnabled", DisplayName = "Follow up", Description = "Tick to enable follow up", Default = true, DisplayOrder = 11)]
        bool FollowUpEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue settings", Key = "NotSearchingJobsEnabled", DisplayName = "Not searching jobs", Description = "Tick to enable flagging for not searching jobs", Default = true, DisplayOrder = 12)]
        bool NotSearchingJobsEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue settings", Key = "JobSeekerSendInactivityEmail", DisplayName = "Jobseeker send inactivity email", Description = "Tick to enable sending email on jobseeker inactivity", Default = false, DisplayOrder = 13)]
        bool JobSeekerSendInactivityEmail { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue settings", Key = "PositiveFeedbackCheckEnabled", DisplayName = "Flag positive survey responses", Description = "Tick to enable flagging for a jobseeker leaving positive feedback", Default = true, DisplayOrder = 14)]
        bool PositiveFeedbackCheckEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue settings", Key = "NegativeFeedbackCheckEnabled", DisplayName = "Flag negative survey responses", Default = true, DisplayOrder = 15)]
        bool NegativeFeedbackCheckEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue settings", Key = "JobIssueFlagPotentialMSFW", DisplayName = "Flag potential MSFW", Default = false, DisplayOrder = 16)]
        bool JobIssueFlagPotentialMSFW { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue settings", Key = "ExpiredAlienCertificationEnabled", DisplayName = "Job seekers with expired alien certification registration date", Description = "Flag job seekers identified for having an expired alien certification registration date.", Default = true, DisplayOrder = 17)]
        bool ExpiredAlienCertificationEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue settings", Key = "PotentialMSFWQuestionsEnabled", DisplayName = "Enable Potential MSFW Questions", Default = false, DisplayOrder = 17)]
        bool PotentialMSFWQuestionsEnabled { get; }

        #endregion

        #region Candidate issue thresholds

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "NotLoggingInDaysThreshold", DisplayName = "Not logging in threshold (days)", Default = 2, DisplayOrder = 1)]
        int NotLoggingInDaysThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "NotClickingLeadsCountThreshold", DisplayName = "Not clicking leads count threshold", Default = 5, DisplayOrder = 2)]
        int NotClickingLeadsCountThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "NotClickingLeadsDaysThreshold", DisplayName = "Not clicking leads days threshold", Default = 14, DisplayOrder = 3)]
        int NotClickingLeadsDaysThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "RejectingJobOffersCountThreshold", DisplayName = "Rejecting job offers count threshold", Default = 2, DisplayOrder = 4)]
        int RejectingJobOffersCountThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "RejectingJobOffersDaysThreshold", DisplayName = "Rejecting job offers days threshold", Default = 14, DisplayOrder = 5)]
        int RejectingJobOffersDaysThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "NotReportingForInterviewCountThreshold", DisplayName = "Not reporting for interview count threshold", Default = 2, DisplayOrder = 6)]
        int NotReportingForInterviewCountThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "NotReportingForInterviewDaysThreshold", DisplayName = "Not reporting for interview days threshold", Default = 14, DisplayOrder = 7)]
        int NotReportingForInterviewDaysThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "NotRespondingToEmployerInvitesCountThreshold", DisplayName = "Not responding to #BUSINESS#:LOWER invites count threshold", Default = 2, DisplayOrder = 8)]
        int NotRespondingToEmployerInvitesCountThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "NotRespondingToEmployerInvitesDaysThreshold", DisplayName = "Not responding to #BUSINESS#:LOWER invites days threshold", Default = 14, DisplayOrder = 9)]
        int NotRespondingToEmployerInvitesDaysThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "NotRespondingToEmployerInvitesGracePeriodDays", DisplayName = "Grace period for not responding to #BUSINESS#:LOWER invites (days)", Default = 2, DisplayOrder = 10)]
        int NotRespondingToEmployerInvitesGracePeriodDays { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "NotRespondingToReferralSuggestionsCountThreshold", DisplayName = "Not responding to referral suggestions count threshold", Default = 2, DisplayOrder = 11)]
        int NotRespondingToReferralSuggestionsCountThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "NotRespondingToReferralSuggestionsDaysThreshold", DisplayName = "Not responding to referral suggestions days threshold", Default = 14, DisplayOrder = 12)]
        int NotRespondingToReferralSuggestionsDaysThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "SelfReferringToUnqualifiedJobsCountThreshold", DisplayName = "Self referring to unqualified jobs count threshold", Default = 2, DisplayOrder = 13)]
        int SelfReferringToUnqualifiedJobsCountThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "SelfReferringToUnqualifiedJobsDaysThreshold", DisplayName = "Self referring to unqualified jobs days threshold", Default = 14, DisplayOrder = 14)]
        int SelfReferringToUnqualifiedJobsDaysThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "SelfReferringToUnqualifiedMinStarThreshold", DisplayName = "Self referring to unqualified jobs minimum star threshold", Default = 3, DisplayOrder = 15)]
        int SelfReferringToUnqualifiedMinStarThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "SelfReferringToUnqualifiedMaxStarThreshold", DisplayName = "Self referring to unqualified jobs maximum star threshold", Default = 2, DisplayOrder = 16)]
        int SelfReferringToUnqualifiedMaxStarThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "ShowingLowQualityMatchesCountThreshold", DisplayName = "Show low quality matches count threshold", Default = 2, DisplayOrder = 17)]
        int ShowingLowQualityMatchesCountThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "ShowingLowQualityMatchesDaysThreshold", DisplayName = "Show low quality matches days threshold", Default = 14, DisplayOrder = 18)]
        int ShowingLowQualityMatchesDaysThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "ShowingLowQualityMatchesMinStarThreshold", DisplayName = "Show low quality matches minimum star threshold", Default = 2, DisplayOrder = 19)]
        int ShowingLowQualityMatchesMinStarThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "PostingLowQualityResumeWordCountThreshold", DisplayName = "Low quality resume word count threshold", Default = 100, DisplayOrder = 20)]
        int PostingLowQualityResumeWordCountThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "PostingLowQualityResumeSkillCountThreshold", DisplayName = "Low quality resume skill count threshold", Default = 5, DisplayOrder = 21)]
        int PostingLowQualityResumeSkillCountThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "NotSearchingJobsDaysThreshold", DisplayName = "Not searching jobs days threshold", Default = 7, DisplayOrder = 22)]
        int NotSearchingJobsDaysThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "NotSearchingJobsCountThreshold", DisplayName = "Not searching jobs count threshold", Default = 2, DisplayOrder = 23)]
        int NotSearchingJobsCountThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "JobSeekerInactivityEmailLimit", DisplayName = "Limit for jobseeker inactivity emails", Default = 60, DisplayOrder = 24)]
        int JobSeekerInactivityEmailLimit { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Candidate issue thresholds", Key = "JobSeekerInactivityEmailGrace", DisplayName = "Grace period for jobseeker inactivity emails", Default = 14, DisplayOrder = 25)]
        int JobSeekerInactivityEmailGrace { get; }

        #endregion

        #region Assist job order issue settings

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issues", Key = "JobIssueMinimumMatchCountThreshold", DisplayName = "Minimum number of matches threshold", Default = 5, Description = "Threshold for minimum number of matches for the job order", DisplayOrder = 1)]
        int JobIssueMinimumMatchCountThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issues", Key = "JobIssueMatchMinimumStarsThreshold", DisplayName = "Minimum stars for match threshold", Default = 4, Description = "Minimum stars threshold for matches to the job order", DisplayOrder = 2)]
        int JobIssueMatchMinimumStarsThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issues", Key = "JobIssueMatchDaysThreshold", DisplayName = "Low matches days threshold", Default = 5, Description = "Threshold in days for flagging low number of matches to job order", DisplayOrder = 3)]
        int JobIssueMatchDaysThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issues", Key = "JobIssueMinimumReferralsCountThreshold", DisplayName = "Minimum referrals count threshold", Default = 5, Description = "Threshold for for flagging low referrals to job order", DisplayOrder = 4)]
        int JobIssueMinimumReferralsCountThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issues", Key = "JobIssueReferralCountDaysThreshold", DisplayName = "Referral count time period threshold (days)", Default = 10, Description = "Time period (in days) over which low referral count job issue is calculated", DisplayOrder = 5)]
        int JobIssueReferralCountDaysThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issues", Key = "JobIssueNotClickingReferralsDaysThreshold", DisplayName = "Not clicking referrals time period threshold (days)", Default = 3, Description = "Time period allowed before not clicking on referrals is flagged as an issue", DisplayOrder = 6)]
        int JobIssueNotClickingReferralsDaysThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issues", Key = "JobIssueSearchesWithoutInviteThreshold", DisplayName = "Searches without invites threshold", Default = 3, Description = "Threshold for searches run without inviting jobseekers before it is flagged as an issue", DisplayOrder = 7)]
        int JobIssueSearchesWithoutInviteThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issues", Key = "JobIssueSearchesWithoutInviteDaysThreshold", DisplayName = "Searching without inviting time period threshold (days)", Default = 5, Description = "Time period over which searching for candidates but not inviting any issue  is calculated (in days)", DisplayOrder = 8)]
        int JobIssueSearchesWithoutInviteDaysThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issues", Key = "JobIssuePostClosedEarlyDaysThreshold", DisplayName = "Early job closing threshold (days)", Default = 3, Description = "The number of days early a job posting can be closed before it is flagged as an issue", DisplayOrder = 9)]
        int JobIssuePostClosedEarlyDaysThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issues", Key = "JobIssueHiredDaysElapsedThreshold", DisplayName = "Elapsed days threshold for post hiring follow up", Default = 14, Description = "The number of days to elapse before a follow up is carried out after hiring", DisplayOrder = 10)]
        int JobIssueHiredDaysElapsedThreshold { get; }

        #endregion

        #region Assist job order issue switches

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issue switches", Key = "JobIssueFlagLowMinimumStarMatches", DisplayName = "Flag low minimum star matches", Default = true, Description = "Toggle whether low number of minimum star matches is flagged", DisplayOrder = 1)]
        bool JobIssueFlagLowMinimumStarMatches { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issue switches", Key = "JobIssueFlagLowReferrals", DisplayName = "Flag low referrals", Default = true, Description = "Toggle whether low referral count is flagged", DisplayOrder = 2)]
        bool JobIssueFlagLowReferrals { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issue switches", Key = "JobIssueFlagNotClickingReferrals", DisplayName = "Flag not clicking referrals", Default = true, Description = "Toggle whether not clicking referrals is flagged", DisplayOrder = 3)]
        bool JobIssueFlagNotClickingReferrals { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issue switches", Key = "JobIssueFlagSearchingNotInviting", DisplayName = "Flag searching without inviting", Default = true, Description = "Toggle whether searching without inviting is flagged", DisplayOrder = 4)]
        bool JobIssueFlagSearchingNotInviting { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issue switches", Key = "JobIssueFlagClosingDateRefreshed", DisplayName = "Flag refreshing of closing date", Default = true, Description = "Toggle whether refreshing the closing date is flagged", DisplayOrder = 5)]
        bool JobIssueFlagClosingDateRefreshed { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issue switches", Key = "JobIssueFlagJobClosedEarly", DisplayName = "Flag closing job early", Default = true, Description = "Toggle whether closing a job early is flagged", DisplayOrder = 6)]
        bool JobIssueFlagJobClosedEarly { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issue switches", Key = "JobIssueFlagJobAfterHiring", DisplayName = "Flag job after hiring", Default = true, Description = "Toggle whether job is flagged a certain time period after hiring", DisplayOrder = 7)]
        bool JobIssueFlagJobAfterHiring { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issue switches", Key = "JobIssueFlagNegativeFeedback", DisplayName = "Flag negative feedback", Default = true, Description = "Toggle whether someone leaving negative feedback is flagged", DisplayOrder = 8)]
        bool JobIssueFlagNegativeFeedback { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Order issue switches", Key = "JobIssueFlagPositiveFeedback", DisplayName = "Flag positive feedback", Default = true, Description = "Toggle whether someone leaving positive feedback is flagged", DisplayOrder = 9)]
        bool JobIssueFlagPositiveFeedback { get; }

        #endregion

        #region Assist job development settings

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Development settings", Key = "JobDevelopmentSpideredJobSearchMatchesMinimumStars", DisplayName = "Spidered jobs search matching minimum stars", Default = 1, DisplayOrder = 1)]
        int JobDevelopmentSpideredJobSearchMatchesMinimumStars { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Development settings", Key = "JobDevelopmentSpideredJobListNumberOfPostingsRequired", DisplayName = "Spidered jobs list number of postings required", Default = 1, DisplayOrder = 2)]
        int JobDevelopmentSpideredJobListNumberOfPostingsRequired { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Development settings", Key = "JobDevelopmentSpideredJobListMaximumNumberOfEmployers", DisplayName = "Spidered jobs list Max number of employers", Default = 30, DisplayOrder = 3)]
        int JobDevelopmentSpideredJobListMaximumNumberOfEmployers { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Development settings", Key = "JobDevelopmentEmployerPlacementsNoOfDays", DisplayName = "Employer placements number of days", Default = 7, DisplayOrder = 4)]
        int JobDevelopmentEmployerPlacementsNoOfDays { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Development settings", Key = "JobDevelopmentSpideredJobListPostingsToDisplay", DisplayName = "Spidered jobs list postings to display", Default = 25, DisplayOrder = 5)]
        int JobDevelopmentSpideredJobListPostingsToDisplay { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Development settings", Key = "JobDevelopmentSpideredJobListPostingAge", DisplayName = "Spidered jobs list posting age", Default = 30, DisplayOrder = 6)]
        int? JobDevelopmentSpideredJobListPostingAge { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Development settings", Key = "JobDevelopmentMinimumRecentMatchesCount", DisplayName = "Minimum matches to recent matched count", Default = 25, DisplayOrder = 7)]
        int JobDevelopmentMinimumRecentMatchesCount { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Job Development settings", Key = "JobDevelopmentMaximumPostingMatchesCount", DisplayName = "Maximum posting matches to jobseekers count", Default = 25, DisplayOrder = 7)]
        int JobDevelopmentMaximumPostingMatchesCount { get; }

        #endregion

        #region Find Employer settings

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Find Employer settings", Key = "FindEmployerFEINsAllowed", DisplayName = "Maximum number of FEINS allowed in Find Employer Search", Default = 20, DisplayOrder = 1)]
        int FindEmployerFEINsAllowed { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Find Employer settings", Key = "FindEmployerFEINsScrolling", DisplayName = "Number of FEINS before scrolling in Find Employer Search", Default = 3, DisplayOrder = 2)]
        int FindEmployerFEINsScrolling { get; }


        #endregion

        #region Office settings

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Office settings", Key = "EnableOfficeDefaultAdmin", DisplayName = "Enable office default administrator", Default = false, DisplayOrder = 1)]
        bool EnableOfficeDefaultAdmin { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Office settings", Key = Constants.ConfigurationItemKeys.ShowAllOfficesReferralRequestQueue, DisplayName = "Show all offices on referral queue", Default = false, DisplayOrder = 2)]
        bool ShowAllOfficesReferralRequestQueue { get; }

        #endregion

        #region Assist reporting settings
        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist Reporting settings", Key = "HideJSCharacteristicsFromReporting", DisplayName = "Show/Hide Job seeker characteristics panel from Reporting", Default = false, DisplayOrder = 3)]
        bool HideJSCharacteristicsFromReporting { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist Reporting settings", Key = "HideJSCharacteristicsFromMessages", DisplayName = "Show/Hide Job seeker characteristics panel from Job seeker messages", Default = false, DisplayOrder = 4)]
        bool HideJSCharacteristicsFromMessages { get; }

        #endregion

        #region Assist Activities/Services Feature Configurations
        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist activites and services settings", Key = Constants.ConfigurationItemKeys.EnableJSActivityBackdatingDateSelection, DisplayName = "Enable Job Seeker Activity Backdating Date Selection", Default = true)]
        bool EnableJSActivityBackdatingDateSelection { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist activites and services settings", Key = Constants.ConfigurationItemKeys.EnableHiringManagerActivityBackdatingDateSelection, DisplayName = "Enable Hiring Manager Activity Backdating Date Selection", Default = true)]
        bool EnableHiringManagerActivityBackdatingDateSelection { get; }
        #endregion

        #region Assist activities and services settings
        //Job Seeker Configuration
        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = Constants.ConfigurationItemKeys.EnableJobSeekerActivitiesServices, DisplayName = "Enable JobSeeker Activities Services", Default = true)]
        bool EnableJobSeekerActivitiesServices { get; }

        //Employer Configuration
        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = Constants.ConfigurationItemKeys.EnableHiringManagerActivitiesServices, DisplayName = "Enable HiringManager Activities Services", Default = true)]
        bool EnableHiringManagerActivitiesServices { get; }

        #endregion

        #region Assist Activities Backdate Feature Configurations - DEPRECATED
        //[DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = Constants.ConfigurationItemKeys.BackdateEmployerActivityMaxDays, DisplayName = "The maximum number of days an employer activity can be backdated by", Default = 30, DisplayOrder = 21)]
        //int BackdateEmployerActivityMaxDays { get; }

        //[DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "Assist application settings", Key = "EnableEmployerActivityDateSelection", DisplayName = "Enable Employer Activity Date Selection", Default = false, DisplayOrder = 11)]
        //bool EnableEmployerActivityDateSelection { get; } 
        #endregion

        #endregion

        #region CareerExplorerSettings

        #region Application settings

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Application settings", Key = "FocusCareerApplicationName", DisplayName = "Career application name", Default = "Focus/Career", DisplayOrder = 1)]
        string FocusCareerApplicationName { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Application settings", Key = "FocusExplorerApplicationName", DisplayName = "Explorer application name", Default = "Focus/Explorer", DisplayOrder = 2)]
        string FocusExplorerApplicationName { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Application settings", Key = "CareerApplicationPath", DisplayName = "Career application path", Default = @"http://careerdev.burning-glass.com", DisplayOrder = 3)]
        string CareerApplicationPath { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Application settings", Key = "CareerEnabled", DisplayName = "Enable Career", Default = false, DisplayOrder = 4)]
        bool CareerEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Application settings", Key = "CareerRegistrationNotificationEmails", DisplayName = "Career registration notification emails", Default = "NOTSET", DisplayOrder = 5)]
        string CareerRegistrationNotificationEmails { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Application settings", Key = "CareerHideSearchJobsByResume", DisplayName = "Career hide search jobs by resume", Default = false, DisplayOrder = 6)]
        bool CareerHideSearchJobsByResume { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.IsCareerExplorerAnonymousOnly, DisplayName = "Allow anonymous access to certain functions in Career/Explorer for recognised domains", Default = AnonymousAccess.None, DisplayOrder = 7)]
        AnonymousAccess IsCareerExplorerAnonymousOnly { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.IsCareerExplorerAnonymousOnlyDomainNames, DisplayName = "Recognised domains that allow anoymous access to Career/Explorer", Default = false, DisplayOrder = 8)]
        string[] IsCareerExplorerAnonymousOnlyDomainNames { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.EnableCareerPasswordSecurity, DisplayName = "Enable Career Password Security", Default = false, DisplayOrder = 9)]
        bool EnableCareerPasswordSecurity { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.ShowTalentJobsAsHighPriority, DisplayName = "Show Talent Jobs As High Priority", Default = false, DisplayOrder = 10)]
        bool ShowTalentJobsAsHighPriority { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.SetExternalIdOnSpideredPostings, DisplayName = "Check external system when searching for job seekers", Default = false, DisplayOrder = 20)]
        bool SetExternalIdOnSpideredPostings { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Application settings", Key = Constants.ConfigurationItemKeys.AllowSelfReferralOnSpideredPostingsWithNoResume, DisplayName = "Allow jobseekers without a resume to self refer for Spidered Postings", Default = false, DisplayOrder = 21)]
        bool AllowSelfReferralOnSpideredPostingsWithNoResume { get; }

        #endregion

        #region Visual settings

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Visual settings", Key = "ShowBurningGlassLogoInCareerExplorer", DisplayName = "Show burning glass logo in Career Explorer", Default = true, DisplayOrder = 1)]
        bool ShowBurningGlassLogoInCareerExplorer { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Visual settings", Key = "CareerExplorerLightColour", DisplayName = "Career Explorer light colour", Default = "cbd4db", DisplayOrder = 2)]
        string CareerExplorerLightColour { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Visual settings", Key = "CareerExplorerDarkColour", DisplayName = "Career Explorer dark colour", Default = "ececec", DisplayOrder = 3)]
        string CareerExplorerDarkColour { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Visual settings", Key = "ShowExpIndustries", DisplayName = "Show Experience Industries", Default = false, DisplayOrder = 4)]
        bool ShowExpIndustries { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Visual settings", Key = "ShowTargetIndustries", DisplayName = "Show Target Industries", Default = false, DisplayOrder = 5)]
        bool ShowTargetIndustries { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Visual settings", Key = "DisplayPoorResumeAnnouncement", DisplayName = "Display Poor Resume Announcement", Default = false, DisplayOrder = 6)]
        bool DisplayPoorResumeAnnouncement { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Visual settings", Key = "ReadOnlyResumeDOB", DisplayName = "Read-only resume date of birth", Default = false, DisplayOrder = 7)]
        bool ReadOnlyResumeDOB { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Visual settings", Key = "DisplayCareerAccountType", DisplayName = "Display Career Account Type dropdown", Default = false, DisplayOrder = 8)]
        bool DisplayCareerAccountType { get; }

        #endregion

        #region Career Settings

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "MaximumAllowedResumeCount", DisplayName = "Maximum allowed resume count", Default = 5, DisplayOrder = 7)]
        int MaximumAllowedResumeCount { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "MaximumSearchCountForClarify", DisplayName = "Maximum search count for clarify", Default = 20, DisplayOrder = 8)]
        int MaximumSearchCountForClarify { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "MaximumNoDocumentToReturnInSearch", DisplayName = "Max num of docs to return in search", Default = 200, DisplayOrder = 9)]
        int MaximumNoDocumentToReturnInSearch { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "MaximumNoDocumentToReturnInJobAlert", DisplayName = "Max num of docs to return in job alert", Default = 200, DisplayOrder = 10)]
        int MaximumNoDocumentToReturnInJobAlert { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "MinimumScoreForSearch", DisplayName = "Min score for search", Default = 0, DisplayOrder = 11)] //defaults class has nothing set
        int MinimumScoreForSearch { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "MinimumScoreForClarify", DisplayName = "Min score for clarify", Default = 0, DisplayOrder = 12)] //defaults class has nothing set
        int MinimumScoreForClarify { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "ReferralMinStars", DisplayName = "Referral min stars", Default = 0, DisplayOrder = 13)]
        int ReferralMinStars { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "NonCustomerSystemOriginIds", DisplayName = "None customer system origin IDs", Default = new[] { 999 }, DisplayOrder = 14)]
        int[] NonCustomerSystemOriginIds { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = Constants.ConfigurationItemKeys.ReviewApplicationEligibilityOriginIds, DisplayName = "Review application eligibility origin IDs", Default = new long[] { 7, 8, 9 }, DisplayOrder = 15)]
        long[] ReviewApplicationEligibilityOriginIds { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "CacheSizeForPrefixDomain", DisplayName = "Cache size for prefix domain", Default = 100, DisplayOrder = 16)]
        int CacheSizeForPrefixDomain { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "MaskingAddress", DisplayName = "Masking address", Default = "focuscareer@aperture-control.com", DisplayOrder = 18)]
        string MaskingAddress { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "ResetPasswordFormat", DisplayName = "Reset password format", Default = "BGT_HtmlForgotPassword.xsl", DisplayOrder = 19)]
        string ResetPasswordFormat { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "PassiveAuthenticationCode", DisplayName = "Passive authentication code", Default = "NOTSET", DisplayOrder = 20)]
        string PassiveAuthenticationCode { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "Customer", DisplayName = "Customer", Default = "BGT", DisplayOrder = 21)]
        string Customer { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "CustomerCenterCode", DisplayName = "Customer cenre code", Default = "BGT001", DisplayOrder = 22)]
        string CustomerCenterCode { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "CustomerRepresentativeId", DisplayName = "Customer representative ID", Default = "1", DisplayOrder = 23)]
        string CustomerRepresentativeId { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "FilterSkillsInAutomatedSummary", DisplayName = "Filter skills in automated summary", Default = true, DisplayOrder = 24)]
        bool FilterSkillsInAutomatedSummary { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "RemoveSpacesFromAutomatedSummary", DisplayName = "Remove spaces from automated summary", Default = false, DisplayOrder = 25)]
        bool RemoveSpacesFromAutomatedSummary { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "DefaultCountryKey", DisplayName = "Default country key", Default = "Country.US", DisplayOrder = 26)]
        string DefaultCountryKey { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "NearbyStateKeys", DisplayName = "Nearby state keys",
            Default = new[] { "State.IL", "State.IN", "State.KY", "State.MO", "State.OH", "State.TN", "State.VA", "State.WV" }, DisplayOrder = 27)]
        string[] NearbyStateKeys { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "SpecialStateKeys", DisplayName = "Special state keys",
            Default = new[] { "State.AE", "State.AA", "State.AP", "State.AS", "State.FM", "State.MH", "State.MP", "State.ZZ", "State.PW", "State.GU", "State.PR", "State.VI" }, DisplayOrder = 28)]
        string[] SpecialStateKeys { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "Spam_JobOriginId", DisplayName = "Spam job origin ID", Default = "NOTSET", DisplayOrder = 29)]
        string SpamJobOriginId { get; } // Default changed from null.

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "Spam_MailAddress", DisplayName = "Spam mail address", Default = "NOTSET", DisplayOrder = 30)]
        string SpamMailAddress { get; } // Default changed from null.

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "Spam_BccAddress", DisplayName = "Spam bcc address", Default = "NOTSET", DisplayOrder = 31)]
        string SpamBccAddress { get; } // Default changed from null.

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "Closed_JobOriginId", DisplayName = "Closed job origin ID", Default = "NOTSET", DisplayOrder = 32)]
        string ClosedJobOriginId { get; } // Default changed from null.

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "Closed_BccAddress", DisplayName = "Closed bcc address", Default = "NOTSET", DisplayOrder = 33)]
        string ClosedBccAddress { get; } // Default changed from null.

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "DocumentStoreServiceUrl", DisplayName = "Document store service URL", Default = "http://careerdocstoresvcstage.usdev.burninglass.com/DocStoreSvc.asmx", DisplayOrder = 34)]
        string DocumentStoreServiceUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "SchoolType", DisplayName = "School type", Default = SchoolTypes.None, DisplayOrder = 35)]
        SchoolTypes SchoolType { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "SchoolName", DisplayName = "School name", Default = "Demo School", DisplayOrder = 36)]
        string SchoolName { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "BrandName", DisplayName = "Brand name", Default = "NOTSET", DisplayOrder = 37)]
        string BrandName { get; } // Default changed from null.

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "JobPostingFormat", DisplayName = "Job posting style path/format", Default = @"~\Assets\Xslts\JobPosting.xsl", DisplayOrder = 39)]
        string JobPostingStylePath { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "ShowProgramArea", DisplayName = "Show program area", Default = true, DisplayOrder = 40)]
        bool ShowProgramArea { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "JobRequirements", DisplayName = "Job requirements", Default = true, DisplayOrder = 41)]
        bool JobRequirements { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "ShowCampus", DisplayName = "Show campus location", Default = false, DisplayOrder = 42)]
        bool ShowCampus { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "SupportsProspectiveStudents", DisplayName = "Supports prospective students", Default = true, DisplayOrder = 43)]
        bool SupportsProspectiveStudents { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "ShowContactDetails", DisplayName = "Allow job seeker contact details to be shown/hidden", Default = false, DisplayOrder = 44)]
        ShowContactDetails ShowContactDetails { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "HideResumeProfile", DisplayName = "Hide the profile tab in the resume builder", Default = false, DisplayOrder = 45)]
        bool HideResumeProfile { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "HideObjectives", DisplayName = "Hide the objectives add-in in the resume builder", Default = false, DisplayOrder = 46)]
        bool HideObjectives { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "MilitaryServiceMinimumAge", DisplayName = "Minimum age for military service", Default = 17, DisplayOrder = 47)]
        int MilitaryServiceMinimumAge { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = Constants.ConfigurationItemKeys.VietmanWarStartDate, DisplayName = "Start date of Vietnam War", Default = "05 August 1964", DisplayOrder = 48)]
        string VietmanWarStartDate { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = Constants.ConfigurationItemKeys.VietmanWarStartDate, DisplayName = "Start date of Vietnam War", Default = "07 May 1975", DisplayOrder = 49)]
        string VietmanWarEndDate { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = Constants.ConfigurationItemKeys.UseResumeTemplate, DisplayName = "Use resume template", Default = false, DisplayOrder = 50)]
        bool UseResumeTemplate { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = Constants.ConfigurationItemKeys.ResumeGuidedPath, DisplayName = "Resume guided path", Default = false, DisplayOrder = 51)]
        bool ResumeGuidedPath { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = Constants.ConfigurationItemKeys.HideMocCodes, DisplayName = "Hide MOC codes", Default = false, DisplayOrder = 52)]
        bool HideMocCodes { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = Constants.ConfigurationItemKeys.MandatoryJSJobAlerts, DisplayName = "Mandatory Job Seeker job alerts", Default = false, DisplayOrder = 53)]
        bool MandatoryJSJobAlerts { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = Constants.ConfigurationItemKeys.ShowMilitaryStatus, DisplayName = "Show military status panel in Talent Pool search", Default = false, DisplayOrder = 54)]
        bool ShowMilitaryStatus { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = Constants.ConfigurationItemKeys.MaxPreferredSalaryThreshold, DisplayName = "Maximum preferred salary threshold", Default = 350000, DisplayOrder = 55)]
        int MaxPreferredSalaryThreshold { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = Constants.ConfigurationItemKeys.AllowDateBoundMilitaryService, DisplayName = "Allow multiple military histories", Default = false, DisplayOrder = 56)]
        bool AllowDateBoundMilitaryService { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = Constants.ConfigurationItemKeys.EnableJobSearchByJobId, DisplayName = "Enable job search by Job Id", Default = false, DisplayOrder = 57)]
        bool EnableJobSearchByJobId { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = Constants.ConfigurationItemKeys.EnableOptionalAlienRegExpiryDate, DisplayName = "Enable optional alien registration expiry date", Default = false, DisplayOrder = 58)]
        bool EnableOptionalAlienRegExpiryDate { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = Constants.ConfigurationItemKeys.ShowBridgestoOppsInterest, DisplayName = "Show link for Bridges to Opportunities in Career Explorer", Default = false, DisplayOrder = 59)]
        bool ShowBridgestoOppsInterest { get; set; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = Constants.ConfigurationItemKeys.BridgestoOppsInterestActivityExternalId, DisplayName = "Get the activity id to log if Bridges to Opportunities is selected", Default = 0, DisplayOrder = 60)]
        long BridgestoOppsInterestActivityExternalId { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = Constants.ConfigurationItemKeys.MaximumGradePointAverage, DisplayName = "Maximum Grade Point Average allowed", Default = 4, DisplayOrder = 61)]
        int MaximumGradePointAverage { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = Constants.ConfigurationItemKeys.ShowSSNStatusMessage, DisplayName = "Display a permanent message on the CE dashboard about the SSN", Default = false, DisplayOrder = 61)]
        bool ShowSSNStatusMessage { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = Constants.ConfigurationItemKeys.MaxDateBoundMilitaryServicePeriods, DisplayName = "Maximum number of military service periods in resume", Default = 0, DisplayOrder = 62)]
        int MaxDateBoundMilitaryServicePeriods { get; }

        //TO DO ------------------------------------------------------------------------------------------------------
        //[DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career settings", Key = "ResumeFormat", DisplayName = "Resume format", Default = @"~\Assets\Xslts\FocusCareerFormat.xsl", DisplayOrder = 38)]
        string GetCustomXslForResume(string resumeFormat);

        #endregion

        #region Explorer settings

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerApplicationPath", DisplayName = "Explorer application path", Default = @"http://explorerdev.burning-glass.com", DisplayOrder = 42)]
        string ExplorerApplicationPath { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerShowDegreeStudyPlaces", DisplayName = "Explorer show degree study places", Default = true, DisplayOrder = 43)]
        bool ExplorerShowDegreeStudyPlaces { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerShowBookmarkForNonLoggedInUser", DisplayName = "Explorer show bookmark for none logged in user", Default = true, DisplayOrder = 44)]
        bool ExplorerShowBookmarkForNonLoggedInUser { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerShowEmailForNonLoggedInUser", DisplayName = "Explorer show email for none logged in user", Default = true, DisplayOrder = 45)]
        bool ExplorerShowEmailForNonLoggedInUser { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerSectionOrder", DisplayName = "Explorer section order", Default = "Explore|Research|Study|Experience", DisplayOrder = 46)]
        string ExplorerSectionOrder { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerSendEmailMaximumMessageLength", DisplayName = "Explorer send email max msg length", Default = 1000, DisplayOrder = 47)]
        int ExplorerSendEmailMaximumMessageLength { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerSendEmailShowEmailFrom", DisplayName = "Explorer send email show email from", Default = true, DisplayOrder = 48)]
        bool ExplorerShowEmailFromInSendEmail { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerSendEmailFromEmail", DisplayName = "Explorer send email from email", Default = "NOTSET", DisplayOrder = 49)]
        string ExplorerSendEmailFromEmail { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerJobSalaryNationwideDisplayType", DisplayName = "Explorer job salary nationwide display type", Default = "Average", DisplayOrder = 50)]
        string ExplorerJobSalaryNationwideDisplayType { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerJobSalaryComparisonPercent", DisplayName = "Explorer job salary comparison (%)", Default = 2, DisplayOrder = 51)]
        int ExplorerJobSalaryComparisonPercent { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerRegistrationNotificationEmails", DisplayName = "Explorer registration notification emails", Default = "NOTSET", DisplayOrder = 52)]
        string ExplorerRegistrationNotificationEmails { get; } // Default changed from string.Empty.

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerDegreeClientDataTag", DisplayName = "Explorer degree client data tag", Default = "NOTSET", DisplayOrder = 53)]
        string ExplorerDegreeClientDataTag { get; } // Default changed from string.Empty.

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerGoogleTrackingId", DisplayName = "Explorer google tracking ID", Default = "NOTSET", DisplayOrder = 54)]
        string ExplorerGoogleTrackingId { get; } // Default changed from string.Empty.

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerGoogleEventTrackingEnabled", DisplayName = "Enable Explorer google event tracking", Default = false, DisplayOrder = 55)]
        bool ExplorerGoogleEventTrackingEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerHelpEmail", DisplayName = "Explorer help email", Default = "NOTSET", DisplayOrder = 56)]
        string ExplorerHelpEmail { get; } // Default changed from string.Empty.

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerUseDegreeTiering", DisplayName = "Enable Explorer use degree tiering", Default = true, DisplayOrder = 58)]
        bool ExplorerUseDegreeTiering { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "CheckCriminalRecordExclusion", DisplayName = "Enable check criminal record exclusion", Default = false, DisplayOrder = 59)]
        bool CheckCriminalRecordExclusion { get; } // Default changed from Theme == FocusThemes.Workforce.

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "OfficesEnabled", DisplayName = "Enable offices", Default = false, DisplayOrder = 60)]
        bool OfficesEnabled { get; } // Default changed from Theme == FocusThemes.Workforce.

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerOnlyShowJobsBasedOnClientDegrees", DisplayName = "Explorer only show jobs based on client degrees", Default = false, DisplayOrder = 61)]
        bool ExplorerOnlyShowJobsBasedOnClientDegrees { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerDegreeFilteringType", DisplayName = "Explorer degree filtering type", Default = DegreeFilteringType.Ours, DisplayOrder = 62)]
        DegreeFilteringType ExplorerDegreeFilteringType { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "CareerExplorerFeatureEmphasis", DisplayName = "Career Explorer feature emphasis", Default = CareerExplorerFeatureEmphasis.Career, DisplayOrder = 63)]
        CareerExplorerFeatureEmphasis CareerExplorerFeatureEmphasis { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerInDemandDropdownOrder", DisplayName = "Explorer 'In Demand' dropdown list order", Default = "Job|Employer|DegreeCertification|Skill|Internship|CareerMoves|CareerArea", Description = "Sets the order of the Explorer 'In Demand' dropdown list", DisplayOrder = 64)]
        string ExplorerInDemandDropdownOrder { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerModalEmailIcon", DisplayName = "Explorer lightbox - Show email option", Default = true, Description = "Whether to show the email icon in Explorer lightboxes", DisplayOrder = 65)]
        bool ExplorerModalEmailIcon { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "ExplorerModalDownloadIcon", DisplayName = "Explorer lightbox - Show download option", Default = false, Description = "Whether to show the download icon in Explorer lightboxes", DisplayOrder = 66)]
        bool ExplorerModalDownloadIcon { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = Constants.ConfigurationItemKeys.ExplorerReadyToMakeAMoveBubble, DisplayName = "Explorer lightbox - Show 'Ready to make a move' in separate bubble", Default = true, DisplayOrder = 67)]
        bool ExplorerReadyToMakeAMoveBubble { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Explorer settings", Key = "MaximumNumberJobOpenings", DisplayName = "Maximum number of job openings allowed", Default = 200, DisplayOrder = 68)]
        int MaximumNumberJobOpenings { get; }

        #endregion

        #region Career Search Defaults

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career search defaults", Key = "CareerSearchShowAllJobs", DisplayName = "Enable show all jobs in career search", Default = true, DisplayOrder = 1)]
        bool CareerSearchShowAllJobs { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career search defaults", Key = "CareerSearchMinimumStarMatchScore", DisplayName = "Career search min star match score", Default = 3, DisplayOrder = 2)]
        int CareerSearchMinimumStarMatchScore { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career search defaults", Key = "CareerSearchDefaultRadiusId", DisplayName = "Career search default radius ID", Default = 0, DisplayOrder = 3)]
        int CareerSearchDefaultRadiusId { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career search defaults", Key = "CareerSearchRadius", DisplayName = "Career search radius", Default = -1, DisplayOrder = 4)]
        long CareerSearchRadius { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career search defaults", Key = "CareerSearchZipcode", DisplayName = "Career search zipcode", Default = "NOTSET", DisplayOrder = 5)]
        string CareerSearchZipcode { get; } // Default changed from string.Empty.

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career search defaults", Key = "CareerSearchCentrePointType", DisplayName = "Career search centre point type", Default = SearchCentrePointOptions.Zipcode, DisplayOrder = 6)]
        SearchCentrePointOptions CareerSearchCentrePointType { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career search defaults", Key = "CareerSearchResumesSearchable", DisplayName = "Career search resumes searchable", Default = ResumeSearchableOptions.NotApplicable, DisplayOrder = 7)]
        ResumeSearchableOptions CareerSearchResumesSearchable { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career search defaults", Key = "VeteranPriorityServiceEnabled", DisplayName = "Enable/Disable Veteran priority service", Default = true, Description = "Toggle whether veterans are able to view jobs before non-veterans", DisplayOrder = 8)]
        bool VeteranPriorityServiceEnabled { get; set; } //TODO: NEEDS looking at as the dfault in AppSettings is Theme == FocusThemes.Workforce (cant have complex statement in attribute)

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career search defaults", Key = "VeteranPriorityStarMatching", DisplayName = "Veteran priority star matching", Default = 3, DisplayOrder = 9)]
        int VeteranPriorityStarMatching { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career search defaults", Key = "VeteranPriorityExclusiveHours", DisplayName = "Veteran priority exclusive hours", Default = 24, Description = "The number of hours veterans gain exclusivtity to new jobs", DisplayOrder = 10)]
        int VeteranPriorityExclusiveHours { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career search defaults", Key = "WorkAvailabilitySearch", DisplayName = "Work availability search ", Default = false, Description = "Whether it is possible to search on work availability", DisplayOrder = 11)]
        bool WorkAvailabilitySearch { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Career search defaults", Key = Constants.ConfigurationItemKeys.JobSearchPostingDate, DisplayName = "Default posting days on search", Default = 7, DisplayOrder = 12)]
        int JobSearchPostingDate { get; }

        #endregion

        #region Job Disclaimer Defaults

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Job disclaimer defaults", Key = Constants.ConfigurationItemKeys.SearchResultsDisclaimerEnabled, DisplayName = "Display search results disclaimer", Default = true, DisplayOrder = 1)]
        bool SearchResultsDisclaimerEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Job disclaimer defaults", Key = Constants.ConfigurationItemKeys.SearchResultsDisclaimerText, Default = "Job search results include two types of jobs. Jobs posted directly by #BUSINESSES#:LOWER registered with this website, are designated by an icon. Spidered jobs from other #BUSINESSES#:LOWER and job sources, which are not registered or verified by this provider, also are provided for your area. Job seekers should use caution in applying for these jobs, as we cannot guarantee the wages, accuracy of the information, or the validity of the #BUSINESS#:LOWER or job opening.", DisplayOrder = 2)]
        string SearchResultsDisclaimerText { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Job disclaimer defaults", Key = Constants.ConfigurationItemKeys.SpideredJobDisclaimerEnabled, DisplayName = "Display search results disclaimer", Default = true, DisplayOrder = 3)]
        bool SpideredJobDisclaimerEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "Job disclaimer defaults", Key = Constants.ConfigurationItemKeys.SpideredJobDisclaimerText, Default = "When you click the link below you will leave this website. Our organization does not endorse, take responsibility for, and exercise any control over the linked organization or its contents. We cannot verify the destination site for accuracy, accessibility, security, or legal compliance.", DisplayOrder = 4)]
        string SpideredJobDisclaimerText { get; }

        #endregion

        #endregion

        #region ReportingSettings


        [DbConfigSetting(Category = ConfigSettingCategory.Reporting, SubCategory = "General settings", Key = "FocusReportingApplicationName", DisplayName = "Reporting application name", Default = "Focus/Reporting", DisplayOrder = 1)]
        string FocusReportingApplicationName { get; }

        // Caching
        CachingMethod CacheMethod { get; }
        string AppFabricCacheName { get; }

        /// <summary>
        /// Gets the application fabric lead hosts.
        /// </summary>
        /// <value>
        /// The application fabric lead hosts. (string is the server name, int is the port number)
        /// </value>
        List<Tuple<string, int>> AppFabricLeadHosts { get; }

        // Veteran Priority
        //bool VeteranPriorityServiceEnabled { get; }
        //int VeteranPriorityStarMatching { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Reporting, SubCategory = "General settings", Key = "ReportingEnabled", DisplayName = "Enable reporting", Default = false, DisplayOrder = 2)]
        bool ReportingEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "ReportActionDaysLimit", DisplayName = "Report action retention limit (days)", Default = 120, Description = "The number of days any 'actions' (used in total fields) should be kept", DisplayOrder = 3)]
        int ReportActionDaysLimit { get; }

        /// <summary>
        /// Gets the show supply and demand report boolean.
        /// </summary>
        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "Application Settings", Key = "ShowSupplyDemandReport", DisplayName = "Show supply and demand report", Description = "Show supply and demand report", Default = false, DisplayOrder = 71)]
        bool ShowSupplyDemandReport { get; }

        #endregion

        #region SSO

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "General SSO settings", Key = "SSOReturnUrl", DisplayName = "SSO Return URL", Default = "about:blank", DisplayOrder = 1)]
        string SSOReturnUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "General SSO settings", Key = "SSOManageAccountUrl", DisplayName = "SSO Manage Account URL", Default = "NOTSET", DisplayOrder = 2)]
        string SSOManageAccountUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "General SSO settings", Key = "SSOChangeUsernamePassword", DisplayName = "SSO Change Username or Password URL", Default = "NOTSET", DisplayOrder = 3)]
        string SSOChangeUsernamePasswordUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "General SSO settings", Key = "SSOErrorRedirectUrl", DisplayName = "SSO error redirect url", Default = "~/ssoerror?message=#ERROR#", DisplayOrder = 4)]
        string SSOErrorRedirectUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "General SSO settings", Key = "SSOEnableDeactivateJobSeeker", DisplayName = "Allow Assist user to deactivate SSO job seeker", Default = false, DisplayOrder = 5)]
        bool SSOEnableDeactivateJobSeeker { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "General SSO settings", Key = "SSOForceNewUserToRegistration", DisplayName = "Force new user to registration", Default = false, DisplayOrder = 6)]
        bool SSOForceNewUserToRegistration { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "General SSO settings", Key = "SSOEnabledForCareer", DisplayName = "SSO Enabled for Career", Default = false, DisplayOrder = 7)]
        bool SSOEnabledForCareer { get; }

        bool SSOEnabled { get; }


        #region OAuthSettings

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "OAuth settings", Key = "OAuthConsumerKey", DisplayName = "Oauth consumer key", Default = "NOTSET", DisplayOrder = 1)]
        string OAuthConsumerKey { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "OAuth settings", Key = "OAuthConsumerSecret", DisplayName = "Oauth consumer secret", Default = "NOTSET", DisplayOrder = 2)]
        string OAuthConsumerSecret { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "OAuth settings", Key = "OAuthRequestTokenUrl", DisplayName = "Oauth request token URL", Default = "NOTSET", DisplayOrder = 3)]
        string OAuthRequestTokenUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "OAuth settings", Key = "OAuthVerifierUrl", DisplayName = "Oauth verifier URL", Default = "NOTSET", DisplayOrder = 4)]
        string OAuthVerifierUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "OAuth settings", Key = "OAuthRequestAccessTokenUrl", DisplayName = "Oauth request access token URL", Default = "NOTSET", DisplayOrder = 5)]
        string OAuthRequestAccessTokenUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "OAuth settings", Key = "OAuthRequestProfileUrl", DisplayName = "Oauth request profile URL", Default = "NOTSET", DisplayOrder = 6)]
        string OAuthRequestProfileUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "OAuth settings", Key = "OAuthScope", DisplayName = "Oauth scope", Default = "NOTSET", DisplayOrder = 7)]
        string OAuthScope { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "OAuth settings", Key = "OAuthRealm", DisplayName = "Oauth realm", Default = "NOTSET", DisplayOrder = 8)]
        string OAuthRealm { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "OAuth settings", Key = "OAuthProfileMapper", DisplayName = "Oauth profile mapper code", Default = "NOTSET", DisplayOrder = 9)]
        string OAuthProfileMapperCode { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "OAuth settings", Key = "OAuthVersion", DisplayName = "Oauth version", Default = "1.0", DisplayOrder = 12)]
        string OAuthVersion { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "OAuth settings", Key = "OAuthEnabled", DisplayName = "Enable Oauth", Default = false, DisplayOrder = 13)]
        bool OAuthEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "OAuth settings", Key = "OutgoingOAuthSettings", DisplayName = "Outgoing", Default = false, DisplayOrder = 14)]
        List<OAuthSettings> OutgoingOAuthSettings { get; }

        #endregion // OAuth

        #region SamlSettings

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlEnabled", DisplayName = "Enable Saml", Default = false, DisplayOrder = 1)]
        bool SamlEnabled { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlEnabledForCareer", DisplayName = "Enable Saml For Career", Default = false, DisplayOrder = 2)]
        bool SamlEnabledForCareer { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlEnabledForTalent", DisplayName = "Enable Saml For Talent", Default = false, DisplayOrder = 3)]
        bool SamlEnabledForTalent { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlEnabledForAssist", DisplayName = "Enable Saml For Assist", Default = false, DisplayOrder = 4)]
        bool SamlEnabledForAssist { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlIdPArtifactIdProviderUrl", DisplayName = "Saml IdP artifact provider URL", Default = "NOTSET", DisplayOrder = 5)]
        string SamlIdPArtifactIdProviderUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlIdPSingleSignonIdProviderUrl", DisplayName = "Saml ID single sign on ID provider URL", Default = "NOTSET", DisplayOrder = 6)]
        string SamlIdPSingleSignonIdProviderUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlMetadataUrl", DisplayName = "Saml metadata URL", Default = "NOTSET", DisplayOrder = 7)]
        string SamlMetadataUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlSpToIdPBindingUrl", DisplayName = "Saml Sp to IdP binding URL", Default = "NOTSET", DisplayOrder = 8)]
        string SamlSpToIdPBindingUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlIdPToSpBindingUrl", DisplayName = "Saml Idp to Sp binding URL", Default = "NOTSET", DisplayOrder = 9)]
        string SamlIdPToSpBindingUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlSpKeyLocation", DisplayName = "Saml Sp key location", Default = "NOTSET", DisplayOrder = 10)]
        string SamlSpKeyLocation { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlSpKeyPassword", DisplayName = "Saml Sp key password", Default = "NOTSET", DisplayOrder = 11)]
        string SamlSpKeyPassword { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlIdPCertKeyLocation", DisplayName = "Saml IdP cert key location", Default = "NOTSET", DisplayOrder = 12)]
        string SamlIdPCertKeyLocation { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlFirstNameAttributeName", DisplayName = "Saml first name attribute name", Default = "FirstName", DisplayOrder = 13)]
        string SamlFirstNameAttributeName { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlLastNameAttributeName", DisplayName = "Saml last name attribute name", Default = "LastName", DisplayOrder = 14)]
        string SamlLastNameAttributeName { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlScreenNameAttributeName", DisplayName = "Saml screen name attribute name", Default = "ScreenName", DisplayOrder = 15)]
        string SamlScreenNameAttributeName { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlEmailAddressAttributeName", DisplayName = "Saml email address attribute name", Default = "EmailAddress", DisplayOrder = 16)]
        string SamlEmailAddressAttributeName { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlCreateRolesAttributeName", DisplayName = "Saml create roles attribute name", Default = "CreateRoles", DisplayOrder = 17)]
        string SamlCreateRolesAttributeName { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlUpdateRolesAttributeName", DisplayName = "Saml update roles attribute name", Default = "UpdateRoles", DisplayOrder = 18)]
        string SamlUpdateRolesAttributeName { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlQueryStringBindingVar", DisplayName = "Saml query string binding", Default = "NOTSET", DisplayOrder = 19)]
        string SamlQueryStringBindingVar { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlArtifactIdentityUrl", DisplayName = "Saml artifact identity URL", Default = "NOTSET", DisplayOrder = 20)]
        string SamlArtifactIdentityUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlSpKeyName", DisplayName = "Saml Service provider Key name", Default = "NOTSET", DisplayOrder = 21)]
        string SamlSpKeyName { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlIdPCertKeyName", DisplayName = "Key name of Certificate containing SAML Identity provider", Default = "NOTSET", DisplayOrder = 22)]
        string SamlIdPCertKeyName { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = Constants.ConfigurationItemKeys.SamlPrintXMLtoDebug, DisplayName = "Print SAML XML to log", Default = false, DisplayOrder = 23)]
        bool SamlPrintXMLtoDebug { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SAMLFederatedLogoutReturnUrl", DisplayName = "SAML federated logout return url", Default = "", DisplayOrder = 24)]
        string SamlFederatedLogoutReturnUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Saml settings", Key = "SamlDefaultAssistRoles", DisplayName = "Default roles to give new Assist user", Default = "AssistEmployersReadOnly;AssistJobSeekersReadOnly;AssistJobSeekersAdministrator", DisplayOrder = 25)]
        string SamlDefaultAssistRoles { get; }

        #region Saml secondary configuration - allows another Identity provider to be configured as anew or a failover

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Secondary Saml settings", Key = "SamlSecIdPSingleSignonIdProviderUrl", DisplayName = "Saml ID single sign on ID provider URL", Default = "NOTSET", DisplayOrder = 26)]
        string SamlSecIdPSingleSignonIdProviderUrl { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Secondary Saml settings", Key = "SamlSecIdPCertKeyName", DisplayName = "Key name of Certificate containing Secondary SAML Identity provider", Default = "NOTSET", DisplayOrder = 27)]
        string SamlSecIdPCertKeyName { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Secondary Saml settings", Key = "SamlLogoutRedirect", DisplayName = "URL to redirect during SAML logout", Default = "about:blank", DisplayOrder = 28)]
        string SamlLogoutRedirect { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.SSO, SubCategory = "Secondary Saml settings", Key = "SamlSecSSOExternalClientTag", DisplayName = "Saml secondary External client tag prefix", Default = "", DisplayOrder = 29)]
        string SamlSecExternalClientTag { get; }
        #endregion

        #endregion // Saml

        #endregion //SSO

        #region SQL Jobs

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "SQL Jobs", Key = Constants.ConfigurationItemKeys.SessionRetentionDays, DisplayName = "Number of days to retain Data.Core.Session records", Default = 1, DisplayOrder = 1)]
        int SessionRetentionDays { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "SQL Jobs", Key = Constants.ConfigurationItemKeys.PageStateRentionDays, DisplayName = "Number of days to retain Data.Core.PageState records", Default = 1, DisplayOrder = 2)]
        int PageStateRentionDays { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "SQL Jobs", Key = Constants.ConfigurationItemKeys.LogEventRetentionDays, DisplayName = "Number of days to retain Log.LogEvent records", Default = 30, DisplayOrder = 3)]
        int LogEventRetentionDays { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "SQL Jobs", Key = Constants.ConfigurationItemKeys.ProfileEventRetentionDays, DisplayName = "Number of days to retain Log.ProfileEvent records", Default = 30, DisplayOrder = 4)]
        int ProfileEventRetentionDays { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "SQL Jobs", Key = Constants.ConfigurationItemKeys.MessageRetentionDays, DisplayName = "Number of days to retain Messaging.Message, and associated, records", Default = 30, DisplayOrder = 5)]
        int MessageRetentionDays { get; }

        #endregion

        #region CompTIA Specific

        [DbConfigSetting(Category = ConfigSettingCategory.Assist, SubCategory = "CompTIA Specific", Key = Constants.ConfigurationItemKeys.IsComptiaSpecific, DisplayName = "Is CompTIA Specific", Default = false, DisplayOrder = 1)]
        bool IsComptiaSpecific { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.General, SubCategory = "EnableSsnFeatureGroup", Key = Constants.ConfigurationItemKeys.EnableSsnFeatureGroup, DisplayName = "Enable Ssn Feature Group", Default = true, DisplayOrder = 2)]
        bool EnableSsnFeatureGroup { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "EnableDobFeatureGroup", Key = Constants.ConfigurationItemKeys.EnableDobFeatureGroup, DisplayName = "Enable Dob Feature Group", Default = true, DisplayOrder = 3)]
        bool EnableDobFeatureGroup { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "EnableNCRCFeatureGroup", Key = Constants.ConfigurationItemKeys.EnableNCRCFeatureGroup, DisplayName = "Enable NCRC Feature Group", Default = true, DisplayOrder = 4)]
        bool EnableNCRCFeatureGroup { get; }

        [DbConfigSetting(Category = ConfigSettingCategory.CareerExplorer, SubCategory = "ShowReasonForLeavingFeature", Key = Constants.ConfigurationItemKeys.ShowReasonForLeavingFeature, DisplayName = "Show Reason For Leaving Feature", Default = true, DisplayOrder = 5)]
        bool ShowReasonForLeavingFeature { get; }
        #endregion

        ConfigSettingInfo[] GetConfigSettings();

        //Integration
        IntegrationClient IntegrationClient { get; set; }
        string IntegrationSettings { get; }
        ActionTypes[] IntegrationLoggableActions { get; }
        ActionTypes[] IntegrationOneADayActions { get; }
        string IntegrationNodesAndAttributesToObfuscate { get; }
        IntegrationLoggingLevel IntegrationLoggingLevel { get; }

        // Encryption
        bool EncryptionEnabled { get; }
        string EncryptionKey { get; }
        string EncryptionVector { get; }

        string SharedEncryptionKey { get; set; }
        string SharedEncryptionVector { get; set; }

        //Custom Header Footer
        bool UseCustomHeaderHtml { get; }
        bool UseCustomFooterHtml { get; }
        bool HideTalentAssistAccountSettings { get; }
        bool HideTalentAssistSignOut { get; }
        bool HideCareerSignIn { get; }
        bool HideCareerRegister { get; }
        bool HideCareerMyAccount { get; }
        bool HideCareerSignOut { get; }
        string CustomHeaderHtml { get; }
        string CustomFooterHtml { get; }
        bool HideLegalNoticeLink { get; }
        bool ShowCareerTutorialLink { get; }
    }
}
