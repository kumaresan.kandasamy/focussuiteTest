﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Focus.Core.Settings.OAuth
{
	public class OAuthSettings
	{
		public OAuthClient Client { get; set; }
		public string EndPoint { get; set; }
		public string ConsumerKey { get; set; }
		public string ConsumerSecret { get; set; }
		public string Token { get; set; }
		public string TokenSecret { get; set; }
	}
}
