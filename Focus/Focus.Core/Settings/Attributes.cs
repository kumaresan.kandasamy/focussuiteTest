﻿using System;

namespace Focus.Core.Settings
{
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
	public abstract class ConfigSettingAttribute : Attribute
	{
		public ConfigSettingCategory Category { get; set; }
		public string SubCategory { get; set; }
		public string Key { get; set; }
		public string DisplayName { get; set; }
		public string Description { get; set; }
		public int DisplayOrder { get; set; }
	}

	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
	public class WebConfigSettingAttribute : ConfigSettingAttribute
	{ }

	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
	public class DbConfigSettingAttribute : ConfigSettingAttribute
	{
		public object Default { get; set; }
	}

	// Custom attribute to add to Enum type
	public class StringValue : Attribute
	{
		private readonly string _value;

		public StringValue(string value)
		{
			_value = value;
		}

		public string Value
		{
			get { return _value; }
		}

	}

	public static class EnumExtensions
	{
		public static string GetStringValue(this Enum value)
		{
			string output = null;
			var type = value.GetType();

			//Check first in our cached results...
			//Look for our 'StringValueAttribute' in the field's custom attributes

			var fi = type.GetField(value.ToString());
			var attrs = fi.GetCustomAttributes(typeof(StringValue), false) as StringValue[];

			if (attrs.Length > 0)
				output = attrs[0].Value;

			return output;
		}
	}

	public enum ConfigSettingCategory
	{
		[StringValue("General")]
		General = 10,
		[StringValue("Talent")]
		Talent = 20,
		[StringValue("Assist")]
		Assist = 30,
		[StringValue("Career Explorer")]
		CareerExplorer = 40,
		[StringValue("Reporting")]
		Reporting = 50,
		[StringValue("SSO")]
		SSO = 60,
    [StringValue("LaborInsight")]
    LaborInsight = 70
	}
}

