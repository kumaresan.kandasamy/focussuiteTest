﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace Focus.Core
{
  [Serializable]
  public class NameValueCollectionSerializable
  {
    public Dictionary<string, List<string>> NameValueData { get; set; }

    public NameValueCollectionSerializable()
    {
      NameValueData = new Dictionary<string, List<string>>();
    }

    public int Count
    {
      get { return NameValueData.Count; }
    }

    public void Add(string key, string value)
    {
      if (NameValueData.ContainsKey(key))
        NameValueData[key].Add(value);
      else
        NameValueData[key] = new List<string> { value };
    }

    public string Get(int index)
    {
      return string.Join(",", NameValueData.Values.ElementAt(index));
    }

    public string Get(string key)
    {
      return NameValueData.ContainsKey(key) ? string.Join(",", NameValueData[key]) : null;
    }

    public string[] GetValues(int index)
    {
      return NameValueData.Values.ElementAt(index).ToArray();
    }

    public string[] GetValues(string key)
    {
      return NameValueData.ContainsKey(key) ? NameValueData[key].ToArray() : null;
    }
  }
}
