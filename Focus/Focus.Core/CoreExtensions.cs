﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;
using Framework.Exceptions;

#endregion

namespace Focus.Core
{
	public static class CoreExtensions
	{
		private static IEqualityComparer<string> _ignoreSpaceComparer;

		/// <summary>
		/// Returns a string comparer that ignores spaces and is case-insensitive
		/// </summary>
		public static IEqualityComparer<string> IgnoreSpaceComparer
		{
			get { return _ignoreSpaceComparer ?? (_ignoreSpaceComparer = new IgnoreSpaceComparer()); }
		}

		/// <summary>
		/// Serializes the specified the object into xml.
		/// </summary>
		/// <param name="theObject">The object.</param>
		/// <returns></returns>
		public static string Serialize(this object theObject)
		{
			using (var sw = new StringWriter())
			{
				var serializer = new XmlSerializer(theObject.GetType());
				serializer.Serialize(sw, theObject);
				sw.Close();
				return sw.ToString();
			}
		}

		/// <summary>
		/// Deserializes the specified serialized data into an object.
		/// </summary>
		/// <param name="serializedData">The serialized data.</param>
		/// <param name="theType">The type.</param>
		/// <returns></returns>
		public static object Deserialize(this string serializedData, Type theType)
		{
			var serializer = new XmlSerializer(theType);
			return serializer.Deserialize(new StringReader(serializedData));
		}

		/// <summary>
		/// Serializes the specified the object using the DataContract
		/// </summary>
		/// <param name="theObject">The object.</param>
		/// <returns>A string representing the serialised object</returns>
		public static string DataContractSerializeRunTime(this object theObject)
		{
			using (var sw = new StringWriter())
			{
				var xmlWriter = new XmlTextWriter(sw);

				var ser = new DataContractSerializer(theObject.GetType());
				ser.WriteObject(xmlWriter, theObject);

				xmlWriter.Close();
				sw.Close();

				return sw.ToString();
			}
		}

		/// <summary>
		/// Serializes the specified the object using the DataContract
		/// </summary>
		/// <param name="theObject">The object.</param>
		/// <returns>A string representing the serialised object</returns>
		public static string DataContractSerialize<T>(this T theObject)
		{
			using (var sw = new StringWriter())
			{
				var xmlWriter = new XmlTextWriter(sw);

				var ser = new DataContractSerializer(typeof(T));
				ser.WriteObject(xmlWriter, theObject);

				xmlWriter.Close();
				sw.Close();

				return sw.ToString();
			}
		}

		/// <summary>
		/// Deserializes the specified serialized data into an object.
		/// </summary>
		/// <param name="serializedData">The serialized data.</param>
		/// <returns>Teh deserialised object</returns>
		public static T DataContractDeserialize<T>(this string serializedData)
		{
			using (var stringReader = new StringReader(serializedData))
			{
				var xmlReader = new XmlTextReader(stringReader);

				// Deserialize the data and read it from the instance.
				var ser = new DataContractSerializer(typeof(T));
				var deserializedObject = (T)ser.ReadObject(xmlReader, true);

				xmlReader.Close();
				stringReader.Close();

				return deserializedObject;
			}
		}

		/// <summary>
		/// Converts to a sentence.
		/// </summary>
		/// <param name="strings">The strings.</param>
		/// <param name="finalSeperator">The final seperator.</param>
		/// <returns></returns>
		public static string ToSentence(this List<string> strings, string finalSeperator)
		{
			if (strings.Count == 0) return string.Empty;
			if (strings.Count == 1) return strings[0];

			var resultBuilder = new StringBuilder();

			foreach (var item in strings)
			{
				resultBuilder.AppendFormat("{0}, ", item);
			}

			var result = resultBuilder.ToString();

			// Remove last comma
			if (result.Length > 2) result = result.Substring(0, result.Length - 2);

			// Replace last comma with "and" or equivalent
			var index = result.LastIndexOf(",");
			if (index > 0)
			{
				result = result.Remove(index, 1);
				result = result.Insert(index, string.Format(" {0}", finalSeperator));
			}

			return result;
		}

		/// <summary>
		/// Converts a string of text to title case.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <returns></returns>
		public static string ToTitleCase(this string text)
		{
			if (string.IsNullOrEmpty(text))
				return text;

			var strings = text.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

			for (var i = 0; i < strings.Length; i++)
				if (strings[i].Length > 0)
					strings[i] = string.Format("{0}{1}", strings[i].Substring(0, 1).ToUpper(), strings[i].Substring(1));

			return string.Join(" ", strings);
		}

		/// <summary>
		/// Converts a string of text to name case.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <returns></returns>
		public static string ToNameCase(this string text)
		{
			if (string.IsNullOrEmpty(text))
				return "";

			var match = Regex.Match(text.ToLower(), @"(mc|mac|fitz|[a-z]*['’])(.+)");
			var outputString = "";

			if (match.Success)
				for (var i = 1; i < match.Groups.Count; i++)
					outputString += match.Groups[i].Value.ToTitleCase();
			else
				outputString = text;

			return outputString;
		}

		/// <summary>
		/// Converts a string of text to sentence case (i.e first letter capitalised, all others lower case).
		/// </summary>
		/// <param name="text">The text to convert.</param>
		/// <returns>The text in sentence case.</returns>
		public static string ToSentenceCase(this string text)
		{
			if (string.IsNullOrEmpty(text))
				return text;

			return text.Length == 1 ? text.ToUpper() : string.Concat(text.Substring(0, 1).ToUpper(), text.Substring(1).ToLower());
		}

		/// <summary>
		/// Takes a word in Pascal case, and splits it by capital letters
		/// </summary>
		/// <param name="text">The text in Pascal case</param>
		/// <returns>A string of words, split by capital letters</returns>
		public static string SplitByPascalCase(this string text)
		{
			return Regex.Replace(text, "([A-Z])", " $1").Trim();
		}

		/// <summary>
		/// Checks to see if the element exists for the provided xpath.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="xpath">The xpath.</param>
		/// <returns></returns>
		public static bool ElementExists(this XElement parentNode, string xpath)
		{
			return parentNode.XPathSelectElement(xpath) != null;
		}

		/// <summary>
		/// Gets the ErrorType for an exception.
		/// </summary>
		/// <param name="exception">The exception.</param>
		/// <returns>The error type</returns>
		public static ErrorTypes GetErrorType(this Exception exception)
		{
			if (exception is SecurityException)
				return ErrorTypes.NotAllowed;

			if (exception is HttpRequestValidationException)
				return ErrorTypes.InvalidHttpRequest;

			var ex = exception as ServiceCallException;
			if (ex != null)
				return (ErrorTypes)ex.ErrorCode;

			return ErrorTypes.Unknown;
		}

		#region PagedList extensions

		public static PagedList<TResult> GetPagedList<T, TResult>( this IQueryable<T> source, Expression<Func<T, TResult>> selector, int pageIndex, int pageSize )
		{
			var count = source.Count();
			var query = source.Select(selector);
			return new PagedList<TResult>( query, count, pageIndex, pageSize );
		}

		#endregion
	}

	/// <summary>
	/// Implementation of an IEqualityComparer that compares strings (case-insensitive) but ignores spaces
	/// </summary>
	public class IgnoreSpaceComparer : IEqualityComparer<string>
	{
		public bool Equals(string x, string y)
		{
			return x.Replace(" ", "").Equals(y.Replace(" ", ""), StringComparison.OrdinalIgnoreCase);
		}

		public int GetHashCode(string obj)
		{
			return obj.Replace(" ", "").ToLowerInvariant().GetHashCode();
		}
	}
}
