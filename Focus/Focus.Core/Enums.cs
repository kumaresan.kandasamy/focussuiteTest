﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Xml.Serialization;
using Framework.Core.Attributes;

#endregion

namespace Focus.Core
{

    #region ControlDisplayType

    public enum ControlDisplayType
    {
        Mandatory,
        Optional,
        Hidden
    }

    #endregion

    #region FocusModules

    public enum FocusModules
    {
        General = 0,
        Talent = 1,
        Assist = 2,
        Explorer = 3,
        Career = 4,
        CareerExplorer = 5,
        MessageBus = 6,
        IntegrationInbound = 7,
        IntegrationOutbound = 8,
        ServicesHost = 9,
        MigrationService = 10
    }

    #endregion

    #region FocusThemes

    public enum FocusThemes
    {
        Workforce,
        Education
    }

    #endregion

    #region EntityTypes

    public enum EntityTypes
    {
        Job = 0,
        BusinessUnit = 1,
        User = 2,
        Employee = 3,
        Resume = 4,
        Posting = 5,
        JobSeeker = 6,
        EmployerRepresentative = 7,
        Staff = 8,
        Employer = 9,
        Application = 10
    }

    #endregion

    #region ErrorTypes

    /// <summary>
    /// Enumeration of error types.
    /// </summary>
    public enum ErrorTypes
    {
        // Localisation test - dont localise this
        Ok = 0,

        // General Errors
        Unknown = 1,
        NotAllowed = 2,
        UnknownClientTag = 3,
        InvalidSessionId = 4,
        LogInRequired = 5,
        ServiceCallFailed = 6,
        CorrelationFailed = 7,
        NotLicensedToUseProduct = 8,
        LensNotAvailable = 9,
        InvalidAccessToken = 10,
        IncorrectVersionNumber = 11,
        [EnumLocalisationDefault("Criteria required")]
        CriteriaRequired = 12,
        InvalidHttpRequest = 13,
        [EnumLocalisationDefault("Argument null: {0}")]
        ArgumentNull = 14,
        [EnumLocalisationDefault("Argument invalid: {0}")]
        ArgumentInvalid = 15,
        [EnumLocalisationDefault("Another user has recently updated this record. Please exit the record and return to it to see the changes made.")]
        LockVersionOutOfDate = 16,

        // Account Errors
        InvalidUserNameOrPassword = 100,
        UnableToChangePassword = 101,
        UserNameAlreadyExists = 102,
        UnableToProcessForgottenPassword = 103,
        MustSupplyUserNameOrEmailAddress = 104,
        UserNotFound = 105,
        UserNameIsNull = 106,
        UserNotEnabled = 107,
        UserEmailAddressNotFound = 108,
        UnableToChangeUserName = 109,
        InvalidPasswordResetValidationKey = 110,
        UnableToValidatePasswordResetValidationKey = 111,
        UnableToResetPassword = 112,
        InvalidUserTypeForModule = 113,
        UserMissingExternalId = 114,
        UserRoleDoesNotExist = 115,
        UnableToUpdateScreenNameAndEmailAddress = 116,
        UnableToChangeSecurityQuestion = 117,
        UnableToChangeProgramOfStudy = 118,
        UnableToChangeEnrollmentStatus = 119,
        UnableToChangeEmailAddress = 120,
        EmailAddressAlreadyExists = 121,
        UnableToGetCareerData = 122,
        UnableToValidateEmail = 123,
        RegistrationPinNotFound = 124,
        [EnumLocalisationDefault("Employer not approved")]
        EmployerNotApproved = 125,
        UnableToChangeManagerFlag = 126,
        [EnumLocalisationDefault("User already exists")]
        UserAlreadyExists = 127,
        RegistrationFieldRequired = 128,
        UnableToLocatePhoneRecord = 129,
        InvalidDocumentType = 130,
        InvalidData = 131,
        InvalidEmail = 132,
        InvalidTitle = 133,
        InvalidPhoneNumber = 134,
        RequiredDataNotFound = 135,
        BulkRegistrationValidationFailed = 136,
        UnableToChangeCampus = 137,
        UnableToChangeSsn = 138,
        SsnAlreadyExists = 139,
        CurrentSsnDoesNotMatch = 140,
        [EnumLocalisationDefault("The current password entered is incorrect. Please check and try again")]
        CurrentPasswordDoesNotMatch = 141,
        [EnumLocalisationDefault("Employer on hold")]
        EmployerOnHold = 142,
        [EnumLocalisationDefault("Unable to validate password reset validation key as no key provided")]
        UserValidationKeyNotProvided = 143,
        [EnumLocalisationDefault("Unable to validate password reset validation key as no user found for supplied key")]
        UserValidationKeyNotFound = 144,
        [EnumLocalisationDefault("Unable to validate password reset validation key as it has already been used")]
        UserValidationKeyAlreadyUsed = 145,
        [EnumLocalisationDefault("Unable to validate password reset validation key as has expired")]
        UserValidationKeyExpired = 146,
        UnableToAuthenticateExternally = 147,
        EnableDisableNotSpecified = 148,
        AccountDisabledReasonNotSpecified = 149,
        UserIdNotSupplied = 150,
        [EnumLocalisationDefault("A approval type was not set")]
        ApprovalTypeNotSet = 151,
        [EnumLocalisationDefault("The current username does not match the currently logged in user. Please check and try again")]
        CurrentUsernameDoesNotMatch = 152,

        // Candidate Errors
        CandidateNotFound = 200,
        JobSeekerListNotSpecified = 201,
        JobSeekerListCandidateNotSpecified = 202,
        IntegrationAssignJobSeekerToAdmin = 203,
        JobSeekerListNotFound = 204,
        CandidateReferralNotFound = 205,
        CandidateApplicationNotFound = 206,
        NoCandidateIdProvided = 207,
        UnableToUpdateMigrationData = 208,

        // Employer Errors
        EmployerNotFound = 300,
        EmployerFienNotFound = 301,
        InvalidEmployer = 302,
        SaveEmployerFailed = 303,

        // Employee Errors
        EmployeeAlreadyExists = 400,
        EmployeeNotFound = 401,
        InvalidEmployee = 402,
        EmployerIdsNotSpecified = 403,
        [EnumLocalisationDefault("Account denied")]
        EmployeeIsDenied = 404,
        [EnumLocalisationDefault("Account on-hold")]
        EmployeeOnHold = 405,

        // ContactDetails
        PhoneNumberMissing = 500,
        PhoneNumberNotFound = 501,

        // BusinessUnit
        [EnumLocalisationDefault("Business unit not found")]
        BusinessUnitNotFound = 600,
        BusinessUnitCannotBeDeletedWithJobAssociation = 601,
        BusinessUnitAddressNotFound = 602,
        [EnumLocalisationDefault("Invalid business unit: {0}")]
        InvalidBusinessUnit = 603,

        // EmployerAddress
        EmployerAddressMissing = 700,

        // Authentication
        InvalidPassword = 800,
        EmailAlreadyInUse = 801,
        UserBlocked = 802,

        // BusinessUnitDescription
        BusinessUnitDescriptionUnitMissing = 900,
        BusinessUnitDescriptionCannotBeDeletedWithJobAssociation = 901,
        BusinessUnitDescriptionDescriptionRequired = 902,
        BusinessUnitDescriptionIdRequired = 903,
        BusinessUnitDescriptionNotFound = 904,

        // Job
        InvalidJob = 1000,
        InvalidStatusUpdate = 1001,
        JobNotFound = 1002,
        JobIdNotSpecified = 1003,

        // Search
        SavedSearchIdNotProvided = 1100,
        SavedSearchNotFound = 1101,
        UnableToRetrieveSearchFromStateStore = 1102,

        // Posting
        RedWordsFound = 1200,
        ProfanityListNotFound = 1201,

        // Person
        PersonDetailsNotSupplied = 1301,
        PersonNotFound = 1302,
        PersonEmailAddressMissing = 1303,
        PersonOfficeMissing = 1304,
        PersonAddressNotFound = 1305,
        PersonIdNotSupplied = 1306,
        InvalidUserType = 1307,

        // Business Unit Logo
        BusinessUnitLogoCannotBeDeletedWithJobAssociation = 1400,

        // Integration 
        IntegrationSearchCandidateFailure = 1500,
        IntegrationSearchOnetFailure = 1501,
        IntegrationSearchClarifyWordsFailure = 1502,
        IntegrationSearchConvertBinaryDataFailure = 1503,
        IntegrationCandidateAccessCandidateFailure = 1504,
        IntegrationCandidateSendCandidateEmailFailure = 1505,
        IntegrationCandidateGetResumeFailure = 1506,
        IntegrationCandidateGetSystemDefaultsFailure = 1507,
        IntegrationCandidateUpdateSystemDefaultsFailure = 1508,
        IntegrationCandidateJobSeekerFailure = 1509,
        IntegrationCandidateGetJobUrlFailure = 1510,
        IntegrationCandidateCreateHomepageAlertFailure = 1511,
        IntegrationCandidateCreateCandidateFailure = 1512,
        IntegrationClientAssignEmployerActivityFailure = 1513,
        IntegrationClientAssignJobSeekerActivityFailure = 1514,
        IntegrationClientAssignEmployerToStaffFailure = 1515,
        IntegrationReferJobSeekerForJobFailure = 1516,

        // Messaging
        ModuleNotProvided = 1600,

        // Single Sign On
        UnableToCreateSingleSignIn = 1700,
        UnableToValidateSingleSignIn = 1701,
        UnableToCreateSingleSignInInvalidAccount = 1702,

        // Notes and reminders
        NoteReminderNotFound = 1800,
        ReminderNotValid = 1801,
        EntityTypeNotSupported = 1802,

        // Email Templates
        EmailTemplateNotFound = 1900,
        EmailTemplateNotConfigured = 1901,

        // Posting 
        PostingNotFound = 2000,
        LensPostingIdNotProvided = 2001,
        [EnumLocalisationDefault("The posting is no longer active")]
        PostingNotActive = 2002,
        PostingIdNotProvided = 2003,
        [EnumLocalisationDefault("The job is no longer active")]
        JobNotActive = 2004,

        // Resume
        ResumeNotFound = 2100,
        ResumeDetailsNotProvided = 2101,
        ResumeIdNotProvided = 2102,
        ResumeIncomplete = 2103,
        ResumeNotSaved = 2104,

        // Application
        ApplicationNotFound = 2200,
        ApplicationIdNotProvided = 2201,
        ApplicationAlreadyMade = 2202,
        ApplicationNotEligible = 2203,

        // RegistrationPin
        UnableToValidatePin = 2300,

        //Job Order Dashboard UI
        NoJobSelected = 2400,
        MultipleJobsSelected = 2401,
        SingleJobSelected = 2402,
        SubActionNoneSelected = 2403,
        SubActionMultipleSelected = 2404,
        SubActionStatus = 2405,
        SubActionActivity = 2406,
        SubActionStaff = 2407,
        JobClosedMoreThan30DaysAgo = 2408,

        // Job seeker dashboard UI
        MultipleCandidatesSelected = 2500,
        NoCandidatesSelected = 2501,
        NoStudyProgramSelected = 2502,
        JobSeekerNotInOffice = 2503,

        // Activity
        ActivityNotFound = 2600,

        // Manage Offices
        NoOfficeSelected = 2700,
        DefaultOfficeNotFound = 2701,
        OfficeNotFound = 2702,
        DuplicateOfficeExternalId = 2703,
        OfficeInactive = 2704,
        OfficeHasStaffMembers = 2705,
        OfficeHasJobSeekers = 2706,
        OfficeHasJobOrders = 2707,
        OfficeHasEmployers = 2708,
        OfficeHasAssignedZips = 2709,
        OfficeIsAssigned = 2710,
        OfficeIsNotActive = 2711,
        OfficeIsNotInactive = 2712,

        // Encryption
        EncryptedValueNotSupplied = 2800,
        DecryptedValueNotFound = 2801,
        EncryptionConfigsNotDefined = 2802,

        // Reporting
        ValidationFailed = 2900,
        FromDateNotSupplied = 2901,
        ToDateNotSupplied = 2902,
        EmailsNotSupplied = 2903,
        FromDateAfterToDate = 2904,

        // Referral Status Reasons
        ReferralStatusReasonsOtherReasonTextNotSpecified = 3000,
        ReferralStatusReasonsOtherReasonTextNotValidWithSpecifiedReasonId = 3001,
        ReferralStatusReasonsNotSpecified = 3002,

        CreateHiringManagerHomepageAlertsFailure = 3100,
        CreateStaffHomepageAlertsFailure = 3101,
        CreateBusinessUnitHomepageAlertsFailure = 3102,

        // Documents
        FileNotSpecified = 3200,
        TitleNotSpecified = 3201,
        DescriptionTooLong = 3202,
        CategoryNotSpecified = 3203,
        GroupNotSpecified = 3204,
        ModuleNotSpecified = 3205,
        ValidationErrorsOccurred = 3206,
        DocumentIdNotSpecified = 3207,
        DocumentNotFound = 3208,
        DocumentCriteriaNull = 3209,
    }

    #endregion

    #region ActionTypes

    /// <summary>
    /// Enumeration of action types.
    /// </summary>
    public enum ActionTypes
    {
        // No Action
        NoAction,

        // Accounts
        LogIn,
        LogOut,
        RegisterAccount,
        RegisterOAuthAccount,
        RequestPasswordReset,
        ChangePassword,
        ValidateEmployer,
        ProcessForgottenPassword,
        FailedPasswordReset,
        CreateUser,
        UpdateUser,
        DeleteUser,
        ChangeUserName,
        RemovedUserFromRole,
        AddedUserToRole,
        DeactivateAccount,
        ActivateAccount,
        InactivateAccount,
        ValidatePasswordResetValidationKey,
        ResetPassword,
        UpdateScreenNameAndEmailAddress,
        ChangeSecurityQuestion,
        ChangeResume,
        UpdateUserAlert,
        ChangeMigratedStatus,
        ChangeRegulationsConsentStatus,
        ChangeProgramOfStudy,
        ChangeEnrollmentStatus,
        ChangeCampus,
        ChangePhoneNumber,
        ChangeAccountType,
        CreateRegistrationPin,
        ResendPin,
        UpdateManagerFlag,
        CreatePersonOfficeMapper,
        BlockUser,
        UnblockUser,

        // Candidate
        ToggleCandidateFlag,
        ApproveCandidateReferral,
        ApproveCandidateReferralFailed,
        DenyCandidateReferral,
        HoldCandidateReferral,
        AccessCandidateAccount,
        AssignCandidateToStaff,
        LookUpActivityToCandidate,
        AssignActivityToCandidate,
        AddCandidateToList,
        CreateCandidateAccount,
        AddJobSeekersToList,
        AssignJobSeekerToAdmin,
        SaveJobSeeker,
        RemovedJobSeekerFromList,
        DeletedJobSeekerList,
        CreateCandidateHomepageAlerts,
        CreateHiringManagerHomepageAlerts,
        CreateStaffHomepageAlerts,
        CreateBusinessUnitHomepageAlerts,
        SendCandidateEmails,
        SendContactRequest,
        InviteJobSeekerToApply,
        InviteJobSeekerToApplyThroughTalent,
        SavePersonNote,
        CreateCandidateApplication,
        UpdateApplicationStatusToRecommended,
        UpdateApplicationStatusToFailedToShow,
        UpdateApplicationStatusToHired,
        UpdateApplicationStatusToInterviewScheduled,
        UpdateApplicationStatusToInterviewed,
        UpdateApplicationStatusToNewApplicant,
        UpdateApplicationStatusToNotApplicable,
        UpdateApplicationStatusToOfferMade,
        UpdateApplicationStatusToNotHired,
        UpdateApplicationStatusToUnderConsideration,
        UpdateApplicationStatusToDidNotApply,
        UpdateApplicationStatusToInterviewDenied,
        UpdateApplicationStatusToRefusedOffer,
        UpdateApplicationStatusToSelfReferred,
        UpdateApplicationStatusToFailedToReportToJob,
        UpdateApplicationStatusToFailedToRespondToInvitation,
        UpdateApplicationStatusToFoundJobFromOtherSource,
        UpdateApplicationStatusToJobAlreadyFilled,
        UpdateApplicationStatusToNotQualified,
        UpdateApplicationStatusToNotYetPlaced,
        UpdateApplicationStatusToRefusedReferral,
        SaveApplication,
        FindJobsForSeeker,
        UpdateCandidateMigrationData,
        AssignJobSeekerToStaff,
        UpdateAssignedOffice,
        UpdateJobSeekerAssignedStaffMember,
        WaivedRequirements,
        JobSeekerEmailed,
        SaveJobSeekerSurvey,
        AssignJobSeekerToMyself,
        SaveUiClaimantStatus,
        UpdatePersonNoteEmployer,
        ViewedJobSeekerContactInfo,
        AutoApprovedReferralBypass,
        BridgesToOpportunity,
        UnsubscribeEmail,
        LookupJobForJobSeeker, // this is different to FindJobsForSeeker as it is called from different places and uses different methods, if this action needed to be logged in future then we would already have a separate action in place

        // UI Actions
        RegisterHowToApply,
        RegisterHowToApplySpidered,
        ShowNotWhatYoureLookingFor,
        ViewJobDetails,
        ExternalReferral,
        // See FVN-3139
        StaffReferralDeprecated,
        StaffAssistedLMI,

        // Configuration
        CreateCodeItem,
        UpdateCodeItem,
        UpdateCandidateSystemDefaults,
        UpdateApplicationImage,
        UpdateConfigurationItem,

        // Employer 
        SaveBusinessUnitLogo,
        SaveBusinessUnit,
        BusinessUnitCannotBeDeletedWithJobAssociation,
        DeleteBusinessUnit,
        BusinessUnitDescriptionCannotBeDeletedWithJobAssociation,
        DeleteBusinessUnitLogo,
        AssignEmployerToStaff,
        ApproveEmployerReferral,
        DenyEmployerReferral,
        HoldEmployerReferral,
        BlockAllEmployerEmployeesAccount,
        UnblockAllEmployerEmployeesAccount,
        AccessEmployerAccount,
        EditCompanyAddress,
        EditHiringManager,
        SaveBusinessUnitDescription,
        DeleteBusinessUnitDescription,
        SaveEmployerDetails,
        ViewEmployerProfile,
        AddPersonOffice,
        DeletePersonOffice,
        SaveOffice,
        UpdateEmployerAssignedOffice,
        DeselectedOfficeAsDefault,
        SelectedOfficeAsDefault,
        AddEmployerOffice,
        DeleteEmployerOffice,
        UpdatedOfficeAssignedZips,
        MarkAsPreferredEmployer,
        UnmarkAsPreferredEmployer,
        UpdateFederalEmployerIdentificationNumber,
        SaveFederalEmployerIdentificationNumberChange,
        UpdateBusinessUnitEmployer,
        ArchiveEmployer,
        CreateEmployer,

        // Employee
        AccessEmployeeAccount,
        EmailEmployee,
        AssignActivityToEmployee,
        ApproveEmployeeReferral,
        DenyEmployeeReferral,
        HoldEmployeeReferral,
        BlockEmployeeAccount,
        UnblockEmployeeAccount,
        ChangeJobStatus,
        MatchJob,
        AddNote,
        AssignBusinessUnitToEmployee,
        DefaultBusinessUnitChanged,
        AssignEmployeeToStaff,
        ChangeJobSeekerSsn,
        UpdateEmployeeEmployer,

        // Job
        CreateJob,
        RefreshJob,
        CloseJob,
        HoldJob,
        DuplicateJob,
        ReactivateJob,
        EditJob,
        DeleteJob,
        SaveJob,
        SaveJobWizardStep,
        PostJob,
        SaveJobLocations,
        SaveJobCertificates,
        SaveJobLanguages,
        SaveJobLicences,
        SaveJobSpecialRequirements,
        SaveJobDrivingLicenceEndorsements,
        SaveJobProgramsOfStudy,
        SaveJobEducationInternshipSkills,
        ApprovePostingReferral,
        DenyPostingReferral,
        EditPostingReferral,
        SaveJobAddress,
        SkipPostJobProfanityCheck,
        RejectJob,
        ReferJob,
        HoldPostJobUntilEmployeeApproval,
        ProcessExpiredJobsStarted,
        ProcessExpiredJobsCompleted,
        ProcessExpiringJobsStarted,
        ProcessExpiringJobsCompleted,
        ProcessEmailAlertsStarted,
        ProcessEmailAlertsCompleted,
        ProcessQueuedInvitesToApplyStarted,
        ProcessQueuedInvitesToApplyCompleted,
        HiringFromTaxCreditProgramNotificationSent,
        UpdateJobAssignedOffice,
        UpdateJobAssignedStaffMember,
        AssignJobOrderToStaff,
        AddJobOffice,
        DeleteJobOffice,
        UpdateCriminalBackgroundExclusionRequired,
        MarkJobIssuesResolved,
        ViewJobInsights,
        UpdateJobEmployer,
        UpdateCreditCheckRequired,
        AutoResolvedIssue,
        PostJobToLens,
        UnregisterJobFromLens,

        // Talent Pool
        EmailCandidate,
        EmailCandidates,
        SaveSearch,
        SearchCandidates,

        // JobSeeker Actions
        SelfReferral,
        ReferralRequest,
        ReapplyReferralRequest,
        EmailResume,
        DownloadResume,
        SaveJobAlert,
        UpdateCandidateStatus,
        MarkCandidateIssuesResolved,
        JobSearch,
        JobSearchHighGrowth,
        RequestFollowUp,
        EditStudentAlumniAccount,
        HighScoreMatches,
        BlockUnblockJobSeeker,
        ViewCareerExplorationTools,
        OpenAccountInAsset,
        RunManualJobSearch,
        DeactivateJobSeeker,
        InactivateReactivateJobSeeker,

        // SavedSearch
        SavePostingSavedSearch,
        SaveCandidateSavedSearch,
        ProcessSearchAlertsStarted,
        ProcessSearchAlertsCompleted,
        DeleteSavedSearch,

        // Messages
        DismissMessage,
        ExpireMessage,
        CreateHomepageAlert,
        SaveMessageText,
        DeleteSavedMessage,
        SaveEmailTemplate,
        EmailFeedback,

        // SingleSignOn
        CreateSingleSignOn,
        ValidateSingleSignOn,

        // Notes and Reminders
        SaveNote,
        SaveReminder,
        SendReminder,
        SendRemindersProcessStarted,
        SendReminderProcessCompleted,

        // Posting
        ClosePosting,
        AddViewedPosting,
        AddExcludedPosting,
        EditPostingViewedCount,
        ViewedPostingFromSearch,

        // Service Bus
        ProcessServiceBusStarted,
        ProcessServiceBusCompleted,

        // Resume
        DeleteResume,
        DeleteResumeDocument,
        SaveResume,
        CreateNewResume,
        SaveResumeDocument,
        SetDefaultResume,
        ViewResume,
        RegisterDefaultResume,
        CompleteResume,
        EditDefaultCompletedResume,
        SearchableResume,
        CompleteNonDefaultResume,
        ResumePreparationAssistance,

        // Recently Placed Matches
        IgnoreRecentlyPlacedMatch,
        AllowRecentlyPlacedMatch,

        // Person to Posting Matches
        IgnorePersonPostingMatch,
        AllowPersonPostingMatch,

        // Offices
        EditOffice,
        CreateOffice,
        DeleteOffice,
        ListStaffMembers,
        SetAsDefault,
        DeactivateOffice,
        ReactivateOffice,
        ExternalStaffReferral,
        LensJobSearch,

        //Updating Referral Status
        UpdateReferralStatusToAutoOnHold,
        UpdateReferralStatusToAutoDenied
    }

    #endregion

    #region LookupTypes

    /// <summary>
    /// Enumeration of lookup types.
    /// </summary>
    public enum LookupTypes
    {
        Unknown,
        Titles,
        States,
        Countries,
        SatisfactionLevels,
        PoolResultSortBys,
        WorkShifts,
        JobFamily,
        Languages,
        Frequencies,
        EmploymentStatuses,
        JobTypes,
        JobStatuses,
        EmployeeActivities,
        JobSeekerActivities,
        ApprovalRequiredReasons,
        Counties,
        FeedbackIssues,
        DrivingLicenceClasses,
        DrivingLicenceEndorsements,
        WorkTypes,
        WorkWeeks,
        OwnershipTypes,
        SecurityQuestions,
        GeneralIssues,
        StaffIssues,
        JobSeekerIssues,
        EmployerIssues,
        CareerExplorationIssues,
        EmployerActivity,
        EmployerActivitySubActivity,
        JobSeekerActivity,
        JobSeekerActivitySubActivity,
        AccountTypes,

        #region Career

        Radiuses,
        PostingAges,
        RequiredEducationLevels,
        PhysicalAbilities,
        UnclassifiedJobs,
        AdditionalResumeSections,
        ResumeFormats,
        Durations,
        EthnicHeritages,
        Races,
        DisabilityCategories,
        MilitaryBranchesOfService,
        MilitaryDischargeTypes,
        MilitaryRanks,
        StateMetropolitanStatisticalAreas,
        Degrees,
        Majors,
        Skills,
        EmergingSectors,
        SpecialGenericTitles,
        CellphoneProviders,
        EmergingSectorClusters,

        #endregion

        NCRCLevel,
        Suffixes,
        MSFWQuestions,
        EmployerDenialReasons,
        JobSeekerDenialReasons,
        EmployerOnHoldReasons,
        JobDenialReasons,
        HiringManagerDenialReasons,
        HiringManagerOnHoldReasons,
        LanguageProficiencies,
        DocumentCategories,
        DocumentGroups
    }

    #endregion

    #region LoginType

    public enum LoginType
    {
        Standard,
        SSO,
        IntegrationClient
    }

    #endregion

    #region InformationTypes

    /// <summary>
    /// Enumeration of information types e.g. Personal Email Address.
    /// </summary>
    public enum InformationTypes
    {
        Personal,
        Work
    }

    #endregion

    #region ApprovalStatuses

    /// <summary>
    /// Enumeration of approval statuses.
    /// </summary>
    public enum ApprovalStatuses
    {
        None = 0,
        [EnumLocalisationDefault("Pending")]
        WaitingApproval = 1,
        [EnumLocalisationDefault("Approved")]
        Approved = 2,
        [EnumLocalisationDefault("Denied")]
        Rejected = 3,
        [EnumLocalisationDefault("On-Hold")]
        OnHold = 4,
        [EnumLocalisationDefault("Reconsider")]
        Reconsider = 5
    }

    [Flags]
    public enum ApprovalStatusReason
    {
        Default = 0,
        CensorshipFailed = 1,
        BusinessUnitCensorshipFailed = 2,
        BusinessUnitDescriptionCensorshipFailed = 4
    }

    #endregion

    #region DeviceTypes

    public enum DeviceTypes
    {
        Windows = 0,
        Apple = 1,
        Android = 2,
        WindowsPhone = 3
    }

    #endregion

    #region JobStatuses

    /// <summary>
    /// Enumeration of job statuses
    /// </summary>
    public enum JobStatuses
    {
        [EnumLocalisationDefault("Draft")]
        Draft,
        [EnumLocalisationDefault("Active")]
        Active,
        [EnumLocalisationDefault("On hold")]
        OnHold,
        [EnumLocalisationDefault("Closed")]
        Closed,
        [EnumLocalisationDefault("Awaiting employer approval")]
        AwaitingEmployerApproval
    }

    #endregion

    #region JobTypes

    [Flags]
    public enum JobTypes
    {
        None = 0,
        Job = 1,
        InternshipPaid = 2,
        InternshipUnpaid = 4,
        All = Job | InternshipPaid | InternshipUnpaid
    }

    #endregion

    #region JobListFilters

    /// <summary>
    /// Enumeration of job list filter types
    /// </summary>
    public enum JobListFilter
    {
        None,
        FlaggedJobs,
        MyOfficesFlaggedJobs,
        NewJobs,
        CourtOrderedAffirmativeActionJobs,
        FederalContractorJobs,
        ForeignLaborJobs,
        ForeignLaborJobsAll,
        ForeignLaborJobsH2A,
        ForeignLaborJobsH2B,
        ForeignLaborJobsOther,
        FullServiceJobs,
        JobsAndInternships,
        JobOrders,
        Internships,
        CommissionOnly,
        SalaryAndCommission,
        HomeBased,
        NoFixedLocation,
        CreditCheck
    }

    #endregion

    #region OfficeFilter

    public enum OfficeListFilter
    {
        None,
        Active,
        Inactive
    }

    #endregion

    #region SurveyTypes

    /// <summary>
    /// Enumeration of survey types
    /// </summary>
    public enum SurveyTypes
    {
        JobRefreshSurvey,
        JobCloseSurvey
    }

    #endregion

    #region KeywordContexts

    public enum KeywordContexts
    {
        FullResume,
        JobTitle,
        EmployerName,
        Education
    }

    #endregion

    #region KeywordScopes

    public enum KeywordScopes
    {
        EntireWorkHistory,
        LastJob,
        LastTwoJobs,
        LastThreeJobs,
        //LastYear,
        //LastThreeYears,
        //LastFiveYears
    }

    #endregion

    #region EducationLevels

    [Flags]
    public enum EducationLevels
    {
        None = 0,
        [EnumLocalisationDefault("High school diploma")]
        HighSchoolDiploma = 1,
        [EnumLocalisationDefault("Associate’s or vocational degree")]
        AssociatesDegree = 2,
        [EnumLocalisationDefault("Bachelor's degree")]
        BachelorsDegree = 4,
        [EnumLocalisationDefault("Graduate degree")]
        GraduateDegree = 8,
        [EnumLocalisationDefault("Doctorate degree")]
        DoctorateDegree = 16,
        [EnumLocalisationDefault("No diploma")]
        NoDiploma = 32,
        [EnumLocalisationDefault("High school diploma or equivalent")]
        HighSchoolDiplomaOrEquivalent = 64,
        [EnumLocalisationDefault("Some college, no degree")]
        SomeCollegeNoDegree = 128,
        [EnumLocalisationDefault("Master’s degree")]
        MastersDegree = 256,
        All = 511
    }

    #endregion

    #region SchoolStatus

    public enum SchoolStatus
    {
        [EnumLocalisationDefault("N/A")]
        NA = 0,
        [EnumLocalisationDefault("In School, H.S. or less")]
        In_School_HS_OR_less = 1,
        [EnumLocalisationDefault("In School, Alternative School")]
        In_School_Alternative_School = 2,
        [EnumLocalisationDefault("In school, Post H.S.")]
        In_School_Post_HS = 3,
        [EnumLocalisationDefault("Not attending school, H.S. Drop out")]
        Not_Attending_School_OR_HS_Dropout = 4,
        [EnumLocalisationDefault("Not attending school, H.S. Graduate")]
        Not_Attending_School_HS_Graduate = 5,
        [EnumLocalisationDefault("Prospective student")]
        Prospective = 6,
        [EnumLocalisationDefault("First year student")]
        FirstYear = 7,
        [EnumLocalisationDefault("Sophomore or above")]
        SophomoreOrAbove = 8,
        [EnumLocalisationDefault("Non-credit/Other")]
        NonCreditOther = 9,
        [EnumLocalisationDefault("Alumni")]
        Alumni = 10,
        [EnumLocalisationDefault("Sophomore")]
        Sophomore = 11,
        [EnumLocalisationDefault("Junior")]
        Junior = 12,
        [EnumLocalisationDefault("Senior")]
        Senior = 13,
        [EnumLocalisationDefault("Graduate student")]
        Graduate = 14
    }

    #endregion

    #region WorkHistory - Reason For Leaving
    public enum ReasonForLeaving
    {
        [EnumLocalisationDefault("None")]
        None = 0,
        [EnumLocalisationDefault("Business closure or mass layoff")]
        ClosureLayoff = 2,
        [EnumLocalisationDefault("Discharged, fired, or terminated")]
        Fired = 6,
        [EnumLocalisationDefault("Laid off; lack of work")]
        LaidOff = 7,
        [EnumLocalisationDefault("Quit, resigned")]
        Quit = 9,
        [EnumLocalisationDefault("Retired")]
        Retired = 10,
        [EnumLocalisationDefault("Still employed")]
        StillEmployed = 11,
        [EnumLocalisationDefault("Other")]
        Other = 13
    }
    #endregion

    #region MinimumAgeReason

    public enum MinimumAgeReason
    {
        AlcoholTobacco = 0,
        ChildLaborLaws = 1,
        CommercialDriversLicense = 2,
        InsuranceBondingRequirements = 3,
        Other = 4
    }

    #endregion

    #region DistanceUnits

    public enum DistanceUnits
    {
        Miles,
        Kilometres
    }

    #endregion

    #region Weekdays

    [Flags]
    public enum DaysOfWeek
    {
        None = 0,
        Monday = 1,
        Tuesday = 2,
        Wednesday = 4,
        Thursday = 8,
        Friday = 16,
        Saturday = 32,
        Sunday = 64,
        Weekdays = 31,
        Weekends = 96,
        All = 127
    }

    #endregion

    #region EmailAlertFrequencies

    public enum EmailAlertFrequencies
    {
        Daily,
        Weekly
    }

    #endregion

    #region EmailFormats

    public enum EmailFormats
    {
        HTML,
        TextOnly
    }

    #endregion

    #region EmailTemplateTypes

    public enum EmailTemplateTypes
    {
        AcknowledgeApplication = 0,
        RejectApplication = 1,
        InvitationToApplyForJobThroughFocusCareer = 2,
        InvitationToApplyThroughFocusCareer = 3,
        CustomMessage = 4,
        DenyCandidateReferral = 5,
        DenyEmployerAccountReferral = 6,
        DenyPostingReferral = 7,
        EditPostingReferralConfirmation = 8,
        ApproveCandidateReferral = 9,
        ApprovePostingReferral = 10,
        ApproveEmployerAccountReferral = 11,
        TalentCandidateSearchAlert = 12,
        ExpiringJob = 13,
        JobReminder = 14,
        AssistCandidateSearchAlert = 15,
        HiringFromTaxCreditProgramNotification = 16,
        BusinessUnitReminder = 17,
        EmployeeReminder = 18,
        HoldEmployerAccountReferral = 19,
        JobSeekerReminder = 20,
        ApprovalQueueAlert = 21,
        CareerJobSearchAlert = 22,
        CareerPasswordReset = 23,
        CareerJobSeekerInactivity = 24,
        ContactEmployerAboutCandidate = 25,
        CareerAccountRegistration = 26,
        AssistAccountCreation = 27,
        JobPostingOfInterest = 28,
        VeteranPriorityPosting = 29,
        JobScreeningRequired = 30,
        TalentPostingApplicationtViaEmail = 31,
        EmployerNotificationPendingJobExpiration = 32,
        EmployerNotificationJobExpiration = 33,
        TalentPasswordReset = 34,
        ImportUserPasswordReset = 35,
        AutoDeniedReferralRequest = 36,
        AutoApprovedJobSeekerReferral = 37,
        JobSeekerNoticeOfOnHoldJob = 38,
        PreScreeningServiceRequest = 39,
        TalentEmployerActivityReport = 40,
        StudentActivityReport = 41,
        RecommendJobSeeker = 42,
        SendMessageJobSeeker = 43,
        SendMessageEmployer = 44,
        HoldCandidateReferralRequest = 45,
        PendingApprovePostingRequest = 46,
        NewApplicantNotification = 47,
        ExpiredAlienRegistration = 48,
        AutoDeniedExpiredPosting = 49,
        StaffReferralNotification = 50
    }

    #endregion

    #region SenderEmailType

    public enum SenderEmailTypes
    {
        SystemDefualt,
        StaffUser,
        ClientSpecific,
    };

    #endregion

    #region EmployerDescriptionPostingPositions

    /// <summary>
    /// Enumeration of employer description posting positions
    /// </summary>
    public enum EmployerDescriptionPostingPositions
    {
        BelowJobPosting,
        AboveJobPosting,
        DoNotShow
    }

    #endregion

    #region ContentRating

    public enum ContentRatings
    {
        Acceptable,
        GreyListed,
        BlackListed
    }

    #endregion

    #region LogicalOperators

    public enum LogicalOperators
    {
        None,
        And,
        Or
    }

    #endregion

    #region JobLocationTypes

    public enum JobLocationTypes
    {
        MainSite,
        OtherLocation,
        MultipleLocations,
        NoFixedLocation
    }

    #endregion

    #region Certificate & License Types

    public enum CertifcateLicenseTypes
    {
        Advanced = 1,
        Common = 2,
        ProductEquipment = 3,
        Skill = 4,
        Speciality = 5
    }

    #endregion

    #region WorkOpportunitiesTaxCreditCategories

    [Flags]
    public enum WorkOpportunitiesTaxCreditCategories
    {
        None = 0,
        TemporaryAssistanceToNeedyFamilyRecipients = 1,
        DesignatedCommunityResidents = 2,
        SNAPRecipients = 4,
        VocationalRehabilitation = 8,
        ExFelons = 16,
        SummerYouth = 32,
        Veterans = 64,
        SupplementSecurityIncomeRecipients = 128,
        LongTermFamilyAssistanceRecipient = 256,
        All = 511
    }

    #endregion

    #region JobPostingFlags

    [Flags]
    public enum JobPostingFlags
    {
        None = 0,
        GreenJob = 1,
        ITEmployer = 2,
        HealthcareEmployer = 4,
        BiotechEmployer = 8,
        TechnologyEmployer = 16,
        AdvancedManufacturingEmployer = 32,
        TransportationEmployer = 64,
        FinancialServicesEmployer = 128,
        Aerospace = 256,
        BusinessServices = 512,
        Consulting = 1024,
        Energy = 2048,
        HealthInformatics = 4096,
        ResearchandDevelopment = 8192,
        SMART = 16384,
        All = 32767
    }

    #endregion

    #region  LeaveBenefits

    [Flags]
    public enum LeaveBenefits
    {
        None = 0,
        PaidHolidays = 1,
        Vacation = 2,
        LeaveSharing = 4,
        Sick = 8,
        Medical = 16,
        All = 31
    }

    #endregion

    #region  RetirementBenefits

    [Flags]
    public enum RetirementBenefits
    {
        None = 0,
        PensionPlan = 1,
        Four01K = 2,
        ProfitSharing = 4,
        Four03BPlan = 8,
        DeferredCompensation = 16,
        All = 31
    }

    #endregion

    #region  InsuranceBenefits

    [Flags]
    public enum InsuranceBenefits
    {
        None = 0,
        Dental = 1,
        Health = 2,
        Life = 4,
        Disability = 8,
        HealthSavings = 16,
        Vision = 32,
        DomesticPartnerCoverage = 64,
        All = 127
    }

    #endregion

    #region  MiscellaneousBenefits

    [Flags]
    public enum MiscellaneousBenefits
    {
        None = 0,
        BenefitsNegotiable = 1,
        TuitionAssistance = 2,
        ClothingAllowance = 4,
        ChildCare = 8,
        Relocation = 16,
        Other = 32,
        All = 63
    }

    #endregion

    #region ScreeningPreferences

    public enum ScreeningPreferences
    {
        AllowUnqualifiedApplications,
        JobSeekersMustBeScreened,
        JobSeekersMustHave1StarMatch,
        JobSeekersMustHave2StarMatch,
        JobSeekersMustHave3StarMatch,
        JobSeekersMustHave4StarMatch,
        JobSeekersMustHave5StarMatch,
        JobSeekersMustHave1StarMatchToApply,
        JobSeekersMustHave2StarMatchToApply,
        JobSeekersMustHave3StarMatchToApply,
        JobSeekersMustHave4StarMatchToApply,
        JobSeekersMustHave5StarMatchToApply
    }

    #endregion

    #region ContactMethods

    [Flags]
    public enum ContactMethods
    {
        None = 0,
        Email = 1,
        Online = 2,
        Mail = 4,
        Fax = 8,
        Telephone = 16,
        InPerson = 32,
        FocusTalent = 64,
        FocusCareer = 128,
        All = 255
    }

    #endregion

    #region JobTaskTypes

    public enum JobTaskTypes
    {
        YesNo,
        MultiOption,
        Open
    }

    #endregion

    #region CensorshipLevels

    public enum CensorshipType
    {
        Profanity = 1,
        CriminalRecordExclusion = 2
    }

    public enum CensorshipLevel
    {
        Unknown = 0,
        Match = 1,
        Yellow = 2,
        Red = 3
    }

    #endregion

    #region PhoneTypes

    /// <summary>
    /// Enumeration of contact detail types.
    /// </summary>
    public enum PhoneTypes
    {
        Phone = 0,
        Mobile = 1,
        Email = 3,
        Fax = 4,
        Other = 6
    }

    #endregion

    #region UserTypes

    [Flags]
    public enum UserTypes
    {
        Anonymous = 0,
        Assist = 1,
        Talent = 2,
        Career = 4,
        Explorer = 8,
    }

    public enum CareerAccountType
    {
        JobSeeker = 0,
        Student = 1
    }

    #endregion

    #region ApplicationStatusTypes

    public enum ApplicationStatusTypes
    {
        [EnumLocalisationDefault("Not applicable")]
        NotApplicable = 0,
        [EnumLocalisationDefault("Self-referred")]
        SelfReferred = 1,
        [EnumLocalisationDefault("Recommended")]
        Recommended = 2,
        [EnumLocalisationDefault("New applicant")]
        NewApplicant = 3,
        [EnumLocalisationDefault("Under consideration")]
        UnderConsideration = 4,
        [EnumLocalisationDefault("Failed to apply to job")]
        DidNotApply = 5,
        [EnumLocalisationDefault("Interview scheduled")]
        InterviewScheduled = 6,
        [EnumLocalisationDefault("Interviewed")]
        Interviewed = 7,
        [EnumLocalisationDefault("Interview denied")]
        InterviewDenied = 8,
        [EnumLocalisationDefault("Failed to report to interview")]
        FailedToShow = 9,
        [EnumLocalisationDefault("Offer made")]
        OfferMade = 10,
        [EnumLocalisationDefault("Refused job")]
        RefusedOffer = 11,
        [EnumLocalisationDefault("Hired")]
        Hired = 12,
        [EnumLocalisationDefault("Not hired")]
        NotHired = 13,
        [EnumLocalisationDefault("Failed to report to job")]
        FailedToReportToJob = 14,
        [EnumLocalisationDefault("Failed to respond to invitation")]
        FailedToRespondToInvitation = 15,
        [EnumLocalisationDefault("Found job from other source")]
        FoundJobFromOtherSource = 16,
        [EnumLocalisationDefault("Job already filled")]
        JobAlreadyFilled = 17,
        [EnumLocalisationDefault("Not qualified")]
        NotQualified = 18,
        [EnumLocalisationDefault("Not yet placed")]
        NotYetPlaced = 19,
        [EnumLocalisationDefault("Refused referral")]
        RefusedReferral = 20,
        [EnumLocalisationDefault("Reconsider")]
        ReconsiderReferral = 21
    }

    #endregion

    #region Frequencies

    public enum Frequencies
    {
        Hourly,
        Daily,
        Weekly,
        Monthly,
        Yearly
    }

    public enum AlertFrequencies
    {
        Daily,
        Weekly,
        Never
    }

    #endregion

    #region TimeSpans

    public enum TimeSpans
    {
        Months,
        Years
    }

    #endregion

    #region DateWithinRanges

    public enum DateWithinRanges
    {
        LastThreeDays,
        LastSevenDays,
        LastThirtyDays,
        LastSixtyDays,
        LastNinetyDays
    }

    #endregion

    #region MessageFormats

    public enum MessageFormats
    {
        Email,
        TextMessage,
        InteractiveVoiceResponse,
        HomepageAlert
    }

    #endregion

    #region MessageAudiences

    public enum MessageAudiences
    {
        JobSeeker,
        Employer,
        BusinessUnit,
        [EnumLocalisationDefault("Job seekers")]
        AllJobSeekers,
        [EnumLocalisationDefault("Hiring managers")]
        AllTalentUsers,
        [EnumLocalisationDefault("Staff members")]
        AllAssistUsers,
        User,
    }

    #endregion

    #region ApplicationImageTypes

    public enum ApplicationImageTypes
    {
        FocusTalentHeader,
        FocusAssistHeader,
        FocusCareerExplorerHeader
    }

    #endregion

    #region CandidateSystemDefaults enums

    public enum SearchCentrePointOptions
    {
        SelectedLocations = 0,
        Zipcode = 1
    }

    public enum ResumeSearchableOptions
    {
        NotApplicable = 0,
        Yes = 1,
        No = 2,
        JobSeekersOptionYesByDefault = 3,
        JobSeekersOptionNoByDefault = 4
    }

    public enum JobAlertsOptions
    {
        On,
        OnButCanUnsubscribe,
        UserConfiguration
    }

    public enum NewResumeApprovalsOptions
    {
        NoApproval,
        ApprovalSystemAvailable,
        ApprovalSystemNotAvailable
    }

    public enum ResumeReferralApprovalsOptions
    {
        SelfReferralForAnyJob,
        SelfReferralRequires3StarPlusMatch,
        SelfReferralRequires4StarPlusMatch,
        OnlyStaffMayReferJobSeekers
    }

    public enum TalentApprovalOptions
    {
        [EnumLocalisationDefault("No approval necessary")]
        NoApproval,

        [EnumLocalisationDefault("Approval required, but system is available")]
        ApprovalSystemAvailable,

        [EnumLocalisationDefault("Approval required, but system is unavailable")]
        ApprovalSystemNotAvailable
    }

    public enum JobApprovalOptions
    {
        NoApproval = 0,
        ApproveAll = 1,
        SpecifiedApproval = 2
    }

    public enum JobApprovalCheck
    {
        [EnumLocalisationDefault("Commission-based jobs")]
        ComissionBased = 0,
        [EnumLocalisationDefault("Commission + salary jobs")]
        ComissionSalary = 1,
        [EnumLocalisationDefault("Court-ordered affirmative action jobs")]
        CourtOrdered = 2,
        [EnumLocalisationDefault("Federal contractor jobs")]
        FederalContractor = 3,
        [EnumLocalisationDefault("Foreign labor (H2A) jobs")]
        ForeignLaborH2A = 4,
        [EnumLocalisationDefault("Foreign labor (H2B) jobs")]
        ForeignLaborH2B = 5,
        [EnumLocalisationDefault("Foreign labor (Other) jobs")]
        ForeignLaborOther = 6,
        [EnumLocalisationDefault("Home-based/remote jobs")]
        HomeBased = 7,
        [EnumLocalisationDefault("Credit background check required")]
        CreditCheck = 8,
        [EnumLocalisationDefault("Criminal background check required")]
        CriminalBackground = 9,
        [EnumLocalisationDefault("Minimum age for \"Other\" reason")]
        MinimumAgeForOther = 10,
        [EnumLocalisationDefault("Special requirements")]
        SpecialRequirements = 11
    }

    #endregion

    #region List Types

    public enum ListTypes
    {
        AssistJobSeeker = 1,
        TalentJobSeeker = 2,
        FlaggedJobSeeker = 3,
    }

    #endregion

    #region Registration

    public enum RegistrationRoute
    {
        Default,
        Route1 // Used currently by Kentucky
    }

    public enum RegistrationCompleteAction
    {
        RestrictedAccess,
        EmailLink
    }

    #endregion Registration

    #region SSO ActionType

    public enum SSOActionType
    {
        CompleteRegistration,
        AccessEmployeeAccount,
        AccessStaffAccount,
        AccessPrintableReport,
        AccessJobAsEmployee,
        None
    }

    #endregion

    #region MessageType

    public enum MessageTypes
    {
        General = 0,
        ImpendingJobExpiryWarning = 1,
        CareerDefaultResumePoor = 2
    }

    #endregion

    #region Office Default Types

    [Flags]
    public enum OfficeDefaultType
    {
        None = 0,
        JobSeeker = 1,
        JobOrder = 2,
        Employer = 4,
        All = 7
    }

    #endregion

    #region Reporting

    public enum ReportEntityType
    {
        None = 0,
        Employer = 1,
        JobOrder = 2,
        JobSeeker = 3,
        Action = 4,
        JobSeekerAction = 5,
        JobSeekerActivityAssignment = 6,
        PersonOffice = 7,
        JobOrderAction = 8,
        JobOffice = 9,
        EmployerAction = 10,
        BusinessUnitOffice = 11,
        EmployerOffice = 12,
        JobSeekerOffice = 13,
        Person = 14,
        StaffMember = 15,
        StaffMemberOffice = 16,
        PersonsCurrentOffice = 17,
        Office = 18,
        EmployerTradeName = 19,
    }

    public enum ReportJobSeekerDataType
    {
        CertificationLicence = 0,
        Skill = 1,
        Internship = 2,
        EducationQualification = 3,
        JobTitle = 4,
        JobEmployer = 5,
        JobDescription = 6,
        MilitaryOccupationCode = 7,
        MilitaryOccupationTitle = 8,
        JobOnet = 9,
        JobROnet = 10,
        Office = 11,
        PhoneNumber = 12,
        TargetIndustry = 13,
        MostExperienceIndustry = 14
    }

    public enum ReportJobOrderDataType
    {
        Office = 1
    }

    public enum ReportEmployerDataType
    {
        Office = 1
    }

    public enum ReportLookUpType
    {
        CertificationLicenses,
        Skills,
        Internships,
        EducationQualifications,
        MilitaryOccupationCodes,
        MilitaryOccupationTitles,
        EmployerNames,
        EmployerFEIN,
        JobOrderTitles,
        EmployerCounties,
        JobOrderCounties,
        JobSeekerCounties
    }

    public enum ReportEthnicHeritage
    {
        [EnumLocalisationDefault("Not Disclosed")]
        NotDisclosed = 1,
        [EnumLocalisationDefault("Non-Hispanic or Latino")]
        NonHispanicLatino = 2,
        [EnumLocalisationDefault("Hispanic or Latino")]
        HispanicLatino = 3
    };

    [Flags]
    public enum ReportRace
    {
        NA = 0,
        [EnumLocalisationDefault("White")]
        White = 1,
        [EnumLocalisationDefault("Not Disclosed")]
        NotDisclosed = 2,
        [EnumLocalisationDefault("Hawaiian or Pacific Islander")]
        HawaiianPacificIslander = 4,
        [EnumLocalisationDefault("Black or African American")]
        BlackAfricanAmerican = 8,
        [EnumLocalisationDefault("Asian")]
        Asian = 16,
        [EnumLocalisationDefault("Alaskan or American Indian")]
        AlaskanAmericanIndian = 32
    }

    public enum ReportSalaryFrequency
    {
        Hourly = 1,
        Daily = 2,
        Weekly = 3,
        Monthly = 4,
        Yearly = 5
    }

    public enum ReportDisabilityType
    {
        NA = 0,
        [EnumLocalisationDefault("Not Disclosed")]
        NotDisclosed = 1,
        [EnumLocalisationDefault("Physical / mobility impairment")]
        PhysicalMobilityImpairment = 2,
        [EnumLocalisationDefault("Mental illness or psychiatric/emotional disability")]
        PsychiatricEmotional = 4,
        [EnumLocalisationDefault("Physical / chronic health conditions")]
        ChronicHealthConditions = 8,
        [EnumLocalisationDefault("Cognitive / intellectual disability")]
        CognitiveIntellectual = 16,
        [EnumLocalisationDefault("Hearing-related impairment")]
        HearingRelatedImpairment = 32,
        [EnumLocalisationDefault("Vision-related impairment")]
        VisionRelatedImpairment = 64,
        [EnumLocalisationDefault("Learning disability")]
        LearningDisability = 128

    }

    public enum ReportMilitaryDischargeType
    {
        [EnumLocalisationDefault("Honorable")]
        Honorable = 1,
        [EnumLocalisationDefault("General")]
        General = 2,
        [EnumLocalisationDefault("Other than Honorable")]
        OtherThanHonorable = 3,
        [EnumLocalisationDefault("Bad Conduct")]
        BadConduct = 4,
        [EnumLocalisationDefault("Dishonorable")]
        Dishonorable = 5,
        [EnumLocalisationDefault("Medical")]
        Medical = 6
    }

    public enum ReportMilitaryBranchOfService
    {
        [EnumLocalisationDefault("United States Air Force")]
        AirForce = 1,
        [EnumLocalisationDefault("United States Army")]
        Army = 2,
        [EnumLocalisationDefault("United States Coast Guard")]
        CoastGuard = 3,
        [EnumLocalisationDefault("United States Marine Corps")]
        MarineCorps = 4,
        [EnumLocalisationDefault("United States Navy")]
        Navy = 5,
        [EnumLocalisationDefault("United States Air Force Reserves")]
        AirForceReserves = 6,
        [EnumLocalisationDefault("United States Air National Guard")]
        AirNationalGuard = 7,
        [EnumLocalisationDefault("United States Army Reserves")]
        ArmyReserves = 8,
        [EnumLocalisationDefault("United States Army National Guard")]
        ArmyNationalGuard = 9,
        [EnumLocalisationDefault("United States Coast Guard Reserves")]
        CoastGuardReserves = 10,
        [EnumLocalisationDefault("United States Marine Corps Reserves")]
        MarineCorpsReserves = 11,
        [EnumLocalisationDefault("United States Navy Reserves")]
        NavyReserves = 12
    }

    public enum ReportType
    {
        JobSeeker = 1,
        JobOrder = 2,
        Employer = 3,
        HiringManager = 4,
        StaffActivity = 5,
        SupplyDemand = 6
    }

    public enum ReportOrder
    {
        Ascending,
        Descending
    }

    public enum ReportSupplyDemandOrderBy
    {
        SupplyCount,
        DemandCount,
        Gap,
        OnetCode,
        GroupBy
    }

    public enum ReportJobSeekerOrderBy
    {
        FirstName,
        LastName,
        EmailAddress,
        County,
        State,
        EmploymentStatus,
        VeteranType,
        VeteranTransitionType,
        VeteranMilitaryDischarge,
        VeteranBranchOfService,
        Logins,
        PostingsViewed,
        ReferralRequests,
        StaffReferrals,
        SelfReferrals,
        StaffReferralsExternal,
        SelfReferralsExternal,
        TotalReferrals,
        TargetingHighGrowthSectors,
        SavedJobAlerts,
        NotesAdded,
        AddedToLists,
        ActivitiesAssigned,
        StaffAssignments,
        FindJobsForSeeker,
        EmailsSent,
        IssuesResolved,
        FollowUpIssues,
        Hired,
        NotHired,
        FailedToApply,
        FailedToReportToInterview,
        InterviewDenied,
        InterviewScheduled,
        NewApplicant,
        Recommended,
        RefusedOffer,
        UnderConsideration,
        Office,
        EducationLevel,
        ActivityServices,
        ReferralsApproved,
        FailedToReportToJob,
        FailedToRespondToInvitation,
        FoundJobFromOtherSource,
        JobAlreadyFilled,
        NotQualified,
        NotYetPlaced,
        RefusedReferral,
        SurveyVerySatisfied,
        SurveySatisfied,
        SurveyDissatisfied,
        SurveyVeryDissatisfied,
        SurveyNoUnexpectedMatches,
        SurveyUnexpectedMatches,
        SurveyReceivedInvitations,
        SurveyDidNotReceiveInvitations
    }

    public enum ReportJobOrderOrderBy
    {
        JobTitle,
        Employer,
        County,
        State,
        Office,
        StaffReferrals,
        SelfReferrals,
        ReferralsRequested,
        TotalReferrals,
        EmployerInvitationsSent,
        InvitedJobSeekerViewed,
        InvitedJobSeekerClicked,
        Hired,
        NotHired,
        FailedToApply,
        FailedToReportToInterview,
        InterviewDenied,
        InterviewScheduled,
        NewApplicant,
        Recommended,
        RefusedOffer,
        UnderConsideration,
        FailedToReportToJob,
        FailedToRespondToInvitation,
        FoundJobFromOtherSource,
        JobAlreadyFilled,
        NotQualified,
        NotYetPlaced,
        RefusedReferral
    }

    public enum ReportEmployerOrderBy
    {
        Name,
        County,
        State,
        Office,
        JobOrdersEdited,
        JobSeekersInterviewed,
        JobSeekersHired,
        JobOrdersCreated,
        JobOrdersPosted,
        JobOrdersPutOnHold,
        JobOrdersRefreshed,
        JobOrdersClosed,
        InvitationsSent,
        JobSeekersNotHired,
        SelfReferrals,
        StaffReferrals
    }

    public enum ReportSupplyDemandGroupBy
    {
        Onet,
        State
    }

    public enum ReportJobSeekerGroupBy
    {
        State,
        EmploymentStatus,
        EducationLevel,
        Office
    }

    public enum ReportJobOrderGroupBy
    {
        State,
        Office
    }

    public enum ReportEmployerGroupBy
    {
        State,
        Office
    }

    public enum ReportDisplayType
    {
        Table,
        BarChart
    }

    public enum ReportExportType
    {
        PDF,
        Excel
    }

    public enum ReportZeroesHandling
    {
        GetAll,
        ExcludeAllZeroes,
        ExcludeAnyZeroes
    }

    public enum ReportingLookUp
    {
        JobOrderCounty = 1,
        JobOrderOffice = 2,
        CertificationLicense = 3,
        Skill = 4,
        OnetOccupationGroup = 5,
        OnetDetailedOccupation = 6,
        WotcInterest = 7,
        ActivityCounty = 8,
        ActivityOffice = 9
    }

    public enum AsyncReportType
    {
        [EnumLocalisationDefault("Employer activity report")]
        EmployerActivity = 1,
        [EnumLocalisationDefault("Student activity report")]
        StudentActivity = 2
    }

    public enum ReportJobSeekerEducationLevels
    {
        [EnumLocalisationDefault("No diploma")]
        NoDiploma = 0,
        [EnumLocalisationDefault("High School diploma")]
        HighSchoolDiploma = 1,
        [EnumLocalisationDefault("Some college, no degree")]
        SomeCollegeNoDegree = 2,
        [EnumLocalisationDefault("Associate’s or Vocational degree")]
        AssociatesDegree = 3,
        [EnumLocalisationDefault("Bachelor's degree")]
        BachelorsDegree = 4,
        [EnumLocalisationDefault("Graduate degree")]
        GraduateDegree = 5,
        [EnumLocalisationDefault("Master's degree")]
        MastersDegree = 6,
        [EnumLocalisationDefault("Doctorate degree")]
        DoctorateDegree = 7,
        [EnumLocalisationDefault("High School diploma or equivalent")]
        HighSchoolDiplomaOrEquivalent = 8,
        [EnumLocalisationDefault("High school diploma only")]
        HighSchoolDiplomaOnly = 9,
        [EnumLocalisationDefault("High School equivalency diploma only")]
        HighSchoolEquivalencyDiplomaOnly = 10,
        [EnumLocalisationDefault("Disabled with Cert. /IEP only")]
        DisabledWithCertOnly = 11
    }

    #region Veteran

    [Flags]
    public enum VeteranDefinition
    {
        Eligible = 1,
        Transaction = 2,
        Campaign = 4,
        Disabled = 8,
        SpecialDisabled = 16,
        All = 31
    }

    [Flags]
    public enum VeteranType
    {
        Veteran = 1,
        Transitioning = 2,
        OtherEligible = 4,
        All = 7
    }

    [Flags]
    public enum VeteranEmploymentStatus
    {
        Employed = 1,
        EmployedReceivedTerminationNotice = 2,
        NotEmployed = 4,
        All = 7
    }

    [Flags]
    public enum VeteranTransactionType
    {
        Discharge = 1,
        Retirement = 2,
        Spouse = 4,
        All = 7
    }

    [Flags]
    public enum VeteranMilitaryDischarge
    {
        Honourable = 1,
        General = 2,
        OtherThanHonourable = 4,
        BadConduct = 8,
        Dishonourable = 16,
        All = 31
    }

    [Flags]
    public enum VeteranBranchOfService
    {
        USAirForce = 1,
        USArmy = 2,
        USCoastGuard = 4,
        USMarineCorps = 8,
        USNavy = 16,
        All = 31
    }

    #endregion

    #region Claimant and OSOS status

    [Flags]
    public enum ClaimantStatus
    {
        None = 1,
        Seek = 2,
        Other = 4,
        Exhaustee = 8,
        EUCSeek = 16,
        EUCOther = 32,
        All = 63
    }

    [Flags]
    public enum OSOSStatus
    {
        Active = 1,
        Pending = 2,
        Inactive = 4,
        Deleted = 8,
        SSIO = 16,
        All = 31
    }

    #endregion

    #region Compliance

    [Flags]
    public enum Compliance
    {
        None = 0,
        ForeignLabourCertification = 1,
        ForeignLabourCertificationWithGoodMatches = 2,
        CourtOrderedAffirmativeAction = 4,
        FederalContract = 8,
        All = 15
    }

    #endregion

    #region Job Characteristics

    [Flags]
    public enum JobStatus
    {
        Posted = 1,
        Registered = 2,
        Expired = 4,
        Expiring = 8,
        Closed = 16,
        All = 31
    }

    #endregion

    #region EmployerCharacteristics

    [Flags]
    public enum Flags
    {
        GreenJob = 1,
        GreenSector = 2,
        VeteranServiceMemberSupportive = 4,
        All = 7
    }

    #endregion

    #region Keyword

    public enum KeyWord
    {
        EmployerName = 1,
        JobTitle = 2,
        JobDescription = 3,
        EducationQualifications = 4,
        Skills = 5,
        Internships = 6,
        FullResume = 7,
        FullJob = 8
    }

    public enum KeywordSearchType
    {
        Any = 1,
        All = 2
    }

    #endregion

    #region Result status

    [Flags]
    public enum ApplicationStatus
    {
        Hired = 1,
        Interviewed = 2,
        Rejected = 4,
        FailedToShow = 8,
        Offer = 16,
        All = 15
    }

    #endregion

    #region Keyword searches

    public enum KeyWordCriteria
    {
        None = 0,
        AllTerms = 1,
        SomeTerms = 2
    }

    #endregion

    #endregion

    #region Focus Explorer

    public enum ModalPrintFormat
    {
        None,
        Html,
        Pdf
    }

    public enum ReportTypes
    {
        [EnumLocalisationDefault("Jobs")]
        Job,
        [EnumLocalisationDefault("Employers")]
        Employer,
        [EnumLocalisationDefault("Degrees/Certificates")]
        DegreeCertification,
        [EnumLocalisationDefault("Skills")]
        Skill,
        [EnumLocalisationDefault("Internships")]
        Internship,
        [EnumLocalisationDefault("Program Areas")]
        ProgramAreaDegree,
        [EnumLocalisationDefault("Career Moves")]
        CareerMoves,
        [EnumLocalisationDefault("Career Areas")]
        CareerArea
    }

    public enum CareerAreaOccupationTypes
    {
        All,
        Job,
        MOS
    }

    public enum JobReportTypes
    {
        Top,
        Similar,
        DegreeCertification,
        Skill
    }

    /// <summary>
    /// Enumeration of Education Criteria Options
    /// </summary>
    public enum EducationCriteriaOptions
    {
        DegreeLevel = 1,
        Specific = 2
    }

    /// <summary>
    /// Enumeration of Degree Levels
    /// </summary>
    public enum DegreeLevels
    {
        AnyOrNoDegree = 1,
        LessThanBachelors = 2,
        BachelorsOrHigher = 3
    }

    /// <summary>
    /// Degreel levels
    /// </summary>
    public enum DetailedDegreeLevels
    {
        /// <summary>
        /// Default (No filtering)
        /// </summary>
        NoneSpecified,

        /// <summary>
        /// Less than a bachelors, but still a degree
        /// </summary>
        CertificateOrAssociate,

        /// <summary>
        /// Bachelors degree
        /// </summary>
        Bachelors,

        /// <summary>
        /// Post bachelors degrees
        /// </summary>
        GraduateOrProfessional
    }

    /// <summary>
    /// Enumeration of Skill Criteria Options
    /// </summary>
    public enum SkillCriteriaOptions
    {
        None,
        SkillByCategory,
        SkillByJob
    }

    /// <summary>
    /// Enumeration of Degree Certification Types
    /// </summary>
    public enum DegreeCertificationTypes
    {
        Degree = 1,
        Certification = 2
    }

    /// <summary>
    /// Enumeration of Education Levels
    /// </summary>
    public enum ExplorerEducationLevels
    {
        NonDegree = 1,
        HighSchool = 12,
        Certificate = 13,
        Associate = 14,
        Bachelor = 16,
        PostBachelorCertificate = 17,
        GraduateProfessional = 18,
        PostMastersCertificate = 19,
        Doctorate = 21
    }

    /// <summary>
    /// Enumeration of Degree Certification Path Types
    /// </summary>
    public enum DegreeCertificationPathTypes
    {
        Education = 1,
        Employment = 2
    }

    /// <summary>
    /// Enumeration of Job Skill Types
    /// </summary>
    public enum JobSkillTypes
    {
        Demanded = 1,
        Held = 2
    }

    /// <summary>
    /// Enumeration of Skill Types
    /// </summary>
    public enum SkillTypes
    {
        Specialized = 1,
        Software = 2,
        Foundation = 3
    }

    public enum InternshipSkillTypes
    {
        General = 1,
        InformationTechnology = 2
    }

    /// <summary>
    /// Enumeration of Career Path Directions
    /// </summary>
    public enum CareerPathDirections
    {
        FromThisJob,
        ToThisJob
    }

    /// <summary>
    /// Enumeration of Education Requirement Types
    /// </summary>
    public enum EducationRequirementTypes
    {
        HighSchoolTechnicalTraining = 1,
        AssociatesDegree = 2,
        BachelorsDegree = 3,
        GraduateProfessionalDegree = 4
    }

    /// <summary>
    /// Enumeration of Experience Levels
    /// </summary>
    public enum ExperienceLevels
    {
        LessThanTwoYears = 1,
        TwoToFiveYears = 2,
        FiveToEightYears = 3,
        EightPlusYears = 4
    }

    public enum RegistrationAdditionalInformationTypes
    {
        FullName,
        DateOfBirth,
        PhoneNumber
    }

    /// <summary>
    /// Enumeration of Bookmark types
    /// </summary>
    public enum BookmarkTypes
    {
        // Add bookmark types here
        Report = 1,
        ReportItem = 2,
        EmailedReportItem = 3,
        Posting = 4
    }

    public enum InternshipListModes
    {
        Report,
        Employer,
        Skill
    }


    public enum DegreeFilteringType
    {
        Ours = 1,
        Theirs = 2,
        Both = 3,
        TheirsThenOurs = 4
    }

    public enum StarterJobLevel
    {
        Some = 1,
        Many = 2
    }

    #endregion

    #region ForeignLaborTypes

    public enum ForeignLaborTypes
    {
        ForeignLaborCertificationAll,
        ForeignLabourCertificationH2A,
        ForeignLabourCertificationH2B,
        ForeignLabourCertificationOther
    }

    #endregion

    #region Talent pool tabs

    /// <summary>
    /// 
    /// </summary>
    public enum TalentPoolListTypes
    {
        AllResumes,
        MatchesForThisJob,
        Applicants,
        FlaggedResumes,
        Referrals,
        Invitees
    }

    #endregion

    #region NoteReminderTypes

    public enum NoteReminderTypes
    {
        Note,
        Reminder
    }

    #endregion

    #region NoteTypes

    public enum NoteTypes
    {
        ResolvedIssues,
        FollowUpRequest
    }

    #endregion

    #region ReminderMedia

    public enum ReminderMedia
    {
        Email,
        DashboardMessage
    }

    #endregion

    #region DocumentType

    public enum DocumentFileType
    {
        Pdf,
        Doc,
        Docx,
        Xlsx,
        Xls,
        Csv
    }

    #endregion

    #region DocumentContentType

    public enum DocumentContentType
    {
        None = 'N',
        Binary = 'B',
        HTML = 'H',
        RTF = 'R'
    }

    #endregion

    #region SavedSearchTypes

    public enum SavedSearchTypes
    {
        TalentCandidateSearch,
        AssistCandidateSearch,
        CareerPostingSearch,
        CareerPreferencesSearch
    }

    #endregion

    #region Lens Service Types

    public enum LensServiceTypes
    {
        Generic,
        Resume,
        Posting,
        JobMine,
        JobAlert
    }

    #endregion

    #region Career Enums

    public enum DocumentType
    {
        Resume = 'R',
        Posting = 'P'
    }

    public enum ExternalSystem
    {
        BGT,
        EKOS,
        AOSOS,
        ATWORKS,
        IOWAWORKS
    }

    public enum Operation
    {
        None,
        Insert,
        Update,
        Delete
    }

    public enum AlertTypes
    {
        Error,
        Info
    }

    public enum PostingKeywordScopes
    {
        JobTitle = 1,
        Employer = 2,
        JobDescription = 3,
        Anywhere = 9
    }

    public enum FilterTypes
    {
        NoFilter,
        ShowOnly,
        Exclude
    }

    public enum ReferralStatuses
    {
        Approved,
        Denied,
        Reviewed
    }

    public enum PostingStatuses
    {
        Active = 1,
        Review = 2,
        Hold = 5,
        Archived = 6,
        Closed = 7,
        Filled = 8,
        Deleted = 9
    }

    public enum Genders
    {
        [EnumLocalisationDefault("Female")]
        Female = 1,
        [EnumLocalisationDefault("Male")]
        Male = 2,
        [EnumLocalisationDefault("Not Disclosed")]
        NotDisclosed = 3
    }

    public enum MimeDocumentType
    {
        None = 'N',
        Binary = 'B',
        HTML = 'H',
        RTF = 'R'
    }

    public enum NcrcLevelTypes
    {
        None = 0,
        Bronze = 1,
        Silver = 2,
        Gold = 3,
        Platinum = 4
    }

    #endregion

    #region User Statuses

    public enum UserStatus
    {
        Active,
        Inactive,
        Deleted,
        Pending
    }

    #endregion

    #region Tool Technology Types

    public enum ToolTechnologyTypes
    {
        Tools,
        Technology
    }

    #endregion

    #region JobTaskScopes

    public enum JobTaskScopes
    {
        Job,
        Resume
    }

    #endregion

    #region Resume Statuses

    public enum ResumeStatuses
    {
        Incomplete = -1,
        Inactive = 0,
        Active = 1,
        Pending = 2,
        Deleted = 3,
        Archived = 4
    }

    #endregion

    #region Resume Completion Statuses

    [Flags]
    public enum ResumeCompletionStatuses
    {
        None = 0,
        WorkHistory = 1,
        Contact = 2,
        Education = 4,
        Summary = 8,
        Options = 16,
        Profile = 32,
        Preferences = 64,
        Completed = WorkHistory | Contact | Education | Summary | Options | Profile | Preferences
    }

    #endregion

    #region SkillTypes

    public enum SkillType
    {
        BaseLine = 1,
        Distinctive = 2,
        Neutral = 3,
        Specialized = 4,
        Unknown = 5
    }

    #endregion

    #region SkillCategory

    public enum SkillCategories
    {
        BurningGlass = 1,
        BiotechPharma = 2,
        Green = 3,
        Healthcare = 4,
        HealthInformation = 5,
        PotentiallyGreen = 6,
        Focus = 7,
        Unclassified = 99
    }

    #endregion

    #region EducationSkillCategory

    public enum EducationSkillCategories
    {
        General = 1,
        Software = 2,
    }

    #endregion

    #region JobSeeker Activity Type

    public enum JobSeekerActivityType
    {
        Action,
        ApplicationStatus,
        JobSeekerActivity
    }

    #endregion

    #region Job seeker issues

    public enum CandidateIssueType
    {
        [EnumLocalisationDefault("#CANDIDATETYPE# not signing in")]
        NotLoggingIn,
        [EnumLocalisationDefault("#CANDIDATETYPE# not clicking on leads")]
        NotClickingLeads,
        [EnumLocalisationDefault("#CANDIDATETYPE# rejecting job offers")]
        RejectingJobOffers,
        [EnumLocalisationDefault("#CANDIDATETYPE# not reporting for interview")]
        NotReportingForInterviews,
        [EnumLocalisationDefault("#CANDIDATETYPE# not responding to employer invitations")]
        NotRespondingToEmployerInvites,
        [EnumLocalisationDefault("Showing low-quality matches")]
        ShowingLowQualityMatches,
        [EnumLocalisationDefault("Posting poor-quality resume")]
        PoorQualityResume,
        [EnumLocalisationDefault("Suggesting post-hire follow-up")]
        SuggestingPostHireFollowUp,
        [EnumLocalisationDefault("Requiring follow-up")]
        RequiringFollowUp,
        [EnumLocalisationDefault("#CANDIDATETYPE# not searching jobs")]
        NotSearchingJobs,
        [EnumLocalisationDefault("Inappropriate email address")]
        InappropriateEmailAddress,
        [EnumLocalisationDefault("Giving positive survey feedback")]
        GivingPositiveFeedback,
        [EnumLocalisationDefault("Giving negative survey feedback")]
        GivingNegativeFeedback,
        [EnumLocalisationDefault("Potential MSFW")]
        PotentialMSFW,
        [EnumLocalisationDefault("#CANDIDATETYPE# with expired alien registration")]
        ExpiredAlienCertification
    }

    #endregion

    #region MigrationService

    public enum MigrationEmailRecordType
    {
        JobSeeker,
        Employee
    }

    public enum MigrationEntityType
    {
        JobSeeker,
        Employer,
        JobOrder,
        User
    }

    public enum MigrationStatus
    {
        ToBeProcessed = 0,
        Processed = 1,
        Failed = 2
    }

    public enum MigrationEmployerRecordType
    {
        Employee = 0,
        BusinessUnitDescription = 1,
        BusinessUnitLogo = 2,
        ResumeSearch = 3,
        ResumeAlert = 4,
        TradeName = 5,
    }

    public enum MigrationJobOrderRecordType
    {
        Job = 0,
        Location = 1,
        Address = 2,
        Certificate = 3,
        Licence = 4,
        Language = 5,
        SpecialRequirement = 6,
        ProgramOfStudy = 7,
        SpideredJob = 8,
        SpideredJobReferral = 9,
        SavedJob = 10,
        JobsViewed = 11,
    }

    public enum MigrationJobSeekerRecordType
    {
        JobSeeker = 0,
        Resume = 1,
        ResumeDocument = 2,
        SavedSearch = 3,
        ViewedPosting = 4,
        Application = 5, // No longer used. Replaced by Referral = 9
        Activity = 6,
        Flagged = 7,
        SearchAlert = 8,
        Referral = 9,
    }

    public enum MigrationAssistUserRecordType
    {
        AssistUser = 0,
        NotesAndReminders = 1,
        Office = 2,
        OfficeMapping = 3
    }

    #endregion

    #region Activity

    public enum ActivityType
    {
        JobSeeker = 0,
        Employer = 1
    }

    public enum ActionEventStatus
    {
        None = 0,
        Backdated = 1,
        Deleted = 2
    }

    #endregion

    #region Orientation

    public enum FlowDirection
    {
        Horizontal,
        Vertical
    }

    #endregion

    #region SchoolTypes

    public enum SchoolTypes
    {
        None = 0,
        TwoYear = 1,
        FourYear = 2
    }

    #endregion

    #region Resume matching type

    public enum MatchingType
    {
        None,
        ProgramOfStudy,
        Resume,
        ResumeAndProgramOfStudy
    }

    #endregion

    #region Career homepage action types

    public enum CareerHomepageActionType
    {
        None,
        PinRegistration,
        SSORegistration,
        ExternalValidation
    }

    #endregion

    #region Statement tenses

    public enum StatementTense
    {
        None,
        Present,
        Past
    }

    #endregion

    #region Office work level

    public enum OfficeWorkLevel
    {
        Office,
        Statewide
    }

    #endregion

    #region Career\Explorer emphasis

    public enum CareerExplorerFeatureEmphasis
    {
        Career = 1,
        Explorer = 2
    }

    #endregion

    #region JobDisplaySection

    public enum JobDisplaySection
    {
        Requirements,
        Details,
        SalaryBenefits,
        RecruitmentInformation
    }

    #endregion

    #region Assignment Types

    public enum AssignmentType
    {
        AllAssignments = 0,
        MyAssignments = 1
    }

    #endregion

    #region OfficeGroups

    public enum OfficeGroup
    {
        MyOffices = 0,
        StatewideOffices = 1
    }

    #endregion

    #region Satisfaction Levels

    public enum SatisfactionLevels
    {
        VerySatisfied = 0,
        SomewhatSatisfied = 1,
        SomewhatDissatisfied = 2,
        VeryDissatisfied = 3
    }

    #endregion

    #region post hire follow up status

    public enum PostHireFollowUpStatus
    {
        Pending = 0,
        Completed = 1
    }

    #endregion

    #region Job Issues

    public enum JobIssuesFilter
    {
        None, // i.e. No issues
        LowQualityMatches,
        LowReferralActivity,
        [EnumLocalisationDefault("Not clicking on referred applicants")]
        NotViewingReferrals,
        [EnumLocalisationDefault("Searching but not inviting")]
        SearchNotInviting,
        ClosingDateRefreshed,
        EarlyJobClosing,
        NegativeSurveyResponse,
        PositiveSurveyResponse,
        PostHireFollowUp,
        FollowUpRequested,
        Any,
        Both
    }

    #endregion

    #region Caching method

    public enum CachingMethod
    {
        HTTP,
        AppFabric
    }

    #endregion

    #region Veteran Filter

    public enum VeteranFilterTypes
    {
        None,
        NonVeteran,
        Veteran
    }

    #endregion

    #region Job Seeker Filter Type

    public enum JobSeekerFilterType
    {
        None,
        NonVeteran,
        Veteran,
        MSFWVerified,
        UnderAge
    }

    #endregion

    #region Approval Queue Filter Types

    public enum ApprovalQueueFilterType
    {
        Job,
        Employer,
        JobSeeker
    }

    #endregion

    #region Integration

    public enum IntegrationClient
    {
        Standalone = 0,
        Mock = 1,
        AOSOS = 2,
        EKOS = 3,
        Aptimus = 4, // Apollo
        AtWorks = 5,
        RightManagement = 6,
        Wisconsin = 7,
        Georgia = 8
    }

    public enum IntegrationOutcome
    {
        Success,
        Failure,
        NotImplemented,
        Ignored
    }

    public enum IntegrationPoint
    {
        None = -1,
        GetJobSeeker,
        SaveJobSeeker,
        ReferJobSeeker,
        AssignJobSeekerActivity,
        AuthenticateJobSeeker,
        SaveJob,
        UpdateJobStatus,
        SaveJobMatches,
        GetJob,
        SaveEmployer,
        SaveEmployee,
        GetEmployer,
        AssignEmployerActivity,
        ChangePassword,
        AssignAdminToEmployer,
        AssignAdminToJobSeeker,
        GetStaff,
        SetApplicationStatus,
        IsUIClaimant,
        LogAction,
        RefreshJobSeeker,
        JobSeekerLogin,
        UpdateEmployeeStatus,
        AssignAdminToJobOrder,
        DisableJobSeekersReport
    }

    public enum ExternalLookUpType
    {
        EducationLevel = 0,
        JobStatus = 1,
        Gender = 2,
        DisabilityStatus = 3,
        SalaryUnit = 4,
        Shift = 5, // Job
        DrivingLicenceClass = 6,
        InsuranceBenefits = 7,
        LeaveBenefits = 8,
        RetirementsBenefits = 9,
        MiscellaneousBenefits = 10,
        WorkType = 11,
        Ethnicity = 12,
        ShiftType = 13, // Job seeker
        DrivingLicenceEndorsements = 14,
        EmployerOwner = 15,
        Salutation = 16,
        EmploymentStatus = 17,
        SchoolStatus = 18,
        WorkWeek = 19,
        DisabilityCategory = 20,
        ApplicationStatus = 21,
        BaseNaics = 22,
        ActionType = 23,
        Boolean = 24,
        Race = 25,
        Radius = 26,
        PhoneType = 27,
        AccountType = 28,
        JobType = 30,
        JobEmploymentStatus = 31,
        JobEducationLevel = 32,
        Duration = 33,
        VeteranEra = 200,
        VeteranDisabilityStatus = 201,
        VeteranTransitionType = 202,
        MilitaryBranchOfService = 203,
        County = 300,
        State = 301,
        Country = 302,
        IntegrationOfficeIdPerZip = 400
    }

    public enum IntegrationLoggingLevel
    {
        None,
        Failed,
        All
    }
    public enum IntegrationAuthentication
    {
        None = 0,
        ReturnDetails = 1,
        Full = 2
    }

    #region Job Seeker

    public enum JobSeekerAuthenticationResult
    {
        UserFound,
        UserNotFound,
        PasswordMismatch,
        DuplicateUserFound
    }

    #endregion

    #region Employer

    public enum EmployerStatusTypes
    {
        NotSet = 0,
        Active = 1,
        // 2 no value
        InActive = 3,
        Delete = 4,
        Archive = 5
    }

    #endregion

    #region Job

    public enum JobStatusTypes
    {
        NotSet = 0,
        Active = 1,
        Review = 2,
        Referred = 3,
        // missing 4
        Hold = 5,
        Deleted = 6,
        Closed = 7,
        Filled = 8
    }

    #endregion

    public enum IntegrationImportRecordType
    {
        Employer,
        JobSeeker
    }

    public enum IntegrationImportRecordStatus
    {
        ToProcess,
        Processing,
        Imported,
        Failed
    }

    #endregion

    #region Account status types

    public enum AccountStatusFilter
    {
        Active,
        Inactive,
        Both
    }

    #endregion

    #region Employer Approval Filter Types

    public enum EmployerApprovalFilterTypes
    {
        None,
        Pending,
        DeniedInLast30Days,
        DeniedOver30DaysAgo
    }

    #endregion

    #region Batch Processes

    public enum BatchProcesses
    {
        Unknown = 0,
        AboutToExpiredJobs = 1,
        CandidateIssues = 2,
        EmailAlerts = 3,
        ExpiredJobs = 4,
        Reminders = 5,
        SearchAlerts = 6,
        JobIssues = 7,
        JobSeekerReportAges = 8,
        DenyReferralRequest = 9,
        Statistics = 10,
        SessionMaintenance = 11,
        MatchesToRecentPlacements = 12,
        QueuedInvitesAndRecommendations = 13
    }

    #endregion

    #region User Validation Keys

    public enum UserValidationKeyStatus
    {
        Sent = 0,
        Validated = 1,
        Used = 2
    }

    #endregion

    #region Account Disabled/Blocked Reasons

    public enum AccountDisabledReason
    {
        Unknown,
        Inactivity,
        InactivatedByStaffRequest,
        BlockedByStaffRequest,
        DeactivatedByStaffRequest
    }

    public enum BlockedReason
    {
        Unknown = 0,
        [EnumLocalisationDefault("Failed password reset security")]
        FailedPasswordReset = 1,
        [EnumLocalisationDefault("Alien registration date expired")]
        AlienRegistrationDateExpired = 2
    }

    #endregion



    #region Enable/Disable Account

    public enum EnableDisableAccount
    {
        Unknown,
        Enable,
        Disable
    }

    #endregion

    #region ApplicationOnHoldReasons

    public enum ApplicationOnHoldReasons
    {
        Unknown,
        JobClosed,
        JobPutOnHold,
        PutOnHoldByStaff
    }

    #endregion

    #region Education levels

    /// <summary>
    /// EducationLevel enum - put the integer value as the last 2 digits of the enum string.  This is needed for Education level validation in
    /// Education.ascx.ValidateEnrolmentEducationLevel
    /// </summary>
    public enum EducationLevel
    {
        [EnumLocalisationDefault("No formal education")]
        No_Grade_00 = 0,
        [EnumLocalisationDefault("1 Grade")]
        Grade_1_01 = 1,
        [EnumLocalisationDefault("2 Grade")]
        Grade_2_02 = 2,
        [EnumLocalisationDefault("3 Grade")]
        Grade_3_03 = 3,
        [EnumLocalisationDefault("4 Grade")]
        Grade_4_04 = 4,
        [EnumLocalisationDefault("5 Grade")]
        Grade_5_05 = 5,
        [EnumLocalisationDefault("6 Grade")]
        Grade_6_06 = 6,
        [EnumLocalisationDefault("7 Grade")]
        Grade_7_07 = 7,
        [EnumLocalisationDefault("8 Grade")]
        Grade_8_08 = 8,
        [EnumLocalisationDefault("9 Grade")]
        Grade_9_09 = 9,
        [EnumLocalisationDefault("10 Grade")]
        Grade_10_10 = 10,
        [EnumLocalisationDefault("11 Grade")]
        Grade_11_11 = 11,
        [EnumLocalisationDefault("12 Grade - no Diploma")]
        Grade_12_No_Diploma_12 = 12,
        [EnumLocalisationDefault("High School Equivalency Diploma")]
        GED_88 = 88,
        [EnumLocalisationDefault("12 Grade - H.S. Graduate")]
        Grade_12_HS_Graduate_13 = 13,
        [EnumLocalisationDefault("Disabled w/Cert./IEP")]
        Disabled_w_Cert_IEP_14 = 14,
        [EnumLocalisationDefault("H.S. + 1 yr coll. or voc/tech - no degree")]
        HS_1_Year_College_OR_VOC_Tech_No_Degree_15 = 15,
        [EnumLocalisationDefault("H.S. + 2 yr coll. or voc/tech - no degree")]
        HS_2_Year_College_OR_VOC_Tech_No_Degree_16 = 16,
        [EnumLocalisationDefault("H.S. + 3 yr coll. or voc/tech - no degree")]
        HS_3_Year_College_OR_VOC_Tech_No_Degree_17 = 17,
        [EnumLocalisationDefault("H.S. + 1 yr Vocational Degree")]
        HS_1_Year_Vocational_Degree_18 = 18,
        [EnumLocalisationDefault("H.S. + 2 yr Vocational Degree")]
        HS_2_Year_Vocational_Degree_19 = 19,
        [EnumLocalisationDefault("H.S. + 3 yr Vocational Degree")]
        HS_3_Year_Vocational_Degree_20 = 20,
        [EnumLocalisationDefault("H.S. + 1 yr Associate's Degree")]
        HS_1_Year_Associates_Degree_21 = 21,
        [EnumLocalisationDefault("H.S. + 2 yr Associate's Degree")]
        HS_2_Year_Associates_Degree_22 = 22,
        [EnumLocalisationDefault("H.S. + 3 yr Associate's Degree")]
        HS_3_Year_Associates_Degree_23 = 23,
        [EnumLocalisationDefault("Bachelor's degree or equivalent")]
        Bachelors_OR_Equivalent_24 = 24,
        [EnumLocalisationDefault("Master's degree")]
        Masters_Degree_25 = 25,
        [EnumLocalisationDefault("Doctorate degree")]
        Doctorate_Degree_26 = 26,
        [EnumLocalisationDefault("High School Diploma or Equivalent")]
        High_School_Diploma_OR_Equivalent_30 = 30,
        [EnumLocalisationDefault("Certificate/Vocational Program")]
        Certificate_Vocational_Program_31 = 31,
        [EnumLocalisationDefault("Associate's Degree")]
        Associates_Degree_32 = 32,
        [EnumLocalisationDefault("College Courses Completed")]
        College_Courses_Completed_33 = 33,
        [EnumLocalisationDefault("Professional Doctorate (M.D., D.O., D.D.S., J.D., etc.)")]
        Professional_Doctorate_34 = 34,
        [EnumLocalisationDefault("College Courses Completed beyond Highest Degree")]
        College_Courses_Completed_Beyond_Highest_Degree_35 = 35,
        [EnumLocalisationDefault("Professional Development")]
        Professional_Development_36 = 36,
        [EnumLocalisationDefault("Other")]
        Other_37 = 37,
    }

    #endregion

    #region AccountTypes

    public enum AccountTypes
    {
        DirectEmployer,
        StaffingAgency,
        NotIndicated
    }

    #endregion

    #region ShowContactDetails

    public enum ShowContactDetails
    {
        None = 0,
        Full = 1,
        Partial = 2
    }

    #endregion

    #region Extend Veteran Priority Of Service

    public enum ExtendVeteranPriorityOfServiceTypes
    {
        None = 0,
        ExtendUntil = 1,
        ExtendForLifeOfPosting = 2
    }

    #endregion

    #region Validation

    public enum ValidationSchema
    {
        None = 0,
        JobUpload = 1
    }

    public enum UploadFileType
    {
        CSV,
        Excel
    }

    public enum UploadLookupTable
    {
        [XmlEnum("None")]
        None,
        [XmlEnum("PostalCode")]
        PostalCode
    }

    [Flags]
    public enum ValidationRecordStatus
    {
        Success = 0,
        [EnumLocalisationDefault("Validation Error")]
        SchemaFailure = 1,
        [EnumLocalisationDefault("Red Words")]
        RedWords = 2,
        [EnumLocalisationDefault("Yellow Words")]
        YellowWords = 4,
        [EnumLocalisationDefault("Criminal Background Exclusion")]
        CriminalBackgroundExclusion = 8
    }

    public enum UploadFileProcessingState
    {
        RequiresValidation,
        Validated,
        RequiresCommitting,
        Committed
    }

    #endregion

    #region ApprovalTypes

    public enum ApprovalType
    {
        Employer = 1,
        BusinessUnit = 2,
        HiringManager = 3
    }

    public enum ApproveReferralResponseType
    {
        Success = 0,
        ParentCompanyLinkedToBusinessUnitNotApproved = 1,
        ParentCompanyLinkedToHiringManagerNotApproved = 2
    }

    #endregion

    #region ConfigSettingTypeEnum

    public enum ConfigSettingType
    {
        Db = 1,
        Web = 2
    }

    #endregion

    #region Statistic types

    public enum StatisticType
    {
        TotalSearchableResumes = 1,
        TotalSearchableResumesAddedYesterday = 2,
        TotalActiveTalentPostings = 3,
        TotalActiveTalentPostingsAddedYesterday = 4,
        TotalActiveSpideredJobs = 5
    }

    #endregion

    #region Anonymous

    public enum AnonymousAccess
    {
        None = 0,
        Domain = 1,
        All = 2
    }

    #endregion

    #region Email footer types

    public enum EmailFooterTypes
    {
        None = 0,
        JobSeekerUnsubscribe = 1,
        HiringManagerUnsubscribe = 2
    }

    #endregion

    #region Encryption Target Types

    public enum TargetTypes
    {
        JobSeekerUnsubscribe = 1,
        HiringManagerUnsubscribe = 2,
        SecurityAnswer = 3
    }

    #endregion

    #region User external pre-existence reason

    public enum ExtenalUserPreExistenceReason
    {
        NotApplicable,
        UsernameExists,
        SsnExists
    }

    #endregion

    #region Security Questions Page Mode

    public enum SecurityQuestionsPageMode
    {
        Register,
        Update
    }

    #endregion

    #region Labor/Insight querystring params

    public enum LaborInsightParams
    {
        NationWide,
        State,
        County,
        MSA,
        Onet,
        BGTOcc,
        BGTOccFamily,
        Certification,
        Skill,
        JobTitleKeyword,
        CertificationKeyword,
        SkillKeyword,
        Keyword
    }

    #endregion

    #region OAuthClient

    public enum OAuthClient
    {
        LaborInsight
    }

    #endregion

    #region Document Focus Modules

    [Flags]
    public enum DocumentFocusModules
    {
        All = 0,
        Assist = 1,
        Talent = 2,
        Career = 4
    }

    #endregion

    #region Find Employer Status Types

    public enum FindEmployerStatusTypes
    {
        All = 0,
        Unapproved = 1,
        Active = 2,
        Blocked = 3
    }

    #endregion

    #region UnsubscribeTypes

    public enum UnsubscribeTypes
    {
        VeteranPriorityEmails,
        BlastEmails
    }
    #endregion

    public enum RequirementsPreference
    {
        [EnumLocalisationDefault("No default")]
        None = 0,
        [EnumLocalisationDefault("Only show application information to people who affirm they meet these criteria")]
        CriteriaMetOnly = 1,
        [EnumLocalisationDefault("Applicants who meet specified criteria are preferred, but allow all")]
        AllowAll = 2
    }
}
