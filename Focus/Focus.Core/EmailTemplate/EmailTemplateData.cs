﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.EmailTemplate
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EmailTemplateData
	{
    [DataMember]
    public string RecipientName { get; set; }

    [DataMember]
    public string JobId { get; set; }

    [DataMember]
    public string JobTitle { get; set; }

    [DataMember]
    public string JobPosting { get; set; }

    [DataMember]
    public string JobPostingDate { get; set; }

    [DataMember]
    public string MatchingJobs { get; set; }

    [DataMember]
    public string EmployerName { get; set; }

    [DataMember]
    public string EmployeeName { get; set; }

    [DataMember]
    public string SenderName { get; set; }

    [DataMember]
    public string SenderPhoneNumber { get; set; }

    [DataMember]
    public string SenderEmailAddress { get; set; }

    [DataMember]
    public string FreeText { get; set; }

    [DataMember]
    public bool? ShowSalutation { get; set; }

    [DataMember]
    public List<string> JobLinkUrls { get; set; }

    [DataMember]
    public string ApplicationInstructions { get; set; }

    [DataMember]
    public string SavedSearchName { get; set; }

    [DataMember]
    public string MatchingCandidates { get; set; }

    [DataMember]
    public string Reminder { get; set; }

    [DataMember]
    public string HiringManagerName { get; set; }

    [DataMember]
    public string HiringManagerPhoneNumber { get; set; }

    [DataMember]
    public string HiringManagerEmail { get; set; }

    [DataMember]
    public string WOTC { get; set; }

    [DataMember]
    public string ApprovalCount { get; set; }

    [DataMember]
    public string ApprovalQueueName { get; set; }

    [DataMember]
    public string EmailAddress { get; set; }

    [DataMember]
    public string Password { get; set; }

    [DataMember]
    public string AuthenticateUrl { get; set; }

    [DataMember]
    public string PinNumber { get; set; }

		[DataMember]
		public string Username { get; set; }

		[DataMember]
		public string Url { get; set; }

    [DataMember]
    public string DaysLeftToJobExpiry { get; set; }

    [DataMember]
    public string ExpiryDate { get; set; }

    [DataMember]
    public string OfficeName { get; set; }

		[DataMember]
		public string ReportFromDate { get; set; }

		[DataMember]
		public string ReportToDate { get; set; }

    [DataMember]
    public string JobStatus { get; set; }
	}
}
