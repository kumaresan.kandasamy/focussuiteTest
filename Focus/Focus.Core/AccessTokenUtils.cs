﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;

#endregion

namespace Focus.Core
{
  public static class AccessTokenUtils
  {
    private static readonly RNGCryptoServiceProvider CryptoServiceProvider = new RNGCryptoServiceProvider();

    /// <summary>
    /// Generates an access token.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="secret">The secret.</param>
    /// <returns></returns>
    public static AccessToken GenerateToken(string key, string secret)
    {
      return GenerateToken(key, secret, GenerateTimestamp(), GenerateNonce());
    }

    /// <summary>
    /// Determines whether the specified access token is valid.
    /// </summary>
    /// <param name="authenticationToken">The authentication token.</param>
    /// <param name="key">The key.</param>
    /// <param name="secret">The secret.</param>
    /// <returns>
    ///   <c>true</c> if the specified authentication token is valid; otherwise, <c>false</c>.
    /// </returns>
    public static bool IsValid(this AccessToken authenticationToken, string key, string secret)
    {
      var validatedToken = GenerateToken(key, secret, authenticationToken.Timestamp, authenticationToken.Nonce);
      return (authenticationToken.Signature == validatedToken.Signature);
    }

    /// <summary>
    /// Generates an access token.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="secret">The secret.</param>
    /// <param name="timestamp">The timestamp.</param>
    /// <param name="nonce">The nonce.</param>
    /// <returns></returns>
    private static AccessToken GenerateToken(string key, string secret, long timestamp, string nonce)
    {
      var data = String.Format("{0}-{1}-{2}-{3}", key, secret, timestamp, nonce);
      return new AccessToken { Timestamp = timestamp, Nonce = nonce, Signature = ComputeHash(secret, data) };
    }

    /// <summary>
    /// Generate the timestamp for the signature        
    /// </summary>
    /// <returns></returns>
    private static long GenerateTimestamp()
    {
      return GenerateTimestamp(DateTime.UtcNow);
    }

    /// <summary>
    /// Generates the time stamp.
    /// </summary>
    /// <param name="dateTime">The date time to generate the timestamp from.</param>
    /// <returns></returns>
    private static long GenerateTimestamp(DateTime dateTime)
    {
      // Default implementation of UNIX time of the current UTC time
      var ts = dateTime - new DateTime(1970, 1, 1, 0, 0, 0, 0);
      return Convert.ToInt64(ts.TotalSeconds);
    }

    /// <summary>
    /// Generates the nonce.
    /// </summary>
    /// <returns></returns>
    private static string GenerateNonce()
    {
      var data = new byte[10];
      CryptoServiceProvider.GetNonZeroBytes(data);
      return Convert.ToBase64String(data);
    }

    /// <summary>
    /// Computes the hash value using the supplied consumer secret.
    /// </summary>
    /// <param name="secret">The secret.</param>
    /// <param name="data">The data.</param>
    /// <returns></returns>
    private static string ComputeHash(string secret, string data)
    {
      //Ensure.Argument.NotNullOrEmpty(secret, "accessSecret");
      //Ensure.Argument.NotNullOrEmpty(data, "data");

      string hashString;

      using (var hmac = new HMACSHA1(Encoding.UTF8.GetBytes(secret.ToUpper())))
      {
        var hash = hmac.ComputeHash(Encoding.UTF8.GetBytes(data));
        hashString = Convert.ToBase64String(hash);
      }

      return hashString;
    }
  }

  [DataContract(Namespace = "")]
  public class AccessToken
  {
    [DataMember]
    public string Nonce { get; set; }

    [DataMember]
    public long Timestamp { get; set; }

    [DataMember]
    public string Signature { get; set; }
  }
}
