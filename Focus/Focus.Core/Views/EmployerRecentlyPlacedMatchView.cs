﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Views
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EmployerRecentlyPlacedView
	{
    [DataMember]
    public long Id { get; set; }

    [DataMember]
    public string EmployerName { get; set; }

		[DataMember]
		public long BusinessUnitId { get; set; }

    [DataMember]
    public string BusinessUnitName { get; set; }

    [DataMember]
    public long PersonId { get; set; }

    [DataMember]
    public string CandidateName { get; set; }

    [DataMember]
    public DateTime PlacementDate { get; set; }

    [DataMember]
    public long JobId { get; set; }

    [DataMember]
    public string JobTitle { get; set; }
    
    [DataMember]
    public List<Candidate> Candidates { get; set; }

		[DataMember]
		public long EmployerId { get; set; }

		[Serializable]
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class Candidate
		{
      [DataMember]
      public long Id { get; set; }

      [DataMember]
      public long RecentlyPlacedId { get; set; }

      [DataMember]
      public int Score { get; set; }

      [DataMember]
      public long PersonId { get; set; }

      [DataMember]
      public string CandidateName { get; set; }

      [DataMember]
      public bool HasMatchesToOpenPositions { get; set; }
		}
	}
}
