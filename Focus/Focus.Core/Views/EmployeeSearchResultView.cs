﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Views
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EmployeeSearchResultView
	{
    [DataMember]
    public long Id { get; set; }

    [DataMember]
    public string FirstName { get; set; }

    [DataMember]
    public string LastName { get; set; }

    [DataMember]
    public string PhoneNumber { get; set; }

		[DataMember]
		public string Extension { get; set; }

    [DataMember]
    public string EmailAddress { get; set; }

    [DataMember]
    public long EmployerId { get; set; }

    [DataMember]
    public string EmployerName { get; set; }

    [DataMember]
    public string BusinessUnitName { get; set; }

    [DataMember]
    public bool AccountEnabled { get; set; }

    [DataMember]
    public ApprovalStatuses EmployerApprovalStatus { get; set; }

    [DataMember]
    public ApprovalStatuses EmployeeApprovalStatus { get; set; }

    [DataMember]
    public string Town { get; set; }

    [DataMember]
    public string State { get; set; }

    [DataMember]
    public long StateId { get; set; }

    [DataMember]
    public long BusinessUnitId { get; set; }

    [DataMember]
    public DateTime? LastLogin { get; set; }

    [DataMember]
    public string JobTitle { get; set; }

    [DataMember]
    public long EmployeeBusinessUnitId { get; set; }

		[DataMember]
		public bool AccountBlocked { get; set; }

    [DataMember]
    public bool IsPreferred { get; set; }

		[DataMember]
		public bool IsPrimary { get; set; }

    [DataMember]
    public ApprovalStatusReason EmployeeApprovalStatusReason { get; set; }

    [DataMember]
    public ApprovalStatuses BusinessUnitApprovalStatus { get; set; }

    [DataMember]
    public ApprovalStatusReason BusinessUnitApprovalStatusReason { get; set; }

		[DataMember]
		public string Suffix { get; set; }

    [DataMember]
    public BlockedReason? AccountBlockedReason { get; set; }
	}
}
