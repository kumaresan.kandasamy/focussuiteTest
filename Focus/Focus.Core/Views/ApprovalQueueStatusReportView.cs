﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Views
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ApprovalQueueStatusReportView
	{
		/// <summary>
		/// Gets or sets the new postings since last login count.
		/// </summary>
		/// <value>The new postings since last login count.</value>
    [DataMember]
    public int NewPostingsSinceLastLoginCount { get; set; }

		/// <summary>
		/// Gets or sets the flagged postings count.
		/// </summary>
		/// <value>The flagged postings count.</value>
    [DataMember]
    public int FlaggedPostingsCount { get; set; }

		/// <summary>
		/// Gets or sets the new employee accounts since last login count.
		/// </summary>
		/// <value>The new employee accounts since last login count.</value>
    [DataMember]
    public int NewEmployeeAccountsSinceLastLoginCount { get; set; }

		/// <summary>
		/// Gets or sets the outstanding employee accounts count.
		/// </summary>
		/// <value>The outstanding employee accounts count.</value>
    [DataMember]
    public int OutstandingEmployeeAccountsCount { get; set; }

		/// <summary>
		/// Gets or sets the new referral requests since last login count.
		/// </summary>
		/// <value>The new referral requests since last login count.</value>
    [DataMember]
    public int NewReferralRequestsSinceLastLoginCount { get; set; }

		/// <summary>
		/// Gets or sets the outstanding referral requests count.
		/// </summary>
		/// <value>The outstanding referral requests count.</value>
    [DataMember]
    public int OutstandingReferralRequestsCount { get; set; }
	}
}
