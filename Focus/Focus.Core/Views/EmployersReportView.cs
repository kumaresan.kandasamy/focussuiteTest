﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

using Focus.Core.DataTransferObjects.Report;

#endregion

namespace Focus.Core.Views
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EmployersReportView
  {
    [DataMember]
    public long Id { get; set; }

    [DataMember]
    public string Name { get; set; }
    
    [DataMember]
    public string County { get; set; }

    [DataMember]
    public string State { get; set; }

    [DataMember]
    public string Office { get; set; }

    [DataMember]
    public long FocusBusinessUnitId { get; set; }

    [DataMember]
    public long? FocusEmployerId { get; set; }

    [DataMember]
    public EmployerActionView Totals { get; set; }
  }
}
