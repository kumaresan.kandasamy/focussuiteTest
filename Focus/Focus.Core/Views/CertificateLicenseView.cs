﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Views
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CertificateLicenseView
	{
    [DataMember]
    public long Id { get; set; }
    
    [DataMember]
    public string NameKey { get; set; }

    [DataMember]
    public string Name { get; set; }

    [DataMember]
    public CertifcateLicenseTypes CertificateLicenseType { get; set; }

    [DataMember]
    public bool IsLicence { get; set; }

    [DataMember]
    public bool IsCertificate { get; set; }

    [DataMember]
    public string OnetParentCode { get; set; }
	}
}
