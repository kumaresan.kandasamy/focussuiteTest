﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives


using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Views
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ValidatedJobSeekerView
  {
    [DataMember]
    public string ExternalId { get; set; }

    [DataMember]
    public string UserName { get; set; }
    
    [DataMember]
    public string FirstName { get; set; }

    [DataMember]
    public string MiddleInitial { get; set; }

    [DataMember]
    public string LastName { get; set; }

    [DataMember]
    public string EmailAddress { get; set; }

    [DataMember]
    public string AddressLine1 { get; set; }

    [DataMember]
    public string AddressTownCity { get; set; }

    [DataMember]
    public long? AddressStateId { get; set; }

    [DataMember]
    public long? AddressCountyId { get; set; }

    [DataMember]
    public string AddressPostcodeZip { get; set; }

    [DataMember]
    public long? AddressCountryId { get; set; }

    [DataMember]
    public string SocialSecurityNumber { get; set; }

    [DataMember]
    public DateTime? DateOfBirth { get; set; }

    [DataMember]
    public string PrimaryPhone { get; set; }

    [DataMember]
    public string FocusUserName { get; set; }
  }
}
