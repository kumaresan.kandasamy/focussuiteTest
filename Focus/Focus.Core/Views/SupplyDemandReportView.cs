﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Views
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SupplyDemandReportView
  {
    [DataMember]
    public long Id { get; set; }

    [DataMember]
    public string GroupBy { get; set; }

    [DataMember]
    public string OnetCode { get; set; }

    [DataMember]
    public int SupplyCount { get; set; }

    [DataMember]
    public int DemandCount { get; set; }

    [DataMember]
    public int Gap { get; set; }
  }
}
