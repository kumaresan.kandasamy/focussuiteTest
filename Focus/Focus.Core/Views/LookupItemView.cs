﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Views
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class LookupItemView 
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="LookupItemView"/> class.
		/// </summary>
		public LookupItemView()
		{ }

    /// <summary>
    /// Initializes a new instance of the <see cref="LookupItemView" /> class.
    /// </summary>
    /// <param name="id">The id.</param>
    /// <param name="key">The key.</param>
    /// <param name="lookupType">Type of the lookup.</param>
    /// <param name="text">The text.</param>
    /// <param name="displayOrder">The display order.</param>
    /// <param name="parentId">The parent unique identifier.</param>
    /// <param name="parentKey">The parent key.</param>
    /// <param name="externalId">The external unique identifier.</param>
    /// <param name="customFilterId">The custom filter unique identifier.</param>
		public LookupItemView(long id, string key, LookupTypes lookupType, string text, int displayOrder, long parentId, string parentKey, string externalId, int? customFilterId)
		{
			Id = id;
			Key = key;
			LookupType = lookupType;
			Text = text;
			DisplayOrder = displayOrder;
		  ParentId = parentId;
		  ParentKey = parentKey;
			ExternalId = externalId;
			CustomFilterId = customFilterId;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="LookupItemView" /> class.
		/// </summary>
		/// <param name="id">The id.</param>
		/// <param name="key">The key.</param>
		/// <param name="lookupType">Type of the lookup.</param>
		/// <param name="text">The text.</param>
		/// <param name="displayOrder">The display order.</param>
		/// <param name="parentId">The parent id.</param>
		/// <param name="parentKey">The parent key.</param>
		/// <param name="externalId">The external id.</param>
		/// <param name="customFilterId">The custom filter id.</param>
		public LookupItemView(long id, string key, string lookupType, string text, int displayOrder, long parentId, string parentKey, string externalId, int? customFilterId)
		{
			Id = id;
			Key = key;

		  LookupTypes result;

		  if (!Enum.TryParse(lookupType, true, out result))
        result = LookupTypes.Unknown;

		  LookupType = result;
			
			Text = text;
      DisplayOrder = displayOrder;
      ParentId = parentId;
      ParentKey = parentKey;
			ExternalId = externalId;
			CustomFilterId = customFilterId;
		}

		/// <summary>
		/// Gets or sets the id.
		/// This maps to the CodeItem Id
		/// </summary>
		/// <value>The id.</value>
    [DataMember]
    public long Id { get; set; }

		/// <summary>
		/// Gets or sets the key.
		/// This maps to the CodeItem Key
		/// </summary>
		/// <value>The key.</value>
    [DataMember]
    public string Key { get; set; }

		/// <summary>
		/// Gets or sets the type of the lookup.
		/// This maps to the CodeGroup Key
		/// </summary>
		/// <value>The type of the lookup.</value>
    [DataMember]
    public LookupTypes LookupType { get; set; }

		/// <summary>
		/// Gets or sets the text.
		/// This maps to the localisation value for the CodeItem Key
		/// </summary>
		/// <value>The text.</value>
    [DataMember]
    public string Text { get; set; }

		/// <summary>
		/// Gets or sets the display order.
		/// This maps to the CodeGroupItem DisplayOrder
		/// </summary>
		/// <value>The display order.</value>
    [DataMember]
    public int DisplayOrder { get; set; }

    /// <summary>
    /// Gets or sets the parent id.
    /// </summary>
    /// <value>The parent id.</value>
    [DataMember]
    public long ParentId { get; set; }

    /// <summary>
    /// Gets or sets the parent key.
    /// </summary>
    /// <value>The parent key.</value>
    [DataMember]
    public string ParentKey { get; set; }

		/// <summary>
		/// Gets or sets the external id.
		/// </summary>
		/// <value>The external id.</value>
    [DataMember]
    public string ExternalId { get; set; }

		/// <summary>
		/// Gets or sets the custom filter id.
		/// </summary>
		/// <value>
		/// The custom filter id.
		/// </value>
    [DataMember]
    public int? CustomFilterId { get; set; }
	}
}