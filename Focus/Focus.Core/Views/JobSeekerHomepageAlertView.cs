﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Views
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobSeekerHomepageAlertView
	{
		/// <summary>
		/// Gets or sets the candidate external id.
		/// </summary>
		/// <value>The candidate external id.</value>
    [DataMember]
    public long CandidateId { get; set; }

		/// <summary>
		/// Gets or sets the message.
		/// </summary>
		/// <value>The message.</value>
    [DataMember]
    public string Message { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is system alert.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is system alert; otherwise, <c>false</c>.
		/// </value>
    [DataMember]
    public bool IsSystemAlert { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [alert for all candidates].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [alert for all candidates]; otherwise, <c>false</c>.
		/// </value>
    [DataMember]
    public bool AlertForAllCandidates { get; set; }

		/// <summary>
		/// Gets or sets the expiry date.
		/// </summary>
		/// <value>The expiry date.</value>
    [DataMember]
    public DateTime ExpiryDate { get; set; }
	}
}
