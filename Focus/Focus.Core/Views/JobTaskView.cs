﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Views
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobTaskDetailsView
	{
		public JobTaskDetailsView()
		{
			MultiOptionPrompts = new List<string>();
		}

    [DataMember]
    public long JobTaskId { get; set; }
    
    [DataMember]
    public string Prompt { get; set; }

    [DataMember]
    public string Response { get; set; }

    [DataMember]
    public JobTaskTypes JobTaskType { get; set; }

    [DataMember]
    public List<string> MultiOptionPrompts { get; set; }

    [DataMember]
    public bool Certificate { get; set; }
	}
}
