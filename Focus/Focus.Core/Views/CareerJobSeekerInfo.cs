﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;

#endregion

namespace Focus.Core.Views
{
	public class CareerJobSeekerInfo
	{
		public int? YearsExperience { get; set; }

		public bool IsVeteran { get; set; }

		public List<CareerWorkHistoryInfo> WorkHistory { get; set; }
	}

	public class CareerWorkHistoryInfo
	{
		public string JobTitle { get; set; }

		public string Employer { get; set; }

		public string StartYear { get; set; }

		public string EndYear { get; set; }
	}
}
