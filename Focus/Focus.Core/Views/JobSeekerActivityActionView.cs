﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

#endregion

namespace Focus.Core.Views
{
	[Serializable]
	public class JobSeekerActivityActionViewOld
	{
		public long Id { get; set; }
		public long UserId { get; set; }	
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string ActionName { get; set; }
		public DateTime ActionedOn { get; set; }
		public JobSeekerActivityType ActivityType { get; set; }
		public long? ActivityId { get; set; }
		public string JobTitle { get; set; }
		public string BusinessUnitName { get; set; }
	}	
}


