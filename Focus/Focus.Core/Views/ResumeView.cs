﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion


namespace Focus.Core.Views
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ResumeView
	{
    [DataMember]
    public long Id { get; set; }

    [DataMember]
    public string Name { get; set; }

    [DataMember]
    public int Score { get; set; }

    [DataMember]
    public int YearsExperience { get; set; }

    [DataMember]
    public List<ResumeJobView> EmploymentHistories { get; set; }

    [DataMember]
    public ApplicationStatusTypes Status { get; set; }

    [DataMember]
    public string EmployerNote { get; set; }

    [DataMember]
    public int NotesAndRemindersCount { get; set; }

    [DataMember]
    public DateTime ApplicationReceivedOn { get; set; }

    [DataMember]
    public DateTime? ApplicationVeteranPriorityEndDate { get; set; }

    [DataMember]
    public bool Flagged { get; set; }

    [DataMember]
    public bool IsVeteran { get; set; }

    [DataMember]
    public bool IsInviteQueued { get; set; }

    [DataMember]
    public ApprovalStatuses ApplicationApprovalStatus { get; set; }

    [DataMember]
    public int StarRating { get; set; }

    [DataMember]
    public string Resume { get; set; }

		[DataMember]
		public DateTime DateInvitedCreatedOn { get; set; }

    public bool NameIsMasked { get { return Id.ToString() == Name; } }
		
		// Additional properties to retrieve recently matched info
    [DataMember]
    public bool HasRecentlyPlacedMatch { get; set; }

    [DataMember]
    public long? RecentlyPlacedPersonId { get; set; }

    [DataMember]
    public string RecentlyPlacedCandidateName { get; set; }

    [DataMember]
    public string RecentlyPlacedJobTitle { get; set; }

    [DataMember]
    public string RecentlyPlaceBusinessUnitName { get; set; }

    [DataMember]
    public int RecentlyPlacedScore { get; set; }

    [DataMember]
    public long? RecentlyPlacedMatchId { get; set; }

    [DataMember]
    public long ApplicationId { get; set; }

		[DataMember]
		public string Branding { get; set; }

		[DataMember]
		public bool ContactInfoVisible { get; set; }

		[DataMember]
		public string CandidateLocation { get; set; }

		[DataMember]
		public NcrcLevelTypes? NcrcLevel { get; set; }

		[DataMember]
		public long? NcrcLevelId { get; set; }

		[DataMember]
		public int ApplicationLockVersion { get; set; }

		[DataMember]
		public bool? IsApplicantForEmployer { get; set; }
		
		public ResumeView()
		{
			EmploymentHistories = new List<ResumeJobView>();
		}

		[Serializable]
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class ResumeJobView
    {
      [DataMember]
      public string JobTitle { get; set; }

      [DataMember]
      public string Employer { get; set; }

      [DataMember]
      public string YearStart { get; set; }

      [DataMember]
      public string YearEnd { get; set; }
		}
	}
}
