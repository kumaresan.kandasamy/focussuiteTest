﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Views
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class MessageView
	{
    [DataMember]
    public long Id { get; set; }

    [DataMember]
    public bool IsSystemAlert { get; set; }

    [DataMember]
    public string Text { get; set; }

    [DataMember]
    public DateTime CreatedOn { get; set; }

    [DataMember]
    public MessageAudiences? Audience { get; set; }

    [DataMember]
    public long? EmployerId { get; set; }

    [DataMember]
    public long? UserId { get; set; }

    [DataMember]
    public long? BusinessUnitId { get; set; }

		[DataMember]
		public string CreatedByFirstName { get; set; }

		[DataMember]
		public string CreatedByLastName { get; set; }
	}
}
