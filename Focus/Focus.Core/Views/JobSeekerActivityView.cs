﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Views
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobSeekerActivityView
	{
    [DataMember]
    public int DaysSinceRegistration { get; set; }

    [DataMember]
    public int FocusCareerSignins { get; set; }

    [DataMember]
    public DateTime? LastLogin { get; set; }

    [DataMember]
    public int ResumeEdited { get; set; }

    [DataMember]
    public int OnlineResumeHelpUsed { get; set; }

    [DataMember]
    public int HighScoreMatches { get; set; }

    [DataMember]
    public int MatchesViewed { get; set; }

    [DataMember]
    public int ReferralsRequested { get; set; }

    [DataMember]
    public int StaffReferrals { get; set; }

    [DataMember]
    public int SelfReferrals { get; set; }

    [DataMember]
    public int JobSearchesPerformed { get; set; }
    
    [DataMember]
    public int JobSearchesByHighGrowthSectors { get; set; }

    [DataMember]
    public int JobSearchesSavedForJobAlerts { get; set; }
	}
}
