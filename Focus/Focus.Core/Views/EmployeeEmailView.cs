﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

#endregion

namespace Focus.Core.Views
{
    [Serializable]
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class EmployeeEmailView
    {
        /// <summary>
        /// Gets or sets the person id.
        /// </summary>
        [DataMember]
        public List<long> employeeIds { get; set; }

        /// <summary>
        /// Gets or sets the subject.
        /// </summary>
        [DataMember]
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the body.
        /// </summary>
        [DataMember]
        public string Body { get; set; }

        /// <summary>
        /// Gets or sets the sender address.
        /// </summary>
        /// <value>
        /// The sender address.
        /// </value>
        [DataMember]
        public string SenderAddress { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [BCC requestor].
        /// </summary>
        [DataMember]
        public string Bcc { get; set; }
        
    }
}
