﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Views
{
	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CareerAreaJobView
	{
		/// <summary>
		/// Gets or sets the job id.
		/// </summary>
		[DataMember]
		public long JobId { get; set; }

		/// <summary>
		/// Gets or sets the name of the job.
		/// </summary>
		[DataMember]
		public string JobName { get; set; }

		/// <summary>
		/// Gets or sets the career area id.
		/// </summary>
		[DataMember]
		public long CareerAreaId { get; set; }

		/// <summary>
		/// Gets or sets the R onet.
		/// </summary>
		[DataMember]
		public string ROnet { get; set; }
	}
}
