﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Views
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobDetailsView
  {
    [DataMember]
    public long JobId { get; set; }

    [DataMember]
    public string JobTitle { get; set; }

    [DataMember]
    public JobTypes JobType { get; set; }

    [DataMember]
    public string JobDescription { get; set; }

		[DataMember]
		public string PrevJobDescription { get; set; }

    [DataMember]
    public string PositionDetails { get; set; }

    [DataMember]
    public int? NumberOfOpenings { get; set; }

    [DataMember]
    public JobStatuses JobStatus { get; set; }

    [DataMember]
    public ApprovalStatuses ApprovalStatus { get; set; }

    [DataMember]
    public DateTime? ClosingDate { get; set; }

    [DataMember]
    public int ApplicationCount { get; set; }

    [DataMember]
    public int VeteranApplicationCount { get; set; }

    [DataMember]
    public int RecommendedCount { get; set; }

    [DataMember]
    public long? PostedBy { get; set; }

    [DataMember]
    public long? HiringManagerId { get; set; }

    [DataMember]
    public long EmployerId { get; set; }

    [DataMember]
    public string ExternalId { get; set; }

    [DataMember]
    public bool IsConfidential { get; set; }

    [DataMember]
    public DateTime? CreatedOn { get; set; }

    [DataMember]
    public ContactMethods? InterviewContactPreferences { get; set; }

    [DataMember]
    public bool HasCheckedCriminalRecordExclusion { get; set; }

    [DataMember]
    public ScreeningPreferences? ScreeningPreferences { get; set; }

    [DataMember]
    public DateTime? PostedOn { get; set; }

    [DataMember]
    public DateTime? LastPostedOn { get; set; }

    [DataMember]
    public DateTime? LastRefreshedOn { get; set; }

    [DataMember]
    public DateTime? UpdatedOn { get; set; }

    [DataMember]
    public string JobRequirements { get; set; }

    [DataMember]
    public string JobDetails { get; set; }

    [DataMember]
    public string JobSalaryBenefits { get; set; }

    [DataMember]
    public string JobRecruitmentInformation { get; set; }

		[DataMember]
		public string ForeignLabourCertificationDetails { get; set; }

		[DataMember]
		public string CourtOrderedAffirmativeActionDetails { get; set; }

		[DataMember]
		public string FederalContractorDetails { get; set; }

		[DataMember]
		public bool? CriminalBackgroundExclusionRequired { get; set; }

		[DataMember]
		public DateTime? ClosedOn { get; set; }

    [DataMember]
    public DateTime? VeteranPriorityEndDate { get; set; }

		[DataMember]
		public ExtendVeteranPriorityOfServiceTypes? ExtendVeteranPriority { get; set; }

		[DataMember]
		public bool? CreditCheckRequired { get; set; }

		[DataMember]
		public bool AutoClosed { get; set; }
  }
}
