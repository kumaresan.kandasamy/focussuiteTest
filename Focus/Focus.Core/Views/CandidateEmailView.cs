﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Views
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CandidateEmailView
	{
		/// <summary>
		/// Gets or sets the person id.
		/// </summary>
    [DataMember]
    public long PersonId { get; set; }

		/// <summary>
		/// Gets or sets the name of the candidate.
		/// </summary>
		/// <value>The name of the candidate.</value>
    [DataMember]
    public string CandidateName { get; set; }
		
		/// <summary>
		/// Gets or sets to.
		/// </summary>
    [DataMember]
    public string To { get; set; }

		/// <summary>
		/// Gets or sets the subject.
		/// </summary>
    [DataMember]
    public string Subject { get; set; }

		/// <summary>
		/// Gets or sets the body.
		/// </summary>
    [DataMember]
    public string Body { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [BCC requestor].
		/// </summary>
    [DataMember]
    public bool BccRequestor { get; set; }

		/// <summary>
		/// Gets or sets the job id.
		/// </summary>
		/// <value>The job id.</value>
    [DataMember]
    public long? JobId { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this email is invite to apply for job.
		/// </summary>
		/// <value><c>true</c> if this instance is invite; otherwise, <c>false</c>.</value>
    [DataMember]
    public bool IsInviteToApply { get; set; }

		/// <summary>
		/// Gets or sets the sender address.
		/// </summary>
		/// <value>
		/// The sender address.
		/// </value>
		[DataMember]
		public string SenderAddress { get; set; }

    /// <summary>
    /// Gets or sets whether this is a contact request.
    /// </summary>
    /// <value><c>true</c> if this instance is a contact request, <c>false</c> if a custom message</value>
    [DataMember]
    public bool ContactRequest { get; set; }
	}
}
