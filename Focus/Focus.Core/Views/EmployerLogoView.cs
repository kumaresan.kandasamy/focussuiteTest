﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

#endregion

namespace Focus.Core.Views
{
	[Serializable]
	public class EmployerLogoView
	{
		public long Id { get; set; }
		public long EmployerId { get; set; }
		public string Name { get; set; }
	}
}
