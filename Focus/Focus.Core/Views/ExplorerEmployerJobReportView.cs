﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace Focus.Core.Views
{
  [Serializable]
  public class ExplorerEmployerJobReportView
  {
    [DataMember]
    public EmployerJobViewDto ReportData { get; set; }

    #region Flags

    [DataMember]
    public bool RelatedDegreeAvailableAtCurrentSchool { get; set; }

    #endregion

    #region Constructors

    public ExplorerEmployerJobReportView(){}

    public ExplorerEmployerJobReportView(EmployerJobViewDto reportView)
    {
      ReportData = reportView;
    }

    #endregion
  }
}
