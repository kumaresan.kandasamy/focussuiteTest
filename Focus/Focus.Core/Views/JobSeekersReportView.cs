﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

using Focus.Core.DataTransferObjects.Report;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Views
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public struct JobSeekersReportView
  {
    [DataMember]
    public long Id { get; set; }

    [DataMember]
    public string FirstName { get; set; }

    [DataMember]
    public string MiddleInitial { get; set; }

    [DataMember]
    public string LastName { get; set; }

    [DataMember]
    public string EmailAddress { get; set; }

    [DataMember]
    public string County { get; set; }

    [DataMember]
    public string State { get; set; }

    [DataMember]
    public string Office { get; set; }

    [DataMember]
    public EmploymentStatus? EmploymentStatus { get; set; }

    [DataMember]
    public VeteranEra? VeteranType { get; set; }

    [DataMember]
    public VeteranTransitionType? VeteranTransitionType { get; set; }

    [DataMember]
    public ReportMilitaryDischargeType? VeteranMilitaryDischarge { get; set; }

    [DataMember]
    public ReportMilitaryBranchOfService? VeteranBranchOfService { get; set; }

    [DataMember]
    public long FocusPersonId { get; set; }

    [DataMember]
    public JobSeekerActionView Totals { get; set; }
  }
}
