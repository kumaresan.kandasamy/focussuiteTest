﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Views
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CandidateSystemDefaultsView
	{
		/// <summary>
		/// Gets or sets the alert defaults.
		/// </summary>
		/// <value>The alert defaults.</value>
    [DataMember]
    public AlertDefaultsView AlertDefaults { get; set; }

		/// <summary>
		/// Gets or sets the approval defaults.
		/// </summary>
		/// <value>The approval defaults.</value>
    [DataMember]
    public ApprovalDefaultsView ApprovalDefaults { get; set; }

		/// <summary>
		/// Gets or sets the activity codes.
		/// </summary>
		/// <value>The activity codes.</value>
    [DataMember]
    public List<ActivityCodeView> ActivityCodes { get; set; }

		/// <summary>
		/// Gets or sets the colour defaults.
		/// </summary>
		/// <value>The colour defaults.</value>
    [DataMember]
    public ColourDefaultsView ColourDefaults { get; set; }

		/// <summary>
		/// Gets or sets the message defaults.
		/// </summary>
		/// <value>The message defaults.</value>
    [DataMember]
    public List<ApplicationMessageView> MessageDefaults { get; set; }

		/// <summary>
		/// Gets or sets the resume builder defaults.
		/// </summary>
		/// <value> The resume builder defaults. </value>
		[DataMember]
		public ResumeBuilderDefaultsView ResumeBuilderDefaults { get; set; }
	}

	#region AlertDefaultsView

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class AlertDefaultsView
	{
		/// <summary>
		/// Gets or sets the job alerts option.
		/// </summary>
		/// <value>The job alerts option.</value>
    [DataMember]
    public JobAlertsOptions JobAlertsOption { get; set; }

		/// <summary>
		/// Gets or sets the alert frequency.
		/// </summary>
		/// <value>The alert frequency.</value>
    [DataMember]
    public Frequencies? AlertFrequency { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to send the other jobs of interest email.
		/// </summary>
		/// <value><c>true</c> if to send other jobs email; otherwise, <c>false</c>.</value>
    [DataMember]
    public bool SendOtherJobsEmail { get; set; }
	}

	#endregion

	#region ApprovalDefaultsView

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ApprovalDefaultsView
	{
		/// <summary>
		/// Gets or sets the new resume approvals option.
		/// </summary>
		/// <value>The new resume approvals option.</value>
    [DataMember]
    public NewResumeApprovalsOptions NewResumeApprovalsOption { get; set; }

		/// <summary>
		/// Gets or sets the resume referral approvals option.
		/// </summary>
		/// <value>The resume referral approvals option.</value>
    [DataMember]
    public ResumeReferralApprovalsOptions ResumeReferralApprovalsOption { get; set; }
	}

	#endregion

	#region ActivityCodeView

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ActivityCodeView
	{
		/// <summary>
		/// Gets or sets the name of the activity.
		/// </summary>
		/// <value>The name of the activity.</value>
    [DataMember]
    public string Name { get; set; }

		/// <summary>
		/// Gets or sets the activity code number.
		/// </summary>
		/// <value>The code number.</value>
    [DataMember]
    public long CodeNumber { get; set; }
	}

	#endregion

	#region ColourDefaultsView

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ColourDefaultsView
	{
		/// <summary>
		/// Gets or sets the light colour.
		/// </summary>
		/// <value>The light colour.</value>
    [DataMember]
    public string LightColour { get; set; }

		/// <summary>
		/// Gets or sets the dark colour.
		/// </summary>
		/// <value>The dark colour.</value>
    [DataMember]
    public string DarkColour { get; set; }
	}

	#endregion

	#region ApplicationMessageView

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ApplicationMessageView
	{
		/// <summary>
		/// Gets or sets the name of the message.
		/// </summary>
		/// <value>The name.</value>
    [DataMember]
    public string Name { get; set; }

		/// <summary>
		/// Gets or sets the text of the message.
		/// </summary>
		/// <value>The text.</value>
    [DataMember]
    public string Text { get; set; }

		/// <summary>
		/// Gets or sets the available variables for the message.
		/// </summary>
		/// <value>The available variables.</value>
    [DataMember]
    public List<MessageVariable> AvailableVariables { get; set; }
	}

	#endregion

	#region MessageVariable

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class MessageVariable
	{
		/// <summary>
		/// Gets or sets the variable name.
		/// </summary>
		/// <value>The name.</value>
    [DataMember]
    public string Name { get; set; }

		/// <summary>
		/// Gets or sets the variable tag.
		/// </summary>
		/// <value>The tag.</value>
    [DataMember]
    public string Tag { get; set; }
	}

	#endregion

	#region ResumeBuilderDefaultsView

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ResumeBuilderDefaultsView
	{
		/// <summary>
		/// Gets or sets the type of the college.
		/// </summary>
		/// <value> The type of the college. </value>
    [DataMember]
    public SchoolTypes CollegeType { get; set; }
	}

	#endregion
}
