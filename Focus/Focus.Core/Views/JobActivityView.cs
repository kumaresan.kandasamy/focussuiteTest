﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Views
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobActivityView
	{
		[DataMember]
    public int StarMatches { get; set; }

    [DataMember]
    public int StarApplicants { get; set; }

    [DataMember]
    public int StarDidNotView { get; set; }

    [DataMember]
    public int StarReferrals { get; set; }

    [DataMember]
    public int StaffReferrals { get; set; }

    [DataMember]
    public int SelfReferrals { get; set; }

    [DataMember]
    public int EmployerInvitationsSent { get; set; }

    [DataMember]
    public int SeekersViewed { get; set; }

    [DataMember]
    public int SeekersDidNotView { get; set; }

    [DataMember]
    public int SeekersClicked { get; set; }

    [DataMember]
    public int SeekersDidNotClick { get; set; }

    [DataMember]
    public int ApplicantsInterviewed { get; set; }

    [DataMember]
    public int ApplicantsFailedToShow { get; set; }

    [DataMember]
    public int ApplicantsDenied { get; set; }

    [DataMember]
    public int ApplicantsHired { get; set; }

    [DataMember]
    public int ApplicantsRejected { get; set; }

    [DataMember]
    public int JobOffersRefused { get; set; }

    [DataMember]
    public int ApplicantDidNotApply { get; set; }
	}
}
