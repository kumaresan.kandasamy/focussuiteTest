﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Views
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ValidatedEmployerView 
	{
    [DataMember]
    public long Id { get; set; }

    [DataMember]
    public string ExternalId { get; set; }

    [DataMember]
    public string Name { get; set; }

		[DataMember]
		public string LegalName { get; set; }

    [DataMember]
    public string FederalEmployerIdentificationNumber { get; set; }

    [DataMember]
    public bool IsValidFederalEmployerIdentificationNumber { get; set; }

    [DataMember]
    public string StateEmployerIdentificationNumber { get; set; }

    [DataMember]
    public ApprovalStatuses ApprovalStatus { get; set; }

    [DataMember]
    public long? AddressId { get; set; }

    [DataMember]
    public string AddressLine1 { get; set; }

    [DataMember]
    public string AddressLine2 { get; set; }

    [DataMember]
    public string AddressLine3 { get; set; }

    [DataMember]
    public string AddressTownCity { get; set; }

    [DataMember]
    public long? AddressStateId { get; set; }

    [DataMember]
    public long? AddressCountyId { get; set; }

    [DataMember]
    public string AddressPostcodeZip { get; set; }

    [DataMember]
    public long? AddressCountryId { get; set; }

    [DataMember]
    public string Url { get; set; }

    [DataMember]
    public long? OwnershipTypeId { get; set; }

		[DataMember]
		public long? AccountTypeId { get; set; }

    [DataMember]
    public string IndustrialClassification { get; set; }

    [DataMember]
    public bool TermsAccepted { get; set; }

    [DataMember]
    public string PrimaryPhone { get; set; }

    [DataMember]
    public string PrimaryPhoneExtension { get; set; }

    [DataMember]
    public string PrimaryPhoneType { get; set; }

    [DataMember]
    public string AlternatePhone1 { get; set; }

    [DataMember]
    public string AlternatePhone1Type { get; set; }

    [DataMember]
    public string AlternatePhone2 { get; set; }

    [DataMember]
    public string AlternatePhone2Type { get; set; }

    [DataMember]
    public bool IsRegistrationComplete { get; set; }

    [DataMember]
    public long? BusinessUnitId { get; set; }

    [DataMember]
    public string BusinessUnit { get; set; }

    [DataMember]
    public long? DescriptionId { get; set; }

    [DataMember]
    public string Description { get; set; }

    [DataMember]
    public long? NoOfEmployees { get; set; }

    [DataMember]
    public bool CompleteDataFromIntegrationClient { get; set; }

		[DataMember]
		public bool CompleteDataFromFocus { get; set; }
	}
}
