﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;

#endregion


namespace Focus.Core.Views
{
	public class ResumeSearchResult
	{
		/// <summary>
		/// Gets or sets the resumes.
		/// </summary>
		/// <value>The resumes.</value>
		public PagedList<ResumeView> Resumes { get; set; }

		/// <summary>
		/// Gets or sets the search id.
		/// </summary>
		/// <value>The search id.</value>
		public Guid SearchId { get; set; }

    /// <summary>
    /// Gets or sets the summarised search results.
    /// </summary>
    /// <value>
    /// The summarised search results.
    /// </value>
    public List<Tuple<long, int>> SummarisedSearchResults { get; set; }

		/// <summary>
		/// Gets or sets the highest star rating.
		/// </summary>
		/// <value>
		/// The highest star rating.
		/// </value>
		public int HighestStarRating { get; set; }
	}
}
