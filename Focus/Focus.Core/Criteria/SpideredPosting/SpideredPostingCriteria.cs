﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.SpideredPosting
{
	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SpideredPostingCriteria : CriteriaBase
	{
		[DataMember]
		public string EmployerName { get; set; }

		[DataMember]
		public List<string> StateCodes { get; set; }

		[DataMember]
		public int? PostingAge { get; set; }
	}
}
