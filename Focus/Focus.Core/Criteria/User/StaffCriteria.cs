﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.User
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class StaffCriteria : CriteriaBase
	{
		/// <summary>
		/// Gets or sets the firstname.
		/// </summary>
		/// <value>The firstname.</value>
    [DataMember]
    public string Firstname { get; set; }

		/// <summary>
		/// Gets or sets the lastname.
		/// </summary>
		/// <value>The lastname.</value>
    [DataMember]
    public string Lastname { get; set; }

		/// <summary>
		/// Gets or sets the email address.
		/// </summary>
		/// <value>The email address.</value>
    [DataMember]
    public string EmailAddress { get; set; }

		/// <summary>
		/// Gets or sets the office id.
		/// </summary>
		/// <value>The office id.</value>
    [DataMember]
    public long? OfficeId { get; set; }

		/// <summary>
		/// Gets or sets the is enabled.
		/// </summary>
		/// <value>The is enabled.</value>
    [DataMember]
    public bool? IsEnabled { get; set; }

		/// <summary>
		/// Gets or sets the user id.
		/// </summary>
		/// <value>The user id.</value>
    [DataMember]
    public long? UserId { get; set; }

		/// <summary>
		/// Gets or sets the days back.
		/// </summary>
		/// <value>The days back.</value>
    [DataMember]
    public int? DaysBack { get; set; }

		/// <summary>
		/// Gets or sets the blocked status.
		/// </summary>
		/// <value>
		/// The blocked status.
		/// </value>
		[DataMember]
		public bool? Blocked { get; set; }
	}
}
