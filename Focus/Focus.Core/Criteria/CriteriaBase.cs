﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  [Serializable]
	public abstract class CriteriaBase
	{
		private int _pageSize = 10;
		private int _listSize = 250;

		/// <summary>
		/// If paged result, what page index to fetch
		/// </summary>
		[DataMember]
    public int PageIndex { get; set; }

		/// <summary>
		/// If paged result, what page size per page to fetch
		/// </summary>
    [DataMember]
    public int PageSize
		{
			get { return _pageSize; }
			set { _pageSize = value; }
		}

		/// <summary>
		/// If list result, what list size to fetch
		/// </summary>
    [DataMember]
    public int ListSize
		{
			get { return _listSize; }
			set { _listSize = value; }
		}

		/// <summary>
		/// Sort expression of the criteria.
		/// </summary>
    [DataMember]
    public string OrderBy { get; set; }

    [DataMember]
    public FetchOptions FetchOption { get; set; }

		public enum FetchOptions
		{
			Single,
			List,
			PagedList,
			Grouped,
			Lookup,
      Count
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return string.Format("PageIndex: {0}, PageSize: {1}, ListSize: {2}, OrderBy: {3}, FetchOption: {4}", PageIndex,
													 PageSize, ListSize, OrderBy, FetchOption);
		}
	}
}
