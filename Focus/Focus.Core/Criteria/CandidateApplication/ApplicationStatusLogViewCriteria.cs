﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.CandidateApplication
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ApplicationStatusLogViewCriteria : CriteriaBase
  {
    [DataMember]
    public long? Id { get; set; }

    [DataMember]
    public long? ApplicationId { get; set; }

    [DataMember]
    public long? JobId { get; set; }

    [DataMember]
    public int? DaysSinceStatusChange { get; set; }

    [DataMember]
    public ApplicationStatusTypes? ApplicationStatus { get; set; }

		[DataMember]
		public List<ApprovalStatuses> ApprovalStatuses { get; set; }

    [DataMember]
    public long? BusinessUnitId { get; set; }
    
    [DataMember]
    public JobStatuses? JobStatus { get; set; }

    [DataMember]
    public bool? IgnoreSameStatuses { get; set; }
	}
}
