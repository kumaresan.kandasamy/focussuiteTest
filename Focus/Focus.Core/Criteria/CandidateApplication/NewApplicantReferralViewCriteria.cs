﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.CandidateApplication
{
	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class NewApplicantReferralViewCriteria : CriteriaBase
	{
		// Job Seeker ID
		[DataMember]
		public long? PersonId { get; set; }

    [DataMember]
    public bool ExcludeVeteranPriorityJobs { get; set; }
	}
}
