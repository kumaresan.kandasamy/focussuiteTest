﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Security.Policy;
using Focus.Core.Models.Assist;

#endregion

namespace Focus.Core.Criteria.JobSeeker
{
	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobSeekerCriteria : CriteriaBase
	{
		/// <summary>
		/// Gets or sets the firstname.
		/// </summary>
		/// <value>The firstname.</value>
		[DataMember]
		public string Firstname { get; set; }

		/// <summary>
		/// Gets or sets the lastname.
		/// </summary>
		/// <value>The lastname.</value>
		[DataMember]
		public string Lastname { get; set; }

		/// <summary>
		/// Gets or sets the username.
		/// </summary>
		/// <value>The username.</value>
		[DataMember]
		public string Username { get; set; }

		/// <summary>
		/// Gets or sets the Ssn.
		/// </summary>
		/// <value>The Ssn.</value>
		[DataMember]
		public string Ssn { get; set; }

		/// <summary>
		/// Gets or sets the Ssn list.
		/// </summary>
		/// <value>The Ssn list.</value>
		[DataMember]
		public List<string> SsnList { get; set; }

		/// <summary>
		/// Gets or sets the email address.
		/// </summary>
		/// <value>The email address.</value>
		[DataMember]
		public string EmailAddress { get; set; }

		/// <summary>
		/// Gets or sets the phone number.
		/// </summary>
		/// <value>The phone number.</value>
		[DataMember]
		public string PhoneNumber { get; set; }

		/// <summary>
		/// Gets or sets the days back.
		/// </summary>
		/// <value>The days back.</value>
		[DataMember]
		public int DaysBack { get; set; }

		/// <summary>
		/// Gets or sets the job seeker id.
		/// </summary>
		/// <value>The job seeker id.</value>
		[DataMember]
		public long? JobSeekerId { get; set; }

		/// <summary>
		/// Gets or sets the user id.
		/// </summary>
		/// <value>The user id.</value>
		[DataMember]
		public long? UserId { get; set; }


		/// <summary>
		/// Gets or sets the person id.
		/// </summary>
		/// <value>The person id.</value>
		[DataMember]
		public long PersonId { get; set; }


		/// <summary>
		/// Gets or sets the action.
		/// </summary>
		/// <value>The action.</value>
		[DataMember]
		public ActionTypes? Action { get; set; }

		/// <summary>
		/// Gets or sets the date of birth.
		/// </summary>
		/// <value>
		/// The date of birth.
		/// </value>
		[DataMember]
		public DateTime? DateOfBirth { get; set; }

		/// <summary>
		/// Gets or sets the application status.
		/// </summary>
		/// <value>The application status.</value>
		[DataMember]
		public ApplicationStatusTypes? ApplicationStatus { get; set; }

		/// <summary>
		/// Gets or sets the job seeker activity.
		/// </summary>
		/// <value>The job seeker activity.</value>
		[DataMember]
		public JobSeekerActivityType? JobSeekerActivity { get; set; }

		/// <summary>
		/// Gets or sets the job seeker issue.
		/// </summary>
		/// <value>
		/// The job seeker issue.
		/// </value>
		[DataMember]
		public CandidateIssueType? JobSeekerIssue { get; set; }

		/// <summary>
		/// Gets or sets the job seeker grouped search.
		/// </summary>
		/// <value>
		/// The job seeker grouped search.
		/// </value>
		[DataMember]
		public bool? JobSeekerGroupedIssueSearch { get; set; }

		/// <summary>
		/// Gets or sets the login issue days.
		/// </summary>
		/// <value>
		/// The login issue days.
		/// </value>
		[DataMember]
		public int LoginIssueDays { get; set; }

		/// <summary>
		/// Gets or sets the job seeker school status.
		/// </summary>
		/// <value>The job seeker school status.</value>
		[DataMember]
		public SchoolStatus? JobSeekerSchoolStatus { get; set; }

		/// <summary>
		/// Gets or sets the job seeker school types.
		/// </summary>
		/// <value>The job seeker school types.</value>
		[DataMember]
		public SchoolTypes JobSeekerSchoolType { get; set; }

		/// <summary>
		/// Gets or sets the job seeker program area id.
		/// </summary>
		[DataMember]
		public long? JobSeekerProgramAreaId { get; set; }

		/// <summary>
		/// Gets or sets the job seeker program degree id.
		/// </summary>
		[DataMember]
		public long? JobSeekerProgramDegreeId { get; set; }

		[DataMember]
		public int UserType { get; set; }

		[DataMember]
		public List<long?> OfficeIds { get; set; }

		[DataMember]
		public long? StaffMemberId { get; set; }

		[DataMember]
		public string ExternalId { get; set; }

		/// <summary>
		/// Gets or sets the external seeker id list.
		/// </summary>
		/// <value>
		/// The external seeker id list.
		/// </value>
		[DataMember]
		public List<string> ExternalSeekerIdList { get; set; }

		[DataMember]
		public VeteranFilterTypes VeteranType { get; set; }

		[DataMember]
		public JobSeekerFilterType JobSeekerFilterType { get; set; }

		[DataMember]
		public long? CampusId { get; set; }

		[DataMember]
		public List<long> PersonIds { get; set; }

		[DataMember]
		public bool? JobSeekerUserStatus { get; set; }

		[DataMember]
		public bool? JobSeekerBlockedStatus { get; set; }

    [DataMember]
    public OfficeFilterModel OfficeFilterModel { get; set; }

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return string.Format("First name: {0}, Surname: {1}, Ssn: {2}, EmailAddress: {3}, PhoneNumber: {4}, Username: {5}, {6}", Firstname,
				Lastname, Ssn, EmailAddress, PhoneNumber, Username, base.ToString());
		}
	}
}
