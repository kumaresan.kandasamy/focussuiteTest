﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.JobLocation
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobLocationCriteria : CriteriaBase
	{
    [DataMember]
    public long? JobLocationId;

    [DataMember]
    public long? EmployerId;

    [DataMember]
    public long? JobId;

		[DataMember] public long? BusinessUnitId;
	}
}
