﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Models.Assist;

#endregion

namespace Focus.Core.Criteria.Job
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobCriteria : CriteriaBase
	{
		// Single Record queries
    [DataMember]
    public long? JobId { get; set; }

		// Single / Multiple Record queries
    [DataMember]
    public JobStatuses? JobStatus { get; set; }

		[DataMember]
		public JobTypes? JobType { get; set; }

    [DataMember]
    public List<JobTypes> JobTypes { get; set; }

    [DataMember]
    public long? EmployerId { get; set; }

    [DataMember]
    public string EmployerName { get; set; }

    [DataMember]
    public long? BusinessUnitId { get; set; }

    [DataMember]
    public string BusinessUnitName { get; set; }

    [DataMember]
    public string JobTitle { get; set; }

    [DataMember]
    public long? EmployeeId { get; set; }

    [DataMember]
    public long? ValidateEmployeeId { get; set; }

    [DataMember]
    public long? EmployeeIdForBusinessUnit { get; set; }

    [DataMember]
    public bool? FederalContractor { get; set; }

    [DataMember]
    public bool? CourtOrderedAffirmativeAction { get; set; }

    [DataMember]
    public DateTime PostedOn { get; set; }

    [DataMember]
    public DateTime? CreatedFrom { get; set; }

    [DataMember]
    public DateTime? CreatedTo { get; set; }

    [DataMember]
    public JobListFilter? JobListFilter { get; set; }

    [DataMember]
    public bool? ForeignLaborJobsAll { get; set; }

    [DataMember]
    public bool? ForeignLaborJobsH2A { get; set; }

    [DataMember]
    public bool? ForeignLaborJobsH2B { get; set; }

    [DataMember]
    public bool? ForeignLaborJobsOther { get; set; }

    [DataMember]
    public string HiringManagerUsername { get; set; }
    
    [DataMember]
    public string HiringManagerFirstName { get; set; }
    
    [DataMember]
    public string HiringManagerLastName { get; set; }

    [DataMember]
    public bool TalentDraftJobs { get; set; }

		[DataMember]
		public List<long?> OfficeIds { get; set; }

		[DataMember]
		public long? StaffMemberId { get; set; }

    [DataMember]
    public JobIssuesFilter? JobIssueFilter { get; set; }

		[DataMember]
		public string ExternalId { get; set; }

    [DataMember]
    public OfficeFilterModel OfficeFilterModel { get; set; }
	}
}
