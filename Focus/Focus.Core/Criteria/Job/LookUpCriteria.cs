﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.Job
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobLookupCriteria : CriteriaBase
  {
    [DataMember]
    public long? JobId { get; set; }

    [DataMember]
    public string JobTitle { get; set; }

    [DataMember]
    public string EmployerName { get; set; }

    [DataMember]
    public DateTime? CreatedFrom { get; set; }

    [DataMember]
    public DateTime? CreatedTo { get; set; }
    
    [DataMember]
    public JobStatuses? JobStatus { get; set; }
	}
}
