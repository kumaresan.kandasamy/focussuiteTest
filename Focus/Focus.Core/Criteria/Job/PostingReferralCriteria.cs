﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.Job
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobPostingReferralCriteria : CriteriaBase
	{
    [DataMember]
    public long? Id { get; set; }

		[DataMember]
		public List<long?> OfficeIds { get; set; }

		[DataMember]
		public JobListFilter? JobType { get; set; }

		[DataMember]
		public long? OfficeId { get; set; }

		[DataMember]
		public long? StaffMemberId { get; set; }

		[DataMember]
		public ApprovalStatuses? ApprovalStatus { get; set; }
	}
}
