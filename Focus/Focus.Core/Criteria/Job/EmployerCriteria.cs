﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.Job
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EmployerJobCriteria : CriteriaBase
	{
    [DataMember]
    public long EmployerId { get; set; }

    [DataMember]
    public long StateAreaId { get; set; }

    [DataMember]
    public DegreeLevels DegreeLevel { get; set; }

    [DataMember]
    public long? DegreeEducationLevelId { get; set; }

    [DataMember]
    public long? CareerAreaId { get; set; }

    [DataMember]
    public long? CareerAreaJobTitleId { get; set; }
	}
}
