﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.Employer
{
	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class OfficeCriteria : CriteriaBase
	{
		[DataMember]
		public long? OfficeId { get; set; }

		[DataMember]
		public long? PersonId { get; set; }

		[DataMember]
		public bool? Manager { get; set; }

    [DataMember]
    public bool? DefaultOffice { get; set; }

    [DataMember]
    public OfficeDefaultType? DefaultType { get; set; }

		[DataMember]
		public long? StateId { get; set; }

		[DataMember]
		public long? JobId { get; set; }

		[DataMember]
		public long? EmployerId { get; set; }

		[DataMember]
		public OfficeGroup? OfficeGroup { get; set; }

		[DataMember]
		public bool? InActive { get; set; }

    [DataMember]
    public bool? ReturnNoOfficesForStatewide { get; set; }

		[DataMember]
		public string OfficeName { get; set; }
	}
}
