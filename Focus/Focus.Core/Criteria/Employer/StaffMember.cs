﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.Employer
{
	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class StaffMemberCriteria : CriteriaBase
	{
		[DataMember]
		public long? PersonId { get; set; }

		[DataMember]
		public List<long?> OfficeIds { get; set; }

		[DataMember]
		public bool? Manager { get; set; }

		[DataMember]
		public bool? Statewide { get; set; }

		[DataMember]
		public bool? Enabled { get; set; }

		[DataMember]
		public bool? Blocked { get; set; }

		#region Type ahead search criteria

		[DataMember]
		public bool? CheckNames { get; set; }

		[DataMember]
		public string Name { get; set; }

		#endregion
	}
}
