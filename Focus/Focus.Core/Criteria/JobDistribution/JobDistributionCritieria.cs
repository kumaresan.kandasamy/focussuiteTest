﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.JobDistribution
{
	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobDistributionCriteria : CriteriaBase
	{
		[DataMember(Name = "category")]
		public string Category { get; set; }

		[DataMember(Name = "date")]
		public string Date { get; set; }
	}
}
