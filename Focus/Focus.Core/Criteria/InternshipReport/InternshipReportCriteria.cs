﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;
using Focus.Core.Criteria.Explorer;

#endregion

namespace Focus.Core.Criteria.InternshipReport
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class InternshipReportCriteria : CriteriaBase
	{
		// Multiple Internship Report query
		[DataMember]
    public ExplorerCriteria ReportCriteria { get; set; }
	}
}
