﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.CaseManagement
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CaseManagementCriteria : CriteriaBase
  {
    [DataMember]
    public DateTime? DateSubmittedFrom { get; set; }

    [DataMember]
    public DateTime? DateSubmittedTo { get; set; }

    [DataMember]
    public bool Resent { get; set; }

    [DataMember]
    public bool Pending { get; set; }

    [DataMember]
    public string ErrorDescription { get; set; }

    [DataMember]
    public IntegrationPoint? MessageType { get; set; }



    [DataMember]
    public bool IncludeIgnored { get; set; }
  }
}
