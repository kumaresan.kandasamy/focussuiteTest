﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.BusinessUnit
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class BusinessUnitCriteria : CriteriaBase
	{
		// Single record Request
    [DataMember]
    public long? BusinessUnitId { get; set; }

    [DataMember]
    public string BusinessUnitName { get; set; }

		// Single / Multiple Record queries
    [DataMember]
    public long? EmployerId { get; set; }

    // Criteria to retrieve data given the number of days
    [DataMember]
    public int DaysBack { get; set; }

		[DataMember]
    public string ZipCode { get; set; }

		[DataMember]
		public OfficeGroup OfficeGroup { get; set; }

		[DataMember]
		public long? OfficeId { get; set; }

		[DataMember]
		public bool? IsPreferred { get; set; }
	}
}
