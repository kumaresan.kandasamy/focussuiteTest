﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.CandidateSearch
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CandidateSearchEducationCriteria
	{
		/// <summary>
		/// Gets or sets the education level.
		/// </summary>
		/// <value>The education level.</value>
		[DataMember]
		public EducationLevels EducationLevel { get; set; }
		
			/// <summary>
		/// Gets or sets a value indicating whether [currently enrolled undergraduate].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [currently enrolled undergraduate]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool CurrentlyEnrolledUndergraduate { get; set; }

		/// <summary>
		/// Gets or sets the minimum undergraduate level. Only use if CurrentlyEnrolledUndergraduate is true.
		/// </summary>
		/// <value>The minimum undergraduate level.</value>
		[DataMember]
		public SchoolStatus MinimumUndergraduateLevel { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [currently enrolled graduate].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [currently enrolled graduate]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool CurrentlyEnrolledGraduate { get; set; }

		/// <summary>
		/// Gets or sets the minimum GPA.
		/// </summary>
		/// <value>The minimum GPA.</value>
		[DataMember]
		public double? MinimumGPA { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [search GPA not specified].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [search GPA not specified]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool AlsoSearchGPANotSpecified { get; set; }

		/// <summary>
		/// Gets or sets the minimum experience months.
		/// </summary>
		/// <value>The minimum experience months.</value>
		[DataMember]
		public int MinimumExperienceMonths { get; set; }

		/// <summary>
		/// Gets or sets the programs of study.
		/// </summary>
		/// <value>The programs of study.</value>
		public List<KeyValuePairSerializable<string,string>> ProgramsOfStudy { get; set; }
    
    /// <summary>
    /// Gets the degree ids where 'All Degrees' is selected for a programs of study above
    /// The degree id in this case will actually be the negative value of the program area id
    /// </summary>
    /// <value>The list of degree ids</value>
    public List<string> ExtraDegreeLevelEducationIds { get; set; }

      /// <summary>
    /// Gets or sets the education internship skills
    /// </summary>
    /// <value>The internship skills.</value>
    [DataMember]
    public List<long> InternshipSkills { get; set; }

		/// <summary>
		/// Gets or sets the enrollment status.
		/// </summary>
		/// <value>The enrollment status.</value>
		[DataMember]
		public SchoolStatus? EnrollmentStatus { get; set; }

		/// <summary>
		/// Gets or sets the months until expected completion.
		/// </summary>
		/// <value>The months until expected completion.</value>
		[DataMember]
		public int? MonthsUntilExpectedCompletion { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [include alumni].
		/// </summary>
		/// <value><c>true</c> if [include alumni]; otherwise, <c>false</c>.</value>
		[DataMember]
		public bool IncludeAlumni { get; set; }

		/// <summary>
		/// Gets or sets the education levels.
		/// </summary>
		[DataMember]
		public List<EducationLevel> EducationLevels { get; set; }
	}
}
