﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.CandidateSearch
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CandidateSearchStatusCriteria
	{
		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="CandidateSearchStatusCriteria"/> is veteran.
		/// </summary>
		/// <value><c>true</c> if veteran; otherwise, <c>false</c>.</value>
    [DataMember]
    public bool Veteran { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [US citizen].
		/// </summary>
		/// <value><c>true</c> if [US citizen]; otherwise, <c>false</c>.</value>
    [DataMember]
    public bool USCitizen { get; set; }
	}
}
