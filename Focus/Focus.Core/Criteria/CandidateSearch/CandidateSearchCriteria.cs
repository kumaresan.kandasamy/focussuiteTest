﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.CandidateSearch
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CandidateSearchCriteria : CriteriaBase
  {
	  private bool _logSearch = true;

		/// <summary>
		/// Gets or sets the type of the search.
		/// </summary>
		/// <value>The type of the search.</value>
    [DataMember]
    public CandidateSearchTypes SearchType { get; set; }

		/// <summary>
		/// Gets or sets the type of the scope.
		/// </summary>
		/// <value>The type of the scope.</value>
    [DataMember]
    public CandidateSearchScopeTypes ScopeType { get; set; }

		/// <summary>
		/// Gets or sets the scope set.
		/// </summary>
		/// <value>The scope set.</value>
    [DataMember]
    public List<string> ScopeSet { get; set; }

		/// <summary>
		/// Gets or sets the job details search criteria.
		/// </summary>
		/// <value>The job details search criteria.</value>
    [DataMember]
    public CandidateSearchJobDetailsCriteria JobDetailsCriteria { get; set; }

		/// <summary>
		/// Gets or sets the resume search criteria.
		/// </summary>
		/// <value>The resume search criteria.</value>
    [DataMember]
    public CandidateSearchResumeCriteria ResumeCriteria { get; set; }

		/// <summary>
		/// Gets or sets the keyword search criteria.
		/// </summary>
		/// <value>The keyword search criteria.</value>
    [DataMember]
    public CandidateSearchKeywordCriteria KeywordCriteria { get; set; }

		/// <summary>
		/// Gets or sets the candidates like this search criteria.
		/// </summary>
		/// <value>The candidates like this search criteria.</value>
    [DataMember]
    public CandidatesLikeThisCriteria CandidatesLikeThisSearchCriteria { get; set; }

		/// <summary>
		/// Gets or sets the additional search criteria.
		/// </summary>
		/// <value>The additional search criteria.</value>
    [DataMember]
    public CandidateSearchAdditionalCriteria AdditionalCriteria { get; set; }

		/// <summary>
		/// Gets or sets the minimum document count.
		/// </summary>
		/// <value>The minimum document count.</value>
    [DataMember]
    public int MinimumDocumentCount { get; set; }

		/// <summary>
		/// Gets or sets the minimum score.
		/// </summary>
		/// <value>The minimum score.</value>
    [DataMember]
    public int MinimumScore { get; set; }

		/// <summary>
		/// Gets or sets the candidate registered from.
		/// </summary>
		/// <value>The candidate registered from.</value>
    [DataMember]
    public DateTime? CandidateRegisteredFrom { get; set; }

		/// <summary>
		/// Gets or sets the candidate updated from.
		/// </summary>
		/// <value>
		/// The candidate updated from.
		/// </value>
		public DateTime? CandidateUpdatedFrom { get; set; }

    /// <summary>
    /// Gets or sets the candidateId.
    /// </summary>
    /// <value>The candidate registered from.</value>
    [DataMember]
    public long? CandidateId { get; set; }

		/// <summary>
		/// Gets info relating to Recently matched placements for candidates
		/// </summary>
		/// <value>
		/// 	<c>true</c> if Recently Matched Info required, <c>false</c>.
		/// </value>
    [DataMember]
    public bool GetRecentlyMatchedInfo { get; set; }

    /// <summary>
    /// Whether to get the count of any Assist user notes or reminders
    /// </summary>
    [DataMember]
    public bool GetNotesAndRemindersCount { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [log search].
		/// </summary>
		/// <value>
		///   <c>true</c> if [log search]; otherwise, <c>false</c>.
		/// </value>
	  [DataMember]
	  public bool LogSearch
	  {
		  get { return _logSearch; }
		  set { _logSearch = value; }
	  }
  }

	#region CandidateSearchTypes

	/// <summary>
	/// Enumeration of candidate search types
	/// </summary>
	public enum CandidateSearchTypes
	{
		/// <summary>
		/// Search for candidates by job description.
		/// </summary>
		ByJobDescription,

		/// <summary>
		/// Search for candidates by resume.
		/// </summary>
		ByResume,

		/// <summary>
		/// Search for candidates like this.
		/// </summary>
		CandidatesLikeThis,

		/// <summary>
		/// Search by keywords
		/// </summary>
		ByKeywords,

		/// <summary>
		/// 
		/// </summary>
		OpenSearch,
	}

	#endregion

	#region CandidateSearchScopeTypes

	/// <summary>
	/// Enumeration of candidate search scope types
	/// </summary>
	public enum CandidateSearchScopeTypes
	{
		/// <summary>
		/// Scopes the search to all resumes.
		/// </summary>
		Open,

		/// <summary>
		/// Scopes the search to applicants of a job only.
		/// </summary>
		JobApplicants,

		/// <summary>
		/// Scopes the search to flagged resumes only.
		/// </summary>
		Flagged,

    /// <summary>
    /// Scopes the search to applicants of a job that are pending referral.
    /// </summary>
    JobApplicantsAwaitingApproval,

		/// <summary>
		/// Scopes the search to invitees of a job only.
		/// </summary>
		JobInvitees,
	}

	#endregion
}
