﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.CandidateSearch
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CandidateSearchLocationCriteria
	{
		private bool _excludeUnknownLocations = true;

		/// <summary>
		/// Gets or sets the distance.
		/// </summary>
		/// <value>The distance.</value>
    [DataMember]
    public long Distance { get; set; }

		/// <summary>
		/// Gets or sets the distance units.
		/// </summary>
		/// <value>The distance units.</value>
    [DataMember]
    public DistanceUnits DistanceUnits { get; set; }

		/// <summary>
		/// Gets or sets the postal code.
		/// </summary>
		/// <value>The postal code.</value>
    [DataMember]
    public string PostalCode { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [exclude unknown locations].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [exclude unknown locations]; otherwise, <c>false</c>.
		/// </value>
    [DataMember]
    public bool ExcludeUnknownLocations
		{
			get { return _excludeUnknownLocations; }
			set { _excludeUnknownLocations = value; }
		}
	}
}
