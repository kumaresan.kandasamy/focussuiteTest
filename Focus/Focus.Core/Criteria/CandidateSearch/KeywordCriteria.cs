﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.CandidateSearch
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CandidateSearchKeywordCriteria
	{
		public CandidateSearchKeywordCriteria()
		{
			Operator = LogicalOperators.Or;
		}

		/// <summary>
		/// Gets or sets the keywords.
		/// </summary>
		/// <value>The keywords.</value>
    [DataMember]
    public string Keywords { get; set; }

		/// <summary>
		/// Gets or sets the keyword search context.
		/// </summary>
		/// <value>The context.</value>
    [DataMember]
    public KeywordContexts Context { get; set; }

		/// <summary>
		/// Gets or sets the keyword search scope.
		/// </summary>
		/// <value>The scope.</value>
    [DataMember]
    public KeywordScopes Scope { get; set; }

		/// <summary>
		/// Gets or sets the operator.
		/// </summary>
		/// <value>The operator.</value>
    [DataMember]
    public LogicalOperators Operator { get; set; }
	}
}
