﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.CandidateSearch
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CandidateSearchJobDetailsCriteria
	{
		/// <summary>
		/// Gets or sets the job id.
		/// </summary>
		/// <value>The job id.</value>
    [DataMember]
    public long JobId { get; set; }

		/// <summary>
		/// Gets or sets the job title.
		/// </summary>
		/// <value>The job title.</value>
    [DataMember]
    public string JobTitle { get; set; }

		/// <summary>
		/// Gets or sets the posting.
		/// </summary>
		/// <value>The posting.</value>
    [DataMember]
    public string Posting { get; set; }

    /// <summary>
    /// Gets or sets the posting Id for the migration (will be translated into a Job Id by the import step of the migration process)
    /// </summary>
    /// <value>The posting Id from the migration.</value>
    [DataMember]
    public string MigrationPostingId { get; set; }
	}
}
