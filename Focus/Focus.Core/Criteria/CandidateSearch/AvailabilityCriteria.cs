﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.CandidateSearch
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CandidateSearchAvailabilityCriteria
	{
		/// <summary>
		/// Gets or sets a value indicating whether [work overtime].
		/// </summary>
		/// <value><c>true</c> if [work overtime]; otherwise, <c>false</c>.</value>
    [DataMember]
    public bool? WorkOvertime { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the job seeker is willing to relocate
    /// </summary>
    /// <value><c>true</c> if the job seeker is rilling to relocate; otherwise, <c>false</c>.</value>
    [DataMember]
    public bool? WillingToRelocate { get; set; }

		/// <summary>
		/// Gets or sets the normal work shift id.
		/// </summary>
		/// <value>The normal work shift id.</value>
    [DataMember]
    public long? NormalWorkShiftId { get; set; }

		/// <summary>
		/// Gets or sets the work type id.
		/// </summary>
		/// <value>The work type id.</value>
    [DataMember]
    public long? WorkTypeId { get; set; }

		/// <summary>
		/// Gets or sets the work week id.
		/// </summary>
		/// <value>The work week id.</value>
    [DataMember]
    public long? WorkWeekId { get; set; }
	}
}
