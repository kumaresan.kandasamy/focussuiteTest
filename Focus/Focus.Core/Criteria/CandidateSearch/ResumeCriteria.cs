﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.CandidateSearch
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CandidateSearchResumeCriteria
	{
		/// <summary>
		/// Gets or sets the resume.
		/// </summary>
		/// <value>The resume.</value>
    [DataMember]
    public byte[] Resume { get; set; }

		/// <summary>
		/// Gets or sets the resume file extension.
		/// </summary>
		/// <value>The resume file extension.</value>
    [DataMember]
    public string ResumeFileExtension { get; set; }

		/// <summary>
		/// Gets or sets the name of the resume file.
		/// </summary>
		/// <value>The name of the resume file.</value>
    [DataMember]
    public string ResumeFilename { get; set; }

		/// <summary>
		/// Gets or sets the resume XML.
		/// </summary>
		/// <value>The resume XML.</value>
    [DataMember]
    public string ResumeXml { get; set; }
	}
}
