﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Criteria.CandidateSearch
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CandidateSearchAdditionalCriteria
	{
		/// <summary>
		/// Gets or sets the education search criteria.
		/// </summary>
		/// <value>The education search criteria.</value>
    [DataMember]
    public CandidateSearchEducationCriteria EducationCriteria { get; set; }

		/// <summary>
		/// Gets or sets the status search criteria.
		/// </summary>
		/// <value>The status search criteria.</value>
    [DataMember]
    public CandidateSearchStatusCriteria StatusCriteria { get; set; }

		/// <summary>
		/// Gets or sets the location search criteria.
		/// </summary>
		/// <value>The location search criteria.</value>
    [DataMember]
    public CandidateSearchLocationCriteria LocationCriteria { get; set; }

		/// <summary>
		/// Gets or sets the availability search criteria.
		/// </summary>
		/// <value>The availability search criteria.</value>
    [DataMember]
    public CandidateSearchAvailabilityCriteria AvailabilityCriteria { get; set; }

		/// <summary>
		/// Gets or sets the licences.
		/// </summary>
		/// <value>The licences.</value>
    [DataMember]
    public List<string> Licences { get; set; }

		/// <summary>
		/// Gets or sets the certifications.
		/// </summary>
		/// <value>The certifications.</value>
    [DataMember]
    public List<string> Certifications { get; set; }

		/// <summary>
		/// Gets or sets the languages.
		/// </summary>
		/// <value>The languages.</value>
    [DataMember]
    public List<string> Languages { get; set; }

    /// <summary>
    /// Gets or sets the languages.
    /// </summary>
    /// <value>The language criteria</value>
    [DataMember]
    public CandidateSearchLanguageCriteria LanguagesWithProficiencies { get; set; }

		/// <summary>
		/// Gets or sets the driving licence criteria.
		/// </summary>
		/// <value>The driving licence criteria.</value>
    [DataMember]
    public CandidateSearchDrivingLicenceCriteria DrivingLicenceCriteria { get; set; }

		/// <summary>
		/// Gets or sets the industries criteria.
		/// </summary>
		/// <value>
		/// The industries criteria.
		/// </value>
		[DataMember]
		public CandidateSearchIndustriesCriteria IndustriesCriteria { get; set; }

		/// <summary>
		/// Gets or sets the job types considered.
		/// </summary>
		/// <value>The job types considered.</value>
		[DataMember]
		public JobTypes JobTypesConsidered { get; set; }

    /// <summary>
    /// Whether to return job seekers who prefer home based jobs
    /// </summary>
    [DataMember]
    public bool HomeBasedJobSeekers { get; set; }

    /// <summary>
    /// The National Career Readiness Certificate level
    /// </summary>
    [DataMember]
    public long? NCRCLevel { get; set; }
	}
}
