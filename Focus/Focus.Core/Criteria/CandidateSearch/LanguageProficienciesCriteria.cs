﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Criteria.CandidateSearch
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CandidateSearchLanguageCriteria
	{
    /// <summary>
    /// Gets or sets the language and proficiency level
    /// </summary>
    /// <value>List of Languages and Proficiency IDs</value>
    [DataMember]
    public List<LanguageProficiency> LanguageProficiencies { get; set; }

    /// <summary>
		/// Gets or sets the language criteria Search type (And / Or)
		/// When multiple languages
		/// </summary>
		/// <value>True if "And", False if "Or"</value>
    [DataMember]
    public bool LanguageSearchType { get; set; }
	}
}
