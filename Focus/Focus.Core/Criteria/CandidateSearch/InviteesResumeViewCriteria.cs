﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.CandidateSearch
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class InviteesResumeViewCriteria : CriteriaBase
	{
		/// <summary>
		/// Gets or sets the job identifier.
		/// </summary>
		/// <value>The job identifier.</value>
		[DataMember]
		public long? JobId { get; set; }
	}
}
