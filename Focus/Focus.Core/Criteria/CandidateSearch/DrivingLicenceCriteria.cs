﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.CandidateSearch
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CandidateSearchDrivingLicenceCriteria
	{
		/// <summary>
		/// Gets or sets the driving licence class id.
		/// </summary>
		/// <value>The driving licence class id.</value>
    [DataMember]
    public long? DrivingLicenceClassId { get; set; }

		/// <summary>
		/// Gets or sets the driving licence endorsements ids.
		/// </summary>
		/// <value>The driving licence endorsements ids.</value>
    [DataMember]
    public List<long> DrivingLicenceEndorsementsIds { get; set; }
	}
}
