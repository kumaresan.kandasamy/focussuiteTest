﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Criteria.Explorer
{
	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
  public class ExplorerCriteria : CriteriaBase
	{
		[DataMember]
		public ReportTypes ReportType { get; set; }

		[DataMember]
		public long? ReportItemId { get; set; }

    /// <summary>
    /// Whether to search for All, Job or Military Occupation in the career area
    /// </summary>
    [DataMember]
    public CareerAreaOccupationTypes CareerAreaOccupationType { get; set; }

    [DataMember]
    public long? CareerAreaId { get; set; }
    [DataMember]
    public string CareerArea { get; set; }

    [DataMember]
    public long? CareerAreaJobTitleId { get; set; }
    [DataMember]
    public string CareerAreaJobTitle { get; set; }

    [DataMember]
    public long? CareerAreaJobId { get; set; }
    [DataMember]
    public string CareerAreaJob { get; set; }

    [DataMember]
    public List<long> CareerAreaJobIdList { get; set; }

    [DataMember]
    public long? CareerAreaMilitaryOccupationId { get; set; }
    [DataMember]
    public string CareerAreaMilitaryOccupation { get; set; }
    [DataMember]
    public List<string> CareerAreaMilitaryOccupationROnetCodes { get; set; }

    [DataMember]
    public EducationCriteriaOptions EducationCriteriaOption { get; set; }
    [DataMember]
    public DegreeLevels DegreeLevel { get; set; }
    [DataMember]
    public string DegreeLevelLabel { get; set; }
    [DataMember]
    public long? DegreeEducationLevelId { get; set; }
    [DataMember]
    public String DegreeEducationLevel { get; set; }

    [DataMember]
    public long? ProgramAreaId { get; set; }
    [DataMember]
    public String ProgramArea { get; set; }

    [DataMember]
    public long? DegreeId { get; set; }
    [DataMember]
    public String Degree { get; set; }

    [DataMember]
    public long? InternshipCategoryId { get; set; }
    [DataMember]
    public String InternshipCategory { get; set; }

    [DataMember]
    public long? EmployerId { get; set; }
    [DataMember]
    public string Employer { get; set; }

		[DataMember]
    public long StateAreaId { get; set; }
    [DataMember]
    public string StateArea { get; set; }

		[DataMember]
    public SkillCriteriaOptions SkillCriteriaOption;
    [DataMember]
    public long? SkillCategoryId { get; set; }
    [DataMember]
    public string SkillCategory { get; set; }
    [DataMember]
    public long? SkillSubCategoryId { get; set; }
    [DataMember]
    public string SkillSubCategory { get; set; }
    [DataMember]
    public long? SkillId { get; set; }
    [DataMember]
    public string Skill { get; set; }

    [DataMember]
    public long? SkillJobId { get; set; }
    [DataMember]
    public string SkillJob { get; set; }
    [DataMember]
    public long? SkillJobSkillId { get; set; }
    [DataMember]
    public string SkillJobSkill { get; set; }

		[DataMember]
		public long? PrimaryJobId { get; set; }

    [DataMember]
    public long ROnetId { get; set; }

		public ExplorerCriteria()
		{
			ReportType = ReportTypes.Job;
			EducationCriteriaOption = EducationCriteriaOptions.DegreeLevel;
			DegreeLevel = DegreeLevels.AnyOrNoDegree;
			SkillCriteriaOption = SkillCriteriaOptions.None;
      CareerAreaOccupationType = CareerAreaOccupationTypes.All;
		}

    /// <summary>
    /// Compares the explorer criteria with another
    /// </summary>
    /// <param name="criteria">The criteria to compare</param>
    /// <returns>A boolean indicating if they have the same criteria</returns>
    public bool Compare(ExplorerCriteria criteria)
    {
      return
        ReportItemId.GetValueOrDefault(0) == criteria.ReportItemId.GetValueOrDefault(0)
          && CareerAreaId.GetValueOrDefault(0) == criteria.CareerAreaId.GetValueOrDefault(0)
          && CareerAreaJobTitleId.GetValueOrDefault(0) == criteria.CareerAreaJobTitleId.GetValueOrDefault(0)
          && CareerAreaJobId.GetValueOrDefault(0) == criteria.CareerAreaJobId.GetValueOrDefault(0)
          && CareerAreaMilitaryOccupationId.GetValueOrDefault(0) == criteria.CareerAreaMilitaryOccupationId.GetValueOrDefault(0)
          && EducationCriteriaOption == criteria.EducationCriteriaOption
          && DegreeLevel == criteria.DegreeLevel
          && DegreeEducationLevelId.GetValueOrDefault(0) == criteria.DegreeEducationLevelId.GetValueOrDefault(0)
          && ProgramAreaId.GetValueOrDefault(0) == criteria.ProgramAreaId.GetValueOrDefault(0)
          && DegreeId.GetValueOrDefault(0) == criteria.DegreeId.GetValueOrDefault(0)
          && InternshipCategoryId.GetValueOrDefault(0) == criteria.InternshipCategoryId.GetValueOrDefault(0)
          && EmployerId.GetValueOrDefault(0) == criteria.EmployerId.GetValueOrDefault(0)
          && StateAreaId == criteria.StateAreaId
          && SkillCriteriaOption == criteria.SkillCriteriaOption
          && SkillCategoryId.GetValueOrDefault(0) == criteria.SkillCategoryId.GetValueOrDefault(0)
          && SkillSubCategoryId.GetValueOrDefault(0) == criteria.SkillSubCategoryId.GetValueOrDefault(0)
          && SkillId.GetValueOrDefault(0) == criteria.SkillId.GetValueOrDefault(0)
          && SkillJobId.GetValueOrDefault(0) == criteria.SkillJobId.GetValueOrDefault(0)
          && SkillJobSkillId.GetValueOrDefault(0) == criteria.SkillJobSkillId.GetValueOrDefault(0)
          && PrimaryJobId.GetValueOrDefault(0) == criteria.PrimaryJobId.GetValueOrDefault(0);
    }

    public string FilterLabel()
    {
      var filterString = new StringBuilder();

      if (!string.IsNullOrEmpty(StateArea))
      {
        filterString.Append(StateArea);
        filterString.Append(", ");
      }
      if (!string.IsNullOrEmpty(Employer))
      {
        filterString.Append(Employer);
        filterString.Append(", ");
      }
      if (!string.IsNullOrEmpty(CareerAreaJob))
      {
        filterString.Append(CareerAreaJob);
        filterString.Append(", ");
      }
      else
      {
        if (!string.IsNullOrEmpty(CareerAreaJobTitle) && string.IsNullOrEmpty(CareerAreaJob))
        {
          filterString.Append(CareerAreaJobTitle);
          filterString.Append(", ");
        }
        if (!string.IsNullOrEmpty(CareerArea))
        {
          filterString.Append(CareerArea);
          filterString.Append(", ");
        }
      }

      if (EducationCriteriaOption == EducationCriteriaOptions.DegreeLevel)
      {
        if (!string.IsNullOrEmpty(DegreeLevelLabel))
        {
          filterString.Append(DegreeLevelLabel);
          filterString.Append(", ");
        }
      }
      else if (EducationCriteriaOption == EducationCriteriaOptions.Specific)
      {
        if (!string.IsNullOrEmpty(DegreeEducationLevel))
        {
          filterString.Append(DegreeEducationLevel);
          filterString.Append(", ");
        }
      }
      if (SkillCriteriaOption == SkillCriteriaOptions.SkillByJob)
      {
        if (!string.IsNullOrEmpty(SkillJobSkill))
        {
          filterString.Append(SkillJobSkill);
          filterString.Append(", ");
        }
        else if (!string.IsNullOrEmpty(SkillJob))
        {
          filterString.Append(SkillJob);
          filterString.Append(", ");
        }
      }
      else if (SkillCriteriaOption == SkillCriteriaOptions.SkillByCategory)
      {
        if (!string.IsNullOrEmpty(Skill))
        {
          filterString.Append(Skill);
          filterString.Append(", ");
        }
        else if (!string.IsNullOrEmpty(SkillSubCategory))
        {
          filterString.Append(SkillSubCategory);
          filterString.Append(", ");
        }
        else if (!string.IsNullOrEmpty(SkillCategory))
        {
          filterString.Append(SkillCategory);
          filterString.Append(", ");
        }
      }
      else
      {
        if (!string.IsNullOrEmpty(Skill))
        {
          filterString.Append(Skill);
          filterString.Append(", ");
        }
      }


      if (filterString.Length > 0)
        filterString.Remove(filterString.Length - 2, 2);

      return filterString.ToString();
    }

    /// <summary>
    /// Clones this instance to allow separation of criteria and breadcrumbs.
    /// </summary>
    /// <returns>A copy of current object but with differnt pointer</returns>
    public ExplorerCriteria Clone()
    {
      return new ExplorerCriteria
               {
                 ReportType = ReportType,
                 ReportItemId = ReportItemId,
                 CareerAreaOccupationType = CareerAreaOccupationType,
                 CareerAreaId = CareerAreaId,
                 CareerArea = CareerArea,
                 CareerAreaJobTitleId = CareerAreaJobTitleId,
                 CareerAreaJobTitle = CareerAreaJobTitle,
                 CareerAreaJobId = CareerAreaJobId,
                 CareerAreaJob = CareerAreaJob,
                 CareerAreaMilitaryOccupation = CareerAreaMilitaryOccupation,
                 CareerAreaMilitaryOccupationId = CareerAreaMilitaryOccupationId,
                 EducationCriteriaOption = EducationCriteriaOption,
                 DegreeLevel = DegreeLevel,
                 DegreeLevelLabel = DegreeLevelLabel,
                 DegreeEducationLevelId = DegreeEducationLevelId,
                 DegreeEducationLevel = DegreeEducationLevel,
                 ProgramAreaId = ProgramAreaId,
                 ProgramArea = ProgramArea,
                 DegreeId = DegreeId,
                 Degree = Degree,
                 InternshipCategoryId = InternshipCategoryId,
                 InternshipCategory = InternshipCategory,
                 EmployerId = EmployerId,
                 Employer = Employer,
                 StateAreaId = StateAreaId,
                 StateArea = StateArea,
                 SkillCriteriaOption = SkillCriteriaOption,
                 SkillCategoryId = SkillCategoryId,
                 SkillCategory = SkillCategory,
                 SkillSubCategoryId = SkillSubCategoryId,
                 SkillSubCategory = SkillSubCategory,
                 SkillId = SkillId,
                 Skill = Skill,
                 SkillJobId = SkillJobId,
                 SkillJob = SkillJob,
                 SkillJobSkillId = SkillJobSkillId,
                 SkillJobSkill = SkillJobSkill,
                 PrimaryJobId = PrimaryJobId,
                 PageIndex = PageIndex,
                 PageSize = PageSize,
                 ListSize = ListSize,
                 OrderBy = OrderBy,
                 FetchOption = FetchOption,
               };
    }
	}
}
