﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core.Criteria.Explorer;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.DegreeEducationLevelReport
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class DegreeEducationLevelReportCriteria : CriteriaBase
	{
		// Multiple DegreeEducationLevel Report query
    [DataMember]
    public ExplorerCriteria ReportCriteria { get; set; }
	}
}
