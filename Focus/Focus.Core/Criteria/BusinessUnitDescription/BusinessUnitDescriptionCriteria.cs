﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.BusinessUnitDescription
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class BusinessUnitDescriptionCriteria : CriteriaBase
	{		
		// Single record Request
    [DataMember]
    public long? BusinessUnitDescriptionId { get; set; }

		// Single / Multiple Record queries
    [DataMember]
    public long? BusinessUnitId { get; set; }

    [DataMember]
    public bool IsPrimary { get; set; }

    [DataMember]
    public bool? AssociatedWithJob { get; set; }
	}
}
