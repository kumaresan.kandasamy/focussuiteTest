﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using Framework.Core;

#endregion

namespace Focus.Core.Criteria.Report
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobOrdersReportCriteria : CriteriaBase, IReportCriteria
  {
    #region Implementation of IReportCriteria

    /// <summary>
    /// The type of report
    /// </summary>
    [IgnoreDataMember]
    public ReportType ReportType {get { return ReportType.JobOrder; } }

    /// <summary>
    /// Whether a table or chart is required
    /// </summary>
    [DataMember]
    public ReportDisplayType DisplayType {get; set;}

    /// <summary>
    /// The title of the report
    /// </summary>
    [DataMember]
    public string ReportTitle { get; set; }

    /// <summary>
    /// How to handle rows with zeroes in
    /// </summary>
    [DataMember]
    public ReportZeroesHandling ZeroesHandling { get; set; }

    /// <summary>
    /// Builds the report criteria for display
    /// </summary>
    /// <param name="textLocalise">The function to localise text.</param>
    /// <param name="enumLocalise">The function to localise enums.</param>
    /// <param name="annotatedEnumLocalise">The function to localise enums which have been annotated</param>
    /// <param name="multiColumnDisplay">If set to <c>true</c> return multiple columns of criteria.</param>
    /// <returns>The criteria to display</returns>
    public List<List<string>> BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, EnumLocaliseFunction enumLocalise, AnnotatedEnumLocaliseFunction annotatedEnumLocalise, bool multiColumnDisplay)
    {
      var criteriaLists = new List<List<string>>();

      var columnList = new List<string>();
      criteriaLists.Add(columnList);

      // Column 1
      if (SalaryInfo.IsNotNull())
        SalaryInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      if (JobOrderCharacteristicsInfo.IsNotNull())
        JobOrderCharacteristicsInfo.BuildCriteriaForDisplay(textLocalise, annotatedEnumLocalise, columnList);

      if (KeywordInfo.IsNotNull())
        KeywordInfo.BuildCriteriaForDisplay(textLocalise, enumLocalise, columnList); 
      if (EmployerCharacteristicsInfo.IsNotNull())
        EmployerCharacteristicsInfo.BuildCriteriaForDisplay(textLocalise, enumLocalise, columnList);

      if (JobOrderComplianceInfo.IsNotNull())
        JobOrderComplianceInfo.BuildCriteriaForDisplay(textLocalise, enumLocalise, columnList);

			if(NcrcInfo.IsNotNull())
				NcrcInfo.BuildCriteriaForDisplay(textLocalise, annotatedEnumLocalise, columnList);
      
      if (multiColumnDisplay)
      {
        columnList = new List<string>();
        criteriaLists.Add(columnList);
      }

      // Column 2
      if (JobOrdersActivitySummaryInfo.IsNotNull())
        JobOrdersActivitySummaryInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      if (JobOrdersReferralOutcomesInfo.IsNotNull())
        JobOrdersReferralOutcomesInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      if (multiColumnDisplay)
      {
        columnList = new List<string>();
        criteriaLists.Add(columnList);
      }

      // Column 3
      if (LocationInfo.IsNotNull())
        LocationInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      if (DateRangeInfo.IsNotNull())
        DateRangeInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      return criteriaLists;
    }

    #endregion

    [DataMember]
    public SalaryReportCriteria SalaryInfo { get; set; }

    [DataMember]
    public JobOrderCharacteristicsReportCriteria JobOrderCharacteristicsInfo { get; set; }

    [DataMember]
    public EmployerCharacteristicsReportCriteria EmployerCharacteristicsInfo { get; set; }

    [DataMember]
    public JobOrderComplianceReportCriteria JobOrderComplianceInfo { get; set; }

    [DataMember]
    public KeywordReportCriteria KeywordInfo { get; set; }

    [DataMember]
    public DateRangeReportCriteria DateRangeInfo { get; set; }

    [DataMember]
    public LocationReportCriteria LocationInfo { get; set; }

    [DataMember]
    public JobOrdersActivitySummaryCriteria JobOrdersActivitySummaryInfo { get; set; }

    [DataMember]
    public ReferralOutcomesCriteria JobOrdersReferralOutcomesInfo { get; set; }

    [DataMember]
    public JobOrdersReportChartGroup ChartGroupInfo { get; set; }

    [DataMember]
    public JobOrdersReportSortOrder OrderInfo { get; set; }

		[DataMember]
		public JobOrdersNcrcReportCriteria NcrcInfo { get; set; }

    public JobOrdersReportCriteria()
    {
      // Set default sort order
      OrderInfo = new JobOrdersReportSortOrder { Direction = ReportOrder.Ascending, OrderBy = ReportJobOrderOrderBy.JobTitle };
    }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobOrdersActivitySummaryCriteria
  {
    [DataMember]
    public bool TotalCount { get; set; }

    [DataMember]
    public bool StaffReferrals { get; set; }

    [DataMember]
    public bool SelfReferrals { get; set; }

    [DataMember]
    public bool ReferralsRequested { get; set; }

    [DataMember]
    public bool TotalReferrals { get; set; }

    [DataMember]
    public bool EmployerInvitationsSent { get; set; }

    [DataMember]
    public bool InvitedJobSeekerViewed { get; set; }

    [DataMember]
    public bool InvitedJobSeekerClicked { get; set; }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, List<string> columnData)
    {
      var criteriaHeader = textLocalise("ActivitySummary", "Activity Summary - ");

      if (InvitedJobSeekerViewed)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.InvitedJobSeekerViewed", Constants.Reporting.JobOrderReportDataColumns.InvitedJobSeekerViewed)));
      if (InvitedJobSeekerClicked)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.InvitedJobSeekerClicked", Constants.Reporting.JobOrderReportDataColumns.InvitedJobSeekerClicked)));
      if (EmployerInvitationsSent)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.EmployerInvitationsSent", Constants.Reporting.JobOrderReportDataColumns.EmployerInvitationsSent)));
      if (StaffReferrals)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.StaffReferrals", Constants.Reporting.JobOrderReportDataColumns.StaffReferrals)));
      if (SelfReferrals)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.SelfReferrals", Constants.Reporting.JobOrderReportDataColumns.SelfReferrals)));
      if (ReferralsRequested)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.ReferralsRequested", Constants.Reporting.JobOrderReportDataColumns.ReferralsRequested)));
      if (TotalReferrals)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.TotalReferrals", Constants.Reporting.JobOrderReportDataColumns.TotalReferrals)));
    }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobOrdersReportChartGroup
  {
    [DataMember]
    public ReportJobOrderGroupBy? GroupBy { get; set; }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobOrdersReportSortOrder
  {
    [DataMember]
    public ReportJobOrderOrderBy OrderBy { get; set; }

    [DataMember]
    public ReportOrder Direction { get; set; }
  }
}
