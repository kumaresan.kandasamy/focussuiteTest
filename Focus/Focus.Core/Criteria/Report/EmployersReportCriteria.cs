﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using Framework.Core;

#endregion

namespace Focus.Core.Criteria.Report
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EmployersReportCriteria : CriteriaBase, IReportCriteria
  {
    #region Implementation of IReportCriteria

    /// <summary>
    /// The type of report
    /// </summary>
    [IgnoreDataMember]
    public ReportType ReportType { get { return ReportType.Employer; } }

    /// <summary>
    /// Whether a table or chart is required
    /// </summary>
    [DataMember]
    public ReportDisplayType DisplayType { get; set; }

    /// <summary>
    /// The title of the report
    /// </summary>
    [DataMember]
    public string ReportTitle { get; set; }

    /// <summary>
    /// How to handle rows with zeroes in
    /// </summary>
    [DataMember]
    public ReportZeroesHandling ZeroesHandling { get; set; }

    /// <summary>
    /// Builds the report criteria for display
    /// </summary>
    /// <param name="textLocalise">The function to localise text.</param>
    /// <param name="enumLocalise">The function to localise enums.</param>
    /// <param name="annotatedEnumLocalise">The function to localise enums which have been annotated</param>
    /// <param name="multiColumnDisplay">If set to <c>true</c> return multiple columns of criteria.</param>
    /// <returns>The criteria to display</returns>
    public List<List<string>> BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, EnumLocaliseFunction enumLocalise, AnnotatedEnumLocaliseFunction annotatedEnumLocalise, bool multiColumnDisplay)
    {
      var criteriaLists = new List<List<string>>();

      var columnList = new List<string>();
      criteriaLists.Add(columnList);

      // Column 1
      if (SalaryInfo.IsNotNull())
        SalaryInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      if (JobOrderCharacteristicsInfo.IsNotNull())
        JobOrderCharacteristicsInfo.BuildCriteriaForDisplay(textLocalise, annotatedEnumLocalise, columnList);

      if (EmployerCharacteristicsInfo.IsNotNull())
        EmployerCharacteristicsInfo.BuildCriteriaForDisplay(textLocalise, enumLocalise, columnList);

      if (JobOrderComplianceInfo.IsNotNull())
        JobOrderComplianceInfo.BuildCriteriaForDisplay(textLocalise, enumLocalise, columnList);

      if (KeywordInfo.IsNotNull())
        KeywordInfo.BuildCriteriaForDisplay(textLocalise, enumLocalise, columnList);

      if (multiColumnDisplay)
      {
        columnList = new List<string>();
        criteriaLists.Add(columnList);
      }

      // Column 2
      if (EmployersEmployerActivityInfo.IsNotNull())
        EmployersEmployerActivityInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      if (multiColumnDisplay)
      {
        columnList = new List<string>();
        criteriaLists.Add(columnList);
      }

      // Column 3
      if (LocationInfo.IsNotNull())
        LocationInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      if (DateRangeInfo.IsNotNull())
        DateRangeInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      return criteriaLists;
    }

    #endregion

    [DataMember]
    public SalaryReportCriteria SalaryInfo { get; set; }

    [DataMember]
    public JobOrderCharacteristicsReportCriteria JobOrderCharacteristicsInfo { get; set; }

    [DataMember]
    public EmployerCharacteristicsReportCriteria EmployerCharacteristicsInfo { get; set; }

    [DataMember]
    public JobOrderComplianceReportCriteria JobOrderComplianceInfo { get; set; }

    [DataMember]
    public KeywordReportCriteria KeywordInfo { get; set; }

    [DataMember]
    public DateRangeReportCriteria DateRangeInfo { get; set; }

    [DataMember]
    public LocationReportCriteria LocationInfo { get; set; }

    [DataMember]
    public EmployersActivityCriteria EmployersEmployerActivityInfo { get; set; }

    [DataMember]
    public EmployersReportChartGroup ChartGroupInfo { get; set; }

    [DataMember]
    public EmployersReportSortOrder OrderInfo { get; set; }

    public EmployersReportCriteria()
    {
      // Set default sort order
      OrderInfo = new EmployersReportSortOrder { Direction = ReportOrder.Ascending, OrderBy = ReportEmployerOrderBy.Name };
    }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EmployersActivityCriteria
  {
    [DataMember]
    public bool TotalCount { get; set; }

    [DataMember]
    public bool JobOrdersEdited { get; set; }

    [DataMember]
    public bool JobSeekersInterviewed { get; set; }

    [DataMember]
    public bool JobSeekersHired { get; set; }

    [DataMember]
    public bool JobOrdersCreated { get; set; }

    [DataMember]
    public bool JobOrdersPosted { get; set; }

    [DataMember]
    public bool JobOrdersPutOnHold { get; set; }

    [DataMember]
    public bool JobOrdersRefreshed { get; set; }

    [DataMember]
    public bool JobOrdersClosed { get; set; }

    [DataMember]
    public bool InvitationsSent { get; set; }

    [DataMember]
    public bool JobSeekersNotHired { get; set; }

    [DataMember]
    public bool SelfReferrals { get; set; }

    [DataMember]
    public bool StaffReferrals { get; set; }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, List<string> columnData)
    {
      var criteriaHeader = textLocalise("EmployerActivity", "Employer Activity - ");

      if (JobOrdersClosed)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("EmployerActivity.JobOrdersClosed", Constants.Reporting.EmployerReportDataColumns.JobOrdersClosed)));
      if (JobOrdersCreated)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("EmployerActivity.JobOrdersCreated", Constants.Reporting.EmployerReportDataColumns.JobOrdersCreated)));
      if (JobOrdersEdited)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("EmployerActivity.JobOrdersEdited", Constants.Reporting.EmployerReportDataColumns.JobOrdersEdited)));
      if (InvitationsSent)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("EmployerActivity.InvitationsSent", Constants.Reporting.EmployerReportDataColumns.InvitationsSent)));
      if (JobOrdersPutOnHold)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("EmployerActivity.JobOrdersPutOnHold", Constants.Reporting.EmployerReportDataColumns.JobOrdersPutOnHold)));
      if (JobOrdersPosted)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("EmployerActivity.JobOrdersPosted", Constants.Reporting.EmployerReportDataColumns.JobOrdersPosted)));
      if (JobOrdersRefreshed)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("EmployerActivity.JobOrdersRefreshed", Constants.Reporting.EmployerReportDataColumns.JobOrdersRefreshed)));
      if (SelfReferrals)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("EmployerActivity.SelfReferrals", Constants.Reporting.EmployerReportDataColumns.SelfReferrals)));
      if (StaffReferrals)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("EmployerActivity.StaffReferrals", Constants.Reporting.EmployerReportDataColumns.StaffReferrals)));
      if (JobSeekersInterviewed)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("EmployerActivity.JobSeekersInterviewed", Constants.Reporting.EmployerReportDataColumns.JobSeekersInterviewed)));
      if (JobSeekersHired)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("EmployerActivity.JobSeekersHired", Constants.Reporting.EmployerReportDataColumns.JobSeekersHired)));
      if (JobSeekersNotHired)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("EmployerActivity.JobSeekersNotHired", Constants.Reporting.EmployerReportDataColumns.JobSeekersNotHired)));
    }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EmployersReportChartGroup
  {
    [DataMember]
    public ReportEmployerGroupBy? GroupBy { get; set; }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EmployersReportSortOrder
  {
    [DataMember]
    public ReportEmployerOrderBy OrderBy { get; set; }

    [DataMember]
    public ReportOrder Direction { get; set; }
  }
}
