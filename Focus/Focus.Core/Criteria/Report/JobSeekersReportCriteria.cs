﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using Focus.Core.Models.Career;

using Framework.Core;

#endregion

namespace Focus.Core.Criteria.Report
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobSeekersReportCriteria : CriteriaBase, IReportCriteria
  {
    #region Implementation of IReportCriteria

    /// <summary>
    /// The type of report
    /// </summary>
    [IgnoreDataMember]
    public ReportType ReportType { get { return ReportType.JobSeeker; } }

    /// <summary>
    /// Whether a table or chart is required
    /// </summary>
    [DataMember]
    public ReportDisplayType DisplayType { get; set; }

    /// <summary>
    /// The title of the report
    /// </summary>
    [DataMember]
    public string ReportTitle { get; set; }

    /// <summary>
    /// How to handle rows with zeroes in
    /// </summary>
    [DataMember]
    public ReportZeroesHandling ZeroesHandling { get; set; }

    #endregion

    [DataMember]
    public JobSeekerStatusReportCriteria JobSeekerStatusInfo { get; set; }

    [DataMember]
    public VeteranReportCriteria VeteranInfo { get; set; }

    [DataMember]
    public JobSeekerCharacteristicsReportCriteria CharacteristicsInfo { get; set; }

    [DataMember]
    public QualificationsReportCriteria QualificationsInfo { get; set; }

    [DataMember]
    public SalaryReportCriteria SalaryInfo { get; set; }

    [DataMember]
    public OccupationsReportCriteria OccupationInfo { get; set; }

    [DataMember]
    public IndustriesReportCriteria IndustriesInfo { get; set; }

    [DataMember]
    public ResumeReportCriteria ResumeInfo { get; set; }

    [DataMember]
    public KeywordReportCriteria KeywordInfo { get; set; }

		[DataMember]
		public IndividualJobSeekerCharacteristicsCriteria IndividualCharacteristicsInfo { get; set; }

    [DataMember]
    public DateRangeReportCriteria DateRangeInfo { get; set; }

    [DataMember]
    public LocationReportCriteria LocationInfo { get; set; }

    [DataMember]
    public JobSeekerActivityLogCriteria JobSeekerActivityLogInfo { get; set; }

    [DataMember]
    public JobSeekerActivitySummaryCriteria JobSeekerActivitySummaryInfo { get; set; }

    [DataMember]
    public ReferralOutcomesCriteria JobSeekerReferralOutcomesInfo { get; set; }

    [DataMember]
    public JobSeekersReportSortOrder OrderInfo { get; set; }

    [DataMember]
    public JobSeekersReportExtraColumns ExtraColumns { get; set; }

    [DataMember]
    public JobSeekersReportChartGroup ChartGroupInfo { get; set; }

		[DataMember]
		public JobSeekerNcrcReportCriteria NcrcInfo { get; set; }

		[DataMember]
		public bool SubscribedOnly { get; set; }

    public JobSeekersReportCriteria()
    {
      // Set default sort order
      OrderInfo = new JobSeekersReportSortOrder { Direction = ReportOrder.Ascending, OrderBy = ReportJobSeekerOrderBy.LastName };
    }

    /// <summary>
    /// Builds the job seeker report criteria.
    /// </summary>
    /// <param name="textLocalise">The function to localise text.</param>
    /// <param name="enumLocalise">The function to localise enums.</param>
    /// <param name="annotatedEnumLocalise">The function to localise enums which have been annotated</param>
    /// <param name="multiColumnDisplay">If set to <c>true</c> return multiple columns of criteria.</param>
    /// <returns>The criteria to display</returns>
    public List<List<string>> BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, EnumLocaliseFunction enumLocalise, AnnotatedEnumLocaliseFunction annotatedEnumLocalise, bool multiColumnDisplay)
    {
      var criteriaLists = new List<List<string>>();

      var columnList = new List<string>();
      criteriaLists.Add(columnList);

      // Column 1
      if (JobSeekerStatusInfo.IsNotNull())
        JobSeekerStatusInfo.BuildCriteriaForDisplay(textLocalise, enumLocalise, columnList);

      if (VeteranInfo.IsNotNull())
        VeteranInfo.BuildCriteriaForDisplay(textLocalise, annotatedEnumLocalise, columnList);

      if (CharacteristicsInfo.IsNotNull())
        CharacteristicsInfo.BuildCriteriaForDisplay(textLocalise, enumLocalise, annotatedEnumLocalise, columnList);

      if (QualificationsInfo.IsNotNull())
				QualificationsInfo.BuildCriteriaForDisplay(textLocalise, annotatedEnumLocalise, columnList);

	    if (NcrcInfo.IsNotNull())
		    NcrcInfo.BuildCriteriaForDisplay(textLocalise, annotatedEnumLocalise, columnList);

      if (OccupationInfo.IsNotNull())
        OccupationInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      if (SalaryInfo.IsNotNull())
        SalaryInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      if (IndustriesInfo.IsNotNull())
        IndustriesInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      if (KeywordInfo.IsNotNull())
        KeywordInfo.BuildCriteriaForDisplay(textLocalise, enumLocalise, columnList);

      if (ResumeInfo.IsNotNull())
        ResumeInfo.BuildCriteriaForDisplay(textLocalise, columnList);

			if (IndividualCharacteristicsInfo.IsNotNull())
				IndividualCharacteristicsInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      if (multiColumnDisplay)
      {
        columnList = new List<string>();
        criteriaLists.Add(columnList);
      }

      //Column 2
      if (JobSeekerActivitySummaryInfo.IsNotNull())
        JobSeekerActivitySummaryInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      if (JobSeekerActivityLogInfo.IsNotNull())
        JobSeekerActivityLogInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      if (JobSeekerReferralOutcomesInfo.IsNotNull())
        JobSeekerReferralOutcomesInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      if (multiColumnDisplay)
      {
        columnList = new List<string>();
        criteriaLists.Add(columnList);
      }

      // Column 3
      if (LocationInfo.IsNotNull())
        LocationInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      if (DateRangeInfo.IsNotNull())
        DateRangeInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      return criteriaLists;
    }

  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobSeekerStatusReportCriteria
  {
    [DataMember]
    public List<EmploymentStatus?> EmploymentStatuses { get; set; }
	[DataMember]
	public List<CareerAccountType?> CareerAccountTypes { get; set; }
    [DataMember]
    public List<string> JobSeekerEnabledStatus { get; set; }
    [DataMember]
    public bool JobSeekerBlockedStatus { get; set; }

    public JobSeekerStatusReportCriteria()
    {
      EmploymentStatuses = new List<EmploymentStatus?>();
			CareerAccountTypes = new List<CareerAccountType?>();
    }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="enumLocalise">Function to localise an enumeration</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, EnumLocaliseFunction enumLocalise, ICollection<string> columnData)
    {
	    if (EmploymentStatuses.IsNotNullOrEmpty())
	    {
		    var criteriaHeader = textLocalise("EmploymentStatus.Header", "Employment status - ");

		    if (EmploymentStatuses.Any(x => x.HasValue && x.Value == EmploymentStatus.Employed))
			    columnData.Add(string.Concat(criteriaHeader, enumLocalise(EmploymentStatus.Employed, "Employed")));
		    if (EmploymentStatuses.Any(x => x.HasValue && x.Value == EmploymentStatus.EmployedTerminationReceived))
			    columnData.Add(string.Concat(criteriaHeader,
				    enumLocalise(EmploymentStatus.EmployedTerminationReceived, "Employed with termination notice")));
		    if (EmploymentStatuses.Any(x => x.HasValue && x.Value == EmploymentStatus.UnEmployed))
			    columnData.Add(string.Concat(criteriaHeader, enumLocalise(EmploymentStatus.UnEmployed, "Unemployed")));
	    }
	    if (CareerAccountTypes.IsNotNullOrEmpty())
	    {
				var criteriaHeader = textLocalise("CareerAccountType.Header", "Account Type - ");

		    if (CareerAccountTypes.Any(x => x.HasValue && x.Value == CareerAccountType.JobSeeker))
			    columnData.Add(string.Concat(criteriaHeader, enumLocalise(CareerAccountType.JobSeeker, "Job Seeker")));
		    if (CareerAccountTypes.Any(x => x.HasValue && x.Value == CareerAccountType.Student))
			    columnData.Add(string.Concat(criteriaHeader, enumLocalise(CareerAccountType.Student, "Student")));
	    }
        if (JobSeekerBlockedStatus || JobSeekerEnabledStatus.IsNotNullOrEmpty())
        {
            var criteriaHeader = textLocalise("CareerStatusType.Header", "Job Seeker Status - ");

            if (JobSeekerBlockedStatus)
                columnData.Add(string.Concat(criteriaHeader,textLocalise("BlockedStatus", "Blocked")));

            if (JobSeekerEnabledStatus.IsNotNullOrEmpty())
            {
                if (JobSeekerEnabledStatus.Count == 2)
                {
                    columnData.Add(string.Concat(criteriaHeader,textLocalise("UserEnabledStatus", "Active")));
                    columnData.Add(string.Concat(criteriaHeader,textLocalise("UserNotEnabledStatus", "Inactive")));
                }
                else
                {
                    if (JobSeekerEnabledStatus[0] == "1")
                        columnData.Add(string.Concat(criteriaHeader,textLocalise("UserEnabledStatus", "Active")));
                    else
                        columnData.Add(string.Concat(criteriaHeader,textLocalise("UserNotEnabledStatus", "Inactive")));
                }
            }
        }
    }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class VeteranReportCriteria
  {
    [DataMember]
    public List<VeteranEra?> Types { get; set; }

    [DataMember]
    public List<VeteranTransitionType?> TransitionTypes { get; set; }

    [DataMember]
    public List<ReportMilitaryDischargeType?> MilitaryDischarges { get; set; }

    [DataMember]
    public List<ReportMilitaryBranchOfService?> BranchOfServices { get; set; }

    [DataMember]
    public List<VeteranDisabilityStatus?> DisabilityStatuses { get; set; }

    [DataMember]
    public bool? CampaignVeteran { get; set; }

		[DataMember]
		public bool? HomelessVeteran { get; set; }

    public VeteranReportCriteria()
    {
      Types = new List<VeteranEra?>();
      TransitionTypes = new List<VeteranTransitionType?>();
      MilitaryDischarges = new List<ReportMilitaryDischargeType?>();
      BranchOfServices = new List<ReportMilitaryBranchOfService?>();
      DisabilityStatuses = new List<VeteranDisabilityStatus?>();
    }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="enumLocalise">Function to localise an enumeration</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, AnnotatedEnumLocaliseFunction enumLocalise, ICollection<string> columnData)
    {
      var criteriaHeader = textLocalise("VeteranType.Header", "Veteran type - ");

      if (CampaignVeteran.GetValueOrDefault())
        columnData.Add(string.Concat(criteriaHeader, textLocalise("VeteranType.CampaignVeteran", "Campaign Veteran")));

			if (DisabilityStatuses.IsNotNullOrEmpty())
			{
				if (DisabilityStatuses.Any(x => x.HasValue && x.Value == VeteranDisabilityStatus.Disabled))
					columnData.Add(string.Concat(criteriaHeader, textLocalise("VeteranDisabilityStatus.OtherEligible", "Disabled")));
				if (DisabilityStatuses.Any(x => x.HasValue && x.Value == VeteranDisabilityStatus.SpecialDisabled))
					columnData.Add(string.Concat(criteriaHeader, textLocalise("VeteranDisabilityStatus.SpecialDisabled", "Disabled - Service connected")));
			}

      if (Types.IsNotNullOrEmpty())
      {
        foreach (var type in Types.Where(t => t.HasValue))
        {
          columnData.Add(string.Concat(criteriaHeader, enumLocalise(type, true)));
        }
      }

      if (TransitionTypes.IsNotNullOrEmpty())
      {
        criteriaHeader = textLocalise("VeteranTransitionType.Header", "Veteran transistion type - ");
        foreach (var type in TransitionTypes.Where(t => t.HasValue))
        {
          columnData.Add(string.Concat(criteriaHeader, enumLocalise(type, true)));
        }
      }

      if (MilitaryDischarges.IsNotNullOrEmpty())
      {
        criteriaHeader = textLocalise("ReportMilitaryDischargeType.Header", "Veteran military discharge - ");
        foreach (var discharge in MilitaryDischarges.Where(d => d.HasValue))
        {
          columnData.Add(string.Concat(criteriaHeader, enumLocalise(discharge, true)));
        }
      }

      if (BranchOfServices.IsNotNullOrEmpty())
      {
        criteriaHeader = textLocalise("ReportMilitaryBranchOfService.Header", "Veteran branch of service - ");
        foreach (var branch in BranchOfServices.Where(bos => bos.HasValue))
        {
          columnData.Add(string.Concat(criteriaHeader, enumLocalise(branch, true)));
        }
      }

			if (HomelessVeteran.GetValueOrDefault(false))
			{
				criteriaHeader = textLocalise("HomelessVeteranHeader.Text", "Other");
				columnData.Add(string.Concat(criteriaHeader, " - ", textLocalise("HomelessVeteran.Text", "Homeless")));	
			}
    }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobSeekerCharacteristicsReportCriteria
  {
    [DataMember]
    public List<Genders?> Genders { get; set; }

    [DataMember]
    public int? MinimumAge { get; set; }

    [DataMember]
    public int? MaximumAge { get; set; }

    [DataMember]
    public List<ReportEthnicHeritage?> EthnicHeritages { get; set; }

    [DataMember]
    public List<ReportRace?> ReportRaces { get; set; }

    [DataMember]
    public List<DisabilityStatus?> DisabilityStatuses { get; set; }

    [DataMember]
    public List<ReportDisabilityType?> DisabilityTypes { get; set; }

    public JobSeekerCharacteristicsReportCriteria()
    {
      EthnicHeritages = new List<ReportEthnicHeritage?>();
      ReportRaces = new List<ReportRace?>();
      DisabilityStatuses = new List<DisabilityStatus?>();
      DisabilityTypes = new List<ReportDisabilityType?>();
    }

		/// <summary>
		/// Builds the criteria for display
		/// </summary>
		/// <param name="textLocalise">Function to localise a string of text</param>
		/// <param name="enumLocalise">Function to localise an enumeration</param>
		/// <param name="annotatedEnumLocalise">Function to localise an enumeration using an annotation</param>
		/// <param name="columnData">The column data.</param>
		public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, EnumLocaliseFunction enumLocalise, AnnotatedEnumLocaliseFunction annotatedEnumLocalise, ICollection<string> columnData)
    {
      string criteriaHeader;

      if (MinimumAge.GetValueOrDefault() > 0 && MaximumAge.GetValueOrDefault() > 0)
        columnData.Add(textLocalise("AgeFromTo.Text", "Age - {0} to {1}", MinimumAge.ToString(), MaximumAge.ToString()));
      else if (MinimumAge.GetValueOrDefault() > 0)
        columnData.Add(textLocalise("AgeMinimum.Text", "Age (min) - {0}", MinimumAge.ToString()));
      else if (MaximumAge.GetValueOrDefault() > 0)
        columnData.Add(textLocalise("AgeMaximum.Text", "Age (max) - {0}", MaximumAge.ToString()));

			if (Genders.IsNotNullOrEmpty())
			{
				criteriaHeader = textLocalise("Gender.Text", "Gender - ");
				Genders.ForEach(gender => columnData.Add(string.Concat(criteriaHeader, annotatedEnumLocalise(gender, true))));
			}

      if (EthnicHeritages.IsNotNullOrEmpty())
      {
        criteriaHeader = textLocalise("EthnicityHeritage.Text", "Ethnicity/Heritage - ");
				EthnicHeritages.ForEach(ethnicHeritage => columnData.Add(string.Concat(criteriaHeader, annotatedEnumLocalise(ethnicHeritage, true))));
      }

      if (ReportRaces.IsNotNullOrEmpty())
      {
        criteriaHeader = textLocalise("ReportRaces.Text", "Race - ");
				ReportRaces.ForEach(race => columnData.Add(string.Concat(criteriaHeader, annotatedEnumLocalise(race, true))));
      }

      if (DisabilityTypes.IsNotNullOrEmpty())
      {
        criteriaHeader = textLocalise("DisabilityStatus.Text", "Disability status - ");
				DisabilityTypes.ForEach(disabilityType => columnData.Add(string.Concat(criteriaHeader, annotatedEnumLocalise(disabilityType, true))));
      }
    }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class QualificationsReportCriteria
  {
    [DataMember]
		public List<ReportJobSeekerEducationLevels?> MinimumEducationLevels { get; set; }

    [DataMember]
    public List<string> CertificationAndLicences { get; set; }

    [DataMember]
    public List<string> Skills { get; set; }

    [DataMember]
    public List<string> Internships { get; set; }

    public QualificationsReportCriteria()
    {
			MinimumEducationLevels = new List<ReportJobSeekerEducationLevels?>();
      CertificationAndLicences = new List<string>();
      Skills = new List<string>();
      Internships = new List<string>();
    }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
		/// <param name="annotatedEnumLocalise">Function to localise an enumeration using its annotation</param>
    /// <param name="columnData">The column data.</param>
		public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, AnnotatedEnumLocaliseFunction annotatedEnumLocalise, List<string> columnData)
    {
      if (MinimumEducationLevels.IsNotNullOrEmpty())
      {
        var criteriaHeader = textLocalise("EducationLevel.Text", "Level of education - ");
				MinimumEducationLevels.ForEach(level => columnData.Add(string.Concat(criteriaHeader, annotatedEnumLocalise(level, true))));
      }

      if (CertificationAndLicences.IsNotNullOrEmpty())
        columnData.AddRange(CertificationAndLicences.Select(licence => textLocalise("CertificationAndLicences.Text", "Certification and Licence - {0}", licence)));

      if (Skills.IsNotNullOrEmpty())
        columnData.AddRange(Skills.Select(skill => textLocalise("Skills.Text", "Skill - {0}", skill)));

      if (Internships.IsNotNullOrEmpty())
        columnData.AddRange(Internships.Select(internship => textLocalise("Internships.Text", "Internship - {0}", internship)));
    }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class OccupationsReportCriteria
  {
    [DataMember]
    public List<string> OccupationGroups { get; set; }

    [DataMember]
    public List<string> OccupationCodes { get; set; }

    [DataMember]
    public List<string> MilitaryOccupationCodes { get; set; }

    [DataMember]
    public List<string> MilitaryOccupationTitles { get; set; }

    public OccupationsReportCriteria()
    {
      MilitaryOccupationCodes = new List<string>();
      MilitaryOccupationTitles = new List<string>();
    }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, List<string> columnData)
    {
      if (MilitaryOccupationCodes.IsNotNullOrEmpty())
        columnData.Add(textLocalise("MilitaryOccupationCodes", "MOC codes - {0}", string.Join(",", MilitaryOccupationCodes)));
      if (MilitaryOccupationTitles.IsNotNullOrEmpty())
        columnData.Add(textLocalise("MilitaryOccupationtitles", "MOC titles - {0}", string.Join(",", MilitaryOccupationTitles)));

    }
  }


  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class IndustriesReportCriteria
  {
    [DataMember]
    public List<string> TargetIndustries { get; set; }

    [DataMember]
    public List<string> MostExperienceIndustries { get; set; }

    [DataMember]
    public List<string> MostExperienceIndustriesCodes { get; set; }

    [DataMember]
    public List<string> TargetIndustriesCodes { get; set; }

    public IndustriesReportCriteria()
    {
      TargetIndustries = new List<string>();
      MostExperienceIndustries = new List<string>();
      MostExperienceIndustriesCodes = new List<string>();
      TargetIndustriesCodes = new List<string>();
    }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, List<string> columnData)
    {
      if (TargetIndustries.IsNotNullOrEmpty())
        columnData.Add(textLocalise("TargetIndustries", "Target Industries - {0}", string.Join(",", TargetIndustries)));
      if (MostExperienceIndustries.IsNotNullOrEmpty())
        columnData.Add(textLocalise("MostExperienceIndustries", "Most Experience Industries - {0}", string.Join(",", MostExperienceIndustries)));

    }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ResumeReportCriteria
  {
    [DataMember]
    public bool? HasResume { get; set; }

    [DataMember]
    public bool? HasNoResume { get; set; }

    [DataMember]
    public bool? HasPendingResume { get; set; }

    [DataMember]
    public bool? HasSearchableResume { get; set; }

    [DataMember]
    public bool? HasMultipleResumes { get; set; }

    [DataMember]
    public bool? WillingToWorkOvertime { get; set; }

    [DataMember]
    public bool? WillingToRelocate { get; set; }

    [DataMember]
    public bool? UsedOnlineResumeHelp { get; set; }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, List<string> columnData)
    {
      var criteriaHeader = textLocalise("Resume", "Resume - ");

      if (HasResume.GetValueOrDefault())
        columnData.Add(string.Concat(criteriaHeader, textLocalise("Resume.HasResume", "Has resume")));
      if (HasPendingResume.GetValueOrDefault())
        columnData.Add(string.Concat(criteriaHeader, textLocalise("Resume.HasPendingResume", "Has pending resume")));
      if (HasSearchableResume.GetValueOrDefault()) 
        columnData.Add(string.Concat(criteriaHeader, textLocalise("Resume.HasSearchableResume", "Has searchable resume")));
      if (HasMultipleResumes.GetValueOrDefault()) 
        columnData.Add(string.Concat(criteriaHeader, textLocalise("Resume.HasMultipleResumes", "Has multiple resumes")));
      if (WillingToWorkOvertime.GetValueOrDefault()) 
        columnData.Add(string.Concat(criteriaHeader, textLocalise("Resume.WillingToWorkOvertime", "Willing to work overtime")));
      if (WillingToRelocate.GetValueOrDefault()) 
        columnData.Add(string.Concat(criteriaHeader, textLocalise("Resume.WillingToRelocate", "Willing to relocate")));
      if (UsedOnlineResumeHelp.GetValueOrDefault())
        columnData.Add(string.Concat(criteriaHeader, textLocalise("Resume.UsedOnlineResumeHelp", "Used on-line resume help")));
      if (HasNoResume.GetValueOrDefault())
        columnData.Add(string.Concat(criteriaHeader, textLocalise("Resume.HasNoResume", "Has no resume")));
    }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobSeekerActivitySummaryCriteria
  {
    [DataMember]
    public bool TotalCount { get; set; }

    [DataMember]
    public bool Logins { get; set; }

    [DataMember]
    public bool PostingsViewed { get; set; }

    [DataMember]
    public bool ReferralRequests { get; set; }

    [DataMember]
    public bool StaffReferrals { get; set; }

    [DataMember]
    public bool SelfReferrals { get; set; }

    [DataMember]
    public bool StaffReferralsExternal { get; set; }

    [DataMember]
    public bool SelfReferralsExternal { get; set; }

    [DataMember]
    public bool TargetingHighGrowthSectors { get; set; }

    [DataMember]
    public bool SavedJobAlerts { get; set; }

    [DataMember]
    public bool SurveyVerySatisfied { get; set; }

    [DataMember]
    public bool SurveySatisfied { get; set; }

    [DataMember]
    public bool SurveyDissatisfied { get; set; }

    [DataMember]
    public bool SurveyVeryDissatisfied { get; set; }

    [DataMember]
    public bool SurveyNoUnexpectedMatches { get; set; }

    [DataMember]
    public bool SurveyUnexpectedMatches { get; set; }

    [DataMember]
    public bool SurveyReceivedInvitations { get; set; }

    [DataMember]
    public bool SurveyDidNotReceiveInvitations { get; set; }

    [DataMember]
    public bool ReferralsApproved { get; set; }

    [DataMember]
    public bool TotalReferrals { get; set; }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, List<string> columnData)
    {
      var criteriaHeader = textLocalise("ActivitySummary", "Activity Summary - ");

      if (TotalCount)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.TotalCount", Constants.Reporting.JobSeekerReportDataColumns.TotalAccounts)));
      if (Logins)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.Logins", Constants.Reporting.JobSeekerReportDataColumns.Logins)));
      if (PostingsViewed)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.PostingsViewed", Constants.Reporting.JobSeekerReportDataColumns.PostingsViewed)));
      if (ReferralRequests)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.ReferralRequests", Constants.Reporting.JobSeekerReportDataColumns.ReferralsRequested)));
      if (ReferralsApproved)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.ReferralsApproved", Constants.Reporting.JobSeekerReportDataColumns.ReferralsApproved)));
      if (StaffReferrals)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.StaffReferrals", Constants.Reporting.JobSeekerReportDataColumns.StaffReferrals)));
      if (StaffReferralsExternal)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.StaffReferralsExternal", Constants.Reporting.JobSeekerReportDataColumns.StaffReferralsExternal)));
      if (SelfReferrals)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.SelfReferrals", Constants.Reporting.JobSeekerReportDataColumns.SelfReferrals)));
      if (SelfReferralsExternal)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.SelfReferralsExternal", Constants.Reporting.JobSeekerReportDataColumns.SelfReferralsExternal)));
      if (TotalReferrals)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.TotalReferrals", Constants.Reporting.JobSeekerReportDataColumns.TotalReferrals)));
      if (TargetingHighGrowthSectors)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.TargetingHighGrowthSectors", Constants.Reporting.JobSeekerReportDataColumns.TargetingHighGrowth)));
      if (SavedJobAlerts)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.SavedJobAlerts", Constants.Reporting.JobSeekerReportDataColumns.SavedJobAlert)));
      if (SurveyVerySatisfied)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.SurveyVerySatisfied", Constants.Reporting.JobSeekerReportDataColumns.SurveyVerySatisfied)));
      if (SurveySatisfied)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.SurveySatisfied", Constants.Reporting.JobSeekerReportDataColumns.SurveySatisfied)));
      if (SurveyDissatisfied)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.SurveyDissatisfied", Constants.Reporting.JobSeekerReportDataColumns.SurveyDissatisfied)));
      if (SurveyVeryDissatisfied)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.SurveyVeryDissatisfied", Constants.Reporting.JobSeekerReportDataColumns.SurveyVeryDissatisfied)));
      if (SurveyNoUnexpectedMatches)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.SurveyNoUnexpectedMatches", Constants.Reporting.JobSeekerReportDataColumns.SurveyNoUnexpectedMatches)));
      if (SurveyUnexpectedMatches)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.SurveyUnexpectedMatches", Constants.Reporting.JobSeekerReportDataColumns.SurveyUnexpectedMatches)));
      if (SurveyReceivedInvitations)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.SurveyReceivedInvitations", Constants.Reporting.JobSeekerReportDataColumns.SurveyReceivedInvitations)));
      if (SurveyDidNotReceiveInvitations)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivitySummary.SurveyDidNotReceiveInvitations", Constants.Reporting.JobSeekerReportDataColumns.SurveyDidNotReceiveInvitations)));
    }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobSeekerActivityLogCriteria
  {
    [DataMember]
    public bool NotesAdded { get; set; }

    [DataMember]
    public bool AddedToLists { get; set; }

    [DataMember]
    public bool ActivitiesAssigned { get; set; }

    [DataMember]
    public bool StaffAssignments { get; set; }

    [DataMember]
    public bool FindJobsForSeeker { get; set; }

    [DataMember]
    public bool EmailsSent { get; set; }

    [DataMember]
    public bool IssuesResolved { get; set; }

    [DataMember]
    public bool FollowUpIssues { get; set; }

    [DataMember]
    public Tuple<long?, string> ActivityCategory { get; set; }

    [DataMember]
    public Tuple<long?, string> Activity { get; set; }
    
    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, List<string> columnData)
    {
      var criteriaHeader = textLocalise("ActivityLog", "Activity Log - ");

      if (NotesAdded)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivityLog.NotesAdded", Constants.Reporting.JobSeekerReportDataColumns.NotesAdded)));
      if (AddedToLists)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivityLog.AddedToLists", Constants.Reporting.JobSeekerReportDataColumns.AddedToLists)));
      if (ActivitiesAssigned)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivityLog.ActivitiesAssigned", Constants.Reporting.JobSeekerReportDataColumns.ActivitiesAssigned)));
      if (StaffAssignments)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivityLog.StaffAssignments", Constants.Reporting.JobSeekerReportDataColumns.StaffAssignments)));
      if (FindJobsForSeeker)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivityLog.FindJobsForSeeker", Constants.Reporting.JobSeekerReportDataColumns.FindJobsForSeeker)));
      if (EmailsSent)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivityLog.EmailsSent", Constants.Reporting.JobSeekerReportDataColumns.EmailsSent)));
      if (IssuesResolved)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivityLog.IssuesResolved", Constants.Reporting.JobSeekerReportDataColumns.IssuesResolved)));
      if (FollowUpIssues)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ActivityLog.FollowUpIssues", Constants.Reporting.JobSeekerReportDataColumns.FollowUpIssues)));

      if (ActivityCategory.IsNotNull())
      {
        criteriaHeader = string.Concat(criteriaHeader, textLocalise("ActivitiesServices", Constants.Reporting.JobSeekerReportDataColumns.ActivityServices));
        var activity = Activity.IsNull() ? ActivityCategory.Item2 : string.Concat(ActivityCategory.Item2, "/", Activity.Item2);
        columnData.Add(string.Concat(criteriaHeader, ": ", textLocalise("ActivityLog.Activity", activity)));
      }
    }
  }


  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobSeekersReportExtraColumns
  {
    [DataMember]
    public bool VeteranType { get; set; }

    [DataMember]
    public bool VeteranTransitionType { get; set; }

    [DataMember]
    public bool VeteranMilitaryDischarge { get; set; }

    [DataMember]
    public bool VeteranBranchOfService { get; set; }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobSeekersReportChartGroup
  {
    [DataMember]
    public ReportJobSeekerGroupBy? GroupBy { get; set; }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobSeekersReportSortOrder
  {
    [DataMember]
    public ReportJobSeekerOrderBy OrderBy { get; set; }

    [DataMember]
    public ReportOrder Direction { get; set; }
  }


	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class IndividualJobSeekerCharacteristicsCriteria
	{
		[DataMember]
		public string FirstName { get; set; }

		[DataMember]
		public string LastName { get; set; }

		[DataMember]
		public string Email { get; set; }

		[DataMember]
		public List<string> Ssns { get; set; }

		public IndividualJobSeekerCharacteristicsCriteria()
		{
			Ssns = new List<string>();
		}

		/// <summary>
		/// Builds the criteria for display
		/// </summary>
		/// <param name="textLocalise">Function to localise a string of text</param>
		/// <param name="columnData">The column data.</param>
		public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, ICollection<string> columnData)
		{
			string criteriaHeader;

			if (FirstName.IsNotNullOrEmpty())
				columnData.Add(textLocalise("FirstName.Text", "First name - {0}", FirstName));

			if (LastName.IsNotNullOrEmpty())
				columnData.Add(textLocalise("LastName.Text", "Last name - {0}", LastName));

			if (Email.IsNotNullOrEmpty())
				columnData.Add(textLocalise("EmailAddress.Text", "Email address - {0}", Email));

			if (Ssns.IsNotNullOrEmpty())
			{
				var ssnRegEx = @"^([0-9a-zA-Z]{3})[\- ]?([0-9a-zA-Z]{2})[\- ]?([0-9a-zA-Z]{4})$";
				criteriaHeader = textLocalise("Ssn.Text", "SSN - ");

				Ssns.ForEach(ssn => columnData.Add(string.Concat(criteriaHeader, Regex.Replace(ssn, ssnRegEx, "$1-$2-$3"))));
			}
		}
	}
}
