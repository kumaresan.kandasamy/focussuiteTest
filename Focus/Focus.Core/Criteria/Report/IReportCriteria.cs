﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

#endregion

using System.Collections.Generic;

namespace Focus.Core.Criteria.Report
{
  public interface IReportCriteria
  {
    /// <summary>
    /// The type of report
    /// </summary>
    ReportType ReportType { get; }

    /// <summary>
    /// Whether a table or chart is required
    /// </summary>
    ReportDisplayType DisplayType { get; set; }

    /// <summary>
    /// The title of the report
    /// </summary>
    string ReportTitle { get; set; }

    /// <summary>
    /// How to handle rows with zeroes in
    /// </summary>
    ReportZeroesHandling ZeroesHandling { get; set; }

    /// <summary>
    /// Builds the report criteria for display
    /// </summary>
    /// <param name="textLocalise">The function to localise text.</param>
    /// <param name="enumLocalise">The function to localise enums.</param>
    /// <param name="annotatedEnumLocalise">The function to localise enums which have been annotated</param>
    /// <param name="multiColumnDisplay">If set to <c>true</c> return multiple columns of criteria.</param>
    /// <returns>The criteria to display</returns>
    List<List<string>> BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, EnumLocaliseFunction enumLocalise, AnnotatedEnumLocaliseFunction annotatedEnumLocalise, bool multiColumnDisplay);
  }
}
