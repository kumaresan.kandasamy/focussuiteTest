﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

using Framework.Core;
using Framework.ServiceLocation;

#endregion

namespace Focus.Core.Criteria.Report
{
  public delegate string TextLocaliseFunction(string key, string defaultValue, params object[] args);
  public delegate string EnumLocaliseFunction(Enum enumValue, string defaultValue);
  public delegate string AnnotatedEnumLocaliseFunction(Enum enumValue, bool useAnnotation = false);

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SalaryReportCriteria
  {
    [DataMember]
    public decimal? MinimumSalary { get; set; }

    [DataMember]
    public decimal? MaximumSalary { get; set; }

    [DataMember]
    public ReportSalaryFrequency? SalaryFrequency { get; set; }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, List<string> columnData)
    {
      if (MaximumSalary.GetValueOrDefault() == 0 && MinimumSalary.GetValueOrDefault() == 0)
        return;

      if (MinimumSalary.HasValue && MaximumSalary.HasValue)
        columnData.Add(textLocalise("SalaryToFrom.Text", "Salary - ${0} to ${1} {2}", MinimumSalary.ToString(), MaximumSalary.ToString(), SalaryFrequency.ToString().ToLower()));
      else if (MinimumSalary.HasValue)
        columnData.Add(textLocalise("SalaryFrom.Text", "Salary - from ${0} {1}", MinimumSalary.ToString(), SalaryFrequency.ToString().ToLower()));
      else if (MaximumSalary.HasValue)
        columnData.Add(textLocalise("SalaryFrom.Text", "Salary - upto ${0} {1}", MaximumSalary.ToString(), SalaryFrequency.ToString().ToLower()));
    }
  }

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobOrdersNcrcReportCriteria
	{
		[DataMember]
		public List<string> NcrcLevels { get; set; }

		[DataMember]
		public bool Required { get; set; }

		[DataMember]
		public bool Preferred { get; set; }

		public JobOrdersNcrcReportCriteria()
		{
			NcrcLevels = new List<string>();
			Required = false;
			Preferred = false;
		}

		public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, AnnotatedEnumLocaliseFunction enumLocalise, List<string> columnData)
		{
			if (NcrcLevels.IsNotNullOrEmpty() && (Required || Preferred))
			{
				var criteriaHeader = textLocalise("NcrcCriteriaHeader.Text", "NCRC&trade; - ");
				NcrcLevels.Where(x => !string.IsNullOrWhiteSpace(x)).ToList().ForEach(x =>
				{
					var enumValue = NcrcLevelTypes.Bronze;
					if(Enum.TryParse(x, true, out enumValue) || (x.Split('.').Length > 1 && Enum.TryParse(x.Split('.')[1], true, out enumValue)))
						columnData.Add(string.Format("{0}{1}{2}", criteriaHeader, enumLocalise(enumValue, true), Required && !Preferred ? " required" : Required && Preferred ? " required or preferred" : " preferred"));
				});
			}
		}
	}

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobSeekerNcrcReportCriteria
	{
		[DataMember]
		public List<string> NcrcLevels { get; set; }

		public JobSeekerNcrcReportCriteria()
		{
			NcrcLevels = new List<string>();
		}

		public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, AnnotatedEnumLocaliseFunction enumLocalise, List<string> columnData)
		{
			if (NcrcLevels.IsNotNullOrEmpty())
			{
				var criteriaHeader = textLocalise("NcrcCriteriaHeader.Text", "NCRC&trade; - ");
				NcrcLevels.Where(x => !string.IsNullOrWhiteSpace(x)).ToList().ForEach(x =>
				{
					var enumValue = NcrcLevelTypes.Bronze;
					if (Enum.TryParse(x, true, out enumValue) || (x.Split('.').Length > 1 && Enum.TryParse(x.Split('.')[1], true, out enumValue)))
						columnData.Add(string.Format("{0}{1}", criteriaHeader, enumLocalise(enumValue, true)));
				});
			}
		}
	}

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobOrderCharacteristicsReportCriteria
  {
    [DataMember]
    public List<JobStatuses?> JobStatuses { get; set; }

    [DataMember]
    public List<EducationLevels?> EducationLevels { get; set; }

    public JobOrderCharacteristicsReportCriteria()
    {
      JobStatuses = new List<JobStatuses?>();
      EducationLevels = new List<EducationLevels?>();
    }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="enumLocalise">Function to localise an enumeration</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, AnnotatedEnumLocaliseFunction enumLocalise, List<string> columnData)
    {
      if (JobStatuses.IsNotNullOrEmpty())
        columnData.AddRange(JobStatuses.Select(status => textLocalise("JobStatus.Text", "Job Status - {0}", enumLocalise(status, true))));

      if (EducationLevels.IsNotNullOrEmpty())
        columnData.AddRange(EducationLevels.Select(status => textLocalise("EducationLevel.Text", "Education Level - {0}", enumLocalise(status, true))));
    }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EmployerCharacteristicsReportCriteria
  {
    [DataMember]
    public List<WorkOpportunitiesTaxCreditCategories?> WorkOpportunitiesTaxCreditInterests { get; set; }
		
		[DataMember]
		public List<AccountTypes?> AccountTypesList { get; set; }

    [DataMember]
    // ReSharper disable InconsistentNaming
    public List<string> FEINs { get; set; }
    // ReSharper restore InconsistentNaming

    [DataMember]
    public List<string> EmployerNames { get; set; }

    [DataMember]
    public List<string> JobTitles { get; set; }

    [DataMember]
    public long? NoOfEmployees { get; set; }

    [DataMember]
    public string NoOfEmployeesText { get; set; }

    public EmployerCharacteristicsReportCriteria()
    {
      FEINs = new List<string>();
      EmployerNames = new List<string>();
      JobTitles = new List<string>();
    }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="enumLocalise">Function to localise an enumeration</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, EnumLocaliseFunction enumLocalise, List<string> columnData)
    {
      if (WorkOpportunitiesTaxCreditInterests.IsNotNullOrEmpty())
      {
        var criteriaHeader = textLocalise("WorkOpportunitiesTaxCredit.Text", "WOTC Interest - ");
        if (WorkOpportunitiesTaxCreditInterests.Any(x => x.HasValue && x.Value == WorkOpportunitiesTaxCreditCategories.TemporaryAssistanceToNeedyFamilyRecipients))
          columnData.Add(string.Concat(criteriaHeader, enumLocalise(WorkOpportunitiesTaxCreditCategories.TemporaryAssistanceToNeedyFamilyRecipients, "TANF recipients")));
        if (WorkOpportunitiesTaxCreditInterests.Any(x => x.HasValue && x.Value == WorkOpportunitiesTaxCreditCategories.DesignatedCommunityResidents))
          columnData.Add(string.Concat(criteriaHeader, enumLocalise(WorkOpportunitiesTaxCreditCategories.DesignatedCommunityResidents, "EZ and RRC")));
        if (WorkOpportunitiesTaxCreditInterests.Any(x => x.HasValue && x.Value == WorkOpportunitiesTaxCreditCategories.SNAPRecipients))
          columnData.Add(string.Concat(criteriaHeader, enumLocalise(WorkOpportunitiesTaxCreditCategories.SNAPRecipients, "SNAP recipients")));
        if (WorkOpportunitiesTaxCreditInterests.Any(x => x.HasValue && x.Value == WorkOpportunitiesTaxCreditCategories.Veterans))
          columnData.Add(string.Concat(criteriaHeader, enumLocalise(WorkOpportunitiesTaxCreditCategories.Veterans, "Veterans")));
        if (WorkOpportunitiesTaxCreditInterests.Any(x => x.HasValue && x.Value == WorkOpportunitiesTaxCreditCategories.VocationalRehabilitation))
          columnData.Add(string.Concat(criteriaHeader, enumLocalise(WorkOpportunitiesTaxCreditCategories.VocationalRehabilitation, "Vocational rehabilitation")));
        if (WorkOpportunitiesTaxCreditInterests.Any(x => x.HasValue && x.Value == WorkOpportunitiesTaxCreditCategories.SupplementSecurityIncomeRecipients))
          columnData.Add(string.Concat(criteriaHeader, enumLocalise(WorkOpportunitiesTaxCreditCategories.SupplementSecurityIncomeRecipients, "SSI")));
        if (WorkOpportunitiesTaxCreditInterests.Any(x => x.HasValue && x.Value == WorkOpportunitiesTaxCreditCategories.ExFelons))
          columnData.Add(string.Concat(criteriaHeader, enumLocalise(WorkOpportunitiesTaxCreditCategories.ExFelons, "Ex-Felons")));
        if (WorkOpportunitiesTaxCreditInterests.Any(x => x.HasValue && x.Value == WorkOpportunitiesTaxCreditCategories.SummerYouth))
          columnData.Add(string.Concat(criteriaHeader, enumLocalise(WorkOpportunitiesTaxCreditCategories.SummerYouth, "Summer youth")));
        if (WorkOpportunitiesTaxCreditInterests.Any(x => x.HasValue && x.Value == WorkOpportunitiesTaxCreditCategories.LongTermFamilyAssistanceRecipient))
          columnData.Add(string.Concat(criteriaHeader, enumLocalise(WorkOpportunitiesTaxCreditCategories.LongTermFamilyAssistanceRecipient, "Long term family assistance recipients")));
      }

      if (FEINs.IsNotNullOrEmpty())
        columnData.AddRange(FEINs.Select(fein => textLocalise("FEIN.Text", "FEIN - {0}", fein)));

      if (EmployerNames.IsNotNullOrEmpty())
        columnData.AddRange(EmployerNames.Select(employer => textLocalise("Employer.Text", "Employer - {0}", employer)));

      if (JobTitles.IsNotNullOrEmpty())
        columnData.AddRange(JobTitles.Select(jobTitle => textLocalise("JobTitle.Text", "Job Title - {0}", jobTitle)));
        
      if (NoOfEmployees.IsNotNull() && NoOfEmployees != 0)
      {
        columnData.Add(textLocalise("NoOfEmployees.Text", "Number of Employees: {0}", NoOfEmployeesText));
      }

        
			if (AccountTypesList.IsNotNullOrEmpty())
			{
				var criteriaHeader = textLocalise("AccountTypes.Text", "Account Type - ");

				if (AccountTypesList.Any(x => x.HasValue && x.Value == Core.AccountTypes.NotIndicated))
				{
					columnData.Add(string.Concat(criteriaHeader, enumLocalise(Core.AccountTypes.NotIndicated, "Unspecified Account Type")));
				}

				if (AccountTypesList.Any(x => x.HasValue && x.Value == Core.AccountTypes.DirectEmployer))
				{
					columnData.Add(string.Concat(criteriaHeader, enumLocalise(Core.AccountTypes.DirectEmployer, "Direct Employer Account Type")));
				}

				if (AccountTypesList.Any(x => x.HasValue && x.Value == Core.AccountTypes.StaffingAgency))
				{
					columnData.Add(string.Concat(criteriaHeader, enumLocalise(Core.AccountTypes.StaffingAgency, "Staffing Agency Account Type")));
				}
			}
    }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobOrderComplianceReportCriteria
  {
    [DataMember]
    public List<ForeignLaborTypes?> ForeignLabourTypes { get; set; }

    [DataMember]
    public bool? NonForeignLabourCertification { get; set; }

    [DataMember]
    public bool? FederalContractor { get; set; }

    [DataMember]
    public bool? CourtOrderedAffirmativeAction { get; set; }

    public JobOrderComplianceReportCriteria()
    {
      ForeignLabourTypes = new List<ForeignLaborTypes?>();
    }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="enumLocalise">Function to localise an enumeration</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, EnumLocaliseFunction enumLocalise, List<string> columnData)
    {
      string criteriaHeader;

      if (ForeignLabourTypes.IsNotNullOrEmpty())
      {
        criteriaHeader = textLocalise("ForeignLaborTypes.Text", "Foreign labor certification - ");
        if (ForeignLabourTypes.Any(x => x.HasValue && x.Value == ForeignLaborTypes.ForeignLabourCertificationH2A))
          columnData.Add(string.Concat(criteriaHeader, enumLocalise(ForeignLaborTypes.ForeignLabourCertificationH2A, "H2A agriculture")));
        if (ForeignLabourTypes.Any(x => x.HasValue && x.Value == ForeignLaborTypes.ForeignLabourCertificationH2B))
          columnData.Add(string.Concat(criteriaHeader, enumLocalise(ForeignLaborTypes.ForeignLabourCertificationH2B, "H2B agriculture")));
        if (ForeignLabourTypes.Any(x => x.HasValue && x.Value == ForeignLaborTypes.ForeignLabourCertificationOther))
          columnData.Add(string.Concat(criteriaHeader, enumLocalise(ForeignLaborTypes.ForeignLabourCertificationOther, "Other")));
      }

      criteriaHeader = textLocalise("ComplianceHeader.Text", "Compliance - ");
      if (NonForeignLabourCertification.HasValue)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("Compliance.NonForeignLabourCertification", "Non-foreign labor")));
      if (FederalContractor.HasValue)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("Compliance.FederalContractor", "Federal contract")));
      if (CourtOrderedAffirmativeAction.HasValue)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("Compliance.CourtOrderedAffirmativeAction", "Court ordered affirmative action")));
    }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ReferralOutcomesCriteria
  {
    [DataMember]
    public bool Hired { get; set; }

    [DataMember]
    public bool NotHired { get; set; }

    [DataMember]
    public bool FailedToApply { get; set; }

    [DataMember]
    public bool FailedToReportToInterview { get; set; }

    [DataMember]
    public bool InterviewDenied { get; set; }

    [DataMember]
    public bool InterviewScheduled { get; set; }

    [DataMember]
    public bool NewApplicant { get; set; }

    [DataMember]
    public bool Recommended { get; set; }

    [DataMember]
    public bool RefusedOffer { get; set; }

    [DataMember]
    public bool UnderConsideration { get; set; }

    [DataMember]
    public bool FailedToReportToJob { get; set; }

    [DataMember]
    public bool FailedToRespondToInvitation { get; set; }

    [DataMember]
    public bool FoundJobFromOtherSource { get; set; }

    [DataMember]
    public bool JobAlreadyFilled { get; set; }

    [DataMember]
    public bool NotQualified { get; set; }

    [DataMember]
    public bool NotYetPlaced { get; set; }

    [DataMember]
    public bool RefusedReferral { get; set; }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, List<string> columnData)
    {
      var criteriaHeader = textLocalise("ReferralOutcome", "Referral outcome - ");

      if (FailedToApply)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ReferralOutcome.FailedToApply", Constants.Reporting.SharedReportDataColumns.FailedToApply)));
      if (FailedToReportToInterview)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ReferralOutcome.FailedToReportToInterview", Constants.Reporting.SharedReportDataColumns.FailedToReportToInterview)));
      if (FailedToReportToJob)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ReferralOutcome.FailedToReportToJob", Constants.Reporting.SharedReportDataColumns.FailedToReportToJob)));
      if (FailedToRespondToInvitation)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ReferralOutcome.FailedToRespondToInvitation", Constants.Reporting.SharedReportDataColumns.FailedToRespondToInvitation)));
      if (FoundJobFromOtherSource)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ReferralOutcome.FoundJobFromOtherSource", Constants.Reporting.SharedReportDataColumns.FoundJobFromOtherSource)));
      if (Hired)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ReferralOutcome.Hired", Constants.Reporting.SharedReportDataColumns.Hired)));
      if (InterviewDenied)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ReferralOutcome.InterviewDenied", Constants.Reporting.SharedReportDataColumns.InterviewDenied)));
      if (InterviewScheduled)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ReferralOutcome.InterviewScheduled", Constants.Reporting.SharedReportDataColumns.InterviewScheduled)));
      if (JobAlreadyFilled)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ReferralOutcome.JobAlreadyFilled", Constants.Reporting.SharedReportDataColumns.JobAlreadyFilled)));
      if (NewApplicant)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ReferralOutcome.NewApplicant", Constants.Reporting.SharedReportDataColumns.NewApplicant)));
      if (NotHired)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ReferralOutcome.NotHired", Constants.Reporting.SharedReportDataColumns.NotHired)));
      if (NotQualified)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ReferralOutcome.NotQualified", Constants.Reporting.SharedReportDataColumns.NotQualified)));
      if (NotYetPlaced)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ReferralOutcome.NotYetPlaced", Constants.Reporting.SharedReportDataColumns.NotYetPlaced)));
      if (Recommended)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ReferralOutcome.Recommended", Constants.Reporting.SharedReportDataColumns.Recommended)));
      if (RefusedOffer)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ReferralOutcome.RefusedOffer", Constants.Reporting.SharedReportDataColumns.RefusedOffer)));
      if (RefusedReferral)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ReferralOutcome.RefusedReferral", Constants.Reporting.SharedReportDataColumns.RefusedReferral)));
      if (UnderConsideration)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("ReferralOutcome.UnderConsideration", Constants.Reporting.SharedReportDataColumns.UnderConsideration)));
    }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class KeywordReportCriteria
  {
    [DataMember]
    public List<KeyWord?> KeyWordSearch { get; set; }

    [DataMember]
    public KeywordSearchType? SearchType { get; set; }

    [DataMember]
    public List<string> KeyWords { get; set; }

    public KeywordReportCriteria()
    {
      KeyWords = new List<string>();
    }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="enumLocalise">Function to localise an enumeration</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, EnumLocaliseFunction enumLocalise, ICollection<string> columnData)
    {
      if (KeyWordSearch.IsNotNullOrEmpty() && KeyWords.IsNotNullOrEmpty() && SearchType.IsNotNull())
      {
        var localisedKeyWords = new List<string>();

        if (KeyWordSearch.Contains(KeyWord.EmployerName))
          localisedKeyWords.Add(enumLocalise(KeyWord.EmployerName, "employer name"));
        if (KeyWordSearch.Contains(KeyWord.JobTitle))
          localisedKeyWords.Add(enumLocalise(KeyWord.JobTitle, "job title"));
        if (KeyWordSearch.Contains(KeyWord.JobDescription))
          localisedKeyWords.Add(enumLocalise(KeyWord.JobDescription, "job description"));
        if (KeyWordSearch.Contains(KeyWord.EducationQualifications))
          localisedKeyWords.Add(enumLocalise(KeyWord.EducationQualifications, "educational qualifications"));
        if (KeyWordSearch.Contains(KeyWord.Skills))
          localisedKeyWords.Add(enumLocalise(KeyWord.Skills, "skills"));
        if (KeyWordSearch.Contains(KeyWord.Internships))
          localisedKeyWords.Add(enumLocalise(KeyWord.Internships, "internships"));
        if (KeyWordSearch.Contains(KeyWord.FullResume))
          localisedKeyWords.Add(enumLocalise(KeyWord.FullResume, "full resume"));
        if (KeyWordSearch.Contains(KeyWord.FullJob))
          localisedKeyWords.Add(enumLocalise(KeyWord.FullJob, "full job"));

        var criteriaTextKey = (KeyWords.Count == 1) ? "KeywordSearchOne.Text" : "KeywordSearch.Text";
        var criteriaText = (KeyWords.Count == 1)
                             ? "Keyword search - Search for the phrase \"{1}\" in the fields: {2}"
                             : "Keyword search - Search for {0} of the phrases \"{1}\" in the fields: {2}";

        columnData.Add(textLocalise(criteriaTextKey, criteriaText,
                                      SearchType.ToString().ToLower(),
                                      string.Join("\", \"", KeyWords),
                                      string.Join(", ", localisedKeyWords)));
      }
    }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class DateRangeReportCriteria
  {
    [DataMember]
    public DateTime? FromDate { get; set; }

    [DataMember]
    public DateTime? ToDate { get; set; }

    [DataMember]
    public int? Days { get; set; }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, List<string> columnData)
    {
      var criteriaHeader = textLocalise("DateRangeHeader.Text", "Date range - ");

      if (Days.HasValue)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("LastXDays", "Last {0} days", Days.ToString())));
      else if (FromDate.HasValue && ToDate.HasValue)
        columnData.Add(string.Concat(criteriaHeader, textLocalise("FromTo", "From {0} to {1}", FromDate.Value.ToString("d"), ToDate.Value.ToString("d"))));
    }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class LocationReportCriteria
  {
    [DataMember]
    public List<string> Counties { get; set; }

    [DataMember]
    public List<string> Offices { get; set; }

    [DataMember]
    public string PostalCode { get; set; }

    [DataMember]
    public int? DistanceFromPostalCode { get; set; }

    public LocationReportCriteria()
    {
      Counties = new List<string>();
      Offices = new List<string>();
    }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, List<string> columnData)
    {
      if (Counties.IsNotNullOrEmpty())
        columnData.AddRange(Counties.Select(county => textLocalise("County.Text", "County - {0}", county)));

      if (Offices.IsNotNullOrEmpty())
        columnData.AddRange(Offices.Select(office => textLocalise("Office.Text", "Office - {0}", office)));

      if (DistanceFromPostalCode.GetValueOrDefault() > 0 && PostalCode.IsNotNullOrEmpty())
        columnData.Add(textLocalise("DistanceFrom.Text", "Distance - {0} miles from {1}", DistanceFromPostalCode.ToString(), PostalCode));
    }
  }
}
