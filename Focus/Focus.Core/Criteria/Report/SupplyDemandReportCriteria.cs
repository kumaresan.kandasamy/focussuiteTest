﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Framework.Core;

#endregion

namespace Focus.Core.Criteria.Report
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SupplyDemandReportCriteria : CriteriaBase, IReportCriteria
  {
    #region Implementation of IReportCriteria

    /// <summary>
    /// The type of report
    /// </summary>
    [IgnoreDataMember]
    public ReportType ReportType { get { return ReportType.SupplyDemand; } }

    /// <summary>
    /// Whether a table or chart is required
    /// </summary>
    [DataMember]
    public ReportDisplayType DisplayType { get; set; }

    /// <summary>
    /// The title of the report
    /// </summary>
    [DataMember]
    public string ReportTitle { get; set; }

    /// <summary>
    /// How to handle rows with zeroes in
    /// </summary>
    [DataMember]
    public ReportZeroesHandling ZeroesHandling { get; set; }

    #endregion

    [DataMember]
    public SupplyDemandOccupationsReportCriteria OccupationInfo { get; set; } 

    [DataMember]
    public SupplyDemandIndustriesReportCriteria IndustriesInfo { get; set; }

    [DataMember]
    public KeywordReportCriteria KeywordInfo { get; set; }

    [DataMember]
    public DateRangeReportCriteria DateRangeInfo { get; set; }

    [DataMember]
    public SupplyDemandLocationReportCriteria LocationInfo { get; set; }

   [DataMember]
    public SupplyDemandReportSortOrder OrderInfo { get; set; }

    [DataMember]
    public SupplyDemandReportChartGroup ChartGroupInfo { get; set; }

    [DataMember]
    public bool SubscribedOnly { get; set; }

    public SupplyDemandReportCriteria()
    {
      // Set default sort order
      OrderInfo = new SupplyDemandReportSortOrder { Direction = ReportOrder.Descending, OrderBy = ReportSupplyDemandOrderBy.OnetCode };
    }

    /// <summary>
    /// Builds the supply demand report criteria.
    /// </summary>
    /// <param name="textLocalise">The function to localise text.</param>
    /// <param name="enumLocalise">The function to localise enums.</param>
    /// <param name="annotatedEnumLocalise">The function to localise enums which have been annotated</param>
    /// <param name="multiColumnDisplay">If set to <c>true</c> return multiple columns of criteria.</param>
    /// <returns>The criteria to display</returns>
    public List<List<string>> BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, EnumLocaliseFunction enumLocalise, AnnotatedEnumLocaliseFunction annotatedEnumLocalise, bool multiColumnDisplay)
    {
      var criteriaLists = new List<List<string>>();

      var columnList = new List<string>();
      criteriaLists.Add(columnList);

      // Column 1
      if (OccupationInfo.IsNotNull())
        OccupationInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      if (IndustriesInfo.IsNotNull())
        IndustriesInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      if (KeywordInfo.IsNotNull())
        KeywordInfo.BuildCriteriaForDisplay(textLocalise, enumLocalise, columnList);

      if (multiColumnDisplay)
      {
        columnList = new List<string>();
        criteriaLists.Add(columnList);
      }

      //Column 2
      
      if (multiColumnDisplay)
      {
        columnList = new List<string>();
        criteriaLists.Add(columnList);
      }

      // Column 3
      if (LocationInfo.IsNotNull())
        LocationInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      if (DateRangeInfo.IsNotNull())
        DateRangeInfo.BuildCriteriaForDisplay(textLocalise, columnList);

      return criteriaLists;
    }

  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SupplyDemandLocationReportCriteria
  {
    [DataMember]
    public Tuple<long, string> State { get; set; }

    [DataMember]
		public Tuple<long, string> MSA { get; set; }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, List<string> columnData)
    {
      if (State.IsNotNull() && State.Item1 > 0)
        columnData.Add(textLocalise("State.Text", "State - {0}", State.Item2));

      if (MSA.IsNotNull() && MSA.Item1 > 0)
        columnData.Add(textLocalise("MSA.Text", "MSA - {0}", MSA.Item2));
    }
  }


  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SupplyDemandOccupationsReportCriteria
  {
    [DataMember]
    public string OnetId { get; set; }

    [DataMember]
    public long OnetFamily { get; set; }

    [DataMember]
    public string OnetTitle { get; set; }

    [DataMember]
    public List<string> MilitaryOccupationCodes { get; set; }

    [DataMember]
    public List<string> MilitaryOccupationTitles { get; set; }

    public SupplyDemandOccupationsReportCriteria()
    {
      MilitaryOccupationCodes = new List<string>();
      MilitaryOccupationTitles = new List<string>();
    }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, List<string> columnData)
    {
      if (MilitaryOccupationCodes.IsNotNullOrEmpty())
        columnData.Add(textLocalise("MilitaryOccupationCodes", "MOC codes - {0}", string.Join(",", MilitaryOccupationCodes)));
      if (MilitaryOccupationTitles.IsNotNullOrEmpty())
        columnData.Add(textLocalise("MilitaryOccupationtitles", "MOC titles - {0}", string.Join(",", MilitaryOccupationTitles)));

    }
  }


  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SupplyDemandIndustriesReportCriteria
  {
    [DataMember]
    public List<string> TargetIndustries { get; set; }

    [DataMember]
    public List<string> MostExperienceIndustries { get; set; }

    [DataMember]
    public List<string> MostExperienceIndustriesCodes { get; set; }

    [DataMember]
    public List<string> TargetIndustriesCodes { get; set; }

    public SupplyDemandIndustriesReportCriteria()
    {
      TargetIndustries = new List<string>();
      MostExperienceIndustries = new List<string>();
      MostExperienceIndustriesCodes = new List<string>();
      TargetIndustriesCodes = new List<string>();
    }

    /// <summary>
    /// Builds the criteria for display
    /// </summary>
    /// <param name="textLocalise">Function to localise a string of text</param>
    /// <param name="columnData">The column data.</param>
    public void BuildCriteriaForDisplay(TextLocaliseFunction textLocalise, List<string> columnData)
    {
      if (TargetIndustries.IsNotNullOrEmpty())
        columnData.Add(textLocalise("TargetIndustries", "Target Industries - {0}", string.Join(",", TargetIndustries)));
      if (MostExperienceIndustries.IsNotNullOrEmpty())
        columnData.Add(textLocalise("MostExperienceIndustries", "Most Experience Industries - {0}",
                                    string.Join(",", MostExperienceIndustries)));
    }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SupplyDemandReportChartGroup
  {
    [DataMember]
    public ReportSupplyDemandGroupBy? GroupBy { get; set; }
  }

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SupplyDemandReportSortOrder
  {
    [DataMember]
    public ReportSupplyDemandOrderBy OrderBy { get; set; }

    [DataMember]
    public ReportOrder Direction { get; set; }
  }
}
