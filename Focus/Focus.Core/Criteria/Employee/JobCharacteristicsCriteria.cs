﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.Employee
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobCharacteristicsCriteria
  {
    [DataMember]
    public List<string> OccupationGroups { get; set; }

    [DataMember]
    public List<string> OccupationTitles { get; set; }

    [DataMember]
    public Frequencies? SalaryFrequency { get; set; }

    [DataMember]
    public int? SalaryMinimum { get; set; }

    [DataMember]
    public int? SalaryMaximum { get; set; }

    [DataMember]
    public bool? ShowPostedJobs { get; set; }

    [DataMember]
    public bool? ShowRegisteredJobs { get; set; }

    [DataMember]
    public bool? ShowExpiredJobs { get; set; }

    [DataMember]
    public bool? ShowClosedJobs { get; set; }

    [DataMember]
    public EducationLevels? LevelOfEducation { get; set; }

    [DataMember]
    public bool? ShowUnspecifiedEducationJobs { get; set; }

    [DataMember]
    public List<string> SearchTerms { get; set; }
    
    [DataMember]
    public bool? MatchAllSearchTerms { get; set; }

    [DataMember]
    public bool? MatchSearchTermsInJobTitle { get; set; }
	}
}
