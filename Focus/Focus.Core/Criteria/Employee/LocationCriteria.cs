﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.Employee
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ReportLocationCriteria
	{
		/// <summary>
		/// Gets or sets the distance.
		/// </summary>
		/// <value>The distance.</value>
    [DataMember]
    public long Distance { get; set; }

		/// <summary>
		/// Gets or sets the distance units.
		/// </summary>
		/// <value>The distance units.</value>
    [DataMember]
    public DistanceUnits DistanceUnits { get; set; }

		/// <summary>
		/// Gets or sets the postal code.
		/// </summary>
		/// <value>The postal code.</value>
    [DataMember]
    public string PostalCode { get; set; }

		/// <summary>
		/// Gets or sets the county.
		/// </summary>
		/// <value>The county.</value>
    [DataMember]
    public List<string> Counties { get; set; }

		/// <summary>
		/// Gets or sets the offices.
		/// </summary>
		/// <value>The offices.</value>
    [DataMember]
    public List<string> Offices { get; set; }

		/// <summary>
		/// Gets or sets the workforce investment boards.
		/// A group of zip codes will be assigned to each Workforce Investment Board
		/// </summary>
		/// <value>The workforce investment boards.</value>
    [DataMember]
    public List<string> WorkforceInvestmentBoards { get; set; }
	}
}
