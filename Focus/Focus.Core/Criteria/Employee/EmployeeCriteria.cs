﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Criteria.Employee
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EmployeeCriteria : CriteriaBase
	{
    /// <summary>
    /// Gets or sets the Employee Id
    /// </summary>
    [DataMember]
    public long? Id { get; set; }

		/// <summary>
		/// Gets or sets the firstname.
		/// </summary>
		/// <value>The firstname.</value>
    [DataMember]
    public string Firstname { get; set; }

    /// <summary>
		/// Gets or sets the lastname.
		/// </summary>
		/// <value>The lastname.</value>
    [DataMember]
    public string Lastname { get; set; }

		/// <summary>
		/// Gets or sets the email address.
		/// </summary>
		/// <value>The email address.</value>
    [DataMember]
    public string EmailAddress { get; set; }

    [DataMember]
    public long? BusinessUnitId { get; set; }

		/// <summary>
		/// Gets or sets the location.
		/// </summary>
		/// <value>The location.</value>
    [DataMember]
    public ReportLocationCriteria Location { get; set; }

		/// <summary>
		/// Gets or sets the job characteristics.
		/// </summary>
		/// <value>The job characteristics.</value>
    [DataMember]
    public JobCharacteristicsCriteria JobCharacteristics { get; set; }

		/// <summary>
		/// Gets or sets the employer characteristics.
		/// </summary>
		/// <value>The employer characteristics.</value>
    [DataMember]
    public EmployerCharacteristicsCriteria EmployerCharacteristics { get; set; }

		/// <summary>
		/// Gets or sets the compliance.
		/// </summary>
		/// <value>The compliance.</value>
    [DataMember]
    public ComplianceCriteria Compliance { get; set; }

    [DataMember]
    public List<long> EmployerIds { get; set; }

    [DataMember]
    public List<long> BusinessUnitIds { get; set; }

		[DataMember]
		public bool SubscribedOnly { get; set; }

		[DataMember]
		public FindEmployerStatusTypes StatusTypes { get; set; }
  }
}
