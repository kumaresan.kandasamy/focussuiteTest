﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.Common;

#endregion

namespace Focus.Core.Criteria.Employee
{
  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EmployerCharacteristicsCriteria
  {
    [DataMember]
    public List<string> Sectors { get; set; }

    [DataMember]
    public WorkOpportunitiesTaxCreditCategories WorkOpportunitiesTaxCreditInterests { get; set; }

    [DataMember]
    public string FederalEmployerIdentificationNumber { get; set; }

    [DataMember]
    public List<string> FederalEmployerIdentificationNumbers { get; set; }

    [DataMember]
    public string EmployerName { get; set; }

    [DataMember]
    public string JobTitle { get; set; }

    [DataMember]
    public bool? IsGreenJob { get; set; }

    [DataMember]
    public bool? IsITEmployer { get; set; }

    [DataMember]
    public bool? IsHealthcareEmployer { get; set; }

    [DataMember]
    public bool? IsBiotechEmployer { get; set; }

    [DataMember]
    public DateCriteria AccountCreationDate { get; set; }

    [DataMember]
    public bool? JobScreeningAssistanceRequested { get; set; }

    [DataMember]
    public List<string> BusinessUnits { get; set; }
	}
}
