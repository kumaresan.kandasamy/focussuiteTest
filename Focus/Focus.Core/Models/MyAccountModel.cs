﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives



#endregion

using System.Runtime.Serialization;

namespace Focus.Core.Models
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class MyAccountModel
	{
    [DataMember]
    public string ScreenName { get; set; }

    [DataMember]
    public string SecurityQuestion { get; set; }

		[DataMember]
		public long? SecurityQuestionId { get; set; }

    [DataMember]
    public ExplorerResumeModel ResumeModel { get; set; }
	}
}
