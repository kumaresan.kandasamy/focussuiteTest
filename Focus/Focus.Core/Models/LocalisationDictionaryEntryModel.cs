﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Common.Models
{
  [DataContract]
  public class LocalisationDictionaryEntry
  {
		/// <summary>
		/// Initializes a new instance of the <see cref="LocalisationDictionaryEntry"/> class.
		/// </summary>
	  public LocalisationDictionaryEntry()
	  { }

		/// <summary>
		/// Initializes a new instance of the <see cref="LocalisationDictionaryEntry"/> class.
		/// </summary>
		/// <param name="localiseKey">The localise key.</param>
		/// <param name="defaultValue">The default value.</param>
		public LocalisationDictionaryEntry(string localiseKey, string defaultValue)
		{
			LocaliseKey = localiseKey;
			LocaliseKeyUpper = localiseKey.ToUpper();
			DefaultValue = defaultValue;
		}

    [DataMember]
    public string DefaultValue { get; set; }
    [DataMember]
    public string LocalisedValue { get; set; }
    [DataMember]
    public string LocaliseKey { get; set; }
    [DataMember]
    public string LocaliseKeyUpper { get; set; }
  }  
}
