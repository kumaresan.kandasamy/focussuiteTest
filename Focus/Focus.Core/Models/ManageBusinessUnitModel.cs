﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Models
{
	public class ManageBusinessUnitModel
	{
		public List<BusinessUnitDescriptionDto> BusinessUnitDescriptions { get; set; }

		public List<BusinessUnitLogoDto> BusinessUnitLogos { get; set; }

		public List<BusinessUnitDescriptionDto> BusinessUnitDescriptionsAssociatedWithJobs { get; set; }

		public List<BusinessUnitLogoDto> LogosAssociatedWithJobs { get; set; } 
	}
}
