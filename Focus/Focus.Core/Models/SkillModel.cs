﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Models
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SkillModel
	{
    [DataMember]
    public long SkillId { get; set; }

    [DataMember]
    public SkillTypes SkillType { get; set; }

    [DataMember]
    public string SkillName { get; set; }

    [DataMember]
    public decimal DemandPercentile { get; set; }

    [DataMember]
    public int Rank { get; set; }
	}
}
