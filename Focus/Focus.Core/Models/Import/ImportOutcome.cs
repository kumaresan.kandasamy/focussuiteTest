﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Focus.Core.Models.Import
{
  public class ImportOutcome : IImportOutcome
  {
    [DataMember]
    public Guid ImportBatch { get; set; }

    [DataMember]
    public EntityTypes Entity { get; set; }

    [DataMember]
    public long FocusId { get; set; }

    [DataMember]
    public string ExternalId { get; set; }

    [DataMember]
    public bool Success { get; set; }

    [DataMember]
    public string ErrorMessage { get; set; }
  }
}
