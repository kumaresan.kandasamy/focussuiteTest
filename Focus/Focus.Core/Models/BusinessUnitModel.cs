﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Models
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class BusinessUnitModel
	{
    [DataMember]
    public BusinessUnitDto BusinessUnit { get; set; }

    [DataMember]
    public BusinessUnitAddressDto BusinessUnitAddress { get; set; }

    [DataMember]
    public BusinessUnitDescriptionDto BusinessUnitDescription { get; set; }

		[DataMember]
		public int? LockVersion { get; set; }
	}
}
