﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Models.Career
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ExternalSystemInfo
	{
    [DataMember]
    public ExternalSystem SystemId { get; set; }

    [DataMember]
    public EntityTypes EntityType { get; set; }

    [DataMember]
    public string EntityId { get; set; }

    [DataMember]
    public object AuxiliaryInfo { get; set; }
	}
}
