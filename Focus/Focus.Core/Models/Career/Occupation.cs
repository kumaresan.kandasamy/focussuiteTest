﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Models.Career
{

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class Occupation
	{
    [DataMember]
    public string OnetCode { get; set; }

    [DataMember]
    public string JobTitle { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class OnetCode
	{
    [DataMember]
    public string CareerArea { get; set; }

    [DataMember]
    public string Code { get; set; }

    [DataMember]
    public string RonetCode { get; set; }

    [DataMember]
    public string Occupation { get; set; }

    [DataMember]
    public string Description { get; set; }
    
    [DataMember]
    public int? StarRating { get; set; } 
	}

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class Question
	{
    [DataMember]
    public JobTaskTypes QuestionType { get; set; }

    [DataMember]
    public bool IsCertificate { get; set; }

    [DataMember]
    public string Prompt { get; set; }
    
    [DataMember]
    public string Response { get; set; }

    [DataMember]
    public List<string> Options { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class Word
	{
    [DataMember]
    public int Score { get; set; }
    
    [DataMember]
    public string Keyword { get; set; }
	}
}
