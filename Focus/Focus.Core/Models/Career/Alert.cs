﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Models.Career
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class Alert
	{
    [DataMember]
    public string AlertId { get; set; }

    [DataMember]
    public string Name { get; set; }

    [DataMember]
    public EmailAlertFrequencies Frequency { get; set; }

    [DataMember]
    public EmailFormats Format { get; set; }

    [DataMember]
    public AlertStatus Status { get; set; }

    [DataMember]
    public string ToEmailAddress { get; set; }

    [DataMember]
    public DateTime ModifiedOn { get; set; }

    [DataMember]
    public bool EmailRequired { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobAlert : Alert
  {
    [DataMember]
    public SearchCriteria JobSearchCriteria { get; set; }
	}

	public enum AlertStatus
	{
		Inactive = 0,
		Active = 1
	}
}


