﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

using Framework.Core;
using Framework.Core.Attributes;

#endregion

namespace Focus.Core.Models.Career
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ResumeDocument : ISearchDocument
	{
    [DataMember]
    public string DocumentId { get; set; }

    [DataMember]
    public int MatchingScore { get; set; }

    [DataMember]
    public ResumeStatuses DocumentStatus { get; set; }

    [DataMember]
    public string SeekerName { get; set; }

    [DataMember]
    public float YearsOfExperience { get; set; }

    [DataMember]
    public List<Job> WorkHistory { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ResumeInfo
	{
    [DataMember]
    public float? YearsOfExperience { get; set; }

    [DataMember]
    public string CurrentJob { get; set; }

    [DataMember]
    public string PreviousJob { get; set; }

    [DataMember]
    public List<string> Skills { get; set; }

    [DataMember]
    public List<string> Certificates { get; set; }

    [DataMember]
    public string DegreeLevel { get; set; }
	}

	#region Resume

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ResumeModel
	{
    [DataMember]
    public ResumeEnvelope ResumeMetaInfo { get; set; }

    [DataMember]
    public ResumeBody ResumeContent { get; set; }

    [DataMember]
    public ResumeSpecialInfo Special { get; set; }
	}

	#region ResumeEnvelope

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public partial class ResumeEnvelope
	{
		#region CANON Info

		//ResDoc/resume/@canonversion
    [DataMember]
    public string CanonVersion { get; set; }

		#endregion

		#region Resume Info

    [DataMember]
    public string ResumeName { get; set; }

    [DataMember]
    public long? ResumeId { get; set; }

    [DataMember]
    public int ResumeVersion { get; set; }

    [DataMember]
    public bool? IsDefault { get; set; }

    [DataMember]
    public string ResumeOnet { get; set; }

    [DataMember]
    public string ResumeROnet { get; set; }

    [DataMember]
    public ResumeStatuses ResumeStatus { get; set; }

    [DataMember]
    public ResumeCompletionStatuses CompletionStatus { get; set; }

    [DataMember]
    public ResumeCreationMethod ResumeCreationMethod { get; set; }

    [DataMember]
    public DateTime? LastUpdated { get; set; }
		
		#endregion

		#region Resume Actors and Activities

    [DataMember]
    public List<ResumeActivity> ResumeOperations { get; set; }

		#endregion
	}

	public partial class ResumeEnvelope
  {
    [DataMember]
    public List<ExternalSystemInfo> ESIResumeInfo { get; set; }
	}

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ResumeActivity
	{
    [DataMember]
    public ResumeOperation Operation { get; set; }

    [DataMember]
    public string OperatedBy { get; set; }

    [DataMember]
    public DateTime OperatedOn { get; set; }
	}

	#endregion

	#region SpecialInfo

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ResumeSpecialInfo
	{
		//resumestatus/@export
		public bool? IsResumeExported { get; set; }

		#region Preference
    
    [DataMember]
    public Preferences Preferences { get; set; }
		
    #endregion

		#region Veteran Info
		
    //ResDoc/resume/statements/personal/veteran/past_five_years_veteran
    [DataMember]
    public bool? IsPastFiveYearsVeteran { get; set; }
		
    #endregion

		#region Hide Resume Detail Info

    [DataMember]
    public HideResumeInfo HideInfo { get; set; }
		
    #endregion

		#region UI Registration Info
		
    //ResDoc/special/seeking_ui_benefits
    [DataMember]
    public bool? IsSeekingUIBenefits { get; set; }
		
    #endregion

		//ResDoc/special/preserved_format
    [DataMember]
    public long? PreservedFormatId { get; set; }
		
    //ResDoc/special/preserved_order
    [DataMember]
    public string PreservedOrder { get; set; }

		//ResDoc/skillrollup/canonskill/@name
    [DataMember]
    public List<string> Canonskills { get; set; }

		//ResDoc/special/branding
		[DataMember]
		public string Branding { get; set; }
	}

	#region Preferences

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class Preferences
	{
		//ResDoc/resume/statements/personal/preferences/sal
    [DataMember]
    public float? Salary { get; set; }
		
    //ResDoc/resume/statements/personal/preferences/salary_unit_cd
    [DataMember]
    public long? SalaryFrequencyId { get; set; }

		//ResDoc/resume/statements/personal/preferences/resume_searchable
    [DataMember]
    public bool? IsResumeSearchable { get; set; }

		//ResDoc/resume/statements/personal/preferences/bridges_opp_interest
		[DataMember]
		public bool? BridgestoOppsInterest { get; set; }

		[IgnoreDataMember]
		public bool BridgestoOppsInterestChanged { get; set; }
    
		//ResDoc/resume/statements/personal/preferences/contact_info_visible
		[DataMember]
		public bool? IsContactInfoVisible { get; set; }
		
    //ResDoc/resume/statements/personal/preferences/postings_interested_in
    [DataMember]
    public JobTypes PostingsInterestedIn { get; set; }
		
    //ResDoc/resume/statements/personal/preferences/email_flag
    [DataMember]
    public bool? IsContactByEmail { get; set; }
		
    //ResDoc/resume/statements/personal/preferences/postal_flag
    [DataMember]
    public bool? IsContactAtPostalAddress { get; set; }
		
    //ResDoc/resume/statements/personal/preferences/phone_pri_flag
    [DataMember]
    public bool? IsContactAtPriPhoneNumber { get; set; }
		
    //ResDoc/resume/statements/personal/preferences/phone_sec_flag
    [DataMember]
    public bool? IsContactAtSecPhoneNumber { get; set; }
		
    //ResDoc/resume/statements/personal/preferences/fax_flag
    [DataMember]
    public bool? IsContactByFax { get; set; }
		
    //ResDoc/resume/statements/personal/preferences/relocate
    [DataMember]
    public bool? WillingToRelocate { get; set; }

    [DataMember]
    public Shift ShiftDetail { get; set; }

    [DataMember]
    public bool? SearchInMyState { get; set; }

    [DataMember]
    public List<DesiredZip> ZipPreference { get; set; }

    [DataMember]
    public List<DesiredCity> CityPreference { get; set; }

    [DataMember]
    public List<DesiredMSA> MSAPreference { get; set; }

    [DataMember]
    public List<DesiredState> StatePreference { get; set; }

		//[DataMember]
		//public bool? HomeBasedJobPostings { get; set; }

		/// <summary>
		/// Gets or sets the title preference.
		/// </summary>
		/// <value>
		/// The title preference.
		/// </value>
		[DataMember]
		public List<DesiredTitle>  TitlePreference { get; set; }
	}

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class Shift
	{
		//ResDoc/resume/statements/personal/preferences/shift/work_week
    [DataMember]
    public long? WorkWeekId { get; set; }
		
    //ResDoc/resume/statements/personal/preferences/shift/work_type
    [DataMember]
    public long? WorkDurationId { get; set; }
		
    //ResDoc/resume/statements/personal/preferences/shift/work_over_time
    [DataMember]
    public bool? WorkOverTime { get; set; }

    [DataMember]
    public List<long> ShiftTypeIds { get; set; }

    [DataMember]
    public List<string> ShiftTypes { get; set; }
	}

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class DesiredZip
	{
		//ResDoc/resume/statements/personal/preferences/desired_zips/desired_zip/choice
    [DataMember]
    public int? Choice { get; set; }
		
    //ResDoc/resume/statements/personal/preferences/desired_zips/desired_zip/zip
    [DataMember]
    public string Zip { get; set; }

    //ResDoc/resume/statements/personal/preferences/desired_zips/desired_zip/radius
    [DataMember]
    public int? Radius { get; set; }

    //ResDoc/resume/statements/personal/preferences/desired_zips/desired_zip/radius
    [DataMember]
    public long? RadiusId { get; set; }
		
    //ResDoc/resume/statements/personal/preferences/desired_zips/desired_zip/statecode
    [DataMember]
    public long? StateId { get; set; }

		/// <summary>
		/// Gets or sets the external identifier.
		/// </summary>
		/// <value>
		/// The external identifier.
		/// </value>
		[DataMember]
		public string ExternalId { get; set; }
	}

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class DesiredCity
	{
    [DataMember]
    public int? Choice { get; set; }

    [DataMember]
    public string CityName { get; set; }
		
    //ResDoc/resume/statements/personal/preferences/desired_states/desired_state/code
    [DataMember]
    public long? StateId { get; set; }
		
    //ResDoc/resume/statements/personal/preferences/desired_states/desired_state/value
    [DataMember]
    public string StateName { get; set; }

		/// <summary>
		/// Gets or sets the external identifier.
		/// </summary>
		/// <value>
		/// The external identifier.
		/// </value>
		[DataMember]
		public string ExternalId { get; set; }
	}

	/// <summary>
	/// A desired title element.
	/// </summary>
	[Serializable, DataContract(Namespace = Constants.DataContractNamespace)]
	public class DesiredTitle
	{
		/// <summary>
		/// Gets or sets the external identifier.
		/// </summary>
		/// <value>
		/// The external identifier.
		/// </value>
		[DataMember]
		public string ExternalId { get; set; }

		/// <summary>
		/// Gets or sets the onet.
		/// </summary>
		/// <value>
		/// The onet.
		/// </value>
		[DataMember]
		public string Onet { get; set; }

		/// <summary>
		/// Gets or sets the experience in months.
		/// </summary>
		/// <value>
		/// The experience in months.
		/// </value>
		[DataMember]
		public int ExperienceInMonths { get; set; }
	}

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class DesiredMSA
	{
    [DataMember]
    public int? Choice { get; set; }

    [DataMember]
    public long? MSAId { get; set; }

    [DataMember]
    public long? StateId { get; set; }

    [DataMember]
    public string StateCode { get; set; }

    [DataMember]
    public string StateName { get; set; }
	}

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class DesiredState
	{
    [DataMember]
    public int? Choice { get; set; }
		
    //ResDoc/resume/statements/personal/preferences/desired_states/desired_state/code
    [DataMember]
    public string StateCode { get; set; }
		
    //ResDoc/resume/statements/personal/preferences/desired_states/desired_state/value
    [DataMember]
    public string StateName { get; set; }

		/// <summary>
		/// Gets or sets the external identifier.
		/// </summary>
		/// <value>
		/// The external identifier.
		/// </value>
		[DataMember]
		public string ExternalId { get; set; }
	}

	#endregion

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class HideResumeInfo
	{
    //ResDoc/resume/hide_options/hide_daterange
    [DataMember]
    public bool HideDateRange { get; set; }
    
    //ResDoc/resume/hide_options/hide_eduDates
    [DataMember]
    public bool HideEducationDates { get; set; }
    
    //ResDoc/resume/hide_options/hide_militaryserviceDates
    [DataMember]
    public bool HideMilitaryServiceDates { get; set; }
    
    //ResDoc/resume/hide_options/hide_workDates
    [DataMember]
    public bool HideWorkDates { get; set; }
    
    //ResDoc/resume/hide_options/hide_contact
    [DataMember]
    public bool HideContact { get; set; }
    
    //ResDoc/resume/hide_options/hide_name
    [DataMember]
    public bool HideName { get; set; }
    
    //ResDoc/resume/hide_options/hide_email
    [DataMember]
    public bool HideEmail { get; set; }
    
    //ResDoc/resume/hide_options/hide_homePhoneNumber
    [DataMember]
    public bool HideHomePhoneNumber { get; set; }
    
    //ResDoc/resume/hide_options/hide_workPhoneNumber
    [DataMember]
    public bool HideWorkPhoneNumber { get; set; }
    
    //ResDoc/resume/hide_options/hide_cellPhoneNumber
    [DataMember]
    public bool HideCellPhoneNumber { get; set; }
    
    //ResDoc/resume/hide_options/hide_faxPhoneNumber
    [DataMember]
    public bool HideFaxPhoneNumber { get; set; }
    
    //ResDoc/resume/hide_options/hide_nonUSPhoneNumber
    [DataMember]
    public bool HideNonUSPhoneNumber { get; set; }
    
    //ResDoc/resume/hide_options/hide_affiliations
    [DataMember]
    public bool HideAffiliations { get; set; }
    
    //ResDoc/resume/hide_options/hide_certifications_professionallicenses
    [DataMember]
    public bool HideCertificationsAndProfessionalLicenses { get; set; }
    
    //ResDoc/resume/hide_options/hide_employer
    [DataMember]
    public bool HideEmployer { get; set; }
    
    //ResDoc/resume/hide_options/hide_driver_license
    [DataMember]
    public bool HideDriverLicense { get; set; }
    
    //ResDoc/resume/hide_options/unhide_driver_license
    [DataMember]
    public bool UnHideDriverLicense { get; set; }
    
    //ResDoc/resume/hide_options/hide_honors
    [DataMember]
    public bool HideHonors { get; set; }
    
    //ResDoc/resume/hide_options/hide_interests
    [DataMember]
    public bool HideInterests { get; set; }
    
    //ResDoc/resume/hide_options/hide_internships
    [DataMember]
    public bool HideInternships { get; set; }
    
    //ResDoc/resume/hide_options/hide_languages
    [DataMember]
    public bool HideLanguages { get; set; }
    
    //ResDoc/resume/hide_options/hide_veteran
    [DataMember]
    public bool HideVeteran { get; set; }
    
    //ResDoc/resume/hide_options/hide_objective
    [DataMember]
    public bool HideObjective { get; set; }
    
    //ResDoc/resume/hide_options/hide_personalinformation
    [DataMember]
    public bool HidePersonalInformation { get; set; }
    
    //ResDoc/resume/hide_options/hide_professionaldevelopment
    [DataMember]
    public bool HideProfessionalDevelopment { get; set; }
    
    //ResDoc/resume/hide_options/hide_publications
    [DataMember]
    public bool HidePublications { get; set; }
    
    //ResDoc/resume/hide_options/hide_references
    [DataMember]
    public bool HideReferences { get; set; }
    
    //ResDoc/resume/hide_options/hide_skills
    [DataMember]
    public bool HideSkills { get; set; }
    
    //ResDoc/resume/hide_options/hide_technicalskills
    [DataMember]
    public bool HideTechnicalSkills { get; set; }
    
    //ResDoc/resume/hide_options/hide_volunteeractivities
    [DataMember]
    public bool HideVolunteerActivities { get; set; }
		
    //ResDoc/resume/hide_options/hide_thesismajorprojects
    [DataMember]
    public bool HideThesisMajorProjects { get; set; }
    
    //ResDoc/resume/experience/job/display_in_resume
    [DataMember]
    public List<string> HiddenJobs { get; set; }
    
    //ResDoc/resume/experience/job/hidden_skills
    [DataMember]
    public List<string> HiddenSkills { get; set; }
	}

	#endregion

	#region ResumeBody

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ResumeBody
	{
		[DataMember]
    public UserProfile Profile { get; set; }

    [DataMember]
    public Contact SeekerContactDetails { get; set; }

    [DataMember]
    public Statements Statements { get; set; }

    [DataMember]
    public SkillInfo Skills { get; set; }

    [DataMember]
    public ProfessionalInfo Professional { get; set; }

    [DataMember]
    public EducationInfo EducationInfo { get; set; }

    [DataMember]
    public SummaryInfo SummaryInfo { get; set; }
    
    [DataMember]
    public List<AdditionalResumeSection> AdditionalResumeSections { get; set; }
		
    //ResDoc/resume/references
    [DataMember]
    public string References { get; set; }
    
    [DataMember]
    public ExperienceInfo ExperienceInfo { get; set; }
	}

	#region Contact

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class Contact
	{
    [DataMember]
    public Name SeekerName { get; set; }

    [DataMember]
    public Address PostalAddress { get; set; }
		
    //ResDoc/resume/contact/email
    [DataMember]
    public string EmailAddress { get; set; }

    [DataMember]
    public List<Phone> PhoneNumber { get; set; }
		
    //ResDoc/resume/contact/website
    [DataMember]
    public string WebSite { get; set; }
	}
	#endregion

	#region Statements

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class Statements
	{
		//public UserProfile PersonalDetails;
		//ResDoc/resume/statements/honors
    [DataMember]
    public string Honors { get; set; }
	}

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class UserProfile
	{
    [DataMember]
    public Name UserGivenName { get; set; }

    [DataMember]
    public DateTime? DOB { get; set; }

    [DataMember]
    public Genders? Sex { get; set; }

    [DataMember]
    public Phone PrimaryPhone { get; set; }

    [DataMember]
    public Address PostalAddress { get; set; }

    [DataMember]
    public string EmailAddress { get; set; }

    [DataMember]
    public string SSN { get; set; }

    [DataMember]
    public EthnicHeritage EthnicHeritage { get; set; }
		
    //ResDoc/resume/statements/personal/citizen_flag
    [DataMember]
    public bool? IsUSCitizen { get; set; }
		
    //ResDoc/resume/statements/personal/migrant_flag
    [DataMember]
    public bool? IsMigrant { get; set; }
	
    //ResDoc/resume/statements/personal/ExOffender_flag
	[DataMember]
    public bool? IsExOffender { get; set; }

    //ResDoc/resume/statements/personal/IsHomeless_flag
    [DataMember]
    public bool? IsHomeless { get; set; }

  

    //ResDoc/resume/statements/personal/IsStaying_flag
    [DataMember]
    public bool? IsStaying { get; set; }


    //ResDoc/resume/statements/personal/IsUnderYouth_flag
    [DataMember]
    public bool? IsUnderYouth { get; set; }


    //ResDoc/resume/statements/personal/disability_status
    [DataMember]
    public DisabilityStatus? DisabilityStatus { get; set; }

    [DataMember]
    public List<long?> DisabilityCategoryIds { get; set; }

    //ResDoc/resume/statements/personal/disability_category_cd
    [DataMember]
    public long? DisabilityCategoryId { get; set; }
    
    [DataMember]
    public VeteranInfo Veteran { get; set; }

    [DataMember]
    public LicenseInfo License { get; set; }

		#region Alien Info
		
    //ResDoc/resume/statements/personal/alien_id
    [DataMember]
    public string AlienId { get; set; }
		
    //ResDoc/resume/statements/personal/alien_perm_flag
    [DataMember]
    public bool? IsPermanentResident { get; set; }

    //ResDoc/resume/statements/personal/alien_authorised_flag
    [DataMember]
    public bool? AuthorisedToWork { get; set; }
		
    //ResDoc/resume/statements/personal/alien_expires
    [DataMember]
    public DateTime? AlienExpires { get; set; }
		
    #endregion

		//ResDoc/resume/statements/personal/crc
    [DataMember]
    public bool? IsCareerReadinessCertified { get; set; }

		//ResDoc/resume/statements/personal/selective_service_flag
		[DataMember]
		public bool? RegisteredWithSelectiveService { get; set; }

    //ResDoc/resume/statements/personal/msfw_flag
    [DataMember]
    public bool? IsMSFW { get; set; }

    //ResDoc/resume/statements/personal/msfw_questions/msfw_question
    [DataMember]
    public List<long> MSFWQuestions { get; set; }

    //ResDoc/resume/statements/personal/LowLevelLiteracyIssues
    [DataMember]
    public LowLevelLiteracyIssues LowLevelLiteracyIssues { get; set; }

     //ResDoc/resume/statements/personal/lowlevel_literacy_flag 
    [DataMember]
    public bool LowLevelLiteracy { get; set; }

    [DataMember]
    public string PreferredLanguage { get; set; }

    //ResDoc/resume/statements/personal/cultural_barriers_flag 
    [DataMember]
    public bool CulturalBarriers { get; set; }

    //ResDoc/resume/statements/personal/no_of_dependants
    [DataMember]
    public string NoOfDependents { get; set; }

    //ResDoc/resume/statements/personal/est_monthly_income
    [DataMember]
    public string EstMonthlyIncome { get; set; }

    //ResDoc/resume/statements/personal/displaced_homemaker 
    [DataMember]
    public bool DisplacedHomemaker { get; set; }

    //ResDoc/resume/statements/personal/single_parent 
    [DataMember]
    public bool SingleParent { get; set; }

    //ResDoc/resume/statements/personal/low_income_status 
    [DataMember]
    public bool LowIncomeStatus { get; set; }
  }


	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class LicenseInfo
	{
		//ResDoc/resume/statements/personal/license/driver_flag
    [DataMember]
    public bool? HasDriverLicense { get; set; }
		
    //ResDoc/resume/statements/personal/license/driver_state
    [DataMember]
    public long? DriverStateId { get; set; }
		
    //ResDoc/resume/statements/personal/license/state_fullname
    [DataMember]
    public string DriverStateName { get; set; }
		
    //ResDoc/resume/statements/personal/license/driver_class
    [DataMember]
    public long? DrivingLicenceClassId { get; set; }

    [DataMember]
    public string DriverClassText { get; set; }

    [DataMember]
    public List<long> DrivingLicenceEndorsementIds { get; set; }

    [DataMember]
    public List<string> DrivingLicenceEndorsements { get; set; }

	}

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class VeteranInfo
	{
		//ResDoc/resume/statements/personal/veteran/vet_flag
    [DataMember]
    public bool? IsVeteran { get; set; }

    //ResDoc/resume/statements/personal/veteran/subscribe_vps
    [DataMember]
    public bool VeteranPriorityServiceAlertsSubscription { get; set; }

		[IgnoreDataMember]
		public bool VeteranPriorityServiceAlertsSubscriptionChanged { get; set; }
		
		//ResDoc/resume/statements/personal/veteran/homeless_flag
		[DataMember]
		public bool? IsHomeless { get; set; }

		//ResDoc/resume/statements/personal/veteran/attended_tap_flag
		[DataMember]
		public bool? AttendedTapCourse { get; set; }

		//ResDoc/resume/statements/personal/veteran/date_attended_tap
		[DataMember]
		public DateTime? DateAttendedTapCourse { get; set; }
		
		//ResDoc/resume/statements/personal/veteran/moc/ronet/
		//ResDoc/resume/statements/personal/veteran/moc/ronet/@starrating
    [DataMember]
    public Dictionary<string, int> MOCRonetInfo { get; set; }

    [DataMember]
    public List<VeteranHistory> History { get; set; } 
	}

  [Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class VeteranHistory
  {
    [DataMember]
    //ResDoc/resume/statements/personal/veteran/veteran_history/employment_status
    public EmploymentStatus? EmploymentStatus { get; set; }

    //ResDoc/resume/statements/personal/veteran/veteran_history/vet_disability_status
    [DataMember]
    public VeteranDisabilityStatus? VeteranDisabilityStatus { get; set; }

    //ResDoc/resume/statements/personal/veteran/veteran_history/vet_tsm_type_cd
    [DataMember]
    public VeteranTransitionType? TransitionType { get; set; }

    //ResDoc/resume/statements/personal/veteran/veteran_history/vet_era
    [DataMember]
    public VeteranEra? VeteranEra { get; set; }

    //ResDoc/resume/statements/personal/veteran/veteran_history/vet_start_date
    [DataMember]
    public DateTime? VeteranStartDate { get; set; }

    //ResDoc/resume/statements/personal/veteran/veteran_history/vet_end_date
    [DataMember]
    public DateTime? VeteranEndDate { get; set; }

    //ResDoc/resume/statements/personal/veteran/veteran_history/campaign_vet_flag
    [DataMember]
    public bool? IsCampaignVeteran { get; set; }

    //ResDoc/resume/statements/personal/veteran/veteran_history/vet_discharge
    [DataMember]
    public long? MilitaryDischargeId { get; set; }

    //ResDoc/resume/statements/personal/veteran/veteran_history/branch_of_service
    [DataMember]
    public long? MilitaryBranchOfServiceId { get; set; }

    //ResDoc/resume/statements/personal/veteran/veteran_history/rank
    [DataMember]
    public long? RankId { get; set; }

    //ResDoc/resume/statements/personal/veteran/veteran_history/moc
    [DataMember]
    public string MilitaryOccupation { get; set; }

    //ResDoc/resume/statements/personal/veteran/veteran_history/moc/@code
    [DataMember]
    public string MilitaryOccupationCode { get; set; }

    //ResDoc/resume/statements/personal/veteran/veteran_history/unit_affiliation
    [DataMember]
    public string UnitOrAffiliation { get; set; }    
  }

	[Serializable]
	public class EthnicHeritage
	{
		//ResDoc/resume/statements/personal/ethnic_heritages/ethnic_heritage/ethnic_id
		//ResDoc/resume/statements/personal/ethnic_heritages/ethnic_heritage/selection_flag
    [DataMember]
		public long? EthnicHeritageId { get; set; }

    [DataMember]
    public List<long?> RaceIds { get; set; }
	}

    [Serializable]
    public class LowLevelLiteracyIssues
    {
        [DataMember]
        public string NativeLanguage { get; set; }

        [DataMember]
        public string CommonLanguage { get; set; }
    }
   	#endregion

	#region SkillInfo

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SkillInfo
	{
		//ResDoc/resume/skills/skills
    [DataMember]
    public List<string> Skills { get; set; }

    [DataMember]
    public bool? SkillsAlreadyCaptured { get; set; }

    //ResDoc/resume/skills/internship_skills
    [DataMember]
    public List<InternshipSkill> InternshipSkills { get; set; }
	  
    //ResDoc/resume/skills/languages
    [DataMember]
		public List<LanguageProficiency> LanguageProficiencies { get; set; }

    [DataMember]
    public List<Certification> Certifications { get; set; }
	}

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class InternshipSkill
	{
    [DataMember]
    public long Id { get; set; }

    [DataMember]
    public string Name { get; set; }
	}

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class Certification
	{
		//ResDoc/resume/skills/courses/certifications/certification/certificate
    [DataMember]
    public string Certificate { get; set; }
		
    //ResDoc/resume/skills/courses/certifications/certification/organization_name
    [DataMember]
    public string OrganizationName { get; set; }
		
    //ResDoc/resume/skills/courses/certifications/certification/completion_date
    [DataMember]
    public DateTime? CompletionDate { get; set; }

		//ResDoc/resume/skills/courses/certifications/certification/expected_completion_date
		[DataMember]
		public DateTime? ExpectedCompletionDate { get; set; }
		
    //ResDoc/resume/skills/courses/certifications/certification/address
    [DataMember]
    public Address CertificationAddress { get; set; }

		/// <summary>
		/// Gets or sets the external identifier.
		/// </summary>
		/// <value>
		/// The external identifier.
		/// </value>
		[DataMember]
		public string ExternalId { get; set; }
	}

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class LanguageProficiency
	{
		//ResDoc/resume/skills/languages/language/language
		[DataMember]
		public string Language { get; set; }

		//ResDoc/resume/skills/languages/language/proficiency
		[DataMember]
		public long Proficiency { get; set; }
	}

	#endregion

	#region ProfessionalInfo

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ProfessionalInfo
	{
		//ResDoc/resume/professional/affiliations
		[DataMember]
    public List<string> Affiliations { get; set; }
		
    //ResDoc/resume/professional/publications
    [DataMember]
    public List<string> Publications { get; set; }
	}

	#endregion

	#region EducationInfo

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EducationInfo
	{
		//ResDoc/resume/education/norm_edu_level_cd
    [DataMember]
    public EducationLevel? EducationLevel { get; set; }
		
    //ResDoc/resume/education/school_status_cd
    [DataMember]
    public SchoolStatus? SchoolStatus { get; set; }
		
    //ResDoc/resume/education/courses
    [DataMember]
    public string Courses { get; set; }

    [DataMember]
    public List<School> Schools { get; set; }

    //ResDoc/resume/education/mostexperienceindustries
    [DataMember]
    public List<long> MostExperienceIndustries { get; set; }

    //ResDoc/resume/education/targetindustries
    [DataMember]
    public List<long> TargetIndustries { get; set; }

		//ResDoc/resume/education/ncrc_confirmation
		[DataMember]
		public bool NCRCConfirmation { get; set; }

		//ResDoc/resume/education/ncrc_level
		[DataMember]
		public long? NCRCLevel { get; set; }

		//ResDoc/resume/education/ncrc_level_name
		[DataMember]
		public String NCRCLevelName { get; set; }

		//ResDoc/resume/education/ncrc_state_id
		[DataMember]
		public long? NCRCStateId { get; set; }

		//ResDoc/resume/education/ncrc_state_name
		[DataMember]
		public string NCRCStateName { get; set; }

		//ResDoc/resume/education/ncrc_issue_date
		[DataMember]
		public DateTime? NCRCIssueDate { get; set; }

		//ResDoc/resume/education/ncrc_displayed
		[DataMember]
		public bool? NCRCDisplayed { get; set; }
	}

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class School
	{
		//ResDoc/resume/education/school/@id
    [DataMember]
    public int SchoolId { get; set; }

		//ResDoc/resume/education/school/completiondate
    [DataMember]
    public DateTime? CompletionDate { get; set; }

		//ResDoc/resume/education/school/expectedcompletiondate
    [DataMember]
    public DateTime? ExpectedCompletionDate { get; set; }

		//ResDoc/resume/education/school/degree
    [DataMember]
    public string Degree { get; set; }

		//ResDoc/resume/education/school/institution
    [DataMember]
    public string Institution { get; set; }

		//ResDoc/resume/education/school/major
    [DataMember]
    public string Major { get; set; }

		//ResDoc/resume/education/school/courses
    [DataMember]
    public string Courses { get; set; }

		//ResDoc/resume/education/school/honors
    [DataMember]
    public string Honors { get; set; }

    [DataMember]
    public GradePointAverage GradePointAverage { get; set; }

		//ResDoc/resume/education/school/activities
    [DataMember]
    public string Activities { get; set; }

    [DataMember]
    public Address SchoolAddress { get; set; }

		/// <summary>
		/// Gets or sets the external identifier.
		/// </summary>
		/// <value>
		/// The external identifier.
		/// </value>
		[DataMember]
		public string ExternalId { get; set; }
	}

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GradePointAverage
	{
		//ResDoc/resume/education/school/gpa/@max
    [DataMember]
    public float? Max { get; set; }

		//ResDoc/resume/education/school/gpa/@value
    [DataMember]
    public float? Value { get; set; }

    [DataMember]
    public string RawGradePointAverage { get; set; }
	}

	#endregion

	#region SummaryInfo

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SummaryInfo
	{
		#region User Summary
		
    [DataMember]
    public bool? IncludeSummary { get; set; }
		
    //ResDoc/resume/summary/summary
    [DataMember]
    public string Summary { get; set; }
		
    #endregion

		#region Automated Summary
		
    //ResDoc/special/automatedsummary/@custom
    [DataMember]
    public bool? UseAutomatedSummary { get; set; }
		
    //ResDoc/special/snapshot
    [DataMember]
    public string AutomatedSummary { get; set; }
		
    #endregion
	}

	#endregion

	#region ExperienceInfo

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ExperienceInfo
	{
		//ResDoc/resume/experience/employment_status_cd
    [DataMember]
    public EmploymentStatus? EmploymentStatus { get; set; }
		
    //ResDoc/resume/experience/@start
    [DataMember]
    public DateTime? ExperienceStartDate { get; set; }
		
    //ResDoc/resume/experience/@end
    [DataMember]
    public DateTime? ExperienceEndDate { get; set; }

    [DataMember]
    public List<Job> Jobs { get; set; }
    
    [DataMember]
    public bool IsExperienceInfoUpdated { get; set; }

    [IgnoreDataMember] 
	  public bool AllJobsComplete
	  {
      get { return Jobs.IsNullOrEmpty() || Jobs.All(x => x.IsComplete); }
	  }
	}

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public partial class Job
	{
		//ResDoc/resume/experience/job/jobid
    [DataMember]
    public Guid? JobId { get; set; }
		
    //ResDoc/resume/experience/job/daterange/end/@currentjob
    [DataMember]
    public bool? IsCurrentJob { get; set; }
		
    //ResDoc/resume/experience/job/daterange/start
    [DataMember]
    public DateTime? JobStartDate { get; set; }
		
    //ResDoc/resume/experience/job/daterange/end
    [DataMember]
    public DateTime? JobEndDate { get; set; }
		
    //ResDoc/resume/experience/job/@onet
    [DataMember]
    public string OnetCode { get; set; }
		
    //ResDoc/resume/experience/job/ronet
    [DataMember]
    public string ROnetCode { get; set; }
		
    //ResDoc/resume/experience/job/starrating
    [DataMember]
    public int? StarRating { get; set; }
		
    //ResDoc/resume/experience/job/@pos
		//public int? Position { get; set; }
		
    //ResDoc/resume/experience/job/description
    [DataMember]
    public string Description { get; set; }
		
    //ResDoc/resume/experience/job/employer
    [DataMember]
    public string Employer { get; set; }
		
    //ResDoc/resume/experience/job/title 
    [DataMember]
    public string Title { get; set; }

		//ResDoc/resume/experience/job/title/@moc 
    [DataMember]
    public string MilitaryOccupationCode { get; set; }

    [DataMember]
    public Address JobAddress { get; set; }
		
    //ResDoc/resume/experience/job/salary_unit_cd
    [DataMember]
    public long? SalaryFrequencyId { get; set; }
		
    //ResDoc/resume/experience/job/sal
    [DataMember]
    public float? Salary { get; set; }

		//ResDoc/resume/experience/job/rank
    [DataMember]
    public long? RankId { get; set; }

    //ResDoc/resume/experience/job/reason_for_leaving
    [DataMember]
    public int JobReasonForLeaving { get; set; }

		//ResDoc/resume/experience/job/branch_of_service
		[DataMember]
		public long? BranchOfServiceId { get; set; }

    [DataMember]
    public List<InternshipSkill> InternshipSkills { get; set; }

		/// <summary>
		/// Gets or sets the external identifier.
		/// </summary>
		/// <value>
		/// The external identifier.
		/// </value>
		[DataMember]
		public string ExternalId { get; set; }

    [IgnoreDataMember]
	  public bool IsComplete
	  {
	    get {
        if (Title.IsNullOrEmpty()) return false;
        if (Employer.IsNullOrEmpty()) return false;
        if (JobStartDate.IsNull()) return false;
				if (JobEndDate.IsNull() && !IsCurrentJob.GetValueOrDefault()) return false;
        if (JobAddress.IsNull() || JobAddress.City.IsNullOrEmpty() || JobAddress.StateId.IsNull() || JobAddress.CountryId.IsNull()) return false;
	      return true;
	    }
	  }
	}

  public partial class Job
	{
		//ResDoc/resume/experience/job/display_in_resume
    [DataMember]
    public bool? IsDisplayInResume { get; set; }
	}

	#endregion

	#region Additional Resume Sections

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class AdditionalResumeSection
	{
    [DataMember]
    public ResumeSectionType SectionType { get; set; }

    [DataMember]
    public bool? UseSectionInResume { get; set; }

    [DataMember]
    public object SectionContent { get; set; }
		//ResDoc/resume/summary/objective
	}
	#endregion

	#endregion

	#region Resume Enumerators

	public enum ResumeOperation
	{
		None
	}

	public enum ResumeSectionType
	{
		None = 0,
		Affiliations = 1,
		Internships = 2,
		ProfessionalDevelopment = 4,
		VolunteerActivities = 8,
		Honors = 16,
		Objective = 32,
		Publications = 64,
		Interests = 128,
		Personals = 256,
		References = 512,
		Description = 1024,
		TechnicalSkills = 2048,
		ThesisMajorProjects = 4096,
		Branding = 8192,
		All = Affiliations | Internships | ProfessionalDevelopment | VolunteerActivities | Honors | Objective | Publications | Interests | Personals | References | Description | TechnicalSkills | ThesisMajorProjects | Branding
	}

  public enum ResumeCreationMethod
  {
    NotSpecified = 0,
    BuildWizard = 1,
    Upload = 2,
    CopyPaste = 3
  }

	

	public enum GuidedPath
	{
		UptoWorkHistory = ResumeCompletionStatuses.WorkHistory,
		UptoContact = ResumeCompletionStatuses.Contact | UptoWorkHistory,
		UptoEducation = ResumeCompletionStatuses.Education | UptoContact,
		UptoSummary = ResumeCompletionStatuses.Summary | UptoEducation,
		UptoOptions = ResumeCompletionStatuses.Options | UptoSummary,
		UptoProfile = ResumeCompletionStatuses.Profile | UptoOptions,
		UptoPreferences = ResumeCompletionStatuses.Preferences | UptoProfile
	}

	public enum EmploymentStatus
	{
    [EnumLocalisationDefault("None")]
    None = 0,
    [EnumLocalisationDefault("Employed")]
		Employed = 1,
    [EnumLocalisationDefault("Employed with termination notice")]
    EmployedTerminationReceived = 2,
    [EnumLocalisationDefault("Not employed")]
    UnEmployed = 3
	}

	public enum DisabilityStatus
	{
		NotDisabled = 1,
		Disabled = 2,
		NotDisclosed = 3
	}

	public enum VeteranDisabilityStatus
	{
    [EnumLocalisationDefault("Not Disabled")]
    NotDisabled = 1,
    [EnumLocalisationDefault("Disabled (Up to 20%)")]
    Disabled = 2,
    [EnumLocalisationDefault("Special Disabled (30% or more)")]
    SpecialDisabled = 3
	}

	public enum VeteranTransitionType
	{
    [EnumLocalisationDefault("Retirement")]
    Retirement = 1,
    [EnumLocalisationDefault("Discharge")]
    Discharge = 2,
    [EnumLocalisationDefault("Spouse")]
    Spouse = 3,
	}

	public enum VeteranEra
	{
    [EnumLocalisationDefault("Vietnam")]
    Vietnam = 1,
    [EnumLocalisationDefault("Other Veteran")]
    OtherVet = 2,
    [EnumLocalisationDefault("Other Eligible")]
    OtherEligible = 3,
    [EnumLocalisationDefault("Transitioning Service Member")]
    TransitioningServiceMember = 4,
    [EnumLocalisationDefault("Transitioning Vietnam Service Member")]
    TransitioningVietnamServiceMember = 5,
    [EnumLocalisationDefault("Transitioning Service Member Spouse")]
    TransitioningServiceMemberSpouse = 6,
	}

	#endregion

	#endregion
}
