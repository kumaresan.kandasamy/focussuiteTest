﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Models.Career
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class Name
	{
    [DataMember]
    public string FirstName { get; set; }

    [DataMember]
    public string LastName { get; set; }

    [DataMember]
    public string MiddleName { get; set; }

    [DataMember]
    public long? SuffixId { get; set; }

    [DataMember]
    public string SuffixName { get; set; }

		public string FullName
		{
			get
			{
				return string.IsNullOrEmpty(MiddleName) 
					? string.Format("{0} {1}", FirstName, LastName.ToNameCase()).ToTitleCase() 
					: string.Format("{0} {1} {2}", FirstName, MiddleName, LastName.ToNameCase()).ToTitleCase();
			}
		}
	}

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class Address
	{
    [DataMember]
    public double? Latitude { get; set; }

    [DataMember]
    public double? Longitude { get; set; }

    [DataMember]
    public string Street1 { get; set; }

    [DataMember]
    public string Street2 { get; set; }

    [DataMember]
    public string MajorCity { get; set; }

    [DataMember]
    public string City { get; set; }

    [DataMember]
    public string CountyName { get; set; }

    [DataMember]
    public long? StateId { get; set; }

    [DataMember]
    public string StateName { get; set; }

    [DataMember]
    public long? CountyId { get; set; }

    [DataMember]
    public long? CountryId { get; set; }

    [DataMember]
    public string CountryName { get; set; }

    [DataMember]
    public string Zip { get; set; }
	}

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class Phone
  {
    [DataMember]
    public PhoneType? PhoneType { get; set; }

    [DataMember]
    public string PhoneNumber { get; set; }

    [DataMember]
    public string Extention { get; set; }

		[DataMember]
		public long? Provider { get; set; }
	}

	public enum ResumeType
	{
		CleanText,
		Binary,
		Tagged
	}

	public enum PhoneType
	{
		Home,
		Work,
		Cell,
		Fax,
		NonUS
	}
}
