﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

namespace Focus.Core.Models.Career
{
    public interface ISearchDocument
    {
        string DocumentId { get; set; }
        int MatchingScore { get; set; }
    }
}
