﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

#endregion

using System.Runtime.Serialization;

namespace Focus.Core.Models.Career
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ReferralPosting
	{
    [DataMember]
    public long? Id { get; set; }

    [DataMember]
    public string LensPostingId { get; set; }

    [DataMember]
    public PostingStatuses PostingStatus { get; set; }

    [DataMember]
    public ApprovalStatuses ApprovalStatus { get; set; }
	}
}
