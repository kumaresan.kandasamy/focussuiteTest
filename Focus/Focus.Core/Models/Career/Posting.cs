﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Models.Career
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class PostingDocument : ISearchDocument
	{
    [DataMember]
    public string DocumentId { get; set; }
    
    [DataMember]
    public int MatchingScore { get; set; }

    [DataMember]
    public PostingInfo PostingInfo { get; set; }

    [DataMember]
    public string PostingOriginId { get; set; }
	}

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class PostingEnvelope
	{
    [DataMember]
    public string PostingXml { get; set; }

    [DataMember]
    public string RequirementsXml { get; set; }

    [DataMember]
    public string PostingHtml { get; set; }

    [DataMember]
    public string EosPostingXml { get; set; }

    [DataMember]
    public PostingInfo PostingInfo { get; set; }

    [DataMember]
    public PostingStatuses PostingStatus { get; set; }
	}

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class PostingInfo
	{
    [DataMember]
    public string Title { get; set; }

    [DataMember]
    public string Employer { get; set; }

    [DataMember]
    public string Description { get; set; }

    [DataMember]
    public List<string> Skills { get; set; }

    [DataMember]
    public DateTime OpenDate { get; set; }
    
    [DataMember]
    public string Url { get; set; }

    [DataMember]
    public long? PostingOriginId { get; set; }

    [DataMember]
    public string CustomerJobId { get; set; }

    [DataMember]
    public int ViewedCount { get; set; }

    [DataMember]
    public long? FocusJobId { get; set; }

    [DataMember]
    public bool Internship { get; set; }

		[DataMember]
		public long? FocusPostingId { get; set; }

		[DataMember]
		public DateTime CreatedOn { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ViewedPostingModel
	{
    [DataMember]
    public string LensPostingId { get; set; }

    [DataMember]
    public string EmployerName { get; set; }
    
    [DataMember]
    public string JobTitle { get; set; }
    
    [DataMember]
    public DateTime ViewedOn { get; set; }

		[DataMember]
		public long? FocusJobId { get; set; }
	}
}
