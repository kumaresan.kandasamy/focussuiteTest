﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives


#endregion

namespace Focus.Core.Models.Career
{
	public class DisplayPreference
	{
		public int PageNumber { get; set; }
		public int PageSize { get; set; }
		public string OrderBy { get; set; }
		public string HighlightedItemKey { get; set; }
	}
}
