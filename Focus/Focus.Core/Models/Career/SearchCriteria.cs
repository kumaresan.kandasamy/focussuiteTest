﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

using Focus.Core.Criteria;
using Framework.Core;

#endregion

namespace Focus.Core.Models.Career
{
	#region Required Result Criterion

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class RequiredResultCriteria
	{
		[DataMember]
		public DocumentType? DocumentsToSearch { get; set; }

		[DataMember]
		public int? MaximumDocumentCount { get; set; }

		[DataMember]
		public StarMatching MinimumStarMatch { get; set; }

		[DataMember]
		public List<string> IncludeData { get; set; }

		[DataMember]
		public bool IncludeDuties { get; set; }
	}

	#endregion

	#region Keyword Search Criterion

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class KeywordCriteria
	{
		[DataMember]
		public string KeywordText { get; set; }

		[DataMember]
		public PostingKeywordScopes SearchLocation { get; set; }

		/// <summary>
		/// Gets or sets the operator.
		/// </summary>
		/// <value>The operator.</value>
		[DataMember]
		public LogicalOperators Operator { get; set; }

		public KeywordCriteria()
		{
			Operator = LogicalOperators.And;
		}

		public KeywordCriteria(string keywordText)
		{
			Operator = LogicalOperators.And;
			KeywordText = keywordText;
		}
	}

	#endregion

	#region Job Location Requirement Criterion

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class RadiusCriteria
	{
		[DataMember]
		public long Distance { get; set; }

		[DataMember]
		public DistanceUnits DistanceUnits { get; set; }

		[DataMember]
		public string PostalCode { get; set; }
	}

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class AreaCriteria
	{
		[DataMember]
		public long? StateId { get; set; }

		[DataMember]
		public string City { get; set; }

		[DataMember]
		public long? MsaId { get; set; }
	}

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobLocationCriteria
	{
		[DataMember]
		public bool OnlyShowJobInMyState { get; set; }

		[DataMember]
		public string SelectedStateInCriteria { get; set; }

		[DataMember]
		public List<string> SearchMultipleStates { get; set; }

		[DataMember]
		public RadiusCriteria Radius { get; set; }

		[DataMember]
		public AreaCriteria Area { get; set; }
	}

	#endregion

	#region Posting Age Criterion

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class PostingAgeCriteria
	{
		[DataMember]
		public int PostingAgeInDays { get; set; }

		[DataMember]
		public int? MinimumPostingAgeInDays { get; set; }
	}

	#endregion

	#region Education Level Criterion

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EducationLevelCriteria
	{
		[DataMember]
		public List<long> RequiredEducationIds { get; set; }

		[DataMember]
		public bool IncludeJobsWithoutEducationRequirement { get; set; }
	}

	#endregion

	#region Salary Requirement Criterion

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SalaryCriteria
	{
		[DataMember]
		public float MinimumSalary { get; set; }

		[DataMember]
		public bool IncludeJobsWithoutSalaryInformation { get; set; }
	}

	#endregion

	#region Work Days Criterion

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class WorkDaysCriteria
	{
		[DataMember]
		public DaysOfWeek? WorkDays { get; set; }

		[DataMember]
		public bool WeekDayVaries { get; set; }

		[DataMember]
		public long? WorkWeek { get; set; }
	}

	#endregion

	#region Internship Requirement Criterion

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class InternshipCriteria
	{
		[DataMember]
		public FilterTypes? Internship { get; set; }

		[DataMember]
		public List<long> InternshipCategories { get; set; }
	}

	#endregion

	#region Job Type Criterion

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class HomeBasedJobsCriteria
	{
		[DataMember]
		public FilterTypes? HomeBasedJobs { get; set; }
	}

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CommissionBasedJobsCriteria
	{
		[DataMember]
		public FilterTypes? CommissionBasedJobs { get; set; }
	}

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SalaryAndCommissionBasedJobsCriteria
	{
		[DataMember]
		public FilterTypes? SalaryAndCommissionBasedJobs { get; set; }
	}
	#endregion

	#region Education School Criteria

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EducationSchoolCriteria
	{
		[DataMember]
		public bool OnlyShowExclusivePostings { get; set; }
	}

	#endregion

	#region Education ProgramOfStudy Criteria

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EducationProgramOfStudyCriteria
	{
		[DataMember]
		public long? ProgramAreaId { get; set; }

		[DataMember]
		public long? DegreeEducationLevelId { get; set; }
	}

	#endregion

	#region Language Criteria

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class LanguageCriteria
	{
		[DataMember]
		public List<LanguageProficiency> LanguagesWithProficiencies { get; set; }

		[DataMember]
		public bool LanguageSearchType { get; set; }
	}

	#endregion

	#region Occupation and Industry Requirement Criterion

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class OccupationCriteria
	{
		// TODO: Look to deprecate and replace with ROnetCriteria as done for Education
		[DataMember]
		public long? JobFamilyId { get; set; }

		[DataMember]
		public List<long> OccupationIds { get; set; }
	}

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class IndustryCriteria
	{
		[DataMember]
		public long? IndustryId { get; set; }

		[DataMember]
		public List<long> IndustryDetailIds { get; set; }
	}

	#endregion

	#region Job Sector Requirement Criterion

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobSectorCriteria
	{
		[DataMember]
		public List<long> RequiredJobSectorIds { get; set; }

		[DataMember]
		public String RequiredJobSectors { get; set; }
	}

	#endregion

	#region Job Ability Requirement Criterion

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class PhysicalAbilityCriteria
	{
		[DataMember]
		public List<long> PhysicalAbilityIds { get; set; }

		[DataMember]
		public string PhysicalAbilities { get; set; }
	}

	#endregion

	#region Exclude Past Job Requirement Criterion

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ExcludePastJobCriteria
	{
		[DataMember]
		public List<Job> ExcludedPastJobs { get; set; }
	}

	#endregion

	#region Exclude Posting Requirement Criterion

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ExcludePostingCriteria
	{
		[DataMember]
		public List<string> LensPostingIds { get; set; }

		[DataMember]
		public List<string> JobStatuses { get; set; }

		[DataMember]
		public bool AnonymousOnly { get; set; }
	}

	#endregion

	#region Reference Document Criterion

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ReferenceDocumentCriteria
	{
		[DataMember]
		public DocumentType DocumentType { get; set; }

		[DataMember]
		public string DocumentId { get; set; }

		[DataMember]
		public string Document { get; set; }

		[DataMember]
		public List<Job> ExcludedJobs { get; set; }
	}

	#endregion

	#region ROnet Criteria

	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ROnetCriteria
	{
		[DataMember]
		public long? CareerAreaId { get; set; }

		[DataMember]
		public List<string> ROnets { get; set; }

		[DataMember]
		public bool IsAllROnets { get; set; }

		public string ROnetCode { get; set; }

		public long ROnetId { get; set; }
	}

	#endregion

	#region Search Criteria Collection

	[Serializable]
	[XmlRoot(ElementName = "SearchCriteria")]
	[DataContract(Namespace = Constants.DataContractNamespace, Name = "SearchCriteria")]
	public class SearchCriteria : CriteriaBase
	{
		/// <summary>
		/// Gets or sets the type of the search.
		/// </summary>
		/// <value>The type of the search.</value>
		[DataMember]
		public PostingSearchTypes SearchType { get; set; }

		[DataMember]
		public RequiredResultCriteria RequiredResultCriteria { get; set; }

		[DataMember]
		public KeywordCriteria KeywordCriteria { get; set; }

		[DataMember]
		public JobLocationCriteria JobLocationCriteria { get; set; }

		[DataMember]
		public PostingAgeCriteria PostingAgeCriteria { get; set; }

		[DataMember]
		public EducationLevelCriteria EducationLevelCriteria { get; set; }

		[DataMember]
		public LanguageCriteria LanguageCriteria { get; set; }

		[DataMember]
		public SalaryCriteria SalaryCriteria { get; set; }

		[DataMember]
		public WorkDaysCriteria WorkDaysCriteria { get; set; }

		[DataMember]
		public EducationSchoolCriteria EducationSchoolCriteria { get; set; }

		[DataMember]
		public InternshipCriteria InternshipCriteria { get; set; }

		[DataMember]
		public HomeBasedJobsCriteria HomeBasedJobsCriteria { get; set; }

		[DataMember]
		public CommissionBasedJobsCriteria CommissionBasedJobsCriteria { get; set; }

		[DataMember]
		public SalaryAndCommissionBasedJobsCriteria SalaryAndCommissionBasedJobsCriteria { get; set; }

		[DataMember]
		public EducationProgramOfStudyCriteria EducationProgramOfStudyCriteria { get; set; }

		[DataMember]
		public OccupationCriteria OccupationCriteria { get; set; }

		[DataMember]
		public IndustryCriteria IndustryCriteria { get; set; }

		[DataMember]
		public JobSectorCriteria JobSectorCriteria { get; set; }

		[DataMember]
		public PhysicalAbilityCriteria PhysicalAbilityCriteria { get; set; }

		[DataMember]
		public ExcludePastJobCriteria ExcludePastJobCriteria { get; set; }

		[DataMember]
		public ExcludePostingCriteria ExcludePostingCriteria { get; set; }

		[DataMember]
		public ReferenceDocumentCriteria ReferenceDocumentCriteria { get; set; }

		[DataMember]
		public ROnetCriteria ROnetCriteria { get; set; }

		[DataMember]
		public string JobIdCriteria { get; set; }

		/// <summary>
		/// Determines whether this search requires matching to a resume or posting.
		/// </summary>
		/// <returns></returns>
		public bool IsMatchedSearch()
		{
			return ReferenceDocumentCriteria.IsNotNull() && ReferenceDocumentCriteria.DocumentId.IsNotNullOrEmpty();
		}
	}

	#endregion

	#region Search Criterion Enum

	public enum DistanceUnits
	{
		Miles,
		Kilometres
	}

	public enum StarMatching
	{
		None,
		ZeroStar,
		OneStar,
		TwoStar,
		ThreeStar,
		FourStar,
		FiveStar
	}

	public enum PostingSearchTypes
	{
		Postings,
		PostingsLikeThis
	}

	#endregion
}