﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Models.Career
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
	[Serializable]
  public class PostingSearchResultView
	{
    [DataMember]
    public string Id { get; set; }

    [DataMember]
    public int Rank { get; set; }

    [DataMember]
    public DateTime JobDate { get; set; }

    [DataMember]
    public string JobTitle { get; set; }

    [DataMember]
    public bool Internship { get; set; }

    [DataMember]
    public string Employer { get; set; }

    [DataMember]
    public string Location { get; set; }

    [DataMember]
    public string Action { get; set; }

		[DataMember]
		public int StarRating { get; set; }

		[DataMember]
		public int OriginId { get; set; }

    [DataMember]
    public List<string> Duties { get; set; }

		[DataMember]
		public int MinExperience { get; set; }

    [DataMember]
    public ScreeningPreferences? ScreeningPreferences { get; set; }

		[DataMember]
		public bool IsReferred { get; set; }
	}
}
