﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using System;

#endregion

namespace Focus.Core.Models.Career
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class PostingBookmark
	{
    [DataMember]
    public PostingDocument Posting { get; set; }

    [DataMember]
    public DateTime SavedTime { get; set; }

    [DataMember]
    public DateTime ViewedTime { get; set; }
    
    [DataMember]
    public string Tags { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SearchDetails
	{
    [DataMember]
    public long SearchId { get; set; }

    [DataMember]
    public string Name { get; set; }
    
    [DataMember]
    public SearchCriteria SearchCriteria { get; set; }
    
    [DataMember]
    public string Notes { get; set; }

    [DataMember]
    public DateTime SavedTime { get; set; }

    [DataMember]
    public JobAlert JobAlert { get; set; }
		
    public bool HasJobAlert
		{
			get { return (JobAlert != null && !string.IsNullOrEmpty(JobAlert.Name)); }
		}
	}
}
