﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Models.Report
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployerActivityReportModel
	{
		[DataMember]
		public DateTime ReportFromDate { get; set; }

		[DataMember]
		public DateTime ReportToDate { get; set; }

		[DataMember]
		public List<EmployerLogInsModel> EmployeeLogIns { get; set; }

		[DataMember]
		public List<TalentJobsPostedModel> TalentJobsPosted { get; set; }

		[DataMember]
		public int SpideredJobCount { get; set; }

		[DataMember]
		public List<TalentRegistrationsModel> TalentRegistrations { get; set; }

		[DataMember]
		public List<CandidateSearchModel> CandidateSearch { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployerLogInsModel
	{
		[DataMember]
		public string CompanyName { get; set; }

		[DataMember]
		public string EmployeeName { get; set; }

		[DataMember]
		public string EmployeeEmailAddress { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class TalentJobsPostedModel
	{
		[DataMember]
		public string JobTitle { get; set; }

		[DataMember]
		public string ROnetJobTitle { get; set; }

		[DataMember]
		public string CompanyName { get; set; }

		[DataMember]
		public string PostedBy { get; set; }

		[DataMember]
		public long? ROnetId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class TalentRegistrationsModel
	{
		[DataMember]
		public string CompanyName { get; set; }

		[DataMember]
		public string EmployeeName { get; set; }

		[DataMember]
		public string AccountStatus { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CandidateSearchModel
	{
		[DataMember]
		public string CompanyName { get; set; }

		[DataMember]
		public string EmployeeName { get; set; }
	}
}
