﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

using System;

namespace Focus.Core.Models
{
	public class PersonModel
	{
		public long? Id { get; set; }
		public long TitleId { get; set; }
		public string FirstName { get; set; }
		public string MiddleInitial { get; set; }
		public string LastName { get; set; }
		public DateTime? DateOfBirth { get; set; }
		public string SocialSecurityNumber { get; set; }
		public string JobTitle { get; set; }
		public string EmailAddress { get; set; }
		public SchoolStatus? EnrollmentStatus { get; set; }
		public long? ProgramAreaId { get; set; }
		public long? DegreeId { get; set; }
		public string ExternalOffice { get; set; }
		public bool? Manager { get; set; }
		public long? AssignedToId { get; set; }
		public DateTime? LastSurveyedOn { get; set; }
		public long? CampusId { get; set; }
	}
}
