﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace Focus.Core.Models
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobDegreeCertificationModel
	{
    [DataMember]
    public List<JobDegreeEducationLevelViewDto> JobDegreeEducationLevels { get; set; }

		public List<DegreeDto> JobDegrees
		{
			get
			{
				var degrees = new List<DegreeDto>();

				var jobDegreeEducationLevels = JobDegreeEducationLevels;
				if (jobDegreeEducationLevels != null && jobDegreeEducationLevels.Count > 0)
				{
					degrees = (from del in jobDegreeEducationLevels
										 orderby del.DegreeName
					           group del by new
					                        	{
					                        		del.DegreeId,
					                        		del.DegreeName,
																			del.IsClientData,
																			del.ClientDataTag,
                                      del.Tier
					                        	}
					           into g
					           select new DegreeDto()
					                  	{
					                  		Id = g.Key.DegreeId,
					                  		Name = g.Key.DegreeName,
																IsClientData = g.Key.IsClientData,
																ClientDataTag = g.Key.ClientDataTag,
                                Tier = g.Key.Tier
					                  	}).ToList();

				}

				return degrees;
			}
		}

		public List<DegreeEducationLevelDto> DegreeEducationLevels(long degreeId)
		{
			var degreeEducationLevels = new List<DegreeEducationLevelDto>();

			var jobDegreeEducationLevels = JobDegreeEducationLevels;
			if (jobDegreeEducationLevels != null && jobDegreeEducationLevels.Count > 0)
			{
				degreeEducationLevels = (from del in jobDegreeEducationLevels
				                         where del.DegreeId == degreeId
																 orderby del.EducationLevel ascending 
				                         select new DegreeEducationLevelDto()
				                                	{
				                                		Id = del.DegreeEducationLevelId,
				                                		DegreeId = del.DegreeId,
				                                		EducationLevel = del.EducationLevel,
				                                		DegreesAwarded = del.DegreesAwarded,
				                                		Name = del.DegreeEducationLevelName
				                                	}).ToList();
			}

			return degreeEducationLevels;
		}

    public List<DegreeEducationLevelDto> DegreeEducationLevels(List<long> degreeIds)
    {
      var degreeEducationLevels = new List<DegreeEducationLevelDto>();

      var jobDegreeEducationLevels = JobDegreeEducationLevels;
      if (jobDegreeEducationLevels != null && jobDegreeEducationLevels.Count > 0)
      {
        degreeEducationLevels = (from del in jobDegreeEducationLevels
                                 where degreeIds.Contains(del.DegreeId)
                                 orderby del.EducationLevel ascending
                                 select new DegreeEducationLevelDto()
                                 {
                                   Id = del.DegreeEducationLevelId,
                                   DegreeId = del.DegreeId,
                                   EducationLevel = del.EducationLevel,
                                   DegreesAwarded = del.DegreesAwarded,
                                   Name = del.DegreeEducationLevelName
                                 }).ToList();
      }

      return degreeEducationLevels;
    }


    public List<DegreeEducationLevelDto> DegreeEducationLevelsByTier(long degreeId, int tier)
    {
      var degreeEducationLevels = new List<DegreeEducationLevelDto>();

      var jobDegreeEducationLevels = JobDegreeEducationLevels;
      if (jobDegreeEducationLevels != null && jobDegreeEducationLevels.Any(x => x.Tier == tier))
      {
        degreeEducationLevels = (from del in jobDegreeEducationLevels
                                 where del.DegreeId == degreeId && del.Tier == tier
                                 orderby del.EducationLevel ascending
                                 select new DegreeEducationLevelDto()
                                 {
                                   Id = del.DegreeEducationLevelId,
                                   DegreeId = del.DegreeId,
                                   EducationLevel = del.EducationLevel,
                                   DegreesAwarded = del.DegreesAwarded,
                                   Name = del.DegreeEducationLevelName
                                 }).ToList();
      }

      return degreeEducationLevels;
    }

    public List<DegreeEducationLevelDto> ProgramAreaDegreeEducationLevelsByTier(List<long> degreeIds, int tier)
    {
      var degreeEducationLevels = new List<DegreeEducationLevelDto>();

      var jobDegreeEducationLevels = JobDegreeEducationLevels;
      if (jobDegreeEducationLevels != null && jobDegreeEducationLevels.Any(x => x.Tier == tier))
      {
        degreeEducationLevels = (from del in jobDegreeEducationLevels
                                 where degreeIds.Contains(del.DegreeId) && del.Tier == tier
                                 orderby del.EducationLevel ascending
                                 select new DegreeEducationLevelDto()
                                 {
                                   Id = del.DegreeEducationLevelId,
                                   DegreeId = del.DegreeId,
                                   EducationLevel = del.EducationLevel,
                                   DegreesAwarded = del.DegreesAwarded,
                                   Name = del.DegreeEducationLevelName
                                 }).ToList();
      }

      return degreeEducationLevels;
    }

    [DataMember]
    public List<JobDegreeEducationLevelReportViewDto> JobDegreeEducationLevelReports { get; set; }

    [DataMember]
		public List<JobCertificationViewDto> JobCertifications { get; set; }
	}
}
