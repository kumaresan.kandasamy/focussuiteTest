﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Xml.Serialization;

#endregion

namespace Focus.Core.Models.Upload
{
  [Serializable]
  [XmlRoot(Namespace = "", IsNullable = false, ElementName = "JobUploadRecord")]
  public class JobUploadRecord : IUploadedRecord
  {
    [XmlElement("JobTitle")]
    public string JobTitle { get; set; }

    [XmlElement("Description")]
    public string Description { get; set; }

    [XmlElement("Zip")]
    public string Zip { get; set; }

    [XmlElement("NumberOfOpenings")]
    public int? NumberOfOpenings { get; set; }

    [XmlElement("ClosingOn")]
    public DateTime? ClosingOn { get; set; }

    [XmlElement("UserId")]
    public long? UserId { get; set; }

    [XmlElement("OnetId")]
    public long? OnetId { get; set; }

    [XmlElement("EmployeeId")]
    public long? EmployeeId { get; set; }

    [XmlElement("BusinessUnitId")]
    public long? BusinessUnitId { get; set; }

		[XmlElement("ApplicantCreditCheck")]
		public bool? ApplicantCreditCheck { get; set; }

		[XmlElement("ApplicantCriminalCheck")]
		public bool? ApplicantCriminalCheck { get; set; }
		
		[XmlElement("EmploymentStatusId")]
		public long? EmploymentStatusId { get; set; }

		[XmlElement("JobTypeId")]
		public long? JobTypeId { get; set; }
	}
}
