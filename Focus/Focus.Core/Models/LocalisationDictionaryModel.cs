﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Focus.Common.Models
{
  [Serializable]
  public class LocalisationDictionary : Dictionary<string, LocalisationDictionaryEntry>
  {
    public LocalisationDictionary()
    {
    }

    public LocalisationDictionary(SerializationInfo info, StreamingContext context)
    {
    }

    public LocalisationDictionary(IEnumerable<LocalisationDictionaryEntry> dict)
    {
      foreach (var li in dict.Where(li => !ContainsKey(li.LocaliseKeyUpper)))
      {
        Add(li.LocaliseKeyUpper, li);
      }
    }

    public LocalisationDictionaryEntry GetValue(string key)
    {
      var keyUpper = key.ToUpper();
      return ContainsKey(keyUpper)
               ? this[keyUpper]
               : null;
    }
  }
}
