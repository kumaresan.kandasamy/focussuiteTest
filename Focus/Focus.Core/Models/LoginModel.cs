﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

#endregion

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Views;

namespace Focus.Core.Models
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class LoginModel
	{
		/// <summary>
		/// Details of the authenticated user (Not all fields need to be populated though)
		/// </summary>
		[DataMember]
		public ValidatedUserView UserView { get; set; }

		/// <summary>
		/// If provider can't authenticate user, all falling back to the Focus provider
		/// </summary>
		[DataMember]
		public bool FallbackToFocus { get; set; }

		/// <summary>
		/// If user did not exist in Focus, whether a new user can be added
		/// If user did exist, whether it can be updated
		/// </summary>
		[DataMember]
		public bool UpdateFocus { get; set; }

		/// <summary>
		/// Whether the user record should be updated from details return from an integration call
		/// </summary>
		[DataMember]
		public bool UpdateFromIntegration { get; set; }

		/// <summary>
		/// Whether to disregard the check on the Enabled flag
		/// </summary>
		[DataMember]
		public bool BypassEnabledCheck { get; set; }

		/// <summary>
		/// Any action to log if the user record has to be created in Focus
		/// </summary>
		[DataMember]
		public ActionTypes UserLogAction { get; set; }

		/// <summary>
		/// Any error raised from authentication
		/// </summary>
		[DataMember]
		public ErrorTypes FailureType { get; set; }

		/// <summary>
		/// Any specific error message from authentication
		/// </summary>
		[DataMember]
		public string FailureMessage { get; set; }
	}
}
