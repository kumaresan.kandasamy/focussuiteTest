﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

#endregion

using System;
using System.Xml.Serialization;

namespace Focus.Core.Models.Validation
{
  [Serializable]
  public class FieldValidationResult
  {
    [XmlAttribute("number")]
    public int FieldNumber { get; set; }

    [XmlAttribute("status")]
    public ValidationRecordStatus Status { get; set; }

    [XmlAttribute("message")]
    public string Message { get; set; }

    [XmlAttribute("originalvalue")]
    public string OriginalValue { get; set; }
  }
}
