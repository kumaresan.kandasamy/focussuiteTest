﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Models
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobCareerPathModel
	{
    [DataMember]
    public JobDto Job { get; set; }

    [DataMember]
    public int Rank { get; set; }

    [DataMember]
    public ExplorerJobReportView JobReportDetails { get; set; }

    [DataMember]
    public List<JobCareerPathModel> CareerPathFrom { get; set; }

    [DataMember]
    public List<JobCareerPathModel> CareerPathTo { get; set; }
	}
}
