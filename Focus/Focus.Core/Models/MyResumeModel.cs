﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion 


namespace Focus.Core.Models
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class MyResumeModel
	{
		private List<string> _skills;

    [DataMember]
    public ExplorerResumeModel ResumeModel { get; set; }

    [DataMember]
    public long? PrimaryJobId { get; set; }

    [DataMember]
    public List<string> Skills
		{
			get
			{
				if (_skills == null)
				{
          if (ResumeModel != null && ResumeModel.Resume != null && !string.IsNullOrWhiteSpace(ResumeModel.Resume.ResumeSkills))
					{
						_skills = (List<string>)ResumeModel.Resume.ResumeSkills.Deserialize(typeof(List<string>));
						// just in case it didn't happen when it was originally saved
						// make all skills lowercase
						if (_skills.Count > 0)
						{
							for (int i = 0; i < _skills.Count; i++)
							{
								_skills[i] = _skills[i].ToLower();
							}
						}
					}
					else
					{
						_skills = new List<string>();
					}					
				}

				return _skills;
			}
      set { _skills = value; }
		}

		public bool CanPersonaliseReport
		{
			get { return PrimaryJobId.HasValue && PrimaryJobId != 0;}
		}

		public bool CanPersonaliseSkills
		{
			get { return Skills != null && Skills.Count > 0; }
		}
	}
}
