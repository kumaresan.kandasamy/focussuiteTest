﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Focus.Core.Models
{
    public class JobsSentInAlertsModel
    {
        public string LensPostingId { get; set; }
        public string JobTitle { get; set; }
        public string OpenDate { get; set; }
    }
}
