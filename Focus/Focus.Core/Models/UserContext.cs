﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;
using System.Threading;
using System.Web;

#endregion

namespace Focus.Core.Models
{	
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UserContext : IUserContext
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="UserContext"/> class.
		/// </summary>
		public UserContext()
		{
      Culture = Thread.CurrentThread.CurrentUICulture.ToString().ToUpperInvariant();
			//Culture = Constants.DefaultCulture;
			IsEnabled = true;
		}

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="UserContext" /> class.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="actionerId">The actioner id. This could be different to the user id if the user has signed on via single sign on.</param>
		/// <param name="firstName">The first name.</param>
		/// <param name="lastName">The last name.</param>
		/// <param name="emailAddress">The email address.</param>
		/// <param name="personId">The person id.</param>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="employerId">The employer id.</param>
		/// <param name="screenName">The screen name.</param>
		/// <param name="userName">Name of the user.</param>
		public UserContext(long userId, long actionerId, string firstName, string lastName, string emailAddress, long? personId, long? employeeId, long? employerId, string screenName, string userName ) : this(userId, actionerId, firstName, lastName, emailAddress, personId, employeeId, employerId, "", "", "", "", screenName, userName)
		{}

		/// <summary>
		/// Initializes a new instance of the <see cref="UserContext" /> class.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="firstName">The first name.</param>
		/// <param name="lastName">The last name.</param>
		/// <param name="emailAddress">The email address.</param>
		/// <param name="personId">The person id.</param>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="employerId">The employer id.</param>
		/// <param name="externalUserId">The external user id.</param>
		/// <param name="externalUserName">Name of the external user.</param>
		/// <param name="externalPassword">The external password.</param>
		/// <param name="externalOfficeId">The external office id.</param>
		/// <param name="screenName">The screen name.</param>
		/// <param name="userName">Name of the user.</param>
		public UserContext(long userId, string firstName, string lastName, string emailAddress, long? personId, long? employeeId, long? employerId, string externalUserId, string externalUserName, string externalPassword, string externalOfficeId, string screenName, string userName) : this(userId, userId, firstName, lastName, emailAddress, personId, employeeId, employerId, externalUserId, externalUserName, externalPassword, externalOfficeId, screenName, userName)
		{}

		/// <summary>
		/// Initializes a new instance of the <see cref="UserContext" /> class.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="actionerId">The actioner id.</param>
		/// <param name="firstName">The first name.</param>
		/// <param name="lastName">The last name.</param>
		/// <param name="emailAddress">The email address.</param>
		/// <param name="personId">The person id.</param>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="employerId">The employer id.</param>
		/// <param name="externalUserId">The external user id.</param>
		/// <param name="externalUserName">Name of the external user.</param>
		/// <param name="externalPassword">The external password.</param>
		/// <param name="externalOfficeId">The external office id.</param>
		/// <param name="screenName">The screen name.</param>
		/// <param name="userName">Name of the user.</param>
		/// <param name="lastLoggedInOn">The last logged in on.</param>
		/// <param name="isMigrated">if set to <c>true</c> [is migrated].</param>
		/// <param name="regulationsConsent">if set to <c>true</c> [regulations consent].</param>
		public UserContext(long userId, long actionerId, string firstName, string lastName, string emailAddress, long? personId, long? employeeId, long? employerId, string externalUserId, string externalUserName, string externalPassword, string externalOfficeId, string screenName, string userName, DateTime? lastLoggedInOn = null, bool isMigrated = true, bool regulationsConsent = true)
		{
			UserId = userId;
			ActionerId = actionerId;
			FirstName = firstName;
			LastName = lastName;
			EmailAddress = emailAddress;

			PersonId = personId;
			EmployeeId = employeeId;
			EmployerId = employerId;

			ExternalUserId = externalUserId;
			ExternalUserName = externalUserName;
			ExternalPassword = externalPassword;
			ExternalOfficeId = externalOfficeId;
      
			Culture = Constants.DefaultCulture;

			LastLoggedInOn = lastLoggedInOn;

      ScreenName = (string.IsNullOrEmpty(screenName)) ? firstName : screenName;

			UserName = userName;

			IsMigrated = isMigrated;

			RegulationsConsent = regulationsConsent;
		}

		#endregion

		/// <summary>
		/// Gets or sets the user id.
		/// </summary>
		[DataMember]
		public long UserId { get; set; }

		/// <summary>
		/// Gets or sets the actioner id. This could be different to the user id if the user has signed on via single sign on.
		/// </summary>
		[DataMember]
		public long ActionerId { get; set; }

		/// <summary>
		/// Gets or sets the first name.
		/// </summary>
		[DataMember]
		public string FirstName { get; set; }

		/// <summary>
		/// Gets or sets the last name.
		/// </summary>
		[DataMember]
		public string LastName { get; set; }

		/// <summary>
		/// Gets or sets the email address.
		/// </summary>
		[DataMember]
		public string EmailAddress { get; set; }

		/// <summary>
		/// Gets or sets the user's screen name.
		/// </summary>
		[DataMember]
		public string ScreenName { get; set; }

		/// <summary>
		/// Gets or sets the person id.
		/// </summary>
		[DataMember]
		public long? PersonId { get; set; }

		/// <summary>
		/// Gets or sets the employee id.
		/// </summary>
		[DataMember]
		public long? EmployeeId { get; set; }

		/// <summary>
		/// Gets or sets the employer id.
		/// </summary>
		[DataMember]
		public long? EmployerId { get; set; }

		/// <summary>
		/// Gets or sets the external user id.
		/// </summary>
		[DataMember]
		public string ExternalUserId { get; set; }

		/// <summary>
		/// Gets or sets the name of the external user.
		/// </summary>
		[DataMember]
		public string ExternalUserName { get; set; }

		/// <summary>
		/// Gets or sets the external password.
		/// </summary>
		[DataMember]
		public string ExternalPassword { get; set; }

		/// <summary>
		/// Gets or sets the external office id.
		/// </summary>
		[DataMember]
		public string ExternalOfficeId { get; set; }

		/// <summary>
		/// Gets or sets the localisation id.
		/// </summary>
		[DataMember]
		public string Culture { get; set; }

		/// <summary>
		/// Gets or sets the last logged in on.
		/// </summary>
    [DataMember]
    public DateTime? LastLoggedInOn { get; set; }

		/// <summary>
		/// Gets or sets the name of the user.
		/// </summary>
		[DataMember]
		public string UserName { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is migrated.
		/// </summary>
		[DataMember]
		public bool IsMigrated { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is enabled.
		/// </summary>
		[DataMember]
		public bool IsEnabled { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [regulations consent].
		/// </summary>
		[DataMember]
		public bool RegulationsConsent { get; set; }

		/// <summary>
		/// Gets or sets the programme area id.
		/// </summary>
		[DataMember]
		public long? ProgramAreaId { get; set; }

		/// <summary>
		/// Gets or sets the degree id.
		/// </summary>
		[DataMember]
		public long? DegreeEducationLevelId { get; set; }

		/// <summary>
		/// Gets or sets the enrollment status.
		/// </summary>
		[DataMember]
		public SchoolStatus? EnrollmentStatus { get; set; }

    /// <summary>
    /// Gets or sets the campus id.
    /// </summary>
    [DataMember]
    public long? CampusId { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this user is blocked.
		/// </summary>
		[DataMember]
		public bool Blocked { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether this user is anonymous (they must have agreed to terms and conditions for this to be set)
    /// </summary>
    [DataMember]
    public bool IsAnonymous { get; set; }

		/// <summary>
		/// Gets a value indicating whether this instance is shadowing an user.
		/// </summary>
		[IgnoreDataMember]
		public bool IsShadowingUser
		{
			get { return UserId != ActionerId; }
		}

		/// <summary>
		/// Gets a value indicating whether this instance is authenticated.
		/// </summary>
		[IgnoreDataMember]
		public bool IsAuthenticated
		{
      get { return HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated && !IsAnonymous; }
		}

		/// <summary>
		/// Gets the default resume identifier.
		/// </summary>
		/// <value>
		/// The default resume identifier.
		/// </value>
		[DataMember]
		public long? DefaultResumeId
		{
			get
			{
				return (HttpContext.Current != null && HttpContext.Current.Items.Contains("Career:DefaultUserId"))
								 ? HttpContext.Current.Items["Career:DefaultUserId"] as long?
								 : null;
			}
			set { if (HttpContext.Current != null) HttpContext.Current.Items["Career:DefaultUserId"] = value; }
		}
	}

	#region Interface

	public interface IUserContext
	{
		/// <summary>
		/// Gets or sets the user id.
		/// </summary>
		[DataMember]
		long UserId { get; set; }

		/// <summary>
		/// Gets or sets the actioner id. This could be different to the user id if the user has signed on via single sign on.
		/// </summary>
		[DataMember]
		long ActionerId { get; set; }

		/// <summary>
		/// Gets or sets the first name.
		/// </summary>
		[DataMember]
		string FirstName { get; set; }

		/// <summary>
		/// Gets or sets the last name.
		/// </summary>
		[DataMember]
		string LastName { get; set; }

		/// <summary>
		/// Gets or sets the email address.
		/// </summary>
		[DataMember]
		string EmailAddress { get; set; }

		/// <summary>
		/// Gets or sets the user's screen name.
		/// </summary>
		[DataMember]
		string ScreenName { get; set; }

		/// <summary>
		/// Gets or sets the person id.
		/// </summary>
		[DataMember]
		long? PersonId { get; set; }

		/// <summary>
		/// Gets or sets the employee id.
		/// </summary>
		[DataMember]
		long? EmployeeId { get; set; }

		/// <summary>
		/// Gets or sets the employer id.
		/// </summary>
		[DataMember]
		long? EmployerId { get; set; }

		/// <summary>
		/// Gets or sets the external user id.
		/// </summary>
		[DataMember]
		string ExternalUserId { get; set; }

		/// <summary>
		/// Gets or sets the name of the external user.
		/// </summary>
		[DataMember]
		string ExternalUserName { get; set; }

		/// <summary>
		/// Gets or sets the external password.
		/// </summary>
		[DataMember]
		string ExternalPassword { get; set; }

		/// <summary>
		/// Gets or sets the external office id.
		/// </summary>
		[DataMember]
		string ExternalOfficeId { get; set; }

		/// <summary>
		/// Gets or sets the localisation id.
		/// </summary>
		[DataMember]
		string Culture { get; set; }

		/// <summary>
		/// Gets or sets the last logged in on.
		/// </summary>
		[DataMember]
		DateTime? LastLoggedInOn { get; set; }

		/// <summary>
		/// Gets or sets the name of the user.
		/// </summary>
		[DataMember]
		string UserName { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is migrated.
		/// </summary>
		[DataMember]
		bool IsMigrated { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is enabled.
		/// </summary>
		[DataMember]
		bool IsEnabled { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [regulations consent].
		/// </summary>
		[DataMember]
		bool RegulationsConsent { get; set; }

		/// <summary>
		/// Gets or sets the program area id.
		/// </summary>
		[DataMember]
		long? ProgramAreaId { get; set; }

		/// <summary>
		/// Gets or sets the degree id.
		/// </summary>
		[DataMember]
		long? DegreeEducationLevelId { get; set; }

		/// <summary>
		/// Gets or sets the enrollment status.
		/// </summary>
		[DataMember]
		SchoolStatus? EnrollmentStatus { get; set; }

    /// <summary>
    /// Gets or sets the campus id.
    /// </summary>
    [DataMember]
    long? CampusId { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this user is blocked.
		/// </summary>
		[DataMember]
		bool Blocked { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether this user is anonymous (they must have agreed to terms and conditions for this to be set)
    /// </summary>
    [DataMember]
    bool IsAnonymous { get; set; }

		/// <summary>
		/// Gets a value indicating whether this instance is shadowing an user.
		/// </summary>
		[IgnoreDataMember]
		bool IsShadowingUser { get; }

		/// <summary>
		/// Gets a value indicating whether this instance is authenticated.
		/// </summary>
		[IgnoreDataMember]
		bool IsAuthenticated { get; }

		/// <summary>
		/// Gets the default resume identifier.
		/// </summary>
		/// <value>
		/// The default resume identifier.
		/// </value>
		[DataMember]
		long? DefaultResumeId { get; }
	}

	#endregion
}
