﻿
using Focus.Core.Models.Career;
using Newtonsoft.Json;

namespace Focus.Core.Models.Integration
{
	public class CharacteristicsModel
	{
		public string Ssn { get; set; }
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string LastName { get; set; }
		public long? JobSeekerId { get; set; }
		public string ServiceId { get; set; }
		public ActivityId Activity { get; set; }
		public string DeliveryDate { get; set; }
		public string EnteredDate { get; set; }
		public long? SeekerCareerCenter { get; set; }
		public long? StaffCareerCenter { get; set; }
		public string StaffExternalId { get; set; }
		public EmploymentStatus? EmploymentStatus { get; set; }
		public EducationLevel? HighestGradeCompleted { get; set; }
		public SchoolStatus? EnrollmentStatus { get; set; }
		public long? CountyOfResidence { get; set; }
		public bool? MigrantSeasonalFarmWorkerVerified { get; set; }
		public long? JobId { get; set; }
        public int HomelessNoShelter { get; set; }
        public int HomelessWithShelter { get; set; }
        public int ExOffender { get; set; }
        public int RunawayYouth18OrUnder { get; set; }
        public int LowLevelLiteracy { get; set; }
        public int CulturalBarriers { get; set; }
        public int LowIncomeStatus { get; set; }
        public int SingleParent { get; set; }
        public int DisplacedHomemaker { get; set; }
        public string NoOfDependents { get; set; }
        public string EstMonthlyIncome { get; set; }
        public string PreferredLanguage { get; set; }
        public string NativeLanguage { get; set; }
        public string CommonLanguage { get; set; }
        public long? ActionId { get; set; }
        public long ActivityStatusId { get; set; }
		[JsonIgnore]
		public long? UserId { get; set; }
	}

    public class EmployeeCharacteristicsModel
    {
        public string FEIN { get; set; }
        public string BusinessLegalName { get; set; }
        public long? BusinessUnitId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public long? EmployeeId { get; set; }
        public string ServiceId { get; set; }
        public ActivityId Activity { get; set; }
        public string DeliveryDate { get; set; }
        public string EnteredDate { get; set; }
        public long? EmployerId { get; set; }
        public long? SeekerCareerCenter { get; set; }
        public long? StaffCareerCenter { get; set; }
        public string StaffExternalId { get; set; }
        public long? ActionId { get; set; }
        [JsonIgnore]
        public long? UserId { get; set; }
    }

	public class ActivityId
	{
		public string Key { get; set; }
		public string Name { get; set; }
	}
}
