﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Models.Integration
{
  public class EmployerModel
  {
    public EmployerModel()
    {
      Employees = new List<EmployeeModel>();
      TrackingId = Guid.NewGuid().ToString();
    }

    [DataMember]
    public bool IsActive { get; set; }

    [DataMember]
    public long Id { get; set; }
    [DataMember]
    public string ExternalId { get; set; }

    [DataMember]
    public long AdminId { get; set; }
    [DataMember]
    public string ExternalAdminId { get; set; }

    [DataMember]
    public EmployerStatusTypes Status { get; set; }
    [DataMember]
    public string Name { get; set; }
    [DataMember]
    public string LegalName { get; set; }
    [DataMember]
    public string FederalEmployerIdentificationNumber { get; set; }
    [DataMember]
    public bool IsValidFederalEmployerIdentificationNumber { get; set; }
    [DataMember]
    public string StateEmployerIdentificationNumber { get; set; }
    [DataMember]
    public bool IsRegistrationComplete { get; set; }
    [DataMember]
    public long OfficeId { get; set; }
    [DataMember]
    public string ExternalOfficeId { get; set; }

    [DataMember]
    public string Phone { get; set; }
    [DataMember]
    public string PhoneExt { get; set; }
    [DataMember]
    public PhoneTypes PhoneType { get; set; }
    [DataMember]
    public string AltPhone { get; set; }
    [DataMember]
    public string AltPhoneExt { get; set; }
    [DataMember]
    public PhoneTypes AltPhoneType { get; set; }
    [DataMember]
    public string Fax { get; set; }
    [DataMember]
    public string Email { get; set; }
    [DataMember]
    public string Url { get; set; }
    [DataMember]
    public string Naics { get; set; }
    [DataMember]
		public long Owner { get; set; }
		[DataMember]
		public long? AccountType { get; set; }
    [DataMember]
    public long? NoOfEmployees { get; set; }
    [DataMember]
    public string BusinessDescription { get; set; }
    [DataMember]
    public string BusinessInterests { get; set; }
    [DataMember]
    public List<EmployeeModel> Employees { get; set; }
    [DataMember]
    public List<ActivityModel> Activities { get; set; }
    [DataMember]
    public AddressModel Address { get; set; }

    [DataMember]
    public string Version { get; set; }
    [DataMember]
    public string TrackingId { get; set; }

    [DataMember]
    public bool PublicTransportAccessible { get; set; }

    [DataMember]
    public bool MoreDetailsRequired { get; set; }

    [DataMember]
    public List<BusinessUnitModel> BusinessUnits { get; set; }

  }

  public class BusinessUnitModel
  {
    // A business unit is the same structure as the employer so reuse that with some added fields
    [DataMember]
    public EmployerModel BusinessUnit { get; set; }

    [DataMember]
    public DateTime? ExternalCreatedDate { get; set; }

    [DataMember]
    public bool IsDefault { get; set; }
  }
}
