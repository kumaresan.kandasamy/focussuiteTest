﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

#endregion

namespace Focus.Core.Models.Integration
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  [Serializable]
  public class JobModel
  {
    [DataMember]
    public long Id { get; set; }
    [DataMember]
    public long PostingId { get; set; }
    [DataMember]
    public string LensId { get; set; }
		[DataMember]
		public long? OriginId { get; set; }
    [DataMember]
    public ApprovalStatuses ApprovalStatus { get; set; }
    [DataMember]
    public long OfficeId { get; set; }
    [DataMember]
    public string ExternalOfficeId { get; set; }
    [DataMember]
    public long AdminId { get; set; }
    [DataMember]
    public string ExternalAdminId { get; set; }
    [DataMember]
    public string OnetCode { get; set; }
    [DataMember]
    public string Onet17Code { get; set; }
    [DataMember]
    public string BGTOcc { get; set; }
    [DataMember]
    public string Title { get; set; }
    [DataMember]
    public string ExternalEmployerId { get; set; }
    [DataMember]
    public string ExternalBusinessUnitId { get; set; }
    [DataMember]
    public string BusinessUnitName { get; set; }
    [DataMember]
    public string BusinessUnitDescription { get; set; }
    [DataMember]
    public bool ConfidentialEmployer { get; set; }
    [DataMember]
    public int MinimumAge { get; set; }
    public string MinimumAgeReason { get; set; }
    [DataMember]
    public string BusinessUnitNaics { get; set; }
    [DataMember]
    public string Description { get; set; }
    [DataMember]
    public decimal? MinSalary { get; set; }
    [DataMember]
    public decimal? MaxSalary { get; set; }
    [DataMember]
    public string SalaryUnit { get; set; }
    [DataMember]
    public long? SalaryUnitId { get; set; }
    [DataMember]
    public EducationLevels? EducationLevel { get; set; }
    [DataMember]
    public long? EmploymentStatusId { get; set; }
    [DataMember]
    public decimal? HoursPerWeek { get; set; }
    [DataMember]
    public int? MinimumHoursPerWeek { get; set; }
    [DataMember]
    public int? MaximumHoursPerWeek { get; set; }
    [DataMember]
    public long? JobTypeId { get; set; }
		[DataMember]
		public long? DurationId { get; set; }
    [DataMember]
    public string Shift { get; set; }
    [DataMember]
    public long? ShiftId { get; set; }
    [DataMember]
    public JobStatuses JobStatus { get; set; }
    [DataMember]
    public DateTime CreatedDate { get; set; }
    [DataMember]
    public DateTime UpdateDate { get; set; }
    [DataMember]
    public DateTime? PostingDate { get; set; }
    [DataMember]
    public DateTime? ClosingDate { get; set; }
    [DataMember]
    public string Category { get; set; }
    [DataMember]
    public int JobSource { get; set; }
    [DataMember]
    public ContactMethods InterviewContactPreferences { get; set; }
    [DataMember]
    public string ContactEmail { get; set; }
    [DataMember]
    public string ContactUrl { get; set; }
    [DataMember]
    public string ContactAddress { get; set; }
    [DataMember]
    public string ContactFax { get; set; }
    [DataMember]
    public string ContactPhone { get; set; }
    [DataMember]
    public string ContactInPerson { get; set; }
    [DataMember]
    public string ContactOther { get; set; }
    [DataMember]
    public string Url { get; set; }
    [DataMember]
    public List<AddressModel> Addresses { get; set; }
    [DataMember]
    public bool PublicTransportAccessible { get; set; }
    [DataMember]
    public bool FederalContractor { get; set; }
    [DataMember]
    public string Version { get; set; }
    [DataMember]
    public long EmployeeId { get; set; }
    [DataMember]
    public string ExternalId { get; set; }
    [DataMember]
    public int Openings { get; set; }
    [DataMember]
    public string TrackingId { get; set; }
    [DataMember]
    public List<IntegrationMapping> IntegrationMappings { get; set; }

    [DataMember]
    public bool? ForeignLabourCertificationH2A { get; set; }
    [DataMember]
    public bool? ForeignLabourCertificationH2B { get; set; }
    [DataMember]
    public bool? ForeignLabourCertificationOther { get; set; }

    [DataMember]
    public bool? CourtOrderedAffirmativeAction { get; set; }

    [DataMember]
    public WorkOpportunitiesTaxCreditCategories? WorkOpportunitiesTaxCreditHires { get; set; }

    [DataMember]
    public bool? WorkDaysVary { get; set; }
    [DataMember]
    public DaysOfWeek? Weekdays { get; set; }

    [DataMember]
    public int? MinimumExperienceYears { get; set; }
    [DataMember]
    public int? MinimumExperienceMonths { get; set; }

    [DataMember]
    public long? DrivingLicenceClassId { get; set; }
    [DataMember]
    public bool? DrivingLicenceRequired { get; set; }
    [DataMember]
    public List<long> DrivingLicenceEndorsements { get; set; }

    [DataMember]
    public LeaveBenefits? LeaveBenefits { get; set; }
    [DataMember]
    public RetirementBenefits? RetirementBenefits { get; set; }
    [DataMember]
    public InsuranceBenefits? InsuranceBenefits { get; set; }
    [DataMember]
    public MiscellaneousBenefits? MiscellaneousBenefits { get; set; }
    [DataMember]
    public string OtherBenefitsDetails { get; set; }
    [DataMember]
    public HiringManager HiringManager { get; set; }

    public List<string> Certificates { get; set; }
    [DataMember]
    public List<string> Licences { get; set; }

    public List<JobReferral> Referrals { get; set; }

    public JobModel Clone()
    {
      return new JobModel
      {
        Id = this.Id,
        OfficeId = this.OfficeId,
        ExternalOfficeId = this.ExternalOfficeId,
        AdminId = this.AdminId,
        ExternalAdminId = this.ExternalAdminId,
        OnetCode = this.OnetCode,
        Title = this.Title,
        Description = this.Description,
        MinSalary = this.MinSalary,
        MaxSalary = this.MaxSalary,
        SalaryUnit = this.SalaryUnit,
        EducationLevel = this.EducationLevel,
        HoursPerWeek = this.HoursPerWeek,
				EmploymentStatusId = this.EmploymentStatusId,
				JobTypeId = this.JobTypeId,
				DurationId = this.DurationId,
        Shift = this.Shift,
        JobStatus = this.JobStatus,
        ClosingDate = this.ClosingDate,
        Category = this.Category,
        JobSource = this.JobSource,
        InterviewContactPreferences = this.InterviewContactPreferences,
        Url = this.Url,
        PublicTransportAccessible = this.PublicTransportAccessible,
        Version = this.Version,
        EmployeeId = this.EmployeeId,
        ExternalId = this.ExternalId,
        Openings = this.Openings,
        TrackingId = this.TrackingId,
        IntegrationMappings = this.IntegrationMappings,
        ForeignLabourCertificationH2A = this.ForeignLabourCertificationH2A,
        ForeignLabourCertificationH2B = this.ForeignLabourCertificationH2B,
        ForeignLabourCertificationOther = this.ForeignLabourCertificationOther,
        CourtOrderedAffirmativeAction = this.CourtOrderedAffirmativeAction,
        WorkOpportunitiesTaxCreditHires = this.WorkOpportunitiesTaxCreditHires,
        Weekdays = this.Weekdays,
        DrivingLicenceRequired = this.DrivingLicenceRequired,
        LeaveBenefits = this.LeaveBenefits,
        RetirementBenefits = this.RetirementBenefits,
        InsuranceBenefits = this.InsuranceBenefits,
        MiscellaneousBenefits = this.MiscellaneousBenefits,
        OtherBenefitsDetails = this.OtherBenefitsDetails,
        Addresses = this.Addresses,
        HiringManager = new HiringManager
        {
          EmployeeId = this.HiringManager.EmployeeId,
          UserId = this.HiringManager.UserId,
          PersonId = this.HiringManager.PersonId,
          FirstName = this.HiringManager.FirstName,
          Surname = this.HiringManager.Surname,
          Email = this.HiringManager.Email,
          PhoneNumber = this.HiringManager.PhoneNumber,
          ExternalId = this.HiringManager.ExternalId,
          Address = new AddressModel
          {
            AddressLine1 = this.HiringManager.Address.AddressLine1,
            AddressLine2 = this.HiringManager.Address.AddressLine2,
            AddressLine3 = this.HiringManager.Address.AddressLine3,
            City = this.HiringManager.Address.City,
            CountryId = this.HiringManager.Address.CountryId,
            CountyId = this.HiringManager.Address.CountyId,
            ExternalCountryId = this.HiringManager.Address.ExternalCountryId,
            ExternalCountyId = this.HiringManager.Address.ExternalCountyId,
            ExternalStateId = this.HiringManager.Address.ExternalStateId,
            PostCode = this.HiringManager.Address.PostCode,
            StateId = this.HiringManager.Address.StateId,
            TrackingId = this.HiringManager.Address.TrackingId
          }
        }
      };
    }

  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  [Serializable]
  public class HiringManager
  {
    [DataMember]
    public long EmployeeId { get; set; }

    [DataMember]
    public long UserId { get; set; }

    [DataMember]
    public long PersonId { get; set; }

    [DataMember]
    public string FirstName { get; set; }

    [DataMember]
    public string Surname { get; set; }

    [DataMember]
    public string Email { get; set; }

    [DataMember]
    public string PhoneNumber { get; set; }

    [DataMember]
    public string Extension { get; set; }

    [DataMember]
    public AddressModel Address { get; set; }

    [DataMember]
    public string ExternalId { get; set; }

  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  [Serializable]
  public class JobReferral
  {
    [DataMember]
    public string JobSeekerExternalId { get; set; }

    [DataMember]
    public string ApplicationExternalId { get; set; }
  }
}
