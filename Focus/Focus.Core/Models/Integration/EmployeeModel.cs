﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

#endregion

namespace Focus.Core.Models.Integration
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EmployeeModel
  {
    public EmployeeModel()
    {
      TrackingId = Guid.NewGuid().ToString();
    }
    [DataMember]
    public long Id { get; set; }
    [DataMember]
    public string ExternalId { get; set; }
    [DataMember]
    public string MigrationId { get; set; }

    [DataMember]
    public long EmployerId { get; set; }
    [DataMember]
    public string ExternalEmployerId { get; set; }

    [DataMember]
    public long? AdminId { get; set; }
    [DataMember]
    public string ExternalAdminId { get; set; }

    [DataMember]
    public string FirstName { get; set; }
    [DataMember]
    public string LastName { get; set; }

    [DataMember]
    public long SalutationId { get; set; }

    [DataMember]
    public string JobTitle { get; set; }

    [DataMember]
    public string Phone { get; set; }
    [DataMember]
    public string PhoneExt { get; set; }
    [DataMember]
    public string AltPhone { get; set; }
    [DataMember]
    public string AltPhoneExt { get; set; }
    [DataMember]
    public string Fax { get; set; }
    [DataMember]
    public string Email { get; set; }
    [DataMember]
    public string Responsibilities { get; set; }
    [DataMember]
    public string Comments { get; set; }
    [DataMember]
    public AddressModel Address { get; set; }
    [DataMember]
    public string Version { get; set; }
    [DataMember]
    public string TrackingId { get; set; }
  }
}
