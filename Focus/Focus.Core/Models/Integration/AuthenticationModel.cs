﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Focus.Core.Models.Integration
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  [Serializable]
  public class AuthenticationModel
  {
    [DataMember]
    public string ExternalOfficeId { get; set; }

    [DataMember]
    public string ExternalAdminId { get; set; }
  }
}
