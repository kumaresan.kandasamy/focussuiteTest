﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Models.Integration
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  [Serializable]
  public class StaffModel
  {
    [DataMember]
    public string ExternalId { get; set; }

    [DataMember]
    public string Password { get; set; }

    [DataMember]
    public string Version { get; set; }

    [DataMember]
    public string Email { get; set; }

    [DataMember]
    public string FirstName { get; set; }

    [DataMember]
    public string MiddleInitial { get; set; }

    [DataMember]
    public string Surname { get; set; }

    [DataMember]
    public int? Saultation { get; set; }

    [DataMember]
    public string JobTitle { get; set; }

    [DataMember]
    public string PhoneNumber { get; set; }

    [DataMember]
    public AuthenticationModel AuthModel { get; set; }
  }
}
