﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Models.Integration
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class IntegrationMapping
  {
    [DataMember]
    public long InternalId { get; set; }
    [DataMember]
    public string ExternalId { get; set; }
    [DataMember]
    public long AssociatedInternalId { get; set; }
    [DataMember]
    public string AssociatedExternalId { get; set; }
    [DataMember]
    public string TrackingId { get; set; }
  }
}
