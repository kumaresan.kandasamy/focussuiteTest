﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Models.Integration
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobSeekerModel
  {
    [DataMember]
    public long Id { get; set; }

    [DataMember]
    public string ExternalId { get; set; }

    [DataMember]
    public string Version { get; set; }

    [DataMember]
    public string OfficeId { get; set; }

    [DataMember]
    public string AdminId { get; set; }

    [DataMember]
    public string Username { get; set; }

    [DataMember]
    public string Password { get; set; }

    [DataMember]
    public string ExternalUsername { get; set; }

    [DataMember]
    public string ExternalPassword { get; set; }

    [DataMember]
    public string EmailAddress { get; set; }

    [DataMember]
    public string FirstName { get; set; }

    [DataMember]
    public string LastName { get; set; }

    [DataMember]
    public string MiddleInitial { get; set; }

    [DataMember]
    public string SocialSecurityNumber { get; set; }

    [DataMember]
    public bool UiClaimant { get; set; }

    [DataMember]
    public string PrimaryOnet { get; set; }

    [DataMember]
    public ResumeCompletionStatuses? DefaultResumeCompletionStatus { get; set; }
    
    [DataMember]
    public ResumeModel Resume { get; set; }

    [DataMember]
    public string ResumeXml { get; set; }

    [DataMember]
    public string AddressLine1 { get; set; }

		[DataMember]
		public string AddressLine2 { get; set; }

    [DataMember]
    public string AddressTownCity { get; set; }

    [DataMember]
    public long? AddressStateId { get; set; }

    [DataMember]
    public long? AddressCountyId { get; set; }

    [DataMember]
    public string AddressPostcodeZip { get; set; }

    [DataMember]
    public long? AddressCountryId { get; set; }

    [DataMember]
    public DateTime? DateOfBirth { get; set; }

    [DataMember]
    public string PrimaryPhone { get; set; }

    [DataMember]
    public SchoolStatus? EnrollmentStatus { get; set; }

    [DataMember]
    public List<ResumeImportModel> Resumes { get; set; }

    [DataMember]
    public ResumeStatistics Stats { get; set; }

		[DataMember]
		public bool JobSeekerFlag { get; set; }

		[DataMember]
		public int SeekerStatusCd { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ResumeImportModel
  {
    [DataMember]
    public ResumeModel Resume { get; set; }

    [DataMember]
    public string ExternalId { get; set; }

    [DataMember]
    public bool IsDefault { get; set; }

    [DataMember]
    public DateTime LastUpdated { get; set; }

    [DataMember]
    public byte[] DocumentBytes { get; set; }

    [DataMember]
    public string FileName { get; set; }

    [DataMember]
    public string ContentType { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public struct ResumeStatistics
  {
    [DataMember]
    public int ActiveResumes { get; set; }

    [DataMember]
    public int SearchableResumes { get; set; }
  }
}
