﻿

using System.Collections.Generic;

namespace Focus.Core.Models.Integration
{
	public class ActiveJobModel
	{
		public string FederalEmployerIdentificationNumber { get; set; }
		public long JobId { get; set; }
		public JobStatuses? JobStatus { get; set; }
		public string JobTitle { get; set; }
		public string JobDescription { get; set; }
		public string JobOffice { get; set; } 
		public long? Onet { get; set; }
		public string JobCity { get; set; }
		public string JobState { get; set; }
		public string JobZip { get; set; }
		public string DatePosted { get; set; }
		public string ExpirationDate { get; set; }
		public decimal? MinSalary { get; set; }
		public decimal? MaxSalary { get; set; }
		public long? SalaryFrequencyId { get; set; }
		public bool? HideSalaryOnPosting { get; set; }
		public long? EmploymentStatusId { get; set; } 
		public string Url { get; set; }
		public string EmployerCity { get; set; }
		public long? EmployerStateId { get; set; }
		public EducationLevels? MinimumEducationLevel { get; set; }
		public int? MinimumExperienceYears { get; set; }
		public int? MinimumExperienceMonths { get; set; }
		public bool? HideEducationOnPosting { get; set; }
		public bool? HideExperienceOnPosting { get; set; }
	}
}
