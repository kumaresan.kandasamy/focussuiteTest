﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Models.Integration
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ActivityModel
  {
    [DataMember]
    public DateTime ActivityDate { get; set; }

    [DataMember]
    public long AdminId { get; set; }

    [DataMember]
    public string ExternalAdminId { get; set; }

    [DataMember]
    public long OfficeId { get; set; }

    [DataMember]
    public string ExternalOfficeId { get; set; }

    [DataMember]
    public long EmployeeId { get; set; }
    [DataMember]
    public string ExternalEmployeeId { get; set; }

    [DataMember]
    public string Comments { get; set; }

    [DataMember]
    public long ActivityType { get; set; }

    [DataMember]
    public string ExternalActivityType { get; set; }

    [DataMember]
    public string TrackingId { get; set; }
  }
}
