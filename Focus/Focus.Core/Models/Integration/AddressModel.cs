﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Models.Integration
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class AddressModel
  {
    [DataMember]
    public string AddressLine1 { get; set; }

    [DataMember]
    public string AddressLine2 { get; set; }

    [DataMember]
    public string AddressLine3 { get; set; }

    [DataMember]
    public string City { get; set; }

    [DataMember]
    public long CountyId { get; set; }

    [DataMember]
    public string ExternalCountyId { get; set; }

    [DataMember]
    public long StateId { get; set; }

    [DataMember]
    public string State { get; set; }

    [DataMember]
    public string ExternalStateId { get; set; }

    [DataMember]
    public long CountryId { get; set; }

    [DataMember]
    public string Country { get; set; }

    [DataMember]
    public string ExternalCountryId { get; set; }

    [DataMember]
    public string PostCode { get; set; }

    [DataMember]
    public string TrackingId { get; set; }

    [DataMember]
    public double? Longitude { get; set; }

    [DataMember]
    public double? Latitude { get; set; }

    [DataMember]
    public bool FromJobAddress { get; set; }

    [DataMember]
    public string OfficeExternalId { get; set; }


    public AddressModel Clone()
    {
      return new AddressModel
               {
                 AddressLine1 = this.AddressLine1,
                 AddressLine2 = this.AddressLine2,
                 AddressLine3 = this.AddressLine3,
                 City = this.City,
                 CountyId = this.CountyId,
                 ExternalCountyId = this.ExternalCountyId,
                 StateId = this.StateId,
                 ExternalStateId = this.ExternalStateId,
                 CountryId = this.CountryId,
                 ExternalCountryId = this.ExternalCountryId,
                 PostCode = this.PostCode,
                 TrackingId = this.TrackingId
               };
    }
  }
}
