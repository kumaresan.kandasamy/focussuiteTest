﻿#region Copyright © 2000 - 2016 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Models.Integration
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ResumeBasicModel
	{
		[DataMember]
		public long Id { get; set; }

		[DataMember]
		public long PersonId { get; set; }

		[DataMember]
		public bool IsDefault { get; set; }

		public EducationLevel? HighestEducationLevel { get; set; }

		public long? CountyId { get; set; }

		public bool? MigrantSeasonalFarmWorker { get; set; }

        public bool HomelessNoShelter { get; set; }
        public bool HomelessWithShelter { get; set; }
        public bool ExOffender { get; set; }
        public bool RunawayYouth18OrUnder { get; set; }

        public bool LowLevelLiteracy { get; set; }

        public bool CulturalBarriers { get; set; }

        public bool LowIncomeStatus { get; set; }

        public bool SingleParent { get; set; }

        public bool DisplacedHomemaker { get; set; }

        public string NoOfDependents { get; set; }

        public string EstMonthlyIncome { get; set; }

        public string PreferredLanguage { get; set; }

        public string NativeLanguage { get; set; }

        public string CommonLanguage { get; set; }

	}
}
