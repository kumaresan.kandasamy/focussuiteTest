﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

#endregion

namespace Focus.Core.Models
{
	public class UpdateSsnModel
	{
		public long PersonId { get; set; }
		public string CurrentSsn { get; set; }
		public string NewSsn { get; set; }

		public ErrorTypes Error { get; set; }
		public string ErrorText { get; set; }
	}
}
