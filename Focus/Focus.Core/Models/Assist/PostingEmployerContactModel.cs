﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;
using System.Text;
using Framework.Core;

#endregion

namespace Focus.Core.Models.Assist
{
	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class PostingEmployerContactModel
	{
		/// <summary>
		/// Gets or sets the name of the job seeker.
		/// </summary>
		/// <value>The name of the job seeker.</value>
    [DataMember]
    public string JobSeekerName { get; set; }

		/// <summary>
		/// Gets or sets the posting contact details.
		/// </summary>
		/// <value>The posting contact details.</value>
    [DataMember]
    public PostingContactDetailsModel PostingContactDetails { get; set; } 
	}

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class PostingContactDetailsModel
	{
		/// <summary>
		/// Gets or sets the name of the employer.
		/// </summary>
		/// <value>The name of the employer.</value>
    [DataMember]
    public string EmployerName { get; set; }

		/// <summary>
		/// Gets or sets the name of the contact.
		/// </summary>
		/// <value>The name of the contact.</value>
    [DataMember]
    public string ContactName { get; set; }

		/// <summary>
		/// Gets or sets the email address.
		/// </summary>
		/// <value>The email address.</value>
    [DataMember]
    public string EmailAddress { get; set; }

		/// <summary>
		/// Gets or sets the telephone number.
		/// </summary>
		/// <value>The telephone number.</value>
    [DataMember]
    public string TelephoneNumber { get; set; }

		/// <summary>
		/// Gets or sets the address.
		/// </summary>
		/// <value>The address.</value>
    [DataMember]
    public PostingContactAddressModel Address { get; set; }
	}

	[Serializable]
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class PostingContactAddressModel
	{
		/// <summary>
		/// Gets or sets the line1.
		/// </summary>
		/// <value>The line1.</value>
    [DataMember]
    public string Line1 { get; set; }

		/// <summary>
		/// Gets or sets the town city.
		/// </summary>
		/// <value>The town city.</value>
    [DataMember]
    public string TownCity { get; set; }

		/// <summary>
		/// Gets or sets the state.
		/// </summary>
		/// <value>The state.</value>
    [DataMember]
    public string State { get; set; }

		/// <summary>
		/// Gets or sets the postcode zip.
		/// </summary>
		/// <value>The postcode zip.</value>
    [DataMember]
    public string PostcodeZip { get; set; }

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			var stringBuilder = new StringBuilder("");
			
			if(Line1.IsNotNullOrEmpty())
			{
				stringBuilder.Append(Line1);

				if (TownCity.IsNotNullOrEmpty() || State.IsNotNullOrEmpty() || PostcodeZip.IsNotNullOrEmpty())
					stringBuilder.Append(", ");
			}

			if(TownCity.IsNotNullOrEmpty())
			{
				stringBuilder.Append(TownCity);

				if (State.IsNotNullOrEmpty() || PostcodeZip.IsNotNullOrEmpty())
					stringBuilder.Append(", ");
			}

			if (State.IsNotNullOrEmpty())
			{
				stringBuilder.Append(State);

				if (PostcodeZip.IsNotNullOrEmpty())
					stringBuilder.Append(" ");
			}

			if (PostcodeZip.IsNotNullOrEmpty())
				stringBuilder.Append(PostcodeZip);

			return stringBuilder.ToString();
		}
	}
}
