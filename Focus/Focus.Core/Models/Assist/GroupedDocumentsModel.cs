﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Models.Assist
{
	public class GroupedDocumentsModel
	{
		[DataMember]
		public long Group { get; set; }

		[DataMember]
		public string GroupName { get; set; }

		[DataMember]
		public List<DocumentDto> Documents { get; set; }
	}
}
