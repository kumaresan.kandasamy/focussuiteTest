﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Models.Assist
{
	public class OfficeDefaultAdministratorModel
	{
		[DataMember]
		public bool IsDefaultAdministrator { get; set; }

		[DataMember]
		public List<OfficeDto> Offices { get; set; }
	}
}
