﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion


namespace Focus.Core.Models.Assist
{
	public class ValidateFEINChangeModel
	{
		[DataMember]
		public bool IsValid { get; set; }

		[DataMember]
		public FEINChangeValidationMessages ValidationMessage { get; set; }

		[DataMember]
		public FEINChangeActions ActionToBeCarriedOut { get; set; }

		[DataMember]
		public string NewEmployerName { get; set; }

		[DataMember]
		public long? NewEmployerId { get; set; }

		[DataMember]
		public string BusinessUnitName { get; set; }
	}

	public enum FEINChangeValidationMessages
	{
		None,
		FEINNotChanged,
		EmployeeAssignedToSiblingBusinessUnit,
	}

	public enum FEINChangeActions
	{
		None,
		PrimaryBusinessUnitNewFEINNoSiblings,
		PrimaryBusinessUnitNewFEINSiblings,
		PrimaryBusinessUnitExistingFEINNoSiblings,
		PrimaryBusinessUnitExistingFEINSiblings,
		NonPrimaryBusinessUnitNewFEIN,
		NonPrimaryBusinessUnitExistingFEIN,
	}
}
