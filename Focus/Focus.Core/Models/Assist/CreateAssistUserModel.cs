﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Common.Models
{
	public class CreateAssistUserModel
	{
		public string AccountUserName { get; set; }
		public string AccountPassword { get; set; }
		public PersonDto UserPerson { get; set; }
		public PersonAddressDto UserAddress { get; set; }
		public string UserPhone { get; set; }
		public string UserAlternatePhone { get; set; }
		public string UserFax { get; set; }
		public string UserEmailAddress { get; set; }
		public bool ValidatePasswordStrength { get; set; }
		public string AccountExternalId { get; set; }
	  public long? AccountUserId { get; set; }

	  public bool IsEnabled { get; set; }
		public bool IsClientAuthenticated { get; set; }
		public string UserExternalOffice { get; set; }

		public List<ErrorTypes> RegistrationError { get; set; }
		public List<string> ErrorText { get; set; }
	}
}
