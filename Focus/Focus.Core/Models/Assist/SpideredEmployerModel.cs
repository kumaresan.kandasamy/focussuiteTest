﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Models.Assist
{
	[Serializable]
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SpideredEmployerModel
	{
		[DataMember]
		public long Id { get; set; }

		[DataMember]
		public string EmployerName { get; set; }

		[DataMember]
		public int NumberOfPostings { get; set; }

		[DataMember]
		public string LensPostingId { get; set; }

		[DataMember]
		public string JobTitle { get; set; }

		[DataMember]
		public DateTime PostingDate { get; set; }
	}
}
