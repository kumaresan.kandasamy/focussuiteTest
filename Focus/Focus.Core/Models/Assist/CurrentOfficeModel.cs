﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

#endregion

namespace Focus.Core.Models.Assist
{
	public class CurrentOfficeModel
	{
		public long PersonId { get; set; }
		public long OfficeId { get; set; }
		public DateTime StartTime { get; set; }
	}
}
