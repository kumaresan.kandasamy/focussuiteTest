﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

namespace Focus.Core.Models.Assist
{
	public class ApprovalQueueFilterModel
	{
		public JobListFilter? JobType { get; set; }
		public string EmployerType { get; set; }
		public long? OfficeId { get; set; }
		public long? StaffMemberId { get; set; }
		public VeteranFilterTypes? JobSeekerType { get; set; }
	}
}
