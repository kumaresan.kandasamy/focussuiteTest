﻿
using System;
using System.Runtime.Serialization;

namespace Focus.Core.DataTransferObjects.FocusCore
{
	public class PushNotificationDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public string Title { get; set; }
		[DataMember]
		public string Text { get; set; }
		[DataMember]
		public bool Sent { get; set; }
		[DataMember]
		public string PersonMobileDeviceToken { get; set; }
		[DataMember]
		public DateTime CreatedOn { get; set; }
		[DataMember]
		public bool Viewed { get; set; }
	}
}
