﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "UploadFile", Namespace = Constants.DataContractNamespace)]
  public class UploadFileDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public DateTime CreatedOn { get; set; }
    [DataMember]
    public DateTime UpdatedOn { get; set; }
    [DataMember]
    public long UserId { get; set; }
    [DataMember]
    public ValidationSchema SchemaType { get; set; }
    [DataMember]
    public ValidationRecordStatus Status { get; set; }
    [DataMember]
    public string RecordTypeName { get; set; }
    [DataMember]
    public UploadFileProcessingState ProcessingState { get; set; }
    [DataMember]
    public string FileName { get; set; }
    [DataMember]
    public UploadFileType FileType { get; set; }
    [DataMember]
    public string CustomInformation { get; set; }
    [DataMember]
    public byte[] FileData { get; set; }
  }

}
