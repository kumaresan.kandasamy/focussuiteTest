﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "SingleSignOn", Namespace = Constants.DataContractNamespace)]
  public class SingleSignOnDto
  {
		/// <summary>
		/// Gets or sets the identifier.
		/// </summary>
		/// <value>
		/// The identifier.
		/// </value>
    [DataMember]
    public Guid? Id { get; set; }

		/// <summary>
		/// Gets or sets the created on.
		/// </summary>
		/// <value>
		/// The created on.
		/// </value>
    [DataMember]
    public DateTime CreatedOn { get; set; }

		/// <summary>
		/// Gets or sets the user identifier.
		/// </summary>
		/// <value>
		/// The user identifier.
		/// </value>
    [DataMember]
    public long UserId { get; set; }

		/// <summary>
		/// Gets or sets the actioner identifier.
		/// </summary>
		/// <value>
		/// The actioner identifier.
		/// </value>
    [DataMember]
    public long ActionerId { get; set; }

		/// <summary>
		/// Gets or sets the external admin identifier.
		/// </summary>
		/// <value>
		/// The external admin identifier.
		/// </value>
		[DataMember]
		public string ExternalAdminId { get; set; }

		/// <summary>
		/// Gets or sets the external office identifier.
		/// </summary>
		/// <value>
		/// The external office identifier.
		/// </value>
		[DataMember]
		public string ExternalOfficeId { get; set; }

		/// <summary>
		/// Gets or sets the external password.
		/// </summary>
		/// <value>
		/// The external password.
		/// </value>
		[DataMember]
		public string ExternalPassword { get; set; }

		/// <summary>
		/// Gets or sets the external username.
		/// </summary>
		/// <value>
		/// The external username.
		/// </value>
		[DataMember]
		public string ExternalUsername { get; set; }
  }
}