﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "ResumeJob", Namespace = Constants.DataContractNamespace)]
  public class ResumeJobDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public string JobTitle { get; set; }
    [DataMember]
    public string Employer { get; set; }
    [DataMember]
    public string StartYear { get; set; }
    [DataMember]
    public string EndYear { get; set; }
    [DataMember]
    public Guid? LensId { get; set; }
    [DataMember]
    public long ResumeId { get; set; }
    [DataMember]
    public string JobOnet { get; set; }
  }
}