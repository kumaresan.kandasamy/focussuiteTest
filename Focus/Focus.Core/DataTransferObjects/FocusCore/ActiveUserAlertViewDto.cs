﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "ActiveUserAlertView", Namespace = Constants.DataContractNamespace)]
  public class ActiveUserAlertViewDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public long UserId { get; set; }
    [DataMember]
    public bool NewReferralRequests { get; set; }
    [DataMember]
    public bool EmployerAccountRequests { get; set; }
    [DataMember]
    public bool NewJobPostings { get; set; }
    [DataMember]
    public string Frequency { get; set; }
    [DataMember]
    public string FirstName { get; set; }
    [DataMember]
    public string LastName { get; set; }
    [DataMember]
    public string EmailAddress { get; set; }
  }
}