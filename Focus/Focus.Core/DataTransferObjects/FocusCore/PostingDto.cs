﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "Posting", Namespace = Constants.DataContractNamespace)]
	public class PostingDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public DateTime CreatedOn { get; set; }
		[DataMember]
		public DateTime UpdatedOn { get; set; }
		[DataMember]
		public string LensPostingId { get; set; }
		[DataMember]
		public string JobTitle { get; set; }
		[DataMember]
		public string PostingXml { get; set; }
		[DataMember]
		public string EmployerName { get; set; }
		[DataMember]
		public string Url { get; set; }
		[DataMember]
		public string ExternalId { get; set; }
		[DataMember]
		public PostingStatuses StatusId { get; set; }
		[DataMember]
		public long? OriginId { get; set; }
		[DataMember]
		public string Html { get; set; }
		[DataMember]
		public int ViewedCount { get; set; }
		[DataMember]
		public string JobLocation { get; set; }
		[DataMember]
		public long? JobId { get; set; }
	}
}