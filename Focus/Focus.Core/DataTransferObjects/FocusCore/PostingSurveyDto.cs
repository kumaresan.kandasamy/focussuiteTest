﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "PostingSurvey", Namespace = Constants.DataContractNamespace)]
  public class PostingSurveyDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public SurveyTypes SurveyType { get; set; }
    [DataMember]
    public long? SatisfactionLevel { get; set; }
    [DataMember]
    public bool? DidInterview { get; set; }
    [DataMember]
    public bool? DidHire { get; set; }
    [DataMember]
    public bool? DidOffer { get; set; }
    [DataMember]
    public long JobId { get; set; }
  }
}