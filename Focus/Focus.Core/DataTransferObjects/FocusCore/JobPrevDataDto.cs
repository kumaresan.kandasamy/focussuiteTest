﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract( Name = "JobPrevData", Namespace = Constants.DataContractNamespace )]
	public class JobPrevDataDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public DateTime? ClosingDate { get; set; }
		[DataMember]
		public int? NumOpenings { get; set; }
		[DataMember]
		public string Locations { get; set; }
		[DataMember]
		public string Description { get; set; }
		[DataMember]
		public string JobRequirements { get; set; }
		[DataMember]
		public string JobDetails { get; set; }
		[DataMember]
		public string JobSalaryBenefits { get; set; }
		[DataMember]
		public string RecruitmentInformation { get; set; }
		[DataMember]
		public string ForeignLabourCertificationDetails { get; set; }
		[DataMember]
		public string CourtOrderedAffirmativeActionDetails { get; set; }
		[DataMember]
		public string FederalContractorDetails { get; set; }
		[DataMember]
		public long JobId { get; set; }
	}
}
