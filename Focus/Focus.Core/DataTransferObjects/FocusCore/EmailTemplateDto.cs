﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "EmailTemplate", Namespace = Constants.DataContractNamespace)]
  public class EmailTemplateDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public EmailTemplateTypes EmailTemplateType { get; set; }
    [DataMember]
    public string Subject { get; set; }
    [DataMember]
    public string Body { get; set; }
    [DataMember]
    public string Salutation { get; set; }
    [DataMember]
    public string Recipient { get; set; }
		[DataMember]
		public SenderEmailTypes SenderEmailType { get; set; }
		[DataMember]
		public string ClientSpecificEmailAddress { get; set; }
  }
}