﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "JobAddress", Namespace = Constants.DataContractNamespace)]
  public class JobAddressDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public string Line1 { get; set; }
    [DataMember]
    public string Line2 { get; set; }
    [DataMember]
    public string Line3 { get; set; }
    [DataMember]
    public string TownCity { get; set; }
    [DataMember]
    public long? CountyId { get; set; }
    [DataMember]
    public string PostcodeZip { get; set; }
    [DataMember]
    public long StateId { get; set; }
    [DataMember]
    public long CountryId { get; set; }
    [DataMember]
    public bool IsPrimary { get; set; }
    [DataMember]
    public long JobId { get; set; }
  }
}