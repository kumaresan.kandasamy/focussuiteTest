﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract(Name = "Job", Namespace = Constants.DataContractNamespace)]
	public class JobDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public int LockVersion { get; set; }
		[DataMember]
		public DateTime CreatedOn { get; set; }
		[DataMember]
		public DateTime UpdatedOn { get; set; }
		[DataMember]
		public string JobTitle { get; set; }
		[DataMember]
		public long CreatedBy { get; set; }
		[DataMember]
		public long UpdatedBy { get; set; }
		[DataMember]
		public ApprovalStatuses ApprovalStatus { get; set; }
		[DataMember]
		public JobStatuses JobStatus { get; set; }
		[DataMember]
		public decimal? MinSalary { get; set; }
		[DataMember]
		public decimal? MaxSalary { get; set; }
		[DataMember]
		public long? SalaryFrequencyId { get; set; }
		[DataMember]
		public decimal? HoursPerWeek { get; set; }
		[DataMember]
		public bool? OverTimeRequired { get; set; }
		[DataMember]
		public DateTime? ClosingOn { get; set; }
		[DataMember]
		public int? NumberOfOpenings { get; set; }
		[DataMember]
		public DateTime? PostedOn { get; set; }
		[DataMember]
		public long? PostedBy { get; set; }
		[DataMember]
		public string Description { get; set; }
		[DataMember]
		public string PostingHtml { get; set; }
		[DataMember]
		public EmployerDescriptionPostingPositions EmployerDescriptionPostingPosition { get; set; }
		[DataMember]
		public int WizardStep { get; set; }
		[DataMember]
		public int WizardPath { get; set; }
		[DataMember]
		public DateTime? HeldOn { get; set; }
		[DataMember]
		public long? HeldBy { get; set; }
		[DataMember]
		public DateTime? ClosedOn { get; set; }
		[DataMember]
		public long? ClosedBy { get; set; }
		[DataMember]
		public JobLocationTypes? JobLocationType { get; set; }
		[DataMember]
		public bool? FederalContractor { get; set; }
		[DataMember]
		public bool? ForeignLabourCertification { get; set; }
		[DataMember]
		public bool? CourtOrderedAffirmativeAction { get; set; }
		[DataMember]
		public DateTime? FederalContractorExpiresOn { get; set; }
		[DataMember]
		public WorkOpportunitiesTaxCreditCategories? WorkOpportunitiesTaxCreditHires { get; set; }
		[DataMember]
		public JobPostingFlags? PostingFlags { get; set; }
		[DataMember]
		public bool? IsCommissionBased { get; set; }
		[DataMember]
		public DaysOfWeek? NormalWorkDays { get; set; }
		[DataMember]
		public bool? WorkDaysVary { get; set; }
		[DataMember]
		public long? NormalWorkShiftsId { get; set; }
		[DataMember]
		public LeaveBenefits? LeaveBenefits { get; set; }
		[DataMember]
		public RetirementBenefits? RetirementBenefits { get; set; }
		[DataMember]
		public InsuranceBenefits? InsuranceBenefits { get; set; }
		[DataMember]
		public MiscellaneousBenefits? MiscellaneousBenefits { get; set; }
		[DataMember]
		public string OtherBenefitsDetails { get; set; }
		[DataMember]
		public ContactMethods? InterviewContactPreferences { get; set; }
		[DataMember]
		public string InterviewEmailAddress { get; set; }
		[DataMember]
		public string InterviewApplicationUrl { get; set; }
		[DataMember]
		public string InterviewMailAddress { get; set; }
		[DataMember]
		public string InterviewFaxNumber { get; set; }
		[DataMember]
		public string InterviewPhoneNumber { get; set; }
		[DataMember]
		public string InterviewDirectApplicationDetails { get; set; }
		[DataMember]
		public string InterviewOtherInstructions { get; set; }
		[DataMember]
		public ScreeningPreferences? ScreeningPreferences { get; set; }
		[DataMember]
		public EducationLevels? MinimumEducationLevel { get; set; }
		[DataMember]
		public bool? MinimumEducationLevelRequired { get; set; }
		[DataMember]
		public int? MinimumAge { get; set; }
		[DataMember]
		public string MinimumAgeReason { get; set; }
		[DataMember]
		public bool? MinimumAgeRequired { get; set; }
		[DataMember]
		public bool? LicencesRequired { get; set; }
		[DataMember]
		public bool? CertificationRequired { get; set; }
		[DataMember]
		public bool? LanguagesRequired { get; set; }
		[DataMember]
		public string Tasks { get; set; }
		[DataMember]
		public string RedProfanityWords { get; set; }
		[DataMember]
		public string YellowProfanityWords { get; set; }
		[DataMember]
		public long? EmploymentStatusId { get; set; }
		[DataMember]
		public long? JobTypeId { get; set; }
		[DataMember]
		public long? JobStatusId { get; set; }
		[DataMember]
		public DateTime? ApprovedOn { get; set; }
		[DataMember]
		public long? ApprovedBy { get; set; }
		[DataMember]
		public string ExternalId { get; set; }
		[DataMember]
		public DateTime? AwaitingApprovalOn { get; set; }
		[DataMember]
		public int? MinimumExperience { get; set; }
		[DataMember]
		public int? MinimumExperienceMonths { get; set; }
		[DataMember]
		public bool? MinimumExperienceRequired { get; set; }
		[DataMember]
		public long? DrivingLicenceClassId { get; set; }
		[DataMember]
		public bool? DrivingLicenceRequired { get; set; }
		[DataMember]
		public bool? HideSalaryOnPosting { get; set; }
		[DataMember]
		public bool? ForeignLabourCertificationH2A { get; set; }
		[DataMember]
		public bool? ForeignLabourCertificationH2B { get; set; }
		[DataMember]
		public bool? ForeignLabourCertificationOther { get; set; }
		[DataMember]
		public bool? HiringFromTaxCreditProgramNotificationSent { get; set; }
		[DataMember]
		public bool? IsConfidential { get; set; }
		[DataMember]
		public bool HasCheckedCriminalRecordExclusion { get; set; }
		[DataMember]
		public DateTime? LastPostedOn { get; set; }
		[DataMember]
		public DateTime? LastRefreshedOn { get; set; }
		[DataMember]
		public int? MinimumAgeReasonValue { get; set; }
		[DataMember]
		public string OtherSalary { get; set; }
		[DataMember]
		public bool? HideOpeningsOnPosting { get; set; }
		[DataMember]
		public bool? StudentEnrolled { get; set; }
		[DataMember]
		public int? MinimumCollegeYears { get; set; }
		[DataMember]
		public bool? ProgramsOfStudyRequired { get; set; }
		[DataMember]
		public DateTime? StartDate { get; set; }
		[DataMember]
		public DateTime? EndDate { get; set; }
		[DataMember]
		public JobTypes JobType { get; set; }
		[DataMember]
		public long? OnetId { get; set; }
		[DataMember]
		public bool HideEducationOnPosting { get; set; }
		[DataMember]
		public bool HideExperienceOnPosting { get; set; }
		[DataMember]
		public bool HideMinimumAgeOnPosting { get; set; }
		[DataMember]
		public bool HideProgramOfStudyOnPosting { get; set; }
		[DataMember]
		public bool HideDriversLicenceOnPosting { get; set; }
		[DataMember]
		public bool HideLicencesOnPosting { get; set; }
		[DataMember]
		public bool HideCertificationsOnPosting { get; set; }
		[DataMember]
		public bool HideLanguagesOnPosting { get; set; }
		[DataMember]
		public bool HideSpecialRequirementsOnPosting { get; set; }
		[DataMember]
		public bool HideWorkWeekOnPosting { get; set; }
		[DataMember]
		public int? DescriptionPath { get; set; }
		[DataMember]
		public long? WorkWeekId { get; set; }
		[DataMember]
		public long? ROnetId { get; set; }
		[DataMember]
		public bool? CriminalBackgroundExclusionRequired { get; set; }
		[DataMember]
		public bool? IsSalaryAndCommissionBased { get; set; }
		[DataMember]
		public DateTime? VeteranPriorityEndDate { get; set; }
		[DataMember]
		public bool MeetsMinimumWageRequirement { get; set; }
		[DataMember]
		public decimal? MinimumHoursPerWeek { get; set; }
		[DataMember]
		public bool? PreScreeningServiceRequest { get; set; }
		[DataMember]
		public bool? SuitableForHomeWorker { get; set; }
		[DataMember]
		public bool HideCareerReadinessOnPosting { get; set; }
		[DataMember]
		public long? CareerReadinessLevel { get; set; }
		[DataMember]
		public bool? CareerReadinessLevelRequired { get; set; }
		[DataMember]
		public ExtendVeteranPriorityOfServiceTypes? ExtendVeteranPriority { get; set; }
		[DataMember]
		public bool? ClosingOnUpdated { get; set; }
		[DataMember]
		public bool UploadIndicator { get; set; }
		[DataMember]
		public string Location { get; set; }
		[DataMember]
		public bool? CreditCheckRequired { get; set; }
		[DataMember]
		public bool DisplayBenefits { get; set; }
		[DataMember]
		public long? AwaitingApprovalActionedBy { get; set; }
		[DataMember]
		public long? BusinessUnitId { get; set; }
		[DataMember]
		public long? EmployeeId { get; set; }
		[DataMember]
		public long EmployerId { get; set; }
		[DataMember]
		public long? AssignedToId { get; set; }
		[DataMember]
		public long? BusinessUnitDescriptionId { get; set; }
		[DataMember]
		public long? BusinessUnitLogoId { get; set; }
		[DataMember]
		public ApprovalStatuses PreviousApprovalStatus { get; set; }
	}
}
