﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "SessionState", Namespace = Constants.DataContractNamespace)]
  public class SessionStateDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public DateTime? DeletedOn { get; set; }
    [DataMember]
    public string Key { get; set; }
    [DataMember]
    public byte[] Value { get; set; }
    [DataMember]
    public string TypeOf { get; set; }
    [DataMember]
    public long Size { get; set; }
    [DataMember]
    public Guid SessionId { get; set; }
  }
}