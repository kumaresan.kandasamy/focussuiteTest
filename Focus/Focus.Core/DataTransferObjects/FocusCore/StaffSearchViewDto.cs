﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract(Name = "StaffSearchView", Namespace = Constants.DataContractNamespace)]
	public class StaffSearchViewDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public string FirstName { get; set; }
		[DataMember]
		public string LastName { get; set; }
		[DataMember]
		public bool Enabled { get; set; }
		[DataMember]
		public string EmailAddress { get; set; }
		[DataMember]
		public long PersonId { get; set; }
		[DataMember]
		public bool? Manager { get; set; }
		[DataMember]
		public string ExternalOffice { get; set; }
		[DataMember]
		public bool Blocked { get; set; }
	}
}