﻿using System;
using System.Runtime.Serialization;

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "Localisation", Namespace = Constants.DataContractNamespace)]
  public class LocalisationDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public string Culture { get; set; }
  }
}