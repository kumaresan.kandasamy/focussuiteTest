﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract(Name = "InviteToApplyBasicView", Namespace = Constants.DataContractNamespace)]
  public class InviteToApplyBasicViewDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public DateTime CreatedOn { get; set; }
    [DataMember]
    public long JobId { get; set; }
    [DataMember]
    public string FirstName { get; set; }
    [DataMember]
    public string LastName { get; set; }
    [DataMember]
    public long PersonId { get; set; }
    [DataMember]
    public string Town { get; set; }
    [DataMember]
    public long? StateId { get; set; }
    [DataMember]
    public string Branding { get; set; }
    [DataMember]
    public bool IsContactInfoVisible { get; set; }
    [DataMember]
    public int? YearsExperience { get; set; }
    [DataMember]
    public long? NcrcLevelId { get; set; }
    [DataMember]
    public bool CandidateIsVeteran { get; set; }
    [DataMember]
    public bool? InvitationQueued { get; set; }
  }

}