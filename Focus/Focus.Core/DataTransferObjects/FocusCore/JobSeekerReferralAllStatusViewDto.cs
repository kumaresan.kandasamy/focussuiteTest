﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract(Name = "JobSeekerReferralAllStatusView", Namespace = Constants.DataContractNamespace)]
	public class JobSeekerReferralAllStatusViewDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public DateTime ReferralDate { get; set; }
		[DataMember]
		public string JobTitle { get; set; }
		[DataMember]
		public string EmployerName { get; set; }
		[DataMember]
		public long EmployerId { get; set; }
		[DataMember]
		public string Posting { get; set; }
		[DataMember]
		public long CandidateId { get; set; }
		[DataMember]
		public long JobId { get; set; }
		[DataMember]
		public string ApprovalRequiredReason { get; set; }
		[DataMember]
		public bool? IsVeteran { get; set; }
		[DataMember]
		public string Town { get; set; }
		[DataMember]
		public long StateId { get; set; }
		[DataMember]
		public long PersonId { get; set; }
		[DataMember]
		public int ApplicationScore { get; set; }
		[DataMember]
		public long? AssignedToId { get; set; }
		[DataMember]
		public ApprovalStatuses ApprovalStatus { get; set; }
		[DataMember]
		public ApplicationStatusTypes ApplicationStatus { get; set; }
		[DataMember]
		public string State { get; set; }
		[DataMember]
		public int? TimeInQueue { get; set; }
		[DataMember]
		public string LensPostingId { get; set; }
		[DataMember]
		public DateTime? StatusLastChangedOn { get; set; }
		[DataMember]
		public long? StatusLastChangedBy { get; set; }
		[DataMember]
		public bool AutomaticallyDenied { get; set; }
		[DataMember]
		public bool? IsConfidential { get; set; }
		[DataMember]
		public int LockVersion { get; set; }
		[DataMember]
		public bool? AutomaticallyOnHold { get; set; }
		[DataMember]
		public bool? ForeignLabourCertificationH2A { get; set; }
		[DataMember]
		public bool? ForeignLabourCertificationH2B { get; set; }
		[DataMember]
		public string JobseekerTown { get; set; }
		[DataMember]
		public long JobseekerStateId { get; set; }
		[DataMember]
		public string JobLocation { get; set; }
		[DataMember]
		public string JobSeekerAddress { get; set; }
		[DataMember]
		public string PostingLocations { get; set; }
	}
}
