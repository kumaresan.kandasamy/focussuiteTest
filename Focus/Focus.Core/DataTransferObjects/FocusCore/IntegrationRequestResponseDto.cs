﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "IntegrationRequestResponse", Namespace = Constants.DataContractNamespace)]
	public class IntegrationRequestResponseDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public string Url { get; set; }
		[DataMember]
		public DateTime RequestSentOn { get; set; }
		[DataMember]
		public DateTime? ResponseReceivedOn { get; set; }
		[DataMember]
		public int? StatusCode { get; set; }
		[DataMember]
		public string StatusDescription { get; set; }
		[DataMember]
		public long IntegrationLogId { get; set; }
	}
}
