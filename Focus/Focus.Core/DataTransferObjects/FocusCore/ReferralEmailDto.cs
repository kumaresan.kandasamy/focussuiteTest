﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "ReferralEmail", Namespace = Constants.DataContractNamespace)]
  public class ReferralEmailDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public string EmailText { get; set; }
    [DataMember]
    public DateTime EmailSentOn { get; set; }
    [DataMember]
    public long EmailSentBy { get; set; }
    [DataMember]
    public ApprovalStatuses? ApprovalStatus { get; set; }
    [DataMember]
    public long PersonId { get; set; }
    [DataMember]
    public long? EmployeeId { get; set; }
    [DataMember]
    public long? ApplicationId { get; set; }
  }
}
