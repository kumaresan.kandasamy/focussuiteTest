﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "UploadRecord", Namespace = Constants.DataContractNamespace)]
  public class UploadRecordDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public int RecordNumber { get; set; }
    [DataMember]
    public ValidationRecordStatus Status { get; set; }
    [DataMember]
    public string ValidationMessage { get; set; }
    [DataMember]
    public string FieldValidation { get; set; }
    [DataMember]
    public string FieldData { get; set; }
    [DataMember]
    public long UploadFileId { get; set; }
  }
}
