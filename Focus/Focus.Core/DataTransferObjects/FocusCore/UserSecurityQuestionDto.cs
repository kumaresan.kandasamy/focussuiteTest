﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract( Name = "UserSecurityQuestion", Namespace = Constants.DataContractNamespace )]
	public class UserSecurityQuestionDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public long UserId { get; set; }
		[DataMember]
		public int QuestionIndex { get; set; }
		[DataMember]
		public string SecurityQuestion { get; set; }
		[DataMember]
		public string SecurityAnswerHash { get; set; }
		[DataMember]
		public string SecurityAnswerEncrypted { get; set; }
		[DataMember]
		public long? SecurityQuestionId { get; set; }
	}
}