﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract(Name = "Resume", Namespace = Constants.DataContractNamespace)]
	public class ResumeDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public DateTime CreatedOn { get; set; }
		[DataMember]
		public DateTime UpdatedOn { get; set; }
		[DataMember]
		public string PrimaryOnet { get; set; }
		[DataMember]
		public string PrimaryROnet { get; set; }
		[DataMember]
		public string ResumeSkills { get; set; }
		[DataMember]
		public string FirstName { get; set; }
		[DataMember]
		public string LastName { get; set; }
		[DataMember]
		public string MiddleName { get; set; }
		[DataMember]
		public DateTime? DateOfBirth { get; set; }
		[DataMember]
		public Genders? Gender { get; set; }
		[DataMember]
		public string AddressLine1 { get; set; }
		[DataMember]
		public string TownCity { get; set; }
		[DataMember]
		public long? StateId { get; set; }
		[DataMember]
		public long? CountryId { get; set; }
		[DataMember]
		public string PostcodeZip { get; set; }
		[DataMember]
		public string WorkPhone { get; set; }
		[DataMember]
		public string HomePhone { get; set; }
		[DataMember]
		public string MobilePhone { get; set; }
		[DataMember]
		public string FaxNumber { get; set; }
		[DataMember]
		public string EmailAddress { get; set; }
		[DataMember]
		public EducationLevel? HighestEducationLevel { get; set; }
		[DataMember]
		public int? YearsExperience { get; set; }
		[DataMember]
		public bool? IsVeteran { get; set; }
		[DataMember]
		public bool? IsActive { get; set; }
		[DataMember]
		public ResumeStatuses? StatusId { get; set; }
		[DataMember]
		public string ResumeXml { get; set; }
		[DataMember]
		public ResumeCompletionStatuses? ResumeCompletionStatusId { get; set; }
		[DataMember]
		public bool IsDefault { get; set; }
		[DataMember]
		public string ResumeName { get; set; }
		[DataMember]
		public bool IsSearchable { get; set; }
		[DataMember]
		public bool? SkillsAlreadyCaptured { get; set; }
		[DataMember]
		public long? CountyId { get; set; }
        [DataMember]
        public bool HomelessNoShelter { get; set; }
        [DataMember]
        public bool HomelessWithShelter { get; set; }
        [DataMember]
        public bool RunawayYouth { get; set; }
        [DataMember]
        public bool ExOffender { get; set; }
		[DataMember]
		public int? SkillCount { get; set; }
		[DataMember]
		public int? WordCount { get; set; }
		[DataMember]
		public bool VeteranPriorityServiceAlertsSubscription { get; set; }
		[DataMember]
		public DateTime? EducationCompletionDate { get; set; }
		[DataMember]
		public bool? ResetSearchable { get; set; }
		[DataMember]
		public bool IsContactInfoVisible { get; set; }
		[DataMember]
		public string ExternalId { get; set; }
		[DataMember]
		public long? NcrcLevelId { get; set; }
		[DataMember]
		public long? SuffixId { get; set; }
		[DataMember]
		public bool? MigrantSeasonalFarmWorker { get; set; }
		[DataMember]
		public long PersonId { get; set; }
		[DataMember]
		public bool? IsUSCitizen { get; set; }
		[DataMember]
		public bool? IsPermanentResident { get; set; }
		[DataMember]
		public string AlienRegistrationNumber { get; set; }
		[DataMember]
		public DateTime? AlienRegExpiryDate { get; set; }
        [DataMember]
        public bool DisplacedHomemaker { get; set; }
        [DataMember]
        public bool SingleParent { get; set; }
        [DataMember]
        public bool LowIncomeStatus { get; set; }
        [DataMember]
        public bool LowLeveLiteracy { get; set; }
        [DataMember]
        public bool CulturalBarriers { get; set; }
        [DataMember]
        public string NativeLanguage { get; set; }
        [DataMember]
        public string CommonLanguage { get; set; }
        [DataMember]
        public string PreferredLanguage { get; set; }
        [DataMember]
        public string NoOfDependents { get; set; }
        [DataMember]
        public string EstMonthlyIncome { get; set; }
	}
}
