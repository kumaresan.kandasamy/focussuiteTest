﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name="JobView", Namespace = Constants.DataContractNamespace)]
	public class JobViewDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public DateTime CreatedOn { get; set; }
		[DataMember]
		public DateTime UpdatedOn { get; set; }
		[DataMember]
		public long EmployerId { get; set; }
		[DataMember]
		public string JobTitle { get; set; }
		[DataMember]
		public JobStatuses JobStatus { get; set; }
		[DataMember]
		public DateTime? PostedOn { get; set; }
		[DataMember]
		public DateTime? ClosingOn { get; set; }
		[DataMember]
		public DateTime? HeldOn { get; set; }
		[DataMember]
		public DateTime? ClosedOn { get; set; }
		[DataMember]
		public string BusinessUnitName { get; set; }
		[DataMember]
		public long? EmployeeId { get; set; }
		[DataMember]
		public ApprovalStatuses ApprovalStatus { get; set; }
		[DataMember]
		public string EmployerName { get; set; }
		[DataMember]
		public int ReferralCount { get; set; }
		[DataMember]
		public bool? FederalContractor { get; set; }
		[DataMember]
		public bool? ForeignLabourCertificationH2A { get; set; }
		[DataMember]
		public bool? ForeignLabourCertificationH2B { get; set; }
		[DataMember]
		public bool? ForeignLabourCertificationOther { get; set; }
		[DataMember]
		public bool? CourtOrderedAffirmativeAction { get; set; }
		[DataMember]
		public string UserName { get; set; }
		[DataMember]
		public string YellowProfanityWords { get; set; }
		[DataMember]
		public long? BusinessUnitId { get; set; }
		[DataMember]
		public bool? IsConfidential { get; set; }
		[DataMember]
		public string HiringManagerFirstName { get; set; }
		[DataMember]
		public string HiringManagerLastName { get; set; }
		[DataMember]
		public long CreatedBy { get; set; }
		[DataMember]
		public long UpdatedBy { get; set; }
		[DataMember]
		public JobTypes JobType { get; set; }
		[DataMember]
		public long? AssignedToId { get; set; }
		[DataMember]
		public int? NumberOfOpenings { get; set; }
		[DataMember]
		public DateTime? VeteranPriorityEndDate { get; set; }
    [DataMember]
    public ExtendVeteranPriorityOfServiceTypes? ExtendVeteranPriority { get; set; }
		[DataMember]
		public string Location { get; set; }
		[DataMember]
		public bool? HasCheckedCriminalRecordExclusion { get; set; }
		[DataMember]
		public string RedProfanityWords { get; set; }
		[DataMember]
		public long? ClosedBy { get; set; }
		[DataMember]
		public DateTime? AwaitingApprovalOn { get; set; }
		[DataMember]
		public long? PostingId { get; set; }
        [DataMember]
        public bool? SuitableForHomeWorker { get; set; }
        [DataMember]
        public int? MinimumAgeReasonValue { get; set;}
        [DataMember]
        public JobLocationTypes? JobLocationType { get; set; }
        [DataMember]
        public bool? CreditCheckRequired { get; set; }
        [DataMember]
        public bool? IsCommissionBased { get; set; }
        [DataMember]
        public bool? IsSalaryAndCommissionBased { get; set; }
        [DataMember]
        public string JobSpecialRequirement { get; set; }
	}
}
