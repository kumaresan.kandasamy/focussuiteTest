﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "Skill", Namespace = Constants.DataContractNamespace)]
  public class SkillDto
  {
    [DataMember]
    public int? Id { get; set; }
    [DataMember]
    public string Name { get; set; }
    [DataMember]
    public SkillType Type { get; set; }
    [DataMember]
    public SkillCategories Category { get; set; }
    [DataMember]
    public string Stem { get; set; }
    [DataMember]
    public int NoiseSkill { get; set; }
    [DataMember]
    public int Cluster { get; set; }
  }
}