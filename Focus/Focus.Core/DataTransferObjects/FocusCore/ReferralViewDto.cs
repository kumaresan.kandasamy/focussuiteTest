﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "ReferralView", Namespace = Constants.DataContractNamespace)]
	public class ReferralViewDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public string LensPostingId { get; set; }
		[DataMember]
		public DateTime ApplicationDate { get; set; }
		[DataMember]
		public string JobTitle { get; set; }
		[DataMember]
		public string EmployerName { get; set; }
		[DataMember]
		public bool? CandidateApplicationAutomaticallyApproved { get; set; }
		[DataMember]
		public string EmployeeName { get; set; }
		[DataMember]
		public long CandidateId { get; set; }
		[DataMember]
		public int CandidateApplicationScore { get; set; }
		[DataMember]
		public ApprovalStatuses ApplicationApprovalStatus { get; set; }
		[DataMember]
		public string EmployeePhoneNumber { get; set; }
		[DataMember]
		public string EmployeeEmail { get; set; }
		[DataMember]
		public string Location { get; set; }
		[DataMember]
		public DateTime PostingCreatedOn { get; set; }
		[DataMember]
		public DateTime? JobClosingOn { get; set; }
		[DataMember]
		public long? JobId { get; set; }
		[DataMember]
		public DateTime? VeteranPriorityEndDate { get; set; }
		[DataMember]
		public bool? IsVeteran { get; set; }
		[DataMember]
		public ApprovalStatuses? PreviousApprovalStatus { get; set; }
		[DataMember]
		public string PostingEmployerName { get; set; }
		[DataMember]
		public bool? IsConfidential { get; set; }
		[DataMember]
		public ContactMethods? InterviewContactPreferences { get; set; }
		[DataMember]
		public string InterviewMailAddress { get; set; }
		[DataMember]
		public string InterviewEmailAddress { get; set; }
		[DataMember]
		public string InterviewFaxNumber { get; set; }
		[DataMember]
		public string InterviewDirectApplicationDetails { get; set; }
		[DataMember]
		public string InterviewApplicationUrl { get; set; }
		[DataMember]
		public string InterviewPhoneNumber { get; set; }
		[DataMember]
		public string InterviewOtherInstructions { get; set; }
	}
}
