﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract(Name = "User", Namespace = Constants.DataContractNamespace)]
	public class UserDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public DateTime CreatedOn { get; set; }
		[DataMember]
		public DateTime UpdatedOn { get; set; }
		[DataMember]
		public DateTime? DeletedOn { get; set; }
		[DataMember]
		public string UserName { get; set; }
		[DataMember]
		public string PasswordHash { get; set; }
		[DataMember]
		public string PasswordSalt { get; set; }
		[DataMember]
		public bool Enabled { get; set; }
		[DataMember]
		public string ValidationKey { get; set; }
		[DataMember]
		public UserTypes? UserType { get; set; }
		[DataMember]
		public DateTime? LoggedInOn { get; set; }
		[DataMember]
		public DateTime? LastLoggedInOn { get; set; }
		[DataMember]
		public string ExternalId { get; set; }
		[DataMember]
		public bool IsClientAuthenticated { get; set; }
		[DataMember]
		public string ScreenName { get; set; }
		[DataMember]
		public bool IsMigrated { get; set; }
		[DataMember]
		public bool RegulationsConsent { get; set; }
		[DataMember]
		public string Pin { get; set; }
		[DataMember]
		public DateTime? FirstLoggedInOn { get; set; }
		[DataMember]
		public bool Blocked { get; set; }
		[DataMember]
		public DateTime? ValidationKeyExpiry { get; set; }
		[DataMember]
		public AccountDisabledReason? AccountDisabledReason { get; set; }
		[DataMember]
		public string MigratedPasswordHash { get; set; }
		[DataMember]
		public string MigrationId { get; set; }
		[DataMember]
		public int PasswordAttempts { get; set; }
		[DataMember]
		public BlockedReason? BlockedReason { get; set; }
		[DataMember]
		public long PersonId { get; set; }
	}
}
