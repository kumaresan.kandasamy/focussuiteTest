﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "Employer", Namespace = Constants.DataContractNamespace)]
  public class EmployerDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public DateTime CreatedOn { get; set; }
    [DataMember]
    public DateTime UpdatedOn { get; set; }
    [DataMember]
    public string Name { get; set; }
    [DataMember]
    public DateTime? CommencedOn { get; set; }
    [DataMember]
    public string FederalEmployerIdentificationNumber { get; set; }
    [DataMember]
    public bool IsValidFederalEmployerIdentificationNumber { get; set; }
    [DataMember]
    public string StateEmployerIdentificationNumber { get; set; }
    [DataMember]
    public ApprovalStatuses ApprovalStatus { get; set; }
    [DataMember]
    public string Url { get; set; }
    [DataMember]
    public DateTime? ExpiredOn { get; set; }
    [DataMember]
    public long OwnershipTypeId { get; set; }
    [DataMember]
    public string IndustrialClassification { get; set; }
    [DataMember]
    public bool TermsAccepted { get; set; }
    [DataMember]
    public string PrimaryPhone { get; set; }
    [DataMember]
    public string PrimaryPhoneExtension { get; set; }
    [DataMember]
    public string PrimaryPhoneType { get; set; }
    [DataMember]
    public string AlternatePhone1 { get; set; }
    [DataMember]
    public string AlternatePhone1Type { get; set; }
    [DataMember]
    public string AlternatePhone2 { get; set; }
    [DataMember]
    public string AlternatePhone2Type { get; set; }
    [DataMember]
    public bool IsRegistrationComplete { get; set; }
    [DataMember]
    public string ExternalId { get; set; }
    [DataMember]
    public bool Archived { get; set; }
    [DataMember]
    public long? AccountTypeId { get; set; }
    [DataMember]
    public long? NoOfEmployees { get; set; }
    [DataMember]
    public string LegalName { get; set; }
    [DataMember]
    public long? AssignedToId { get; set; }
  }
}