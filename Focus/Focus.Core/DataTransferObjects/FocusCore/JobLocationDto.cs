﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "JobLocation", Namespace = Constants.DataContractNamespace)]
	public class JobLocationDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public DateTime CreatedOn { get; set; }
		[DataMember]
		public string Location { get; set; }
		[DataMember]
		public bool IsPublicTransitAccessible { get; set; }
		[DataMember]
		public string AddressLine1 { get; set; }
		[DataMember]
		public string City { get; set; }
		[DataMember]
		public string State { get; set; }
		[DataMember]
		public string Zip { get; set; }
		[DataMember]
		public string County { get; set; }
		[DataMember]
		public bool? NewLocation { get; set; }
		[DataMember]
		public long JobId { get; set; }
	}
}