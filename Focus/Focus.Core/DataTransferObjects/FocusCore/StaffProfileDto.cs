﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "StaffProfile", Namespace = Constants.DataContractNamespace)]
  public class StaffProfileDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public string FirstName { get; set; }
    [DataMember]
    public string LastName { get; set; }
    [DataMember]
    public bool Enabled { get; set; }
    [DataMember]
    public string PhoneNumber { get; set; }
    [DataMember]
    public string FaxNumber { get; set; }
    [DataMember]
    public string TownCity { get; set; }
    [DataMember]
    public long? StateId { get; set; }
    [DataMember]
    public string EmailAddress { get; set; }
    [DataMember]
		public string ExternalOffice { get; set; }
  }
}