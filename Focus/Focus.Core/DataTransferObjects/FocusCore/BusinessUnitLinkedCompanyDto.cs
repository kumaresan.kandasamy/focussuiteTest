﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract(Name = "BusinessUnitLinkedCompany", Namespace = Constants.DataContractNamespace)]
	public class BusinessUnitLinkedCompanyDto
	{
		[DataMember]
		public BusinessUnitDto BusinessUnit { get; set; }
		[DataMember]
		public BusinessUnitAddressDto BusinessUnitAddress { get; set; }
		[DataMember]
		public List<HiringManagerSummaryDto> HiringManagers { get; set; }
	}
}
