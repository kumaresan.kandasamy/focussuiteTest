﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract(Name = "Issues", Namespace = Constants.DataContractNamespace)]
	public class IssuesDto
	{
		[DataMember]
		public long? Id { get; set; }

		[DataMember]
		public long PersonId { get; set; }

		[DataMember]
		public bool NoLoginTriggered { get; set; }

		[DataMember]
		public DateTime? NoLoginResolvedDate { get; set; }

		[DataMember]
		public int JobOfferRejectionCount { get; set; }

		[DataMember]
		public DateTime? JobOfferRejectionResolvedDate { get; set; }

		[DataMember]
		public bool JobOfferRejectionTriggered { get; set; }

		[DataMember]
		public int NotReportingToInterviewCount { get; set; }

		[DataMember]
		public DateTime? NotReportingToInterviewResolvedDate { get; set; }

		[DataMember]
		public bool NotReportingToInterviewTriggered { get; set; }

		[DataMember]
		public int NotClickingOnLeadsCount { get; set; }

		[DataMember]
		public DateTime? NotClickingOnLeadsResolvedDate { get; set; }

		[DataMember]
		public bool NotClickingOnLeadsTriggered { get; set; }

		[DataMember]
		public int NotRespondingToEmployerInvitesCount { get; set; }

		[DataMember]
		public DateTime? NotRespondingToEmployerInvitesResolvedDate { get; set; }

		[DataMember]
		public bool NotRespondingToEmployerInvitesTriggered { get; set; }

		[DataMember]
		public int ShowingLowQualityMatchesCount { get; set; }

		[DataMember]
		public DateTime? ShowingLowQualityMatchesResolvedDate { get; set; }

		[DataMember]
		public bool ShowingLowQualityMatchesTriggered { get; set; }

		[DataMember]
		public int PostingLowQualityResumeCount { get; set; }

		[DataMember]
		public DateTime? PostingLowQualityResumeResolvedDate { get; set; }

		[DataMember]
		public bool PostingLowQualityResumeTriggered { get; set; }

		[DataMember]
		public bool FollowUpRequested { get; set; }

		[DataMember]
		public bool PostHireFollowUpTriggered { get; set; }

		[DataMember]
		public DateTime? PostHireFollowUpResolvedDate { get; set; }

		[DataMember]
		public bool NotSearchingJobsTriggered { get; set; }

		[DataMember]
		public DateTime? NotSearchingJobsResolvedDate { get; set; }

		[DataMember]
		public DateTime? LastLoggedInDateForAlert { get; set; }

		[DataMember]
		public bool? InappropriateEmailAddress { get; set; }

		[DataMember]
		public bool? GivingPositiveFeedback { get; set; }

		[DataMember]
		public bool? GivingNegativeFeedback { get; set; }

		[DataMember]
		public bool MigrantSeasonalFarmWorkerTriggered { get; set; }

		[DataMember]
		public bool? HasExpiredAlienCertificationRegistration { get; set; }
	}
}