﻿using System;
using System.Runtime.Serialization;

namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract(Name = "UserActionTypeActivity", Namespace = Constants.DataContractNamespace)]
	public class UserActionTypeActivityDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public DateTime? DeletedOn { get; set; }
		[DataMember]
		public long UserId { get; set; }
		[DataMember]
		public DateTime LastActivityDate { get; set; }
		[DataMember]
		public string ExternalActivityId { get; set; }
		[DataMember]
		public long? ActionerId { get; set; }
	}
}