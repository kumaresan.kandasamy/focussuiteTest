﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "Bookmark", Namespace = Constants.DataContractNamespace)]
  public class BookmarkDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public string Name { get; set; }
    [DataMember]
    public BookmarkTypes Type { get; set; }
    [DataMember]
    public string Criteria { get; set; }
    [DataMember]
    public long? UserId { get; set; }
    [DataMember]
    public UserTypes UserType { get; set; }
    [DataMember]
    public long? EntityId { get; set; }
  }
}