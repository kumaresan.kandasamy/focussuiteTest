﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "JobIssuesView", Namespace = Constants.DataContractNamespace)]
  public class JobIssuesViewDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public long JobId { get; set; }
    [DataMember]
    public bool LowQualityMatches { get; set; }
    [DataMember]
    public bool LowReferralActivity { get; set; }
    [DataMember]
    public bool NotViewingReferrals { get; set; }
    [DataMember]
    public bool SearchingButNotInviting { get; set; }
    [DataMember]
    public bool ClosingDateRefreshed { get; set; }
    [DataMember]
    public bool EarlyJobClosing { get; set; }
    [DataMember]
    public bool NegativeSurveyResponse { get; set; }
    [DataMember]
    public bool PositiveSurveyResponse { get; set; }
    [DataMember]
    public bool FollowUpRequested { get; set; }
    [DataMember]
    public int PostHireFollowUpCount { get; set; }
  }
}
