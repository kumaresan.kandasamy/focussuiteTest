﻿using System;
using System.Runtime.Serialization;

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "BusinessUnitPlacementsView", Namespace = Constants.DataContractNamespace)]
  public class BusinessUnitPlacementsViewDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public long BuId { get; set; }
    [DataMember]
    public long JobSeekerId { get; set; }
    [DataMember]
    public string BuName { get; set; }
    [DataMember]
    public string JobTitle { get; set; }
    [DataMember]
    public int ApplicationStatus { get; set; }
    [DataMember]
    public string FirstName { get; set; }
    [DataMember]
    public string LastName { get; set; }
  }
}