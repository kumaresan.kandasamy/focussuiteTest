﻿#region Copyright © 2000 - 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "ResumeTemplate", Namespace = Constants.DataContractNamespace)]
  public class ResumeTemplateDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public DateTime CreatedOn { get; set; }
    [DataMember]
    public DateTime UpdatedOn { get; set; }
    [DataMember]
    public string ResumeXml { get; set; }
    [DataMember]
    public long PersonId { get; set; }
  }
}
