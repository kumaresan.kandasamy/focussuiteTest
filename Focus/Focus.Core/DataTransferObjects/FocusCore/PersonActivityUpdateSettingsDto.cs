﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives
using System;
using System.Runtime.Serialization; 
#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
    [Serializable]
    [DataContract(Name = "PersonActivityUpdateSettings", Namespace = Constants.DataContractNamespace)]
    public class PersonActivityUpdateSettingsDto
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long PersonId { get; set; }



        [DataMember]
        public long? BackdateMyEntriesJSActionMenuMaxDays { get; set; }

        [DataMember]
        public long? BackdateAnyEntryJSActivityLogMaxDays { get; set; }

        [DataMember]
        public long? BackdateMyEntriesJSActivityLogMaxDays { get; set; }

        [DataMember]
        public long? DeleteAnyEntryJSActivityLogMaxDays { get; set; }

        [DataMember]
        public long? DeleteMyEntriesJSActivityLogMaxDays { get; set; }



        [DataMember]
        public long? BackdateMyEntriesHMActionMenuMaxDays { get; set; }

        [DataMember]
        public long? BackdateAnyEntryHMActivityLogMaxDays { get; set; }

        [DataMember]
        public long? BackdateMyEntriesHMActivityLogMaxDays { get; set; }

        [DataMember]
        public long? DeleteAnyEntryHMActivityLogMaxDays { get; set; }

        [DataMember]
        public long? DeleteMyEntriesHMActivityLogMaxDays { get; set; }
    }
}
