﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion
namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract(Name = "JobSeekerSurveyTotalsView", Namespace = Constants.DataContractNamespace)]
	public class JobSeekerSurveyTotalsViewDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public int VerySatisfied { get; set; }
		[DataMember]
		public int SomewhatSatisfied { get; set; }
		[DataMember]
		public int SomewhatDissatisfied { get; set; }
		[DataMember]
		public int Dissatisfied { get; set; }
		[DataMember]
		public int UnanticipatedMatches { get; set; }
		[DataMember]
		public int NoUnanticipatedMatches { get; set; }
		[DataMember]
		public int WasInvitedDidApply { get; set; }
		[DataMember]
		public int WasNotInvitedOrDidNotApply { get; set; }
	}
}
