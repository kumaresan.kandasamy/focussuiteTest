﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract(Name = "LaborInsightMappingView", Namespace = Constants.DataContractNamespace)]
	public class LaborInsightMappingViewDto
	{
		[DataMember]
		public Guid? Id { get; set; }
		[DataMember]
		public long CodeGroupItemId { get; set; }
		[DataMember]
		public string CodeGroupKey { get; set; }
		[DataMember]
		public string LaborInsightId { get; set; }
		[DataMember]
		public string LaborInsightValue { get; set; }
	}
}
