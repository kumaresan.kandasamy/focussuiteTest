﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "Person", Namespace = Constants.DataContractNamespace)]
  public class PersonDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public long TitleId { get; set; }
    [DataMember]
    public string FirstName { get; set; }
    [DataMember]
    public string MiddleInitial { get; set; }
    [DataMember]
    public string LastName { get; set; }
    [DataMember]
    public short? Age { get; set; }
    [DataMember]
    public DateTime? DateOfBirth { get; set; }
    [DataMember]
    public string SocialSecurityNumber { get; set; }
    [DataMember]
    public string JobTitle { get; set; }
    [DataMember]
    public string EmailAddress { get; set; }
    [DataMember]
    public SchoolStatus? EnrollmentStatus { get; set; }
    [DataMember]
    public long? ProgramAreaId { get; set; }
    [DataMember]
    public long? DegreeId { get; set; }
    [DataMember]
    public bool? Manager { get; set; }
    [DataMember]
    public string ExternalOffice { get; set; }
    [DataMember]
    public DateTime? LastSurveyedOn { get; set; }
    [DataMember]
    public bool? IsVeteran { get; set; }
    [DataMember]
    public bool? IsUiClaimant { get; set; }
    [DataMember]
    public bool? Unsubscribed { get; set; }
    [DataMember]
    public long? SuffixId { get; set; }
    [DataMember]
    public bool? MigrantSeasonalFarmWorkerVerified { get; set; }
    [DataMember]
    public long? AssignedToId { get; set; }
    [DataMember]
    public long? CampusId { get; set; }
    [DataMember]
    public CareerAccountType? AccountType { get; set; }
    [DataMember]
    public bool? LocalVeteranEmploymentRepresentative { get; set; }
    [DataMember]
    public bool? DisabledVeteransOutreachProgramSpecialist { get; set; }
  }
}