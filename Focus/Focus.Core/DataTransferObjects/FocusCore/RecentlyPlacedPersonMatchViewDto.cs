﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion



namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract(Name = "RecentlyPlacedPersonMatchView", Namespace = Constants.DataContractNamespace)]
	public class RecentlyPlacedPersonMatchViewDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public long MatchedPersonId { get; set; }
		[DataMember]
		public string MatchedCandidateName { get; set; }
		[DataMember]
		public long RecentlyPlacedId { get; set; }
		[DataMember]
		public long PlacedPersonId { get; set; }
		[DataMember]
		public string PlacedCandidateName { get; set; }
		[DataMember]
		public int Score { get; set; }
		[DataMember]
		public long BusinessUnitId { get; set; }
		[DataMember]
		public string BusinessUnitName { get; set; }
		[DataMember]
		public long EmployerId { get; set; }
		[DataMember]
		public string EmployerName { get; set; }
		[DataMember]
		public DateTime PlacementDate { get; set; }
		[DataMember]
		public long JobId { get; set; }
		[DataMember]
		public string JobTitle { get; set; }
	}
}
