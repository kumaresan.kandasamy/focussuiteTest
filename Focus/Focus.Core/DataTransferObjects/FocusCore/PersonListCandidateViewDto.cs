﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "PersonListCandidateView", Namespace = Constants.DataContractNamespace)]
  public class PersonListCandidateViewDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public ListTypes ListType { get; set; }
    [DataMember]
    public long PersonId { get; set; }
    [DataMember]
    public long CandidateId { get; set; }
    [DataMember]
    public string CandidateFirstName { get; set; }
    [DataMember]
    public string CandidateLastName { get; set; }
    [DataMember]
    public bool CandidateIsVeteran { get; set; }
    [DataMember]
    public long CandidatePersonId { get; set; }
		[DataMember]
		public string CandidateTownCity { get; set; }
		[DataMember]
		public long? CandidateStateId { get; set; }
		[DataMember]
		public bool CandidateIsContactInfoVisible { get; set; }
  }
}