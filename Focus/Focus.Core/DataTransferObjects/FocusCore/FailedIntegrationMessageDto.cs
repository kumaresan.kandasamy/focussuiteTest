﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "FailedIntegrationMessage", Namespace = Constants.DataContractNamespace)]
  public class FailedIntegrationMessageDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public DateTime DateSubmitted { get; set; }
    [DataMember]
    public string MessageType { get; set; }
    [DataMember]
    public string ErrorDescription { get; set; }
    [DataMember]
    public string Request { get; set; }
    [DataMember]
    public bool IsIgnored { get; set; }
		[DataMember]
		public DateTime? DateResent { get; set; }
    [DataMember]
    public IntegrationPoint? IntegrationPoint { get; set; }
	}
}
