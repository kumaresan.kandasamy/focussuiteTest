﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "EmployeeSearchView", Namespace = Constants.DataContractNamespace)]
  public class EmployeeSearchViewDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public string FirstName { get; set; }
    [DataMember]
    public string LastName { get; set; }
    [DataMember]
    public long EmployerId { get; set; }
    [DataMember]
    public string EmployerName { get; set; }
    [DataMember]
    public string IndustrialClassification { get; set; }
    [DataMember]
    public string FederalEmployerIdentificationNumber { get; set; }
    [DataMember]
    public WorkOpportunitiesTaxCreditCategories? WorkOpportunitiesTaxCreditHires { get; set; }
    [DataMember]
    public JobPostingFlags? PostingFlags { get; set; }
    [DataMember]
    public ScreeningPreferences? ScreeningPreferences { get; set; }
    [DataMember]
    public string JobTitle { get; set; }
    [DataMember]
    public DateTime EmployerCreatedOn { get; set; }
    [DataMember]
    public string EmailAddress { get; set; }
    [DataMember]
    public string PhoneNumber { get; set; }
    [DataMember]
    public bool AccountEnabled { get; set; }
    [DataMember]
    public ApprovalStatuses EmployerApprovalStatus { get; set; }
    [DataMember]
    public ApprovalStatuses EmployeeApprovalStatus { get; set; }
    [DataMember]
    public string TownCity { get; set; }
    [DataMember]
    public string State { get; set; }
    [DataMember]
    public string BusinessUnitName { get; set; }
    [DataMember]
    public long BusinessUnitId { get; set; }
    [DataMember]
    public DateTime? LastLogin { get; set; }
    [DataMember]
    public long EmployeeBusinessUnitId { get; set; }
    [DataMember]
    public long StateId { get; set; }
    [DataMember]
    public bool AccountBlocked { get; set; }
    [DataMember]
    public long EmployeeId { get; set; }
    [DataMember]
    public string Extension { get; set; }
    [DataMember]
    public bool EmployerIsPreferred { get; set; }
    [DataMember]
    public bool BusinessUnitIsPrimary { get; set; }
    [DataMember]
    public ApprovalStatusReason EmployeeApprovalStatusReason { get; set; }
    [DataMember]
    public ApprovalStatuses BusinessUnitApprovalStatus { get; set; }
    [DataMember]
    public ApprovalStatusReason BusinessUnitApprovalStatusReason { get; set; }
    [DataMember]
    public bool? Unsubscribed { get; set; }
    [DataMember]
    public BlockedReason? AccountBlockedReason { get; set; }
  }
}