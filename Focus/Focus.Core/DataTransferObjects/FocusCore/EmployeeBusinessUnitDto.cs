﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "EmployeeBusinessUnit", Namespace = Constants.DataContractNamespace)]
  public class EmployeeBusinessUnitDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public bool Default { get; set; }
    [DataMember]
    public long BusinessUnitId { get; set; }
    [DataMember]
    public long EmployeeId { get; set; }
  }
}