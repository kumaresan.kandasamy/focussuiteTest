﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract( Name = "CandidateBasicView", Namespace = Constants.DataContractNamespace )]
	public class CandidateBasicViewDto
	{
		[DataMember]
		public long Id { get; set; }
		[DataMember]
		public string FirstName { get; set; }
		[DataMember]
		public string LastName { get; set; }
		[DataMember]
		public long UserId { get; set; }
		[DataMember]
		public bool Enabled { get; set; }
		[DataMember]
		public DateTime? LastLoggedInOn { get; set; }
		[DataMember]
		public DateTime CreatedOn { get; set; }
		[DataMember]
		public string ClientId { get; set; }
		[DataMember]
		public string EmailAddress { get; set; }
	}
}
