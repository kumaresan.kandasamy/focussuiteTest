﻿using System;
using System.Runtime.Serialization;

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "ActivityView", Namespace = Constants.DataContractNamespace)]
  public class ActivityViewDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public string CategoryLocalisationKey { get; set; }
    [DataMember]
    public string CategoryName { get; set; }
    [DataMember]
    public bool CategoryVisible { get; set; }
    [DataMember]
    public bool CategoryAdministrable { get; set; }
    [DataMember]
    public ActivityType ActivityType { get; set; }
    [DataMember]
    public string ActivityLocalisationKey { get; set; }
    [DataMember]
    public string ActivityName { get; set; }
    [DataMember]
    public bool ActivityVisible { get; set; }
    [DataMember]
    public bool ActivityAdministrable { get; set; }
    [DataMember]
    public int SortOrder { get; set; }
    [DataMember]
    public long ActivityCategoryId { get; set; }
    [DataMember]
    public long ActivityExternalId { get; set; }
		[DataMember]
		public bool Editable { get; set; }
  }
}