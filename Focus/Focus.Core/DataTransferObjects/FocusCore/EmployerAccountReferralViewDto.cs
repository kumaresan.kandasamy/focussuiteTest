﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract(Name = "EmployerAccountReferralView", Namespace = Constants.DataContractNamespace)]
	public class EmployerAccountReferralViewDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public long EmployerId { get; set; }
		[DataMember]
		public long EmployeeId { get; set; }
		[DataMember]
		public string EmployeeFirstName { get; set; }
		[DataMember]
		public string EmployeeLastName { get; set; }
		[DataMember]
		public DateTime AccountCreationDate { get; set; }
		[DataMember]
		public string EmployerName { get; set; }
		[DataMember]
		public string EmployerIndustrialClassification { get; set; }
		[DataMember]
		public string EmployerPhoneNumber { get; set; }
		[DataMember]
		public string EmployerFederalEmployerIdentificationNumber { get; set; }
		[DataMember]
		public string EmployerStateEmployerIdentificationNumber { get; set; }
		[DataMember]
		public string EmployerFaxNumber { get; set; }
		[DataMember]
		public string EmployerUrl { get; set; }
		[DataMember]
		public string EmployeeAddressLine1 { get; set; }
		[DataMember]
		public string EmployeeAddressTownCity { get; set; }
		[DataMember]
		public long? EmployeeAddressStateId { get; set; }
		[DataMember]
		public string EmployeeAddressPostcodeZip { get; set; }
		[DataMember]
		public string EmployeeEmailAddress { get; set; }
		[DataMember]
		public string EmployeePhoneNumber { get; set; }
		[DataMember]
		public string EmployeeFaxNumber { get; set; }
		[DataMember]
		public string EmployerAddressLine1 { get; set; }
		[DataMember]
		public string EmployerAddressTownCity { get; set; }
		[DataMember]
		public long EmployerAddressStateId { get; set; }
		[DataMember]
		public string EmployerAddressPostcodeZip { get; set; }
		[DataMember]
		public int EmployeeApprovalStatus { get; set; }
		[DataMember]
		public int EmployerApprovalStatus { get; set; }
		[DataMember]
		public string EmployerAddressLine2 { get; set; }
		[DataMember]
		public long EmployerOwnershipTypeId { get; set; }
		[DataMember]
		public long? EmployerAssignedToId { get; set; }
		[DataMember]
		public string RedProfanityWords { get; set; }
		[DataMember]
		public string YellowProfanityWords { get; set; }
		[DataMember]
		public string BusinessUnitDescription { get; set; }
		[DataMember]
		public string BusinessUnitName { get; set; }
		[DataMember]
		public string EmployeeAddressLine2 { get; set; }
		[DataMember]
		public long BusinessUnitId { get; set; }
		[DataMember]
		public bool EmployerPublicTransitAccessible { get; set; }
		[DataMember]
		public string PrimaryPhone { get; set; }
		[DataMember]
		public string PrimaryPhoneType { get; set; }
		[DataMember]
		public string AlternatePhone1 { get; set; }
		[DataMember]
		public string AlternatePhone1Type { get; set; }
		[DataMember]
		public string AlternatePhone2 { get; set; }
		[DataMember]
		public string AlternatePhone2Type { get; set; }
		[DataMember]
		public long? ApprovalStatusChangedBy { get; set; }
		[DataMember]
		public DateTime? ApprovalStatusChangedOn { get; set; }
		[DataMember]
		public int TypeOfApproval { get; set; }
		[DataMember]
		public int BusinessUnitApprovalStatus { get; set; }
		[DataMember]
		public bool EmployerIsPrimary { get; set; }
		[DataMember]
		public string BusinessUnitLegalName { get; set; }
		[DataMember]
		public string BusinessUnitRedProfanityWords { get; set; }
		[DataMember]
		public string BusinessUnitYellowProfanityWords { get; set; }
		[DataMember]
		public long? SuffixId { get; set; }
		[DataMember]
		public string Suffix { get; set; }
		[DataMember]
		public string AccountLockVersion { get; set; }
		[DataMember]
		public long? EmployerAccountTypeId { get; set; }
		[DataMember]
		public DateTime AwaitingApprovalDate { get; set; }
	}
}