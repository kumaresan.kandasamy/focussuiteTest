﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "JobSeekerActivityActionView", Namespace = Constants.DataContractNamespace)]
  public class JobSeekerActivityActionViewDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public string UserName { get; set; }
    [DataMember]
    public DateTime ActionedOn { get; set; }
    [DataMember]
    public string ActionType { get; set; }
    [DataMember]
    public string JobTitle { get; set; }
    [DataMember]
    public string BusinessUnitName { get; set; }
    [DataMember]
    public long? JobSeekerId { get; set; }
    [DataMember]
    public long UserId { get; set; }
    [DataMember]
    public long? EntityIdAdditional01 { get; set; }
    [DataMember]
    public string AdditionalDetails { get; set; }
		[DataMember]
		public long? EntityIdAdditional02 { get; set; }
		[DataMember]
		public long? JobId { get; set; }
	}
}