﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "Application", Namespace = Constants.DataContractNamespace)]
	public class ApplicationDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public int LockVersion { get; set; }
		[DataMember]
		public DateTime CreatedOn { get; set; }
		[DataMember]
		public DateTime UpdatedOn { get; set; }
		[DataMember]
		public ApprovalStatuses ApprovalStatus { get; set; }
		[DataMember]
		public string ApprovalRequiredReason { get; set; }
		[DataMember]
		public ApplicationStatusTypes ApplicationStatus { get; set; }
		[DataMember]
		public int ApplicationScore { get; set; }
		[DataMember]
		public DateTime? StatusLastChangedOn { get; set; }
		[DataMember]
		public bool Viewed { get; set; }
		[DataMember]
		public PostHireFollowUpStatus? PostHireFollowUpStatus { get; set; }
		[DataMember]
		public bool? AutomaticallyApproved { get; set; }
		[DataMember]
		public long? StatusLastChangedBy { get; set; }
		[DataMember]
		public bool AutomaticallyDenied { get; set; }
		[DataMember]
		public ApprovalStatuses? PreviousApprovalStatus { get; set; }
		[DataMember]
		public bool? AutomaticallyOnHold { get; set; }
		[DataMember]
		public long ResumeId { get; set; }
		[DataMember]
		public long PostingId { get; set; }
	}
}