﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion


namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract(Name = "EmployerPlacementsView", Namespace = Constants.DataContractNamespace)]
	public class EmployerPlacementsViewDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public string EmployerName { get; set; }
		[DataMember]
		public string JobTitle { get; set; }
		[DataMember]
		public DateTime? StartDate { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public long EmployerId { get; set; }
		[DataMember]
		public long JobSeekerId { get; set; }
		[DataMember]
		public long BusinessUnitId { get; set; }
	}
}
