﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract(Name = "Document", Namespace = Constants.DataContractNamespace)]
	public class DocumentDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public byte[] File { get; set; }
		[DataMember]
		public string Title { get; set; }
		[DataMember]
		public string Description { get; set; }
		[DataMember]
		public long Category { get; set; }
		[DataMember]
		public long Group { get; set; }
		[DataMember]
		public int Order { get; set; }
		[DataMember]
		public DocumentFocusModules Module { get; set; }
		[DataMember]
		public string FileName { get; set; }
		[DataMember]
		public string MimeType { get; set; }
	}
}
