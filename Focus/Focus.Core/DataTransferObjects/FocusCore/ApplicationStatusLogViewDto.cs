﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "ApplicationStatusLogView", Namespace = Constants.DataContractNamespace)]
	public class ApplicationStatusLogViewDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public DateTime ActionedOn { get; set; }
		[DataMember]
		public int CandidateApplicationScore { get; set; }
		[DataMember]
		public string CandidateFirstName { get; set; }
		[DataMember]
		public string CandidateLastName { get; set; }
		[DataMember]
		public long CandidateApplicationStatus { get; set; }
		[DataMember]
		public long JobId { get; set; }
		[DataMember]
		public int? CandidateYearsExperience { get; set; }
		[DataMember]
		public bool? CandidateIsVeteran { get; set; }
		[DataMember]
		public string LatestJobTitle { get; set; }
		[DataMember]
		public string LatestJobEmployer { get; set; }
		[DataMember]
		public string LatestJobStartYear { get; set; }
		[DataMember]
		public string LatestJobEndYear { get; set; }
		[DataMember]
		public long CandidateApplicationId { get; set; }
		[DataMember]
		public string JobTitle { get; set; }
		[DataMember]
		public long? BusinessUnitId { get; set; }
		[DataMember]
		public long PersonId { get; set; }
		[DataMember]
		public ApprovalStatuses CandidateApprovalStatus { get; set; }
		[DataMember]
		public DateTime? VeteranPriorityEndDate { get; set; }
		[DataMember]
		public long? CandidateOriginalApplicationStatus { get; set; }
		[DataMember]
		public long? ProgramAreaId { get; set; }
		[DataMember]
		public long? DegreeId { get; set; }
		[DataMember]
		public DateTime? EducationCompletionDate { get; set; }
		[DataMember]
		public JobStatuses JobStatus { get; set; }
	}
}