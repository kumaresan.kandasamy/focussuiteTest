﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "NoteReminderView", Namespace = Constants.DataContractNamespace)]
  public class NoteReminderViewDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public DateTime CreatedOn { get; set; }
    [DataMember]
    public NoteReminderTypes NoteReminderType { get; set; }
    [DataMember]
    public long EntityId { get; set; }
    [DataMember]
    public string Text { get; set; }
    [DataMember]
    public string CreatedByFirstname { get; set; }
    [DataMember]
    public string CreatedByLastname { get; set; }
    [DataMember]
    public EntityTypes EntityType { get; set; }
    [DataMember]
    public NoteTypes? NoteType { get; set; }
  }
}