﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "ApplicationView", Namespace = Constants.DataContractNamespace)]
	public class ApplicationViewDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public long JobId { get; set; }
		[DataMember]
		public long EmployerId { get; set; }
		[DataMember]
		public long CandidateId { get; set; }
		[DataMember]
		public ApplicationStatusTypes CandidateApplicationStatus { get; set; }
		[DataMember]
		public int CandidateApplicationScore { get; set; }
		[DataMember]
		public DateTime CandidateApplicationReceivedOn { get; set; }
		[DataMember]
		public ApprovalStatuses CandidateApplicationApprovalStatus { get; set; }
		[DataMember]
		public string CandidateFirstName { get; set; }
		[DataMember]
		public string CandidateLastName { get; set; }
		[DataMember]
		public int? CandidateYearsExperience { get; set; }
		[DataMember]
		public bool? CandidateIsVeteran { get; set; }
		[DataMember]
		public DateTime? ApplicationStatusLastChangedOn { get; set; }
		[DataMember]
		public long? EmployeeId { get; set; }
		[DataMember]
		public long? BusinessUnitId { get; set; }
		[DataMember]
		public bool Viewed { get; set; }
		[DataMember]
		public PostHireFollowUpStatus? PostHireFollowUpStatus { get; set; }
		[DataMember]
		public string BusinuessUnitName { get; set; }
		[DataMember]
		public string EmployeeFirstName { get; set; }
		[DataMember]
		public string EmployeeLastName { get; set; }
		[DataMember]
		public string EmployeeEmail { get; set; }
		[DataMember]
		public DateTime? JobClosingOn { get; set; }
		[DataMember]
		public string JobTitle { get; set; }
		[DataMember]
		public string LensPostingId { get; set; }
		[DataMember]
		public string EmployeePhoneNumber { get; set; }
		[DataMember]
		public DateTime? VeteranPriorityEndDate { get; set; }
		[DataMember]
		public bool? CandidateApplicationAutomaticallyApproved { get; set; }
		[DataMember]
		public bool CandidateIsContactInfoVisible { get; set; }
		[DataMember]
		public string Branding { get; set; }
		[DataMember]
		public long? CandidateNcrcLevelId { get; set; }
		[DataMember]
		public int LockVersion { get; set; }
		[DataMember]
		public int StarRating { get; set; }
	}
}