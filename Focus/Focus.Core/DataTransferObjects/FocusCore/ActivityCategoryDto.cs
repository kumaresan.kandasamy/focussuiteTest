﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "ActivityCategory", Namespace = Constants.DataContractNamespace)]
  public class ActivityCategoryDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public string LocalisationKey { get; set; }
    [DataMember]
    public string Name { get; set; }
    [DataMember]
    public bool EnableDisable { get; set; }
    [DataMember]
    public bool Administrable { get; set; }
    [DataMember]
    public ActivityType ActivityType { get; set; }
  }
}