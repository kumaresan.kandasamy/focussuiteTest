﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "JobPostingReferralView", Namespace = Constants.DataContractNamespace)]
  public class JobPostingReferralViewDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public string JobTitle { get; set; }
    [DataMember]
    public long EmployerId { get; set; }
    [DataMember]
    public string EmployerName { get; set; }
    [DataMember]
    public int? TimeInQueue { get; set; }
    [DataMember]
    public string EmployeeFirstName { get; set; }
    [DataMember]
    public string EmployeeLastName { get; set; }
    [DataMember]
    public long? EmployeeId { get; set; }
    [DataMember]
    public DateTime? AwaitingApprovalOn { get; set; }
    [DataMember]
    public bool? CourtOrderedAffirmativeAction { get; set; }
    [DataMember]
    public bool? FederalContractor { get; set; }
    [DataMember]
    public bool? ForeignLabourCertificationH2A { get; set; }
    [DataMember]
    public bool? ForeignLabourCertificationH2B { get; set; }
    [DataMember]
    public bool? ForeignLabourCertificationOther { get; set; }
    [DataMember]
    public bool? IsCommissionBased { get; set; }
    [DataMember]
    public bool? IsSalaryAndCommissionBased { get; set; }
    [DataMember]
    public long? AssignedToId { get; set; }
    [DataMember]
    public bool? SuitableForHomeWorker { get; set; }
    [DataMember]
    public ApprovalStatuses ApprovalStatus { get; set; }
    [DataMember]
    public JobLocationTypes? JobLocationType { get; set; }
    [DataMember]
    public bool? CreditCheckRequired { get; set; }
    [DataMember]
    public JobStatuses JobStatus { get; set; }
		[DataMember]
		public int LockVersion { get; set; }
		[DataMember]
		public bool? CriminalBackgroundExclusionRequired { get; set; }
  }
}