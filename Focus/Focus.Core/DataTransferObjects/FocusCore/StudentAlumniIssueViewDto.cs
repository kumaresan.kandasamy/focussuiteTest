﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
	[Serializable]
	[DataContract(Name = "StudentAlumniIssueView", Namespace = Constants.DataContractNamespace)]
  public class StudentAlumniIssueViewDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public string FirstName { get; set; }
    [DataMember]
    public string LastName { get; set; }
    [DataMember]
    public DateTime? DateOfBirth { get; set; }
    [DataMember]
    public string SocialSecurityNumber { get; set; }
    [DataMember]
    public string EmailAddress { get; set; }
    [DataMember]
    public DateTime? LastLoggedInOn { get; set; }
    [DataMember]
    public bool? NoLoginTriggered { get; set; }
    [DataMember]
    public bool? JobOfferRejectionTriggered { get; set; }
    [DataMember]
    public bool? NotReportingToInterviewTriggered { get; set; }
    [DataMember]
    public bool? NotClickingOnLeadsTriggered { get; set; }
    [DataMember]
    public bool? NotRespondingToEmployerInvitesTriggered { get; set; }
    [DataMember]
    public bool? ShowingLowQualityMatchesTriggered { get; set; }
    [DataMember]
    public bool? PostingLowQualityResumeTriggered { get; set; }
    [DataMember]
    public bool? PostHireFollowUpTriggered { get; set; }
    [DataMember]
    public bool? FollowUpRequested { get; set; }
    [DataMember]
    public bool? NotSearchingJobsTriggered { get; set; }
    [DataMember]
    public int? EnrollmentStatus { get; set; }
    [DataMember]
    public long? ProgramAreaId { get; set; }
    [DataMember]
    public long? DegreeId { get; set; }
    [DataMember]
    public int? UserType { get; set; }
    [DataMember]
    public string ExternalId { get; set; }
    [DataMember]
    public bool? InappropriateEmailAddress { get; set; }
    [DataMember]
    public long? CampusId { get; set; }
    [DataMember]
    public bool Blocked { get; set; }
    [DataMember]
    public bool? GivingPositiveFeedback { get; set; }
    [DataMember]
    public bool? GivingNegativeFeedback { get; set; }
    [DataMember]
    public string MiddleInitial { get; set; }
		[DataMember]
		public string UserName { get; set; }
  }
}
