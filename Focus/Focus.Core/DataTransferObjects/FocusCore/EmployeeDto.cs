﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "Employee", Namespace = Constants.DataContractNamespace)]
  public class EmployeeDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public ApprovalStatuses ApprovalStatus { get; set; }
    [DataMember]
    public long? StaffUserId { get; set; }
    [DataMember]
    public string RedProfanityWords { get; set; }
    [DataMember]
    public string YellowProfanityWords { get; set; }
    [DataMember]
    public long? ApprovalStatusChangedBy { get; set; }
    [DataMember]
    public DateTime? ApprovalStatusChangedOn { get; set; }
    [DataMember]
    public ApprovalStatusReason ApprovalStatusReason { get; set; }
    [DataMember]
    public DateTime? AwaitingApprovalDate { get; set; }
    [DataMember]
    public long EmployerId { get; set; }
    [DataMember]
    public long PersonId { get; set; }
		
  }
}