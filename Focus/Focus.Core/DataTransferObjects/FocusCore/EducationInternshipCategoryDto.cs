﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusCore
{
  [Serializable]
  [DataContract(Name = "EducationInternshipCategory", Namespace = Constants.DataContractNamespace)]
  public class EducationInternshipCategoryDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public string Name { get; set; }
    [DataMember]
    public long LocalisationId { get; set; }
    [DataMember]
    public EducationSkillCategories CategoryType { get; set; }
    [DataMember]
    public string PastTenseStatement { get; set; }
    [DataMember]
    public string PresentTenseStatement { get; set; }
		[DataMember]
		public List<EducationInternshipSkillDto> CategorySkills { get; set; }
  }
}