﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusExplorer
{
  [Serializable]
  [DataContract(Name = "JobReportView", Namespace = Constants.DataContractNamespace)]
  public class JobReportViewDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public int HiringDemand { get; set; }
    [DataMember]
    public string GrowthTrend { get; set; }
    [DataMember]
    public decimal GrowthPercentile { get; set; }
    [DataMember]
    public string SalaryTrend { get; set; }
    [DataMember]
    public decimal SalaryTrendPercentile { get; set; }
    [DataMember]
    public decimal SalaryTrendAverage { get; set; }
    [DataMember]
    public long JobId { get; set; }
    [DataMember]
    public string Name { get; set; }
    [DataMember]
    public string HiringTrend { get; set; }
    [DataMember]
    public decimal HiringTrendPercentile { get; set; }
    [DataMember]
    public int SalaryTrendMin { get; set; }
    [DataMember]
    public int SalaryTrendMax { get; set; }
    [DataMember]
    public string SalaryTrendRealtime { get; set; }
    [DataMember]
    public int? SalaryTrendRealtimeAverage { get; set; }
    [DataMember]
    public int? SalaryTrendRealtimeMin { get; set; }
    [DataMember]
    public int? SalaryTrendRealtimeMax { get; set; }
    [DataMember]
    public long StateAreaId { get; set; }
    [DataMember]
    public string DegreeIntroStatement { get; set; }
    [DataMember]
    public string ROnet { get; set; }
    [DataMember]
    public StarterJobLevel? StarterJob { get; set; }
    [DataMember]
    public string HiringTrendSubLevel { get; set; }
  }
}