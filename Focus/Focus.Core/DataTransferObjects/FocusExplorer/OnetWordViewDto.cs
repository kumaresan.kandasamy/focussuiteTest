#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusExplorer
{
  [Serializable]
  [DataContract(Name = "OnetWordView", Namespace = Constants.DataContractNamespace)]
  public class OnetWordViewDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public long OnetId { get; set; }
    [DataMember]
    public long OnetRingId { get; set; }
    [DataMember]
    public string OnetSoc { get; set; }
    [DataMember]
    public string Stem { get; set; }
    [DataMember]
    public string Word { get; set; }
    [DataMember]
    public long JobFamilyId { get; set; }
    [DataMember]
    public int JobZone { get; set; }
    [DataMember]
    public string OnetKey { get; set; }
    [DataMember]
    public string OnetCode { get; set; }
    [DataMember]
    public int? Frequency { get; set; }
  }
}