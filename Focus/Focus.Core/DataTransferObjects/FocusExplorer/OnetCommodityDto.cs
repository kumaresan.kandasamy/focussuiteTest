#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusExplorer
{
  [Serializable]
  [DataContract(Name = "OnetCommodity", Namespace = Constants.DataContractNamespace)]
  public class OnetCommodityDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public ToolTechnologyTypes ToolTechnologyType { get; set; }
    [DataMember]
    public string ToolTechnologyExample { get; set; }
    [DataMember]
    public string Code { get; set; }
    [DataMember]
    public string Title { get; set; }
    [DataMember]
    public long OnetId { get; set; }
  }
}