﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusExplorer
{
  [Serializable]
  [DataContract(Name = "OnetToROnetConversionView", Namespace = Constants.DataContractNamespace)]
  public class OnetToROnetConversionViewDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public long OnetId { get; set; }
    [DataMember]
    public string OnetCode { get; set; }
    [DataMember]
    public string OnetKey { get; set; }
    [DataMember]
    public long ROnetId { get; set; }
    [DataMember]
    public string ROnetCode { get; set; }
		[DataMember]
    public string ROnetKey { get; set; }
    [DataMember]
    public bool BestFit { get; set; }
  }
}
