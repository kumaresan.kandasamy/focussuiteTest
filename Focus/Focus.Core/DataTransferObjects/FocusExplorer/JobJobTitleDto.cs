﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusExplorer
{
  [Serializable]
  [DataContract(Name = "JobJobTitle", Namespace = Constants.DataContractNamespace)]
  public class JobJobTitleDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public decimal DemandPercentile { get; set; }
    [DataMember]
    public long JobId { get; set; }
    [DataMember]
    public long JobTitleId { get; set; }
  }
}