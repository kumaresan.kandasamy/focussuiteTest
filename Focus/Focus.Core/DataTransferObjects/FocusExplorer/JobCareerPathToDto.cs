﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusExplorer
{
  [Serializable]
  [DataContract(Name = "JobCareerPathTo", Namespace = Constants.DataContractNamespace)]
  public class JobCareerPathToDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public int Rank { get; set; }
    [DataMember]
    public long CurrentJobId { get; set; }
    [DataMember]
    public long Next1JobId { get; set; }
    [DataMember]
    public long? Next2JobId { get; set; }
  }
}