﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusExplorer
{
  [Serializable]
  [DataContract(Name = "EmployerSkill", Namespace = Constants.DataContractNamespace)]
  public class EmployerSkillDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public int Postings { get; set; }
    [DataMember]
    public long EmployerId { get; set; }
    [DataMember]
    public long SkillId { get; set; }
  }
}