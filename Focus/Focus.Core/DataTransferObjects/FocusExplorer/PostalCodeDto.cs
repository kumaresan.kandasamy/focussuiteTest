#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusExplorer
{
  [Serializable]
  [DataContract(Name = "PostalCode", Namespace = Constants.DataContractNamespace)]
  public class PostalCodeDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public string Code { get; set; }
    [DataMember]
    public double Longitude { get; set; }
    [DataMember]
    public double Latitude { get; set; }
    [DataMember]
    public string CountryKey { get; set; }
    [DataMember]
    public string StateKey { get; set; }
    [DataMember]
    public string CountyKey { get; set; }
    [DataMember]
    public string CityName { get; set; }
    [DataMember]
    public string OfficeKey { get; set; }
    [DataMember]
    public string WibLocationKey { get; set; }
    [DataMember]
    public string MsaId { get; set; }
  }
}