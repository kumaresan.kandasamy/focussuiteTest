using System;
using System.Runtime.Serialization;

namespace Focus.Core.DataTransferObjects.FocusExplorer
{
  [Serializable]
  [DataContract(Name = "NAICS", Namespace = Constants.DataContractNamespace)]
  public class NAICSDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public string Code { get; set; }
    [DataMember]
    public string Name { get; set; }
    [DataMember]
    public long ParentId { get; set; }
  }
}