﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusExplorer
{
	[Serializable]
	[DataContract(Name = "Onet17Onet12MappingView", Namespace = Constants.DataContractNamespace)]
	public class Onet17Onet12MappingViewDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public string Onet17Code { get; set; }
		[DataMember]
		public string Onet12Code { get; set; }
		[DataMember]
		public long OnetId { get; set; }
	}
}
