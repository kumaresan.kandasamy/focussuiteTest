﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusExplorer
{
  [Serializable]
  [DataContract(Name = "CareerAreaJobDegreeView", Namespace = Constants.DataContractNamespace)]
  public class CareerAreaJobDegreeViewDto
  {
    [DataMember]
    public System.Guid? Id { get; set; }
    [DataMember]
    public long CareerAreaId { get; set; }
    [DataMember]
    public long JobId { get; set; }
    [DataMember]
    public long DegreeEducationLevelId { get; set; }
    [DataMember]
    public ExplorerEducationLevels EducationLevel { get; set; }
    [DataMember]
    public long DegreeId { get; set; }
    [DataMember]
    public bool IsClientData { get; set; }
    [DataMember]
    public string ClientDataTag { get; set; }
  }
}
