#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusExplorer
{
  [Serializable]
  [DataContract(Name = "OnetView", Namespace = Constants.DataContractNamespace)]
  public class OnetViewDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public string Key { get; set; }
    [DataMember]
    public string Occupation { get; set; }
    [DataMember]
    public long JobFamilyId { get; set; }
    [DataMember]
    public string OnetCode { get; set; }
    [DataMember]
    public string Culture { get; set; }
    [DataMember]
    public string Description { get; set; }
    [DataMember]
    public bool JobTasksAvailable { get; set; }
  }
}