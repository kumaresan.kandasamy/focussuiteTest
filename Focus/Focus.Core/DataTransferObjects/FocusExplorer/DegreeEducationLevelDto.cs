﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusExplorer
{
  [Serializable]
  [DataContract(Name = "DegreeEducationLevel", Namespace = Constants.DataContractNamespace)]
  public class DegreeEducationLevelDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public ExplorerEducationLevels EducationLevel { get; set; }
    [DataMember]
    public int DegreesAwarded { get; set; }
    [DataMember]
    public string Name { get; set; }
    [DataMember]
    public bool ExcludeFromReport { get; set; }
    [DataMember]
    public long DegreeId { get; set; }
  }
}