﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.FocusExplorer
{
  [Serializable]
  [DataContract(Name = "EmployerJobView", Namespace = Constants.DataContractNamespace)]
  public class EmployerJobViewDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public string EmployerName { get; set; }
    [DataMember]
    public long JobId { get; set; }
    [DataMember]
    public string JobName { get; set; }
    [DataMember]
    public int Positions { get; set; }
    [DataMember]
    public long EmployerId { get; set; }
    [DataMember]
    public long StateAreaId { get; set; }
    [DataMember]
    public string ROnet { get; set; }
    [DataMember]
    public StarterJobLevel? StarterJob { get; set; }
  }
}