﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Focus.Core;

namespace Focus.Core.DataTransferObjects.FocusMigration
{
	public class EmployerTradeNameDto
	{
		[DataMember]
		public long? Id { get; set; }
		[DataMember]
		public long SourceId { get; set; }
		[DataMember]
		public long FocusId { get; set; }
		[DataMember]
		public string Request { get; set; }
		[DataMember]
		public MigrationStatus Status { get; set; }
		[DataMember]
		public string ExternalId { get; set; }
		[DataMember]
		public string Message { get; set; }
		[DataMember]
		public MigrationEmployerRecordType RecordType { get; set; }
	}
}
