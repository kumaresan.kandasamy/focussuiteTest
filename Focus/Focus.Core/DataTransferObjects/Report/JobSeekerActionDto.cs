﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.Report
{
  [Serializable]
  [DataContract(Name = "JobSeekerAction", Namespace = Constants.DataContractNamespace)]
  public class JobSeekerActionDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public DateTime ActionDate { get; set; }
    [DataMember]
    public int PostingsViewed { get; set; }
    [DataMember]
    public int Logins { get; set; }
    [DataMember]
    public int ReferralRequests { get; set; }
    [DataMember]
    public int SelfReferrals { get; set; }
    [DataMember]
    public int StaffReferrals { get; set; }
    [DataMember]
    public int NotesAdded { get; set; }
    [DataMember]
    public int AddedToLists { get; set; }
    [DataMember]
    public int ActivitiesAssigned { get; set; }
    [DataMember]
    public int StaffAssignments { get; set; }
    [DataMember]
    public int EmailsSent { get; set; }
    [DataMember]
    public int FollowUpIssues { get; set; }
    [DataMember]
    public int IssuesResolved { get; set; }
    [DataMember]
    public int Hired { get; set; }
    [DataMember]
    public int NotHired { get; set; }
    [DataMember]
    public int FailedToApply { get; set; }
    [DataMember]
    public int FailedToReportToInterview { get; set; }
    [DataMember]
    public int InterviewDenied { get; set; }
    [DataMember]
    public int InterviewScheduled { get; set; }
    [DataMember]
    public int NewApplicant { get; set; }
    [DataMember]
    public int Recommended { get; set; }
    [DataMember]
    public int RefusedOffer { get; set; }
    [DataMember]
    public int UnderConsideration { get; set; }
    [DataMember]
    public int UsedOnlineResumeHelp { get; set; }
    [DataMember]
    public int SavedJobAlerts { get; set; }
    [DataMember]
    public int TargetingHighGrowthSectors { get; set; }
    [DataMember]
    public int SelfReferralsExternal { get; set; }
    [DataMember]
    public int StaffReferralsExternal { get; set; }
    [DataMember]
    public int ReferralsApproved { get; set; }
    [DataMember]
    public int ActivityServices { get; set; }
    [DataMember]
    public int FindJobsForSeeker { get; set; }
    [DataMember]
    public int FailedToReportToJob { get; set; }
    [DataMember]
    public int FailedToRespondToInvitation { get; set; }
    [DataMember]
    public int FoundJobFromOtherSource { get; set; }
    [DataMember]
    public int JobAlreadyFilled { get; set; }
    [DataMember]
    public int NotQualified { get; set; }
    [DataMember]
    public int NotYetPlaced { get; set; }
    [DataMember]
    public int RefusedReferral { get; set; }
    [DataMember]
    public int SurveySatisfied { get; set; }
    [DataMember]
    public int SurveyDissatisfied { get; set; }
    [DataMember]
    public int SurveyNoUnexpectedMatches { get; set; }
    [DataMember]
    public int SurveyUnexpectedMatches { get; set; }
    [DataMember]
    public int SurveyReceivedInvitations { get; set; }
    [DataMember]
    public int SurveyDidNotReceiveInvitations { get; set; }
    [DataMember]
    public int SurveyVerySatisfied { get; set; }
    [DataMember]
    public int SurveyVeryDissatisfied { get; set; }
    [DataMember]
    public long JobSeekerId { get; set; }

    public int TotalReferrals
    {
      get { return SelfReferrals + SelfReferralsExternal + StaffReferrals + StaffReferralsExternal; }
    }
  }
}