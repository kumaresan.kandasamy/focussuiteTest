﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.Report
{
  [Serializable]
  [DataContract(Name = "JobOrderAction", Namespace = Constants.DataContractNamespace)]
  public class JobOrderActionDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public DateTime ActionDate { get; set; }
    [DataMember]
    public int StaffReferrals { get; set; }
    [DataMember]
    public int SelfReferrals { get; set; }
    [DataMember]
    public int EmployerInvitationsSent { get; set; }
    [DataMember]
    public int ApplicantsInterviewed { get; set; }
    [DataMember]
    public int ApplicantsFailedToShow { get; set; }
    [DataMember]
    public int ApplicantsDeniedInterviews { get; set; }
    [DataMember]
    public int ApplicantsHired { get; set; }
    [DataMember]
    public int JobOffersRefused { get; set; }
    [DataMember]
    public int ApplicantsDidNotApply { get; set; }
    [DataMember]
    public int InvitedJobSeekerViewed { get; set; }
    [DataMember]
    public int InvitedJobSeekerClicked { get; set; }
    [DataMember]
    public int ReferralsRequested { get; set; }
    [DataMember]
    public int ApplicantsNotHired { get; set; }
    [DataMember]
    public int ApplicantsNotYetPlaced { get; set; }
    [DataMember]
    public int FailedToRespondToInvitation { get; set; }
    [DataMember]
    public int FailedToReportToJob { get; set; }
    [DataMember]
    public int FoundJobFromOtherSource { get; set; }
    [DataMember]
    public int JobAlreadyFilled { get; set; }
    [DataMember]
    public int NewApplicant { get; set; }
    [DataMember]
    public int NotQualified { get; set; }
    [DataMember]
    public int ApplicantRecommended { get; set; }
    [DataMember]
    public int RefusedReferral { get; set; }
    [DataMember]
    public int UnderConsideration { get; set; }
    [DataMember]
    public long JobOrderId { get; set; }

    public int TotalReferrals
    {
      get { return SelfReferrals + StaffReferrals; }
    }
  }
}
