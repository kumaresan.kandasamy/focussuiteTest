﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.Report
{
  [Serializable]
  [DataContract(Name = "SavedReport", Namespace = Constants.DataContractNamespace)]
  public class SavedReportDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public ReportType ReportType { get; set; }
    [DataMember]
    public string Criteria { get; set; }
    [DataMember]
    public long? UserId { get; set; }
    [DataMember]
    public string Name { get; set; }
    [DataMember]
    public DateTime ReportDate { get; set; }
    [DataMember]
    public bool DisplayOnDashboard { get; set; }
    [DataMember]
    public bool IsSessionReport { get; set; }
    [DataMember]
    public ReportDisplayType? ReportDisplayType { get; set; }
  }
}
