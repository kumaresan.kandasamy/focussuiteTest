﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.DataTransferObjects.Report
{
  [Serializable]
  [DataContract(Name = "EmployerAction", Namespace = Constants.DataContractNamespace)]
  public class EmployerActionDto
  {
    [DataMember]
    public long? Id { get; set; }
    [DataMember]
    public DateTime ActionDate { get; set; }
    [DataMember]
    public int JobOrdersEdited { get; set; }
    [DataMember]
    public int JobSeekersInterviewed { get; set; }
    [DataMember]
    public int JobSeekersHired { get; set; }
    [DataMember]
    public int JobOrdersCreated { get; set; }
    [DataMember]
    public int JobOrdersPosted { get; set; }
    [DataMember]
    public int JobOrdersPutOnHold { get; set; }
    [DataMember]
    public int JobOrdersRefreshed { get; set; }
    [DataMember]
    public int JobOrdersClosed { get; set; }
    [DataMember]
    public int InvitationsSent { get; set; }
    [DataMember]
    public int JobSeekersNotHired { get; set; }
    [DataMember]
    public int SelfReferrals { get; set; }
    [DataMember]
    public int StaffReferrals { get; set; }
    [DataMember]
    public long EmployerId { get; set; }
  }
}
