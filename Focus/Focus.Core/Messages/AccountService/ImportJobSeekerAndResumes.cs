﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AccountService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ImportJobSeekerAndResumesRequest : ServiceRequest
  {
    [DataMember]
    public string JobSeekerExternalId { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ImportJobSeekerAndResumesResponse : ServiceResponse
  {
    public ImportJobSeekerAndResumesResponse(ImportJobSeekerAndResumesRequest request) : base(request) { }

    [DataMember]
    public long? FocusId { get; set; }

    [DataMember]
    public List<string> Exceptions { get; set; }
  }
}
