﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.User;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateUserAlertRequest : ServiceRequest
	{
		[DataMember]
		public UserAlertCriteria Criteria;

		[DataMember]
    public UserAlertDto UserAlert { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateUserAlertResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="UpdateUserAlertResponse"/> class.
		/// </summary>
		public UpdateUserAlertResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="UpdateUserAlertResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public UpdateUserAlertResponse(UpdateUserAlertRequest request) : base(request) { }

		[DataMember]
    public UserAlertDto UserAlert { get; set; }
	}
}
