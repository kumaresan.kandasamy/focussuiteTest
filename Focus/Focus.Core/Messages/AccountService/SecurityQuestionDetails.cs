﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract( Namespace = Constants.DataContractNamespace )]
	public class SecurityQuestionDetailsRequest : ServiceRequest
	{
		[DataMember]
		public long UserId;

		[DataMember]
		public int QuestionIndex;
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SecurityQuestionDetailsResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetUserDetailsResponse"/> class.
		/// </summary>
		public SecurityQuestionDetailsResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="GetUserDetailsResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SecurityQuestionDetailsResponse( SecurityQuestionDetailsRequest request ) : base( request ) { }

		[DataMember]
		public IEnumerable<UserSecurityQuestionDto> UserSecurityQuestions;

		[DataMember]
		public bool HasQuestions { get; set; }
	}
}
