﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives


using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetSSNRequest : ServiceRequest
	{
		[DataMember]
		public long PersonId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetSSNResponse : ServiceResponse
	{
		public GetSSNResponse()
		{ }

		public GetSSNResponse(GetSSNRequest request) : base(request)
		{ }

		[DataMember]
		public string SSN { get; set; }
	}
}
