﻿#region Copyright © 2000-2016 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetUserTypeRequest : ServiceRequest
	{
		[DataMember]
		public long UserId;
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetUserTypeResponse : ServiceResponse
	{
		public GetUserTypeResponse() { }

		public GetUserTypeResponse(GetUserTypeRequest request) : base(request) { }

		[DataMember]
		public UserTypes UserType { get; set; }
	}
}
