﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Common.Models;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CreateAssistUserRequest : ServiceRequest
	{
		//[DataMember]
		//public string AccountUserName { get; set; }

		//[DataMember]
		//public string AccountPassword { get; set; }

		//[DataMember]
		//public string AccountExternalId { get; set; }

		//// Contact (Employee) Details
		//[DataMember]
		//public PersonDto UserPerson { get; set; }

		//[DataMember]
		//public PersonAddressDto UserAddress { get; set; }

		//[DataMember]
		//public string UserPhone { get; set; }

		//[DataMember]
		//public string UserAlternatePhone { get; set; }

		//[DataMember]
		//public string UserFax { get; set; }

		//[DataMember]
		//public string UserEmailAddress { get; set; }

		//[DataMember]
		//public string UserExternalOffice { get; set; }

		//[DataMember]
		//public bool ValidatePasswordStrength { get; set; }

		//[DataMember]
		//public bool IsClientAuthenticated { get; set; }

		//[DataMember]
		//public bool IsEnabled { get; set; }

		//[DataMember]
		//public string UserMigrationId { get; set; }

		[DataMember]
		public bool GeneratePassword { get; set; }

		[DataMember]
		public bool SendEmail { get; set; }

		[DataMember]
		public List<CreateAssistUserModel> Models { get; set; }

    [DataMember]
    public List<string> ExtraRoles { get; set; }
    
    [DataMember]
		public bool BulkUpload { get; set; }

    [DataMember]
    public string UserMigrationId { get; set; }

    [DataMember]
    public string UserSecondaryMigrationId { get; set; }

    [DataMember]
    public string MigrationValidationMessage { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CreateAssistUserResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CreateAssistUserResponse"/> class.
		/// </summary>
		public CreateAssistUserResponse()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="CreateAssistUserResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public CreateAssistUserResponse(CreateAssistUserRequest request) : base(request)
		{ }

    [DataMember]
    public long UserId { get; set; }

		[DataMember]
		public List<CreateAssistUserModel> Models { get; set; }
	}
}
