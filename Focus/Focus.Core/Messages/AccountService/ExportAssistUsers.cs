﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;
using Focus.Common.Models;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ExportAssistUsersRequest : ServiceRequest
	{
		[DataMember]
		public string FileName { get; set; }

		[DataMember]
		public byte[] File { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ExportAssistUsersResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ExportAssistUsersResponse"/> class.
		/// </summary>
		public ExportAssistUsersResponse()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="ExportAssistUsersResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public ExportAssistUsersResponse(ExportAssistUsersRequest request): base(request)
		{ }

		public List<CreateAssistUserModel> ModelList { get; set; }

		public bool ValidationErrors { get; set; }
	}
}
