﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
#endregion


namespace Focus.Core.Messages.AccountService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CheckSocialSecurityNumberInUseRequest : ServiceRequest
  {
    [DataMember]
    public string SocialSecurityNumber { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CheckSocialSecurityNumberInUseResponse : ServiceResponse
  {
    /// <summary>
    /// Default Constructor. 
    /// </summary>
    public CheckSocialSecurityNumberInUseResponse()
    { }

    /// <summary>
    /// Initializes a new instance of the <see cref="CheckSocialSecurityNumberInUseResponse"/> class.
    /// </summary>
    /// <param name="request">The request.</param>
    public CheckSocialSecurityNumberInUseResponse(CheckSocialSecurityNumberInUseRequest request)
      : base(request)
    { }

    [DataMember]
    public bool Exists { get; set; }
  }
}
