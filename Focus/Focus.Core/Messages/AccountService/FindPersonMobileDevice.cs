﻿using System;
using System.Runtime.Serialization;

namespace Focus.Core.Messages.AccountService
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class FindPersonMobileDeviceRequest : ServiceRequest
    {
        [DataMember]
        public string Token { get; set; }
    }

    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class FindPersonMobileDeviceResponse : ServiceResponse
    {
        public FindPersonMobileDeviceResponse()
        { }

        public FindPersonMobileDeviceResponse(FindPersonMobileDeviceRequest request)
            : base(request)
        { }

        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string ApiKey { get; set; }

        [DataMember]
        public string Token { get; set; }

        [DataMember]
        public long PersonId { get; set; }

        [DataMember]
        public DeviceTypes DeviceType { get; set; }

        [DataMember]
        public DateTime CreatedOn { get; set; }
    }
}