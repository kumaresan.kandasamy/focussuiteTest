﻿#region Copyright © 2000 - 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AccountService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ImportEmployerAndArtifactsRequest:ServiceRequest
  {
    [DataMember]
    public string EmployerExternalId { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ImportEmployerAndArtifactsResponse : ServiceResponse
  {
    public ImportEmployerAndArtifactsResponse(ImportEmployerAndArtifactsRequest request) : base(request) { }

    [DataMember]
    public long? FocusId { get; set; }

    [DataMember]
    public List<string> Exceptions { get; set; }
  }
}
