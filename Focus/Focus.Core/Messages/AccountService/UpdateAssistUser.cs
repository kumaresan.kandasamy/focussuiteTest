﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.AccountService
{
  public class UpdateAssistUserRequest : ServiceRequest
  {
    [DataMember]
    public long UserId { get; set; }

    [DataMember]
    public bool? Manager { get; set; }

    [DataMember]
    public bool? LocalVeteranEmploymentRepresentative { get; set; }

    [DataMember]
    public bool? DisabledVeteransOutreachProgramSpecialist { get; set; }
  }

  public class UpdateAssistUserResponse : ServiceResponse
  {
    public UpdateAssistUserResponse() { }

    public UpdateAssistUserResponse(UpdateAssistUserRequest request) : base(request) { }

    [DataMember]
    public UserDto User { get; set; }
  }
}
