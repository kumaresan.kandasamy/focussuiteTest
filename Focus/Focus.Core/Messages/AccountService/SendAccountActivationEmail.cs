#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AccountService
{

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SendAccountActivationEmailRequest : ServiceRequest
  {
    [DataMember]
    public string Name { get; set; }

    [DataMember]
    public string Email { get; set; }

    [DataMember]
    public string SingleSignOnURL { get; set; }

		[DataMember]
	  public string UserName { get; set; }

    [DataMember]
    public FocusModules Module { get; set; }
  }
  

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SendAccountActivationEmailResponse : ServiceResponse
  {
    public SendAccountActivationEmailResponse() { }

    public SendAccountActivationEmailResponse(SendAccountActivationEmailRequest request) : base(request) { }
  }
}
