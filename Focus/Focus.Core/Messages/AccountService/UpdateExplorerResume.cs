﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Models;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateExplorerResumeRequest : ServiceRequest
	{
		[DataMember]
		public ExplorerResumeModel ResumeModel { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class  UpdateExplorerResumeResponse : ServiceResponse
	{
		public UpdateExplorerResumeResponse() : base() {}

		public UpdateExplorerResumeResponse(UpdateExplorerResumeRequest request) : base(request) { }

		[DataMember]
		public ExplorerResumeModel ResumeModel { get; set; }
	}
}
