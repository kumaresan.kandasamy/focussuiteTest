﻿using System.Runtime.Serialization;

namespace Focus.Core.Messages.AccountService
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class DeletePersonMobileDeviceRequest : ServiceRequest
    {
        [DataMember]
        public string Token { get; set; }
    }

    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class DeletePersonMobileDeviceResponse : ServiceResponse
    {
        public DeletePersonMobileDeviceResponse()
        {
        }

        public DeletePersonMobileDeviceResponse(DeletePersonMobileDeviceRequest request) : base(request) { }
    }
}
