﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.AccountService
{
	public class UpdateUserRequest : ServiceRequest
	{
		[DataMember]
		public long UserId { get; set; }

		[DataMember]
		public DateTime? DateOfBirth { get; set; }

		[DataMember]
		public string UserName { get; set; }

		[DataMember]
		public string EmailAddress { get; set; }

		[DataMember]
		public string Password { get; set; }

		[DataMember]
		public string SocialSecurityNumber { get; set; }

		[DataMember]
		public string FirstName { get; set; }

		[DataMember]
		public string MiddleInitial { get; set; }

		[DataMember]
		public string LastName { get; set; }

		[DataMember]
		public PhoneNumberDto PhoneNumber { get; set; }

		[DataMember]
		public Address PostalAddress { get; set; }

		[DataMember]
		public string SecurityQuestion { get; set; }

		[DataMember]
		public long? SecurityQuestionId { get; set; }

		[DataMember]
		public string SecurityAnswer { get; set; }

		[DataMember]
		public SchoolStatus? EnrollmentStatus { get; set; }

    [DataMember]
    public long? CampusId { get; set; }

		[DataMember]
		public long? ProgramOfStudyId { get; set; }

		[DataMember]
		public long? DegreeId { get; set; }
	}

	public class UpdateUserResponse : ServiceResponse
	{
		public UpdateUserResponse() { }

		public UpdateUserResponse(UpdateUserRequest request) : base(request) { }

		[DataMember]
		public long UserId { get; set; }
	}
}
