﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateScreenNameAndEmailAddressRequest : ServiceRequest
	{
		[DataMember]
		public string ScreenName { get; set; }

		[DataMember]
		public string EmailAddress { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateScreenNameAndEmailAddressResponse : ServiceResponse
	{
		public UpdateScreenNameAndEmailAddressResponse() : base() {}

		public UpdateScreenNameAndEmailAddressResponse(UpdateScreenNameAndEmailAddressRequest request) : base(request) { }
	}
}
