﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AccountService
{

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ChangeProgramOfStudyRequest : ServiceRequest
  {
    [DataMember]
    public long? StudyProgramAreaId { get; set; }

    [DataMember]
    public long? StudyProgramDegreeId { get; set; }
 
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ChangeProgramOfStudyResponse : ServiceResponse
  {
    public ChangeProgramOfStudyResponse() : base() { }

    public ChangeProgramOfStudyResponse(ChangeProgramOfStudyRequest request) : base(request) { }
  }
}
