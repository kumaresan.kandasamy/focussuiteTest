﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.User;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetUserDetailsRequest : ServiceRequest
	{
		[DataMember]
		public UserCriteria Criteria;
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetUserDetailsResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetUserDetailsResponse"/> class.
		/// </summary>
		public GetUserDetailsResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="GetUserDetailsResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GetUserDetailsResponse(GetUserDetailsRequest request) : base(request) { }

    [DataMember]
    public string SecurityQuestion { get; set; }

		[DataMember]
		public long? SecurityQuestionId { get; set; }

		[DataMember]
		public string SecurityAnswer { get; set; }

		[DataMember]
		public UserDto UserDetails { get; set; }

		[DataMember]
		public PersonDto PersonDetails { get; set; }

		[DataMember]
		public PersonAddressDto AddressDetails { get; set; }

		[DataMember]
		public PhoneNumberDto PrimaryPhoneNumber { get; set; }

		[DataMember]
		public PhoneNumberDto AlternatePhoneNumber1 { get; set; }

		[DataMember]
		public PhoneNumberDto AlternatePhoneNumber2 { get; set; }

		[DataMember]
		public List<PersonOfficeMapperDto> OfficeRoles { get; set; }

		[DataMember]
		public int? EmployeeLockNumber { get; set; }
	}
}
