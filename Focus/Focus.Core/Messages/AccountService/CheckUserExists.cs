﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CheckUserExistsRequest: ServiceRequest
	{
    [DataMember]
    public long? ExistingUserId { get; set; }

		[DataMember]
		public FocusModules ModuleToCheck { get; set; }

    [DataMember]
    public bool CheckLocalOnly { get; set; }

		[DataMember]
		public string UserName { get; set; }

		// Below are additional items for Career validation
		// We may extend the Talent / Assist validation to be the same
		//
		// If ModuleToCheck is anything other than Career or CareerExplorer then all below is ingored
		
		[DataMember]
		public DateTime? DateOfBirth { get; set; }

		[DataMember]
		public string EmailAddress { get; set; }

		[DataMember]
		public string SSN { get; set; }

		[DataMember]
		public string FirstName { get; set; }

		[DataMember]
		public string LastName { get; set; }

		[DataMember]
		public Phone PrimaryPhone { get; set; }

		[DataMember]
		public Address PostalAddress { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CheckUserExistsResponse : ServiceResponse
	{
		/// <summary>
		/// Default Constructor. 
		/// </summary>
		public CheckUserExistsResponse()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="CheckUserExistsResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public CheckUserExistsResponse(CheckUserExistsRequest request) : base(request)
		{ }

		[DataMember]
		public bool Exists { get; set; }

    [DataMember]
    public ExtenalUserPreExistenceReason ExistenceReason { get; set; }
	}
}
