﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.User;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateContactDetailsRequest : ServiceRequest
	{
		[DataMember]
		public UserCriteria Criteria;

		[DataMember]
		public PersonDto PersonDetails { get; set; }

		[DataMember]
		public PhoneNumberDto PrimaryContactDetails { get; set; }

		[DataMember]
		public PhoneNumberDto Alternate1ContactDetails { get; set; }

		[DataMember]
		public PhoneNumberDto Alternate2ContactDetails { get; set; }

		[DataMember]
		public PersonAddressDto AddressDetails { get; set; }

    [DataMember]
    public UserDto UserDetails { get; set; }

    [DataMember]
    public bool CheckCensorship { get; set; }

		[DataMember]
		public int? LockVersion { get; set; }
  }

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateContactDetailsResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="UpdateContactDetailsResponse"/> class.
		/// </summary>
		public UpdateContactDetailsResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="UpdateContactDetailsResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public UpdateContactDetailsResponse(UpdateContactDetailsRequest request) : base(request) { }

		[DataMember]
		public UserDto User { get; set; }

		[DataMember]
		public PersonDto Person { get; set; }

		[DataMember]
		public PhoneNumberDto PrimaryPhoneNumber { get; set; }

		[DataMember]
		public PhoneNumberDto AlternatePhoneNumber1 { get; set; }

		[DataMember]
		public PhoneNumberDto AlternatePhoneNumber2 { get; set; }

		[DataMember]
		public PersonAddressDto AddressDetails { get; set; }

    [DataMember]
    public bool FailedCensorshipCheck { get; set; }

		[DataMember]
		public int? LockVersion { get; set; }
	}
}
