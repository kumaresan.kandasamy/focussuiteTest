﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SendRegistrationPinEmailRequest : ServiceRequest
	{
		[DataMember]
		public List<string> Email { get; set; }

		[DataMember]
		public string PinRegistrationUrl { get; set; }

    [DataMember]
    public string ResetPasswordUrl { get; set; }

		[DataMember]
		public FocusModules TargetModule { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SendRegistrationPinEmailResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SendRegistrationPinEmailResponse"/> class.
		/// </summary>
		public SendRegistrationPinEmailResponse(){ }

		/// <summary>
		/// Initializes a new instance of the <see cref="SendRegistrationPinEmailResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SendRegistrationPinEmailResponse(SendRegistrationPinEmailRequest request) : base(request){ }
	}
}
