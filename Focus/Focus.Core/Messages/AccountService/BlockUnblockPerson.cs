﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AccountService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class BlockUnblockPersonRequest : ServiceRequest
  {
    [DataMember]
    public bool Block { get; set; }

    [DataMember]
    public long PersonId { get; set; }

		[DataMember]
		public bool DeactivateJobSeeker { get; set; }

		[DataMember]
		public string ReasonText { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class BlockUnblockPersonResponse : ServiceResponse
  {
    public BlockUnblockPersonResponse() { }

    public BlockUnblockPersonResponse(BlockUnblockPersonRequest request) : base(request) { }

    [DataMember]
    public bool Success { get; set; }
  }
}
