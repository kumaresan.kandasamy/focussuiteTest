﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class FindUserRequest: ServiceRequest
	{
		[DataMember]
		public UserFindBy FindBy { get;  set; }

		[DataMember]
		public string SearchFirstName { get; set; }

		[DataMember]
		public string SearchLastName { get; set; }

		[DataMember]
		public string SearchEmailAddress { get; set; }

		[DataMember]
		public string SearchExternalOffice { get; set; }

		[DataMember]
		public string SearchRole { get; set; }

		[DataMember]
		public string SearchExternalId { get; set; }

		[DataMember]
		public int PageIndex { get; set; }

		[DataMember]
		public int PageSize { get; set; }

		public enum UserFindBy
		{
			Name,
			Email,
			ExternalOffice,
			ExternalId
		}
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class FindUserResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="FindUserResponse"/> class.
		/// </summary>
		public FindUserResponse()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="FindUserResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public FindUserResponse(FindUserRequest request) : base(request)
		{ }

		/// <summary>
		/// Gets or sets the users.
		/// </summary>
		/// <value>The users.</value>
		[DataMember]
		public PagedList<UserView> Users { get; set; }
	}
}
