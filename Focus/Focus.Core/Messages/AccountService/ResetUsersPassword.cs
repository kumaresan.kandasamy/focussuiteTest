﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ResetUsersPasswordRequest : ServiceRequest
	{
		[DataMember]
		public long ResetUserId { get; set; }

		[DataMember]
		public string ResetPasswordUrl { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ResetUsersPasswordResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ResetUsersPasswordResponse"/> class.
		/// </summary>
		public ResetUsersPasswordResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="ResetUsersPasswordResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public ResetUsersPasswordResponse(ResetUsersPasswordRequest request) : base(request) { }
	}
}
