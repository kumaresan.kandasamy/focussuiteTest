﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UnsubscribeUserFromEmailsRequest : ServiceRequest
	{
		[DataMember]
		public string EncryptedPersonId { get; set; }

		[DataMember]
		public long EncryptionId { get; set; }

		[DataMember]
		public bool UrlEncoded { get; set; }

		[DataMember]
		public TargetTypes TargetType { get; set; }

		[DataMember]
		public EntityTypes UserType { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UnsubscribeUserFromEmailsResponse : ServiceResponse
	{
		public UnsubscribeUserFromEmailsResponse() { }

		public UnsubscribeUserFromEmailsResponse(UnsubscribeUserFromEmailsRequest request) : base(request) { }
	}
}
