﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ValidateEmailRequest : ServiceRequest
	{
		[DataMember]
		public List<string> Emails { get; set; }

		[DataMember]
		public UserTypes UserType { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ValidateEmailResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ValidateEmailResponse"/> class.
		/// </summary>
		public ValidateEmailResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="ValidateEmailResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public ValidateEmailResponse(ValidateEmailRequest request) : base(request) { }

		[DataMember]
		public List<string> InvalidEmailFormat { get; set; }

		[DataMember]
		public List<string> MultipleEmailError { get; set; }

    [DataMember]
    public List<string> DuplicateEmailError { get; set; }

		[DataMember]
		public List<string> UsernameInUse { get; set; }
		
		[DataMember]
		public List<string> ValidEmail { get; set; }
	}
}
