﻿using System.Runtime.Serialization;

namespace Focus.Core.Messages.AccountService
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class CreatePersonMobileDeviceRequest : ServiceRequest
    {
        [DataMember]
        public string ApiKey { get; set; }

        [DataMember]
        public string Token { get; set; }

        [DataMember]
        public long PersonId { get; set; }

        [DataMember]
        public DeviceTypes DeviceType { get; set; }
    }

    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class CreatePersonMobileDeviceResponse : ServiceResponse
    {
        public CreatePersonMobileDeviceResponse() { }

        public CreatePersonMobileDeviceResponse(CreatePersonMobileDeviceRequest request) : base(request) { }

    }
}
