﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.Role;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class RoleRequest : ServiceRequest
	{
		[DataMember]
		public RoleCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class RoleResponse : ServiceResponse
	{
		public RoleResponse(){}

		public RoleResponse(RoleRequest request) : base(request){}

		[DataMember]
		public RoleDto Role { get; set; }

		[DataMember]
		public List<RoleDto> Roles { get; set; }

		[DataMember]
		public PagedList<RoleDto> RolesPaged { get; set; }

		[DataMember]
		public Dictionary<long, string> RolesLookup { get; set; } 
	}
}
