﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateSsnRequest : ServiceRequest
	{
		[DataMember]
		public long Id { get; set; }
		[DataMember]
		public string CurrentSsn { get; set; }

		[DataMember]
		public string NewSsn { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateSsnResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="UpdateSsnResponse"/> class.
		/// </summary>
		public UpdateSsnResponse()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="UpdateSsnResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public UpdateSsnResponse(UpdateSsnRequest request)
			: base(request)
		{ }

		[DataMember]
		public PersonDto Person { get; set; }

		[DataMember]
		public ErrorTypes UiError { get; set; }

		[DataMember]
		public string ErrorText { get; set; }
	}
}
