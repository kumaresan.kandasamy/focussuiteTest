﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.AccountService
{
  public class RegisterCareerUserRequest : ServiceRequest
  {
    // Account Details
    [DataMember]
    public string AccountUserName { get; set; }

    [DataMember]
    public string AccountPassword { get; set; }

    [DataMember]
    public DateTime? DateOfBirth { get; set; }

    [DataMember]
    public string EmailAddress { get; set; }

    [DataMember]
    public string SocialSecurityNumber { get; set; }

    [DataMember]
    public string FirstName { get; set; }

    [DataMember]
    public string MiddleInitial { get; set; }

    [DataMember]
    public string LastName { get; set; }

    [DataMember]
    public long? SuffixId { get; set; }

    [DataMember]
    public Phone PrimaryPhone { get; set; }

    [DataMember]
    public PhoneNumberDto UpdatePhoneNumber { get; set; }

    [DataMember]
    public Address PostalAddress { get; set; }

    [DataMember]
    public string SecurityQuestion { get; set; }

		[DataMember]
		public long? SecurityQuestionId { get; set; }

    [DataMember]
    public string SecurityAnswer { get; set; }

    [DataMember]
    public bool? IsMigrated { get; set; }

    [DataMember]
    public bool? RegulationsConsent { get; set; }

    [DataMember]
    public string ExternalId { get; set; }

    [DataMember]
    public SchoolStatus? EnrollmentStatus { get; set; }

    [DataMember]
    public long? ProgramOfStudyId { get; set; }

    [DataMember]
    public long? DegreeId { get; set; }

    [DataMember]
    public long? CampusId { get; set; }

    [DataMember]
    public bool? Enabled { get; set; }

    [DataMember]
    public string Pin { get; set; }

    [DataMember]
    public string PinRegisteredEmailAddress { get; set; }

    [DataMember]
    public long? OverrideOfficeId { get; set; }

    [DataMember]
    public DateTime? OverrideCreationDate { get; set; }

    [DataMember]
    public DateTime? LastLoggedIn { get; set; }

    [DataMember]
    public long? UpdateUserId { get; set; }

    [DataMember]
    public long? UpdatePersonId { get; set; }

    [DataMember]
    public long? InsertPersonId { get; set; }

    [DataMember]
    public string JobSeekerMigrationId { get; set; }

    [DataMember]
    public string MigrationValidationMessage { get; set; }

    [DataMember]
    public string MigratedPasswordHash { get; set; }

    [DataMember]
    public bool IgnoreClient { get; set; }

    [DataMember]
    public CareerAccountType? AccountType { get; set; }

		[DataMember]
		public string ScreenName { get; set; }
	}

  public class RegisterCareerUserResponse : ServiceResponse
  {
    public RegisterCareerUserResponse() { }

    public RegisterCareerUserResponse(RegisterCareerUserRequest request) : base(request) { }

    [DataMember]
    public long UserId { get; set; }

    [DataMember]
    public long PersonId { get; set; }

    [DataMember]
    public string VerificationCode { get; set; }

    [DataMember]
    public string VerificationLink { get; set; }
  }
}
