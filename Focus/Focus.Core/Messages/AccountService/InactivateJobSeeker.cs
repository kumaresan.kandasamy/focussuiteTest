﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class InactivateJobSeekerRequest : ServiceRequest
	{
		[DataMember]
		public long PersonId { get; set; }

		[DataMember]
		public AccountDisabledReason AccountDisabledReason { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class InactivateJobSeekerResponse : ServiceResponse
	{
		public InactivateJobSeekerResponse() { }

		public InactivateJobSeekerResponse(InactivateJobSeekerRequest request) : base(request) { }

		[DataMember]
		public bool Success { get; set; }
	}
}
