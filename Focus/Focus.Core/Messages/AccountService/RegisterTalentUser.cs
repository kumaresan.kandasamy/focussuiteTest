﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.AccountService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class RegisterTalentUserRequest : ServiceRequest
	{
		// User needed for SSO registration
		[DataMember]
		public UserDto User { get; set; }

		[DataMember]
		public UserSecurityQuestionDto[] SecurityQuestions { get; set; }

		[DataMember]
		public DateTime? OverrideUserCreatedOn { get; set; }

		[DataMember]
		public DateTime? OverrideUserUpdatedOn { get; set; }

		// Account Details
		[DataMember]
		public string AccountUserName { get; set; }

		[DataMember]
		public string AccountPassword { get; set; }

		// Contact (Employee) Details
		[DataMember]
		public PersonDto EmployeePerson { get; set; }

		[DataMember]
		public PersonAddressDto EmployeeAddress { get; set; }

		[DataMember]
		public string EmployeePhone { get; set; }

		[DataMember]
		public string EmployeePhoneExtension { get; set; }

		[DataMember]
		public string EmployeePhoneType { get; set; }

		[DataMember]
		public string EmployeeAlternatePhone1 { get; set; }

		[DataMember]
		public string EmployeeAlternatePhone1Type { get; set; }

		[DataMember]
		public string EmployeeAlternatePhone2 { get; set; }

		[DataMember]
		public string EmployeeAlternatePhone2Type { get; set; }

		[DataMember]
		public string EmployeeEmailAddress { get; set; }

		// Corporate (Employer) Details
		[DataMember]
		public EmployerDto Employer { get; set; }

		[DataMember]
		public DateTime? OverrideEmployerCreatedOn { get; set; }

		[DataMember]
		public DateTime? OverrideEmployerUpdatedOn { get; set; }

		[DataMember]
		public string EmployerExternalId { get; set; }

		[DataMember]
		public string EmployeeExternalId { get; set; }

		[DataMember]
		public string StateEmployerIdentificationNumber { get; set; }

		[DataMember]
		public EmployerAddressDto EmployerAddress { get; set; }

		[DataMember]
		public BusinessUnitDto BusinessUnit { get; set; }

		[DataMember]
		public BusinessUnitDescriptionDto BusinessUnitDescription { get; set; }

		[DataMember]
		public BusinessUnitAddressDto BusinessUnitAddress { get; set; }

		[DataMember]
		public string EmployeeMigrationId { get; set; }

		[DataMember]
		public FocusModules Module { get; set; }

		[DataMember]
		public bool IgnoreClient { get; set; }

		[DataMember]
		public long? UpdateExistingEmployeeId { get; set; }

		[DataMember]
		public string MigrationValidationMessage { get; set; }

		[DataMember]
		public string MigratedPasswordHash { get; set; }

		[DataMember]
		public ApprovalStatuses? InitialApprovalStatus { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class RegisterTalentUserResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="RegisterTalentUserResponse"/> class.
		/// </summary>
		public RegisterTalentUserResponse()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="RegisterTalentUserResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public RegisterTalentUserResponse(RegisterTalentUserRequest request) : base(request)
		{ }

		[DataMember]
		public long UserId { get; set; }

		[DataMember]
		public long EmployeeId { get; set; }

		[DataMember]
		public long EmployerId { get; set; }

		[DataMember]
		public DateTime? LastLoggedInOn { get; set; }

		[DataMember]
		public bool IsMigrated { get; set; }

		[DataMember]
		public bool RegulationsConsent { get; set; }

		[DataMember]
		public bool FailedCensorshipCheck { get; set; }

		[DataMember]
		public ApprovalStatuses EmployeeApprovalStatus { get; set; }
	}
}
