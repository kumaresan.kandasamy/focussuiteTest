﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.PostingService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ClosePostingRequest : ServiceRequest
	{
		[DataMember]
		public string LensPostingId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ClosePostingResponse : ServiceResponse
	{
		public ClosePostingResponse() { }

		public ClosePostingResponse(ClosePostingRequest request) : base(request) { }
	}
}
