﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

using Focus.Core.Criteria.SpideredPosting;
using Focus.Core.Models.Assist;

#endregion

namespace Focus.Core.Messages.PostingService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SpideredPostingRequest : ServiceRequest
	{
		[DataMember]
		public SpideredPostingCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SpideredPostingResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SpideredPostingResponse"/> class.
		/// </summary>
		public SpideredPostingResponse() : base() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SpideredPostingResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SpideredPostingResponse(SpideredPostingRequest request) : base(request) { }

		[DataMember]
		public PagedList<SpideredPostingModel> SpideredPostingsPaged { get; set; }
	}
}
