﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.PostingService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class AddViewedPostingRequest : ServiceRequest
	{
		[DataMember]
		public ViewedPostingDto ViewedPosting { get; set; }

    [DataMember]
    public string ViewedPostingMigrationId { get; set; }

    [DataMember]
    public string JobSeekerMigrationId { get; set; }

    [DataMember]
    public string MigrationValidationMessage { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class AddViewedPostingResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="AddViewedPostingResponse"/> class.
		/// </summary>
		public AddViewedPostingResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="AddViewedPostingResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public AddViewedPostingResponse(AddViewedPostingRequest request) : base(request) { }

		[DataMember]
		public ViewedPostingDto ViewedPosting { get; set; }
	}
}
