﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Models.Assist;

#endregion

namespace Focus.Core.Messages.PostingService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetComparePostingToResumeModalModelRequest : ServiceRequest
	{
		/// <summary>
		/// Gets or sets the posting id.
		/// </summary>
		/// <value>The posting id.</value>
		[DataMember]
		public string LensPostingId { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [do live search].
		/// </summary>
		/// <value><c>true</c> if [do live search]; otherwise, <c>false</c>.</value>
		[DataMember]
		public bool DoLiveSearch { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetComparePostingToResumeModalModelResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetComparePostingToResumeModalModelResponse"/> class.
		/// </summary>
		public GetComparePostingToResumeModalModelResponse() : base() {}

		/// <summary>
		/// Initializes a new instance of the <see cref="GetComparePostingToResumeModalModelResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GetComparePostingToResumeModalModelResponse(GetComparePostingToResumeModalModelRequest request) : base(request) { }

		/// <summary>
		/// Gets or sets the model.
		/// </summary>
		/// <value>The model.</value>
		[DataMember]
		public ComparePostingToResumeModalModel Model { get; set; }
	}
}
