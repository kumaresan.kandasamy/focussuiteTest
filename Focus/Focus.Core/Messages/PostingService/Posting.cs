﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.PostingService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class PostingRequest : ServiceRequest
	{
		[DataMember]
		public string LensPostingId { get; set; }

		[DataMember]
		public bool LogViewAction { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class PostingResponse : ServiceResponse
	{
		public PostingResponse() { }

		public PostingResponse(PostingRequest request) : base(request) { }

		[DataMember]
		public PostingEnvelope PostingDetails { get; set; }

		[DataMember]
    public long? PostingId { get; set; }

		[DataMember]
    public string PostingBGTOcc { get; set; }
	}
}
