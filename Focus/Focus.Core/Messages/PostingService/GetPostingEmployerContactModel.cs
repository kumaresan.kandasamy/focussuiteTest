﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Models.Assist;

#endregion

namespace Focus.Core.Messages.PostingService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetPostingEmployerContactModelRequest : ServiceRequest
	{
		/// <summary>
		/// Gets or sets the posting id.
		/// </summary>
		/// <value>The posting id.</value>
		[DataMember]
		public long PostingId { get; set; }

		/// <summary>
		/// Gets or sets the person id.
		/// </summary>
		/// <value>The person id.</value>
		[DataMember]
		public long? PersonId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetPostingEmployerContactModelResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetPostingEmployerContactModelResponse"/> class.
		/// </summary>
		public GetPostingEmployerContactModelResponse() : base() {}

		/// <summary>
		/// Initializes a new instance of the <see cref="GetPostingEmployerContactModelResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GetPostingEmployerContactModelResponse(GetPostingEmployerContactModelRequest request) : base(request) {}

		[DataMember]
		public PostingEmployerContactModel Model { get; set; }
	}
}
