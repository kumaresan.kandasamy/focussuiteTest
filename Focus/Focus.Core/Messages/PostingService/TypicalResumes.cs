﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.PostingService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class TypicalResumesRequest : ServiceRequest
	{
    [DataMember]
    public string LensPostingId { get; set; }
    
    [DataMember]
    public PostingEnvelope Posting { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class TypicalResumesResponse : ServiceResponse
	{
		public TypicalResumesResponse() { }

		public TypicalResumesResponse(TypicalResumesRequest request) : base(request) { }

    [DataMember]
    public List<ResumeInfo> TypicalResumes { get; set; }
	}
}
