﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.DataImportService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobOrderImportRequest : ServiceRequest
  {
    [DataMember]
    public long JobOrderRequestId { get; set; }

    [DataMember]
    public long? JobId { get; set; }

    [DataMember]
    public bool IgnoreLens { get; set; }

    [DataMember]
    public bool IgnoreClient { get; set; }

    [DataMember]
    public string InitialLensPostingId { get; set; }

    [DataMember]
    public string OfficeMigrationId { get; set; }

    [DataMember]
    public JobDto SavedJobWithPostingHtml { get; set; }

		[DataMember]
		public PostingDto SavedSpideredJobWithPostingHtml { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobOrderImportResponse : ServiceResponse
  {
    public JobOrderImportResponse() { }

    public JobOrderImportResponse(JobOrderImportRequest request) : base(request) { }
  }
}
