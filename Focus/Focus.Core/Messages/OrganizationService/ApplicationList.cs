﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Models.Career;

#endregion


namespace Focus.Core.Messages.OrganizationService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ApplicationListRequest : ServiceRequest 
	{
		[DataMember]
		public int ReferralCount { get; set; }

		[DataMember]
		public DateTime FromDate { get; set; }

		[DataMember]
		public DateTime ToDate { get; set; }

		[DataMember]
		public ReferralStatuses? ReferralStatus { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ApplicationListResponse : ServiceResponse
	{
		public ApplicationListResponse() : base() {}

		public ApplicationListResponse(ApplicationListRequest request) : base(request) { }

		[DataMember]
		public List<ReferralPosting> ReferralPostings { get; set; }
	}
}
