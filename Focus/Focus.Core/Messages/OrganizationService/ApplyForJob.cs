﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.OrganizationService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ApplyForJobRequest : ServiceRequest
	{
		[DataMember]
		public bool ReviewApplication { get; set; }

		[DataMember]
		public long ResumeId { get; set; }

		[DataMember]
		public string LensPostingId { get; set; }

		[DataMember]
		public int? MatchingScore { get; set; }

		[DataMember]
		public string Notes { get; set; }

		[DataMember]
		public bool IsEligible { get; set; }

    [DataMember]
    public List<string> WaivedRequirements { get; set; }

		[DataMember]
		public bool IsMigratedReferral { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ApplyForJobResponse : ServiceResponse
	{
		public ApplyForJobResponse() { }

		public ApplyForJobResponse(ApplyForJobRequest request) : base(request) { }

    public long? ApplicationId { get; set; }
	}
}
