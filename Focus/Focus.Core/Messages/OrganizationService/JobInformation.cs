﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.OrganizationService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobInformationRequest : ServiceRequest
	{
		[DataMember]
		public string JobLensId { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobInformationResponse : ServiceResponse
	{
		public JobInformationResponse() : base () {}

		public JobInformationResponse(JobInformationRequest request) : base(request) {}

		[DataMember]
		public PostingEnvelope JobInformation { get; set; }
	}
}
