﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.Criteria.SavedSearch;

#endregion


namespace Focus.Core.Messages.SearchService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetCandidateSavedSearchRequest : ServiceRequest
	{
		/// <summary>
		/// Gets or sets the criteria.
		/// </summary>
		/// <value>The criteria.</value>
		[DataMember]
		public SavedSearchCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetCandidateSavedSearchResponse : ServiceResponse
	{
		private CandidateSearchCriteria _criteria;

		/// <summary>
		/// Initializes a new instance of the <see cref="GetCandidateSavedSearchResponse"/> class.
		/// </summary>
		public GetCandidateSavedSearchResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="GetCandidateSavedSearchResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GetCandidateSavedSearchResponse(GetCandidateSavedSearchRequest request) : base(request) { }

		[DataMember]
		public SavedSearchDto SavedSearch { get; set; }

		[DataMember]
		public List<SavedSearchDto> SavedSearches { get; set; }

    [DataMember]
    public PagedList<SavedSearchDto> SavedSearchesPaged { get; set; }

		/// <summary>
		/// Gets or sets the criteria.
		/// </summary>
		/// <value>The criteria.</value>
		[DataMember]
		public CandidateSearchCriteria Criteria
		{
			get
			{
				if (_criteria == null) _criteria = new CandidateSearchCriteria();
				_criteria = (CandidateSearchCriteria)SavedSearch.SearchCriteria.Deserialize(_criteria.GetType());

				return _criteria;
			}
      set { _criteria = value; }
		}
	}
}
