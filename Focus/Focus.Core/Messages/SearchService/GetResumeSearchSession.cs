﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.Views;

#endregion



namespace Focus.Core.Messages.SearchService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetResumeSearchSessionRequest : ServiceRequest
	{
		/// <summary>
		/// Gets or sets the search id.
		/// </summary>
		/// <value>The search id.</value>
		[DataMember]
		public Guid SearchId { get; set; }

		/// <summary>
		/// Gets or sets the star rating to filter on.
		/// </summary>
		/// <value>
		/// The star rating.
		/// </value>
		[DataMember]
		public int? StarRating { get; set; }

	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetResumeSearchSessionResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetResumeSearchSessionResponse"/> class.
		/// </summary>
		public GetResumeSearchSessionResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="GetResumeSearchSessionResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GetResumeSearchSessionResponse(GetResumeSearchSessionRequest request) : base(request) { }

		/// <summary>
		/// Gets or sets the resumes.
		/// </summary>
		/// <value>
		/// The resumes.
		/// </value>
		[DataMember]
		public List<ResumeView> Resumes { get; set; }
	}
}
