﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.SearchService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class LensSearchRequest : ServiceRequest
	{
    [DataMember]
    public SearchCriteria Criteria { get; set; }

    [DataMember]
    public DocumentType? DocumentType { get; set; }

    [DataMember]
    public int? MaximumDocumentCount { get; set; }

    [DataMember]
    public int? MinimumScore { get; set; }

		[DataMember]
		public bool ManualSearch { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class LensSearchResponse : ServiceResponse
	{
		public LensSearchResponse(){}

		public LensSearchResponse(LensSearchRequest request) : base(request) { }

    [DataMember]
    public int DocumentCount { get; set; }

    [DataMember]
    public List<ISearchDocument> SearchedDocuments { get; set; }

    [DataMember]
    public List<PostingSearchResultView> Postings { get; set; }
	}
}
