﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.SearchService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ClarifyWordRequest : ServiceRequest
	{
		/// <summary>
		/// Gets or sets the search id.
		/// </summary>
		/// <value>The search id.</value>
    [DataMember]
    public Guid SearchId { get; set; }

		/// <summary>
		/// Gets or sets the text.
		/// </summary>
		/// <value>The text.</value>
		[DataMember]
		public string Text { get; set; }

	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ClarifyWordResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ClarifyWordResponse"/> class.
		/// </summary>
		public ClarifyWordResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="ClarifyWordResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public ClarifyWordResponse(ClarifyWordRequest request) : base(request) { }

		[DataMember]
		public List<string> ClarifiedWords { get; set; }
	}
}
