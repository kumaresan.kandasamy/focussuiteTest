﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Criteria.SavedSearch;

#endregion

namespace Focus.Core.Messages.SearchService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetSavedSearchRequest : ServiceRequest
	{
		/// <summary>
		/// Gets or sets the criteria.
		/// </summary>
		/// <value>The criteria.</value>
		[DataMember]
		public SavedSearchCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetSavedSearchResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetSavedSearchResponse"/> class.
		/// </summary>
		public GetSavedSearchResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="GetSavedSearchResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GetSavedSearchResponse(GetSavedSearchRequest request) : base(request) { }

		///// <summary>
		///// Gets or sets the saved search.
		///// </summary>
		///// <value>The saved search.</value>
		//public SavedSearchView SavedSearch { get; set; }

		///// <summary>
		///// Gets or sets the saved searches.
		///// </summary>
		///// <value>The saved searches.</value>
		//public List<SavedSearchView> SavedSearches { get; set; }
	}
}
