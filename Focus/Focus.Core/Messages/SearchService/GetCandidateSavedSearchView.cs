﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.SavedSearch;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.SearchService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetCandidateSavedSearchViewRequest: ServiceRequest
	{
		/// <summary>
		/// Gets or sets the criteria.
		/// </summary>
		/// <value>The criteria.</value>
		[DataMember]
		public SavedSearchCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetCandidateSavedSearchViewResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetCandidateSavedSearchViewResponse"/> class.
		/// </summary>
		public GetCandidateSavedSearchViewResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="GetCandidateSavedSearchViewResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GetCandidateSavedSearchViewResponse(GetCandidateSavedSearchViewRequest request) : base(request) { }

		[DataMember]
		public List<SavedSearchView> SavedSearches { get; set; }
	}
}


