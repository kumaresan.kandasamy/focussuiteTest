﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Criteria.CandidateSearch;

#endregion

namespace Focus.Core.Messages.SearchService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveCandidateSavedSearchRequest : ServiceRequest
	{
		/// <summary>
		/// Gets or sets the criteria.
		/// </summary>
		/// <value>The criteria.</value>
    [DataMember]
    public CandidateSearchCriteria Criteria { get; set; }
		
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		/// <value>The id.</value>
		[DataMember]
		public long Id { get; set; }
		
		/// <summary>
		/// Gets or sets the name of the search.
		/// </summary>
		/// <value>The name of the search.</value>
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether talent alert required.
		/// </summary>
		/// <value><c>true</c> if [talent alert required]; otherwise, <c>false</c>.</value>
		[DataMember]
		public bool AlertEmailRequired { get; set; }

		/// <summary>
		/// Gets or sets the alert email frequency.
		/// </summary>
		/// <value>The alert email frequency.</value>
		[DataMember]
		public EmailAlertFrequencies AlertEmailFrequency { get; set; }

		/// <summary>
		/// Gets or sets the alert email format.
		/// </summary>
		/// <value>The alert email format.</value>
		[DataMember]
		public EmailFormats AlertEmailFormat { get; set; }

		/// <summary>
		/// Gets or sets the alert email address.
		/// </summary>
		/// <value>The alert email address.</value>
		[DataMember]
		public string AlertEmailAddress { get; set; }

		/// <summary>
		/// Gets or sets the type of the search.
		/// </summary>
		/// <value>The type of the search.</value>
		[DataMember]
		public SavedSearchTypes SearchType { get; set; }

    /// <summary>
    /// Whether to save for a user other than the logged on user
    /// </summary>
    [DataMember]
    public long? SaveForUserId { get; set; }

    /// <summary>
    /// Used by the migration process
    /// </summary>
    [DataMember]
    public string SavedSearchMigrationId { get; set; }

    /// <summary>
    /// Used by the migration process
    /// </summary>
    [DataMember]
    public string EmployeeMigrationId { get; set; }

    [DataMember]
    public string MigrationValidationMessage { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveCandidateSavedSearchResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SaveCandidateSavedSearchResponse"/> class.
		/// </summary>
		public SaveCandidateSavedSearchResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SaveCandidateSavedSearchResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SaveCandidateSavedSearchResponse(SaveCandidateSavedSearchRequest request) : base(request) { }

    /// <summary>
    /// The Id of the saved search
    /// </summary>
    public long SavedSearchId { get; set; }
	}
}
