#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.OnetTask;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.SearchService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class OnetTaskRequest : ServiceRequest
	{
		[DataMember]
		public OnetTaskCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class OnetTaskResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="OnetTaskResponse"/> class.
		/// </summary>
		public OnetTaskResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="OnetTaskResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public OnetTaskResponse(OnetTaskRequest request) : base(request) { }

		[DataMember]
		public List<OnetTaskView> OnetTasks { get; set; }
	}
}
