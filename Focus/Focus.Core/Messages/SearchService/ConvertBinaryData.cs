﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.SearchService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ConvertBinaryDataRequest: ServiceRequest
	{
		[DataMember]
		public Guid SearchId { get; set; }

		[DataMember]
		public byte[] BinaryFileData { get; set; }

		[DataMember]
		public string FileName { get; set; }

		[DataMember]
		public string ContentType { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ConvertBinaryDataResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ConvertBinaryDataResponse"/> class.
		/// </summary>
		public ConvertBinaryDataResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="ConvertBinaryDataResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public ConvertBinaryDataResponse(ConvertBinaryDataRequest request) : base(request) { }

		[DataMember]
		public string Text { get; set; }
	}
}
