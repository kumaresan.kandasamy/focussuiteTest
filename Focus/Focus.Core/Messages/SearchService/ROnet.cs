﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.Onet;
using Focus.Core.Criteria.ROnet;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.SearchService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ROnetRequest : ServiceRequest
  {
    [DataMember]
    public Guid SearchId { get; set; }

    /// <summary>
    /// Gets or sets the criteria.
    /// </summary>
    /// <value>The criteria.</value>
    [DataMember]
    public ROnetCodeCriteria ROnetCriteria { get; set; }

    /// <summary>
    /// Gets or sets the criteria.
    /// </summary>
    /// <value>The criteria.</value>
    [DataMember]
    public OnetCriteria Criteria { get; set; }
    
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ROnetResponse : ServiceResponse
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="ROnetResponse"/> class.
    /// </summary>
    public ROnetResponse() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="ROnetResponse"/> class.
    /// </summary>
    /// <param name="request">The request.</param>
    public ROnetResponse(ROnetRequest request) : base(request) { }

    /// <summary>
    /// Gets or sets the search id.
    /// </summary>
    [DataMember]
    public Guid SearchId { get; set; }

    /// <summary>
    /// Gets or sets the ROnets.
    /// </summary>
    [DataMember]
    public List<ROnetDetailsView> ROnets { get; set; }

    /// <summary>
    /// Gets or sets the ROnetId.
    /// </summary>
    [DataMember]
    public long? ROnetId { get; set; }

    /// <summary>
    /// Gets or sets the OnetId.
    /// </summary>
    [DataMember]
    public long? OnetId { get; set; }
  }
}
