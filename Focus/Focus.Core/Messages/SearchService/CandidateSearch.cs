﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;
using Focus.Core.Views;
using Focus.Core.Criteria.CandidateSearch;

#endregion

namespace Focus.Core.Messages.SearchService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CandidateSearchRequest : ServiceRequest
	{
		/// <summary>
		/// Gets or sets the search id.
		/// </summary>
		/// <value>The search id.</value>
    [DataMember]
    public Guid SearchId { get; set; }

		/// <summary>
		/// Gets or sets the criteria.
		/// </summary>
		/// <value>The criteria.</value>
		[DataMember]
		public CandidateSearchCriteria Criteria { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the search is for a count only. If it is it will not persist it to session and will not do some of the processing.
		/// </summary>
		/// <value><c>true</c> if [for count]; otherwise, <c>false</c>.</value>
		[DataMember]
		public bool ForCount { get; set; }
		
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CandidateSearchResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CandidateSearchResponse"/> class.
		/// </summary>
		public CandidateSearchResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="CandidateSearchResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public CandidateSearchResponse(CandidateSearchRequest request) : base(request) { }

		/// <summary>
		/// Gets or sets the candidates.
		/// </summary>
		/// <value>The candidates.</value>
		[DataMember]
		public PagedList<ResumeView> Candidates { get; set; }

		/// <summary>
		/// Gets or sets the search id.
		/// </summary>
		/// <value>The search id.</value>
		[DataMember]
		public Guid SearchId { get; set; }

		/// <summary>
		/// Gets or sets the highest star rating.
		/// </summary>
		/// <value>
		/// The highest star rating.
		/// </value>
		[DataMember]
		public int HighestStarRating { get; set; }
	}
}
