﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.Criteria.Report;
using Focus.Core.Models;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.ReportService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobSeekersDataTableRequest : ServiceRequest
  {
    [DataMember]
    public JobSeekersReportCriteria ReportCriteria { get; set; }

    [DataMember]
    public string ReportName { get; set; }

    [DataMember]
    public List<JobSeekersReportView> JobSeekers { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobSeekersDataTableResponse : ServiceResponse
  {
    public JobSeekersDataTableResponse(JobSeekersDataTableRequest request) : base(request) { }

    [DataMember]
    public ReportDataTableModel ReportData { get; set; }
  }
}
