﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.Criteria.Report;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.ReportService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobSeekersReportRequest : ServiceRequest
  {
    [DataMember]
    public JobSeekersReportCriteria ReportCriteria { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobSeekersReportResponse : ServiceResponse
  {
    public JobSeekersReportResponse(JobSeekersReportRequest request) : base(request) { }

    [DataMember]
    public PagedList<JobSeekersReportView> JobSeekers { get; set; }
  }
}
