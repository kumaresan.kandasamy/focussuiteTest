﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.ReportService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class LookupReportDataRequest : ServiceRequest
  {
    [DataMember]
    public ReportLookUpType LookUpType { get; set; }

    [DataMember]
    public string SearchString { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class LookupReportDataResponse : ServiceResponse
  {
    public LookupReportDataResponse(LookupReportDataRequest request) : base(request) { }

    [DataMember]
    public string[] LookupData { get; set; }
  }
}
