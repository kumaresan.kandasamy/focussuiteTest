﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

using Focus.Core.Criteria.Report;

#endregion

namespace Focus.Core.Messages.ReportService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetSavedReportRequest : ServiceRequest
  {
    [DataMember]
    public long ReportId { get; set; }

    [DataMember]
    public bool IsSessionReport { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetSavedReportResponse : ServiceResponse
  {
    public GetSavedReportResponse(GetSavedReportRequest request) : base(request) { }

    [DataMember]
    public IReportCriteria ReportCriteria { get; set; }
  }
}
