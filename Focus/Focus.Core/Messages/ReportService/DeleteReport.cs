﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.DataTransferObjects.Report;

#endregion

namespace Focus.Core.Messages.ReportService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class DeleteReportRequest : ServiceRequest
  {
    [DataMember]
    public long ReportId { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class DeleteReportResponse : ServiceResponse
  {
    public DeleteReportResponse(DeleteReportRequest request) : base(request) { }
  }
}
