﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.ReportService
{
	 [DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetStatisticsRequest : ServiceRequest
	{
		 [DataMember]
		 public List<StatisticType> StatisticTypes { get; set; }

		 [DataMember]
		 public DateTime StatisticDate { get; set; }
	}

	 [DataContract(Namespace = Constants.DataContractNamespace)]
	 public class GetStatisticsResponse : ServiceResponse
	 {
		 public GetStatisticsResponse(GetStatisticsRequest request) : base(request) { }

		 [DataMember]
		 public List<KeyValuePair<StatisticType, string>> Statistics { get; set; }
	 }
}
