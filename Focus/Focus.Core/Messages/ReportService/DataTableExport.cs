﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.Models;

#endregion

namespace Focus.Core.Messages.ReportService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class DataTableExportRequest : ServiceRequest
  {
    [DataMember]
    public ReportDataTableModel ReportData { get; set; }

    [DataMember]
    public List<string> CriteriaDisplay { get; set; }

    [DataMember]
    public ReportExportType ExportType { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class DataTableExportResponse : ServiceResponse
  {
    public DataTableExportResponse(DataTableExportRequest request) : base(request) { }

    [DataMember]
    public byte[] ExportBytes { get; set; }
  }
}
