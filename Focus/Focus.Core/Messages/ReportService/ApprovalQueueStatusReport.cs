﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.ReportService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ApprovalQueueStatusReportRequest : ServiceRequest
	{
		[DataMember]
		public long UserId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ApprovalQueueStatusReportResponse : ServiceResponse
	{
		public ApprovalQueueStatusReportResponse(){}

		public ApprovalQueueStatusReportResponse(ApprovalQueueStatusReportRequest request) : base(request){}

		[DataMember]
		public ApprovalQueueStatusReportView ReportView { get; set; }
	}
}
