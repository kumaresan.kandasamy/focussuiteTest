﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

using Focus.Core.Criteria.Report;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.ReportService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobOrdersReportRequest : ServiceRequest
  {
    [DataMember]
    public JobOrdersReportCriteria ReportCriteria { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobOrdersReportResponse : ServiceResponse
  {
    public JobOrdersReportResponse(JobOrdersReportRequest request) : base(request) { }

    [DataMember]
    public PagedList<JobOrdersReportView> JobOrders { get; set; }
  }
}
