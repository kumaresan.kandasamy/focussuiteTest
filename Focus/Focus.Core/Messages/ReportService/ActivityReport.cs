﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion


namespace Focus.Core.Messages.ReportService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ActivityReportRequest : ServiceRequest
	{
    [DataMember]
    public AsyncReportType ReportType { get; set; }
		[DataMember]
		public DateTime FromDate { get; set; }

		[DataMember]
		public DateTime ToDate { get; set; }

		[DataMember]
		public string Recipients { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ActivityReportResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ActivityReportResponse"/> class.
		/// </summary>
		public ActivityReportResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="ActivityReportResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public ActivityReportResponse(ActivityReportRequest request) : base(request) { }

		[DataMember]
		public List<ErrorTypes> Errors { get; set; }

		[DataMember]
		public List<string> InvalidEmails { get; set; }
	}
}
