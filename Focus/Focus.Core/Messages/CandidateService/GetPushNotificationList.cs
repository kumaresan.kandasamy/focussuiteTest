﻿
using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetPushNotificationListRequest : ServiceRequest
	{
		[DataMember]
		public string DeviceToken { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetPushNotificationListResponse : ServiceResponse
	{
		public GetPushNotificationListResponse(GetPushNotificationListRequest request) : base(request) { }

    [DataMember]
    public PagedList<PushNotificationDto> PagedPushNotifications { get; set; }

		[DataMember]
		public List<PushNotificationDto> PushNotifications { get; set; }
	}
	
}
