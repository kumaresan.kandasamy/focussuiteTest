﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.CandidateService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetApplicationRequest : ServiceRequest
  {
    [DataMember]
    public long? PersonId { get; set; }

    [DataMember]
    public List<long> PersonIds { get; set; }

    [DataMember]
    public long JobId { get; set; }

    [DataMember]
    public long? PostingId { get; set; }
  }
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetApplicationResponse : ServiceResponse
  {
    public GetApplicationResponse(GetApplicationRequest request) : base(request) { }

    [DataMember]
    public ApplicationDto Application { get; set; }

    [DataMember]
    public Dictionary<long, ApplicationDto> Applications { get; set; }
  }
}
