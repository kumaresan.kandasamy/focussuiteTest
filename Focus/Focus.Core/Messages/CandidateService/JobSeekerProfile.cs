﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.CandidateService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobSeekerProfileRequest : ServiceRequest
  {
    [DataMember]
    public long JobSeekerId { get; set; }

    [DataMember]
    public string CountryCode { get; set; }

    [DataMember]
    public bool ShowContactDetails { get; set; }
  }

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobSeekerProfileResponse : ServiceResponse
	{
		public JobSeekerProfileResponse(JobSeekerProfileRequest request) : base(request)
		{
		}

    [DataMember]
    public JobSeekerProfileViewDto JobSeekerProfileView { get; set; }

		// Profile Panel
    [DataMember]
    public string UnemploymentInfo { get; set; }

    [DataMember]
    public VeteranInfo VeteranInfo { get; set; }

    [DataMember]
    public DisabilityStatus DisabilityStatus { get; set; }

    [DataMember]
    public List<long?> DisabilityCategoryIds { get; set; }

    [DataMember]
    public long? DisabilityCategoryId { get; set; }

    [DataMember]
    public bool? IsMSFW { get; set; }

    [DataMember]
    public List<long> MSFWQuestions { get; set; }

    [DataMember]
    public string NativeLanguage { get; set; }

    [DataMember]
    public string CommonLanguage { get; set; }

    [DataMember]
    public bool LowLevelLiteracy { get; set; }

    [DataMember]
    public string PreferredLanguage { get; set; }

    [DataMember]
    public bool CulturalBarriers { get; set; }

    [DataMember]
    public string NoOfDependents { get; set; }

    [DataMember]
    public string EstMonthlyIncome { get; set; }

    [DataMember]
    public bool DisplacedHomemaker { get; set; }

    [DataMember]
    public bool SingleParent { get; set; }

    [DataMember]
    public bool LowIncomeStatus { get; set; }

    [DataMember]
    public bool HomelessWithoutShelter { get; set; }

    [DataMember]
    public bool HomelessWithShelter { get; set; }

    [DataMember]
    public bool RunawayYouth { get; set; }

    [DataMember]
    public bool ExOffender { get; set; }

    [DataMember]
    public long? ResumeId { get; set; }

		// Resume
    [DataMember]
    public int ResumeLength { get; set; }

    [DataMember]
    public int SkillsCodedFromResume { get; set; }

    [DataMember]
    public int NumberOfResumes { get; set; }

    [DataMember]
    public bool? ResumeIsSearchable { get; set; }

    [DataMember]
    public bool? WillingToWorkOvertime { get; set; }
    
    [DataMember]
    public bool? WillingToRelocate { get; set; }

		[DataMember]
		public int NumberOfIncompleteResumes { get; set; }

    [DataMember]
    public SchoolStatus? EnrollmentStatus { get; set; }

    [DataMember]
    public EducationLevel? EducationLevel { get; set; }

    [DataMember]
    public EmploymentStatus? EmploymentStatus { get; set; }

		// Survey responses
    [DataMember]
    public int MatchQualityDissatisfied { get; set; }

    [DataMember]
    public int MatchQualitySomewhatDissatisfied { get; set; }

    [DataMember]
    public int MatchQualitySomewhatSatisfied { get; set; }

    [DataMember]
    public int MatchQualitySatisfied { get; set; }

    [DataMember]
    public int NotReceiveUnexpectedMatches { get; set; }

    [DataMember]
    public int ReceivedUnexpectedResults { get; set; }

    [DataMember]
    public int NotEmployerInvitationsReceive { get; set; }

    [DataMember]
    public int ReceivedEmployerInvitations { get; set; }

		[DataMember]
		public List<long?> OfficeIds { get; set; }

    [DataMember]
    public Phone PrimaryPhone { get; set; }


		// REFERRAL OUTCOMES
		[DataMember]
		public int ApplicantsInterviewed { get; set; }

		[DataMember]
		public int ApplicantsFailedToShow { get; set; }

		[DataMember]
		public int ApplicantsInterviewDenied { get; set; }

		[DataMember]
		public int ApplicantsHired { get; set; }

		[DataMember]
		public int ApplicantsRejected { get; set; }

		[DataMember]
		public int ApplicantsRefused { get; set; }

		[DataMember]
		public int ApplicantsDidNotApply { get; set; }

		[DataMember]
		public int ApplicantsFailedToReportToJob { get; set; }

		[DataMember]
		public int ApplicantsFailedToRespondToInvitation { get; set; }

		[DataMember]
		public int ApplicantsFoundJobFromOtherSource { get; set; }

		[DataMember]
		public int ApplicantsJobAlreadyFilled { get; set; }

		[DataMember]
		public int ApplicantsNewApplicant { get; set; }

		[DataMember]
		public int ApplicantsNotQualified { get; set; }

		[DataMember]
		public int ApplicantsNotYetPlaced { get; set; }

		[DataMember]
		public int ApplicantsRecommended { get; set; }

		[DataMember]
		public int ApplicantsRefusedReferral { get; set; }

		[DataMember]
		public int ApplicantsUnderConsideration { get; set; }
	}
}
