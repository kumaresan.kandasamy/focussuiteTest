﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.JobSeeker;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobSeekerActivityViewRequest : ServiceRequest
	{
	  public JobSeekerActivityViewRequest()
	  {
	    GetStaffReferrals = true;
	    GetHighGrowthSectors = true;
	  }

		[DataMember]
		public JobSeekerCriteria Criteria { get; set; }

    [DataMember]
    public bool GetStaffReferrals { get; set; }

    [DataMember]
    public bool GetHighGrowthSectors { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobSeekerActivityViewResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CandidateApplicationResponse"/> class.
		/// </summary>
		public JobSeekerActivityViewResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="CandidateApplicationResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public JobSeekerActivityViewResponse(JobSeekerActivityViewRequest request) : base(request) { }

		[DataMember]
		public JobSeekerActivityView JobSeekerActivityView { get; set; }

		[DataMember]
		public PagedList<JobSeekerActivityView> JobSeekerActivityViewPaged { get; set; }

		[DataMember]
		public List<JobSeekerActivityView> JobSeekerActivityViewList { get; set; }
	}
}
