﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CandidateApplicationRequest : ServiceRequest
	{
		[DataMember]
		public long Id { get; set; }

		[DataMember]
		public bool AutoApproval { get; set; }

		[DataMember]
		public int? LockVersion { get; set; }

		[DataMember]
		public List<KeyValuePair<long, string>> ReferralStatusReasons { get; set; }
		
		[DataMember]
		public ApplicationOnHoldReasons? OnHoldReason { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CandidateApplicationResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CandidateApplicationResponse"/> class.
		/// </summary>
		public CandidateApplicationResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="CandidateApplicationResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public CandidateApplicationResponse(CandidateApplicationRequest request) : base(request) { }
	}
}
