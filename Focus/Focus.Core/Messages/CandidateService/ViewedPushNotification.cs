﻿
using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ViewedPushNotificationRequest : ServiceRequest
	{
		[DataMember]
		public long Id { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ViewedPushNotificationResponse : ServiceResponse
	{
		public ViewedPushNotificationResponse(ViewedPushNotificationRequest request) : base(request) { }
	}
}
