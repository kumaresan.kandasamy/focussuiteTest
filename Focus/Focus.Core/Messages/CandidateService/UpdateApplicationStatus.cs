﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateApplicationStatusRequest :ServiceRequest
	{
		[DataMember]
		public long? ApplicationId { get; set; }

		[DataMember]
		public long? PersonId { get; set; }

		[DataMember]
		public long JobId { get; set; }

		[DataMember]
		public ApplicationStatusTypes NewStatus { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateApplicationStatusResponse : ServiceResponse
	{
		public UpdateApplicationStatusResponse(){}

		public UpdateApplicationStatusResponse(UpdateApplicationStatusRequest request) : base(request){}
	}
}
