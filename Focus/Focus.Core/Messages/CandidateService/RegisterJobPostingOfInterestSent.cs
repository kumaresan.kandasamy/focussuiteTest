﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.CandidateService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class RegisterJobPostingOfInterestSentRequest: ServiceRequest
  {
    [DataMember]
    public string LensPostingId { get; set; }

    [DataMember]
    public long CandidatePersonId { get; set; }

		[DataMember]
		public int Score { get; set; }

		[DataMember]
		public bool IsRecommendation { get; set; }

    [DataMember]
    public bool QueueRecommendation { get; set; }

    [DataMember]
    public long? CreatedBy { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class RegisterJobPostingOfInterestSentResponse : ServiceResponse
  {
    public RegisterJobPostingOfInterestSentResponse(){}

    public RegisterJobPostingOfInterestSentResponse(RegisterJobPostingOfInterestSentRequest request) : base(request) { }
  }
}
