﻿
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetPushNotificationRequest : ServiceRequest
	{
		[DataMember]
		public long Id { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetPushNotificationResponse : ServiceResponse
	{
		public GetPushNotificationResponse(GetPushNotificationRequest request) : base(request) { }

		[DataMember]
		public PushNotificationDto PushNotification { get; set; }
	}
}
