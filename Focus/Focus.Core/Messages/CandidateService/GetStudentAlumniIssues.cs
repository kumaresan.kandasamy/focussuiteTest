﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.JobSeeker;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetStudentAlumniIssuesRequest: ServiceRequest
	{
		[DataMember]
		public JobSeekerCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetStudentAlumniIssuesResponse: ServiceResponse
	{
		public GetStudentAlumniIssuesResponse() { }

		public GetStudentAlumniIssuesResponse(GetStudentAlumniIssuesRequest request) : base(request) { }

    [DataMember]
		public PagedList<StudentAlumniIssueViewDto> StudentAlumniIssuesPaged { get; set; }
    [DataMember]
		public List<StudentAlumniIssueViewDto> StudentAlumniIssues { get; set; }
    [DataMember]
		public StudentAlumniIssueViewDto StudentsAlumniIssues { get; set; }
	}
}
