﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class RemoveJobSeekersFromListRequest : ServiceRequest
	{
		// Candidate / Job Seeker details
		[DataMember]
		public JobSeekerView[] JobSeekers { get; set; }

		// List details
		[DataMember]
		public long ListId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class RemoveJobSeekersFromListResponse : ServiceResponse
	{
		public RemoveJobSeekersFromListResponse(){}

		public RemoveJobSeekersFromListResponse(RemoveJobSeekersFromListRequest request) : base(request){}
	}
}
