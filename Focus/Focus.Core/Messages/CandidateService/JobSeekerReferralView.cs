﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobSeekerReferralViewRequest : ServiceRequest
	{
		[DataMember]
		public JobSeekerReferralCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobSeekerReferralViewResponse : ServiceResponse
	{
		public JobSeekerReferralViewResponse(){}

		public JobSeekerReferralViewResponse(JobSeekerReferralViewRequest request) : base(request){}

		[DataMember]
		public PagedList<JobSeekerReferralAllStatusViewDto> ReferralViewsPaged { get; set; }

		[DataMember]
    public JobSeekerReferralAllStatusViewDto Referral { get; set; }
	}
}
