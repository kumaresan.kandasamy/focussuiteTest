﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ToggleCandidateFlagRequest : ServiceRequest
	{
		[DataMember]
		public long? PersonId { get; set; }

    [DataMember]
    public long? FlaggingPersonId { get; set; }

    // Remaining fields set up purely for importing data from other systems
    [DataMember]
    public string ToggleFlagMigrationId { get; set; }

    [DataMember]
    public string JobSeekerMigrationId { get; set; }

    [DataMember]
    public string EmployeeMigrationId { get; set; }

    [DataMember]
    public string MigrationValidationMessage { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ToggleCandidateFlagResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ToggleCandidateFlagResponse"/> class.
		/// </summary>
		public ToggleCandidateFlagResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="ToggleCandidateFlagResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public ToggleCandidateFlagResponse(ToggleCandidateFlagRequest request) : base(request) { }

    public long FlaggedApplicantsListId { get; set; }
	}
}
