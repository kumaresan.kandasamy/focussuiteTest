﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetResumeAsHtmlRequest : ServiceRequest
	{
		[DataMember]
		public long? PersonId { get; set; }

		[DataMember]
		public long? ResumeId { get; set; }

		[DataMember]
		public bool ReturnFullResume { get; set; }

    [DataMember]
    public bool LogView { get; set; }

		[DataMember]
		public bool IncludeAge { get; set; }

        [DataMember]
        public long ApplicationJobId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetResumeAsHtmlResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetResumeAsHtmlResponse"/> class.
		/// </summary>
		public GetResumeAsHtmlResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="GetResumeAsHtmlResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GetResumeAsHtmlResponse(GetResumeAsHtmlRequest request) : base(request) { }

		[DataMember]
		public string ResumeHtml { get; set; }
	}
}
