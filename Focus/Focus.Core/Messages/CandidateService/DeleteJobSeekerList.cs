﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class DeleteJobSeekerListRequest : ServiceRequest
	{
		[DataMember]
		public long ListId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class DeleteJobSeekerListResponse : ServiceResponse
	{
		public DeleteJobSeekerListResponse(){}

		public DeleteJobSeekerListResponse(DeleteJobSeekerListRequest request) : base(request) { }
	}
}
