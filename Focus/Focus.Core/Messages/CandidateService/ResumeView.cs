﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ResumeViewRequest : ServiceRequest
	{
		[DataMember]
		public ResumeViewCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ResumeViewResponse : ServiceResponse
	{
		public ResumeViewResponse() {}

		public ResumeViewResponse(ResumeViewRequest request) : base(request) {}

		[DataMember]
		public ResumeView ResumeView { get; set; }

		[DataMember]
		public PagedList<ResumeView> ResumeViewsPaged { get; set; }

		[DataMember]
		public List<ResumeView> ResumeViews { get; set; }
	}
}
