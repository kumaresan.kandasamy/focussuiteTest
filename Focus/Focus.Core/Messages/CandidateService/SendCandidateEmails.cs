﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using System.Collections.Generic;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SendCandidateEmailsRequest : ServiceRequest
	{
		[DataMember]
		public List<CandidateEmailView> Emails { get; set; }

    [DataMember]
    public bool DetectUrl { get; set; }

		[DataMember]
		public bool SendCopy { get; set; }

		[DataMember]
		public EmailFooterTypes FooterType { get; set; }

		[DataMember]
		public string FooterUrl { get; set; }

    [DataMember]
    public bool CheckSubscribed { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SendCandidateEmailsResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SendCandidateEmailsResponse"/> class.
		/// </summary>
		public SendCandidateEmailsResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SendCandidateEmailsResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SendCandidateEmailsResponse(SendCandidateEmailsRequest request) : base(request) { }

    public int UnsubscribedCount { get; set; }
	}
}
