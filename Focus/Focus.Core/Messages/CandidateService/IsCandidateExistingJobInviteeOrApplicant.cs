﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.Candidate;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class IsCandidateExistingInviteeOrApplicantRequest : ServiceRequest
	{
		[DataMember]
		public IsCandidateExistingInviteeOrApplicantCriteria IsCandidateInviteeCriteria { get; set; }
	}
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class IsCandidateExistingInviteeOrApplicantResponse : ServiceResponse
	{
		public IsCandidateExistingInviteeOrApplicantResponse(IsCandidateExistingInviteeOrApplicantRequest request) : base(request) { }

		[DataMember]
		public bool IsCandidateExistingJobInviteeOrApplicant { get; set; }

		[DataMember]
		public bool IsCandidateExistingApplicantToScreenedJobPendingApproval { get; set; }
	}
}
