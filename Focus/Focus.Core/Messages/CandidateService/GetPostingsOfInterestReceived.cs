﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.CandidateService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetPostingsOfInterestReceivedRequest: ServiceRequest
  {
		[DataMember]
		public long? PersonId { get; set; }

		[DataMember]
		public string LensPostingId { get; set; }

		[DataMember]
		public int NumberOfDays { get; set; }
  }

  public class GetPostingsOfInterestReceivedResponse: ServiceResponse
  {
    public GetPostingsOfInterestReceivedResponse(){}

    public GetPostingsOfInterestReceivedResponse(GetPostingsOfInterestReceivedRequest request) : base(request){}

    [DataMember]
    public List<JobPostingOfInterestSentViewDto> PostingsSent { get; set; }

		[DataMember]
		public JobPostingOfInterestSentViewDto PostingSent { get; set; }
  }
}
