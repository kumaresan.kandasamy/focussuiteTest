﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class AssignJobSeekerToAdminRequest : ServiceRequest
	{
		[DataMember]
		public string ClientId { get; set; }

		[DataMember]
		public string UserName { get; set; }

		[DataMember]
		public string FirstName { get; set; }

		[DataMember]
		public string LastName { get; set; }

		[DataMember]
		public string PhoneNumber { get; set; }

		[DataMember]
		public string EmailAddress { get; set; }

		[DataMember]
		public long AdminId { get; set; }

		[DataMember]
		public string ExternalAdminId { get; set; }

		[DataMember]
		public long PersonId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class AssignJobSeekerToAdminResponse : ServiceResponse
	{
		public AssignJobSeekerToAdminResponse()
		{}

		public AssignJobSeekerToAdminResponse(AssignJobSeekerToAdminRequest request) : base(request)
		{}
	}
}
