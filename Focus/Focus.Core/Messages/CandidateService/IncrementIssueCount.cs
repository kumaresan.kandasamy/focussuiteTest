﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.CandidateService
{
  class IncrementIssueCount
  {
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class IncrementIssueCountRequest : ServiceRequest
  {
    [DataMember]
    public long CandidateId { get; set; }

    //public JobSeekerIssueType Issue { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class IncrementIssueCountResponse : ServiceResponse
  {
    public IncrementIssueCountResponse() { }

    public IncrementIssueCountResponse(IncrementIssueCountRequest request) : base(request) { }
  }
}
