﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetCandidateSystemDefaultsRequest : ServiceRequest
	{
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetCandidateSystemDefaultsResponse : ServiceResponse
	{
		public GetCandidateSystemDefaultsResponse(){}

		public GetCandidateSystemDefaultsResponse(GetCandidateSystemDefaultsRequest request) : base(request){}

		[DataMember]
		public CandidateSystemDefaultsView SystemDefaults { get; set; }
	}
}
