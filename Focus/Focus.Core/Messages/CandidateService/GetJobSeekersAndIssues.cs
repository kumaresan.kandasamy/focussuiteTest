﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.JobSeeker;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models;

#endregion

namespace Focus.Core.Messages.CandidateService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetCandidatesAndIssuesRequest : ServiceRequest
  {
    [DataMember]
    public JobSeekerCriteria Criteria { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetCanididatesAndIssuesResponse : ServiceResponse
  {
    public GetCanididatesAndIssuesResponse() { }

    public GetCanididatesAndIssuesResponse(GetCandidatesAndIssuesRequest request) : base(request) { }

    [DataMember]
		public PagedList<JobSeekerDashboardModel> CandidatesAndIssuesPaged { get; set; }
    [DataMember]
    public List<CandidateIssueViewDto> CandidatesAndIssues { get; set; }
    [DataMember]
    public CandidateIssueViewDto CandidateAndIssues { get; set; }
  }
}
