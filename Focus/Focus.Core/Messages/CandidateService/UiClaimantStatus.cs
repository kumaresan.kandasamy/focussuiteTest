﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UiClaimantStatusRequest : ServiceRequest
	{
	  public UiClaimantStatusRequest()
	  {
	    CheckExternalSystem = true;
	  }

		[DataMember]
		public long PersonId { get; set; }

    [DataMember]
    public bool CheckExternalSystem { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UiClaimantStatusResponse : ServiceResponse
	{
		public UiClaimantStatusResponse() { }

		public UiClaimantStatusResponse(UiClaimantStatusRequest request) : base(request) { }

		[DataMember]
		public bool IsUiClaimant { get; set; }
	}
}
