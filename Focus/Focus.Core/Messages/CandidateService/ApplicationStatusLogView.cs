﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.EmployerService;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ApplicationStatusLogViewRequest : ServiceRequest
	{
		[DataMember]
		public ApplicationStatusLogViewCriteria Criteria { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ApplicationStatusLogViewResponse : ServiceResponse
	{
		public ApplicationStatusLogViewResponse() : base() {}

		public ApplicationStatusLogViewResponse(ApplicationStatusLogViewRequest request) : base(request) {}

		[DataMember]
		public ApplicationStatusLogViewDto ApplicationStatusLogView { get; set; }

		[DataMember]
		public PagedList<ApplicationStatusLogViewDto> ApplicationStatusLogViewsPaged { get; set; }

		[DataMember]
		public List<ApplicationStatusLogViewDto> ApplicationStatusLogViews { get; set; }

		[DataMember]
		public PagedList<EmployerJobReferralListItem> EmployerJobReferralListItems { get; set; }
	}
}
