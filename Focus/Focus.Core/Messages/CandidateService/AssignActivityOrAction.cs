﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class AssignActivityOrActionRequest : ServiceRequest
	{
		// Candidate / Job Seeker detail
		[DataMember]
		public long AdminId { get; set; }

		[DataMember]
		public long OfficeId { get; set; }

		[DataMember]
		public long EmployeeId { get; set; }

		[DataMember]
		public string ClientId { get; set; }

		[DataMember]
		public string UserName { get; set; }

		[DataMember]
		public string FirstName { get; set; }

		[DataMember]
		public string LastName { get; set; }

		[DataMember]
		public string PhoneNumber { get; set; }

		[DataMember]
		public string EmailAddress { get; set; }

		[DataMember]
		public List<JobSeekerView> Jobseekers { get; set; }
		
		// Activity details
		[DataMember]
		public long ActivityId { get; set; }

		[DataMember]
		public long ActionTypeId { get; set; }

		[DataMember]
		public long PersonId { get; set; }

    [DataMember]
    public bool IgnoreClient { get; set; }

    [DataMember]
    public DateTime? ActivityTime { get; set; }

    [DataMember]
    public long? AssigningUserId { get; set; }

		[DataMember]
		public bool IsSelfAssigned { get; set; }

    // Remaining fields set up purely for importing data from other systems
    [DataMember]
    public string ActivityMigrationId { get; set; }

    [DataMember]
    public string JobSeekerMigrationId { get; set; }

    [DataMember]
    public string StaffMigrationId { get; set; }

    [DataMember]
    public string StaffSecondaryMigrationId { get; set; }

    [DataMember]
    public string MigrationValidationMessage { get; set; }

		[DataMember]
		public DateTime? OverrideCreatedOn { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class AssignActivityOrActionResponse : ServiceResponse
	{
		public AssignActivityOrActionResponse() { }

		public AssignActivityOrActionResponse(AssignActivityOrActionRequest request) : base(request) { }

		public long? FocusId { get; set; }
	}
}
