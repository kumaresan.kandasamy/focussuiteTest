﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Focus.Core.Messages.CandidateService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class MarkApplicationViewedRequest : ServiceRequest
  {
    [DataMember]
    public long JobId { get; set; }

    [DataMember]
    public long CandidateId { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class MarkApplicationViewedResponse : ServiceResponse
  {
    public MarkApplicationViewedResponse(){}

    public MarkApplicationViewedResponse(MarkApplicationViewedRequest request) : base (request){}
  }
}
