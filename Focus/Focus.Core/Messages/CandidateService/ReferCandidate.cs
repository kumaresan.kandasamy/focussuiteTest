﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.CandidateService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ReferCandidateRequest : ServiceRequest
  {
    [DataMember]
    public long JobId { get; set; }

		[DataMember]
		public long PersonId { get; set; }

		[DataMember]
		public int ApplicationScore { get; set; }

    [DataMember]
    public List<string> WaivedRequirements { get; set; }

    [DataMember]
    public bool StaffReferral { get; set; } // Referral initiated by AssistUser, not Talent or Career user

    [DataMember]
    public ApplicationStatusTypes? InitialApplicationStatus { get; set; }

    // Remaining fields set up purely for importing data from other systems
    [DataMember]
    public DateTime? DateReferred { get; set; }

    [DataMember]
    public DateTime? DateApplied { get; set; }

    [DataMember]
    public string ReferralMigrationId { get; set; }

    [DataMember]
    public string PostingMigrationId { get; set; }

    [DataMember]
    public string JobSeekerMigrationId { get; set; }

    [DataMember]
    public string MigrationValidationMessage { get; set; }

		[DataMember]
		public string MigrationStaffId { get; set; }

		[DataMember]
		public long? UserId { get; set; }

		[DataMember]
		public string JobMigrationId { get; set; }

		[DataMember]
		public int ReferralId { get; set; }

		[DataMember]
		public string LensPostingId { get; set; }

		[DataMember]
		public long ResumeMigrationId { get; set; }

		[DataMember]
		public int ReferralStatus { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ReferCandidateResponse : ServiceResponse
  {
    public ReferCandidateResponse() { }

    public ReferCandidateResponse(ReferCandidateRequest request) : base(request) { }

    [DataMember]
    public bool ApplicationQueued { get; set; }

    [DataMember]
    public long ApplicationId { get; set; }
  }
}
