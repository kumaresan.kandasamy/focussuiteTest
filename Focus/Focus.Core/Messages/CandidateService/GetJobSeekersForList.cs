﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.Candidate;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetJobSeekersForListRequest : ServiceRequest
	{
		[DataMember]
		public JobSeekerListCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetJobSeekersForListResponse : ServiceResponse
	{
		public GetJobSeekersForListResponse() { }

		public GetJobSeekersForListResponse(GetJobSeekersForListRequest request) : base(request) { }

    [DataMember]
    public PagedList<JobSeekerView> PagedJobSeekers { get; set; }

		[DataMember]
		public List<JobSeekerView> JobSeekers { get; set; }
	}
}
