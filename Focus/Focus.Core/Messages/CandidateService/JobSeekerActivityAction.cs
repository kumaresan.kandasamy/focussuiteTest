﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.JobSeeker;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.CandidateService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobSeekerActivityActionViewRequest : ServiceRequest
	{
		[DataMember]
		public JobSeekerCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobSeekerActivityActionViewResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CandidateApplicationResponse"/> class.
		/// </summary>
		public JobSeekerActivityActionViewResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="CandidateApplicationResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public JobSeekerActivityActionViewResponse(JobSeekerActivityActionViewRequest request) : base(request) { }

		[DataMember]
		public JobSeekerActivityActionViewDto JobSeekerActivityActionView { get; set; }

		[DataMember]
		public PagedList<JobSeekerActivityActionViewDto> JobSeekerActivityActionViewPaged { get; set; }

		[DataMember]
		public List<JobSeekerActivityActionViewDto> JobSeekerActivityActionViewList { get; set; }

		[DataMember]
		public List<UserView> JobSeekerActivityActionViewUserList { get; set; }
	}
}
