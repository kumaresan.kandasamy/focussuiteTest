﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.OccupationService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ResumeBuilderQuestionsRequest : ServiceRequest
	{
    [DataMember]
    public string OnetCode { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ResumeBuilderQuestionsResponse : ServiceResponse
	{
    public ResumeBuilderQuestionsResponse(){ }
    public ResumeBuilderQuestionsResponse(ResumeBuilderQuestionsRequest request): base(request){ }

    [DataMember]
    public List<Question> Questions { get; set; }
	}
}
