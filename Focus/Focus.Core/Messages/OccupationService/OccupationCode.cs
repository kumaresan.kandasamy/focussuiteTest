﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.OccupationService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
	public class OccupationCodeRequest : ServiceRequest
	{
    [DataMember]
    public string OccupationCode { get; set; }

    [DataMember]
    public string OccupationTitle { get; set; }

    [DataMember]
    public long? OccupationId { get; set; }

    [DataMember]
    public int? SOCCount { get; set; }

    [DataMember]
    public bool IgnoreJobTaskAvailability { get; set; }
	}


  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class OccupationCodeResponse : ServiceResponse
	{
		public OccupationCodeResponse(OccupationCodeRequest request) : base(request)
		{
		}

    [DataMember]
    public List<OnetCode> OnetCodes { get; set; }
	}
}