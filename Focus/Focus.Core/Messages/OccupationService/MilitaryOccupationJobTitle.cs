﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion



namespace Focus.Core.Messages.OccupationService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class MilitaryOccupationJobTitleRequest : ServiceRequest
	{
    [DataMember]
    public string JobTitle { get; set; }

    [DataMember]
    public bool FullTitleSearch { get; set; }

		[DataMember]
		public int ListSize { get; set; }

    [DataMember]
    public long? MilitaryOccupationId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class MilitaryOccupationJobTitleResponse : ServiceResponse
	{
		public MilitaryOccupationJobTitleResponse() {}

		public MilitaryOccupationJobTitleResponse(MilitaryOccupationJobTitleRequest request) : base(request) {}

		[DataMember]
		public List<MilitaryOccupationJobTitleViewDto> JobTitles { get; set; }

    [DataMember]
    public MilitaryOccupationJobTitleViewDto JobTitle { get; set; }
	}
}
