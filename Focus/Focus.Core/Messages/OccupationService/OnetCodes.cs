﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.OccupationService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class OnetCodesRequest : ServiceRequest
	{
    [DataMember]
    public string JobTitle { get; set; }
    
    [DataMember]
    public int Count { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class OnetCodesResponse : ServiceResponse
	{
		public OnetCodesResponse() { }
		public OnetCodesResponse(OnetCodesRequest request) : base(request) { }

    [DataMember]
    public List<string> OnetCodes { get; set; }
	}
}
