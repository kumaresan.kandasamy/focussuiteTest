﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.EventService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CleanOpenMatchesRequest : ServiceRequest
	{
		[DataMember]
		public long? JobId { get; set; }

		[DataMember]
		public long? PersonId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CleanOpenMatchesResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CleanOpenMatchesResponse"/> class.
		/// </summary>
		public CleanOpenMatchesResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="CleanOpenMatchesResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public CleanOpenMatchesResponse(CleanOpenMatchesRequest request) : base(request) { }

	}
}
