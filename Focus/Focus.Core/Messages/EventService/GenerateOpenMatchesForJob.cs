﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.EventService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GenerateOpenMatchesForJobRequest : ServiceRequest
	{
		[DataMember]
		public long JobId { get; set; }
		
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GenerateOpenMatchesForJobResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GenerateOpenMatchesForJobResponse"/> class.
		/// </summary>
		public GenerateOpenMatchesForJobResponse() { }


		/// <summary>
		/// Initializes a new instance of the <see cref="GenerateOpenMatchesForJobResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GenerateOpenMatchesForJobResponse(GenerateOpenMatchesForJobRequest request) : base(request) { }
		
	}
}
