﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.UtilityService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CanonRequest : ServiceRequest
	{
    [DataMember]
    public string TaggedDocument { get; set; }

    [DataMember]
    public DocumentType DocumentType { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CanonResponse : ServiceResponse
	{
		public CanonResponse(){}

		public CanonResponse(CanonRequest request) : base(request) { }

    [DataMember]
    public string CanonDocument { get; set; }
	}
}
