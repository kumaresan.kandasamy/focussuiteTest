﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.UtilityService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class BinaryDocumentRequest : ServiceRequest
	{
    [DataMember]
    public DocumentType DocumentType { get; set; }

    [DataMember]
    public string FileExtension { get; set; }
    
    [DataMember]
    public byte[] BinaryContent { get; set; }

    [DataMember]
    public bool Canonize { get; set; }

    [DataMember]
    public MimeDocumentType WithDocument { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class BinaryDocumentResponse : ServiceResponse
	{
		public BinaryDocumentResponse() { }

		public BinaryDocumentResponse(BinaryDocumentRequest request) : base(request) { }

    [DataMember]
    public string TaggedDocument { get; set; }

    [DataMember]
    public string Html { get; set; }
	}
}
