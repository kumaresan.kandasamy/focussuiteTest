﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Models;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.MigrationService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class RegistrationRequest : ServiceRequest
  {
    [DataMember]
    public string OperationMode { get; set; }

    [DataMember]
    public CustomerInfo CustomerInfo { get; set; }

    [DataMember]
    public UserProfile UserInfo { get; set; }
    //public bool? IsRQP { get; set; }
    //public SeekerProgramInfo ProgramInfo { get; set; }
    //public RQPApplicant RQPApplicant { get; set; }

    [DataMember]
    public bool? IsMigrated { get; set; }

    [DataMember]
    public bool? IsConsentGiven { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class RegistrationResponse : ServiceResponse
  {
    public UserContext UserContext { get; set; }

    [DataMember]
    public string VerificationCode { get; set; }

    [DataMember]
    public string VerificationLink { get; set; }
  }
}
