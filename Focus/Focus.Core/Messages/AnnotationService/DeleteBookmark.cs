﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AnnotationService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class DeleteBookmarkRequest : ServiceRequest
  {
    [DataMember]
    public long BookmarkId { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class DeleteBookmarkResponse : ServiceResponse
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="DeleteBookmarkResponse"/> class.
    /// </summary>
    public DeleteBookmarkResponse() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="DeleteBookmarkResponse"/> class.
    /// </summary>
    /// <param name="request">The request.</param>
    public DeleteBookmarkResponse(DeleteBookmarkRequest request) : base(request) { }
  }
}
