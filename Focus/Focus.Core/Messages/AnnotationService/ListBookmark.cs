﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Models.Career;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.AnnotationService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ListBookmarkRequest : ServiceRequest
	{
    [DataMember]
    public string LabelFilter { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ListBookmarkResponse : ServiceResponse
	{
		public ListBookmarkResponse() { }
		public ListBookmarkResponse(ListBookmarkRequest request) : base(request) { }

    [DataMember]
    public List<PostingBookmark> Bookmarks { get; set; }

    [DataMember]
    public List<LookupItemView> BookmarkLookupItems { get; set; }
	}
}
