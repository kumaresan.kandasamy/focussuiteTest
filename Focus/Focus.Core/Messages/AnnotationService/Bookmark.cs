﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.Criteria.Bookmark;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.AnnotationService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class BookmarkRequest : ServiceRequest
  {
    [DataMember]
    public BookmarkTypes Type { get; set; }

    [DataMember]
    public BookmarkCriteria Criteria { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class BookmarkResponse : ServiceResponse
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="BookmarkResponse"/> class.
    /// </summary>
    public BookmarkResponse() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="BookmarkResponse"/> class.
    /// </summary>
    /// <param name="request">The request.</param>
    public BookmarkResponse(BookmarkRequest request) : base(request) { }

    [DataMember]
    public BookmarkDto Bookmark { get; set; }

    [DataMember]
    public List<BookmarkDto> Bookmarks;
  }
}
