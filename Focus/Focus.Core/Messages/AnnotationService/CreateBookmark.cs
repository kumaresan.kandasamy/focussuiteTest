﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.AnnotationService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CreateBookmarkRequest : ServiceRequest
  {
    [DataMember]
    public BookmarkDto Bookmark { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CreateBookmarkResponse : ServiceResponse
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="CreateBookmarkResponse"/> class.
    /// </summary>
    public CreateBookmarkResponse() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="CreateBookmarkResponse"/> class.
    /// </summary>
    /// <param name="request">The request.</param>
    public CreateBookmarkResponse(CreateBookmarkRequest request) : base(request) { }
  }
}

