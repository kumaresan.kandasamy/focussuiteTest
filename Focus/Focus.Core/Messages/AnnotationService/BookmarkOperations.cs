﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AnnotationService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class BookmarkOperationsRequest : ServiceRequest
	{
    [DataMember]
    public string DocumentId { get; set; }

    [DataMember]
    public Operation Operation { get; set; }
    
    [DataMember]
    public int MatchingScore { get; set; }

    [DataMember]
    public string Tags { get; set; }

    [DataMember]
    public string Label { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class BookmarkOperationsResponse : ServiceResponse
	{
		public BookmarkOperationsResponse() { }
		public BookmarkOperationsResponse(BookmarkOperationsRequest request) : base(request) { }
	}
}
