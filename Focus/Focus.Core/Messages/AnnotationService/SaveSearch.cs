﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.AnnotationService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveSearchRequest : ServiceRequest
	{
    [DataMember]
    public long Id { get; set; }

    [DataMember]
    public string Name { get; set; }

    [DataMember]
    public SearchCriteria SearchCriteria { get; set; }

    [DataMember]
    public bool AlertEmailRequired { get; set; }

    [DataMember]
    public EmailAlertFrequencies AlertEmailFrequency { get; set; }

    [DataMember]
    public EmailFormats AlertEmailFormat { get; set; }

    [DataMember]
    public AlertStatus AlertEmailStatus { get; set; }

    [DataMember]
    public string AlertEmailAddress { get; set; }
    
    [DataMember]
    public SavedSearchTypes SearchType { get; set; }

    [DataMember]
    public long? SaveForUserId { get; set; }

    [DataMember]
    public string SavedSearchMigrationId { get; set; }

    [DataMember]
    public string JobSeekerMigrationId { get; set; }

    [DataMember]
    public string MigrationValidationMessage { get; set; }

    [DataMember]
    public DateTime? OverrideCreatedOn { get; set; }

    [DataMember]
    public DateTime? OverrideUpdatedOn { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveSearchResponse : ServiceResponse
	{
		public SaveSearchResponse() { }

		public SaveSearchResponse(SaveSearchRequest request) : base(request) { }

    [DataMember]
    public long SavedSearchId { get; set; }
  }
}
