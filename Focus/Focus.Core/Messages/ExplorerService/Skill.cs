﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.Skill;
using Focus.Core.Criteria.SkillReport;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Models;

#endregion

namespace Focus.Core.Messages.ExplorerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SkillRequest : ServiceRequest
	{
		[DataMember]
		public SkillReportCriteria Criteria { get; set; }

		[DataMember]
		public long SkillId { get; set; }

		[DataMember]
		public TermCriteria TermSearchCriteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SkillResponse : ServiceResponse
	{
		public SkillResponse() { }

		public SkillResponse(SkillRequest request) : base(request) { }

		[DataMember]
		public List<SkillModel> SkillReports { get; set; }

		[DataMember]
		public List<SkillCategorySkillViewDto> Skills { get; set; }

		[DataMember]
		public List<SkillsForJobsViewDto> SkillsForJobs { get; set; }

    [DataMember]
    public SkillDto Skill { get; set; }
	}
}
