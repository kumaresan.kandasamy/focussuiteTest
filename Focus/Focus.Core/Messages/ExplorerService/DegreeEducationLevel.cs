﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.DegreeEducationLevel;
using Focus.Core.Criteria.DegreeEducationLevelReport;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace Focus.Core.Messages.ExplorerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class DegreeEducationLevelRequest : ServiceRequest
	{
		[DataMember]
		public DegreeEducationLevelReportCriteria ReportCriteria { get; set; }

		[DataMember]
		public DegreeEducationLevelCriteria SearchCriteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class DegreeEducationLevelResponse : ServiceResponse
	{
		public DegreeEducationLevelResponse() { }

		public DegreeEducationLevelResponse(DegreeEducationLevelRequest request) : base(request) { }

		[DataMember]
		public List<DegreeAliasViewDto> DegreeAliases { get; set; }

		[DataMember]
		public List<JobDegreeEducationLevelReportViewDto> DegreeEducationLevelReports { get; set; }

		[DataMember]
		public DegreeEducationLevelViewDto DegreeEducationLevel { get; set; }

		[DataMember]
		public List<DegreeEducationLevelExtDto> DegreeEducationLevelExts { get; set; }
	}
}
