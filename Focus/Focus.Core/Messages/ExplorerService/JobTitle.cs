﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace Focus.Core.Messages.ExplorerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobTitleRequest : ServiceRequest
	{
		[DataMember]
		public long? JobTitleId { get; set; }

		[DataMember]
		public string SearchTerm { get; set; }

		[DataMember]
		public long JobId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobTitleResponse : ServiceResponse
	{
		public JobTitleResponse() { }

		public JobTitleResponse(JobTitleRequest request) : base(request) { }

		[DataMember]
		public JobTitleDto JobTitle { get; set; }

		[DataMember]
		public List<JobTitleDto> JobTitles { get; set; }
	}
}
