﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.Skill;
using Focus.Core.Models;

#endregion

namespace Focus.Core.Messages.ExplorerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class InternshipSkillRequest : ServiceRequest
	{
		[DataMember]
		public InternshipSkillCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class InternshipSkillResponse : ServiceResponse
	{
		public InternshipSkillResponse() { }

		public InternshipSkillResponse(InternshipSkillRequest request) : base(request) { }

		[DataMember]
		public List<SkillModel> Skills { get; set; }
	}
}
