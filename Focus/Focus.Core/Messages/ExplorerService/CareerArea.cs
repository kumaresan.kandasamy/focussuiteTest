﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace Focus.Core.Messages.ExplorerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CareerAreaRequest : ServiceRequest
	{
    [DataMember]
    public long? CareerAreaId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CareerAreaResponse : ServiceResponse
	{
		public CareerAreaResponse() { }

		public CareerAreaResponse(CareerAreaRequest request) : base(request) { }

    [DataMember]
    public CareerAreaDto CareerArea { get; set; }

		[DataMember]
		public List<CareerAreaDto> CareerAreas { get; set; }
	}
}
