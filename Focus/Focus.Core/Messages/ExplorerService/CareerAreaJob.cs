﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.ExplorerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CareerAreaJobRequest : ServiceRequest
	{
		[DataMember]
		public long CareerAreaId { get; set; }

    [DataMember]
    public bool FilterMappedROnets { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CareerAreaJobResponse : ServiceResponse
	{
		public CareerAreaJobResponse() { }

		public CareerAreaJobResponse(CareerAreaJobRequest request) : base(request) { }

		[DataMember]
		public List<CareerAreaJobView> CareerAreaJobs { get; set; }
	}
}
