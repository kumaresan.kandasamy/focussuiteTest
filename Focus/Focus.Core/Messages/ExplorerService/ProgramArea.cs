﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.Explorer;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace Focus.Core.Messages.ExplorerService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ProgramAreaRequest : ServiceRequest
  {
    [DataMember]
    public long? ProgramAreaId { get; set; }

    [DataMember]
    public ExplorerCriteria Criteria { get; set; }

    [DataMember]
    public bool GetEducationLevels { get; set; }

    [DataMember]
    public long ROnetId { get; set; }

    [DataMember]
    public DetailedDegreeLevels? DetailedDegreeLevel { get; set; }

    [DataMember]
    public bool? CheckDegreeLevelByJob { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ProgramAreaResponse : ServiceResponse
  {
    public ProgramAreaResponse() { }

    public ProgramAreaResponse(ProgramAreaRequest request) : base(request) { }

    [DataMember]
    public ProgramAreaDto ProgramArea { get; set; }

    [DataMember]
    public List<ProgramAreaDto> ProgramAreas { get; set; }

    [DataMember]
    public List<ProgramAreaDegreeViewDto> ProgramAreaDegrees { get; set; }

    [DataMember]
    public List<ProgramAreaDegreeEducationLevelViewDto> ProgramAreaDegreeEducationLevels { get; set; }
  }
}
