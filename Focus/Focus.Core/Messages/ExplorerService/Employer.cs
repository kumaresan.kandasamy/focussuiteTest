﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.EmployerReport;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace Focus.Core.Messages.ExplorerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployerRequest : ServiceRequest
	{
		[DataMember]
		public long StateAreaId { get; set; }

		[DataMember]
		public string SearchTerm { get; set; }

		[DataMember]
		public EmployerReportCriteria Criteria { get; set; }

		[DataMember]
		public long EmployerId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployerResponse : ServiceResponse
	{
		public EmployerResponse() { }

		public EmployerResponse(EmployerRequest request) : base(request) { }

		[DataMember]
		public List<EmployerDto> Employers { get; set; }

		[DataMember]
		public List<EmployerJobViewDto> EmployerReports { get; set; }

		[DataMember]
		public EmployerDto Employer { get; set; }
	}
}
