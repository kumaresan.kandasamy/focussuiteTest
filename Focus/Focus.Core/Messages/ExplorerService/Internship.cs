﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.InternshipReport;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace Focus.Core.Messages.ExplorerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class InternshipRequest : ServiceRequest
	{
		[DataMember]
		public InternshipReportCriteria Criteria { get; set; }

		[DataMember]
		public long InternshipCategoryId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class InternshipResponse : ServiceResponse
	{
		public InternshipResponse() { }

		public InternshipResponse(InternshipRequest request) : base(request) { }

		[DataMember]
		public List<InternshipReportViewDto> InternshipReports { get; set; }

		[DataMember]
		public InternshipCategoryDto Internship { get; set; }
	}
}
