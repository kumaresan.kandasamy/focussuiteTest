﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Criteria.Explorer;

#endregion

namespace Focus.Core.Messages.ExplorerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SendEmailRequest : ServiceRequest
	{
		[DataMember]
		public ExplorerCriteria ReportCriteria { get; set; }

		[DataMember]
		public string EmailAddresses { get; set; }

		[DataMember]
		public string EmailSubject { get; set; }

		[DataMember]
		public string EmailBody { get; set; }

		[DataMember]
		public string BookmarkUrl { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SendEmailResponse : ServiceResponse
	{
		public SendEmailResponse() { }

		public SendEmailResponse(SendEmailRequest request) : base(request) { }
	}
}
