﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Models;

#endregion

namespace Focus.Core.Messages.ExplorerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobCareerPathRequest : ServiceRequest
	{
		[DataMember]
		public CareerPathDirections CareerPathDirection { get; set; }

		[DataMember]
		public long JobId { get; set; }

    [DataMember]
    public long? HiringDemandStateAreaId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobCareerPathResponse : ServiceResponse
	{
		public JobCareerPathResponse() { }

		public JobCareerPathResponse(JobCareerPathRequest request) : base(request) { }

		[DataMember]
		public JobCareerPathModel CareerPaths { get; set; }
	}
}
