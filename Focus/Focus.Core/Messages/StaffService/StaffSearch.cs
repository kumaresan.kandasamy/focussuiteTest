﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.User;

#endregion

namespace Focus.Core.Messages.StaffService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class StaffSearchRequest : ServiceRequest
	{
		[DataMember]
		public StaffCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class StaffSearchResponse : ServiceResponse
	{
		public StaffSearchResponse(){}

		public StaffSearchResponse(StaffSearchRequest request) : base(request) { }

		[DataMember]
		public PagedList<StaffSearchViewDto> StaffSearchViewsPaged { get; set; }
	}
}
