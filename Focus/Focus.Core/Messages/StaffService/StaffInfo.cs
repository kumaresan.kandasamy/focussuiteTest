﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.EmployeeService;

#endregion

namespace Focus.Core.Messages.StaffService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class StaffInfoRequest : ServiceRequest
	{
		[DataMember]
		public long Id { get; set; }

	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class StaffInfoResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmployeeResponse"/> class.
		/// </summary>
		public StaffInfoResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="EmployeeResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public StaffInfoResponse(StaffInfoRequest request) : base(request) { }

		[DataMember]
		public string StaffName { get; set; }

		[DataMember]
		public PersonDto Person { get; set; }

    [DataMember]
    public List<PersonAddressDto> PersonAddresses { get; set; }

    [DataMember]
    public List<PhoneNumberDto> PhoneNumbers { get; set; }

		[DataMember]
		public int Logins7Days { get; set; }

		[DataMember]
		public DateTime? LastLogin { get; set; }

		[DataMember]
		public int ActivityReferrals7Days { get; set; }

		[DataMember]
		public int ActivityResumesCreated7Days { get; set; }

		[DataMember]
		public int ActivityResumesEdited7Days { get; set; }

		[DataMember]
		public int ActivityJobSeekersAssisted7Days { get; set; }

		[DataMember]
		public int ActivityJobSeekersUnsubscribed7Days { get; set; }

		[DataMember]
		public int ActivityEmployerAccountsCreated7Days { get; set; }

		[DataMember]
		public int ActivityEmployerAccountsAssisted7Days { get; set; }

		[DataMember]
		public int ActivityJobOrdersCreated7Days { get; set; }

		[DataMember]
		public int ActivityJobOrdersEdited7Days { get; set; }

		[DataMember]
		public int ActivityReferrals30Days { get; set; }

		[DataMember]
		public int ActivityResumesCreated30Days { get; set; }

		[DataMember]
		public int ActivityResumesEdited30Days { get; set; }

		[DataMember]
		public int ActivityJobSeekersAssisted30Days { get; set; }

		[DataMember]
		public int ActivityJobSeekersUnsubscribed30Days { get; set; }

		[DataMember]
		public int ActivityEmployerAccountsCreated30Days { get; set; }

		[DataMember]
		public int ActivityEmployerAccountsAssisted30Days { get; set; }

		[DataMember]
		public int ActivityJobOrdersCreated30Days { get; set; }

		[DataMember]
		public int ActivityJobOrdersEdited30Days { get; set; }
	}
}
