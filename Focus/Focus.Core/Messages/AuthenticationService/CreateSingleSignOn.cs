﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AuthenticationService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CreateSingleSignOnRequest : ServiceRequest
	{
		[DataMember]
		public long? AccountUserId { get; set; }

		[DataMember]
		public long? AccountEmployeeId { get; set; }

		[DataMember]
		public long? AccountPersonId { get; set; }

		[DataMember]
		public string UserName { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CreateSingleSignOnResponse : ServiceResponse
	{
		public CreateSingleSignOnResponse() : base(){}

		public CreateSingleSignOnResponse(CreateSingleSignOnRequest request) : base(request){}

		[DataMember]
		public Guid SingleSignOnKey { get; set; }
	}
}
