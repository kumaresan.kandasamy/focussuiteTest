﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AuthenticationService
{
	/// <summary>
	/// Respresents a session request message from client to the service.
	/// </summary>
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SessionRequest : ServiceRequest
	{
		// Nothing 
	}

	/// <summary>
	/// Represents a session response message from the service to client.
	/// </summary>
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SessionResponse : ServiceResponse
	{
		/// <summary>
		/// Default Constructor for SessionResponse. 
		/// </summary>
		public SessionResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SessionResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SessionResponse(SessionRequest request) : base(request) { }

		/// <summary>
		/// Gets or sets the access token.
		/// </summary>
		[DataMember]
		public Guid SessionId { get; set; }
	}
}
