﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.AuthenticationService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ExternalAuthenticationRequest : ServiceRequest
  {
    [DataMember]
    public Dictionary<string, string> AuthenticationDetails { get; set; }

    [DataMember]
    public string SocialSecurityNumber { get; set; }

    [DataMember]
    public string ExternalId { get; set; }

    [DataMember]
    public bool CheckFocus { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ExternalAuthenticationResponse : ServiceResponse
  {
    public ExternalAuthenticationResponse() : base() { }

    public ExternalAuthenticationResponse(ExternalAuthenticationRequest request) : base(request) { }

    [DataMember]
		public ValidatedUserView ExternalsDetails { get; set; }
  }
}
