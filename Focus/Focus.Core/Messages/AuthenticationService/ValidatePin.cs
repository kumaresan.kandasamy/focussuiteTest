﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AuthenticationService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ValidatePinRequest : ServiceRequest
	{
		[DataMember]
		public string Pin { get; set; }

		[DataMember]
		public string EmailAddress { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ValidatePinResponse : ServiceResponse
	{
		public ValidatePinResponse() : base() { }

		public ValidatePinResponse(ValidatePinRequest request) : base(request) { }
	}
}
