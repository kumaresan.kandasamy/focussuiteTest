﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AuthenticationService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class LogOutRequest : ServiceRequest
	{
		
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class LogOutResponse : ServiceResponse
	{
		/// <summary>
		/// Default Constructor for SaveCodeItemResponse. 
		/// </summary>
		public LogOutResponse() { }

		/// <summary>
		/// Overloaded Constructor for LogoutResponse. 
		/// Sets CorrelationId from incoming Request.
		/// </summary>
		/// <param name="request">The request.</param>
		public LogOutResponse(LogOutRequest request) : base(request) { }
	}
}
