﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;

using Focus.Core.Models.Career;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.AuthenticationService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class LogInRequest : ServiceRequest
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="LogInRequest"/> class.
		/// </summary>
		public LogInRequest()
		{
			Type = LoginType.Standard;
		}

		[DataMember]
		public LoginType Type { get; set; } 

		// Standard Authentication Properties

		[DataMember]
		public string UserName { get; set; }

		[DataMember]
		[ScriptIgnore]
		public string Password { get; set; }

		// oAuth Authentication Properties

		[DataMember]
		public string ExternalId { get; set; }

		[DataMember]
		public string ScreenName { get; set; }

		[DataMember]
		public string EmailAddress { get; set; }

		[DataMember]
		public string FirstName { get; set; }

		[DataMember]
		public string LastName { get; set; }

		// Additional Properties

		[DataMember]
		public FocusModules ModuleToLoginTo { get; set; }

		[DataMember]
		public string ValidationUrl { get; set; }

		[DataMember]
		public string CreateRoles { get; set; }

		[DataMember]
		public string UpdateRoles { get; set; }
		
		[DataMember]
		public bool AllowResetSessionId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
  public class LogInResponse : ServiceResponse, ICareerUserData
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="LogInResponse"/> class.
		/// </summary>
		public LogInResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="LogInResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public LogInResponse(LogInRequest request) : base(request) { }

		/// <summary>
		/// Gets or sets the user id.
		/// </summary>
		[DataMember]
		public long UserId { get; set; }

		/// <summary>
		/// Gets or sets the first name.
		/// </summary>
		[DataMember]
		public string FirstName { get; set; }

		/// <summary>
		/// Gets or sets the last name.
		/// </summary>
		[DataMember]
		public string LastName { get; set; }

		/// <summary>
		/// Gets or sets the user's screen name.
		/// </summary>
		/// <value>The screen name.</value>
		[DataMember]
		public string ScreenName { get; set; }

		/// <summary>
		/// Gets or sets the email address.
		/// </summary>
		[DataMember]
		public string EmailAddress { get; set; }

		/// <summary>
		/// Gets or sets the person id.
		/// </summary>
		/// <value>The person id.</value>
		[DataMember]
		public long PersonId { get; set; }

		/// <summary>
		/// Gets or sets the employer id.
		/// </summary>
		/// <value>The employer id.</value>
		[DataMember]
		public long? EmployeeId { get; set; }

		/// <summary>
		/// Gets or sets the employer id.
		/// </summary>
		/// <value>The employer id.</value>
		[DataMember]
		public long? EmployerId { get; set; }

		/// <summary>
		/// Gets or sets the external user id.
		/// </summary>
		[DataMember]
		public string ExternalUserId { get; set; }

		/// <summary>
		/// Gets or sets the name of the external user.
		/// </summary>
		[DataMember]
		public string ExternalUserName { get; set; }

		/// <summary>
		/// Gets or sets the external password.
		/// </summary>
		[DataMember]
		public string ExternalPassword { get; set; }

		/// <summary>
		/// Gets or sets the external office id.
		/// </summary>
		[DataMember]
		public string ExternalOfficeId { get; set; }
		
    /// <summary>
    /// Gets or sets the last login.
    /// </summary>
    [DataMember]
    public DateTime? LastLoggedInOn { get; set; }

		/// <summary>
		/// Gets or sets the default resume id.
		/// </summary>
		[DataMember]
		public long? DefaultResumeId { get; set; }

		/// <summary>
		/// Gets or sets the default resume completion status.
		/// </summary>
		/// <value>The default resume completion status.</value>
		public ResumeCompletionStatuses DefaultResumeCompletionStatus { get; set; }

		[DataMember]
    public UserProfile Profile { get; set; }

		/// <summary>
		/// Gets or sets the name of the user.
		/// </summary>
		[DataMember]
		public string UserName { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is migrated.
		/// </summary>
		[DataMember]
		public bool IsMigrated { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether this user exists on an external system
    /// </summary>
    [DataMember]
    public bool IsIntegrationUser { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is enabled.
		/// </summary>
		[DataMember]
		public bool IsEnabled { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [regulations consent].
		/// </summary>
		[DataMember]
		public bool RegulationsConsent { get; set; }

		/// <summary>
		/// Gets or sets the program area id.
		/// </summary>
		[DataMember]
		public long? ProgramAreaId { get; set; }

		/// <summary>
		/// Gets or sets the degree id.
		/// </summary>
		[DataMember]
		public long? DegreeId { get; set; }

		/// <summary>
		/// Gets or sets the enrollment status.
		/// </summary>
		public SchoolStatus? EnrollmentStatus { get; set; }

    /// <summary>
    /// Gets or sets the campus location id
    /// </summary>
    [DataMember]
    public long? CampusId { get; set; }

    /// <summary>
    /// Gets or sets the details from any integration client
    /// </summary>
    [DataMember]
    public ValidatedUserView IntegrationDetails { get; set; }

    /// <summary>
    /// Whether the session id needs to be reset
    /// </summary>
    [DataMember]
    public Guid? ResetSessionId { get; set; }

		/// <summary>
		/// Whether the acount was previously disabled and has now been reactivated
		/// </summary>
		[DataMember]
		public bool AccountReactivateRequest { get; set; }
	}
}
