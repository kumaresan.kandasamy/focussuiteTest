﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AuthenticationService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SecurityQuestionRequest : ServiceRequest
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="LogInRequest"/> class.
		/// </summary>
		public SecurityQuestionRequest()
    {
    }

		[DataMember]
		public string UserName { get; set; }

		[DataMember]
		public string EmailAddress { get; set; }

		[DataMember]
		public FocusModules Module { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SecurityQuestionResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="LogInResponse"/> class.
		/// </summary>
		public SecurityQuestionResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SecurityQuestionResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SecurityQuestionResponse(SecurityQuestionRequest request) : base(request) { }

		/// <summary>/
		/// Gets or sets the security question.
		/// </summary>
		/// <value>
		/// The security question.
		/// </value>
		[DataMember]
		public string SecurityQuestion1 { get; set; }

		/// <summary>/
		/// Gets or sets the security question Id.
		/// </summary>
		/// <value>
		/// The security question Id.
		/// </value>
		[DataMember]
		public long? SecurityQuestionId1 { get; set; }

		/// <summary>
		/// Gets or sets the security answer.
		/// </summary>
		/// <value>
		/// The security answer.
		/// </value>
		[DataMember]
		public string SecurityAnswer1 { get; set; }

		/// <summary>
		/// Gets of sets the security question index number
		/// </summary>
		[DataMember]
		public int SecurityQuestionIndex1 { get; set; }

		/// <summary>
		/// Gets or sets the security question2.
		/// </summary>
		/// <value>
		/// The security question2.
		/// </value>
		[DataMember]
		public string SecurityQuestion2 { get; set; }

		/// <summary>/
		/// Gets or sets the security question2 Id.
		/// </summary>
		/// <value>
		/// The security question2 Id.
		/// </value>
		[DataMember]
		public long? SecurityQuestionId2 { get; set; }

		/// <summary>
		/// Gets or sets the security answer2.
		/// </summary>
		/// <value>
		/// The security answer2.
		/// </value>
		[DataMember]
		public string SecurityAnswer2 { get; set; }

		/// <summary>
		/// Gets of sets the security question index number
		/// </summary>
		[DataMember]
		public int SecurityQuestionIndex2 { get; set; }

	}
}
