﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.AuthenticationService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CareerUserDataRequest : ServiceRequest
  {

  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CareerUserDataResponse : ServiceResponse, ICareerUserData
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="CareerUserDataResponse"/> class.
    /// </summary>
    public CareerUserDataResponse() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="CareerUserDataResponse"/> class.
    /// </summary>
    /// <param name="request">The request.</param>
    public CareerUserDataResponse(CareerUserDataRequest request) : base(request) { }

		/// <summary>
		/// Gets or sets the default resume id.
		/// </summary>
		[DataMember]
		public long? DefaultResumeId { get; set; }

		/// <summary>
		/// Gets or sets the default resume completion status.
		/// </summary>
		/// <value>The default resume completion status.</value>
		[DataMember]
		public ResumeCompletionStatuses DefaultResumeCompletionStatus { get; set; }

    /// <summary>
    /// Gets or set the user profile
    /// </summary>
    [DataMember]
    public UserProfile Profile { get; set; }
  }

  public interface ICareerUserData
  {
		/// <summary>
		/// Gets or sets the default resume id.
		/// </summary>
		[DataMember]
		long? DefaultResumeId { get; set; }

		/// <summary>
		/// Gets or sets the default resume completion status.
		/// </summary>
		/// <value>The default resume completion status.</value>
		[DataMember]
		ResumeCompletionStatuses DefaultResumeCompletionStatus { get; set; }

    /// <summary>
    /// Gets or set the user profile
    /// </summary>
    [DataMember]
    UserProfile Profile { get; set; }
  }
}
