﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.AuthenticationService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ValidateSingleSignOnRequest : ServiceRequest
	{
		[DataMember]
		public Guid SingleSignOnKey { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ValidateSingleSignOnResponse : ServiceResponse, ICareerUserData
	{
		public ValidateSingleSignOnResponse() : base(){}

		public ValidateSingleSignOnResponse(ValidateSingleSignOnRequest request) : base(request){}

		/// <summary>
		/// Gets or sets the user id.
		/// </summary>
		[DataMember]
		public long UserId { get; set; }

		/// <summary>
		/// Gets or sets the actioner id.
		/// </summary>
		/// <value>The actioner id.</value>
		[DataMember]
		public long ActionerId { get; set; }

		/// <summary>
		/// Gets or sets the first name.
		/// </summary>
		[DataMember]
		public string FirstName { get; set; }

		/// <summary>
		/// Gets or sets the last name.
		/// </summary>
		[DataMember]
		public string LastName { get; set; }

		/// <summary>
		/// Gets or sets the email address.
		/// </summary>
		[DataMember]
		public string EmailAddress { get; set; }

		/// <summary>
		/// Gets or sets the person id.
		/// </summary>
		/// <value>The person id.</value>
		[DataMember]
		public long PersonId { get; set; }

		/// <summary>
		/// Gets or sets the employer id.
		/// </summary>
		/// <value>The employer id.</value>
		[DataMember]
		public long? EmployeeId { get; set; }

		/// <summary>
		/// Gets or sets the employer id.
		/// </summary>
		/// <value>The employer id.</value>
		[DataMember]
		public long? EmployerId { get; set; }

		/// <summary>
		/// Gets or sets the user's screen name.
		/// </summary>
		/// <value>The screen name.</value>
		[DataMember]
		public string ScreenName { get; set; }

		/// <summary>
		/// Gets or sets the default resume Id.
		/// </summary>
		/// <value>The default resume Id.</value>
		[DataMember]
		public long? DefaultResumeId { get; set; }

		/// <summary>
		/// Gets or sets the default resume completion status.
		/// </summary>
		/// <value>The default resume completion status.</value>
		public ResumeCompletionStatuses DefaultResumeCompletionStatus { get; set; }

		/// <summary>
		/// Gets or sets the profile.
		/// </summary>
		/// <value>The profile.</value>
		[DataMember]
		public UserProfile Profile { get; set; }

		/// <summary>
		/// Gets or sets the name of the user.
		/// </summary>
		[DataMember]
		public string UserName { get; set; }

		/// <summary>
		/// Gets or sets the program area id.
		/// </summary>
		[DataMember]
		public long? ProgramAreaId { get; set; }

		/// <summary>
		/// Gets or sets the degree id.
		/// </summary>
		/// <value> The degree id. </value>
		[DataMember]
		public long? DegreeId { get; set; }

		/// <summary>
		/// Gets or sets the enrollment status.
		/// </summary>
		/// <value> The enrollment status. </value>
		[DataMember]
		public SchoolStatus? EnrollmentStatus { get; set; }

    /// <summary>
    /// Gets or sets the campus location id
    /// </summary>
    [DataMember]
    public long? CampusId { get; set; }

		/// <summary>
		/// Gets or sets the external admin identifier.
		/// </summary>
		/// <value>
		/// The external admin identifier.
		/// </value>
		[DataMember]
		public string ExternalAdminId { get; set; }

		/// <summary>
		/// Gets or sets the external office identifier.
		/// </summary>
		/// <value>
		/// The external office identifier.
		/// </value>
		[DataMember]
		public string ExternalOfficeId { get; set; }

		/// <summary>
		/// Gets or sets the external password.
		/// </summary>
		/// <value>
		/// The external password.
		/// </value>
		[DataMember]
		public string ExternalPassword { get; set; }

		/// <summary>
		/// Gets or sets the external username.
		/// </summary>
		/// <value>
		/// The external username.
		/// </value>
		[DataMember]
		public string ExternalUsername { get; set; }
	}
}
