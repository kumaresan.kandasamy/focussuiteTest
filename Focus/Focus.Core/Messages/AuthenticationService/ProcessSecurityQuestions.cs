﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.AuthenticationService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ProcessSecurityQuestionsRequest : ServiceRequest
	{
		[DataMember]
		public string UserName { get; set; }

		[DataMember]
		public string EmailAddress { get; set; }

		[DataMember]
		public string ResetPasswordUrl { get; set; }

		[DataMember]
		public string PinRegistrationUrl { get; set; }

		[DataMember]
		public FocusModules Module { get; set; }

		[DataMember]
		public int SecurityQuestionIndex1 { get; set; }

		[DataMember]
		public string SecurityAnswer1 { get; set; }

		[DataMember]
		public int SecurityQuestionIndex2 { get; set; }

		[DataMember]
		public string SecurityAnswer2 { get; set; }

	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ProcessSecurityQuestionsResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessSecurityQuestionsResponse"/> class.
		/// </summary>
		public ProcessSecurityQuestionsResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessSecurityQuestionsResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public ProcessSecurityQuestionsResponse(ProcessSecurityQuestionsRequest request) : base(request) { }

		[DataMember]
		public bool AttemptsFailed { get; set; }

		[DataMember]
		public int AttemptsLeft { get; set; }
	}
}
