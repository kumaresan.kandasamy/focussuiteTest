﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.EmployeeService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployeeBusinessUnitViewRequest : ServiceRequest
	{
		[DataMember]
		public EmployeeBusinessUnitCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployeeBusinessUnitViewResponse : ServiceResponse
	{
		public EmployeeBusinessUnitViewResponse() {}

		public EmployeeBusinessUnitViewResponse(EmployeeBusinessUnitViewRequest request) : base(request) {}

		[DataMember]
		public PagedList<EmployeeBusinessUnitView> EmployeeBusinessUnitViewsPaged { get; set; }

		[DataMember]
		public List<EmployeeBusinessUnitView> EmployeeBusinessUnitViews { get; set; }

		[DataMember]
		public EmployeeBusinessUnitView EmployeeBusinessUnitView { get; set; }
	}
}
