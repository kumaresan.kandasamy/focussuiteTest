﻿using System;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusMigration;

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployerTradeNamesRequest : ServiceRequest
	{
		[DataMember]
		public string TradeNameId { get; set; }

		[DataMember]
		public string TradeName { get; set; }

		[DataMember]
		public int IsDefault { get; set; }

		[DataMember]
		public string EmployerLegalName { get; set; }

		[DataMember]
		public int EmployerId { get; set; }

		public FocusModules Module { get; set; }

		public string MigrationValidationMessage { get; set; }

		[DataMember]
		public string EmployeeMigrationId { get; set; }

		[DataMember]
		public string Fein { get; set; }

		public int SourceId { get; set; }

		public string Request { get; set; }

		public MigrationStatus Status { get; set; }

		public string ExternalId { get; set; }

		public string Message { get; set; }

		public MigrationEmployerRecordType RecordType { get; set; }

		public DateTime ProcessedBy { get; set; }

		public bool IsUpdate { get; set; }

		public ReportEntityType ReportEntityType { get { return ReportEntityType.EmployerTradeName; } }
		public long ReportEntityId { get { return Id; } }
		public long? ReportAdditionalEntityId { get { return null; } }
		public bool IsChild { get { return false; } }

		public long Id { get; set; }

		[DataMember]
		public string Line1 { get; set; }

		[DataMember]
		public string Line2 { get; set; }

		[DataMember]
		public string City { get; set; }

		[DataMember]
		public string County { get; set; }

		[DataMember]
		public string Country { get; set; }

		[DataMember]
		public string PostalCode { get; set; }

		[DataMember]
		public string State { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployerTradeNamesResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="Core.Messages.EmployerService.EmployerTradeNamesRequest"/> class.
		/// </summary>
		public EmployerTradeNamesResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="Core.Messages.EmployerService.EmployerTradeNamesRequest"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public EmployerTradeNamesResponse(Core.Messages.EmployerService.EmployerTradeNamesRequest request) : base(request) { }

		[DataMember]
		public EmployerTradeNameDto EmployerTradeName { get; set; }
	}

}
