﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Criteria.Employee;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.EmployeeService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployeeSearchRequest : ServiceRequest
	{
		[DataMember]
		public EmployeeCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployeeSearchResponse : ServiceResponse
	{
		public EmployeeSearchResponse(){}

		public EmployeeSearchResponse(EmployeeSearchRequest request) : base(request) {}

		[DataMember]
		public PagedList<EmployeeSearchResultView> EmployeeSearchViewsPaged { get; set; }
	}
}
