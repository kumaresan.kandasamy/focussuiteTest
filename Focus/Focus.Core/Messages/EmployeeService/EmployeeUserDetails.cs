﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployeeService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployeeUserDetailsRequest : ServiceRequest
	{
		[DataMember]
		public long? EmployeeId;
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployeeUserDetailsResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmployeeUserDetailsResponse"/> class.
		/// </summary>
		public EmployeeUserDetailsResponse() {}

		/// <summary>
		/// Initializes a new instance of the <see cref="EmployeeUserDetailsResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public EmployeeUserDetailsResponse(EmployeeUserDetailsRequest request) : base(request) {}

		[DataMember]
		public long UserId { get; set; }

		[DataMember]
		public PersonDto PersonDetails { get; set; }

		[DataMember]
		public PersonAddressDto AddressDetails { get; set; }

		[DataMember]
		public PhoneNumberDto PrimaryPhoneNumber { get; set; }

		[DataMember]
		public PhoneNumberDto AlternatePhoneNumber1 { get; set; }

		[DataMember]
		public PhoneNumberDto AlternatePhoneNumber2 { get; set; }
	}
}
