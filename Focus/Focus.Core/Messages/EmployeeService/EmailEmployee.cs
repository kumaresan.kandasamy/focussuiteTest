﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.EmployeeService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmailEmployeeRequest : ServiceRequest
	{
		[DataMember]
		public long EmployeeId { get; set; }

		[DataMember]
		public string EmailSubject { get; set; }

		[DataMember]
		public string EmailBody { get; set; }

		[DataMember]
		public bool BccSelf { get; set; }

    [DataMember]
    public byte[] Attachment { get; set; }

    [DataMember]
    public string AttachmentName { get; set; }

		[DataMember]
		public string From { get; set; }

		[DataMember]
		public bool SendCopy { get; set; }

		[DataMember]
		public EmailFooterTypes FooterType { get; set; }

		[DataMember]
		public string FooterUrl { get; set; }
  }

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmailEmployeeResponse : ServiceResponse
	{
		public EmailEmployeeResponse(){}

		public EmailEmployeeResponse(EmailEmployeeRequest request) : base(request){}
	}
}
