﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployeeService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployeeBusinessUnitActivityRequest : ServiceRequest
	{
		[DataMember]
		public EmployeeBusinessUnitCriteria Criteria { get; set; }

		[DataMember]
		public long EmployeeBuId { get; set; }
		
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployeeBusinessUnitActivityResponse : ServiceResponse
	{
		public EmployeeBusinessUnitActivityResponse()
		{
		}

		public EmployeeBusinessUnitActivityResponse(EmployeeBusinessUnitActivityRequest request) : base(request)
		{
		}

		[DataMember]
    public PagedList<HiringManagerActivityViewDto> EmployeeBusinessUnitActivityPaged { get; set; }

		[DataMember]
    public List<HiringManagerActivityViewDto> EmployeeBusinessActivityViews { get; set; }

		[DataMember]
		public List<HiringManagerActivityViewDto> EmployeeBusinessUnitActivityUsers { get; set; }

	}
}
