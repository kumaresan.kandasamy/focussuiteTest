﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Net.Sockets;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployeeService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployeeRequest : ServiceRequest
	{
		[DataMember]
		public long Id { get; set; }

		[DataMember]
		public long EmployeeId { get; set; }

		[DataMember]
		public long BusinessUnitId { get; set; }

		[DataMember]
		public ApprovalType EmployerApprovalType { get; set; }

		[DataMember]
		public string LockVersion { get; set; }

		[DataMember]
		public List<KeyValuePair<long, string>> ReferralStatusReasons { get; set; }

	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployeeResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmployeeResponse"/> class.
		/// </summary>
		public EmployeeResponse(){}

		/// <summary>
		/// Initializes a new instance of the <see cref="EmployeeResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public EmployeeResponse(EmployeeRequest request) : base(request){}

		[DataMember]
		public EmployeeDto Employee { get; set; }

		[DataMember]
		public ApprovalStatuses EmployerApprovalStatus { get; set; }

		[DataMember]
		public ApprovalStatuses BusinessUnitApprovalStatus { get; set; }

		[DataMember]
		public List<long> SameCompanyEmployeeIds { get; set; }

	  [DataMember]
	  public List<string> HiringAreas { get; set; }

	  [DataMember]
    public ApproveReferralResponseType ReferralResponseType {get; set;}

	}
	
}
