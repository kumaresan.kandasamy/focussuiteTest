﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployeeService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployeeBusinessUnitInfoRequest : ServiceRequest
	{
		[DataMember]
		public long Id { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployeeBusinessUnitInfoResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmployeeResponse"/> class.
		/// </summary>
		public EmployeeBusinessUnitInfoResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="EmployeeResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public EmployeeBusinessUnitInfoResponse(EmployeeBusinessUnitInfoRequest request) : base(request) { }

		[DataMember]
		public long EmployeeId { get; set; }

		[DataMember]
		public long EmployerId { get; set; }

		[DataMember]
		public string EmployeeName { get; set; }

		[DataMember]
		public string EmployeeEmail { get; set; }

		[DataMember]
		public List<PhoneNumberDto> EmployeePhoneNumber { get; set; }

		[DataMember]
		public long BusinessUnitId { get; set; }

		[DataMember]
		public string BusinessUnitName { get; set; }

		[DataMember]
		public BusinessUnitDto BusinessUnit { get; set; }

    [DataMember]
    public List<BusinessUnitAddressDto> BusinessUnitAddresses { get; set; }

		[DataMember]
		public int DaysSinceRegistration { get; set; }

		[DataMember]
		public int LoginsSinceRegistration { get; set; }

		[DataMember]
		public int Logins7Days { get; set; }

		[DataMember]
		public DateTime? LastLogin { get; set; }

		[DataMember]
		public int ApplicantsInterviewed { get; set; }

		[DataMember]
		public int ApplicantsFailedToShow { get; set; }

		[DataMember]
		public int ApplicantsInterviewDenied { get; set; }

		[DataMember]
		public int ApplicantsHired { get; set; }

		[DataMember]
		public int ApplicantsRejected { get; set; }

		[DataMember]
		public int ApplicantsRefused { get; set; }

		[DataMember]
		public int ApplicantsDidNotApply { get; set; }

		[DataMember]
		public int ApplicantsFailedToReportToJob { get; set; }

		[DataMember]
		public int ApplicantsFailedToRespondToInvitation { get; set; }

		[DataMember]
		public int ApplicantsFoundJobFromOtherSource { get; set; }

		[DataMember]
		public int ApplicantsJobAlreadyFilled { get; set; }

		[DataMember]
		public int ApplicantsNewApplicant { get; set; }

		[DataMember]
		public int ApplicantsNotQualified { get; set; }

		[DataMember]
		public int ApplicantsNotYetPlaced { get; set; }

		[DataMember]
		public int ApplicantsRecommended { get; set; }

		[DataMember]
		public int ApplicantsRefusedReferral { get; set; }

		[DataMember]
		public int ApplicantsUnderConsideration { get; set; }

		[DataMember]
		public int SurveyVeryDissatified { get; set; }

		[DataMember]
		public int SurveyVerySatisfied { get; set; }

		[DataMember]
		public int SurveySomewhatDissatified { get; set; }

		[DataMember]
		public int SurveySomewhatSatisfied { get; set; }

		[DataMember]
		public int SurveyDidNotInterview { get; set; }

		[DataMember]
		public int SurveyInterviewed { get; set; }

		[DataMember]
		public int SurveyDidNotHire { get; set; }

		[DataMember]
		public int SurveyHired { get; set; }

		#region ActivitySummary

		[DataMember]
		public int ApplicantsSeven { get; set; }

		[DataMember]
		public int ApplicantsThirty { get; set; }

		[DataMember]
		public int MatchesSeven { get; set; }

		[DataMember]
		public int MatchesThirty { get; set; }

		[DataMember]
		public int ThreeFiveStar { get; set; }

		[DataMember]
		public int ThreeFiveStarNotViewed { get; set; }

		[DataMember]
		public int InvitationsSentSeven { get; set; }
		
		[DataMember]
		public int InvitationsSentThirty { get; set; }

		[DataMember]
		public int InvitedSeekersViewedJobSeven { get; set; }

		[DataMember]
		public int InvitedSeekersViewedJobThirty { get; set; }

		[DataMember]
		public int InvitedSeekersNotViewJobSeven { get; set; }

		[DataMember]
		public int InvitedSeekersNotViewJobThirty { get; set; }

		[DataMember]
		public int InvitedSeekersClickedHowToApplySeven { get; set; }

		[DataMember]
		public int InvitedSeekersClickedHowToApplyThirty { get; set; }

		[DataMember]
		public int InvitedSeekersNotClickHowToApplySeven { get; set; }

		[DataMember]
		public int InvitedSeekersNotClickHowToApplyThirty { get; set; }
		#endregion
	}
}
