﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.EmployeeService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class AssignEmployeeToBusinessUnitRequest : ServiceRequest
	{
		[DataMember]
		public long EmployeeId { get; set; }

		[DataMember]
		public long BusinessUnitId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class AssignEmployeeToBusinessUnitResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="AssignEmployeeToBusinessUnitResponse"/> class.
		/// </summary>
		public AssignEmployeeToBusinessUnitResponse() {}

		/// <summary>
		/// Initializes a new instance of the <see cref="AssignEmployeeToBusinessUnitResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public AssignEmployeeToBusinessUnitResponse(AssignEmployeeToBusinessUnitRequest request) : base(request) {}
	}
}
