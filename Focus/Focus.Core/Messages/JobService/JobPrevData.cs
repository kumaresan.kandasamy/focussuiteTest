﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract( Namespace = Constants.DataContractNamespace )]
	public class JobPrevDataRequest : ServiceRequest
	{
		[DataMember]
		public long JobId { get; set; }

		[DataMember]
		public FocusModules Module { get; set; }
	}

	[DataContract( Namespace = Constants.DataContractNamespace )]
	public class JobPrevDataResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobDetailsResponse"/> class.
		/// </summary>
		public JobPrevDataResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="JobDetailsResponse"/> class.
		/// </summary>
		/// <param name="viewRequest">The viewRequest.</param>
		public JobPrevDataResponse( ServiceRequest viewRequest ) : base( viewRequest ) { }

		[DataMember]
		public JobPrevDataDto JobPrevData { get; set; }
	}
}
