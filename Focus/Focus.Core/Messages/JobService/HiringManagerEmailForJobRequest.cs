﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Models.Career;


#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class HiringManagerPersonNameEmailForJobRequest : ServiceRequest
	{
		[DataMember]
		public long JobId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class HiringManagerPersonNameEmailForJobResponse : ServiceResponse
	{
		public HiringManagerPersonNameEmailForJobResponse(HiringManagerPersonNameEmailForJobRequest request) : base(request) { }

		[DataMember]
		public PersonNameEmail Person { get; set; }
	}

}
