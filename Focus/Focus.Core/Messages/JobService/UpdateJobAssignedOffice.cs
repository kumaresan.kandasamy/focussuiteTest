﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateJobAssignedOfficeRequest : ServiceRequest
	{
		[DataMember]
		public long JobId { get; set; }

		[DataMember]
		public List<JobOfficeMapperDto> JobOffices { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateJobAssignedOfficeResponse : ServiceResponse
	{
		public UpdateJobAssignedOfficeResponse() { }

		public UpdateJobAssignedOfficeResponse(UpdateJobAssignedOfficeRequest request) : base(request) { }

		[DataMember]
		public JobDto Job { get; set; }
	}
}
