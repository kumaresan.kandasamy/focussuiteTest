﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveSavedJobRequest: ServiceRequest
	{
    #region Optional child objects (to be used to avoid separate service calls to save them)

    [DataMember]
    public List<SavedJobs> SavedJobs { get; set; }

    [DataMember]
    public List<ApplicationDto> Applications { get; set; }

		#endregion
		
    [DataMember]
    public string JobMigrationId;

    [DataMember]
    public string EmployeeMigrationId;

    [DataMember]
    public string EmployerMigrationLogoId;

    [DataMember]
    public string LensPostingMigrationId { get; set; }

    [DataMember]
    public string OfficeMigrationId { get; set; }

    [DataMember]
    public JobStatuses? InitialJobStatus { get; set; }

    [DataMember]
    public bool GeneratePostingForNewJob { get; set; }

		[DataMember]
		public int JobId { get; set; }

		[DataMember]
		public string JobSeekerMigrationId { get; set; }

		[DataMember]
		public string BookmarkName { get; set; }

		[DataMember]
		public int SavedJobId { get; set; }

		[DataMember]
		public string SavedJobMigrationId { get; set; }

		[DataMember]
		public int ReferralId { get; set; }

		[DataMember]
		public string MigrationValidationMessage { get; set; }
	}
	
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveSavedJobResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SaveJobResponse"/> class.
		/// </summary>
		public SaveSavedJobResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SaveJobResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SaveSavedJobResponse(SaveSpideredJobRequest request) : base(request) { }
	}
}
