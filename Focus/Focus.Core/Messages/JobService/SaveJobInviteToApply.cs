﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.JobService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveJobInviteToApplyRequest : ServiceRequest
  {
    [DataMember]
    public long PersonId { get; set; }

    [DataMember]
    public long JobId { get; set; }

    [DataMember]
    public string LensPostingId { get; set; }

		[DataMember]
		public int Score { get; set; }

    [DataMember]
    public bool QueueInvitation { get; set; }
	}

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveJobInviteToReplyResponse : ServiceResponse
  {
    public SaveJobInviteToReplyResponse (SaveJobInviteToApplyRequest request) :base(request) {}
    public SaveJobInviteToReplyResponse(){}
  }
}
