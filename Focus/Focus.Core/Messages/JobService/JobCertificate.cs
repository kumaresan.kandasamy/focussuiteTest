﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.JobCertificate;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobCertificateRequest : ServiceRequest
	{
		[DataMember]
		public JobCertificateCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobCertificateResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobCertificateResponse"/> class.
		/// </summary>
		public JobCertificateResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="JobCertificateResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public JobCertificateResponse(JobCertificateRequest request) : base(request) { }

		[DataMember]
		public List<JobCertificateDto> JobCertificates { get; set; }
	}
}
