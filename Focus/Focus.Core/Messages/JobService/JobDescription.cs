﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Criteria.Job;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobDescriptionRequest : ServiceRequest
	{
		[DataMember]
		public JobCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobDescriptionResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobDescriptionResponse"/> class.
		/// </summary>
		public JobDescriptionResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="JobDescriptionResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public JobDescriptionResponse(JobDescriptionRequest request) : base(request) { }

		[DataMember]
		public string JobDescription { get; set; }
	}
}
