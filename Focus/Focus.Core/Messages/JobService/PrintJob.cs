﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.JobService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class PrintJobRequest : ServiceRequest
  {
    [DataMember]
    public long JobId { get; set; }

    [DataMember]
    public JobDetailsView JobDetails { get; set; }
    
    [DataMember]
    public JobViewDto JobView { get; set; }

    [DataMember]
    public string JobStatus { get; set; }

    [DataMember]
    public string EmployerFirstName { get; set; }

    [DataMember]
    public string EmployerLastName { get; set; }

    [DataMember]
    public string PhoneNumber { get; set; }

    [DataMember]
    public string Email { get; set; }

    [DataMember]
    public List<JobActionEventViewDto> ActionEvents { get; set; }

    [DataMember]
    public List<ApplicationStatusLogViewDto> Referrals { get; set; }

    [DataMember]
    public List<NoteReminderViewDto> Notes { get; set; }

    [DataMember]
    public List<NoteReminderViewDto> Reminders { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class PrintJobResponse : ServiceResponse
  {
    public PrintJobResponse(PrintJobRequest request) : base(request){}

    [DataMember]
    public byte[] JobDocument { get; set; }

    [DataMember]
    public string DocumentName { get; set; }
  }
}
