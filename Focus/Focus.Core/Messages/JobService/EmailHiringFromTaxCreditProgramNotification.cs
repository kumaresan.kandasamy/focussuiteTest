﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.JobService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EmailHiringFromTaxCreditProgramNotificationRequest : ServiceRequest
  {
		[DataMember]
		public string EmailTo { get; set; }

    [DataMember]
    public string Name { get; set; }

    [DataMember]
    public string PhoneNumber { get; set; }

    [DataMember]
    public string HiringManagerEmailAddress { get; set; }

    [DataMember]
    public WorkOpportunitiesTaxCreditCategories? WorkOpportunitiesTaxCreditHires { get; set; }

    [DataMember]
    public DateTime? PostedOn { get; set; }
    
   }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EmailHiringFromTaxCreditProgramNotificationResponse : ServiceResponse
  {
    public EmailHiringFromTaxCreditProgramNotificationResponse() { }

    public EmailHiringFromTaxCreditProgramNotificationResponse(EmailHiringFromTaxCreditProgramNotificationRequest request) : base(request) { }
  }
}


