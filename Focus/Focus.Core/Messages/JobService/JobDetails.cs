﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobDetailsRequest : ServiceRequest
	{
		[DataMember]
		public long JobId { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to [get recommendation count]. Setting to true adds some time to the request as a lens search has to be run.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [get recommendation count]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool GetRecommendationCount { get; set; }

    [DataMember]
    public FocusModules Module { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobDetailsResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobDetailsResponse"/> class.
		/// </summary>
		public JobDetailsResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="JobDetailsResponse"/> class.
		/// </summary>
		/// <param name="viewRequest">The viewRequest.</param>
		public JobDetailsResponse(JobDetailsRequest viewRequest) : base(viewRequest) { }

		[DataMember]
		public JobDetailsView JobDetails { get; set; }

		[DataMember]
		public JobActivityView JobActivitySummary { get; set; }
	}
}
