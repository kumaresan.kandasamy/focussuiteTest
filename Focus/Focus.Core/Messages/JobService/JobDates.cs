﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.JobService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobDatesRequest : ServiceRequest
  {
    [DataMember]
    public long JobId { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobDatesResponse : ServiceResponse
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="JobDatesResponse"/> class.
    /// </summary>
    public JobDatesResponse() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="JobDatesResponse"/> class.
    /// </summary>
    /// <param name="request">The request.</param>
    public JobDatesResponse(JobDatesRequest request) : base(request) { }

    /// <summary>
    /// The posted on date
    /// </summary>
    [DataMember]
    public JobDatesView JobDates { get; set; }
  }
}
