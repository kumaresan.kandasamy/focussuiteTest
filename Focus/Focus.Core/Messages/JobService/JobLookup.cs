﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.Job;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobLookupRequest : ServiceRequest
	{
		[DataMember]
		public JobLookupCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobLookupResponse : ServiceResponse
	{
		public JobLookupResponse(){}

		public JobLookupResponse(JobLookupRequest request) : base(request){}

		[DataMember]
		public PagedList<JobLookupView> JobLookupsPaged { get; set; }

		[DataMember]
		public List<JobLookupView> JobLookups { get; set; }

		[DataMember]
		public JobLookupView JobLookup { get; set; }
	}
}
