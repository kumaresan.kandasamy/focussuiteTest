﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateCreditCheckRequiredRequest : ServiceRequest
	{
		[DataMember]
		public long JobId { get; set; }

		[DataMember]
		public bool CreditCheckRequired { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateCreditCheckRequiredResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="UpdateCreditCheckRequiredResponse"/> class.
		/// </summary>
		public UpdateCreditCheckRequiredResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="UpdateCreditCheckRequiredResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public UpdateCreditCheckRequiredResponse(UpdateCreditCheckRequiredRequest request) : base(request) { }

		[DataMember]
		public bool CreditCheckRequired { get; set; }

		[DataMember]
		public JobDto Job { get; set; }
	}
}
