﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.OnetTask;
using Focus.Core.Views;

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobOnetTaskRequest : ServiceRequest
	{
		[DataMember]
		public OnetTaskCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobOnetTaskResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobOnetTaskResponse"/> class.
		/// </summary>
		public JobOnetTaskResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="JobOnetTaskResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public JobOnetTaskResponse(JobOnetTaskRequest request) : base(request) { }

		[DataMember]
		public List<OnetTaskView> OnetTasks { get; set; }
	}
}
