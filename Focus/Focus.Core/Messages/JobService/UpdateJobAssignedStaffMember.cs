﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateJobAssignedStaffMemberRequest : ServiceRequest
	{
		[DataMember]
		public List<long> JobIds { get; set; }

		[DataMember]
		public long PersonId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateJobAssignedStaffMemberResponse : ServiceResponse
	{
		public UpdateJobAssignedStaffMemberResponse() { }

		public UpdateJobAssignedStaffMemberResponse(UpdateJobAssignedStaffMemberRequest request) : base(request) { }

		[DataMember]
		public JobDto Job { get; set; }
	}
}
