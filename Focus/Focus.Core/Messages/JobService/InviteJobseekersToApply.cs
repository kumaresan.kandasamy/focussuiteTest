﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion



namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class InviteJobseekersToApplyRequest : ServiceRequest
	{
		/// <summary>
		/// Gets or sets the person ids of the jobseekers to invite, along with their matching scores
		/// </summary>
		[DataMember]
		public Dictionary<long, int> PersonIdsWithScores { get; set; }

		/// <summary>
		/// Gets or sets the job id.
		/// </summary>
		[DataMember]
		public long JobId { get; set; }

		/// <summary>
		/// Gets or sets the job link.
		/// </summary>
		[DataMember]
		public string JobLink { get; set; }

		/// <summary>
		/// Gets or sets the lens posting id.
		/// </summary>
		[DataMember]
		public string LensPostingId { get; set; }

		/// <summary>
		/// Gets or sets the sender email.
		/// </summary>
		[DataMember] 
		public string SenderEmail { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class InviteJobseekersToApplyResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="InviteJobseekersToApplyResponse"/> class.
		/// </summary>
		public InviteJobseekersToApplyResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="InviteJobseekersToApplyResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public InviteJobseekersToApplyResponse(InviteJobseekersToApplyRequest request) : base(request) { }
	}
}
