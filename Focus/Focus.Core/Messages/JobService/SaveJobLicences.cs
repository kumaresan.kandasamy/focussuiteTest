﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.JobLicence;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveJobLicencesRequest : ServiceRequest
	{
		[DataMember]
		public JobLicenceCriteria Criteria { get; set; }

		[DataMember]
		public List<JobLicenceDto> JobLicences { get; set; }

    [DataMember]
    public string JobMigrationId;

    [DataMember]
    public string JobLicenceMigrationId;

    [DataMember]
    public string MigrationValidationMessage { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveJobLicencesResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SaveJobLicencesResponse"/> class.
		/// </summary>
		public SaveJobLicencesResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SaveJobLicencesResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SaveJobLicencesResponse(SaveJobLicencesRequest request) : base(request) { }

		[DataMember]
		public List<JobLicenceDto> JobLicences { get; set; }
	}
}
