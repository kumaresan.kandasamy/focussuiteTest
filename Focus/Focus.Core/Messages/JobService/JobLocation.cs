﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.JobLocation;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobLocationRequest : ServiceRequest
	{
		[DataMember]
		public JobLocationCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobLocationResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobLocationResponse"/> class.
		/// </summary>
		public JobLocationResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="JobLocationResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public JobLocationResponse(JobLocationRequest request) : base(request) { }

		[DataMember]
		public List<JobLocationDto> JobLocations { get; set; }
	}
}
