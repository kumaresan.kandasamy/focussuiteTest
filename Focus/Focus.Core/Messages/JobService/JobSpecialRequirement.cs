﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.JobSpecialRequirement;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobSpecialRequirementRequest : ServiceRequest
	{
		[DataMember]
		public JobSpecialRequirementCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobSpecialRequirementResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobSpecialRequirementResponse"/> class.
		/// </summary>
		public JobSpecialRequirementResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="JobSpecialRequirementResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public JobSpecialRequirementResponse(JobSpecialRequirementRequest request) : base(request) { }

		[DataMember]
		public List<JobSpecialRequirementDto> JobSpecialRequirements { get; set; }

		[DataMember]
		public int JobRequirementsCount { get; set; }
	}
}
