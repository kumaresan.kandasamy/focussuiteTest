﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.JobTask;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobTaskRequest: ServiceRequest
	{
		[DataMember]
		public JobTaskCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobTaskResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobTaskResponse"/> class.
		/// </summary>
		public JobTaskResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="JobTaskResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public JobTaskResponse(JobTaskRequest request) : base(request) { }

		[DataMember]
		public List<JobTaskDetailsView> JobTasks { get; set; }
	}
}
