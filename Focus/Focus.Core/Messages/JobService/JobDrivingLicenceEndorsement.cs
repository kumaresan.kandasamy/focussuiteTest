﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.JobDrivingLicenceEndorsement;

#endregion


namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobDrivingLicenceEndorsementRequest : ServiceRequest
	{
		[DataMember]
		public JobDrivingLicenceEndorsementCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobDrivingLicenceEndorsementResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobDrivingLicenceEndorsementResponse"/> class.
		/// </summary>
		public JobDrivingLicenceEndorsementResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="JobDrivingLicenceEndorsementResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public JobDrivingLicenceEndorsementResponse(JobDrivingLicenceEndorsementRequest request) : base(request) { }

		[DataMember]
		public List<JobDrivingLicenceEndorsementDto> JobDrivingLicenceEndorsements { get; set; }
	}
}
