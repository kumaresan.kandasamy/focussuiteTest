﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class PostJobRequest : ServiceRequest
	{
		/// <summary>
		/// Gets or sets the job.
		/// </summary>
		[DataMember]
		public JobDto Job { get; set; }

    [DataMember]
    public FocusModules Module { get; set; }

    [DataMember]
    public long? JobPostedBy { get; set; }

    [DataMember]
    public bool IgnoreLens { get; set; }

    [DataMember]
    public bool IgnoreClient { get; set; }

    [DataMember]
    public string InitialLensPostingId { get; set; }

    [DataMember]
    public string OfficeMigrationId { get; set; }

    [DataMember]
    public ApprovalStatuses? InitialApprovalStatus { get; set; }

    [DataMember]
    public JobStatuses? InitialJobStatus { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class PostJobResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="PostJobResponse"/> class.
		/// </summary>
		public PostJobResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="PostJobResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public PostJobResponse(PostJobRequest request) : base(request) { }

		/// <summary>
		/// Gets or sets the content rating.
		/// </summary>
		/// <value>The content rating.</value>
		[DataMember]
		public ContentRatings ContentRating { get; set; }

		/// <summary>
		/// Gets or sets the inappropiate words.
		/// </summary>
		/// <value>The inappropiate words.</value>
		[DataMember]
		public List<string> InappropiateWords { get; set; }

		/// <summary>
		/// Gets or sets the job.
		/// </summary>
		/// <value>The job.</value>
		[DataMember]
		public JobDto Job { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [special conditions].
		/// </summary>
		/// <value>
		///   <c>true</c> if [special conditions]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool SpecialConditions { get; set; }
	
	}
}
