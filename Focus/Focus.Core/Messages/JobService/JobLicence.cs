﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.JobLicence;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobLicenceRequest : ServiceRequest
	{
		[DataMember]
		public JobLicenceCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobLicenceResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobLicenceResponse"/> class.
		/// </summary>
		public JobLicenceResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="JobLicenceResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public JobLicenceResponse(JobLicenceRequest request) : base(request) { }

		[DataMember]
		public List<JobLicenceDto> JobLicences { get; set; }
	}
}
