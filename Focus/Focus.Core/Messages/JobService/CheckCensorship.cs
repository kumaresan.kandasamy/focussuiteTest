﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CheckCensorshipRequest : ServiceRequest
	{
		public CensorshipType Type { get; set; }

		/// <summary>
		/// Gets or sets the job.
		/// </summary>
		[DataMember]
		public JobDto Job { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class CheckCensorshipResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CheckCensorshipResponse"/> class.
		/// </summary>
		public CheckCensorshipResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="CheckCensorshipResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public CheckCensorshipResponse(CheckCensorshipRequest request) : base(request) { }
		
		/// <summary>
		/// Gets or sets the inappropiate words.
		/// </summary>
		/// <value>The inappropiate words.</value>
		[DataMember]
		public List<string> CensoredWords { get; set; }
	}
}
