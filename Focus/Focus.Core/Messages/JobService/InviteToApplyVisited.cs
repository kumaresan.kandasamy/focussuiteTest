﻿#region Copyright © 2000 - 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.JobService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class InviteToApplyVisitedRequest : ServiceRequest
  {
    [DataMember]
    public long PersonId { get; set; }
    
    [DataMember]
    public string LensPostingId { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class InviteToApplyVisitedResponse : ServiceResponse
  {
    public InviteToApplyVisitedResponse(){}
    public InviteToApplyVisitedResponse(InviteToApplyVisitedRequest request): base(request){}

    [DataMember]
    public bool IsInvitee { get; set; }

		[DataMember]
		public bool IsRecommended { get; set; }
  }
}
