﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveJobAddressRequest : ServiceRequest
	{
		[DataMember]
		public JobAddressDto JobAddress { get; set; }

    [DataMember]
    public string JobMigrationId;

    [DataMember]
    public string JobAddressMigrationId;

    [DataMember]
    public string MigrationValidationMessage { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveJobAddressResponse : ServiceResponse
	{
		public SaveJobAddressResponse() : base() {}

		public SaveJobAddressResponse(SaveJobAddressRequest request) : base(request) {}

    [DataMember]
    public JobAddressDto JobAddress { get; set; }
	}
}
