﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.JobService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ResolveJobIssuesRequest : ServiceRequest
  {
    [DataMember]
    public long JobId { get; set; }

    [DataMember]
    public List<JobIssuesFilter> IssuesResolved { get; set; }

    [DataMember]
    public List<long> PostHireFollowUpsResolved { get; set; }

		[DataMember]
		public bool IsAutoResolution { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ResolveJobIssuesResponse : ServiceResponse
  {
    public ResolveJobIssuesResponse(){}

    public ResolveJobIssuesResponse(ResolveJobIssuesRequest request):base(request){}
  }
}
