﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Runtime.Serialization;

using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.JobService
{
	public class SavedJobs
	{
		public object Id { get; set; }
	}
	
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveSpideredJobRequest : ServiceRequest
	{
		/// <summary>
		/// Gets or sets the job.
		/// </summary>
		[DataMember]
		public string JobTitle { get; set; }

    [DataMember]
    public long? JobCreatedBy { get; set; }

		[DataMember]
		public DateTime? OverrideCreatedOn { get; set; }

    #region Optional child objects (to be used to avoid separate service calls to save them)

    [DataMember]
    public List<SavedJobs> SavedJobs { get; set; }
		
    [DataMember]
    public List<ApplicationDto> Applications { get; set; }

		#endregion


    [DataMember]
    public string JobMigrationId;

    [DataMember]
    public string EmployeeMigrationId;

    [DataMember]
    public string EmployerMigrationLogoId;

    [DataMember]
    public string LensPostingMigrationId { get; set; }

    [DataMember]
    public string OfficeMigrationId { get; set; }

    [DataMember]
    public JobStatuses? InitialJobStatus { get; set; }

    [DataMember]
    public bool GeneratePostingForNewJob { get; set; }

    [DataMember]
    public string MigrationValidationMessage { get; set; }
		
		[DataMember]
		public string EmployerName { get; set; }

		[DataMember]
		public long JobId { get; set; }

		[DataMember]
		public DateTime DateCreated { get; set; }

		[DataMember]
		public DateTime DateUpdated { get; set; }

		[DataMember]
		public int OriginId { get; set; }

		[DataMember]
		public long? JobSeekerMigrationId { get; set; }

		[DataMember]
		public int Status { get; set; }
	}
	
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveSpideredJobResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SaveJobResponse"/> class.
		/// </summary>
		public SaveSpideredJobResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SaveJobResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SaveSpideredJobResponse(SaveSpideredJobRequest request) : base(request) { }

		/// <summary>
		/// Gets or sets the job.
		/// </summary>
		[DataMember]
		public PostingDto Posting { get; set; }
	}
}
