﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.JobProgramsOfStudy;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.JobLicence;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobProgramOfStudyRequest : ServiceRequest
	{
		[DataMember]
		public JobProgramsOfStudyCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobProgramOfStudyResponse : ServiceResponse
	{

		/// <summary>
		/// Initializes a new instance of the <see cref="JobProgramOfStudyResponse"/> class.
		/// </summary>
		public JobProgramOfStudyResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="JobProgramOfStudyResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public JobProgramOfStudyResponse(JobProgramOfStudyRequest request) : base(request) { }

		[DataMember]
		public List<JobProgramOfStudyDto> JobProgramsOfStudy { get; set; }
	}
}
