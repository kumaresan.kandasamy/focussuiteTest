﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.Criteria.JobDrivingLicenceEndorsement;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveJobDrivingLicenceEndorsementsRequest : ServiceRequest
	{
		[DataMember]
		public JobDrivingLicenceEndorsementCriteria Criteria { get; set; }

		[DataMember]
		public List<JobDrivingLicenceEndorsementDto> JobDrivingLicenceEndorsements { get; set; }

		[DataMember]
		public string JobMigrationId;

		[DataMember]
		public string JobDrivingLicenceEndorsementMigrationId;
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveJobDrivingLicenceEndorsementsResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SaveJobDrivingLicenceEndorsementsResponse"/> class.
		/// </summary>
		public SaveJobDrivingLicenceEndorsementsResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SaveJobDrivingLicenceEndorsementsResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SaveJobDrivingLicenceEndorsementsResponse(SaveJobDrivingLicenceEndorsementsRequest request) : base(request) { }

		[DataMember]
		public List<JobDrivingLicenceEndorsementDto> JobDrivingLicenceEndorsements { get; set; }
	}
}
