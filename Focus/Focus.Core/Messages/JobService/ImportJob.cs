﻿#region Copyright © 2000 - 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.Models.Import;

#endregion

namespace Focus.Core.Messages.JobService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ImportJobRequest: ServiceRequest
  {
    [DataMember]
    public List<Tuple<string, ApprovalStatuses>> ExternalJobIds { get; set; }

    [DataMember]
    public Guid? ImportBatchId { get; set; }
  }
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ImportJobResponse: ServiceResponse
  {
    public ImportJobResponse(){}

    public ImportJobResponse(ImportJobRequest request):base(request){}

    [DataMember]
    public List<ImportOutcome> Results { get; set; }

    [DataMember]
    public Guid ImportBatchId { get; set; }
  }
}
