﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SimilarJobRequest : ServiceRequest
	{
		[DataMember]
		public string JobTitle { get; set; }

    [DataMember]
    public JobTypes? JobType { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SimilarJobResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SimilarJobResponse"/> class.
		/// </summary>
		public SimilarJobResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SimilarJobResponse"/> class.
		/// </summary>
		/// <param name="viewRequest">The viewRequest.</param>
		public SimilarJobResponse(SimilarJobRequest viewRequest) : base(viewRequest) { }

		[DataMember]
		public List<SimilarJobView> Jobs { get; set; }
	}
}
