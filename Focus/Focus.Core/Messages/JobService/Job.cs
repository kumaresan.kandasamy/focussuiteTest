﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.Job;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobRequest: ServiceRequest
	{
		[DataMember]
		public JobCriteria Criteria { get; set; }

		[DataMember]
		public PostingSurveyDto PostingSurvey { get; set; }

		[DataMember]
		public DateTime? NewExpirationDate { get; set; }

		[DataMember]
		public JobStatuses JobStatus { get; set; }

		[DataMember]
		public string NewPosting { get; set; }

		[DataMember]
		public int? NewNumberOfOpenings { get; set; }

    [DataMember]
    public FocusModules Module { get; set; }

    [DataMember]
    public ApprovalStatuses? ApprovalStatus { get; set; }

		[DataMember]
		public int? LockVersion { get; set; }

		[DataMember]
		public List<KeyValuePair<long, string>> ReferralStatusReasons { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobResponse"/> class.
		/// </summary>
		public JobResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="JobResponse"/> class.
		/// </summary>
		/// <param name="request">The summaryRequest.</param>
		public JobResponse(JobRequest request) : base(request) { }

		[DataMember]
		public PagedList<JobDto> JobsPaged { get; set; }

		[DataMember]
		public IEnumerable<JobDto> Jobs { get; set; }

		[DataMember]
		public JobDto Job { get; set; }

		[DataMember]
		public ErrorTypes ResponseType { get; set; }
	}
}
