﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.Job;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobViewRequest : ServiceRequest
	{
		[DataMember]
		public JobCriteria Criteria { get; set; }

    [DataMember]
    public FocusModules Module { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobViewResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobViewResponse"/> class.
		/// </summary>
		public JobViewResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="JobViewResponse"/> class.
		/// </summary>
		/// <param name="viewRequest">The viewRequest.</param>
		public JobViewResponse(JobViewRequest viewRequest) : base(viewRequest) { }

		[DataMember]
		public PagedList<JobViewModel> JobViewsPaged { get; set; }

		[DataMember]
    public List<JobViewDto> JobViews { get; set; }

		[DataMember]
    public JobViewDto JobView { get; set; }
	}
}
