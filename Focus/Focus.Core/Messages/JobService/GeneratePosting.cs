﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GeneratePostingRequest : ServiceRequest
	{
		[DataMember]
		public JobDto Job;

		[DataMember]
		public List<JobLocationDto> JobLocations;

		[DataMember]
		public List<JobLanguageDto> JobLanguages;

		[DataMember]
		public List<JobCertificateDto> JobCertificates;

		[DataMember]
		public List<JobLicenceDto> JobLicences;

		[DataMember]
		public List<JobSpecialRequirementDto> JobSpecialRequirements;

		[DataMember]
		public List<JobProgramOfStudyDto> JobProgramsOfStudy;

		[DataMember]
		public List<JobDrivingLicenceEndorsementDto> JobDrivingLicenceEndorsements;
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GeneratePostingResponse : ServiceResponse
	{
		public GeneratePostingResponse() : base(){}

		public GeneratePostingResponse(GeneratePostingRequest request) : base(request) {}

		[DataMember]
		public string Posting { get; set; }
	}
}
