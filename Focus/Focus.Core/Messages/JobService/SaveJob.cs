﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveJobRequest : ServiceRequest
	{
		/// <summary>
		/// Gets or sets the job.
		/// </summary>
		[DataMember]
		public JobDto Job { get; set; }

    [DataMember]
    public long? JobCreatedBy { get; set; }


    #region Optional child objects (to be used to avoid separate service calls to save them)

    [DataMember]
    public List<JobLocationDto> JobLocations { get; set; }

    [DataMember]
    public JobAddressDto JobAddress { get; set; }

    [DataMember]
    public List<JobCertificateDto> JobCertificates { get; set; }

    [DataMember]
    public List<JobLicenceDto> JobLicences { get; set; }

    [DataMember]
    public List<JobLanguageDto> JobLanguages { get; set; }

    [DataMember]
    public List<JobSpecialRequirementDto> JobSpecialRequirements { get; set; }

    [DataMember]
    public List<JobProgramOfStudyDto> JobProgramsOfStudy { get; set; }

		[DataMember]
		public List<JobDrivingLicenceEndorsementDto> JobDrivingLicenceEndorsements { get; set; }

		#endregion


		[DataMember]
		public FocusModules Module { get; set; }

    [DataMember]
    public string JobMigrationId;

    [DataMember]
    public string EmployeeMigrationId;

    [DataMember]
    public string EmployerMigrationLogoId;

    [DataMember]
    public string LensPostingMigrationId { get; set; }

    [DataMember]
    public string OfficeMigrationId { get; set; }

    [DataMember]
    public JobStatuses? InitialJobStatus { get; set; }

    [DataMember]
    public bool GeneratePostingForNewJob { get; set; }

    [DataMember]
    public string MigrationValidationMessage { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveJobResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SaveJobResponse"/> class.
		/// </summary>
		public SaveJobResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SaveJobResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SaveJobResponse(SaveJobRequest request) : base(request) { }

		/// <summary>
		/// Gets or sets the job.
		/// </summary>
		[DataMember]
		public JobDto Job { get; set; }
	}
}
