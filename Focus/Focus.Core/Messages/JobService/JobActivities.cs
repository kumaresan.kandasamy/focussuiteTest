﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.Job;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.JobService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobActivitiesRequest : ServiceRequest
  {
    [DataMember]
    public JobActivityCriteria Criteria { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class JobActivitiesResponse : ServiceResponse
  {
    public JobActivitiesResponse(){}
    public JobActivitiesResponse(JobActivitiesRequest request) : base(request) { }

    [DataMember]
    public List<JobActionEventViewDto> JobActionEventsList { get; set; }

    [DataMember]
    public PagedList<JobActionEventViewDto> JobActionEventViewPaged { get; set; }
  }
}
