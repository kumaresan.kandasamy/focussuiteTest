﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.JobLanguage;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveJobLanguagesRequest : ServiceRequest
	{
		[DataMember]
		public JobLanguageCriteria Criteria { get; set; }

		[DataMember]
		public List<JobLanguageDto> JobLanguages { get; set; }

    [DataMember]
    public string JobMigrationId;

    [DataMember]
    public string JobLanguageMigrationId;

    [DataMember]
    public string MigrationValidationMessage { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveJobLanguagesResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SaveJobLanguagesResponse"/> class.
		/// </summary>
		public SaveJobLanguagesResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SaveJobLanguagesResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SaveJobLanguagesResponse(SaveJobLanguagesRequest request) : base(request) { }

		[DataMember]
		public List<JobLanguageDto> JobLanguages { get; set; }
	}
}
