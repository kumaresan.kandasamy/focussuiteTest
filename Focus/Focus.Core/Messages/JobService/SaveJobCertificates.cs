﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.JobCertificate;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveJobCertificatesRequest : ServiceRequest
	{
		[DataMember]
		public JobCertificateCriteria Criteria { get; set; }

		[DataMember]
		public List<JobCertificateDto> JobCertificates { get; set; }

    [DataMember]
    public string JobMigrationId;

    [DataMember]
    public string JobCertificationMigrationId;

    [DataMember]
    public string MigrationValidationMessage { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveJobCertificatesResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SaveJobCertificatesResponse"/> class.
		/// </summary>
		public SaveJobCertificatesResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SaveJobCertificatesResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SaveJobCertificatesResponse(SaveJobCertificatesRequest request) : base(request) { }

		[DataMember]
		public List<JobCertificateDto> JobCertificates { get; set; }
	}
}
