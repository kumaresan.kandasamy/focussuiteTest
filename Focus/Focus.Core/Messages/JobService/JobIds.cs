﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.JobService
{
	[DataContract( Namespace = Constants.DataContractNamespace )]
	public class JobIdsRequest : ServiceRequest
	{
		[DataMember]
		public string CheckId { get; set; }

		[DataMember]
		public bool CheckVeteranPriorityDate { get; set; }
		
		[DataMember]
		public List<JobStatuses> JobStatuses { get; set; }
	}

	[DataContract( Namespace = Constants.DataContractNamespace )]
	public class JobIdsResponse : ServiceResponse
	{
		public JobIdsResponse() { }

		public JobIdsResponse( JobIdsRequest request ) : base( request ) { }

		[DataMember]
		public string LensPostingId { get; set; }
	}
}
