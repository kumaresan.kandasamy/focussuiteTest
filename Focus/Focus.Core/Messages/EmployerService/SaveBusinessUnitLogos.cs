﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveBusinessUnitLogosRequest : ServiceRequest
	{
		[DataMember]
		public List<BusinessUnitLogoDto> BusinessUnitLogos { get; set; }

		[DataMember]
		public long? BusinessUnitId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveBusinessUnitLogosResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SaveBusinessUnitLogosResponse"/> class.
		/// </summary>
		public SaveBusinessUnitLogosResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SaveBusinessUnitLogosResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SaveBusinessUnitLogosResponse(SaveBusinessUnitLogosRequest request) : base(request) { }

		[DataMember]
		public List<BusinessUnitLogoDto> BusinessUnitLogos { get; set; }

		[DataMember]
		public List<BusinessUnitLogoDto> BusinessUnitLogosAssociatedWithJob { get; set; }
	}
}
