﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Criteria.Employer;

#endregion

namespace Focus.Core.Messages.EmployerService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveEmployerDetailsRequest : ServiceRequest
  {
    [DataMember]
    public EmployerCriteria Criteria;

  	[DataMember]
		public string FederalEmployerIdentificationNumber { get; set; }

		[DataMember]
		public string StateEmployerIdentificationNumber { get; set; }

		[DataMember]
		public string Name { get; set; }

		[DataMember]
		public int? LockVersion { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveEmployerDetailsResponse : ServiceResponse
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="SaveEmployerDetailsResponse"/> class.
    /// </summary>
    public SaveEmployerDetailsResponse() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="SaveEmployerDetailsResponse"/> class.
    /// </summary>
    /// <param name="request">The request.</param>
    public SaveEmployerDetailsResponse(SaveEmployerDetailsRequest request) : base(request) { }
  }
}
