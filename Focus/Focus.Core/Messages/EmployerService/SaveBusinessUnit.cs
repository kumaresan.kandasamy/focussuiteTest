﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployerService
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class SaveBusinessUnitRequest : ServiceRequest
    {
        [DataMember]
        public BusinessUnitDto BusinessUnit { get; set; }
    }

    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class SaveBusinessUnitResponse : ServiceResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SaveBusinessUnitResponse"/> class.
        /// </summary>
        public SaveBusinessUnitResponse() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SaveBusinessUnitResponse"/> class.
        /// </summary>
        /// <param name="request">The request.</param>
        public SaveBusinessUnitResponse(SaveBusinessUnitRequest request) : base(request) { }

        [DataMember]
        public BusinessUnitDto BusinessUnit { get; set; }
    }
}
