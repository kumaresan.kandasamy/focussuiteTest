﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class PersonOfficeMapperRequest : ServiceRequest
	{
		[DataMember]
		public List<PersonOfficeMapperDto> PersonOffices { get; set; }

		[DataMember]
		public long? PersonId { get; set; }

    [DataMember]
    public string UserMigrationId { get; set; }

    [DataMember]
    public List<string> OfficeMigrationIds { get; set; }

    [DataMember]
    public string MigrationValidationMessage { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class PersonOfficeMapperResponse : ServiceResponse
	{
		public PersonOfficeMapperResponse() { }

		public PersonOfficeMapperResponse(PersonOfficeMapperRequest request) : base(request) { }

		[DataMember]
		public List<PersonOfficeMapperDto> OfficeRoles { get; set; }
	}
}
