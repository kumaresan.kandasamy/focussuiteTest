﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateOfficeAssignedZipsRequest : ServiceRequest
	{
		[DataMember]
		public List<long> OfficeIds { get; set; }

		[DataMember]
		public List<string> Zips { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdateOfficeAssignedZipsResponse : ServiceResponse
	{
		public UpdateOfficeAssignedZipsResponse() { }

		public UpdateOfficeAssignedZipsResponse(UpdateOfficeAssignedZipsRequest request) : base(request) { }

		[DataMember]
		public List<OfficeDto> Offices { get; set; }
	}
}
