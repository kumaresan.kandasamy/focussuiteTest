﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetFEINRequest : ServiceRequest
	{
		[DataMember]
		public long? BusinessUnitId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetFEINResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetFEINResponse"/> class.
		/// </summary>
		public GetFEINResponse() : base() {}

		/// <summary>
		/// Initializes a new instance of the <see cref="GetFEINResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GetFEINResponse(GetFEINRequest request) : base(request) { }

		[DataMember]
		public string FEIN { get; set; }
	}
}
