﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveBusinessUnitLogoRequest : ServiceRequest
	{
		[DataMember]
		public BusinessUnitLogoDto BusinessUnitLogo { get; set; }

    [DataMember]
    public string EmployeeMigrationId { get; set; }

    [DataMember]
    public string BusinessUnitLogoMigrationId { get; set; }

    [DataMember]
    public string MigrationValidationMessage { get; set; }
		
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveBusinessUnitLogoResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SaveBusinessUnitLogoResponse"/> class.
		/// </summary>
		public SaveBusinessUnitLogoResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SaveBusinessUnitLogoResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SaveBusinessUnitLogoResponse(SaveBusinessUnitLogoRequest request) : base(request) { }

		[DataMember]
		public long BusinessUnitLogoId { get; set; }
	}
}
