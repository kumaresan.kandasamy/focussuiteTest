﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.BusinessUnitLogo;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class BusinessUnitLogoRequest : ServiceRequest
	{
		[DataMember]
		public BusinessUnitLogoCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class BusinessUnitLogoResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="BusinessUnitLogoResponse"/> class.
		/// </summary>
		public BusinessUnitLogoResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="BusinessUnitLogoResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public BusinessUnitLogoResponse(BusinessUnitLogoRequest request) : base(request) { }

		[DataMember]
		public BusinessUnitLogoDto BusinessUnitLogo { get; set; }

		[DataMember]
		public List<BusinessUnitLogoDto> BusinessUnitLogos { get; set; }

		[DataMember]
		public PagedList<BusinessUnitLogoDto> BusinessUnitLogosPaged { get; set; }
	}
}
