﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.BusinessUnit;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveBusinessUnitsRequest : ServiceRequest
	{
		[DataMember]
		public List<BusinessUnitDto> BusinessUnits { get; set; }

		[DataMember]
		public BusinessUnitCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveBusinessUnitsResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SaveBusinessUnitsResponse"/> class.
		/// </summary>
		public SaveBusinessUnitsResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SaveBusinessUnitsResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SaveBusinessUnitsResponse(SaveBusinessUnitsRequest request) : base(request) { }

		[DataMember]
		public List<BusinessUnitDto> BusinessUnitsAssociatedWithJob { get; set; }

		[DataMember]
		public List<BusinessUnitDto> BusinessUnits { get; set; }
	}
}
