﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class OfficeJobsRequest : ServiceRequest
	{
		[DataMember]
		public long OfficeId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class OfficeJobsResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="OfficeJobsResponse"/> class.
		/// </summary>
		public OfficeJobsResponse()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="OfficeJobsResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public OfficeJobsResponse(OfficeJobsRequest request)
			: base(request)
		{
		}

		[DataMember]
		public List<JobOfficeMapperDto> OfficeJobs { get; set; }
	}
}
