﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.BusinessUnit;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class BusinessUnitPlacementsRequest : ServiceRequest
	{
		[DataMember]
		public BusinessUnitCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class BusinessUnitPlacementsResponse : ServiceResponse
	{
		
		public BusinessUnitPlacementsResponse() { }

		
		public BusinessUnitPlacementsResponse(BusinessUnitPlacementsRequest request) : base(request) { }

		[DataMember]
		public List<BusinessUnitPlacementsViewDto> BusinessUnitPlacementsViews { get; set; }

		[DataMember]
    public PagedList<BusinessUnitPlacementsViewDto> BusinessUnitPlacementsViewsPaged { get; set; }
		
	}
}
