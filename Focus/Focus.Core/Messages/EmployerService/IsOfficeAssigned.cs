﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class IsOfficeAssignedRequest : ServiceRequest
	{
		[DataMember]
		public long OfficeId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class IsOfficeAssignedResponse : ServiceResponse
	{
		public IsOfficeAssignedResponse() { }

		public IsOfficeAssignedResponse(IsOfficeAssignedRequest request) : base(request) { }

		[DataMember]
		public List<ErrorTypes> Errors { get; set; }

		[DataMember]
		public bool IsAssigned { get; set; }
	}
}
