﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.BusinessUnitAddress;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class BusinessUnitAddressRequest : ServiceRequest
	{
		[DataMember]
		public BusinessUnitAddressCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class BusinessUnitAddressResponse : ServiceResponse
	{
		public BusinessUnitAddressResponse() {}

		public BusinessUnitAddressResponse(BusinessUnitAddressRequest request) : base(request) {}

		[DataMember]
		public PagedList<BusinessUnitAddressDto> BusinessUnitAddressesPaged { get; set; }

		[DataMember]
		public List<BusinessUnitAddressDto> BusinessUnitAddresses { get; set; }

		[DataMember]
		public BusinessUnitAddressDto BusinessUnitAddress { get; set; }
	}
}
