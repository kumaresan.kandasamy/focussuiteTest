﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class AssignEmployerToAdminRequest : ServiceRequest
	{
		[DataMember]
		public long EmployeeId { get; set; }

		[DataMember]
		public long AdminId { get; set; }
		
		[DataMember]
		public string ExternalAdminId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class AssignEmployerToAdminResponse : ServiceResponse
	{
		public AssignEmployerToAdminResponse(){}

		public AssignEmployerToAdminResponse(AssignEmployerToAdminRequest request) : base(request){}

    [DataMember]
    public bool AssignSuccessful { get; set; }
	}
}
