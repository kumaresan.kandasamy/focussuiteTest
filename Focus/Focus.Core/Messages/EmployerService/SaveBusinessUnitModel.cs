﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Models;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveBusinessUnitModelRequest : ServiceRequest
	{
		[DataMember]
		public BusinessUnitModel Model { get; set; }

    [DataMember]
    public bool CheckCensorshipBusinessUnit { get; set; }

    [DataMember]
    public bool CheckCensorshipDescription { get; set; }
  }

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveBusinessUnitModelResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SaveBusinessUnitModelResponse"/> class.
		/// </summary>
		public SaveBusinessUnitModelResponse() {}

		/// <summary>
		/// Initializes a new instance of the <see cref="SaveBusinessUnitModelResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SaveBusinessUnitModelResponse(SaveBusinessUnitModelRequest request) : base(request) {}

		[DataMember]
		public BusinessUnitModel Model { get; set; }

    [DataMember]
    public bool FailedCensorshipCheck { get; set; }
	}
}
