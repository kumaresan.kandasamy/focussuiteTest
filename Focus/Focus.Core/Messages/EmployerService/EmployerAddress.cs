﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.EmployerAddress;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployerAddressRequest : ServiceRequest
	{	
		[DataMember]
		public EmployerAddressCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployerAddressResponse : ServiceResponse
	{
		private List<EmployerAddressDto> _otherEmployerAddresses = new List<EmployerAddressDto>();
		private List<EmployerAddressDto> _allEmployerAddresses = new List<EmployerAddressDto>();

		/// <summary>
		/// Initializes a new instance of the <see cref="EmployerAddressResponse"/> class.
		/// </summary>
		public EmployerAddressResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="EmployerAddressResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public EmployerAddressResponse(EmployerAddressRequest request) : base(request) { }

		/// <summary>
		/// Gets or sets the primary employer address.
		/// </summary>
		/// <value>The primary employer address.</value>
		[DataMember]
		public EmployerAddressDto PrimaryEmployerAddress { get; set; }

		/// <summary>
		/// Gets or sets the other employer addresses.
		/// </summary>
		/// <value>The other employer addresses.</value>
		[DataMember]
		public List<EmployerAddressDto> OtherEmployerAddresses
		{
			get { return _otherEmployerAddresses; }
			set { _otherEmployerAddresses = value; }
		}

		/// <summary>
		/// Gets or sets all employer addresses.
		/// </summary>
		/// <value>All employer addresses.</value>
		[DataMember]
		public List<EmployerAddressDto> AllEmployerAddresses
		{
			get { return _allEmployerAddresses; }
			set
			{
				_allEmployerAddresses = value;
				if (_allEmployerAddresses.Count == 0) return;

				if (PrimaryEmployerAddress == null)
					PrimaryEmployerAddress = _allEmployerAddresses.Where(x => x.IsPrimary).SingleOrDefault();

				if (OtherEmployerAddresses.Count == 0)
					OtherEmployerAddresses = _allEmployerAddresses.Where(x => !x.IsPrimary).ToList();
			}
		}
	}
}
