﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.BusinessUnitDescription;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class BusinessUnitDescriptionRequest : ServiceRequest
	{
		[DataMember]
		public BusinessUnitDescriptionCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class BusinessUnitDescriptionResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="BusinessUnitDescriptionResponse"/> class.
		/// </summary>
		public BusinessUnitDescriptionResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="BusinessUnitResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public BusinessUnitDescriptionResponse(BusinessUnitDescriptionRequest request) : base(request) { }

		[DataMember]
		public PagedList<BusinessUnitDescriptionDto> BusinessUnitDescriptionsPaged { get; set; }

		[DataMember]
		public List<BusinessUnitDescriptionDto> BusinessUnitDescriptions { get; set; }

		[DataMember]
		public BusinessUnitDescriptionDto BusinessUnitDescription { get; set; }
	}
}
