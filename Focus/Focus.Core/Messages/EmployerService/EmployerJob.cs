﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.Job;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployerJobRequest : ServiceRequest
	{
		[DataMember]
		public JobCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployerJobResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmployerJobResponse"/> class.
		/// </summary>
		public EmployerJobResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="EmployerJobTitleResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public EmployerJobResponse(EmployerJobRequest request) : base(request) { }

		/// <summary>
		/// Gets or sets the jobs.
		/// </summary>
		/// <value>The jobs.</value>
		[DataMember]
		public List<SelectView> Jobs { get; set; }
	}
}
