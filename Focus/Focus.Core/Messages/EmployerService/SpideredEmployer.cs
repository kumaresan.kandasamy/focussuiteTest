﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

using Focus.Core.Criteria.SpideredEmployer;
using Focus.Core.Models.Assist;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SpideredEmployerRequest : ServiceRequest
	{
		[DataMember]
		public SpideredEmployerCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SpideredEmployerResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SpideredEmployerResponse"/> class.
		/// </summary>
		public SpideredEmployerResponse() : base() {}

		/// <summary>
		/// Initializes a new instance of the <see cref="SpideredEmployerResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SpideredEmployerResponse(SpideredEmployerRequest request) : base(request) {}

		[DataMember]
		public PagedList<SpideredEmployerModel> SpideredEmployersPaged { get; set; } 
	}
}
