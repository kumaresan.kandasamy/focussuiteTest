﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.Employer;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class StaffMemberRequest : ServiceRequest
	{
		[DataMember]
		public StaffMemberCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class StaffMemberResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="StaffMemberResponse"/> class.
		/// </summary>
		public StaffMemberResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="StaffMemberResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public StaffMemberResponse(StaffMemberRequest request) : base(request) { }

		[DataMember]
		public PagedList<PersonModel> StaffMembersPaged { get; set; }

		[DataMember]
		public List<PersonModel> StaffMembers { get; set; }
		
		[DataMember]
		public PersonDto StaffMember { get; set; }

		[DataMember]
		public int StaffMemberCount { get; set; }
	}
}
