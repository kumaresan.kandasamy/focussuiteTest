﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.Criteria.Employer;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployerPlacementsViewRequest : ServiceRequest
	{
		[DataMember]
		public PlacementsCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployerPlacementsViewResponse : ServiceResponse
	{
		public EmployerPlacementsViewResponse() { }

		public EmployerPlacementsViewResponse(EmployerPlacementsViewRequest request) : base(request) { }

		[DataMember]
		public EmployerPlacementsView EmployerPlacementsView { get; set; }

		[DataMember]
		public PagedList<EmployerPlacementsView> EmployerPlacementsViewsPaged { get; set; }

		[DataMember]
		public List<EmployerPlacementsView> EmployerPlacementsViews { get; set; }
	}
}
