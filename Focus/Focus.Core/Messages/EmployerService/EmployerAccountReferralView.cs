﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Criteria.Employer;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployerAccountReferralViewRequest : ServiceRequest
	{
		[DataMember]
		public EmployerAccountReferralCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployerAccountReferralViewResponse : ServiceResponse
	{
		public EmployerAccountReferralViewResponse() { }

		public EmployerAccountReferralViewResponse(EmployerAccountReferralViewRequest request) : base(request) { }

		[DataMember]
		public PagedList<EmployerAccountReferralViewDto> ReferralViewsPaged { get; set; }

		[DataMember]
		public EmployerAccountReferralViewDto Referral { get; set; }
	}
}