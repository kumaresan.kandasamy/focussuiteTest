﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveOfficeRequest : ServiceRequest
	{
		[DataMember]
		public OfficeDto Office { get; set; }

    [DataMember]
    public string OfficeMigrationId { get; set; }

    [DataMember]
    public string MigrationValidationMessage { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveOfficeResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SaveOfficeResponse"/> class.
		/// </summary>
		public SaveOfficeResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SaveOfficeResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SaveOfficeResponse(SaveOfficeRequest request) : base(request) { }

		[DataMember]
		public OfficeDto Office { get; set; }
	}
}
