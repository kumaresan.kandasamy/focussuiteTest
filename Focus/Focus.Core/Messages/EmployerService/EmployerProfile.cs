﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class BusinessUnitProfileRequest : ServiceRequest
	{
    [DataMember]
    public long BusinessUnitId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class BusinessUnitProfileResponse : ServiceResponse
	{
		public BusinessUnitProfileResponse(BusinessUnitProfileRequest request) : base(request) { }

    [DataMember]
    public int Logins7Days { get; set; }

    [DataMember]
    public BusinessUnitDto BusinessUnit { get; set; }

    [DataMember]
    public List<BusinessUnitAddressDto> BusinessUnitAddresses { get; set; }

		[DataMember]
		public int EmployerLockVersion { get; set; }

    [DataMember]
    public DateTime? LastLogin { get; set; }

    [DataMember]
    public string LastLoginEmail { get; set; }

    [DataMember]
    public int NoOfJobOrdersOpen7Days { get; set; }

    [DataMember]
    public int NoOfOrdersEditedByStaff7Days { get; set; }

    [DataMember]
    public int NoOfResumesReferred7Days { get; set; }

    [DataMember]
    public int NoOfJobSeekersInterviewed7Days { get; set; }

    [DataMember]
    public int NoOfJobSeekersRejected7Days { get; set; }

    [DataMember]
    public int NoOfJobSeekersHired7Days { get; set; }

    [DataMember]
    public int NoOfJobOrdersOpen30Days { get; set; }

    [DataMember]
    public int NoOfOrdersEditedByStaff30Days { get; set; }

    [DataMember]
    public int NoOfResumesReferred30Days { get; set; }

    [DataMember]
    public int NoOfJobSeekersInterviewed30Days { get; set; }

    [DataMember]
    public int NoOfJobSeekersRejected30Days { get; set; }

    [DataMember]
    public int NoOfJobSeekersHired30Days { get; set; }

		[DataMember]
		public List<long> OfficeIds { get; set; }

	}
}
