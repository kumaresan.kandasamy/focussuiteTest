﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Models.Assist;

#endregion



namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ValidateFEINChangeRequest : ServiceRequest
	{
		[DataMember]
		public long? BusinessUnitId { get; set; }

		[DataMember]
		public string NewFEIN { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ValidateFEINChangeResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ValidateFEINChangeResponse"/> class.
		/// </summary>
		public ValidateFEINChangeResponse() : base() {}

		/// <summary>
		/// Initializes a new instance of the <see cref="ValidateFEINChangeResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public ValidateFEINChangeResponse(ValidateFEINChangeRequest request) : base(request) { }

		[DataMember]
		public ValidateFEINChangeModel ValidationResult { get; set; }
 	}
}
