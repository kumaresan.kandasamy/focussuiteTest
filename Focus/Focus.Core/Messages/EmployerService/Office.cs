﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.Employer;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class OfficeRequest : ServiceRequest
	{
		[DataMember]
		public OfficeCriteria Criteria { get; set; }

		[DataMember]
		public OfficeDto Office { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class OfficeResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="OfficeResponse"/> class.
		/// </summary>
		public OfficeResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="OfficeResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public OfficeResponse(OfficeRequest request) : base(request) { }

		[DataMember]
		public PagedList<OfficeDto> OfficesPaged { get; set; }

		[DataMember]
		public List<OfficeDto> Offices { get; set; }

		[DataMember]
		public OfficeDto Office { get; set; }

		[DataMember]
		public bool? HasAssignedPostcodes { get; set; }

		[DataMember]
		public bool IsDefaultAdministrator { get; set; }
	}
}
