﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Criteria.Employer;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployerService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetEmployerDetailsRequest : ServiceRequest
  {
    [DataMember]
    public EmployerCriteria Criteria;
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetEmployerDetailsResponse : ServiceResponse
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="GetEmployerDetailsResponse"/> class.
    /// </summary>
    public GetEmployerDetailsResponse() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="GetEmployerDetailsResponse"/> class.
    /// </summary>
    /// <param name="request">The request.</param>
    public GetEmployerDetailsResponse(GetEmployerDetailsRequest request) : base(request) { }

    [DataMember]
    public string Name { get; set; }

    [DataMember]
		public BusinessUnitDescriptionDto Description { get; set; }

    [DataMember]
    public string FederalEmployerIdentificationNumber { get; set; }

    [DataMember]
    public string StateEmployerIdentificationNumber { get; set; }

    [DataMember]
    public string IndustrialClassification { get; set; }

    [DataMember]
    public long OwnershipTypeId { get; set; }

		[DataMember]
		public long? AccountTypeId { get; set; }

    [DataMember]
    public EmployerAddressDto PrimaryAddress { get; set; }  

    [DataMember]
    public PhoneNumberDto PrimaryPhoneNumber { get; set; }  

    [DataMember]
    public PhoneNumberDto AlternatePhoneNumber1 { get; set; }

    [DataMember]
    public PhoneNumberDto AlternatePhoneNumber2 { get; set; }

		[DataMember]
		public ApprovalStatuses ApprovalStatus { get; set; }

		[DataMember]
		public bool IsAlreadyApproved { get; set; }

		[DataMember]
		public int? LockVersion { get; set; }
		
		[DataMember]
		public string LegalName { get; set; }
  }
}
