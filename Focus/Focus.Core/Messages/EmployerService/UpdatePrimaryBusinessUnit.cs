﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Models.Assist;

#endregion


namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdatePrimaryBusinessUnitRequest : ServiceRequest
	{
		[DataMember]
		public long? CurrentPrimaryBusinessUnitId { get; set; }

		[DataMember]
		public long? NewPrimaryBusinessUnitId { get; set; }
		
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class UpdatePrimaryBusinessUnitResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="UpdatePrimaryBusinessUnitResponse"/> class.
		/// </summary>
		public UpdatePrimaryBusinessUnitResponse() : base() {}

		/// <summary>
		/// Initializes a new instance of the <see cref="UpdatePrimaryBusinessUnitResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public UpdatePrimaryBusinessUnitResponse(UpdatePrimaryBusinessUnitRequest request) : base(request) {}
	}
}
