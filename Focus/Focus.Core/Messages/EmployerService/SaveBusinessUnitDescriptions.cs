﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveBusinessUnitDescriptionsRequest : ServiceRequest
	{
		[DataMember]
		public List<BusinessUnitDescriptionDto> Descriptions { get; set; }

		[DataMember]
		public long? BusinessUnitId { get; set; }

    [DataMember]
    public bool CheckCensorship { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveBusinessUnitDescriptionsResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SaveBusinessUnitDescriptionsResponse"/> class.
		/// </summary>
		public SaveBusinessUnitDescriptionsResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SaveBusinessUnitDescriptionsResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public SaveBusinessUnitDescriptionsResponse(SaveBusinessUnitDescriptionsRequest request) : base(request) { }

		[DataMember]
		public List<BusinessUnitDescriptionDto> Descriptions { get; set; }

		[DataMember]
		public List<BusinessUnitDescriptionDto> DescriptionsAssociatedWithJob { get; set; }

    [DataMember]
    public bool FailedCensorshipCheck { get; set; }
	}
}
