﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

using Focus.Core.Criteria.BusinessUnit;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployerRecentlyPlacedMatchRequest : ServiceRequest
	{
		[DataMember]
		public BusinessUnitCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmployerRecentlyPlacedMatchResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmployerRecentlyPlacedMatchResponse"/> class.
		/// </summary>
		public EmployerRecentlyPlacedMatchResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="EmployerRecentlyPlacedMatchResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public EmployerRecentlyPlacedMatchResponse(EmployerRecentlyPlacedMatchRequest request) : base(request) { }

		[DataMember]
		public PagedList<EmployerRecentlyPlacedView> RecentlyPlacedPaged { get; set; }
	}
}
