﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class OfficeJobSeekersRequest : ServiceRequest
	{
		[DataMember]
		public long OfficeId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class OfficeJobSeekersResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="OfficeJobSeekersResponse"/> class.
		/// </summary>
		public OfficeJobSeekersResponse()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="OfficeJobSeekersResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public OfficeJobSeekersResponse(OfficeJobSeekersRequest request)
			: base(request)
		{
		}

		[DataMember]
		public List<PersonDto> OfficeJobSeekers { get; set; }
	}
}
