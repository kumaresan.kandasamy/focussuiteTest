﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.EmployerService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetHiringManagerOfficeEmailRequest : ServiceRequest
	{
		[DataMember]
		public long PersonId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetHiringManagerOfficeEmailResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetHiringManagerOfficeEmailResponse"/> class.
		/// </summary>
		public GetHiringManagerOfficeEmailResponse() : base() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="GetHiringManagerOfficeEmailResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GetHiringManagerOfficeEmailResponse(GetHiringManagerOfficeEmailRequest request) : base(request) { }

		[DataMember]
		public string Email { get; set; }
	}
}
