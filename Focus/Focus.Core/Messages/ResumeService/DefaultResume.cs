﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.ResumeService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class DefaultResumeRequest : ServiceRequest
	{
		[DataMember]
		public long ResumeId { get; set; }

    [DataMember]
    public long? PersonId { get; set; }

    [DataMember]
    public bool CompletedResumesOnly { get; set; }

		[DataMember]
		public bool FetchDefaultResume { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class DefaultResumeResponse : ServiceResponse
	{
		public DefaultResumeResponse() { }
		public DefaultResumeResponse(DefaultResumeRequest request) : base(request) { }

		[DataMember]
		public long ResumeId { get; set; }

		[DataMember]
		public ResumeModel Resume { get; set; }
	}
}
