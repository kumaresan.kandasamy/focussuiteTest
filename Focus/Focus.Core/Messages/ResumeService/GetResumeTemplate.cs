﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Focus.Core.Models.Career;

namespace Focus.Core.Messages.ResumeService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetResumeTemplateRequest : ServiceRequest
  {
    [DataMember]
    public long? PersonId { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetResumeTemplateResponse : ServiceResponse
  {
    public GetResumeTemplateResponse() { }
    public GetResumeTemplateResponse(GetResumeTemplateRequest request) : base(request) { }
    
    [DataMember]
    public ResumeModel Resume { get; set; }
  }
}
