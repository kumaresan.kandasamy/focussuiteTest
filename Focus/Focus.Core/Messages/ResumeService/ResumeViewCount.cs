﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.ResumeService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ResumeViewCountRequest : ServiceRequest
	{
		[DataMember]
		public long? PersonId { get; set; }

		[DataMember]
		public DateTime FromDate { get; set; }

		[DataMember]
		public UserTypes? UserType { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ResumeViewCountResponse : ServiceResponse
	{
		public ResumeViewCountResponse() { }

		public ResumeViewCountResponse(ResumeViewCountRequest request) : base(request) { }

		[DataMember]
		public int ViewCount { get; set; }
	}
}
