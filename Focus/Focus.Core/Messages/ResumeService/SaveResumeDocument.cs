﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.ResumeService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveResumeDocumentRequest : ServiceRequest
  {
		[DataMember]
		public ResumeDocumentDto ResumeDocument { get; set; }

    [DataMember]
    public string ResumeMigrationId { get; set; }

    [DataMember]
    public string JobSeekerMigrationId { get; set; }

    [DataMember]
    public string MigrationValidationMessage { get; set; }
  }

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveResumeDocumentResponse : ServiceResponse
  {
    public SaveResumeDocumentResponse(){}

		public SaveResumeDocumentResponse(SaveResumeDocumentRequest request): base(request){}

		[DataMember]
		public ResumeDocumentDto ResumeDocument { get; set; }
  }
}
