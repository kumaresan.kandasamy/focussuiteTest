﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.ResumeJob;

#endregion

namespace Focus.Core.Messages.ResumeService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ResumeJobRequest : ServiceRequest
	{
		[DataMember]
		public ResumeJobCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ResumeJobResponse : ServiceResponse
	{
		public ResumeJobResponse() { }
		public ResumeJobResponse(ResumeJobRequest request) : base(request) { }

		[DataMember]
		public Dictionary<string, string> ResumeJobLookup { get; set; }
	}
}
