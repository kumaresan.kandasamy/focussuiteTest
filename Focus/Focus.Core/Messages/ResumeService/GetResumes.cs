﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.ResumeService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetResumesRequest : ServiceRequest
	{
		[DataMember]
		public long? PersonId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetResumesResponse : ServiceResponse
	{
		public GetResumesResponse() { }

		public GetResumesResponse(GetResumesRequest request) : base(request) { }
		
		[DataMember]
		public List<ResumeSummary> CandidateResumes { get; set; }
	}
}
