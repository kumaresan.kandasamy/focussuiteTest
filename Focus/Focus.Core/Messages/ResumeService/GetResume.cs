﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Models.Career;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.ResumeService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetResumeRequest : ServiceRequest
	{
		[DataMember]
		public long? ResumeId { get; set; }

    [DataMember]
    public long? PersonId { get; set; }

    [DataMember]
    public long? JobId { get; set; }

    [DataMember]
    public bool ReturnFullResume { get; set; }

    [DataMember]
    public string OnetCode { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetResumeResponse : ServiceResponse
	{
		public GetResumeResponse() { }

		public GetResumeResponse(GetResumeRequest request) : base(request) { }

		[DataMember]
		public ResumeModel SeekerResume { get; set; }

    [DataMember]
    public ResumeView ResumeDetails { get; set; }

    [DataMember]
    public int ResumeCount { get; set; }
	}
}
