﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Focus.Core.Models.Career;

namespace Focus.Core.Messages.ResumeService
{
		[DataContract(Namespace = Constants.DataContractNamespace)]
		public class ParseResumeRequest : ServiceRequest
		{
			[DataMember]
			public string ResumeXML { get; set; }
		}

		[DataContract(Namespace = Constants.DataContractNamespace)]
		public class ParseResumeResponse : ServiceResponse
		{
			public ParseResumeResponse() { }
			public ParseResumeResponse(ParseResumeRequest request) : base(request) { }

			[DataMember]
			public ResumeModel SeekerResume { get; set; }
		}
	
}
