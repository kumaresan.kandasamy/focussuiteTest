﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.ResumeService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ResumeConvertionRequest : ServiceRequest
	{
		[DataMember]
		public long? ResumeId { get; set; }

		[DataMember]
		public ResumeModel SeekerResume { get; set; }

		[DataMember]
		public bool Canonize { get; set; }

		[DataMember]
		public string TaggedResume { get; set; }

    [DataMember]
    public byte[] ResumeBytes { get; set; }

    [DataMember]
    public string ResumeFileExtension { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ResumeConvertionResponse : ServiceResponse
	{
		public ResumeConvertionResponse() { }
		public ResumeConvertionResponse(ResumeConvertionRequest request) : base(request) { }

		[DataMember]
		public string TaggedResume { get; set; }

		[DataMember]
		public ResumeModel SeekerResume { get; set; }
	}
}
