﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.Resume;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.ResumeService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ResumeMetaInfoRequest : ServiceRequest
	{
		[DataMember]
		public ResumeCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ResumeMetaInfoResponse : ServiceResponse
	{
		public ResumeMetaInfoResponse() { }
		public ResumeMetaInfoResponse(ResumeMetaInfoRequest request) : base(request) { }

		[DataMember]
		public ResumeEnvelope ResumeInfo { get; set; }
		
		[DataMember]
		public Dictionary<string, string> ResumeInfoLookup { get; set; }

    [DataMember]
    public int ResumeCount { get; set; }

    [DataMember]
    public List<ResumeEnvelope> AllResumeInfo { get; set; }
	}
}
