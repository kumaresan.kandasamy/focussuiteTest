﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.ResumeService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class OriginalResumeRequest : ServiceRequest
  {
		[DataMember]
		public long ResumeId { get; set; }

		[DataMember]
		public DocumentContentType ResumeType { get; set; }	
  }

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class OriginalResumeResponse : ServiceResponse
  {
		public OriginalResumeResponse() { }
		public OriginalResumeResponse(OriginalResumeRequest request) : base(request) { }

		[DataMember]
		public string HTMLResume { get; set; }

		[DataMember]
		public byte[] ResumeContent { get; set; }

		[DataMember]
		public string FileName { get; set; }

		[DataMember]
		public string MimeType { get; set; }
  }
}
