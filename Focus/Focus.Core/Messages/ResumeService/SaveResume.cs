﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Core.Messages.ResumeService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveResumeRequest : ServiceRequest
	{
		[DataMember]
		public ResumeModel SeekerResume { get; set; }

    [DataMember]
    public ResumeDocumentDto ResumeDocument { get; set; }

    [DataMember]
    public long? PersonId { get; set; }

    [DataMember]
    public DateTime? OverrideCreationDate { get; set; }

    [DataMember]
    public bool IgnoreLens { get; set; }

    [DataMember]
    public bool IgnoreClient { get; set; }

    [DataMember]
    public string ResumeMigrationId { get; set; }

    [DataMember]
    public string JobSeekerMigrationId { get; set; }

    [DataMember]
    public string MigrationValidationMessage { get; set; }

    [DataMember]
    public string ExternalResumeId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveResumeResponse : ServiceResponse
	{
		public SaveResumeResponse() { }
		public SaveResumeResponse(SaveResumeRequest request) : base(request) { }

		[DataMember]
		public ResumeModel SeekerResume { get; set; }
	}
}
