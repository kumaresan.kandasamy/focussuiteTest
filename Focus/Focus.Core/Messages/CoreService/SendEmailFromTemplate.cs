﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.EmailTemplate;

#endregion

namespace Focus.Core.Messages.CoreService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SendEmailFromTemplateRequest : ServiceRequest
  {
    [DataMember]
    public string EmailToAddresses { get; set; }

		[DataMember]
		public string CC { get; set; }

		[DataMember]
		public string BCC { get; set; }

    [DataMember]
    public EmailTemplateTypes TemplateType { get; set; }

    [DataMember]
    public EmailTemplateData TemplateData { get; set; }

		[DataMember]
		public bool SendAsync { get; set; }

    [DataMember]
    public bool IsHtml { get; set; }

    [DataMember]
    public byte[] Attachment { get; set; }

    [DataMember]
    public string AttachmentName { get; set; }

    [DataMember]
    public bool DetectUrl { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SendEmailFromTemplateResponse : ServiceResponse
  {
    public SendEmailFromTemplateResponse() { }

		public SendEmailFromTemplateResponse(SendEmailFromTemplateRequest request) : base(request) { }
  }
}
