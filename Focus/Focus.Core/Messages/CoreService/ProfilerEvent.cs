﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ProfilerEventRequest : ServiceRequest
	{
		[DataMember]
		public string Server { get; set; }

		[DataMember]
		public DateTime InTime { get; set; }

		[DataMember]
		public DateTime OutTime { get; set; }

		[DataMember]
		public long Milliseconds { get; set; }

		[DataMember]
		public string DeclaringType { get; set; }

		[DataMember]
		public string Method { get; set; }

		[DataMember]
		public string Parameters { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ProfilerEventResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ProfilerEventResponse"/> class.
		/// </summary>
		public ProfilerEventResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="ProfilerEventResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public ProfilerEventResponse(ProfilerEventRequest request) : base(request) { }
	}
}