﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.CoreService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveSelfServiceRequest : ServiceRequest
  {
    [DataMember]
    public ActionTypes Action { get; set; }

    [DataMember]
    public long? ActingOnBehalfOfUserId { get; set; }
    [DataMember]
    public long? ActingOnBehalfOfPersonId { get; set; }
  }



  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveSelfServiceResponse : ServiceResponse
  {
    public SaveSelfServiceResponse() { }
    public SaveSelfServiceResponse(SaveSelfServiceRequest request) : base(request) { }
  }
}
