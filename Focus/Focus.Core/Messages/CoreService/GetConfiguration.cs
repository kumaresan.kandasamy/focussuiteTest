﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetConfigurationRequest : ServiceRequest
	{

	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetConfigurationResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetConfigurationResponse"/> class.
		/// </summary>
		public GetConfigurationResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="GetConfigurationResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GetConfigurationResponse(GetConfigurationRequest request) : base(request) { }

    /// <summary>
    /// Gets or sets the configuration.
    /// </summary>
    /// <value>The configuration.</value>
    [DataMember]
    public IEnumerable<ConfigurationItemView> ConfigurationItems { get; set; }
	}
}
