﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class DismissMessageRequest : ServiceRequest
	{
		[DataMember]
		public long MessageId { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class DismissMessageResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="DismissMessageResponse"/> class.
		/// </summary>
		public DismissMessageResponse(){}

		/// <summary>
		/// Initializes a new instance of the <see cref="DismissMessageResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public DismissMessageResponse(DismissMessageRequest request) : base(request){}
	}
}
