﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ApplicationImageRequest : ServiceRequest
	{
		[DataMember]
		public ApplicationImageTypes Type { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ApplicationImageResponse : ServiceResponse
	{
		public ApplicationImageResponse(){}

		public ApplicationImageResponse(ApplicationImageRequest request) : base(request){}

		[DataMember]
		public ApplicationImageDto ApplicationImage { get; set; }
	}
}
