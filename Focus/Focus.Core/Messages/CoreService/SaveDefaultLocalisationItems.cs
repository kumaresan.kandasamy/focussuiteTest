﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.CoreService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveDefaultLocalisationItemsRequest : ServiceRequest
  {
    [DataMember]
		public List<KeyValuePairSerializable<string, string>> Items { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveDefaultLocalisationItemsResponse : ServiceResponse
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="SaveDefaultLocalisationItemsResponse"/> class.
    /// </summary>
    public SaveDefaultLocalisationItemsResponse()
    { }

    /// <summary>
    /// Initializes a new instance of the <see cref="SaveLocalisationItemsResponse"/> class.
    /// </summary>
    /// <param name="request">The request.</param>
		public SaveDefaultLocalisationItemsResponse(SaveDefaultLocalisationItemsRequest request) : base(request)
    { }
  }
}
