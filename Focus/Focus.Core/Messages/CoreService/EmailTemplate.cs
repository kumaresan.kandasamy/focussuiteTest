﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmailTemplateRequest : ServiceRequest
	{
		[DataMember]
		public EmailTemplateTypes EmailTemplateType { get; set; }

		[DataMember]
		public EmailTemplateData TemplateValues { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EmailTemplateResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmailTemplateResponse"/> class.
		/// </summary>
		public EmailTemplateResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="EmailTemplateResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public EmailTemplateResponse(EmailTemplateRequest request) : base(request) { }

		[DataMember]
		public EmailTemplateView EmailTemplatePreview { get; set; }

		[DataMember]
		public EmailTemplateDto EmailTemplate { get; set; }
	}
}
