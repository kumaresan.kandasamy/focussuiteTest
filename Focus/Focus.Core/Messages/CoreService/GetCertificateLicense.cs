﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.CertificateLicence;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetCertificateLicenseRequest : ServiceRequest
	{
		[DataMember]
		public CertificateLicenseCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetCertificateLicenseResponse : ServiceResponse
	{
		public GetCertificateLicenseResponse() { }
		public GetCertificateLicenseResponse(GetCertificateLicenseRequest request) : base(request) { }

		[DataMember]
		public List<CertificateLicenseView> CertificateLicenses { get; set; }
	}
}


