﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveEmailTemplateRequest : ServiceRequest
	{
		[DataMember]
		public EmailTemplateDto EmailTemplate { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveEmailTemplateResponse : ServiceResponse
	{
		public SaveEmailTemplateResponse(){}

		public SaveEmailTemplateResponse(SaveEmailTemplateRequest request) : base(request){}

		[DataMember]
		public EmailTemplateDto EmailTemplate { get; set; }
	}
}
