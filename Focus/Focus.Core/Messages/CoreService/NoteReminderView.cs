﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.NoteReminder;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class NoteReminderViewRequest : ServiceRequest
	{
		[DataMember]
		public NoteReminderCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class NoteReminderViewResponse : ServiceResponse
	{
		public NoteReminderViewResponse() {}

		public NoteReminderViewResponse(NoteReminderViewRequest request) : base(request) {}

		[DataMember]
		public NoteReminderViewDto NoteReminder { get; set; }

		[DataMember]
		public List<NoteReminderViewDto> NoteReminders { get; set; }

		[DataMember]
		public PagedList<NoteReminderViewDto> NoteRemindersPaged { get; set; }
	}
}
