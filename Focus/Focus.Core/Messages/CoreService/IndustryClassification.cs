﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.IndustryClassification;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class IndustryClassificationRequest : ServiceRequest
	{
		[DataMember]
		public IndustryClassificationCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class IndustryClassificationResponse : ServiceResponse
	{
		public IndustryClassificationResponse() : base() {}

		public IndustryClassificationResponse(IndustryClassificationRequest request) : base(request) {}

		[DataMember]
		public NAICSDto IndustryClassification { get; set; }

		[DataMember]
		public List<NAICSDto> IndustryClassifications { get; set; }

		[DataMember]
		public PagedList<NAICSDto> IndustryClassificationsPaged { get; set; }

		[DataMember]
		public Dictionary<string, string> IndustryClassificationLookup { get; set; } 
	}
}
