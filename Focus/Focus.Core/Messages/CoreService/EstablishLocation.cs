﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EstablishLocationRequest : ServiceRequest
	{
		[DataMember]
		public string PostalCode { get; set; }

    [DataMember]
    public string City { get; set; }

		[DataMember]
		public string State { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EstablishLocationResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EstablishLocationResponse"/> class.
		/// </summary>
		public EstablishLocationResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="EstablishLocationResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public EstablishLocationResponse(EstablishLocationRequest request) : base(request) { }

		[DataMember]
		public PostalCodeViewDto Location { get; set; }
	}
}
