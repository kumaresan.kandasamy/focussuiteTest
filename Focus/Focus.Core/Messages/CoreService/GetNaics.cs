﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetNaicsRequest : ServiceRequest
	{
		private int _listSize = 15;

		/// <summary>
		/// Gets or sets the prefix.
		/// </summary>
		/// <value>The prefix.</value>
    [DataMember]
		public string Prefix { get; set; }

    /// <summary>
    /// Whether to ignore NAICS records with ParentId = 0
    /// </summary>
    [DataMember]
    public bool ChildrenOnly { get; set; }

		/// <summary>
		/// If list result, what list size to fetch
		/// </summary>
    [DataMember]
    public int ListSize
		{
			get { return _listSize; }
			set { _listSize = value; }
		}
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetNaicsResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetNaicsResponse"/> class.
		/// </summary>
		public GetNaicsResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="GetNaicsResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GetNaicsResponse(GetNaicsRequest request) : base(request) { }

		/// <summary>
		/// Gets or sets the naics.
		/// </summary>
		/// <value>The naics.</value>
		[DataMember]
		public List<NaicsView> Naics { get; set; }
	}
}
