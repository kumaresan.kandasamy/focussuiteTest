﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion


#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.CoreService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class UploadFileValidationRequest : ServiceRequest
  {
    [DataMember]
    public string FileName { get; set; }

    [DataMember]
    public ValidationSchema SchemaType { get; set; }

    [DataMember]
    public UploadFileType FileType { get; set; }

    [DataMember]
    public byte[] FileBytes { get; set; }

    [DataMember]
    public bool IgnoreHeader { get; set; }

    [DataMember]
    public Dictionary<string, string> CustomInformation { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class UploadFileValidationResponse : ServiceResponse
  {
    public UploadFileValidationResponse() { }

    public UploadFileValidationResponse(UploadFileValidationRequest request) : base(request) { }

    [DataMember]
    public List<UploadRecordDto> ValidationResults { get; set; }

    [DataMember]
    public long UploadFileId { get; set; }

    [DataMember]
    public UploadFileProcessingState ProcessingState { get; set; } 
  }
}