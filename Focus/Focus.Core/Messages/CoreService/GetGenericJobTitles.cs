﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetGenericJobTitlesRequest : ServiceRequest
	{
		private int _listSize = 15;

		/// <summary>
		/// Gets or sets the job title.
		/// </summary>
		/// <value>The job title.</value>
    [DataMember]
		public string JobTitle { get; set; }

		/// <summary>
		/// If list result, what list size to fetch
		/// </summary>
    [DataMember]
    public int ListSize
		{
			get { return _listSize; }
			set { _listSize = value; }
		}
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetGenericJobTitlesResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetGenericJobTitlesResponse"/> class.
		/// </summary>
		public GetGenericJobTitlesResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="GetGenericJobTitlesResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GetGenericJobTitlesResponse(GetGenericJobTitlesRequest request) : base(request) { }

		/// <summary>
		/// Gets or sets the job titles.
		/// </summary>
		/// <value>The job titles.</value>
		[DataMember]
		public List<string> JobTitles { get; set; }
	}
}
