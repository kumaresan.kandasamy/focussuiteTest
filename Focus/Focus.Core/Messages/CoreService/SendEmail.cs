﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using System;

#endregion

namespace Focus.Core.Messages.CoreService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SendEmailRequest : ServiceRequest
  {
    [DataMember]
    public string EmailToAddresses { get; set; }

    [DataMember]
    public string EmailSubject { get; set; }

    [DataMember]
    public string EmailBody { get; set; }

    [DataMember]
    public bool IsHtml { get; set; }

    [DataMember]
    public byte[] Attachment { get; set; }

    [DataMember]
    public string AttachmentName { get; set; }

    [DataMember]
    public bool DetectUrl { get; set; }

    [DataMember]
    public DateTime? SendOnFutureDate { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SendEmailResponse : ServiceResponse
  {
    public SendEmailResponse() { }

    public SendEmailResponse(SendEmailRequest request) : base(request) { }
  }
}
