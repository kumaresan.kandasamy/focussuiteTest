﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveMessageTextsRequest : ServiceRequest
	{
		[DataMember]
		public SavedMessageDto Message { get; set; }

		[DataMember]
		public List<SavedMessageTextDto> MessageTexts { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveMessageTextsResponse : ServiceResponse
	{
		public SaveMessageTextsResponse() { }

		public SaveMessageTextsResponse(SaveMessageTextsRequest request) : base(request) { }
	}
}
