﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion


#region Using Directives

using System.Runtime.Serialization;
using System.Xml.Linq;

#endregion

namespace Focus.Core.Messages.CoreService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ValidationSchemaRequest : ServiceRequest
  {
    [DataMember]
    public ValidationSchema SchemaType { get; set; }

    [DataMember]
    public bool GetTemplate { get; set; }

  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ValidationSchemaResponse : ServiceResponse
  {
    public ValidationSchemaResponse() { }

    public ValidationSchemaResponse(ValidationSchemaRequest request) : base(request) { }

    public XDocument ValidationSchema { get; set; }

    public string Template { get; set; }
  }
}