﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

namespace Focus.Core.Messages.CoreService
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class GetActionEventRequest : ServiceRequest
    {
        [DataMember]
        public long ActionEventId { get; set; }
    }

    public class GetActionEventResponse : ServiceResponse
    {
        public GetActionEventResponse() { }
        public GetActionEventResponse(GetActionEventRequest request) : base(request) { }

        public ActionEventDto ActionEvent { get; set; }
    }
}
