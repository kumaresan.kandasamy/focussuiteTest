﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion


#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.CoreService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class UploadFileDetailsRequest : ServiceRequest
  {
    [DataMember]
    public long? UploadFileId { get; set; }

    [DataMember]
    public bool ExceptionsOnly { get; set; }

    [DataMember]
    public bool BasicDataOnly { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class UploadFileDetailsResponse : ServiceResponse
  {
    public UploadFileDetailsResponse() { }

    public UploadFileDetailsResponse(UploadFileDetailsRequest request) : base(request) { }

    [DataMember]
    public List<UploadRecordDto> ValidationResults { get; set; }

    [DataMember]
    public UploadFileProcessingState ProcessingState { get; set; }
  }
}