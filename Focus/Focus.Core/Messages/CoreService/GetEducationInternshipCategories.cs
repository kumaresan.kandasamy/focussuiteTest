﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.EducationInternship;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.CoreService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetEducationInternshipCategoriesRequest : ServiceRequest
  {
    [DataMember]
    public EducationInternshipCategoryCriteria Criteria { get; set; }

		[DataMember]
	  public bool IncludeSkills { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetEducationInternshipCategoriesResponse : ServiceResponse
  {
    public GetEducationInternshipCategoriesResponse() { }
    public GetEducationInternshipCategoriesResponse(GetEducationInternshipCategoriesRequest request) : base(request) { }

    [DataMember]
    public List<EducationInternshipCategoryDto> EducationInternshipCategories { get; set; }

    [DataMember]
    public PagedList<EducationInternshipCategoryDto> EducationInternshipCategoriesPaged { get; set; }

   [DataMember]
    public EducationInternshipCategoryDto EducationInternshipCategory { get; set; }
  } 
}
