﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.Criteria.CaseManagement;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.CoreService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CaseManagementRequest : ServiceRequest
  {
    [DataMember]
    public CaseManagementCriteria Criteria { get; set; }

    [DataMember]
    public long FailedIntegrationMessageId { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class CaseManagementResponse : ServiceResponse
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="CaseManagementResponse"/> class.
    /// </summary>
    public CaseManagementResponse() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="CaseManagementResponse"/> class.
    /// </summary>
    /// <param name="request">The request.</param>
    public CaseManagementResponse(CaseManagementRequest request) : base(request) { }

    [DataMember]
    public PagedList<FailedIntegrationMessageDto> CaseManagementMessagesPaged { get; set; }

    [DataMember]
    public List<FailedIntegrationMessageDto> CaseManagementMessages { get; set; }

    [DataMember]
    public FailedIntegrationMessageDto CaseManagementMessage { get; set; }
  }
}
