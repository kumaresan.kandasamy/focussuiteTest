﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ConfigurationItemsRequest : ServiceRequest
	{
		
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class ConfigurationItemsResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ConfigurationItemsResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public ConfigurationItemsResponse(ConfigurationItemsRequest request) : base(request) { }

		/// <summary>
		/// Gets or sets the configuration items.
		/// </summary>
		/// <value>
		/// The configuration items.
		/// </value>
		[DataMember]
		public Dictionary<string, bool> ConfigurationItems { get; set; }
	}
}