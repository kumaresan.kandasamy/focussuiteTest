﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion


#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.CoreService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class UploadFileCommitRequest : ServiceRequest
  {
    [DataMember]
    public long UploadFileId { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class UploadFileCommitResponse : ServiceResponse
  {
    public UploadFileCommitResponse() { }

    public UploadFileCommitResponse(UploadFileCommitRequest request) : base(request) { }
  }
}