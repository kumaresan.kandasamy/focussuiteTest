﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetMessageRequest : ServiceRequest
	{
		[DataMember]
		public string OrderBy { get; set; }

		[DataMember]
		public FocusModules Module { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetMessageResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetMessageResponse"/> class.
		/// </summary>
		public GetMessageResponse(){}

		/// <summary>
		/// Initializes a new instance of the <see cref="GetMessageResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GetMessageResponse(GetMessageRequest request) : base(request){}

		[DataMember]
		public List<MessageView> Messages { get; set; }
	}
}
