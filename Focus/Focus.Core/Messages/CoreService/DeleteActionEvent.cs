﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Focus.Core.Messages.CoreService
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class DeleteActionEventRequest : ServiceRequest
    {
        [DataMember]
        public long actionId { get; set; }

        [DataMember]
        public string activityTypeId { get; set; }
    }

    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class DeleteActionEventResponse : ServiceResponse
    {
        public DeleteActionEventResponse() { }
        public DeleteActionEventResponse(DeleteActionEventRequest request) : base(request) { }

        public bool isDeleted { get; set; }
    }
}
