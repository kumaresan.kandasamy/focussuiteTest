﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Focus.Core.Messages.CoreService
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class CheckLastActionRequest:ServiceRequest
    {
        [DataMember]
        public long? PersonId { get; set; }
    }

    [DataContract(Namespace = Constants.DataContractNamespace)]
     public class CheckLastActionResponse : ServiceResponse
    {
        public CheckLastActionResponse(){}
        public CheckLastActionResponse(CheckLastActionRequest request):base(request){}

        [DataMember]
        public bool IsAccountActivated { get; set; }
    }

}
