﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveConfigurationItemsRequest : ServiceRequest
	{
		[DataMember]
		public List<ConfigurationItemDto> ConfigurationItems { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveConfigurationItemsResponse : ServiceResponse
	{
		public SaveConfigurationItemsResponse(){}

		public SaveConfigurationItemsResponse(SaveConfigurationItemsRequest request) : base(request){}
	}
}
