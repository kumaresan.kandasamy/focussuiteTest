﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Common.Models;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetLocalisationDictionaryRequest : ServiceRequest
	{
    [DataMember]
    public string Key { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetLocalisationDictionaryResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetLocalisationDictionaryResponse"/> class.
		/// </summary>
		public GetLocalisationDictionaryResponse()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="GetLocalisationDictionaryResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GetLocalisationDictionaryResponse(GetLocalisationDictionaryRequest request) : base(request)
		{ }

		/// <summary>
		/// Gets or sets the localisation dictionary.
		/// </summary>
		/// <value>The localisation dictionary.</value>
		[DataMember]
		public LocalisationDictionary LocalisationDictionary { get; set; }

    /// <summary>
    /// Gets or sets the localisation dictionary entry.
    /// </summary>
    /// <value>The localisation dictionary entry.</value>
    [DataMember]
    public LocalisationDictionaryEntry LocalisationDictionaryEntry { get; set; }
	}
}
