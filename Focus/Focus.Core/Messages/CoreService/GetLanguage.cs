﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.Language;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetLanguageRequest : ServiceRequest
	{
		[DataMember]
		public LanguageCriteria Criteria { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetLanguageResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetLanguageResponse"/> class.
		/// </summary>
		public GetLanguageResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="GetLanguageResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GetLanguageResponse(GetLanguageRequest request) : base(request) { }

		[DataMember]
		public List<LookupItemView> Languages { get; set; }
	}
}
