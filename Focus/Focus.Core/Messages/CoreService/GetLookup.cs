﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetLookupRequest : ServiceRequest
	{
		[DataMember]
		public LookupTypes LookupType { get; set; }

		[DataMember]
		public string Key { get; set; }

    [DataMember]
    public long ParentId { get; set; }

    [DataMember]
    public string Prefix { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetLookupResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetLookupResponse"/> class.
		/// </summary>
		public GetLookupResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="GetLookupResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GetLookupResponse(GetLookupRequest request) : base(request) { }

		/// <summary>
		/// Gets or sets the localisation dictionary.
		/// </summary>
		/// <value>The localisation dictionary.</value>
		[DataMember]
		public IList<LookupItemView> LookupItems { get; set; }
	}
}
