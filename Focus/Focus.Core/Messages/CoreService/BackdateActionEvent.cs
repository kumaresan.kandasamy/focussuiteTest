﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using System;

#endregion


namespace Focus.Core.Messages.CoreService
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class BackdateActionEventRequest : ServiceRequest
    {
        [DataMember]
        public long activityId { get; set; }

        [DataMember]
        public DateTime backdateActivityTo { get; set; }

        [DataMember]
        public string activityTypeId { get; set; }
    }

    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class BackdateActionEventResponse : ServiceResponse
    {
        public BackdateActionEventResponse() { }
        public BackdateActionEventResponse(BackdateActionEventRequest request) : base(request) { }

    }
}
