﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.CoreService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveActivitiesRequest :ServiceRequest
  {
    [DataMember]
    public long? Id { get; set; }

    [DataMember]
    public string Name { get; set; }

    [DataMember]
    public long ActivityCategoryId { get; set; }

    [DataMember]
    public long ExternalId { get; set; }

    [DataMember]
    public bool Visible { get; set; }

    [DataMember]
    public List<ActivityViewDto> Activities { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SaveActivitiesResponse : ServiceResponse
  {
    public SaveActivitiesResponse(){}
    public SaveActivitiesResponse(SaveActivitiesRequest request) : base(request) { }

    [DataMember]
    public bool Saved { get; set; }

    [DataMember]
    public ActivityDto Activity { get; set; }
  }
}
