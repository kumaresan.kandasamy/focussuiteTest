﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.CoreService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetReferralEmailRequest : ServiceRequest
  {
    [DataMember]
    public long? ApplicationId { get; set; }

    [DataMember]
    public long? EmployeeId { get; set; }

    [DataMember]
    public ApprovalStatuses? ApprovalStatus { get; set; }

    [DataMember]
    public int? EmailsRequired { get; set; }

    public long? JobId { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetReferralEmailResponse : ServiceResponse
  {
    public GetReferralEmailResponse() { }

    public GetReferralEmailResponse(GetReferralEmailRequest request) : base(request) { }

    public List<ReferralEmailDto> EmailsSent { get; set; }
  }
}
