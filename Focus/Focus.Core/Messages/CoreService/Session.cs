﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.CoreService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SessionRequest : ServiceRequest
  {
    [DataMember]
    public string Key { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SessionRequest<T> : ServiceRequest
  {
    [DataMember]
    public string Key { get; set; }

    [DataMember]
    public T Value { get; set; }

    [DataMember]
    public bool UseValueAsDefault { get; set; }

    [IgnoreDataMember]
    public IEnumerable<Type> KnownTypes { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SessionResponse : ServiceResponse
  {
    public SessionResponse() { }

    public SessionResponse(SessionRequest request) : base(request) { }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class SessionResponse<T> : ServiceResponse
  {
    public SessionResponse() { }

    public SessionResponse(SessionRequest<T> request) : base(request) { }

    public T Value { get; set; }
  }
}


