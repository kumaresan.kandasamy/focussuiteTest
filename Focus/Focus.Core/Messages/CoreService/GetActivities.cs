﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.Criteria.Activity;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.CoreService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetActivitiesRequest : ServiceRequest
  {
    [DataMember]
    public ActivityCriteria Criteria { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class GetActivitiesResponse : ServiceResponse
  {
    public GetActivitiesResponse() { }
    public GetActivitiesResponse(GetActivitiesRequest request) : base(request) { }
    
    [DataMember]
    public List<ActivityViewDto> Activities { get; set; }
    
    [DataMember]
    public PagedList<ActivityViewDto> ActivitiesPaged { get; set; }
    
    [DataMember]
    public Dictionary<long, string> ActivitiesLookUp { get; set; }
    
    [DataMember]
    public ActivityViewDto Activity { get; set; }
  } 
}
