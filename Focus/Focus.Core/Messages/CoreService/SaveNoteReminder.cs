﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveNoteReminderRequest : ServiceRequest
	{
		[DataMember]
		public NoteReminderDto NoteReminder { get; set; }

    [DataMember]
    public List<NoteReminderRecipientDto> ReminderRecipients { get; set; }

    [DataMember]
    public string NoteReminderMigrationId { get; set; }

    [DataMember]
    public DateTime? OverrideCreatedOnDate { get; set; }

    [DataMember]
    public string JobSeekerMigrationId { get; set; }

    [DataMember]
    public string StaffMigrationId { get; set; }

    [DataMember]
    public string StaffSecondaryMigrationId { get; set; }

    [DataMember]
    public string MigrationValidationMessage { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class SaveNoteReminderResponse : ServiceResponse
	{
		public SaveNoteReminderResponse() {}

		public SaveNoteReminderResponse(SaveNoteReminderRequest request) : base(request) {}

		[DataMember]
		public NoteReminderDto NoteReminder { get; set; }
	}
}
