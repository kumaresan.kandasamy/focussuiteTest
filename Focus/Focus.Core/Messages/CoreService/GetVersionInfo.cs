﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetVersionInfoRequest : ServiceRequest
	{
		
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetVersionInfoResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetVersionInfoResponse"/> class.
		/// </summary>
		public GetVersionInfoResponse() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="GetVersionInfoResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public GetVersionInfoResponse(GetVersionInfoRequest request) : base(request) { }

		[DataMember]
		public string VersionInfo { get; set; }
	}
}


