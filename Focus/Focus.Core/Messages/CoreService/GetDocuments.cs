﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core.Criteria.Document;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Assist;
using Focus.Core.Views;

#endregion

namespace Focus.Core.Messages.CoreService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetDocumentsRequest : ServiceRequest
	{
		[DataMember]
		public DocumentCriteria Criteria { get; set; }

		[DataMember]
		public bool Grouped { get; set; }

		[DataMember]
		public DocumentDto Document { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class GetDocumentsResponse : ServiceResponse
	{
		public GetDocumentsResponse(){}

		public GetDocumentsResponse(GetDocumentsRequest request) : base(request){}

		[DataMember]
		public List<DocumentDto> Documents { get; set; }

		[DataMember]
		public List<GroupedDocumentsModel> GroupedDocuments { get; set; }

		[DataMember]
		public DocumentDto Document { get; set; }

		[DataMember]
		public List<ErrorTypes> Errors { get; set; }
	}
}
