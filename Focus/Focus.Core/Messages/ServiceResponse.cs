﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public abstract class ServiceResponse
	{
		private ErrorTypes _error = ErrorTypes.Ok;

		/// <summary>
		/// Default Constructor for ResponseBase. 
		/// </summary>
		protected ServiceResponse() { }

		/// <summary>
		/// Overloaded Constructor for ResponseBase.
		/// Sets CorrelationId from incoming Request.
		/// </summary>
		/// <param name="viewRequest">The viewRequest.</param>		
		protected ServiceResponse(ServiceRequest viewRequest)
		{
			CorrelationId = viewRequest.RequestId;
		}

		/// <summary>
		/// A flag indicating success or failure of the service response back to the 
		/// client. Default is success. 
		/// </summary>
		[DataMember]
		public AcknowledgementType Acknowledgement = AcknowledgementType.Success;

		/// <summary>
		/// CorrelationId mostly returns the RequestId back to client. 
		/// </summary>
		[DataMember]
		public Guid CorrelationId;

		/// <summary>
		/// Message back to client. Mostly used when a service viewRequest failure occurs. 
		/// </summary>
		[DataMember]
		public string Message;

		/// <summary>
		/// Exception back to client.
		/// </summary>
		[DataMember]
		public Exception Exception;

		/// <summary>
		/// Gets a value indicating whether this instance has raised exception.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance has raised exception; otherwise, <c>false</c>.
		/// </value>
		public bool HasRaisedException
		{
			get { return Exception != null; }
		}

		/// <summary>
		/// The error type (if any)
		/// </summary>
		public ErrorTypes Error
		{
			get { return _error; }
		}

		/// <summary>
		/// Sets the response including a message.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="isError">if set to <c>true</c> then this response is in error.</param>
		/// <param name="args">The args.</param>
		private void SetResponse(string message, bool isError, params object[] args)
		{
      if (!string.IsNullOrEmpty(Message))
				Message += Environment.NewLine;

			if (args != null && args.Length > 0)
				Message = string.Concat(Message, string.Format(message, args));
			else
				Message = string.Concat(Message, message);

			if (Acknowledgement == AcknowledgementType.Success && isError)
				Acknowledgement = AcknowledgementType.Failure;
		}

		/// <summary>
		/// Sets the result as successful for the.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="args">The args.</param>
		public void AddMessage(string message, params object[] args)
		{
			SetResponse(message, false, args);
		}

		/// <summary>
		/// Sets the response as successful for the.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="args">The args.</param>
		public void SetSuccess(string message, params object[] args)
		{
			SetResponse(message, false, args);
		}

		/// <summary>
		/// Sets the response as a failure.
		/// </summary>
		/// <param name="description">The description.</param>
		/// <param name="args">The args.</param>
		public void SetFailure(string description, params object[] args)
		{
			SetResponse(description, true, args);
		}

		/// <summary>
		/// Sets the response as a failure.
		/// </summary>
		/// <param name="error">The error.</param>
		public void SetFailure(ErrorTypes error)
		{
			_error = error;
			SetResponse(null, true, null);
		}

		/// <summary>
		/// Sets the response as a failure.
		/// </summary>
		/// <param name="error">The error.</param>
		/// <param name="description">The description.</param>
		/// <param name="args">The args.</param>
    public void SetFailure(ErrorTypes error, string description, params object[] args)
    {
      _error = error;
      SetResponse(description, true, args);
    }

		/// <summary>
		/// Sets the response as a failure.
		/// </summary>
		/// <param name="description">The description.</param>
		/// <param name="exception">The exception.</param>
		/// <param name="args">The args.</param>
		public void SetFailure(string description, Exception exception, params object[] args)
		{
			SetResponse(description, true, args);
			Exception = exception;
		}
	}
}
