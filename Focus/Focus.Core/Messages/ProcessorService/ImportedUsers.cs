﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.ProcessorService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ImportedUsersRequest : ServiceRequest
  {
    [DataMember]
    public MigrationEmailRecordType RecordType { get; set; }

    [DataMember]
    public string ResetPasswordUrl { get; set; }

    [DataMember]
    public int EmailsToSend { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ImportedUsersResponse : ServiceResponse
  {
    public ImportedUsersResponse() { }

    public ImportedUsersResponse(ImportedUsersRequest request) : base(request) { }

    [DataMember]
    public int EmailsSent { get; set; }
  }
}
