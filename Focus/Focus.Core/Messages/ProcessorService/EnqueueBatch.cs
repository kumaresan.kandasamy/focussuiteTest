﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

using Framework.Messaging;

#endregion

namespace Focus.Core.Messages.ProcessorService
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EnqueueBatchRequest : ServiceRequest
	{
		[DataMember]
		public string Identifier { get; set; }

    [DataMember]
    public BatchProcesses? Process { get; set; }

    [DataMember]
    public MessageSchedulePlan Schedule { get; set; }
	}

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class EnqueueBatchResponse : ServiceResponse
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EnqueueBatchResponse"/> class.
		/// </summary>
		public EnqueueBatchResponse() : base() {}

		/// <summary>
		/// Initializes a new instance of the <see cref="EnqueueBatchResponse"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public EnqueueBatchResponse(EnqueueBatchRequest request) : base(request) { }
	}
}
