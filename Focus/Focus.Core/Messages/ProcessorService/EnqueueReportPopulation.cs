﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.ProcessorService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EnqueueReportPopulationRequest : ServiceRequest
  {
    [DataMember]
    public ReportEntityType EntityType { get; set; }

    [DataMember]
    public List<long> EntityIds { get; set; }

    [DataMember]
    public bool ResetAll { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EnqueueReportPopulationResponse : ServiceResponse
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="EnqueueReportPopulationResponse"/> class.
    /// </summary>
    public EnqueueReportPopulationResponse() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="EnqueueReportPopulationResponse"/> class.
    /// </summary>
    /// <param name="request">The request.</param>
    public EnqueueReportPopulationResponse(EnqueueReportPopulationRequest request) : base(request) { }
  }
}
