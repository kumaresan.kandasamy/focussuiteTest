﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives



#endregion

using System.Runtime.Serialization;

namespace Focus.Core.Messages.ProcessorService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ResetCacheRequest : ServiceRequest
  {
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ResetCacheResponse : ServiceResponse
  {
    public ResetCacheResponse()  { }

    public ResetCacheResponse(ResetCacheRequest request) : base(request) { }
  }
}
