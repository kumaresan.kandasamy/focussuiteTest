﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Core.Messages.ProcessorService
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EncryptionRequest : ServiceRequest
  {

  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class EncryptionResponse : ServiceResponse
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="EncryptionResponse"/> class.
    /// </summary>
    public EncryptionResponse() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="EncryptionResponse"/> class.
    /// </summary>
    /// <param name="request">The request.</param>
    public EncryptionResponse(EncryptionRequest request) : base(request) { }

    [DataMember]
    public bool AlreadyProcessed { get; set; }
  }
}
