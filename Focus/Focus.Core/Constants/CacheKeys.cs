﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

namespace Focus.Core
{
	public partial class Constants
	{
		public class CacheKeys
		{
			public const string IsLicensedKey = "IsLicensedKey";
			public const string Application = "Application-{0}";
			public const string Configuration = "Configuration";
			public const string LocalisationDictionary = "Localisation-{0}";
      public const string LocalisationModeDictionary = "LocalisationMode-{0}";
      public const string LookupKey = "Lookup-{0}-{1}";
			public const string JobTitlesKey = "JobTitlesKey";
			public const string OnetKey = "Onet-{0}:{1}";
			public const string ROnetKey = "ROnet-{0}";
			public const string OnetTaskKey = "OnetTask-{0}:{1}";
			public const string CertificateLicenseKey = "CertLic-{0}";
			public const string CensorshipRulesKey = "CensorshipRules";
			public const string NaicsNamesKey = "NaicsNamesKey";
      public const string ExplorerStateAreaKey = "ExplorerStateAreaKey";
      public const string ExplorerEmployersJobsKey = "ExplorerEmployersJobsKey-{0}";
			public const string ExplorerLocationsKey = "ExplorerLocations-{0}";
			public const string ExplorerStateAreasKey = "ExplorerStateAreas-{0}";
			public const string ExplorerCareerAreasKey = "ExplorerCareerAreas-{0}";
			public const string ExplorerDegreeAliasKey = "ExplorerDegreeAliasKey-{0}";
			public const string ExplorerDegreeEducationLevelStateAreaViewKey = "ExplorerDegreeEducationLevelStateAreaViewKey-{0}-{1}-{2}";
			public const string ExplorerEmployersKey = "ExplorerEmployersKey-{0}";
			public const string ExplorerJobsKey = "ExplorerJobsKey-{0}";
			public const string ExplorerJobTitlesKey = "ExplorerJobTitles-{0}";
			public const string ExplorerSkillCategoriesKey = "ExplorerSkillCategories-{0}";
      public const string ExplorerSkillsKey = "ExplorerSkills-{0}";
      public const string ExplorerSkillsForJobsKey = "ExplorerSkillsForJobs-{0}";
      public const string ExplorerProgramAreasKey = "ExplorerProgramAreas-{0}";
      public const string ExplorerProgramAreaDegreesKey = "ExplorerProgramAreaDegrees-{0}";
      public const string ExplorerProgramAreaDegreeducationLevelsKey = "ExplorerProgramAreaDegreeEducationLevels-{0}";
      public const string ApplicationImage = "ApplicationImage-{0}";
      public const string ReportLookupKey = "ReportLookup-{0}";
      public const string ReportingLookupKey = "ReportingLookup-{0}";
      public const string ExplorerROnetCrossWalkKey = "ExplorerROnetCrossWalkKey-{0}";
      public const string MilitaryOccupationJobTitlesKey = "MilitaryOccupationJobTitlesKey";
      public const string ExplorerCurrentSchoolsDegreeJobs = "ExplorerCurrentSchoolsDegreeJobs";
			public const string CareerAreaJobsKey = "CareerAreaJobsKey";
      public const string ExternalLookUpItems = "ExternalLookUpItems";
			public const string StaffMembersKey = "StaffMembersKey";
			public const string LaborInsightMappingsKey = "LaborInsightMappingsKey";
      public const string LaborInsightCriteriaKey = "LaborInsightCriteriaKey";
      public const string LaborInsightResultsSetKey = "LaborInsightResultsSetKey";
		}
	}
}
