﻿
namespace Focus.Core
{
	public partial class Constants
	{
		public class AlertMessageKeys
		{
			public const string JobPosted = "AlertMessage.JobPosted";
			public const string JobExpiring = "AlertMessage.JobExpiring";
      public const string JobExpired = "AlertMessage.JobExpired";
      public const string JobReminder = "AlertMessage.JobReminder";
      public const string BusinessUnitReminder = "AlertMessage.BusinessUnitReminder";
      public const string EmployeeReminder = "AlertMessage.EmployeeReminder";
      public const string JobSeekerReminder = "AlertMessage.JobSeekerReminder";
			public const string JobSeekerMessage = "AlertMessage.JobSeekerMessage";
			public const string HiringManagerMessage = "AlertMessage.HiringManagerMessage";
			public const string StaffMessage = "AlertMessage.StaffMessage";
			public const string BusinessUnitMessage = "AlertMessage.BusinessUnitMessage";
		}
	}
}
