﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Focus.Core
{
  public partial class Constants
  {
    public class PostingXmlPaths
    {
      public const string JobTitlePath = "//JobDoc/posting/duties/title";
      public const string OnetPath = "//JobDoc/special/cf008";
      public const string JobDescriptionPath = "//JobDoc/posting/duties";
      public const string UrlPath = "//JobDoc/posting/contact/url";
      public const string CityPath = "//JobDoc/posting/contact/address/city";
      public const string StatePath = "//JobDoc/posting/contact/address/state";
      public const string ZipPath = "//JobDoc/posting/contact/address/postalcode";
      public const string EducationLevelPath = "//JobDoc/special/cf016";
      public const string NaicsPath = "//JobDoc/special/cf014";
    }
  }
}
