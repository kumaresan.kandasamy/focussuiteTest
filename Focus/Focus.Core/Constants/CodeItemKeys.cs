﻿using System.Linq;

namespace Focus.Core
{
	public partial class Constants
	{
		public class CodeItemKeys
		{
			/// <summary>
			/// Gets the lookup type for the code item key.
			/// If you add a new code item group it must be added here.
			/// </summary>
			/// <param name="codeItemKey">The code item key.</param>
			/// <returns></returns>
			public static LookupTypes GetLookupType(string codeItemKey)
			{
				var mappings = new []
				               	{
				               		new LookupTypeMapItem {Key = "SatisfactionLevels", Type = LookupTypes.SatisfactionLevels},
				               		new LookupTypeMapItem {Key = "Frequencies", Type = LookupTypes.Frequencies},
				               		new LookupTypeMapItem {Key = "Country", Type = LookupTypes.Countries},
													new LookupTypeMapItem {Key = "County", Type = LookupTypes.Counties},
				               		new LookupTypeMapItem {Key = "State", Type = LookupTypes.States},
													new LookupTypeMapItem {Key = "DrivingLicenceClasses", Type = LookupTypes.DrivingLicenceClasses},
													new LookupTypeMapItem {Key = "NCRCLevel", Type = LookupTypes.NCRCLevel}
				               	};

				// Get LookupType for lookup key
				var parts = codeItemKey.Split('.');
				return mappings.Single(x => x.Key == parts[0]).Type;
			}

			public class SatisfactionLevels
			{
				public const string VerySatisfied = "SatisfactionLevels.VerySatisfied";
				public const string SomewhatSatisfied = "SatisfactionLevels.SomewhatSatisfied";
				public const string SomewhatDissatisfied = "SatisfactionLevels.SomewhatDissatisfied";
				public const string VeryDissatisfied = "SatisfactionLevels.VeryDissatisfied";
			}
		
			public class Frequencies
			{
				public const string Hourly = "Frequencies.Hourly";
				public const string Daily = "Frequencies.Daily";
				public const string Weekly = "Frequencies.Weekly";
				public const string Monthly = "Frequencies.Monthly";
				public const string Yearly = "Frequencies.Yearly";
			}

			public class Countries
			{
				public const string US = "Country.US";
			}

			public class Counties
			{
				public const string UnspecifiedZZ = "County.UnspecifiedZZ";
				public const string OutsideUS = "County.OutsideUS";
			}

			public class States
			{
				public const string ZZ = "State.ZZ";
			}

			public class DrivingLicenceClasses
			{
				public const string ClassA = "DrivingLicenceClasses.ClassA";
				public const string ClassB = "DrivingLicenceClasses.ClassB";
				public const string ClassC = "DrivingLicenceClasses.ClassC";
				public const string ClassD = "DrivingLicenceClasses.ClassD";
				public const string Motorcycle = "DrivingLicenceClasses.Motorcycle";
			}

			private class LookupTypeMapItem
			{
				public string Key { get; set; }
				public LookupTypes Type { get; set; }
			}
		}
	}
}
