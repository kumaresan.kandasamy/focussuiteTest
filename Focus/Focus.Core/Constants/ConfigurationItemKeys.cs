﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

namespace Focus.Core
{
    public partial class Constants
    {
        public class ConfigurationItemKeys
        {
            public const string AppVersion = "AppVersion";
            public const string Theme = "Theme";
            public const string FocusTalentApplicationName = "FocusTalentApplicationName";
            public const string FocusAssistApplicationName = "FocusAssistApplicationName";
            public const string FocusCareerApplicationName = "FocusCareerApplicationName";
            public const string FocusReportingApplicationName = "FocusReportingApplicationName";
            public const string FocusExplorerApplicationName = "FocusExplorerApplicationName";

            public const string LogSeverity = "LogSeverity";
            public const string ProfilingEnabled = "ProfilingEnabled";
            public const string JobProofingEnabled = "JobProofingEnabled";
            public const string CachePrefix = "CachePrefix";
            public const string OnErrorEmail = "OnErrorEmail";
            public const string EmailsEnabled = "EmailsEnabled";
            public const string EmailRedirect = "EmailRedirect";
            public const string EmailLogFileLocation = "EmailLogFileLocation";
            public const string MaximumEmailLength = "MaximumEmailLength";
            public const string MaximumUserNameLength = "MaximumUserNameLength";

            public const string DemoShieldCustomerCode = "DemoShieldCustomerCode";
            public const string DemoShieldRedirectUrl = "DemoShieldRedirectUrl";

            public const string PageStatePersister = "PageStatePersister";
            public const string PageStateQueueMaxLength = "PageStateQueueMaxLength";
            public const string PageStateTimeOut = "PageStateTimeOut";
            public const string PageStateConnectionString = "PageStateConnectionString";

            public const string SupportEmail = "SupportEmail";
            public const string SupportPhone = "SupportPhone";
            public const string SupportHoursOfBusiness = "SupportHoursOfBusiness";
            public const string SupportHelpTeam = "SupportHelpTeam";
            public const string SupportDaysResponse = "SupportDaysResponse";

            public const string IrsOnlineApplicationHours = "IrsOnlineApplicationHours";
            public const string IrsOnlineApplicationUrl = "IrsOnlineApplicationUrl";

            public const string LicenseExpiration = "LE"; // LE is this way so that it doesn't make sense in the config table			
            public const string ValidClientTags = "ValidClientTags";
            public const string SmtpServer = "SmtpServer";
            public const string SystemEmailFrom = "SystemEmailFrom";
            public const string UserNameRegExPattern = "UserNameRegExPattern";
            public const string EmailAddressRegExPattern = "EmailAddressRegExPattern";
            public const string PasswordRegExPattern = "PasswordRegExPattern";
            public const string UrlRegExPattern = "UrlRegExPattern";
            public const string PhoneNumberRegExPattern = "PhoneNumberRegExPattern";
            public const string PhoneNumberStrictRegExPattern = "PhoneNumberStrictRegExPattern";
            public const string PhoneNumberMaskPattern = "PhoneNumberMaskPattern";
            public const string NonUsPhoneNumberMaskPattern = "NonUsPhoneNumberMaskPattern";
            public const string PhoneNumberFormat = "PhoneNumberFormat";
            public const string DiacriticalMarkCheck = "DiacriticalMarkCheck";

            public const string AllowTalentCompanyInformationUpdate = "AllowTalentCompanyInformationUpdate";
            public const string ResumeUploadAllowedFileRegExPattern = "ResumeUploadAllowedFileRegExPattern";
            public const string ResumeUploadMaximumAllowedFileSize = "ResumeUploadMaximumAllowedFileSize";
            public const string TalentPoolSearchMaximumRecordsToReturn = "TalentPoolSearchMaximumRecordsToReturn";
            public const string StarRatingMinimumScores = "StarRatingMinimumScores";
            public const string DistanceUnits = "DistanceUnits";
            public const string DefaultJobExpiry = "DefaultJobExpiry";
            public const string SimilarJobsCount = "SimilarJobsCount";
            public const string SessionTimeout = "SessionTimeout";
            public const string UserSessionTimeout = "UserSessionTimeout";
            public const string DisplayServerName = "DisplayServerName";
            public const string UseSqlSessionState = "UseSqlSessionState";
            public const string EmployeeSearchMaximumRecordsToReturn = "EmployeeSearchMaximumRecordsToReturn";
            public const string TalentApplicationPath = "TalentApplicationPath";
            public const string AssistApplicationPath = "AssistApplicationPath";
            public const string CareerApplicationPath = "CareerApplicationPath";
            public const string ShowBurningGlassLogoInTalent = "ShowBurningGlassLogoInTalent";
            public const string ShowBurningGlassLogoInAssist = "ShowBurningGlassLogoInAssist";
            public const string ShowBurningGlassLogoInCareerExplorer = "ShowBurningGlassLogoInCareerExplorer";
            public const string DisplaySurveyFrequency = "DisplaySurveyFrequency";
            public const string SurveyFrequency = "SurveyFrequency";
            public const string PasswordValidationKeyExpiryHours = "PasswordValidationKeyExpiryHours";
            public const string ShowBurningGlassLogoInExplorer = "ShowBurningGlassLogoInExplorer";
            public const string NewEmployerApproval = "NewEmployerApproval";
            public const string NewEmployerCensorshipFiltering = "NewEmployerCensorshipFiltering";
            public const string AllJobPostingsRequireApproval = "AllJobPostingsRequireApproval";
            public const string ActivationYellowWordFiltering = "ActivationYellowWordFiltering";
            public const string EmployerPreferencesOverideSystemDefaults = "EmployerPreferencesOverideSystemDefaults";
            public const string FeedbackEmailAddress = "FeedbackEmailAddress";
            public const string SendHiringFromTaxCreditProgramNotification = "SendHiringFromTaxCreditProgramNotification";
            public const string JobPostingStylesheet = "JobPostingStylesheet";
            public const string FullResumeStylesheet = "FullResumeStylesheet";
            public const string PartialResumeStylesheet = "PartialResumeStylesheet";
            public const string RegistrationCompleteAction = "RegistrationCompleteAction";
            public const string FEINRegExPattern = "FEINRegExPattern";
            public const string SEINRegExPattern = "SEINRegExPattern";
            public const string SEINMaskPattern = "SEINMaskPattern";
            public const string RegistrationRoute = "RegistrationRoute";
            public const string DummyUsernameFormat = "DummyUsernameFormat";
            public const string DummyEmailFormat = "DummyEmailFormat";
            public const string AuthenticateAssistUserViaClient = "AuthenticateAssistUserViaClient";
            public const string ClientAllowsPasswordReset = "ClientAllowsPasswordReset";
            public const string TalentLightColour = "TalentLightColour";
            public const string TalentDarkColour = "TalentDarkColour";
            public const string AssistLightColour = "AssistLightColour";
            public const string AssistDarkColour = "AssistDarkColour";
            public const string CareerExplorerLightColour = "CareerExplorerLightColour";
            public const string CareerExplorerDarkColour = "CareerExplorerDarkColour";
            public const string ShowExpIndustries = "ShowExpIndustries";
            public const string SearchByIndustries = "SearchByIndustries";
            public const string JobExpiryCountdownDays = "JobExpiryCountdownDays";
            public const string ProcessExpiredJobsRunning = "ProcessExpiredJobsRunning";
            public const string ProcessExpiredJobsLastRun = "ProcessExpiredJobsLastRun";
            public const string ProcessExpiringJobsRunning = "ProcessExpiringJobsRunning";
            public const string ProcessExpiringJobsLastRun = "ProcessExpiringJobsLastRun";
            public const string ProcessEmailAlertsRunning = "ProcessEmailAlertsRunning";
            public const string ProcessEmailAlertsLastRun = "ProcessEmailAlertsLastRun";
            public const string ProcessQueuedInvitesAndRecommendationsRunning = "ProcessQueuedInvitesAndRecommendationsRunning";
            public const string ProcessQueuedInvitesAndRecommendationsLastRun = "ProcessQueuedInvitesAndRecommendationsLastRun";
            public const string DefaultStateKey = "DefaultStateKey";
            public const string JobSeekerReferralsEnabled = "JobSeekerReferralsEnabled";
            public const string MaximumNumberOfDaysCanExtendJobBy = "MaximumNumberOfDaysCanExtendJobBy";
            public const string MaximumSalary = "MaximumSalary";
            public const string MinimumSalary = "MinimumSalary";
            public const string AdjustSalaryRangeByType = "AdjustSalaryRangeByType";
            public const string HideSalary = "HideSalary";
            public const string MaximumSavedSearches = "MaximumSavedSearches";
            public const string PostalCodeRegExPattern = "PostalCodeRegExPattern";
            public const string ExtendedPostalCodeRegExPattern = "ExtendedPostalCodeRegExPattern";
            public const string PostalCodeMaskPattern = "PostalCodeMaskPattern";
            public const string ExtendedPostalCodeMaskPattern = "ExtendedPostalCodeMaskPattern";
            public const string LaborInsightAccess = "LaborInsightAccess";
            public const string LaborInsightLinkUrl = "LaborInsightLinkUrl";
            public const string ConfidentialEmployersEnabled = "ConfidentialEmployersEnabled";
            public const string ConfidentialEmployersDefault = "ConfidentialEmployersDefault";
            public const string DirectEmails = "DirectEmails";
            public const string ASyncEmails = "ASyncEmails";
            public const string RecentPlacementTimeframe = "RecentPlacementTimeframe";
            public const string MaximumRecentlyViewedPostings = "MaximumRecentlyViewedPostings";
            public const string RecommendedMatchesMinimumStars = "RecommendedMatchesMinimumStars";
            public const string ResumeReferralApprovalsMinimumStars = "ResumeReferralApprovalsMinimumStars";
            public const string ShowLicenseRequirement = "ShowLicenseRequirement";
            public const string ShowStudentExternalId = "ShowStudentExternalId";
            public const string CheckCriminalRecordExclusion = "CheckCriminalRecordExclusion";
            public const string CareerExplorerModule = "CareerExplorerModule";
            public const string DefaultDrivingLicenseType = "DefaultDrivingLicenseType";
            public const string DisableAssistAuthentication = "DisableAssistAuthentication";
            public const string DisableTalentAuthentication = "DisableTalentAuthentication";
            public const string TalentPoolSearch = "TalentPoolSearch";
            public const string DocumentStoreServiceUrl = "DocumentStoreServiceUrl";
            public const string OfficesEnabled = "OfficesEnabled";
            public const string RecaptchaEnabled = "RecaptchaEnabled";
            public const string RecaptchaPublicKey = "RecaptchaPublicKey";
            public const string RecaptchaPrivateKey = "RecaptchaPrivateKey";
            public const string RecaptchaAuthorizeAfterFirstCheck = "RecaptchaAuthorizeAfterFirstCheck";
            public const string SplitJobsByLocation = "SplitJobsByLocation";
            public const string TalentModulePresent = "TalentModulePresent";
            public const string BrandIdentifier = "BrandIdentifier";
            public const string PagerDropdownLimit = "PagerDropdownLimit";
            public const string HideScreeningPreferences = "HideScreeningPreferences";
            public const string HideInterviewContactPreferences = "HideInterviewContactPreferences";
            public const string MeetsMinimumWageRequirementCheckBox = "MeetsMinimumWageRequirementCheckBox";
            public const string DaysForDefaultJobClosingDate = "DaysForDefaultJobClosingDate";
            public const string DaysForMinimumJobClosingDate = "DaysForMinimumJobClosingDate";
            public const string DaysForJobPostingLifetime = "DaysForJobPostingLifetime";
            public const string DaysForFederalContractorLifetime = "DaysForFederalContractorLifetime";
            public const string FederalContratorExpiryDateMandatory = "FederalContratorExpiryDateMandatory";
            public const string RequirementsPreferenceDefault = "RequirementsPreferenceDefault";
            public const string DaysForForeignLaborH2ALifetime = "DaysForForeignLaborH2ALifetime";
            public const string DaysForForeignLaborH2BLifetime = "DaysForForeignLaborH2BLifetime";
            public const string DaysForForeignLaborOtherLifetime = "DaysForForeignLaborOtherLifetime";
            public const string ClientApplyToPostingUrl = "ClientApplyToPostingUrl";
            public const string HideDrivingLicence = "HideDrivingLicence";
            public const string HidePersonalInformation = "HidePersonalInformation";
            public const string HideReferences = "HideReferences";
            public const string HideEnrollmentStatus = "HideEnrollmentStatus";
            public const string ShowBrandingStatement = "ShowBrandingStatement";
            public const string HideWOTC = "HideWOTC";
            public const string HideMinimumAge = "HideMinimumAge";
            public const string HideJobConditions = "HideJobConditions";
            public const string PreScreeningServiceRequest = "PreScreeningServiceRequest";
            public const string HasMinimumHoursPerWeek = "HasMinimumHoursPerWeek";
            public const string NewBusinessUnitApproval = "NewBusinessUnitApproval";
            public const string NewHiringManagerApproval = "NewHiringManagerApproval";
            public const string ScreeningPrefExclusiveToMinStarMatch = "ScreeningPrefExclusiveToMinStarMatch";
            public const string HasJobUpload = "HasJobUpload";
            public const string DisplayJobActivityLogInTalent = "DisplayJobActivityLogInTalent";
            public const string EnableTalentPasswordSecurity = "EnableTalentPasswordSecurity";
            public const string ShowPostingHowToApply = "ShowPostingHowToApply";
            public const string PartTimeEmploymentLimit = "PartTimeEmploymentLimit";
            public const string MandatoryHours = "MandatoryHours";
            public const string MandatoryHoursPerWeek = "MandatoryHoursPerWeek";
            public const string MandatoryJobType = "MandatoryJobType";
            public const string ShowAssistMultipleJSSearchButtons = "ShowAssistMultipleJSSearchButtons";
            public const string ClientSpecificExternalJobseekerIDRegex = "ClientSpecificExternalJobseekerIDRegex";
            public const string MaxSearchableIDs = "MaxSearchableIDs";
            public const string MaxStaffForOffices = "MaxStaffForOffices";
            public const string EnablePreferredEmployers = "EnablePreferredEmployers";
            public const string MultipleJobseekerInvitesEnabled = "MultipleJobseekerInvitesEnabled";
            public const string ShowCustomMessageTemplate = "ShowCustomMessageTemplate";
            public const string JobTitleRequired = "JobTitleRequired";
            public const string HideSeasonalDuration = "HideSeasonalDuration";
            public const string ShowLocalisedEduLevels = "ShowLocalisedEduLevels";
            public const string ShowTargetIndustries = "ShowTargetIndustries";
            public const string ShowWorkNetLink = "ShowWorkNetLink";
            public const string WorkNetLink = "WorkNetLink";
            public const string SystemDefaultSenderEmailAddress = "SystemDefaultSenderEmailAddress";
            public const string SelectiveServiceRegistration = "SelectiveServiceRegistration";
            public const string AccountTypeDisplayType = "AccountTypeDisplayType";
            public const string ShowEmployeeNumbers = "ShowEmployeeNumbers";
            public const string JobDescriptionCharsShown = "JobDescriptionCharsShown";
            public const string HideYearsOfExpInSearchResults = "HideYearsOfExpInSearchResults";
            public const string ShowYearsOfExperienceInPostingResults = "ShowYearsOfExperienceInPostingResults";
            public const string ShowHomeLocationInSearchResults = "ShowHomeLocationInSearchResults";
            public const string ShowPersonalEmail = "ShowPersonalEmail";
            public const string EnableDefaultOfficePerRecordType = "EnableDefaultOfficePerRecordType";
            public const string HiringManagerTalentPoolSearch = "HiringManagerTalentPoolSearch";
            public const string ExtendedVeteranPriorityofService = "ExtendedVeteranPriorityofService";
            public const string ShowWorkLocationFullAddress = "ShowWorkLocationFullAddress";
            public const string BusinessUnitTalentPoolSearch = "BusinessUnitTalentPoolSearch";
            public const string HideJSCharacteristicsFromReporting = "HideJSCharacteristicsFromReporting";
            public const string HideJSCharacteristicsFromMessages = "HideJSCharacteristicsFromMessages";
            public const string MaximumNumberJobOpenings = "MaximumNumberJobOpenings";

            public const string AutoDenyJobseekerTimePeriod = "AutoDenyJobseekerTimePeriod";

            public const string AssistShowChangeSSNActivityInWF = "AssistShowChangeSSNActivityInWF";
            public const string DisplayFEIN = "DisplayFEIN";
            public const string EditFEIN = "EditFEIN";
            public const string DocumentFileTypes = "DocumentFileTypes";

            public const string NumberOfHiringManagersOnEmployerProfile = "NumberOfHiringManagersOnEmployerProfile";
            public const string UnderAgeJobSeekerRestrictionThreshold = "UnderAgeJobSeekerRestrictionThreshold";
            public const string MinimumCareerAgeThreshold = "MinimumCareerAgeThreshold";
            public const string UseOfficeAddressToSendMessages = "UseOfficeAddressToSendMessages";
            public const string ShowAssetAccess = "ShowAssetAccess";
            public const string AssetUrl = "AssetUrl";

            public const string SiteUnderMaintenance = "SiteUnderMaintenance";

            public const string StatisticsArchiveInDays = "StatisticsArchiveInDays";

            public const string ReadOnlyResumeDOB = "ReadOnlyResumeDOB";

            public const string EnableOfficeDefaultAdmin = "EnableOfficeDefaultAdmin";
            public const string ShowAllOfficesReferralRequestQueue = "ShowAllOfficesReferralRequestQueue";

            public const string HideMocCodes = "HideMocCodes";
            public const string MandatoryJSJobAlerts = "MandatoryJSJobAlerts";
            public const string ShowMilitaryStatus = "ShowMilitaryStatus";
            public const string MaxPreferredSalaryThreshold = "MaxPreferredSalaryThreshold";
            public const string AllowDateBoundMilitaryService = "AllowDateBoundMilitaryService";
            public const string EnableJobSearchByJobId = "EnableJobSearchByJobId";

            public const string UnsubscribeFromJSMessages = "UnsubscribeFromJSMessages";

            public const string UnsubscribeEmailFooter = "UnsubscribeEmailFooter";

            public const string UnsubscribeFromHMMessages = "UnsubscribeFromHMMessages";

            public const string NoFixedLocation = "NoFixedLocation";

            public const string JobDistributionFeeds = "JobDistributionFeeds";
            public const string EnableOptionalAlienRegExpiryDate = "EnableOptionalAlienRegExpiryDate";
            public const string ShowBridgestoOppsInterest = "ShowBridgestoOppsInterest";
            public const string BridgestoOppsInterestActivityExternalId = "BridgestoOppsInterestActivityExternalId";
            public const string ShowSSNStatusMessage = "ShowSSNStatusMessage";
            public const string MaximumGradePointAverage = "MaximumGradePointAverage";
            public const string MaxDateBoundMilitaryServicePeriods = "MaxDateBoundMilitaryServicePeriods";

            public const string ShowTalentActivityReport = "ShowTalentActivityReport";
            public const string EnableAssistErrorHandling = "EnableAssistErrorHandling";
            public const string ShowStaffAssistedLMI = "ShowStaffAssistedLMI";

            public const string PostingFooterTitle = "PostingFooterTitle";

            public const string ShowCreditCheck = "ShowCreditCheck";

            public const string TEGLNoticeToEmployersURL = "TEGLNoticeToEmployersURL";

            public const string TEGLNoticeToJobSeekersURL = "TEGLNoticeToJobSeekersURL";

            public const string TalentCreditCheckJobPostingWarning = "TalentCreditCheckJobPostingWarning";

            public const string AssistCreditCheckJobPostingWarning = "AssistCreditCheckJobPostingWarning";

            public const string TEGLNoticeToStaffMembersURL = "TEGLNoticeToStaffMembersURL";

            public const string UserNameMinLengthRegExPattern = "UserNameMinLengthRegExPattern";

            public const string ReferralRequestApprovalQueueOfficeFilterType = "ReferralRequestApprovalQueueOfficeFilterType";

            public const string JobSeekerCreditCheckJobPostingWarning = "JobSeekerCreditCheckJobPostingWarning";

            public const string ShowCriminalBackgroundCheck = "ShowCriminalBackgroundCheck";

            public const string JobSeekerCriminalBackgroundJobPostingWarning = "JobSeekerCriminalBackgroundJobPostingWarning";

            public const string ShowSupplyDemandReport = "ShowSupplyDemandReport";

            #region Custom Header/Footer

            public const string UseCustomHeaderHtml = "UseCustomHeaderHtml";
            public const string UseCustomFooterHtml = "UseCustomFooterHtml";
            public const string HideTalentAssistAccountSettings = "HideTalentAssistAccountSettings";
            public const string HideTalentAssistSignOut = "HideTalentAssistSignOut";
            public const string HideUploadMultipleJobs = "HideUploadMultipleJobs";
            public const string HideCareerSignIn = "HideCareerSignIn";
            public const string HideCareerRegister = "HideCareerRegister";
            public const string HideCareerMyAccount = "HideCareerMyAccount";
            public const string HideCareerSignOut = "HideCareerSignOut";
            public const string CustomHeaderHtml = "CustomHeaderHtml";
            public const string CustomFooterHtml = "CustomFooterHtml";
            public const string ShowCareerHelpLink = "ShowCareerHelpLink";
            public const string UseUsernameAsAccountLink = "UseUsernameAsAccountLink";
            public const string HideLegalNoticeLink = "HideLegalNoticeLink";

            #endregion

            public const string RecentlyPlacedDays = "RecentlyPlacedDays";

            public const string SaveDefaultLocalisationItems = "SaveDefaultLocalisationItems";

            #region Job Development

            public const string JobDevelopmentScoreThreshold = "JobDevelopmentScoreThreshold";
            public const string JobDevelopmentSpideredJobSearchMatchesMinimumStars = "JobDevelopmentSpideredJobSearchMatchesMinimumStars";
            public const string JobDevelopmentSpideredJobListNumberOfPostingsRequired = "JobDevelopmentSpideredJobListNumberOfPostingsRequired";
            public const string JobDevelopmentSpideredJobListMaximumNumberOfEmployers = "JobDevelopmentSpideredJobListMaximumNumberOfEmployers";
            public const string JobDevelopmentSpideredJobListPostingsToDisplay = "JobDevelopmentSpideredJobListPostingsToDisplay";
            public const string JobDevelopmentSpideredJobListPostingAge = "JobDevelopmentSpideredJobListPostingAge";
            public const string JobDevelopmentEmployerPlacementsNoOfDays = "JobDevelopmentEmployerPlacementsNoOfDays";
            public const string JobDevelopmentMinimumRecentMatchesCount = "JobDevelopmentMinimumRecentMatchesCount";
            public const string JobDevelopmentMaximumPostingMatchesCount = "JobDevelopmentMaximumPostingMatchesCount";

            #endregion

            #region Find Employer settings

            public const string FindEmployerFEINsAllowed = "FindEmployerFEINsAllowed";
            public const string FindEmployerFEINsScrolling = "FindEmployerFEINsScrolling";

            #endregion

            #region Approval settings

            public const string AutoApprovalOfReferrals = "AutoApprovalOfReferrals";

            public const string ApprovalDefaultForCustomerCreatedJobs = "ApprovalDefaultForCustomerCreatedJobs";
            public const string ApprovalChecksForCustomerCreatedJobs = "ApprovalChecksForCustomerCreatedJobs";

			public const string ApprovalDefaultForStaffCreatedJobs = "ApprovalDefaultForStaffCreatedJobs";
			public const string ApprovalChecksForStaffCreatedJobs = "ApprovalChecksForStaffCreatedJobs";
            public const string ActivateYellowWordFilteringForStaffCreatedJobs = "ActivateYellowWordFilteringForStaffCreatedJobs";

            #endregion

            #region Live Jobs Repository

            public const string LiveJobsApiRootUrl = "LiveJobsApiRootUrl";
            public const string LiveJobsApiKey = "LiveJobsApiKey";
            public const string LiveJobsApiSecret = "LiveJobsApiSecret";
            public const string LiveJobsApiToken = "LiveJobsApiToken";
            public const string LiveJobsApiTokenSecret = "LiveJobsApiTokenSecret";

            #endregion

            public const string LaborRegulationsUrl = "LaborRegulationsUrl";

            public const string SchoolName = "SchoolName";
            public const string SchoolType = "SchoolType";
            public const string SchoolStateAreaId = "SchoolStateAreaId";

            public const string DisplayResumeBuilderDuration = "DisplayResumeBuilderDuration";

            public const string MessageBusUsername = "MessageBusUsername";
            public const string BatchProcessSettings = "BatchProcessSettings";
            public const string DrivingLicenceEndorsementRules = "DrivingLicenceEndorsementRules";

            public const string OfficeExternalIdMandatory = "OfficeExternalIdMandatory";

            public const string ScreeningPrefBelowStarMatchApproval = "ScreeningPrefBelowStarMatchApproval";

            public const string ScreeningPrefExclusiveToMinStarMatchDefault = "ScreeningPrefExclusiveToMinStarMatchDefault";

            public const string DisplayPoorResumeAnnouncement = "DisplayPoorResumeAnnouncement";
            public const string EnableEmployerActivityDateSelection = "EnableEmployerActivityDateSelection";

            public const string CheckExternalSystemOnSeekerSearch = "CheckExternalSystemOnSeekerSearch";
            public const string SetExternalIdOnSpideredPostings = "SetExternalIdOnSpideredPostings";
            public const string AllowSelfReferralOnSpideredPostingsWithNoResume = "AllowSelfReferralOnSpideredPostingsWithNoResume";
            public static string ReadOnlyContactInfo = "ReadOnlyContactInfo";
            public const string DisplayCareerAccountType = "DisplayCareerAccountType";
            public const string BusinessTerminology = "BusinessTerminology";
            public const string BusinessesTerminology = "BusinessesTerminology";

            public const string EnableCDN = "EnableCDN";

            #region Assist Job Issues

            // Job issue settings.
            public const string JobIssueMinimumMatchCountThreshold = "JobIssueMinimumMatchCountThreshold";
            public const string JobIssueMatchMinimumStarsThreshold = "JobIssueMatchMinimumStarsThreshold";
            public const string JobIssueMatchDaysThreshold = "JobIssueMatchDaysThreshold";
            public const string JobIssueMinimumReferralsCountThreshold = "JobIssueMinimumReferralsCountThreshold";
            public const string JobIssueReferralCountDaysThreshold = "JobIssueReferralCountDaysThreshold";
            public const string JobIssueNotClickingReferralsDaysThreshold = "JobIssueNotClickingReferralsDaysThreshold";
            public const string JobIssueSearchesWithoutInviteThreshold = "JobIssueSearchesWithoutInviteThreshold";
            public const string JobIssueSearchesWithoutInviteDaysThreshold = "JobIssueSearchesWithoutInviteDaysThreshold";
            public const string JobIssuePostClosedEarlyDaysThreshold = "JobIssuePostClosedEarlyDaysThreshold";
            public const string JobIssueHiredDaysElapsedThreshold = "JobIssueHiredDaysElapsedThreshold";

            // Job issue switches.
            public const string JobIssueFlagLowMinimumStarMatches = "JobIssueFlagLowMinimumStarMatches";
            public const string JobIssueFlagLowReferrals = "JobIssueFlagLowReferrals";
            public const string JobIssueFlagNotClickingReferrals = "JobIssueFlagNotClickingReferrals";
            public const string JobIssueFlagSearchingNotInviting = "JobIssueFlagSearchingNotInviting";
            public const string JobIssueFlagClosingDateRefreshed = "JobIssueFlagClosingDateRefreshed";
            public const string JobIssueFlagJobClosedEarly = "JobIssueFlagJobClosedEarly";
            public const string JobIssueFlagJobAfterHiring = "JobIssueFlagJobAfterHiring";
            public const string JobIssueFlagNegativeFeedback = "JobIssueFlagNegativeFeedback";
            public const string JobIssueFlagPositiveFeedback = "JobIssueFlagPositiveFeedback";

            #endregion

            #region Assist Activities and Services settings
            public const string EnableJobSeekerActivitiesServices = "EnableJobSeekerActivitiesServices";
            public const string EnableHiringManagerActivitiesServices = "EnableHiringManagerActivitiesServices";
            #endregion

            #region Assist Activities/Services Feature Configurations
            //Job Seeker Config keys
            public const string EnableJSActivityBackdatingDateSelection = "EnableJSActivityBackdatingDateSelection";

            //Employer Config keys
            public const string EnableHiringManagerActivityBackdatingDateSelection = "EnableHiringManagerActivityBackdatingDateSelection";
            #endregion

            #region Assist Activities Backdate Feature Configurations - DEPRECATED
            //public const string BackdateEmployerActivityMaxDays = "BackdateEmployerActivityMaxDays"; 
            #endregion

            #region SSO

            public const string SSOReturnUrl = "SSOReturnUrl";
            public const string SSOManageAccountUrl = "SSOManageAccountUrl";
            public const string SSOChangeUsernamePasswordUrl = "SSOChangeUsernamePasswordUrl";
            public const string SSOErrorRedirectUrl = "SSOErrorRedirectUrl";
            public const string SSOEnableDeactivateJobSeeker = "SSOEnableDeactivateJobSeeker";
            public const string SSOForceNewUserToRegistration = "SSOForceNewUserToRegistration";
            public const string SSOEnabledForCareer = "SSOEnabledForCareer";

            #region oAuth

            public const string OAuthEnabled = "OAuthEnabled";
            public const string OAuthConsumerKey = "OAuthConsumerKey";
            public const string OAuthConsumerSecret = "OAuthConsumerSecret";
            public const string OAuthRequestTokenUrl = "OAuthRequestTokenUrl";
            public const string OAuthVerifierUrl = "OAuthVerifierUrl";
            public const string OAuthRequestAccessTokenUrl = "OAuthRequestAccessTokenUrl";
            public const string OAuthRequestProfileUrl = "OAuthRequestProfileUrl";
            public const string OAuthScope = "OAuthScope";
            public const string OAuthRealm = "OAuthRealm";
            public const string OAuthProfileMapper = "OAuthProfileMapper";
            public const string OAuthVersion = "OAuthVersion";
            public const string OAuthLogoutUrl = "OAuthLogoutUrl";
            public const string OutgoingOAuthSettings = "OutgoingOAuthSettings";

            #endregion

            #region Saml

            public const string SamlEnabled = "SamlEnabled";
            public const string SamlEnabledForCareer = "SamlEnabledForCareer";
            public const string SamlEnabledForTalent = "SamlEnabledForTalent";
            public const string SamlEnabledForAssist = "SamlEnabledForAssist";
            public const string SamlIdPArtifactIdProviderUrl = "SamlIdPArtifactIdProviderUrl";
            public const string SamlIdPSingleSignonIdProviderUrl = "SamlIdPSingleSignonIdProviderUrl";
            public const string SamlMetadataUrl = "SamlMetadataUrl";

            public const string SamlSpToIdPBindingUrl = "SamlSpToIdPBindingUrl";
            public const string SamlIdPToSpBindingUrl = "SamlIdPToSpBindingUrl";
            public const string SamlQueryStringBindingVar = "SamlQueryStringBindingVar";

            public const string SamlArtifactIdentityUrl = "SamlArtifactIdentityUrl";

            public const string SamlSpKeyLocation = "SamlSpKeyLocation";
            public const string SamlSpKeyName = "SamlSpKeyName";
            public const string SamlSpKeyPassword = "SamlSpKeyPassword";
            public const string SamlIdPCertKeyLocation = "SamlIdPCertKeyLocation";
            public const string SamlIdPCertKeyName = "SamlIdPCertKeyName";

            public const string SamlFirstNameAttributeName = "SamlFirstNameAttributeName";
            public const string SamlLastNameAttributeName = "SamlLastNameAttributeName";
            public const string SamlScreenNameAttributeName = "SamlScreenNameAttributeName";
            public const string SamlEmailAddressAttributeName = "SamlEmailAddressAttributeName";
            public const string SamlCreateRolesAttributeName = "SamlCreateRolesAttributeName";
            public const string SamlUpdateRolesAttributeName = "SamlUpdateRolesAttributeName";

            public const string SamlPrintXMLtoDebug = "SamlPrintXMLtoDebug";
            public const string SamlFederatedLogoutReturnUrl = "SamlFederatedLogoutReturnUrl";
            public const string SamlDefaultAssistRoles = "SamlDefaultAssistRoles";

            #region Saml Secondary configurations keys
            public const string SamlSecIdPSingleSignonIdProviderUrl = "SamlSecIdPSingleSignonIdProviderUrl";
            public const string SamlSecIdPCertKeyName = "SamlSecIdPCertKeyName";
            public const string SamlLogoutRedirect = "SamlLogoutRedirect";
            public const string SamlSecExternalClientTag = "SamlSecExternalClientTag";
            #endregion

            #endregion

            #endregion

            #region Issues

            //Switches
            public const string NotLoggingInEnabled = "NotLoggingInEnabled";
            public const string NotClickingLeadsEnabled = "NotClickingLeadsEnabled";
            public const string RejectingJobOffersEnabled = "RejectingJobOffersEnabled";
            public const string NotReportingForInterviewEnabled = "NotReportingForInterviewEnabled";
            public const string NotRespondingToEmployerInvitesEnabled = "NotRespondingToEmployerInvitesEnabled";
            public const string NotRespondingToReferralSuggestionsEnabled = "NotRespondingToReferralSuggestionsEnabled";
            public const string SelfReferringToUnqualifiedJobsEnabled = "SelfReferringToUnqualifiedJobsEnabled";
            public const string ShowingLowQualityMatchesEnabled = "ShowingLowQualityMatchesEnabled";
            public const string PostingLowQualityResumeEnabled = "PostingLowQualityResumeEnabled";
            public const string PostHireFollowUpEnabled = "PostHireFollowUpEnabled";
            public const string FollowUpEnabled = "FollowUpEnabled";
            public const string NotSearchingJobsEnabled = "NotSearchingJobsEnabled";
            public const string NotViewingInviteGracePeriodDays = "NotViewingInviteGracePeriodDays";
            public const string InappropriateEmailAddressCheckEnabled = "InappropriateEmailAddressCheckEnabled";
            public const string PositiveFeedbackCheckEnabled = "PositiveFeedbackCheckEnabled";
            public const string NegativeFeedbackCheckEnabled = "NegativeFeedbackCheckEnabled";
            public const string JobIssueFlagPotentialMSFW = "JobIssueFlagPotentialMSFW";
            public const string ExpiredAlienCertificationEnabled = "ExpiredAlienCertificationEnabled";
            public const string PotentialMSFWQuestionsEnabled = "PotentialMSFWQuestionsEnabled";


            // Thresholds
            public const string NotLoggingInDaysThreshold = "NotLoggingInDaysThreshold";
            public const string NotClickingLeadsCountThreshold = "NotClickingLeadsCountThreshold";
            public const string NotClickingLeadsDaysThreshold = "NotClickingLeadsDaysThreshold";
            public const string RejectingJobOffersCountThreshold = "RejectingJobOffersCountThreshold";
            public const string RejectingJobOffersDaysThreshold = "RejectingJobOffersDaysThreshold";
            public const string NotReportingForInterviewCountThreshold = "NotReportingForInterviewCountThreshold";
            public const string NotReportingForInterviewDaysThreshold = "NotReportingForInterviewDaysThreshold";
            public const string NotRespondingToEmployerInvitesCountThreshold = "NotRespondingToEmployerInvitesCountThreshold";
            public const string NotRespondingToEmployerInvitesDaysThreshold = "NotRespondingToEmployerInvitesDaysThreshold";
            public const string NotRespondingToReferralSuggestionsCountThreshold = "NotRespondingToReferralSuggestionsCountThreshold";
            public const string NotRespondingToReferralSuggestionsDaysThreshold = "NotRespondingToReferralSuggestionsDaysThreshold";
            public const string SelfReferringToUnqualifiedJobsCountThreshold = "SelfReferringToUnqualifiedJobsCountThreshold";
            public const string SelfReferringToUnqualifiedJobsDaysThreshold = "SelfReferringToUnqualifiedJobsDaysThreshold";
            public const string SelfReferringToUnqualifiedMinStarThreshold = "SelfReferringToUnqualifiedMinStarThreshold";
            public const string SelfReferringToUnqualifiedMaxStarThreshold = "SelfReferringToUnqualifiedMaxStarThreshold";
            public const string ShowingLowQualityMatchesCountThreshold = "ShowingLowQualityMatchesCountThreshold";
            public const string ShowingLowQualityMatchesDaysThreshold = "ShowingLowQualityMatchesDaysThreshold";
            public const string ShowingLowQualityMatchesMinStarThreshold = "ShowingLowQualityMatchesMinStarThreshold";
            public const string PostingLowQualityResumeWordCountThreshold = "PostingLowQualityResumeWordCountThreshold";
            public const string PostingLowQualityResumeSkillCountThreshold = "PostingLowQualityResumeSkillCountThreshold";
            public const string NotSearchingJobsDaysThreshold = "NotSearchingJobsDaysThreshold";
            public const string NotSearchingJobsCountThreshold = "NotSearchingJobsCountThreshold";
            public const string NotRespondingToEmployerInvitesGracePeriodDays = "NotRespondingToEmployerInvitesGracePeriodDays";


            #endregion

            #region Reporting

            public const string ReportingEnabled = "ReportingEnabled";
            public const string ReportActionDaysLimit = "ReportActionDaysLimit";

            #endregion

            #region Explorer

            public const string CareerEnabled = "CareerEnabled";
            public const string ExplorerResumeUploadAllowedFileRegExPattern = "ExplorerResumeUploadAllowedFileRegExPattern";
            public const string ExplorerResumeUploadMaximumAllowedFileSize = "ExplorerResumeUploadMaximumAllowedFileSize";
            public const string ExplorerApplicationPath = "ExplorerApplicationPath";
            public const string ExplorerShowBookmarkForNonLoggedInUser = "ExplorerShowBookmarkForNonLoggedInUser";
            public const string ExplorerShowEmailForNonLoggedInUser = "ExplorerShowEmailForNonLoggedInUser";
            public const string ExplorerShowResumeUploadForNonLoggedInUser = "ExplorerShowResumeUploadForNonLoggedInUser";
            public const string ExplorerSectionOrder = "ExplorerSectionOrder";
            public const string ExplorerInDemandDropdownOrder = "ExplorerInDemandDropdownOrder";
            public const string ExplorerSendEmailMaximumMessageLength = "ExplorerSendEmailMaximumMessageLength";
            public const string ExplorerSendEmailShowEmailFrom = "ExplorerSendEmailShowEmailFrom";
            public const string ExplorerSendEmailFromEmail = "ExplorerSendEmailFromEmail";
            public const string ExplorerAllowPersonalizationByResume = "ExplorerAllowPersonalizationByResume";
            public const string ExplorerJobSalaryNationwideDisplayType = "ExplorerJobSalaryNationwideDisplayType";
            public const string ExplorerJobSalaryComparisonPercent = "ExplorerJobSalaryComparisonPercent";
            public const string ExplorerRegistrationNotificationEmails = "ExplorerRegistrationNotificationEmails";
            public const string ExplorerDegreeClientDataTag = "ExplorerDegreeClientDataTag";
            public const string ExplorerShowDegreeStudyPlaces = "ExplorerShowDegreeStudyPlaces";
            public const string ExplorerGoogleTrackingId = "ExplorerGoogleTrackingId";
            public const string ExplorerGoogleEventTrackingEnabled = "ExplorerGoogleEventTrackingEnabled";
            public const string ExplorerHelpEmail = "ExplorerHelpEmail";
            public const string ExplorerLocationSearchRadius = "ExplorerLocationSearchRadius";
            public const string ExplorerLocationSearchUnit = "ExplorerLocationSearchUnit";
            public const string ExplorerDegreeFilteringType = "ExplorerDegreeFilteringType";
            public const string ExplorerUseDegreeTiering = "ExplorerUseDegreeTiering";
            public const string ExplorerOnlyShowJobsBasedOnClientDegrees = "ExplorerOnlyShowJobsBasedOnClientDegrees";
            public const string ExplorerModalEmailIcon = "ExplorerModalEmailIcon";
            public const string ExplorerModalDownloadIcon = "ExplorerModalDownloadIcon";
            public const string ExplorerReadyToMakeAMoveBubble = "ExplorerReadyToMakeAMoveBubble";

            #endregion

            #region Lens

            public const string LensService = "LensService";

            #endregion

            #region Career

            public const string MaximumAllowedResumeCount = "MaximumAllowedResumeCount";
            public const string DefaultCountryKey = "DefaultCountryKey";
            public const string PassiveAuthenticationCode = "PassiveAuthenticationCode";
            public const string MaskingAddress = "MaskingAddress";
            public const string SMTPServer = "SMTPServer";
            public const string SMTPPort = "SMTPPort";
            public const string SMTPUsername = "SMTPUsername";
            public const string SMTPPassword = "SMTPPassword";
            public const string MinimumScoreForClarify = "MinimumScoreForClarify";
            public const string MaximumSearchCountForClarify = "MaximumSearchCountForClarify";
            public const string MinimumScoreForSearch = "MinimumScoreForSearch";
            public const string MaximumNoDocumentToReturnInSearch = "MaximumNoDocumentToReturnInSearch";
            public const string MaximumNoDocumentToReturnInJobAlert = "MaximumNoDocumentToReturnInJobAlert";
            public const string SpamJobOriginId = "Spam_JobOriginId";
            public const string SpamMailAddress = "Spam_MailAddress";
            public const string SpamBccAddress = "Spam_BccAddress";
            public const string ClosedJobOriginId = "Closed_JobOriginId";
            public const string ClosedMailAddress = "Closed_MailAddress";
            public const string ClosedBccAddress = "Closed_BccAddress";
            public const string CacheSizeForPrefixDomain = "CacheSizeForPrefixDomain";
            public const string BrandName = "BrandName";
            public const string ResetPasswordFormat = "ResetPasswordFormat";
            public const string ResumeFormat = "ResumeFormat";
            public const string Customer = "Customer";
            public const string CustomerCenterCode = "CustomerCenterCode";
            public const string CustomerRepresentativeId = "CustomerRepresentativeId";
            public const string DefaultSearchRadiusKey = "DefaultSearchRadiusKey";
            public const string NationWideInstance = "NationWideInstance";
            public const string NearbyStateKeys = "NearbyStateKeys";
            public const string SearchableStateKeys = "SearchableStateKeys";
            public const string SpecialStateKeys = "SpecialStateKeys";
            public const string JobRequirements = "JobRequirements";
            public const string ReferralMinStars = "ReferralMinStars";
            public const string CareerRegistrationNotificationEmails = "CareerRegistrationNotificationEmails";
            public const string FilterSkillsInAutomatedSummary = "FilterSkillsInAutomatedSummary";
            public const string RemoveSpacesFromAutomatedSummary = "RemoveSpacesFromAutomatedSummary";
            public const string NonCustomerSystemOriginIds = "NonCustomerSystemOriginIds";
            public const string ReviewApplicationEligibilityOriginIds = "ReviewApplicationEligibilityOriginIds";
            public const string CountyEnabled = "CountyEnabled";
            public const string MyAccountEnabled = "MyAccountEnabled";
            public const string LogoutRedirectUrl = "LogoutRedirectUrl";
            public const string JobPostingFormat = "JobPostingFormat";
            public const string CareerExplorerFeatureEmphasis = "CareerExplorerFeatureEmphasis";
            public const string ShowProgramArea = "ShowProgramArea";
            public const string ShowCampus = "ShowCampus";
            public const string SupportsProspectiveStudents = "SupportsProspectiveStudents";
            public const string ShowContactDetails = "ShowContactDetails";
            public const string HideResumeProfile = "HideResumeProfile";
            public const string HideObjectives = "HideObjectives";
            public const string MilitaryServiceMinimumAge = "MilitaryServiceMinimumAge";
            public const string CareerHideSearchJobsByResume = "CareerHideSearchJobsByResume";
            public const string VietmanWarStartDate = "VietmanWarStartDate";
            public const string VietmanWarEndDate = "VietmanWarEndDate";
            public const string IsCareerExplorerAnonymousOnly = "IsCareerExplorerAnonymousOnly";
            public const string IsCareerExplorerAnonymousOnlyDomainNames = "IsCareerExplorerAnonymousOnlyDomainNames";
            public const string UseResumeTemplate = "UseResumeTemplate";
            public const string ResumeGuidedPath = "ResumeGuidedPath";
            public const string EnableCareerPasswordSecurity = "EnableCareerPasswordSecurity";
            public const string ShowTalentJobsAsHighPriority = "ShowTalentJobsAsHighPriority";

            #endregion

            #region Career Search Defaults

            public const string CareerSearchShowAllJobs = "CareerSearchShowAllJobs";
            public const string CareerSearchMinimumStarMatchScore = "CareerSearchMinimumStarMatchScore";
            public const string CareerSearchDefaultRadiusId = "CareerSearchDefaultRadiusId";
            public const string CareerSearchCentrePointType = "CareerSearchCentrePointType";
            public const string CareerSearchStates = "CareerSearchStates";
            public const string CareerSearchRadius = "CareerSearchRadius";
            public const string CareerSearchZipcode = "CareerSearchZipcode";
            public const string CareerSearchSpideredJobsExpiry = "CareerSearchSpideredJobsExpiry";
            public const string CareerSearchResumesSearchable = "CareerSearchResumesSearchable";

            public const string VeteranPriorityServiceEnabled = "VeteranPriorityServiceEnabled";
            public const string VeteranPriorityStarMatching = "VeteranPriorityStarMatchingReportActionDaysLimit";
            public const string VeteranPriorityExclusiveHours = "VeteranPriorityExclusiveHours";

            public const string WorkAvailabilitySearch = "WorkAvailabilitySearch";
            public const string JobSearchPostingDate = "JobSearchPostingDate";

            #endregion

            #region Alert Defaults

            public const string JobAlertsOption = "JobAlertsOption";
            public const string JobAlertFrequency = "JobAlertFrequency";
            public const string SendOtherJobsEmail = "SendOtherJobsEmail";
            public const string StaffEmailAlerts = "StaffEmailAlerts";
            public const string JobAlertTruncateWords = "JobAlertTruncateWords";

            #endregion

            #region Job Disclaimer Defaults

            public const string SpideredJobDisclaimerEnabled = "SpideredJobDisclaimerEnabled";
            public const string SpideredJobDisclaimerText = "SpideredJobDisclaimerText";
            public const string SearchResultsDisclaimerEnabled = "SearchResultsDisclaimerEnabled";
            public const string SearchResultsDisclaimerText = "SearchResultsDisclaimerText";

            #endregion


            #region Sharing Defaults

            public const string SharingJobsForSameFEIN = "SharingJobsForSameFEIN";

            #endregion

            #region Job Seeker Inactivity Email Defaults

            public const string JobSeekerSendInactivityEmail = "JobSeekerSendInactivityEmail";
            public const string JobSeekerInactivityEmailLimit = "JobSeekerInactivityEmailLimit";
            public const string JobSeekerInactivityEmailGrace = "JobSeekerInactivityEmailGrace";

            #endregion

            #region Access Tokens

            public const string AccessTokenKey = "AccessTokenKey";
            public const string AccessTokenSecret = "AccessTokenSecret";

            #endregion

            #region Caching

            public const string CacheMethod = "CacheMethod";
            public const string AppFabricCacheName = "AppFabricCacheName";
            public const string AppFabricLeadHosts = "AppFabricLeadHosts";

            #endregion

            #region Integration

            public const string IntegrationClient = "IntegrationClient";
            public const string IntegrationSettings = "IntegrationSettings";
            public const string IntegrationNodesAndAttributesToObfuscate = "IntegrationNodesAndAttributesToObfuscate";
            public const string IntegrationLoggableActions = "IntegrationLoggableActions";
            public const string IntegrationOneADayActions = "IntegrationOneADayActions";
            public const string EnableFrontEndMigration = "EnableFrontEndMigration";
            public const string IntegrationImportBatchSize = "IntegrationImportBatchSize";
            public const string IntegrationLoggingLevel = "IntegrationLoggingLevel";

            #endregion

            #region Encyrption

            public const string SharedEncryptionKey = "SharedEncryptionKey";
            public const string SharedEncryptionVector = "SharedEncryptionVector";

            #endregion

            #region SQL Jobs

            public const string SessionRetentionDays = "SessionRetentionDays";
            public const string PageStateRentionDays = "PageStateRentionDays";
            public const string LogEventRetentionDays = "LogEventRetentionDays";
            public const string ProfileEventRetentionDays = "ProfileEventRetentionDays";
            public const string MessageRetentionDays = "MessageRetentionDays";

            #endregion

            #region Comptia Settings

            public const string IsComptiaSpecific = "IsComptiaSpecific";
            public const string EnableSsnFeatureGroup = "EnableSsnFeatureGroup";
            public const string EnableDobFeatureGroup = "EnableDobFeatureGroup";
            public const string EnableNCRCFeatureGroup = "EnableNCRCFeatureGroup";
            public const string ShowReasonForLeavingFeature = "ShowReasonForLeavingFeature";

            #endregion
        }
    }
}
