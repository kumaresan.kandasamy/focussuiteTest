﻿namespace Focus.Core
{
	public partial class Constants
	{
		public class StateKeys
		{
			public const string UploadedJobDescription = "UploadedJobDescription";
			public const string UploadedLogo = "UploadedLogo";
			public const string UploadedLogoIsValid = "UploadedLogoIsValid";
			public const string ManageBusinessUnitModel = "ManageBusinessUnitModel";
      public const string EmployerDetailsModel = "EmployerDetailsModel";
			public const string CandidateSearchCriteria = "CandidateSearchCriteria";
			public const string CandidateSearchTitle = "CandidateSearchTitle";
			public const string UploadedImage = "UploadedImage";
			public const string UploadedImageIsValid = "UploadedImageIsValid";
			public const string PageError = "PageError";
		}
	}
}
