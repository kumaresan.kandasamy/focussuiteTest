﻿namespace Focus.Core
{
	public partial class Constants
	{
		public class WelcomeModalStateKeys
		{
			public const string ShowModal = "0";
			public const string ShowOfficeOnly = "1";
			public const string DoNotShow = "2";
		}
	}
}
