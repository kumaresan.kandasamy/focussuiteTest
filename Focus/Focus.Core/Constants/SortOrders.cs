﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

#endregion

namespace Focus.Core
{
	public partial class Constants
	{
		public class SortOrders
		{
			// Job Referral Summary
			public const string DateReceivedAsc = "datereceived asc";
			public const string DateReceivedDesc = "datereceived desc";
			public const string ScoreAsc = "score asc";
			public const string ScoreDesc = "score desc";
			public const string NameAsc = "name asc";
			public const string NameDesc = "name desc";
			public const string YearsExperienceAsc = "yearsexperience asc";
			public const string YearsExperienceDesc = "yearsexperience desc";
			public const string StatusAsc = "status asc";
			public const string StatusDesc = "status desc";
			public const string JobTitleAsc = "jobtitle asc";
			public const string JobTitleDesc = "jobtitle desc";
      public const string EducationCompletionDateAsc = "educationcompletiondate asc";
      public const string EducationCompletionDateDesc = "educationcompletiondate desc";

			// Job Activity Log. Reporting -BU Activity Log  -Hiring Manager Activity Log
			public const string ActivityDateAsc = "activitydate asc";
			public const string ActivityDateDesc = "activitydate desc";
			public const string ActivityActionAsc = "activityaction asc";
			public const string ActivityActionDesc = "activityaction desc";
			public const string ActivityUsernameAsc = "activityusername asc";
			public const string ActivityUsernameDesc = "activityusername desc";

			// Find Hiring Manager
			public const string EmployerNameAsc = "employername asc";
			public const string EmployerNameDesc = "employername desc";
			public const string ContactNameAsc = "contactname asc";
			public const string ContactNameDesc = "contactname desc";
			public const string PhoneNumberAsc = "phonenumber asc";
			public const string PhoneNumberDesc = "phonenumber desc";
			public const string LocationAsc = "location asc";
			public const string LocationDesc = "location desc";
			public const string BusinessUnitNameAsc = "businessunitname asc";
			public const string BusinessUnitNameDesc = "businessunitname desc";
			public const string TitleAsc = "title asc";
		  public const string TitleDesc = "title desc";
      public const string HiringAreasAsc = "hiringareas asc";
      public const string HiringAreasDesc = "hiringareas desc";
      public const string EmailAddressAsc = "emailaddress asc";
      public const string EmailAddressDesc = "emailaddress desc";
      public const string LastLoginAsc = "lastlogin asc";
      public const string LastLoginDesc = "lastlogin desc";

			// Match Recent Placements
			public const string RecentPlacementNameAsc = "recentplacementname asc";
			public const string RecentPlacementNameDesc = "recentplacementname desc";
			public const string PlacementDateDesc = "placementdate desc";

			// Reporting-BU Placements
			public const string PlacementStartDateAsc = "placementstartdate asc";
			public const string PlacementStartDateDesc = "placementstartdate desc";
			public const string PlacementNameAsc = "placementname asc";
			public const string PlacementNameDesc = "placementname desc";
			public const string PlacementJobTitleAsc = "placementjobtitle asc";
			public const string PlacementJobTitleDesc = "placementjobtitle desc";

      // Job seeker issues search
      public const string JobSeekerNameAsc = "jobseekername asc";
      public const string JobSeekerNameDesc = "jobseekername desc";
      public const string LastActivityAsc = "lastactivity asc";
      public const string LastActivityDesc = "lastactivity desc";
			public const string StudyProgramAsc = "studyprogram asc";
			public const string StudyProgramDesc = "studyprogram desc";

      public const string ActivityCategoryAsc = "activitycategory asc";
      public const string ActivityCategoryDesc = "activitycategory desc";
      public const string ActivityAsc = "activity asc";
      public const string ActivityDesc = "activity desc";

			public const string PostingDateAsc = "postingdate asc";
			public const string PostingDateDesc = "postingdate desc";
		}
	}
}
