﻿namespace Focus.Core
{
	public partial class Constants
	{
		public class WebConfigKeys
		{
			public const string Module = "Focus-Module";
			public const string Application = "Focus-Application";
			public const string ClientTag = "Focus-ClientTag";
      public const string Theme = "Focus-Theme";
			
			#region Migration Settings

			public const string MigrationProvider = "Focus-MigrationProvider";
      public const string MigrationBatchSize = "Focus-MigrationBatchSize";
      public const string ImportBatchSize = "Focus-ImportBatchSize";

			#endregion

			#region Database Encryption Settings

			public const string EncryptionEnabled = "Focus-Encryption-Enabled";
      public const string EncryptionKey = "Focus-Encryption-Key";
      public const string EncryptionVector = "Focus-Encryption-Vector";

			#endregion

			#region Service Method Validation

			public const string ValidateServiceVersion = "ValidateServiceVersion";
      public const string UsesAccessToken = "UsesAccessToken";

      #endregion
		}
	}
}
