﻿namespace Focus.Core
{
    public partial class Constants
    {
        public class RoleKeys
        {
            public const string SystemAdmin = "SystemAdmin";
            public const string Admin = "Admin";
            public const string TalentUser = "TalentUser";
            public const string AssistUser = "AssistUser";
            public const string ExplorerUser = "ExplorerUser";
            public const string CareerUser = "CareerUser";
            public const string AssistAccountAdministrator = "AssistAccountAdministrator";
            public const string AssistSystemAdministrator = "AssistSystemAdministrator";
            public const string AssistBroadcastsAdministrator = "AssistBroadcastsAdministrator";
            public const string AssistPostingApprover = "AssistPostingApprover";
            public const string AssistPostingApprovalViewer = "AssistPostingApprovalViewer";
            public const string AssistEmployerAccountApprover = "AssistEmployerAccountApprover";
            public const string AssistEmployerAccountApprovalViewer = "AssistEmployerAccountApprovalViewer";
            public const string AssistJobSeekerReferralApprover = "AssistJobSeekerReferralApprover";
            public const string AssistJobSeekerReferralApprovalViewer = "AssistJobSeekerReferralApprovalViewer";
            public const string AssistCourtOrderedAffirmativeActionApprover = "ACourtOrderedAffirmativeActionApprover";
            public const string AssistFederalContractorApprover = "AFederalContractorApprover";
            public const string AssistForeignLabourH2AAgApprover = "AForeignLabourH2AAgApprover";
            public const string AssistForeignLabourH2BNonAgApprover = "AForeignLabourH2BNonAgApprover";
            public const string AssistForeignLabourOtherApprover = "AForeignLabourOtherApprover";
            public const string AssistCommissionOnlyApprover = "ACommissionOnlyApprover";
            public const string AssistSalaryAndCommissionApprover = "ASalaryAndCommissionApprover";
            public const string AssistEmployersReadOnly = "AssistEmployersReadOnly";
            public const string AssistEmployersAdministrator = "AssistEmployersAdministrator";
            public const string AssistEmployersAccountBlocker = "AssistEmployersAccountBlocker";
            public const string AssistEmployersSendMessages = "AssistEmployersSendMessages";
            public const string AssistEmployersCreateNewAccount = "AssistEmployersCreateNewAccount";
            public const string AssistJobSeekersReadOnly = "AssistJobSeekersReadOnly";
            public const string AssistJobSeekersAdministrator = "AssistJobSeekersAdministrator";
            public const string AssistJobSeekersAccountInactivator = "AssistJobSeekersAccountInactivator";
            public const string BackdateMyEntriesJSActionMenuMaxDays = "BackdateMyEntriesJSActionMenuMaxDays";
            public const string BackdateAnyEntryJSActivityLogMaxDays = "BackdateAnyEntryJSActivityLogMaxDays";
            public const string BackdateMyEntriesJSActivityLogMaxDays = "BackdateMyEntriesJSActivityLogMaxDays";
            public const string DeleteAnyEntryJSActivityLogMaxDays = "DeleteAnyEntryJSActivityLogMaxDays";
            public const string DeleteMyEntriesJSActivityLogMaxDays = "DeleteMyEntriesJSActivityLogMaxDays";
            public const string BackdateMyEntriesHMActionMenuMaxDays = "BackdateMyEntriesHMActionMenuMaxDays";
            public const string BackdateAnyEntryHMActivityLogMaxDays = "BackdateAnyEntryHMActivityLogMaxDays";
            public const string BackdateMyEntriesHMActivityLogMaxDays = "BackdateMyEntriesHMActivityLogMaxDays";
            public const string DeleteMyEntriesHMActivityLogMaxDays = "DeleteMyEntriesHMActivityLogMaxDays";
            public const string DeleteAnyEntryHMActivityLogMaxDays = "DeleteAnyEntryHMActivityLogMaxDays";
            public const string AssistJobSeekersAccountBlocker = "AssistJobSeekersAccountBlocker";
            public const string AssistJobSeekersSendMessages = "AssistJobSeekersSendMessages";
            public const string AssistJobSeekersCreateNewAccount = "AssistJobSeekersCreateNewAccount";
            public const string AssistStudentAccountAdministrator = "AssistStudentAccountAdministrator";
            public const string AssistHandleJobDevelopment = "AssistHandleJobDevelopment";
            public const string AssistJobSeekerOfficeAdministrator = "AJSOfficeAdmin";
            public const string AssistJobOrderOfficeAdministrator = "AJOOfficeAdmin";
            public const string AssistEmployerOfficeAdministrator = "AEOfficeAdmin";
            public const string AssistEmployerJobDevelopmentManager = "AEJobDevelopmentManager";
            public const string AssistEmployerOverrideCriminalExclusions = "AEOverrideCriminalExclusions";
            public const string AssistOfficesActivateDeactivate = "AssistOfficesActivateDeactivate";
            public const string AssistOfficesCreate = "AssistOfficesCreate";
            public const string AssistOfficesListStaff = "AssistOfficesListStaff";
            public const string AssistOfficesSetDefault = "AssistOfficesSetDefault";
            public const string AssistOfficesViewEdit = "AssistOfficesViewEdit";
            public const string AssistHomeBasedApprover = "AHomeBasedApprover";
            public const string AssistEmployerOverrideCreditCheckRequired = "AssistEmployerOverrideCreditCheckRequired";
            public const string AssistJobSeekerReports = "AssistJobSeekerReports";
            public const string AssistJobOrderReports = "AssistJobOrderReports";
            public const string AssistEmployerReports = "AssistEmployerReports";
            public const string AssistJobSeekerReportsViewOnly = "AssistJobSeekerReportsViewOnly";
            public const string AssistJobOrderReportsViewOnly = "AssistJobOrderReportsViewOnly";
            public const string AssistEmployerReportsViewOnly = "AssistEmployerReportsViewOnly";
            public const string AssistStaffView = "AssistStaffView";
            public const string AssistStaffViewEdit = "AssistStaffViewEdit";
            public const string AssistStaffResetPassword = "AssistStaffResetPassword";
            public const string AssistStaffDeactivateActivate = "AssistStaffDeactivateActivate";
            public const string AssistStaffDelete = "AssistStaffDelete";
            public const string AssistStudentActivityReport = "AssistStudentActivityReport";
            public const string AssistAssignJobSeekerOfficeFromDefault = "AssistAssignJobSeekerOfficeFromDefault";
            public const string AssistAssignBusinessOfficeFromDefault = "AssistAssignBusinessOfficeFromDefault";
            public const string AssistStaffDetails = "AssistStaffDetails";
            public const string AssistUpdatePermissions = "AssistUpdatePermissions";
            public const string AssistManageDocuments = "AssistManageDocuments";
            public const string ReassignJobSeekerToNewOffice = "ReassignJobSeekerToNewOffice";
            public const string ReassignBusinessToNewOffice = "ReassignBusinessToNewOffice";
            public const string ReassignJobPostingToNewOffice = "ReassignJobPostingToNewOffice";
        }
    }
}
