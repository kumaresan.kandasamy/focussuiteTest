﻿
namespace Focus.Core
{
	public partial class LocalisationKeys
	{
		public class Explorer
		{
			public class Pages
			{
				[LocalisationContextKey("Explorer.Pages.Dummy", "Explorer Pages - Dummy", "6")]
				public class Dummy
				{
					public const string Title = "Title";
					public const string TestLabelText = "TestLabelText";
				}
			}
		}
	}		
}
