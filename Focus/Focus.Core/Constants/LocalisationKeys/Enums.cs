﻿
namespace Focus.Core
{
	public partial class LocalisationKeys
	{
		[LocalisationContextKey("Enums", "Enumerations", "3")]
		public class Enums
		{
			public const string ErrorTypes = "ErrorTypes";
			public const string ApprovalStatuses = "ApprovalStatuses";
		}		
	}
}
