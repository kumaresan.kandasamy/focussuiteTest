﻿
namespace Focus.Core
{
	public partial class LocalisationKeys
	{
		public class Assist
		{
			public class Pages
			{
				[LocalisationContextKey("Assist.Pages.Dummy", "Assist Pages - Dummy", "5")]
				public class Dummy
				{
					public const string Title = "Title";
					public const string TestLabelText = "TestLabelText";
				}
			}
		}
	}		
}
