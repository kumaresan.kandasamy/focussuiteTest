﻿
namespace Focus.Core
{
	public partial class LocalisationKeys
	{
		public class Global
		{
			[LocalisationContextKey("Global.Buttons", "Global Buttons", "1")]
			public class Buttons
			{
				public const string OkButtonText = "OkButton.Text";
				public const string OkButtonToolTip = "OkButton.ToolTip";
			}

			[LocalisationContextKey("Global.Labels", "Global Labels", "1")]
			public class Labels
			{
				public const string UnknownLabel = "Unknown.Label";
			}
		}		
	}
}
