﻿
namespace Focus.Core
{
	public partial class LocalisationKeys
	{
		public class System
		{
			[LocalisationContextKey("System.Email", "System - Emails", "2")]
			public class Emails
			{
				public const string ResetPasswordBody = "ResetPassword.Body";
				public const string ResetPasswordSubject = "ResetPassword.Subject";
			}
		}		
	}
}
