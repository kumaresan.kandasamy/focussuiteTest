﻿
namespace Focus.Core
{
	public partial class LocalisationKeys
	{
		public class Talent
		{
			public class Pages
			{
				[LocalisationContextKey("Talent.Pages.Dummy", "Talent Pages - Dummy", "4")]
				public class Dummy
				{
					public const string Title = "Title";
					public const string TestLabelText = "TestLabelText";
				}
			}
		}
	}		
}
