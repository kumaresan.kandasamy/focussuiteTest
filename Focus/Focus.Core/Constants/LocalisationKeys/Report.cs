﻿
namespace Focus.Core
{
	public partial class LocalisationKeys
	{
		public class Report
		{
			public class Pages
			{
				[LocalisationContextKey("Report.Pages.Dummy", "Report Pages - Dummy", "7")]
				public class Dummy
				{
					public const string Title = "Title";
					public const string TestLabelText = "TestLabelText";
				}
			}
		}
	}		
}
