﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

namespace Focus.Core
{
	public partial class Constants
	{
		public class PlaceHolders
		{
			public const string FocusTalent = "#FOCUSTALENT#";
			public const string FocusAssist = "#FOCUSASSIST#";
			public const string FocusCareer = "#FOCUSCAREER#";
			public const string FocusReporting = "#FOCUSREPORTING#";
			public const string FocusExplorer = "#FOCUSEXPLORER#";

			public const string FocusTalentUrl = "#FOCUSTALENTURL#";
			public const string FocusAssistUrl = "#FOCUSASSISTURL#";
			public const string FocusCareerUrl = "#FOCUSCAREERURL#";

			public const string SupportEmail = "#SUPPORTEMAIL#";
			public const string SupportPhone = "#SUPPORTPHONE#";
			public const string SupportHoursOfBusiness = "#SUPPORTHOURSOFBUSINESS#";
			public const string SupportHelpTeam = "#SUPPORTHELPTEAM#";

			public const string RecipientName = "#RECIPIENTNAME#";
			public const string JobId = "#JOBID#";
			public const string JobTitle = "#JOBTITLE#";
			public const string JobStatus = "#JOBSTATUS#";
			public const string JobPosting = "#JOBPOSTING#";
			public const string JobPostingDate = "#JOBPOSTINGDATE#";
			public const string DaysLeftToJobExpiry = "#DAYSLEFTTOJOBEXPIRY#";
			public const string ExpiryDate = "#JOBEXPIRYDATE#";
			public const string MatchedJobs = "#MATCHEDJOBS#";
			public const string WOTC = "#WOTC#";
			public const string EmployerName = "#EMPLOYERNAME#";
			public const string EmployeeName = "#EMPLOYEENAME#";
			public const string SenderName = "#SENDERNAME#";
			public const string SenderPhoneNumber = "#SENDERPHONENUMBER#";
			public const string SenderEmailAddress = "#SENDEREMAILADDRESS#";
			public const string FreeText = "#FREETEXT#";
			public const string JobLinkUrl = "#JOBLINKURL#";
			public const string ApplicationInstructions = "#APPLICATIONINSTRUCTIONS#";
			public const string SavedSearchName = "#SAVEDSEARCHNAME#";
			public const string MatchedCandidates = "#MATCHEDCANDIDATES#";
			public const string Reminder = "#REMINDER#";
			public const string ApprovalCount = "#APPROVALCOUNT#";
			public const string ApprovalQueueName = "#APPROVALQUEUENAME#";

			public const string FullName = "#FULLNAME#";
			public const string FirstName = "#FIRSTNAME#";
			public const string LastName = "#LASTNAME#";
			public const string EmailAddress = "#EMAILADDRESS#";
			public const string PhoneNumber = "#PHONENUMBER#";
			public const string NowDateTime = "#NOWDATETIME#";
			public const string Password = "#PASSWORD#";
			public const string PasswordResetUrl = "#PASSWORDRESETURL#";

			public const string AuthenticateUrl = "#AUTHENTICATEURL#";
			public const string PinNumber = "#PINNUMBER#";

			public const string HiringManagerName = "#HIRINGMANAGERNAME#";
			public const string HiringManagerPhoneNumber = "#HIRINGMANAGERPHONENUMBER#";
			public const string HiringManagerEmail = "#HIRINGMANAGEREMAIL#";

			public const string OfficeName = "#OFFICENAME#";

			public const string SchoolName = "#SCHOOLNAME#";
			public const string CandidateType = "#CANDIDATETYPE#";
			public const string EmploymentType = "#EMPLOYMENTTYPE#";
			public const string EmploymentTypes = "#EMPLOYMENTTYPES#";
			public const string PostingType = "#POSTINGTYPE#";
			public const string PostingTypes = "#POSTINGTYPES#";
			public const string CandidateTypes = "#CANDIDATETYPES#";

			public const string Username = "#USERNAME#";
			public const string Url = "#URL#";

			public const string PostingFooterTitle = "#POSTINGFOOTERTITLE#";
			public const string CrimFooter = "#CRIMFOOTER#";
			public const string CredFooter = "#CREDFOOTER#";
			public const string PostingFooter = "#POSTINGFOOTER#";

			public const string IrsOnlineApplicationHours = "#IRSONLINEAPPLICATIONHOURS#";
			public const string IrsOnlineApplicationUrl = "#IRSONLINEAPPLICATIONURL#";

			public const string LensJobId = "#ID#";

			public const string UnsubscribeLinkUrl = "#UNSUBSCRIBELINKURL#";

			public const string ReportFromDate = "#REPORTFROMDATE#";
			public const string ReportToDate = "#REPORTTODATE#";

			public const string DenialReasons = "#DENIALREASONS#";
			public const string HoldReasons = "#HOLDREASONS#";

			public const string Business = "#BUSINESS#";
			public const string Businesses = "#BUSINESSES#";

			public const string CriminalBackgroundUrl = "#CRIMINALBACKGROUNDURL#";
		}
	}
}
