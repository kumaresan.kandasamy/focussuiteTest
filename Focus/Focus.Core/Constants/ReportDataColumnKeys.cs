﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace Focus.Core
{
  public partial class Constants
  {
    public class Reporting
    {
      public class JobSeekerReportDataColumns
      {
        public const string State = "State";
        public const string Office = "Office";
        public const string EducationLevel = "Education level";
        public const string EmploymentStatus = "Employment status";
        public const string Name = "Name";
        public const string Email = "Email";
        public const string Location = "Location";
        public const string Status = "Status";
        public const string VeteranType = "Veteran Type";
        public const string VeteranTransitionType = "Transition Type";
        public const string VeteranMilitaryDischarge = "Military Discharge";
        public const string VeteranBranchOfService = "Branch of Service";
        public const string Logins = "Logins";
        public const string PostingsViewed = "Postings viewed";
        public const string ReferralsRequested = "Referrals requested";
        public const string SavedJobAlert = "Saved job alert";
        public const string SurveyVerySatisfied = "Very Satisfied with match quality";
        public const string SurveySatisfied = "Somewhat satisfied with match quality";
        public const string SurveyDissatisfied = "Somewhat dissatisfied with match quality";
        public const string SurveyVeryDissatisfied = "Very dissatisfied with match quality";
        public const string SurveyNoUnexpectedMatches = "No unexpected matches received";
        public const string SurveyUnexpectedMatches = "Unexpected matches received";
				public const string SurveyReceivedInvitations = "Received #BUSINESS#:LOWER invitations";
				public const string SurveyDidNotReceiveInvitations = "Didn't receive #BUSINESS#:LOWER invitations";
        public const string SelfReferrals = "Self referrals (statewide)";
        public const string StaffReferrals = "Staff referrals (statewide)";
        public const string SelfReferralsExternal = "Self referrals (external)";
        public const string StaffReferralsExternal = "Staff referrals (external)";
        public const string TotalReferrals = "Total referrals";
        public const string ReferralsApproved = "Referrals approved by staff";
        public const string TargetingHighGrowth = "Targeting high growth";
        public const string ActivitiesAssigned = "Activities assigned";
        public const string AddedToLists = "Added to lists";
        public const string FollowUpIssues = "Followup issues";
        public const string IssuesResolved = "Issues resolved";
        public const string NotesAdded = "Notes added";
        public const string StaffAssignments = "Staff assignments";
        public const string ActivityServices = "Activity Count";
        public const string FindJobsForSeeker = "Find jobs for job seeker";
        public const string EmailsSent = "Emails sent";
        public const string TotalAccounts = "Total accounts";
      }

      public class JobOrderReportDataColumns
      {
        public const string JobTitle = "Title";
        public const string Employer = "#BUSINESS#";
        public const string County = "County";
        public const string State = "State";
        public const string Location = "Location";
        public const string Office = "Office";
        public const string SelfReferrals = "Self referrals";
        public const string StaffReferrals = "Staff referrals";
        public const string TotalReferrals = "Total referrals";
        public const string ReferralsRequested = "Referrals requested";
        public const string EmployerInvitationsSent = "Total number of invitations sent";
        public const string InvitedJobSeekerViewed = "Who viewed the job";
        public const string InvitedJobSeekerClicked = "Invitee applied to posting";
        public const string InvitedJobSeekerNotViewed = "Who didn't view the job";
        public const string InvitedJobSeekerNotClicked = "Invitee didn't apply to posting";
        public const string TotalAccounts = "Totalaccounts";
      }

      public class EmployerReportDataColumns
      {
        public const string Name = "Name";
        public const string State = "State";
        public const string County = "County";
        public const string Location = "Location";
        public const string Office = "Office";
        public const string JobOrdersEdited = "Edited";
        public const string JobSeekersInterviewed = "Interviews scheduled";
        public const string JobSeekersHired = "Hired";
        public const string JobOrdersCreated = "Created";
        public const string JobOrdersPosted = "Posted";
        public const string JobOrdersPutOnHold = "On-hold";
        public const string JobOrdersRefreshed = "Refreshed";
        public const string JobOrdersClosed = "Closed";
        public const string InvitationsSent = "Invitations sent";
        public const string JobSeekersNotHired = "Not hired";
        public const string SelfReferrals = "Self referrals";
        public const string StaffReferrals = "Staff referrals";
        public const string TotalAccounts = "Total accounts";
      }

      public class SupplyDemandDataColumns
      {
        public const string OnetCode = "Onet";
        public const string GroupBy = "Group By Detail";
        public const string SupplyCount = "Supply Count";
        public const string DemandCount = "Demand Count";
        public const string Gap = "Gap";
        public const string ReportTotal = "Page Totals";
      }

      public class SharedReportDataColumns
      {
        public const string FailedToApply = "Failed to apply";
        public const string FailedToReportToInterview = "Failed to report to interview";
        public const string Hired = "Hired";
        public const string InterviewDenied = "Interview denied";
        public const string InterviewScheduled = "Interview scheduled";
        public const string NewApplicant = "New applicant";
        public const string NotHired = "Not hired";
        public const string Recommended = "Recommended";
        public const string RefusedOffer = "Refused job";
        public const string UnderConsideration = "Under consideration";
        public const string FailedToReportToJob = "Failed to report to job";
        public const string FailedToRespondToInvitation = "Failed to respond to invite";
        public const string FoundJobFromOtherSource = "Found job from other source";
        public const string JobAlreadyFilled = "Job already filled";
        public const string NotQualified = "Not qualified";
        public const string NotYetPlaced = "Not yet placed";
        public const string RefusedReferral = "Refused referral";
      }
    }
  }
}
