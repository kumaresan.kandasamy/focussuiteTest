﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Focus.Core
{
	/// <summary>
	/// Class designed to organise and return full list of localisation keys
	/// </summary>
	public partial class LocalisationKeys
	{
		private static List<LocalisationKey> _localisationKeys;
		private static readonly object SyncObject = new object();

		/// <summary>
		/// Gets the keys.
		/// </summary>
		public static List<LocalisationKey> Keys
		{
			get
			{
				if (_localisationKeys == null)
				{
					lock (SyncObject)
						_localisationKeys = GetLocalisationKeys();
				}

				return _localisationKeys;
			}
		}

		/// <summary>
		/// Gets the localisation keys.
		/// </summary>
		/// <returns></returns>
		private static List<LocalisationKey> GetLocalisationKeys()
		{
			var localisationKeys = new List<LocalisationKey>();
			var types = Assembly.GetExecutingAssembly().GetTypes();

			foreach (var type in types)
			{
				var customAttributes = type.GetCustomAttributes(typeof(LocalisationContextKey), false);

				if (customAttributes.Length > 0)
				{
					var localisationContextKey = (LocalisationContextKey)customAttributes[0];
					var fieldInfos = type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);

					// Go through the list and only pick out the constants
					foreach (var fi in fieldInfos.Where(fi => fi.IsLiteral && !fi.IsInitOnly))
						localisationKeys.Add(new LocalisationKey(localisationContextKey.ContextKey, localisationContextKey.ContextKeyName, (string)fi.GetValue(null), localisationContextKey.SortOrder));
				}
			}

			return localisationKeys.OrderBy(x => x.SortOrder).ThenBy(x => x.ContextKeyName).ThenBy(x => x.Key).ToList();
		}
	}

	[AttributeUsage(AttributeTargets.Class)]
	public class LocalisationContextKey : Attribute
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="LocalisationContextKey"/> class.
		/// </summary>
		/// <param name="contextKey">The context key.</param>
		/// <param name="contextKeyName">Name of the context key.</param>
		/// <param name="sortOrder">The sort order.</param>
		public LocalisationContextKey(string contextKey, string contextKeyName, string sortOrder)
		{
			ContextKey = contextKey;
			ContextKeyName = contextKeyName;
			SortOrder = sortOrder;
		}

		public string ContextKey { get; set; }
		public string ContextKeyName { get; set; }
		public string SortOrder { get; set; }
	}

	public class LocalisationKey
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="LocalisationKey"/> class.
		/// </summary>
		public LocalisationKey()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="LocalisationKey"/> class.
		/// </summary>
		/// <param name="contextKey">The context key.</param>
		/// <param name="contextKeyName">Name of the context key.</param>
		/// <param name="key">The key.</param>
		/// <param name="sortOrder">The sort order.</param>
		public LocalisationKey(string contextKey, string contextKeyName, string key, string sortOrder)
		{
			ContextKey = contextKey;
			ContextKeyName = contextKeyName;
			Key = key;
			SortOrder = sortOrder;
		}

		/// <summary>
		/// Gets or sets the context key.
		/// </summary>
		/// <value>The context key.</value>
		public string ContextKey { get; set; }

		/// <summary>
		/// Gets or sets the name of the context key.
		/// </summary>
		/// <value>The name of the context key.</value>
		public string ContextKeyName { get; set; }

		/// <summary>
		/// Gets or sets the key.
		/// </summary>
		/// <value>The key.</value>
		public string Key { get; set; }

		/// <summary>
		/// Gets or sets the sort order.
		/// </summary>
		/// <value>The sort order.</value>
		public string SortOrder { get; set; }
	}
}
