﻿namespace Focus.Core
{
	public partial class Constants
	{
		public class CustomFilterKeys
		{
			public const string VeteranStatus = "VeteranStatus";
			public const string DrivingLicenceClass = "DrivingLicenceClass";
			public const string DrivingLicenceEndorsement = "DrivingLicenceEndorsement";
			public const string WorkOvertime = "WorkOvertime";
			public const string ShiftType = "ShiftType";
			public const string WorkType = "WorkType";
			public const string WorkWeek = "WorkWeek";
			public const string JobseekerId = "JobseekerId";
			public const string MinimumSalary = "MinimumSalary";
			public const string MaximumSalary = "MaximumSalary";
			public const string ExternalPostingId = "ExternalPostingId";
			public const string Occupation = "Occupation";
			public const string EducationLevel = "EducationLevel";
			public const string Industry = "Industry";
			public const string Sector = "Sector";
			public const string OpenDate = "OpenDate";
			public const string ClosingOn = "ClosingOn";
			public const string Status = "Status";
			public const string DateOfBirth = "DateOfBirth";
			public const string CitizenFlag = "CitizenFlag";
			public const string RegisteredOn = "RegisteredOn";
			public const string ModifiedOn = "ModifiedOn";
			public const string Languages = "Languages";
			public const string Certificates = "Certificates";
			public const string Relocation = "Relocation";
			public const string StateCode = "StateCode";
			public const string CountyCode = "CountyCode";
			public const string Msa = "Msa";
			public const string PostingId = "PostingId";
			public const string Origin = "Origin";
			public const string Sic2 = "Sic2";
			public const string GreenJob = "GreenJob";
			public const string Internship = "Internship";
			public const string JobTypes = "JobTypes";
			public const string AnnualPay = "AnnualPay";
			public const string Salary = "Salary";
			public const string UnpaidPosition = "UnpaidPosition";
			public const string GPA = "GPA";
			public const string MonthsExperience = "MonthsExperience";
			public const string EnrollmentStatus = "EnrollmentStatus";
			public const string ProgramsOfStudy = "ProgramsOfStudy";
			public const string InternshipCategories = "InternshipCategories";
			public const string ROnet = "ROnet";
			public const string RequiredProgramOfStudyId = "RequiredProgramOfStudyId";
			public const string DegreeCompletionDate = "DegreeCompletionDate";
			public const string ResumeIndustries = "ResumeIndustries";
      public const string HomeBasedTag = "HomeBased";
      public const string NCRCLevel = "NCRCLevel";
			public const string CommissionBasedTag = "CommissionBased";
			public const string SalaryAndCommissionBasedTag = "SalaryAndCommissionBased";
    }
	}
}
