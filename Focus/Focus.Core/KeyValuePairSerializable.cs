﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

#endregion

namespace Focus.Core
{
	[Serializable, StructLayout(LayoutKind.Sequential)]
	public struct KeyValuePairSerializable<TKey, TValue>
	{
		public KeyValuePairSerializable(TKey key, TValue value) : this()
		{
			Key = key;
			Value = value;
		}

		/// <summary>
		/// Gets the Value in the Key/Value Pair
		/// </summary>
		public TValue Value { get; set; }

		/// <summary>
		/// Gets the Key in the Key/Value pair
		/// </summary>
		public TKey Key { get; set; }

		public static explicit operator KeyValuePairSerializable<TKey,TValue>(KeyValuePair<TKey,TValue> pair)
		{
			var serializablePair = new KeyValuePairSerializable<TKey, TValue>(pair.Key, pair.Value);

			return serializablePair;
		}

		public static explicit operator KeyValuePair<TKey, TValue>(KeyValuePairSerializable<TKey, TValue> serializablePair) 
		{
			var pair = new KeyValuePair<TKey, TValue>(serializablePair.Key, serializablePair.Value);

			return pair;
		}
	}
}
