﻿#region Copyright Â© 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

namespace Focus.Core
{
    public partial class Constants
    {
        public const string DefaultCulture = "**-**";
        public const string StateKeyPrefix = "Focus";
        public const string DataContractNamespace = "http://www.burning-glass.com/focus/types/";
        public const string ServiceContractNamespace = "http://www.burning-glass.com/focus/services/";

        // 00.00.0000 = Version . Release . Patch
        // This format is now used in the message bus to ensure only messages for the current version of the application are processed
        public const string SystemVersion = "3.62.0073";

        // Other constants classes have been moved to their own file
        // For AlertMessageKeys constants goto Constants/AlertMessageKeys.cs
        // For CacheKeys constants goto Constants/CacheKeys.cs
        // For ConfigurationItemKeys constants goto Constants/ConfigurationItemKeys.cs
        // For PlaceHolders constants goto Constants/PlaceHolders.cs
        // For RoleKeys constants goto Constants/RoleKeys.cs
        // For StateKeys constants goto Constants/StateKeys.cs
        // For WebConfigKeys constants goto Constants/WebConfigKeys.cs
        // For LocalisationKeys constants got Constants/LocalisationKeys.cs
    }
}