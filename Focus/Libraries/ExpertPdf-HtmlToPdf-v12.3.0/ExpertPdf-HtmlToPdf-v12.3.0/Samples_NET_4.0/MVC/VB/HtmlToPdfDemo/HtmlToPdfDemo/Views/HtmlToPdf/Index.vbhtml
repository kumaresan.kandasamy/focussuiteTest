﻿@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

@Code
    Using (Html.BeginForm("Convert", "HtmlToPdf", FormMethod.Post))
End Code

<img src="~/Content/logo.jpg" runat="server" alt="ExpertPdf" />
<h1>ExpertPdf Html To Pdf Converter - MVC Demo</h1>

<div>
    Url: &nbsp;
    <input type="text" value="http://www.html-to-pdf.net" name="TxtUrl" style="width: 500px;"/>
    &nbsp;
    <input type="submit" name="BtnConvert" value="Export to PDF"/>
</div>    

@Code
End Using
End Code