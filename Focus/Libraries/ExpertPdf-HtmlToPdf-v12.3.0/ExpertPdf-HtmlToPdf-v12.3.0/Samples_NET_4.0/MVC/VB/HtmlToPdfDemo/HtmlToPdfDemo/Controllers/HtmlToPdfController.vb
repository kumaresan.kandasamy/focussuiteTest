﻿Imports ExpertPdf.HtmlToPdf

Public Class HtmlToPdfController
    Inherits System.Web.Mvc.Controller

    '
    ' GET: /HtmlToPdf

    Function Index() As ActionResult
        Return View()
    End Function

    <HttpPost()>
    Function Convert(collection As FormCollection) As ActionResult
        Dim pdfConverter As New PdfConverter()
        Dim url As String = collection("TxtUrl")
        Dim pdf As Byte() = pdfConverter.GetPdfBytesFromUrl(url)

        Dim fileResult As FileResult = New FileContentResult(pdf, "application/pdf")
        fileResult.FileDownloadName = "RenderedPage.pdf"

        Return fileResult
    End Function
End Class