﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExpertPdf.HtmlToPdf;

namespace HtmlToPdfDemo.Controllers
{
    public class HtmlToPdfController : Controller
    {
        //
        // GET: /HtmlToPdf/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Convert(FormCollection collection)
        {
            PdfConverter pdfConverter = new PdfConverter();
            string url = collection["TxtUrl"];
            byte[] pdf = pdfConverter.GetPdfBytesFromUrl(url);

            FileResult fileResult = new FileContentResult(pdf, "application/pdf");
            fileResult.FileDownloadName = "RenderedPage.pdf";

            return fileResult;
        }
    }
}
