
Partial Class InvoiceReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not IsPostBack) Then
            LoadInvoiceData()
        End If
    End Sub

    Private Sub LoadInvoiceData()
        litInvoiceDate.Text = DateTime.Now.ToString("MMMM dd, yyyy")
        litInvoiceNum.Text = "30643"
        LoadCustomerInfo()
        LoadInvoiceItems()
        lblTotalPrice.Text = InvoiceData.GetInvoiceData().IDTotalInvoice.ToString("N2")
    End Sub

    Private Sub LoadCustomerInfo()
        Dim customerInfo As CustomerInfo = InvoiceData.GetInvoiceData().IDCustomerInfo

        If (customerInfo Is Nothing) Then
            Return
        End If
        litCustomerName.Text = customerInfo.CustomerName
        litCustomerAddress1.Text = customerInfo.CustomerAddress1
        litCustomerAddress2.Text = customerInfo.CustomerAddress2
        litCustomerPhone.Text = customerInfo.CustomerPhone
        litCustomerEmail.Text = customerInfo.CustomerEmail
    End Sub

    Private Sub LoadInvoiceItems()
        itemsGrid.Visible = False
        If (InvoiceData.GetInvoiceData().IDInvoiceItems.Count > 0) Then
            itemsGrid.DataSource = InvoiceData.GetInvoiceData().IDInvoiceItems
            itemsGrid.DataBind()
            itemsGrid.Visible = True
        End If
    End Sub
End Class
