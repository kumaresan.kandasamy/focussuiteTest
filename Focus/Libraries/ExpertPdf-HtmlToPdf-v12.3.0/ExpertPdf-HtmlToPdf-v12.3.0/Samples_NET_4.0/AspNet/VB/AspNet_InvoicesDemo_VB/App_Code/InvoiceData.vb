Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections

Public Class InvoiceData

    Dim customerInfo As CustomerInfo = Nothing
    Dim invoiceItems As ArrayList = Nothing
    Dim totalInvoice As Double = 0

    Public Sub New()
        invoiceItems = New ArrayList()
    End Sub

    Public Shared Function GetInvoiceData() As InvoiceData
        If (HttpContext.Current.Session.Item("CustomerData") Is Nothing) Then
            HttpContext.Current.Session.Item("CustomerData") = New InvoiceData()
        End If
        Return CType(HttpContext.Current.Session.Item("CustomerData"), InvoiceData)
    End Function

    Public Sub Reset()
        invoiceItems.Clear()
        totalInvoice = 0
        customerInfo = Nothing
    End Sub

    Public ReadOnly Property IDTotalInvoice() As Double
        Get
            Return totalInvoice
        End Get
    End Property

    Public Sub AddItem(ByVal invoiceItem As InvoiceItem)
        invoiceItems.Add(invoiceItem)
        totalInvoice += invoiceItem.ItemPrice
    End Sub

    Public Property IDInvoiceItems() As ArrayList
        Get
            Return invoiceItems
        End Get
        Set(ByVal value As ArrayList)
            invoiceItems = value
        End Set
    End Property

    Public Property IDCustomerInfo() As CustomerInfo
        Get
            Return customerInfo
        End Get
        Set(ByVal value As CustomerInfo)
            customerInfo = value
        End Set
    End Property

End Class


Public Class InvoiceItem
    Dim productCode As String = Nothing
    Dim productDescription As String = String.Empty
    Dim productName As String = Nothing
    Dim productPrice As Double = 0
    Dim quantity As Integer = 0

    Public Sub New(ByVal productCode As String, ByVal productDescription As String, ByVal productName As String, ByVal productPrice As Double, ByVal quantity As Integer)
        Me.productCode = productCode
        Me.productDescription = productDescription
        Me.productName = productName
        Me.productPrice = productPrice
        Me.quantity = quantity
    End Sub

    Public Property ItemProductCode() As String
        Get
            Return productCode
        End Get
        Set(ByVal value As String)
            productCode = Value
        End Set
    End Property

    Public Property ItemProductDescription() As String
        Get
            Return productDescription
        End Get
        Set(ByVal value As String)
            productDescription = value
        End Set
    End Property

    Public Property ItemProductName() As String
        Get
            Return productName
        End Get
        Set(ByVal value As String)
            productName = Value
        End Set
    End Property

    Public Property ItemProductPrice() As Double
        Get
            Return productPrice
        End Get
        Set(ByVal value As Double)
            productPrice = value
        End Set
    End Property

    Public ReadOnly Property ItemPrice() As Double
        Get
            Return productPrice * quantity
        End Get
    End Property

    Public Property ItemQuantity() As Integer

        Get
            Return quantity
        End Get
        Set(ByVal value As Integer)
            quantity = value
        End Set
    End Property
End Class

Public Class CustomerInfo
    Dim name As String = Nothing
    Dim address1 As String = Nothing
    Dim address2 As String = Nothing
    Dim phone As String = Nothing
    Dim email As String = Nothing

    Public Sub New(ByVal name As String, ByVal address1 As String, ByVal address2 As String, ByVal phone As String, ByVal email As String)
        Me.name = name
        Me.address1 = address1
        Me.address2 = address2
        Me.phone = phone
        Me.email = email
    End Sub

    Public Property CustomerName() As String
        Get
            Return name
        End Get
        Set(ByVal value As String)
            name = value
        End Set
    End Property

    Public Property CustomerAddress1() As String
        Get
            Return address1
        End Get
        Set(ByVal value As String)
            address1 = value
        End Set
    End Property

    Public Property CustomerAddress2() As String
        Get
            Return address2
        End Get
        Set(ByVal value As String)
            address2 = Value
        End Set
    End Property

    Public Property CustomerPhone() As String
        Get
            Return phone
        End Get
        Set(ByVal value As String)
            phone = value
        End Set
    End Property

    Public Property CustomerEmail() As String
        Get
            Return email
        End Get
        Set(ByVal value As String)
            email = value
        End Set
    End Property

End Class
