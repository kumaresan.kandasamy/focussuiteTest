<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>HTML to PDF Converter - HTML Elements Position in PDF</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta http-equiv="Content-Language" content="english">
    <meta content="index,follow" name="robots">
    <link href="styles.css" type="text/css" rel="stylesheet">
    <meta content="MSHTML 6.00.2900.2963" name="GENERATOR">
</head>
<body>
    <form runat="server" id="convertForm">
        <!-- Header -->
        <div class="left">
            <table class="head" border="0">
                <tbody>
                    <tr>
                        <td style="width: 10px">
                            &nbsp;</td>
                        <td style="width: 90px">
                            <img src="img/logo.jpg" /></td>
                        <td valign="bottom" height="60" align="left" style="width: 85%">
                            <h1>
                                HTML Elements Position in PDF
                            </h1>
                        </td>
                        <td class="langoff" valign="top" align="right">
                            &nbsp;</td>
                    </tr>
                </tbody>
            </table>
            <br />
            <table>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        This demo shows how to obtain the position in PDF for a list of HTML elements specified
                        by HTML ID or by HTML tag name. The demo application will highlight with a green
                        rectangle all the IMG, H1 and H2 elements from the converted web page.
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td align="left" width="100%">
                        <table cellspacing="0" cellpadding="0" align="left" width="100%">
                            <tr>
                                <td style="height: 29px">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td style="font-weight: bold">
                                                Enter URL:</td>
                                            <td>
                                                &nbsp;</td>
                                            <td>
                                                <asp:TextBox ID="textBoxWebPageURL" runat="server" Text="http://www.html-to-pdf.net"
                                                    Width="315px"></asp:TextBox></td>
                                            <td>
                                                <asp:RequiredFieldValidator ID="urlRequired" runat="server" ControlToValidate="textBoxWebPageURL">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 5px">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblConvertMessage" runat="server" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:Button runat="server" Text="Convert" ID="btnConvert" OnClick="btnConvert_Click" />&nbsp;&nbsp;
                        &nbsp; &nbsp;</td>
                </tr>
            </table>
            <!-- Footer -->
            <br />
            <table class="footer">
                <tbody>
                    <tr>
                        <td class="seph">
                        </td>
                    </tr>
                    <tr>
                        <td class="lightbluetext">
                            Copyright by <a class="lightbluetext" href="http://www.html-to-pdf.net"
                                target="_blank">ExpertPDF</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>