Imports System.Drawing

Imports ExpertPdf.HtmlToPdf

Partial Class _Default
    Inherits System.Web.UI.Page


    Protected Sub btnConvert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConvert.Click
        Dim pdfConverter As PdfConverter = New PdfConverter()

        ' the URL of the HTML document to convert
        Dim thisPageURL As String = HttpContext.Current.Request.Url.AbsoluteUri
        Dim htmlTableFilePath As String = thisPageURL.Substring(0, thisPageURL.LastIndexOf("/"c)) + "/HtmlTable/table_with_repeated_head.html"

        ' call the converter
        Dim pdfBytes As Byte() = pdfConverter.GetPdfBytesFromUrl(htmlTableFilePath)

        ' send the PDF document as a response to the browser for download
        Dim Response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
        Response.Clear()
        Response.AddHeader("Content-Type", "binary/octet-stream")
        Response.AddHeader("Content-Disposition", "attachment; filename=ConversionResult.pdf; size=" & pdfBytes.Length.ToString())
        Response.Flush()
        Response.BinaryWrite(pdfBytes)
        Response.Flush()
        Response.End()

    End Sub

End Class
