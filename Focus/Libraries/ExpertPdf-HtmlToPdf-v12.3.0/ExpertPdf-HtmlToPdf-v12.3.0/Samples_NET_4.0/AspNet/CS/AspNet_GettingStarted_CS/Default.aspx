<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Getting Started With HTML to PDF Converter for .NET C#</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta http-equiv="Content-Language" content="english">
    <meta content="index,follow" name="robots">
    <link href="styles.css" type="text/css" rel="stylesheet">
    <meta content="MSHTML 6.00.2900.2963" name="GENERATOR">
</head>
<body>
    <form runat="server" id="convertForm">
        <!-- Header -->
        <div class="left">
            <table class="head" border="0">
                <tbody>
                    <tr>
                        <td style="width: 10px">
                            &nbsp;</td>
                        <td style="width: 90px">
                            <img src="img/logo.jpg" /></td>
                        <td valign="bottom" height="60" align="left" style="width: 85%">
                            <h1>
                                Getting Started with HTML to PDF Converter for .NET
                            </h1>
                        </td>
                        <td class="langoff" valign="top" align="right">
                            &nbsp;</td>
                    </tr>
                </tbody>
            </table>
            <br />
            <table>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        First select to convert a URL or a HTML code, then select to <strong>convert to PDF
                            or image format </strong>and press the Convert button. When converting a HTML
                        string the <strong>Base URL </strong>option help the converter from where to take
                        the CSS and images referenced by relative URLs. The Base URL option is active only
                        if the HEAD tag appears in the HTML code. The HTML string also contains a <strong>custom
                            page break</strong> introduced by the <strong>page-break-after:always</strong>
                        style.
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td style="height: 50px">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="3" style="font-weight: bold; height: 13px;">
                                    Select HTML source:</td>
                            </tr>
                            <tr>
                                <td colspan="3" style="height: 5px">
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 20px">
                                    <asp:RadioButton ID="radioConvertURL" runat="server" GroupName="SelectConvertSource"
                                        Text="Convert HTML from URL" AutoPostBack="True" OnCheckedChanged="radioConvertURL_CheckedChanged"
                                        Checked="True" Font-Bold="False" />
                                </td>
                                <td width="20px" style="height: 20px">
                                </td>
                                <td style="height: 20px">
                                    <asp:RadioButton ID="radioConvertHTML" runat="server" GroupName="SelectConvertSource"
                                        Text="Convert HTML String" AutoPostBack="True" OnCheckedChanged="radioConvertHTML_CheckedChanged"
                                        Font-Bold="False" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 5px">
                    </td>
                </tr>
                <tr>
                    <td align="left" width="100%">
                        <asp:Panel ID="pnlConvertURL" runat="server" Width="100%">
                            <table cellspacing="0" cellpadding="0" align="left" width="100%">
                                <tr>
                                    <td style="height: 29px">
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="font-weight: bold">
                                                    Enter URL:</td>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    <asp:TextBox ID="textBoxWebPageURL" runat="server" Text="http://www.html-to-pdf.net"
                                                        Width="315px"></asp:TextBox></td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="urlRequired" runat="server" ControlToValidate="textBoxWebPageURL">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pnlConvertHTML" runat="server" Width="100%" Visible="false">
                            <table cellspacing="0" cellpadding="0" align="left" width="100%">
                                <tr>
                                    <td style="height: 29px">
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="height: 29px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold; width: 147px;" valign="top">
                                                    Enter HTML Code:</td>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    <asp:TextBox ID="textBoxHTMLCode" runat="server" Width="451px" Height="202px" TextMode="MultiLine">
<html>
<head>
    <title>Simple HTML String</title>
</head>
<body>
    <table>
        <!-- first page -->
        <!-- Insert a CSS page break with page-break-after:always-->
        <tr>
            <td style="page-break-after:always;font-family: Verdana; font-size: medium; color: Blue">
                <a href="http://www.html-to-pdf.net"> Hello World!!!</a>
            </td>
        </tr>
        <!-- Second page -->
        <tr>
            <td style="font-family: Verdana; font-size: medium; color: Blue">
                Hello World from Second Page !!!
            </td>
        </tr>
    </table>
</body>
</html>


                                                    </asp:TextBox></td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="textBoxWebPageURL">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold; width: 147px;">
                                                    Base URL (Optional):
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    <asp:TextBox ID="textBoxBaseURL" runat="server" Text="" Width="447px"></asp:TextBox></td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="height: 5px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 550px">
                        <table style="width: 506px" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table style="width: 351px" id="convertTypeTable" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td style="height: 22px">
                                                    <asp:RadioButton ID="radioConvertToSelectablePDF" runat="server" Text="Convert to PDF"
                                                        GroupName="ConvertFormat" Checked="True"></asp:RadioButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 22px">
                                                    <asp:RadioButton ID="radioConvertToImage" runat="server" Text="Convert to image"
                                                        GroupName="ConvertFormat"></asp:RadioButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 15px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="width: 500px">
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="cbFitWidth" runat="server" Text="Fit Width" ToolTip="Resize the rendered HTML content to fit the specified size"
                                                    Checked="True" /></td>
                                            <td style="width: 20px">
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="cbLiveLinks" runat="server" Text="Live Links" ToolTip="Enable HTTP links in the rendered PDF document"
                                                    Checked="True" /></td>
                                            <td>
                                            </td>
                                            <td style="width: 20px">
                                            </td>
                                            <td>
                                                &nbsp;<asp:CheckBox ID="cbEmbedFonts" runat="server" Text="Embed Fonts" ToolTip="Embed the true type fonts in the rendered PDF document" /></td>
                                            <td style="width: 20px">
                                            </td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="cbBookmarks" runat="server" Text="Bookmarks" ToolTip="Bookmark H1 and H2 HTML tags" /></td>
                                            <td style="width: 20px">
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="cbClientScripts" runat="server" Text="JavaScript" ToolTip="Enable JavaScript and other client scripts during conversion" Checked="true" />
                                            </td>
                                            <td>
                                            </td>
                                            <td style="width: 20px">
                                            </td>
                                            <td>
                                                &nbsp;<asp:CheckBox ID="cbActiveXEnabled" runat="server" Text="ActiveX - Flash" ToolTip="Enable ActiveX download and execution during conversion" Checked="true" />
                                            </td>
                                            <td style="width: 20px"></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="cbJpegCompression" runat="server" Text="JPEG Compression" ToolTip="Enable or disable JPEG compression of images in PDF"
                                                    Checked="True" />
                                            </td>
                                            <td style="width: 20px">
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="cbAddHeader" runat="server" Text="Add Header" ToolTip="Add a HTML header to the rendered PDF document" />
                                            </td>
                                            <td>
                                            </td>
                                            <td style="width: 20px">
                                            </td>
                                            <td colspan="3">
                                                &nbsp;<asp:CheckBox ID="cbAddFooter" runat="server" Text="Add Footer" ToolTip="Add
                                                a footer to the rendered PDF document" Checked="True" /></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 5px">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblConvertMessage" runat="server" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:Button runat="server" Text="Convert" ID="btnConvert" OnClick="btnConvert_Click" />&nbsp;&nbsp;
                        &nbsp; &nbsp;</td>
                </tr>
            </table>
            <!-- Footer -->
            <br />
            <table class="footer">
                <tbody>
                    <tr>
                        <td class="seph">
                        </td>
                    </tr>
                    <tr>
                        <td class="lightbluetext">
                            Copyright by <a class="lightbluetext" href="http://www.html-to-pdf.net"
                                target="_blank">ExpertPDF</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
