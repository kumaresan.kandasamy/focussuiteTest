﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form id="form1" runat="server">
        <table>
            <tr>
                <td style="width: 667px">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 667px">
                    To convert a web page to image or to PDF enter the web page full URL in the text
                    box below and press 'Convert' button. To customize the converter press 'More Converter
                    Settings'.
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td style="width: 667px">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 667px">
                    <table cellspacing="0" cellpadding="0" align="left">
                        <tr>
                            <td style="width: 634px">
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 29px; width: 634px;">
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="font-weight: bold">
                                            Enter URL:</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            <asp:TextBox ID="textBoxWebPageURL" runat="server" Text="http://www.html-to-pdf.net"
                                                Width="315px"></asp:TextBox></td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="urlRequired" runat="server" ControlToValidate="textBoxWebPageURL">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 634px">
                                <table cellspacing="0" cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <td style="width: 639px">
                                                <table style="width: 351px" id="convertTypeTable" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="height: 22px">
                                                                <asp:RadioButton ID="radioConvertToSelectableText" runat="server" Text="Convert to PDF"
                                                                    OnCheckedChanged="radioConvertToSelectableText_CheckedChanged" AutoPostBack="True"
                                                                    GroupName="ConvertFormat" Checked="True"></asp:RadioButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 22px">
                                                                <asp:RadioButton ID="radioConvertToImage" runat="server" Text="Convert to image"
                                                                    OnCheckedChanged="radioConvertToImage_CheckedChanged" AutoPostBack="True" GroupName="ConvertFormat">
                                                                </asp:RadioButton>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 639px">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 639px">
                                                <asp:Panel ID="htmlConvertControlPanel" runat="server" Width="100%">
                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0" style="width: 635px">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellpadding="0" cellspacing="0" style="width: 436px">
                                                                                        <tr>
                                                                                            <td style="width: 130px; height: 20px">
                                                                                                <asp:CheckBox ID="cbLiveLinksEnabled" runat="server" Text="Live HTTP links" Checked="True"
                                                                                                    ToolTip="Make the HTTP links live in the generated PDF"></asp:CheckBox>
                                                                                            </td>
                                                                                            <td style="width: 8px; height: 20px;">
                                                                                            </td>
                                                                                            <td style="height: 20px; width: 105px;">
                                                                                                <asp:CheckBox ID="cbBookmarks" runat="server" Text="Bookmarks" Checked="False" ToolTip="Bookmark H1 and H2 tags">
                                                                                                </asp:CheckBox>
                                                                                            </td>
                                                                                            <td style="width: 6px; height: 20px;">
                                                                                            </td>
                                                                                            <td style="width: 158px; height: 20px">
                                                                                                &nbsp;<asp:CheckBox ID="cbFitWidth" runat="server" Text="Fit Width" Checked="True"
                                                                                                    ToolTip="When is checked the HTML content will be resized if necessary to fit the PDF page">
                                                                                                </asp:CheckBox></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="height: 28px; width: 130px;">
                                                                                                <asp:CheckBox ID="cbScriptsEnabled" runat="server" Text="Scripts Enabled" Checked="true"
                                                                                                    ToolTip="Enable JavaScripts and Java applets during conversion"></asp:CheckBox>
                                                                                            </td>
                                                                                            <td style="width: 8px">
                                                                                            </td>
                                                                                            <td style="width: 105px">
                                                                                                <asp:CheckBox ID="cbEmbedFonts" runat="server" Text="Embed Fonts" Checked="False"
                                                                                                    ToolTip="When is checked the true type fonts will be embedded in the PDF document">
                                                                                                </asp:CheckBox>
                                                                                            </td>
                                                                                            <td style="width: 6px">
                                                                                            </td>
                                                                                            <td style="width: 158px">
                                                                                                &nbsp;<asp:CheckBox ID="cbJpegCompression" runat="server" Text="JPEG Compression"
                                                                                                    Checked="True" ToolTip="When is checked the images are compressed with JPEG in PDF"
                                                                                                    Width="145px"></asp:CheckBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="height: 15px">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="height: 15px">
                                                                                    <asp:LinkButton ID="lnkBtnSettings" OnClick="lnkBtnSettings_Click" runat="server">More Converter Settings >></asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 639px">
                                                <asp:Panel ID="pnlRenderMoreOptions" runat="server" Visible="false">
                                                    <table cellspacing="0" cellpadding="0" style="width: 394px">
                                                        <tbody>
                                                            <tr>
                                                                <td style="width: 424px">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 424px">
                                                                    <asp:Panel ID="pnlRenderCommonOptions" runat="server">
                                                                        <table style="width: 292px" id="commonOptionsTable" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="font-weight: bold; width: 246px; height: 30px">
                                                                                        Web page size:</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 246px">
                                                                                        <table style="width: 285px" cellspacing="0" cellpadding="0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:RadioButton ID="radioAutodetectWebPageSize" runat="server" Text="Autodetect"
                                                                                                            OnCheckedChanged="radioAutodetectWebPageSize_CheckedChanged" AutoPostBack="True"
                                                                                                            GroupName="WebPageSize"></asp:RadioButton>
                                                                                                    </td>
                                                                                                    <td style="height: 30px">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                    <td style="width: 116px; height: 30px">
                                                                                                        <asp:RadioButton ID="radioCustomWebPageSize" runat="server" Text="Custom" OnCheckedChanged="radioCustomWebPageSize_CheckedChanged"
                                                                                                            AutoPostBack="True" GroupName="WebPageSize" Checked="True"></asp:RadioButton>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="3">
                                                                                                        <asp:Panel ID="pnlCustomPageSize" runat="server" Visible="true">
                                                                                                            <table cellspacing="2" cellpadding="2">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            Set Page Width:</td>
                                                                                                                        <td>
                                                                                                                            <asp:TextBox ID="textBoxCustomWebPageWidth" runat="server" Columns="4" MaxLength="10"
                                                                                                                                Width="71px">1024</asp:TextBox><asp:CustomValidator ID="cvCustomPageWidth" runat="server"
                                                                                                                                    Display="Dynamic" OnServerValidate="cvCustomPageWidth_ServerValidate">*</asp:CustomValidator></td>
                                                                                                                        <td>
                                                                                                                            px</td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </asp:Panel>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 246px">
                                                                                        &nbsp;</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 424px">
                                                                    <asp:Panel ID="pnlImageRenderOptions" runat="server" Visible="false">
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="font-weight: bold">
                                                                                        Image format:</td>
                                                                                    <td>
                                                                                        &nbsp;</td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="ddlImageFormat" runat="server">
                                                                                        </asp:DropDownList></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 424px">
                                                                    <asp:Panel ID="pnlPDFRenderOptions" runat="server">
                                                                        <table id="PDFRenderOptionsTable" cellspacing="0" cellpadding="0" style="width: 355px">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="font-weight: bold" colspan="2">
                                                                                        PDF Document Options
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="height: 23px">
                                                                                        PDF page format:</td>
                                                                                    <td style="width: 64px; height: 23px;">
                                                                                        <asp:DropDownList ID="ddlPDFPageFormat" runat="server">
                                                                                        </asp:DropDownList></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="height: 23px">
                                                                                        PDF Compression Level:</td>
                                                                                    <td style="width: 64px; height: 23px;">
                                                                                        <asp:DropDownList ID="ddlCompressionLevel" runat="server">
                                                                                        </asp:DropDownList></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="height: 23px">
                                                                                        PDF Page Orientation:
                                                                                    </td>
                                                                                    <td style="width: 64px; height: 23px">
                                                                                        <asp:DropDownList ID="ddlPageOrientation" runat="server">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="height: 23px">
                                                                                        PDF Standard:
                                                                                    </td>
                                                                                    <td style="width: 64px; height: 23px;">
                                                                                        <asp:DropDownList ID="ddlPdfSubset" runat="server">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="height: 24px" colspan="3">
                                                                                                        Header &amp; Footer:
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="height: 20px">
                                                                                                        <asp:CheckBox ID="cbShowheader" runat="server" Text="Show header" OnCheckedChanged="cbShowheader_CheckedChanged"
                                                                                                            AutoPostBack="true"></asp:CheckBox>
                                                                                                    </td>
                                                                                                    <td style="height: 20px; width: 16px;">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                    <td style="height: 20px">
                                                                                                        <asp:CheckBox ID="cbShowFooter" runat="server" Text="Show Footer" OnCheckedChanged="cbShowFooter_CheckedChanged"
                                                                                                            AutoPostBack="true"></asp:CheckBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="height: 25px">
                                                                                                        Document Margins:
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        Left:
                                                                                                                        <asp:TextBox ID="textBoxLeftMargin" runat="server" Columns="5" MaxLength="10">0</asp:TextBox>pt
                                                                                                                        <asp:CustomValidator ID="cvLeftMargin" runat="server" OnServerValidate="cvLeftMargin_ServerValidate"
                                                                                                                            Display="Dynamic">*</asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        Right: &nbsp; &nbsp;
                                                                                                                        <asp:TextBox ID="textBoxRightMargin" runat="server" Columns="5" MaxLength="10">0</asp:TextBox>pt
                                                                                                                        <asp:CustomValidator ID="cvRightMargin" runat="server" OnServerValidate="cvRightMargin_ServerValidate"
                                                                                                                            Display="Dynamic">*</asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="height: 48px">
                                                                                                                        Top:
                                                                                                                        <asp:TextBox ID="textBoxTopMargin" runat="server" Columns="5" MaxLength="10">0</asp:TextBox>pt
                                                                                                                        <asp:CustomValidator ID="cvTopMargin" runat="server" OnServerValidate="cvTopMargin_ServerValidate"
                                                                                                                            Display="Dynamic">*</asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                    <td style="height: 48px">
                                                                                                                        Bottom:
                                                                                                                        <asp:TextBox ID="textBoxBottomMargin" runat="server" Columns="5" MaxLength="10">0</asp:TextBox>pt
                                                                                                                        <asp:CustomValidator ID="cvBottomMargin" runat="server" OnServerValidate="cvBottomMargin_ServerValidate"
                                                                                                                            Display="Dynamic">*</asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" style="height: 25px">
                                                                                        <asp:CheckBox ID="cbAvoidImageBreak" runat="server" Text="Avoid image break" /></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <asp:Panel ID="pnlPDFHeaderOptions" runat="server" Visible="false">
                                                                                            <table cellspacing="0" cellpadding="0" style="width: 347px">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td colspan="2">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="font-weight: bold; height: 20px;" colspan="2">
                                                                                                            PDF Header Options:
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="2">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="height: 23px">
                                                                                                            Header Title:
                                                                                                        </td>
                                                                                                        <td style="width: 230px; height: 23px;">
                                                                                                            <asp:TextBox ID="textBoxHeaderText" runat="server" Width="213px">Title</asp:TextBox>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="height: 23px">
                                                                                                            Header Text Color:
                                                                                                        </td>
                                                                                                        <td style="width: 230px; height: 23px;">
                                                                                                            <asp:DropDownList ID="ddlHeaderColor" runat="server">
                                                                                                            </asp:DropDownList>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="height: 23px">
                                                                                                            Header Subtitle:
                                                                                                        </td>
                                                                                                        <td style="width: 230px; height: 23px;">
                                                                                                            <asp:TextBox ID="textBoxHeaderSubtitle" runat="server" Width="211px">Subtitle</asp:TextBox>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="2" style="height: 23px">
                                                                                                            <asp:CheckBox ID="cbDrawHeaderLine" runat="server" Text="Draw Header Line" Checked="True">
                                                                                                            </asp:CheckBox>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </asp:Panel>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <asp:Panel ID="pnlPDFFooterOptions" runat="server" Visible="false">
                                                                                            <table cellspacing="0" cellpadding="0" style="width: 345px">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td colspan="2">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="2" style="font-weight: bold; width: 70px; height: 20px;">
                                                                                                            PDF Footer Options:
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="2">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 125px; height: 23px;">
                                                                                                            Footer Text:
                                                                                                        </td>
                                                                                                        <td style="height: 23px">
                                                                                                            <asp:TextBox ID="textBoxFooterText" runat="server" Width="205px">Footer</asp:TextBox>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 125px; height: 23px;">
                                                                                                            Footer Text Color:
                                                                                                        </td>
                                                                                                        <td style="height: 23px">
                                                                                                            <asp:DropDownList ID="ddlFooterTextColor" runat="server">
                                                                                                            </asp:DropDownList>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="2" style="height: 23px">
                                                                                                            <asp:CheckBox ID="cbShowPageNumber" runat="server" Text="Show Page Number" Checked="True">
                                                                                                            </asp:CheckBox>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 125px; height: 24px;">
                                                                                                            Page Number Text:
                                                                                                        </td>
                                                                                                        <td style="height: 24px">
                                                                                                            <asp:TextBox ID="textBoxPageNmberText" runat="server" Width="204px">Page </asp:TextBox>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="2" style="height: 23px">
                                                                                                            <asp:CheckBox ID="cbDrawFooterLine" runat="server" Text="Draw Footer Line" Checked="True">
                                                                                                            </asp:CheckBox>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </asp:Panel>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </asp:Panel>
                                                &nbsp;&nbsp;
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 634px">
                                <asp:Label ID="lblConvertMessage" runat="server" ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 634px">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 634px">
                                <asp:Button runat="server" Text="Convert" ID="btnConvert" OnClick="btnConvert_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</asp:Content>
