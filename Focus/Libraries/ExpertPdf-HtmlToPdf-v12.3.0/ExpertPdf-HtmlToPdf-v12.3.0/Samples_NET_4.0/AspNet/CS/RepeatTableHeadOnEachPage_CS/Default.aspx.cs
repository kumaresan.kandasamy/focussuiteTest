using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Drawing;

using ExpertPdf.HtmlToPdf;
using ExpertPdf.HtmlToPdf.PdfDocument;

public partial class _Default : System.Web.UI.Page 
{
    private const int TOC_ENTRIES_COUNT = 16;
    private const int PAGE_NUMBER_FONT_SIZE = 10;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnConvert_Click(object sender, EventArgs e)
    {
        PdfConverter pdfConverter = new PdfConverter();

        // the URL of the HTML document to convert
        string thisPageURL = HttpContext.Current.Request.Url.AbsoluteUri;
        string htmlTableFilePath = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/HtmlTable/table_with_repeated_head.html";

        // call the converter
        byte[] pdfBytes = pdfConverter.GetPdfBytesFromUrl(htmlTableFilePath);

        // send the PDF document as a response to the browser for download
        System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        response.Clear();
        response.AddHeader("Content-Type", "binary/octet-stream");
        response.AddHeader("Content-Disposition",
            "attachment; filename=ConversionResult.pdf; size=" + pdfBytes.Length.ToString());
        response.Flush();
        response.BinaryWrite(pdfBytes);
        response.Flush();
        response.End();
    }
}
