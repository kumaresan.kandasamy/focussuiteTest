using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Drawing;

using ExpertPdf.HtmlToPdf;
using ExpertPdf.HtmlToPdf.PdfDocument;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnConvert_Click(object sender, EventArgs e)
    {
        PdfConverter pdfConverter = new PdfConverter();

        // inform the converter about the HTML elements for which we want the location in PDF
        // in this sample we want the location of IMG, H1 and H2 elements
        pdfConverter.HtmlElementsMappingOptions.HtmlTagNames = new string[] { "IMG", "H1", "H2" };
        // add an addtional list of the HTML IDs of the HTML elements for which to retrieve position in PDF
        pdfConverter.HtmlElementsMappingOptions.HtmlElementIds = new string[] { "id1", "id2" };

        // call the converter and get a Document object from URL
        Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromUrl(textBoxWebPageURL.Text.Trim());

        // iterate over the HTML elements locations and hightlight each element with a green rectangle
        foreach (HtmlElementMapping elementMapping in pdfConverter.HtmlElementsMappingOptions.HtmlElementsMappingResult)
        {
            // because a HTML element can span over many PDF pages the mapping 
            // of the HTML element in PDF document consists in a list of rectangles,
            // one rectangle for each PDF page where this element was rendered
            foreach (HtmlElementPdfRectangle elementLocationInPdf in elementMapping.PdfRectangles)
            {
                // get the PDF page
                PdfPage pdfPage = pdfDocument.Pages[elementLocationInPdf.PageIndex];
                RectangleF pdfRectangleInPage = elementLocationInPdf.Rectangle;

                // create a RectangleElement to highlight the HTML element
                RectangleElement highlightRectangle = new RectangleElement(pdfRectangleInPage.X, pdfRectangleInPage.Y,
                    pdfRectangleInPage.Width, pdfRectangleInPage.Height);
                highlightRectangle.ForeColor = Color.Green;

                pdfPage.AddElement(highlightRectangle);
            }
        }

        byte[] pdfBytes = null;

        try
        {
            pdfBytes = pdfDocument.Save();
        }
        finally
        {
            // close the Document to realease all the resources
            pdfDocument.Close();
        }

        // send the PDF document as a response to the browser for download
        System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        response.Clear();
        response.AddHeader("Content-Type", "binary/octet-stream");
        response.AddHeader("Content-Disposition",
            "attachment; filename=ConversionResult.pdf; size=" + pdfBytes.Length.ToString());
        response.Flush();
        response.BinaryWrite(pdfBytes);
        response.Flush();
        response.End();
    }
}
