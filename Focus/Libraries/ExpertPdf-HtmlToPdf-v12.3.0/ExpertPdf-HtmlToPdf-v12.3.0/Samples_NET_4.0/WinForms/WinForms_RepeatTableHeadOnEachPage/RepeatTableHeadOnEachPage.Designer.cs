namespace RepeatTableHeadOnEachPage_CS
{
    partial class RepeatTableHeadOnEachPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepeatTableHeadOnEachPage));
            this.label3 = new System.Windows.Forms.Label();
            this.btnConvert = new System.Windows.Forms.Button();
            this.lnkViewHtmlTable = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(19, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(542, 52);
            this.label3.TabIndex = 11;
            this.label3.Text = resources.GetString("label3.Text");
            // 
            // btnConvert
            // 
            this.btnConvert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConvert.Location = new System.Drawing.Point(22, 100);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(140, 23);
            this.btnConvert.TabIndex = 10;
            this.btnConvert.Text = "Convert to PDF";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // lnkViewHtmlTable
            // 
            this.lnkViewHtmlTable.AutoSize = true;
            this.lnkViewHtmlTable.Location = new System.Drawing.Point(192, 105);
            this.lnkViewHtmlTable.Name = "lnkViewHtmlTable";
            this.lnkViewHtmlTable.Size = new System.Drawing.Size(203, 13);
            this.lnkViewHtmlTable.TabIndex = 12;
            this.lnkViewHtmlTable.TabStop = true;
            this.lnkViewHtmlTable.Text = "View HTML Table with Repeated Header";
            this.lnkViewHtmlTable.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkViewHtmlTable_LinkClicked);
            // 
            // RepeatTableHeadOnEachPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(577, 134);
            this.Controls.Add(this.lnkViewHtmlTable);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnConvert);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "RepeatTableHeadOnEachPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Repeat HTML Table Head on Each PDF Page";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.LinkLabel lnkViewHtmlTable;
    }
}

