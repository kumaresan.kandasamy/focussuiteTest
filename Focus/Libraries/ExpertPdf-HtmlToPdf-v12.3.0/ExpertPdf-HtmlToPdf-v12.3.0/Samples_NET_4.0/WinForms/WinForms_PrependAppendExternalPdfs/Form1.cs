using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using ExpertPdf.HtmlToPdf;

namespace WinForms_ConvertAndMergePdf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnConvertMerge_Click(object sender, EventArgs e)
        {
            MemoryStream pdfStream2 = null;
            MemoryStream pdfStream3 = null;

            try
            {
                PdfConverter pdfConverter = new PdfConverter();

                //pdfConverter.LicenseKey = "put your license key here";

                // get the documents that will be appended to the result of the conversion of first URL
                byte[] doc2Bytes = pdfConverter.GetPdfFromUrlBytes(textBoxURL2.Text);
                pdfStream2 = new MemoryStream(doc2Bytes);

                byte[] doc3Bytes = pdfConverter.GetPdfFromUrlBytes(textBoxURL3.Text);
                pdfStream3 = new MemoryStream(doc3Bytes);

                // set the PDF streams to be appended to the result of the conversion of the first URL
                pdfConverter.PdfDocumentOptions.AppendPDFStreamArray = new Stream[] { pdfStream2, pdfStream3 };

                //Produce the final PDF as a merge of the URL1, URL2 and URL 3 conversions
                string outFilePath = Path.Combine(Application.StartupPath, "MergedDocument.pdf");
                pdfConverter.SavePdfFromUrlToFile(textBoxURL1.Text, outFilePath);

                DialogResult dr = MessageBox.Show("Open the rendered file in an external viewer?", "Open Rendered File", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    System.Diagnostics.Process.Start(outFilePath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                if (pdfStream2 != null) 
                    pdfStream2.Close();

                if (pdfStream3 != null)
                    pdfStream3.Close();
            }
        }
    }
}