namespace NavigationConvertDemo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.topPanel = new System.Windows.Forms.Panel();
            this.btnNavigate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxURL = new System.Windows.Forms.TextBox();
            this.webBrowserPanel = new System.Windows.Forms.Panel();
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.controlPanel = new System.Windows.Forms.Panel();
            this.cbBookmarks = new System.Windows.Forms.CheckBox();
            this.cbActiveX = new System.Windows.Forms.CheckBox();
            this.cbAvoidImageBreak = new System.Windows.Forms.CheckBox();
            this.cbScriptsEnabled = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.topPanel.SuspendLayout();
            this.webBrowserPanel.SuspendLayout();
            this.controlPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.btnNavigate);
            this.topPanel.Controls.Add(this.label1);
            this.topPanel.Controls.Add(this.textBoxURL);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(1016, 46);
            this.topPanel.TabIndex = 0;
            // 
            // btnNavigate
            // 
            this.btnNavigate.Location = new System.Drawing.Point(695, 10);
            this.btnNavigate.Name = "btnNavigate";
            this.btnNavigate.Size = new System.Drawing.Size(133, 23);
            this.btnNavigate.TabIndex = 2;
            this.btnNavigate.Text = "Navigate to URL";
            this.btnNavigate.UseVisualStyleBackColor = true;
            this.btnNavigate.Click += new System.EventHandler(this.btnNavigate_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "URL:";
            // 
            // textBoxURL
            // 
            this.textBoxURL.Location = new System.Drawing.Point(51, 12);
            this.textBoxURL.Name = "textBoxURL";
            this.textBoxURL.Size = new System.Drawing.Size(623, 20);
            this.textBoxURL.TabIndex = 0;
            this.textBoxURL.Text = "http://www.html-to-pdf.net";
            this.textBoxURL.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxURL_KeyDown);
            // 
            // webBrowserPanel
            // 
            this.webBrowserPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.webBrowserPanel.Controls.Add(this.webBrowser);
            this.webBrowserPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowserPanel.Location = new System.Drawing.Point(0, 46);
            this.webBrowserPanel.Name = "webBrowserPanel";
            this.webBrowserPanel.Size = new System.Drawing.Size(1016, 677);
            this.webBrowserPanel.TabIndex = 1;
            // 
            // webBrowser
            // 
            this.webBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser.Location = new System.Drawing.Point(0, 0);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(1012, 673);
            this.webBrowser.TabIndex = 0;
            // 
            // controlPanel
            // 
            this.controlPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.controlPanel.Controls.Add(this.cbBookmarks);
            this.controlPanel.Controls.Add(this.cbActiveX);
            this.controlPanel.Controls.Add(this.cbAvoidImageBreak);
            this.controlPanel.Controls.Add(this.cbScriptsEnabled);
            this.controlPanel.Controls.Add(this.button1);
            this.controlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.controlPanel.Location = new System.Drawing.Point(0, 670);
            this.controlPanel.Name = "controlPanel";
            this.controlPanel.Size = new System.Drawing.Size(1016, 53);
            this.controlPanel.TabIndex = 2;
            // 
            // cbBookmarks
            // 
            this.cbBookmarks.AutoSize = true;
            this.cbBookmarks.Location = new System.Drawing.Point(720, 23);
            this.cbBookmarks.Name = "cbBookmarks";
            this.cbBookmarks.Size = new System.Drawing.Size(79, 17);
            this.cbBookmarks.TabIndex = 1;
            this.cbBookmarks.Text = "Bookmarks";
            this.cbBookmarks.UseVisualStyleBackColor = true;
            // 
            // cbActiveX
            // 
            this.cbActiveX.AutoSize = true;
            this.cbActiveX.Location = new System.Drawing.Point(642, 23);
            this.cbActiveX.Name = "cbActiveX";
            this.cbActiveX.Size = new System.Drawing.Size(63, 17);
            this.cbActiveX.TabIndex = 1;
            this.cbActiveX.Text = "ActiveX";
            this.cbActiveX.UseVisualStyleBackColor = true;
            // 
            // cbAvoidImageBreak
            // 
            this.cbAvoidImageBreak.AutoSize = true;
            this.cbAvoidImageBreak.Location = new System.Drawing.Point(431, 23);
            this.cbAvoidImageBreak.Name = "cbAvoidImageBreak";
            this.cbAvoidImageBreak.Size = new System.Drawing.Size(114, 17);
            this.cbAvoidImageBreak.TabIndex = 1;
            this.cbAvoidImageBreak.Text = "Avoid image break";
            this.cbAvoidImageBreak.UseVisualStyleBackColor = true;
            // 
            // cbScriptsEnabled
            // 
            this.cbScriptsEnabled.AutoSize = true;
            this.cbScriptsEnabled.Location = new System.Drawing.Point(551, 23);
            this.cbScriptsEnabled.Name = "cbScriptsEnabled";
            this.cbScriptsEnabled.Size = new System.Drawing.Size(76, 17);
            this.cbScriptsEnabled.TabIndex = 1;
            this.cbScriptsEnabled.Text = "JavaScript";
            this.cbScriptsEnabled.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(11, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(334, 38);
            this.button1.TabIndex = 0;
            this.button1.Text = "Convert To PDF";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 723);
            this.Controls.Add(this.controlPanel);
            this.Controls.Add(this.webBrowserPanel);
            this.Controls.Add(this.topPanel);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Navigate and Convert Demo";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.webBrowserPanel.ResumeLayout(false);
            this.controlPanel.ResumeLayout(false);
            this.controlPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.TextBox textBoxURL;
        private System.Windows.Forms.Panel webBrowserPanel;
        private System.Windows.Forms.WebBrowser webBrowser;
        private System.Windows.Forms.Panel controlPanel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnNavigate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbActiveX;
        private System.Windows.Forms.CheckBox cbScriptsEnabled;
        private System.Windows.Forms.CheckBox cbAvoidImageBreak;
        private System.Windows.Forms.CheckBox cbBookmarks;
    }
}

