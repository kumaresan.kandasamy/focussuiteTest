namespace HtmlConvertAppDemo
{
    partial class HtmlConvertForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.pnlContent = new System.Windows.Forms.Panel();
            this.pnlSettingsPanel = new System.Windows.Forms.Panel();
            this.pnlPDFSettings = new System.Windows.Forms.Panel();
            this.pnlFooterOptions = new System.Windows.Forms.Panel();
            this.groupBoxPDFFooterOptions = new System.Windows.Forms.GroupBox();
            this.label62 = new System.Windows.Forms.Label();
            this.cbShowFooterOnFirstPage = new System.Windows.Forms.CheckBox();
            this.label63 = new System.Windows.Forms.Label();
            this.textBoxPageNumberYLocation = new System.Windows.Forms.TextBox();
            this.cbShowFooterOnEvenPages = new System.Windows.Forms.CheckBox();
            this.cbShowFooterOnOddPages = new System.Windows.Forms.CheckBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.textBoxFooterTextYLocation = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.textBoxFooterWidth = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.textBoxPageNumberText = new System.Windows.Forms.TextBox();
            this.ddlFooterBackColor = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.cbShowPageNumber = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.cbDrawFooterLine = new System.Windows.Forms.CheckBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.textBoxFooterText = new System.Windows.Forms.TextBox();
            this.ddlPageNumberTextColor = new System.Windows.Forms.ComboBox();
            this.ddlFooterColor = new System.Windows.Forms.ComboBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.textBoxFooterHeight = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.textBoxPageNumberFontSize = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.ddlPageNumberTextFont = new System.Windows.Forms.ComboBox();
            this.textBoxFooterTextFontSize = new System.Windows.Forms.TextBox();
            this.ddlFooterTextFont = new System.Windows.Forms.ComboBox();
            this.pnlHeaderOptions = new System.Windows.Forms.Panel();
            this.groupBoxPDFHeaderOptions = new System.Windows.Forms.GroupBox();
            this.cbShowHeaderOnEvenPages = new System.Windows.Forms.CheckBox();
            this.cbShowHeaderOnFirstPage = new System.Windows.Forms.CheckBox();
            this.cbShowHeaderOnOddPages = new System.Windows.Forms.CheckBox();
            this.ddlTitleAlign = new System.Windows.Forms.ComboBox();
            this.label60 = new System.Windows.Forms.Label();
            this.ddlSubtitleFont = new System.Windows.Forms.ComboBox();
            this.ddlSubtitleColor = new System.Windows.Forms.ComboBox();
            this.ddlHeaderBackColor = new System.Windows.Forms.ComboBox();
            this.btnChangeHeaderImage = new System.Windows.Forms.Button();
            this.lblLicensesDirectory = new System.Windows.Forms.Label();
            this.textBoxHeaderImagePath = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxHeadeSubtitle = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxHeaderTitle = new System.Windows.Forms.TextBox();
            this.ddlHeaderFont = new System.Windows.Forms.ComboBox();
            this.textBoxHeaderImageYLocation = new System.Windows.Forms.TextBox();
            this.textBoxHeaderHeight = new System.Windows.Forms.TextBox();
            this.textBoxHeaderImageXLocation = new System.Windows.Forms.TextBox();
            this.textBoxSubtitleYSpacing = new System.Windows.Forms.TextBox();
            this.textBoxTitleYLocation = new System.Windows.Forms.TextBox();
            this.ddlHeaderTextColor = new System.Windows.Forms.ComboBox();
            this.textBoxSubtitleFontSize = new System.Windows.Forms.TextBox();
            this.textBoxHeaderTextFontSize = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.cbDrawHeaderLine = new System.Windows.Forms.CheckBox();
            this.label32 = new System.Windows.Forms.Label();
            this.pnlDocumentOptions = new System.Windows.Forms.Panel();
            this.groupBoxPDFDocumentOptions = new System.Windows.Forms.GroupBox();
            this.ddlPdfSubset = new System.Windows.Forms.ComboBox();
            this.label71 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.textBoxCustomPdfPageHeight = new System.Windows.Forms.TextBox();
            this.textBoxCustomPdfPageWidth = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxRightMargin = new System.Windows.Forms.TextBox();
            this.textBoxTopMargin = new System.Windows.Forms.TextBox();
            this.textBoxBottomMargin = new System.Windows.Forms.TextBox();
            this.textBoxLeftMargin = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbShowHeader = new System.Windows.Forms.CheckBox();
            this.cbShowFooter = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cbAvoidTextBreak = new System.Windows.Forms.CheckBox();
            this.cbAvoidImageBreak = new System.Windows.Forms.CheckBox();
            this.cbGenerateSelectableText = new System.Windows.Forms.CheckBox();
            this.ddlCompression = new System.Windows.Forms.ComboBox();
            this.ddlPageOrientation = new System.Windows.Forms.ComboBox();
            this.ddlPageFormat = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pnlImgeOptions = new System.Windows.Forms.Panel();
            this.ddlImageFormat = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlCommonSettings = new System.Windows.Forms.Panel();
            this.pnlWebPageLoaded = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbMediaPrint = new System.Windows.Forms.CheckBox();
            this.textBoxConversionDelay = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.groupBoxCommonSettings = new System.Windows.Forms.GroupBox();
            this.pnlWebPageSize = new System.Windows.Forms.Panel();
            this.textBoxWebPageHeight = new System.Windows.Forms.TextBox();
            this.textBoxWebPageWidth = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPageWidth = new System.Windows.Forms.Label();
            this.pnlWebPageSizeControl = new System.Windows.Forms.Panel();
            this.lblWebPageSize = new System.Windows.Forms.Label();
            this.radioButtonCustomSize = new System.Windows.Forms.RadioButton();
            this.radioButtonAutodetect = new System.Windows.Forms.RadioButton();
            this.pnlControlPanel = new System.Windows.Forms.Panel();
            this.cbEmbedFonts = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbAutoSizePdfPage = new System.Windows.Forms.CheckBox();
            this.cbJpegCompression = new System.Windows.Forms.CheckBox();
            this.cbFitWidth = new System.Windows.Forms.CheckBox();
            this.cbBookmarks = new System.Windows.Forms.CheckBox();
            this.cbActiveXEnabled = new System.Windows.Forms.CheckBox();
            this.cbScriptsEnabled = new System.Windows.Forms.CheckBox();
            this.cbLiveLinks = new System.Windows.Forms.CheckBox();
            this.lnkBtnSettings = new System.Windows.Forms.LinkLabel();
            this.radioConvertToImage = new System.Windows.Forms.RadioButton();
            this.radioButtonConvertToPDFSelectable = new System.Windows.Forms.RadioButton();
            this.textBoxWebPageURL = new System.Windows.Forms.TextBox();
            this.lblEnterURL = new System.Windows.Forms.Label();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlConvertCommand = new System.Windows.Forms.Panel();
            this.btnConvert = new System.Windows.Forms.Button();
            this.pnlHeader.SuspendLayout();
            this.pnlContent.SuspendLayout();
            this.pnlSettingsPanel.SuspendLayout();
            this.pnlPDFSettings.SuspendLayout();
            this.pnlFooterOptions.SuspendLayout();
            this.groupBoxPDFFooterOptions.SuspendLayout();
            this.pnlHeaderOptions.SuspendLayout();
            this.groupBoxPDFHeaderOptions.SuspendLayout();
            this.pnlDocumentOptions.SuspendLayout();
            this.groupBoxPDFDocumentOptions.SuspendLayout();
            this.pnlImgeOptions.SuspendLayout();
            this.pnlCommonSettings.SuspendLayout();
            this.pnlWebPageLoaded.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBoxCommonSettings.SuspendLayout();
            this.pnlWebPageSize.SuspendLayout();
            this.pnlWebPageSizeControl.SuspendLayout();
            this.pnlControlPanel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.pnlConvertCommand.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.Controls.Add(this.label27);
            this.pnlHeader.Controls.Add(this.label26);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(724, 96);
            this.pnlHeader.TabIndex = 0;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(186, 9);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(286, 17);
            this.label27.TabIndex = 1;
            this.label27.Text = "ExpertPDF HtmlToPdf Converter Demo";
            // 
            // label26
            // 
            this.label26.Location = new System.Drawing.Point(12, 39);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(708, 54);
            this.label26.TabIndex = 0;
            this.label26.Text = "To convert a web page to image or to PDF enter the web page full URL in the text " +
    "box below and press \'Convert\' button. To customize the converter press \'More Con" +
    "verter Settings\'. ";
            // 
            // pnlContent
            // 
            this.pnlContent.AutoScroll = true;
            this.pnlContent.AutoSize = true;
            this.pnlContent.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlContent.Controls.Add(this.pnlSettingsPanel);
            this.pnlContent.Controls.Add(this.pnlControlPanel);
            this.pnlContent.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlContent.Location = new System.Drawing.Point(0, 96);
            this.pnlContent.MaximumSize = new System.Drawing.Size(720, 560);
            this.pnlContent.Name = "pnlContent";
            this.pnlContent.Size = new System.Drawing.Size(720, 560);
            this.pnlContent.TabIndex = 1;
            // 
            // pnlSettingsPanel
            // 
            this.pnlSettingsPanel.AutoSize = true;
            this.pnlSettingsPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlSettingsPanel.Controls.Add(this.pnlPDFSettings);
            this.pnlSettingsPanel.Controls.Add(this.pnlImgeOptions);
            this.pnlSettingsPanel.Controls.Add(this.pnlCommonSettings);
            this.pnlSettingsPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSettingsPanel.Location = new System.Drawing.Point(0, 129);
            this.pnlSettingsPanel.Name = "pnlSettingsPanel";
            this.pnlSettingsPanel.Size = new System.Drawing.Size(703, 1240);
            this.pnlSettingsPanel.TabIndex = 1;
            this.pnlSettingsPanel.Visible = false;
            // 
            // pnlPDFSettings
            // 
            this.pnlPDFSettings.AutoSize = true;
            this.pnlPDFSettings.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlPDFSettings.Controls.Add(this.pnlFooterOptions);
            this.pnlPDFSettings.Controls.Add(this.pnlHeaderOptions);
            this.pnlPDFSettings.Controls.Add(this.pnlDocumentOptions);
            this.pnlPDFSettings.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlPDFSettings.Location = new System.Drawing.Point(0, 248);
            this.pnlPDFSettings.Name = "pnlPDFSettings";
            this.pnlPDFSettings.Size = new System.Drawing.Size(703, 992);
            this.pnlPDFSettings.TabIndex = 2;
            // 
            // pnlFooterOptions
            // 
            this.pnlFooterOptions.Controls.Add(this.groupBoxPDFFooterOptions);
            this.pnlFooterOptions.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFooterOptions.Location = new System.Drawing.Point(0, 672);
            this.pnlFooterOptions.Name = "pnlFooterOptions";
            this.pnlFooterOptions.Size = new System.Drawing.Size(703, 320);
            this.pnlFooterOptions.TabIndex = 2;
            this.pnlFooterOptions.Visible = false;
            // 
            // groupBoxPDFFooterOptions
            // 
            this.groupBoxPDFFooterOptions.Controls.Add(this.label62);
            this.groupBoxPDFFooterOptions.Controls.Add(this.cbShowFooterOnFirstPage);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label63);
            this.groupBoxPDFFooterOptions.Controls.Add(this.textBoxPageNumberYLocation);
            this.groupBoxPDFFooterOptions.Controls.Add(this.cbShowFooterOnEvenPages);
            this.groupBoxPDFFooterOptions.Controls.Add(this.cbShowFooterOnOddPages);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label64);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label70);
            this.groupBoxPDFFooterOptions.Controls.Add(this.textBoxFooterTextYLocation);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label65);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label61);
            this.groupBoxPDFFooterOptions.Controls.Add(this.textBoxFooterWidth);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label25);
            this.groupBoxPDFFooterOptions.Controls.Add(this.textBoxPageNumberText);
            this.groupBoxPDFFooterOptions.Controls.Add(this.ddlFooterBackColor);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label19);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label24);
            this.groupBoxPDFFooterOptions.Controls.Add(this.cbShowPageNumber);
            this.groupBoxPDFFooterOptions.Controls.Add(this.checkBox1);
            this.groupBoxPDFFooterOptions.Controls.Add(this.cbDrawFooterLine);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label58);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label23);
            this.groupBoxPDFFooterOptions.Controls.Add(this.textBoxFooterText);
            this.groupBoxPDFFooterOptions.Controls.Add(this.ddlPageNumberTextColor);
            this.groupBoxPDFFooterOptions.Controls.Add(this.ddlFooterColor);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label51);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label49);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label50);
            this.groupBoxPDFFooterOptions.Controls.Add(this.textBoxFooterHeight);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label57);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label56);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label53);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label55);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label54);
            this.groupBoxPDFFooterOptions.Controls.Add(this.textBoxPageNumberFontSize);
            this.groupBoxPDFFooterOptions.Controls.Add(this.label52);
            this.groupBoxPDFFooterOptions.Controls.Add(this.ddlPageNumberTextFont);
            this.groupBoxPDFFooterOptions.Controls.Add(this.textBoxFooterTextFontSize);
            this.groupBoxPDFFooterOptions.Controls.Add(this.ddlFooterTextFont);
            this.groupBoxPDFFooterOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxPDFFooterOptions.Location = new System.Drawing.Point(0, 0);
            this.groupBoxPDFFooterOptions.Name = "groupBoxPDFFooterOptions";
            this.groupBoxPDFFooterOptions.Size = new System.Drawing.Size(703, 320);
            this.groupBoxPDFFooterOptions.TabIndex = 0;
            this.groupBoxPDFFooterOptions.TabStop = false;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(286, 248);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(126, 13);
            this.label62.TabIndex = 72;
            this.label62.Text = "Page Number Y Location";
            // 
            // cbShowFooterOnFirstPage
            // 
            this.cbShowFooterOnFirstPage.AutoSize = true;
            this.cbShowFooterOnFirstPage.Checked = true;
            this.cbShowFooterOnFirstPage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowFooterOnFirstPage.Location = new System.Drawing.Point(174, 283);
            this.cbShowFooterOnFirstPage.Name = "cbShowFooterOnFirstPage";
            this.cbShowFooterOnFirstPage.Size = new System.Drawing.Size(118, 17);
            this.cbShowFooterOnFirstPage.TabIndex = 67;
            this.cbShowFooterOnFirstPage.Text = "Show on First Page";
            this.cbShowFooterOnFirstPage.UseVisualStyleBackColor = true;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(532, 247);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(35, 13);
            this.label63.TabIndex = 70;
            this.label63.Text = "points";
            // 
            // textBoxPageNumberYLocation
            // 
            this.textBoxPageNumberYLocation.Location = new System.Drawing.Point(422, 245);
            this.textBoxPageNumberYLocation.Name = "textBoxPageNumberYLocation";
            this.textBoxPageNumberYLocation.Size = new System.Drawing.Size(104, 20);
            this.textBoxPageNumberYLocation.TabIndex = 71;
            this.textBoxPageNumberYLocation.Text = "15";
            // 
            // cbShowFooterOnEvenPages
            // 
            this.cbShowFooterOnEvenPages.AutoSize = true;
            this.cbShowFooterOnEvenPages.Checked = true;
            this.cbShowFooterOnEvenPages.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowFooterOnEvenPages.Location = new System.Drawing.Point(455, 283);
            this.cbShowFooterOnEvenPages.Name = "cbShowFooterOnEvenPages";
            this.cbShowFooterOnEvenPages.Size = new System.Drawing.Size(129, 17);
            this.cbShowFooterOnEvenPages.TabIndex = 69;
            this.cbShowFooterOnEvenPages.Text = "Show on Even Pages";
            this.cbShowFooterOnEvenPages.UseVisualStyleBackColor = true;
            // 
            // cbShowFooterOnOddPages
            // 
            this.cbShowFooterOnOddPages.AutoSize = true;
            this.cbShowFooterOnOddPages.Checked = true;
            this.cbShowFooterOnOddPages.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowFooterOnOddPages.Location = new System.Drawing.Point(318, 283);
            this.cbShowFooterOnOddPages.Name = "cbShowFooterOnOddPages";
            this.cbShowFooterOnOddPages.Size = new System.Drawing.Size(124, 17);
            this.cbShowFooterOnOddPages.TabIndex = 68;
            this.cbShowFooterOnOddPages.Text = "Show on Odd Pages";
            this.cbShowFooterOnOddPages.UseVisualStyleBackColor = true;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(286, 157);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(115, 13);
            this.label64.TabIndex = 67;
            this.label64.Text = "Footer Text Y Location";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(518, 154);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(35, 13);
            this.label70.TabIndex = 65;
            this.label70.Text = "points";
            // 
            // textBoxFooterTextYLocation
            // 
            this.textBoxFooterTextYLocation.Location = new System.Drawing.Point(408, 154);
            this.textBoxFooterTextYLocation.Name = "textBoxFooterTextYLocation";
            this.textBoxFooterTextYLocation.Size = new System.Drawing.Size(102, 20);
            this.textBoxFooterTextYLocation.TabIndex = 66;
            this.textBoxFooterTextYLocation.Text = "8";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(286, 132);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(92, 13);
            this.label65.TabIndex = 64;
            this.label65.Text = "Footer Text Width";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(518, 132);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(35, 13);
            this.label61.TabIndex = 62;
            this.label61.Text = "points";
            // 
            // textBoxFooterWidth
            // 
            this.textBoxFooterWidth.Location = new System.Drawing.Point(408, 129);
            this.textBoxFooterWidth.Name = "textBoxFooterWidth";
            this.textBoxFooterWidth.Size = new System.Drawing.Size(102, 20);
            this.textBoxFooterWidth.TabIndex = 63;
            this.textBoxFooterWidth.Text = "-1";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(32, 195);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(99, 13);
            this.label25.TabIndex = 12;
            this.label25.Text = "Page Number Text:";
            // 
            // textBoxPageNumberText
            // 
            this.textBoxPageNumberText.Location = new System.Drawing.Point(157, 192);
            this.textBoxPageNumberText.Name = "textBoxPageNumberText";
            this.textBoxPageNumberText.Size = new System.Drawing.Size(215, 20);
            this.textBoxPageNumberText.TabIndex = 11;
            this.textBoxPageNumberText.Text = "Page ";
            // 
            // ddlFooterBackColor
            // 
            this.ddlFooterBackColor.FormattingEnabled = true;
            this.ddlFooterBackColor.Location = new System.Drawing.Point(139, 72);
            this.ddlFooterBackColor.Name = "ddlFooterBackColor";
            this.ddlFooterBackColor.Size = new System.Drawing.Size(121, 21);
            this.ddlFooterBackColor.TabIndex = 27;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(12, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(122, 13);
            this.label19.TabIndex = 7;
            this.label19.Text = "PDF Footer Options:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(31, 45);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(64, 13);
            this.label24.TabIndex = 10;
            this.label24.Text = "Footer Text:";
            // 
            // cbShowPageNumber
            // 
            this.cbShowPageNumber.AutoSize = true;
            this.cbShowPageNumber.Checked = true;
            this.cbShowPageNumber.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowPageNumber.Location = new System.Drawing.Point(34, 165);
            this.cbShowPageNumber.Name = "cbShowPageNumber";
            this.cbShowPageNumber.Size = new System.Drawing.Size(121, 17);
            this.cbShowPageNumber.TabIndex = 4;
            this.cbShowPageNumber.Text = "Show Page Number";
            this.cbShowPageNumber.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(35, 386);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(107, 17);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = "Draw Footer Line";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // cbDrawFooterLine
            // 
            this.cbDrawFooterLine.AutoSize = true;
            this.cbDrawFooterLine.Checked = true;
            this.cbDrawFooterLine.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDrawFooterLine.Location = new System.Drawing.Point(34, 283);
            this.cbDrawFooterLine.Name = "cbDrawFooterLine";
            this.cbDrawFooterLine.Size = new System.Drawing.Size(107, 17);
            this.cbDrawFooterLine.TabIndex = 5;
            this.cbDrawFooterLine.Text = "Draw Footer Line";
            this.cbDrawFooterLine.UseVisualStyleBackColor = true;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(31, 248);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(126, 13);
            this.label58.TabIndex = 8;
            this.label58.Text = "Page Number Text Color:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(31, 131);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(91, 13);
            this.label23.TabIndex = 8;
            this.label23.Text = "Footer Text Color:";
            // 
            // textBoxFooterText
            // 
            this.textBoxFooterText.Location = new System.Drawing.Point(139, 42);
            this.textBoxFooterText.Name = "textBoxFooterText";
            this.textBoxFooterText.Size = new System.Drawing.Size(215, 20);
            this.textBoxFooterText.TabIndex = 9;
            this.textBoxFooterText.Text = "Footer";
            // 
            // ddlPageNumberTextColor
            // 
            this.ddlPageNumberTextColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPageNumberTextColor.FormattingEnabled = true;
            this.ddlPageNumberTextColor.Location = new System.Drawing.Point(157, 245);
            this.ddlPageNumberTextColor.Name = "ddlPageNumberTextColor";
            this.ddlPageNumberTextColor.Size = new System.Drawing.Size(121, 21);
            this.ddlPageNumberTextColor.TabIndex = 6;
            // 
            // ddlFooterColor
            // 
            this.ddlFooterColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlFooterColor.FormattingEnabled = true;
            this.ddlFooterColor.Location = new System.Drawing.Point(139, 128);
            this.ddlFooterColor.Name = "ddlFooterColor";
            this.ddlFooterColor.Size = new System.Drawing.Size(121, 21);
            this.ddlFooterColor.TabIndex = 6;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(286, 74);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(71, 13);
            this.label51.TabIndex = 22;
            this.label51.Text = "Footer Height";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(516, 74);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(35, 13);
            this.label49.TabIndex = 7;
            this.label49.Text = "points";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(31, 74);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(95, 13);
            this.label50.TabIndex = 5;
            this.label50.Text = "Footer Back Color:";
            // 
            // textBoxFooterHeight
            // 
            this.textBoxFooterHeight.Location = new System.Drawing.Point(406, 72);
            this.textBoxFooterHeight.Name = "textBoxFooterHeight";
            this.textBoxFooterHeight.Size = new System.Drawing.Size(104, 20);
            this.textBoxFooterHeight.TabIndex = 18;
            this.textBoxFooterHeight.Text = "40";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(31, 220);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(123, 13);
            this.label57.TabIndex = 5;
            this.label57.Text = "Page Number Text Font:";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(285, 220);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(119, 13);
            this.label56.TabIndex = 22;
            this.label56.Text = "Page Number Font Size";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(31, 103);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(88, 13);
            this.label53.TabIndex = 5;
            this.label53.Text = "Footer Text Font:";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(532, 220);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(35, 13);
            this.label55.TabIndex = 7;
            this.label55.Text = "points";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(286, 103);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(108, 13);
            this.label54.TabIndex = 22;
            this.label54.Text = "Footer Text Font Size";
            // 
            // textBoxPageNumberFontSize
            // 
            this.textBoxPageNumberFontSize.Location = new System.Drawing.Point(422, 218);
            this.textBoxPageNumberFontSize.Name = "textBoxPageNumberFontSize";
            this.textBoxPageNumberFontSize.Size = new System.Drawing.Size(104, 20);
            this.textBoxPageNumberFontSize.TabIndex = 18;
            this.textBoxPageNumberFontSize.Text = "10";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(517, 103);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(35, 13);
            this.label52.TabIndex = 7;
            this.label52.Text = "points";
            // 
            // ddlPageNumberTextFont
            // 
            this.ddlPageNumberTextFont.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPageNumberTextFont.FormattingEnabled = true;
            this.ddlPageNumberTextFont.Location = new System.Drawing.Point(157, 217);
            this.ddlPageNumberTextFont.Name = "ddlPageNumberTextFont";
            this.ddlPageNumberTextFont.Size = new System.Drawing.Size(121, 21);
            this.ddlPageNumberTextFont.TabIndex = 2;
            // 
            // textBoxFooterTextFontSize
            // 
            this.textBoxFooterTextFontSize.Location = new System.Drawing.Point(407, 101);
            this.textBoxFooterTextFontSize.Name = "textBoxFooterTextFontSize";
            this.textBoxFooterTextFontSize.Size = new System.Drawing.Size(104, 20);
            this.textBoxFooterTextFontSize.TabIndex = 18;
            this.textBoxFooterTextFontSize.Text = "8";
            // 
            // ddlFooterTextFont
            // 
            this.ddlFooterTextFont.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlFooterTextFont.FormattingEnabled = true;
            this.ddlFooterTextFont.Location = new System.Drawing.Point(140, 100);
            this.ddlFooterTextFont.Name = "ddlFooterTextFont";
            this.ddlFooterTextFont.Size = new System.Drawing.Size(121, 21);
            this.ddlFooterTextFont.TabIndex = 2;
            // 
            // pnlHeaderOptions
            // 
            this.pnlHeaderOptions.Controls.Add(this.groupBoxPDFHeaderOptions);
            this.pnlHeaderOptions.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeaderOptions.Location = new System.Drawing.Point(0, 313);
            this.pnlHeaderOptions.Name = "pnlHeaderOptions";
            this.pnlHeaderOptions.Size = new System.Drawing.Size(703, 359);
            this.pnlHeaderOptions.TabIndex = 1;
            this.pnlHeaderOptions.Visible = false;
            // 
            // groupBoxPDFHeaderOptions
            // 
            this.groupBoxPDFHeaderOptions.Controls.Add(this.cbShowHeaderOnEvenPages);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.cbShowHeaderOnFirstPage);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.cbShowHeaderOnOddPages);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.ddlTitleAlign);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label60);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.ddlSubtitleFont);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.ddlSubtitleColor);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.ddlHeaderBackColor);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.btnChangeHeaderImage);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.lblLicensesDirectory);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.textBoxHeaderImagePath);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label18);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label20);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.textBoxHeadeSubtitle);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label36);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label44);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label48);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label46);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label38);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label43);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label33);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label6);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.textBoxHeaderTitle);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.ddlHeaderFont);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.textBoxHeaderImageYLocation);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.textBoxHeaderHeight);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.textBoxHeaderImageXLocation);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.textBoxSubtitleYSpacing);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.textBoxTitleYLocation);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.ddlHeaderTextColor);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.textBoxSubtitleFontSize);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.textBoxHeaderTextFontSize);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label34);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label42);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label31);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label41);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label22);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label47);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label40);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label45);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label35);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label37);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label39);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.cbDrawHeaderLine);
            this.groupBoxPDFHeaderOptions.Controls.Add(this.label32);
            this.groupBoxPDFHeaderOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxPDFHeaderOptions.Location = new System.Drawing.Point(0, 0);
            this.groupBoxPDFHeaderOptions.Name = "groupBoxPDFHeaderOptions";
            this.groupBoxPDFHeaderOptions.Size = new System.Drawing.Size(703, 359);
            this.groupBoxPDFHeaderOptions.TabIndex = 0;
            this.groupBoxPDFHeaderOptions.TabStop = false;
            // 
            // cbShowHeaderOnEvenPages
            // 
            this.cbShowHeaderOnEvenPages.AutoSize = true;
            this.cbShowHeaderOnEvenPages.Checked = true;
            this.cbShowHeaderOnEvenPages.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowHeaderOnEvenPages.Location = new System.Drawing.Point(467, 326);
            this.cbShowHeaderOnEvenPages.Name = "cbShowHeaderOnEvenPages";
            this.cbShowHeaderOnEvenPages.Size = new System.Drawing.Size(129, 17);
            this.cbShowHeaderOnEvenPages.TabIndex = 68;
            this.cbShowHeaderOnEvenPages.Text = "Show on Even Pages";
            this.cbShowHeaderOnEvenPages.UseVisualStyleBackColor = true;
            // 
            // cbShowHeaderOnFirstPage
            // 
            this.cbShowHeaderOnFirstPage.AutoSize = true;
            this.cbShowHeaderOnFirstPage.Checked = true;
            this.cbShowHeaderOnFirstPage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowHeaderOnFirstPage.Location = new System.Drawing.Point(174, 326);
            this.cbShowHeaderOnFirstPage.Name = "cbShowHeaderOnFirstPage";
            this.cbShowHeaderOnFirstPage.Size = new System.Drawing.Size(118, 17);
            this.cbShowHeaderOnFirstPage.TabIndex = 67;
            this.cbShowHeaderOnFirstPage.Text = "Show on First Page";
            this.cbShowHeaderOnFirstPage.UseVisualStyleBackColor = true;
            // 
            // cbShowHeaderOnOddPages
            // 
            this.cbShowHeaderOnOddPages.AutoSize = true;
            this.cbShowHeaderOnOddPages.Checked = true;
            this.cbShowHeaderOnOddPages.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowHeaderOnOddPages.Location = new System.Drawing.Point(318, 326);
            this.cbShowHeaderOnOddPages.Name = "cbShowHeaderOnOddPages";
            this.cbShowHeaderOnOddPages.Size = new System.Drawing.Size(124, 17);
            this.cbShowHeaderOnOddPages.TabIndex = 67;
            this.cbShowHeaderOnOddPages.Text = "Show on Odd Pages";
            this.cbShowHeaderOnOddPages.UseVisualStyleBackColor = true;
            // 
            // ddlTitleAlign
            // 
            this.ddlTitleAlign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlTitleAlign.FormattingEnabled = true;
            this.ddlTitleAlign.Location = new System.Drawing.Point(431, 42);
            this.ddlTitleAlign.Name = "ddlTitleAlign";
            this.ddlTitleAlign.Size = new System.Drawing.Size(121, 21);
            this.ddlTitleAlign.TabIndex = 65;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(372, 46);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(56, 13);
            this.label60.TabIndex = 66;
            this.label60.Text = "Title Align:";
            // 
            // ddlSubtitleFont
            // 
            this.ddlSubtitleFont.FormattingEnabled = true;
            this.ddlSubtitleFont.Location = new System.Drawing.Point(139, 178);
            this.ddlSubtitleFont.Name = "ddlSubtitleFont";
            this.ddlSubtitleFont.Size = new System.Drawing.Size(121, 21);
            this.ddlSubtitleFont.TabIndex = 29;
            // 
            // ddlSubtitleColor
            // 
            this.ddlSubtitleColor.FormattingEnabled = true;
            this.ddlSubtitleColor.Location = new System.Drawing.Point(139, 203);
            this.ddlSubtitleColor.Name = "ddlSubtitleColor";
            this.ddlSubtitleColor.Size = new System.Drawing.Size(121, 21);
            this.ddlSubtitleColor.TabIndex = 28;
            // 
            // ddlHeaderBackColor
            // 
            this.ddlHeaderBackColor.FormattingEnabled = true;
            this.ddlHeaderBackColor.Location = new System.Drawing.Point(139, 96);
            this.ddlHeaderBackColor.Name = "ddlHeaderBackColor";
            this.ddlHeaderBackColor.Size = new System.Drawing.Size(121, 21);
            this.ddlHeaderBackColor.TabIndex = 27;
            // 
            // btnChangeHeaderImage
            // 
            this.btnChangeHeaderImage.Location = new System.Drawing.Point(476, 240);
            this.btnChangeHeaderImage.Name = "btnChangeHeaderImage";
            this.btnChangeHeaderImage.Size = new System.Drawing.Size(75, 23);
            this.btnChangeHeaderImage.TabIndex = 26;
            this.btnChangeHeaderImage.Text = "Change";
            this.btnChangeHeaderImage.UseVisualStyleBackColor = true;
            this.btnChangeHeaderImage.Click += new System.EventHandler(this.btnChangeHeaderImage_Click);
            // 
            // lblLicensesDirectory
            // 
            this.lblLicensesDirectory.AutoSize = true;
            this.lblLicensesDirectory.Location = new System.Drawing.Point(31, 245);
            this.lblLicensesDirectory.Name = "lblLicensesDirectory";
            this.lblLicensesDirectory.Size = new System.Drawing.Size(100, 13);
            this.lblLicensesDirectory.TabIndex = 24;
            this.lblLicensesDirectory.Text = "Header image path:";
            // 
            // textBoxHeaderImagePath
            // 
            this.textBoxHeaderImagePath.Location = new System.Drawing.Point(136, 242);
            this.textBoxHeaderImagePath.Name = "textBoxHeaderImagePath";
            this.textBoxHeaderImagePath.Size = new System.Drawing.Size(334, 20);
            this.textBoxHeaderImagePath.TabIndex = 25;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(12, 16);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(127, 13);
            this.label18.TabIndex = 4;
            this.label18.Text = "PDF Header Options:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(31, 73);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(83, 13);
            this.label20.TabIndex = 7;
            this.label20.Text = "Header Subtitle:";
            // 
            // textBoxHeadeSubtitle
            // 
            this.textBoxHeadeSubtitle.Location = new System.Drawing.Point(139, 68);
            this.textBoxHeadeSubtitle.Name = "textBoxHeadeSubtitle";
            this.textBoxHeadeSubtitle.Size = new System.Drawing.Size(215, 20);
            this.textBoxHeadeSubtitle.TabIndex = 6;
            this.textBoxHeadeSubtitle.Text = "Subtitle";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(263, 98);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(76, 13);
            this.label36.TabIndex = 22;
            this.label36.Text = "Header Height";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(263, 207);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(132, 13);
            this.label44.TabIndex = 22;
            this.label44.Text = "Header Subtitle Y Spacing";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(31, 299);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(128, 13);
            this.label48.TabIndex = 22;
            this.label48.Text = "Header Image Y Location";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(31, 274);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(128, 13);
            this.label46.TabIndex = 22;
            this.label46.Text = "Header Image X Location";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(263, 153);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(119, 13);
            this.label38.TabIndex = 22;
            this.label38.Text = "Header Title Y Location";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(263, 181);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(127, 13);
            this.label43.TabIndex = 22;
            this.label43.Text = "Header Subtitle Font Size";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(263, 126);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(112, 13);
            this.label33.TabIndex = 22;
            this.label33.Text = "Header Title Font Size";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Header Title:";
            // 
            // textBoxHeaderTitle
            // 
            this.textBoxHeaderTitle.Location = new System.Drawing.Point(139, 42);
            this.textBoxHeaderTitle.Name = "textBoxHeaderTitle";
            this.textBoxHeaderTitle.Size = new System.Drawing.Size(215, 20);
            this.textBoxHeaderTitle.TabIndex = 9;
            this.textBoxHeaderTitle.Text = "Title";
            // 
            // ddlHeaderFont
            // 
            this.ddlHeaderFont.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlHeaderFont.FormattingEnabled = true;
            this.ddlHeaderFont.Location = new System.Drawing.Point(139, 123);
            this.ddlHeaderFont.Name = "ddlHeaderFont";
            this.ddlHeaderFont.Size = new System.Drawing.Size(121, 21);
            this.ddlHeaderFont.TabIndex = 2;
            // 
            // textBoxHeaderImageYLocation
            // 
            this.textBoxHeaderImageYLocation.Location = new System.Drawing.Point(174, 296);
            this.textBoxHeaderImageYLocation.Name = "textBoxHeaderImageYLocation";
            this.textBoxHeaderImageYLocation.Size = new System.Drawing.Size(104, 20);
            this.textBoxHeaderImageYLocation.TabIndex = 18;
            this.textBoxHeaderImageYLocation.Text = "0";
            // 
            // textBoxHeaderHeight
            // 
            this.textBoxHeaderHeight.Location = new System.Drawing.Point(406, 96);
            this.textBoxHeaderHeight.Name = "textBoxHeaderHeight";
            this.textBoxHeaderHeight.Size = new System.Drawing.Size(104, 20);
            this.textBoxHeaderHeight.TabIndex = 18;
            this.textBoxHeaderHeight.Text = "50";
            // 
            // textBoxHeaderImageXLocation
            // 
            this.textBoxHeaderImageXLocation.Location = new System.Drawing.Point(174, 271);
            this.textBoxHeaderImageXLocation.Name = "textBoxHeaderImageXLocation";
            this.textBoxHeaderImageXLocation.Size = new System.Drawing.Size(104, 20);
            this.textBoxHeaderImageXLocation.TabIndex = 18;
            this.textBoxHeaderImageXLocation.Text = "0";
            // 
            // textBoxSubtitleYSpacing
            // 
            this.textBoxSubtitleYSpacing.Location = new System.Drawing.Point(406, 204);
            this.textBoxSubtitleYSpacing.Name = "textBoxSubtitleYSpacing";
            this.textBoxSubtitleYSpacing.Size = new System.Drawing.Size(104, 20);
            this.textBoxSubtitleYSpacing.TabIndex = 18;
            this.textBoxSubtitleYSpacing.Text = "25";
            // 
            // textBoxTitleYLocation
            // 
            this.textBoxTitleYLocation.Location = new System.Drawing.Point(406, 150);
            this.textBoxTitleYLocation.Name = "textBoxTitleYLocation";
            this.textBoxTitleYLocation.Size = new System.Drawing.Size(104, 20);
            this.textBoxTitleYLocation.TabIndex = 18;
            this.textBoxTitleYLocation.Text = "5";
            // 
            // ddlHeaderTextColor
            // 
            this.ddlHeaderTextColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlHeaderTextColor.FormattingEnabled = true;
            this.ddlHeaderTextColor.Location = new System.Drawing.Point(139, 150);
            this.ddlHeaderTextColor.Name = "ddlHeaderTextColor";
            this.ddlHeaderTextColor.Size = new System.Drawing.Size(121, 21);
            this.ddlHeaderTextColor.TabIndex = 2;
            // 
            // textBoxSubtitleFontSize
            // 
            this.textBoxSubtitleFontSize.Location = new System.Drawing.Point(406, 178);
            this.textBoxSubtitleFontSize.Name = "textBoxSubtitleFontSize";
            this.textBoxSubtitleFontSize.Size = new System.Drawing.Size(104, 20);
            this.textBoxSubtitleFontSize.TabIndex = 18;
            this.textBoxSubtitleFontSize.Text = "12";
            // 
            // textBoxHeaderTextFontSize
            // 
            this.textBoxHeaderTextFontSize.Location = new System.Drawing.Point(406, 124);
            this.textBoxHeaderTextFontSize.Name = "textBoxHeaderTextFontSize";
            this.textBoxHeaderTextFontSize.Size = new System.Drawing.Size(104, 20);
            this.textBoxHeaderTextFontSize.TabIndex = 18;
            this.textBoxHeaderTextFontSize.Text = "18";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(31, 98);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(100, 13);
            this.label34.TabIndex = 5;
            this.label34.Text = "Header Back Color:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(31, 181);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(107, 13);
            this.label42.TabIndex = 5;
            this.label42.Text = "Header Subtitle Font:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(31, 126);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(92, 13);
            this.label31.TabIndex = 5;
            this.label31.Text = "Header Title Font:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(31, 208);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(110, 13);
            this.label41.TabIndex = 5;
            this.label41.Text = "Header Subtitle Color:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(32, 153);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(95, 13);
            this.label22.TabIndex = 5;
            this.label22.Text = "Header Title Color:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(284, 299);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(35, 13);
            this.label47.TabIndex = 7;
            this.label47.Text = "points";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(516, 207);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(35, 13);
            this.label40.TabIndex = 7;
            this.label40.Text = "points";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(284, 274);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(35, 13);
            this.label45.TabIndex = 7;
            this.label45.Text = "points";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(516, 98);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(35, 13);
            this.label35.TabIndex = 7;
            this.label35.Text = "points";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(516, 153);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(35, 13);
            this.label37.TabIndex = 7;
            this.label37.Text = "points";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(516, 181);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(35, 13);
            this.label39.TabIndex = 7;
            this.label39.Text = "points";
            // 
            // cbDrawHeaderLine
            // 
            this.cbDrawHeaderLine.AutoSize = true;
            this.cbDrawHeaderLine.Checked = true;
            this.cbDrawHeaderLine.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDrawHeaderLine.Location = new System.Drawing.Point(34, 326);
            this.cbDrawHeaderLine.Name = "cbDrawHeaderLine";
            this.cbDrawHeaderLine.Size = new System.Drawing.Size(112, 17);
            this.cbDrawHeaderLine.TabIndex = 3;
            this.cbDrawHeaderLine.Text = "Draw Header Line";
            this.cbDrawHeaderLine.UseVisualStyleBackColor = true;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(516, 126);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(35, 13);
            this.label32.TabIndex = 7;
            this.label32.Text = "points";
            // 
            // pnlDocumentOptions
            // 
            this.pnlDocumentOptions.Controls.Add(this.groupBoxPDFDocumentOptions);
            this.pnlDocumentOptions.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDocumentOptions.Location = new System.Drawing.Point(0, 0);
            this.pnlDocumentOptions.Name = "pnlDocumentOptions";
            this.pnlDocumentOptions.Size = new System.Drawing.Size(703, 313);
            this.pnlDocumentOptions.TabIndex = 0;
            // 
            // groupBoxPDFDocumentOptions
            // 
            this.groupBoxPDFDocumentOptions.Controls.Add(this.ddlPdfSubset);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label71);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label69);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label66);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.textBoxCustomPdfPageHeight);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.textBoxCustomPdfPageWidth);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label68);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label67);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label13);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label17);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label15);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label10);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.textBoxRightMargin);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.textBoxTopMargin);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.textBoxBottomMargin);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.textBoxLeftMargin);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label16);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.cbShowHeader);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.cbShowFooter);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label14);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label12);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label11);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.cbAvoidTextBreak);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.cbAvoidImageBreak);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.cbGenerateSelectableText);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.ddlCompression);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.ddlPageOrientation);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.ddlPageFormat);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label9);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label28);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label8);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label21);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label7);
            this.groupBoxPDFDocumentOptions.Controls.Add(this.label5);
            this.groupBoxPDFDocumentOptions.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxPDFDocumentOptions.Location = new System.Drawing.Point(0, 0);
            this.groupBoxPDFDocumentOptions.Name = "groupBoxPDFDocumentOptions";
            this.groupBoxPDFDocumentOptions.Size = new System.Drawing.Size(703, 307);
            this.groupBoxPDFDocumentOptions.TabIndex = 0;
            this.groupBoxPDFDocumentOptions.TabStop = false;
            // 
            // ddlPdfSubset
            // 
            this.ddlPdfSubset.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPdfSubset.FormattingEnabled = true;
            this.ddlPdfSubset.Location = new System.Drawing.Point(136, 98);
            this.ddlPdfSubset.Name = "ddlPdfSubset";
            this.ddlPdfSubset.Size = new System.Drawing.Size(121, 21);
            this.ddlPdfSubset.TabIndex = 37;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(31, 101);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(77, 13);
            this.label71.TabIndex = 38;
            this.label71.Text = "PDF Standard:";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(419, 47);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(79, 13);
            this.label69.TabIndex = 35;
            this.label69.Text = "Custom Height:";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(285, 47);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(76, 13);
            this.label66.TabIndex = 36;
            this.label66.Text = "Custom Width:";
            // 
            // textBoxCustomPdfPageHeight
            // 
            this.textBoxCustomPdfPageHeight.Location = new System.Drawing.Point(501, 44);
            this.textBoxCustomPdfPageHeight.Name = "textBoxCustomPdfPageHeight";
            this.textBoxCustomPdfPageHeight.Size = new System.Drawing.Size(32, 20);
            this.textBoxCustomPdfPageHeight.TabIndex = 34;
            this.textBoxCustomPdfPageHeight.Text = "842";
            // 
            // textBoxCustomPdfPageWidth
            // 
            this.textBoxCustomPdfPageWidth.Location = new System.Drawing.Point(363, 44);
            this.textBoxCustomPdfPageWidth.Name = "textBoxCustomPdfPageWidth";
            this.textBoxCustomPdfPageWidth.Size = new System.Drawing.Size(32, 20);
            this.textBoxCustomPdfPageWidth.TabIndex = 33;
            this.textBoxCustomPdfPageWidth.Text = "595";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(532, 47);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(16, 13);
            this.label68.TabIndex = 31;
            this.label68.Text = "pt";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(394, 47);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(16, 13);
            this.label67.TabIndex = 32;
            this.label67.Text = "pt";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(283, 220);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "Right:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(283, 249);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "Top:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(61, 246);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 13);
            this.label15.TabIndex = 23;
            this.label15.Text = "Bottom:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(61, 220);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Left:";
            // 
            // textBoxRightMargin
            // 
            this.textBoxRightMargin.Location = new System.Drawing.Point(323, 217);
            this.textBoxRightMargin.Name = "textBoxRightMargin";
            this.textBoxRightMargin.Size = new System.Drawing.Size(104, 20);
            this.textBoxRightMargin.TabIndex = 20;
            this.textBoxRightMargin.Text = "0";
            // 
            // textBoxTopMargin
            // 
            this.textBoxTopMargin.Location = new System.Drawing.Point(323, 246);
            this.textBoxTopMargin.Name = "textBoxTopMargin";
            this.textBoxTopMargin.Size = new System.Drawing.Size(104, 20);
            this.textBoxTopMargin.TabIndex = 19;
            this.textBoxTopMargin.Text = "0";
            // 
            // textBoxBottomMargin
            // 
            this.textBoxBottomMargin.Location = new System.Drawing.Point(109, 243);
            this.textBoxBottomMargin.Name = "textBoxBottomMargin";
            this.textBoxBottomMargin.Size = new System.Drawing.Size(104, 20);
            this.textBoxBottomMargin.TabIndex = 17;
            this.textBoxBottomMargin.Text = "0";
            // 
            // textBoxLeftMargin
            // 
            this.textBoxLeftMargin.Location = new System.Drawing.Point(109, 217);
            this.textBoxLeftMargin.Name = "textBoxLeftMargin";
            this.textBoxLeftMargin.Size = new System.Drawing.Size(104, 20);
            this.textBoxLeftMargin.TabIndex = 18;
            this.textBoxLeftMargin.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(433, 249);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(16, 13);
            this.label16.TabIndex = 12;
            this.label16.Text = "pt";
            // 
            // cbShowHeader
            // 
            this.cbShowHeader.AutoSize = true;
            this.cbShowHeader.Location = new System.Drawing.Point(64, 162);
            this.cbShowHeader.Name = "cbShowHeader";
            this.cbShowHeader.Size = new System.Drawing.Size(91, 17);
            this.cbShowHeader.TabIndex = 5;
            this.cbShowHeader.Text = "Show Header";
            this.cbShowHeader.UseVisualStyleBackColor = true;
            this.cbShowHeader.CheckedChanged += new System.EventHandler(this.cbShowHeader_CheckedChanged);
            // 
            // cbShowFooter
            // 
            this.cbShowFooter.AutoSize = true;
            this.cbShowFooter.Location = new System.Drawing.Point(189, 162);
            this.cbShowFooter.Name = "cbShowFooter";
            this.cbShowFooter.Size = new System.Drawing.Size(86, 17);
            this.cbShowFooter.TabIndex = 6;
            this.cbShowFooter.Text = "Show Footer";
            this.cbShowFooter.UseVisualStyleBackColor = true;
            this.cbShowFooter.CheckedChanged += new System.EventHandler(this.cbShowFooter_CheckedChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(219, 246);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(16, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "pt";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(433, 220);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(16, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "pt";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(219, 220);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "pt";
            // 
            // cbAvoidTextBreak
            // 
            this.cbAvoidTextBreak.AutoSize = true;
            this.cbAvoidTextBreak.Checked = true;
            this.cbAvoidTextBreak.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAvoidTextBreak.Location = new System.Drawing.Point(212, 280);
            this.cbAvoidTextBreak.Name = "cbAvoidTextBreak";
            this.cbAvoidTextBreak.Size = new System.Drawing.Size(108, 17);
            this.cbAvoidTextBreak.TabIndex = 10;
            this.cbAvoidTextBreak.Text = "Avoid Text Break";
            this.cbAvoidTextBreak.UseVisualStyleBackColor = true;
            // 
            // cbAvoidImageBreak
            // 
            this.cbAvoidImageBreak.AutoSize = true;
            this.cbAvoidImageBreak.Location = new System.Drawing.Point(326, 280);
            this.cbAvoidImageBreak.Name = "cbAvoidImageBreak";
            this.cbAvoidImageBreak.Size = new System.Drawing.Size(116, 17);
            this.cbAvoidImageBreak.TabIndex = 10;
            this.cbAvoidImageBreak.Text = "Avoid Image Break";
            this.cbAvoidImageBreak.UseVisualStyleBackColor = true;
            // 
            // cbGenerateSelectableText
            // 
            this.cbGenerateSelectableText.AutoSize = true;
            this.cbGenerateSelectableText.Checked = true;
            this.cbGenerateSelectableText.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbGenerateSelectableText.Location = new System.Drawing.Point(34, 280);
            this.cbGenerateSelectableText.Name = "cbGenerateSelectableText";
            this.cbGenerateSelectableText.Size = new System.Drawing.Size(141, 17);
            this.cbGenerateSelectableText.TabIndex = 10;
            this.cbGenerateSelectableText.Text = "Generate selectable text";
            this.cbGenerateSelectableText.UseVisualStyleBackColor = true;
            // 
            // ddlCompression
            // 
            this.ddlCompression.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCompression.FormattingEnabled = true;
            this.ddlCompression.Location = new System.Drawing.Point(136, 71);
            this.ddlCompression.Name = "ddlCompression";
            this.ddlCompression.Size = new System.Drawing.Size(121, 21);
            this.ddlCompression.TabIndex = 11;
            // 
            // ddlPageOrientation
            // 
            this.ddlPageOrientation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ddlPageOrientation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ddlPageOrientation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPageOrientation.FormattingEnabled = true;
            this.ddlPageOrientation.Location = new System.Drawing.Point(404, 71);
            this.ddlPageOrientation.Name = "ddlPageOrientation";
            this.ddlPageOrientation.Size = new System.Drawing.Size(121, 21);
            this.ddlPageOrientation.TabIndex = 8;
            // 
            // ddlPageFormat
            // 
            this.ddlPageFormat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ddlPageFormat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ddlPageFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPageFormat.FormattingEnabled = true;
            this.ddlPageFormat.Location = new System.Drawing.Point(136, 44);
            this.ddlPageFormat.Name = "ddlPageFormat";
            this.ddlPageFormat.Size = new System.Drawing.Size(121, 21);
            this.ddlPageFormat.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(31, 192);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Document Margins:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(285, 75);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(113, 13);
            this.label28.TabIndex = 13;
            this.label28.Text = "PDF Page Orientation:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 136);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Header and Footer:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(31, 47);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(82, 13);
            this.label21.TabIndex = 13;
            this.label21.Text = "PDF Page Size:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 75);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "PDF Compression:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(143, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "PDF Document Options:";
            // 
            // pnlImgeOptions
            // 
            this.pnlImgeOptions.Controls.Add(this.ddlImageFormat);
            this.pnlImgeOptions.Controls.Add(this.label4);
            this.pnlImgeOptions.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlImgeOptions.Location = new System.Drawing.Point(0, 206);
            this.pnlImgeOptions.Name = "pnlImgeOptions";
            this.pnlImgeOptions.Size = new System.Drawing.Size(703, 42);
            this.pnlImgeOptions.TabIndex = 1;
            this.pnlImgeOptions.Visible = false;
            // 
            // ddlImageFormat
            // 
            this.ddlImageFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlImageFormat.FormattingEnabled = true;
            this.ddlImageFormat.Location = new System.Drawing.Point(136, 10);
            this.ddlImageFormat.Name = "ddlImageFormat";
            this.ddlImageFormat.Size = new System.Drawing.Size(121, 21);
            this.ddlImageFormat.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Image Format:";
            // 
            // pnlCommonSettings
            // 
            this.pnlCommonSettings.AutoSize = true;
            this.pnlCommonSettings.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlCommonSettings.Controls.Add(this.pnlWebPageLoaded);
            this.pnlCommonSettings.Controls.Add(this.groupBoxCommonSettings);
            this.pnlCommonSettings.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCommonSettings.Location = new System.Drawing.Point(0, 0);
            this.pnlCommonSettings.Name = "pnlCommonSettings";
            this.pnlCommonSettings.Size = new System.Drawing.Size(703, 206);
            this.pnlCommonSettings.TabIndex = 0;
            // 
            // pnlWebPageLoaded
            // 
            this.pnlWebPageLoaded.Controls.Add(this.groupBox2);
            this.pnlWebPageLoaded.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlWebPageLoaded.Location = new System.Drawing.Point(0, 132);
            this.pnlWebPageLoaded.Name = "pnlWebPageLoaded";
            this.pnlWebPageLoaded.Size = new System.Drawing.Size(703, 74);
            this.pnlWebPageLoaded.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbMediaPrint);
            this.groupBox2.Controls.Add(this.textBoxConversionDelay);
            this.groupBox2.Controls.Add(this.label73);
            this.groupBox2.Controls.Add(this.label72);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.textBoxPassword);
            this.groupBox2.Controls.Add(this.label59);
            this.groupBox2.Controls.Add(this.textBoxUsername);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(703, 74);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            // 
            // cbMediaPrint
            // 
            this.cbMediaPrint.AutoSize = true;
            this.cbMediaPrint.Location = new System.Drawing.Point(343, 41);
            this.cbMediaPrint.Name = "cbMediaPrint";
            this.cbMediaPrint.Size = new System.Drawing.Size(136, 17);
            this.cbMediaPrint.TabIndex = 26;
            this.cbMediaPrint.Text = "Use CSS @Media Print";
            this.cbMediaPrint.UseVisualStyleBackColor = true;
            // 
            // textBoxConversionDelay
            // 
            this.textBoxConversionDelay.Location = new System.Drawing.Point(138, 39);
            this.textBoxConversionDelay.Name = "textBoxConversionDelay";
            this.textBoxConversionDelay.Size = new System.Drawing.Size(109, 20);
            this.textBoxConversionDelay.TabIndex = 24;
            this.textBoxConversionDelay.Text = "0";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(253, 42);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(47, 13);
            this.label73.TabIndex = 23;
            this.label73.Text = "seconds";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label72.Location = new System.Drawing.Point(15, 42);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(110, 13);
            this.label72.TabIndex = 22;
            this.label72.Text = "Conversion Delay:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(15, 16);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(93, 13);
            this.label30.TabIndex = 21;
            this.label30.Text = "Authentication:";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(404, 13);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(104, 20);
            this.textBoxPassword.TabIndex = 20;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(340, 16);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(56, 13);
            this.label59.TabIndex = 17;
            this.label59.Text = "Password:";
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.Location = new System.Drawing.Point(197, 13);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(104, 20);
            this.textBoxUsername.TabIndex = 19;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(133, 16);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(58, 13);
            this.label29.TabIndex = 18;
            this.label29.Text = "Username:";
            // 
            // groupBoxCommonSettings
            // 
            this.groupBoxCommonSettings.AutoSize = true;
            this.groupBoxCommonSettings.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBoxCommonSettings.Controls.Add(this.pnlWebPageSize);
            this.groupBoxCommonSettings.Controls.Add(this.pnlWebPageSizeControl);
            this.groupBoxCommonSettings.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxCommonSettings.Location = new System.Drawing.Point(0, 0);
            this.groupBoxCommonSettings.Name = "groupBoxCommonSettings";
            this.groupBoxCommonSettings.Size = new System.Drawing.Size(703, 132);
            this.groupBoxCommonSettings.TabIndex = 0;
            this.groupBoxCommonSettings.TabStop = false;
            // 
            // pnlWebPageSize
            // 
            this.pnlWebPageSize.Controls.Add(this.textBoxWebPageHeight);
            this.pnlWebPageSize.Controls.Add(this.textBoxWebPageWidth);
            this.pnlWebPageSize.Controls.Add(this.label3);
            this.pnlWebPageSize.Controls.Add(this.label1);
            this.pnlWebPageSize.Controls.Add(this.label2);
            this.pnlWebPageSize.Controls.Add(this.lblPageWidth);
            this.pnlWebPageSize.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlWebPageSize.Location = new System.Drawing.Point(3, 58);
            this.pnlWebPageSize.Name = "pnlWebPageSize";
            this.pnlWebPageSize.Size = new System.Drawing.Size(697, 71);
            this.pnlWebPageSize.TabIndex = 7;
            // 
            // textBoxWebPageHeight
            // 
            this.textBoxWebPageHeight.Location = new System.Drawing.Point(133, 38);
            this.textBoxWebPageHeight.Name = "textBoxWebPageHeight";
            this.textBoxWebPageHeight.Size = new System.Drawing.Size(104, 20);
            this.textBoxWebPageHeight.TabIndex = 1;
            this.textBoxWebPageHeight.Text = "0";
            // 
            // textBoxWebPageWidth
            // 
            this.textBoxWebPageWidth.Location = new System.Drawing.Point(133, 10);
            this.textBoxWebPageWidth.Name = "textBoxWebPageWidth";
            this.textBoxWebPageWidth.Size = new System.Drawing.Size(104, 20);
            this.textBoxWebPageWidth.TabIndex = 1;
            this.textBoxWebPageWidth.Text = "1024";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(248, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "px";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(248, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "px";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Web page height:";
            // 
            // lblPageWidth
            // 
            this.lblPageWidth.AutoSize = true;
            this.lblPageWidth.Location = new System.Drawing.Point(28, 13);
            this.lblPageWidth.Name = "lblPageWidth";
            this.lblPageWidth.Size = new System.Drawing.Size(88, 13);
            this.lblPageWidth.TabIndex = 0;
            this.lblPageWidth.Text = "Web page width:";
            // 
            // pnlWebPageSizeControl
            // 
            this.pnlWebPageSizeControl.Controls.Add(this.lblWebPageSize);
            this.pnlWebPageSizeControl.Controls.Add(this.radioButtonCustomSize);
            this.pnlWebPageSizeControl.Controls.Add(this.radioButtonAutodetect);
            this.pnlWebPageSizeControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlWebPageSizeControl.Location = new System.Drawing.Point(3, 16);
            this.pnlWebPageSizeControl.Name = "pnlWebPageSizeControl";
            this.pnlWebPageSizeControl.Size = new System.Drawing.Size(697, 42);
            this.pnlWebPageSizeControl.TabIndex = 6;
            // 
            // lblWebPageSize
            // 
            this.lblWebPageSize.AutoSize = true;
            this.lblWebPageSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWebPageSize.Location = new System.Drawing.Point(12, 15);
            this.lblWebPageSize.Name = "lblWebPageSize";
            this.lblWebPageSize.Size = new System.Drawing.Size(98, 13);
            this.lblWebPageSize.TabIndex = 3;
            this.lblWebPageSize.Text = "Web Page Size:";
            // 
            // radioButtonCustomSize
            // 
            this.radioButtonCustomSize.AutoSize = true;
            this.radioButtonCustomSize.Checked = true;
            this.radioButtonCustomSize.Location = new System.Drawing.Point(285, 15);
            this.radioButtonCustomSize.Name = "radioButtonCustomSize";
            this.radioButtonCustomSize.Size = new System.Drawing.Size(60, 17);
            this.radioButtonCustomSize.TabIndex = 5;
            this.radioButtonCustomSize.TabStop = true;
            this.radioButtonCustomSize.Text = "Custom";
            this.radioButtonCustomSize.UseVisualStyleBackColor = true;
            // 
            // radioButtonAutodetect
            // 
            this.radioButtonAutodetect.AutoSize = true;
            this.radioButtonAutodetect.Location = new System.Drawing.Point(136, 15);
            this.radioButtonAutodetect.Name = "radioButtonAutodetect";
            this.radioButtonAutodetect.Size = new System.Drawing.Size(77, 17);
            this.radioButtonAutodetect.TabIndex = 4;
            this.radioButtonAutodetect.Text = "Autodetect";
            this.radioButtonAutodetect.UseVisualStyleBackColor = true;
            this.radioButtonAutodetect.CheckedChanged += new System.EventHandler(this.radioButtonAutodetect_CheckedChanged);
            // 
            // pnlControlPanel
            // 
            this.pnlControlPanel.Controls.Add(this.cbEmbedFonts);
            this.pnlControlPanel.Controls.Add(this.groupBox1);
            this.pnlControlPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlControlPanel.Location = new System.Drawing.Point(0, 0);
            this.pnlControlPanel.Name = "pnlControlPanel";
            this.pnlControlPanel.Size = new System.Drawing.Size(703, 129);
            this.pnlControlPanel.TabIndex = 0;
            // 
            // cbEmbedFonts
            // 
            this.cbEmbedFonts.AutoSize = true;
            this.cbEmbedFonts.Location = new System.Drawing.Point(162, 102);
            this.cbEmbedFonts.Name = "cbEmbedFonts";
            this.cbEmbedFonts.Size = new System.Drawing.Size(85, 17);
            this.cbEmbedFonts.TabIndex = 12;
            this.cbEmbedFonts.Text = "Embed fonts";
            this.cbEmbedFonts.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbAutoSizePdfPage);
            this.groupBox1.Controls.Add(this.cbJpegCompression);
            this.groupBox1.Controls.Add(this.cbFitWidth);
            this.groupBox1.Controls.Add(this.cbBookmarks);
            this.groupBox1.Controls.Add(this.cbActiveXEnabled);
            this.groupBox1.Controls.Add(this.cbScriptsEnabled);
            this.groupBox1.Controls.Add(this.cbLiveLinks);
            this.groupBox1.Controls.Add(this.lnkBtnSettings);
            this.groupBox1.Controls.Add(this.radioConvertToImage);
            this.groupBox1.Controls.Add(this.radioButtonConvertToPDFSelectable);
            this.groupBox1.Controls.Add(this.textBoxWebPageURL);
            this.groupBox1.Controls.Add(this.lblEnterURL);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(703, 129);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // cbAutoSizePdfPage
            // 
            this.cbAutoSizePdfPage.AutoSize = true;
            this.cbAutoSizePdfPage.Checked = true;
            this.cbAutoSizePdfPage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAutoSizePdfPage.Location = new System.Drawing.Point(504, 102);
            this.cbAutoSizePdfPage.Name = "cbAutoSizePdfPage";
            this.cbAutoSizePdfPage.Size = new System.Drawing.Size(123, 17);
            this.cbAutoSizePdfPage.TabIndex = 13;
            this.cbAutoSizePdfPage.Text = "Auto Size PDF Page";
            this.cbAutoSizePdfPage.UseVisualStyleBackColor = true;
            // 
            // cbJpegCompression
            // 
            this.cbJpegCompression.AutoSize = true;
            this.cbJpegCompression.Checked = true;
            this.cbJpegCompression.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbJpegCompression.Location = new System.Drawing.Point(260, 102);
            this.cbJpegCompression.Name = "cbJpegCompression";
            this.cbJpegCompression.Size = new System.Drawing.Size(116, 17);
            this.cbJpegCompression.TabIndex = 12;
            this.cbJpegCompression.Text = "JPEG Compression";
            this.cbJpegCompression.UseVisualStyleBackColor = true;
            // 
            // cbFitWidth
            // 
            this.cbFitWidth.AutoSize = true;
            this.cbFitWidth.Checked = true;
            this.cbFitWidth.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbFitWidth.Location = new System.Drawing.Point(397, 102);
            this.cbFitWidth.Name = "cbFitWidth";
            this.cbFitWidth.Size = new System.Drawing.Size(68, 17);
            this.cbFitWidth.TabIndex = 12;
            this.cbFitWidth.Text = "Fit Width";
            this.cbFitWidth.UseVisualStyleBackColor = true;
            // 
            // cbBookmarks
            // 
            this.cbBookmarks.AutoSize = true;
            this.cbBookmarks.Location = new System.Drawing.Point(260, 79);
            this.cbBookmarks.Name = "cbBookmarks";
            this.cbBookmarks.Size = new System.Drawing.Size(79, 17);
            this.cbBookmarks.TabIndex = 11;
            this.cbBookmarks.Text = "Bookmarks";
            this.cbBookmarks.UseVisualStyleBackColor = true;
            // 
            // cbActiveXEnabled
            // 
            this.cbActiveXEnabled.AutoSize = true;
            this.cbActiveXEnabled.Location = new System.Drawing.Point(504, 79);
            this.cbActiveXEnabled.Name = "cbActiveXEnabled";
            this.cbActiveXEnabled.Size = new System.Drawing.Size(63, 17);
            this.cbActiveXEnabled.TabIndex = 10;
            this.cbActiveXEnabled.Text = "ActiveX";
            this.cbActiveXEnabled.UseVisualStyleBackColor = true;
            // 
            // cbScriptsEnabled
            // 
            this.cbScriptsEnabled.AutoSize = true;
            this.cbScriptsEnabled.Checked = true;
            this.cbScriptsEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbScriptsEnabled.Location = new System.Drawing.Point(397, 79);
            this.cbScriptsEnabled.Name = "cbScriptsEnabled";
            this.cbScriptsEnabled.Size = new System.Drawing.Size(76, 17);
            this.cbScriptsEnabled.TabIndex = 10;
            this.cbScriptsEnabled.Text = "JavaScript";
            this.cbScriptsEnabled.UseVisualStyleBackColor = true;
            // 
            // cbLiveLinks
            // 
            this.cbLiveLinks.AutoSize = true;
            this.cbLiveLinks.Checked = true;
            this.cbLiveLinks.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbLiveLinks.Location = new System.Drawing.Point(162, 79);
            this.cbLiveLinks.Name = "cbLiveLinks";
            this.cbLiveLinks.Size = new System.Drawing.Size(70, 17);
            this.cbLiveLinks.TabIndex = 10;
            this.cbLiveLinks.Text = "Live links";
            this.cbLiveLinks.UseVisualStyleBackColor = true;
            // 
            // lnkBtnSettings
            // 
            this.lnkBtnSettings.AutoSize = true;
            this.lnkBtnSettings.Location = new System.Drawing.Point(12, 80);
            this.lnkBtnSettings.Name = "lnkBtnSettings";
            this.lnkBtnSettings.Size = new System.Drawing.Size(136, 13);
            this.lnkBtnSettings.TabIndex = 8;
            this.lnkBtnSettings.TabStop = true;
            this.lnkBtnSettings.Text = "More Converter Settings >>";
            this.lnkBtnSettings.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkBtnSettings_LinkClicked);
            // 
            // radioConvertToImage
            // 
            this.radioConvertToImage.AutoSize = true;
            this.radioConvertToImage.Location = new System.Drawing.Point(360, 48);
            this.radioConvertToImage.Name = "radioConvertToImage";
            this.radioConvertToImage.Size = new System.Drawing.Size(105, 17);
            this.radioConvertToImage.TabIndex = 7;
            this.radioConvertToImage.Text = "Convert to image";
            this.radioConvertToImage.UseVisualStyleBackColor = true;
            this.radioConvertToImage.CheckedChanged += new System.EventHandler(this.radioConvertToImage_CheckedChanged);
            // 
            // radioButtonConvertToPDFSelectable
            // 
            this.radioButtonConvertToPDFSelectable.AutoSize = true;
            this.radioButtonConvertToPDFSelectable.Checked = true;
            this.radioButtonConvertToPDFSelectable.Location = new System.Drawing.Point(238, 48);
            this.radioButtonConvertToPDFSelectable.Name = "radioButtonConvertToPDFSelectable";
            this.radioButtonConvertToPDFSelectable.Size = new System.Drawing.Size(98, 17);
            this.radioButtonConvertToPDFSelectable.TabIndex = 6;
            this.radioButtonConvertToPDFSelectable.TabStop = true;
            this.radioButtonConvertToPDFSelectable.Text = "Convert to PDF";
            this.radioButtonConvertToPDFSelectable.UseVisualStyleBackColor = true;
            this.radioButtonConvertToPDFSelectable.CheckedChanged += new System.EventHandler(this.radioButtonConvertToPDFSelectable_CheckedChanged);
            // 
            // textBoxWebPageURL
            // 
            this.textBoxWebPageURL.Location = new System.Drawing.Point(238, 13);
            this.textBoxWebPageURL.Name = "textBoxWebPageURL";
            this.textBoxWebPageURL.Size = new System.Drawing.Size(447, 20);
            this.textBoxWebPageURL.TabIndex = 5;
            this.textBoxWebPageURL.Text = "http://www.html-to-pdf.net";
            // 
            // lblEnterURL
            // 
            this.lblEnterURL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnterURL.Location = new System.Drawing.Point(15, 16);
            this.lblEnterURL.Name = "lblEnterURL";
            this.lblEnterURL.Size = new System.Drawing.Size(217, 29);
            this.lblEnterURL.TabIndex = 4;
            this.lblEnterURL.Text = "Enter Web Page URL or File Path:";
            // 
            // errorProvider
            // 
            this.errorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider.ContainerControl = this;
            // 
            // pnlConvertCommand
            // 
            this.pnlConvertCommand.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlConvertCommand.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlConvertCommand.Controls.Add(this.btnConvert);
            this.pnlConvertCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlConvertCommand.Location = new System.Drawing.Point(0, 656);
            this.pnlConvertCommand.Name = "pnlConvertCommand";
            this.pnlConvertCommand.Size = new System.Drawing.Size(724, 55);
            this.pnlConvertCommand.TabIndex = 3;
            // 
            // btnConvert
            // 
            this.btnConvert.Location = new System.Drawing.Point(293, 18);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(118, 23);
            this.btnConvert.TabIndex = 0;
            this.btnConvert.Text = "Convert";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // HtmlConvertForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(724, 715);
            this.Controls.Add(this.pnlConvertCommand);
            this.Controls.Add(this.pnlContent);
            this.Controls.Add(this.pnlHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(730, 39);
            this.Name = "HtmlConvertForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ExpertPdf Html To Pdf Converter Demo";
            this.Load += new System.EventHandler(this.HtmlConvertForm_Load);
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.pnlContent.ResumeLayout(false);
            this.pnlContent.PerformLayout();
            this.pnlSettingsPanel.ResumeLayout(false);
            this.pnlSettingsPanel.PerformLayout();
            this.pnlPDFSettings.ResumeLayout(false);
            this.pnlFooterOptions.ResumeLayout(false);
            this.groupBoxPDFFooterOptions.ResumeLayout(false);
            this.groupBoxPDFFooterOptions.PerformLayout();
            this.pnlHeaderOptions.ResumeLayout(false);
            this.groupBoxPDFHeaderOptions.ResumeLayout(false);
            this.groupBoxPDFHeaderOptions.PerformLayout();
            this.pnlDocumentOptions.ResumeLayout(false);
            this.groupBoxPDFDocumentOptions.ResumeLayout(false);
            this.groupBoxPDFDocumentOptions.PerformLayout();
            this.pnlImgeOptions.ResumeLayout(false);
            this.pnlImgeOptions.PerformLayout();
            this.pnlCommonSettings.ResumeLayout(false);
            this.pnlCommonSettings.PerformLayout();
            this.pnlWebPageLoaded.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBoxCommonSettings.ResumeLayout(false);
            this.pnlWebPageSize.ResumeLayout(false);
            this.pnlWebPageSize.PerformLayout();
            this.pnlWebPageSizeControl.ResumeLayout(false);
            this.pnlWebPageSizeControl.PerformLayout();
            this.pnlControlPanel.ResumeLayout(false);
            this.pnlControlPanel.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.pnlConvertCommand.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Panel pnlContent;
        private System.Windows.Forms.Panel pnlControlPanel;
        private System.Windows.Forms.Panel pnlSettingsPanel;
        private System.Windows.Forms.Panel pnlPDFSettings;
        private System.Windows.Forms.Panel pnlImgeOptions;
        private System.Windows.Forms.Panel pnlCommonSettings;
        private System.Windows.Forms.ComboBox ddlImageFormat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel pnlFooterOptions;
        private System.Windows.Forms.Panel pnlHeaderOptions;
        private System.Windows.Forms.Panel pnlDocumentOptions;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupBoxPDFDocumentOptions;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxRightMargin;
        private System.Windows.Forms.TextBox textBoxTopMargin;
        private System.Windows.Forms.TextBox textBoxBottomMargin;
        private System.Windows.Forms.TextBox textBoxLeftMargin;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox cbShowHeader;
        private System.Windows.Forms.CheckBox cbShowFooter;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox cbGenerateSelectableText;
        private System.Windows.Forms.ComboBox ddlCompression;
        private System.Windows.Forms.ComboBox ddlPageFormat;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBoxPDFHeaderOptions;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBoxHeadeSubtitle;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxHeaderTitle;
        private System.Windows.Forms.ComboBox ddlHeaderTextColor;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.CheckBox cbDrawHeaderLine;
        private System.Windows.Forms.GroupBox groupBoxPDFFooterOptions;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBoxPageNumberText;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.CheckBox cbShowPageNumber;
        private System.Windows.Forms.CheckBox cbDrawFooterLine;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBoxFooterText;
        private System.Windows.Forms.ComboBox ddlFooterColor;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.LinkLabel lnkBtnSettings;
        private System.Windows.Forms.RadioButton radioConvertToImage;
        private System.Windows.Forms.TextBox textBoxWebPageURL;
        private System.Windows.Forms.Label lblEnterURL;
        private System.Windows.Forms.GroupBox groupBoxCommonSettings;
        private System.Windows.Forms.Panel pnlWebPageSize;
        private System.Windows.Forms.TextBox textBoxWebPageHeight;
        private System.Windows.Forms.TextBox textBoxWebPageWidth;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPageWidth;
        private System.Windows.Forms.Panel pnlWebPageSizeControl;
        private System.Windows.Forms.Label lblWebPageSize;
        private System.Windows.Forms.RadioButton radioButtonCustomSize;
        private System.Windows.Forms.RadioButton radioButtonAutodetect;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.Panel pnlConvertCommand;
        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.RadioButton radioButtonConvertToPDFSelectable;
        private System.Windows.Forms.Panel pnlWebPageLoaded;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox cbAvoidImageBreak;
        private System.Windows.Forms.ComboBox ddlPageOrientation;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckBox cbAvoidTextBreak;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ComboBox ddlHeaderFont;
        private System.Windows.Forms.TextBox textBoxHeaderTextFontSize;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBoxHeaderHeight;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textBoxTitleYLocation;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox textBoxSubtitleYSpacing;
        private System.Windows.Forms.TextBox textBoxSubtitleFontSize;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button btnChangeHeaderImage;
        private System.Windows.Forms.Label lblLicensesDirectory;
        private System.Windows.Forms.TextBox textBoxHeaderImagePath;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox textBoxHeaderImageYLocation;
        private System.Windows.Forms.TextBox textBoxHeaderImageXLocation;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.ComboBox ddlHeaderBackColor;
        private System.Windows.Forms.ComboBox ddlSubtitleColor;
        private System.Windows.Forms.ComboBox ddlSubtitleFont;
        private System.Windows.Forms.ComboBox ddlFooterBackColor;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox textBoxFooterHeight;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox textBoxFooterTextFontSize;
        private System.Windows.Forms.ComboBox ddlFooterTextFont;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.ComboBox ddlPageNumberTextColor;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox textBoxPageNumberFontSize;
        private System.Windows.Forms.ComboBox ddlPageNumberTextFont;
        private System.Windows.Forms.CheckBox cbLiveLinks;
        private System.Windows.Forms.CheckBox cbShowFooterOnEvenPages;
        private System.Windows.Forms.CheckBox cbShowFooterOnOddPages;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox textBoxFooterTextYLocation;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox textBoxFooterWidth;
        private System.Windows.Forms.CheckBox cbShowHeaderOnEvenPages;
        private System.Windows.Forms.CheckBox cbShowHeaderOnOddPages;
        private System.Windows.Forms.ComboBox ddlTitleAlign;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox textBoxCustomPdfPageHeight;
        private System.Windows.Forms.TextBox textBoxCustomPdfPageWidth;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox textBoxPageNumberYLocation;
        private System.Windows.Forms.CheckBox cbScriptsEnabled;
        private System.Windows.Forms.CheckBox cbFitWidth;
        private System.Windows.Forms.CheckBox cbBookmarks;
        private System.Windows.Forms.CheckBox cbActiveXEnabled;
        private System.Windows.Forms.CheckBox cbEmbedFonts;
        private System.Windows.Forms.CheckBox cbShowFooterOnFirstPage;
        private System.Windows.Forms.CheckBox cbShowHeaderOnFirstPage;
        private System.Windows.Forms.CheckBox cbJpegCompression;
        private System.Windows.Forms.ComboBox ddlPdfSubset;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox textBoxConversionDelay;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.CheckBox cbAutoSizePdfPage;
        private System.Windows.Forms.CheckBox cbMediaPrint;
    }
}

