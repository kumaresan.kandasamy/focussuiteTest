using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ExpertPdf.HtmlToPdf;

namespace WnvHtmlConvertConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length>2)
            {
                Console.WriteLine("Usage: HtmlConvertConsoleApp.exe [/format:(pdf|image)] [URL] ");
                return;
            }
            string url = "http://www.html-to-pdf.net";
            bool pdfFormat = true; // output a PDF by default

            if (args.Length == 1)
            {
                if (args[0] == "/format:pdf" || args[0] == "/format:image")
                {
                    // this is a format specification
                    if (args[0] == "/format:image")
                    {
                        pdfFormat = false;
                    }
                }
                else
                {
                    // this must be the URL
                    url = args[0];
                }
            }
            else if (args.Length == 2)
            {
                // the first argument is expected to be the 
                // output format
                string outFormatString = args[0];
                if (!outFormatString.StartsWith("/format:"))
                {
                    Console.WriteLine("Usage: WnvHtmlConvertConsoleApp.exe [/format:(pdf|image)] URL ");
                    return;
                }
                outFormatString = outFormatString.Substring("/format:".Length, outFormatString.Length - "/format:".Length);
                if (outFormatString != "pdf" && outFormatString != "image")
                {
                    Console.WriteLine("Usage: WnvHtmlConvertConsoleApp.exe [/format:(pdf|image)] URL ");
                    return;
                }

                if (outFormatString == "image")
                {
                    // change output format to image
                    pdfFormat = false;
                }

                //the second argument is expected to be the URL
                url = args[1];
            }

            Console.WriteLine(String.Format("Converting web page '{0}' ...", url));

            try
            {
                string outFile = null;
                if (pdfFormat)
                {
                    PdfConverter pdfConverter = new PdfConverter();
                    //pdfConverter.LicenseKey = "put your license key here";
                    pdfConverter.PdfDocumentOptions.EmbedFonts = false;
                    pdfConverter.PdfDocumentOptions.ShowFooter = false;
                    pdfConverter.PdfDocumentOptions.ShowHeader = false;
                    pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                    outFile = Path.Combine(GetAppPath(), "RenderedPage.pdf");
                    try
                    {
                        pdfConverter.SavePdfFromUrlToFile(url, outFile);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return;
                    }
                }
                else
                {
                    ImgConverter imgConverter = new ImgConverter();
                    //imgConverter.LicenseKey = "put your license key here";
                    outFile = Path.Combine(GetAppPath(), "RenderedPage.jpeg");
                    try
                    {
                        imgConverter.SaveImageFromUrlToFile(
                                url,
                                System.Drawing.Imaging.ImageFormat.Jpeg,
                                outFile
                             );
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return;
                    }
                }

                Console.WriteLine(String.Format("The web page was rendered in '{0}'.", outFile));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
        }

        private static string GetAppPath()
        {
            return Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        }
    }
}
