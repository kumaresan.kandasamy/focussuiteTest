using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ExpertPdf.HtmlToPdf;

/*
 * This simple console application will convert all the HTML files from the specified folder.
 */

namespace WinConsole_BatchConversion
{
    class Program
    {
        static void Main(string[] args)
        {
            // check if the source folder was specified
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: batchconvert.exe html_source_folder_path");
                return;
            }

            string folderPath = args[0];

            if (!Directory.Exists(folderPath))
            {
                Console.WriteLine(String.Format("The specified folder '{0}' does not exist.", folderPath));
                return;
            }

            // get all the .html files from the folder
            string[] htmlFiles = Directory.GetFiles(folderPath, "*.html", SearchOption.TopDirectoryOnly);

            if (htmlFiles.Length == 0)
            {
                Console.WriteLine("No .html file to convert.");
                return;
            }

            PdfConverter pdfConverter = new PdfConverter();

            //pdfConverter.LicenseKey = "put your license key here";

            foreach (string htmlFileName in htmlFiles)
            {
                string htmlFilePath = Path.GetFullPath(htmlFileName);
                // the resulted PDF will be located in the same directory with the source
                // HTML file and will have the same name except the extension
                string pdfFilePath = Path.Combine(Path.GetDirectoryName(htmlFilePath), 
                    Path.GetFileNameWithoutExtension(htmlFilePath)) + ".pdf";

                // convert the HTML file to PDF
                Console.WriteLine(String.Format("{0:hh:mm:ss}: converting {1}", DateTime.Now, htmlFilePath));
                try
                {
                    pdfConverter.SavePdfFromHtmlFileToFile(htmlFilePath, pdfFilePath);
                    Console.WriteLine(String.Format("{0:hh:mm:ss}: converted to {1}", DateTime.Now, pdfFilePath));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
