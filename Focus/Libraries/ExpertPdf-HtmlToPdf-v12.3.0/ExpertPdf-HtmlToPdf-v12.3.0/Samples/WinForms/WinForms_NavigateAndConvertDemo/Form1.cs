using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ExpertPdf.HtmlToPdf;

namespace NavigationConvertDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            webBrowser.Navigate("http://www.html-to-pdf.net");
        }

        private void btnNavigate_Click(object sender, EventArgs e)
        {
            webBrowser.Navigate(textBoxURL.Text.Trim());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PdfConverter pdfConverter = new PdfConverter();

            //pdfConverter.LicenseKey = "put your license key here";

            pdfConverter.ScriptsEnabled = cbScriptsEnabled.Checked;
            pdfConverter.ActiveXEnabled = cbActiveX.Checked;
            pdfConverter.AvoidImageBreak = cbAvoidImageBreak.Checked;
            pdfConverter.PdfBookmarkOptions.TagNames = cbBookmarks.Checked ? new string[] { "h1", "h2" } : null;

            string outFile = System.IO.Path.Combine(Application.StartupPath, "RenderedPage.pdf");

            this.Cursor = Cursors.WaitCursor;

            try
            {
                pdfConverter.SavePdfFromHtmlStringToFile(webBrowser.DocumentText, outFile, webBrowser.Url.AbsoluteUri);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                this.Cursor = Cursors.Arrow;
            }

            DialogResult dr = MessageBox.Show("Open the rendered file in an external viewer?", "Open Rendered File", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                try
                {
                    System.Diagnostics.Process.Start(outFile);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return;
                }
            }
        }

        private void textBoxURL_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                webBrowser.Navigate(textBoxURL.Text.Trim());
            }
        }
    }
}