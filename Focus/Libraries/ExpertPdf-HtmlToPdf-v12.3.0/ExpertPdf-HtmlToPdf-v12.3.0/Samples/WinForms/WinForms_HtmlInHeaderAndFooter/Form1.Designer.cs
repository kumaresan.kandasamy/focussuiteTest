namespace WinForms_HtmlHeaderAndFooter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxURL1 = new System.Windows.Forms.TextBox();
            this.btnConvertMerge = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cbAlternateHeaderAndFooter = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Web Page URL or Full File Path:";
            // 
            // textBoxURL1
            // 
            this.textBoxURL1.Location = new System.Drawing.Point(210, 63);
            this.textBoxURL1.Name = "textBoxURL1";
            this.textBoxURL1.Size = new System.Drawing.Size(347, 20);
            this.textBoxURL1.TabIndex = 1;
            this.textBoxURL1.Text = "http://www.html-to-pdf.net";
            // 
            // btnConvertMerge
            // 
            this.btnConvertMerge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConvertMerge.Location = new System.Drawing.Point(15, 102);
            this.btnConvertMerge.Name = "btnConvertMerge";
            this.btnConvertMerge.Size = new System.Drawing.Size(140, 23);
            this.btnConvertMerge.TabIndex = 2;
            this.btnConvertMerge.Text = "Convert";
            this.btnConvertMerge.UseVisualStyleBackColor = true;
            this.btnConvertMerge.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(15, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(542, 41);
            this.label3.TabIndex = 3;
            this.label3.Text = "This demo shows how to add HTML in the header and footer of the generated PDF doc" +
    "ument and how to add page numbers in the footer.";
            // 
            // cbAlternateHeaderAndFooter
            // 
            this.cbAlternateHeaderAndFooter.AutoSize = true;
            this.cbAlternateHeaderAndFooter.Location = new System.Drawing.Point(198, 106);
            this.cbAlternateHeaderAndFooter.Name = "cbAlternateHeaderAndFooter";
            this.cbAlternateHeaderAndFooter.Size = new System.Drawing.Size(160, 17);
            this.cbAlternateHeaderAndFooter.TabIndex = 5;
            this.cbAlternateHeaderAndFooter.Text = "Alternate Header and Footer";
            this.cbAlternateHeaderAndFooter.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(581, 133);
            this.Controls.Add(this.cbAlternateHeaderAndFooter);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnConvertMerge);
            this.Controls.Add(this.textBoxURL1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ExpertPDF HTML in Header and Footer Sample";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxURL1;
        private System.Windows.Forms.Button btnConvertMerge;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cbAlternateHeaderAndFooter;
    }
}

