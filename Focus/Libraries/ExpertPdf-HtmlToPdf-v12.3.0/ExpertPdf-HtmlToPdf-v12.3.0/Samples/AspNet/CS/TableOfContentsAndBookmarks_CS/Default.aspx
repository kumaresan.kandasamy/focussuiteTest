<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>HTML to PDF Converter - Generate a Table of Contents with Page Numbers</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta http-equiv="Content-Language" content="english">
    <meta content="index,follow" name="robots">
    <link href="styles.css" type="text/css" rel="stylesheet">
    <meta content="MSHTML 6.00.2900.2963" name="GENERATOR">
</head>
<body>
    <form runat="server" id="convertForm">
        <!-- Header -->
        <div class="left">
            <table class="head" border="0">
                <tbody>
                    <tr>
                        <td style="width: 10px">
                            &nbsp;</td>
                        <td style="width: 90px">
                            <img src="img/logo.jpg" /></td>
                        <td valign="bottom" height="60" align="left" style="width: 85%">
                            <h1>
                                Generate a Table of Contents with Page Numbers
                            </h1>
                        </td>
                        <td class="langoff" valign="top" align="right">
                            &nbsp;</td>
                    </tr>
                </tbody>
            </table>
            <br />
            <table>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        This demo shows how to create a table of contents in the PDF document generated
                        from a HTML book and how to add bookmarks in PDF to the sections of the book.
                        <br />
                        <br />
                        The creation of the table of contents is based on the capabilities of the converter
                        to convert the internal links from HTML to internal links in PDF and to obtain the
                        position in PDF of a HTML element based on the ID of the element.
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblConvertMessage" runat="server" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:Button runat="server" Text="Convert to PDF" ID="btnConvert" OnClick="btnConvert_Click" />&nbsp;&nbsp;
                        &nbsp; &nbsp;<a href="HtmlBook/Book.htm" target="_blank">View HTML Book</a></td>
                </tr>
            </table>
            <!-- Footer -->
            <br />
            <table class="footer">
                <tbody>
                    <tr>
                        <td class="seph">
                        </td>
                    </tr>
                    <tr>
                        <td class="lightbluetext">
                            Copyright by <a class="lightbluetext" href="http://www.html-to-pdf.net"
                                target="_blank">ExpertPDF</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
