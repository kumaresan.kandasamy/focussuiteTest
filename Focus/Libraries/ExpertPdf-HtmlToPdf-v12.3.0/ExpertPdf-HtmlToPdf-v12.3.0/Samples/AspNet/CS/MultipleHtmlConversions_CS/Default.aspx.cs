using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Drawing;

using ExpertPdf.HtmlToPdf;
using ExpertPdf.HtmlToPdf.PdfDocument;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnConvert_Click(object sender, EventArgs e)
    {
        PdfConverter pdfConverter = new PdfConverter();

        // add header and footer
        if (cbAddHeader.Checked)
            AddHeader(pdfConverter);
        if (cbAddFooter.Checked)
            AddFooter(pdfConverter);

        // call the converter and get a Document object from URL
        Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromUrl(textBoxURL1.Text.Trim());

        // get the conversion summary object from the event arguments
        ConversionSummary conversionSummary = pdfConverter.ConversionSummary;

        // the PDF page where the previous conversion ended
        PdfPage lastPage = pdfDocument.Pages[conversionSummary.LastPageIndex];
        // the last rectangle in the last PDF page where the previous conversion ended
        RectangleF lastRectangle = conversionSummary.LastPageRectangle;

        // the result of adding an element to a PDF page
        // ofers the index of the PDF page where the rendering ended 
        // and the bounding rectangle of the rendered content in the last page
        AddElementResult addResult = null;

        // the converter for the second URL
        HtmlToPdfElement htmlToPdfURL2 = null;

        if (cbStartOnNewPage.Checked)
        {
            // render the HTML from the second URL on a new page after the first URL
            PdfPage newPage = pdfDocument.Pages.AddNewPage();
            htmlToPdfURL2 = new HtmlToPdfElement(0, 0, textBoxURL2.Text);
            addResult = newPage.AddElement(htmlToPdfURL2);
        }
        else
        {
            // render the HTML from the second URL immediately after the first URL
            htmlToPdfURL2 = new HtmlToPdfElement(lastRectangle.Left, lastRectangle.Bottom, textBoxURL2.Text);
            addResult = lastPage.AddElement(htmlToPdfURL2);
        }

        // the PDF page where the previous conversion ended
        lastPage = pdfDocument.Pages[addResult.EndPageIndex];

        // add a HTML string after all the rendered content
        HtmlToPdfElement htmlStringToPdf = new HtmlToPdfElement(addResult.EndPageBounds.Left, addResult.EndPageBounds.Bottom,
            "<b><i>The rendered content ends here</i></b>", null);

        lastPage.AddElement(htmlStringToPdf);

        byte[] pdfBytes = null;

        try
        {
            pdfBytes = pdfDocument.Save();
        }
        finally
        {
            // close the Document to realease all the resources
            pdfDocument.Close();
        }

        // send the PDF document as a response to the browser for download
        System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        response.Clear();
        response.AddHeader("Content-Type", "binary/octet-stream");
        response.AddHeader("Content-Disposition",
            "attachment; filename=ConversionResult.pdf; size=" + pdfBytes.Length.ToString());
        response.Flush();
        response.BinaryWrite(pdfBytes);
        response.Flush();
        response.End();
    }

    private void AddHeader(PdfConverter pdfConverter)
    {
        string thisPageURL = HttpContext.Current.Request.Url.AbsoluteUri;
        string headerAndFooterHtmlUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/HeaderAndFooterHtml.htm";

        //enable header
        pdfConverter.PdfDocumentOptions.ShowHeader = true;
        // set the header height in points
        pdfConverter.PdfHeaderOptions.HeaderHeight = 60;
        // set the header HTML area
        pdfConverter.PdfHeaderOptions.HtmlToPdfArea = new HtmlToPdfArea(0, 0, -1, pdfConverter.PdfHeaderOptions.HeaderHeight,
                    headerAndFooterHtmlUrl, 1024, -1);
        pdfConverter.PdfHeaderOptions.HtmlToPdfArea.FitHeight = true;
    }

    private void AddFooter(PdfConverter pdfConverter)
    {
        string thisPageURL = HttpContext.Current.Request.Url.AbsoluteUri;
        string headerAndFooterHtmlUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/HeaderAndFooterHtml.htm";

        //enable footer
        pdfConverter.PdfDocumentOptions.ShowFooter = true;
        // set the footer height in points
        pdfConverter.PdfFooterOptions.FooterHeight = 60;
        //write the page number
        pdfConverter.PdfFooterOptions.TextArea = new TextArea(0, 30, "This is page &p; of &P;  ",
            new System.Drawing.Font(new System.Drawing.FontFamily("Times New Roman"), 10, System.Drawing.GraphicsUnit.Point));
        pdfConverter.PdfFooterOptions.TextArea.EmbedTextFont = true;
        pdfConverter.PdfFooterOptions.TextArea.TextAlign = HorizontalTextAlign.Right;
        // set the footer HTML area
        pdfConverter.PdfFooterOptions.HtmlToPdfArea = new HtmlToPdfArea(0, 0, -1, pdfConverter.PdfFooterOptions.FooterHeight,
                    headerAndFooterHtmlUrl, 1024, -1);
        pdfConverter.PdfFooterOptions.HtmlToPdfArea.FitHeight = true;
    }

}
