<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>HTML to PDF Converter - Multiple Conversions in the Same PDF Document</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta http-equiv="Content-Language" content="english">
    <meta content="index,follow" name="robots">
    <link href="styles.css" type="text/css" rel="stylesheet">
    <meta content="MSHTML 6.00.2900.2963" name="GENERATOR">
</head>
<body>
    <form runat="server" id="convertForm">
        <!-- Header -->
        <div class="left">
            <table class="head" border="0">
                <tbody>
                    <tr>
                        <td style="width: 10px">
                            &nbsp;</td>
                        <td style="width: 90px">
                            <img src="img/logo.jpg" /></td>
                        <td valign="bottom" height="60" align="left" style="width: 85%">
                            <h1>
                                Multiple Conversions in the Same PDF Document
                            </h1>
                        </td>
                        <td class="langoff" valign="top" align="right">
                            &nbsp;</td>
                    </tr>
                </tbody>
            </table>
            <br />
            <table>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        This demo shows how to convert multiple URLs to the same PDF document. There is
                        also an option to specify if the result of the conversion of an URL starts right
                        after the previous conversion or on a new PDF page.
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td align="left" width="100%">
                        <table cellspacing="0" cellpadding="0" align="left" width="100%">
                            <tr>
                                <td style="height: 33px">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td style="font-weight: bold">
                                                Enter First URL:</td>
                                            <td>
                                                &nbsp;</td>
                                            <td>
                                                <asp:TextBox ID="textBoxURL1" runat="server" Text="http://www.html-to-pdf.net"
                                                    Width="315px"></asp:TextBox></td>
                                            <td>
                                                <asp:RequiredFieldValidator ID="urlRequired" runat="server" ControlToValidate="textBoxURL1">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="height: 12px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                Enter Second URL:</td>
                                            <td>
                                                &nbsp;</td>
                                            <td>
                                                <asp:TextBox ID="textBoxURL2" runat="server" Text="http://www.google.com" Width="315px"></asp:TextBox></td>
                                            <td>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="textBoxURL2">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 12px">
                    </td>
                </tr>
                <tr>
                    <td style="height: 5px">
                        <asp:CheckBox ID="cbAddHeader" runat="server" Text="Add Header" ToolTip="Add a HTML header to the rendered PDF document" />&nbsp;&nbsp;
                        &nbsp;
                        <asp:CheckBox ID="cbAddFooter" runat="server" Text="Add Footer" ToolTip="Add
                                                a footer to the rendered PDF document" Checked="True" />
                    </td>
                </tr>
                <tr>
                    <td style="height: 12px">
                    </td>
                </tr>
                <tr>
                    <td style="height: 5px">
                        <asp:CheckBox ID="cbStartOnNewPage" runat="server" Text="Start second URL conversion on a new PDF page"
                            ToolTip="Start second URL conversion on a new PDF page" /></td>
                </tr>
                <tr>
                    <td style="height: 12px">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblConvertMessage" runat="server" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:Button runat="server" Text="Convert" ID="btnConvert" OnClick="btnConvert_Click" />&nbsp;&nbsp;
                        &nbsp; &nbsp;</td>
                </tr>
            </table>
            <!-- Footer -->
            <br />
            <table class="footer">
                <tbody>
                    <tr>
                        <td class="seph">
                        </td>
                    </tr>
                    <tr>
                        <td class="lightbluetext">
                            Copyright by <a class="lightbluetext" href="http://www.html-to-pdf.net"
                                target="_blank">ExpertPDF</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
