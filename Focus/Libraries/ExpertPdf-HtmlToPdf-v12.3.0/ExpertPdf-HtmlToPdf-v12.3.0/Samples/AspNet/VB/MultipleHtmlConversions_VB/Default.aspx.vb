Imports System.Drawing

Imports ExpertPdf.HtmlToPdf
Imports ExpertPdf.HtmlToPdf.PdfDocument

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub btnConvert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConvert.Click

        Dim pdfConverter As PdfConverter = New PdfConverter()

        ' add header and footer
        If (cbAddHeader.Checked) Then
            AddHeader(pdfConverter)
        End If
        If (cbAddFooter.Checked) Then
            AddFooter(pdfConverter)
        End If


        ' call the converter and get a Document object from URL
        Dim pdfDocument As Document = pdfConverter.GetPdfDocumentObjectFromUrl(textBoxURL1.Text.Trim())

        ' get the conversion summary object from the event arguments
        Dim conversionSummary As ConversionSummary = pdfConverter.ConversionSummary

        ' the PDF page where the previous conversion ended
        Dim lastPage As PdfPage = pdfDocument.Pages.Item(conversionSummary.LastPageIndex)
        ' the last rectangle in the last PDF page where the previous conversion ended
        Dim lastRectangle As RectangleF = conversionSummary.LastPageRectangle

        ' the result of adding an element to a PDF page
        ' ofers the index of the PDF page where the rendering ended 
        ' and the bounding rectangle of the rendered content in the last page
        Dim addResult As AddElementResult

        ' the converter for the second URL
        Dim htmlToPdfURL2 As HtmlToPdfElement = Nothing

        If (cbStartOnNewPage.Checked) Then

            ' render the HTML from the second URL on a new page after the first URL
            Dim newPage As PdfPage = pdfDocument.Pages.AddNewPage()
            htmlToPdfURL2 = New HtmlToPdfElement(0, 0, textBoxURL2.Text)
            addResult = newPage.AddElement(htmlToPdfURL2)

        Else

            ' render the HTML from the second URL immediately after the first URL
            htmlToPdfURL2 = New HtmlToPdfElement(lastRectangle.Left, lastRectangle.Bottom, textBoxURL2.Text)
            addResult = lastPage.AddElement(htmlToPdfURL2)

        End If

        ' the PDF page where the previous conversion ended
        lastPage = pdfDocument.Pages.Item(addResult.EndPageIndex)

        ' add a HTML string after all the rendered content
        Dim htmlStringToPdf As HtmlToPdfElement = New HtmlToPdfElement(addResult.EndPageBounds.Left, addResult.EndPageBounds.Bottom, "<b><i>The rendered content ends here</i></b>", Nothing)

        lastPage.AddElement(htmlStringToPdf)

        ' call the converter
        Dim pdfBytes As Byte()

        Try
            pdfBytes = pdfDocument.Save()
        Finally
            ' close the Document to realease all the resources
            pdfDocument.Close()
        End Try

        ' send the PDF document as a response to the browser for download
        Dim Response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
        Response.Clear()
        Response.AddHeader("Content-Type", "binary/octet-stream")
        Response.AddHeader("Content-Disposition", "attachment; filename=ConversionResult.pdf; size=" & pdfBytes.Length.ToString())
        Response.Flush()
        Response.BinaryWrite(pdfBytes)
        Response.Flush()
        Response.End()
    End Sub


    Private Sub AddHeader(ByRef pdfConverter As PdfConverter)
        Dim thisPageURL As String = HttpContext.Current.Request.Url.AbsoluteUri
        Dim headerAndFooterHtmlUrl As String = thisPageURL.Substring(0, thisPageURL.LastIndexOf("/")) + "/HeaderAndFooterHtml.htm"

        'enable header
        pdfConverter.PdfDocumentOptions.ShowHeader = True
        ' set the header height in points
        pdfConverter.PdfHeaderOptions.HeaderHeight = 60
        ' set the header HTML area
        pdfConverter.PdfHeaderOptions.HtmlToPdfArea = New HtmlToPdfArea(0, 0, -1, pdfConverter.PdfHeaderOptions.HeaderHeight, headerAndFooterHtmlUrl, 1024, -1)
        pdfConverter.PdfHeaderOptions.HtmlToPdfArea.FitHeight = True
    End Sub

    Private Sub AddFooter(ByRef pdfConverter As PdfConverter)
        Dim thisPageURL As String = HttpContext.Current.Request.Url.AbsoluteUri
        Dim headerAndFooterHtmlUrl As String = thisPageURL.Substring(0, thisPageURL.LastIndexOf("/")) + "/HeaderAndFooterHtml.htm"

        'enable footer
        pdfConverter.PdfDocumentOptions.ShowFooter = True
        ' set the footer height in points
        pdfConverter.PdfFooterOptions.FooterHeight = 60
        'write the page number
        pdfConverter.PdfFooterOptions.TextArea = New TextArea(0, 30, "This is page &p; of &P;  ", New System.Drawing.Font(New System.Drawing.FontFamily("Times New Roman"), 10, System.Drawing.GraphicsUnit.Point))
        pdfConverter.PdfFooterOptions.TextArea.EmbedTextFont = True
        pdfConverter.PdfFooterOptions.TextArea.TextAlign = HorizontalTextAlign.Right
        ' set the footer HTML area
        pdfConverter.PdfFooterOptions.HtmlToPdfArea = New HtmlToPdfArea(0, 0, -1, pdfConverter.PdfFooterOptions.FooterHeight, headerAndFooterHtmlUrl, 1024, -1)
        pdfConverter.PdfFooterOptions.HtmlToPdfArea.FitHeight = True
    End Sub

End Class
