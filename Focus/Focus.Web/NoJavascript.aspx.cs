﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Web.Code;

#endregion

namespace Focus.Web
{
	public partial class NoJavascript : PageBase
	{
		/// <summary>
		/// Gets the help text.
		/// </summary>
		/// <value>The help text.</value>
		protected string HelpText
		{
			get
			{
				const string helpText = @"JavaScript must be enabled in order for you to use this application. 
			However, it seems JavaScript is either disabled or not supported by your browser. <br/> Please click <a href='{0}' target='_blank'>here</a> for assistance.  After changing browser setting, click <a href='{1}'>here</a> to login.";

				return CodeLocalise("Help.Text", string.Format(helpText, UrlBuilder.JavascriptHelp(), UrlBuilder.LogIn()));
			}
		}
	}
}