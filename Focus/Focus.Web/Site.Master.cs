﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using AjaxControlToolkit;

using Focus.Common;
using Focus.Common.Controls;
using Focus.Common.Helpers;
using Focus.Core;
using Focus.Web.Code;
using Framework.Core;

#endregion

namespace Focus.Web
{
	public partial class SiteMaster : MasterPageBase
	{
		protected HelpTypes PrivacyHelpType()
		{
			return HelpTypes.PrivacyAndSecurity;
		}

		protected HelpTypes FrequentlyAskedQuestionsHelpType()
		{
			return HelpTypes.FrequentlyAskedQuestions;
		}

		protected HelpTypes TermsOfUseHelpType()
		{
			return HelpTypes.TermsOfUse;
		}

		public HiddenField ReturnUrl1
		{
			get { return ReturnUrl; }
			set { ReturnUrl = value; }
		}

		public string CoreStylesheetUrl
		{
			get { return UrlHelper.GetCacheBusterUrl(string.Format("~/Branding/{0}/Focus.{1}.css", App.Settings.BrandIdentifier, GetModule())); }
		}

		public string DeviceStylesheetUrl
		{
			get { return UrlHelper.GetCacheBusterUrl(string.Format("~/Branding/{0}/Focus.Device.css", App.Settings.BrandIdentifier)); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			ScriptManagerMain.EnableCdn = App.Settings.EnableCDN;

      //needed to here for the ResolveUrl method to work
      Page.Header.DataBind();

			// Need to establish what to show here but if there's a client logo then it should be fetched and listed here
			MainLogoImage.ImageUrl = UrlBuilder.MainLogoImage(IsAssist);
      
			// Do we need to show burning glass logo
			FooterImage.Visible = IsTalent ? App.Settings.ShowBurningGlassLogoInTalent : App.Settings.ShowBurningGlassLogoInAssist;

			const string headerStyles = @"<style>#header {{background:#{1};height:133px;}} #headerTopWrap {{background:#{2};height:74px;padding:7px 0;}}</style>";

			var lightColour = IsAssist ? App.Settings.AssistLightColour : App.Settings.TalentLightColour;
			var darkColour = IsAssist ? App.Settings.AssistDarkColour : App.Settings.TalentDarkColour;

			FocusStyle.Text = string.Format(headerStyles, UrlBuilder.MainCss(IsAssist), lightColour, darkColour);

			if (App.Settings.UseCustomHeaderHtml)
			{
				CustomHeader.InnerHtml = App.Settings.CustomHeaderHtml;
				HideStandardHeader();
			}
			else
			{
				CustomHeader.InnerHtml = string.Empty;
			}

			if (App.Settings.UseCustomFooterHtml)
			{
				CustomFooter.InnerHtml = App.Settings.CustomFooterHtml;
				// TODO: check below as this started erroring after a GIT update / merge
				FooterContent.Visible = false;
			}
			else
			{
				CustomFooter.InnerHtml = string.Empty;
			}

			if (App.Settings.HideTalentAssistAccountSettings)
			{
				var accountsettingControl = (Panel)HeadLoginView.FindControl("AccountSettingsControl");
				if (accountsettingControl !=null)
					accountsettingControl.Visible = false;
			}
			else
			{
				var accountSettingsLink = (HyperLink) HeadLoginView.FindControl("AccountSettingsLink");
				if (accountSettingsLink.IsNotNull())
				{
					accountSettingsLink.NavigateUrl = UrlBuilder.AccountSettings(IsAssist,
					(IsAssist ? DisableAssistAuthentication : DisableTalentAuthentication), DisableChangePassword);
					accountSettingsLink.Text = App.Settings.UseUsernameAsAccountLink
						? string.Format("{0} {1}", App.User.FirstName, App.User.LastName)
						: HtmlLocalise("Global.AccountSettings.LinkText", "Account settings");
				}
			}

			if (App.Settings.HideTalentAssistSignOut)
			{
				var signoutcontrol = (Panel)HeadLoginView.FindControl("SignOutControl");
				if (signoutcontrol !=null)
					signoutcontrol.Visible = false;
      }

			if (App.Settings.Theme == FocusThemes.Workforce)
			{
				var category = Convert.ToInt64(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DocumentCategories).Where(x => x.Key.Equals("DocumentCategories.LegalNotice")).Select(x => x.Id).FirstOrDefault());

				LegalNoticesUrl.HRef = UrlBuilder.Notice(Uri.EscapeDataString(HtmlLocalise("LegalNotice.Title", "Legal Notices")),category, IsAssist ? DocumentFocusModules.Assist : DocumentFocusModules.Talent);
			}
			else
			{
				LegalNoticesCell.Visible = false;
			}
			
      #region Localise

      var localiseChangeOfficeLinkButton = (Literal)HeadLoginView.FindControl("LocaliseChangeOfficeLinkButton");
		  if (localiseChangeOfficeLinkButton.IsNotNull())
		    localiseChangeOfficeLinkButton.Text = UrlBuilder.EditLocalisationLink("Global.ChangeOffice.Text.NoEdit");

      var localiseAccountSettings = (Literal)HeadLoginView.FindControl("localiseAccountSettings");
      if (localiseAccountSettings.IsNotNull())
        localiseAccountSettings.Text = UrlBuilder.EditLocalisationLink("Global.AccountSettings.Text.NoEdit");

      var localiseSignOut = (Literal)HeadLoginView.FindControl("LocaliseSignOut");
      if (localiseSignOut.IsNotNull())
        localiseSignOut.Text = UrlBuilder.EditLocalisationLink("Global.LogOut.Text.NoEdit");

      var localiseGlobalLogin = (Literal)HeadLoginView.FindControl("LocaliseGlobalLogin");
      if (localiseGlobalLogin.IsNotNull())
        localiseGlobalLogin.Text = UrlBuilder.EditLocalisationLink("Global.LogIn.Text.NoEdit");

      var localisePrivacyAndSecurity = (Literal)FooterContent.FindControl("LocalisePrivacyAndSecurity");
      if (localisePrivacyAndSecurity.IsNotNull())
        localisePrivacyAndSecurity.Text = UrlBuilder.EditLocalisationLink("Global.PrivacyAndSecurity.Text.NoEdit");

      var localiseFAQ = (Literal)FooterContent.FindControl("LocaliseFAQ");
      if (localiseFAQ.IsNotNull())
        localiseFAQ.Text = UrlBuilder.EditLocalisationLink("Global.FrequentlyAskedQuestions.Text.NoEdit");

      var localiseTerms = (Literal)FooterContent.FindControl("LocaliseTerms");
      if (localiseTerms.IsNotNull())
        localiseTerms.Text = UrlBuilder.EditLocalisationLink("Global.TermsOfUse.Text.NoEdit");
        
      //if (LocaliseGlobalLogin.IsNotNull())
      //  LocaliseGlobalLogin.Text = UrlBuilder.EditLocalisationLink("Global.LogIn.Text.Url");

		  LocalisePanel.Visible = true;
		  var pageLocaliseControl = LocalisePanel.FindControl("Localise");
      if(pageLocaliseControl.IsNotNull()) pageLocaliseControl.Visible = true;
		  //if (pageLocaliseControl.IsNotNull())
      //pageLocaliseControl.Visible = Context.User.IsInRole(Constants.RoleKeys.SystemAdmin);

      #endregion
    }

    /// <summary>
    /// Handles the PreRender event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Page_PreRender(object sender, EventArgs e)
		{
      var script = string.Format("var SiteMaster_LoggedInUserName = '{0}', SiteMaster_IsShadowingUser = {1};", 
        (App.User.IsAuthenticated ? HttpUtility.JavaScriptStringEncode(App.User.FirstName) : string.Empty),
        App.User.IsShadowingUser.ToString().ToLowerInvariant()
      );

      Page.ClientScript.RegisterClientScriptBlock(GetType(), "SiteMaster_LoggedInUserName", script, true);

      if (App.GetOfficesForUser(App.User.PersonId).Count > 1 && App.Settings.OfficesEnabled)
      {
        var changeOfficeControl = (Panel)HeadLoginView.FindControl("ChangeOfficeControl");
        if (changeOfficeControl != null)
          changeOfficeControl.Visible = true;
      }

      if (App.User.IsAuthenticated)
      {
        var prefix = App.User.IsShadowingUser
          ? HtmlLocalise("LoggedInAs.Literal", "Logged in as")
          : HtmlLocalise("Hi.Literal", "Hi,");

        var label = (Literal)HeadLoginView.FindControl("LoginPrefix");
        if (label.IsNotNull())
          label.Text = prefix;

        label = (Literal)HeadLoginView.FindControl("LoginPrefix1");
        if (label.IsNotNull())
          label.Text = prefix;
      }
		}

	/// <summary>
		/// Hides the standard header.
		/// </summary>
		private void HideStandardHeader()
		{
			MainLogoImage.Visible = false;

			var usernameSpeech = (Panel)HeadLoginView.Controls[0].FindControl("speechbubblepanel");
			if (usernameSpeech != null)
			usernameSpeech.Visible = false;
		}


		public delegate void MasterOkCommandHandler(object sender, CommandEventArgs eventArgs);
    public event MasterOkCommandHandler MasterOkCommand;

		/// <summary>
		/// Handles the OkCommand event of the ConfirmationModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
		protected void ConfirmationModal_OkCommand(object sender, CommandEventArgs e)
		{
			OnMasterOkCommand(e);

		}

		/// <summary>
		/// Raises the <see cref="E:MasterOkCommand" /> event.
		/// </summary>
		/// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void OnMasterOkCommand(CommandEventArgs e)
    {
      if (MasterOkCommand != null)
      {
        MasterOkCommand(this, e);
      }
    }

		/// <summary>
		/// Gets the toolkit script manager.
		/// </summary>
		/// <returns></returns>
		protected override ToolkitScriptManager GetToolkitScriptManager()
		{
			return ScriptManagerMain;
		}

		/// <summary>
		/// Gets the module.
		/// </summary>
		/// <returns></returns>
		protected override FocusModules GetModule()
		{
			return App.Settings.Module;
		}

		/// <summary>
		/// Displays the error.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="closeLink">The close link.</param>
		/// <param name="height">The height.</param>
		/// <param name="width">The width.</param>
		/// <param name="top">The top.</param>
		protected override void DisplayError(string message = "", string closeLink = "", int height = 200, int width = 400, int top = 210)
		{
			if(message.IsNullOrEmpty())
				ErrorModal.Show(closeLink, height, width, top);
			else
				ErrorModal.Show(message, closeLink, height, width);
		}

		/// <summary>
		/// Displays the error.
		/// </summary>
		/// <param name="alertType">Type of the alert.</param>
		/// <param name="message">The message.</param>
		protected override void DisplayError(AlertTypes alertType, string message = "")
		{
			DisplayError(message, "", 20);
		}

		/// <summary>
		/// Displays the modal alert.
		/// </summary>
		/// <param name="alertType">Type of the alert.</param>
		/// <param name="message">The message.</param>
		/// <param name="title">The title.</param>
		/// <param name="closeLink">The close link.</param>
		/// <param name="width">The width.</param>
		protected override void DisplayModalAlert(AlertTypes alertType, string message, string title = "", string closeLink = "", int width = 400)
		{
			AlertModal.Show(alertType, message, title, closeLink, 20, width);
		}

    /// <summary>
    /// Displays the change office modal.
    /// </summary>
		protected override void ShowChangeOfficeModal(Object sender, EventArgs e)
    {
			ChangeOffice.Show();
    }
	}
}
