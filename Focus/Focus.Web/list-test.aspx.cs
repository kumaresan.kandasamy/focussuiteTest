﻿using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Web.Code.Controls.User;
using Framework.Core;

namespace Focus.Web
{
	public partial class list_test : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				// set the selectable values in the dropdown list
				DropdownClientList.SelectableItems = new List<UpdateableClientListItem>
			                                     {
				                                     new UpdateableClientListItem
																						 {
																							 Value = "1",
					                                     Label = "Option 1"
				                                     },
				                                     new UpdateableClientListItem
																						 {
																							 Value = "2",
					                                     Label = "Option 2"
				                                     },
																						 new UpdateableClientListItem
																						 {
																							 Value = "3",
					                                     Label = "Option 3"
				                                     },
																						 new UpdateableClientListItem
																						 {
																							 Value = "4",
					                                     Label = "Option 4"
				                                     },
																						 new UpdateableClientListItem
																						 {
																							 Value = "5",
					                                     Label = "Option 5"
				                                     },
																						 new UpdateableClientListItem
																						 {
																							 Value = "6",
					                                     Label = "Option 6"
				                                     },
																						 new UpdateableClientListItem
																						 {
																							 Value = "7",
					                                     Label = "Option 7"
				                                     },
																						 new UpdateableClientListItem
																						 {
																							 Value = "8",
					                                     Label = "Option 8"
				                                     },
																						 new UpdateableClientListItem
																						 {
																							 Value = "9",
					                                     Label = "Option 9"
				                                     },
																						 new UpdateableClientListItem
																						 {
																							 Value = "10",
					                                     Label = "Option 10"
				                                     }
			                                     };

				/*
				 * Pre-select some initial values for the lists
				 * If one of the pre-selected items is defined as the default selected item, set it's IsDefault property to true
				 */

				// when in textbox mode, each item supplied as a pre-selected item must have a Value and Label
				TextBoxClientList.SelectedItems = new List<UpdateableClientListItem>
			                             {
																		 new UpdateableClientListItem
																		 {
																			 Value = "Test 1",
																			 Label = "Test 1"
																		 },
																		 new UpdateableClientListItem
																		 {
																			 Value = "Test 2",
																			 Label = "Test 2"
																		 },
																		 new UpdateableClientListItem
																		 {
																			 Value = "Test 3",
																			 Label = "Test 3"
																		 }
			                             };

				/*	
				 * when in dropdownlist mode, each item supplied as a pre-selected item only requires a Value; 
				 * this is then matched in the JS with the corresponding item in the dropdownlist in order to set it's label 
				 * and also remove it from the dropdownlist (as if it's already selected, no point letting the user select it again!)
				 */
				DropdownClientList.SelectedItems = new List<UpdateableClientListItem>
			                              {
																			new UpdateableClientListItem
																			{
																				Value = "2"
																			},
																			new UpdateableClientListItem
																			{
																				Value = "5"
																			},
																			new UpdateableClientListItem
																			{
																				Value = "7"
																			}
			                              };

				// when in autocomplete mode, each item supplied as a pre-selected item must have a Value and Label
				AutoCompleteClientList.SelectedItems = new List<UpdateableClientListItem>
				                                       {
				                                         new UpdateableClientListItem
				                                         {
				                                           Value = "4841308",
				                                           Label = "FVN336 Assigned To Office 1"
				                                         }
				                                       };
			}
			
		}

		protected void PostbackButton_OnClick(object sender, EventArgs e)
		{
			PostbackResultsPanel.Visible = true;
			// get the selected items for each instance of the control and format them all pretty
			TextBoxPostbackList.Text = BuildPostbackDisplayValue(TextBoxClientList.SelectedItems);
			DropDownListPostbackList.Text = BuildPostbackDisplayValue(DropdownClientList.SelectedItems);
			AutoCompletePostbackList.Text = BuildPostbackDisplayValue(AutoCompleteClientList.SelectedItems);
		}

		private string BuildPostbackDisplayValue(ICollection<UpdateableClientListItem> selectedItems)
		{
			if (selectedItems.IsNullOrEmpty())
				return string.Empty;

			var retVal = selectedItems.Count + " items selected:<br/>";

			return selectedItems.Aggregate(retVal, (current, item) => current + string.Format("<em>Label: {0}, Value: {1}, Default: {2}</em><br/>", item.Label, item.Value, item.IsDefault));
		}

		protected void ClearButton_OnClick(object sender, EventArgs e)
		{
			TextBoxClientList.Clear();
			DropdownClientList.Clear();
			AutoCompleteClientList.Clear();
		}

		protected void ReinitButton_OnClick(object sender, EventArgs e)
		{
			TextBoxClientList.Clear();
			TextBoxClientList.SetSelectedItems(new List<UpdateableClientListItem>
			                                   {
				                                   new UpdateableClientListItem
				                                   {
					                                   Value = "Test 10",
																						 Label = "Test 10"
				                                   },
																					 new UpdateableClientListItem
				                                   {
					                                   Value = "Test 11",
																						 Label = "Test 11"
				                                   },
																					 new UpdateableClientListItem
				                                   {
					                                   Value = "Test 12",
																						 Label = "Test 12"
				                                   },
																					 new UpdateableClientListItem
				                                   {
					                                   Value = "Test 13",
																						 Label = "Test 13"
				                                   },
																					 new UpdateableClientListItem
				                                   {
					                                   Value = "Test 14",
																						 Label = "Test 14"
				                                   }
			                                   });
		}
	}
}