﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.Security;
using System.Web.UI.WebControls;
using Focus.Common.Code;
using Focus.Common.Models;
using Focus.Common;
using Focus.Core;
using Focus.Web.Code;
using Focus.Web.Code.Controllers;

#endregion

namespace Focus.Web
{
	public partial class Error : PageBase
	{
		/// <summary>
		/// Gets the page controller.
		/// </summary>
		/// <value>
		/// The page controller.
		/// </value>
		protected ErrorController PageController { get { return PageControllerBaseProperty as ErrorController; } }

		/// <summary>
		/// Registers the page controller.
		/// </summary>
		/// <returns></returns>
		protected override IPageController RegisterPageController()
		{
			return new ErrorController(App);
		}

		protected string ErrorInfo { get; private set; }
		protected string ErrorStack { get; private set; }
		protected string ErrorHeading { get { return CodeLocalise("ErrorType.Heading", "System  Error:"); } }

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{

			if (Page.RouteData.Values.ContainsKey("errorType"))
			{
				ErrorTypes errorType;
				Enum.TryParse(Page.RouteData.Values["errorType"].ToString(), out errorType);
                Page.Title = CodeLocalise("Page.Title", "Error" + Page.RouteData.Values["errorType"]);
				if (Handle(PageController.TryProcessError(errorType)))
				{
          HomePageLink.Visible = false;
          Heading.Visible = false;

				  return;
				}

				//switch (errorType)
				//{

				//  case ErrorTypes.UserNotEnabled:
				//    Confirmation.Show(CodeLocalise("UnexpectedHold.Title", "Unexpected hold"),
				//                      CodeLocalise("UnexpectedHold.Message","An unexpected hold has been placed on your account, indicating a problem our staff needs to discuss with you. Please contact #SUPPORTHELPTEAM# at #SUPPORTPHONE# immediately. #SUPPORTHOURSOFBUSINESS# We apologize for this inconvenience."),
				//                      CodeLocalise("Close.Button", "Close"),
				//                      "Redirect");
				//    return;
				//  case ErrorTypes.UserBlocked:
				//        Confirmation.Show(CodeLocalise("AssistUserBlocked.Title", "Account on hold"),
				//                          CodeLocalise("EmployerBlocked.Message","Your account has been placed on hold. Please contact Support at #SUPPORTPHONE# or <a href='mailto:#SUPPORTEMAIL#'>#SUPPORTEMAIL#</a> Our business hours are between #SUPPORTHOURSOFBUSINESS#."),
				//                          "OK",
				//                          "Redirect");
				//    return;
				//}		

			}
			ProcessException();
		}

    /// <summary>
    /// Overrides the on error event (to stop error page redirecting to itself continually)
    /// </summary>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected override void OnError(EventArgs e)
    {
      Response.Redirect(string.Concat("ErrorSafe.htm?", Guid.NewGuid().ToString().ToLower()), true);
    }

		/// <summary>
		/// Handles the Click event of the Redirect control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void Redirect_Click(object sender, CommandEventArgs e)
		{
			try
			{
				ServiceClientLocator.AuthenticationClient(App).LogOut();
			}
			finally
			{
				FormsAuthentication.SignOut();
        App.ClearSession();

				Response.Redirect(UrlBuilder.LogIn(), true);
			}
		}

		/// <summary>
		/// Processes the exception.
		/// </summary>
		private void ProcessException()
		{
		  ExceptionModel exception;
		  try
		  {
        exception = App.Exception;
		  }
		  catch (Exception ex)
		  {
		    exception = new ExceptionModel
		    {
		      Message = ex.Message,
          StackTrace = ex.StackTrace
		    };
		  }
			
			if (exception == null)
				ErrorInfo = CodeLocalise("ErrorType.Unknown", "Unknown error!");
			else
			{
				ErrorInfo = exception.Message;

				if (exception.StackTrace != null)
				{
#if DEBUG
					ErrorStack = exception.StackTrace.Replace("\n", "<br/>");
#else
					ErrorStack = "<!--\n\n" + exception.StackTrace + "\n\n-->";
#endif
				}
			}
		}
	}
}