﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AllPageElements.aspx.cs" Inherits="Focus.Web.AllPageElements" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
  
  <style type="text/css">
    /* Styling for horizontal scroller user in Reports */

    .sliderHeader .heading {color:#9b6e36;font-weight:bold;font-size:23px;}
    .sliderHeader .arrowBox {width:32px;height:29px;display:inline-block;position:relative;top:6px;-webkit-border-top-right-radius:7px;border-top-right-radius:7px;-moz-border-radius-topright:7px;
    -webkit-border-top-left-radius:7px;border-top-left-radius:7px;-moz-border-radius-topleft:7px;background:#fff6e5 url(Assets/Images/brown-down-arrow.png) center center no-repeat;}

    .sliderWrap {background:#fff6e5;width: 776px;height:107px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;padding:16px;}
    .sliderBar {position:absolute;background:#ffe4b5;width:776px;height:5px;margin-top:124px;margin-left:16px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;}

    .scroll-pane { overflow: auto; width: 776px; float:left;background:none;border:none;}
    .scroll-content { width: 1420px; float: left; }
    .scroll-content-item { width: 125px; height: 93px; float: left; font-size: 11px;padding:0 32px 16px 0;background:none;border:none;}
    .scroll-content-item a {text-decoration:none;}
    .scroll-content-item  .content {display:block;padding-top:12px;font-weight:normal;}
    .scroll-bar-wrap { clear: left; padding: 0 4px 0 2px; margin: 0 -1px -1px -1px; background:#fff6e5;border:none;}
    .scroll-bar-wrap .ui-slider { background: none; border:0; height: 12px; margin: 0 auto;  }
    .scroll-bar-wrap .ui-handle-helper-parent { position: relative; width: 100%; height: 100%; margin: 0 auto; }
    .scroll-bar-wrap .ui-slider-handle { height: 12px;background:#e4cda4;border:none;}
    .scroll-bar-wrap .ui-slider-handle .ui-icon { margin: -8px auto 0; position: relative; top: 50%; display:none;}
  </style>
  
  <script>
    $(function () {
      //scrollpane parts
      var scrollPane = $(".scroll-pane"),
      scrollContent = $(".scroll-content");

      //build slider
      var scrollbar = $(".scroll-bar").slider({
        slide: function (event, ui) {
          if (scrollContent.width() > scrollPane.width()) {
            scrollContent.css("margin-left", Math.round(
            ui.value / 100 * (scrollPane.width() - scrollContent.width())
          ) + "px");
          } else {
            scrollContent.css("margin-left", 0);
          }
        }
      });

      //append icon to handle
      var handleHelper = scrollbar.find(".ui-slider-handle")
    .mousedown(function () {
      scrollbar.width(handleHelper.width());
    })
    .mouseup(function () {
      scrollbar.width("100%");
    })
    .append("<span class='ui-icon ui-icon-grip-dotted-vertical'></span>")
    .wrap("<div class='ui-handle-helper-parent'></div>").parent();

      //change overflow to hidden now that slider handles the scrolling
      scrollPane.css("overflow", "hidden");

      //size scrollbar and handle proportionally to scroll distance
      function sizeScrollbar() {
        var remainder = scrollContent.width() - scrollPane.width();
        var proportion = remainder / scrollContent.width();
        var handleSize = 82;
        scrollbar.find(".ui-slider-handle").css({
          width: handleSize,
          "margin-left": -handleSize / 2
        });
        handleHelper.width("").width(scrollbar.width() - handleSize);
      }

      //reset slider value based on scroll content position
      function resetValue() {
        var remainder = scrollPane.width() - scrollContent.width();
        var leftVal = scrollContent.css("margin-left") === "auto" ? 0 :
        parseInt(scrollContent.css("margin-left"));
        var percentage = Math.round(leftVal / remainder * 100);
        scrollbar.slider("value", percentage);
      }

      //if the slider is 100% and window gets larger, reveal content
      function reflowContent() {
        var showing = scrollContent.width() + parseInt(scrollContent.css("margin-left"), 10);
        var gap = scrollPane.width() - showing;
        if (gap > 0) {
          scrollContent.css("margin-left", parseInt(scrollContent.css("margin-left"), 10) + gap);
        }
      }

      //change handle position on window resize
      $(window).resize(function () {
        resetValue();
        sizeScrollbar();
        reflowContent();
      });
      //init scrollbar size
      setTimeout(sizeScrollbar, 10); //safari wants a timeout
    });
  </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<style type="text/css">
  #allPageElements tr  td {padding:4px;}
</style>

<table id="allPageElements" class="FocusCareer">
  <tr>
    <td>Body text</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <a href="#">hyperlink</a></td>
  </tr>

  <tr>
    <td>Heading 1</td>
    <td><h1>Lorem ipsum</h1></td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Bulletted list</td>
    <td><ul>
                  <li><a href="#">Skill 1</a></li>
                  <li><a href="#">Skill 2</a></li>
                  <li><a href="#">Skill 3</a></li>
                </ul>
      </td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Page Nav</td>
    <td><p class="pageNav"><strong>Dashboard</strong> | <a href="#">Post a new job</a></p></td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Progress bar</td>
    <td>
      <table class="progressBar" style="width:900px;">
    <tbody><tr>
        <td><span class="visited">Account Setup</span></td>
        <td><span class="active">Corporate Information</span></td>
        <td><span>Contact Information</span></td>
        <td><span class="last">Terms of Use</span></td>
    </tr>
</tbody></table>
    </td>
  </tr>
  <tr>
    <td style="vertical-align:top;"></td>
    <td>
      <table class="accordionTable">
    <tbody><tr class="multipleAccordionTitle on">
        <td><span class="accordionArrow"></span>Update my contact information <span class="instructions">*required fields</span></td>
    </tr>
    <tr class="accordionContent open" style="display: none; ">
        <td>
            <table width="100%">
                <tbody><tr>
                    <td width="50%">Location(s) where employee will report to work*</td>
                    <td width="50%"><input name="ctl00$MainContent$ctl00" type="text"> <input type="submit" name="ctl00$MainContent$Button1" value="Add" id="MainContent_Button1" class="button3"> <input type="submit" name="ctl00$MainContent$Button2" value="Look up zip" id="MainContent_Button2" class="button3"></td>
                </tr>
                <tr>
                    <td style="vertical-align:top;">
                        <select name="ctl00$MainContent$DropDownList3" id="MainContent_DropDownList3" style="width:250px;">
	<option value="All open jobs">All open jobs</option>

</select>
                    </td>
                    <td style="vertical-align:top;">Example: Trenton NJ<br>Example: 08647</td>
                </tr>
                <tr>
                    <td colspan="2"><br></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <table width="400" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <th class="focusTable">Location</th>
                                    <th class="focusTable">Public transit accessible</th>
                                    <th class="focusTable">Delete</th>
                                </tr>
                                <tr>
                                    <td>Cityname, NJ 02184</td>
                                    <td><input id="MainContent_ctl01" type="checkbox" name="ctl00$MainContent$ctl01"></td>
                                    <td><img src="Assets/Images/button_x_delete_off.png" width="16" height="15" alt="[close]" onkeypress="alert('deletableListItem1')" onclick="alert('deletableListItem1')"></td>
                                </tr>
                                <tr>
                                    <td>Cityname, NJ 02184</td>
                                    <td><input id="MainContent_CheckBox1" type="checkbox" name="ctl00$MainContent$CheckBox1"></td>
                                    <td><img src="Assets/Images/button_x_delete_off.png" width="16" height="15" alt="[close]" onkeypress="alert('deletableListItem1') onclick="alert('deletableListItem1')"></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
    <tr class="multipleAccordionTitle on">
        <td><span class="accordionArrow"></span>Interest in work opportunities credit hires</td>
    </tr>
    <tr class="accordionContent open" style="display: none; ">
        <td>Lorem ipsum</td>
    </tr>

</tbody></table>
    </td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Tooltip</td>
    <td>
      <div style="height:100px;">
                            <div class="tooltipWithArrow helpMessage">
                                <div class="tooltipImage"></div> 
                                <div class="tooltipPopup" style="display: block !important; ">
                                    <div class="tooltipPopupInner" style="bottom:auto !important;right:auto !important; ">
                                    <div class="arrow" style="top: 16.5px; "></div>
                                    nnerThis is an extra long tooltip with arrow. This is an extra lonrow. This is an extra long tooltip with arrow. 
                                    </div>
                                </div> 
                            </div>
                        </div>
    </td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Accordion list</td>
    <td>
      <div style="width:400px;">
<ul class="accordionList">
    <li class="multipleAccordionTitle2 on"><span class="plusMinusImage"></span>Keywords</li>
    <li class="accordionContent2 open" style="display: list-item; "><span class="plusMinusImage"></span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam feugiat est egestas eros dictum sed vulputate diam fringilla. Suspendisse mi mi, sodales et lacin</li>
    <li class="multipleAccordionTitle2"><span class="plusMinusImage"></span>Statements</li>
    <li class="accordionContent2" style="display: none; "><span class="plusMinusImage"></span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam feugiat est egestas eros dictum sed vulputate diam fringilla. Suspendisse mi mi, sodales et lacin</li>
</ul>
</div>
    </td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Dropdown list</td>
    <td><select name="ctl00$MainContent$DropDownList1" id="MainContent_DropDownList1" class="highlightedSelectBox" style="width:200px;">
	<option value="Full resume">Full resume</option>

</select></td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Instructional text</td>
    <td><i>* required fields</i></td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Button style 1</td>
    <td><input type="submit" name="button1" value="Post a new job" class="button1"></td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Button style 2</td>
    <td><input type="submit" name="button2" value="Save to draft/Move to next step" class="button2"></td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Button style 3</td>
    <td><input type="submit" name="button3" value="Preview" class="button3"></td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Button style 4</td>
    <td><input type="submit" name="button4" value="Cancel" class="button4"></td>
  </tr>
  <tr>
    <td style="vertical-align:top;"></td>
    <td>
      <ul class="navTab localNavTab">
	<li class="active"><a href="#">All Resumes</a></li>
	
</ul>
<div id="PoolResultUpdatePanel">
	
		<div id="tabbedContent">
			<div style="width:100%;">
				
			</div>
			
					<p>
						<i>Find a job description that is similar to yours. Select it below and click "Add..."
						to copy it into your job description.</i>
					</p>
					<a href="#" class="button7"><strong>I have a job description:</strong> click to add
					your job description and continue</a> <a href="#" class="button7"><strong>I have a
					job description:</strong> click to add your job description and continue</a>
					<a href="#" class="button7"><strong>I have a job description:</strong> click to add
					your job description and continue</a>
					<div class="button7">
						<strong>Select a saved search:</strong>
						<select name="ctl00$MainContent$Result$SearchResultListView$ctrl0$DropDownList9" id="MainContent_Result_SearchResultListView_DropDownList9">
		<option value="Name of saved search">Name of saved search</option>

	</select>
						<input type="submit" name="ctl00$MainContent$Result$SearchResultListView$ctrl0$Button5" value="Go" id="MainContent_Result_SearchResultListView_Button5" class="button3">
					</div>
				
		
			
		</div>
			
		<input type="hidden" name="ctl00$MainContent$Result$CandidatesLikeThisModalDummyTarget" id="MainContent_Result_CandidatesLikeThisModalDummyTarget">

		

		<div id="MainContent_Result_CandidatesLikeThisModal" class="modal" style="display: none; position: fixed; z-index: 100001; ">
		
			<div style="width:100%">
				<div class="right">
					<input type="image" name="ctl00$MainContent$Result$CandidatesLikeThisModalCloseButton" id="MainContent_Result_CandidatesLikeThisModalCloseButton" src="~/Assets/Images/button_x_close_off.png" alt="Close">
				</div>
				<div class="left">
					<strong>More candidates like</strong>
				</div>
			</div>
				
				 [TODO] - No candidates found! 
				

			
		
	</div>

		<input type="hidden" name="ctl00$MainContent$Result$PoolAction$ModalDummyTarget" id="MainContent_Result_PoolAction_ModalDummyTarget">


<div id="MainContent_Result_PoolAction_ModalPanel" class="modal" style="display: none; position: fixed; z-index: 100001; ">
		
	<table width="350px">
		<tbody><tr>
			<th style="vertical-align:top;"></th>
		</tr>
		<tr>
			<td>
				<p><strong>Contact job seeker directly by confidential email</strong></p>
				<div id="MainContent_Result_PoolAction_EmailActionUpdatePanel">
			
						<p>
							</p><div id="MainContent_Result_PoolAction_JobSpecifiedControls">
				
								Message type
								<select name="ctl00$MainContent$Result$PoolAction$MessageTypeDropDown" onchange="javascript:setTimeout('__doPostBack(\'ctl00$MainContent$Result$PoolAction$MessageTypeDropDown\',\'\')', 0)" id="MessageTypeDropDown">

				</select>	
							
			</div>
					
							<div id="MainContent_Result_PoolAction_JobNotSpecifiedControls">
				
								<table>
									<tbody><tr>
										<td>Company name</td>
										<td><select name="ctl00$MainContent$Result$PoolAction$CompanyNameDropDown" onchange="javascript:setTimeout('__doPostBack(\'ctl00$MainContent$Result$PoolAction$CompanyNameDropDown\',\'\')', 0)" id="CompanyNameDropDown">

				</select></td>
									</tr>
									<tr>
										<td>Job title</td>
										<td><select name="ctl00$MainContent$Result$PoolAction$JobTitleDropDown" onchange="javascript:setTimeout('__doPostBack(\'ctl00$MainContent$Result$PoolAction$JobTitleDropDown\',\'\')', 0)" id="JobTitleDropDown">

				</select></td>		
									</tr>
									<tr>
										<td>Contact method</td>
										<td><select name="ctl00$MainContent$Result$PoolAction$ContactMethodDropDown" onchange="javascript:setTimeout('__doPostBack(\'ctl00$MainContent$Result$PoolAction$ContactMethodDropDown\',\'\')', 0)" id="ContactMethodDropDown">

				</select></td>		
									</tr>					
								</tbody></table>
							
			</div>
						<p></p>
						<p><textarea name="ctl00$MainContent$Result$PoolAction$EmailBodyTextBox" rows="10" cols="20" readonly="readonly" id="EmailBodyTextBox"></textarea></p>
						<span id="MainContent_Result_PoolAction_EmailBodyRequired" class="error" style="display:none;"></span>
						<input type="hidden" name="ctl00$MainContent$Result$PoolAction$EmailBodyRequiredCallout_ClientState" id="MainContent_Result_PoolAction_EmailBodyRequiredCallout_ClientState">
					
		</div>
			</td>
		</tr>
		<tr>
			<td align="left">
				<input type="submit" name="ctl00$MainContent$Result$PoolAction$OkButton" value="" onkeypress="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$MainContent$Result$PoolAction$OkButton&quot;, &quot;&quot;, true, &quot;PoolActionModal&quot;, &quot;&quot;, false, false))" 
				onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$MainContent$Result$PoolAction$OkButton&quot;, &quot;&quot;, true, &quot;PoolActionModal&quot;, &quot;&quot;, false, false))" id="MainContent_Result_PoolAction_OkButton" class="button3">
				  
				<input type="submit" name="ctl00$MainContent$Result$PoolAction$CloseButton" value="" id="MainContent_Result_PoolAction_CloseButton" class="button4">
			</td>
		</tr>
	</tbody></table>

	</div>

<input type="hidden" name="ctl00$MainContent$Result$PoolAction$Confirmation$ModalDummyTarget" id="MainContent_Result_PoolAction_Confirmation_ModalDummyTarget">


<div id="MainContent_Result_PoolAction_Confirmation_ModalPanel" class="modal" style="display: none; position: fixed; z-index: 100001; ">
		
	<table width="300px">
		<tbody><tr>
			<th style="vertical-align:top;"></th>
		</tr>
		<tr>
			<td style="vertical-align:top;" height=""></td>
		</tr>
		<tr>
			<td align="left">
				<input type="submit" name="ctl00$MainContent$Result$PoolAction$Confirmation$OkButton" value="" id="MainContent_Result_PoolAction_Confirmation_OkButton" class="button1">
				  
				<input type="submit" name="ctl00$MainContent$Result$PoolAction$Confirmation$CloseButton" value="" id="MainContent_Result_PoolAction_Confirmation_CloseButton" class="button1">
			</td>
		</tr>
	</tbody></table>

	</div>

	
<div id="CandidatesLikeThisModalPopup_backgroundElement" style="display: none; position: fixed; left: 0px; top: 0px; z-index: 10000; " class="modalBackground"></div><div id="MainContent_Result_PoolAction_ModalPopup_backgroundElement" style="display: none; position: fixed; left: 0px; top: 0px; z-index: 10000; " class="modalBackground"></div><div id="MainContent_Result_PoolAction_Confirmation_ModalPopup_backgroundElement" style="display: none; position: fixed; left: 0px; top: 0px; z-index: 10000; " class="modalBackground"></div></div>
    </td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Bar chart</td>
    <td><div id="barChart" style="width:800px;height:350px;"></div></td>
  </tr>
  <tr>
    <td style="vertical-align:top;">System message</td>
    <td><p class="messageSystem"><img src="Assets/Images/button_x_close_off.png" width="16" height="15" alt="[close]" onkeypress="alert('messageSystem1')" onclick="alert('messageSystem1')">On
          July 5, Focus/Talent will be unavailable due to planned maintenance.</p></td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Message status</td>
    <td><p class="messageStatus"><img src="Assets/Images/button_x_close_off.png" width="16" height="15" alt="[close]" onkeypress="alert('messageStatus1')" onclick="alert('messageStatus1')">Job
          posting for System Engineer – Level 1 expires in 10 days.
        </p></td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Pager</td>
    <td><table class="Pager">
              <tbody><tr>
                <td>107 RESULTS</td>
                <td><a href="#"><img id="PagerStart" src="Assets/Images/pager_start.png" alt="Pager Start" width="9" height="9"></a></td>
                <td><a href="#"><img id="PagerBack" src="Assets/Images/pager_back.png" alt="Pager Back" width="6" height="9"></a></td>
                <td>PAGE</td>
                <td><input name="ctl00$MainContent$PageNumber" type="text" maxlength="2" id="PageNumber" tabindex="4" style="width:20px;"></td>
                <td>of 5</td>
                <td><a href="#"><img id="PagerNext" src="Assets/Images/pager_next.png" alt="Pager Next" width="6" height="9"></a></td>
                <td><a href="#"><img id="PagerEnd" src="Assets/Images/pager_end.png" alt="Pager End" width="9" height="9"></a></td>
                <td style="padding-left:15px">
                  <select name="ctl00$MainContent$ctl00">
	<option value="25">25</option>

</select>
                </td>
                <td>RESULTS PER PAGE</td>
              </tr>
            </tbody></table></td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Report grid</td>
    <td>
      <table width="100%" class="ReportGrid">
          <thead>
            <tr>
              <th width="25%">
                NAME
                <img id="Col1Up" class="UpArrow" src="Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6">
                <img id="Col1Down" class="DownArrow" src="Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6">
              </th>
              <th width="15%">
              EMAIL
              <img id="Img1" class="UpArrow" src="Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6">
              <img id="Img2" class="DownArrow" src="Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6">
              </th>
              <th width="15%">SPECIAL<br>STATUS</th>
              <th width="15%">SEEKER CITY</th>
              <th width="15%">WEEKS SINCE<br>SACCOUNT CREATION</th>
              <th width="15%">LAST TITLE/<br>SEMPLOYER</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><input id="Checkbox1" type="checkbox" name="ctl00$MainContent$ctl01"><label for="MainContent_ctl01"><a href="#">Lastname, Firstname</a></label></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td><input id="Checkbox2" type="checkbox" name="ctl00$MainContent$ctl01"><label for="MainContent_ctl01"><a href="#">Lastname, Firstname</a></label></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
    </td>
  </tr>

  <tr>
    <td style="vertical-align:top;"></td>
    <td></td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Modal popup</td>
    <td>
      <div id="loginModal" class="modal simplemodal-data" style="display: block; width:400px;">
	

    <table width="100%">
        <tbody><tr>
            <td><h1>Welcome to Focus/Assist!</h1></td>
            <td><img src="Assets/images/button_x_close_off.png" onkeypress="alert('close')" onclick="alert('close')" width="16" height="15" alt="" style="float:right;position:relative;left:13px;bottom:18px;"></td>
        </tr>
        <tr>
            <td colspan="2">Use your dashboard to keep an eye on important reports.</td>
        </tr>
    </tbody></table>

</div>
    </td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Deletable list item table</td>
    <td>
      <table class="deletableListItemTable">
                                        <tbody>
                                            <tr>
                                                <td class="column1">Kentucky</td>
                                                <td class="column2"><img src="Assets/Images/button_x_delete_off.png" width="16" height="15" alt="[close]" onkeypress="alert('deletableListItem1')" onclick="alert('deletableListItem1')"></td>
                                            </tr>
                                            <tr>
                                                <td class="column1">Ohio</td>
                                                <td class="column2"><img src="Assets/Images/button_x_delete_off.png" width="16" height="15" alt="[close]" onkeypress="alert('deletableListItem2') onclick="alert('deletableListItem2')"></td>
                                            </tr>
                                            <tr>
                                                <td class="column1">Ohio</td>
                                                <td class="column2"><img src="Assets/Images/button_x_delete_off.png" width="16" height="15" alt="[close]" onkeypress="alert('deletableListItem2')" onclick="alert('deletableListItem2')"></td>
                                            </tr>
                                        </tbody>
                                    </table>
    </td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Saved reports</td>
    <td>
      <div class="SavedReport">
  <a id="MainContent_DashboardReports_ctl00_reportLink" href="/reporting/savedreport/1112371" tabindex="0">
	  <img id="MainContent_DashboardReports_ctl00_reportImage" alt="Report Image" class="Thumbnail" src="/Assets/Images/SavedReportChart.png" style="height:72px;width:133px;">
  </a>
  
  <span class="Date"><span id="MainContent_DashboardReports_ctl00_reportDate">09/08/2012</span></span>
  <span class="Caption"><span id="MainContent_DashboardReports_ctl00_reportCaption">Lee Test</span></span>
</div>
<div class="SavedReport">
  <a id="A1" href="/reporting/savedreport/1112371" tabindex="0"><img id="Img3" alt="Saved Report Chart" class="Thumbnail" src="/Assets/Images/SavedReportChart.png" style="height:72px;width:133px;"></a>
  
  <span class="Date"><span id="Span1">09/08/2012</span></span>
  <span class="Caption"><span id="Span2">Lee Test</span></span>
</div>
    </td>
  </tr>
  
    
  <tr>
    <td style="vertical-align:top;">Reports slider - css and jQuery is all in the header</td>
    <td>
		<div class="sliderHeader">
			<span class="heading">Reports</span>
			<span class="arrowBox"></span>
		</div>
		<div class="sliderBar"></div>
		<div class="sliderWrap">
			
			<div class="scroll-pane ui-widget ui-widget-header ui-corner-all">
			  <div class="scroll-content">
				<div class="scroll-content-item ui-widget-header">
						<a href="#">
							<img src="Assets/Images/SavedReportGrid.png" width="80" height="58" alt="Saved Report Grid Image" />
							<span class="content">Report title goes here and maybe long</span>
						</a>
				</div>
				<div class="scroll-content-item ui-widget-header">
						<a href="#">
							<img src="Assets/Images/SavedReportGrid.png" width="80" height="58" alt="Saved Report Grid Image" />
							<span class="content">Report title goes here and maybe long</span>
						</a>
				</div>
				<div class="scroll-content-item ui-widget-header">
						<a href="#">
							<img src="Assets/Images/SavedReportGrid.png" width="80" height="58" alt="Saved Report Grid Image" />
							<span class="content">Report title goes here and maybe long</span>
						</a>
				</div>
				<div class="scroll-content-item ui-widget-header">
						<a href="#">
							<img src="Assets/Images/SavedReportGrid.png" width="80" height="58" alt="Saved Report Grid Image" />
							<span class="content">Report title goes here and maybe long</span>
						</a>
				</div>
				<div class="scroll-content-item ui-widget-header">
						<a href="#">
							<img src="Assets/Images/SavedReportGrid.png" width="80" height="58" alt="Saved Report Grid Image" />
							<span class="content">Report title goes here and maybe long</span>
						</a>
				</div>
				<div class="scroll-content-item ui-widget-header">
						<a href="#">
							<img src="Assets/Images/SavedReportGrid.png" width="80" height="58" alt="Saved Report Grid Image" />
							<span class="content">Report title goes here and maybe long</span>
						</a>
				</div>
				<div class="scroll-content-item ui-widget-header">
						<a href="#">
							<img src="Assets/Images/SavedReportGrid.png" width="80" height="58" alt="Saved Report Grid Image" />
							<span class="content">Report title goes here and maybe long</span>
						</a>
				</div>
				<div class="scroll-content-item ui-widget-header">
						<a href="#">
							<img src="Assets/Images/SavedReportGrid.png" width="80" height="58" alt="Saved Report Grid Image" />
							<span class="content">Report title goes here and maybe long</span>
						</a>
				</div>
				<div class="scroll-content-item ui-widget-header">
						<a href="#">
							<img src="Assets/Images/SavedReportGrid.png" width="80" height="58" alt="Saved Report Grid Image" />
							<span class="content">Report title goes here and maybe long</span>
						</a>
				</div>
			  </div>
			  <div class="scroll-bar-wrap ui-widget-content ui-corner-bottom">
				<div class="scroll-bar"></div>
			  </div>
			</div>
		</div>
	</td>
  </tr>


  <tr>
    <td style="vertical-align:top;"></td>
    <td></td>
  </tr>

</table>

<script class="code" type="text/javascript">
  $(document).ready(function () {
    //JQ Plot
    xAxisMinValue = 0; // X Axis min value.
    yAxisMaxValue = 800; // Y Axis min value.

    // Y Axis labels.
    var yAxisLabels = [
	  'Bar label goes<br />in this spot',
	  'Bar label 2 goes<br />in this spot',
	  'Bar label 3 goes<br />in this spot',
	  'Bar label 4 goes<br />iin this spot',
	  'Bar label 5 goes<br />iin this spot',
	  'Bar label 6 goes<br />in this spot'];

    var plot = $.jqplot('barChart', [
    // Y Axis values go here in reverse order as it is a horizontal bar chart.
        [[133, 1], [343, 2], [425, 3], [513, 4], [524, 5], [790, 6]]], {
          seriesDefaults: {
            shadow: false,
            renderer: $.jqplot.BarRenderer,
            pointLabels: { show: true, location: 'e', edgeTolerance: -55 },
            rendererOptions: {
              barDirection: 'horizontal',
              barMargin: 15,
              highlightMouseOver: false
            }
          },
          axesDefaults: {

          },
          axes: {
            xaxis: {
              tickOptions: {
                show: true,
                mark: 'cross'
              },
              min: xAxisMinValue,
              max: yAxisMaxValue,
              showTickMarks: true
            },
            yaxis: {
              renderer: $.jqplot.CategoryAxisRenderer,
              ticks: yAxisLabels,
              tickOptions: {
                showMark: false
              }
            }
          },
          grid: {
            drawGridLines: false,
            gridLineColor: '#ffffff', /**/
            borderColor: '#509790',
            shadowWidth: 0,
            borderWidth: 0,
            shadow: false
          },
          series: [{ color: '#226C67'}]
        });
  });
</script>

<!-- End example scripts -->

<!-- Don't touch this! -->


    <script class="include" type="text/javascript" src="Assets/Scripts/jquery.jqplot.min.js"></script>
<!-- End Don't touch this! -->

<!-- Additional plugins go here -->

    <script class="include" type="text/javascript" src="Assets/Scripts/jqplot.barRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="Assets/Scripts/jqplot.categoryAxisRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="Assets/Scripts/jqplot.pointLabels.min.js"></script>

<!-- End additional plugins -->

</asp:Content>
