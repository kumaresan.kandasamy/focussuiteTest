﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Focus.Web.WebReporting.Default" %>
<%@ Register Src="Controls/ReportSelector.ascx" tagName="ReportSelector" tagPrefix="uc" %>
<%@ Register src="Controls/CriteriaSelector.ascx" tagName="CriteriaSelector" tagPrefix="uc" %>
<%@ Register src="~/Code/Controls/User/Reporting/SavedReports.ascx" tagName="SavedReports" tagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="TabNavigation" Src="~/WebAssist/Controls/TabNavigation.ascx" %>
<%@ Register tagPrefix="uc" tagName="ReportHeader" src="Controls/ReportHeader.ascx" %>
<%@ Register src="Controls/ReportCriteriaDisplay.ascx" tagName="ReportCriteriaDisplay" tagPrefix="uc" %>
<%@ Register src="Controls/ReportDisplay.ascx" tagName="ReportDisplay" tagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="<%# ResolveUrl("~/Assets/Scripts/Focus.Reporting.js") %>" type="text/javascript"></script>
		<script src="<%# ResolveUrl("~/Assets/Scripts/UpdateableList.js") %>" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <uc:tabnavigation id="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
		<h1 class="sr-only">Reporting</h1>
    <uc:ReportSelector runat="server" ID="ReportSelector" OnReportSelected="ReportSelector_ReportSelected" />
    <uc:SavedReports runat="server" ID="SavedReports" AllowDelete="True" />
    <uc:ReportHeader runat="server" ID="ReportHeader" Visible="False" OnActionRequested="ReportHeader_ActionRequested"/>
    <uc:ReportCriteriaDisplay runat="server" ID="CriteriaDisplay" Visible="False" />
    <uc:CriteriaSelector runat="server" ID="CriteriaSelector" OnRunReportClicked="CriteriaSelector_RunReportClicked" />
    <uc:ReportDisplay runat="server" ID="ReportDisplay" Visible="False" OnReportCriteriaChanged="ReportDisplay_CriteriaChanged" />
    
    
    

</asp:Content>
