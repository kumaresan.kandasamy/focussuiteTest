﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Security.Permissions;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Report;

#endregion

namespace Focus.Web.WebReporting
{
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistUser)] 
  public partial class Default : PageBase
	{

    public IReportCriteria ReportCriteria
    {
      get { return GetViewStateValue<IReportCriteria>("ReportingDefault:ReportCriteria"); }
      set { SetViewStateValue("ReportingDefault:ReportCriteria", value); }
    }
    public ReportType CurrentReportType
    {
      get { return GetViewStateValue<ReportType>("ReportingDefault:CurrentReportType"); }
      set { SetViewStateValue("ReportingDefault:CurrentReportType", value); }
    }

    /// <summary>
    /// Fires when the page is loaded
    /// </summary>
    /// <param name="sender">Page raising the event</param>
    /// <param name="e">Standard event argument</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      Page.Title = CodeLocalise("PageTitle", "Reporting");

      if (!App.Settings.ReportingEnabled)
        Response.RedirectToRoute("default");
    }

    /// <summary>
    /// Fires when the page about to be rendered
    /// </summary>
    /// <param name="sender">Page raising the event</param>
    /// <param name="e">Standard event argument</param>
    protected void Page_PreRender(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        SetReturnUrl();

        if (Page.RouteData.Values.ContainsKey("report"))
        {
          CurrentReportType = (ReportType) Enum.Parse(typeof (ReportType), Page.RouteData.Values["report"].ToString(), true);
          if (Page.RouteData.Values.ContainsKey("session"))
          {
            var reportId = long.Parse(Page.RouteData.Values["session"].ToString());
            ReportCriteria = ServiceClientLocator.ReportClient(App).GetSavedReportFromSession(reportId);
            RunReport();
          }
          else
          {
            NewReport();
          }
        }
        else if (Page.RouteData.Values.ContainsKey("savedreportid"))
        {
          var reportId = long.Parse(Page.RouteData.Values["savedreportid"].ToString());
          ReportCriteria = ServiceClientLocator.ReportClient(App).GetSavedReport(reportId);
          CurrentReportType = ReportCriteria.ReportType;

	        var hasPermission = false;

					switch (CurrentReportType)
					{
						case ReportType.Employer:
              hasPermission = App.User.IsInAnyRole(Constants.RoleKeys.AssistEmployerReports, Constants.RoleKeys.AssistEmployerReportsViewOnly);
							break;

						case ReportType.JobSeeker:
              hasPermission = App.User.IsInAnyRole(Constants.RoleKeys.AssistJobSeekerReports, Constants.RoleKeys.AssistJobSeekerReportsViewOnly);
							break;

						case ReportType.JobOrder:
              hasPermission = App.User.IsInAnyRole(Constants.RoleKeys.AssistJobOrderReports, Constants.RoleKeys.AssistJobOrderReportsViewOnly);
							break;

						case ReportType.SupplyDemand:
							hasPermission = true;
							break;
					}

					ReportHeader.DisplayReportSettingsLink(hasPermission);
          RunReport();
        }
      }
    }

    #region visiblity

    /// <summary>
    /// Sets the visibility for reports.
    /// </summary>
    private void SetVisibilityForReports()
    {
      SavedReports.Visible = ReportSelector.Visible = CriteriaSelector.Visible = false;
      ReportHeader.Visible = CriteriaDisplay.Visible = ReportDisplay.Visible = true;
    }


    #endregion

    #region events

    /// <summary>
    /// Handles the ReportSelected event of the ReportSelector control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void ReportSelector_ReportSelected(object sender, CommandEventArgs e)
    {
      CurrentReportType = (ReportType) Convert.ToInt32(e.CommandArgument);
      
      NewReport();
    }

    /// <summary>
    /// Handles the RunReportClicked event of the CriteriaSelector control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    /// <exception cref="System.Exception"></exception>
    protected void CriteriaSelector_RunReportClicked(object sender, CommandEventArgs e)
    {
      ReportType report;
      if (!Enum.TryParse(e.CommandName, out report))
        throw new Exception(e.CommandName + "is not recognised as a valid report type.");

      ReportCriteria = (IReportCriteria)e.CommandArgument;

      RunReport();
    }


    protected void ReportHeader_ActionRequested(object sender, CommandEventArgs eventargs)
    {
      CriteriaSelector.Show(ReportCriteria);
    }

    protected void ReportDisplay_CriteriaChanged(object sender, CommandEventArgs eventargs)
    {
      ReportCriteria = (IReportCriteria) eventargs.CommandArgument;
      CurrentReportType = ReportCriteria.ReportType;
    }

    #endregion

    #region Shared

    /// <summary>
    /// Begins a new instance of the report
    /// </summary>
    private void NewReport()
    {
      switch (CurrentReportType)
      {
        case ReportType.JobSeeker:
          ReportCriteria = new JobSeekersReportCriteria
          {
            KeywordInfo = new KeywordReportCriteria
            {
              KeyWordSearch = new List<KeyWord?> { KeyWord.FullResume }
            }
          };
          break;
        case ReportType.JobOrder:
          ReportCriteria = new JobOrdersReportCriteria
          {
            KeywordInfo = new KeywordReportCriteria
            {
              KeyWordSearch = new List<KeyWord?> { KeyWord.FullJob }
            }
          };
          break;
        case ReportType.Employer:
          ReportCriteria = new EmployersReportCriteria
          {
            KeywordInfo = new KeywordReportCriteria
            {
              KeyWordSearch = new List<KeyWord?> { KeyWord.FullJob }
            }
          };
          break;
        case ReportType.SupplyDemand:
          ReportCriteria = new SupplyDemandReportCriteria
          {
            KeywordInfo = new KeywordReportCriteria
            {
              KeyWordSearch = new List<KeyWord?> { KeyWord.FullJob }
            }
          };
          break;
      }

      CriteriaSelector.Show(ReportCriteria);
    }

    /// <summary>
    /// Runs a report
    /// </summary>
    private void RunReport()
    {
      SetVisibilityForReports();
      CriteriaDisplay.ShowCriteria(ReportCriteria);
      ReportDisplay.ShowReport(ReportCriteria);
    }

    #endregion
  }
}