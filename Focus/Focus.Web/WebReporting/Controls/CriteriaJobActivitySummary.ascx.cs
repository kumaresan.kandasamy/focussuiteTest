﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Core;
using Focus.Core.Criteria.Report;
using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaJobActivitySummary : UserControlBase
	{
    protected void Page_Load(object sender, EventArgs e)
    {
      if(!Page.IsPostBack)
        Localise();
    }

    /// <summary>
    /// Resets this instance.
    /// </summary>
    private void Reset()
    {
      ViewedJobCheckBox.Checked = 
        ClickedHowToApplyCheckBox.Checked = 
        StaffReferralsCheckBox.Checked =
        SelfReferralsCheckBox.Checked =
        ReferralsRequestedCheckBox.Checked = 
        TotalReferralsCheckBox.Checked =
        EmployerInvitationsSentCheckBox.Checked = false;
    }

    /// <summary>
    /// Unbinds this instance.
    /// </summary>
    /// <returns></returns>
    public JobOrdersActivitySummaryCriteria Unbind(JobOrdersActivitySummaryCriteria criteria = null)
    {
      if (criteria.IsNull()) 
        criteria = new JobOrdersActivitySummaryCriteria();

      criteria.InvitedJobSeekerViewed = ViewedJobCheckBox.Checked;
      criteria.InvitedJobSeekerClicked = ClickedHowToApplyCheckBox.Checked;

      criteria.StaffReferrals = StaffReferralsCheckBox.Checked;
      criteria.SelfReferrals = SelfReferralsCheckBox.Checked;
      criteria.ReferralsRequested = ReferralsRequestedCheckBox.Checked;
      criteria.TotalReferrals = TotalReferralsCheckBox.Checked;
      criteria.EmployerInvitationsSent = EmployerInvitationsSentCheckBox.Checked;

      return criteria;
    }

    /// <summary>
    /// Binds the controls to the criteria
    /// </summary>
    /// <param name="criteria">The criteria to which to bind</param>
    public void BindCriteria(JobOrdersActivitySummaryCriteria criteria)
    {
      Reset();

      if (criteria.IsNull()) return;

      ViewedJobCheckBox.Checked = criteria.InvitedJobSeekerViewed;
      ClickedHowToApplyCheckBox.Checked = criteria.InvitedJobSeekerClicked;

      StaffReferralsCheckBox.Checked = criteria.StaffReferrals;
      SelfReferralsCheckBox.Checked = criteria.SelfReferrals;
      ReferralsRequestedCheckBox.Checked = criteria.ReferralsRequested;
      TotalReferralsCheckBox.Checked = criteria.TotalReferrals;
      EmployerInvitationsSentCheckBox.Checked = criteria.EmployerInvitationsSent;
    }

    private void Localise()
    {
      CriteriaJobActivitySummaryLabel.Text = CodeLocalise("CriteriaJobActivitySummaryLabel.Text", "Activity summary");

      InvitationsHeader.Text = CodeLocalise("InvitationsHeader.Text", "#CANDIDATETYPE# invitations sent");
      ReferralsHeader.Text = CodeLocalise("ReferralsHeader.Text", "Referral activity");

      ViewedJobCheckBox.Text = CodeLocalise("ViewedJobCheckBox.Text", Constants.Reporting.JobOrderReportDataColumns.InvitedJobSeekerViewed);
      DidntViewJobCheckBox.Text = CodeLocalise("DidntViewJobCheckBox.Text", Constants.Reporting.JobOrderReportDataColumns.InvitedJobSeekerNotViewed);
      ClickedHowToApplyCheckBox.Text = CodeLocalise("ClickedHowToApplyCheckBox.Text", Constants.Reporting.JobOrderReportDataColumns.InvitedJobSeekerClicked);
      DidntClickHowToApplyCheckBox.Text = CodeLocalise("DidntClickHowToApplyCheckBox.Text",Constants.Reporting.JobOrderReportDataColumns.InvitedJobSeekerNotClicked);
      EmployerInvitationsSentCheckBox.Text = CodeLocalise("EmployerInvitationsSentCheckBox.Text", Constants.Reporting.JobOrderReportDataColumns.EmployerInvitationsSent);

      StaffReferralsCheckBox.Text = CodeLocalise("StaffReferralsCheckBox.Text", "Staff referrals");
      SelfReferralsCheckBox.Text = CodeLocalise("SelfReferralsCheckBox.Text", "Self referrals");
      ReferralsRequestedCheckBox.Text = CodeLocalise("ReferralsRequestedCheckBox.Text", "Referrals requested");
      TotalReferralsCheckBox.Text = CodeLocalise("TotalReferralsCheckBox.Text", "Total referrals");
    }
  }
}