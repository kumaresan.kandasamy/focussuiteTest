﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Framework.Core;

using Focus.Core.Criteria.Report;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaJobSeekerActivityLog : UserControlBase
	{
    public bool ShowActivitiesAndServices { get; set; }

    private bool IsBound
    {
      get { return GetViewStateValue<bool>("CriteriaJobSeekerActivityLog:IsBound"); }
      set { SetViewStateValue("CriteriaJobSeekerActivityLog:IsBound", value); }
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The page raising the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();
        IsBound = false;
        ActivitiesServicesTable.Visible = ShowActivitiesAndServices;
      }
    }

    /// <summary>
    /// Handles the PreRender event of the Page control.
    /// </summary>
    /// <param name="sender">The page raising the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_PreRender(object sender, EventArgs e)
    {
      if (!IsBound)
      {
        Bind();
        IsBound = true;
      }
    }

    /// <summary>
    /// Resets this instance.
    /// </summary>
    private void Reset()
    {
      AddNoteCheckBox.Checked =
        AddSeekerToListCheckBox.Checked =
        AssignActivityOrServiceCheckBox.Checked =
        AssignSeekerToStaffCheckBox.Checked =
        FindJobsForSeekersCheckBox.Checked =
        EmailSeekersCheckBox.Checked =
        MarkForFollowUpCheckBox.Checked =
        MarkIssueResolvedCheckBox.Checked = false;
    }

    /// <summary>
    /// Unbinds this instance.
    /// </summary>
    /// <returns></returns>
    public JobSeekerActivityLogCriteria Unbind(JobSeekerActivityLogCriteria criteria = null)
    {
      if(criteria.IsNull()) criteria = new JobSeekerActivityLogCriteria();

      criteria.NotesAdded = AddNoteCheckBox.Checked;
      criteria.AddedToLists = AddSeekerToListCheckBox.Checked;
      criteria.ActivitiesAssigned = AssignActivityOrServiceCheckBox.Checked;
      criteria.StaffAssignments = AssignSeekerToStaffCheckBox.Checked;
      criteria.FindJobsForSeeker = FindJobsForSeekersCheckBox.Checked;
      criteria.EmailsSent = EmailSeekersCheckBox.Checked;
      criteria.FollowUpIssues = MarkForFollowUpCheckBox.Checked;
      criteria.IssuesResolved = MarkIssueResolvedCheckBox.Checked;

      if (CategoryDropDown.SelectedIndex > 0)
      {
        criteria.ActivityCategory = new Tuple<long?, string>(CategoryDropDown.SelectedValue.ToLong(), CategoryDropDown.SelectedItem.Text);
        if (ActivityDropDownList.SelectedValue.IsNotNullOrEmpty())
        {
          var activity = ActivityCascader.SelectedValue.Split(new [] {":::"}, StringSplitOptions.RemoveEmptyEntries);
          criteria.Activity = new Tuple<long?, string>(activity[0].ToLong(), activity[1]);
        }
      }

      return criteria;
    }

    /// <summary>
    /// Binds from criteria.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    public void BindCriteria(JobSeekerActivityLogCriteria criteria)
    {
      Reset();

      if (criteria.IsNull()) return;

      AddNoteCheckBox.Checked = criteria.NotesAdded;
      AddSeekerToListCheckBox.Checked = criteria.AddedToLists;
      AssignActivityOrServiceCheckBox.Checked = criteria.ActivitiesAssigned;
      AssignSeekerToStaffCheckBox.Checked = criteria.StaffAssignments;
      FindJobsForSeekersCheckBox.Checked = criteria.FindJobsForSeeker;
      EmailSeekersCheckBox.Checked = criteria.EmailsSent;
      MarkForFollowUpCheckBox.Checked = criteria.FollowUpIssues;
      MarkIssueResolvedCheckBox.Checked = criteria.IssuesResolved;

      if (criteria.ActivityCategory.IsNotNull())
      {
        CategoryDropDown.SelectedValue = criteria.ActivityCategory.Item1.ToString();
        if (criteria.Activity.IsNotNull())
        {
          ActivityDropDownList.SelectValue(criteria.Activity.Item1.ToString());
          ActivityCascader.SelectedValue = criteria.Activity.Item1.ToString();
        }
      }
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      CriteriaJobSeekerActivityLogLabel.Text = CodeLocalise("CriteriaJobSeekerActivityLogLabel.Text", "Job seeker activity log");
      ActionsHeader.Text = CodeLocalise("ActionsHeader.Text", "Actions");
      ActivitiesServicesHeader.Text = CodeLocalise("ActivitiesServicesHeader.Text", "Activities/Services");

      AddNoteCheckBox.Text = CodeLocalise("ActionsCheckBoxList.AddNote.Text", "Add a note");
      AddSeekerToListCheckBox.Text = CodeLocalise("ActionsCheckBoxList.AddToList.Text", "Add seekers to list");
      AssignActivityOrServiceCheckBox.Text = CodeLocalise("ActionsCheckBoxList.AssignActivityService.Text", "Assign activities/services");
      AssignSeekerToStaffCheckBox.Text = CodeLocalise("ActionsCheckBoxList.AssignToStaff.Text", "Assign seekers to staff ");
      FindJobsForSeekersCheckBox.Text = CodeLocalise("ActionsCheckBoxList.FindJobForSeeker.Text", "Find jobs for seekers");
      EmailSeekersCheckBox.Text = CodeLocalise("ActionsCheckBoxList.EmailSeekers.Text", "Email seekers ");
      MarkForFollowUpCheckBox.Text = CodeLocalise("ActionsCheckBoxList.MarkFollowUp.Text", "Mark issue as follow-up");
      MarkIssueResolvedCheckBox.Text = CodeLocalise("ActionsCheckBoxList.MarkResolved.Text", "Mark issue as resolved");

      ActivityCascader.LoadingText = CodeLocalise("ActivityCascader.LoadingText.NoEdit", "Loading activities");

      //Enable the Activities/Services controls if Enable Job Seeker Activities/Services is checked
      AssignActivityOrServiceCheckBox.Visible = CategoryDropDown.Visible = ActivityDropDownList.Visible = ActivitiesServicesHeader.Visible = CategoryDropDownLabel.Visible = ActivityDropDownListLabel.Visible = App.Settings.EnableJSActivityBackdatingDateSelection;
    }

    /// <summary>
    /// Binds this instance.
    /// </summary>
    private void Bind()
    {
      CategoryDropDown.DataTextField = "Name";
      CategoryDropDown.DataValueField = "Id";
      CategoryDropDown.DataSource = ServiceClientLocator.CoreClient(App).GetActivityCategories(ActivityType.JobSeeker);
      CategoryDropDown.DataBind();
      CategoryDropDown.Items.Insert(0, CodeLocalise("ActivityCategory.Top.NoEdit", "- Category -"));
    }
  }
}
