﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaIndividualJobSeekerCharacteristics.ascx.cs" Inherits="Focus.Web.WebReporting.Controls.CriteriaIndividualJobSeekerCharacteristics" %>
<%@ Register Src="~/Code/Controls/User/UpdateableClientList.ascx" TagName="UpdateableClientList" TagPrefix="uc" %>

<table role="presentation" style="width: 100%;" class="accordionWithIcon">
  <tr class="multipleAccordionTitle">
    <td style="vertical-align: top; width: 10%;">
      <span class="accordionIcon" style="height: 25px"></span>
    </td>
    <td style="width: 90%;">
      <asp:Literal runat="server" ID="CriteriaIndividualJobSeekerCharacteristicsLabel" />
    </td>
  </tr>
  <tr class="accordionContent">
		<td colspan="2">
			<table role="presentation" style="width:100%;">
				<tr>
					<td>
						<strong><focus:LocalisedLabel ID="IndividualCharacteristicsFirstNameLabel"  runat="server" DefaultText="First name" LocalisationKey="IndividualCharacteristicsFirstName.Text"/></strong>
					</td>
				</tr>
				<tr>
					<td>
						<asp:TextBox runat="server" ID="IndividualCharacteristicsFirstNameTextBox" title="Individual Characteristics FirstName TextBox" ></asp:TextBox>
					</td>
				</tr>
				<tr>
					<td>
						<strong><focus:LocalisedLabel ID="IndividualCharacteristicsLastNameLabel" runat="server" DefaultText="Last name" LocalisationKey="IndividualCharacteristicsLastNameLabel.Text"/></strong>
					</td>
				</tr>
				<tr>
					<td>
						<asp:TextBox runat="server" ID="IndividualCharacteristicsLastNameTextBox" title="Individual Characteristics LastName TextBox"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<td>
						<strong><focus:LocalisedLabel ID="IndividualCharacteristicsEmailLabel" runat="server" DefaultText="Email address" LocalisationKey="IndividualCharacteristicsEmailLabel.Text"/></strong>
					</td>
				</tr>
				<tr>
					<td>
						<asp:TextBox runat="server" ID="IndividualCharacteristicsEmailTextBox" title="Individual Characteristics Email TextBox"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<td>
						<strong><focus:LocalisedLabel ID="IndividualCharacteristicsSsnLabel" runat="server" DefaultText="SSN" LocalisationKey="IndividualCharacteristicsSsnLabel.Text"/></strong>
					</td>
				</tr>
				<tr>
					<td>
						<uc:UpdateableClientList ID="IndividualCharacteristicsSsnClientList" runat="server" AllowNonAdded="True" InputMask="9?99-99-9999"
                          MaskPromptCharacter="_" ValidatorRegex="^\d{3}-\d{2}-\d{4}$" ValidationGroup="SendMessagesGroup" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
