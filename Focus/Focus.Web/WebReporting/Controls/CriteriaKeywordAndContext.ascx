﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaKeywordAndContext.ascx.cs"
    Inherits="Focus.Web.WebReporting.Controls.CriteriaKeywordAndContext" %>
<table role="presentation" style="width:100%;" class="accordionWithIcon">
    <tr class="multipleAccordionTitle">
        <td style="vertical-align:top; width:10%;">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td style="width:90%;">
            <asp:Literal runat="server" ID="CriteriaKeywordAndContextLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
            <table role="presentation" style="width:100%;">
                <tr>
                    <td>
                        <strong><asp:Literal runat="server" ID="SearchTermsHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:RadioButtonList runat="server" ID="SearchTypeRadioButtonList" RepeatDirection="Vertical"
                            RepeatLayout="Table" role="presentation"/>
                    </td>
                </tr>
                <tr>
                    <td>
	                    <focus:LocalisedLabel runat="server" ID="SearchTermsTextBoxLabel" AssociatedControlID="SearchTermsTextBox" LocalisationKey="Search" DefaultText="Search" CssClass="sr-only"/>
                        <asp:TextBox runat="server" TextMode="MultiLine" ID="SearchTermsTextBox" Width="95%"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Literal runat="server" ID="SearchResumeContextHeader"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBoxList ID="SearchResumeContextCheckBoxList" TextAlign="Right" runat="server" role="presentation" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
