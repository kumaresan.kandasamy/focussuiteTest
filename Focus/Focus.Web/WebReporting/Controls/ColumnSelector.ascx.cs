﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;

using Focus.Core;
using Focus.Core.Criteria.Report;
using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class ColumnSelector : UserControlBase
	{

    private IReportCriteria ReportCriteria
    {
      get { return GetViewStateValue<IReportCriteria>("ColumnSelector:ReportCriteria"); }
      set { SetViewStateValue("ColumnSelector:ReportCriteria", value); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
        Localise();
    }

    /// <summary>
    /// Shows the specified criteria.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    public void Show(IReportCriteria criteria)
    {
      ReportCriteria = criteria;

      switch (ReportCriteria.ReportType)
      {
        case ReportType.JobSeeker:
          ShowJobSeekerReportColumns((JobSeekersReportCriteria)ReportCriteria);
          ColumnSelectorHeading.Text = CodeLocalise("JobSeekerReportColumnSelector.Header", "Job seekers report columns");
          break;
        case ReportType.JobOrder:
          ShowJobOrderReportColumns((JobOrdersReportCriteria)ReportCriteria);
					ColumnSelectorHeading.Text = CodeLocalise("JobOrderReportColumnSelector.Header", "#EMPLOYMENTTYPES# report columns");
          break;
        case ReportType.Employer:
          ShowEmployerReportColumns((EmployersReportCriteria)ReportCriteria);
					ColumnSelectorHeading.Text = CodeLocalise("EmployerReportColumnSelector.Header", "#BUSINESSES# report columns");
          break;
      }
      ColumnSelectorModalPopup.Show();
    }

    private void Localise()
    {
      VeteranStatusHeader.Text = CodeLocalise("VeteranStatusHeader.Text", "VETERAN STATUS");
      VeteranTypeCheckBox.Text = CodeLocalise("VeteranTypeCheckBox.Text", "Veteran type");
      VeteranTransitionTypeCheckBox.Text = CodeLocalise("TransitionTypeCheckBox.Text", "Transition type");
      VeteranMilitaryDischargeCheckBox.Text = CodeLocalise("VeteranMilitaryDischargeCheckBox.Text", "Military discharge");
      VeteranBranchOfServiceCheckBox.Text = CodeLocalise("VeteranBranchOfServiceCheckBox.Text", "Branch of service");
      ColumnSelectorHeading.Text = CodeLocalise("ReportColumnSelector.Header", "Report columns");

      CancelButton.Text = CodeLocalise("CancelButton.Text", "Cancel");
      SaveButton.Text = CodeLocalise("SaveButton.Text", "Save");
    }

    #region job order

    private void ShowJobOrderReportColumns(JobOrdersReportCriteria criteria)
    {
      CriteriaJobActivitySummary.Visible = CriteriaReferralsOutcome.Visible = true;
      
      CriteriaJobActivitySummary.BindCriteria(criteria.JobOrdersActivitySummaryInfo);
      CriteriaReferralsOutcome.BindCriteria(criteria.JobOrdersReferralOutcomesInfo);
    }

    private void UnbindJobOrderCriteria()
    {
      var criteria = (JobOrdersReportCriteria)ReportCriteria;
      criteria.JobOrdersActivitySummaryInfo = CriteriaJobActivitySummary.Unbind(criteria.JobOrdersActivitySummaryInfo);
      criteria.JobOrdersReferralOutcomesInfo = CriteriaReferralsOutcome.Unbind(criteria.JobOrdersReferralOutcomesInfo);

      ReportCriteria = criteria;
    }

    #endregion

    #region employer

    private void ShowEmployerReportColumns(EmployersReportCriteria criteria)
    {
      CriteriaEmployerActivity.Visible = true;
      CriteriaEmployerActivity.BindCriteria(criteria.EmployersEmployerActivityInfo);
    }

    private void UnbindEmployerCriteria()
    {
      var criteria = (EmployersReportCriteria) ReportCriteria;
      criteria.EmployersEmployerActivityInfo = CriteriaEmployerActivity.Unbind();
      ReportCriteria = criteria;

    }

    #endregion

    #region job seeker

    /// <summary>
    /// Shows the job seeker report columns.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    private void ShowJobSeekerReportColumns(JobSeekersReportCriteria criteria)
    {
      CriteriaJobSeekerActivitySummary.Visible =
        CriteriaJobSeekerActivityLog.Visible = 
        CriteriaReferralsOutcome.Visible =
        JobSeekerExtraColumnsPlaceHolder.Visible = true;

      CriteriaJobSeekerActivitySummary.BindCriteria(criteria.JobSeekerActivitySummaryInfo);
      CriteriaJobSeekerActivityLog.BindCriteria(criteria.JobSeekerActivityLogInfo);
      CriteriaReferralsOutcome.BindCriteria(criteria.JobSeekerReferralOutcomesInfo);

      if (criteria.IsNull() || criteria.ExtraColumns.IsNull())
      {
        VeteranTypeCheckBox.Checked =
          VeteranTransitionTypeCheckBox.Checked = 
          VeteranMilitaryDischargeCheckBox.Checked =
          VeteranBranchOfServiceCheckBox.Checked = false;
      }
      else
      {
        VeteranTypeCheckBox.Checked = criteria.ExtraColumns.VeteranType;
        VeteranTransitionTypeCheckBox.Checked = criteria.ExtraColumns.VeteranTransitionType;
        VeteranMilitaryDischargeCheckBox.Checked = criteria.ExtraColumns.VeteranMilitaryDischarge;
        VeteranBranchOfServiceCheckBox.Checked = criteria.ExtraColumns.VeteranBranchOfService;
      }
    }

    private void UnbindJobSeekerCriteria()
    {
      var criteria = (JobSeekersReportCriteria) ReportCriteria;
      criteria.JobSeekerActivitySummaryInfo = CriteriaJobSeekerActivitySummary.Unbind(criteria.JobSeekerActivitySummaryInfo);
      criteria.JobSeekerActivityLogInfo = CriteriaJobSeekerActivityLog.Unbind(criteria.JobSeekerActivityLogInfo);
      criteria.JobSeekerReferralOutcomesInfo = CriteriaReferralsOutcome.Unbind(criteria.JobSeekerReferralOutcomesInfo);

      if (criteria.ExtraColumns.IsNull())
        criteria.ExtraColumns = new JobSeekersReportExtraColumns();

      criteria.ExtraColumns.VeteranType = VeteranTypeCheckBox.Checked;
      criteria.ExtraColumns.VeteranTransitionType = VeteranTransitionTypeCheckBox.Checked;
      criteria.ExtraColumns.VeteranMilitaryDischarge = VeteranMilitaryDischargeCheckBox.Checked;
      criteria.ExtraColumns.VeteranBranchOfService = VeteranBranchOfServiceCheckBox.Checked;

      ReportCriteria = criteria;
    }

    #endregion

    #region events

    protected void CancelButton_Clicked(object sender, EventArgs e)
    {
      ColumnSelectorModalPopup.Hide();
    }

    protected void SaveButton_Clicked(object sender, EventArgs e)
    {
      switch (ReportCriteria.ReportType)
      {
        case ReportType.JobSeeker:
          UnbindJobSeekerCriteria();
          break;
        case ReportType.JobOrder:
          UnbindJobOrderCriteria();
          break;
        case ReportType.Employer:
          UnbindEmployerCriteria();
          break;

      }
      ColumnSelectorModalPopup.Hide();
      OnReportColumnsChanged(new CommandEventArgs("ColumnsChanged", ReportCriteria));
    }

    #endregion

    #region page generated events

    public delegate void ReportColumnsChangedHandler(object sender, CommandEventArgs eventArgs);
    public event ReportColumnsChangedHandler ReportColumnsChanged;

    /// <summary>
    /// Raises the <see cref="CommandEventArgs" /> event.
    /// </summary>
    /// <param name="eventargs">The <see cref="CommandEventArgs" /> instance containing the event data.</param>
    public void OnReportColumnsChanged(CommandEventArgs eventargs)
    {
      var handler = ReportColumnsChanged;
      if (handler != null) handler(this, eventargs);
    }

    #endregion
  }
}