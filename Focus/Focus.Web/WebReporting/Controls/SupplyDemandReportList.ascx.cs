﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.Report;
using Focus.Core.Views;
using Focus.Web.Code;
using Framework.Core;

namespace Focus.Web.WebReporting.Controls
{
  public partial class SupplyDemandReportList : ReportControl
  {
    int _supplyDemandCount;
    int _supplyTotalCount;
    int _demandTotalCount;
    int _gapTotalCount;

    public SupplyDemandReportCriteria ReportCriteria
    {
      get { return GetViewStateValue<SupplyDemandReportCriteria>("SupplyDemandReportList:ReportCriteria"); }
      set { SetViewStateValue("SupplyDemandReportList:ReportCriteria", value); }
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
        Localise();
    }

    /// <summary>
    /// Binds the specified criteria.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    public void Bind(SupplyDemandReportCriteria criteria)
    {
      Visible = true;
      ReportCriteria = criteria;
      SupplyDemandList.DataBind();
      BindSortingArrows();
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      EditColumnsLink.Text = CodeLocalise("EditColumnsLink.Text", "Edit table columns");
      EditColumnsLink.Enabled = false;
		}

    #region list binding methods

    /// <summary>
    /// Gets the supply demand list.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="orderBy">The order by.</param>
    /// <param name="startRowIndex">Start index of the row.</param>
    /// <param name="maximumRows">The maximum rows.</param>
    /// <returns></returns>
    public List<SupplyDemandReportView> GetSupplyDemandList(SupplyDemandReportCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
    {
      var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

      criteria.PageSize = maximumRows;
      criteria.PageIndex = pageIndex;
      var sortedSupplyDemandReports = new List<SupplyDemandReportView>();
      var supplyDemandReports = ServiceClientLocator.ReportClient(App).GetSupplyDemandReport(criteria);
      
      if (criteria.OrderInfo.IsNotNull())
      {
        switch (criteria.OrderInfo.OrderBy)
        {
          case ReportSupplyDemandOrderBy.DemandCount:
            sortedSupplyDemandReports = criteria.OrderInfo.Direction == ReportOrder.Ascending
                                          ? supplyDemandReports.OrderBy(x => x.DemandCount).ToList()
                                          : supplyDemandReports.OrderByDescending(x => x.DemandCount).ToList();
            break;

          case ReportSupplyDemandOrderBy.SupplyCount:
            sortedSupplyDemandReports = criteria.OrderInfo.Direction == ReportOrder.Ascending
                                          ? supplyDemandReports.OrderBy(x => x.SupplyCount).ToList()
                                          : supplyDemandReports.OrderByDescending(x => x.SupplyCount).ToList();
            break;

          case ReportSupplyDemandOrderBy.Gap:
            sortedSupplyDemandReports = criteria.OrderInfo.Direction == ReportOrder.Ascending
                                          ? supplyDemandReports.OrderBy(x => x.Gap).ToList()
                                          : supplyDemandReports.OrderByDescending(x => x.Gap).ToList();
            break;

          case ReportSupplyDemandOrderBy.OnetCode:
            sortedSupplyDemandReports = criteria.OrderInfo.Direction == ReportOrder.Ascending
                                          ? supplyDemandReports.OrderBy(x => x.OnetCode).ToList()
                                          : supplyDemandReports.OrderByDescending(x => x.OnetCode).ToList();
            break;

          case ReportSupplyDemandOrderBy.GroupBy:
            sortedSupplyDemandReports = criteria.OrderInfo.Direction == ReportOrder.Ascending
                                          ? supplyDemandReports.OrderBy(x => x.GroupBy).ToList()
                                          : supplyDemandReports.OrderByDescending(x => x.GroupBy).ToList();
            break;

          default:
            sortedSupplyDemandReports = supplyDemandReports.OrderBy(x => x.OnetCode).ToList();
            break;
        }
      }

      _supplyDemandCount = supplyDemandReports.TotalCount;
      return sortedSupplyDemandReports;
    }

    /// <summary>
    /// Gets the supply demand list count.
    /// </summary>
    /// <returns></returns>
    public int GetSupplyDemandListCount()
    {
      return _supplyDemandCount;
    }

    /// <summary>
    /// Gets the supply demand list count.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <returns></returns>
    public int GetSupplyDemandListCount(SupplyDemandReportCriteria criteria)
    {
      return _supplyDemandCount;
    }
    
    #endregion

    #region list formatting

    /// <summary>
    /// Binds the sorting arrows.
    /// </summary>
    private void BindSortingArrows()
    {
      FormatSortingButtons((ImageButton)SupplyDemandList.FindControl("OnetCodeSortAscButton"));
      FormatSortingButtons((ImageButton)SupplyDemandList.FindControl("OnetCodeSortDescButton"));
      FormatSortingButtons((ImageButton)SupplyDemandList.FindControl("OnetTitleSortAscButton"));
      FormatSortingButtons((ImageButton)SupplyDemandList.FindControl("OnetTitleSortDescButton"));
      FormatSortingButtons((ImageButton)SupplyDemandList.FindControl("SupplyCountSortAscButton"));
      FormatSortingButtons((ImageButton)SupplyDemandList.FindControl("SupplyCountSortDescButton"));
      FormatSortingButtons((ImageButton)SupplyDemandList.FindControl("DemandCountSortAscButton"));
      FormatSortingButtons((ImageButton)SupplyDemandList.FindControl("DemandCountSortDescButton"));
      FormatSortingButtons((ImageButton)SupplyDemandList.FindControl("GapSortAscButton"));
      FormatSortingButtons((ImageButton)SupplyDemandList.FindControl("GapSortDescButton"));
    }

    /// <summary>
    /// Formats the sorting buttons.
    /// </summary>
    /// <param name="button">The button.</param>
    private void FormatSortingButtons(ImageButton button)
    {
      if (button.IsNull()) return;

      var sortingParams = button.CommandArgument.Split(Convert.ToChar(" "));

      if (sortingParams.IsNullOrEmpty() || sortingParams.GetLength(0) != 2)
        throw new Exception("Supply demand report sorting error getting parameters.");

      ReportSupplyDemandOrderBy orderby;
      ReportOrder orderDirection;

      if (!Enum.TryParse(sortingParams[0], out orderby))
        throw new Exception("Supply demand report sorting error getting order by values.");
      if (!Enum.TryParse(sortingParams[1], out orderDirection))
        throw new Exception("Supply demand report sorting error getting order direction values.");

      if (orderby == ReportCriteria.OrderInfo.OrderBy && orderDirection == ReportCriteria.OrderInfo.Direction)
      {
        button.Enabled = false;
        button.ImageUrl = orderDirection == ReportOrder.Ascending ? UrlBuilder.ActivityListSortAscImage() : UrlBuilder.ActivityListSortDescImage();
      }
      else
      {
        button.Enabled = true;
        button.ImageUrl = orderDirection == ReportOrder.Ascending ? UrlBuilder.ActivityOnSortAscImage() : UrlBuilder.CategorySortDescImage();
      }
    }

    #endregion
    
    #region events


    /// <summary>
    /// Handles the ItemDatabound event of the SupplyDemandList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
    protected void SupplyDemandList_ItemDatabound(object sender, ListViewItemEventArgs e)
    {
      var supplyDemandView = (SupplyDemandReportView) e.Item.DataItem;

			((Literal)e.Item.FindControl("OnetCodeLiteral")).Text = supplyDemandView.OnetCode.FormatToOnetPattern();
      ((Literal) e.Item.FindControl("OnetTitleLiteral")).Text = supplyDemandView.GroupBy;
      ((Literal) e.Item.FindControl("SupplyCountLiteral")).Text = supplyDemandView.SupplyCount.ToString();
      ((Literal) e.Item.FindControl("DemandCountLiteral")).Text = supplyDemandView.DemandCount.ToString();
      ((Literal) e.Item.FindControl("GapLiteral")).Text =
        (supplyDemandView.DemandCount - supplyDemandView.SupplyCount).ToString();
    }

    /// <summary>
    /// Handles the Selecting event of the SupplyDemandListDataSource control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
    protected void SupplyDemandListDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
      e.InputParameters["criteria"] = ReportCriteria;
    }

    /// <summary>
    /// Called when [layout created].
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SupplyDemandList_OnLayoutCreated(object sender, EventArgs e)
    {
      ((Literal)SupplyDemandList.FindControl("OnetCodeHeader")).Text = CodeLocalise("OnetCodeHeader.Text",
                                                                                    Constants.Reporting
                                                                                             .SupplyDemandDataColumns
                                                                                             .OnetCode);

      ((Literal) SupplyDemandList.FindControl("OnetTitleHeader")).Text = CodeLocalise("OnetTitle.Text",
                                                                                        Constants.Reporting
                                                                                                 .SupplyDemandDataColumns
                                                                                                 .GroupBy);
      ((Literal)SupplyDemandList.FindControl("SupplyCountHeader")).Text = CodeLocalise("SupplyCount.Text",
                                                                                        Constants.Reporting
                                                                                                 .SupplyDemandDataColumns
                                                                                                 .SupplyCount);
      ((Literal)SupplyDemandList.FindControl("DemandCountHeader")).Text = CodeLocalise("DemandCount.Text",
                                                                                        Constants.Reporting
                                                                                                 .SupplyDemandDataColumns
                                                                                                 .DemandCount);
      ((Literal)SupplyDemandList.FindControl("GapHeader")).Text = CodeLocalise("Gap.Text",
                                                                                Constants.Reporting
                                                                                         .SupplyDemandDataColumns.Gap);

      ((ImageButton)SupplyDemandList.FindControl("OnetCodeSortAscButton")).CommandArgument =
        string.Concat(ReportSupplyDemandOrderBy.OnetCode, " ", ReportOrder.Ascending);
      ((ImageButton)SupplyDemandList.FindControl("OnetCodeSortDescButton")).CommandArgument =
        string.Concat(ReportSupplyDemandOrderBy.OnetCode, " ", ReportOrder.Descending);

      ((ImageButton)SupplyDemandList.FindControl("OnetTitleSortAscButton")).CommandArgument =
        string.Concat(ReportSupplyDemandOrderBy.GroupBy, " ", ReportOrder.Ascending);
      ((ImageButton)SupplyDemandList.FindControl("OnetTitleSortDescButton")).CommandArgument =
        string.Concat(ReportSupplyDemandOrderBy.GroupBy, " ", ReportOrder.Descending);

      ((ImageButton)SupplyDemandList.FindControl("SupplyCountSortAscButton")).CommandArgument =
        string.Concat(ReportSupplyDemandOrderBy.SupplyCount, " ", ReportOrder.Ascending);
      ((ImageButton)SupplyDemandList.FindControl("SupplyCountSortDescButton")).CommandArgument =
        string.Concat(ReportSupplyDemandOrderBy.SupplyCount, " ", ReportOrder.Descending);

      ((ImageButton)SupplyDemandList.FindControl("DemandCountSortAscButton")).CommandArgument =
        string.Concat(ReportSupplyDemandOrderBy.DemandCount, " ", ReportOrder.Ascending);
      ((ImageButton)SupplyDemandList.FindControl("DemandCountSortDescButton")).CommandArgument =
        string.Concat(ReportSupplyDemandOrderBy.DemandCount, " ", ReportOrder.Descending);

      ((ImageButton)SupplyDemandList.FindControl("GapSortAscButton")).CommandArgument =
        string.Concat(ReportSupplyDemandOrderBy.Gap, " ", ReportOrder.Ascending);
      ((ImageButton)SupplyDemandList.FindControl("GapSortDescButton")).CommandArgument =
        string.Concat(ReportSupplyDemandOrderBy.Gap, " ", ReportOrder.Descending);
    }

    /// <summary>
    /// Handles the Sorting event of the SupplyDemandList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
    protected void SupplyDemandList_Sorting(object sender, ListViewSortEventArgs e)
    {
      if (e.SortExpression.IsNullOrEmpty())
        throw new Exception("Supply demand report sorting error.");

      var sortingParams = e.SortExpression.Split(Convert.ToChar(" "));

      if (sortingParams.IsNullOrEmpty() || sortingParams.GetLength(0) != 2)
        throw new Exception("Supply demand report sorting error getting parameters.");

      ReportSupplyDemandOrderBy orderby;
      ReportOrder orderDirection;

      if (!Enum.TryParse(sortingParams[0], out orderby))
        throw new Exception("Supply demand report sorting error getting order by values.");
      if (!Enum.TryParse(sortingParams[1], out orderDirection))
        throw new Exception("Supply demand report sorting error getting order direction values.");

      ReportCriteria.OrderInfo = new SupplyDemandReportSortOrder { OrderBy = orderby, Direction = orderDirection };

      BindSortingArrows();
    }

   /// <summary>
    /// Handles the Clicked event of the EditColumnsLink control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void EditColumnsLink_Clicked(object sender, EventArgs e)
    {
      ColumnSelector.Show(ReportCriteria);
    }

    /// <summary>
    /// Handles the ReportColumnsChanged event of the ColumnSelector control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="eventargs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected void ColumnSelector_ReportColumnsChanged(object sender, CommandEventArgs eventargs)
    {
      Bind((SupplyDemandReportCriteria)eventargs.CommandArgument);
      OnReportCriteriaChanged(eventargs);
    }

    #endregion

    #region page generated events

    public delegate void ReportCriteriaChangedHandler(object sender, CommandEventArgs eventArgs);
    public event ReportCriteriaChangedHandler ReportCriteriaChanged;

    /// <summary>
    /// Raises the <see cref="CommandEventArgs" /> event.
    /// </summary>
    /// <param name="eventargs">The <see cref="CommandEventArgs" /> instance containing the event data.</param>
    public void OnReportCriteriaChanged(CommandEventArgs eventargs)
    {
      var handler = ReportCriteriaChanged;
      if (handler != null) handler(this, eventargs);
    }

    #endregion

    /// <summary>
    /// Handles the OnPreRender event of the SupplyDemandList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SupplyDemandList_OnPreRender(object sender, EventArgs e)
    {
      if (SupplyDemandList.FindControl("SupplyCountTotalLabel") != null)
        ((Label) SupplyDemandList.FindControl("SupplyCountTotalLabel")).Text =
          _supplyTotalCount.ToString(CultureInfo.InvariantCulture);
      if (SupplyDemandList.FindControl("DemandCountTotalLabel") != null)
        ((Label) SupplyDemandList.FindControl("DemandCountTotalLabel")).Text =
          _demandTotalCount.ToString(CultureInfo.InvariantCulture);
      if (SupplyDemandList.FindControl("GapTotalLabel") != null)
        ((Label) SupplyDemandList.FindControl("GapTotalLabel")).Text =
          _gapTotalCount.ToString(CultureInfo.InvariantCulture);
      if (SupplyDemandList.FindControl("TotalLabel") != null)
        ((Label) SupplyDemandList.FindControl("TotalLabel")).Text = CodeLocalise("ReportTotalLabel.Text",
                                                                                 Constants.Reporting
                                                                                          .SupplyDemandDataColumns
                                                                                          .ReportTotal);
			if ((ImageButton)SupplyDemandList.FindControl("OnetCodeSortAscButton") != null)
				((ImageButton)SupplyDemandList.FindControl("OnetCodeSortAscButton")).AlternateText = CodeLocalise("Global.SortAscending", "Sort ascending");
			if ((ImageButton)SupplyDemandList.FindControl("OnetCodeSortDescButton") != null)
				((ImageButton)SupplyDemandList.FindControl("OnetCodeSortDescButton")).AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			if ((ImageButton)SupplyDemandList.FindControl("OnetTitleSortAscButton") != null)
				((ImageButton)SupplyDemandList.FindControl("OnetTitleSortAscButton")).AlternateText = CodeLocalise("Global.SortAscending", "Sort ascending");
			if ((ImageButton)SupplyDemandList.FindControl("OnetTitleSortDescButton") != null)
				((ImageButton)SupplyDemandList.FindControl("OnetTitleSortDescButton")).AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			if ((ImageButton)SupplyDemandList.FindControl("SupplyCountSortAscButton") != null)
				((ImageButton)SupplyDemandList.FindControl("SupplyCountSortAscButton")).AlternateText = CodeLocalise("Global.SortAscending", "Sort ascending");
			if ((ImageButton)SupplyDemandList.FindControl("SupplyCountSortDescButton") != null)
				((ImageButton)SupplyDemandList.FindControl("SupplyCountSortDescButton")).AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			if ((ImageButton)SupplyDemandList.FindControl("DemandCountSortAscButton") != null)
				((ImageButton)SupplyDemandList.FindControl("DemandCountSortAscButton")).AlternateText = CodeLocalise("Global.SortAscending", "Sort ascending");
			if ((ImageButton)SupplyDemandList.FindControl("DemandCountSortDescButton") != null)
				((ImageButton)SupplyDemandList.FindControl("DemandCountSortDescButton")).AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			if ((ImageButton)SupplyDemandList.FindControl("GapSortAscButton") != null)
				((ImageButton)SupplyDemandList.FindControl("GapSortAscButton")).AlternateText = CodeLocalise("Global.SortAscending", "Sort ascending");
			if ((ImageButton)SupplyDemandList.FindControl("GapSortDescButton") != null)
				((ImageButton)SupplyDemandList.FindControl("GapSortDescButton")).AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
    }

    /// <summary>
    /// Handles the OnSelected event of the SupplyDemandListDataSource control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs"/> instance containing the event data.</param>
    protected void SupplyDemandListDataSource_OnSelected(object sender, ObjectDataSourceStatusEventArgs e)
	  {
		  if (e.ReturnValue is int) return;
      var data = (List<SupplyDemandReportView>) e.ReturnValue;
			// sort the counts here as if set in the select method, they get wiped! Looks like something triggers the select on the objectdatasource twice, which wipes them
      _supplyDemandCount = data.Count;
      _supplyTotalCount = data.Sum(report => report.SupplyCount);
      _demandTotalCount = data.Sum(report => report.DemandCount);
      _gapTotalCount = _demandTotalCount - _supplyTotalCount;
	  }
  }
}