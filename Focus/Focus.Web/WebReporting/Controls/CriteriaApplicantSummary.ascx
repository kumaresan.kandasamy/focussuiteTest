﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaApplicantSummary.ascx.cs" Inherits="Focus.Web.WebReporting.Controls.CriteriaApplicantSummary" %>
<table width="100%" class="accordionWithIcon">
    <tr class="multipleAccordionTitle">
        <td valign="top" width="10%">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td width="90%">
            <asp:Literal runat="server" ID="ApplicantSummaryLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <strong>
                            <asp:Literal runat="server" ID="OutcomesStatusHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="InterviewCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="FailedToShowCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="DeniedInterviewCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="HiredCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="RejectedCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="JobOfferRefusedCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="DidNotApplyCheckBox"/></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
