﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployerReportList.ascx.cs"
    Inherits="Focus.Web.WebReporting.Controls.EmployerReportList" %>
<%@ Register TagPrefix="uc" TagName="Pager" Src="~/Code/Controls/User/Pager.ascx" %>
<%@ Register Src="ColumnSelector.ascx" TagName="ColumnSelector" TagPrefix="uc" %>

<table role="presentation">
    <tr>
        <td style="width: 100%;">
            <uc:Pager ID="EmployerListPager" runat="server" PagedControlId="EmployerList" PageSize="10" DisplayRecordCount="True" />
        </td>
        <td style="white-space:  nowrap">
            <asp:LinkButton runat="server" ID="EditColumnsLink" OnClick="EditColumnsLink_Clicked" />
        </td>
    </tr>
</table>
<div id="ReportResults" class="horizontalscroll">
    <asp:ListView runat="server" ID="EmployerList" ItemPlaceholderID="EmployerListPlaceHolder"
        OnItemDataBound="EmployerList_ItemDatabound" DataKeyNames="FocusBusinessUnitId" OnDataBound="EmployerList_DataBound"
        OnSorting="EmployerList_Sorting" DataSourceID="EmployerListDataSource">
    <LayoutTemplate>
            <table style="width: 100%" class="table">
            <tr>
                <th runat="server" id="EmployerColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="EmployerHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="EmployerSortAscButton"
                                  runat="server" CommandName="Sort" CssClass="AscButton" />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="EmployerSortDescButton"
                                  runat="server" CommandName="Sort" CssClass="DescButton" />
                      </div>
                </th>
                <th runat="server" id="CountyColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="CountyHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="CountySortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="County asc" CssClass="AscButton" />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="CountySortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="County desc" CssClass="DescButton" />
                      </div>
                </th>
                <th runat="server" id="StateColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="StateHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="StateSortAscButton"
                                  runat="server" CommandName="Sort" CssClass="AscButton" />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="StateSortDescButton"
                                  runat="server" CommandName="Sort" CssClass="DescButton" />
                      </div>
                </th>
                <th runat="server" id="OfficeColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="OfficeHeader" />
                </th>
                <th runat="server" id="JobOrdersClosedColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="JobOrdersClosedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="JobOrdersClosedSortAscButton"
                                  runat="server" CommandName="Sort" CssClass="AscButton" />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="JobOrdersClosedSortDescButton"
                                  runat="server" CommandName="Sort" CssClass="DescButton" />
                      </div>
                </th>
                <th runat="server" id="JobOrdersCreatedColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="JobOrdersCreatedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="JobOrdersCreatedSortAscButton"
                                  runat="server" CommandName="Sort" CssClass="AscButton" />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="JobOrdersCreatedSortDescButton"
                                  runat="server" CommandName="Sort" CssClass="DescButton" />
                      </div>
                </th>
                <th runat="server" id="JobOrdersEditedColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="JobOrdersEditedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="JobOrdersEditedSortAscButton"
                                  runat="server" CommandName="Sort" CssClass="AscButton" />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="JobOrdersEditedSortDescButton"
                                  runat="server" CommandName="Sort" CssClass="DescButton" />
                      </div>
                </th>
                <th runat="server" id="InvitationsSentColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="InvitationsSentHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="InvitationsSentSortAscButton"
                                  runat="server" CommandName="Sort" CssClass="AscButton" />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="InvitationsSentSortDescButton"
                                  runat="server" CommandName="Sort" CssClass="DescButton" />
                      </div>
                </th>
                <th runat="server" id="JobOrdersPutOnHoldColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="JobOrdersPutOnHoldHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="JobOrdersPutOnHoldSortAscButton"
                                  runat="server" CommandName="Sort" CssClass="AscButton" />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="JobOrdersPutOnHoldSortDescButton"
                                  runat="server" CommandName="Sort" CssClass="DescButton" />
                      </div>
                </th>
                <th runat="server" id="JobOrdersPostedColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="JobOrdersPostedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="JobOrdersPostedSortAscButton"
                                  runat="server" CommandName="Sort" CssClass="AscButton" />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="JobOrdersPostedSortDescButton"
                                  runat="server" CommandName="Sort" CssClass="DescButton" />
                      </div>
                </th>
                <th runat="server" id="JobOrdersRefreshedColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="JobOrdersRefreshedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="JobOrdersRefreshedSortAscButton"
                                  runat="server" CommandName="Sort" CssClass="AscButton" />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="JobOrdersRefreshedSortDescButton"
                                  runat="server" CommandName="Sort" CssClass="DescButton" />
                      </div>
                </th>
                <th runat="server" id="SelfReferralsColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="SelfReferralsHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="SelfReferralsSortAscButton"
                                  runat="server" CommandName="Sort" CssClass="AscButton" />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="SelfReferralsSortDescButton"
                                  runat="server" CommandName="Sort" CssClass="DescButton" />
                      </div>
                </th>
                <th runat="server" id="StaffReferralsColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="StaffReferralsHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="StaffReferralsSortAscButton"
                                  runat="server" CommandName="Sort" CssClass="AscButton" />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="StaffReferralsSortDescButton"
                                  runat="server" CommandName="Sort" CssClass="DescButton" />
                      </div>
                </th>
                <th runat="server" id="JobSeekersInterviewedColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="JobSeekersInterviewedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="JobSeekersInterviewedSortAscButton"
                                  runat="server" CommandName="Sort" CssClass="AscButton" />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="JobSeekersInterviewedSortDescButton"
                                  runat="server" CommandName="Sort" CssClass="DescButton" />
                      </div>
                </th>
                <th runat="server" id="JobSeekersHiredColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="JobSeekersHiredHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="JobSeekersHiredSortAscButton"
                                  runat="server" CommandName="Sort" CssClass="AscButton" />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="JobSeekersHiredSortDescButton"
                                  runat="server" CommandName="Sort" CssClass="DescButton" />
                      </div>
                </th>
                <th runat="server" id="JobSeekersNotHiredColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="JobSeekersNotHiredHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="JobSeekersNotHiredSortAscButton"
                                  runat="server" CommandName="Sort" CssClass="AscButton" />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="JobSeekersNotHiredSortDescButton"
                                  runat="server" CommandName="Sort" CssClass="DescButton" />
                      </div>
                </th>
                </tr>
                <asp:PlaceHolder ID="EmployerListPlaceHolder" runat="server" />
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td nowrap="nowrap">
                    <asp:LinkButton runat="server" ID="EmployerLink" CommandName="Employer" OnCommand="EmployerLink_OnCommand"></asp:LinkButton>
                </td>
                <td id="CountyCell" runat="server">
                    <asp:Literal runat="server" ID="CountyLiteral"></asp:Literal>
                </td>
                <td id="StateCell" runat="server">
                    <asp:Literal runat="server" ID="StateLiteral"></asp:Literal>
                </td>
                <td id="OfficeCell" runat="server" style="white-space: nowrap">
                    <asp:Literal runat="server" ID="OfficeLiteral"></asp:Literal>
                    <asp:HyperLink runat="server" ID="ShowAdditionalOfficesLink" Visible="False" NavigateUrl="javascript:void(0);"  onclick="Report_ShowAdditionalOffices($(this));" ClientIDMode="Predictable"></asp:HyperLink>
                    <span style="display: none">
                      <asp:Literal runat="server" ID="AdditionalOfficesLiteral"></asp:Literal>
                      <a href="javascript:void(0);" onclick="Report_HideAdditionalOffices($(this));"><%=HtmlLocalise("Hide.text", "Hide") %></a>
                    </span>
                </td>
                <td id="JobOrdersClosedCell" runat="server">
                    <asp:Literal runat="server" ID="JobOrdersClosedLiteral"></asp:Literal>
                </td>
                <td id="JobOrdersCreatedCell" runat="server">
                    <asp:Literal runat="server" ID="JobOrdersCreatedLiteral"></asp:Literal>
                </td>
                <td id="JobOrdersEditedCell" runat="server">
                    <asp:Literal runat="server" ID="JobOrdersEditedLiteral"></asp:Literal>
                </td>
                <td id="InvitationsSentCell" runat="server">
                    <asp:Literal runat="server" ID="InvitationsSentLiteral"></asp:Literal>
                </td>
                <td id="JobOrdersPutOnHoldCell" runat="server">
                    <asp:Literal runat="server" ID="JobOrdersPutOnHoldLiteral"></asp:Literal>
                </td>
                <td id="JobOrdersPostedCell" runat="server">
                    <asp:Literal runat="server" ID="JobOrdersPostedLiteral"></asp:Literal>
                </td>
                <td id="JobOrdersRefreshedCell" runat="server">
                    <asp:Literal runat="server" ID="JobOrdersRefreshedLiteral"></asp:Literal>
                </td>
                <td id="SelfReferralsCell" runat="server">
                    <asp:Literal runat="server" ID="SelfReferralsLiteral"></asp:Literal>
                </td>
                <td id="StaffReferralsCell" runat="server">
                    <asp:Literal runat="server" ID="StaffReferralsLiteral"></asp:Literal>
                </td>
                <td id="JobSeekersInterviewedCell" runat="server">
                    <asp:Literal runat="server" ID="JobSeekersInterviewedLiteral"></asp:Literal>
                </td>
                <td id="JobSeekersHiredCell" runat="server">
                    <asp:Literal runat="server" ID="JobSeekersHiredLiteral"></asp:Literal>
                </td>
                <td id="JobSeekersNotHiredCell" runat="server">
                    <asp:Literal runat="server" ID="JobSeekersNotHiredLiteral"></asp:Literal>
                </td>
            </tr>
        </ItemTemplate>
        <EmptyDataTemplate><%= HtmlLocalise("JobOrdersReport.EmptyDataTemplate.Text", "No #BUSINESSES#:LOWER found matching the criteria you chose.")%></EmptyDataTemplate>
    </asp:ListView>
</div>
<asp:ObjectDataSource runat="server" TypeName="Focus.Web.WebReporting.Controls.EmployerReportList"
    ID="EmployerListDataSource" EnablePaging="True" SelectMethod="GetEmployerList"
    SelectCountMethod="GetEmployerListCount" OnSelecting="EmployerListDataSource_Selecting"
    SortParameterName="orderby" />


<uc:ColumnSelector Id="ColumnSelector" runat="server" OnReportColumnsChanged="ColumnSelector_ReportColumnsChanged" />