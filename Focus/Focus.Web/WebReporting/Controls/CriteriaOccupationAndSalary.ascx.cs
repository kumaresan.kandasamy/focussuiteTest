﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;

using Focus.Core;
using Focus.Core.Criteria.Report;
using Framework.Core;
using System.Collections.Generic;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaOccupationAndSalary : UserControlBase
	{
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        ExperienceIndustriesCriteria.Visible = App.Settings.ShowExpIndustries;
        TargetIndustriesCriteria.Visible = App.Settings.ShowTargetIndustries;
        Localise();
        Bind();
      }
    }

    /// <summary>
    /// Resets this instance.
    /// </summary>
    private void Reset(ReportType reportType)
    {
      DetailedOccupationTextBox.Text =
        MOCCodeTextBox.Text =
        MOCTitleTextBox.Text =
        LowSalaryTextBox.Text =
        HighSalaryTextBox.Text =
        HiddenMOCCodeList.Value =
        HiddenMOCTitleList.Value = 
        HiddenOccupationGroupList.Value = 
        HiddenDetailedOccupationList.Value =
        HiddenTargetIndustryList.Value = 
        HiddenExperienceIndustryList.Value = string.Empty;
      
      SalaryRangeDropDown.SelectedIndex = 0;

      ExperienceIndustrialClassificationValidator.ValidationGroup =
        TargetIndustrialClassificationValidator.ValidationGroup =
      SalaryFromValueValidator.ValidationGroup =
        SalaryFromNumericValidator.ValidationGroup =
        SalaryToNumericValidator.ValidationGroup =
        SalaryToValueValidator.ValidationGroup = 
        SalaryToFromComparisonValidator.ValidationGroup = reportType.ToString();

      
    }

    /// <summary>
    /// Unbinds the salary.
    /// </summary>
    /// <returns></returns>
    public SalaryReportCriteria UnbindSalary()
    {
      var criteria = new SalaryReportCriteria();

      decimal salary;
      if (decimal.TryParse(LowSalaryTextBox.Text, out salary))
        criteria.MinimumSalary = salary;
      if (decimal.TryParse(HighSalaryTextBox.Text, out salary))
        criteria.MaximumSalary = salary;

      if (criteria.MinimumSalary.HasValue || criteria.MaximumSalary.HasValue)
        criteria.SalaryFrequency = SalaryRangeDropDown.SelectedValueToEnum<ReportSalaryFrequency>();

      return criteria;
    }

    public void BindCriteria(SalaryReportCriteria salCriteria, OccupationsReportCriteria occCriteria, IndustriesReportCriteria indCriteria, ReportType reportType)
    {
      Reset(reportType);

      if (salCriteria.IsNotNull())
      {
        if (salCriteria.MinimumSalary.HasValue) LowSalaryTextBox.Text = salCriteria.MinimumSalary.ToString();
        if (salCriteria.MaximumSalary.HasValue) HighSalaryTextBox.Text = salCriteria.MaximumSalary.ToString();
        if (salCriteria.SalaryFrequency.HasValue) SalaryRangeDropDown.SelectedValue = salCriteria.SalaryFrequency.ToString();
      }

      if (occCriteria.IsNotNull())
      {
        const string delimiter = "|*|";
        if (occCriteria.MilitaryOccupationCodes.IsNotNullOrEmpty())
          HiddenMOCCodeList.Value = string.Join(delimiter, occCriteria.MilitaryOccupationCodes);
        if (occCriteria.MilitaryOccupationTitles.IsNotNullOrEmpty())
          HiddenMOCTitleList.Value = string.Join(delimiter, occCriteria.MilitaryOccupationTitles);
      }

      if (indCriteria.IsNotNull())
      {
        const string delimiter = "|*|";
        if (indCriteria.MostExperienceIndustries.IsNotNullOrEmpty())
          HiddenExperienceIndustryList.Value = string.Join(delimiter, indCriteria.MostExperienceIndustries);
        if (indCriteria.TargetIndustries.IsNotNullOrEmpty())
          HiddenTargetIndustryList.Value = string.Join(delimiter, indCriteria.TargetIndustries);
      }
    }

    /// <summary>
    /// Unbinds the occupations.
    /// </summary>
    /// <returns></returns>
    public OccupationsReportCriteria UnbindOccupations()
    {
      var delimiter = new[] {"|*|"};
      var criteria = new OccupationsReportCriteria
                       {
                         MilitaryOccupationCodes = HiddenMOCCodeList.Value.Split(delimiter, StringSplitOptions.RemoveEmptyEntries).ToList(),
                         MilitaryOccupationTitles = HiddenMOCTitleList.Value.Split(delimiter, StringSplitOptions.RemoveEmptyEntries).ToList()
                       };

      return criteria;
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      #region labels

      CriteriaOccupationAndSalaryLabel.Text = CodeLocalise("CriteriaOccupationAndSalaryLabel.Text", "Occupation & salary");
      OccupationGroupHeader.Text = CodeLocalise("OccupationGroupHeader.Text", "Occupation group");
      DetailedOccupationHeader.Text = CodeLocalise("DetailedOccupationHeader.Text", "Detailed occupation");

      OccupationGroupButton.Value = DetailedOccupationButton.Value = MOCCodeButton.Value = MOCTitleButton.Value = CodeLocalise("Global.Add.NoEdit", "Add");
      ExperienceIndustryAddButton.Value = CodeLocalise("Global.Add.NoEdit", "Add");
      TargetIndustryAddButton.Value = CodeLocalise("Global.Add.NoEdit", "Add");

      #endregion

      #region error messages

      SalaryFromValueValidator.ErrorMessage = CodeLocalise("OccupationAndSalary.SalaryFromValueValidator.ErrorMessage",
                                                       "Occupation and salary - \"Salary From\" must be greater than 0");
      SalaryToValueValidator.ErrorMessage = CodeLocalise("OccupationAndSalary.SalaryToValueValidator.ErrorMessage",
                                                        "Occupation and salary - \"Salary To\" must be greater than 0");
      SalaryToFromComparisonValidator.ErrorMessage = CodeLocalise("OccupationAndSalary.SalaryToFromComparisonValidator.ErrorMessage",
                                                        "Occupation and salary - \"Salary To\" must be greater than or equal to \"Salary From\"");
      SalaryToNumericValidator.ErrorMessage = CodeLocalise("OccupationAndSalary.SalaryToNumericValidator.ErrorMessage",
                                                           "Occupation and salary - Upper salary value must be numeric or blank");
      SalaryFromNumericValidator.ErrorMessage = CodeLocalise("OccupationAndSalary.SalaryFromNumericValidator.ErrorMessage",
                                                           "Occupation and salary - Lower salary value must be numeric or blank");

      #endregion
    }

    /// <summary>
    /// Binds this instance.
    /// </summary>
    private void Bind()
    {
      SalaryRangeDropDown.Items.Add(new ListItem(CodeLocalise("SalaryRangeDropDown.Hourly.Text.NoEdit", "hourly"), ReportSalaryFrequency.Hourly.ToString()));
      SalaryRangeDropDown.Items.Add(new ListItem(CodeLocalise("SalaryRangeDropDown.Daily.Text.NoEdit", "daily"), ReportSalaryFrequency.Daily.ToString()));
      SalaryRangeDropDown.Items.Add(new ListItem(CodeLocalise("SalaryRangeDropDown.Weekly.Text.NoEdit", "weekly"), ReportSalaryFrequency.Weekly.ToString()));
      SalaryRangeDropDown.Items.Add(new ListItem(CodeLocalise("SalaryRangeDropDown.Montly.Text.NoEdit", "monthly"), ReportSalaryFrequency.Monthly.ToString()));
      SalaryRangeDropDown.Items.Add(new ListItem(CodeLocalise("SalaryRangeDropDown.Yearly.Text.NoEdit", "yearly"), ReportSalaryFrequency.Yearly.ToString()));
    }

    public IndustriesReportCriteria UnbindIndustries()
    {  
      var delimiter = new[] { "|*|" };

      var industries = HiddenExperienceIndustryList.Value.Split(delimiter, StringSplitOptions.RemoveEmptyEntries).ToList();
      var industrycodes = industries.Select(industry => industry.Split('-').FirstOrDefault()).ToList();

      var targetindustries = HiddenTargetIndustryList.Value.Split(delimiter, StringSplitOptions.RemoveEmptyEntries).ToList();
      var targetindustrycodes = targetindustries.Select(industry => industry.Split('-').FirstOrDefault()).ToList();

      var criteria = new IndustriesReportCriteria
      {
        MostExperienceIndustriesCodes = industrycodes,
        MostExperienceIndustries = industries,
        TargetIndustries = targetindustries,
        TargetIndustriesCodes = targetindustrycodes
      };
     
      return criteria;
    }
  }
}