﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaEmployerCharacteristics.ascx.cs" Inherits="Focus.Web.WebReporting.Controls.CriteriaEmployerCharacteristics" %>
<table role="presentation" style="width:100%;" class="accordionWithIcon">
    <tr class="multipleAccordionTitle" id="EmployerCharacteristicsPanelHeader">
        <td style="width:10%; vertical-align:top;" >
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td style="width:90%;">
              <asp:Literal runat="server" ID="CriteriaEmployerCharacteristicsLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
             <table role="presentation" style="width:100%;">
                <tr>
                    <td>
                        <strong><asp:Literal runat="server" ID="WOTCInterestHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                  <td>
                    <asp:CheckBoxList ID="WOTCInterestCheckBoxList" TextAlign="Right" runat="server" role="presentation" />
                  </td>
                </tr>
            </table>
            <table role="presentation" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong><%=HtmlLabel(FEINTextBox,"FEINHeader.Text", "FEIN") %></strong>
                    </td>
                </tr>
                <tr>
                    <td>
											<div style="position: relative;">
                        <asp:TextBox runat="server" ID="FEINTextBox" MaxLength="10"></asp:TextBox>
												<div style="position:absolute; width:500px;">
													<act:AutoCompleteExtender ID="FEINTextBoxAutoCompleteExtender" runat="server" TargetControlID="FEINTextBox" MinimumPrefixLength="2" CompletionListCssClass="autocompleteCompletionList" CompletionInterval="100" ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetReportingEmployerFEIN" CompletionListElementID="FEINTextBoxAutoCompleteExtenderCompletionList" />
													<div id="FEINTextBoxAutoCompleteExtenderCompletionList" class="autocompleteCompletionList"></div>
												</div>
											</div>
                    </td>
                    <td>
                        <input type="button" class="button3" runat="server" id="FEINButton" name="FEINButton" />
                    </td>
                </tr>
                    <tr>
                        <td colspan="2"><table role="presentation" id="FEINTable" style="width:75%;"><tbody></tbody></table><asp:HiddenField runat="server" ID="HiddenFEINList"/></td>
                    </tr>
            </table>
            <table role="presentation" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                            <%= HtmlLabel(EmployersTextBox, "EmployersHeader.Text", "#BUSINESSES#")%></strong>
                    </td>
                </tr>
                <tr>
                    <td>
	                    <div style="position: relative;">
                        <asp:TextBox runat="server" ID="EmployersTextBox"></asp:TextBox>
												<div style="position:absolute; width:500px;">
													<act:AutoCompleteExtender ID="EmployersAutoCompleteExtender" runat="server" TargetControlID="EmployersTextBox" MinimumPrefixLength="2" CompletionListCssClass="autocompleteCompletionList" CompletionInterval="100" ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetReportingEmployerNames" CompletionListElementID="EmployersAutoCompleteExtenderCompletionList" />
													<div id="EmployersAutoCompleteExtenderCompletionList" class="autocompleteCompletionList"></div>
												</div>
											</div>
                    </td>
                    <td>
                        <input type="button" class="button3" runat="server" id="EmployersButton" name="EmployersButton" />
                    </td>
                </tr>
                    <tr>
                        <td colspan="2"><table role="presentation" id="EmployersTable" style="width:75%;"><tbody></tbody></table><asp:HiddenField runat="server" ID="HiddenEmployersList"/></td>
                    </tr>
            </table>
            <table role="presentation" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                            <%=HtmlLabel(JobTitlesTextBox,"JobTitlesHeader.Text", "Job titles")%></strong>
                    </td>
                </tr>
                <tr>
                    <td>
	                    <div style="position: relative;">
                        <asp:TextBox runat="server" ID="JobTitlesTextBox"></asp:TextBox>
												<div style="position:absolute; width:500px;">
													<act:AutoCompleteExtender ID="JobTitlesAutoCompleteExtender" runat="server" TargetControlID="JobTitlesTextBox" MinimumPrefixLength="2" CompletionListCssClass="autocompleteCompletionList" CompletionInterval="100" ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetReportingJobOrderTitles" CompletionListElementID="JobTitlesAutoCompleteExtenderCompletionList" />
													<div id="JobTitlesAutoCompleteExtenderCompletionList" class="autocompleteCompletionList"></div>
												</div>
											</div>
                    </td>
                    <td>
                        <input type="button" class="button3" runat="server" id="JobTitlesButton" name="JobTitlesButton" />
                    </td>
                </tr>
                    <tr>
                        <td colspan="2"><table role="presentation" id="JobTitlesTable" style="width:75%;" ><tbody></tbody></table><asp:HiddenField runat="server" ID="HiddenJobTitlesList"/></td>
                    </tr>
            </table>
            <table role="presentation" style="width:100%;">
						 <tr>
                    <td colspan="2">
						                        <strong id="AccountTypeTitle"><asp:Literal runat="server" ID="AccountTypeHeader" ></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                  <td>
	                  <asp:CheckBoxList ID="AccountTypeCheckBoxList" TextAlign="Right" runat="server" role="presentation"/>
                  </td>
                </tr>
            </table>
						 <table role="presentation" id="NoOfEmployeesCriteria" runat="server" style="width:100%;">
                <tr>
                    <td colspan="4">
                        <strong>
                          <focus:LocalisedLabel id="NumberOfEmployeesLabel" runat="server" LocalisationKey="NoOfEmployees.Label" DefaultText="Number of Employees" /></strong>
                    </td>

                </tr>
                <tr>
                  <td colspan="2"></td>
                   <td>  <asp:DropDownList ID="NumberOfEmployeesDropDownList" title="Number Of Employees DropDownList" runat="server"></asp:DropDownList></td>
                </tr>
            </table>

        </td>
    </tr>
</table>
<script type="text/javascript">
$(document).ready(function () {
        $("#<%=FEINButton.ClientID%>").click(function () {
            AddListItem('<%=FEINTextBox.ClientID %>', '<%=HiddenFEINList.ClientID %>', 'FEINTable');
        });
        $("#<%=EmployersButton.ClientID%>").click(function () {
            AddListItem('<%=EmployersTextBox.ClientID %>', '<%=HiddenEmployersList.ClientID %>', 'EmployersTable');
        });
        $("#<%=JobTitlesButton.ClientID%>").click(function () {
            AddListItem('<%=JobTitlesTextBox.ClientID %>', '<%=HiddenJobTitlesList.ClientID %>', 'JobTitlesTable');
        });
        
        BuildTableFromScratch('<%=HiddenFEINList.ClientID%>', 'FEINTable');
        BuildTableFromScratch('<%=HiddenEmployersList.ClientID%>', 'EmployersTable');
        BuildTableFromScratch('<%=HiddenJobTitlesList.ClientID%>', 'JobTitlesTable');

    });
</script>