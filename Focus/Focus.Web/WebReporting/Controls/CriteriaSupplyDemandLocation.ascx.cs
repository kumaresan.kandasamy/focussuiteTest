﻿using System;
using System.Linq;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.Report;

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaSupplyDemandLocation : UserControlBase
  {
    public ReportType ReportType { get; set; }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (IsPostBack) return;
      BindDropdowns();
      Localise();
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      CriteriaLocationLabel.Text = CodeLocalise("CriteriaLocationLabel.Text", "Location");
    }

    /// <summary>
    /// Binds the dropdowns.
    /// </summary>
    private void BindDropdowns()
    {
      try
      {
        var nearbyStates = (from s in ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States) where App.Settings.NearbyStateKeys.Contains(s.Key) select s).ToList();
        ddlState.BindLookup(nearbyStates, null, CodeLocalise("State.TopDefault.NoEdit", "- select state -"), "0");
        ddlMSA.Items.AddLocalisedTopDefault("City.SelectCity.TopDefault", "- select city -");
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Unbinds this instance.
    /// </summary>
    /// <returns></returns>
    public SupplyDemandLocationReportCriteria Unbind()
    {
      var criteria = new SupplyDemandLocationReportCriteria
      {
        State = new Tuple<long, string>(ddlState.SelectedValueToLong(), ddlState.SelectedItem.Text),
        MSA = new Tuple<long, string>(ddlMSA.SelectedValueToLong(), ddlMSA.SelectedItem.Text)
      };
      
      return criteria;
    }
  }
}