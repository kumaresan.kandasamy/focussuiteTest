﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Core;
using Focus.Core.Criteria.Report;

#endregion

namespace Focus.Web.WebReporting.Controls
{
	public partial class CriteriaSelector : UserControlBase
	{
		public ReportType CurrentReportType
		{
			get { return GetViewStateValue<ReportType>("CriteriaSelector:CurrentReportType"); }
			set { SetViewStateValue("CriteriaSelector:CurrentReportType", value); }
		}

		public JobSeekersReportExtraColumns JobSeekerExtraColumns
		{
			get { return GetViewStateValue<JobSeekersReportExtraColumns>("CriteriaSelector:JobSeekerExtraColumns"); }
			set { SetViewStateValue("CriteriaSelector:JobSeekerExtraColumns", value); }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				Localise();
				Bind();
			}
		}

		/// <summary>
		/// Handles the OnClick event of the RunReportButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void RunReportButton_OnClick(object sender, EventArgs e)
		{
			switch (CurrentReportType)
			{
				case ReportType.JobSeeker:
					OnRunReportClicked(new CommandEventArgs(ReportType.JobSeeker.ToString(), UnBindJobSeekerCriteria()));
					break;
				case ReportType.JobOrder:
					OnRunReportClicked(new CommandEventArgs(ReportType.JobOrder.ToString(), UnBindJobOrderCriteria()));
					break;
				case ReportType.Employer:
					OnRunReportClicked(new CommandEventArgs(ReportType.Employer.ToString(), UnBindEmployerCriteria()));
					break;
				case ReportType.SupplyDemand:
					OnRunReportClicked(new CommandEventArgs(ReportType.SupplyDemand.ToString(), UnBindSupplyDemandCriteria()));
					break;
			}
		}

		/// <summary>
		/// Shows the specified report criteria
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		public void Show(IReportCriteria criteria)
		{
			Visible = true;
			CriteriaSelectorPopup.Show();
			CurrentReportType = criteria.ReportType;
			ResetCriteriaControls();
			HideAllCriteriaControls();

			switch (CurrentReportType)
			{
				case ReportType.JobSeeker:
					PrepareJobSeekerReport((JobSeekersReportCriteria)criteria);
					break;
				case ReportType.JobOrder:
					PrepareJobOrderReport((JobOrdersReportCriteria)criteria);
					break;
				case ReportType.Employer:
					PrepareEmployerReport((EmployersReportCriteria)criteria);
					break;
				case ReportType.SupplyDemand:
					PrepareSupplyDemandReport((SupplyDemandReportCriteria)criteria);
					break;
			}
			ReportDisplayTypeList.SelectedValue = criteria.DisplayType.ToString();
			// Validation specific
			RunReportButton.ValidationGroup = CurrentReportType.ToString();
			CriteriaValidationSummary.ValidationGroup = CurrentReportType.ToString();
		}

		/// <summary>
		/// Localises this instance.
		/// </summary>
		private void Localise()
		{
			LocaliseCloseButton.Text = EditLocalisationLink("Global.Close.Text.NoEdit");
			RunReportButton.Text = CodeLocalise("RunReportButton.Text.NoEdit", "Run report") + @" >";
			GroupedByLiteral.Text = CodeLocalise("GroupedByLiteral.Text", "grouped by");
			Column4Header.Text = CodeLocalise("Column4Header.Text", "Run report as;");
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			ReportDisplayTypeList.Items.Add(new ListItem(CodeLocalise(ReportDisplayType.Table, "table"), ReportDisplayType.Table.ToString()));
			ReportDisplayTypeList.Items.Add(new ListItem(CodeLocalise(ReportDisplayType.BarChart, "bar chart"), ReportDisplayType.BarChart.ToString()));
			ReportDisplayTypeList.Items[0].Selected = true;
		}

		/// <summary>
		/// Resets all criteria controls.
		/// </summary>
		private void ResetCriteriaControls()
		{
			ReportDisplayTypeList.Items[0].Selected = true;
		}

		/// <summary>
		/// Hides all criteria controls.
		/// </summary>
		private void HideAllCriteriaControls()
		{
			CriteriaJobSeekerStatus.Visible = false;
			CriteriaVeteranStatus.Visible = false;
			CriteriaJobSeekerCharacteristics.Visible = false;
			CriteriaJobSeekerQualifications.Visible = false;
			CriteriaOccupationAndSalary.Visible = false;
			CriteriaKeywordAndContext.Visible = false;
			CriteriaResume.Visible = false;
			CriteriaJobSeekerActivitySummary.Visible = false;
			CriteriaJobSeekerActivityLog.Visible = false;
			CriteriaReferralsOutcome.Visible = false;
			Column2CriteriaLocation.Visible = false;
			Column2CriteriaDateRange.Visible = false;
			Column3CriteriaLocation.Visible = false;
			Column3CriteriaDateRange.Visible = false;
			CriteriaJobCharacteristics.Visible = false;
			CriteriaEmployerCharacteristics.Visible = false;
			CriteriaCompliance.Visible = false;
			CriteriaJobActivitySummary.Visible = false;
			CriteriaEmployerActivity.Visible = false;
			CriteraNcrc.Visible = false;
		  CriteriaIndividualJobSeekerCharacteristics.Visible = false;
			CriteriaJobSeekerNcrc.Visible = false;
			CriteriaOccupation.Visible = false;
			Column2CriteriaSupplyDemandLocation.Visible = false;
		}

		#region Report specific

		#region Employer report

		private void PrepareEmployerReport(EmployersReportCriteria criteria)
		{
			SetEmployerReportCriteriaVisibility();

			CriteriaJobCharacteristics.BindCriteria(ReportType.Employer, criteria.SalaryInfo, criteria.JobOrderCharacteristicsInfo, criteria.KeywordInfo);
			CriteriaEmployerCharacteristics.BindCriteria(criteria.EmployerCharacteristicsInfo);
			CriteriaCompliance.BindCriteria(criteria.JobOrderComplianceInfo);
			Column3CriteriaLocation.BindCriteria(criteria.LocationInfo, criteria.ReportType);
			Column3CriteriaDateRange.BindCriteria(criteria.DateRangeInfo, criteria.ReportType);

			GroupByDropDownList.Items.Clear();
			//GroupByDropDownList.Items.Add(new ListItem(CodeLocalise(ReportEmployerGroupBy.Office, "office"), ReportEmployerGroupBy.Office.ToString()));
			GroupByDropDownList.Items.Add(new ListItem(CodeLocalise(ReportEmployerGroupBy.State, "state"), ReportEmployerGroupBy.State.ToString()));

			if (criteria.DisplayType == ReportDisplayType.BarChart)
				GroupByDropDownList.SelectedValue = criteria.ChartGroupInfo.GroupBy.ToString();
		}

		private void SetEmployerReportCriteriaVisibility()
		{
			LocaliseEmployerReport();
			Column3CriteriaLocation.ReportType = ReportType.Employer;
			Column3CriteriaLocation.ShowCounty = true;
			Column3CriteriaLocation.Visible = true;
			Column3CriteriaDateRange.Visible = true;
			Column3Panel.Visible = true;
			CriteriaJobCharacteristics.Visible = true;
			CriteriaEmployerCharacteristics.Visible = true;
			CriteriaCompliance.Visible = true;
			CriteriaEmployerActivity.Visible = true;
		  CriteriaIndividualJobSeekerCharacteristics.Visible = false;
		}

		/// <summary>
		/// Localises the job order report.
		/// </summary>
		private void LocaliseEmployerReport()
		{
			CriteriaHeader.Text = CodeLocalise("CriteriaHeader.Employer.Text", "Report on #BUSINESSES#:LOWER");
			Column1Header.Text = CodeLocalise("Column1Header.Employer.Text", "Which #BUSINESSES#:LOWER are you looking for?");
			Column2Header.Text = CodeLocalise("Column2Header.Employer.Text", "View the #BUSINESS#:LOWER activity");
			Column3Header.Text = CodeLocalise("Column3Header.Employer.Text", "Where are the #BUSINESSES#:LOWER located, and what's the overall time period for this report?");
		}

		private IReportCriteria UnBindEmployerCriteria()
		{
			EmployersReportChartGroup chartGroup = null;
			var reportDisplayType =
				(ReportDisplayType)Enum.Parse(typeof(ReportDisplayType), ReportDisplayTypeList.SelectedValue);
			if (reportDisplayType == ReportDisplayType.BarChart)
				chartGroup = new EmployersReportChartGroup
				{
					GroupBy = GroupByDropDownList.Items.Cast<ListItem>().Where(x => x.Selected).Select(
						x => (ReportEmployerGroupBy?)Enum.Parse(typeof(ReportEmployerGroupBy), x.Value)).
						SingleOrDefault()
				};

			return new EmployersReportCriteria
			{
				SalaryInfo = CriteriaJobCharacteristics.UnbindSalary(),
				JobOrderCharacteristicsInfo = CriteriaJobCharacteristics.UnbindCharacteristics(),
				KeywordInfo = CriteriaJobCharacteristics.UnbindKeyWords(),
				EmployerCharacteristicsInfo = CriteriaEmployerCharacteristics.Unbind(),
				JobOrderComplianceInfo = CriteriaCompliance.Unbind(),
				EmployersEmployerActivityInfo = CriteriaEmployerActivity.Unbind(),
				DateRangeInfo = Column3CriteriaDateRange.UnBind(),
				LocationInfo = Column3CriteriaLocation.Unbind(),
				DisplayType = reportDisplayType,
				ChartGroupInfo = chartGroup
			};
		}

		#endregion

		#region Job order report

		private void PrepareJobOrderReport(JobOrdersReportCriteria criteria)
		{
			SetJobOrderReportCriteriaVisibility();

			CriteriaJobCharacteristics.BindCriteria(ReportType.JobOrder, criteria.SalaryInfo, criteria.JobOrderCharacteristicsInfo, criteria.KeywordInfo);
			CriteriaJobActivitySummary.BindCriteria(criteria.JobOrdersActivitySummaryInfo);
			CriteriaReferralsOutcome.BindCriteria(criteria.JobOrdersReferralOutcomesInfo);
			CriteriaEmployerCharacteristics.BindCriteria(criteria.EmployerCharacteristicsInfo);
			Column3CriteriaLocation.BindCriteria(criteria.LocationInfo, criteria.ReportType);
			Column3CriteriaDateRange.BindCriteria(criteria.DateRangeInfo, criteria.ReportType);
			CriteraNcrc.BindCriteria(criteria.NcrcInfo);

			GroupByDropDownList.Items.Clear();
			//GroupByDropDownList.Items.Add(new ListItem(CodeLocalise(ReportJobOrderGroupBy.Office, "office"), ReportJobOrderGroupBy.Office.ToString()));
			GroupByDropDownList.Items.Add(new ListItem(CodeLocalise(ReportJobOrderGroupBy.State, "state"), ReportJobOrderGroupBy.State.ToString()));

			if (criteria.DisplayType == ReportDisplayType.BarChart)
				GroupByDropDownList.SelectedValue = criteria.ChartGroupInfo.GroupBy.ToString();
		}

		private void SetJobOrderReportCriteriaVisibility()
		{
			LocaliseJobOrderReport();
			Column3CriteriaLocation.ReportType = ReportType.JobOrder;
			Column3CriteriaLocation.ShowCounty = true;
			Column3CriteriaLocation.Visible = true;
			Column3CriteriaDateRange.Visible = true;
			CriteriaJobCharacteristics.Visible = true;
			CriteriaEmployerCharacteristics.Visible = true;
			CriteriaCompliance.Visible = true;
			CriteraNcrc.Visible = App.Settings.Theme == FocusThemes.Workforce;
            CriteraNcrc.Visible = App.Settings.EnableNCRCFeatureGroup;
			CriteriaJobActivitySummary.Visible = true;
			CriteriaReferralsOutcome.Visible = true;
		  CriteriaIndividualJobSeekerCharacteristics.Visible = false;
			Column3Panel.Visible = true;
		}

		/// <summary>
		/// Localises the job order report.
		/// </summary>
		private void LocaliseJobOrderReport()
		{
			CriteriaHeader.Text = CodeLocalise("CriteriaHeader.JobOrder.Text", "Report on #EMPLOYMENTTYPES#:LOWER");
			Column1Header.Text = CodeLocalise("Column1Header.JobOrder.Text", "Which #EMPLOYMENTTYPES#:LOWER are you looking for?");
			Column2Header.Text = CodeLocalise("Column2Header.JobOrder.Text", "How are the #EMPLOYMENTTYPES#:LOWER progressing?");
			Column3Header.Text = CodeLocalise("Column3Header.JobOrder.Text", "Where are the #EMPLOYMENTTYPES#:LOWER located, and what's the overall time period for this report?");
		}

		private IReportCriteria UnBindJobOrderCriteria()
		{
			JobOrdersReportChartGroup chartGroup = null;
			var reportDisplayType =
				(ReportDisplayType)Enum.Parse(typeof(ReportDisplayType), ReportDisplayTypeList.SelectedValue);
			if (reportDisplayType == ReportDisplayType.BarChart)
				chartGroup = new JobOrdersReportChartGroup
				{
					GroupBy = GroupByDropDownList.Items.Cast<ListItem>().Where(x => x.Selected).Select(
						x => (ReportJobOrderGroupBy?)Enum.Parse(typeof(ReportJobOrderGroupBy), x.Value)).
						SingleOrDefault()
				};

			return new JobOrdersReportCriteria
			{
				SalaryInfo = CriteriaJobCharacteristics.UnbindSalary(),
				JobOrderCharacteristicsInfo = CriteriaJobCharacteristics.UnbindCharacteristics(),
				EmployerCharacteristicsInfo = CriteriaEmployerCharacteristics.Unbind(),
				KeywordInfo = CriteriaJobCharacteristics.UnbindKeyWords(),
				JobOrderComplianceInfo = CriteriaCompliance.Unbind(),
				JobOrdersActivitySummaryInfo = CriteriaJobActivitySummary.Unbind(),
				JobOrdersReferralOutcomesInfo = CriteriaReferralsOutcome.Unbind(),
				DateRangeInfo = Column3CriteriaDateRange.UnBind(),
				LocationInfo = Column3CriteriaLocation.Unbind(),
				DisplayType = reportDisplayType,
				ChartGroupInfo = chartGroup,
				NcrcInfo = CriteraNcrc.Unbind()
			};
		}

		#endregion

		#region Job seeker report

		/// <summary>
		/// Prepares the job seeker report.
		/// </summary>
		private void PrepareJobSeekerReport(JobSeekersReportCriteria criteria)
		{
			SetJobSeekerReportCriteriaVisibility();

			CriteriaJobSeekerStatus.BindCriteria(criteria.JobSeekerStatusInfo);
			CriteriaVeteranStatus.BindCriteria(criteria.VeteranInfo);
			CriteriaJobSeekerCharacteristics.BindCriteria(criteria.CharacteristicsInfo, criteria.ReportType);
			CriteriaJobSeekerQualifications.BindCriteria(criteria.QualificationsInfo);
			CriteriaOccupationAndSalary.BindCriteria(criteria.SalaryInfo, criteria.OccupationInfo, criteria.IndustriesInfo, criteria.ReportType);
			CriteriaKeywordAndContext.BindCriteria(criteria.KeywordInfo);
			CriteriaResume.BindCriteria(criteria.ResumeInfo, criteria.ReportType);
			CriteriaJobSeekerActivitySummary.BindCriteria(criteria.JobSeekerActivitySummaryInfo);
			CriteriaJobSeekerActivityLog.BindCriteria(criteria.JobSeekerActivityLogInfo);
			CriteriaReferralsOutcome.BindCriteria(criteria.JobSeekerReferralOutcomesInfo);
			Column3CriteriaLocation.BindCriteria(criteria.LocationInfo, criteria.ReportType);
			Column3CriteriaDateRange.BindCriteria(criteria.DateRangeInfo, criteria.ReportType);

			JobSeekerExtraColumns = criteria.ExtraColumns;

			GroupByDropDownList.Items.Clear();
			GroupByDropDownList.Items.Add(new ListItem(CodeLocalise(ReportJobSeekerGroupBy.EducationLevel, "education level"), ReportJobSeekerGroupBy.EducationLevel.ToString()));
			GroupByDropDownList.Items.Add(new ListItem(CodeLocalise(ReportJobSeekerGroupBy.EmploymentStatus, "employment status"), ReportJobSeekerGroupBy.EmploymentStatus.ToString()));
			//GroupByDropDownList.Items.Add(new ListItem(CodeLocalise(ReportJobSeekerGroupBy.Office, "office"), ReportJobSeekerGroupBy.Office.ToString()));
			GroupByDropDownList.Items.Add(new ListItem(CodeLocalise(ReportJobSeekerGroupBy.State, "state"), ReportJobSeekerGroupBy.State.ToString()));

			if (criteria.DisplayType == ReportDisplayType.BarChart)
				GroupByDropDownList.SelectedValue = criteria.ChartGroupInfo.GroupBy.ToString();
		}

		/// <summary>
		/// Sets the job seeker report criteria visibility.
		/// </summary>
		private void SetJobSeekerReportCriteriaVisibility()
		{
			LocaliseJobSeekerReport();
			CriteriaJobSeekerStatus.Visible = true;
			CriteriaVeteranStatus.Visible = true;
			CriteriaJobSeekerCharacteristics.Visible = !App.Settings.HideJSCharacteristicsFromReporting;
			CriteriaJobSeekerQualifications.Visible = true;
			CriteriaOccupationAndSalary.Visible = true;
			CriteriaJobSeekerNcrc.Visible = App.Settings.Theme == FocusThemes.Workforce;
            CriteriaJobSeekerNcrc.Visible = App.Settings.EnableNCRCFeatureGroup;
			CriteriaKeywordAndContext.Visible = true;
			CriteriaResume.Visible = true;
			CriteriaJobSeekerActivitySummary.Visible = true;
			CriteriaJobSeekerActivityLog.Visible = true;
			CriteriaReferralsOutcome.Visible = true;
      CriteriaIndividualJobSeekerCharacteristics.Visible = true;
			Column3CriteriaLocation.ReportType = ReportType.JobSeeker;
			Column3CriteriaLocation.ShowCounty = true;
			Column3CriteriaLocation.Visible = true;
			Column3CriteriaDateRange.Visible = true;
			Column3Panel.Visible = true;
		}

		/// <summary>
		/// Localises the job seeker report.
		/// </summary>
		private void LocaliseJobSeekerReport()
		{
			CriteriaHeader.Text = CodeLocalise("CriteriaHeader.JobSeeker.Text", "Report on job seekers");
			Column1Header.Text = CodeLocalise("Column1Header.JobSeeker.Text", "Which job seekers are you looking for?");
			Column2Header.Text = CodeLocalise("Column2Header.JobSeeker.Text", "What have the job seekers been doing?");
			Column3Header.Text = CodeLocalise("Column3Header.JobSeeker.Text", "Where are the job seekers located, and what's the overall time period for this report?");
		}

		/// <summary>
		/// Functions the bind job seeker criteria.
		/// </summary>
		/// <returns></returns>
		private IReportCriteria UnBindJobSeekerCriteria()
		{
			JobSeekersReportChartGroup chartGroup = null;
			var reportDisplayType =
				(ReportDisplayType)Enum.Parse(typeof(ReportDisplayType), ReportDisplayTypeList.SelectedValue);
			if (reportDisplayType == ReportDisplayType.BarChart)
				chartGroup = new JobSeekersReportChartGroup
				{
					GroupBy = GroupByDropDownList.Items.Cast<ListItem>().Where(x => x.Selected).Select(
						x => (ReportJobSeekerGroupBy?)Enum.Parse(typeof(ReportJobSeekerGroupBy), x.Value)).
						SingleOrDefault()
				};

			return new JobSeekersReportCriteria
			{
				DisplayType = reportDisplayType,
				JobSeekerStatusInfo = CriteriaJobSeekerStatus.Unbind(),
				VeteranInfo = CriteriaVeteranStatus.Unbind(),
				CharacteristicsInfo = CriteriaJobSeekerCharacteristics.Unbind(),
				QualificationsInfo = CriteriaJobSeekerQualifications.Unbind(),
				NcrcInfo = CriteriaJobSeekerNcrc.Unbind(),
				DateRangeInfo = Column3CriteriaDateRange.UnBind(),
				SalaryInfo = CriteriaOccupationAndSalary.UnbindSalary(),
				OccupationInfo = CriteriaOccupationAndSalary.UnbindOccupations(),
				IndustriesInfo = CriteriaOccupationAndSalary.UnbindIndustries(),
				KeywordInfo = CriteriaKeywordAndContext.Unbind(),
				ResumeInfo = CriteriaResume.Unbind(),
				JobSeekerActivitySummaryInfo = CriteriaJobSeekerActivitySummary.Unbind(),
				JobSeekerActivityLogInfo = CriteriaJobSeekerActivityLog.Unbind(),
				LocationInfo = Column3CriteriaLocation.Unbind(),
				JobSeekerReferralOutcomesInfo = CriteriaReferralsOutcome.Unbind(),
				IndividualCharacteristicsInfo = CriteriaIndividualJobSeekerCharacteristics.Unbind(),
				ChartGroupInfo = chartGroup,
				ExtraColumns = JobSeekerExtraColumns
			};
		}

		#endregion

		#region Supply and demand report

		/// <summary>
		/// Prepares the supply and demand report.
		/// </summary>
		private void PrepareSupplyDemandReport(SupplyDemandReportCriteria criteria)
		{
			SetSupplyDemandReportCriteriaVisibility();
			CriteriaOccupation.BindCriteria(criteria.OccupationInfo, criteria.ReportType);
			Column2CriteriaDateRange.BindCriteria(criteria.DateRangeInfo, criteria.ReportType);
			GroupByDropDownList.Items.Clear();
			GroupByDropDownList.Items.Add(new ListItem(CodeLocalise(ReportSupplyDemandGroupBy.Onet, "Onet"), ReportSupplyDemandGroupBy.Onet.ToString()));
			GroupByDropDownList.Items.Add(new ListItem(CodeLocalise(ReportSupplyDemandGroupBy.State, "state"), ReportSupplyDemandGroupBy.State.ToString()));
			GroupByDropDownList.Enabled = true;
			if (criteria.DisplayType == ReportDisplayType.BarChart)
				GroupByDropDownList.SelectedValue = criteria.ChartGroupInfo.GroupBy.ToString();
		}

		/// <summary>
		/// Sets the supply demand report criteria visibility.
		/// </summary>
		private void SetSupplyDemandReportCriteriaVisibility()
		{
			LocaliseSupplyDemandReport();
			CriteriaOccupation.Visible = true;
			CriteriaIndustries.Visible = false;
		  CriteriaIndividualJobSeekerCharacteristics.Visible = false;
			Column2CriteriaSupplyDemandLocation.Visible = true;
			Column2CriteriaSupplyDemandLocation.ReportType = ReportType.SupplyDemand;
			Column2CriteriaSupplyDemandLocation.Visible = true;
			Column2CriteriaDateRange.Visible = true;
			Column3Panel.Visible = false;
		}

		/// <summary>
		/// Localises the job seeker report.
		/// </summary>
		private void LocaliseSupplyDemandReport()
		{
			CriteriaHeader.Text = CodeLocalise("CriteriaHeader.SupplyDemand.Text", "Report on supply and demand");
			Column1Header.Text = CodeLocalise("Column1Header.SupplyDemand.Text", "Which occupations or industries are you looking for?");
			Column2Header.Text = CodeLocalise("Column2Header.SupplyDemand.Text", "Where are the job postings and job seekers located,and whats the overall time period for this report?");
		}

		/// <summary>
		/// Functions the bind job seeker criteria.
		/// </summary>
		/// <returns></returns>
		private IReportCriteria UnBindSupplyDemandCriteria()
		{
			var reportDisplayType = (ReportDisplayType)Enum.Parse(typeof(ReportDisplayType), ReportDisplayTypeList.SelectedValue);
			//if (reportDisplayType == ReportDisplayType.BarChart)
			var chartGroup = new SupplyDemandReportChartGroup
			{
				GroupBy = GroupByDropDownList.Items.Cast<ListItem>().Where(x => x.Selected).Select(
					x => (ReportSupplyDemandGroupBy?)Enum.Parse(typeof(ReportSupplyDemandGroupBy), x.Value)).
					SingleOrDefault()
			};

			return new SupplyDemandReportCriteria
			{
				DisplayType = reportDisplayType,
				DateRangeInfo = Column2CriteriaDateRange.UnBind(),
				OccupationInfo = CriteriaOccupation.UnbindOccupations(),
				IndustriesInfo = CriteriaIndustries.UnbindIndustries(),
				KeywordInfo = CriteriaKeywordAndContext.Unbind(),
				LocationInfo = Column2CriteriaSupplyDemandLocation.Unbind(),
				ChartGroupInfo = chartGroup,
			};
		}

		#endregion

		#endregion

		#region page generated events

		public delegate void RunReportClickedHandler(object sender, CommandEventArgs eventArgs);

		public event RunReportClickedHandler RunReportClicked;

		/// <summary>
		/// Raises the <see cref="CommandEventArgs" /> event.
		/// </summary>
		/// <param name="eventargs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
		public void OnRunReportClicked(CommandEventArgs eventargs)
		{
			var handler = RunReportClicked;
			if (handler != null) handler(this, eventargs);
		}

		#endregion
	}
}
