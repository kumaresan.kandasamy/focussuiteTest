﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.Report;
using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
	public partial class CriteriaJobSeekerNcrc : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				Localise();
				Bind();
			}
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.NCRCLevel).ToList().ForEach(lookupViewItem =>
			{
				NcrcLevelCheckboxList.Items.Add(new ListItem(lookupViewItem.Text, lookupViewItem.Key));
			});
		}

		/// <summary>
		/// Binds the controls to the criteria
		/// </summary>
		/// <param name="criteria">The criteria to which to bind</param>
		public void BindCriteria(Focus.Core.Criteria.Report.JobOrdersNcrcReportCriteria criteria)
		{
			Reset();

			if (criteria.IsNull()) return;
			criteria.NcrcLevels.ForEach(ncrcLevel =>
			{
				if (NcrcLevelCheckboxList.Items.Cast<ListItem>().Any(listItem => listItem.Value == ncrcLevel))
					NcrcLevelCheckboxList.Items.FindByValue(ncrcLevel).Selected = true;
			});
		}

		/// <summary>
		/// Localises this instance.
		/// </summary>
		private void Localise()
		{
			CriteriaNcrcCredentialsLabel.Text = CodeLocalise("CriteriaNcrcCredentialsLabel.Text", "NCRC&trade;");
			NcrcLevelHeader.Text = CodeLocalise("NcrcLevelHeader.Text", "NCRC&trade; Credentials");
		}

		/// <summary>
		/// Resets this instance.
		/// </summary>
		private void Reset()
		{
			NcrcLevelCheckboxList.Items.Cast<ListItem>().ToList().ForEach(x => x.Selected = false);
		}

		/// <summary>
		/// Gets the criteria object based on the control values.
		/// </summary>
		/// <returns>The criteria.</returns>
		public JobSeekerNcrcReportCriteria Unbind()
		{
			var lookups = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.NCRCLevel);
			var criteria = new JobSeekerNcrcReportCriteria()
			{
				NcrcLevels = NcrcLevelCheckboxList.Items.Cast<ListItem>().Where(listItem => listItem.Selected).Select(listItem => listItem.Value).ToList()
			};
			return criteria;
		}
	}
}