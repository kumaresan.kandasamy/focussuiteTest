﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaJobSeekerActivitySummary.ascx.cs"
    Inherits="Focus.Web.WebReporting.Controls.CriteriaJobSeekerActivitySummary" %>

<table role="presentation" style="width:100%;" class="accordionWithIcon">
    <tr class="multipleAccordionTitle">
        <td style="vertical-align:top; width:10%;">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td style="width:90%;">
            <asp:Literal runat="server" ID="CriteriaJobSeekerActivitySummaryLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
            <table role="presentation" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                            <asp:Literal runat="server" ID="AccountsAndSignInsHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr id="AccountsRow" runat="server">
                    <td>
                        <asp:CheckBox runat="server" ID="JobSeekerAccountsCheckBox"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="JobSeekerSignInsCheckBox"/>
                    </td>
                </tr>
            </table>
            <table role="presentation" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                            <asp:Literal runat="server" ID="PostingsHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="PostingsViewedCheckBox"/>
                    </td>
                </tr>
                <tr id="ThreeToFiveStaffMatchesTableRow" runat="server" Visible="False">
                    <td>
                        <asp:CheckBox runat="server" ID="ThreeToFiveStaffMatchesCheckBox"/>
                    </td>
                </tr>
            </table>
            <table role="presentation" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                            <asp:Literal runat="server" ID="ReferralsHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="ReferralsRequestedCheckBox"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="ReferralsApprovedCheckBox"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="StatewideStaffReferralsCheckBox"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="ExternalStaffReferralsCheckBox"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="StatewideSelfReferralsCheckBox"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="ExternalSelfReferralsCheckbox"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="TotalReferralsCheckBox"/>
                    </td>
                </tr>
            </table>
            <table role="presentation" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                            <asp:Literal runat="server" ID="SeekerSearchesHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="TargettingHighGrowthSectorsCheckBox"/>
                    </td>
                </tr>
                <tr id="ExcludingByPhysicalAbilityTableRow" runat="server" Visible="False">
                    <td>
                        <asp:CheckBox runat="server" ID="ExcludingByPhysicalAbilityCheckBox"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="SavingJobAlertsCheckBox"/>
                    </td>
                </tr>
            </table>
            <table role="presentation" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong><asp:Literal runat="server" ID="SurveyResponsesHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="SurveyVerySatisfiedCheckBox"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="SurveySatisfiedCheckBox"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="SurveyDissatisfiedCheckBox"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="SurveyVeryDissatisfiedCheckBox"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="SurveyNoUnexpectedMatchesCheckBox"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="SurveyUnexpectedMatchesCheckBox"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="SurveyReceivedInvitationsCheckBox"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="SurveyDidNotReceiveInvitationsCheckBox"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
