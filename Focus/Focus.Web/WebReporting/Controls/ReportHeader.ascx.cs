﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;

using Focus.Web.Code;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class ReportHeader : UserControlBase
	{
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
        Localise();
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      EditReportSettingsLinkButton.Text = CodeLocalise("EditReportSettingsLinkButton.Text", "edit report settings");
      NewReportButton.Text = CodeLocalise("NewReportButton.Text", "Create new report");
    }

    protected void EditReportSettingsLinkButton_Clicked(object sender, EventArgs e)
    {
      OnActionRequested(new CommandEventArgs("EditCriteria", null));
    }

    protected void NewReportButton_Clicked(object sender, EventArgs e)
    {
      Response.Redirect(UrlBuilder.ReportingHome());
    }

		public void DisplayReportSettingsLink(bool display)
		{
      EditReportSettingsLinkButton.Visible = 
        NewReportButton.Visible = display;
		}


    #region page generated events

    public delegate void ReportHeaderActionHandler(object sender, CommandEventArgs eventArgs);
    public event ReportHeaderActionHandler ActionRequested;

    /// <summary>
    /// Raises the <see cref="CommandEventArgs" /> event.
    /// </summary>
    /// <param name="eventargs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    public void OnActionRequested(CommandEventArgs eventargs)
    {
      var handler = ActionRequested;
      if (handler != null) handler(this, eventargs);
    }

    #endregion

  }
}