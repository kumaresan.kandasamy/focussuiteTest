﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.Report;

using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
    public partial class CriteriaJobCharacteristics : UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Localise();
                Bind();
            }

        }

        /// <summary>
        /// Binds the controls to the report criteria.
        /// </summary>
        /// <param name="reportType">Type of the report.</param>
        /// <param name="salaryCriteria">The salary criteria.</param>
        /// <param name="characteristicsCriteria">The characteristics criteria.</param>
        /// <param name="keyWordCriteria">The key word criteria.</param>
        public void BindCriteria(ReportType reportType, SalaryReportCriteria salaryCriteria, JobOrderCharacteristicsReportCriteria characteristicsCriteria, KeywordReportCriteria keyWordCriteria)
        {
            Reset(reportType);

            if (salaryCriteria.IsNotNull())
            {
                if (salaryCriteria.MinimumSalary.HasValue) LowSalaryTextBox.Text = salaryCriteria.MinimumSalary.ToString();
                if (salaryCriteria.MaximumSalary.HasValue) HighSalaryTextBox.Text = salaryCriteria.MaximumSalary.ToString();
                if (salaryCriteria.SalaryFrequency.HasValue) SalaryRangeDropDown.SelectedValue = salaryCriteria.SalaryFrequency.ToString();
            }

            if (characteristicsCriteria.IsNotNull())
            {
                var educationLevels = characteristicsCriteria.EducationLevels;
                if (educationLevels.IsNotNull())
                {
                    if (educationLevels.Contains(EducationLevels.GraduateDegree))
                    {
                        // Graduate option now replace by separate Masters and Doctorate options
                        educationLevels.Remove(EducationLevels.GraduateDegree);
                        if (!educationLevels.Contains(EducationLevels.MastersDegree))
                            educationLevels.Add(EducationLevels.MastersDegree);
                        if (!educationLevels.Contains(EducationLevels.DoctorateDegree))
                            educationLevels.Add(EducationLevels.DoctorateDegree);
                    }

                    if (educationLevels.Contains(EducationLevels.HighSchoolDiploma))
                    {
                        // HighSchoolDiploma option now replace by separate NoDiploma,HighSchoolDiplomaOrEquivalent and SomeCollegeNoDegree options
                        educationLevels.Remove(EducationLevels.HighSchoolDiploma);
                        educationLevels.Add(EducationLevels.NoDiploma);
                        educationLevels.Add(EducationLevels.HighSchoolDiplomaOrEquivalent);
                        educationLevels.Add(EducationLevels.SomeCollegeNoDegree);
                    }
                }
                JobStatusCheckBoxList.BindEnumListToControlList(characteristicsCriteria.JobStatuses);
                LevelOfEducationCheckBoxList.BindEnumListToControlList(educationLevels);
            }

            if (keyWordCriteria.IsNotNull())
            {
                if (keyWordCriteria.KeyWords.IsNotNullOrEmpty())
                    SearchTermsTextBox.Text = string.Join(Environment.NewLine, keyWordCriteria.KeyWords);

                SearchResumeContextCheckBoxList.BindEnumListToControlList(keyWordCriteria.KeyWordSearch);
                if (keyWordCriteria.SearchType.IsNotNull())
                    SearchTypeRadioButtonList.SelectedValue = keyWordCriteria.SearchType.ToString();
            }
        }

        /// <summary>
        /// Unbinds the salary.
        /// </summary>
        /// <returns>The salary criteria</returns>
        public SalaryReportCriteria UnbindSalary()
        {
            var criteria = new SalaryReportCriteria();

            decimal salary;

            if (decimal.TryParse(LowSalaryTextBox.Text, out salary))
                criteria.MinimumSalary = salary;

            if (decimal.TryParse(HighSalaryTextBox.Text, out salary))
                criteria.MaximumSalary = salary;

            if (criteria.MinimumSalary.HasValue || criteria.MaximumSalary.HasValue)
                criteria.SalaryFrequency = SalaryRangeDropDown.SelectedValueToEnum<ReportSalaryFrequency>();

            return criteria;
        }

        /// <summary>
        /// Unbinds the job status and education level
        /// </summary>
        /// <returns>The job characteristic criteria</returns>
        public JobOrderCharacteristicsReportCriteria UnbindCharacteristics()
        {
            var criteria = new JobOrderCharacteristicsReportCriteria
            {
                JobStatuses = JobStatusCheckBoxList.UnbindNullableEnumControlList<JobStatuses>(),
                EducationLevels = LevelOfEducationCheckBoxList.UnbindNullableEnumControlList<EducationLevels>()
            };

            return criteria;
        }

        /// <summary>
        /// Unbinds the keywords
        /// </summary>
        /// <returns>The key words criteria</returns>
        public KeywordReportCriteria UnbindKeyWords()
        {
            var criteria = new KeywordReportCriteria
            {
                KeyWords = SearchTermsTextBox.Text.Split(new[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries).ToList(),
                KeyWordSearch = SearchResumeContextCheckBoxList.UnbindNullableEnumControlList<KeyWord>()
            };

            if (SearchTypeRadioButtonList.Items.Cast<ListItem>().Any(x => x.Selected))
                criteria.SearchType = SearchTypeRadioButtonList.UnbindNullableEnumControlList<KeywordSearchType>();

            return criteria;
        }

        /// <summary>
        /// Resets all the controls on the page
        /// </summary>
        private void Reset(ReportType reportType)
        {
            SalaryRangeDropDown.SelectedIndex = 0;
            SearchResumeContextCheckBoxList.Items.Reset();
            SearchTypeRadioButtonList.Items.Reset();
            SearchTypeRadioButtonList.SelectedValue = KeywordSearchType.All.ToString();
            LevelOfEducationCheckBoxList.Items.Reset();
            JobStatusCheckBoxList.Items.Reset();
            LowSalaryTextBox.Text = HighSalaryTextBox.Text = SearchTermsTextBox.Text = string.Empty;

            SalaryFromNumericValidator.ValidationGroup =
            SalaryToNumericValidator.ValidationGroup =
            SalaryFromValueValidator.ValidationGroup =
            SalaryToValueValidator.ValidationGroup =
            SalaryToFromComparisonValidator.ValidationGroup = reportType.ToString();
        }

        /// <summary>
        /// Localises this instance.
        /// </summary>
        private void Localise()
        {
            #region labels

            CriteriaJobCharacteristicsLabel.Text = CodeLocalise("CriteriaJobCharacteristicsLabel.Text", "Job characteristics");
            SearchResumeContextHeader.Text = CodeLocalise("SearchResumeContextHeader.Text", "Search these contexts");
            SearchTermsHeader.Text = CodeLocalise("SearchTermsHeader.Text", "Show results with these keywords");
            SalaryHeader.Text = CodeLocalise("SalaryHeader.Text", "Salary");
            LevelOfEducationHeader.Text = CodeLocalise("LevelOfEducationCheckBoxList.Text", "Level of education");
            JobStatusHeader.Text = CodeLocalise("JobStatusHeader.Text", "Job status");

            #endregion

            #region error messages

            SalaryFromValueValidator.ErrorMessage = CodeLocalise("JobCharacteristics.SalaryFromValueValidator.ErrorMessage",
                                                             "Job characteristics - \"Salary From\" must be greater than 0");
            SalaryToValueValidator.ErrorMessage = CodeLocalise("JobCharacteristics.SalaryToValueValidator.ErrorMessage",
                                                              "Job characteristics - \"Salary To\" must be greater than 0");
            SalaryToFromComparisonValidator.ErrorMessage = CodeLocalise("JobCharacteristics.SalaryToFromComparisonValidator.ErrorMessage",
                                                              "Job characteristics - \"Salary To\" must be greater than or equal to \"Salary From\"");
            SalaryToNumericValidator.ErrorMessage = CodeLocalise("JobCharacteristics.SalaryToNumericValidator.ErrorMessage",
                                                                 "Job characteristics - Upper salary value must be numeric or blank");
            SalaryFromNumericValidator.ErrorMessage = CodeLocalise("JobCharacteristics.SalaryFromNumericValidator.ErrorMessage",
                                                                 "Job characteristics - Lower salary value must be numeric or blank");

            #endregion
        }


        /// <summary>
        /// Binds this instance.
        /// </summary>
        private void Bind()
        {
            SalaryRangeDropDown.Items.Add(new ListItem(CodeLocalise("SalaryRangeDropDown.Hourly.Text.NoEdit", "hourly"), ReportSalaryFrequency.Hourly.ToString()));
            SalaryRangeDropDown.Items.Add(new ListItem(CodeLocalise("SalaryRangeDropDown.Daily.Text.NoEdit", "daily"), ReportSalaryFrequency.Daily.ToString()));
            SalaryRangeDropDown.Items.Add(new ListItem(CodeLocalise("SalaryRangeDropDown.Weekly.Text.NoEdit", "weekly"), ReportSalaryFrequency.Weekly.ToString()));
            SalaryRangeDropDown.Items.Add(new ListItem(CodeLocalise("SalaryRangeDropDown.Montly.Text.NoEdit", "monthly"), ReportSalaryFrequency.Monthly.ToString()));
            SalaryRangeDropDown.Items.Add(new ListItem(CodeLocalise("SalaryRangeDropDown.Yearly.Text.NoEdit", "yearly"), ReportSalaryFrequency.Yearly.ToString()));

            SearchResumeContextCheckBoxList.Items.Add(new ListItem(CodeLocalise("SearchResumeContextCheckBoxList.Employer.Text", "#BUSINESS# name"), KeyWord.EmployerName.ToString()));
            SearchResumeContextCheckBoxList.Items.Add(new ListItem(CodeLocalise("SearchResumeContextCheckBoxList.JobTitle.Text", "Job title"), KeyWord.JobTitle.ToString()));
            SearchResumeContextCheckBoxList.Items.Add(new ListItem(CodeLocalise("SearchResumeContextCheckBoxList.JobDescription.Text", "Job description"), KeyWord.JobDescription.ToString()));
            SearchResumeContextCheckBoxList.Items.Add(new ListItem(CodeLocalise("SearchResumeContextCheckBoxList.FullJob.Text", "Full job"), KeyWord.FullJob.ToString()));

            SearchTypeRadioButtonList.Items.Add(new ListItem(CodeLocalise("SearchTypeRadioButtonList.AllTerms.Text", "results use all terms"), KeywordSearchType.All.ToString()));
            SearchTypeRadioButtonList.Items.Add(new ListItem(CodeLocalise("SearchTypeRadioButtonList.SomeTerms.Text", "results use some terms"), KeywordSearchType.Any.ToString()));
            SearchTypeRadioButtonList.SelectedValue = KeywordSearchType.All.ToString();


            LevelOfEducationCheckBoxList.Items.Add(new ListItem(CodeLocalise("LevelOfEducationCheckBoxList.NoSpecificRequirement.Text", "No specific requirement"), EducationLevels.None.ToString()));
            LevelOfEducationCheckBoxList.Items.Add(new ListItem(CodeLocalise("LevelOfEducationCheckBoxList.NoDiploma.Text", "No diploma"), EducationLevels.NoDiploma.ToString()));
            LevelOfEducationCheckBoxList.Items.Add(new ListItem(CodeLocalise("LevelOfEducationCheckBoxList.HighSchoolDiplomaOrEquivalent.Text", "High school diploma or equivalent"), EducationLevels.HighSchoolDiplomaOrEquivalent.ToString()));
            LevelOfEducationCheckBoxList.Items.Add(new ListItem(CodeLocalise("LevelOfEducationCheckBoxList.NoDegree.Text", "Some college, no degree"), EducationLevels.SomeCollegeNoDegree.ToString()));
            LevelOfEducationCheckBoxList.Items.Add(new ListItem(CodeLocalise("LevelOfEducationCheckBoxList.AssociatesDegree.Text", "Associate’s or vocational degree"), EducationLevels.AssociatesDegree.ToString()));
            LevelOfEducationCheckBoxList.Items.Add(new ListItem(CodeLocalise("LevelOfEducationCheckBoxList.BachelorsDegree.Text", "Bachelor's degree"), EducationLevels.BachelorsDegree.ToString()));
            LevelOfEducationCheckBoxList.Items.Add(new ListItem(CodeLocalise("LevelOfEducationCheckBoxList.MastersDegree.Text", "Master’s degree"), EducationLevels.MastersDegree.ToString()));
            LevelOfEducationCheckBoxList.Items.Add(new ListItem(CodeLocalise("LevelOfEducationCheckBoxList.DoctorateDegree.Text", "Doctorate degree"), EducationLevels.DoctorateDegree.ToString()));

            JobStatusCheckBoxList.Items.Add(new ListItem(CodeLocalise("JobStatusCheckBoxList.Active.Text", "Active"), JobStatuses.Active.ToString()));
            JobStatusCheckBoxList.Items.Add(new ListItem(CodeLocalise("JobStatusCheckBoxList.OnHold.Text", "On hold"), JobStatuses.OnHold.ToString()));
            JobStatusCheckBoxList.Items.Add(new ListItem(CodeLocalise("JobStatusCheckBoxList.Closed.Text", "Closed"), JobStatuses.Closed.ToString()));
        }
    }
}