﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;

using Framework.Core;

using Focus.Core;
using Focus.Core.Criteria.Report;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaDateRange : UserControlBase
	{
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();
        Bind();
      }
    }

    /// <summary>
    /// Resets this instance.
    /// </summary>
    private void Reset(ReportType reportType)
    {
      WithinRadioButton.Checked = true;
      WithinDaysDropdown.SelectedIndex = 2;
      ResultsFromMonthDropdown.SelectedIndex =
        ResultsFromYearDropdown.SelectedIndex =
        ShowResultsFromMonthDropdown.SelectedIndex =
        ShowResultsFromYearDropdown.SelectedIndex =
        ShowResultsToYearDropdown.SelectedIndex = ShowResultsToMonthDropdown.SelectedIndex = 0;

      ResultsFromValidator.ValidationGroup = ResultsFromToValidator.ValidationGroup = reportType.ToString();
    }

    public void BindCriteria(DateRangeReportCriteria criteria, ReportType reportType)
    {
      Reset(reportType);
      if (criteria.IsNull()) return;

      if (criteria.Days.HasValue)
      {
        WithinDaysDropdown.SelectedValue = criteria.Days.ToString();
        WithinRadioButton.Checked = true;
        return;
      }
      if (criteria.FromDate.IsNull() || criteria.ToDate.IsNull())
      {
        WithinRadioButton.Checked = true;
        return;
      }
      var fromDate = criteria.FromDate.GetValueOrDefault();
      var toDate = criteria.ToDate.GetValueOrDefault();

      if (fromDate.Year == toDate.Year && fromDate.Month == toDate.Month)
      {
        FromRadioButton.Checked = true;
        ResultsFromYearDropdown.SelectedValue = fromDate.Year.ToString();
        ResultsFromMonthDropdown.SelectedValue = fromDate.Month.ToString();
      }
      else
      {
        FromToRadioButton.Checked = true;
        ShowResultsFromYearDropdown.SelectedValue = fromDate.Year.ToString();
        ShowResultsFromMonthDropdown.SelectedValue = fromDate.Month.ToString();
        ShowResultsToYearDropdown.SelectedValue = toDate.Year.ToString();
        ShowResultsToMonthDropdown.SelectedValue = toDate.Month.ToString();
      }
    }

    /// <summary>
    /// Functions the bind.
    /// </summary>
    /// <returns></returns>
    public DateRangeReportCriteria UnBind()
    {
      var criteria = new DateRangeReportCriteria();
      if (WithinRadioButton.Checked)
      {
        // Last x days
        criteria.Days = Convert.ToInt32(WithinDaysDropdown.SelectedValue);
      }
      else if (FromRadioButton.Checked)
      {
        // Results for a given month
        criteria.FromDate = new DateTime(Convert.ToInt32(ResultsFromYearDropdown.SelectedValue), Convert.ToInt32(ResultsFromMonthDropdown.SelectedValue), 1);
        criteria.ToDate = new DateTime(criteria.FromDate.GetValueOrDefault().Year,
                                       criteria.FromDate.GetValueOrDefault().Month,
                                       DateTime.DaysInMonth(criteria.FromDate.GetValueOrDefault().Year,
                                                            criteria.FromDate.GetValueOrDefault().Month));
      }
      else if (FromToRadioButton.Checked)
      {
        // Results for a date range
        var year1 = Convert.ToInt32(ShowResultsFromYearDropdown.SelectedValue);
        var month1 = Convert.ToInt32(ShowResultsFromMonthDropdown.SelectedValue);
        var year2 = Convert.ToInt32(ShowResultsToYearDropdown.SelectedValue);
        var month2 = Convert.ToInt32(ShowResultsToMonthDropdown.SelectedValue);

        // Work out which one is the from date and build critera accordingly
        if (year1 < year2 || (year1 == year2 && month1 < month2))
        {
          criteria.FromDate = new DateTime(year1, month1, 1);
          criteria.ToDate = new DateTime(year2, month2, DateTime.DaysInMonth(year2, month2));
        }
        else
        {
          criteria.FromDate = new DateTime(year2, month2, 1);
          criteria.ToDate = new DateTime(year1, month1, DateTime.DaysInMonth(year1, month1));
        }

      }
      return criteria;
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      #region labels

      CriteriaDateRangeLabel.Text = CodeLocalise("CriteriaDateRangeLabel.Text", "Time period");

      #endregion

      #region error messages

      ResultsFromValidator.ErrorMessage = CodeLocalise("CriteriaDateRange.ResultsFromValidator.ErrorMessage",
                                                       "Time period - Valid date not provided when requesting results from a particular month");
      ResultsFromToValidator.ErrorMessage = CodeLocalise("CriteriaDateRange.ResultsFromToValidator.ErrorMessage",
                                                       "Time period - Valid dates not provided when requesting results from a particular date range");

      #endregion
    }

    /// <summary>
    /// Binds this instance.
    /// </summary>
    private void Bind()
    {
      WithinRadioButton.Checked = true;
      WithinDaysDropdown.Items.Add(new ListItem(CodeLocalise("Last7Days.Text.NoEdit", "last 7 days"), "7"));
      WithinDaysDropdown.Items.Add(new ListItem(CodeLocalise("Last14Days.Text.NoEdit", "last 14 days"), "14"));
      WithinDaysDropdown.Items.Add(new ListItem(CodeLocalise("Last30Days.Text.NoEdit", "last 30 days"), "30"));
      WithinDaysDropdown.Items.Add(new ListItem(CodeLocalise("Last60Days.Text.NoEdit", "last 60 days"), "60"));
      WithinDaysDropdown.Items.Add(new ListItem(CodeLocalise("Last90Days.Text.NoEdit", "last 90 days"), "90"));
      WithinDaysDropdown.Items.Add(new ListItem(CodeLocalise("Last120Days.Text.NoEdit", "last 120 days"), "120"));
      WithinDaysDropdown.SelectedIndex = 2;

      BuildMonthSelections(ResultsFromMonthDropdown);
      BuildYearSelections(ResultsFromYearDropdown, 2000, DateTime.Now.Year);
      BuildMonthSelections(ShowResultsFromMonthDropdown);
      BuildYearSelections(ShowResultsFromYearDropdown, 2000, DateTime.Now.Year);
      BuildMonthSelections(ShowResultsToMonthDropdown);
      BuildYearSelections(ShowResultsToYearDropdown, 2000, DateTime.Now.Year);
    }



    private void BuildYearSelections(ListControl dropDown, int startYear, int endYear = 2020, int selectedYear = 0)
    {
      if (startYear > endYear)
        throw new Exception("Unable to build year drop downs, incorrect year range received");

      dropDown.Items.Clear();

      dropDown.Items.Add(new ListItem(CodeLocalise("Global.Year.NoEdit", "year"), ""));

      while (startYear <= endYear)
      {
        dropDown.Items.Add(new ListItem(startYear.ToString(), startYear.ToString()));
        startYear++;
      }
      var item = dropDown.Items.FindByValue(selectedYear.ToString());
      if (item.IsNotNull()) item.Selected = true;
    }


    private void BuildMonthSelections(ListControl dropDown, int selectedMonth = 0)
    {
      dropDown.Items.Clear();

      dropDown.Items.Add(new ListItem(CodeLocalise("Global.Month.NoEdit", "month"), ""));

      dropDown.Items.Add(new ListItem(CodeLocalise("Global.January.Abbreviated.NoEdit", "Jan"), "1"));
      dropDown.Items.Add(new ListItem(CodeLocalise("Global.February.Abbreviated.NoEdit", "Feb"), "2"));
      dropDown.Items.Add(new ListItem(CodeLocalise("Global.March.Abbreviated.NoEdit", "Mar"), "3"));
      dropDown.Items.Add(new ListItem(CodeLocalise("Global.April.Abbreviated.NoEdit", "Apr"), "4"));
      dropDown.Items.Add(new ListItem(CodeLocalise("Global.May.Abbreviated.NoEdit", "May"), "5"));
      dropDown.Items.Add(new ListItem(CodeLocalise("Global.June.Abbreviated.NoEdit", "Jun"), "6"));
      dropDown.Items.Add(new ListItem(CodeLocalise("Global.July.Abbreviated.NoEdit", "Jul"), "7"));
      dropDown.Items.Add(new ListItem(CodeLocalise("Global.August.Abbreviated.NoEdit", "Aug"), "8"));
      dropDown.Items.Add(new ListItem(CodeLocalise("Global.September.Abbreviated.NoEdit", "Sep"), "9"));
      dropDown.Items.Add(new ListItem(CodeLocalise("Global.October.Abbreviated.NoEdit", "Oct"), "10"));
      dropDown.Items.Add(new ListItem(CodeLocalise("Global.November.Abbreviated.NoEdit", "Nov"), "11"));
      dropDown.Items.Add(new ListItem(CodeLocalise("Global.December.Abbreviated.NoEdit", "Dec"), "12"));

      dropDown.SelectedIndex = selectedMonth;
    }
  }

}