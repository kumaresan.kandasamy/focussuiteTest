﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;
using Focus.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class ReportSelector : UserControlBase
	{
    public delegate void ReportSelectedHandler(object sender, CommandEventArgs eventArgs);
    public event ReportSelectedHandler ReportSelected;

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();
	      Initialise();
      }
    }
    
    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      JobSeekerReportButton.Text = CodeLocalise("JobSeekerReportButton.Text.NoEdit", "Job seekers");
			JobOrderReportButton.Text = CodeLocalise("JobOrderReportButton.Text.NoEdit", "#EMPLOYMENTTYPES#");
      HiringManagerReportButton.Text = CodeLocalise("HiringManagerReportButton.Text", "Hiring managers");
      EmployerReportButton.Text = CodeLocalise("EmployerReportButton.Text.NoEdit", "Employers");
      SupplyDemandReportButton.Text = CodeLocalise("SupplyDemandReportButton.Text", "Supply & Demand");
      StaffReportButton.Text = CodeLocalise("StaffReportButton.Text", "Staff");
      WhatDoYouWantLiteral.Text = CodeLocalise("WhatDoYouWantLiteral.Text", "What do you want to report on?");
      SelectorHeading.Text = CodeLocalise("SelectorHeading.Text", "Report options");
    }

		/// <summary>
		/// Initialises this instance.
		/// </summary>
		private void Initialise()
		{
      JobSeekerReportButton.Visible = App.User.IsInAnyRole(Constants.RoleKeys.AssistJobSeekerReports, Constants.RoleKeys.AssistJobSeekerReportsViewOnly);
      JobOrderReportButton.Visible = App.User.IsInAnyRole(Constants.RoleKeys.AssistJobOrderReports, Constants.RoleKeys.AssistJobOrderReportsViewOnly);
      EmployerReportButton.Visible = App.User.IsInAnyRole(Constants.RoleKeys.AssistEmployerReports, Constants.RoleKeys.AssistEmployerReportsViewOnly);
		  SupplyDemandReportButton.Visible = App.Settings.ShowSupplyDemandReport;
		}

    #region events

    /// <summary>
    /// Handles the Command event of the ReportButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void ReportButton_Command(Object sender, CommandEventArgs e)
    {
      // Bubble event down
      OnReportSelected(new CommandEventArgs(e));
    }

    #endregion

    #region event invocations

    /// <summary>
    /// Raises the <see cref="CommandEventArgs" /> event.
    /// </summary>
    /// <param name="eventargs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    public void OnReportSelected(CommandEventArgs eventargs)
    {
      var handler = ReportSelected;
      if (handler != null) handler(this, eventargs);
    }

    #endregion
  }
}