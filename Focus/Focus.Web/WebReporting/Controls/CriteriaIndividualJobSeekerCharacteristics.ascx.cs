﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.Report;

#endregion

namespace Focus.Web.WebReporting.Controls
{
	public partial class CriteriaIndividualJobSeekerCharacteristics : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
            IndividualCharacteristicsSsnClientList.Visible = IndividualCharacteristicsSsnLabel.Visible = App.Settings.EnableJSActivityBackdatingDateSelection || App.Settings.EnableSsnFeatureGroup;
            

			if (!Page.IsPostBack)
			{
				Localise();
				IndividualCharacteristicsSsnClientList.MaxSize = App.Settings.MaxSearchableIDs;
			}
		}

		/// <summary>
		/// Localises this instance.
		/// </summary>
		private void Localise()
		{
			CriteriaIndividualJobSeekerCharacteristicsLabel.Text = CodeLocalise("CriteriaIndividualJobSeekerCharacteristicsLabel.Text", "Individual #CANDIDATETYPE# characteristics");
			IndividualCharacteristicsSsnClientList.ValidatorRegexErrorMessage = CodeLocalise("IndividualCharacteristicsSsnClientList.ErrorMessage", "SSN must consist of 9 numbers");
			IndividualCharacteristicsSsnClientList.SelectionBoxTitle = CodeLocalise("IndividualCharacteristicsSsnClientList.Title", "Selected SSNs");
           
		}

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <returns></returns>
		public IndividualJobSeekerCharacteristicsCriteria Unbind()
		{
			var criteria = new IndividualJobSeekerCharacteristicsCriteria
			{
				FirstName = IndividualCharacteristicsFirstNameTextBox.TextTrimmed(),
				LastName = IndividualCharacteristicsLastNameTextBox.TextTrimmed(),
				Email = IndividualCharacteristicsEmailTextBox.TextTrimmed(),
                Ssns = (IndividualCharacteristicsSsnClientList.SelectedItems!=null)?(IndividualCharacteristicsSsnClientList.SelectedItems.Count > 0 ? IndividualCharacteristicsSsnClientList.SelectedItems.Select(ssn => ssn.Value.Replace("-", "")).ToList() : null):null
			};
			
			return criteria;
		}
	}
}
