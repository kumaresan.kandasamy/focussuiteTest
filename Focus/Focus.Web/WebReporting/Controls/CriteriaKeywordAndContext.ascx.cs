﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.Report;
using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaKeywordAndContext : UserControlBase
	{
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();
        Bind();
      }
    }

    /// <summary>
    /// Resets this instance.
    /// </summary>
    private void Reset()
    {
      SearchResumeContextCheckBoxList.Items.Reset();
      SearchTypeRadioButtonList.Items.Reset();
      SearchTypeRadioButtonList.SelectedValue = KeywordSearchType.All.ToString();
    }

    public KeywordReportCriteria Unbind()
    {
      var criteria = new KeywordReportCriteria
                       {
                         KeyWords =
                           SearchTermsTextBox.Text.Split(new[] {"\r\n", "\n"}, StringSplitOptions.RemoveEmptyEntries).
                           ToList(),
                         KeyWordSearch = SearchResumeContextCheckBoxList.UnbindNullableEnumControlList<KeyWord>()
                       };


      if (SearchTypeRadioButtonList.Items.Cast<ListItem>().Any(x => x.Selected))
        criteria.SearchType = SearchTypeRadioButtonList.UnbindNullableEnumControlList<KeywordSearchType>();
      
      return criteria;
    }

    public void BindCriteria(KeywordReportCriteria criteria)
    {
      Reset();

      if (criteria.IsNull()) return;

      if (criteria.KeyWords.IsNotNullOrEmpty())
        SearchTermsTextBox.Text = string.Join(Environment.NewLine, criteria.KeyWords);

      SearchResumeContextCheckBoxList.BindEnumListToControlList(criteria.KeyWordSearch);
      if (criteria.SearchType.IsNotNull())
        SearchTypeRadioButtonList.SelectedValue = criteria.SearchType.ToString();
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      CriteriaKeywordAndContextLabel.Text = CodeLocalise("CriteriaKeywordAndContextLabel.Text", "Keyword & context");
      SearchResumeContextHeader.Text = CodeLocalise("SearchResumeContextHeader.Text", "Search resume contexts");
      SearchTermsHeader.Text = CodeLocalise("SearchTermsHeader.Text", "Show results with these keywords");
    }

    /// <summary>
    /// Binds this instance.
    /// </summary>
    private void Bind()
    {
			SearchResumeContextCheckBoxList.Items.Add(new ListItem(CodeLocalise("SearchResumeContextCheckBoxList.Employer.Text", "#BUSINESS# name"), KeyWord.EmployerName.ToString()));
      SearchResumeContextCheckBoxList.Items.Add(new ListItem(CodeLocalise("SearchResumeContextCheckBoxList.JobTitle.Text", "Job title"), KeyWord.JobTitle.ToString()));
      SearchResumeContextCheckBoxList.Items.Add(new ListItem(CodeLocalise("SearchResumeContextCheckBoxList.JobDescription.Text", "Work history"), KeyWord.JobDescription.ToString()));
      SearchResumeContextCheckBoxList.Items.Add(new ListItem(CodeLocalise("SearchResumeContextCheckBoxList.EducationQualifications.Text", "Education qualifications"), KeyWord.EducationQualifications.ToString()));
      SearchResumeContextCheckBoxList.Items.Add(new ListItem(CodeLocalise("SearchResumeContextCheckBoxList.Skills.Text", "Skills"), KeyWord.Skills.ToString()));
      SearchResumeContextCheckBoxList.Items.Add(new ListItem(CodeLocalise("SearchResumeContextCheckBoxList.Internships.Text", "Internships"), KeyWord.Internships.ToString()));
      SearchResumeContextCheckBoxList.Items.Add(new ListItem(CodeLocalise("SearchResumeContextCheckBoxList.FullResume.Text", "Full resume"), KeyWord.FullResume.ToString()));

      SearchTypeRadioButtonList.Items.Add(new ListItem(CodeLocalise("SearchTypeRadioButtonList.AllTerms.Text", "results use all terms"), KeywordSearchType.All.ToString()));
      SearchTypeRadioButtonList.Items.Add(new ListItem(CodeLocalise("SearchTypeRadioButtonList.SomeTerms.Text", "results use some terms"), KeywordSearchType.Any.ToString()));
      SearchTypeRadioButtonList.SelectedValue = KeywordSearchType.All.ToString();
    }
  }
}