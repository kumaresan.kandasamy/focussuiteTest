﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaJobSeekerStatus.ascx.cs"
    Inherits="Focus.Web.WebReporting.Controls.CriteriaJobSeekerStatus" %>
<table role="presentation" style="width:100%;" class="accordionWithIcon">
    <tr class="multipleAccordionTitle">
        <td style="vertical-align:top; width:10%;">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td style="width:90%;">
            <asp:Literal runat="server" ID="CriteriaJobSeekerStatusLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
        <table role="presentation" style="width:100%;" id="CareerAccountTypeTable" runat="server" Visible="True">
                <tr>
                    <td colspan="2">
                        <strong id="CareerAccountTypeHeaderLabel">
                            <asp:Literal runat="server" ID="CareerAccountTypeHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                       <asp:CheckBoxList ID="CareerAccountTypeCheckBoxList" TextAlign="Right" runat="server" role="presentation"/>
                    </td>
                </tr>
            </table>
            <table id="JobSeekerStatusHeaderPanel" role="presentation" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                            <asp:Literal runat="server" ID="EmploymentStatusHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBoxList ID="EmploymentStatusCheckBoxList" TextAlign="Right" runat="server" role="presentation"/>

                    </td>
                </tr>
            </table>
            <table role="presentation" style="width:100%;" id="JobSeekerRecordStatusTable" runat="server" Visible="true">
                <tr>
                    <td colspan="2">
                        <strong>
                            <asp:Literal runat="server" ID="JobSeekerRecordStatusHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBoxList ID="JobSeekerRecordStatusCheckBoxList" TextAlign="Right" runat="server" role="presentation"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
