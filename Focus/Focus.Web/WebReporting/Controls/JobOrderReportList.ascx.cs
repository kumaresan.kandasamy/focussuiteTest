﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Report;
using Focus.Core.Views;
using Focus.Web.Code;
using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class JobOrderReportList : ReportControl
	{
    private int _jobOrderCount;

    public JobOrdersReportCriteria ReportCriteria
    {
      get { return GetViewStateValue<JobOrdersReportCriteria>("JobOrderReportList:ReportCriteria"); }
      set { SetViewStateValue("JobOrderReportList:ReportCriteria", value); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
        Localise();
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      EditColumnsLink.Text = CodeLocalise("EditColumnsLink.Text", "edit table columns");
    }

    public void Bind(JobOrdersReportCriteria criteria)
    {
      Visible = true;
      ReportCriteria = criteria;
      JobOrderList.DataBind();
      BindSortingArrows();
    }

    protected void EditColumnsLink_Clicked(object sender, EventArgs e)
    {
      ColumnSelector.Show(ReportCriteria);
    }

    protected void ColumnSelector_ReportColumnsChanged(object sender, CommandEventArgs eventargs)
    {
      Bind((JobOrdersReportCriteria)eventargs.CommandArgument);
      OnReportCriteriaChanged(eventargs);
    }

    protected void JobOrderListDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
      e.InputParameters["criteria"] = ReportCriteria;
    }

    protected void JobOrderList_ItemDatabound(object sender, ListViewItemEventArgs e)
    {
      var jobOrder = (JobOrdersReportView)e.Item.DataItem;
      var jobOrderLink = (LinkButton)e.Item.FindControl("JobOrderLink");
      jobOrderLink.Text = jobOrder.JobTitle;
      jobOrderLink.CommandArgument = jobOrder.FocusJobId.ToString(CultureInfo.InvariantCulture);

      ((Literal)e.Item.FindControl("EmployerLiteral")).Text = jobOrder.Employer;
      ((Literal)e.Item.FindControl("CountyLiteral")).Text = jobOrder.County;
      ((Literal)e.Item.FindControl("StateLiteral")).Text = jobOrder.State;

      BuildOfficeField(jobOrder.Office, e.Item);

      ((Literal)e.Item.FindControl("SuccessfulInvitesLiteral")).Text = jobOrder.Totals.InvitedJobSeekerViewed.ToString(CultureInfo.CurrentUICulture);
      ((Literal)e.Item.FindControl("UnsuccessfulInvitesLiteral")).Text = "TODO";
      ((Literal)e.Item.FindControl("InvitedWhoAppliedLiteral")).Text = jobOrder.Totals.InvitedJobSeekerClicked.ToString(CultureInfo.CurrentUICulture);
      ((Literal)e.Item.FindControl("InvitedWhoDidntApplyLiteral")).Text = "TODO";
      ((Literal)e.Item.FindControl("SelfReferralsLiteral")).Text = jobOrder.Totals.SelfReferrals.ToString(CultureInfo.CurrentUICulture);
      ((Literal)e.Item.FindControl("StaffReferralsLiteral")).Text = jobOrder.Totals.StaffReferrals.ToString(CultureInfo.CurrentUICulture);
      ((Literal)e.Item.FindControl("ReferralsRequestedLiteral")).Text = jobOrder.Totals.ReferralsRequested.ToString(CultureInfo.CurrentUICulture);
      ((Literal)e.Item.FindControl("TotalReferralsLiteral")).Text = jobOrder.Totals.TotalReferrals.ToString(CultureInfo.CurrentUICulture);
      ((Literal)e.Item.FindControl("InvitationsSentLiteral")).Text = jobOrder.Totals.EmployerInvitationsSent.ToString(CultureInfo.CurrentUICulture);
      ((Literal)e.Item.FindControl("HiredLiteral")).Text = jobOrder.Totals.ApplicantsHired.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("NotHiredLiteral")).Text = jobOrder.Totals.ApplicantsNotHired.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("FailedToApplyLiteral")).Text = jobOrder.Totals.ApplicantsDidNotApply.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("FailedToReportToInterviewLiteral")).Text = jobOrder.Totals.ApplicantsFailedToShow.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("InterviewDeniedLiteral")).Text = jobOrder.Totals.ApplicantsDeniedInterviews.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("InterviewScheduledLiteral")).Text = jobOrder.Totals.ApplicantsInterviewed.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("NewApplicantLiteral")).Text = jobOrder.Totals.NewApplicant.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("RecommendedLiteral")).Text = jobOrder.Totals.ApplicantRecommended.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("RefusedOfferLiteral")).Text = jobOrder.Totals.JobOffersRefused.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("UnderConsiderationLiteral")).Text = jobOrder.Totals.UnderConsideration.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("FailedToReportToJobLiteral")).Text = jobOrder.Totals.FailedToReportToJob.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("FailedToRespondToInvitationLiteral")).Text = jobOrder.Totals.FailedToRespondToInvitation.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("FoundJobFromOtherSourceLiteral")).Text = jobOrder.Totals.FoundJobFromOtherSource.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("JobAlreadyFilledLiteral")).Text = jobOrder.Totals.JobAlreadyFilled.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("NotQualifiedLiteral")).Text = jobOrder.Totals.NotQualified.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("NotYetPlacedLiteral")).Text = jobOrder.Totals.ApplicantsNotYetPlaced.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("RefusedReferralLiteral")).Text = jobOrder.Totals.RefusedReferral.ToString(CultureInfo.InvariantCulture);

      SetCellVisibility(e.Item);
    }

    protected void JobOrderList_DataBound(object sender, EventArgs e)
    {
      if (JobOrderList.Items.IsNullOrEmpty())
      {
        JobOrderListPager.Visible = EditColumnsLink.Visible = false;
        return; // No data so empty data template is displayed
      }
      JobOrderListPager.Visible = EditColumnsLink.Visible = true;

      #region header binding

      ((Literal)JobOrderList.FindControl("JobTitleHeader")).Text = CodeLocalise("JobTitleHeader.Text", Constants.Reporting.JobOrderReportDataColumns.JobTitle);
      ((ImageButton)JobOrderList.FindControl("JobTitleSortAscButton")).CommandArgument = ReportJobOrderOrderBy.JobTitle + " " + ReportOrder.Ascending;
      ((ImageButton)JobOrderList.FindControl("JobTitleSortDescButton")).CommandArgument = ReportJobOrderOrderBy.JobTitle + " " + ReportOrder.Descending;

      ((Literal)JobOrderList.FindControl("EmployerHeader")).Text = CodeLocalise("EmployerHeader.Text", Constants.Reporting.JobOrderReportDataColumns.Employer);
      ((ImageButton)JobOrderList.FindControl("EmployerSortAscButton")).CommandArgument = ReportJobOrderOrderBy.Employer + " " + ReportOrder.Ascending;
      ((ImageButton)JobOrderList.FindControl("EmployerSortDescButton")).CommandArgument = ReportJobOrderOrderBy.Employer + " " + ReportOrder.Descending;

      ((Literal)JobOrderList.FindControl("CountyHeader")).Text = CodeLocalise("CountyHeader.Text", Constants.Reporting.JobOrderReportDataColumns.County);
      ((ImageButton)JobOrderList.FindControl("CountySortAscButton")).CommandArgument = ReportJobOrderOrderBy.County + " " + ReportOrder.Ascending;
      ((ImageButton)JobOrderList.FindControl("CountySortDescButton")).CommandArgument = ReportJobOrderOrderBy.County + " " + ReportOrder.Descending;

      ((Literal)JobOrderList.FindControl("StateHeader")).Text = CodeLocalise("StateColumnHeader.Text", Constants.Reporting.JobOrderReportDataColumns.State);
      ((ImageButton)JobOrderList.FindControl("StateSortAscButton")).CommandArgument = ReportJobOrderOrderBy.State + " " + ReportOrder.Ascending;
      ((ImageButton)JobOrderList.FindControl("StateSortDescButton")).CommandArgument = ReportJobOrderOrderBy.State + " " + ReportOrder.Descending;

      ((Literal)JobOrderList.FindControl("OfficeHeader")).Text = CodeLocalise("OfficeColumnHeader.Text", Constants.Reporting.JobOrderReportDataColumns.Office);
      //((ImageButton)JobOrderList.FindControl("OfficeSortAscButton")).CommandArgument = ReportJobOrderOrderBy.Office + " " + ReportOrder.Ascending;
      //((ImageButton)JobOrderList.FindControl("OfficeSortDescButton")).CommandArgument = ReportJobOrderOrderBy.Office + " " + ReportOrder.Descending;

      ((Literal)JobOrderList.FindControl("SuccessfulInvitesHeader")).Text = CodeLocalise("SuccessfulInvitesHeader.Text", Constants.Reporting.JobOrderReportDataColumns.InvitedJobSeekerViewed);
      ((ImageButton)JobOrderList.FindControl("SuccessfulInvitesSortAscButton")).CommandArgument = ReportJobOrderOrderBy.InvitedJobSeekerViewed + " " + ReportOrder.Ascending;
      ((ImageButton)JobOrderList.FindControl("SuccessfulInvitesSortDescButton")).CommandArgument = ReportJobOrderOrderBy.InvitedJobSeekerViewed + " " + ReportOrder.Descending;

      ((Literal)JobOrderList.FindControl("UnsuccessfulInvitesHeader")).Text = CodeLocalise("UnsuccessfulInvitesHeader.Text", "Unsuccessful invites");
      // TODO: Unsuccessful invites
      ((ImageButton)JobOrderList.FindControl("UnsuccessfulInvitesSortAscButton")).CommandArgument = ReportJobOrderOrderBy.InvitedJobSeekerViewed + " " + ReportOrder.Ascending;
      ((ImageButton)JobOrderList.FindControl("UnsuccessfulInvitesSortDescButton")).CommandArgument = ReportJobOrderOrderBy.InvitedJobSeekerViewed + " " + ReportOrder.Descending;

      ((Literal)JobOrderList.FindControl("InvitedWhoAppliedHeader")).Text = CodeLocalise("InvitedWhoAppliedColumnHeader.Text", Constants.Reporting.JobOrderReportDataColumns.InvitedJobSeekerClicked);
      ((ImageButton)JobOrderList.FindControl("InvitedWhoAppliedSortAscButton")).CommandArgument = ReportJobOrderOrderBy.InvitedJobSeekerClicked + " " + ReportOrder.Ascending;
      ((ImageButton)JobOrderList.FindControl("InvitedWhoAppliedSortDescButton")).CommandArgument = ReportJobOrderOrderBy.InvitedJobSeekerClicked + " " + ReportOrder.Descending;

      ((Literal)JobOrderList.FindControl("InvitedWhoDidntApplyHeader")).Text = CodeLocalise("InvitedWhoDidntApplyColumnHeader.Text", "Invitees who didn't apply");
      // TODO: Invitees who didn't appliy
      ((ImageButton)JobOrderList.FindControl("InvitedWhoDidntApplySortAscButton")).CommandArgument = ReportJobOrderOrderBy.InvitedJobSeekerClicked + " " + ReportOrder.Ascending;
      ((ImageButton)JobOrderList.FindControl("InvitedWhoDidntApplySortDescButton")).CommandArgument = ReportJobOrderOrderBy.InvitedJobSeekerClicked + " " + ReportOrder.Descending;

      ((Literal)JobOrderList.FindControl("StaffReferralsHeader")).Text = CodeLocalise("StaffReferralsHeader.Text", Constants.Reporting.JobOrderReportDataColumns.StaffReferrals);
      ((ImageButton)JobOrderList.FindControl("StaffReferralsSortAscButton")).CommandArgument = ReportJobOrderOrderBy.StaffReferrals + " " + ReportOrder.Ascending;
      ((ImageButton)JobOrderList.FindControl("StaffReferralsSortDescButton")).CommandArgument = ReportJobOrderOrderBy.StaffReferrals + " " + ReportOrder.Descending;

      ((Literal)JobOrderList.FindControl("SelfReferralsHeader")).Text = CodeLocalise("SelfReferralsHeader.Text", Constants.Reporting.JobOrderReportDataColumns.SelfReferrals);
      ((ImageButton)JobOrderList.FindControl("SelfReferralsSortAscButton")).CommandArgument = ReportJobOrderOrderBy.SelfReferrals + " " + ReportOrder.Ascending;
      ((ImageButton)JobOrderList.FindControl("SelfReferralsSortDescButton")).CommandArgument = ReportJobOrderOrderBy.SelfReferrals + " " + ReportOrder.Descending;

      ((Literal)JobOrderList.FindControl("ReferralsRequestedHeader")).Text = CodeLocalise("ReferralsRequestedHeader.Text", Constants.Reporting.JobOrderReportDataColumns.ReferralsRequested);
      ((ImageButton)JobOrderList.FindControl("ReferralsRequestedSortAscButton")).CommandArgument = ReportJobOrderOrderBy.ReferralsRequested + " " + ReportOrder.Ascending;
      ((ImageButton)JobOrderList.FindControl("ReferralsRequestedSortDescButton")).CommandArgument = ReportJobOrderOrderBy.ReferralsRequested + " " + ReportOrder.Descending;

      ((Literal)JobOrderList.FindControl("TotalReferralsHeader")).Text = CodeLocalise("TotalReferralsHeader.Text", Constants.Reporting.JobOrderReportDataColumns.TotalReferrals);
      ((ImageButton)JobOrderList.FindControl("TotalReferralsSortAscButton")).CommandArgument = ReportJobOrderOrderBy.TotalReferrals + " " + ReportOrder.Ascending;
      ((ImageButton)JobOrderList.FindControl("TotalReferralsSortDescButton")).CommandArgument = ReportJobOrderOrderBy.TotalReferrals + " " + ReportOrder.Descending;

      ((Literal)JobOrderList.FindControl("InvitationsSentHeader")).Text = CodeLocalise("InvitationsSentColumnHeader.Text", Constants.Reporting.JobOrderReportDataColumns.EmployerInvitationsSent);
      ((ImageButton)JobOrderList.FindControl("InvitationsSentSortAscButton")).CommandArgument = ReportJobOrderOrderBy.EmployerInvitationsSent + " " + ReportOrder.Ascending;
      ((ImageButton)JobOrderList.FindControl("InvitationsSentSortDescButton")).CommandArgument = ReportJobOrderOrderBy.EmployerInvitationsSent + " " + ReportOrder.Descending;

      ((Literal)JobOrderList.FindControl("HiredHeader")).Text = CodeLocalise("HiredHeader.Text", Constants.Reporting.SharedReportDataColumns.Hired);
      ((ImageButton)JobOrderList.FindControl("HiredSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.Hired, " ", ReportOrder.Ascending);
      ((ImageButton)JobOrderList.FindControl("HiredSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.Hired, " ", ReportOrder.Descending);

      ((Literal)JobOrderList.FindControl("NotHiredHeader")).Text = CodeLocalise("NotHiredHeader.Text", Constants.Reporting.SharedReportDataColumns.NotHired);
      ((ImageButton)JobOrderList.FindControl("NotHiredSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.NotHired, " ", ReportOrder.Ascending);
      ((ImageButton)JobOrderList.FindControl("NotHiredSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.NotHired, " ", ReportOrder.Descending);

      ((Literal)JobOrderList.FindControl("FailedToApplyHeader")).Text = CodeLocalise("FailedToApplyHeader.Text", Constants.Reporting.SharedReportDataColumns.FailedToApply);
      ((ImageButton)JobOrderList.FindControl("FailedToApplySortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FailedToApply, " ", ReportOrder.Ascending);
      ((ImageButton)JobOrderList.FindControl("FailedToApplySortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FailedToApply, " ", ReportOrder.Descending);

      ((Literal)JobOrderList.FindControl("FailedToReportToInterviewHeader")).Text = CodeLocalise("FailedToReportToInterviewHeader.Text", Constants.Reporting.SharedReportDataColumns.FailedToReportToInterview);
      ((ImageButton)JobOrderList.FindControl("FailedToReportToInterviewSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FailedToReportToInterview, " ", ReportOrder.Ascending);
      ((ImageButton)JobOrderList.FindControl("FailedToReportToInterviewSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FailedToReportToInterview, " ", ReportOrder.Descending);

      ((Literal)JobOrderList.FindControl("InterviewDeniedHeader")).Text = CodeLocalise("InterviewDeniedHeader.Text", Constants.Reporting.SharedReportDataColumns.InterviewDenied);
      ((ImageButton)JobOrderList.FindControl("InterviewDeniedSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.InterviewDenied, " ", ReportOrder.Ascending);
      ((ImageButton)JobOrderList.FindControl("InterviewDeniedSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.InterviewDenied, " ", ReportOrder.Descending);

      ((Literal)JobOrderList.FindControl("InterviewScheduledHeader")).Text = CodeLocalise("InterviewScheduledHeader.Text", Constants.Reporting.SharedReportDataColumns.InterviewScheduled);
      ((ImageButton)JobOrderList.FindControl("InterviewScheduledSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.InterviewScheduled, " ", ReportOrder.Ascending);
      ((ImageButton)JobOrderList.FindControl("InterviewScheduledSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.InterviewScheduled, " ", ReportOrder.Descending);

      ((Literal)JobOrderList.FindControl("NewApplicantHeader")).Text = CodeLocalise("NewApplicantHeader.Text", Constants.Reporting.SharedReportDataColumns.NewApplicant);
      ((ImageButton)JobOrderList.FindControl("NewApplicantSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.NewApplicant, " ", ReportOrder.Ascending);
      ((ImageButton)JobOrderList.FindControl("NewApplicantSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.NewApplicant, " ", ReportOrder.Descending);

      ((Literal)JobOrderList.FindControl("RecommendedHeader")).Text = CodeLocalise("RecommendedHeader.Text", Constants.Reporting.SharedReportDataColumns.Recommended);
      ((ImageButton)JobOrderList.FindControl("RecommendedSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.Recommended, " ", ReportOrder.Ascending);
      ((ImageButton)JobOrderList.FindControl("RecommendedSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.Recommended, " ", ReportOrder.Descending);

      ((Literal)JobOrderList.FindControl("RefusedOfferHeader")).Text = CodeLocalise("RefusedOfferHeader.Text", Constants.Reporting.SharedReportDataColumns.RefusedOffer);
      ((ImageButton)JobOrderList.FindControl("RefusedOfferSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.RefusedOffer, " ", ReportOrder.Ascending);
      ((ImageButton)JobOrderList.FindControl("RefusedOfferSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.RefusedOffer, " ", ReportOrder.Descending);

      ((Literal)JobOrderList.FindControl("UnderConsiderationHeader")).Text = CodeLocalise("UnderConsiderationHeader.Text", Constants.Reporting.SharedReportDataColumns.UnderConsideration);
      ((ImageButton)JobOrderList.FindControl("UnderConsiderationSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.UnderConsideration, " ", ReportOrder.Ascending);
      ((ImageButton)JobOrderList.FindControl("UnderConsiderationSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.UnderConsideration, " ", ReportOrder.Descending);

      ((Literal)JobOrderList.FindControl("FailedToReportToJobHeader")).Text = CodeLocalise("FailedToReportToJobHeader.Text", Constants.Reporting.SharedReportDataColumns.FailedToReportToJob);
      ((ImageButton)JobOrderList.FindControl("FailedToReportToJobSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FailedToReportToJob, " ", ReportOrder.Ascending);
      ((ImageButton)JobOrderList.FindControl("FailedToReportToJobSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FailedToReportToJob, " ", ReportOrder.Descending);

      ((Literal)JobOrderList.FindControl("FailedToRespondToInvitationHeader")).Text = CodeLocalise("FailedToRespondToInvitationHeader.Text", Constants.Reporting.SharedReportDataColumns.FailedToRespondToInvitation);
      ((ImageButton)JobOrderList.FindControl("FailedToRespondToInvitationSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FailedToRespondToInvitation, " ", ReportOrder.Ascending);
      ((ImageButton)JobOrderList.FindControl("FailedToRespondToInvitationSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FailedToRespondToInvitation, " ", ReportOrder.Descending);

      ((Literal)JobOrderList.FindControl("FoundJobFromOtherSourceHeader")).Text = CodeLocalise("FoundJobFromOtherSourceHeader.Text", Constants.Reporting.SharedReportDataColumns.FoundJobFromOtherSource);
      ((ImageButton)JobOrderList.FindControl("FoundJobFromOtherSourceSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FoundJobFromOtherSource, " ", ReportOrder.Ascending);
      ((ImageButton)JobOrderList.FindControl("FoundJobFromOtherSourceSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FoundJobFromOtherSource, " ", ReportOrder.Descending);

      ((Literal)JobOrderList.FindControl("JobAlreadyFilledHeader")).Text = CodeLocalise("JobAlreadyFilledHeader.Text", Constants.Reporting.SharedReportDataColumns.JobAlreadyFilled);
      ((ImageButton)JobOrderList.FindControl("JobAlreadyFilledSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.JobAlreadyFilled, " ", ReportOrder.Ascending);
      ((ImageButton)JobOrderList.FindControl("JobAlreadyFilledSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.JobAlreadyFilled, " ", ReportOrder.Descending);

      ((Literal)JobOrderList.FindControl("NotQualifiedHeader")).Text = CodeLocalise("NotQualifiedHeader.Text", Constants.Reporting.SharedReportDataColumns.NotQualified);
      ((ImageButton)JobOrderList.FindControl("NotQualifiedSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.NotQualified, " ", ReportOrder.Ascending);
      ((ImageButton)JobOrderList.FindControl("NotQualifiedSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.NotQualified, " ", ReportOrder.Descending);

      ((Literal)JobOrderList.FindControl("NotYetPlacedHeader")).Text = CodeLocalise("NotYetPlacedHeader.Text", Constants.Reporting.SharedReportDataColumns.NotYetPlaced);
      ((ImageButton)JobOrderList.FindControl("NotYetPlacedSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.NotYetPlaced, " ", ReportOrder.Ascending);
      ((ImageButton)JobOrderList.FindControl("NotYetPlacedSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.NotYetPlaced, " ", ReportOrder.Descending);

      ((Literal)JobOrderList.FindControl("RefusedReferralHeader")).Text = CodeLocalise("RefusedReferralHeader.Text", Constants.Reporting.SharedReportDataColumns.RefusedReferral);
      ((ImageButton)JobOrderList.FindControl("RefusedReferralSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.RefusedReferral, " ", ReportOrder.Ascending);
      ((ImageButton)JobOrderList.FindControl("RefusedReferralSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.RefusedReferral, " ", ReportOrder.Descending);
      
      #endregion

      SetHeaderVisibility();
    }

    protected void JobOrderList_Sorting(object sender, ListViewSortEventArgs e)
    {
      if (e.SortExpression.IsNullOrEmpty())
        throw new Exception("Job order report sorting error.");

      var sortingParams = e.SortExpression.Split(Convert.ToChar(" "));

      if (sortingParams.IsNullOrEmpty() || sortingParams.GetLength(0) != 2)
        throw new Exception("Job order report sorting error getting parameters.");

      ReportJobOrderOrderBy orderby;
      ReportOrder orderDirection;

      if (!Enum.TryParse(sortingParams[0], out orderby))
        throw new Exception("Job order report sorting error getting order by values.");
      if (!Enum.TryParse(sortingParams[1], out orderDirection))
        throw new Exception("Job order report sorting error getting order direction values.");

      ReportCriteria.OrderInfo = new JobOrdersReportSortOrder { OrderBy = orderby, Direction = orderDirection };

      BindSortingArrows();
    }

    /// <summary>
    /// Fires when the job seeker link is click to navigate to the job seeker profile page
    /// </summary>
    /// <param name="sender">Link button raising the event</param>
    /// <param name="e">Command arguments for the link</param>
    protected void JobOrderLink_OnCommand(object sender, CommandEventArgs e)
    {
      var jobId = long.Parse(e.CommandArgument.ToString());
      var reportId = ServiceClientLocator.ReportClient(App).SaveReportToSession(ReportCriteria);

      Response.Redirect(UrlBuilder.ReportingJobView(jobId, reportId.ToString(CultureInfo.InvariantCulture)));
    }

    #region helpers

    private void SetHeaderVisibility()
    {
      // TODO: Not all criteria items available
      JobOrderList.FindControl("SuccessfulInvitesColumnHeader").Visible = ReportCriteria.JobOrdersActivitySummaryInfo.InvitedJobSeekerViewed;
      JobOrderList.FindControl("UnsuccessfulInvitesColumnHeader").Visible = false;
      JobOrderList.FindControl("InvitedWhoAppliedColumnHeader").Visible = ReportCriteria.JobOrdersActivitySummaryInfo.InvitedJobSeekerClicked;
      JobOrderList.FindControl("InvitedWhoDidntApplyColumnHeader").Visible = false;

      JobOrderList.FindControl("StaffReferralsColumnHeader").Visible = ReportCriteria.JobOrdersActivitySummaryInfo.StaffReferrals;
      JobOrderList.FindControl("SelfReferralsColumnHeader").Visible = ReportCriteria.JobOrdersActivitySummaryInfo.SelfReferrals;
      JobOrderList.FindControl("ReferralsRequestedColumnHeader").Visible = ReportCriteria.JobOrdersActivitySummaryInfo.ReferralsRequested;
      JobOrderList.FindControl("TotalReferralsColumnHeader").Visible = ReportCriteria.JobOrdersActivitySummaryInfo.TotalReferrals;
      JobOrderList.FindControl("InvitationsSentColumnHeader").Visible = ReportCriteria.JobOrdersActivitySummaryInfo.EmployerInvitationsSent;

      if (ReportCriteria.JobOrdersReferralOutcomesInfo.IsNotNull())
      {
        JobOrderList.FindControl("HiredColumnHeader").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.Hired;
        JobOrderList.FindControl("NotHiredColumnHeader").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.NotHired;
        JobOrderList.FindControl("FailedToApplyColumnHeader").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.FailedToApply;
        JobOrderList.FindControl("FailedToReportToInterviewColumnHeader").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.FailedToReportToInterview;
        JobOrderList.FindControl("InterviewDeniedColumnHeader").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.InterviewDenied;
        JobOrderList.FindControl("InterviewScheduledColumnHeader").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.InterviewScheduled;
        JobOrderList.FindControl("NewApplicantColumnHeader").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.NewApplicant;
        JobOrderList.FindControl("RecommendedColumnHeader").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.Recommended;
        JobOrderList.FindControl("RefusedOfferColumnHeader").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.RefusedOffer;
        JobOrderList.FindControl("UnderConsiderationColumnHeader").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.UnderConsideration;
        JobOrderList.FindControl("FailedToReportToJobColumnHeader").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.FailedToReportToJob;
        JobOrderList.FindControl("FailedToRespondToInvitationColumnHeader").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.FailedToRespondToInvitation;
        JobOrderList.FindControl("FoundJobFromOtherSourceColumnHeader").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.FoundJobFromOtherSource;
        JobOrderList.FindControl("JobAlreadyFilledColumnHeader").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.JobAlreadyFilled;
        JobOrderList.FindControl("NotQualifiedColumnHeader").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.NotQualified;
        JobOrderList.FindControl("NotYetPlacedColumnHeader").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.NotYetPlaced;
        JobOrderList.FindControl("RefusedReferralColumnHeader").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.RefusedReferral;
      }
    }

    private void SetCellVisibility(ListViewItem item)
    {
      // TODO: Not all criteria items available
      item.FindControl("SuccessfulInvitesCell").Visible = ReportCriteria.JobOrdersActivitySummaryInfo.InvitedJobSeekerViewed;
      item.FindControl("UnsuccessfulInvitesViewedCell").Visible = false;
      item.FindControl("InvitedWhoAppliedCell").Visible = ReportCriteria.JobOrdersActivitySummaryInfo.InvitedJobSeekerClicked;
      item.FindControl("InvitedWhoDidntApplyCell").Visible = false;

      item.FindControl("StaffReferralsCell").Visible = ReportCriteria.JobOrdersActivitySummaryInfo.StaffReferrals;
      item.FindControl("SelfReferralsCell").Visible = ReportCriteria.JobOrdersActivitySummaryInfo.SelfReferrals;
      item.FindControl("ReferralsRequestedCell").Visible = ReportCriteria.JobOrdersActivitySummaryInfo.ReferralsRequested; 
      item.FindControl("TotalReferralsCell").Visible = ReportCriteria.JobOrdersActivitySummaryInfo.TotalReferrals;
      item.FindControl("InvitationsSentCell").Visible = ReportCriteria.JobOrdersActivitySummaryInfo.EmployerInvitationsSent;

      if (ReportCriteria.JobOrdersReferralOutcomesInfo.IsNotNull())
      {
        item.FindControl("HiredCell").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.Hired;
        item.FindControl("NotHiredCell").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.NotHired;
        item.FindControl("FailedToApplyCell").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.FailedToApply;
        item.FindControl("FailedToReportToInterviewCell").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.FailedToReportToInterview;
        item.FindControl("InterviewDeniedCell").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.InterviewDenied;
        item.FindControl("InterviewScheduledCell").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.InterviewScheduled;
        item.FindControl("NewApplicantCell").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.NewApplicant;
        item.FindControl("RecommendedCell").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.Recommended;
        item.FindControl("RefusedOfferCell").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.RefusedOffer;
        item.FindControl("UnderConsiderationCell").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.UnderConsideration;
        item.FindControl("FailedToReportToJobCell").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.FailedToReportToJob;
        item.FindControl("FailedToRespondToInvitationCell").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.FailedToRespondToInvitation;
        item.FindControl("FoundJobFromOtherSourceCell").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.FoundJobFromOtherSource;
        item.FindControl("JobAlreadyFilledCell").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.JobAlreadyFilled;
        item.FindControl("NotQualifiedCell").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.NotQualified;
        item.FindControl("NotYetPlacedCell").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.NotYetPlaced;
        item.FindControl("RefusedReferralCell").Visible = ReportCriteria.JobOrdersReferralOutcomesInfo.RefusedReferral;
      }
    }

    #endregion

    #region list binding methods

    public List<JobOrdersReportView> GetJobOrderList(JobOrdersReportCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
    {
      var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

      criteria.PageSize = maximumRows;
      criteria.PageIndex = pageIndex;

      var jobOrders = ServiceClientLocator.ReportClient(App).GetJobOrdersReport(criteria);
      _jobOrderCount = jobOrders.TotalCount;

      return jobOrders;
    }


    public int GetJobOrderListCount(JobOrdersReportCriteria criteria)
    {
      return _jobOrderCount;
    }


    public int GetJobOrderListCount()
    {
      return _jobOrderCount;
    }

    #endregion

    #region list formatting

    /// <summary>
    /// Binds the sorting arrows.
    /// </summary>
    private void BindSortingArrows()
    {
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("JobTitleSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("JobTitleSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("EmployerSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("EmployerSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("CountySortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("CountySortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("StateSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("StateSortDescButton"));
      //FormatSortingButtons((ImageButton)JobOrderList.FindControl("OfficeSortAscButton"));
      //FormatSortingButtons((ImageButton)JobOrderList.FindControl("OfficeSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("SuccessfulInvitesSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("SuccessfulInvitesSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("UnsuccessfulInvitesSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("UnsuccessfulInvitesSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("InvitedWhoAppliedSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("InvitedWhoAppliedSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("InvitedWhoDidntApplySortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("InvitedWhoDidntApplySortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("SelfReferralsSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("SelfReferralsSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("ReferralsRequestedSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("ReferralsRequestedSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("StaffReferralsSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("StaffReferralsSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("TotalReferralsSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("TotalReferralsSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("InvitationsSentSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("InvitationsSentSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("HiredSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("HiredSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("NotHiredSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("NotHiredSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("FailedToApplySortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("FailedToApplySortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("FailedToReportToInterviewSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("FailedToReportToInterviewSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("InterviewDeniedSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("InterviewDeniedSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("InterviewScheduledSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("InterviewScheduledSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("NewApplicantSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("NewApplicantSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("RecommendedSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("RecommendedSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("RefusedOfferSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("RefusedOfferSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("UnderConsiderationSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("UnderConsiderationSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("FailedToReportToJobSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("FailedToReportToJobSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("FailedToRespondToInvitationSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("FailedToRespondToInvitationSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("FoundJobFromOtherSourceSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("FoundJobFromOtherSourceSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("JobAlreadyFilledSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("JobAlreadyFilledSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("NotQualifiedSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("NotQualifiedSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("NotYetPlacedSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("NotYetPlacedSortDescButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("RefusedReferralSortAscButton"));
      FormatSortingButtons((ImageButton)JobOrderList.FindControl("RefusedReferralSortDescButton"));
    }

    /// <summary>
    /// Formats the sorting buttons.
    /// </summary>
    /// <param name="button">The button.</param>
    /// <exception cref="System.Exception">
    /// Job order report sorting error getting parameters.
    /// or
    /// Job order report sorting error getting order by values.
    /// or
    /// Job order report sorting error getting order direction values.
    /// </exception>
    private void FormatSortingButtons(ImageButton button)
    {
      if (button.IsNull()) return;

      var sortingParams = button.CommandArgument.Split(Convert.ToChar(" "));

      if (sortingParams.IsNullOrEmpty() || sortingParams.GetLength(0) != 2)
        throw new Exception("Job order report sorting error getting parameters.");

      ReportJobOrderOrderBy orderby;
      ReportOrder orderDirection;

      if (!Enum.TryParse(sortingParams[0], out orderby))
        throw new Exception("Job order report sorting error getting order by values.");
      if (!Enum.TryParse(sortingParams[1], out orderDirection))
        throw new Exception("Job order report sorting error getting order direction values.");

      if (orderby == ReportCriteria.OrderInfo.OrderBy && orderDirection == ReportCriteria.OrderInfo.Direction)
      {
        button.Enabled = false;
        button.ImageUrl = orderDirection == ReportOrder.Ascending ? UrlBuilder.ActivityListSortAscImage() : UrlBuilder.ActivityListSortDescImage();
      }
      else
      {
        button.Enabled = true;
        button.ImageUrl = orderDirection == ReportOrder.Ascending ? UrlBuilder.ActivityOnSortAscImage() : UrlBuilder.CategorySortDescImage();
      }

			button.ToolTip = button.AlternateText = orderDirection == ReportOrder.Ascending
				? CodeLocalise("Global.SortAscending", "Sort ascending")
				: CodeLocalise("Global.SortDescending", "Sort descending");
    }

    #endregion

    #region page generated events

    public delegate void ReportCriteriaChangedHandler(object sender, CommandEventArgs eventArgs);
    public event ReportCriteriaChangedHandler ReportCriteriaChanged;

    /// <summary>
    /// Raises the <see cref="CommandEventArgs" /> event.
    /// </summary>
    /// <param name="eventargs">The <see cref="CommandEventArgs" /> instance containing the event data.</param>
    public void OnReportCriteriaChanged(CommandEventArgs eventargs)
    {
      var handler = ReportCriteriaChanged;
      if (handler != null) handler(this, eventargs);
    }

    #endregion

    
  }
}