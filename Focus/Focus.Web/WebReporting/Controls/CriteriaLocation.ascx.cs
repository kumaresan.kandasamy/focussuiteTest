﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Report;
using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaLocation : UserControlBase
	{
    public bool ShowCounty { get; set; }
    public ReportType ReportType { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();
        Bind();
      }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
      CountyControls.Visible = ShowCounty;

      OfficeControls.Visible = App.Settings.OfficesEnabled;

      switch (ReportType)
      {
        case ReportType.Employer:
          CountyAutoCompleteExtender.ServiceMethod = "GetReportingEmployerCounties";
          break;
        case ReportType.JobOrder:
          CountyAutoCompleteExtender.ServiceMethod = "GetReportingJobOrderCounties";
          break;
      }
    }

    /// <summary>
    /// Resets this instance.
    /// </summary>
    private void Reset()
    {
      OfficeDropDown.SelectedIndex = -1;
      CountyTextBox.Text = ZipCodeTextBox.Text = string.Empty;
      HiddenCountyList.Value = HiddenOfficeList.Value = string.Empty;
      RadiusDropDown.SelectedIndex = 0;
    }

    /// <summary>
    /// Unbinds this instance.
    /// </summary>
    /// <returns></returns>
    public LocationReportCriteria Unbind()
    {
      var delimiter = new[] {"|*|"};
      var criteria = new LocationReportCriteria
                       {
                         Counties =
                           HiddenCountyList.Value.Split(delimiter, StringSplitOptions.RemoveEmptyEntries).ToList(),
                         Offices =
                           HiddenOfficeList.Value.Split(delimiter, StringSplitOptions.RemoveEmptyEntries).ToList(),
                         PostalCode = ZipCodeTextBox.Text
                       };
      if (RadiusDropDown.SelectedIndex > 0)
        criteria.DistanceFromPostalCode = Convert.ToInt32(RadiusDropDown.SelectedValue);

      return criteria;
    }

    public void BindCriteria(LocationReportCriteria criteria, ReportType reportType)
    {
      Reset();

      if (App.Settings.OfficesEnabled)
      {
        OfficeDropDown.Items.Clear();
        OfficeDropDown.DataSource = ServiceClientLocator.ReportClient(App).GetReportOfficeLookup(reportType);
        OfficeDropDown.DataValueField = "Name";
        OfficeDropDown.DataTextField = "Name";
        OfficeDropDown.DataBind();
        OfficeDropDown.Items.AddLocalisedTopDefault("OfficeDropDown.Default.Text.NoEdit", "Office location");
      }

      if (criteria.IsNull()) return;

      const string delimiter = "|*|";
      if (criteria.Counties.IsNotNullOrEmpty())
        HiddenCountyList.Value = string.Join(delimiter, criteria.Counties);
      if (criteria.Offices.IsNotNullOrEmpty())
        HiddenOfficeList.Value = string.Join(delimiter, criteria.Offices);
      ZipCodeTextBox.Text = criteria.PostalCode;
      if (criteria.DistanceFromPostalCode.IsNotNull())
        RadiusDropDown.SelectedValue = criteria.DistanceFromPostalCode.ToString();

    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      CriteriaLocationLabel.Text = CodeLocalise("CriteriaLocationLabel.Text", "Location");
      CountyHeader.Text = CodeLocalise("CountyHeader.Text", "County");
      OfficeHeader.Text = CodeLocalise("OfficeHeader.Text", "Office");
      DistanceFromHeader.Text = CodeLocalise("DistanceFromHeader.Text", "Distance from");
      CountyButton.Value = OfficeButton.Value = CodeLocalise("Global.Add.NoEdit", "Add");
    }

    /// <summary>
    /// Binds this instance.
    /// </summary>
    private void Bind()
    {
      RadiusDropDown.Items.Add(string.Empty);
      RadiusDropDown.Items.Add("5");
      RadiusDropDown.Items.Add("10");
      RadiusDropDown.Items.Add("25");
      RadiusDropDown.Items.Add("50");
      RadiusDropDown.Items.Add("75");
      RadiusDropDown.Items.Add("100");
    }
  }
}