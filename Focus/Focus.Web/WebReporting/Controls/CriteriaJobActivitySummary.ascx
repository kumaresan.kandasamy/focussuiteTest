﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaJobActivitySummary.ascx.cs"
    Inherits="Focus.Web.WebReporting.Controls.CriteriaJobActivitySummary" %>
<table role="presentation" width="100%" class="accordionWithIcon">
    <tr class="multipleAccordionTitle">
        <td valign="top" width="10%">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td width="90%">
            <asp:Literal runat="server" ID="CriteriaJobActivitySummaryLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
            <table role="presentation" width="100%">
                <tr>
                    <td>
                        <strong>
                            <asp:Literal runat="server" ID="InvitationsHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="ViewedJobCheckBox" />
                    </td>
                </tr>
                <tr id="DidntViewJobRow" runat="server" Visible="False">
                    <td>
                        <asp:CheckBox runat="server" ID="DidntViewJobCheckBox" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="ClickedHowToApplyCheckBox" />
                    </td>
                </tr>
                <tr id="DidntClickHowToApplyRow" runat="server" Visible="False">
                    <td>
                        <asp:CheckBox runat="server" ID="DidntClickHowToApplyCheckBox" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="EmployerInvitationsSentCheckBox" />
                    </td>
                </tr>
            </table>
            <table role="presentation" width="100%">
                <tr>
                    <td>
                        <strong>
                            <asp:Literal runat="server" ID="ReferralsHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="StaffReferralsCheckBox" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="SelfReferralsCheckBox" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="ReferralsRequestedCheckBox" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="TotalReferralsCheckBox" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
