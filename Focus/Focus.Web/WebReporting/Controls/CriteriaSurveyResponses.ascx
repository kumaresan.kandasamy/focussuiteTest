﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaSurveyResponses.ascx.cs"
    Inherits="Focus.Web.WebReporting.Controls.CriteriaSurveyResponses" %>
<table width="100%" class="accordionWithIcon">
    <tr class="multipleAccordionTitle">
        <td valign="top" width="10%">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td width="90%">
            <asp:Literal runat="server" ID="CriteriaSurveyResponsesLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <strong>
                            <asp:Literal runat="server" ID="ShowNumberofSeekerWhoHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBoxList ID="SurveyResponsesCheckBoxList" TextAlign="Right" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
