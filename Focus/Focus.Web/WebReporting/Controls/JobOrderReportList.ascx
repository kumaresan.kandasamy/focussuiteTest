﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobOrderReportList.ascx.cs" Inherits="Focus.Web.WebReporting.Controls.JobOrderReportList" %>
<%@ Register TagPrefix="uc" TagName="Pager" Src="~/Code/Controls/User/Pager.ascx" %>
<%@ Register src="ColumnSelector.ascx" tagName="ColumnSelector" tagPrefix="uc"  %>

<table role="presentation">
  <tr>
    <td style="width: 100%;">
      <uc:Pager ID="JobOrderListPager" runat="server" PagedControlID="JobOrderList" PageSize="10" DisplayRecordCount="True" />
    </td>
    <td style="white-space: nowrap">
      <asp:LinkButton runat="server" ID="EditColumnsLink" OnClick="EditColumnsLink_Clicked" />
    </td>
  </tr>
</table>
<div class="horizontalscroll">
     <asp:ListView runat="server" ID="JobOrderList" ItemPlaceholderID="JobOrderListPlaceHolder"
        OnItemDataBound="JobOrderList_ItemDatabound" DataKeyNames="FocusJobId" OnDataBound="JobOrderList_DataBound"
        OnSorting="JobOrderList_Sorting" DataSourceID="JobOrderListDataSource">
        <LayoutTemplate>
            <table style="width: 100%;" class="table">
            <tr>
                <th style="white-space: nowrap">
                      <asp:Literal runat="server" ID="JobTitleHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="JobTitleSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="JobTitle asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="JobTitleSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="JobTitle desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="EmployerColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="EmployerHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="EmployerSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="Employer asc" CssClass="AscButton" AlternateText="." />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="EmployerSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="Employer desc" CssClass="DescButton" AlternateText="." />
                      </div>
                </th>
                <th runat="server" id="CountyColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="CountyHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="CountySortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="County asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="CountySortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="County desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="StateColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="StateHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="StateSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="State asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="StateSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="State desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="OfficeColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="OfficeHeader" />
                </th>
                <th runat="server" id="SuccessfulInvitesColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="SuccessfulInvitesHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="SuccessfulInvitesSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="InvitedJobSeekerViewed asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="SuccessfulInvitesSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="InvitedJobSeekerViewed desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="UnsuccessfulInvitesColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="UnsuccessfulInvitesHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="UnsuccessfulInvitesSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="unsuccessfulinvites asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="UnsuccessfulInvitesSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="unsuccessfulinvites desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="InvitedWhoAppliedColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="InvitedWhoAppliedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="InvitedWhoAppliedSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="invitedwhoapplied asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="InvitedWhoAppliedSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="invitedwhoapplied desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="InvitedWhoDidntApplyColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="InvitedWhoDidntApplyHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="InvitedWhoDidntApplySortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="invitedwhodidntapply asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="InvitedWhoDidntApplySortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="invitedwhodidntapply desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="InvitationsSentColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="InvitationsSentHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="InvitationsSentSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="EmployerInvitationsSent asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="InvitationsSentSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="EmployerInvitationsSent desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="StaffReferralsColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="StaffReferralsHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="StaffReferralsSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="StaffReferrals asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="StaffReferralsSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="StaffReferrals desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="SelfReferralsColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="SelfReferralsHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="SelfReferralsSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="SelfReferrals asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="SelfReferralsSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="SelfReferrals desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="ReferralsRequestedColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="ReferralsRequestedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="ReferralsRequestedSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="ReferralsRequested asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="ReferralsRequestedSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="ReferralsRequested desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="TotalReferralsColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="TotalReferralsHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="TotalReferralsSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="TotalReferrals asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="TotalReferralsSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="TotalReferrals desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="FailedToApplyColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="FailedToApplyHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="FailedToApplySortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="FailedToApply asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="FailedToApplySortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="FailedToApply desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="FailedToReportToInterviewColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="FailedToReportToInterviewHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="FailedToReportToInterviewSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="FailedToReportToInterview asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="FailedToReportToInterviewSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="FailedToReportToInterview desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="FailedToReportToJobColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="FailedToReportToJobHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="FailedToReportToJobSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="FailedToReportToJob asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="FailedToReportToJobSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="FailedToReportToJob desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="FailedToRespondToInvitationColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="FailedToRespondToInvitationHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="FailedToRespondToInvitationSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="FailedToRespondToInvitation asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="FailedToRespondToInvitationSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="FailedToRespondToInvitation desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="FoundJobFromOtherSourceColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="FoundJobFromOtherSourceHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="FoundJobFromOtherSourceSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="FoundJobFromOtherSource asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="FoundJobFromOtherSourceSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="FoundJobFromOtherSource desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="HiredColumnHeader" style="white-space: nowrap">
                    <asp:Literal runat="server" ID="HiredHeader" />
                    <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="HiredSortAscButton"
                                runat="server" CommandName="Sort" CommandArgument="Hired asc" CssClass="AscButton" AlternateText="."/>
                       <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="HiredSortDescButton"
                                runat="server" CommandName="Sort" CommandArgument="Hired desc" CssClass="DescButton" AlternateText="."/>
                    </div>
                </th>
                <th runat="server" id="InterviewDeniedColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="InterviewDeniedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="InterviewDeniedSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="InterviewDenied asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="InterviewDeniedSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="InterviewDenied desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="InterviewScheduledColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="InterviewScheduledHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="InterviewScheduledSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="InterviewScheduled asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="InterviewScheduledSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="InterviewScheduled desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="JobAlreadyFilledColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="JobAlreadyFilledHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="JobAlreadyFilledSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="JobAlreadyFilled asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="JobAlreadyFilledSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="JobAlreadyFilled desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="NewApplicantColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="NewApplicantHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="NewApplicantSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="NewApplicant asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="NewApplicantSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="NewApplicant desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="NotHiredColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="NotHiredHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="NotHiredSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="NotHired asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="NotHiredSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="NotHired desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="NotQualifiedColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="NotQualifiedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="NotQualifiedSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="NotQualified asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="NotQualifiedSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="NotQualified desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="NotYetPlacedColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="NotYetPlacedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="NotYetPlacedSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="NotYetPlaced asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="NotYetPlacedSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="NotYetPlaced desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="RecommendedColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="RecommendedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="RecommendedSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="Recommended asc" CssClass="AscButton" AlternateText="."/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="RecommendedSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="Recommended desc" CssClass="DescButton" AlternateText="."/>
                      </div>
                </th>
                <th runat="server" id="RefusedOfferColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="RefusedOfferHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="RefusedOfferSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="RefusedOffer asc" CssClass="AscButton" AlternateText="." />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="RefusedOfferSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="RefusedOffer desc" CssClass="DescButton" AlternateText="." />
                      </div>
                </th>
                <th runat="server" id="RefusedReferralColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="RefusedReferralHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="RefusedReferralSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="RefusedReferral asc" CssClass="AscButton" AlternateText="." />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="RefusedReferralSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="RefusedReferral desc" CssClass="DescButton" AlternateText="." />
                      </div>
                </th>
                <th runat="server" id="UnderConsiderationColumnHeader" style="white-space: nowrap">
                      <asp:Literal runat="server" ID="UnderConsiderationHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="UnderConsiderationSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="UnderConsideration asc" CssClass="AscButton" AlternateText="." />
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="UnderConsiderationSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="UnderConsideration desc" CssClass="DescButton" AlternateText="." />
                      </div>
                </th>
                </tr>
                <asp:PlaceHolder ID="JobOrderListPlaceHolder" runat="server" />
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td nowrap="nowrap">
                    <asp:LinkButton runat="server" ID="JobOrderLink" CommandName="Posting" OnCommand="JobOrderLink_OnCommand"></asp:LinkButton>
                </td>
                <td id="EmployerCell" runat="server">
                    <asp:Literal runat="server" ID="EmployerLiteral"></asp:Literal>
                </td>
                <td id="CountyCell" runat="server">
                    <asp:Literal runat="server" ID="CountyLiteral"></asp:Literal>
                </td>
                <td id="StateCell" runat="server">
                    <asp:Literal runat="server" ID="StateLiteral"></asp:Literal>
                </td>
                <td id="OfficeCell" runat="server" style="white-space: nowrap">
                    <asp:Literal runat="server" ID="OfficeLiteral"></asp:Literal>
                    <asp:HyperLink runat="server" ID="ShowAdditionalOfficesLink" Visible="False" NavigateUrl="javascript:void(0);"  onclick="Report_ShowAdditionalOffices($(this));" ClientIDMode="Predictable"></asp:HyperLink>
                    <span style="display: none">
                      <asp:Literal runat="server" ID="AdditionalOfficesLiteral"></asp:Literal>
                      <a href="javascript:void(0);" onclick="Report_HideAdditionalOffices($(this));"><%=HtmlLocalise("Hide.text", "Hide") %></a>
                    </span>
                </td>
                <td id="SuccessfulInvitesCell" runat="server">
                    <asp:Literal runat="server" ID="SuccessfulInvitesLiteral"></asp:Literal>
                </td>
                <td id="UnsuccessfulInvitesViewedCell" runat="server">
                    <asp:Literal runat="server" ID="UnsuccessfulInvitesLiteral"></asp:Literal>
                </td>
                <td id="InvitedWhoAppliedCell" runat="server">
                    <asp:Literal runat="server" ID="InvitedWhoAppliedLiteral"></asp:Literal>
                </td>
                <td id="InvitedWhoDidntApplyCell" runat="server">
                    <asp:Literal runat="server" ID="InvitedWhoDidntApplyLiteral"></asp:Literal>
                </td>
                <td id="InvitationsSentCell" runat="server">
                    <asp:Literal runat="server" ID="InvitationsSentLiteral"></asp:Literal>
                </td>
                <td id="StaffReferralsCell" runat="server">
                    <asp:Literal runat="server" ID="StaffReferralsLiteral"></asp:Literal>
                </td>
                <td id="SelfReferralsCell" runat="server">
                    <asp:Literal runat="server" ID="SelfReferralsLiteral"></asp:Literal>
                </td>
                <td id="ReferralsRequestedCell" runat="server">
                    <asp:Literal runat="server" ID="ReferralsRequestedLiteral"></asp:Literal>
                </td>
                <td id="TotalReferralsCell" runat="server">
                    <asp:Literal runat="server" ID="TotalReferralsLiteral"></asp:Literal>
                </td>
                <td id="FailedToApplyCell" runat="server">
                    <asp:Literal runat="server" ID="FailedToApplyLiteral"></asp:Literal>
                </td>
                <td id="FailedToReportToInterviewCell" runat="server">
                    <asp:Literal runat="server" ID="FailedToReportToInterviewLiteral"></asp:Literal>
                </td>
                <td id="FailedToReportToJobCell" runat="server">
                    <asp:Literal runat="server" ID="FailedToReportToJobLiteral"></asp:Literal>
                </td>
                <td id="FailedToRespondToInvitationCell" runat="server">
                    <asp:Literal runat="server" ID="FailedToRespondToInvitationLiteral"></asp:Literal>
                </td>
                <td id="FoundJobFromOtherSourceCell" runat="server">
                    <asp:Literal runat="server" ID="FoundJobFromOtherSourceLiteral"></asp:Literal>
                </td>
                <td id="HiredCell" runat="server">
                    <asp:Literal runat="server" ID="HiredLiteral"></asp:Literal>
                </td>
                <td id="InterviewDeniedCell" runat="server">
                    <asp:Literal runat="server" ID="InterviewDeniedLiteral"></asp:Literal>
                </td>
                <td id="InterviewScheduledCell" runat="server">
                    <asp:Literal runat="server" ID="InterviewScheduledLiteral"></asp:Literal>
                </td>
                <td id="JobAlreadyFilledCell" runat="server">
                    <asp:Literal runat="server" ID="JobAlreadyFilledLiteral"></asp:Literal>
                </td>
                <td id="NewApplicantCell" runat="server">
                    <asp:Literal runat="server" ID="NewApplicantLiteral"></asp:Literal>
                </td>
                <td id="NotHiredCell" runat="server">
                    <asp:Literal runat="server" ID="NotHiredLiteral"></asp:Literal>
                </td>
                <td id="NotQualifiedCell" runat="server">
                    <asp:Literal runat="server" ID="NotQualifiedLiteral"></asp:Literal>
                </td>
                <td id="NotYetPlacedCell" runat="server">
                    <asp:Literal runat="server" ID="NotYetPlacedLiteral"></asp:Literal>
                </td>
                <td id="RecommendedCell" runat="server">
                    <asp:Literal runat="server" ID="RecommendedLiteral"></asp:Literal>
                </td>
                <td id="RefusedOfferCell" runat="server">
                    <asp:Literal runat="server" ID="RefusedOfferLiteral"></asp:Literal>
                </td>
                <td id="RefusedReferralCell" runat="server">
                    <asp:Literal runat="server" ID="RefusedReferralLiteral"></asp:Literal>
                </td>
                <td id="UnderConsiderationCell" runat="server">
                    <asp:Literal runat="server" ID="UnderConsiderationLiteral"></asp:Literal>
                </td>
            </tr>
        </ItemTemplate>
        <EmptyDataTemplate><%= HtmlLocalise("JobOrdersReport.EmptyDataTemplate.Text", "No job orders found matching the criteria you chose.") %></EmptyDataTemplate>
    </asp:ListView>
</div>
<asp:ObjectDataSource runat="server" TypeName="Focus.Web.WebReporting.Controls.JobOrderReportList"
    ID="JobOrderListDataSource" EnablePaging="True" SelectMethod="GetJobOrderList"
    SelectCountMethod="GetJobOrderListCount" OnSelecting="JobOrderListDataSource_Selecting"
    SortParameterName="orderby" />


<uc:ColumnSelector Id="ColumnSelector" runat="server" OnReportColumnsChanged="ColumnSelector_ReportColumnsChanged" />