﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core;
using Focus.Core.Criteria.Report;

using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaResume : UserControlBase
	{
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();
      }
    }

    /// <summary>
    /// Resets this instance.
    /// </summary>
    /// <param name="reportType">The report type</param>
    private void Reset(ReportType reportType)
    {
      HasResumeCheckBox.Checked =
        HasNoResumeCheckBox.Checked = 
        HasPendingResumeCheckBox.Checked = 
        HasSearchableResumeCheckBox.Checked =
        HasMultipleResumesCheckBox.Checked = 
        WillingToWorkOvertimeCheckBox.Checked = 
        WillingToRelocateCheckBox.Checked = 
        UsedOnlineResumeHelpCheckBox.Checked = false;

      HasNoResumeValidator.ValidationGroup = reportType.ToString();
    }

    /// <summary>
    /// Displays the currently selected criteria
    /// </summary>
    /// <param name="criteria">The currently selected criteria</param>
    /// <param name="reportType">The report type</param>
    public void BindCriteria(ResumeReportCriteria criteria, ReportType reportType)
    {
      Reset(reportType);

      if (criteria.IsNull()) return;

      HasResumeCheckBox.Checked = criteria.HasResume.GetValueOrDefault();
      HasNoResumeCheckBox.Checked = criteria.HasNoResume.GetValueOrDefault();
      HasPendingResumeCheckBox.Checked = criteria.HasPendingResume.GetValueOrDefault();
      HasSearchableResumeCheckBox.Checked = criteria.HasSearchableResume.GetValueOrDefault();
      HasMultipleResumesCheckBox.Checked = criteria.HasMultipleResumes.GetValueOrDefault();
      WillingToWorkOvertimeCheckBox.Checked = criteria.WillingToWorkOvertime.GetValueOrDefault();
      WillingToRelocateCheckBox.Checked = criteria.WillingToRelocate.GetValueOrDefault();
      UsedOnlineResumeHelpCheckBox.Checked = criteria.UsedOnlineResumeHelp.GetValueOrDefault(false);
    }


    /// <summary>
    /// Unbinds this instance.
    /// </summary>
    /// <returns></returns>
    public ResumeReportCriteria Unbind()
    {
      var criteria = new ResumeReportCriteria
                     {
                       HasResume = HasResumeCheckBox.Checked ? HasResumeCheckBox.Checked : (bool?) null,
                       HasNoResume = HasNoResumeCheckBox.Checked ? HasNoResumeCheckBox.Checked : (bool?)null,
                       HasPendingResume = HasPendingResumeCheckBox.Checked ? HasPendingResumeCheckBox.Checked : (bool?)null,
                       HasSearchableResume = HasSearchableResumeCheckBox.Checked ? HasSearchableResumeCheckBox.Checked : (bool?)null,
                       HasMultipleResumes = HasMultipleResumesCheckBox.Checked ? HasMultipleResumesCheckBox.Checked : (bool?)null,
                       WillingToWorkOvertime = WillingToWorkOvertimeCheckBox.Checked ? WillingToWorkOvertimeCheckBox.Checked : (bool?)null,
                       WillingToRelocate = WillingToRelocateCheckBox.Checked ? WillingToRelocateCheckBox.Checked : (bool?)null,
                       UsedOnlineResumeHelp = UsedOnlineResumeHelpCheckBox.Checked ? UsedOnlineResumeHelpCheckBox.Checked : (bool?)null,
                     };
      return criteria;
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      CriteriaResumeLabel.Text = CodeLocalise("CriteriaResumeLabel.Text", "Resume");
      ResumeAndJobSeekerHeader.Text = CodeLocalise("ResumeAndJobSeekerHeader.Text", "Resume");

      HasResumeCheckBox.Text = CodeLocalise("ResumeStatusCheckBoxList.HasResume.Text", "Has resume");
      HasNoResumeCheckBox.Text = CodeLocalise("ResumeStatusCheckBoxList.NoResume.Text", "Has no resume");
      HasPendingResumeCheckBox.Text = CodeLocalise("ResumeStatusCheckBoxList.Pending.Text", "Has pending resume");
      HasSearchableResumeCheckBox.Text = CodeLocalise("ResumeStatusCheckBoxList.Searchable.Text", "Has searchable resume");
      HasMultipleResumesCheckBox.Text = CodeLocalise("ResumeStatusCheckBoxList.Multiple.Text", "Has multiple resumes");
      WillingToWorkOvertimeCheckBox.Text = CodeLocalise("ResumeStatusCheckBoxList.Overtime.Text", "Willing to work overtime");
      WillingToRelocateCheckBox.Text = CodeLocalise("ResumeStatusCheckBoxList.Relocate.Text", "Willing to relocate");
      UsedOnlineResumeHelpCheckBox.Text = CodeLocalise("ResumeStatusCheckBoxList.UsedOnlineHelp.Text", "Used on-line resume help");

      HasNoResumeValidator.ErrorMessage = CodeLocalise("HasNoResumeValidator.Text", "\"Has No Resume\" should not be selected when other resume options have been selected");
    }
  }
}