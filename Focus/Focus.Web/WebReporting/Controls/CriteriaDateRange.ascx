﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaDateRange.ascx.cs"
  Inherits="Focus.Web.WebReporting.Controls.CriteriaDateRange" %>
<table role="presentation" style="width:100%;" class="accordionWithIcon">
  <tr class="multipleAccordionTitle">
    <td style="vertical-align:top; width:10%;">
      <span class="accordionIcon" style="height: 25px"></span>
    </td>
    <td style="width:90%;">
      <asp:Literal runat="server" ID="CriteriaDateRangeLabel" />
    </td>
  </tr>
  <tr class="accordionContent">
    <td colspan="2">
      <table role="presentation" style="width:100%;">
        <tr>
          <td colspan="2">
            <asp:RadioButton runat="server" GroupName="DateOption" ID="WithinRadioButton" />
            <strong>
              <%=HtmlLabel(WithinRadioButton,"WithinHeader.Text", "within")%></strong>
          </td>
        </tr>
        <tr>
          <td>
	          <focus:LocalisedLabel runat="server" ID="WithinDaysDropdownLabel" AssociatedControlID="WithinDaysDropdown" LocalisationKey="WithInDays" DefaultText="Within days" CssClass="sr-only"/>
            <asp:DropDownList runat="server" ID="WithinDaysDropdown"/>
          </td>
        </tr>
      </table>
      <table role="presentation" style="width:100%;">
        <tr>
          <td colspan="2">
            <asp:RadioButton runat="server" GroupName="DateOption" ID="FromRadioButton" />
            <strong><%= HtmlLabel(FromRadioButton,"ShowResultsFromHeader.Text", "Show results from")%></strong>
          </td>
        </tr>
        <tr>
          <td>
	          <focus:LocalisedLabel runat="server" ID="ResultsFromMonthDropdownLabel" AssociatedControlID="ResultsFromMonthDropdown" LocalisationKey="MonthFrom" DefaultText="Month from" CssClass="sr-only"/>
            <asp:DropDownList runat="server" ID="ResultsFromMonthDropdown" />
						<focus:LocalisedLabel runat="server" ID="ResultsFromYearDropdownLabel" AssociatedControlID="ResultsFromYearDropdown" LocalisationKey="YearFrom" DefaultText="Year from" CssClass="sr-only"/>
            &nbsp;<asp:DropDownList runat="server" ID="ResultsFromYearDropdown"/>
          </td>
        </tr>
      </table>
      <table role="presentation" style="width:100%;">
        <tr>
          <td colspan="2">
            <asp:RadioButton runat="server" GroupName="DateOption" ID="FromToRadioButton" />
            <strong>
              <%=HtmlLabel(FromToRadioButton,"ShowResultsFromRangeHeader.Text", "Show results from")%></strong>
          </td>
        </tr>
        <tr>
          <td>
	          <focus:LocalisedLabel runat="server" ID="ShowResultsFromMonthDropdownLabel" AssociatedControlID="ShowResultsFromMonthDropdown" LocalisationKey="MonthFrom" DefaultText="Month from" CssClass="sr-only"/>
            <asp:DropDownList runat="server" ID="ShowResultsFromMonthDropdown" />
            &nbsp;<focus:LocalisedLabel runat="server" ID="ShowResultsFromYearDropdownLabel" AssociatedControlID="ShowResultsFromYearDropdown" LocalisationKey="YearFrom" DefaultText="Year from" CssClass="sr-only"/>
						<asp:DropDownList runat="server" ID="ShowResultsFromYearDropdown" />
            &nbsp;<%= HtmlLocalise("Global.To", "to") %>&nbsp;
						<focus:LocalisedLabel runat="server" ID="ShowResultsToMonthDropdownLabel" AssociatedControlID="ShowResultsToMonthDropdown" LocalisationKey="MonthTo" DefaultText="Month to" CssClass="sr-only"/>
            <asp:DropDownList runat="server" ID="ShowResultsToMonthDropdown" />
            &nbsp;<focus:LocalisedLabel runat="server" ID="ShowResultsToYearDropdownLabel" AssociatedControlID="ShowResultsToYearDropdown" LocalisationKey="YearTo" DefaultText="Year to" CssClass="sr-only"/>
						<asp:DropDownList runat="server" ID="ShowResultsToYearDropdown" />
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<asp:CustomValidator runat="server" ID="ResultsFromValidator" Display="None" ControlToValidate="ResultsFromMonthDropdown" ClientValidationFunction="ValidateResultsFrom" ValidateEmptyText="True" />
<asp:CustomValidator runat="server" ID="ResultsFromToValidator" Display="None" ControlToValidate="ShowResultsFromMonthDropdown"   ClientValidationFunction="ValidateResultsFromTo" ValidateEmptyText="True" />
<script type="text/javascript">
  function ValidateResultsFrom(src, args) {
    args.IsValid = true;
    if ($('#<%=FromRadioButton.ClientID %>').is(":checked")) {
      if ($("#<%=ResultsFromMonthDropdown.ClientID %>").prop("selectedIndex") == 0) {
        args.IsValid = false;
      }
      if ($("#<%=ResultsFromYearDropdown.ClientID %>").prop("selectedIndex") == 0) {
        args.IsValid = false;
      }
    }
  }
  function ValidateResultsFromTo(src, args) {
    args.IsValid = true;
    if ($('#<%=FromToRadioButton.ClientID %>').is(":checked")) {
      if ($("#<%=ShowResultsFromMonthDropdown.ClientID %>").prop("selectedIndex") == 0) {
        args.IsValid = false;	
      }
      if ($("#<%=ShowResultsFromYearDropdown.ClientID %>").prop("selectedIndex") == 0) {
        args.IsValid = false;
      }
      if ($("#<%=ShowResultsToMonthDropdown.ClientID %>").prop("selectedIndex") == 0) {
        args.IsValid = false;
      }
      if ($("#<%=ShowResultsToYearDropdown.ClientID %>").prop("selectedIndex") == 0) {
        args.IsValid = false;
      }
    }
  }

  $(document).ready(function () {
    $("#<%=WithinDaysDropdown.ClientID %>").change(function () {
      $('#<%=WithinRadioButton.ClientID %>').prop('checked', true);
    });
    $("#<%=ResultsFromMonthDropdown.ClientID %>, #<%=ResultsFromYearDropdown.ClientID %>").change(function () {
      $('#<%=FromRadioButton.ClientID %>').prop('checked', true);
    });
    $("#<%=ShowResultsFromMonthDropdown.ClientID %>, #<%=ShowResultsFromYearDropdown.ClientID %>, #<%=ShowResultsToMonthDropdown.ClientID%>, #<%=ShowResultsToYearDropdown.ClientID %>").change(function () {
      $('#<%=FromToRadioButton.ClientID %>').prop('checked', true);
    });

  });

</script>
