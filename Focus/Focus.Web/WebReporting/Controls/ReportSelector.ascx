﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportSelector.ascx.cs" Inherits="Focus.Web.WebReporting.Controls.ReportSelector" %>
<h2><asp:Literal runat="server" ID="SelectorHeading" /></h2>
<asp:Literal runat="server" ID="WhatDoYouWantLiteral"></asp:Literal>
<ul class="reporttypeList">
    <li><asp:Button runat="server" CssClass="button3" ID="JobSeekerReportButton" OnCommand="ReportButton_Command" Width="150" CommandArgument="1"/></li>
    <li><asp:Button runat="server" CssClass="button3" ID="JobOrderReportButton" OnCommand="ReportButton_Command" Width="150" CommandArgument="2"/></li>
    <li style="display: none;"><asp:Button runat="server" CssClass="button3" ID="HiringManagerReportButton" OnCommand="ReportButton_Command" Width="150" CommandArgument="4" Visible="False"/></li>
    <li><asp:Button runat="server" CssClass="button3" ID="EmployerReportButton" OnCommand="ReportButton_Command" Width="150" CommandArgument="3"/></li>
    <li><asp:Button runat="server" CssClass="button3" ID="SupplyDemandReportButton" OnCommand="ReportButton_Command" Width="150" CommandArgument="6"/></li>
    <li style="display: none;"><asp:Button runat="server" CssClass="button3" ID="StaffReportButton" OnCommand="ReportButton_Command" Width="150" CommandArgument="5" Visible="False"/></li>
</ul>