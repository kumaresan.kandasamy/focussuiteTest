﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Data;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Report;
using Focus.Core.Models;
using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class TotalsDisplay : UserControlBase
	{
    private ReportDataTableModel ReportData { get; set; }

    #region Page properties

    public IReportCriteria ReportCriteria
    {
      get { return GetViewStateValue<IReportCriteria>("BarChartDisplay:ReportCriteria"); }
      set { SetViewStateValue("BarChartDisplay:ReportCriteria", value); }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Binds the specified criteria.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    public void Bind(IReportCriteria criteria)
    {
      Visible = true;
      ReportCriteria = criteria;

      switch (criteria.ReportType)
      {
        case ReportType.JobSeeker:
					if (criteria.ReportTitle.IsNullOrEmpty()) criteria.ReportTitle = CodeLocalise("JobSeekerReport.Name", "Job Seeker Report");
					ReportData = ServiceClientLocator.ReportClient(App).GetJobSeekersDataTable((JobSeekersReportCriteria)criteria);
          break;
        case ReportType.JobOrder:
					if (criteria.ReportTitle.IsNullOrEmpty()) criteria.ReportTitle = CodeLocalise("JobOrderReport.Name", "#EMPLOYMENTTYPE# report");
					ReportData = ServiceClientLocator.ReportClient(App).GetJobOrdersDataTable((JobOrdersReportCriteria)criteria);
          break;
        case ReportType.Employer:
					if (criteria.ReportTitle.IsNullOrEmpty()) criteria.ReportTitle = CodeLocalise("EmployerReport.Name", "#BUSINESS# report");
          ReportData = ServiceClientLocator.ReportClient(App).GetEmployersDataTable((EmployersReportCriteria)criteria);
          break;
      }

      DrawReport();
    }

    /// <summary>
    /// Draws the report.
    /// </summary>
    private void DrawReport()
    {
      HeaderCellsRepeater.DataSource = ReportData.Data.Columns;
      HeaderCellsRepeater.DataBind();

      BodyCellsRepeater.DataSource = ReportData.Data.Rows[0].ItemArray;
      BodyCellsRepeater.DataBind();
    }

    /// <summary>
    /// Handles the OnItemDataBound event of the HeaderCellsRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="RepeaterItemEventArgs"/> instance containing the event data.</param>
    /// <exception cref="System.NotImplementedException"></exception>
    protected void HeaderCellsRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      var column = (DataColumn)e.Item.DataItem;
      var headerLiteral = (Literal)e.Item.FindControl("HeaderCell");

      headerLiteral.Text = column.ColumnName;
    }

    /// <summary>
    /// Handles the OnItemDataBound event of the BodyCellsRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="RepeaterItemEventArgs"/> instance containing the event data.</param>
    /// <exception cref="System.NotImplementedException"></exception>
    protected void BodyCellsRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      var cell = e.Item.DataItem;
      var totalLiteral = (Literal)e.Item.FindControl("TotalCell");

      totalLiteral.Text = cell.ToString();
    }
  }
}