﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Core.Criteria.Report;
using Focus.Web.Code;
using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class ReportCriteriaDisplay : UserControlBase
	{
    public bool ShowButtons { get; set; }
    public string ButtonText { get; set; }

    private List<List<string>> ColumnLists { get; set; }

    public ReportCriteriaDisplay()
    {
      ButtonText = CodeLocalise("ButtonText.Default", "report");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
        Localise();
	      ApplyBranding();
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      NoCriteriaLabel.Text = HtmlLocalise("NoCriteriaLabel.Text", "No criteria selected");
      EditReportSettingsLinkButton.Text = CodeLocalise("EditReportSettingsLinkButton.Text", "edit {0} settings", ButtonText);
      NewReportButton.Text = CodeLocalise("NewReportButton.Text", "Create new {0}", ButtonText);
    }

	  private void ApplyBranding()
	  {
      CriteriaDisplayHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      CriteriaDisplayPanelExtender.CollapsedImage = UrlBuilder.OpenAccordionImage();
      CriteriaDisplayPanelExtender.ExpandedImage = UrlBuilder.CloseAccordionImage();
	  }

    /// <summary>
    /// Shows the job seeker criteria.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    public void ShowCriteria(IReportCriteria criteria)
    {
      ColumnLists = criteria.BuildCriteriaForDisplay(CodeLocalise, CodeLocalise, CodeLocalise, true);
      CriteriaButtonsPanel.Visible = ShowButtons;

      BindLists();
    }

    /// <summary>
    /// Set whether the panel is collapsed or expanded
    /// </summary>
    /// <param name="collapsed">Whether the panel is collapsed or expanded</param>
    public void SetCriteriaPanelCollapsedState(bool collapsed)
    {
      CriteriaDisplayPanelExtender.Collapsed = collapsed;
    }

    #region Shared methods

    /// <summary>
    /// Binds the lists.
    /// </summary>
    private void BindLists()
    {
      NoCriteriaLabel.Visible = false;

      if (ColumnLists.IsNotNullOrEmpty() && ColumnLists.Any(list => list.IsNotNullOrEmpty()))
      {
        MainColumnsRepeater.Visible = true;
        MainColumnsRepeater.DataSource = ColumnLists;
        MainColumnsRepeater.DataBind();
      }
      else
      {
        MainColumnsRepeater.Visible = false;
        NoCriteriaLabel.Visible = true;
      }
    }

    /// <summary>
    /// Fires for each occurence of a column to bind the list
    /// </summary>
    /// <param name="sender">Repeater raising the event</param>
    /// <param name="e">Repeater event arguments</param>
    protected void MainColumnsRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      var childRepeater = (Repeater)e.Item.FindControl("ColumnRepeater");
      var columnList = ColumnLists[e.Item.ItemIndex];

      if (columnList.IsNotNullOrEmpty())
      {
        childRepeater.Visible = true;
        childRepeater.DataSource = columnList;
        childRepeater.DataBind();
      }
      else
      {
        childRepeater.Visible = false;
      }
    }

    /// <summary>
    /// Fires when the EditReportSettings button is clicked
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void EditReportSettingsLinkButton_Clicked(object sender, EventArgs e)
    {
      OnActionRequested(new CommandEventArgs("EditCriteria", null));
    }

    /// <summary>
    /// Fires when the NewReportButton button is clicked
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void NewReportButton_Clicked(object sender, EventArgs e)
    {
      OnActionRequested(new CommandEventArgs("NewReport", null));
    }

    #endregion

    #region page generated events

    public delegate void ReportHeaderActionHandler(object sender, CommandEventArgs eventArgs);
    public event ReportHeaderActionHandler ActionRequested;

    /// <summary>
    /// Raises the <see cref="CommandEventArgs" /> event.
    /// </summary>
    /// <param name="eventargs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    public void OnActionRequested(CommandEventArgs eventargs)
    {
      var handler = ActionRequested;
      if (handler != null) handler(this, eventargs);
    }

    #endregion
  }
}