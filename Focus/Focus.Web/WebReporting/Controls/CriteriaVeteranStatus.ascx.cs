﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.Report;
using Focus.Core.Models.Career;

using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaVeteranStatus : UserControlBase
	{
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();
        Bind();
      }
    }

    /// <summary>
    /// Resets this instance.
    /// </summary>
    private void Reset()
    {
      VeteranTypeCheckBoxList.Items.Reset();
      TransitionTypeCheckBoxList.Items.Reset();
      MilitaryDischargeCheckBoxList.Items.Reset();
      BranchOfServiceCheckBoxList.Items.Reset();
	    HomelessVeteran.Checked = false;
	    IsCampaignVeteranCheckBox.Checked = false;
    }

    public void BindCriteria(VeteranReportCriteria criteria)
    {
      Reset();

      if (criteria.IsNull()) return;

      TransitionTypeCheckBoxList.BindEnumListToControlList(criteria.TransitionTypes);
      MilitaryDischargeCheckBoxList.BindEnumListToControlList(criteria.MilitaryDischarges);
      BranchOfServiceCheckBoxList.BindEnumListToControlList(criteria.BranchOfServices);

      IsCampaignVeteranCheckBox.Checked = criteria.CampaignVeteran.GetValueOrDefault(false);
      VeteranTypeCheckBoxList.BindEnumListToControlList(criteria.Types);
			HomelessVeteran.Checked = criteria.HomelessVeteran.GetValueOrDefault( false );
    }

    /// <summary>
    /// Unbinds this instance.
    /// </summary>
    /// <returns></returns>
    public VeteranReportCriteria Unbind()
    {
      var criteria = new VeteranReportCriteria
				{
					CampaignVeteran = IsCampaignVeteranCheckBox.Checked ? true : (bool?)null,
					HomelessVeteran = HomelessVeteran.Checked ? true : (bool?)null,
					Types = VeteranTypeCheckBoxList.UnbindNullableEnumControlList<VeteranEra>(),
					TransitionTypes = TransitionTypeCheckBoxList.UnbindNullableEnumControlList<VeteranTransitionType>(),
					MilitaryDischarges = MilitaryDischargeCheckBoxList.UnbindNullableEnumControlList<ReportMilitaryDischargeType>(),
					BranchOfServices = BranchOfServiceCheckBoxList.UnbindNullableEnumControlList<ReportMilitaryBranchOfService>()
				};
      return criteria;
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      CriteriaVeteranStatusLabel.Text = CodeLocalise("CriteriaVeteranStatusLabel.Text", "Veteran status");
      VeteranTypeHeader.Text = CodeLocalise("VeteranTypeHeader.Text", "Veteran type");
      TransitionTypeHeader.Text = CodeLocalise("TransitionTypeHeader.Text", "Transition type");
      MilitaryDischargeHeader.Text = CodeLocalise("MilitaryDischargeHeader.Text", "Military discharge");
      BranchOfServiceHeader.Text = CodeLocalise("BranchOfServiceHeader.Text", "Branch of service");
			IsCampaignVeteranCheckBox.Text = CodeLocalise( "IsCampaignVeteranCheckBox.Text", "Campaign veteran" );
			HomelessVeteranHeader.Text = CodeLocalise( "HomelessVeteranHeader.Text", "Other" );
			HomelessVeteran.Text = CodeLocalise( "HomelessVeteran.Text", "Homeless" );
    }

    /// <summary>
    /// Binds this instance.
    /// </summary>
    private void Bind()
    {
      VeteranTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise("VeteranTypeCheckBoxList.OtherEligible.Text", "Other eligible"), VeteranEra.OtherEligible.ToString()));
      VeteranTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise("VeteranTypeCheckBoxList.TransitioningServiceMember.Text", "Transitioning service member"), VeteranEra.TransitioningServiceMember.ToString()));
      VeteranTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise("VeteranTypeCheckBoxList.Veteran.Text", "Veteran"), VeteranEra.OtherVet.ToString()));

      TransitionTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise("TransitionTypeCheckBoxList.Discharge.Text", "Discharge"), VeteranTransitionType.Discharge.ToString()));
      TransitionTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise("TransitionTypeCheckBoxList.Retirement.Text", "Retirement"), VeteranTransitionType.Retirement.ToString()));
      TransitionTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise("TransitionTypeCheckBoxList.Spouse.Text", "Spouse"), VeteranTransitionType.Spouse.ToString()));

      MilitaryDischargeCheckBoxList.Items.Add(new ListItem(CodeLocalise("MilitaryDischargeCheckBoxList.BadConduct.Text", "Bad conduct"), ReportMilitaryDischargeType.BadConduct.ToString()));
      MilitaryDischargeCheckBoxList.Items.Add(new ListItem(CodeLocalise("MilitaryDischargeCheckBoxList.Dishonorable.Text", "Dishonorable"), ReportMilitaryDischargeType.Dishonorable.ToString()));
      MilitaryDischargeCheckBoxList.Items.Add(new ListItem(CodeLocalise("MilitaryDischargeCheckBoxList.General.Text", "General"), ReportMilitaryDischargeType.General.ToString()));
      MilitaryDischargeCheckBoxList.Items.Add(new ListItem(CodeLocalise("MilitaryDischargeCheckBoxList.Honorable.Text", "Honorable"), ReportMilitaryDischargeType.Honorable.ToString()));
      MilitaryDischargeCheckBoxList.Items.Add(new ListItem(CodeLocalise("MilitaryDischargeCheckBoxList.Medical.Text", "Medical"), ReportMilitaryDischargeType.Medical.ToString()));
      MilitaryDischargeCheckBoxList.Items.Add(new ListItem(CodeLocalise("MilitaryDischargeCheckBoxList.OtherThanHonorable.Text", "Other than honorable"), ReportMilitaryDischargeType.OtherThanHonorable.ToString()));

      BranchOfServiceCheckBoxList.Items.Add(new ListItem(CodeLocalise("BranchOfServiceCheckBoxList.USAirForce.Text", "US Air Force"), ReportMilitaryBranchOfService.AirForce.ToString()));
			BranchOfServiceCheckBoxList.Items.Add(new ListItem(CodeLocalise("BranchOfServiceCheckBoxList.USAirForceReserves.Text", "US Air Force Reserves"), ReportMilitaryBranchOfService.AirForceReserves.ToString()));
			BranchOfServiceCheckBoxList.Items.Add(new ListItem(CodeLocalise("BranchOfServiceCheckBoxList.USAirNationalGuard.Text", "US Air National Guard"), ReportMilitaryBranchOfService.AirNationalGuard.ToString()));
      BranchOfServiceCheckBoxList.Items.Add(new ListItem(CodeLocalise("BranchOfServiceCheckBoxList.USArmy.Text", "US Army"), ReportMilitaryBranchOfService.Army.ToString()));
			BranchOfServiceCheckBoxList.Items.Add(new ListItem(CodeLocalise("BranchOfServiceCheckBoxList.USArmyReserves.Text", "US Army Reserves"), ReportMilitaryBranchOfService.ArmyReserves.ToString()));
			BranchOfServiceCheckBoxList.Items.Add(new ListItem(CodeLocalise("BranchOfServiceCheckBoxList.USArmyNationalGuard.Text", "US Army National Guard"), ReportMilitaryBranchOfService.ArmyNationalGuard.ToString()));
      BranchOfServiceCheckBoxList.Items.Add(new ListItem(CodeLocalise("BranchOfServiceCheckBoxList.USCoastGuard.Text", "US Coast Guard"), ReportMilitaryBranchOfService.CoastGuard.ToString()));
			BranchOfServiceCheckBoxList.Items.Add(new ListItem(CodeLocalise("BranchOfServiceCheckBoxList.USCoastGuardReserves.Text", "US Coast Guard Reserves"), ReportMilitaryBranchOfService.CoastGuardReserves.ToString()));
      BranchOfServiceCheckBoxList.Items.Add(new ListItem(CodeLocalise("BranchOfServiceCheckBoxList.USMarineCorps.Text", "US Marine Corps"), ReportMilitaryBranchOfService.MarineCorps.ToString()));
			BranchOfServiceCheckBoxList.Items.Add(new ListItem(CodeLocalise("BranchOfServiceCheckBoxList.USMarineCorpsReserves.Text", "US Marine Corps Reserves"), ReportMilitaryBranchOfService.MarineCorpsReserves.ToString()));
      BranchOfServiceCheckBoxList.Items.Add(new ListItem(CodeLocalise("BranchOfServiceCheckBoxList.USNavy.Text", "US Navy"), ReportMilitaryBranchOfService.Navy.ToString()));
			BranchOfServiceCheckBoxList.Items.Add(new ListItem(CodeLocalise("BranchOfServiceCheckBoxList.USNavyReserves.Text", "US Navy Reserves"), ReportMilitaryBranchOfService.NavyReserves.ToString()));
    }
  }
}