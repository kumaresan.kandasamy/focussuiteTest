﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportHeader.ascx.cs"
    Inherits="Focus.Web.WebReporting.Controls.ReportHeader" %>
<div class="grid100">
    <div class="reportgrid50" style="width: 50%">
        <asp:LinkButton runat="server" ID="EditReportSettingsLinkButton" OnClick="EditReportSettingsLinkButton_Clicked"></asp:LinkButton>&nbsp;
    </div>
    <div class="reportgrid50" style="text-align: right">
        <asp:Button runat="server" CssClass="button3" ID="NewReportButton" OnClick="NewReportButton_Clicked" />
    </div>
</div>
