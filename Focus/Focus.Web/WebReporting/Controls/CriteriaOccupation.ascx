﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaOccupation.ascx.cs"
    Inherits="Focus.Web.WebReporting.Controls.CriteriaOccupation" %>
<table role="presentation" style="width:100%;" class="accordionWithIcon">
    <tr class="multipleAccordionTitle">
        <td style="vertical-align:top; width:10%;">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td style="width:90%;">
           <asp:Literal runat="server" ID="CriteriaOccupationLabel" />
        </td>
    </tr>
    
    
    <tr class="accordionContent">
        <td colspan="2">
           <table role="presentation" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                          <focus:LocalisedLabel runat="server" ID="OccupationsFamily" LocalisationKey="OccupationsFamily.Text"  DefaultText="Occupations and groups" /></strong>
                    </td>
                </tr>
                <tr>
                    <td>
	                    <div style="position: relative;">
                               <asp:DropDownList ID="OccupationsFamilyDropDownList" runat="server" Width="100%" ClientIDMode="Static" /><br /> 
							          <focus:AjaxDropDownList ID="OccupationsDropDownList" runat="server"  Width="100%" ClientIDMode="Static" />
							        <act:CascadingDropDown ID="OccupationsDropDownListCascadingDropDown" runat="server" TargetControlID="OccupationsDropDownList" ParentControlID="OccupationsFamilyDropDownList" 
																		         ServicePath="~/Services/AjaxService.svc" Category="Occupations" BehaviorID="OccupationsDropDownListCascadingDropDown"/> 
											</div>
                    </td>
                   </tr>
               </table>
           
        </td>
    </tr>
</table>
<asp:RequiredFieldValidator ID="OccupationFamilyRequired" runat="server" ControlToValidate="OccupationsFamilyDropDownList" SetFocusOnError="true" Display="None" CssClass="error" />

<script language="javascript">
	Sys.Application.add_load(CriteriaOccupation_PageLoad);

	function CriteriaOccupation_PageLoad(sender, args) {
		var behavior = $find(CriteriaOccupation_Variables["OccupationsBehaviorID"]);
		if (behavior != null) {
			behavior.add_populated(function () {
				var first = $("#OccupationsDropDownList option:first");
				if (first.text() != CriteriaOccupation_Variables["OccupationsLoadingText"]) {
					if ($("#OccupationsFamilyDropDownList").prop("selectedIndex") > 0) {
						first.text(CriteriaOccupation_Variables["OccupationsAllText"]);
					} else {
						first.text(CriteriaOccupation_Variables["OccupationsSelectText"]);
					}
				}
			});
		}
	}
</script>
