﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaJobCharacteristics.ascx.cs"
    Inherits="Focus.Web.WebReporting.Controls.CriteriaJobCharacteristics" %>
<table role="presentation" style="width:100%;" class="accordionWithIcon">
    <tr class="multipleAccordionTitle">
        <td style="vertical-align:top; width:10%;">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td style="width:90%;">
            <asp:Literal runat="server" ID="CriteriaJobCharacteristicsLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
            <table role="presentation" style="width:100%;">
                <tr>
                    <td>
                        <strong>
                            <asp:Literal runat="server" ID="SalaryHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=HtmlLocalise("Global.DefaultCurrencySymbol", "$") %><focus:LocalisedLabel runat="server" ID="LowSalaryTextBoxLabel" AssociatedControlID="LowSalaryTextBox" LocalisationKey="MinimumSalary" DefaultText="Minimum salary" CssClass="sr-only"/><asp:TextBox runat="server"
                            ID="LowSalaryTextBox" Width="70" />&nbsp;<%=HtmlLocalise("Global.To", "to") %>&nbsp;<%=HtmlLocalise("Global.DefaultCurrencySymbol", "$") %>&nbsp;<focus:LocalisedLabel runat="server" ID="HighSalaryTextBoxLabel" AssociatedControlID="HighSalaryTextBox" LocalisationKey="MaximumSalary" DefaultText="Maximum salary" CssClass="sr-only"/><asp:TextBox
                                runat="server" ID="HighSalaryTextBox" Width="70" />&nbsp;<focus:LocalisedLabel runat="server" ID="SalaryRangeDropDownLabel" AssociatedControlID="SalaryRangeDropDown" LocalisationKey="SalaryRange" DefaultText="Salry range" CssClass="sr-only"/><asp:DropDownList runat="server"
                                    ID="SalaryRangeDropDown" />
                    </td>
                </tr>
            </table>
            <table role="presentation" style="width:100%;">
                <tr>
                    <td>
                        <strong>
                            <asp:Literal runat="server" ID="JobStatusHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBoxList ID="JobStatusCheckBoxList" TextAlign="Right" runat="server" role="presentation"/>
                    </td>
                </tr>
            </table>
            <table role="presentation" style="width:100%;">
                <tr>
                    <td>
                        <strong>
                            <asp:Literal runat="server" ID="LevelOfEducationHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBoxList ID="LevelOfEducationCheckBoxList" TextAlign="Right" runat="server" role="presentation"/>
                    </td>
                </tr>
            </table>
            <table role="presentation" style="width:100%;">
                <tr>
                    <td>
                        <asp:Literal runat="server" ID="SearchTermsHeader"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:RadioButtonList runat="server" ID="SearchTypeRadioButtonList" RepeatDirection="Vertical"
                            role="presentation"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox runat="server" TextMode="MultiLine" ID="SearchTermsTextBox" Width="95%" Tooltip="Search terms"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <asp:Literal runat="server" ID="SearchResumeContextHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBoxList ID="SearchResumeContextCheckBoxList" TextAlign="Right" runat="server" role="presentation"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:CompareValidator runat="server" ID="SalaryFromNumericValidator" ControlToValidate="LowSalaryTextBox" Type="Currency" Display="None" Operator="DataTypeCheck" />
<asp:CompareValidator runat="server" ID="SalaryToNumericValidator" ControlToValidate="HighSalaryTextBox" Type="Currency" Display="None" Operator="DataTypeCheck" />
<asp:CompareValidator runat="server" ID="SalaryFromValueValidator" ControlToValidate="LowSalaryTextBox" ValueToCompare="0" Operator="GreaterThan" Type="Currency" Display="None" />
<asp:CompareValidator runat="server" ID="SalaryToValueValidator" ControlToValidate="HighSalaryTextBox" ValueToCompare="0" Operator="GreaterThan" Type="Currency" Display="None" />
<asp:CompareValidator runat="server" ID="SalaryToFromComparisonValidator" ControlToValidate="HighSalaryTextBox" ControlToCompare="LowSalaryTextBox" Operator="GreaterThanEqual" Type="Currency" Display="None" />
