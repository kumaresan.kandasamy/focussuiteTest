﻿using System;
using System.Linq;
using Focus.Core;
using Focus.Core.Criteria.Report;
using Framework.Core;

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaIndustries : UserControlBase
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
        Localise();
    }

    /// <summary>
    /// Resets this instance.
    /// </summary>
    private void Reset()
    {
      HiddenTargetIndustryList.Value =
        HiddenExperienceIndustryList.Value = string.Empty;
      ExperienceIndustrialClassificationValidator.ValidationGroup =
        TargetIndustrialClassificationValidator.ValidationGroup = string.Empty;
    }

    public void BindCriteria(IndustriesReportCriteria indCriteria, ReportType reportType)
    {
      Reset();

      if (indCriteria.IsNotNull())
      {
        const string delimiter = "|*|";
        if (indCriteria.MostExperienceIndustries.IsNotNullOrEmpty())
          HiddenExperienceIndustryList.Value = string.Join(delimiter, indCriteria.MostExperienceIndustries);
        if (indCriteria.TargetIndustries.IsNotNullOrEmpty())
          HiddenTargetIndustryList.Value = string.Join(delimiter, indCriteria.TargetIndustries);
      }
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      #region labels

      CriteriaIndustryLabel.Text = CodeLocalise("CriteriaIndustryLabel.Text", "Industry");
      ExperienceIndustryAddButton.Value = CodeLocalise("Global.Add.NoEdit", "Add");
      TargetIndustryAddButton.Value = CodeLocalise("Global.Add.NoEdit", "Add");

      #endregion
    }

    public SupplyDemandIndustriesReportCriteria UnbindIndustries()
    {
      var delimiter = new[] { "|*|" };

      var industries = HiddenExperienceIndustryList.Value.Split(delimiter, StringSplitOptions.RemoveEmptyEntries).ToList();
      var industrycodes = industries.Select(industry => industry.Split('-').FirstOrDefault()).ToList();

      var targetindustries = HiddenTargetIndustryList.Value.Split(delimiter, StringSplitOptions.RemoveEmptyEntries).ToList();
      var targetindustrycodes = targetindustries.Select(industry => industry.Split('-').FirstOrDefault()).ToList();

      var criteria = new SupplyDemandIndustriesReportCriteria
      {
        MostExperienceIndustriesCodes = industrycodes,
        MostExperienceIndustries = industries,
        TargetIndustries = targetindustries,
        TargetIndustriesCodes = targetindustrycodes
      };

      return criteria;
    }
  }
}