﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Focus.Web.WebReporting.Controls {
    
    
    public partial class CriteriaIndividualJobSeekerCharacteristics {
        
        /// <summary>
        /// CriteriaIndividualJobSeekerCharacteristicsLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal CriteriaIndividualJobSeekerCharacteristicsLabel;
        
        /// <summary>
        /// IndividualCharacteristicsFirstNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel IndividualCharacteristicsFirstNameLabel;
        
        /// <summary>
        /// IndividualCharacteristicsFirstNameTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox IndividualCharacteristicsFirstNameTextBox;
        
        /// <summary>
        /// IndividualCharacteristicsLastNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel IndividualCharacteristicsLastNameLabel;
        
        /// <summary>
        /// IndividualCharacteristicsLastNameTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox IndividualCharacteristicsLastNameTextBox;
        
        /// <summary>
        /// IndividualCharacteristicsEmailLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel IndividualCharacteristicsEmailLabel;
        
        /// <summary>
        /// IndividualCharacteristicsEmailTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox IndividualCharacteristicsEmailTextBox;
        
        /// <summary>
        /// IndividualCharacteristicsSsnLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel IndividualCharacteristicsSsnLabel;
        
        /// <summary>
        /// IndividualCharacteristicsSsnClientList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.UpdateableClientList IndividualCharacteristicsSsnClientList;
    }
}
