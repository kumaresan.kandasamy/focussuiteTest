﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaIndustries.ascx.cs"
    Inherits="Focus.Web.WebReporting.Controls.CriteriaIndustries" %>
   <table role="presentation" style="width:100%;" class="accordionWithIcon">
    <tr class="multipleAccordionTitle">
        <td style="vertical-align:top; width:10%;">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td style="width:90%;">
           <asp:Literal runat="server" ID="CriteriaIndustryLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
           <table role="presentation" id="ExperienceIndustriesCriteria" runat="server" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                            <focus:LocalisedLabel runat="server" ID="ExperienceIndustryLabel" LocalisationKey="ExperienceIndustry.Label"  DefaultText="Most Experienced Industries" /></strong>
                    </td>
                </tr>
                <tr>
                    <td>
												<div style="position: relative;">
													<focus:LocalisedLabel runat="server" ID="ExperienceIndustryTextBoxLabel" AssociatedControlID="ExperienceIndustryTextBox" LocalisationKey="ExperienceInIndustry" DefaultText="Experience in industry text box" CssClass="sr-only"/>
													<asp:TextBox runat="server" ID="ExperienceIndustryTextBox" ></asp:TextBox>
													<div style="position:absolute; width:500px;">
														<act:AutoCompleteExtender ID="ExperienceIndustryAutoCompleteExtender" runat="server" TargetControlID="ExperienceIndustryTextBox" MinimumPrefixLength="2" CompletionListElementID="ExperienceIndustryAutoCompleteExtenderCompletionList" CompletionListCssClass="autocompleteCompletionList" CompletionInterval="100" CompletionSetCount="15" ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetNaics" />
														<div id="ExperienceIndustryAutoCompleteExtenderCompletionList" class="autocompleteCompletionList"></div>
													</div>
												</div>
                    </td>
                    <td>
                        <input type="button" class="button3" runat="server" id="ExperienceIndustryAddButton" name="ExperienceIndustryAddButton" />
                    </td>
                </tr>
                <tr><td colspan="2"> <asp:CustomValidator runat="server" ID="ExperienceIndustrialClassificationValidator" ControlToValidate="ExperienceIndustryTextBox" ClientValidationFunction="doesNAICSExist" ErrorMessage="Industry classification (NAICS) not found"  SetFocusOnError="true" Display="Dynamic" CssClass="error" /></td></tr>
                 <tr>

                        <td colspan="2"><table role="presentation" id="ExperienceIndustryTable" style="width:75%;"><tbody></tbody></table><asp:HiddenField runat="server" ID="HiddenExperienceIndustryList"/></td>
                    </tr>
            </table>
             <table role="presentation" id="TargetIndustriesCriteria" runat="server" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                            <focus:LocalisedLabel runat="server" ID="TargetIndustryLabel" LocalisationKey="TargetIndustry.Label"  DefaultText="Target Industries" /></strong>
                    </td>
                </tr>
                <tr>
                    <td>
												<div style="position: relative;">
													<focus:LocalisedLabel runat="server" ID="TargetIndustryTextBoxLabel" AssociatedControlID="TargetIndustryTextBox" LocalisationKey="Target Industry" DefaultText="Target industry" CssClass="sr-only"/>
													<asp:TextBox runat="server" ID="TargetIndustryTextBox"></asp:TextBox>
													<div style="position:absolute; width:500px;">
														<act:AutoCompleteExtender ID="TargetIndustryAutoCompleteExtender" runat="server" TargetControlID="TargetIndustryTextBox" MinimumPrefixLength="2" CompletionListElementID="TargetIndustryAutoCompleteExtenderCompletionList" CompletionListCssClass="autocompleteCompletionList" CompletionInterval="100" CompletionSetCount="15" ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetNaics" /> 
														<div id="TargetIndustryAutoCompleteExtenderCompletionList" class="autocompleteCompletionList"></div>
													</div>
												</div>
                    </td>
                    <td>
                        <input type="button" class="button3" runat="server" id="TargetIndustryAddButton" name="TargetIndustryAddButton" />
                    </td>
                </tr>
                <tr><td colspan="2"> <asp:CustomValidator runat="server" ID="TargetIndustrialClassificationValidator" ControlToValidate="TargetIndustryTextBox" ClientValidationFunction="doesNAICSExist" ErrorMessage="Industry classification (NAICS) not found"  SetFocusOnError="true" Display="Dynamic" CssClass="error" /></td></tr>
                 <tr>
                        <td colspan="2"><table role="presentation" id="TargetIndustryTable" style="width:75%;"><tbody></tbody></table><asp:HiddenField runat="server" ID="HiddenTargetIndustryList"/></td>
                    </tr>
            </table>
        </td>
    </tr>
</table>

<script type="text/javascript">
  $(document).ready(function () {
    $("#<%=ExperienceIndustryAddButton.ClientID%>").click(function () {
      if ($('#<%=ExperienceIndustrialClassificationValidator.ClientID%>').is(":hidden")) {
        AddListItem('<%=ExperienceIndustryTextBox.ClientID %>', '<%=HiddenExperienceIndustryList.ClientID %>', 'ExperienceIndustryTable');
      }
    });
    $("#<%=TargetIndustryAddButton.ClientID%>").click(function () {
      if ($('#<%=TargetIndustrialClassificationValidator.ClientID%>').is(":hidden")) {
        AddListItem('<%=TargetIndustryTextBox.ClientID %>', '<%=HiddenTargetIndustryList.ClientID %>', 'TargetIndustryTable');
      }
    });

    BuildTableFromScratch('<%=HiddenExperienceIndustryList.ClientID %>', 'ExperienceIndustryTable');
    BuildTableFromScratch('<%=HiddenTargetIndustryList.ClientID %>', 'TargetIndustryTable');

  });

  function doesNAICSExist(source, arguments) {
    var exists;

    $.ajax({
      type: "POST",
      contentType: "application/json; charset=utf-8",
      url: "<%= UrlBuilder.AjaxService() %>/CheckNaicsExists", //+ arguments.Value,
      dataType: "json",
      data: '{"naicsText": "' + arguments.Value + '"}',
      async: false,
      success: function (result) {
        exists = (result.d);
      }
    });

    arguments.IsValid = exists;
  }
</script>
