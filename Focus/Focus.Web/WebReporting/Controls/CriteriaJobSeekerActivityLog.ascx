﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaJobSeekerActivityLog.ascx.cs"
    Inherits="Focus.Web.WebReporting.Controls.CriteriaJobSeekerActivityLog" %>

<table role="presentation" style="width:100%;" class="accordionWithIcon">
    <tr class="multipleAccordionTitle">
        <td style="vertical-align:top; width:10%;">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td style="width:90%">
            <asp:Literal runat="server" ID="CriteriaJobSeekerActivityLogLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
            <table role="presentation" style="width:100%;">
                <tr>
                    <td>
                        <strong>
                            <asp:Literal runat="server" ID="ActionsHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="AddNoteCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="AddSeekerToListCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="AssignActivityOrServiceCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="AssignSeekerToStaffCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="FindJobsForSeekersCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="EmailSeekersCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="MarkForFollowUpCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="MarkIssueResolvedCheckBox"/></td>
                </tr>
            </table>
            <table role="presentation" style="width:100%;" id="ActivitiesServicesTable" runat="server">
                <tr>
                    <td>
                        <strong>
                            <asp:Literal runat="server" ID="ActivitiesServicesHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
												<focus:LocalisedLabel runat="server" ID="CategoryDropDownLabel" AssociatedControlID="CategoryDropDown" LocalisationKey="Category" DefaultText="Category" CssClass="sr-only"/>
                        <asp:DropDownList runat="server" ID="CategoryDropDown"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
	                    <focus:LocalisedLabel runat="server" ID="ActivityDropDownListLabel" AssociatedControlID="ActivityDropDownList" LocalisationKey="Activity" DefaultText="Activity" CssClass="sr-only"/>
                        <focus:AjaxDropDownList runat="server" ID="ActivityDropDownList" CssClass="jobSeekerActivityLogCategory" ></focus:AjaxDropDownList>
                        <act:CascadingDropDown ID="ActivityCascader" runat="server" TargetControlID="ActivityDropDownList" ParentControlID="CategoryDropDown" ServicePath="~/Services/AjaxService.svc" Category="Activities"  ServiceMethod="GetActivities"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
