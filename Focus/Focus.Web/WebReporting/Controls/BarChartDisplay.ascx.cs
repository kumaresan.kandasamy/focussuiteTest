﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Report;
using Focus.Core.Models;

using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class BarChartDisplay : UserControlBase
	{

    private ReportDataTableModel ReportData { get; set; }

    #region Page properties

    public IReportCriteria ReportCriteria
    {
      get { return GetViewStateValue<IReportCriteria>("BarChartDisplay:ReportCriteria"); }
      set { SetViewStateValue("BarChartDisplay:ReportCriteria", value); }
    }

    protected string YAxis
    {
      get { return GetViewStateValue<string>("BarChartDisplay:YAxis"); }
      set { SetViewStateValue("BarChartDisplay:YAxis", value); }
    }
    protected string ChartData
    {
      get { return GetViewStateValue<string>("BarChartDisplay:ChartData"); }
      set { SetViewStateValue("BarChartDisplay:ChartData", value); }
    }
    protected int ChartHeight
    {
      get { return GetViewStateValue<int>("BarChartDisplay:ChartHeight"); }
      set { SetViewStateValue("BarChartDisplay:ChartHeight", value); }
    }
    protected string LegendData
    {
      get { return GetViewStateValue<string>("BarChartDisplay:LegendData"); }
      set { SetViewStateValue("BarChartDisplay:LegendData", value); }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }


    /// <summary>
    /// Binds the specified criteria.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    public void Bind(IReportCriteria criteria)
    {
      Visible = true;
      ReportCriteria = criteria;

      YAxis = ChartData = LegendData = string.Empty;
      Visible = true;
      switch (criteria.ReportType)
      {
        case ReportType.JobSeeker:
          if (criteria.ReportTitle.IsNullOrEmpty())
            criteria.ReportTitle = CodeLocalise("JobSeekerReport.Name", "Job Seeker Report");
          ReportData = ServiceClientLocator.ReportClient(App)
                                           .GetJobSeekersDataTable((JobSeekersReportCriteria) criteria);
          break;
        case ReportType.JobOrder:
 	if (criteria.ReportTitle.IsNullOrEmpty()) 
 	  criteria.ReportTitle = CodeLocalise("JobOrderReport.Name", "#EMPLOYMENTTYPE# report");
          ReportData = ServiceClientLocator.ReportClient(App).GetJobOrdersDataTable((JobOrdersReportCriteria) criteria);
          break;
        case ReportType.Employer:
          if (criteria.ReportTitle.IsNullOrEmpty())
            criteria.ReportTitle = CodeLocalise("EmployerReport.Name", "#BUSINESS# report");
          ReportData = ServiceClientLocator.ReportClient(App).GetEmployersDataTable((EmployersReportCriteria) criteria);
          break;
        case ReportType.SupplyDemand:
          if (criteria.ReportTitle.IsNullOrEmpty())
            criteria.ReportTitle = CodeLocalise("SupplyDemandReport.Name", "Supply demand report");
          ReportData = ServiceClientLocator.ReportClient(App).GetSupplyDemandDataTable((SupplyDemandReportCriteria)criteria);
          break;
      }
      BuildReportLegend();
      BuildReportYAxis();
      DrawReport();
    }

    #region helpers

    /// <summary>
    /// Builds the job seeker report y axis.
    /// </summary>
    private void BuildReportYAxis()
    {
      var yAxis = "[";
      foreach (DataRow row in ReportData.Data.Rows)
      {
        string holdingText = HttpUtility.JavaScriptStringEncode(row[0].ToString());
        if (holdingText.IsNullOrEmpty())
          holdingText = CodeLocalise("UnknownYAxisValue", "Unknown");

        yAxis += "'" + holdingText + "',";
      }
      yAxis = yAxis.TrimEnd(Convert.ToChar(","));
      yAxis += "]";
      YAxis = yAxis;
    }

    /// <summary>
    /// Builds the job seeker report legend.
    /// </summary>
    private void BuildReportLegend()
    {
      var legendData = string.Empty;
      for (var i = 1; i < ReportData.Data.Columns.Count; i++)
      {
        // Column names should be constants already localised to the default setting
        legendData += "{label:'" + HttpUtility.JavaScriptStringEncode(CodeLocalise(ReportData.Data.Columns[i].ColumnName, ReportData.Data.Columns[i].ColumnName)) + "'},";
      }
      legendData = legendData.TrimEnd(Convert.ToChar(","));
      LegendData = legendData;
    }

    #endregion

    /// <summary>
    /// Draws the report.
    /// </summary>
    private void DrawReport()
    {
      GraphPlaceHolder.Visible = ErrorLiteral.Visible = false;
      if (ReportData == null || ReportData.Data.Rows.Count == 0)
      {
        ErrorLiteral.Visible = true;
        ErrorLiteral.Text = CodeLocalise("NoData.Error", "No data available to display");
        return;
      }
      GraphPlaceHolder.Visible = true;
      
      // Build data arrays (See JQPlot documentation online)
      var dataList = new List<string>();

      // Build opening brackets
      for (int i = 1; i < ReportData.Data.Columns.Count; i++)
      {
        dataList.Add("[");
      }
      var rowCount = 1;
      // Build inner data
      foreach (DataRow row in ReportData.Data.Rows)
      {
        for (int i = 1; i < ReportData.Data.Columns.Count; i++)
        {
          var processorString = dataList[i - 1];
          processorString += "[" + row[i] + "," + rowCount + "],";
          dataList[i - 1] = processorString;
        }
        rowCount++;
      }
      // Build closing brackets
      for (int i = 1; i < ReportData.Data.Columns.Count; i++)
      {
        var processorString = dataList[i - 1];
        processorString = processorString.TrimEnd(Convert.ToChar(",")) + "]";
        dataList[i - 1] = processorString;
      }
      // Build final data string
      ChartData = "[" + string.Join(",", dataList) + "]";

      // Calculate height of the graph otehrwise it'll appear squashed
      var calculcatedChartHeight = ReportData.Data.Rows.Count*(ReportData.Data.Columns.Count - 1)*25;
      ChartHeight = calculcatedChartHeight < 250 ? 250 : calculcatedChartHeight;
    }

  }
}