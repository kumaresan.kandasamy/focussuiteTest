﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobSeekerReportList.ascx.cs" Inherits="Focus.Web.WebReporting.Controls.JobSeekerReportList" %>

<%@ Register src="ColumnSelector.ascx" tagName="ColumnSelector" tagPrefix="uc"  %>
<%@ Register TagPrefix="uc" TagName="Pager" Src="~/Code/Controls/User/Pager.ascx" %>
<%@ Register Src="~/Code/Controls/User/EmailJobSeekerModal.ascx" TagName="EmailJobSeekerModal" TagPrefix="uc" %>

<table role="presentation">
  <tr>
    <td style="width:100%;">
      <uc:Pager ID="JobSeekerListPager" runat="server" PagedControlID="JobSeekerList" PageSize="10" DisplayRecordCount="True" />
    </td>
    <td style="white-space:nowrap;">
      <asp:LinkButton runat="server" ID="EditColumnsLink" OnClick="EditColumnsLink_Clicked" />
    </td>
  </tr>
</table>
<div id="JobSeekerReportResults" class="horizontalscroll">
    <asp:ListView runat="server" ID="JobSeekerList" ItemPlaceholderID="JobSeekerListPlaceHolder"
        OnItemDataBound="JobSeekerList_ItemDatabound" DataKeyNames="FocusPersonId" OnDataBound="JobSeekerList_DataBound"
        OnSorting="JobSeekerList_Sorting" DataSourceID="JobSeekerListDataSource">
        <LayoutTemplate>
            <table style="width:100%;" class="table">
              <tr>
                <th>
										<focus:LocalisedLabel runat="server" ID="JobSeekerListAllCheckBoxLabel" AssociatedControlID="JobSeekerListAllCheckBox" LocalisationKey="JobSeekerListAll" DefaultText="List all jobseekers" CssClass="sr-only"/>
                    <asp:CheckBox runat="server" ID="JobSeekerListAllCheckBox" onclick="JobSeekerReportList_HeaderCheckBoxClick(this)" />
                </th>
                <th style="white-space:nowrap;">
                  <asp:Literal runat="server" ID="JobSeekerNameHeader" />
                  <div class="orderingcontrols">
                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="JobSeekerNameSortAscButton"
                              runat="server" CommandName="Sort" CommandArgument="name asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9" />
                   <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="JobSeekerNameSortDescButton"
                              runat="server" CommandName="Sort" CommandArgument="name desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                  </div>
                </th>
                <th runat="server" id="EmailColumnHeader" style="white-space: nowrap;">
                    <asp:Literal runat="server" ID="EmailHeader" />
                    <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="EmailSortAscButton"
                            runat="server" CommandName="Sort" CommandArgument="email asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                       <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="EmailSortDescButton"
                            runat="server" CommandName="Sort" CommandArgument="email desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                    </div>
                </th>
                <th runat="server" id="LocationColumnHeader"  style="white-space: nowrap">
                    <asp:Literal runat="server" ID="LocationHeader" />
                    <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="LocationSortAscButton"
                                runat="server" CommandName="Sort" CommandArgument="location asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                       <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="LocationSortDescButton"
                                runat="server" CommandName="Sort" CommandArgument="location desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                    </div>
                </th>
                <th runat="server" id="OfficeColumnHeader"  style="white-space: nowrap">
                    <asp:Literal runat="server" ID="OfficeHeader" />
                </th>
                <th runat="server" id="EmploymentStatusColumnHeader"  style="white-space: nowrap">
                    <asp:Literal runat="server" ID="EmploymentStatusHeader" />
                    <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="EmploymentStatusSortAscButton"
                                runat="server" CommandName="Sort" CommandArgument="employmentstatus asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                       <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="EmploymentStatusSortDescButton"
                                runat="server" CommandName="Sort" CommandArgument="employmentstatus desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                    </div>
                </th>
                <th runat="server" id="VeteranTypeColumnHeader"  style="white-space: nowrap">
                    <asp:Literal runat="server" ID="VeteranTypeHeader" />
                    <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="VeteranTypeSortAscButton"
                                runat="server" CommandName="Sort" CommandArgument="VeteranType asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                       <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="VeteranTypeSortDescButton"
                                runat="server" CommandName="Sort" CommandArgument="VeteranType desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                    </div>
                </th>
                <th runat="server" id="VeteranTransitionTypeColumnHeader"  style="white-space: nowrap">
                    <asp:Literal runat="server" ID="VeteranTransitionTypeHeader" />
                    <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="VeteranTransitionTypeSortAscButton"
                                runat="server" CommandName="Sort" CommandArgument="VeteranTransitionType asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                       <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="VeteranTransitionTypeSortDescButton"
                                runat="server" CommandName="Sort" CommandArgument="VeteranTransitionType desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                    </div>
                </th>
                <th runat="server" id="VeteranMilitaryDischargeColumnHeader"  style="white-space: nowrap">
                    <asp:Literal runat="server" ID="VeteranMilitaryDischargeHeader" />
                    <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="VeteranMilitaryDischargeSortAscButton"
                                runat="server" CommandName="Sort" CommandArgument="VeteranMilitaryDischarge asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                       <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="VeteranMilitaryDischargeSortDescButton"
                                runat="server" CommandName="Sort" CommandArgument="VeteranMilitaryDischarge desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                    </div>
                </th>
                <th runat="server" id="VeteranBranchOfServiceColumnHeader"  style="white-space: nowrap">
                    <asp:Literal runat="server" ID="VeteranBranchOfServiceHeader" />
                    <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="VeteranBranchOfServiceSortAscButton"
                                runat="server" CommandName="Sort" CommandArgument="VeteranBranchOfService asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                       <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="VeteranBranchOfServiceSortDescButton"
                                runat="server" CommandName="Sort" CommandArgument="VeteranBranchOfService desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                    </div>
                </th>
                <th runat="server" id="LoginsColumnHeader"  style="white-space: nowrap">
                    <asp:Literal runat="server" ID="LoginsHeader" />
                    <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="LoginsSortAscButton"
                                runat="server" CommandName="Sort" CommandArgument="logins asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                       <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="LoginsSortDescButton"
                                runat="server" CommandName="Sort" CommandArgument="logins desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                    </div>
                </th>
                <th runat="server" id="PostingsViewedColumnHeader"  style="white-space: nowrap">
                    <asp:Literal runat="server" ID="PostingsViewedHeader" />
                    <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="PostingsViewedSortAscButton"
                                runat="server" CommandName="Sort" CommandArgument="PostingsViewed asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                       <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="PostingsViewedSortDescButton"
                                runat="server" CommandName="Sort" CommandArgument="PostingsViewed desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                    </div>
                </th>
                <th runat="server" id="ReferralsRequestedColumnHeader"  style="white-space: nowrap">
                    <asp:Literal runat="server" ID="ReferralsRequestedHeader" />
                    <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="ReferralsRequestedSortAscButton"
                                runat="server" CommandName="Sort" CommandArgument="referralsrequested asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                       <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="ReferralsRequestedSortDescButton"
                                runat="server" CommandName="Sort" CommandArgument="referralsrequested desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                    </div>
                </th>
                <th runat="server" id="ReferralsApprovedColumnHeader"  style="white-space: nowrap">
                    <asp:Literal runat="server" ID="ReferralsApprovedHeader" />
                    <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="ReferralsApprovedSortAscButton"
                                runat="server" CommandName="Sort" CommandArgument="ReferralsApproved asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                       <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="ReferralsApprovedSortDescButton"
                                runat="server" CommandName="Sort" CommandArgument="ReferralsApproved desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                    </div>
                </th>
                <th runat="server" id="StaffReferralsColumnHeader"  style="white-space: nowrap">
                    <asp:Literal runat="server" ID="StaffReferralsHeader" />
                    <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="StaffReferralsSortAscButton"
                                runat="server" CommandName="Sort" CommandArgument="StaffReferrals asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                       <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="StaffReferralsSortDescButton"
                                runat="server" CommandName="Sort" CommandArgument="staffreferrals desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                    </div>
                </th>
                <th runat="server" id="StaffReferralsExternalColumnHeader"  style="white-space: nowrap">
                    <asp:Literal runat="server" ID="StaffReferralsExternalHeader" />
                    <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="StaffReferralsExternalSortAscButton"
                                runat="server" CommandName="Sort" CommandArgument="StaffReferralsExternal asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                       <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="StaffReferralsExternalSortDescButton"
                                runat="server" CommandName="Sort" CommandArgument="staffReferralsExternal desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                    </div>
                </th>
                <th runat="server" id="SelfReferralsColumnHeader"  style="white-space: nowrap">
                    <asp:Literal runat="server" ID="SelfReferralsHeader" />
                    <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="SelfReferralsSortAscButton"
                                runat="server" CommandName="Sort" CommandArgument="selfreferrals asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                       <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="SelfReferralsSortDescButton"
                                runat="server" CommandName="Sort" CommandArgument="selfreferrals desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                    </div>
                </th>
                <th runat="server" id="SelfReferralsExternalColumnHeader"  style="white-space: nowrap">
                    <asp:Literal runat="server" ID="SelfReferralsExternalHeader" />
                    <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="SelfReferralsExternalSortAscButton"
                                runat="server" CommandName="Sort" CommandArgument="selfReferralsExternal asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                       <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="SelfReferralsExternalSortDescButton"
                                runat="server" CommandName="Sort" CommandArgument="selfReferralsExternal desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                    </div>
                </th>
                <th runat="server" id="TotalReferralsColumnHeader"  style="white-space: nowrap">
                    <asp:Literal runat="server" ID="TotalReferralsHeader" />
                    <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="TotalReferralsSortAscButton"
                                runat="server" CommandName="Sort" CommandArgument="TotalReferrals asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                       <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="TotalReferralsSortDescButton"
                                runat="server" CommandName="Sort" CommandArgument="TotalReferrals desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                    </div>
                </th>
                <th runat="server" id="TargettingHighGrowthSectorsColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="TargettingHighGrowthSectorsHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="TargettingHighGrowthSectorsSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="targettinghighgrowthsectors asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="TargettingHighGrowthSectorsSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="targettinghighgrowthsectors desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="SavedJobAlertsColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="SavedJobAlertsHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="SavedJobAlertsSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="SavedJobAlerts asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="SavedJobAlertsSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="SavedJobAlerts desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="SurveyVerySatisfiedColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="SurveyVerySatisfiedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="SurveyVerySatisfiedSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="SurveyVerySatisfied asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="SurveyVerySatisfiedSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="SurveyVerySatisfied desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="SurveySatisfiedColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="SurveySatisfiedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="SurveySatisfiedSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="SurveySatisfied asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="SurveySatisfiedSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="SurveySatisfied desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="SurveyDissatisfiedColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="SurveyDissatisfiedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="SurveyDissatisfiedSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="SurveyDissatisfied asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="SurveyDissatisfiedSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="SurveyDissatisfied desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="SurveyVeryDissatisfiedColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="SurveyVeryDissatisfiedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="SurveyVeryDissatisfiedSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="SurveyVeryDissatisfied asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="SurveyVeryDissatisfiedSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="SurveyVeryDissatisfied desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="SurveyNoUnexpectedMatchesColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="SurveyNoUnexpectedMatchesHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="SurveyNoUnexpectedMatchesSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="SurveyNoUnexpectedMatches asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="SurveyNoUnexpectedMatchesSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="SurveyNoUnexpectedMatches desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="SurveyUnexpectedMatchesColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="SurveyUnexpectedMatchesHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="SurveyUnexpectedMatchesSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="SurveyUnexpectedMatches asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="SurveyUnexpectedMatchesSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="SurveyUnexpectedMatches desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="SurveyReceivedInvitationsColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="SurveyReceivedInvitationsHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="SurveyReceivedInvitationsSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="SurveyReceivedInvitations asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="SurveyReceivedInvitationsSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="SurveyReceivedInvitations desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="SurveyDidNotReceiveInvitationsColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="SurveyDidNotReceiveInvitationsHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="SurveyDidNotReceiveInvitationsSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="SurveyDidNotReceiveInvitations asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="SurveyDidNotReceiveInvitationsSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="SurveyDidNotReceiveInvitations desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="NotesAddedColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="NotesAddedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="NotesAddedSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="notesadded asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="NotesAddedSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="notesadded desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="AddedToListColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="AddedToListHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="AddedToListSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="addedtolist asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="AddedToListSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="addedtolist desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="ActivitiesAssignedColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="ActivitiesAssignedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="ActivitiesAssignedSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="ActivitiesAssigned asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="ActivitiesAssignedSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="ActivitiesAssigned desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="AssignedToStaffColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="AssignedToStaffHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="AssignedToStaffSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="AssignedToStaff asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="AssignedToStaffSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="AssignedToStaff desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="ActivityServicesColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="ActivityServicesHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="ActivityServicesSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="ActivityServices asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="ActivityServicesSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="ActivityServices desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="FindJobsForSeekerColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="FindJobsForSeekerHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="FindJobsForSeekerSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="FindJobsForSeeker asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="FindJobsForSeekerSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="FindJobsForSeeker desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="EmailsSentColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="EmailsSentHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="EmailsSentSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="EmailsSent asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="EmailsSentSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="emailssent desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="FollowUpIssueColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="FollowUpIssueHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="FollowUpIssueSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="followupissue asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="FollowUpIssueSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="followupissue desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="IssuesResolvedColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="IssuesResolvedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="IssuesResolvedSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="issuesresolved asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="IssuesResolvedSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="issuesresolved desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="FailedToApplyColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="FailedToApplyHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="FailedToApplySortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="FailedToApply asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="FailedToApplySortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="FailedToApply desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="FailedToReportToInterviewColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="FailedToReportToInterviewHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="FailedToReportToInterviewSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="FailedToReportToInterview asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="FailedToReportToInterviewSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="FailedToReportToInterview desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="FailedToReportToJobColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="FailedToReportToJobHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="FailedToReportToJobSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="FailedToReportToJob asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="FailedToReportToJobSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="FailedToReportToJob desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="FailedToRespondToInvitationColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="FailedToRespondToInvitationHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="FailedToRespondToInvitationSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="FailedToRespondToInvitation asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="FailedToRespondToInvitationSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="FailedToRespondToInvitation desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="FoundJobFromOtherSourceColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="FoundJobFromOtherSourceHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="FoundJobFromOtherSourceSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="FoundJobFromOtherSource asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="FoundJobFromOtherSourceSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="FoundJobFromOtherSource desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="HiredColumnHeader"  style="white-space: nowrap">
                    <asp:Literal runat="server" ID="HiredHeader" />
                    <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="HiredSortAscButton"
                                runat="server" CommandName="Sort" CommandArgument="Hired asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                       <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="HiredSortDescButton"
                                runat="server" CommandName="Sort" CommandArgument="Hired desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                    </div>
                </th>
                <th runat="server" id="InterviewDeniedColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="InterviewDeniedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="InterviewDeniedSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="InterviewDenied asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="InterviewDeniedSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="InterviewDenied desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="InterviewScheduledColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="InterviewScheduledHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="InterviewScheduledSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="InterviewScheduled asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="InterviewScheduledSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="InterviewScheduled desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="JobAlreadyFilledColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="JobAlreadyFilledHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="JobAlreadyFilledSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="JobAlreadyFilled asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="JobAlreadyFilledSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="JobAlreadyFilled desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="NewApplicantColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="NewApplicantHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="NewApplicantSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="NewApplicant asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="NewApplicantSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="NewApplicant desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="NotHiredColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="NotHiredHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="NotHiredSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="NotHired asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="NotHiredSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="NotHired desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="NotQualifiedColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="NotQualifiedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="NotQualifiedSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="NotQualified asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="NotQualifiedSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="NotQualified desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="NotYetPlacedColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="NotYetPlacedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="NotYetPlacedSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="NotYetPlaced asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="NotYetPlacedSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="NotYetPlaced desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="RecommendedColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="RecommendedHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="RecommendedSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="Recommended asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="RecommendedSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="Recommended desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="RefusedOfferColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="RefusedOfferHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="RefusedOfferSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="RefusedOffer asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="RefusedOfferSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="RefusedOffer desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="RefusedReferralColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="RefusedReferralHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="RefusedReferralSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="RefusedReferral asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="RefusedReferralSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="RefusedReferral desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
                <th runat="server" id="UnderConsiderationColumnHeader"  style="white-space: nowrap">
                      <asp:Literal runat="server" ID="UnderConsiderationHeader" />
                      <div class="orderingcontrols">
                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="UnderConsiderationSortAscButton"
                                  runat="server" CommandName="Sort" CommandArgument="UnderConsideration asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
                         <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="UnderConsiderationSortDescButton"
                                  runat="server" CommandName="Sort" CommandArgument="UnderConsideration desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
                      </div>
                </th>
              </tr>
              <asp:PlaceHolder ID="JobSeekerListPlaceHolder" runat="server" />
          </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td>
	                <focus:LocalisedLabel runat="server" ID="JobSeekerCheckBoxLabel" AssociatedControlID="JobSeekerCheckBox" LocalisationKey="JobSeekerSelection" DefaultText="Select job seeker" CssClass="sr-only"/>
                    <asp:CheckBox runat="server" ID="JobSeekerCheckBox" GroupName="JobSeekerSelection"
                        ClientIDMode="AutoID" CausesValidation="False" onclick="JobSeekerReportList_ItemCheckBoxClick(this)" />
                </td>
                <td>
                    <asp:LinkButton runat="server" ID="JobSeekerLink" CommandName="Profile" OnCommand="JobSeekerLink_OnCommand"></asp:LinkButton>
                </td>
                <td id="EmailCell" runat="server">
                    <asp:Literal runat="server" ID="EmailLiteral"></asp:Literal>
                </td>
                <td id="LocationCell" runat="server">
                    <asp:Literal runat="server" ID="LocationLiteral"></asp:Literal>
                </td>
                <td id="OfficeCell" runat="server" style="white-space: nowrap">
                    <asp:Literal runat="server" ID="OfficeLiteral"></asp:Literal>
                    <asp:HyperLink runat="server" ID="ShowAdditionalOfficesLink" Visible="False" NavigateUrl="javascript:void(0);"  onclick="Report_ShowAdditionalOffices($(this));" ClientIDMode="Predictable"></asp:HyperLink>
                    <span style="display: none">
                      <asp:Literal runat="server" ID="AdditionalOfficesLiteral"></asp:Literal>
                      <a href="javascript:void(0);" onclick="Report_HideAdditionalOffices($(this));"><%=HtmlLocalise("Hide.text", "Hide") %></a>
                    </span>
                </td>
                <td id="EmploymentStatusCell" runat="server">
                    <asp:Literal runat="server" ID="EmploymentStatusLiteral"></asp:Literal>
                </td>
                <td id="VeteranTypeCell" runat="server">
                    <asp:Literal runat="server" ID="VeteranTypeLiteral"></asp:Literal>
                </td>
                <td id="VeteranTransitionTypeCell" runat="server">
                    <asp:Literal runat="server" ID="VeteranTransitionTypeLiteral"></asp:Literal>
                </td>
                <td id="VeteranMilitaryDischargeCell" runat="server">
                    <asp:Literal runat="server" ID="VeteranMilitaryDischargeLiteral"></asp:Literal>
                </td>
                <td id="VeteranBranchOfServiceCell" runat="server">
                    <asp:Literal runat="server" ID="VeteranBranchOfServiceLiteral"></asp:Literal>
                </td>
                <td id="LoginsCell" runat="server" style="text-align: center;">
                    <asp:Literal runat="server" ID="LoginsLiteral"></asp:Literal>
                </td>
                <td id="PostingsViewedCell" runat="server">
                    <asp:Literal runat="server" ID="PostingsViewedLiteral"></asp:Literal>
                </td>
                <td id="ReferralsRequestedCell" runat="server">
                    <asp:Literal runat="server" ID="ReferralsRequestedLiteral"></asp:Literal>
                </td>
                <td id="ReferralsApprovedCell" runat="server">
                    <asp:Literal runat="server" ID="ReferralsApprovedLiteral"></asp:Literal>
                </td>
                <td id="StaffReferralsCell" runat="server">
                    <asp:Literal runat="server" ID="StaffReferralsLiteral"></asp:Literal>
                </td>
                <td id="StaffReferralsExternalCell" runat="server">
                    <asp:Literal runat="server" ID="StaffReferralsExternalLiteral"></asp:Literal>
                </td>
                <td id="SelfReferralsCell" runat="server">
                    <asp:Literal runat="server" ID="SelfReferralsLiteral"></asp:Literal>
                </td>
                <td id="SelfReferralsExternalCell" runat="server">
                    <asp:Literal runat="server" ID="SelfReferralsExternalLiteral"></asp:Literal>
                </td>
                <td id="TotalReferralsCell" runat="server">
                    <asp:Literal runat="server" ID="TotalReferralsLiteral"></asp:Literal>
                </td>
                <td id="TargettingHighGrowthSectorsCell" runat="server">
                    <asp:Literal runat="server" ID="TargettingHighGrowthSectorsLiteral"></asp:Literal>
                </td>
                <td id="SavedJobAlertsCell" runat="server">
                    <asp:Literal runat="server" ID="SavedJobAlertsLiteral"></asp:Literal>
                </td>
                <td id="SurveyVerySatisfiedCell" runat="server">
                    <asp:Literal runat="server" ID="SurveyVerySatisfiedLiteral"></asp:Literal>
                </td>
                <td id="SurveySatisfiedCell" runat="server">
                    <asp:Literal runat="server" ID="SurveySatisfiedLiteral"></asp:Literal>
                </td>
                <td id="SurveyDissatisfiedCell" runat="server">
                    <asp:Literal runat="server" ID="SurveyDissatisfiedLiteral"></asp:Literal>
                </td>
                <td id="SurveyVeryDissatisfiedCell" runat="server">
                    <asp:Literal runat="server" ID="SurveyVeryDissatisfiedLiteral"></asp:Literal>
                </td>
                <td id="SurveyNoUnexpectedMatchesCell" runat="server">
                    <asp:Literal runat="server" ID="SurveyNoUnexpectedMatchesLiteral"></asp:Literal>
                </td>
                <td id="SurveyUnexpectedMatchesCell" runat="server">
                    <asp:Literal runat="server" ID="SurveyUnexpectedMatchesLiteral"></asp:Literal>
                </td>
                <td id="SurveyReceivedInvitationsCell" runat="server">
                    <asp:Literal runat="server" ID="SurveyReceivedInvitationsLiteral"></asp:Literal>
                </td>
                <td id="SurveyDidNotReceiveInvitationsCell" runat="server">
                    <asp:Literal runat="server" ID="SurveyDidNotReceiveInvitationsLiteral"></asp:Literal>
                </td>
                <td id="NotesAddedCell" runat="server">
                    <asp:Literal runat="server" ID="NotesAddedLiteral"></asp:Literal>
                </td>
                <td id="AddedToListCell" runat="server">
                    <asp:Literal runat="server" ID="AddedToListLiteral"></asp:Literal>
                </td>
                <td id="ActivitiesAssignedCell" runat="server">
                    <asp:Literal runat="server" ID="ActivitiesAssignedLiteral"></asp:Literal>
                </td>
                <td id="AssignedToStaffCell" runat="server">
                    <asp:Literal runat="server" ID="AssignedToStaffLiteral"></asp:Literal>
                </td>
                <td id="ActivityServicesCell" runat="server">
                    <asp:Literal runat="server" ID="ActivityServicesLiteral"></asp:Literal>
                </td>
                <td id="FindJobsForSeekerCell" runat="server">
                    <asp:Literal runat="server" ID="FindJobsForSeekerLiteral"></asp:Literal>
                </td>
                <td id="EmailsSentCell" runat="server">
                    <asp:Literal runat="server" ID="EmailsSentLiteral"></asp:Literal>
                </td>
                <td id="FollowUpIssueCell" runat="server">
                    <asp:Literal runat="server" ID="FollowUpIssueLiteral"></asp:Literal>
                </td>
                <td id="IssuesResolvedCell" runat="server">
                    <asp:Literal runat="server" ID="IssuesResolvedLiteral"></asp:Literal>
                </td>
                <td id="FailedToApplyCell" runat="server">
                    <asp:Literal runat="server" ID="FailedToApplyLiteral"></asp:Literal>
                </td>
                <td id="FailedToReportToInterviewCell" runat="server">
                    <asp:Literal runat="server" ID="FailedToReportToInterviewLiteral"></asp:Literal>
                </td>
                <td id="FailedToReportToJobCell" runat="server">
                    <asp:Literal runat="server" ID="FailedToReportToJobLiteral"></asp:Literal>
                </td>
                <td id="FailedToRespondToInvitationCell" runat="server">
                    <asp:Literal runat="server" ID="FailedToRespondToInvitationLiteral"></asp:Literal>
                </td>
                <td id="FoundJobFromOtherSourceCell" runat="server">
                    <asp:Literal runat="server" ID="FoundJobFromOtherSourceLiteral"></asp:Literal>
                </td>
                <td id="HiredCell" runat="server">
                    <asp:Literal runat="server" ID="HiredLiteral"></asp:Literal>
                </td>
                <td id="InterviewDeniedCell" runat="server">
                    <asp:Literal runat="server" ID="InterviewDeniedLiteral"></asp:Literal>
                </td>
                <td id="InterviewScheduledCell" runat="server">
                    <asp:Literal runat="server" ID="InterviewScheduledLiteral"></asp:Literal>
                </td>
                <td id="JobAlreadyFilledCell" runat="server">
                    <asp:Literal runat="server" ID="JobAlreadyFilledLiteral"></asp:Literal>
                </td>
                <td id="NewApplicantCell" runat="server">
                    <asp:Literal runat="server" ID="NewApplicantLiteral"></asp:Literal>
                </td>
                <td id="NotHiredCell" runat="server">
                    <asp:Literal runat="server" ID="NotHiredLiteral"></asp:Literal>
                </td>
                <td id="NotQualifiedCell" runat="server">
                    <asp:Literal runat="server" ID="NotQualifiedLiteral"></asp:Literal>
                </td>
                <td id="NotYetPlacedCell" runat="server">
                    <asp:Literal runat="server" ID="NotYetPlacedLiteral"></asp:Literal>
                </td>
                <td id="RecommendedCell" runat="server">
                    <asp:Literal runat="server" ID="RecommendedLiteral"></asp:Literal>
                </td>
                <td id="RefusedOfferCell" runat="server">
                    <asp:Literal runat="server" ID="RefusedOfferLiteral"></asp:Literal>
                </td>
                <td id="RefusedReferralCell" runat="server">
                    <asp:Literal runat="server" ID="RefusedReferralLiteral"></asp:Literal>
                </td>
                <td id="UnderConsiderationCell" runat="server">
                    <asp:Literal runat="server" ID="UnderConsiderationLiteral"></asp:Literal>
                </td>
            </tr>
        </ItemTemplate>
        <EmptyDataTemplate><%= HtmlLocalise("JobSeekersReport.EmptyDataTemplate.Text", "No job seekers found matching the criteria you chose.") %></EmptyDataTemplate>
    </asp:ListView>
</div>
<asp:ObjectDataSource runat="server" TypeName="Focus.Web.WebReporting.Controls.JobSeekerReportList"
    ID="JobSeekerListDataSource" EnablePaging="True" SelectMethod="GetJobSeekerList"
    SelectCountMethod="GetJobSeekerListCount" OnSelecting="JobSeekerListDataSource_Selecting"
    SortParameterName="orderby" />
<br />

<asp:Button runat="server" ID="ContactJobSeekersButton" OnClick="ContactJobSeekersButton_Clicked" CssClass="button3" />
<br />
<asp:Label ID="ContactMessage" runat="server" CssClass="error" EnableViewState="False" />

<uc:ColumnSelector Id="ColumnSelector" runat="server" OnReportColumnsChanged="ColumnSelector_ReportColumnsChanged" />
<uc:EmailJobSeekerModal ID="EmailJobSeekerModal" runat="server" />

<script type="text/javascript">
function JobSeekerReportList_HeaderCheckBoxClick(box) {
  var isChecked = $(box).prop('checked');
  $("span[GroupName='JobSeekerSelection'] input[type='checkbox']").prop("checked", isChecked);
}
function JobSeekerReportList_ItemCheckBoxClick(box) {
  var unchecked = $("span[GroupName='JobSeekerSelection'] input:checkbox:not(:checked)");
  $("input:checkbox[id$='JobSeekerListAllCheckBox']").prop("checked", unchecked.length == 0);
}
</script>