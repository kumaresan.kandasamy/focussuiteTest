﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ColumnSelector.ascx.cs" Inherits="Focus.Web.WebReporting.Controls.ColumnSelector" %>
<%@ Register src="CriteriaJobSeekerActivitySummary.ascx" tagName="CriteriaJobSeekerActivitySummary" tagPrefix="uc" %>
<%@ Register src="CriteriaJobSeekerActivityLog.ascx" tagName="CriteriaJobSeekerActivityLog" tagPrefix="uc" %>
<%@ Register src="CriteriaReferralsOutcome.ascx" tagName="CriteriaReferralsOutcome" tagPrefix="uc" %>
<%@ Register src="CriteriaJobActivitySummary.ascx" tagName="CriteriaJobActivitySummary" tagPrefix="uc" %>
<%@ Register src="CriteriaEmployerActivity.ascx" tagName="CriteriaEmployerActivity" tagPrefix="uc" %>


<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ColumnSelectorModalPopup" runat="server" BehaviorID="ColumnSelectorModalPanel"
												TargetControlID="ModalDummyTarget"
												PopupControlID="ColumnSelectorModalPanel" 
												PopupDragHandleControlID="ColumnSelectorModalPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="ColumnSelectorModalPanel" runat="server" CssClass="modal" Style="display:none">
    <h2><asp:Literal runat="server" ID="ColumnSelectorHeading"></asp:Literal></h2>
    <uc:CriteriaJobSeekerActivitySummary ID="CriteriaJobSeekerActivitySummary" runat="server"  Visible="False"/>
    <uc:CriteriaJobSeekerActivityLog ID="CriteriaJobSeekerActivityLog" runat="server" ShowActivitiesAndServices="False" Visible="False" />
    <uc:CriteriaJobActivitySummary ID="CriteriaJobActivitySummary" runat="server" Visible="False" />
    <uc:CriteriaEmployerActivity ID="CriteriaEmployerActivity" runat="server" Visible="False" />
    <uc:CriteriaReferralsOutcome ID="CriteriaReferralsOutcome" runat="server" Visible="False" />
    <asp:PlaceHolder runat="server" ID="JobSeekerExtraColumnsPlaceHolder" Visible="False">
      <table style="width:100%;" class="accordionWithIcon" role="presentation">
        <tr class="multipleAccordionTitle" id="VeteranStatusTableColumns">
          <td style="vertical-align:top; width:10%;">
            <span class="accordionIcon" style="height: 25px"></span>
          </td>
          <td style="width:90%;">
            <asp:Literal runat="server" ID="VeteranStatusHeader" />
          </td>
        </tr>
        <tr class="accordionContent">
          <td colspan="2">
            <table style="width:100%;" role="presentation">
              <tr>
                <td><asp:CheckBox runat="server" ID="VeteranTypeCheckBox"/></td>
              </tr>
              <tr>
                <td><asp:CheckBox runat="server" ID="VeteranTransitionTypeCheckBox"/></td>
              </tr>
              <tr>
                <td><asp:CheckBox runat="server" ID="VeteranMilitaryDischargeCheckBox"/></td>
              </tr>
              <tr>
                <td><asp:CheckBox runat="server" ID="VeteranBranchOfServiceCheckBox"/></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </asp:PlaceHolder>
    <br/><asp:Button runat="server" ID="CancelButton" CssClass="button3" OnClick="CancelButton_Clicked"/>&nbsp;<asp:Button runat="server" ID="SaveButton" CssClass="button4" OnClick="SaveButton_Clicked"/>
</asp:Panel>