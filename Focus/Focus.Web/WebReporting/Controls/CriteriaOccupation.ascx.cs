﻿using System;
using System.Collections.Specialized;
using System.Linq;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.Report;
using Framework.Core;

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaOccupation : UserControlBase
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();
        BindControls();
      }

			RegisterScriptVariables();
    }

    private void BindControls()
    {
      var onetJobFamilies = ServiceClientLocator.SearchClient(App).GetOnets(true).Select(onet => onet.JobFamilyId).Distinct();
      var jobFamilies = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.JobFamily).Where(family => onetJobFamilies.Contains(family.Id)).ToList();

      OccupationsFamilyDropDownList.DataSource = jobFamilies;
      OccupationsFamilyDropDownList.DataTextField = "Text";
      OccupationsDropDownListCascadingDropDown.ServiceMethod = "GetOccupations";

      OccupationsFamilyDropDownList.DataValueField = "Id";
      OccupationsFamilyDropDownList.DataBind();
      OccupationsFamilyDropDownList.Items.AddLocalisedTopDefault("Global.OccupationFamily.TopDefault", "- select an occupation family -");

      // Add prompt to Occupations cascader
      OccupationsDropDownListCascadingDropDown.PromptText = CodeLocalise("Global.Occupations.TopDefault", "- select an occupation -");
      OccupationsDropDownListCascadingDropDown.LoadingText = CodeLocalise("Global.Occupations.Progress", "[Loading occupations ...]");
      OccupationsDropDownListCascadingDropDown.PromptValue = string.Empty;
    }

		/// <summary>
		/// Registers some server side information as javascript variables
		/// </summary>
		private void RegisterScriptVariables()
		{
			var variables = new NameValueCollection
		  {
				{"OccupationsBehaviorID", OccupationsDropDownListCascadingDropDown.BehaviorID},
				{"OccupationsLoadingText", CodeLocalise("Global.Occupations.Progress", "[Loading occupations ...]")},
			  {"OccupationsSelectText", CodeLocalise("Global.Occupations.TopDefault", "- select an occupation -")},
			  {"OccupationsAllText", CodeLocalise("Global.Occupations.AllOccupations", "- all occupations -")},
		  };

			RegisterCodeValuesJson("CriteriaOccupation_Variables", "CriteriaOccupation_Variables", variables);
		}

	  public void BindCriteria(SupplyDemandOccupationsReportCriteria criteria, ReportType reportType)
	  {
		  Reset(reportType);
	  }

    /// <summary>
    /// Unbinds the occupations.
    /// </summary>
    /// <returns></returns>
    public SupplyDemandOccupationsReportCriteria UnbindOccupations()
    {
      var criteria = new SupplyDemandOccupationsReportCriteria
                       {
                         OnetId = GetFormattedOnet(),
												 OnetFamily = OccupationsFamilyDropDownList.SelectedValue.IsNullOrEmpty() ? 0 : Convert.ToInt64(OccupationsFamilyDropDownList.SelectedValue),
                       };

      return criteria;
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      #region labels

      CriteriaOccupationLabel.Text = CodeLocalise("CriteriaOccupationLabel.Text", "Occupation");
      
      #endregion

      #region error messages

			OccupationFamilyRequired.ErrorMessage = CodeLocalise("CriteriaOccupation.OccupationFamilyRequired.ErrorMessage",
																											 "Occupation - Occupation Family is a mandatory field");

      #endregion
    }

    #region Helpers

		private void Reset(ReportType reportType)
		{
			OccupationFamilyRequired.ValidationGroup = reportType.ToString();
		}

    /// <summary>
    /// Gets the formatted onet.
    /// </summary>
    /// <returns></returns>
    private string GetFormattedOnet()
    {
      var onetId = OccupationsDropDownListCascadingDropDown.SelectedValue;
      if (onetId.IsNotNullOrEmpty() && onetId.IndexOf(":::") > 0)
        onetId = onetId.Substring(0, onetId.IndexOf(":::"));

      return onetId == "::::::" ? string.Empty : onetId;
    }

    #endregion
  }
}