﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SupplyDemandReportList.ascx.cs" Inherits="Focus.Web.WebReporting.Controls.SupplyDemandReportList" %>
<%@ Register TagPrefix="uc" TagName="Pager" Src="~/Code/Controls/User/Pager.ascx" %>
<%@ Register src="ColumnSelector.ascx" tagName="ColumnSelector" tagPrefix="uc"  %>

<table>
  <tr>
    <td width="100%">
      <uc:Pager ID="SupplyDemandListPager" runat="server" PagedControlID="SupplyDemandList" PageSize="10" DisplayRecordCount="True" />
    </td>
    <td nowrap="nowrap">
      <asp:LinkButton runat="server" ID="EditColumnsLink" OnClick="EditColumnsLink_Clicked" />
    </td>
  </tr>
</table>
<div class="horizontalscroll">
     <asp:ListView runat="server" ID="SupplyDemandList" ItemPlaceholderID="SupplyDemandListPlaceHolder"
        OnItemDataBound="SupplyDemandList_ItemDatabound" OnLayoutCreated="SupplyDemandList_OnLayoutCreated" 
        OnSorting="SupplyDemandList_Sorting" DataSourceID="SupplyDemandListDataSource" OnPreRender="SupplyDemandList_OnPreRender">
        <LayoutTemplate>
         <table width="100%" class="table">
	         <thead>
		         <tr>
							<th runat="server" id="OnetCodeColumnHeader" nowrap="True">
							  <asp:Literal runat="server" ID="OnetCodeHeader" />
                <div class="orderingcontrols">
									<asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="OnetCodeSortAscButton" runat="server" CommandName="Sort" CommandArgument="OnetCode asc" CssClass="AscButton" />
                  <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="OnetCodeSortDescButton" runat="server" CommandName="Sort" CommandArgument="OnetCode desc" CssClass="DescButton" />
                </div>
							</th>
							<th runat="server" id="OnetTitleColumnHeader" nowrap="True">
							  <asp:Literal runat="server" ID="OnetTitleHeader" />
                <div class="orderingcontrols">
									<asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="OnetTitleSortAscButton" runat="server" CommandName="Sort" CommandArgument="OnetTitle asc" CssClass="AscButton" />
                  <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="OnetTitleSortDescButton" runat="server" CommandName="Sort" CommandArgument="OnetTitle desc" CssClass="DescButton" />
                </div>
							</th>
							<th runat="server" id="SupplyCountColumnHeader" nowrap="True">
								<asp:Literal runat="server" ID="SupplyCountHeader" />
								<div class="orderingcontrols">
									<asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="SupplyCountSortAscButton" runat="server" CommandName="Sort" CommandArgument="SupplyCount asc" CssClass="AscButton" />
                  <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="SupplyCountSortDescButton" runat="server" CommandName="Sort" CommandArgument="SupplyCount desc" CssClass="DescButton" />
                </div>
							</th>
							<th runat="server" id="DemandCountColumnHeader" nowrap="True">
								<asp:Literal runat="server" ID="DemandCountHeader" />
									<div class="orderingcontrols">
										<asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="DemandCountSortAscButton" runat="server" CommandName="Sort" CommandArgument="DemandCount asc" CssClass="AscButton" />
                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="DemandCountSortDescButton" runat="server" CommandName="Sort" CommandArgument="DemandCount desc" CssClass="DescButton" />
                  </div>
							</th>
							<th runat="server" id="GapColumnHeader" nowrap="True">
								<asp:Literal runat="server" ID="GapHeader" />
                <div class="orderingcontrols">
									<asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="GapSortAscButton" runat="server" CommandName="Sort" CommandArgument="GapCount asc" CssClass="AscButton" />
                  <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="GapSortDescButton" runat="server" CommandName="Sort" CommandArgument="GapCount desc" CssClass="DescButton" />
                </div>
							</th>
						</tr>
					</thead>
					<tbody>
						<asp:PlaceHolder ID="SupplyDemandListPlaceHolder" runat="server" />
					</tbody>
          <tfoot>
           <td><asp:Label runat="server" ID="TotalLabel" CssClass="embolden"></asp:Label></td>
            <td>&nbsp;</td>
            <td><asp:Label runat="server" ID="SupplyCountTotalLabel" CssClass="embolden"></asp:Label></td>
            <td><asp:Label runat="server" ID="DemandCountTotalLabel" CssClass="embolden"></asp:Label></td>
            <td><asp:Label runat="server" ID="GapTotalLabel" CssClass="embolden"></asp:Label></td>
          </tfoot>
         </table>
        </LayoutTemplate>
        <ItemTemplate>
         <tr>
                <td id="OnetCodeCell" runat="server">
                    <asp:Literal runat="server" ID="OnetCodeLiteral"></asp:Literal>
                </td>
                <td id="OnetTitleCell" runat="server">
                    <asp:Literal runat="server" ID="OnetTitleLiteral"></asp:Literal>
                </td>
                <td id="SupplyCountCell" runat="server">
                    <asp:Literal runat="server" ID="SupplyCountLiteral"></asp:Literal>
                </td>
                <td id="DemandCountCell" runat="server">
                    <asp:Literal runat="server" ID="DemandCountLiteral"></asp:Literal>
                </td>
                <td id="GapCell" runat="server">
                    <asp:Literal runat="server" ID="GapLiteral"></asp:Literal>
                </td>
            </tr>
        </ItemTemplate>
        <EmptyDataTemplate><%= HtmlLocalise("SupplyDemandReport.EmptyDataTemplate.Text", "No supply demand data found matching the criteria you chose.") %></EmptyDataTemplate>
    </asp:ListView>
 </div>
<asp:ObjectDataSource runat="server" TypeName="Focus.Web.WebReporting.Controls.SupplyDemandReportList"
    ID="SupplyDemandListDataSource" EnablePaging="True" SelectMethod="GetSupplyDemandList"
    SelectCountMethod="GetSupplyDemandListCount" OnSelecting="SupplyDemandListDataSource_Selecting"
    SortParameterName="orderby" OnSelected="SupplyDemandListDataSource_OnSelected"/>


<uc:ColumnSelector Id="ColumnSelector" runat="server" OnReportColumnsChanged="ColumnSelector_ReportColumnsChanged" />