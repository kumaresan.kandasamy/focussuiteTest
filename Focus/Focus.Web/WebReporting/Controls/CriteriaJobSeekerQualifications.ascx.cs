﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.Report;

using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaJobSeekerQualifications : UserControlBase
	{
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();
        Bind();
      }
    }

    /// <summary>
    /// Resets this instance.
    /// </summary>
    private void Reset()
    {
      CertificationsAndLicencesTextBox.Text = InternshipsTextBox.Text = SkillTextBox.Text = string.Empty;
      HiddenCertificationsAndLicencesList.Value = HiddenInternshipsList.Value = HiddenSkillList.Value = string.Empty;
      foreach (ListItem item in LevelOfEducationCheckBoxList.Items)
      {
        item.Selected = false;
      }
      foreach (ListItem item in LevelOfEducationCheckBoxList1.Items)
      {
          item.Selected = false;
      }
      foreach (ListItem item in LevelOfEducationCheckBoxList2.Items)
      {
          item.Selected = false;
      }
    }

    /// <summary>
    /// Unbinds this instance.
    /// </summary>
    /// <returns></returns>
    public QualificationsReportCriteria Unbind()
    {
      var delimiter = new[] {"|*|"};
      var educationLevels = LevelOfEducationCheckBoxList1.UnbindNullableEnumControlList<ReportJobSeekerEducationLevels>();
      educationLevels.AddRange(LevelOfEducationCheckBoxList2.UnbindNullableEnumControlList<ReportJobSeekerEducationLevels>());
      educationLevels.AddRange(LevelOfEducationCheckBoxList.UnbindNullableEnumControlList<ReportJobSeekerEducationLevels>());
      var criteria = new QualificationsReportCriteria
                     {
											 MinimumEducationLevels = educationLevels,
                       Internships = HiddenInternshipsList.Value.Split(delimiter, StringSplitOptions.RemoveEmptyEntries).ToList(),
                       CertificationAndLicences = HiddenCertificationsAndLicencesList.Value.Split(delimiter, StringSplitOptions.RemoveEmptyEntries).ToList(),
                       Skills = HiddenSkillList.Value.Split(delimiter, StringSplitOptions.RemoveEmptyEntries).ToList()
                     };
      return criteria;
    }

    public void BindCriteria(QualificationsReportCriteria criteria)
    {
      Reset();
      const string delimiter = "|*|";

      if (criteria.IsNull()) return;
      if (criteria.MinimumEducationLevels.IsNotNullOrEmpty())
      {
          if (criteria.MinimumEducationLevels.Contains(ReportJobSeekerEducationLevels.GraduateDegree))
          {
              // Graduate option now replace by separate Masters and Doctorate option
              criteria.MinimumEducationLevels.Remove(ReportJobSeekerEducationLevels.GraduateDegree);
              if (!criteria.MinimumEducationLevels.Contains(ReportJobSeekerEducationLevels.MastersDegree))
                  criteria.MinimumEducationLevels.Add(ReportJobSeekerEducationLevels.MastersDegree);
              if (!criteria.MinimumEducationLevels.Contains(ReportJobSeekerEducationLevels.DoctorateDegree))
                  criteria.MinimumEducationLevels.Add(ReportJobSeekerEducationLevels.DoctorateDegree);
          }

          if (criteria.MinimumEducationLevels.Contains(ReportJobSeekerEducationLevels.HighSchoolDiploma))
          {
              // HighSchoolDiploma option now replace by separate HighSchoolDiplomaOrEquivalent and SomeCollegeNoDegree option
              criteria.MinimumEducationLevels.Remove(ReportJobSeekerEducationLevels.HighSchoolDiploma);
              if (!criteria.MinimumEducationLevels.Contains(ReportJobSeekerEducationLevels.HighSchoolDiplomaOrEquivalent))
                  criteria.MinimumEducationLevels.Add(ReportJobSeekerEducationLevels.HighSchoolDiplomaOrEquivalent);
              if (!criteria.MinimumEducationLevels.Contains(ReportJobSeekerEducationLevels.SomeCollegeNoDegree))
                  criteria.MinimumEducationLevels.Add(ReportJobSeekerEducationLevels.SomeCollegeNoDegree);
          }
      }

      LevelOfEducationCheckBoxList.BindEnumListToControlList(criteria.MinimumEducationLevels);
      LevelOfEducationCheckBoxList1.BindEnumListToControlList(criteria.MinimumEducationLevels);
      LevelOfEducationCheckBoxList2.BindEnumListToControlList(criteria.MinimumEducationLevels);

      if (criteria.Internships.IsNotNullOrEmpty())
        HiddenInternshipsList.Value = string.Join(delimiter, criteria.Internships);
      if (criteria.CertificationAndLicences.IsNotNullOrEmpty())
        HiddenCertificationsAndLicencesList.Value = string.Join(delimiter, criteria.CertificationAndLicences);
      if (criteria.Skills.IsNotNullOrEmpty())
        HiddenSkillList.Value = string.Join(delimiter, criteria.Skills);


    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      CriteriaJobSeekerQualificationsLabel.Text = CodeLocalise("CriteriaJobSeekerQualificationsLabel.Text", "Job seeker qualifications");
      LevelOfEducationHeader.Text = CodeLocalise("LevelOfEducationCheckBoxList.Text", "Level of education");
      CertificationsAndLicencesHeader.Text = CodeLocalise("CertificationsAndLicencesHeader.Text",
                                                          "Certification & Licences");
      SkillHeader.Text = CodeLocalise("SkillHeader.Text", "Skill name");
      InternshipsHeader.Text = CodeLocalise("InternshipsHeader.Text", "Internships");
      CertificationsAndLicencesButton.Value =
        SkillButton.Value = InternshipsButton.Value = CodeLocalise("Global.Add.NoEdit", "Add");
    }

    /// <summary>
    /// Binds this instance.
    /// </summary>
    private void Bind()
    {
			LevelOfEducationCheckBoxList1.Items.Add(new ListItem(CodeLocalise(ReportJobSeekerEducationLevels.NoDiploma, true), ReportJobSeekerEducationLevels.NoDiploma.ToString()));
			LevelOfEducationCheckBoxList1.Items.Add(new ListItem(CodeLocalise(ReportJobSeekerEducationLevels.HighSchoolDiplomaOrEquivalent, true), ReportJobSeekerEducationLevels.HighSchoolDiplomaOrEquivalent.ToString()));
            LevelOfEducationCheckBoxList2.Items.Add(new ListItem(CodeLocalise(ReportJobSeekerEducationLevels.HighSchoolDiplomaOnly, true), ReportJobSeekerEducationLevels.HighSchoolDiplomaOnly.ToString()));
            LevelOfEducationCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportJobSeekerEducationLevels.SomeCollegeNoDegree, true), ReportJobSeekerEducationLevels.SomeCollegeNoDegree.ToString()));
			LevelOfEducationCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportJobSeekerEducationLevels.AssociatesDegree, true), ReportJobSeekerEducationLevels.AssociatesDegree.ToString()));
			LevelOfEducationCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportJobSeekerEducationLevels.BachelorsDegree, true), ReportJobSeekerEducationLevels.BachelorsDegree.ToString()));
			//LevelOfEducationCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportJobSeekerEducationLevels.GraduateDegree, true), ReportJobSeekerEducationLevels.GraduateDegree.ToString()));
			LevelOfEducationCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportJobSeekerEducationLevels.MastersDegree, true), ReportJobSeekerEducationLevels.MastersDegree.ToString()));
			LevelOfEducationCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportJobSeekerEducationLevels.DoctorateDegree, true), ReportJobSeekerEducationLevels.DoctorateDegree.ToString()));

            if (!App.Settings.IsComptiaSpecific)
            {
                LevelOfEducationCheckBoxList2.Items.Add(new ListItem(CodeLocalise(ReportJobSeekerEducationLevels.DisabledWithCertOnly, true), ReportJobSeekerEducationLevels.DisabledWithCertOnly.ToString()));
                LevelOfEducationCheckBoxList2.Items.Add(new ListItem(CodeLocalise(ReportJobSeekerEducationLevels.HighSchoolEquivalencyDiplomaOnly, true), ReportJobSeekerEducationLevels.HighSchoolEquivalencyDiplomaOnly.ToString()));
                LevelOfEducationCheckBoxList2.Attributes.Add("style", "position: relative; left: 10px;");
            }

    }
  }
}