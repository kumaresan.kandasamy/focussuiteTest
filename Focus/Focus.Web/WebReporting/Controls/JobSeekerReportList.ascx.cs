﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Core;
using Focus.Web.Code;
using Framework.Core;

using Focus.Common;
using Focus.Core.Criteria.Report;
using Focus.Core.Models.Career;
using Focus.Core.Views;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class JobSeekerReportList : ReportControl
	{
    private int _jobSeekerCount;

    public JobSeekersReportCriteria ReportCriteria
    {
      get { return GetViewStateValue<JobSeekersReportCriteria>("JobSeekerReportList:ReportCriteria"); }
      set { SetViewStateValue("JobSeekerReportList:ReportCriteria", value); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
        Localise();
    }

    public void Bind(JobSeekersReportCriteria criteria)
    {
      Visible = true;
      ReportCriteria = criteria;
      JobSeekerList.DataBind();
      BindSortingArrows();
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      ContactJobSeekersButton.Text = CodeLocalise("ContactJobSeekersButton.Text", "Contact selected job seeker(s)");
      EditColumnsLink.Text = CodeLocalise("EditColumnsLink.Text", "edit table columns");
    }

    #region list binding methods

    public List<JobSeekersReportView> GetJobSeekerList(JobSeekersReportCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
    {
      var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

      criteria.PageSize = maximumRows;
      criteria.PageIndex = pageIndex;

      var jobSeekers = ServiceClientLocator.ReportClient(App).GetJobSeekersReport(criteria);
      _jobSeekerCount = jobSeekers.TotalCount;

      return jobSeekers;
    }


    public int GetJobSeekerListCount(JobSeekersReportCriteria criteria)
    {
      return _jobSeekerCount;
    }


    public int GetJobSeekerListCount()
    {
      return _jobSeekerCount;
    }

    #endregion

    #region list formatting

    /// <summary>
    /// Buids the employment status text.
    /// </summary>
    /// <param name="status">The status.</param>
    /// <returns></returns>
    private string EmploymentStatusText(EmploymentStatus? status)
    {
      if (!status.HasValue) return "-";

      switch (status)
      {
        case EmploymentStatus.Employed:
          return CodeLocalise(EmploymentStatus.Employed, "Employed");
        case EmploymentStatus.UnEmployed:
          return CodeLocalise(EmploymentStatus.UnEmployed, "Unemployed");
        case EmploymentStatus.EmployedTerminationReceived:
          return CodeLocalise(EmploymentStatus.EmployedTerminationReceived, "Employed (Termination received)");
        case EmploymentStatus.None:
          return CodeLocalise(EmploymentStatus.None, "None");
        default:
          return "-";
      }
    }

    /// <summary>
    /// Buids the veteran type text.
    /// </summary>
    /// <param name="type">The status.</param>
    /// <returns></returns>
    private string VeteranTypeText(VeteranEra? type)
    {
      if (!type.HasValue) return "-";

      switch (type)
      {
        case VeteranEra.OtherVet:
        case VeteranEra.TransitioningServiceMember:
        case VeteranEra.OtherEligible:
          return CodeLocalise(type, true);
        default:
          return "-";
      }
    }

    /// <summary>
    /// Buids the veteran transition type text.
    /// </summary>
    /// <param name="type">The transition type.</param>
    /// <returns></returns>
    private string GetEnumText(Enum type)
    {
      return type == null ? "-" : CodeLocalise(type, true);
    }

    /// <summary>
    /// Builds the comma delimited string.
    /// </summary>
    /// <param name="string1">The string1.</param>
    /// <param name="string2">The string2.</param>
    /// <returns></returns>
    private string BuildCommaDelimitedString(string string1, string string2)
    {
      if (string1.IsNotNullOrEmpty() && string2.IsNotNullOrEmpty())
        return string1 + ", " + string2;
      if (string1.IsNotNullOrEmpty())
        return string1;
      if (string2.IsNotNullOrEmpty())
        return string2;
      
      return "-";
    }

    private void BindSortingArrows()
    {
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("JobSeekerNameSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("JobSeekerNameSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("EmailSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("EmailSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("LocationSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("LocationSortDescButton"));
      //FormatSortingButtons((ImageButton)JobSeekerList.FindControl("OfficeSortAscButton"));
      //FormatSortingButtons((ImageButton)JobSeekerList.FindControl("OfficeSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("EmploymentStatusSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("EmploymentStatusSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("VeteranTypeSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("VeteranTypeSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("VeteranTransitionTypeSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("VeteranTransitionTypeSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("VeteranMilitaryDischargeSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("VeteranMilitaryDischargeSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("VeteranBranchOfServiceSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("VeteranBranchOfServiceSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("LoginsSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("LoginsSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("PostingsViewedSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("PostingsViewedSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("ReferralsRequestedSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("ReferralsRequestedSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("StaffReferralsSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("StaffReferralsSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SelfReferralsSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SelfReferralsSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("StaffReferralsExternalSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("StaffReferralsExternalSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SelfReferralsExternalSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SelfReferralsExternalSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("TotalReferralsSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("TotalReferralsSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("ReferralsApprovedSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("ReferralsApprovedSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("TargettingHighGrowthSectorsSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("TargettingHighGrowthSectorsSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SavedJobAlertsSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SavedJobAlertsSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SurveyVerySatisfiedSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SurveyVerySatisfiedSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SurveySatisfiedSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SurveySatisfiedSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SurveyDissatisfiedSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SurveyDissatisfiedSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SurveyVeryDissatisfiedSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SurveyVeryDissatisfiedSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SurveyNoUnexpectedMatchesSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SurveyNoUnexpectedMatchesSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SurveyUnexpectedMatchesSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SurveyUnexpectedMatchesSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SurveyReceivedInvitationsSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SurveyReceivedInvitationsSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SurveyDidNotReceiveInvitationsSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("SurveyDidNotReceiveInvitationsSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("NotesAddedSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("NotesAddedSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("AddedToListSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("AddedToListSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("ActivitiesAssignedSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("ActivitiesAssignedSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("AssignedToStaffSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("AssignedToStaffSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("ActivityServicesSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("ActivityServicesSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("FindJobsForSeekerSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("FindJobsForSeekerSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("EmailsSentSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("EmailsSentSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("FollowUpIssueSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("FollowUpIssueSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("IssuesResolvedSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("IssuesResolvedSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("HiredSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("HiredSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("NotHiredSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("NotHiredSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("FailedToApplySortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("FailedToApplySortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("FailedToReportToInterviewSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("FailedToReportToInterviewSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("InterviewDeniedSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("InterviewDeniedSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("InterviewScheduledSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("InterviewScheduledSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("NewApplicantSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("NewApplicantSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("RecommendedSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("RecommendedSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("RefusedOfferSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("RefusedOfferSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("UnderConsiderationSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("UnderConsiderationSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("FailedToReportToJobSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("FailedToReportToJobSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("FailedToRespondToInvitationSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("FailedToRespondToInvitationSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("FoundJobFromOtherSourceSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("FoundJobFromOtherSourceSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("JobAlreadyFilledSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("JobAlreadyFilledSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("NotQualifiedSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("NotQualifiedSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("NotYetPlacedSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("NotYetPlacedSortDescButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("RefusedReferralSortAscButton"));
      FormatSortingButtons((ImageButton)JobSeekerList.FindControl("RefusedReferralSortDescButton"));
    }

    private void FormatSortingButtons(ImageButton button)
    {
      if (button.IsNull()) return;

      var sortingParams = button.CommandArgument.Split(Convert.ToChar(" "));

      if (sortingParams.IsNullOrEmpty() || sortingParams.GetLength(0) != 2)
        throw new Exception("Job seeker report sorting error getting parameters.");

      ReportJobSeekerOrderBy orderby;
      ReportOrder orderDirection;

      if (!Enum.TryParse(sortingParams[0], out orderby))
        throw new Exception("Job seeker report sorting error getting order by values.");
      if (!Enum.TryParse(sortingParams[1], out orderDirection))
        throw new Exception("Job seeker report sorting error getting order direction values.");

      if (orderby == ReportCriteria.OrderInfo.OrderBy && orderDirection == ReportCriteria.OrderInfo.Direction)
      {
        button.Enabled = false;
	      button.ImageUrl = orderDirection == ReportOrder.Ascending ? UrlBuilder.ActivityListSortAscImage() : UrlBuilder.ActivityListSortDescImage();
      }
      else
      {
        button.Enabled = true;
        button.ImageUrl = orderDirection == ReportOrder.Ascending ? UrlBuilder.ActivityOnSortAscImage() : UrlBuilder.CategorySortDescImage();
      }

			button.ToolTip = button.AlternateText = orderDirection == ReportOrder.Ascending
				? CodeLocalise("Global.SortAscending", "Sort ascending")
				: CodeLocalise("Global.SortDescending", "Sort descending");
    }

    #endregion



    #region events

    protected void JobSeekerList_ItemDatabound(object sender, ListViewItemEventArgs e)
    {
      var jobSeeker = (JobSeekersReportView)e.Item.DataItem;

      var jobSeekerLink = (LinkButton) e.Item.FindControl("JobSeekerLink");
      jobSeekerLink.Text = BuildCommaDelimitedString(jobSeeker.LastName, string.Concat(jobSeeker.FirstName,  " ", jobSeeker.MiddleInitial).Trim());
      jobSeekerLink.CommandArgument = jobSeeker.FocusPersonId.ToString(CultureInfo.InvariantCulture);

      ((Literal)e.Item.FindControl("EmailLiteral")).Text = jobSeeker.EmailAddress;
      ((Literal)e.Item.FindControl("LocationLiteral")).Text = jobSeeker.County.IsNotNullOrEmpty()
                                                                 ? string.Join(", ", new[] { jobSeeker.County, jobSeeker.State })
                                                                 : string.Empty;
      
      BuildOfficeField(jobSeeker.Office, e.Item);

      ((Literal)e.Item.FindControl("EmploymentStatusLiteral")).Text = EmploymentStatusText(jobSeeker.EmploymentStatus);
      ((Literal)e.Item.FindControl("VeteranTypeLiteral")).Text = VeteranTypeText(jobSeeker.VeteranType);
      ((Literal)e.Item.FindControl("VeteranTransitionTypeLiteral")).Text = GetEnumText(jobSeeker.VeteranTransitionType);
      ((Literal)e.Item.FindControl("VeteranMilitaryDischargeLiteral")).Text = GetEnumText(jobSeeker.VeteranMilitaryDischarge);
      ((Literal)e.Item.FindControl("VeteranBranchOfServiceLiteral")).Text = GetEnumText(jobSeeker.VeteranBranchOfService);
      ((Literal)e.Item.FindControl("LoginsLiteral")).Text = jobSeeker.Totals.Logins.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("PostingsViewedLiteral")).Text = jobSeeker.Totals.PostingsViewed.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("ReferralsRequestedLiteral")).Text = jobSeeker.Totals.ReferralRequests.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("StaffReferralsLiteral")).Text = jobSeeker.Totals.StaffReferrals.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("SelfReferralsLiteral")).Text = jobSeeker.Totals.SelfReferrals.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("StaffReferralsExternalLiteral")).Text = jobSeeker.Totals.StaffReferralsExternal.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("SelfReferralsExternalLiteral")).Text = jobSeeker.Totals.SelfReferralsExternal.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("TotalReferralsLiteral")).Text = jobSeeker.Totals.TotalReferrals.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("ReferralsApprovedLiteral")).Text = jobSeeker.Totals.ReferralsApproved.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("TargettingHighGrowthSectorsLiteral")).Text = jobSeeker.Totals.TargetingHighGrowthSectors.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("SavedJobAlertsLiteral")).Text = jobSeeker.Totals.SavedJobAlerts.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("SurveyVerySatisfiedLiteral")).Text = jobSeeker.Totals.SurveyVerySatisfied.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("SurveySatisfiedLiteral")).Text = jobSeeker.Totals.SurveySatisfied.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("SurveyDissatisfiedLiteral")).Text = jobSeeker.Totals.SurveyDissatisfied.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("SurveyVeryDissatisfiedLiteral")).Text = jobSeeker.Totals.SurveyVeryDissatisfied.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("SurveyNoUnexpectedMatchesLiteral")).Text = jobSeeker.Totals.SurveyNoUnexpectedMatches.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("SurveyUnexpectedMatchesLiteral")).Text = jobSeeker.Totals.SurveyUnexpectedMatches.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("SurveyReceivedInvitationsLiteral")).Text = jobSeeker.Totals.SurveyReceivedInvitations.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("SurveyDidNotReceiveInvitationsLiteral")).Text = jobSeeker.Totals.SurveyDidNotReceiveInvitations.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("NotesAddedLiteral")).Text = jobSeeker.Totals.NotesAdded.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("AddedToListLiteral")).Text = jobSeeker.Totals.AddedToLists.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("ActivitiesAssignedLiteral")).Text = jobSeeker.Totals.ActivitiesAssigned.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("AssignedToStaffLiteral")).Text = jobSeeker.Totals.StaffAssignments.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("ActivityServicesLiteral")).Text = jobSeeker.Totals.ActivityServices.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("FindJobsForSeekerLiteral")).Text = jobSeeker.Totals.FindJobsForSeeker.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("EmailsSentLiteral")).Text = jobSeeker.Totals.EmailsSent.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("FollowUpIssueLiteral")).Text = jobSeeker.Totals.FollowUpIssues.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("IssuesResolvedLiteral")).Text = jobSeeker.Totals.IssuesResolved.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("HiredLiteral")).Text = jobSeeker.Totals.Hired.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("NotHiredLiteral")).Text = jobSeeker.Totals.NotHired.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("FailedToApplyLiteral")).Text = jobSeeker.Totals.FailedToApply.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("FailedToReportToInterviewLiteral")).Text = jobSeeker.Totals.FailedToReportToInterview.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("InterviewDeniedLiteral")).Text = jobSeeker.Totals.InterviewDenied.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("InterviewScheduledLiteral")).Text = jobSeeker.Totals.InterviewScheduled.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("NewApplicantLiteral")).Text = jobSeeker.Totals.NewApplicant.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("RecommendedLiteral")).Text = jobSeeker.Totals.Recommended.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("RefusedOfferLiteral")).Text = jobSeeker.Totals.RefusedOffer.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("UnderConsiderationLiteral")).Text = jobSeeker.Totals.UnderConsideration.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("FailedToReportToJobLiteral")).Text = jobSeeker.Totals.FailedToReportToJob.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("FailedToRespondToInvitationLiteral")).Text = jobSeeker.Totals.FailedToRespondToInvitation.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("FoundJobFromOtherSourceLiteral")).Text = jobSeeker.Totals.FoundJobFromOtherSource.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("JobAlreadyFilledLiteral")).Text = jobSeeker.Totals.JobAlreadyFilled.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("NotQualifiedLiteral")).Text = jobSeeker.Totals.NotQualified.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("NotYetPlacedLiteral")).Text = jobSeeker.Totals.NotYetPlaced.ToString(CultureInfo.InvariantCulture);
      ((Literal)e.Item.FindControl("RefusedReferralLiteral")).Text = jobSeeker.Totals.RefusedReferral.ToString(CultureInfo.InvariantCulture);

      SetCellVisibility(e.Item);

    }

    protected void JobSeekerListDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
      e.InputParameters["criteria"] = ReportCriteria;
    }

    protected void JobSeekerList_DataBound(object sender, EventArgs e)
    {
      if (JobSeekerList.Items.IsNullOrEmpty())
      {
        ContactJobSeekersButton.Visible = JobSeekerListPager.Visible = EditColumnsLink.Visible = false;
        return; // No data so empty data template is displayed
      }
      ContactJobSeekersButton.Visible = JobSeekerListPager.Visible = EditColumnsLink.Visible = true;

      #region header binding

      ((CheckBox) JobSeekerList.FindControl("JobSeekerListAllCheckBox")).Checked = false;

      ((Literal)JobSeekerList.FindControl("JobSeekerNameHeader")).Text = CodeLocalise("JobSeekerNameHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.Name);
      ((ImageButton)JobSeekerList.FindControl("JobSeekerNameSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.LastName, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("JobSeekerNameSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.LastName, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("EmailHeader")).Text = CodeLocalise("EmailHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.Email);
      ((ImageButton)JobSeekerList.FindControl("EmailSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.EmailAddress, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("EmailSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.EmailAddress, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("LocationHeader")).Text = CodeLocalise("LocationHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.Location);
      ((ImageButton)JobSeekerList.FindControl("LocationSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.County, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("LocationSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.County, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("OfficeHeader")).Text = CodeLocalise("OfficeHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.Office);
      //((ImageButton)JobSeekerList.FindControl("OfficeSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.Office, " ", ReportOrder.Ascending);
      //((ImageButton)JobSeekerList.FindControl("OfficeSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.Office, " ", ReportOrder.Descending);

      ((Literal)JobSeekerList.FindControl("EmploymentStatusHeader")).Text = CodeLocalise("EmploymentStatusHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.Status);
      ((ImageButton)JobSeekerList.FindControl("EmploymentStatusSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.EmploymentStatus, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("EmploymentStatusSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.EmploymentStatus, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("VeteranTypeHeader")).Text = CodeLocalise("VeteranTypeHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.VeteranType);
      ((ImageButton)JobSeekerList.FindControl("VeteranTypeSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.VeteranType, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("VeteranTypeSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.VeteranType, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("VeteranTransitionTypeHeader")).Text = CodeLocalise("VeteranTransitionTypeHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.VeteranTransitionType);
      ((ImageButton)JobSeekerList.FindControl("VeteranTransitionTypeSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.VeteranTransitionType, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("VeteranTransitionTypeSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.VeteranTransitionType, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("VeteranMilitaryDischargeHeader")).Text = CodeLocalise("VeteranMilitaryDischargeHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.VeteranMilitaryDischarge);
      ((ImageButton)JobSeekerList.FindControl("VeteranMilitaryDischargeSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.VeteranMilitaryDischarge, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("VeteranMilitaryDischargeSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.VeteranMilitaryDischarge, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("VeteranBranchOfServiceHeader")).Text = CodeLocalise("VeteranBranchOfServiceHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.VeteranBranchOfService);
      ((ImageButton)JobSeekerList.FindControl("VeteranBranchOfServiceSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.VeteranBranchOfService, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("VeteranBranchOfServiceSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.VeteranBranchOfService, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("LoginsHeader")).Text = CodeLocalise("Logins.Text", Constants.Reporting.JobSeekerReportDataColumns.Logins);
      ((ImageButton) JobSeekerList.FindControl("LoginsSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.Logins, " ", ReportOrder.Ascending);
			((ImageButton) JobSeekerList.FindControl("LoginsSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.Logins, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("PostingsViewedHeader")).Text = CodeLocalise("PostingsViewedLiteral.Text", Constants.Reporting.JobSeekerReportDataColumns.PostingsViewed);
      ((ImageButton)JobSeekerList.FindControl("PostingsViewedSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.PostingsViewed, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("PostingsViewedSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.PostingsViewed, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("ReferralsRequestedHeader")).Text = CodeLocalise("ReferralsRequestedHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.ReferralsRequested);
      ((ImageButton)JobSeekerList.FindControl("ReferralsRequestedSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.ReferralRequests, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("ReferralsRequestedSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.ReferralRequests, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("StaffReferralsHeader")).Text = CodeLocalise("StaffReferralsHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.StaffReferrals);
      ((ImageButton)JobSeekerList.FindControl("StaffReferralsSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.StaffReferrals, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("StaffReferralsSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.StaffReferrals, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("SelfReferralsHeader")).Text = CodeLocalise("SelfReferralsHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.SelfReferrals);
      ((ImageButton)JobSeekerList.FindControl("SelfReferralsSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SelfReferrals, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("SelfReferralsSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SelfReferrals, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("StaffReferralsExternalHeader")).Text = CodeLocalise("StaffReferralsExternalHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.StaffReferralsExternal);
      ((ImageButton)JobSeekerList.FindControl("StaffReferralsExternalSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.StaffReferralsExternal, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("StaffReferralsExternalSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.StaffReferralsExternal, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("SelfReferralsExternalHeader")).Text = CodeLocalise("SelfReferralsExternalHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.SelfReferralsExternal);
      ((ImageButton)JobSeekerList.FindControl("SelfReferralsExternalSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SelfReferralsExternal, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("SelfReferralsExternalSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SelfReferralsExternal, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("TotalReferralsHeader")).Text = CodeLocalise("TotalReferralsHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.TotalReferrals);
      ((ImageButton)JobSeekerList.FindControl("TotalReferralsSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.TotalReferrals, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("TotalReferralsSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.TotalReferrals, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("ReferralsApprovedHeader")).Text = CodeLocalise("ReferralsApprovedHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.ReferralsApproved);
      ((ImageButton)JobSeekerList.FindControl("ReferralsApprovedSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.ReferralsApproved, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("ReferralsApprovedSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.ReferralsApproved, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("TargettingHighGrowthSectorsHeader")).Text = CodeLocalise("TargettingHighGrowthSectorsHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.TargetingHighGrowth);
      ((ImageButton)JobSeekerList.FindControl("TargettingHighGrowthSectorsSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.TargetingHighGrowthSectors, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("TargettingHighGrowthSectorsSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.TargetingHighGrowthSectors, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("SavedJobAlertsHeader")).Text = CodeLocalise("SavedJobAlertsHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.SavedJobAlert);
      ((ImageButton)JobSeekerList.FindControl("SavedJobAlertsSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SavedJobAlerts, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("SavedJobAlertsSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SavedJobAlerts, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("SurveyVerySatisfiedHeader")).Text = CodeLocalise("SurveyVerySatisfiedHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.SurveyVerySatisfied);
      ((ImageButton)JobSeekerList.FindControl("SurveyVerySatisfiedSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SurveyVerySatisfied, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("SurveyVerySatisfiedSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SurveyVerySatisfied, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("SurveySatisfiedHeader")).Text = CodeLocalise("SurveySatisfiedHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.SurveySatisfied);
      ((ImageButton)JobSeekerList.FindControl("SurveySatisfiedSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SurveySatisfied, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("SurveySatisfiedSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SurveySatisfied, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("SurveyDissatisfiedHeader")).Text = CodeLocalise("SurveyDissatisfiedHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.SurveyDissatisfied);
      ((ImageButton)JobSeekerList.FindControl("SurveyDissatisfiedSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SurveyDissatisfied, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("SurveyDissatisfiedSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SurveyDissatisfied, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("SurveyVeryDissatisfiedHeader")).Text = CodeLocalise("SurveyVeryDissatisfiedHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.SurveyVeryDissatisfied);
      ((ImageButton)JobSeekerList.FindControl("SurveyVeryDissatisfiedSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SurveyVeryDissatisfied, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("SurveyVeryDissatisfiedSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SurveyVeryDissatisfied, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("SurveyNoUnexpectedMatchesHeader")).Text = CodeLocalise("SurveyNoUnexpectedMatchesHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.SurveyNoUnexpectedMatches);
      ((ImageButton)JobSeekerList.FindControl("SurveyNoUnexpectedMatchesSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SurveyNoUnexpectedMatches, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("SurveyNoUnexpectedMatchesSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SurveyNoUnexpectedMatches, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("SurveyUnexpectedMatchesHeader")).Text = CodeLocalise("SurveyUnexpectedMatchesHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.SurveyUnexpectedMatches);
      ((ImageButton)JobSeekerList.FindControl("SurveyUnexpectedMatchesSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SurveyUnexpectedMatches, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("SurveyUnexpectedMatchesSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SurveyUnexpectedMatches, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("SurveyReceivedInvitationsHeader")).Text = CodeLocalise("SurveyReceivedInvitationsHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.SurveyReceivedInvitations);
      ((ImageButton)JobSeekerList.FindControl("SurveyReceivedInvitationsSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SurveyReceivedInvitations, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("SurveyReceivedInvitationsSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SurveyReceivedInvitations, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("SurveyDidNotReceiveInvitationsHeader")).Text = CodeLocalise("SurveyDidNotReceiveInvitationsHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.SurveyDidNotReceiveInvitations);
      ((ImageButton)JobSeekerList.FindControl("SurveyDidNotReceiveInvitationsSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SurveyDidNotReceiveInvitations, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("SurveyDidNotReceiveInvitationsSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.SurveyDidNotReceiveInvitations, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("NotesAddedHeader")).Text = CodeLocalise("NotesAddedHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.NotesAdded);
      ((ImageButton)JobSeekerList.FindControl("NotesAddedSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.NotesAdded, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("NotesAddedSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.NotesAdded, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("AddedToListHeader")).Text = CodeLocalise("AddedToListHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.AddedToLists);
      ((ImageButton)JobSeekerList.FindControl("AddedToListSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.AddedToLists, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("AddedToListSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.AddedToLists, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("ActivitiesAssignedHeader")).Text = CodeLocalise("ActivitiesAssignedHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.ActivitiesAssigned);
      ((ImageButton)JobSeekerList.FindControl("ActivitiesAssignedSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.ActivitiesAssigned, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("ActivitiesAssignedSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.ActivitiesAssigned, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("AssignedToStaffHeader")).Text = CodeLocalise("AssignedToStaffHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.StaffAssignments);
      ((ImageButton)JobSeekerList.FindControl("AssignedToStaffSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.StaffAssignments, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("AssignedToStaffSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.StaffAssignments, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("ActivityServicesHeader")).Text = CodeLocalise("ActivityServicesHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.ActivityServices);
      ((ImageButton)JobSeekerList.FindControl("ActivityServicesSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.ActivityServices, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("ActivityServicesSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.ActivityServices, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("FindJobsForSeekerHeader")).Text = CodeLocalise("FindJobsForSeekerHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.FindJobsForSeeker);
      ((ImageButton)JobSeekerList.FindControl("FindJobsForSeekerSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FindJobsForSeeker, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("FindJobsForSeekerSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FindJobsForSeeker, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("EmailsSentHeader")).Text = CodeLocalise("EmailsSentHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.EmailsSent);
      ((ImageButton)JobSeekerList.FindControl("EmailsSentSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.EmailsSent, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("EmailsSentSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.EmailsSent, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("FollowUpIssueHeader")).Text = CodeLocalise("FollowUpIssueHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.FollowUpIssues);
      ((ImageButton)JobSeekerList.FindControl("FollowUpIssueSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FollowUpIssues, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("FollowUpIssueSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FollowUpIssues, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("IssuesResolvedHeader")).Text = CodeLocalise("IssuesResolvedHeader.Text", Constants.Reporting.JobSeekerReportDataColumns.IssuesResolved);
      ((ImageButton)JobSeekerList.FindControl("IssuesResolvedSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.IssuesResolved, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("IssuesResolvedSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.IssuesResolved, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("HiredHeader")).Text = CodeLocalise("HiredHeader.Text", Constants.Reporting.SharedReportDataColumns.Hired);
      ((ImageButton)JobSeekerList.FindControl("HiredSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.Hired, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("HiredSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.Hired, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("NotHiredHeader")).Text = CodeLocalise("NotHiredHeader.Text", Constants.Reporting.SharedReportDataColumns.NotHired);
      ((ImageButton)JobSeekerList.FindControl("NotHiredSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.NotHired, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("NotHiredSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.NotHired, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("FailedToApplyHeader")).Text = CodeLocalise("FailedToApplyHeader.Text", Constants.Reporting.SharedReportDataColumns.FailedToApply);
      ((ImageButton)JobSeekerList.FindControl("FailedToApplySortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FailedToApply, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("FailedToApplySortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FailedToApply, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("FailedToReportToInterviewHeader")).Text = CodeLocalise("FailedToReportToInterviewHeader.Text", Constants.Reporting.SharedReportDataColumns.FailedToReportToInterview);
      ((ImageButton)JobSeekerList.FindControl("FailedToReportToInterviewSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FailedToReportToInterview, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("FailedToReportToInterviewSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FailedToReportToInterview, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("InterviewDeniedHeader")).Text = CodeLocalise("InterviewDeniedHeader.Text", Constants.Reporting.SharedReportDataColumns.InterviewDenied);
      ((ImageButton)JobSeekerList.FindControl("InterviewDeniedSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.InterviewDenied, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("InterviewDeniedSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.InterviewDenied, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("InterviewScheduledHeader")).Text = CodeLocalise("InterviewScheduledHeader.Text", Constants.Reporting.SharedReportDataColumns.InterviewScheduled);
      ((ImageButton)JobSeekerList.FindControl("InterviewScheduledSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.InterviewScheduled, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("InterviewScheduledSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.InterviewScheduled, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("NewApplicantHeader")).Text = CodeLocalise("NewApplicantHeader.Text", Constants.Reporting.SharedReportDataColumns.NewApplicant);
      ((ImageButton)JobSeekerList.FindControl("NewApplicantSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.NewApplicant, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("NewApplicantSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.NewApplicant, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("RecommendedHeader")).Text = CodeLocalise("RecommendedHeader.Text", Constants.Reporting.SharedReportDataColumns.Recommended);
      ((ImageButton)JobSeekerList.FindControl("RecommendedSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.Recommended, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("RecommendedSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.Recommended, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("RefusedOfferHeader")).Text = CodeLocalise("RefusedOfferHeader.Text", Constants.Reporting.SharedReportDataColumns.RefusedOffer);
      ((ImageButton)JobSeekerList.FindControl("RefusedOfferSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.RefusedOffer, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("RefusedOfferSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.RefusedOffer, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("UnderConsiderationHeader")).Text = CodeLocalise("UnderConsiderationHeader.Text", Constants.Reporting.SharedReportDataColumns.UnderConsideration);
      ((ImageButton)JobSeekerList.FindControl("UnderConsiderationSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.UnderConsideration, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("UnderConsiderationSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.UnderConsideration, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("FailedToReportToJobHeader")).Text = CodeLocalise("FailedToReportToJobHeader.Text", Constants.Reporting.SharedReportDataColumns.FailedToReportToJob);
      ((ImageButton)JobSeekerList.FindControl("FailedToReportToJobSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FailedToReportToJob, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("FailedToReportToJobSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FailedToReportToJob, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("FailedToRespondToInvitationHeader")).Text = CodeLocalise("FailedToRespondToInvitationHeader.Text", Constants.Reporting.SharedReportDataColumns.FailedToRespondToInvitation);
      ((ImageButton)JobSeekerList.FindControl("FailedToRespondToInvitationSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FailedToRespondToInvitation, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("FailedToRespondToInvitationSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FailedToRespondToInvitation, " ", ReportOrder.Descending);
			
      ((Literal)JobSeekerList.FindControl("FoundJobFromOtherSourceHeader")).Text = CodeLocalise("FoundJobFromOtherSourceHeader.Text", Constants.Reporting.SharedReportDataColumns.FoundJobFromOtherSource);
      ((ImageButton)JobSeekerList.FindControl("FoundJobFromOtherSourceSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FoundJobFromOtherSource, " ", ReportOrder.Ascending);
			((ImageButton)JobSeekerList.FindControl("FoundJobFromOtherSourceSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.FoundJobFromOtherSource, " ", ReportOrder.Descending);

      ((Literal)JobSeekerList.FindControl("JobAlreadyFilledHeader")).Text = CodeLocalise("JobAlreadyFilledHeader.Text", Constants.Reporting.SharedReportDataColumns.JobAlreadyFilled);
      ((ImageButton)JobSeekerList.FindControl("JobAlreadyFilledSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.JobAlreadyFilled, " ", ReportOrder.Ascending);
      ((ImageButton)JobSeekerList.FindControl("JobAlreadyFilledSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.JobAlreadyFilled, " ", ReportOrder.Descending);

      ((Literal)JobSeekerList.FindControl("NotQualifiedHeader")).Text = CodeLocalise("NotQualifiedHeader.Text", Constants.Reporting.SharedReportDataColumns.NotQualified);
      ((ImageButton)JobSeekerList.FindControl("NotQualifiedSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.NotQualified, " ", ReportOrder.Ascending);
      ((ImageButton)JobSeekerList.FindControl("NotQualifiedSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.NotQualified, " ", ReportOrder.Descending);

      ((Literal)JobSeekerList.FindControl("NotYetPlacedHeader")).Text = CodeLocalise("NotYetPlacedHeader.Text", Constants.Reporting.SharedReportDataColumns.NotYetPlaced);
      ((ImageButton)JobSeekerList.FindControl("NotYetPlacedSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.NotYetPlaced, " ", ReportOrder.Ascending);
      ((ImageButton)JobSeekerList.FindControl("NotYetPlacedSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.NotYetPlaced, " ", ReportOrder.Descending);

      ((Literal)JobSeekerList.FindControl("RefusedReferralHeader")).Text = CodeLocalise("RefusedReferralHeader.Text", Constants.Reporting.SharedReportDataColumns.RefusedReferral);
      ((ImageButton)JobSeekerList.FindControl("RefusedReferralSortAscButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.RefusedReferral, " ", ReportOrder.Ascending);
      ((ImageButton)JobSeekerList.FindControl("RefusedReferralSortDescButton")).CommandArgument = string.Concat(ReportJobSeekerOrderBy.RefusedReferral, " ", ReportOrder.Descending);

      #endregion

      SetHeaderVisibility();

    }

    protected void JobSeekerList_Sorting(object sender, ListViewSortEventArgs e)
    {
      if (e.SortExpression.IsNullOrEmpty())
        throw new Exception("Job seeker report sorting error.");

      var sortingParams = e.SortExpression.Split(Convert.ToChar(" "));

      if (sortingParams.IsNullOrEmpty() || sortingParams.GetLength(0) != 2)
        throw new Exception("Job seeker report sorting error getting parameters.");

      ReportJobSeekerOrderBy orderby;
      ReportOrder orderDirection;

      if (!Enum.TryParse(sortingParams[0], out orderby))
        throw new Exception("Job seeker report sorting error getting order by values.");
      if (!Enum.TryParse(sortingParams[1], out orderDirection))
        throw new Exception("Job seeker report sorting error getting order direction values.");

      ReportCriteria.OrderInfo = new JobSeekersReportSortOrder {OrderBy = orderby, Direction = orderDirection};

      BindSortingArrows();

    }

    /// <summary>
    /// Fires when the job seeker link is click to navigate to the job seeker profile page
    /// </summary>
    /// <param name="sender">Link button raising the event</param>
    /// <param name="e">Command arguments for the link</param>
    protected void JobSeekerLink_OnCommand(object sender, CommandEventArgs e)
    {
      var personId = long.Parse(e.CommandArgument.ToString());

      var reportId = ServiceClientLocator.ReportClient(App).SaveReportToSession(ReportCriteria);

      Response.Redirect(UrlBuilder.ReportingJobSeekerProfile(personId, reportId.ToString(CultureInfo.InvariantCulture)));
    }

    /// <summary>
    /// Fires when the Contact Job Seeker button is clicked
    /// </summary>
    /// <param name="sender">Button raising the event</param>
    /// <param name="e">Standard event arguments</param>
    protected void ContactJobSeekersButton_Clicked(object sender, EventArgs e)
    {
      var candidateIds = (from item in JobSeekerList.Items
                          let checkBox = (CheckBox) item.FindControl("JobSeekerCheckBox")
                          where checkBox.Checked
                          select (long)(JobSeekerList.DataKeys[item.DisplayIndex]["FocusPersonId"])).ToList();

      if (candidateIds.Any())
        EmailJobSeekerModal.Show(candidateIds);
      else
        ContactMessage.Text = HtmlLocalise("ContactMessage.Text", "Please select at least one #CANDIDATETYPE#:LOWER");
    }

    protected void EditColumnsLink_Clicked(object sender, EventArgs e)
    {
      ColumnSelector.Show(ReportCriteria);
    }


    protected void ColumnSelector_ReportColumnsChanged(object sender, CommandEventArgs eventargs)
    {
      Bind((JobSeekersReportCriteria) eventargs.CommandArgument);
      OnReportCriteriaChanged(eventargs);
    }

    #endregion

    #region helpers

    /// <summary>
    /// Sets the header visibility.
    /// </summary>
    private void SetHeaderVisibility()
    {
      JobSeekerList.FindControl("VeteranTypeColumnHeader").Visible = ReportCriteria.ExtraColumns.IsNotNull() && ReportCriteria.ExtraColumns.VeteranType;
      JobSeekerList.FindControl("VeteranTransitionTypeColumnHeader").Visible = ReportCriteria.ExtraColumns.IsNotNull() && ReportCriteria.ExtraColumns.VeteranTransitionType;
      JobSeekerList.FindControl("VeteranMilitaryDischargeColumnHeader").Visible = ReportCriteria.ExtraColumns.IsNotNull() && ReportCriteria.ExtraColumns.VeteranMilitaryDischarge;
      JobSeekerList.FindControl("VeteranBranchOfServiceColumnHeader").Visible = ReportCriteria.ExtraColumns.IsNotNull() && ReportCriteria.ExtraColumns.VeteranBranchOfService;

      JobSeekerList.FindControl("LoginsColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.Logins;
      JobSeekerList.FindControl("PostingsViewedColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.PostingsViewed;
      JobSeekerList.FindControl("ReferralsRequestedColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.ReferralRequests;
      JobSeekerList.FindControl("SelfReferralsColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SelfReferrals;
      JobSeekerList.FindControl("StaffReferralsColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.StaffReferrals;
      JobSeekerList.FindControl("SelfReferralsExternalColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SelfReferralsExternal;
      JobSeekerList.FindControl("StaffReferralsExternalColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.StaffReferralsExternal;
      JobSeekerList.FindControl("TotalReferralsColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.TotalReferrals;
      JobSeekerList.FindControl("ReferralsApprovedColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.ReferralsApproved;
      JobSeekerList.FindControl("TargettingHighGrowthSectorsColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.TargetingHighGrowthSectors;
      JobSeekerList.FindControl("SavedJobAlertsColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SavedJobAlerts;
      JobSeekerList.FindControl("SurveyVerySatisfiedColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SurveyVerySatisfied;
      JobSeekerList.FindControl("SurveySatisfiedColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SurveySatisfied;
      JobSeekerList.FindControl("SurveyDissatisfiedColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SurveyDissatisfied;
      JobSeekerList.FindControl("SurveyVeryDissatisfiedColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SurveyVeryDissatisfied;
      JobSeekerList.FindControl("SurveyNoUnexpectedMatchesColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SurveyNoUnexpectedMatches;
      JobSeekerList.FindControl("SurveyUnexpectedMatchesColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SurveyUnexpectedMatches;
      JobSeekerList.FindControl("SurveyReceivedInvitationsColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SurveyReceivedInvitations;
      JobSeekerList.FindControl("SurveyDidNotReceiveInvitationsColumnHeader").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SurveyDidNotReceiveInvitations;

      JobSeekerList.FindControl("NotesAddedColumnHeader").Visible = ReportCriteria.JobSeekerActivityLogInfo.NotesAdded;
      JobSeekerList.FindControl("AddedToListColumnHeader").Visible = ReportCriteria.JobSeekerActivityLogInfo.AddedToLists;
      JobSeekerList.FindControl("ActivitiesAssignedColumnHeader").Visible = ReportCriteria.JobSeekerActivityLogInfo.ActivitiesAssigned;
      JobSeekerList.FindControl("AssignedToStaffColumnHeader").Visible = ReportCriteria.JobSeekerActivityLogInfo.StaffAssignments;
      JobSeekerList.FindControl("ActivityServicesColumnHeader").Visible = ReportCriteria.JobSeekerActivityLogInfo.ActivityCategory.IsNotNull();
      JobSeekerList.FindControl("FindJobsForSeekerColumnHeader").Visible = ReportCriteria.JobSeekerActivityLogInfo.FindJobsForSeeker; 
      JobSeekerList.FindControl("EmailsSentColumnHeader").Visible = ReportCriteria.JobSeekerActivityLogInfo.EmailsSent;
      JobSeekerList.FindControl("FollowUpIssueColumnHeader").Visible = ReportCriteria.JobSeekerActivityLogInfo.FollowUpIssues;
      JobSeekerList.FindControl("IssuesResolvedColumnHeader").Visible = ReportCriteria.JobSeekerActivityLogInfo.IssuesResolved;

      JobSeekerList.FindControl("HiredColumnHeader").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.Hired;
      JobSeekerList.FindControl("NotHiredColumnHeader").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.NotHired;
      JobSeekerList.FindControl("FailedToApplyColumnHeader").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.FailedToApply;
      JobSeekerList.FindControl("FailedToReportToInterviewColumnHeader").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.FailedToReportToInterview;
      JobSeekerList.FindControl("InterviewDeniedColumnHeader").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.InterviewDenied;
      JobSeekerList.FindControl("InterviewScheduledColumnHeader").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.InterviewScheduled;
      JobSeekerList.FindControl("NewApplicantColumnHeader").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.NewApplicant;
      JobSeekerList.FindControl("RecommendedColumnHeader").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.Recommended;
      JobSeekerList.FindControl("RefusedOfferColumnHeader").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.RefusedOffer;
      JobSeekerList.FindControl("UnderConsiderationColumnHeader").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.UnderConsideration;
      JobSeekerList.FindControl("FailedToReportToJobColumnHeader").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.FailedToReportToJob;
      JobSeekerList.FindControl("FailedToRespondToInvitationColumnHeader").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.FailedToRespondToInvitation;
      JobSeekerList.FindControl("FoundJobFromOtherSourceColumnHeader").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.FoundJobFromOtherSource;
      JobSeekerList.FindControl("JobAlreadyFilledColumnHeader").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.JobAlreadyFilled;
      JobSeekerList.FindControl("NotQualifiedColumnHeader").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.NotQualified;
      JobSeekerList.FindControl("NotYetPlacedColumnHeader").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.NotYetPlaced;
      JobSeekerList.FindControl("RefusedReferralColumnHeader").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.RefusedReferral;
    }

    /// <summary>
    /// Sets the cell visibility.
    /// </summary>
    /// <param name="item">The item.</param>
    private void SetCellVisibility(ListViewItem item)
    {
      item.FindControl("VeteranTypeCell").Visible = ReportCriteria.ExtraColumns.IsNotNull() && ReportCriteria.ExtraColumns.VeteranType;
      item.FindControl("VeteranTransitionTypeCell").Visible = ReportCriteria.ExtraColumns.IsNotNull() && ReportCriteria.ExtraColumns.VeteranTransitionType;
      item.FindControl("VeteranMilitaryDischargeCell").Visible = ReportCriteria.ExtraColumns.IsNotNull() && ReportCriteria.ExtraColumns.VeteranMilitaryDischarge;
      item.FindControl("VeteranBranchOfServiceCell").Visible = ReportCriteria.ExtraColumns.IsNotNull() && ReportCriteria.ExtraColumns.VeteranBranchOfService;

      item.FindControl("LoginsCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.Logins;
      item.FindControl("PostingsViewedCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.PostingsViewed;
      item.FindControl("ReferralsRequestedCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.ReferralRequests;
      item.FindControl("SelfReferralsCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SelfReferrals;
      item.FindControl("StaffReferralsCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.StaffReferrals;
      item.FindControl("SelfReferralsExternalCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SelfReferralsExternal;
      item.FindControl("StaffReferralsExternalCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.StaffReferralsExternal;
      item.FindControl("TotalReferralsCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.TotalReferrals;
      item.FindControl("ReferralsApprovedCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.ReferralsApproved;
      item.FindControl("TargettingHighGrowthSectorsCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.TargetingHighGrowthSectors;
      item.FindControl("SavedJobAlertsCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SavedJobAlerts;
      item.FindControl("SurveyVerySatisfiedCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SurveyVerySatisfied;
      item.FindControl("SurveySatisfiedCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SurveySatisfied;
      item.FindControl("SurveyDissatisfiedCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SurveyDissatisfied;
      item.FindControl("SurveyVeryDissatisfiedCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SurveyVeryDissatisfied;
      item.FindControl("SurveyNoUnexpectedMatchesCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SurveyNoUnexpectedMatches;
      item.FindControl("SurveyUnexpectedMatchesCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SurveyUnexpectedMatches;
      item.FindControl("SurveyReceivedInvitationsCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SurveyReceivedInvitations;
      item.FindControl("SurveyDidNotReceiveInvitationsCell").Visible = ReportCriteria.JobSeekerActivitySummaryInfo.SurveyDidNotReceiveInvitations;

      item.FindControl("NotesAddedCell").Visible = ReportCriteria.JobSeekerActivityLogInfo.NotesAdded;
      item.FindControl("AddedToListCell").Visible = ReportCriteria.JobSeekerActivityLogInfo.AddedToLists;
      item.FindControl("ActivitiesAssignedCell").Visible = ReportCriteria.JobSeekerActivityLogInfo.ActivitiesAssigned;
      item.FindControl("AssignedToStaffCell").Visible = ReportCriteria.JobSeekerActivityLogInfo.StaffAssignments;
      item.FindControl("ActivityServicesCell").Visible = ReportCriteria.JobSeekerActivityLogInfo.ActivityCategory.IsNotNull();
      item.FindControl("FindJobsForSeekerCell").Visible = ReportCriteria.JobSeekerActivityLogInfo.FindJobsForSeeker;
      item.FindControl("EmailsSentCell").Visible = ReportCriteria.JobSeekerActivityLogInfo.EmailsSent;
      item.FindControl("FollowUpIssueCell").Visible = ReportCriteria.JobSeekerActivityLogInfo.FollowUpIssues;
      item.FindControl("IssuesResolvedCell").Visible = ReportCriteria.JobSeekerActivityLogInfo.IssuesResolved;

      item.FindControl("HiredCell").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.Hired;
      item.FindControl("NotHiredCell").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.NotHired;
      item.FindControl("FailedToApplyCell").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.FailedToApply;
      item.FindControl("FailedToReportToInterviewCell").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.FailedToReportToInterview;
      item.FindControl("InterviewDeniedCell").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.InterviewDenied;
      item.FindControl("InterviewScheduledCell").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.InterviewScheduled;
      item.FindControl("NewApplicantCell").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.NewApplicant;
      item.FindControl("RecommendedCell").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.Recommended;
      item.FindControl("RefusedOfferCell").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.RefusedOffer;
      item.FindControl("UnderConsiderationCell").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.UnderConsideration;
      item.FindControl("FailedToReportToJobCell").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.FailedToReportToJob;
      item.FindControl("FailedToRespondToInvitationCell").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.FailedToRespondToInvitation;
      item.FindControl("FoundJobFromOtherSourceCell").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.FoundJobFromOtherSource;
      item.FindControl("JobAlreadyFilledCell").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.JobAlreadyFilled;
      item.FindControl("NotQualifiedCell").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.NotQualified;
      item.FindControl("NotYetPlacedCell").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.NotYetPlaced;
      item.FindControl("RefusedReferralCell").Visible = ReportCriteria.JobSeekerReferralOutcomesInfo.RefusedReferral;
    }

    #endregion
    
    #region page generated events

    public delegate void ReportCriteriaChangedHandler(object sender, CommandEventArgs eventArgs);
    public event ReportCriteriaChangedHandler ReportCriteriaChanged;

    /// <summary>
    /// Raises the <see cref="CommandEventArgs" /> event.
    /// </summary>
    /// <param name="eventargs">The <see cref="CommandEventArgs" /> instance containing the event data.</param>
    public void OnReportCriteriaChanged(CommandEventArgs eventargs)
    {
      var handler = ReportCriteriaChanged;
      if (handler != null) handler(this, eventargs);
    }

    #endregion
	}
}