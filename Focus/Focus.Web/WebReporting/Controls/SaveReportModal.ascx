﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SaveReportModal.ascx.cs" Inherits="Focus.Web.WebReporting.Controls.SaveReportModal" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagName="ConfirmationModal" tagPrefix="uc" %>

<asp:HiddenField ID="SaveReportModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="SaveReportModalExtender" runat="server" 
                        BehaviorID="SaveReportModalExtender"
												TargetControlID="SaveReportModalDummyTarget"
												PopupControlID="SaveReportPanel" 
												PopupDragHandleControlID="SaveReportPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="SaveReportPanel" runat="server" CssClass="modal" Width="325px" ClientIDMode="Static" >
	<div style="float:left; width: 90%">
		<table style="width: 100%">
			<tr>
				<th colspan="2">
				  <%= HtmlLocalise("SaveReport.Header.Text", "Save report")%>
				</th>
			</tr>
			<tr>
				<td style="vertical-align: top">
				  <%= HtmlLocalise("SaveReport.ReportName.Label", "Report name")%>&nbsp;
				</td>
        <td>
          <asp:TextBox ID="ReportNameTextBox" runat="server" ClientIDMode="Static" MaxLength="100" Width="100%" />
          <br/>
          <asp:RequiredFieldValidator ID="ReportNameRequired" runat="server" ControlToValidate="ReportNameTextBox" CssClass="error" ValidationGroup="SaveReportValidation"/>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: top">
          <%= HtmlLocalise("SaveReport.Addtodashboard.Label", "Add to dashboard?")%>&nbsp;
        </td>
        <td>
          <asp:CheckBox runat="server" ID="AddToDashboardCheckbox"/>
        </td>
      </tr>
      <tr>
        <td style="text-align: right" colspan="2">
          <asp:Button ID="SaveReportButton" runat="server" CssClass="button2" Text="Save" onclick="SaveReportButton_Click" CausesValidation="True" ValidationGroup="SaveReportValidation" />
        </td>
      </tr>
		</table>
	</div>
	<div class="closeIcon"><img src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('SaveReportModalExtender').hide();" /></div>
</asp:Panel>
<uc:confirmationmodal runat="server" ID="SaveConfirmation" Visible="False" />