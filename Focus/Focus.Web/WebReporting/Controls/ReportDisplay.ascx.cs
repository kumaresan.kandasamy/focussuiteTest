﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Report;

using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class ReportDisplay : UserControlBase
	{
    public ReportType ReportType
    {
      get { return GetViewStateValue<ReportType>("ReportDisplay:ReportType"); }
      set { SetViewStateValue("ReportDisplay:ReportType", value); }
    }

    public IReportCriteria ReportCriteria
    {
      get
      {
        if (BarChartDisplay.Visible)
          return BarChartDisplay.ReportCriteria;

        if (TotalsDisplay.Visible)
          return TotalsDisplay.ReportCriteria;

        switch (ReportType)
        {
          case ReportType.JobSeeker:
            return JobSeekerReport.ReportCriteria;
          case ReportType.JobOrder:
            return JobOrderReport.ReportCriteria;
          case ReportType.Employer:
            return EmployerReportList.ReportCriteria;
          case ReportType.SupplyDemand:
            return SupplyDemandReportList.ReportCriteria;
          default:
            throw new Exception("Unknown criteria");
        }
      }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
			Page.Title = CodeLocalise("PageTitle", "Reporting");

	    if (Page.RouteData.Values.ContainsKey("savedreportid"))
	    {
		    long id;
		    Int64.TryParse(Page.RouteData.Values["savedreportid"].ToString(), out id);

				Page.Title = string.Format("{0} - {1}",CodeLocalise("PageTitle", "Reporting"),id);
	    }

	    if (!Page.IsPostBack)
      {
        Localise();
        Bind();
      }
    }

    /// <summary>
    /// Shows the job seeker report.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    public void ShowReport(IReportCriteria criteria)
    {
      Visible = true;
      ReportType = criteria.ReportType;

      switch (criteria.ReportType)
      {
          case ReportType.JobSeeker:
            DrawJobSeekerReport(criteria);
          break;
          case ReportType.JobOrder:
            DrawJobOrderReport(criteria);
          break;
          case ReportType.Employer:
            DrawEmployerReport(criteria);
          break;
          case ReportType.SupplyDemand:
          DrawSupplyDemandReport(criteria);
          break;
      }
    }

    private void Bind()
    {
      ExportDropDownList.Items.Add(new ListItem(CodeLocalise("ExportTo.Text.NoEdit", "Export to"), ""));
      ExportDropDownList.Items.AddEnum(ReportExportType.Excel, CodeLocalise("Excel.Text", "Excel"));
      ExportDropDownList.Items.AddEnum(ReportExportType.PDF, CodeLocalise("PDF.Text", "Pdf"));
    }

    /// <summary>
    /// Localises the text on the page
    /// </summary>
    private void Localise()
    {
      AddToDashboardButton.Text = CodeLocalise("AddToDashboard.Text.NoEdit", "Add to dashboard");
      SaveReportButton.Text = CodeLocalise("SaveReportButton.Text.NoEdit", "Save");
      ExportButton.Text = HtmlLocalise("ExportButton.Text.NoEdit", "Export");
      ExportValidator.ErrorMessage = CodeLocalise("ExportValidator.Message", "Please select an export format");
    }

    private void HideAllReports()
    {
      JobSeekerReport.Visible =
        JobOrderReport.Visible =
        EmployerReportList.Visible =
        SupplyDemandReportList.Visible = BarChartDisplay.Visible = TotalsDisplay.Visible = false;
    }

    #region Events

    /// <summary>
    /// Fires when the export button is clicked to export the report to Excel or PDF
    /// </summary>
    /// <param name="sender">Button raising the event</param>
    /// <param name="e">Standard event arguments</param>
    protected void ExportButton_Clicked(object sender, EventArgs e)
    {
      if (ExportDropDownList.SelectedIndex > 0)
      {
        var exportType = ExportDropDownList.SelectedValueToEnum<ReportExportType>();
        var reportName = ReportTitleLiteral.Text;

        var reportBytes = ServiceClientLocator.ReportClient(App).GetReportExportBytes(ReportCriteria, reportName, exportType);

        reportName = ReportTitleLiteral.Text.Replace(" ", "_");

        var fileExtension = (exportType == ReportExportType.Excel) ? "xlsx" : "pdf";
        var mimeType = (exportType == ReportExportType.Excel) ? "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" : "application/pdf";
        Response.Clear();
        Response.AddHeader("Content-Type", mimeType);
        Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.{1}; size={2}", reportName, fileExtension, reportBytes.Length));
        Response.Flush();
        Response.BinaryWrite(reportBytes);
        Response.Flush();
        Response.End();
      }
    }

    /// <summary>
    /// Fires when the Add to Dashboard button is clicked to show the Save Report modal
    /// </summary>
    /// <param name="sender">Button raising the event</param>
    /// <param name="e">Standard event arguments</param>
    protected void AddToDashboardButton_Clicked(object sender, EventArgs e)
    {
      SaveReportModal.Visible = true;
      SaveReportModal.Show(true);
    }

    protected void ReportEntities_ReportCriteriaChanged(object sender, CommandEventArgs eventargs)
    {
      OnReportCriteriaChanged(eventargs);
    }

    /// <summary>
    /// Fires when the save button is clicked to show the Save Report modal
    /// </summary>
    /// <param name="sender">Button raising the event</param>
    /// <param name="e">Standard event arguments</param>
    protected void SaveReportButton_Clicked(object sender, EventArgs e)
    {
      SaveReportModal.Visible = true;
      SaveReportModal.Show();
    }

    /// <summary>
    /// Fires when the report is to be saved to get the criteria
    /// </summary>
    /// <param name="sender">Save Report Modal raising the event</param>
    /// <param name="eventargs">Save arguments</param>
    protected IReportCriteria SaveReportModal_GetCriteriaForSaveEvent(object sender, EventArgs eventargs)
    {
      return ReportCriteria;
    }

    #endregion

    #region Job Seeker

    /// <summary>
    /// Renders the job seeker report on the page
    /// </summary>
    /// <param name="criteria">The report criteria</param>
    private void DrawJobSeekerReport(IReportCriteria criteria)
    {
      HideAllReports();

      ExportPlaceHolder.Visible = App.User.IsInRole(Constants.RoleKeys.AssistJobSeekerReports);

      ReportTitleLiteral.Text = criteria.ReportTitle.IsNotNullOrEmpty() ? criteria.ReportTitle : CodeLocalise("JobSeekerReportTitle.Text", "Job seeker report");
      if (criteria.DisplayType == ReportDisplayType.Table)
      {
        var jobSeekerCriteria = (JobSeekersReportCriteria) criteria;
        if (jobSeekerCriteria.JobSeekerActivitySummaryInfo.IsNotNull() && jobSeekerCriteria.JobSeekerActivitySummaryInfo.TotalCount)
          TotalsDisplay.Bind(jobSeekerCriteria);
        else
          JobSeekerReport.Bind(jobSeekerCriteria);
      }
      else
      {
        BarChartDisplay.Bind(criteria);
      }
    }

    #endregion

    #region Job order

    /// <summary>
    /// Renders the job order report on the page
    /// </summary>
    /// <param name="criteria">The report criteria</param>
    private void DrawJobOrderReport(IReportCriteria criteria)
    {
      HideAllReports();

      ExportPlaceHolder.Visible = App.User.IsInRole(Constants.RoleKeys.AssistJobOrderReports);

			ReportTitleLiteral.Text = criteria.ReportTitle.IsNotNullOrEmpty() ? criteria.ReportTitle : CodeLocalise("JobOrderReportTitle.Text", "#EMPLOYMENTTYPE# report");

      if (criteria.DisplayType == ReportDisplayType.Table)
        JobOrderReport.Bind((JobOrdersReportCriteria)criteria);
      else
        BarChartDisplay.Bind(criteria);
      
    }
    
    #endregion

    #region employer

    /// <summary>
    /// Renders the employer report on the page
    /// </summary>
    /// <param name="criteria">The report criteria</param>
    private void DrawEmployerReport(IReportCriteria criteria)
    {
      HideAllReports();

      ExportPlaceHolder.Visible = App.User.IsInRole(Constants.RoleKeys.AssistEmployerReports);

      ReportTitleLiteral.Text = criteria.ReportTitle.IsNotNullOrEmpty() ? criteria.ReportTitle : CodeLocalise("EmployerReportTitle.Text", "#BUSINESS# report");

      if (criteria.DisplayType == ReportDisplayType.Table)
        EmployerReportList.Bind((EmployersReportCriteria)criteria);
      else
        BarChartDisplay.Bind(criteria);
    }

    #endregion

    #region Supply Demand

    /// <summary>
    /// Renders the supply demand report on the page
    /// </summary>
    /// <param name="criteria">The report criteria</param>
    private void DrawSupplyDemandReport(IReportCriteria criteria)
    {
      HideAllReports();

      ExportPlaceHolder.Visible = true;

      ReportTitleLiteral.Text = criteria.ReportTitle.IsNotNullOrEmpty() ? criteria.ReportTitle : CodeLocalise("SupplyDemandReportTitle.Text", "Supply and demand report");

      if (criteria.DisplayType == ReportDisplayType.Table)
        SupplyDemandReportList.Bind((SupplyDemandReportCriteria)criteria);
      else
        BarChartDisplay.Bind(criteria);


    }

    #endregion

    #region page generated events

    public delegate void ReportCriteriaChangedHandler(object sender, CommandEventArgs eventArgs);
    public event ReportCriteriaChangedHandler ReportCriteriaChanged;

    /// <summary>
    /// Raises the <see cref="CommandEventArgs" /> event.
    /// </summary>
    /// <param name="eventargs">The <see cref="CommandEventArgs" /> instance containing the event data.</param>
    public void OnReportCriteriaChanged(CommandEventArgs eventargs)
    {
      var handler = ReportCriteriaChanged;
      if (handler != null) handler(this, eventargs);
    }

    #endregion
  }
}