﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaSelector.ascx.cs" Inherits="Focus.Web.WebReporting.Controls.CriteriaSelector" %>
<%@ Register Src="CriteriaJobSeekerStatus.ascx" TagName="CriteriaJobSeekerStatus" TagPrefix="uc" %>
<%@ Register Src="CriteriaVeteranStatus.ascx" TagName="CriteriaVeteranStatus" TagPrefix="uc" %>
<%@ Register Src="CriteriaJobSeekerCharacteristics.ascx" TagName="CriteriaJobSeekerCharacteristics" TagPrefix="uc" %>
<%@ Register Src="CriteriaJobSeekerQualifications.ascx" TagName="CriteriaJobSeekerQualifications" TagPrefix="uc" %>
<%@ Register Src="CriteriaOccupationAndSalary.ascx" TagName="CriteriaOccupationAndSalary" TagPrefix="uc" %>
<%@ Register Src="CriteriaKeywordAndContext.ascx" TagName="CriteriaKeywordAndContext" TagPrefix="uc" %>
<%@ Register Src="CriteriaResume.ascx" TagName="CriteriaResume" TagPrefix="uc" %>
<%@ Register Src="CriteriaJobSeekerActivitySummary.ascx" TagName="CriteriaJobSeekerActivitySummary" TagPrefix="uc" %>
<%@ Register Src="CriteriaJobSeekerActivityLog.ascx" TagName="CriteriaJobSeekerActivityLog" TagPrefix="uc" %>
<%@ Register Src="CriteriaReferralsOutcome.ascx" TagName="CriteriaReferralsOutcome" TagPrefix="uc" %>
<%@ Register Src="CriteriaLocation.ascx" TagName="CriteriaLocation" TagPrefix="uc" %>
<%@ Register Src="CriteriaSupplyDemandLocation.ascx" TagName="CriteriaSupplyDemandLocation" TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="CriteriaDateRange" Src="CriteriaDateRange.ascx" %>
<%@ Register Src="CriteriaJobCharacteristics.ascx" TagName="CriteriaJobCharacteristics" TagPrefix="uc" %>
<%@ Register Src="CriteriaEmployerCharacteristics.ascx" TagName="CriteriaEmployerCharacteristics" TagPrefix="uc" %>
<%@ Register src="CriteriaIndividualJobSeekerCharacteristics.ascx" tagName="CriteriaIndividualJobSeekerCharacteristics" tagPrefix="uc" %>
<%@ Register Src="CriteriaCompliance.ascx" TagName="CriteriaCompliance" TagPrefix="uc" %>
<%@ Register Src="CriteriaJobActivitySummary.ascx" TagName="CriteriaJobActivitySummary" TagPrefix="uc" %>
<%@ Register Src="CriteriaEmployerActivity.ascx" TagName="CriteriaEmployerActivity" TagPrefix="uc" %>
<%@ Register Src="CriteriaNcrc.ascx" TagName="CriteriaNcrc" TagPrefix="uc" %>
<%@ Register Src="CriteriaJobSeekerNcrc.ascx" TagName="CriteriaJobSeekerNcrc" TagPrefix="uc" %>
<%@ Register Src="CriteriaOccupation.ascx" TagName="CriteriaOccupation" TagPrefix="uc" %>
<%@ Register Src="CriteriaIndustries.ascx" TagName="CriteriaIndustries" TagPrefix="uc" %>
<asp:HiddenField ID="CriteriaSelectorDummyTarget" runat="server" />
<act:ModalPopupExtender ID="CriteriaSelectorPopup" runat="server" TargetControlID="CriteriaSelectorDummyTarget" BehaviorID="CriteriaSelectorPopup" PopupControlID="CriteriaSelectorPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
<asp:Panel ID="CriteriaSelectorPanel" runat="server" CssClass="modal lightbox" Style="display: none; min-width: 1000px; overflow-y: auto" Width="90%">
	<div class="grid100" style="height: 16px">
		<div class="closeIcon">
			<asp:Literal ID="LocaliseCloseButton" runat="server" /><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text.NoEdit", "Close") %>" onclick="$find('CriteriaSelectorPopup').hide(); return false;" /></div>
	</div>
	<div class="grid100">
		<div class="reportgrid25">
			<h2>
				<asp:Literal runat="server" ID="CriteriaHeader" Text="Report Criteria"></asp:Literal>
			</h2>
		</div>
		<div class="reportgrid50">
			<table role="presentation">
				<tr>
					<td class="reportTableCell">
						<asp:Literal runat="server" ID="Column4Header" />
					</td>
					<td class="reportTableCell">
						<asp:RadioButtonList runat="server" ID="ReportDisplayTypeList" RepeatDirection="Vertical" role="presentation">
						</asp:RadioButtonList>
					</td>
					<td class="reportTableCell">
						<asp:Literal runat="server" ID="GroupedByLiteral" />&nbsp;
						<focus:LocalisedLabel runat="server" ID="GroupByDropDownListLabel" AssociatedControlID="GroupByDropDownList" LocalisationKey="GroupBy" DefaultText="Group by" CssClass="sr-only"/>
						<asp:DropDownList runat="server" ID="GroupByDropDownList" ></asp:DropDownList>
					</td>
				</tr>
			</table>
		</div>
		<div class="reportgrid24">
			<asp:Button runat="server" CssClass="button3 right" ID="RunReportButton" OnClick="RunReportButton_OnClick" ValidationGroup="SendMessagesGroup" CausesValidation="True" />
		</div>
	</div>
	<asp:ValidationSummary runat="server" ID="CriteriaValidationSummary" CssClass="error" DisplayMode="BulletList" />
	<br />
	<hr />
	<br />
	<div class="reportgrid33">
		<div class="reportgrid95">
			<div class="criteria-selector-column-header">
				<asp:Literal runat="server" ID="Column1Header" />
			</div>
			<uc:CriteriaJobSeekerStatus ID="CriteriaJobSeekerStatus" runat="server" Visible="False" />
			<uc:CriteriaVeteranStatus ID="CriteriaVeteranStatus" runat="server" Visible="False" />
			<uc:CriteriaJobSeekerCharacteristics ID="CriteriaJobSeekerCharacteristics" runat="server" Visible="False" />
			<uc:CriteriaJobSeekerQualifications ID="CriteriaJobSeekerQualifications" runat="server" Visible="False" />
			<uc:CriteriaJobSeekerNcrc ID="CriteriaJobSeekerNcrc" runat="server" Visible="False" />
			<uc:CriteriaOccupationAndSalary ID="CriteriaOccupationAndSalary" runat="server" Visible="False" />
			<uc:CriteriaOccupation ID="CriteriaOccupation" runat="server" Visible="False" />
			<uc:CriteriaIndustries ID="CriteriaIndustries" runat="server" Visible="False" />
			<uc:CriteriaKeywordAndContext ID="CriteriaKeywordAndContext" runat="server" Visible="False" />
			<uc:CriteriaResume runat="server" ID="CriteriaResume" Visible="False" />
			<uc:CriteriaJobCharacteristics runat="server" ID="CriteriaJobCharacteristics" Visible="False" />
			<uc:CriteriaEmployerCharacteristics runat="server" ID="CriteriaEmployerCharacteristics" Visible="False" />
			<uc:CriteriaCompliance runat="server" ID="CriteriaCompliance" Visible="False" />
			<uc:CriteriaNcrc runat="server" ID="CriteraNcrc" Visible="False" />
      <uc:CriteriaIndividualJobSeekerCharacteristics runat="server" ID="CriteriaIndividualJobSeekerCharacteristics" Visible="False"/>
		</div>
	</div>
	<div class="reportgrid33">
		<div class="reportgrid95">
			<div class="criteria-selector-column-header">
				<asp:Literal runat="server" ID="Column2Header" />
			</div>
			<uc:CriteriaLocation runat="server" ID="Column2CriteriaLocation" Visible="False" />
			<uc:CriteriaSupplyDemandLocation runat="server" ID="Column2CriteriaSupplyDemandLocation" Visible="False" />
			<uc:CriteriaDateRange runat="server" ID="Column2CriteriaDateRange" Visible="False" />
			<uc:CriteriaJobSeekerActivitySummary runat="server" ID="CriteriaJobSeekerActivitySummary" Visible="False" ShowJobSeekerAccountsCritria="True" />
			<uc:CriteriaJobSeekerActivityLog runat="server" ID="CriteriaJobSeekerActivityLog" ShowActivitiesAndServices="True" Visible="False" />
			<uc:CriteriaJobActivitySummary ID="CriteriaJobActivitySummary" runat="server" Visible="False" />
			<uc:CriteriaEmployerActivity ID="CriteriaEmployerActivity" runat="server" Visible="False" />
			<uc:CriteriaReferralsOutcome runat="server" ID="CriteriaReferralsOutcome" Visible="False" />
		</div>
	</div>
	<asp:Panel ID="Column3Panel" CssClass="reportgrid33" runat="server">
		<div class="reportgrid95">
			<div class="criteria-selector-column-header">
				<asp:Literal runat="server" ID="Column3Header" />
			</div>
			<uc:CriteriaLocation runat="server" ID="Column3CriteriaLocation" Visible="False" />
			<uc:CriteriaDateRange runat="server" ID="Column3CriteriaDateRange" Visible="False" />
		</div>
	</asp:Panel>
</asp:Panel>
<script type="text/javascript">

	$(document).ready(function() {
		$('td').delegate('.deleteItem', 'click', function(e) {
			RemoveListItem($(this));
			e.stopPropagation();
		});
	});

    $("input[name='<%=ReportDisplayTypeList.UniqueID %>']").change(function () {
//      if ($("input[name='<%=ReportDisplayTypeList.UniqueID %>']:radio:checked").val() == "Table") {
//        $("#<%=GroupByDropDownList.ClientID %>").prop('disabled', true);
//      } else {
//        $("#<%=GroupByDropDownList.ClientID %>").prop('disabled', false);
//      }
//    });
//    $("input[name='<%=ReportDisplayTypeList.UniqueID %>']").change();
  });

  function CriteriaSelector_FixModal() {
    $("#<%=CriteriaSelectorPanel.ClientID %>").css("position", "absolute");
  }

  Sys.Application.add_load(CriteriaSelector_FixModal);
</script>
