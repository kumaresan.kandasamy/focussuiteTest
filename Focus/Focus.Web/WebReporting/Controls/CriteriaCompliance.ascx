﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaCompliance.ascx.cs" Inherits="Focus.Web.WebReporting.Controls.CriteriaCompliance" %>
<table role="presentation" style="width:100%;" class="accordionWithIcon">
    <tr class="multipleAccordionTitle">
        <td style="width:10%; vertical-align:top;">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td style="width:90%;">
            <asp:Literal runat="server" ID="CriteriaComplianceLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
            <table role="presentation" style="width:100%;">
                <tr>
                    <td>
                        <strong><asp:Literal runat="server" ID="ForiegnLabourCertificationHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                  <td><asp:CheckBoxList runat="server" ID="ForeignLaborTypesCheckBoxList"/></td>
                </tr>
            </table>
            <table role="presentation" style="width:100%;">
                <tr>
                    <td>
                        <strong><asp:Literal runat="server" ID="ForiegnLabourCertificationMatchesHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="NonForeignLabourCertificationCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="CourtOrderedAffirmativeActionCheckbox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="FederalContractJobsCheckbox"/></td>
                </tr>
            </table>
        </td>
    </tr>
</table>