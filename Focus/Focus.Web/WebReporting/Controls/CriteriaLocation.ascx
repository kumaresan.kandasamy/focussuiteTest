﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaLocation.ascx.cs"
    Inherits="Focus.Web.WebReporting.Controls.CriteriaLocation" %>
<%@ Import Namespace="Focus" %>
<table role="presentation" style="width:100%;" class="accordionWithIcon">
    <tr class="multipleAccordionTitle" id="CriteriaLocationHeader">
        <td style="vertical-align:top; width:10%;">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td style="width:90%;">
            <asp:Literal runat="server" ID="CriteriaLocationLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
            <asp:PlaceHolder id="CountyControls" runat="server" Visible="False">
              <table role="presentation" style="width:100%;">
                  <tr>
                      <td colspan="2">
                          <strong>
                              <asp:Literal runat="server" ID="CountyHeader"></asp:Literal></strong>
                      </td>
                  </tr>
                  <tr>
                      <td>
	                      <div style="position: relative;">
		                      <focus:LocalisedLabel runat="server" ID="CountyTextBoxLabel" AssociatedControlID="CountyTextBox" LocalisationKey="County" DefaultText="County" CssClass="sr-only"/>
                          <asp:TextBox runat="server" ID="CountyTextBox" AutoCompleteType="Disabled"></asp:TextBox>
													<div style="position:absolute; width:100%;">
														<act:AutoCompleteExtender ID="CountyAutoCompleteExtender" runat="server" TargetControlID="CountyTextBox" MinimumPrefixLength="2" CompletionInterval="100" ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetAllCounties"
															CompletionListCssClass="autocomplete_completionListElement" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" CompletionListItemCssClass="autocomplete_listItem" CompletionListElementID="CountyAutoCompleteExtenderCompletionList" />
														<div id="CountyAutoCompleteExtenderCompletionList" class="autocompleteCompletionList" style="height:180px;min-width:100px"></div>
													</div>
												</div>
                      </td>
                      <td style="text-align: right;">
                          <input type="button" class="button3" runat="server" id="CountyButton" name="CountyButton" />
                      </td>
                  </tr>
                    <tr>
                        <td colspan="2"><table role="presentation" id="CountyTable" style="width:75%;"><tbody></tbody></table><asp:HiddenField runat="server" ID="HiddenCountyList"/></td>
                    </tr>
                </table>
            </asp:PlaceHolder>
            <asp:PlaceHolder id="OfficeControls" runat="server" Visible="False">
            <table role="presentation" style="width:100%;">
              <tr>
                  <td colspan="2">
                      <strong>
                          <asp:Literal runat="server" ID="OfficeHeader"></asp:Literal></strong>
                  </td>
              </tr>
              <tr>
                  <td>
											<focus:LocalisedLabel runat="server" ID="OfficeDropDownLabel" AssociatedControlID="OfficeDropDown" LocalisationKey="Offices" DefaultText="Offices" CssClass="sr-only"/>
                      <asp:DropDownList runat="server" ID="OfficeDropDown" />
                  </td>
                  <td style="text-align: right;">
                      <input type="button" class="button3" runat="server" id="OfficeButton" name="OfficeButton" />
                  </td>
              </tr>
                  <tr>
                      <td colspan="2"><table role="presentation" id="OfficeTable" style="width:75%;"><tbody></tbody></table><asp:HiddenField runat="server" ID="HiddenOfficeList"/></td>
                  </tr>
            </table>
            </asp:PlaceHolder>
            <table role="presentation" style="width:100%;">
                <tr>
                    <td>
                        <strong>
                            <asp:Literal runat="server" ID="DistanceFromHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td style="white-space:nowrap;">
												<focus:LocalisedLabel runat="server" ID="RadiusDropDownLabel" AssociatedControlID="RadiusDropDown" LocalisationKey="Radius" DefaultText="Radius" CssClass="sr-only"/>
                        <asp:DropDownList runat="server" ID="RadiusDropDown"/>
                        &nbsp;<%=HtmlLocalise("MilesFrom", "miles from") %>&nbsp;
                        <div style="display: inline-block">
                          <%= HtmlInFieldLabel("ZipCodeTextBox", "ZipCodeTextBox.Label", "zip code", 55)%>
                          <asp:TextBox runat="server" ID="ZipCodeTextBox" ClientIDMode="Static" size="5" Width="55px" AutoCompleteType="Disabled" />
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<script type="text/javascript">
    $(document).ready(function () {
        $("#<%=CountyButton.ClientID%>").click(function () {
          AddListItem('<%=CountyTextBox.ClientID %>', '<%=HiddenCountyList.ClientID %>', 'CountyTable');
        });
        $("#<%=OfficeButton.ClientID%>").click(function () {
          AddListItem('<%=OfficeDropDown.ClientID %>', '<%=HiddenOfficeList.ClientID %>', 'OfficeTable');
        });
        BuildTableFromScratch('<%=HiddenCountyList.ClientID %>', 'CountyTable');
        BuildTableFromScratch('<%=HiddenOfficeList.ClientID %>', 'OfficeTable');
        $("#ZipCodeTextBox").mask("<%= OldApp_RefactorIfFound.Settings.PostalCodeMaskPattern %>", { placeholder: " " });
    });
</script>