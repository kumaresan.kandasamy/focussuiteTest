﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Core.Criteria.Report;
using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaApplicantSummary : UserControlBase
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (Page.IsPostBack)
        Localise();
    }

    /// <summary>
    /// Resets this instance.
    /// </summary>
    private void Reset()
    {
      InterviewCheckBox.Checked =
        FailedToShowCheckBox.Checked =
        DeniedInterviewCheckBox.Checked =
        HiredCheckBox.Checked =
        RejectedCheckBox.Checked =
        JobOfferRefusedCheckBox.Checked = 
        DidNotApplyCheckBox.Checked = false;
    }

    /// <summary>
    /// Unbinds this instance.
    /// </summary>
    /// <returns></returns>
    public JobOrdersApplicantSummaryCriteria Unbind(JobOrdersApplicantSummaryCriteria criteria = null)
    {
      if (criteria.IsNull()) criteria = new JobOrdersApplicantSummaryCriteria();

      criteria.ApplicantsInterviewed = InterviewCheckBox.Checked;
      criteria.ApplicantsFailedToShow = FailedToShowCheckBox.Checked;
      criteria.ApplicantsDeniedInterviews = DeniedInterviewCheckBox.Checked;
      criteria.ApplicantsHired = HiredCheckBox.Checked;
      criteria.ApplicantsRejected = RejectedCheckBox.Checked;
      criteria.JobOffersRefused = JobOfferRefusedCheckBox.Checked;
      criteria.ApplicantsDidNotApply = DidNotApplyCheckBox.Checked;

      return criteria;
    }

    /// <summary>
    /// Binds the controls to the criteria
    /// </summary>
    /// <param name="criteria">The criteria to which to bind</param>
    public void BindCriteria(JobOrdersApplicantSummaryCriteria criteria)
    {
      Reset();

      if (criteria.IsNull()) return;

      InterviewCheckBox.Checked = criteria.ApplicantsInterviewed;
      FailedToShowCheckBox.Checked = criteria.ApplicantsFailedToShow;
      DeniedInterviewCheckBox.Checked = criteria.ApplicantsDeniedInterviews;
      HiredCheckBox.Checked = criteria.ApplicantsHired;
      RejectedCheckBox.Checked = criteria.ApplicantsRejected;
      JobOfferRefusedCheckBox.Checked = criteria.JobOffersRefused;
      DidNotApplyCheckBox.Checked = criteria.ApplicantsDidNotApply;
    }

    private void Localise()
    {
      ApplicantSummaryLabel.Text = CodeLocalise("ApplicantSummaryLabel.Text", "Applicant summary");
      OutcomesStatusHeader.Text = CodeLocalise("OutcomesStatusHeader.Text", "Outcomes");

      InterviewCheckBox.Text = CodeLocalise("InterviewCheckBox.Text", "Interviewed/pending interviews");
      FailedToShowCheckBox.Text = CodeLocalise("FailedToShowCheckBox.Text", "Failed to show");
      DeniedInterviewCheckBox.Text = CodeLocalise("DeniedInterviewCheckBox.Text", "Denied interviews");
      HiredCheckBox.Text = CodeLocalise("HiredCheckBox.Text", "Hired");
      RejectedCheckBox.Text = CodeLocalise("RejectedCheckBox.Text", "Rejected");
      JobOfferRefusedCheckBox.Text = CodeLocalise("JobOfferRefusedCheckBox.Text", "Job offers refused");
      DidNotApplyCheckBox.Text = CodeLocalise("DidNotApplyCheckBox.Text", "Did not apply");
    }
  }
}