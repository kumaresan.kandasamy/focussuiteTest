﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core;
using Focus.Core.Criteria.Report;

using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaEmployerActivity : UserControlBase
	{
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
        Localise();
    }

    /// <summary>
    /// Unbinds the specified criteria from the control
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <returns></returns>
    public EmployersActivityCriteria Unbind(EmployersActivityCriteria criteria = null)
    {
      if (criteria.IsNull()) criteria = new EmployersActivityCriteria();

      criteria.JobOrdersEdited = JobOrdersEditedCheckBox.Checked;
      criteria.JobSeekersInterviewed = JobSeekersInterviewedCheckBox.Checked;
      criteria.JobSeekersHired = JobSeekersHiredCheckBox.Checked;
      criteria.JobOrdersCreated = JobOrdersCreatedCheckBox.Checked;
      criteria.JobOrdersPosted = JobOrdersPostedCheckBox.Checked;
      criteria.JobOrdersPutOnHold = JobOrdersPutOnHoldCheckBox.Checked;
      criteria.JobOrdersRefreshed = JobOrdersRefreshedCheckBox.Checked;
      criteria.JobOrdersClosed = JobOrdersClosedCheckBox.Checked;
      criteria.InvitationsSent = InvitationsSentCheckBox.Checked;
      criteria.JobSeekersNotHired = JobSeekersNotHiredCheckBox.Checked;
      criteria.SelfReferrals = SelfReferralsCheckBox.Checked;
      criteria.StaffReferrals = StaffReferralsCheckBox.Checked;

      return criteria;
    }

    /// <summary>
    /// Binds the criteria to the controls
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    public void BindCriteria(EmployersActivityCriteria criteria)
    {
      Reset();
      if (criteria.IsNull()) return;

      JobOrdersEditedCheckBox.Checked = criteria.JobOrdersEdited;
      JobSeekersInterviewedCheckBox.Checked = criteria.JobSeekersInterviewed;
      JobSeekersHiredCheckBox.Checked = criteria.JobSeekersHired;
      JobOrdersCreatedCheckBox.Checked = criteria.JobOrdersCreated;
      JobOrdersPostedCheckBox.Checked = criteria.JobOrdersPosted;
      JobOrdersPutOnHoldCheckBox.Checked = criteria.JobOrdersPutOnHold;
      JobOrdersRefreshedCheckBox.Checked = criteria.JobOrdersRefreshed;
      JobOrdersClosedCheckBox.Checked = criteria.JobOrdersClosed;
      InvitationsSentCheckBox.Checked = criteria.InvitationsSent;
      JobSeekersNotHiredCheckBox.Checked = criteria.JobSeekersNotHired;
      SelfReferralsCheckBox.Checked = criteria.SelfReferrals;
      StaffReferralsCheckBox.Checked = criteria.StaffReferrals;
    }


    /// <summary>
    /// Resets this instance.
    /// </summary>
    private void Reset()
    {
      JobOrdersEditedCheckBox.Checked =
      JobSeekersInterviewedCheckBox.Checked =
      JobSeekersHiredCheckBox.Checked =
      JobOrdersCreatedCheckBox.Checked =
      JobOrdersPostedCheckBox.Checked =
      JobOrdersPutOnHoldCheckBox.Checked =
      JobOrdersRefreshedCheckBox.Checked =
      JobOrdersClosedCheckBox.Checked =
      InvitationsSentCheckBox.Checked =
      JobSeekersNotHiredCheckBox.Checked =
      SelfReferralsCheckBox.Checked =
      StaffReferralsCheckBox.Checked = false;
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
			CriteriaEmployerActivityLabel.Text = CodeLocalise("CriteriaEmployerActivityLabel.Text", "#BUSINESS# activity");
			JobOrderHeader.Text = CodeLocalise("JobOrderHeader.Text", "#EMPLOYMENTTYPE# activity summary");
      SeekerHeader.Text = CodeLocalise("SeekerHeader.Text", "#CANDIDATETYPE# activity summary");

      JobOrdersEditedCheckBox.Text = CodeLocalise("JobOrdersEditedCheckBox.Text", Constants.Reporting.EmployerReportDataColumns.JobOrdersEdited);
      JobSeekersInterviewedCheckBox.Text = CodeLocalise("JobSeekersInterviewedCheckBox.Text", Constants.Reporting.EmployerReportDataColumns.JobSeekersInterviewed);
      JobSeekersHiredCheckBox.Text = CodeLocalise("JobSeekersHiredCheckBox.Text", Constants.Reporting.EmployerReportDataColumns.JobSeekersHired);
      JobOrdersCreatedCheckBox.Text = CodeLocalise("JobOrdersCreatedCheckBox.Text", Constants.Reporting.EmployerReportDataColumns.JobOrdersCreated);
      JobOrdersPostedCheckBox.Text = CodeLocalise("JobOrdersPostedCheckBox.Text", Constants.Reporting.EmployerReportDataColumns.JobOrdersPosted);
      JobOrdersPutOnHoldCheckBox.Text = CodeLocalise("JobOrdersPutOnHoldCheckBox.Text", Constants.Reporting.EmployerReportDataColumns.JobOrdersPutOnHold);
      JobOrdersRefreshedCheckBox.Text = CodeLocalise("JobOrdersRefreshedCheckBox.Text", Constants.Reporting.EmployerReportDataColumns.JobOrdersRefreshed);
      JobOrdersClosedCheckBox.Text = CodeLocalise("JobOrdersClosedCheckBox.Text", Constants.Reporting.EmployerReportDataColumns.JobOrdersClosed);
      InvitationsSentCheckBox.Text = CodeLocalise("InvitationsSentCheckBox.Text", Constants.Reporting.EmployerReportDataColumns.InvitationsSent);
      JobSeekersNotHiredCheckBox.Text = CodeLocalise("JobSeekersNotHiredCheckBox.Text", Constants.Reporting.EmployerReportDataColumns.JobSeekersNotHired);
      SelfReferralsCheckBox.Text = CodeLocalise("SelfReferralsCheckBox.Text", Constants.Reporting.EmployerReportDataColumns.SelfReferrals);
      StaffReferralsCheckBox.Text = CodeLocalise("StaffReferralsCheckBox.Text", Constants.Reporting.EmployerReportDataColumns.StaffReferrals);
    }
  }
}