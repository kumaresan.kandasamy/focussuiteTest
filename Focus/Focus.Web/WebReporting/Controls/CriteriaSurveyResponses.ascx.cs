﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;

using Framework.Core;

using Focus.Common.Extensions;


#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaSurveyResponses : UserControlBase
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();
        Bind();
      }
    }

    /// <summary>
    /// Unbinds this instance.
    /// </summary>
    public void Unbind()
    {
      //TODO: Survey unbind and criteria object
    }

    public void BindCriteria()
    {
      // TODO: Reporting for surveys
    }


    /// <summary>
    /// Resets this instance.
    /// </summary>
    private void Reset()
    {
      SurveyResponsesCheckBoxList.Items.Reset();
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      CriteriaSurveyResponsesLabel.Text = CodeLocalise("CriteriaSurveyResponsesLabel.Text", "Survey responses");
      ShowNumberofSeekerWhoHeader.Text = CodeLocalise("ShowNumberofSeekerWhoHeader.Text", "Show number of seekers who");
    }

    /// <summary>
    /// Binds this instance.
    /// </summary>
    private void Bind()
    {
      SurveyResponsesCheckBoxList.Items.Add(new ListItem(CodeLocalise("SurveyResponsesCheckBoxList.Dissatisfied.Text", "Are dissatisfiedwith match quality"), "1"));
      SurveyResponsesCheckBoxList.Items.Add(new ListItem(CodeLocalise("SurveyResponsesCheckBoxList.Satisfied.Text", "Are satisfied with match quality"), "2"));
      SurveyResponsesCheckBoxList.Items.Add(new ListItem(CodeLocalise("SurveyResponsesCheckBoxList.DidNotReceiveUnexpectedMatches.Text", "Did not receive unexpected matches"), "3"));
      SurveyResponsesCheckBoxList.Items.Add(new ListItem(CodeLocalise("SurveyResponsesCheckBoxList.ReceivedExpectedMatches.Text", "Received expected matches"), "4"));
      SurveyResponsesCheckBoxList.Items.Add(new ListItem(CodeLocalise("SurveyResponsesCheckBoxList.DidNotRecieveEmployerInvitations.Text", "Did not receive employer invitations"), "5"));
      SurveyResponsesCheckBoxList.Items.Add(new ListItem(CodeLocalise("SurveyResponsesCheckBoxList.ReceivedEmployerInvitations.Text", "Received employer invitations"), "6"));
    }
  }
}