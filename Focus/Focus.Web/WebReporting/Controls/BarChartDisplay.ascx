﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BarChartDisplay.ascx.cs" Inherits="Focus.Web.WebReporting.Controls.BarChartDisplay" %>


<!-- On-page scripts for jqPlot  -->
<script type="text/javascript" src="<%= ResolveUrl("~/Assets/Scripts/jqplot.barRenderer.min.js") %>"></script>
<script type="text/javascript" src="<%= ResolveUrl("~/Assets/Scripts/jqplot.categoryAxisRenderer.min.js") %>"></script>
<script type="text/javascript" src="<%= ResolveUrl("~/Assets/Scripts/jqplot.pointLabels.min.js") %>"></script>
<link href="<%= ResolveUrl("~/Assets/Css/jquery.jqplot.min.css")%>" rel="stylesheet" type="text/css"/>

<asp:Literal runat="server" ID="ErrorLiteral" Visible="False" />
<asp:PlaceHolder runat="server" ID="GraphPlaceHolder" Visible="False">
    <div id="reportbarchart" class="reportgrid95" style="min-height: 100%" ></div>
    <script type="text/javascript">



            var yAxisLabels = <%=YAxis %>;
            $(document).ready(function () {
    //JQ Plot
    // Y Axis labels.

    var plot = $.jqplot('reportbarchart', <%=ChartData %>, {
        seriesDefaults: {
          shadow: false,
          renderer: $.jqplot.BarRenderer,
          pointLabels: { show: true, location: 'e', edgeTolerance: -55 },
          rendererOptions: {
            barDirection: 'horizontal',
            barPadding: 5,
            barWidth : 15,
            highlightMouseOver: false
          }
        },
        series:[
            <%=LegendData %>
        ],
        legend: {
            show: true,
            placement: 'insideGrid'
        },
        axes: {
          xaxis: {
            tickOptions: {
              show: false,
              mark: 'cross',
       formatString: '%d'
            },
            min: 0,
            max: null,
            showTickMarks: false
          },
          yaxis: {
            renderer: $.jqplot.CategoryAxisRenderer,
            ticks: yAxisLabels,
            tickOptions: {
              showMark: false
            }
          }
        },
        grid: {
          drawGridLines: false, /*
          gridLineColor: '#ffffff', /*
          borderColor: '#509790',*/
          shadowWidth: 0,
          borderWidth: 0,
          shadow: false
        }
      });

        $("#reportbarchart").height(<%= ChartHeight %>);
        plot.replot();
  });


            

    </script>
</asp:PlaceHolder>