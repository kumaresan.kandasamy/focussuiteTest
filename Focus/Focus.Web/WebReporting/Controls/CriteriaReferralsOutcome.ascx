﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaReferralsOutcome.ascx.cs"
    Inherits="Focus.Web.WebReporting.Controls.CriteriaReferralsOutcome" %>
<table role="presentation" style="width:100%;" class="accordionWithIcon">
    <tr class="multipleAccordionTitle">
        <td style="vertical-align:top; width:10%;">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td style="width:90%;">
            <asp:Literal runat="server" ID="ReferralsOutcomeLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
            <table role="presentation" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                            <asp:Literal runat="server" ID="OutcomesStatusHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="FailedToApplyCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="FailedToReportToInterviewCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="FailedToReportToJobCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="FailedToRespondCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="FoundJobOtherSourceCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="HiredCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="InterviewDeniedCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="InterviewScheduledCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="JobAlreadyFilledCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="NewApplicantCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="NotHiredCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="NotQualifiedCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="NotYetPlacedCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="RecommendedCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="RefusedJobCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="RefusedReferralCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="UnderConsiderationCheckBox"/></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
