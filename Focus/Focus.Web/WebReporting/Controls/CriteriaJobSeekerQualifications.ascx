﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaJobSeekerQualifications.ascx.cs"
    Inherits="Focus.Web.WebReporting.Controls.CriteriaJobSeekerQualifications" %>
<table role="presentation" style="width: 100%;" class="accordionWithIcon">
    <tr class="multipleAccordionTitle">
        <td style="vertical-align: top; width: 10%;">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td style="width: 90%;">
            <asp:Literal runat="server" ID="CriteriaJobSeekerQualificationsLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
            <table role="presentation" style="width: 100%;">
                <tr>
                    <td>
                        <strong>
                            <asp:Literal runat="server" ID="LevelOfEducationHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBoxList ID="LevelOfEducationCheckBoxList1" TextAlign="Right" runat="server"
                            role="presentation" />
                        <asp:CheckBoxList ID="LevelOfEducationCheckBoxList2" TextAlign="Right" runat="server"
                            role="presentation" />
                        <asp:CheckBoxList ID="LevelOfEducationCheckBoxList" TextAlign="Right" runat="server"
                            role="presentation" />
                    </td>
                </tr>
            </table>
            <table role="presentation" style="width: 100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                            <asp:Literal runat="server" ID="JobSeekerRecordStatusHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBoxList ID="JobSeekerRecordStatusCheckBoxList" TextAlign="Right" runat="server"
                            role="presentation" />
                    </td>
                </tr>
            </table>
            <table role="presentation" style="width: 100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                            <asp:Literal runat="server" ID="CertificationsAndLicencesHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="position: relative">
                            <focus:LocalisedLabel runat="server" ID="CertificationsAndLicencesTextBoxLabel" AssociatedControlID="CertificationsAndLicencesTextBox"
                                LocalisationKey="CertificationsLicenses" DefaultText="Certifications and licences"
                                CssClass="sr-only" />
                            <asp:TextBox runat="server" ID="CertificationsAndLicencesTextBox"></asp:TextBox>
                            <div style="position: absolute; width: 500px;">
                                <act:AutoCompleteExtender ID="CertificationsAndLicencesAutoCompleteExtender" runat="server"
                                    TargetControlID="CertificationsAndLicencesTextBox" MinimumPrefixLength="2" CompletionInterval="100"
                                    ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetCertifications"
                                    CompletionListElementID="CertificationsAndLicencesAutoCompleteExtenderCompletionList"
                                    CompletionListCssClass="autocomplete_completionListElement" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                    CompletionListItemCssClass="autocomplete_listItem" />
                                <div id="CertificationsAndLicencesAutoCompleteExtenderCompletionList" class="autocompleteCompletionList">
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <input type="button" class="button3" runat="server" id="CertificationsAndLicencesButton"
                            name="CertificationsAndLicencesButton" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table role="presentation" id="CertificationsAndLicencesTable" style="width: 75%;">
                            <tbody>
                            </tbody>
                        </table>
                        <asp:HiddenField runat="server" ID="HiddenCertificationsAndLicencesList" />
                    </td>
                </tr>
            </table>
            <table role="presentation" style="width: 100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                            <asp:Literal runat="server" ID="SkillHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="position: relative">
                            <focus:LocalisedLabel runat="server" ID="SkillTextBoxLabel" AssociatedControlID="SkillTextBox"
                                LocalisationKey="Skills" DefaultText="Skills" CssClass="sr-only" />
                            <asp:TextBox runat="server" ID="SkillTextBox"></asp:TextBox>
                            <div style="position: absolute; width: 500px;">
                                <act:AutoCompleteExtender ID="SkillAutoCompleteExtender" runat="server" TargetControlID="SkillTextBox"
                                    MinimumPrefixLength="2" CompletionInterval="100" ServicePath="~/Services/AjaxService.svc"
                                    ServiceMethod="GetReportingSkills" CompletionListElementID="SkillAutoCompleteExtenderCompletionList"
                                    CompletionListCssClass="autocomplete_completionListElement" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                    CompletionListItemCssClass="autocomplete_listItem" />
                                <div id="SkillAutoCompleteExtenderCompletionList" class="autocompleteCompletionList">
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <input type="button" class="button3" runat="server" id="SkillButton" name="SkillButton" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table role="presentation" id="SkillTable" style="width: 75%;">
                            <tbody>
                            </tbody>
                        </table>
                        <asp:HiddenField runat="server" ID="HiddenSkillList" />
                    </td>
                </tr>
            </table>
            <table role="presentation" style="width: 100%;" id="InternshipsSectionTable" runat="server"
                visible="False">
                <tr>
                    <td colspan="2">
                        <strong>
                            <asp:Literal runat="server" ID="InternshipsHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="position: relative">
                            <asp:TextBox runat="server" ID="InternshipsTextBox"></asp:TextBox>
                            <div style="position: absolute; width: 500px;">
                                <act:AutoCompleteExtender ID="InternshipsAutoCompleteExtender" runat="server" TargetControlID="InternshipsTextBox"
                                    MinimumPrefixLength="2" CompletionListCssClass="autocompleteCompletionList" CompletionInterval="100"
                                    ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetReportingInternships"
                                    CompletionListElementID="InternshipsAutoCompleteExtenderCompletionList" />
                                <div id="InternshipsAutoCompleteExtenderCompletionList" class="autocompleteCompletionList">
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <input type="button" class="button3" runat="server" id="InternshipsButton" name="InternshipsButton" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table role="presentation" id="InternshipsTable" style="width: 75%;">
                            <tbody>
                            </tbody>
                        </table>
                        <asp:HiddenField runat="server" ID="HiddenInternshipsList" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<script type="text/javascript">
    $(document).ready(function () {
        $("#<%=CertificationsAndLicencesButton.ClientID%>").click(function () {
            AddListItem('<%=CertificationsAndLicencesTextBox.ClientID %>', '<%=HiddenCertificationsAndLicencesList.ClientID %>', 'CertificationsAndLicencesTable');
        });
        $("#<%=SkillButton.ClientID%>").click(function () {
            AddListItem('<%=SkillTextBox.ClientID %>', '<%=HiddenSkillList.ClientID %>', 'SkillTable');
        });
        $("#<%=InternshipsButton.ClientID%>").click(function () {
            AddListItem('<%=InternshipsTextBox.ClientID %>', '<%=HiddenInternshipsList.ClientID %>', 'InternshipsTable');
        });

        $('#<%=LevelOfEducationCheckBoxList1.ClientID%> :checkbox').trigger('change');

        BuildTableFromScratch('<%=HiddenCertificationsAndLicencesList.ClientID %>', 'CertificationsAndLicencesTable');
        BuildTableFromScratch('<%=HiddenSkillList.ClientID %>', 'SkillTable');
        //BuildTableFromScratch('<%=HiddenInternshipsList.ClientID %>', 'InternshipsTable');

    });
    $('#<%=LevelOfEducationCheckBoxList1.ClientID%> :checkbox').change(function () {
    	if ($(this).val() == 'HighSchoolDiplomaOrEquivalent') {
    		if ($(this).is(':checked')) {
    			$("#<%=LevelOfEducationCheckBoxList2.ClientID%>  input[type='checkbox']").each(function () { $(this).prop("checked", false); $(this).attr("disabled", "disabled"); });
    		} else {
    			$("#<%=LevelOfEducationCheckBoxList2.ClientID%>  input[type='checkbox']").each(function () { $(this).removeAttr("disabled"); });
    		} 
    	}
    });
</script>
