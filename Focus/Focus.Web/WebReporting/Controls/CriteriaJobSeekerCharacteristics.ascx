﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaJobSeekerCharacteristics.ascx.cs" Inherits="Focus.Web.WebReporting.Controls.CriteriaJobSeekerCharacteristics" %>
<table role="presentation" style="width:100%;" class="accordionWithIcon">
    <tr class="multipleAccordionTitle">
        <td style="vertical-align:top; width:10%;">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td style="width:90%;">
           <asp:Literal runat="server" ID="CriteriaJobSeekerCharacteristicsLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2"> <table id="AgeTable" role="presentation" style="width:100%;" runat="server">
  <tr>
    <td colspan="2"><strong><asp:Literal runat="server" ID="AgeHeader"></asp:Literal></strong></td>
  </tr>
  <tr>
    <td>
				<focus:LocalisedLabel runat="server" ID="AgeFromLabel" AssociatedControlID="AgeFrom" LocalisationKey="AgeFrom" DefaultText="Age from" CssClass="sr-only"/>
        <asp:TextBox runat="server" ID="AgeFrom" Width="30" MaxLength="3"  />&nbsp;<%=HtmlLabel(AgeTo,"Global.To", "to") %>&nbsp;<asp:TextBox ID="AgeTo" runat="server" Width="30" MaxLength="3" />
    </td>
  </tr>
</table>
  <table role="presentation" style="width:100%;">
  <tr>
    <td colspan="2"><strong><asp:Literal runat="server" ID="GenderHeader"></asp:Literal></strong></td>
  </tr>
  <tr>
    <td>
        <asp:CheckBoxList runat="server" TextAlign="Right" ID="GenderCheckBoxList" role="presentation"></asp:CheckBoxList>
    </td>
  </tr>
</table>
<table role="presentation" style="width:100%;">
  <tr>
    <td colspan="2"><strong><asp:Literal runat="server" ID="EthinicityHeritageHeader"></asp:Literal></strong></td>
  </tr>
  <tr>
    <td>
        <asp:CheckBoxList id="EthinicityHeritageCheckBoxList" TextAlign="Right" runat="server" role="presentation"/>
    </td>
  </tr>
</table>
<table role="presentation" style="width:100%;">
  <tr>
    <td colspan="2"><strong><asp:Literal runat="server" ID="RaceHeader"></asp:Literal></strong></td>
  </tr>
  <tr>
    <td>
        <asp:CheckBoxList id="RaceCheckBoxList" TextAlign="Right" runat="server" role="presentation"/>
    </td>
  </tr>
</table>
<table role="presentation" style="width:100%;">
  <tr>
    <td colspan="2"><strong><asp:Literal runat="server" ID="DisabilityTypeHeader"></asp:Literal></strong></td>
  </tr>
  <tr>
    <td>
        <asp:CheckBoxList id="DisabilityTypeCheckBoxList" TextAlign="Right" runat="server" role="presentation"/>
    </td>
  </tr>
</table></td>
    </tr>
</table>
<asp:CompareValidator runat="server" ID="AgeFromTypeValidator" ControlToValidate="AgeFrom" Type="Integer" Operator="DataTypeCheck" Display="None"></asp:CompareValidator>
<asp:CompareValidator runat="server" ID="AgeToTypeValidator" ControlToValidate="AgeTo" Type="Integer" Operator="DataTypeCheck" Display="None"></asp:CompareValidator>
<asp:CompareValidator runat="server" ID="AgeFromValueValidator" ControlToValidate="AgeFrom" ValueToCompare="0" Operator="GreaterThan" Display="None"></asp:CompareValidator>
<asp:CompareValidator runat="server" ID="AgeToValueValidator" ControlToValidate="AgeTo" ValueToCompare="0" Operator="GreaterThan" Display="None"></asp:CompareValidator>
<asp:CompareValidator runat="server" ID="AgeToFromComparisonValidator" ControlToValidate="AgeTo" ControlToCompare="AgeFrom" Operator="GreaterThanEqual" Display="None"></asp:CompareValidator>