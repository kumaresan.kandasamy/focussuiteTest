﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaOccupationAndSalary.ascx.cs"
    Inherits="Focus.Web.WebReporting.Controls.CriteriaOccupationAndSalary" %>
<table role="presentation" style="width:100%;" class="accordionWithIcon">
    <tr class="multipleAccordionTitle">
        <td style="vertical-align:top; width:10%;">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td style="width:90%;">
           <asp:Literal runat="server" ID="CriteriaOccupationAndSalaryLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
            <asp:PlaceHolder runat="server" ID="OccupationCriteriaPlaceHolder" Visible="False">
            <table role="presentation" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                            <asp:Literal runat="server" ID="OccupationGroupHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
											<div style="position: relative;">
												<focus:LocalisedLabel runat="server" ID="OccupationGroupTextBoxLabel" AssociatedControlID="OccupationGroupTextBox" LocalisationKey="OccupationGroup" DefaultText="Occupation group" CssClass="sr-only"/>
                        <asp:TextBox runat="server" ID="OccupationGroupTextBox"></asp:TextBox>
												<div style="position:absolute; width:500px;">
													<act:AutoCompleteExtender ID="OccupationGroupAutoCompleteExtender" runat="server" TargetControlID="OccupationGroupTextBox" MinimumPrefixLength="2" CompletionListCssClass="autocompleteCompletionList" CompletionInterval="100" ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetReportingOnetOccupationGroups" CompletionListElementID="OccupationGroupAutoCompleteExtenderCompletionList" />
													<div id="OccupationGroupAutoCompleteExtenderCompletionList" class="autocompleteCompletionList"></div>
												</div>
											</div>
                    </td>
                    <td>
                        <input type="button" class="button3" runat="server" id="OccupationGroupButton" name="OccupationGroupButton" />
                    </td>
                </tr>
                    <tr>
                        <td colspan="2"><table role="presentation" id="OccupationGroupTable" style="width:75%;"><tbody></tbody></table><asp:HiddenField runat="server" ID="HiddenOccupationGroupList"/></td>
                    </tr>
            </table>
            <table role="presentation" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                            <asp:Literal runat="server" ID="DetailedOccupationHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
											<div style="position: relative;">
												<focus:LocalisedLabel runat="server" ID="DetailedOccupationTextBoxLabel" AssociatedControlID="DetailedOccupationTextBox" LocalisationKey="DetailedOccupation" DefaultText="Detailed occupation" CssClass="sr-only"/>
                        <asp:TextBox runat="server" ID="DetailedOccupationTextBox"></asp:TextBox>
												<div style="position:absolute; width:500px;">
													<act:AutoCompleteExtender ID="DetailedOccupationAutoCompleteExtender" runat="server" TargetControlID="DetailedOccupationTextBox" MinimumPrefixLength="2" CompletionListCssClass="autocompleteCompletionList" CompletionInterval="100" ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetReportingOnetDetailedOccupations" CompletionListElementID="DetailedOccupationAutoCompleteExtenderCompletionList" />
													<div id="DetailedOccupationAutoCompleteExtenderCompletionList" class="autocompleteCompletionList"></div>
												</div>
											</div>
                    </td>
                    <td>
                        <input type="button" class="button3" runat="server" id="DetailedOccupationButton"
                            name="DetailedOccupationButton" />
                    </td>
                </tr>
                    <tr>
                        <td colspan="2"><table role="presentation" id="DetailedOccupationTable" style="width:75%;"><tbody></tbody></table><asp:HiddenField runat="server" ID="HiddenDetailedOccupationList"/></td>
                    </tr>
            </table>
            </asp:PlaceHolder>
            <table role="presentation" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                          <focus:LocalisedLabel runat="server" ID="MOCCodeHeader" LocalisationKey="MOCCodeHeader.Text" DefaultText ="MOC code" />
                        </strong>
                    </td>
                </tr>
                <tr>
                    <td>
	                    <div style="position: relative;">
		                    <focus:LocalisedLabel runat="server" ID="MOCCodeTextBoxLabel" AssociatedControlID="MOCCodeTextBox" LocalisationKey="MOCCode" DefaultText="MOC Code" CssClass="sr-only"/>
                        <asp:TextBox runat="server" ID="MOCCodeTextBox"></asp:TextBox>
												<div style="position:absolute; width:500px;">
													<act:AutoCompleteExtender ID="MOCCodeAutoCompleteExtender" runat="server" TargetControlID="MOCCodeTextBox" MinimumPrefixLength="2" CompletionListCssClass="autocompleteCompletionList" CompletionInterval="100" ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetReportingMOCCOdes" CompletionListElementID="MOCCodeAutoCompleteExtenderCompletionList" />
													<div id="MOCCodeAutoCompleteExtenderCompletionList" class="autocompleteCompletionList"></div>
												</div>
											</div>
                    </td>
                    <td>
                        <input type="button" class="button3" runat="server" id="MOCCodeButton" name="MOCCodeButton" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                     <table role="presentation" id="MOCCodeTable" style="width:75%;"><tbody></tbody></table><asp:HiddenField runat="server" ID="HiddenMOCCodeList"/>
                    </td>
                </tr>
            </table>
            <table role="presentation" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                          <focus:LocalisedLabel runat="server" ID="MOCTitleHeader" LocalisationKey="MOCTitleHeader.Text"  DefaultText="MOC title" /></strong>
                    </td>
                </tr>
                <tr>
                    <td>
	                    <div style="position: relative;">
		                    <focus:LocalisedLabel runat="server" ID="MOCTitleTextBoxLabel" AssociatedControlID="MOCTitleTextBox" LocalisationKey="MOCTitle" DefaultText="MOC title" CssClass="sr-only"/>
                        <asp:TextBox runat="server" ID="MOCTitleTextBox" ></asp:TextBox>
												<div style="position:absolute; width:500px;">
													<act:AutoCompleteExtender ID="MOCTitleAutoCompleteExtender" runat="server" TargetControlID="MOCTitleTextBox" MinimumPrefixLength="2" CompletionListCssClass="autocompleteCompletionList" CompletionInterval="100" ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetReportingMOCTitles" CompletionListElementID="MOCTitleAutoCompleteExtenderCompletionList" />
													<div id="MOCTitleAutoCompleteExtenderCompletionList" class="autocompleteCompletionList"></div>
												</div>
											</div>
                    </td>
                    <td>
                        <input type="button" class="button3" runat="server" id="MOCTitleButton" name="MOCTitleButton" />
                    </td>
                </tr>
                    <tr>
                        <td colspan="2"><table role="presentation" id="MOCTitleTable" style="width:75%;"><tbody></tbody></table><asp:HiddenField runat="server" ID="HiddenMOCTitleList"/></td>
                    </tr>
            </table>
            <table role="presentation" style="width:100%;">
                <tr>
                    <td>
                        <strong><focus:LocalisedLabel runat="server" ID="SalaryHeader" LocalisationKey="SalaryHeader.Text"  DefaultText="Salary" /></strong>
                    </td>
                </tr>
                <tr>
                    <td style="white-space:nowrap;">
                        <%=HtmlLocalise("Global.DefaultCurrencySymbol", "$") %>
												<focus:LocalisedLabel runat="server" ID="LowSalaryTextBoxLabel" AssociatedControlID="LowSalaryTextBox" LocalisationKey="MinimumSalary" DefaultText="Minimum salary" CssClass="sr-only"/>
												<asp:TextBox runat="server" ID="LowSalaryTextBox" Width="50" />&nbsp;
                        <%=HtmlLocalise("Global.To", "to") %>&nbsp;
                        <%=HtmlLocalise("Global.DefaultCurrencySymbol", "$") %>
												<focus:LocalisedLabel runat="server" ID="HighSalaryTextBoxLabel" AssociatedControlID="HighSalaryTextBox" LocalisationKey="MaximumSalary" DefaultText="Maximum salary" CssClass="sr-only"/>
												<asp:TextBox runat="server" ID="HighSalaryTextBox" Width="50" />
												<focus:LocalisedLabel runat="server" ID="SalaryRangeDropDownLabel" AssociatedControlID="SalaryRangeDropDown" LocalisationKey="SalaryRange" DefaultText="Salary range" CssClass="sr-only"/>
                        <asp:DropDownList runat="server" ID="SalaryRangeDropDown" />
                    </td>
                </tr>
            </table>
            <table role="presentation" id="ExperienceIndustriesCriteria" runat="server" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                            <focus:LocalisedLabel runat="server" ID="ExperienceIndustryLabel" LocalisationKey="ExperienceIndustry.Label"  DefaultText="Most Experienced Industries" /></strong>
                    </td>
                </tr>
                <tr>
                    <td>
												<div style="position: relative;">
													<focus:LocalisedLabel runat="server" ID="ExperienceIndustryTextBoxLabel" AssociatedControlID="ExperienceIndustryTextBox" LocalisationKey="ExperienceInIndustry" DefaultText="Experience in industry" CssClass="sr-only"/>
													<asp:TextBox runat="server" ID="ExperienceIndustryTextBox" ></asp:TextBox>
													<div style="position:absolute; width:500px;">
														<act:AutoCompleteExtender ID="ExperienceIndustryAutoCompleteExtender" runat="server" TargetControlID="ExperienceIndustryTextBox" MinimumPrefixLength="2" CompletionListElementID="ExperienceIndustryAutoCompleteExtenderCompletionList" CompletionListCssClass="autocompleteCompletionList" CompletionInterval="100" CompletionSetCount="15" ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetNaics" />
														<div id="ExperienceIndustryAutoCompleteExtenderCompletionList" class="autocompleteCompletionList"></div>
													</div>
												</div>
                    </td>
                    <td>
                        <input type="button" class="button3" runat="server" id="ExperienceIndustryAddButton" name="ExperienceIndustryAddButton" />
                    </td>
                </tr>
                <tr><td colspan="2"> <asp:CustomValidator runat="server" ID="ExperienceIndustrialClassificationValidator" ControlToValidate="ExperienceIndustryTextBox" ClientValidationFunction="doesNAICSExist" ErrorMessage="Industry classification (NAICS) not found"  SetFocusOnError="true" Display="Dynamic" CssClass="error" /></td></tr>
                 <tr>

                        <td colspan="2"><table role="presentation" id="ExperienceIndustryTable" style="width:75%;"><tbody></tbody></table><asp:HiddenField runat="server" ID="HiddenExperienceIndustryList"/></td>
                    </tr>
            </table>
             <table role="presentation" id="TargetIndustriesCriteria" runat="server" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong>
                            <focus:LocalisedLabel runat="server" ID="TargetIndustryLabel" LocalisationKey="TargetIndustry.Label"  DefaultText="Target Industries" /></strong>
                    </td>
                </tr>
                <tr>
                    <td>
												<div style="position: relative;">
													<focus:LocalisedLabel runat="server" ID="TargetIndustryTextBoxLabel" AssociatedControlID="TargetIndustryTextBox" LocalisationKey="TargetIndustry" DefaultText="Target industry" CssClass="sr-only"/>
													<asp:TextBox runat="server" ID="TargetIndustryTextBox"></asp:TextBox>
													<div style="position:absolute; width:500px;">
														<act:AutoCompleteExtender ID="TargetIndustryAutoCompleteExtender" runat="server" TargetControlID="TargetIndustryTextBox" MinimumPrefixLength="2" CompletionListElementID="TargetIndustryAutoCompleteExtenderCompletionList" CompletionListCssClass="autocompleteCompletionList" CompletionInterval="100" CompletionSetCount="15" ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetNaics" /> 
														<div id="TargetIndustryAutoCompleteExtenderCompletionList" class="autocompleteCompletionList"></div>
													</div>
												</div>
                    </td>
                    <td>
                        <input type="button" class="button3" runat="server" id="TargetIndustryAddButton" name="TargetIndustryAddButton" />
                    </td>
                </tr>
                <tr><td colspan="2"> <asp:CustomValidator runat="server" ID="TargetIndustrialClassificationValidator" ControlToValidate="TargetIndustryTextBox" ClientValidationFunction="doesNAICSExist" ErrorMessage="Industry classification (NAICS) not found"  SetFocusOnError="true" Display="Dynamic" CssClass="error" /></td></tr>
                 <tr>
                        <td colspan="2"><table role="presentation" id="TargetIndustryTable" style="width:75%;"><tbody></tbody></table><asp:HiddenField runat="server" ID="HiddenTargetIndustryList"/></td>
                    </tr>
            </table>
        </td>
    </tr>
</table>
<asp:CompareValidator runat="server" ID="SalaryFromNumericValidator" ControlToValidate="LowSalaryTextBox" Type="Currency" Operator="DataTypeCheck" Display="None"></asp:CompareValidator>
<asp:CompareValidator runat="server" ID="SalaryToNumericValidator" ControlToValidate="HighSalaryTextBox" Type="Currency" Operator="DataTypeCheck" Display="None"></asp:CompareValidator>
<asp:CompareValidator runat="server" ID="SalaryFromValueValidator" ControlToValidate="LowSalaryTextBox" ValueToCompare="0" Operator="GreaterThan" Type="Currency" Display="None"></asp:CompareValidator>
<asp:CompareValidator runat="server" ID="SalaryToValueValidator" ControlToValidate="HighSalaryTextBox" ValueToCompare="0" Operator="GreaterThan" Type="Currency" Display="None"></asp:CompareValidator>
<asp:CompareValidator runat="server" ID="SalaryToFromComparisonValidator" ControlToValidate="HighSalaryTextBox" ControlToCompare="LowSalaryTextBox" Type="Currency" Operator="GreaterThanEqual" Display="None"></asp:CompareValidator>
<script type="text/javascript">
    $(document).ready(function () {
        $("#<%=OccupationGroupButton.ClientID%>").click(function () {
            AddListItem('<%=OccupationGroupTextBox.ClientID %>', '<%=HiddenOccupationGroupList.ClientID %>', 'OccupationGroupTable');
        });
        $("#<%=DetailedOccupationButton.ClientID%>").click(function () {
            AddListItem('<%=DetailedOccupationTextBox.ClientID %>', '<%=HiddenDetailedOccupationList.ClientID %>', 'DetailedOccupationTable');
        });
        $("#<%=MOCCodeButton.ClientID%>").click(function () {
            AddListItem('<%=MOCCodeTextBox.ClientID %>', '<%=HiddenMOCCodeList.ClientID %>', 'MOCCodeTable');
        });
        $("#<%=MOCTitleButton.ClientID%>").click(function () {
            AddListItem('<%=MOCTitleTextBox.ClientID %>', '<%=HiddenMOCTitleList.ClientID %>', 'MOCTitleTable');
          });
          $("#<%=ExperienceIndustryAddButton.ClientID%>").click(function () {
            if ($('#<%=ExperienceIndustrialClassificationValidator.ClientID%>').is(":hidden")) {
            AddListItem('<%=ExperienceIndustryTextBox.ClientID %>', '<%=HiddenExperienceIndustryList.ClientID %>', 'ExperienceIndustryTable');
          }
         });
        $("#<%=TargetIndustryAddButton.ClientID%>").click(function () {
          if ($('#<%=TargetIndustrialClassificationValidator.ClientID%>').is(":hidden")) {
            AddListItem('<%=TargetIndustryTextBox.ClientID %>', '<%=HiddenTargetIndustryList.ClientID %>', 'TargetIndustryTable');
          }
        });

        BuildTableFromScratch('<%=HiddenMOCCodeList.ClientID %>', 'MOCCodeTable');
        BuildTableFromScratch('<%=HiddenMOCTitleList.ClientID %>', 'MOCTitleTable');
        BuildTableFromScratch('<%=HiddenExperienceIndustryList.ClientID %>', 'ExperienceIndustryTable');
        BuildTableFromScratch('<%=HiddenTargetIndustryList.ClientID %>', 'TargetIndustryTable');

      });

      function doesNAICSExist(source, arguments) {
        var exists;

        $.ajax({
          type: "POST",
          contentType: "application/json; charset=utf-8",
          url: "<%= UrlBuilder.AjaxService() %>/CheckNaicsExists", //+ arguments.Value,
          dataType: "json",
          data: '{"naicsText": "' + arguments.Value + '"}',
          async: false,
          success: function (result) {
            exists = (result.d);
          }
        });

        arguments.IsValid = exists;
       }
</script>