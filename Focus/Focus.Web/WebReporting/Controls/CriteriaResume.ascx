﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaResume.ascx.cs"
    Inherits="Focus.Web.WebReporting.Controls.CriteriaResume" %>
<table role="presentation" style="width:100%;" class="accordionWithIcon">
    <tr class="multipleAccordionTitle">
        <td style="vertical-align:top; width:10%;">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td style="width:90%;">
            <asp:Literal runat="server" ID="CriteriaResumeLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
            <table role="presentation" style="width:100%;" data-table="CriteriaResume">
                <tr>
                    <td colspan="2">
                        <strong>
                            <asp:Literal runat="server" ID="ResumeAndJobSeekerHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="HasResumeCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="HasPendingResumeCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="HasSearchableResumeCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="HasMultipleResumesCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="WillingToRelocateCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="WillingToWorkOvertimeCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="UsedOnlineResumeHelpCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="HasNoResumeCheckBox"/></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:CustomValidator runat="server" ID="HasNoResumeValidator" ClientValidationFunction="CriteriaResume_HasNoResumeValidator" EnableClientScript="True" Display="None"></asp:CustomValidator>
<script type="text/javascript">
  function CriteriaResume_HasNoResumeValidator(src, args) {
    args.IsValid = true;

    var hasNoResumeBox = $("#<%=HasNoResumeCheckBox.ClientID %>");
    if (hasNoResumeBox.prop("checked")) {
      var boxesTicked = $("table[data-table='CriteriaResume'] input:checkbox:checked").length;
      args.IsValid = (boxesTicked == 1);
    }
  }
</script>
