﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.Report;
using Focus.Core.Models.Career;
using Focus.Services.Core;
using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaJobSeekerStatus : UserControlBase
	{
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();
        Bind();
      }
    }

    /// <summary>
    /// Resets this instance.
    /// </summary>
    private void Reset()
    {
      EmploymentStatusCheckBoxList.Items.Reset();
      JobSeekerRecordStatusCheckBoxList.Items.Reset();
			CareerAccountTypeCheckBoxList.Items.Reset();
    }

    /// <summary>
    /// Unbinds this instance.
    /// </summary>
    /// <returns></returns>
    public JobSeekerStatusReportCriteria Unbind()
    {
      return new JobSeekerStatusReportCriteria
      {
        EmploymentStatuses = EmploymentStatusCheckBoxList.Items.Cast<ListItem>()
                                                          .Where(x => x.Selected)
                                                          .Select( x => (EmploymentStatus?)Enum.Parse(typeof (EmploymentStatus), x.Value))
                                                          .ToList(),
				CareerAccountTypes = CareerAccountTypeCheckBoxList.Items.Cast<ListItem>()
																													.Where(x => x.Selected)
																													.Select(x => (CareerAccountType?)Enum.Parse(typeof(CareerAccountType),x.Value))
																													.ToList(),
        JobSeekerBlockedStatus = JobSeekerRecordStatusCheckBoxList.Items.Cast<ListItem>().Where(x=>x.Value == "2" && x.Selected).Any(),
        JobSeekerEnabledStatus = JobSeekerRecordStatusCheckBoxList.Items.Cast<ListItem>().Where(x=>x.Value != "2" && x.Selected).Select(x=>x.Value).ToList()
                                                          
      };
    }

    public void BindCriteria(JobSeekerStatusReportCriteria criteria)
    {
      Reset();

      if (criteria.IsNull()) return;

      EmploymentStatusCheckBoxList.BindEnumListToControlList(criteria.EmploymentStatuses);
			CareerAccountTypeCheckBoxList.BindEnumListToControlList(criteria.CareerAccountTypes);
            JobSeekerRecordStatusCheckBoxList.BindEnumListToControlList(criteria.JobSeekerEnabledStatus);
            if (criteria.JobSeekerBlockedStatus)
            {
                var checkbox = JobSeekerRecordStatusCheckBoxList.Items.Cast<ListItem>().SingleOrDefault(x => x.Value == "2");
                checkbox.Selected = true;
            }
        
    }
    
    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      CriteriaJobSeekerStatusLabel.Text = CodeLocalise("CriteriaClaimantStatusLabel.Text", "Job seeker statuses");
      EmploymentStatusHeader.Text = CodeLocalise("EmploymentStatusHeader.Text", "Employment Status");
      JobSeekerRecordStatusHeader.Text = CodeLocalise("JobSeekerRecordStatusHeader.Text", "Job Seeker Status");
	    CareerAccountTypeHeader.Text = CodeLocalise("CareerAccountType.Text", "Account Type");
    }

    /// <summary>
    /// Binds this instance.
    /// </summary>
    private void Bind()
    {
      EmploymentStatusCheckBoxList.Items.Add(new ListItem(CodeLocalise("EmploymentStatusCheckBoxList.Employed.Text", "Employed"), EmploymentStatus.Employed.ToString()));
      EmploymentStatusCheckBoxList.Items.Add(new ListItem(CodeLocalise("EmploymentStatusCheckBoxList.EmployedTrmnNotice.Text", "Employed with termination notice"), EmploymentStatus.EmployedTerminationReceived.ToString()));
      EmploymentStatusCheckBoxList.Items.Add(new ListItem(CodeLocalise("EmploymentStatusCheckBoxList.Unemployed.Text", "Unemployed"), EmploymentStatus.UnEmployed.ToString()));

      JobSeekerRecordStatusCheckBoxList.Items.Add(new ListItem(CodeLocalise("JobSeekerRecordStatusCheckBoxList.Active.Text", "Active"), "1"));
      JobSeekerRecordStatusCheckBoxList.Items.Add(new ListItem(CodeLocalise("JobSeekerRecordStatusCheckBoxList.Blocked.Text", "Blocked"), "2"));
      JobSeekerRecordStatusCheckBoxList.Items.Add(new ListItem(CodeLocalise("JobSeekerRecordStatusCheckBoxList.Inactive.Text", "Inactive"), "0"));

	    CareerAccountTypeTable.Visible = App.Settings.DisplayCareerAccountType && App.Settings.Theme == FocusThemes.Workforce;

			CareerAccountTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise("CareerAccountTypeCheckBoxList.JobSeeker.Text", "Job seeker"), CareerAccountType.JobSeeker.ToString()));
			CareerAccountTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise("CareerAccountTypeCheckBoxList.Student.Text", "Student"), CareerAccountType.Student.ToString()));
    
		}
  }
}