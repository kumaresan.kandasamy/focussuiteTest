﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.Report;

using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaCompliance : UserControlBase
	{
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();
        Bind();
      }
    }

    /// <summary>
    /// Binds the controls to the criteria
    /// </summary>
    /// <param name="criteria">The criteria to which to bind</param>
    public void BindCriteria(JobOrderComplianceReportCriteria criteria)
    {
      Reset();

      if (criteria.IsNull())
        return;

      ForeignLaborTypesCheckBoxList.BindEnumListToControlList(criteria.ForeignLabourTypes);

      NonForeignLabourCertificationCheckBox.Checked = criteria.NonForeignLabourCertification.GetValueOrDefault(false);
      CourtOrderedAffirmativeActionCheckbox.Checked = criteria.CourtOrderedAffirmativeAction.GetValueOrDefault(false);
      FederalContractJobsCheckbox.Checked = criteria.FederalContractor.GetValueOrDefault(false);
    }

    /// <summary>
    /// Gets the criteria object based on the control values
    /// </summary>
    /// <returns>The criteria</returns>
    public JobOrderComplianceReportCriteria Unbind()
    {
      var criteria = new JobOrderComplianceReportCriteria
      {
        ForeignLabourTypes = ForeignLaborTypesCheckBoxList.UnbindNullableEnumControlList<ForeignLaborTypes>(),
        NonForeignLabourCertification = NonForeignLabourCertificationCheckBox.Checked ? NonForeignLabourCertificationCheckBox.Checked : (bool?) null,
        CourtOrderedAffirmativeAction = CourtOrderedAffirmativeActionCheckbox.Checked ? CourtOrderedAffirmativeActionCheckbox.Checked : (bool?) null,
        FederalContractor = FederalContractJobsCheckbox.Checked ? FederalContractJobsCheckbox.Checked : (bool?)null
      };

      return criteria;
    }

    /// <summary>
    /// Clears the controls on the page
    /// </summary>
    private void Reset()
    {
      ForeignLaborTypesCheckBoxList.Items.Reset();
    }

    private void Localise()
    {
      CriteriaComplianceLabel.Text = CodeLocalise("CriteriaComplianceLabel.Text", "Compliance");
      ForiegnLabourCertificationHeader.Text = CodeLocalise("ForiegnLabourCertificationHeader.Text", "Foreign labor certification");
      ForiegnLabourCertificationMatchesHeader.Text = CodeLocalise("ForiegnLabourCertificationMatchesHeader.Text", "Foreign labor certification matches");

      NonForeignLabourCertificationCheckBox.Text = CodeLocalise("NonForeignLabourCertificationCheckBox.Text", "Non-foreign labor");
      CourtOrderedAffirmativeActionCheckbox.Text = CodeLocalise("CourtOrderedAffirmativeActionCheckbox.Text", "Court ordered affirmative action");
      FederalContractJobsCheckbox.Text = CodeLocalise("FederalContractJobsCheckbox.Text", "Federal contract");
    }

    private void Bind()
    {
      ForeignLaborTypesCheckBoxList.Items.Add(new ListItem(CodeLocalise("H2AAgricultureCheckBox.Text", "H2A agriculture"), ForeignLaborTypes.ForeignLabourCertificationH2A.ToString()));
      ForeignLaborTypesCheckBoxList.Items.Add(new ListItem(CodeLocalise("H2BNonAgricultureCheckBox.Text", "H2B non-agriculture"), ForeignLaborTypes.ForeignLabourCertificationH2B.ToString()));
      ForeignLaborTypesCheckBoxList.Items.Add(new ListItem(CodeLocalise("Other.Text", "Other"), ForeignLaborTypes.ForeignLabourCertificationOther.ToString()));
    }
  }
}