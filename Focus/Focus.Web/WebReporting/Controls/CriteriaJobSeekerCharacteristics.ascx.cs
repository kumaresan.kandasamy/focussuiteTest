﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.Report;
using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaJobSeekerCharacteristics : UserControlBase
	{
    protected void Page_Load(object sender, EventArgs e)
    {
        AgeTable.Visible = App.Settings.EnableDobFeatureGroup;
      if (!Page.IsPostBack)
      {
        Localise();
        Bind();
      }
    }

    /// <summary>
    /// Resets this instance.
    /// </summary>
    public void Reset(ReportType reportType)
    {
      AgeFrom.Text = string.Empty;
      AgeTo.Text = string.Empty;
      GenderCheckBoxList.Items.Reset();
      EthinicityHeritageCheckBoxList.Items.Reset();
      RaceCheckBoxList.Items.Reset();
      DisabilityTypeCheckBoxList.Items.Reset();
      AgeFromTypeValidator.ValidationGroup =
        AgeToTypeValidator.ValidationGroup =
        AgeFromValueValidator.ValidationGroup =
        AgeToValueValidator.ValidationGroup = 
        AgeToFromComparisonValidator.ValidationGroup = reportType.ToString();
    }

    public void BindCriteria(JobSeekerCharacteristicsReportCriteria criteria, ReportType reportType)
    {
      Reset(reportType);
      if (criteria.IsNull()) return;

      if (criteria.MinimumAge.HasValue) 
				AgeFrom.Text = criteria.MinimumAge.ToString();

      if (criteria.MaximumAge.HasValue) 
				AgeTo.Text = criteria.MaximumAge.ToString();

			GenderCheckBoxList.BindEnumListToControlList(criteria.Genders);
      DisabilityTypeCheckBoxList.BindEnumListToControlList(criteria.DisabilityTypes);
      EthinicityHeritageCheckBoxList.BindEnumListToControlList(criteria.EthnicHeritages);
			RaceCheckBoxList.BindEnumListToControlList(criteria.ReportRaces);
    }

    /// <summary>
    /// Unbinds this instance.
    /// </summary>
    /// <returns></returns>
    public JobSeekerCharacteristicsReportCriteria Unbind()
    {
      var criteria = new JobSeekerCharacteristicsReportCriteria
                       {
												 Genders = GenderCheckBoxList.Items.Cast<ListItem>().Where(x => x.Selected).Select(
                           x => (Genders?) Enum.Parse(typeof (Genders), x.Value)).ToList(),
                         EthnicHeritages =
                           EthinicityHeritageCheckBoxList.Items.Cast<ListItem>().Where(x => x.Selected).Select(
                             x => (ReportEthnicHeritage?) Enum.Parse(typeof (ReportEthnicHeritage), x.Value)).ToList(),
                         ReportRaces = RaceCheckBoxList.Items.Cast<ListItem>().Where(x => x.Selected).Select(
                           x => (ReportRace?) Enum.Parse(typeof (ReportRace), x.Value)).ToList(),
                         DisabilityTypes = DisabilityTypeCheckBoxList.Items.Cast<ListItem>().Where(x => x.Selected).Select(
                         x => (ReportDisabilityType?)Enum.Parse(typeof(ReportDisabilityType), x.Value)).ToList()
                       };
      if (AgeFrom.Text.Trim().Length > 0)
        criteria.MinimumAge = Convert.ToInt32(AgeFrom.Text);
      if (AgeTo.Text.Trim().Length > 0)
        criteria.MaximumAge = Convert.ToInt32(AgeTo.Text);

      return criteria;
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      #region labels
      CriteriaJobSeekerCharacteristicsLabel.Text = CodeLocalise("CriteriaJobSeekerCharacteristicsLabel.Text", "Job seeker characteristics");
      AgeHeader.Text = CodeLocalise("AgeHeader.Text", "Age");
      GenderHeader.Text = CodeLocalise("GenderHeader.Text", "Gender");
      EthinicityHeritageHeader.Text = CodeLocalise("EthnicityHeritage.Text", "Ethnicity/Heritage");
      RaceHeader.Text = CodeLocalise("RaceHeader.Text", "Race");
      DisabilityTypeHeader.Text = CodeLocalise("DisabilityTypeHeader.Text", "Disability status");
      #endregion

      #region error messages

      AgeFromTypeValidator.ErrorMessage = CodeLocalise("JobSeekerCharacteristics.AgeFromTypeValidator.ErrorMessage",
                                                        "Job seeker characteristics - \"Age From\" must be a valid number");
      AgeToTypeValidator.ErrorMessage = CodeLocalise("JobSeekerCharacteristics.AgeToTypeValidator.ErrorMessage",
                                                        "Job seeker characteristics - \"Age To\" must be a valid number");
      AgeFromValueValidator.ErrorMessage = CodeLocalise("JobSeekerCharacteristics.AgeFromValueValidator.ErrorMessage",
                                                        "Job seeker characteristics - \"Age From\" must be greater than 0");
      AgeToValueValidator.ErrorMessage = CodeLocalise("JobSeekerCharacteristics.AgeToValueValidator.ErrorMessage",
                                                        "Job seeker characteristics - \"Age To\" must be greater than 0");
      AgeToFromComparisonValidator.ErrorMessage = CodeLocalise("JobSeekerCharacteristics.AgeToFromComparisonValidator.ErrorMessage",
                                                        "Job seeker characteristics - \"Age To\" must be greater than or equal to \"Age From\"");

      #endregion
    }

    /// <summary>
    /// Binds this instance.
    /// </summary>
    private void Bind()
    {
			GenderCheckBoxList.Items.Add(new ListItem(CodeLocalise(Genders.Female, true), Genders.Female.ToString()));
			GenderCheckBoxList.Items.Add(new ListItem(CodeLocalise(Genders.Male, true), Genders.Male.ToString()));
			GenderCheckBoxList.Items.Add(new ListItem(CodeLocalise(Genders.NotDisclosed, true), Genders.NotDisclosed.ToString()));

			EthinicityHeritageCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportEthnicHeritage.HispanicLatino, true), ReportEthnicHeritage.HispanicLatino.ToString()));
			EthinicityHeritageCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportEthnicHeritage.NonHispanicLatino, true), ReportEthnicHeritage.NonHispanicLatino.ToString()));
			EthinicityHeritageCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportEthnicHeritage.NotDisclosed, true), ReportEthnicHeritage.NotDisclosed.ToString()));

			RaceCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportRace.AlaskanAmericanIndian, true), ReportRace.AlaskanAmericanIndian.ToString()));
			RaceCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportRace.Asian, true), ReportRace.Asian.ToString()));
			RaceCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportRace.BlackAfricanAmerican, true), ReportRace.BlackAfricanAmerican.ToString()));
			RaceCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportRace.HawaiianPacificIslander, true), ReportRace.HawaiianPacificIslander.ToString()));
			RaceCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportRace.White, true), ReportRace.White.ToString()));
			RaceCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportRace.NotDisclosed, true), ReportRace.NotDisclosed.ToString()));

            DisabilityTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportDisabilityType.ChronicHealthConditions, true), ReportDisabilityType.ChronicHealthConditions.ToString()));
            DisabilityTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportDisabilityType.CognitiveIntellectual, true), ReportDisabilityType.CognitiveIntellectual.ToString()));
            DisabilityTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportDisabilityType.HearingRelatedImpairment, true), ReportDisabilityType.HearingRelatedImpairment.ToString()));
            DisabilityTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportDisabilityType.LearningDisability, true), ReportDisabilityType.LearningDisability.ToString()));
            DisabilityTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportDisabilityType.PhysicalMobilityImpairment, true), ReportDisabilityType.PhysicalMobilityImpairment.ToString()));
            DisabilityTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportDisabilityType.PsychiatricEmotional, true), ReportDisabilityType.PsychiatricEmotional.ToString()));
            DisabilityTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportDisabilityType.VisionRelatedImpairment, true), ReportDisabilityType.VisionRelatedImpairment.ToString()));
            DisabilityTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise(ReportDisabilityType.NotDisclosed, true), ReportDisabilityType.NotDisclosed.ToString()));
		}
  }
}