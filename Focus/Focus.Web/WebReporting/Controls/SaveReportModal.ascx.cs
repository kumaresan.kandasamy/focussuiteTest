﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common;
using Focus.Core.Criteria.Report;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class SaveReportModal : UserControlBase
	{
    public delegate IReportCriteria GetCriteriaForSave(object sender, EventArgs eventArgs);

    public event GetCriteriaForSave GetCriteriaForSaveEvent;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack) Localise();
    }

    /// <summary>
    /// Handles the Click event of the btnSaveReport control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SaveReportButton_Click(object sender, EventArgs e)
    {
      if (GetCriteriaForSaveEvent != null)
      {
        var reportCriteria = GetCriteriaForSaveEvent(this, e);
        ServiceClientLocator.ReportClient(App).SaveReport(ReportNameTextBox.Text.Trim(), reportCriteria, AddToDashboardCheckbox.Checked);

        SaveReportModalExtender.Hide();

        SaveConfirmation.Visible = true;
        SaveConfirmation.Show(CodeLocalise("SaveReportModal.Header", "Confirmation"),
                              CodeLocalise("SaveReportModal.Body", "The report has been saved"),
                              CodeLocalise("CloseModal.Text", "OK"), height: 75);
      }
    }

    /// <summary>
    /// Shows the modal
    /// </summary>
    /// <param name="addToDashboard">Whether the Add to Dashboard button should be ticked</param>
    public void Show(bool addToDashboard = false)
    {
      ResetFields(addToDashboard);
      SaveReportModalExtender.Show();
    }


    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      ReportNameRequired.ErrorMessage = CodeLocalise("ReportNameRequired.ErrorMessage", "Report name required");
      SaveReportButton.Text = CodeLocalise("Save", "Save");
    }

    /// <summary>
    /// Resets the fields.
    /// </summary>
    /// <param name="addToDashboard">Whether the Add to Dashboard button should be ticked</param>
    private void ResetFields(bool addToDashboard)
    {
      ReportNameTextBox.Text = string.Empty;
      AddToDashboardCheckbox.Checked = addToDashboard;
    }
  }
}