﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TotalsDisplay.ascx.cs" Inherits="Focus.Web.WebReporting.Controls.TotalsDisplay" %>

<table style="width: 100%;" class="table" role="presentation">
  <tr>
    <asp:Repeater runat="server" ID="HeaderCellsRepeater" OnItemDataBound="HeaderCellsRepeater_OnItemDataBound">
      <ItemTemplate>
        <th><asp:Literal runat="server" ID="HeaderCell">Header</asp:Literal>
      </ItemTemplate>
    </asp:Repeater>
  </tr>
  <tr>
    <asp:Repeater runat="server" ID="BodyCellsRepeater" OnItemDataBound="BodyCellsRepeater_OnItemDataBound">
      <ItemTemplate>
        <td><asp:Literal runat="server" ID="TotalCell">0</asp:Literal></td>
      </ItemTemplate>
    </asp:Repeater>
  </tr>
</table>