﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Report;
using Focus.Core.Views;
using Focus.Web.Code;
using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class EmployerReportList : ReportControl
	{

    private int _employers;

    public EmployersReportCriteria ReportCriteria
    {
      get { return GetViewStateValue<EmployersReportCriteria>("EmployerReportList:ReportCriteria"); }
      set { SetViewStateValue("EmployerReportList:ReportCriteria", value); }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
        Localise();
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      EditColumnsLink.Text = CodeLocalise("EditColumnsLink.Text", "edit table columns");
    }

    public void Bind(EmployersReportCriteria criteria)
    {
      Visible = true;
      ReportCriteria = criteria;
      EmployerList.DataBind();
      BindSortingArrows();
    }

    protected void EditColumnsLink_Clicked(object sender, EventArgs e)
    {
      ColumnSelector.Show(ReportCriteria);
    }

    protected void EmployerList_ItemDatabound(object sender, ListViewItemEventArgs e)
    {
      var employer = (EmployersReportView)e.Item.DataItem;
      var employerLink = (LinkButton)e.Item.FindControl("EmployerLink");
      employerLink.Text = employer.Name;
      employerLink.CommandArgument = employer.FocusBusinessUnitId.ToString(CultureInfo.InvariantCulture);

      ((Literal)e.Item.FindControl("StateLiteral")).Text = employer.State;
      ((Literal)e.Item.FindControl("CountyLiteral")).Text = employer.County;

      BuildOfficeField(employer.Office, e.Item);

      ((Literal)e.Item.FindControl("JobOrdersEditedLiteral")).Text = employer.Totals.JobOrdersEdited.ToString(CultureInfo.CurrentUICulture);
      ((Literal)e.Item.FindControl("JobSeekersInterviewedLiteral")).Text = employer.Totals.JobSeekersInterviewed.ToString(CultureInfo.CurrentUICulture);
      ((Literal)e.Item.FindControl("JobSeekersHiredLiteral")).Text = employer.Totals.JobSeekersHired.ToString(CultureInfo.CurrentUICulture);
      ((Literal)e.Item.FindControl("JobOrdersCreatedLiteral")).Text = employer.Totals.JobOrdersCreated.ToString(CultureInfo.CurrentUICulture);
      ((Literal)e.Item.FindControl("JobOrdersPostedLiteral")).Text = employer.Totals.JobOrdersPosted.ToString(CultureInfo.CurrentUICulture);
      ((Literal)e.Item.FindControl("JobOrdersPutOnHoldLiteral")).Text = employer.Totals.JobOrdersPutOnHold.ToString(CultureInfo.CurrentUICulture);
      ((Literal)e.Item.FindControl("JobOrdersRefreshedLiteral")).Text = employer.Totals.JobOrdersRefreshed.ToString(CultureInfo.CurrentUICulture);
      ((Literal)e.Item.FindControl("JobOrdersClosedLiteral")).Text = employer.Totals.JobOrdersClosed.ToString(CultureInfo.CurrentUICulture);
      ((Literal)e.Item.FindControl("InvitationsSentLiteral")).Text = employer.Totals.InvitationsSent.ToString(CultureInfo.CurrentUICulture);
      ((Literal)e.Item.FindControl("JobSeekersNotHiredLiteral")).Text = employer.Totals.JobSeekersNotHired.ToString(CultureInfo.CurrentUICulture);
      ((Literal)e.Item.FindControl("SelfReferralsLiteral")).Text = employer.Totals.SelfReferrals.ToString(CultureInfo.CurrentUICulture);
      ((Literal)e.Item.FindControl("StaffReferralsLiteral")).Text = employer.Totals.StaffReferrals.ToString(CultureInfo.CurrentUICulture);

      SetCellVisibility(e.Item);
    }

    protected void EmployerList_DataBound(object sender, EventArgs e)
    {
      if (EmployerList.Items.IsNullOrEmpty())
      {
        EmployerListPager.Visible = EditColumnsLink.Visible = false;
        return; // No data so empty data template is displayed
      }
      EmployerListPager.Visible = EditColumnsLink.Visible = true;

      ((Literal)EmployerList.FindControl("EmployerHeader")).Text = CodeLocalise("EmployerHeader.Text", Constants.Reporting.EmployerReportDataColumns.Name);
      ((ImageButton)EmployerList.FindControl("EmployerSortAscButton")).CommandArgument = ReportEmployerOrderBy.Name + " " + ReportOrder.Ascending;
      ((ImageButton)EmployerList.FindControl("EmployerSortDescButton")).CommandArgument = ReportEmployerOrderBy.Name + " " + ReportOrder.Descending;
			
      ((Literal)EmployerList.FindControl("CountyHeader")).Text = CodeLocalise("CountyHeader.Text", Constants.Reporting.JobOrderReportDataColumns.County);
      ((ImageButton)EmployerList.FindControl("CountySortAscButton")).CommandArgument = ReportJobOrderOrderBy.County + " " + ReportOrder.Ascending;
      ((ImageButton)EmployerList.FindControl("CountySortDescButton")).CommandArgument = ReportJobOrderOrderBy.County + " " + ReportOrder.Descending;
			
      ((Literal)EmployerList.FindControl("StateHeader")).Text = CodeLocalise("StateHeader.Text", Constants.Reporting.EmployerReportDataColumns.State);
      ((ImageButton)EmployerList.FindControl("StateSortAscButton")).CommandArgument = ReportEmployerOrderBy.State + " " + ReportOrder.Ascending;
      ((ImageButton)EmployerList.FindControl("StateSortDescButton")).CommandArgument = ReportEmployerOrderBy.State + " " + ReportOrder.Descending;
			
      ((Literal)EmployerList.FindControl("OfficeHeader")).Text = CodeLocalise("OfficeHeader.Text", Constants.Reporting.EmployerReportDataColumns.Office);
      //((ImageButton)EmployerList.FindControl("OfficeSortAscButton")).CommandArgument = ReportEmployerOrderBy.Office + " " + ReportOrder.Ascending;
      //((ImageButton)EmployerList.FindControl("OfficeSortDescButton")).CommandArgument = ReportEmployerOrderBy.Office + " " + ReportOrder.Descending;

      ((Literal)EmployerList.FindControl("JobOrdersEditedHeader")).Text = CodeLocalise("JobOrdersEditedHeader.Text", Constants.Reporting.EmployerReportDataColumns.JobOrdersEdited);
      ((ImageButton)EmployerList.FindControl("JobOrdersEditedSortAscButton")).CommandArgument = ReportEmployerOrderBy.JobOrdersEdited + " " + ReportOrder.Ascending;
      ((ImageButton)EmployerList.FindControl("JobOrdersEditedSortDescButton")).CommandArgument = ReportEmployerOrderBy.JobOrdersEdited + " " + ReportOrder.Descending;
			
      ((Literal)EmployerList.FindControl("JobSeekersInterviewedHeader")).Text = CodeLocalise("JobSeekersInterviewedHeader.Text", Constants.Reporting.EmployerReportDataColumns.JobSeekersInterviewed);
      ((ImageButton)EmployerList.FindControl("JobSeekersInterviewedSortAscButton")).CommandArgument = ReportEmployerOrderBy.JobSeekersInterviewed + " " + ReportOrder.Ascending;
      ((ImageButton)EmployerList.FindControl("JobSeekersInterviewedSortDescButton")).CommandArgument = ReportEmployerOrderBy.JobSeekersInterviewed + " " + ReportOrder.Descending;
			
      ((Literal)EmployerList.FindControl("JobSeekersHiredHeader")).Text = CodeLocalise("JobSeekersHiredHeader.Text", Constants.Reporting.EmployerReportDataColumns.JobSeekersHired);
      ((ImageButton)EmployerList.FindControl("JobSeekersHiredSortAscButton")).CommandArgument = ReportEmployerOrderBy.JobSeekersHired + " " + ReportOrder.Ascending;
      ((ImageButton)EmployerList.FindControl("JobSeekersHiredSortDescButton")).CommandArgument = ReportEmployerOrderBy.JobSeekersHired + " " + ReportOrder.Descending;
			
      ((Literal)EmployerList.FindControl("JobOrdersCreatedHeader")).Text = CodeLocalise("JobOrdersCreatedHeader.Text", Constants.Reporting.EmployerReportDataColumns.JobOrdersCreated);
      ((ImageButton)EmployerList.FindControl("JobOrdersCreatedSortAscButton")).CommandArgument = ReportEmployerOrderBy.JobOrdersCreated + " " + ReportOrder.Ascending;
      ((ImageButton)EmployerList.FindControl("JobOrdersCreatedSortDescButton")).CommandArgument = ReportEmployerOrderBy.JobOrdersCreated + " " + ReportOrder.Descending;
			
      ((Literal)EmployerList.FindControl("JobOrdersPostedHeader")).Text = CodeLocalise("JobOrdersPostedHeader.Text", Constants.Reporting.EmployerReportDataColumns.JobOrdersPosted);
      ((ImageButton)EmployerList.FindControl("JobOrdersPostedSortAscButton")).CommandArgument = ReportEmployerOrderBy.JobOrdersPosted + " " + ReportOrder.Ascending;
      ((ImageButton)EmployerList.FindControl("JobOrdersPostedSortDescButton")).CommandArgument = ReportEmployerOrderBy.JobOrdersPosted + " " + ReportOrder.Descending;
			
      ((Literal)EmployerList.FindControl("JobOrdersPutOnHoldHeader")).Text = CodeLocalise("JobOrdersPutOnHoldHeader.Text", Constants.Reporting.EmployerReportDataColumns.JobOrdersPutOnHold);
      ((ImageButton)EmployerList.FindControl("JobOrdersPutOnHoldSortAscButton")).CommandArgument = ReportEmployerOrderBy.JobOrdersPutOnHold + " " + ReportOrder.Ascending;
      ((ImageButton)EmployerList.FindControl("JobOrdersPutOnHoldSortDescButton")).CommandArgument = ReportEmployerOrderBy.JobOrdersPutOnHold + " " + ReportOrder.Descending;
			
      ((Literal)EmployerList.FindControl("JobOrdersRefreshedHeader")).Text = CodeLocalise("JobOrdersRefreshedHeader.Text", Constants.Reporting.EmployerReportDataColumns.JobOrdersRefreshed);
      ((ImageButton)EmployerList.FindControl("JobOrdersRefreshedSortAscButton")).CommandArgument = ReportEmployerOrderBy.JobOrdersRefreshed + " " + ReportOrder.Ascending;
      ((ImageButton)EmployerList.FindControl("JobOrdersRefreshedSortDescButton")).CommandArgument = ReportEmployerOrderBy.JobOrdersRefreshed + " " + ReportOrder.Descending;
			
      ((Literal)EmployerList.FindControl("JobOrdersClosedHeader")).Text = CodeLocalise("JobOrdersClosedHeader.Text", Constants.Reporting.EmployerReportDataColumns.JobOrdersClosed);
      ((ImageButton)EmployerList.FindControl("JobOrdersClosedSortAscButton")).CommandArgument = ReportEmployerOrderBy.JobOrdersClosed + " " + ReportOrder.Ascending;
      ((ImageButton)EmployerList.FindControl("JobOrdersClosedSortDescButton")).CommandArgument = ReportEmployerOrderBy.JobOrdersClosed + " " + ReportOrder.Descending;
			
      ((Literal)EmployerList.FindControl("InvitationsSentHeader")).Text = CodeLocalise("InvitationsSentHeader.Text", Constants.Reporting.EmployerReportDataColumns.InvitationsSent);
      ((ImageButton)EmployerList.FindControl("InvitationsSentSortAscButton")).CommandArgument = ReportEmployerOrderBy.InvitationsSent + " " + ReportOrder.Ascending;
      ((ImageButton)EmployerList.FindControl("InvitationsSentSortDescButton")).CommandArgument = ReportEmployerOrderBy.InvitationsSent + " " + ReportOrder.Descending;
			
      ((Literal)EmployerList.FindControl("JobSeekersNotHiredHeader")).Text = CodeLocalise("JobSeekersNotHiredHeader.Text", Constants.Reporting.EmployerReportDataColumns.JobSeekersNotHired);
      ((ImageButton)EmployerList.FindControl("JobSeekersNotHiredSortAscButton")).CommandArgument = ReportEmployerOrderBy.JobSeekersNotHired + " " + ReportOrder.Ascending;
      ((ImageButton)EmployerList.FindControl("JobSeekersNotHiredSortDescButton")).CommandArgument = ReportEmployerOrderBy.JobSeekersNotHired + " " + ReportOrder.Descending;
			
      ((Literal)EmployerList.FindControl("SelfReferralsHeader")).Text = CodeLocalise("SelfReferralsHeader.Text", Constants.Reporting.EmployerReportDataColumns.SelfReferrals);
      ((ImageButton)EmployerList.FindControl("SelfReferralsSortAscButton")).CommandArgument = ReportEmployerOrderBy.SelfReferrals + " " + ReportOrder.Ascending;
      ((ImageButton)EmployerList.FindControl("SelfReferralsSortDescButton")).CommandArgument = ReportEmployerOrderBy.SelfReferrals + " " + ReportOrder.Descending;
			
      ((Literal)EmployerList.FindControl("StaffReferralsHeader")).Text = CodeLocalise("StaffReferralsHeader.Text", Constants.Reporting.EmployerReportDataColumns.StaffReferrals);
      ((ImageButton)EmployerList.FindControl("StaffReferralsSortAscButton")).CommandArgument = ReportEmployerOrderBy.StaffReferrals + " " + ReportOrder.Ascending;
      ((ImageButton)EmployerList.FindControl("StaffReferralsSortDescButton")).CommandArgument = ReportEmployerOrderBy.StaffReferrals + " " + ReportOrder.Descending;
			
      SetHeaderVisibility();
    }

    protected void EmployerList_Sorting(object sender, ListViewSortEventArgs e)
    {
      if (e.SortExpression.IsNullOrEmpty())
        throw new Exception("Job seeker report sorting error.");

      var sortingParams = e.SortExpression.Split(Convert.ToChar(" "));

      if (sortingParams.IsNullOrEmpty() || sortingParams.GetLength(0) != 2)
        throw new Exception("Job seeker report sorting error getting parameters.");

      ReportEmployerOrderBy orderby;
      ReportOrder orderDirection;

      if (!Enum.TryParse(sortingParams[0], out orderby))
        throw new Exception("Job seeker report sorting error getting order by values.");
      if (!Enum.TryParse(sortingParams[1], out orderDirection))
        throw new Exception("Job seeker report sorting error getting order direction values.");

      ReportCriteria.OrderInfo = new EmployersReportSortOrder { OrderBy = orderby, Direction = orderDirection };

      BindSortingArrows();
    }

    protected void ColumnSelector_ReportColumnsChanged(object sender, CommandEventArgs eventargs)
    {
      Bind((EmployersReportCriteria)eventargs.CommandArgument);
      OnReportCriteriaChanged(eventargs);
    }

    protected void EmployerListDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
      e.InputParameters["criteria"] = ReportCriteria;
    }

    protected void EmployerLink_OnCommand(object sender, CommandEventArgs e)
    {
      var employerId = long.Parse(e.CommandArgument.ToString());
      var reportId = ServiceClientLocator.ReportClient(App).SaveReportToSession(ReportCriteria);

      Response.Redirect(UrlBuilder.ReportingEmployerProfile(employerId, reportId.ToString(CultureInfo.InvariantCulture)));
    }

    #region helpers

    private void SetHeaderVisibility()
    {
      EmployerList.FindControl("JobOrdersEditedColumnHeader").Visible = ReportCriteria.EmployersEmployerActivityInfo.JobOrdersEdited;
      EmployerList.FindControl("JobSeekersInterviewedColumnHeader").Visible = ReportCriteria.EmployersEmployerActivityInfo.JobSeekersInterviewed;
      EmployerList.FindControl("JobSeekersHiredColumnHeader").Visible = ReportCriteria.EmployersEmployerActivityInfo.JobSeekersHired;
      EmployerList.FindControl("JobOrdersCreatedColumnHeader").Visible = ReportCriteria.EmployersEmployerActivityInfo.JobOrdersCreated;
      EmployerList.FindControl("JobOrdersPostedColumnHeader").Visible = ReportCriteria.EmployersEmployerActivityInfo.JobOrdersPosted;
      EmployerList.FindControl("JobOrdersPutOnHoldColumnHeader").Visible = ReportCriteria.EmployersEmployerActivityInfo.JobOrdersPutOnHold;
      EmployerList.FindControl("JobOrdersRefreshedColumnHeader").Visible = ReportCriteria.EmployersEmployerActivityInfo.JobOrdersRefreshed;
      EmployerList.FindControl("JobOrdersClosedColumnHeader").Visible = ReportCriteria.EmployersEmployerActivityInfo.JobOrdersClosed;
      EmployerList.FindControl("InvitationsSentColumnHeader").Visible = ReportCriteria.EmployersEmployerActivityInfo.InvitationsSent;
      EmployerList.FindControl("JobSeekersNotHiredColumnHeader").Visible = ReportCriteria.EmployersEmployerActivityInfo.JobSeekersNotHired;
      EmployerList.FindControl("SelfReferralsColumnHeader").Visible = ReportCriteria.EmployersEmployerActivityInfo.SelfReferrals;
      EmployerList.FindControl("StaffReferralsColumnHeader").Visible = ReportCriteria.EmployersEmployerActivityInfo.StaffReferrals;
    }

    private void SetCellVisibility(ListViewItem item)
    {
      item.FindControl("JobOrdersEditedCell").Visible = ReportCriteria.EmployersEmployerActivityInfo.JobOrdersEdited;
      item.FindControl("JobSeekersInterviewedCell").Visible = ReportCriteria.EmployersEmployerActivityInfo.JobSeekersInterviewed;
      item.FindControl("JobSeekersHiredCell").Visible = ReportCriteria.EmployersEmployerActivityInfo.JobSeekersHired;
      item.FindControl("JobOrdersCreatedCell").Visible = ReportCriteria.EmployersEmployerActivityInfo.JobOrdersCreated;
      item.FindControl("JobOrdersPostedCell").Visible = ReportCriteria.EmployersEmployerActivityInfo.JobOrdersPosted;
      item.FindControl("JobOrdersPutOnHoldCell").Visible = ReportCriteria.EmployersEmployerActivityInfo.JobOrdersPutOnHold;
      item.FindControl("JobOrdersRefreshedCell").Visible = ReportCriteria.EmployersEmployerActivityInfo.JobOrdersRefreshed;
      item.FindControl("JobOrdersClosedCell").Visible = ReportCriteria.EmployersEmployerActivityInfo.JobOrdersClosed;
      item.FindControl("InvitationsSentCell").Visible = ReportCriteria.EmployersEmployerActivityInfo.InvitationsSent;
      item.FindControl("JobSeekersNotHiredCell").Visible = ReportCriteria.EmployersEmployerActivityInfo.JobSeekersNotHired;
      item.FindControl("SelfReferralsCell").Visible = ReportCriteria.EmployersEmployerActivityInfo.SelfReferrals;
      item.FindControl("StaffReferralsCell").Visible = ReportCriteria.EmployersEmployerActivityInfo.StaffReferrals;
    }

    #endregion

    #region list binding methods

    public List<EmployersReportView> GetEmployerList(EmployersReportCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
    {
      var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

      criteria.PageSize = maximumRows;
      criteria.PageIndex = pageIndex;

      var employers = ServiceClientLocator.ReportClient(App).GetEmployersReport(criteria);
      _employers = employers.TotalCount;

      return employers;
    }


    public int GetEmployerListCount(EmployersReportCriteria criteria)
    {
      return _employers;
    }


    public int GetEmployerListCount()
    {
      return _employers;
    }

    #endregion

    #region list formatting

    private void BindSortingArrows()
    {
      FormatSortingButtons((ImageButton)EmployerList.FindControl("EmployerSortAscButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("EmployerSortDescButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("CountySortAscButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("CountySortDescButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("StateSortAscButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("StateSortDescButton"));
      //FormatSortingButtons((ImageButton)EmployerList.FindControl("OfficeSortAscButton"));
      //FormatSortingButtons((ImageButton)EmployerList.FindControl("OfficeSortDescButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("JobOrdersEditedSortAscButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("JobOrdersEditedSortDescButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("JobSeekersInterviewedSortAscButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("JobSeekersInterviewedSortDescButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("JobSeekersHiredSortAscButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("JobSeekersHiredSortDescButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("JobOrdersCreatedSortAscButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("JobOrdersCreatedSortDescButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("JobOrdersPostedSortAscButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("JobOrdersPostedSortDescButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("JobOrdersPutOnHoldSortAscButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("JobOrdersPutOnHoldSortDescButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("JobOrdersRefreshedSortAscButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("JobOrdersRefreshedSortDescButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("JobOrdersClosedSortAscButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("JobOrdersClosedSortDescButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("InvitationsSentSortAscButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("InvitationsSentSortDescButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("JobSeekersNotHiredSortAscButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("JobSeekersNotHiredSortDescButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("SelfReferralsSortAscButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("SelfReferralsSortDescButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("StaffReferralsSortAscButton"));
      FormatSortingButtons((ImageButton)EmployerList.FindControl("StaffReferralsSortDescButton"));
    }

    private void FormatSortingButtons(ImageButton button)
    {
      if (button.IsNull()) return;

      var sortingParams = button.CommandArgument.Split(Convert.ToChar(" "));

      if (sortingParams.IsNullOrEmpty() || sortingParams.GetLength(0) != 2)
        throw new Exception("Job seeker report sorting error getting parameters.");

      ReportEmployerOrderBy orderby;
      ReportOrder orderDirection;

      if (!Enum.TryParse(sortingParams[0], out orderby))
        throw new Exception("Job seeker report sorting error getting order by values.");
      if (!Enum.TryParse(sortingParams[1], out orderDirection))
        throw new Exception("Job seeker report sorting error getting order direction values.");

      if (orderby == ReportCriteria.OrderInfo.OrderBy && orderDirection == ReportCriteria.OrderInfo.Direction)
      {
        button.Enabled = false;
        button.ImageUrl = orderDirection == ReportOrder.Ascending ? UrlBuilder.ActivityListSortAscImage() : UrlBuilder.ActivityListSortDescImage();
      }
      else
      {
        button.Enabled = true;
        button.ImageUrl = orderDirection == ReportOrder.Ascending ? UrlBuilder.ActivityOnSortAscImage() : UrlBuilder.CategorySortDescImage();
      }
			button.ToolTip = button.AlternateText = orderDirection == ReportOrder.Ascending
				? CodeLocalise("Global.SortAscending", "Sort ascending")
				: CodeLocalise("Global.SortDescending", "Sort descending");
    }


    #endregion


    #region page generated events

    public delegate void ReportCriteriaChangedHandler(object sender, CommandEventArgs eventArgs);
    public event ReportCriteriaChangedHandler ReportCriteriaChanged;

    /// <summary>
    /// Raises the <see cref="CommandEventArgs" /> event.
    /// </summary>
    /// <param name="eventargs">The <see cref="CommandEventArgs" /> instance containing the event data.</param>
    public void OnReportCriteriaChanged(CommandEventArgs eventargs)
    {
      var handler = ReportCriteriaChanged;
      if (handler != null) handler(this, eventargs);
    }

    #endregion
	}
}