﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaEmployerActivity.ascx.cs" Inherits="Focus.Web.WebReporting.Controls.CriteriaEmployerActivity" %>
<table role="presentation" style="width:100%;" class="accordionWithIcon">
    <tr class="multipleAccordionTitle">
        <td style="vertical-align:top; width:10%;">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td style="width:90%;">
            <asp:Literal runat="server" ID="CriteriaEmployerActivityLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
            <table role="presentation" style="width:100%;">
                <tr>
                    <td>
                        <strong><asp:Literal runat="server" ID="JobOrderHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="JobOrdersClosedCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="JobOrdersCreatedCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="JobOrdersEditedCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="InvitationsSentCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="JobOrdersPutOnHoldCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="JobOrdersPostedCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="JobOrdersRefreshedCheckBox"/></td>
                </tr>
            </table>
            <table role="presentation" style="width:100%;">
                <tr>
                    <td>
                        <strong><asp:Literal runat="server" ID="SeekerHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="SelfReferralsCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="StaffReferralsCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="JobSeekersInterviewedCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="JobSeekersHiredCheckBox"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="JobSeekersNotHiredCheckBox"/></td>
                </tr>
            </table>
        </td>
    </tr>
</table>