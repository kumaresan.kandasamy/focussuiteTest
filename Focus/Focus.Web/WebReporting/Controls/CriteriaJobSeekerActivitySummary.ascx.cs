﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core;
using Framework.Core;

using Focus.Core.Criteria.Report;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaJobSeekerActivitySummary : UserControlBase
	{
    public bool ShowJobSeekerAccountsCritria { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();

        AccountsRow.Visible = ShowJobSeekerAccountsCritria;
      }
    }

    /// <summary>
    /// Resets this instance.
    /// </summary>
    private void Reset()
    {
      JobSeekerAccountsCheckBox.Checked =
        JobSeekerSignInsCheckBox.Checked =
        PostingsViewedCheckBox.Checked =
        ReferralsRequestedCheckBox.Checked =
        StatewideStaffReferralsCheckBox.Checked =
        StatewideSelfReferralsCheckBox.Checked =
        ExternalStaffReferralsCheckBox.Checked =
        ExternalSelfReferralsCheckbox.Checked =
        TotalReferralsCheckBox.Checked =
        ReferralsApprovedCheckBox.Checked =
        TargettingHighGrowthSectorsCheckBox.Checked =
        SavingJobAlertsCheckBox.Checked =
        SurveyVerySatisfiedCheckBox.Checked =
        SurveySatisfiedCheckBox.Checked =
        SurveyDissatisfiedCheckBox.Checked =
        SurveyVeryDissatisfiedCheckBox.Checked =
        SurveyNoUnexpectedMatchesCheckBox.Checked =
        SurveyUnexpectedMatchesCheckBox.Checked =
        SurveyReceivedInvitationsCheckBox.Checked =
        SurveyDidNotReceiveInvitationsCheckBox.Checked = false;
    }

    /// <summary>
    /// Unbinds this instance.
    /// </summary>
    /// <returns></returns>
    public JobSeekerActivitySummaryCriteria Unbind(JobSeekerActivitySummaryCriteria criteria = null)
    {
      if (criteria.IsNull()) criteria = new JobSeekerActivitySummaryCriteria();
      criteria.TotalCount = JobSeekerAccountsCheckBox.Checked;
      criteria.Logins = JobSeekerSignInsCheckBox.Checked;
      criteria.PostingsViewed = PostingsViewedCheckBox.Checked;
      criteria.ReferralRequests = ReferralsRequestedCheckBox.Checked;
      criteria.StaffReferrals = StatewideStaffReferralsCheckBox.Checked;
      criteria.SelfReferrals = StatewideSelfReferralsCheckBox.Checked;
      criteria.StaffReferralsExternal = ExternalStaffReferralsCheckBox.Checked;
      criteria.SelfReferralsExternal = ExternalSelfReferralsCheckbox.Checked;
      criteria.TotalReferrals = TotalReferralsCheckBox.Checked;
      criteria.ReferralsApproved = ReferralsApprovedCheckBox.Checked;
      criteria.TargetingHighGrowthSectors = TargettingHighGrowthSectorsCheckBox.Checked;
      criteria.SavedJobAlerts = SavingJobAlertsCheckBox.Checked;
      criteria.SurveyVerySatisfied = SurveyVerySatisfiedCheckBox.Checked;
      criteria.SurveySatisfied = SurveySatisfiedCheckBox.Checked;
      criteria.SurveyDissatisfied = SurveyDissatisfiedCheckBox.Checked;
      criteria.SurveyVeryDissatisfied = SurveyVeryDissatisfiedCheckBox.Checked;
      criteria.SurveyNoUnexpectedMatches = SurveyNoUnexpectedMatchesCheckBox.Checked;
      criteria.SurveyUnexpectedMatches = SurveyUnexpectedMatchesCheckBox.Checked;
      criteria.SurveyReceivedInvitations = SurveyReceivedInvitationsCheckBox.Checked;
      criteria.SurveyDidNotReceiveInvitations = SurveyDidNotReceiveInvitationsCheckBox.Checked;
      return criteria;
    }

    public void BindCriteria(JobSeekerActivitySummaryCriteria criteria)
    {
      Reset();

      if (criteria.IsNull()) return;

      JobSeekerAccountsCheckBox.Checked = criteria.TotalCount;
      JobSeekerSignInsCheckBox.Checked = criteria.Logins;
      PostingsViewedCheckBox.Checked = criteria.PostingsViewed;
      ReferralsRequestedCheckBox.Checked = criteria.ReferralRequests;
      StatewideStaffReferralsCheckBox.Checked = criteria.StaffReferrals;
      StatewideSelfReferralsCheckBox.Checked = criteria.SelfReferrals;
      ExternalStaffReferralsCheckBox.Checked = criteria.StaffReferralsExternal;
      ExternalSelfReferralsCheckbox.Checked = criteria.SelfReferralsExternal;
      TotalReferralsCheckBox.Checked = criteria.TotalReferrals;
      ReferralsApprovedCheckBox.Checked = criteria.ReferralsApproved;
      TargettingHighGrowthSectorsCheckBox.Checked = criteria.TargetingHighGrowthSectors;
      SavingJobAlertsCheckBox.Checked = criteria.SavedJobAlerts;
      SurveyVerySatisfiedCheckBox.Checked = criteria.SurveyVerySatisfied;
      SurveySatisfiedCheckBox.Checked = criteria.SurveySatisfied;
      SurveyDissatisfiedCheckBox.Checked = criteria.SurveyDissatisfied;
      SurveyVeryDissatisfiedCheckBox.Checked = criteria.SurveyVeryDissatisfied;
      SurveyNoUnexpectedMatchesCheckBox.Checked = criteria.SurveyNoUnexpectedMatches;
      SurveyUnexpectedMatchesCheckBox.Checked = criteria.SurveyUnexpectedMatches;
      SurveyReceivedInvitationsCheckBox.Checked = criteria.SurveyReceivedInvitations;
      SurveyDidNotReceiveInvitationsCheckBox.Checked = criteria.SurveyDidNotReceiveInvitations;
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      CriteriaJobSeekerActivitySummaryLabel.Text = CodeLocalise("CriteriaJobSeekerActivitySummaryLabel.Text", "Job seeker activity summary");
      AccountsAndSignInsHeader.Text = ShowJobSeekerAccountsCritria
                                        ? CodeLocalise("AccountsAndSignInsHeader.Text", "Accounts & sign-ins")
                                        : CodeLocalise("AccountsHeader.Text", "Accounts");

      PostingsHeader.Text = CodeLocalise("PostingsHeader.Text", "Postings");
      ReferralsHeader.Text = CodeLocalise("ReferralsHeader.Text", "Referrals");
      SeekerSearchesHeader.Text = CodeLocalise("SeekerSearchesHeader.Text", "Seeker Searches");
      SurveyResponsesHeader.Text = CodeLocalise("SurveyResponsesHeader.Text", "Survey Responses");

      JobSeekerAccountsCheckBox.Text = CodeLocalise("AccountsAndSignInsCheckBoxList.JobSeekerAccounts.Text",
                                                    "Job seeker accounts");
      JobSeekerSignInsCheckBox.Text = CodeLocalise("AccountsAndSignInsCheckBoxList.JobSeekerSignIns.Text",
                                                   "Job seeker sign ins");

      PostingsViewedCheckBox.Text = CodeLocalise("PostingsViewedCheckBox.MatchesViewed.Text", "Postings viewed");
      ThreeToFiveStaffMatchesCheckBox.Text = CodeLocalise("MatchesCheckBoxList.3-5StaffMatches.Text",
                                                          "3-5 staff matches");

      ReferralsRequestedCheckBox.Text = CodeLocalise("ReferralsCheckBoxList.Requested.Text", Constants.Reporting.JobSeekerReportDataColumns.ReferralsRequested);
      StatewideStaffReferralsCheckBox.Text = CodeLocalise("ReferralsCheckBoxList.StaffStatewide.Text",
                                                          "Staff referrals (statewide job feed)");
      ExternalStaffReferralsCheckBox.Text = CodeLocalise("ReferralsCheckBoxList.StaffExternal.Text",
                                                         "Staff referrals (external job feeds)");
      StatewideSelfReferralsCheckBox.Text = CodeLocalise("ReferralsCheckBoxList.SelfStatewide.Text",
                                                         "Self-referrals (statewide job feed)");
      ExternalSelfReferralsCheckbox.Text = CodeLocalise("ReferralsCheckBoxList.SelfExternal.Text",
                                                         "Self-referrals (external job feeds)");
      TotalReferralsCheckBox.Text = CodeLocalise("ReferralsCheckBoxList.Total.Text", Constants.Reporting.JobSeekerReportDataColumns.TotalReferrals);
      ReferralsApprovedCheckBox.Text = CodeLocalise("ReferralsCheckBoxList.Text", Constants.Reporting.JobSeekerReportDataColumns.ReferralsApproved);

      TargettingHighGrowthSectorsCheckBox.Text = CodeLocalise("SeekerSearchesCheckBoxList.HighGrowth.Text",
                                                              "Targeting high-growth sectors");
      ExcludingByPhysicalAbilityCheckBox.Text = CodeLocalise("SeekerSearchesCheckBoxList.ExcludeByPhysicalAbility.Text", 
                                                             "Excluding jobs by physical abilities");

      SavingJobAlertsCheckBox.Text = CodeLocalise("SeekerSearchesCheckBoxList.SavingJobAlerts.Text",
                                                  "Saving for job alerts");

      SurveyVerySatisfiedCheckBox.Text = CodeLocalise("SurveysCheckBox.SurveyVerySatisfied.Text", Constants.Reporting.JobSeekerReportDataColumns.SurveyVerySatisfied);
      SurveySatisfiedCheckBox.Text = CodeLocalise("SurveysCheckBox.SurveySatisfied.Text", Constants.Reporting.JobSeekerReportDataColumns.SurveySatisfied);
      SurveyDissatisfiedCheckBox.Text = CodeLocalise("SurveysCheckBox.SurveyDissatisfied.Text", Constants.Reporting.JobSeekerReportDataColumns.SurveyDissatisfied);
      SurveyVeryDissatisfiedCheckBox.Text = CodeLocalise("SurveysCheckBox.SurveyVeryDissatisfied.Text", Constants.Reporting.JobSeekerReportDataColumns.SurveyVeryDissatisfied);
      SurveyNoUnexpectedMatchesCheckBox.Text = CodeLocalise("SurveysCheckBox.SurveyNoUnexpectedMatches.Text", Constants.Reporting.JobSeekerReportDataColumns.SurveyNoUnexpectedMatches);
      SurveyUnexpectedMatchesCheckBox.Text = CodeLocalise("SurveysCheckBox.SurveyUnexpectedMatches.Text", Constants.Reporting.JobSeekerReportDataColumns.SurveyUnexpectedMatches);
      SurveyReceivedInvitationsCheckBox.Text = CodeLocalise("SurveysCheckBox.SurveyReceivedInvitations.Text", Constants.Reporting.JobSeekerReportDataColumns.SurveyReceivedInvitations);
      SurveyDidNotReceiveInvitationsCheckBox.Text = CodeLocalise("SurveysCheckBox.SurveyDidNotReceiveInvitations.Text", Constants.Reporting.JobSeekerReportDataColumns.SurveyDidNotReceiveInvitations);
    }
  }
}