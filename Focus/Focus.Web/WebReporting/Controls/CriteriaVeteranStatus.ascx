﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaVeteranStatus.ascx.cs"
    Inherits="Focus.Web.WebReporting.Controls.CriteriaVeteranStatus" %>

<table role="presentation" style="width:100%;" class="accordionWithIcon">
	<tr class="multipleAccordionTitle">
		<td style="vertical-align:top; width:10%;">
			<span class="accordionIcon" style="height: 25px"></span>
		</td>
		<td style="width:90%;">
			<asp:Literal runat="server" ID="CriteriaVeteranStatusLabel" />
		</td>
	</tr>
	<tr class="accordionContent">
		<td colspan="2">
			<table role="presentation" style="width:100%;">
				<tr>
					<td colspan="2">
						<strong>
						<asp:Literal runat="server" ID="VeteranTypeHeader"></asp:Literal></strong>
					</td>
					</tr>
				<tr>
					<td> 
						<asp:CheckBox ID="IsCampaignVeteranCheckBox" TextAlign="Right" runat="server"/><br />
						<asp:CheckBoxList ID="VeteranTypeCheckBoxList" TextAlign="Right" runat="server" RepeatLayout="Flow" />
					</td>
				</tr>
			</table>
			<table role="presentation" style="width:100%;">
				<tr>
					<td colspan="2">
						<strong>
						<asp:Literal runat="server" ID="TransitionTypeHeader"></asp:Literal></strong>
					</td>
					</tr>
				<tr>
					<td>
						<asp:CheckBoxList ID="TransitionTypeCheckBoxList" TextAlign="Right" runat="server" role="presentation"/>
					</td>
				</tr>
			</table>
			<table role="presentation" style="width:100%;">
				<tr>
					<td colspan="2">
						<strong>
						<asp:Literal runat="server" ID="MilitaryDischargeHeader"></asp:Literal></strong>
					</td>
					</tr>
				<tr>
					<td>
						<asp:CheckBoxList ID="MilitaryDischargeCheckBoxList" TextAlign="Right" runat="server" role="presentation"/>
					</td>
				</tr>
			</table>
			<table role="presentation" style="width:100%;">
				<tr>
					<td colspan="2">
						<strong>
						<asp:Literal runat="server" ID="BranchOfServiceHeader"></asp:Literal></strong>
					</td>
					</tr>
				<tr>
					<td>
						<asp:CheckBoxList ID="BranchOfServiceCheckBoxList" TextAlign="Right" runat="server" role="presentation"/>
					</td>
				</tr>
			</table>
			<table role="presentation" style="width:100%;">
				<tr>
					<td colspan="2">
						<strong>
						<asp:Literal runat="server" ID="HomelessVeteranHeader"></asp:Literal></strong>
					</td>
					</tr>
				<tr>
					<td>
						<asp:CheckBox ID="HomelessVeteran" TextAlign="Right" runat="server" role="presentation"/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>