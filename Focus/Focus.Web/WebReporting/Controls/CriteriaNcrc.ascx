﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaNcrc.ascx.cs"
    Inherits="Focus.Web.WebReporting.Controls.CriteriaNcrc" %>
<table role="presentation" style="width:100%;" class="accordionWithIcon" id="<%=this.ClientID %>">
    <tr class="multipleAccordionTitle">
        <td style="vertical-align:top; width:10%;">
            <span class="accordionIcon" style="height: 25px" id="<%=this.ClientID %>_NcrcPanelHeader"></span>
        </td>
        <td style="width:90%;">
            <asp:Literal runat="server" ID="CriteriaNcrcCredentialsLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
            <table role="presentation" style="width:100%;">
                <tr>
                    <td colspan="2">
                        <strong id="<%=this.ClientID %>_NcrcLevelHeaderLabel">
                            <asp:Literal runat="server" ID="NcrcLevelHeader"></asp:Literal></strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBoxList ID="NcrcLevelCheckboxList" TextAlign="Right" runat="server" />
                    </td>
                    <td style="vertical-align:top;">
                        <asp:CheckBoxList ID="NcrcRequiredCheckboxList" TextAlign="Right" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
