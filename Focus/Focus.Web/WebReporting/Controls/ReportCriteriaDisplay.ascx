﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportCriteriaDisplay.ascx.cs" Inherits="Focus.Web.WebReporting.Controls.ReportCriteriaDisplay" %>
<br/>
<div class="singleAccordionContentWrapper on">
<asp:Panel ID="CriteriaDisplayHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
    <asp:Image ID="CriteriaDisplayHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" Width="22" height="22" AlternateText="."/>&nbsp;&nbsp;<%= HtmlLocalise("Criteria.Label", "Criteria")%>
</asp:Panel>
<asp:Panel ID="CriteriaDisplayPanel" runat="server" CssClass="singleAccordionContent">
<div class="grid100">
  <asp:PlaceHolder runat="server" ID="CriteriaButtonsPanel">
    <div class="reportgrid50">
        <asp:LinkButton runat="server" ID="EditReportSettingsLinkButton" OnClick="EditReportSettingsLinkButton_Clicked"></asp:LinkButton>
    </div>
    <div class="reportgrid50" style="text-align: right">
        <asp:Button runat="server" CssClass="button3" ID="NewReportButton" OnClick="NewReportButton_Clicked" />
    </div>
    <div style="clear:both"></div>
  </asp:PlaceHolder>
  <asp:Repeater runat="server" ID="MainColumnsRepeater" OnItemDataBound="MainColumnsRepeater_OnItemDataBound">
    <ItemTemplate>
      <div class="reportgrid25">
        <asp:Repeater runat="server" ID="ColumnRepeater">
          <HeaderTemplate><ul></HeaderTemplate>
          <ItemTemplate><li><%# Container.DataItem %></li></ItemTemplate>
          <FooterTemplate></ul></FooterTemplate>
        </asp:Repeater>
      </div>
    </ItemTemplate>
  </asp:Repeater>
  <asp:Label runat="server" ID="NoCriteriaLabel" Visible="False"></asp:Label>
</div>
<br />
</asp:Panel>
</div>
<act:CollapsiblePanelExtender ID="CriteriaDisplayPanelExtender" runat="server" TargetControlID="CriteriaDisplayPanel" ExpandControlID="CriteriaDisplayHeaderPanel" CollapseControlID="CriteriaDisplayHeaderPanel" Collapsed="True" ImageControlID="CriteriaDisplayHeaderImage" CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>" SuppressPostBack="true" CollapsedSize="0" />
