﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaSupplyDemandLocation.ascx.cs" Inherits="Focus.Web.WebReporting.Controls.CriteriaSupplyDemandLocation" %>
<%@ Register TagPrefix="ajaxtoolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.50731.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<%@ Import Namespace="Focus" %>
<table role="presentation" style="width:100%;" class="accordionWithIcon">
    <tr class="multipleAccordionTitle" id="CriteriaLocationHeader">
        <td style="vertical-align:top; width:10%;">
            <span class="accordionIcon" style="height: 25px"></span>
        </td>
        <td style="width:90%;">
            <asp:Literal runat="server" ID="CriteriaLocationLabel" />
        </td>
    </tr>
    <tr class="accordionContent">
        <td colspan="2">
            <table role="presentation" style="width:100%;">
		          <tr>
			          <td style="width:535px;vertical-align:top;">
				          </td>
				          <td style="width:12px;"></td>
				          <td style="width:535px;vertical-align:top;">
				          <table role="presentation">
					          <tr>
						          <td colspan="3" style="vertical-align: top;">
							          <focus:LocalisedLabel runat="server" ID="ddlStateLabel" AssociatedControlID="ddlState" LocalisationKey="State" DefaultText="State" CssClass="sr-only"/>
							          <asp:DropDownList ID="ddlState" runat="server" Width="320px" ClientIDMode="Static" 
								          CssClass="StateClass" >
								          <asp:ListItem Value="">- select a state -</asp:ListItem>
							          </asp:DropDownList>
							          <div style="height: 10px;">
							          </div>
												<focus:LocalisedLabel runat="server" ID="ddlMSALabel" AssociatedControlID="ddlMSA" LocalisationKey="MSA" DefaultText="MSA" CssClass="sr-only"/>
							          <focus:AjaxDropDownList ID="ddlMSA" runat="server" Width="320px"
								          ClientIDMode="Static" TabIndex="10" Enabled="False" />
							          <ajaxtoolkit:CascadingDropDown ID="ccdMSA" ClientIDMode="Static" runat="server" Category="MSA" LoadingText="[Loading City...]"
								          ParentControlID="ddlState" ServiceMethod="GetMSA" ServicePath="~/Services/AjaxService.svc"
								          BehaviorID="ccdMSABehaviorID" TargetControlID="ddlMSA">
							          </ajaxtoolkit:CascadingDropDown>
						          </td>
					          </tr>
				          <tr>
		        </table>
	</tr>
</table>
        </td>
    </tr>
</table>
