﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Focus.Web.WebReporting.Controls {
    
    
    public partial class CriteriaNcrc {
        
        /// <summary>
        /// CriteriaNcrcCredentialsLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal CriteriaNcrcCredentialsLabel;
        
        /// <summary>
        /// NcrcLevelHeader control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal NcrcLevelHeader;
        
        /// <summary>
        /// NcrcLevelCheckboxList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBoxList NcrcLevelCheckboxList;
        
        /// <summary>
        /// NcrcRequiredCheckboxList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBoxList NcrcRequiredCheckboxList;
    }
}
