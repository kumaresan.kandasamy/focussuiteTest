﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.Report;

using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaEmployerCharacteristics : UserControlBase
	{
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        NoOfEmployeesCriteria.Visible = App.Settings.ShowEmployeeNumbers;
        Localise();
        Bind();
      }
			HandleAccountTypeDisplay();
		}

		private void HandleAccountTypeDisplay()
		{
			switch (App.Settings.AccountTypeDisplayType)
			{
				case ControlDisplayType.Optional:
					AccountTypeCheckBoxList.Visible = true;
					AccountTypeHeader.Visible = true;
					break;
				case ControlDisplayType.Mandatory:
					AccountTypeHeader.Visible = true;
					AccountTypeCheckBoxList.Items.Remove(new ListItem("Not Indicated", AccountTypes.NotIndicated.ToString()));
					AccountTypeCheckBoxList.Visible = true;
					break;
				case ControlDisplayType.Hidden:
					AccountTypeCheckBoxList.Visible = false;
					AccountTypeHeader.Visible = false;
					break;
			}
		  if (App.Settings.Theme == FocusThemes.Education)
		  {
        AccountTypeCheckBoxList.Visible = false;
        AccountTypeHeader.Visible = false;
		  }
		}

	  /// <summary>
    /// Binds the controls to the criteria
    /// </summary>
    /// <param name="criteria">The criteria to which to bind</param>
    public void BindCriteria(EmployerCharacteristicsReportCriteria criteria)
    {
      Reset();

      const string delimiter = "|*|";

      if (criteria.IsNull())
        return;

      WOTCInterestCheckBoxList.BindEnumListToControlList(criteria.WorkOpportunitiesTaxCreditInterests);
			AccountTypeCheckBoxList.BindEnumListToControlList(criteria.AccountTypesList);

      if (criteria.FEINs.IsNotNullOrEmpty())
        HiddenFEINList.Value = string.Join(delimiter, criteria.FEINs);
      if (criteria.EmployerNames.IsNotNullOrEmpty())
        HiddenEmployersList.Value = string.Join(delimiter, criteria.EmployerNames);
      if (criteria.JobTitles.IsNotNullOrEmpty())
        HiddenJobTitlesList.Value = string.Join(delimiter, criteria.JobTitles);

      if (criteria.NoOfEmployees.IsNotNull() && criteria.NoOfEmployees != 0)
        NumberOfEmployeesDropDownList.SelectValue(criteria.NoOfEmployees.Value.ToString(CultureInfo.InvariantCulture));
    }

    /// <summary>
    /// Gets the criteria object based on the control values
    /// </summary>
    /// <returns>The criteria</returns>
    public EmployerCharacteristicsReportCriteria Unbind()
    {
      var delimiter = new[] { "|*|" };
      
      var criteria = new EmployerCharacteristicsReportCriteria
      {
        WorkOpportunitiesTaxCreditInterests = WOTCInterestCheckBoxList.UnbindNullableEnumControlList<WorkOpportunitiesTaxCreditCategories>(),
				AccountTypesList =  AccountTypeCheckBoxList.UnbindNullableEnumControlList<AccountTypes>(),
        FEINs = HiddenFEINList.Value.Split(delimiter, StringSplitOptions.RemoveEmptyEntries).ToList(),
        EmployerNames = HiddenEmployersList.Value.Split(delimiter, StringSplitOptions.RemoveEmptyEntries).ToList(),
        JobTitles = HiddenJobTitlesList.Value.Split(delimiter, StringSplitOptions.RemoveEmptyEntries).ToList(),
        NoOfEmployees = NumberOfEmployeesDropDownList.SelectedValueToLong(),
        NoOfEmployeesText = NumberOfEmployeesDropDownList.SelectedItem.Text
      };

      return criteria;
    }

    /// <summary>
    /// Clears the controls on the page
    /// </summary>
    private void Reset()
    {
      WOTCInterestCheckBoxList.Items.Reset();
			AccountTypeCheckBoxList.Items.Reset();
      FEINTextBox.Text = EmployersTextBox.Text = JobTitlesTextBox.Text = string.Empty;
      HiddenFEINList.Value = HiddenEmployersList.Value = HiddenJobTitlesList.Value = string.Empty;
    }

    /// <summary>
    /// Localises the text
    /// </summary>
    private void Localise()
    {
      CriteriaEmployerCharacteristicsLabel.Text = CodeLocalise("CriteriaEmployerCharacteristicsLabel.Text", "#BUSINESS# characteristics");
      WOTCInterestHeader.Text = CodeLocalise("WOTCInterestHeader.Text", "WOTC interest");
	    AccountTypeHeader.Text = CodeLocalise("AccountTypeHeader.Text", "Account Type");

      FEINButton.Value = EmployersButton.Value = JobTitlesButton.Value = CodeLocalise("Global.Add.NoEdit", "Add");
    }

    /// <summary>
    /// Binds this instance.
    /// </summary>
    private void Bind()
    {
      WOTCInterestCheckBoxList.Items.Add(new ListItem(CodeLocalise("TANFRecipient.Text", "TANF recipients"), WorkOpportunitiesTaxCreditCategories.TemporaryAssistanceToNeedyFamilyRecipients.ToString()));
      WOTCInterestCheckBoxList.Items.Add(new ListItem(CodeLocalise("EZAndRRC.Text", "EZ and RRC"), WorkOpportunitiesTaxCreditCategories.DesignatedCommunityResidents.ToString()));
      WOTCInterestCheckBoxList.Items.Add(new ListItem(CodeLocalise("SnapRecipient.Text", "SNAP recipients"), WorkOpportunitiesTaxCreditCategories.SNAPRecipients.ToString()));
      WOTCInterestCheckBoxList.Items.Add(new ListItem(CodeLocalise("Veterans.Text", "Veterans"), WorkOpportunitiesTaxCreditCategories.Veterans.ToString()));
      WOTCInterestCheckBoxList.Items.Add(new ListItem(CodeLocalise("VocationalRehabilitation.Text", "Vocational rehabilitation"), WorkOpportunitiesTaxCreditCategories.VocationalRehabilitation.ToString()));
      WOTCInterestCheckBoxList.Items.Add(new ListItem(CodeLocalise("SSI.Text", "SSI"), WorkOpportunitiesTaxCreditCategories.SupplementSecurityIncomeRecipients.ToString()));
      WOTCInterestCheckBoxList.Items.Add(new ListItem(CodeLocalise("ExFelons.Text", "Ex-Felons"), WorkOpportunitiesTaxCreditCategories.ExFelons.ToString()));
      WOTCInterestCheckBoxList.Items.Add(new ListItem(CodeLocalise("SummerYouth.Text", "Summer youth"), WorkOpportunitiesTaxCreditCategories.SummerYouth.ToString()));
      WOTCInterestCheckBoxList.Items.Add(new ListItem(CodeLocalise("LTFamilyAssistanceRecipients.Text", "Long term family assistance recipients"), WorkOpportunitiesTaxCreditCategories.LongTermFamilyAssistanceRecipient.ToString()));

      NumberOfEmployeesDropDownList.Items.Clear();
      NumberOfEmployeesDropDownList.Items.AddLocalised("Global.NoOfEmployees.TopDefault", "- select number of employees -", "");
      NumberOfEmployeesDropDownList.Items.AddLocalised("NoOfEmployeesRange1.Text", "1-49", "1");
      NumberOfEmployeesDropDownList.Items.AddLocalised("NoOfEmployeesRange2.Text", "50-99", "2");
      NumberOfEmployeesDropDownList.Items.AddLocalised("NoOfEmployeesRange3.Text", "100-499", "3");
      NumberOfEmployeesDropDownList.Items.AddLocalised("NoOfEmployeesRange4.Text", "500-999", "4");
      NumberOfEmployeesDropDownList.Items.AddLocalised("NoOfEmployeesRange5.Text", "1000-4999", "5");
      NumberOfEmployeesDropDownList.Items.AddLocalised("NoOfEmployeesRange6.Text", "5000+", "6");
			AccountTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise("NotIndicated.Text", "Not Indicated"), AccountTypes.NotIndicated.ToString()));
			AccountTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise("DirectEmployer.Text", "Direct #BUSINESS#"), AccountTypes.DirectEmployer.ToString()));
			AccountTypeCheckBoxList.Items.Add(new ListItem(CodeLocalise("StaffingAgency.Text", "Staffing Agency"), AccountTypes.StaffingAgency.ToString()));
    }
  }
}