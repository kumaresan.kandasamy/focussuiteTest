﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Core.Criteria.Report;

using Framework.Core;

#endregion

namespace Focus.Web.WebReporting.Controls
{
  public partial class CriteriaReferralsOutcome : UserControlBase
	{
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();
      }
    }

    /// <summary>
    /// Binds the criteria.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    public void BindCriteria(ReferralOutcomesCriteria criteria)
    {
      Reset();

      if (criteria.IsNull()) return;

      HiredCheckBox.Checked = criteria.Hired;
      NotHiredCheckBox.Checked = criteria.NotHired;
      NotYetPlacedCheckBox.Checked = criteria.NotYetPlaced;
      FailedToApplyCheckBox.Checked = criteria.FailedToApply;
      FailedToRespondCheckBox.Checked = criteria.FailedToRespondToInvitation;
      FailedToReportToInterviewCheckBox.Checked = criteria.FailedToReportToInterview;
      FailedToReportToJobCheckBox.Checked = criteria.FailedToReportToJob;
      FoundJobOtherSourceCheckBox.Checked = criteria.FoundJobFromOtherSource;
      InterviewDeniedCheckBox.Checked = criteria.InterviewDenied;
      InterviewScheduledCheckBox.Checked = criteria.InterviewScheduled;
      JobAlreadyFilledCheckBox.Checked = criteria.JobAlreadyFilled;
      NewApplicantCheckBox.Checked = criteria.NewApplicant;
      NotQualifiedCheckBox.Checked = criteria.NotQualified;
      RecommendedCheckBox.Checked = criteria.Recommended;
      RefusedReferralCheckBox.Checked = criteria.RefusedReferral;
      RefusedJobCheckBox.Checked = criteria.RefusedOffer;
      UnderConsiderationCheckBox.Checked = criteria.UnderConsideration;
    }

    /// <summary>
    /// Unbinds this instance.
    /// </summary>
    public ReferralOutcomesCriteria Unbind(ReferralOutcomesCriteria criteria = null)
    {
      if (criteria.IsNull()) criteria = new ReferralOutcomesCriteria();

      criteria.Hired = HiredCheckBox.Checked;
      criteria.NotHired = NotHiredCheckBox.Checked;
      criteria.NotYetPlaced = NotYetPlacedCheckBox.Checked;
      criteria.FailedToApply = FailedToApplyCheckBox.Checked;
      criteria.FailedToRespondToInvitation = FailedToRespondCheckBox.Checked;
      criteria.FailedToReportToInterview = FailedToReportToInterviewCheckBox.Checked;
      criteria.FailedToReportToJob = FailedToReportToJobCheckBox.Checked;
      criteria.FoundJobFromOtherSource = FoundJobOtherSourceCheckBox.Checked;
      criteria.InterviewDenied = InterviewDeniedCheckBox.Checked;
      criteria.InterviewScheduled = InterviewScheduledCheckBox.Checked;
      criteria.JobAlreadyFilled = JobAlreadyFilledCheckBox.Checked;
      criteria.NewApplicant = NewApplicantCheckBox.Checked;
      criteria.NotQualified = NotQualifiedCheckBox.Checked;
      criteria.Recommended = RecommendedCheckBox.Checked;
      criteria.RefusedReferral = RefusedReferralCheckBox.Checked;
      criteria.RefusedOffer = RefusedJobCheckBox.Checked;
      criteria.UnderConsideration = UnderConsiderationCheckBox.Checked;
      return criteria;
    }

    /// <summary>
    /// Resets this instance.
    /// </summary>
    private void Reset()
    {
      HiredCheckBox.Checked = 
      NotHiredCheckBox.Checked = 
      NotYetPlacedCheckBox.Checked = 
      FailedToApplyCheckBox.Checked = 
      FailedToRespondCheckBox.Checked = 
      FailedToReportToInterviewCheckBox.Checked = 
      FailedToReportToJobCheckBox.Checked = 
      FoundJobOtherSourceCheckBox.Checked =
      InterviewDeniedCheckBox.Checked = 
      InterviewScheduledCheckBox.Checked = 
      JobAlreadyFilledCheckBox.Checked = 
      NewApplicantCheckBox.Checked = 
      NotQualifiedCheckBox.Checked = 
      RecommendedCheckBox.Checked = 
      RefusedReferralCheckBox.Checked = 
      RefusedJobCheckBox.Checked = 
      UnderConsiderationCheckBox.Checked = false;
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      ReferralsOutcomeLabel.Text = CodeLocalise("ReferralsOutcomeLabel.Text", "Referral outcomes");
      OutcomesStatusHeader.Text = CodeLocalise("OutcomesStatusHeader.Text", "Show number of");

      HiredCheckBox.Text = CodeLocalise("OutcomesCheckBoxList.Hired.Text", "Hired");
      NotHiredCheckBox.Text = CodeLocalise("OutcomesCheckBoxList.NotHired.Text", "Not hired");
      NotYetPlacedCheckBox.Text = CodeLocalise("OutcomesCheckBoxList.NotYetPlaced.Text", "Not yet placed");
      FailedToApplyCheckBox.Text = CodeLocalise("OutcomesCheckBoxList.FailedToApply.Text", "Failed to apply");
      FailedToRespondCheckBox.Text = CodeLocalise("OutcomesCheckBoxList.FailedToRespond.Text", "Failed to respond");
      FailedToReportToInterviewCheckBox.Text = CodeLocalise("OutcomesCheckBoxList.FailedToReportInterview.Text", "Failed to report to interview");
      FailedToReportToJobCheckBox.Text = CodeLocalise("OutcomesCheckBoxList.FailedToReportJob.Text", "Failed to report to job");
      FoundJobOtherSourceCheckBox.Text = CodeLocalise("OutcomesCheckBoxList.FoundJobOtherSource.Text", "Found job from other source");
      InterviewDeniedCheckBox.Text = CodeLocalise("OutcomesCheckBoxList.InterviewDenied.Text", "Interview denied");
      InterviewScheduledCheckBox.Text = CodeLocalise("OutcomesCheckBoxList.InterviewScheduled.Text", "Interview scheduled");
      JobAlreadyFilledCheckBox.Text = CodeLocalise("OutcomesCheckBoxList.JobAlreadyFilled.Text", "Job already filled");
      NewApplicantCheckBox.Text = CodeLocalise("OutcomesCheckBoxList.NewApplicant.Text", "New applicant");
      NotQualifiedCheckBox.Text = CodeLocalise("OutcomesCheckBoxList.NotQualified.Text", "Not qualified");
      RecommendedCheckBox.Text = CodeLocalise("OutcomesCheckBoxList.Recommended.Text", "Recommended");
      RefusedReferralCheckBox.Text = CodeLocalise("OutcomesCheckBoxList.RefusedReferral.Text", "Refused referral");
      RefusedJobCheckBox.Text = CodeLocalise("OutcomesCheckBoxList.RefusedJob.Text", "Refused job");
      UnderConsiderationCheckBox.Text = CodeLocalise("OutcomesCheckBoxList.UnderConsideration.Text", "Under consideration");
    }
  }
}