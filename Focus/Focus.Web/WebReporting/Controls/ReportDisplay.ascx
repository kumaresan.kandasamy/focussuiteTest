﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportDisplay.ascx.cs" Inherits="Focus.Web.WebReporting.Controls.ReportDisplay" %>
<%@ Register src="BarChartDisplay.ascx" tagName="BarChartDisplay" tagPrefix="uc" %>
<%@ Register src="TotalsDisplay.ascx" tagName="TotalsDisplay" tagPrefix="uc" %>
<%@ Register src="JobSeekerReportList.ascx" tagName="JobSeekerReportList" tagPrefix="uc" %>
<%@ Register src="JobOrderReportList.ascx" tagName="JobOrderReportList" tagPrefix="uc" %>
<%@ Register src="EmployerReportList.ascx" tagName="EmployerReportList" tagPrefix="uc" %>
<%@ Register src="SupplyDemandReportList.ascx" tagName="SupplyDemandReportList" tagPrefix="uc" %>
<%@ Register src="SaveReportModal.ascx" tagName="SaveReportModal" tagPrefix="uc" %>
<div class="grid100">
    <div class="reportgrid50">
        <h1><asp:Literal runat="server" ID="ReportTitleLiteral"></asp:Literal></h1>
    </div>
    <div class="reportgrid50"  style="text-align:right;">
      <asp:PlaceHolder runat="server" ID="ExportPlaceHolder">
	      <focus:LocalisedLabel runat="server" ID="ExportDropDownListLabel" AssociatedControlID="ExportDropDownList" LocalisationKey="Export" DefaultText="Export" CssClass="sr-only"/>
        <asp:DropDownList runat="server" ID="ExportDropDownList" />
        <asp:Button runat="server" CssClass="button3" ID="ExportButton" OnClick="ExportButton_Clicked" ValidationGroup="Export" />
      </asp:PlaceHolder>
      <asp:Button runat="server" CssClass="button3" ID="AddToDashboardButton" OnClick="AddToDashboardButton_Clicked" />
      <asp:Button runat="server" CssClass="button1" ID="SaveReportButton" OnClick="SaveReportButton_Clicked" />
      <br />
      <asp:RequiredFieldValidator ID="ExportValidator" runat="server" ControlToValidate="ExportDropDownList" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Export" />
    </div>
</div>
<uc:JobSeekerReportList runat="server" ID="JobSeekerReport" Visible="False" OnReportCriteriaChanged="ReportEntities_ReportCriteriaChanged" />
<uc:JobOrderReportList runat="server" ID="JobOrderReport" Visible="False" OnReportCriteriaChanged="ReportEntities_ReportCriteriaChanged" />
<uc:EmployerReportList runat="server" ID="EmployerReportList" Visible="False" OnReportCriteriaChanged="ReportEntities_ReportCriteriaChanged" />
<uc:SupplyDemandReportList runat="server" ID="SupplyDemandReportList" Visible="False" OnReportCriteriaChanged="ReportEntities_ReportCriteriaChanged" />
<uc:BarChartDisplay runat="server" ID="BarChartDisplay" Visible="False" />
<uc:TotalsDisplay runat="server" ID="TotalsDisplay" Visible="False" />
<uc:SaveReportModal runat="server" ID="SaveReportModal" Visible="False" OnGetCriteriaForSaveEvent="SaveReportModal_GetCriteriaForSaveEvent" />
