﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Security;
using Focus.Common.Authentication;
using Focus.Common.Models;
using Focus.Core;
using Focus.Web.Code;
using Focus.Web.WebAuth;
using Framework.Core;
using Framework.Logging;
using System.Net;


#endregion

namespace Focus.Web
{

	public class Global : HttpApplication
	{
		private IApp _app;

		private IApp App
		{
      //get { return _app = new HttpApp(); }
			get { return _app ?? (_app = new HttpApp()); }
		}

		#region Application Start

		/// <summary>
		/// Handles the Start event of the Application control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		void Application_Start(object sender, EventArgs e)
		{
            System.Net.ServicePointManager.SecurityProtocol = (SecurityProtocolType)48 | (SecurityProtocolType)192 | (SecurityProtocolType)768 | (SecurityProtocolType)3072;
			App.Start();

			// Log App Start
			Logger.Debug("---------- Focus Web (Talent-Assist) Application started ----------");
			Logger.Flush(true);
		}

		#endregion

		#region Application End

		/// <summary>
		/// Handles the End event of the Application control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		void Application_End(object sender, EventArgs e)
		{
			App.Shutdown();

			// Log App End
			Logger.Debug("---------- Focus Web (Talent-Assist) Application shutdown ----------");
			Logger.Flush(true);
		}

		#endregion

		#region Application AuthenticateRequest

		/// <summary>
		/// Handles the AuthenticateRequest event of the Application control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Application_AuthenticateRequest(object sender, EventArgs e)
		{
			var cookie = FormsAuthentication.FormsCookieName;
			var httpCookie = Context.Request.Cookies[cookie];

			if (httpCookie == null || string.IsNullOrEmpty(httpCookie.Value)) return;

			var ticket = FormsAuthentication.Decrypt(httpCookie.Value);
			if (ticket == null || ticket.Expired) return;

			var identity = new FormsIdentity(ticket);
			
			var userContext = ticket.UserData.DeserializeUserContext();
			userContext.Culture = Thread.CurrentThread.CurrentUICulture.ToString().ToUpperInvariant();
		 
			var principal = new UserPrincipal(identity, userContext);

			Context.User = principal;
		}

		#endregion

    #region Application PostAuthenticateRequest

    /// <summary>
    /// Handles the PostAuthenticateRequest event of the Application control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
    {
      App.PostAuthenticateRequest();

			//Store the resource being accessesed if the user isn't authenticated so that we can navigate to that resource following a successful login
      if ((!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForAssist) && App.Settings.Module == FocusModules.Assist) || (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForTalent) && App.Settings.Module == FocusModules.Talent) || App.User.IsAuthenticated) return;

			// Only store the requested url if it is the first one accessed (not an internal redirect)
			var originalUrl = App.GetCookieValue(Saml.RquestedResourceUrl);
	    var returnUrl = App.Settings.SSOReturnUrl.StartsWith("~")
												? new Uri(Request.Url, VirtualPathUtility.ToAbsolute(App.Settings.SSOReturnUrl)).AbsoluteUri
		                    : App.Settings.SSOReturnUrl;
			var errorUrl = App.Settings.SSOErrorRedirectUrl.StartsWith("~")
												? new Uri(Request.Url, VirtualPathUtility.ToAbsolute(App.Settings.SSOErrorRedirectUrl.Replace("#ERROR#", ".*"))).AbsoluteUri
												: App.Settings.SSOErrorRedirectUrl.Replace("#ERROR#", ".*");
			var errorRegex = new Regex("^" + errorUrl.Replace("?", @"\?") + "$");
			var iconUrl = new Uri(Request.Url, "/favicon.ico").AbsoluteUri;
			if (String.IsNullOrEmpty(originalUrl) && Request.Url.AbsoluteUri.IsNotIn(returnUrl, iconUrl) && !errorRegex.IsMatch(Request.Url.AbsoluteUri))
				App.SetCookieValue(Saml.RquestedResourceUrl, Request.Url.AbsoluteUri);
    }

    #endregion

    #region Application_BeginRequest

    /// <summary>
		/// Handles the BeginRequest event of the Application control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Application_BeginRequest(object sender, EventArgs e)
		{
            //if (Request.IsLocal)
            //{
            //    MiniProfiler.Start();
            //}

            // Call code in service layer that needs to be executed before the request
			App.BeginRequest();
		}

		#endregion

		#region Application EndRequest

		/// <summary>
		/// Application_s the end request.
		/// </summary>
		protected void Application_EndRequest()
		{
            //MiniProfiler.Stop();
			App.EndRequest();
		}

		#endregion

		#region Application Error

		/// <summary>
		/// Application error event. This is where last resort error handling occurs. 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Application_Error(object sender, EventArgs e)
		{		
			var exception = Server.GetLastError().GetBaseException();

			try
			{
        // See FVN-2257 for reason for ignoring the second exception
        if (!(exception is SecurityException
					   	|| exception.Message.StartsWith("Invalid postback or callback argument.")
							|| exception.Message.StartsWith("Maximum request length exceeded.")))
				{
					try
					{
						var formParameters = new WebRequestLog(Request);
						Logger.Fatal(App.SessionId, App.RequestId, App.User.UserId, App.Settings.Application + " Exception", exception, formParameters);
					}
					catch 
					{
						Logger.Fatal(App.Settings.Application + " Exception", exception);
					}
				}
			}
			catch
			{
				// ignored
			}

			// Set the exception so 
		  App.Exception = new ExceptionModel
		  {
		    Message = exception.Message,
		    StackTrace = exception.StackTrace
		  };

			// Set the error type
      var errorType = exception.GetErrorType();

			// Redirect accordingly
			if ((App.Settings.SSOEnabled || (App.Settings.SamlEnabledForTalent && App.Settings.Module == FocusModules.Talent) || (App.Settings.Module == FocusModules.Assist && App.Settings.SamlEnabledForAssist)) && !App.User.IsAuthenticated)
			{
				// If we have a SecurityException then redirect to SAML page.
				// If exceptions occur during the SAML process it is ubnlikely that they will be of type SecurityException so will not result in a redirect loop.
				if (exception is SecurityException)
				{
					Response.Redirect(UrlBuilder.Saml(), true);
					return;
				}

				// Remove the requested resource url from the cookie if one exists to prevent confusion when redirecting within Focus after the error jas occurred.
				App.RemoveCookieValue(Saml.RquestedResourceUrl);

				var plainTextBytes = Encoding.UTF8.GetBytes(exception.Message.Replace(Environment.NewLine, " "));
				var qureyStringParam = HttpUtility.UrlEncode(Convert.ToBase64String(plainTextBytes));

				Response.Redirect(App.Settings.SSOErrorRedirectUrl.Replace("#ERROR#", qureyStringParam), true);
			}
			else
			{
				if (exception is SecurityException && !Request.Url.AbsolutePath.Contains("/login"))
					SetupPageRedirect();

				if (!Request.Url.AbsolutePath.Contains("/ErrorSafe.htm"))
				{
					var url = UrlBuilder.Error(errorType);

					if (Request.Url.AbsolutePath.Contains("/error"))
						url = string.Concat("/ErrorSafe.htm?", Guid.NewGuid().ToString().ToLower());
					else if (App.User.UserId == 0 && !Request.Url.AbsolutePath.Contains("/login"))
						url = UrlBuilder.LogIn();

					Response.Redirect(url, true);
				}
			}
		}

		private void SetupPageRedirect()
		{
			if (HttpContext.Current == null) return;

			if (!App.User.IsAuthenticated)
			{
				App.SetCookieValue("Focus:Redirect", HttpContext.Current.Request.Url.AbsolutePath, true);
			}
		}

		#endregion

		#region Session Start

		void Session_Start(object sender, EventArgs e)
		{
			// Code that runs when a new session is started
            System.Net.ServicePointManager.SecurityProtocol = (SecurityProtocolType)48 | (SecurityProtocolType)192 | (SecurityProtocolType)768 | (SecurityProtocolType)3072;
		}

		#endregion

		#region Session End

		void Session_End(object sender, EventArgs e)
		{
			// Code that runs when a session ends. 
			// Note: The Session_End event is raised only when the sessionstate mode
			// is set to InProc in the Web.config file. If session mode is set to StateServer 
			// or SQLServer, the event is not raised.
		}

		#endregion
	}
}
