﻿<%@ Page Title="Forgotten Password Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ForgottenPassword.aspx.cs" Inherits="Focus.Web.WebAuth.ForgottenPassword" %>

<%@ Register Src="../Code/Controls/User/ConfirmationModal.ascx" TagName="ConfirmationModal" TagPrefix="uc" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<asp:HiddenField ID="ForgottenPasswordModalDummyTarget" runat="server" />
	<act:ModalPopupExtender ID="ForgottenPasswordModal" runat="server" TargetControlID="ForgottenPasswordModalDummyTarget" PopupControlID="ForgottenPasswordPanel" PopupDragHandleControlID="ForgottenPasswordHeader" RepositionMode="RepositionOnWindowResizeAndScroll" BackgroundCssClass="modalBackground" />
	<asp:Panel ID="ForgottenPasswordPanel" runat="server" CssClass="modal" Width="325px">
		<div style="width: 100%;">
			<asp:Panel ID="ForgottenPasswordHeader" runat="server" CssClass="modal-header">
				<h2 style="font-size: 18px">
					<%= HtmlLocalise("Header.Text", "Forgot your username or password?")%>
				</h2>
			</asp:Panel>
			<p>
				<%= HtmlLocalise("Notification.Text", "Please enter your username or email address below, and we'll send you an email to help get back online.")%></p>
			<br />
			<p>
				<span class="inFieldLabel">
					<%= HtmlLabel(ForgottenPasswordUserNameTextBox, "UserName.Label", "username")%>
					<asp:TextBox ID="ForgottenPasswordUserNameTextBox" runat="server" ClientIDMode="Static" Width="275px" />
				</span>
			</p>
			<p>
				<%= HtmlLocalise("Or.Text", "or")%></p>
			<p>
				<span class="inFieldLabel">
					<%= HtmlLabel(ForgottenPasswordEmailAddressTextBox, "EmailAddress.Label", "email address")%>
					<asp:TextBox ID="ForgottenPasswordEmailAddressTextBox" runat="server" ClientIDMode="Static" Width="275px" />
				</span>
			</p>
			<p>
				<a href="<%= UrlBuilder.LogIn() %>">
					<%= HtmlLocalise("Login.Text", "Sign in")%>
				</a>
			</p>
			<focus:AjaxValidationSummary ID="ForgottenPasswordValidationSummary" runat="server" DisplayMode="List" CssClass="error" ValidationGroup="ForgottenPassword" />
			<div style="width: 100%; text-align: right; margin-top: 10px;">
				<asp:Button ID="SubmitButton" runat="server" SkinID="Button1" OnClick="SubmitButton_Click" ValidationGroup="ForgottenPassword" />
			</div>
		</div>
	</asp:Panel>
	<asp:HiddenField ID="IdentityConfirmationModalDummyTarget" runat="server" />
	<act:ModalPopupExtender ID="IdentityConfirmationModal" runat="server" TargetControlID="IdentityConfirmationModalDummyTarget" PopupControlID="IdentityConfirmationPanel" PopupDragHandleControlID="IdentityConfirmationHeader" RepositionMode="RepositionOnWindowResizeAndScroll" BackgroundCssClass="modalBackground" />
	<asp:Panel ID="IdentityConfirmationPanel" runat="server" CssClass="modal" Width="500px">
		<div style="width: 100%;">
			<asp:Panel ID="IdentityConfirmationHeader" runat="server" CssClass="modal-header">
				<h1 style="font-size: 18px">
					<%= HtmlLocalise("IdentityConfirmation.Label", "Identity Confirmation")%>
				</h1>
			</asp:Panel>
			<p>
				<%= HtmlLocalise("IdentityConfirmationLine1.Label", "Before we reset your password we need to confirm your identity")%>
			</p>
			<p>
				<%= HtmlLocalise("IdentityConfirmationLine2.Label", "Below are two of the identity confirmation questions you created when you registered for #FOCUSTALENT#. You must answer both of these correctly in order to reset your password.")%>
			</p>
			<div runat="server" id="ErrorPanel" class="error-panel" style="margin-bottom: 10px;" visible="False">
			</div>
			<div runat="server" id="InfoPanel" class="information-panel" style="margin-bottom: 10px;" visible="False">
			</div>
			<asp:Panel ID="Question1Panel" runat="server" CssClass="questpanelleft">
				<div class="questbox" style="width: 100%;">
					<h4 class="questboxheader">
						Security question 1</h4>
					<div style="width: 100%; height: 40px;">
						<asp:Label runat="server" ID="SecurityQuestion1Label" Text="Security question 1" Width="100%" />
					</div>
					<div>
						<div style="display: inline-block;">
							<%= HtmlRequiredLabel(AnswerTextBox1, "TalentSecurityAnswer.Label", "Answer")%>
						</div>
						<div style="display: inline-block;">
							<asp:TextBox ID="AnswerTextBox1" runat="server" Width="100%"></asp:TextBox>
						</div>
					</div>
					<div>
						<asp:RequiredFieldValidator ID="AnswerRequired1" runat="server" ControlToValidate="AnswerTextBox1" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
						<asp:HiddenField runat="server" ID="SecurityQuestion1Index" />
					</div>
				</div>
			</asp:Panel>
			<asp:Panel ID="Question2Panel" runat="server" CssClass="questpanelright">
				<div class="questbox" style="width: 100%;">
					<h4 class="questboxheader">
						Security question 2</h4>
					<div style="width: 100%; height: 40px;">
						<asp:Label runat="server" ID="SecurityQuestion2Label" Text="Security question 2" Width="100%" />
					</div>
					<div>
						<div style="display: inline-block;">
							<%= HtmlRequiredLabel(AnswerTextBox2, "TalentSecurityAnswer.Label", "Answer")%>
						</div>
						<div style="display: inline-block;">
							<asp:TextBox ID="AnswerTextBox2" runat="server" Width="100%"></asp:TextBox>
						</div>
					</div>
					<div>
						<asp:RequiredFieldValidator ID="AnswerRequired2" runat="server" ControlToValidate="AnswerTextBox1" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
						<asp:HiddenField runat="server" ID="SecurityQuestion2Index" />
					</div>
				</div>
			</asp:Panel>
			<div style="width: 100%; text-align: right; margin-top: 10px;" id="ButtonDiv" runat="server">
			  <asp:Button runat="server" Id="SignInButton" CssClass="button1" OnClick="SignInButton_OnClick" style="Float:left;" CausesValidation="false"  />
				<asp:Button runat="server" Text="Reset my password" ID="ResetPasswordButton" CssClass="button1" OnClick="ResetPasswordButton_OnClick" style="Float:right;" />
			</div>
		</div>
	</asp:Panel>
	<asp:RegularExpressionValidator ID="UserNameRegEx" runat="server" ControlToValidate="ForgottenPasswordUserNameTextBox" Display="None" ValidationGroup="ForgottenPassword" />
	<asp:RegularExpressionValidator ID="EmailAddressRegEx" runat="server" ControlToValidate="ForgottenPasswordEmailAddressTextBox" Display="None" ValidationGroup="ForgottenPassword" />
	<asp:CustomValidator ID="EitherFieldValidator" runat="server" Display="None" ClientValidationFunction="ValidateEitherFieldPresent" OnServerValidate="EitherFieldValidator_OnServerValidate" ValidationGroup="ForgottenPassword" />
	<asp:CustomValidator ID="NotBothFieldsValidator" runat="server" Display="None" ClientValidationFunction="NotBothFieldsPresent" OnServerValidate="NotBothFieldsValidator_OnServerValidate" ValidationGroup="ForgottenPassword" />
	<asp:CustomValidator ID="ForgottenPasswordValidator" runat="server" Display="None" ValidationGroup="ForgottenPassword" />
	<%-- Confirmation Modal --%>
	<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />
	<script language="javascript" type="text/javascript">
		function ValidateEitherFieldPresent(sender, args)
		{
			var userNameEntered = $("#ForgottenPasswordUserNameTextBox").val().trim().length > 0;
			var emailAddressEntered = $("#ForgottenPasswordEmailAddressTextBox").val().trim().length > 0;

			args.IsValid = (userNameEntered || emailAddressEntered);
		}

		function NotBothFieldsPresent(sender, args)
		{
			var userNameEntered = $("#ForgottenPasswordUserNameTextBox").val().trim().length > 0;
			var emailAddressEntered = $("#ForgottenPasswordEmailAddressTextBox").val().trim().length > 0;

			args.IsValid = !(userNameEntered && emailAddressEntered);
	  }

	</script>
</asp:Content>
