﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Core;
using Focus.Web.Code;

using Framework.Core;
using Framework.Exceptions;

#endregion

namespace Focus.Web.WebAuth
{
	public partial class ForgottenPassword : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				ForgottenPasswordModal.Show();
				LocaliseUi();

				UserNameRegEx.ValidationExpression = (App.Settings.Module == FocusModules.Assist && App.Settings.AuthenticateAssistUserViaClient) ? "" : App.Settings.UserNameRegExPattern;
				UserNameRegEx.Enabled = UserNameRegEx.ValidationExpression.IsNotNullOrEmpty();

				EmailAddressRegEx.ValidationExpression = App.Settings.EmailAddressRegExPattern;
			}
		}

		/// <summary>
		/// Localises the text on the page
		/// </summary>
		private void LocaliseUi()
		{
			UserNameRegEx.ErrorMessage = CodeLocalise("UserNameRegEx.ErrorMessage", "Username is not the correct format");
			EmailAddressRegEx.ErrorMessage = CodeLocalise("EmailAddressRegEx.ErrorMessage", "Email address is not the correct format");
			EitherFieldValidator.ErrorMessage = CodeLocalise("EitherFieldValidator.RequiredErrorMessage", "Please enter either a username or email address");
			NotBothFieldsValidator.ErrorMessage = CodeLocalise("EitherFieldValidator.RequiredErrorMessage", "Please enter just one field, but not both");
			SubmitButton.Text = CodeLocalise("Global.Submit.Text", "Submit");
			SignInButton.Text = CodeLocalise("Global.LogIn.Text", "Sign in");
			AnswerRequired1.ErrorMessage = CodeLocalise("SecurityAnswer.RequiredErrorMessage", "Answer is required");
			AnswerRequired2.ErrorMessage = CodeLocalise("SecurityAnswer.RequiredErrorMessage", "Answer is required");
		}

		/// <summary>
		/// Resets the user's password
		/// </summary>
		public void ResetPassword()
		{
			var username = (ForgottenPasswordUserNameTextBox.Text ?? string.Empty).Trim();
			var emailAddress = (ForgottenPasswordEmailAddressTextBox.Text ?? string.Empty).Trim();

			try
			{
				string resetPasswordUrl;

				switch (App.Settings.Module)
				{
					case FocusModules.Assist:
						resetPasswordUrl = string.Format("{0}{1}", App.Settings.AssistApplicationPath, UrlBuilder.ResetPassword(false));
						ServiceClientLocator.AuthenticationClient(App).ProcessForgottenPassword(username, emailAddress, resetPasswordUrl, FocusModules.Assist);
						break;

					case FocusModules.Talent:
						resetPasswordUrl = string.Format("{0}{1}", App.Settings.TalentApplicationPath, UrlBuilder.ResetPassword(false));
						ServiceClientLocator.AuthenticationClient(App).ProcessForgottenPassword(username, emailAddress, resetPasswordUrl, FocusModules.Talent);
						break;

					default:
						throw new Exception(FormatError(ErrorTypes.UnableToProcessForgottenPassword, "Unable to process forgotten password"));
				}

				ForgottenPasswordModal.Hide();

				ConfirmationModal.Show(CodeLocalise("Success.Title", "Forgotten Password Request Sent"),
					CodeLocalise("Success.Body", "An email has been sent to your account which will allow you to reset your password."),
					CodeLocalise("Global.LogIn.Text", "Sign in"), closeLink: UrlBuilder.LogIn());
			}
			catch (ServiceCallException ex)
			{
				ForgottenPasswordValidator.IsValid = false;

				switch (ex.ErrorCode)
				{
					case (int)ErrorTypes.UnableToProcessForgottenPassword:
						ShowUserNotFoundMessage(username);
						break;

					case (int)ErrorTypes.UserBlocked:
						ForgottenPasswordValidator.ErrorMessage = CodeLocalise("UserBlocked.Text", "Unable to reset password because an unexpected hold has been placed on your account");
						break;

					case (int)ErrorTypes.UserNotEnabled:
						ForgottenPasswordValidator.ErrorMessage = CodeLocalise("UserBlocked.Text", "Unable to reset password because your account is currently not active");
						break;

					default:
						ForgottenPasswordValidator.ErrorMessage = ex.Message;
						break;
				}

				ForgottenPasswordModal.Show();
			}
			catch (Exception ex)
			{
				ForgottenPasswordValidator.IsValid = false;
				ForgottenPasswordValidator.ErrorMessage = ex.Message;
				ForgottenPasswordModal.Show();
			}
		}

		/// <summary>
		/// Shows a message if the user is not found
		/// </summary>
		/// <param name="username"></param>
		private void ShowUserNotFoundMessage(string username)
		{
			ForgottenPasswordValidator.ErrorMessage = username.IsNotNullOrEmpty()
				? CodeLocalise("InvalidUser.Text", "Username not recognised. Please check the details and try again.")
				: CodeLocalise("InvalidEmail.Text", "Email address not recognised. Please check the details and try again.");
		}

		/// <summary>
		/// Handles the Click event of the SubmitButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SubmitButton_Click(object sender, EventArgs e)
		{
			if (App.Settings.EnableTalentPasswordSecurity && App.Settings.Module == FocusModules.Talent)
			{
				var username = ForgottenPasswordUserNameTextBox.Text.Trim();
				var email = ForgottenPasswordEmailAddressTextBox.Text.Trim();
				var questions = ServiceClientLocator.AuthenticationClient(App).GetSecurityQuestions(email, username, App.Settings.Module);

				if (questions.Error == ErrorTypes.UserNotFound)
				{
					ForgottenPasswordValidator.IsValid = false;
					ShowUserNotFoundMessage(username);
					ForgottenPasswordModal.Show();
				}
				else if (questions.SecurityQuestion1.IsNull() && questions.SecurityQuestionId1.IsNullOrZero())
				{
					ResetPassword();
				}
				else
				{
					var questionList = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.SecurityQuestions);
					if (questions.SecurityQuestionId1.HasValue)
					{
						var questionObj = questionList.FirstOrDefault(sq => sq.Id == questions.SecurityQuestionId1);
						SecurityQuestion1Label.Text = questionObj.IsNull() ? string.Empty : questionObj.Text;
					}
					else
					{
						SecurityQuestion1Label.Text = questions.SecurityQuestion1;
					}
					SecurityQuestion1Index.Value = questions.SecurityQuestionIndex1.ToString();
					if (questions.SecurityQuestionId1.HasValue)
					{
						var questionObj = questionList.FirstOrDefault(sq => sq.Id == questions.SecurityQuestionId2);
						SecurityQuestion2Label.Text = questionObj.IsNull() ? string.Empty : questionObj.Text;
					}
					else
					{
						SecurityQuestion2Label.Text = questions.SecurityQuestion2;
					}
					SecurityQuestion2Index.Value = questions.SecurityQuestionIndex2.ToString();

					ForgottenPasswordModal.Hide();
					IdentityConfirmationModal.Show();
				}
			}
			else
			{
				ResetPassword();
			}
		}

		/// <summary>
		/// Handles the OnClick event of the ResetPasswordButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		/// <exception cref="NotImplementedException"></exception>
		protected void ResetPasswordButton_OnClick(object sender, EventArgs e)
		{
			try
			{
				ErrorPanel.Visible = false;
				InfoPanel.Visible = false;

				var emailAddress = (ForgottenPasswordEmailAddressTextBox.Text ?? string.Empty).Trim();
				var userName = (ForgottenPasswordUserNameTextBox.Text ?? string.Empty).Trim();
				var questionIndex1 = Convert.ToInt32(SecurityQuestion1Index.Value);
				var questionIndex2 = Convert.ToInt32(SecurityQuestion2Index.Value);
				var answer1 = (AnswerTextBox1.Text ?? string.Empty).Trim();
				var answer2 = (AnswerTextBox2.Text ?? string.Empty).Trim();

				string resetPasswordUrl;
				int? attemptsLeft;

				switch (App.Settings.Module)
				{
					case FocusModules.Assist:
						resetPasswordUrl = string.Format("{0}{1}", App.Settings.AssistApplicationPath, UrlBuilder.ResetPassword(false));
						attemptsLeft = ServiceClientLocator.AuthenticationClient(App).ProcessSecurityQuestions(emailAddress, userName, questionIndex1, answer1, questionIndex2, answer2, FocusModules.Assist, resetPasswordUrl);
						break;

					case FocusModules.Talent:
						resetPasswordUrl = string.Format("{0}{1}", App.Settings.TalentApplicationPath, UrlBuilder.ResetPassword(false));
						attemptsLeft = ServiceClientLocator.AuthenticationClient(App).ProcessSecurityQuestions(emailAddress, userName, questionIndex1, answer1, questionIndex2, answer2, FocusModules.Talent, resetPasswordUrl);
						break;

					default:
						throw new Exception(FormatError(ErrorTypes.UnableToProcessForgottenPassword, "Unable to process forgotten password"));
				}

				if (attemptsLeft.IsNull())
				{
					ResetPasswordButton.Visible = false;
					ShowResultInfo(CodeLocalise("EmailSent", "Email has been successfully sent."));
				}
				else
				{
					var message = attemptsLeft.Value == 1
						? CodeLocalise("FailedAttemptLeft", "You have 1 more attempt to answer correctly before your account becomes blocked")
						: CodeLocalise("FailedAttemptsLeft", "You have {0} more attempts to answer correctly before your account becomes blocked", attemptsLeft.Value);

					ShowResult(message);
				}
			}
			catch (ApplicationException ex)
			{
				var errorType = ex.GetErrorType();

				switch (errorType)
				{
					case ErrorTypes.ValidationFailed:
					case ErrorTypes.UserBlocked:
						ShowResult(CodeLocalise("UserBlocked.Text", "You have not answered your security questions correctly. Your account is now on hold. Please contact your Support team at #SUPPORTPHONE# or #SUPPORTEMAIL# Our business hours are 8:00-4:30 EST."));
						break;

					default:
						ShowResult(CodeLocalise("ResetPasswordError.Text", "No match to the email address entered was found. Please check and try again or contact your administration team to request an invitation to register."));
						break;
				}
			}

			IdentityConfirmationModal.Show();
		}

		/// <summary>
		/// Shows an error message
		/// </summary>
		/// <param name="message">The message to show</param>
		private void ShowResult(string message)
		{
			ErrorPanel.InnerText = message;
			ErrorPanel.Visible = true;
		}

		/// <summary>
		/// Shows an informational message
		/// </summary>
		/// <param name="message">The message to show</param>
		private void ShowResultInfo(string message)
		{
			InfoPanel.InnerText = message;
			InfoPanel.Visible = true;
		}

		/// <summary>
		/// Server validation to check either field is entered
		/// </summary>
		/// <param name="source">The custom validator</param>
		/// <param name="args">Validation arguments</param>
		protected void EitherFieldValidator_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			var userNameEntered = ForgottenPasswordUserNameTextBox.Text.Trim().Length > 0;
			var emailAddressEntered = ForgottenPasswordEmailAddressTextBox.Text.Trim().Length > 0;

			args.IsValid = (userNameEntered || emailAddressEntered);
		}

		/// <summary>
		/// Server validation to check that not both fields is entered
		/// </summary>
		/// <param name="source">The custom validator</param>
		/// <param name="args">Validation arguments</param>
		protected void NotBothFieldsValidator_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			var userNameEntered = ForgottenPasswordUserNameTextBox.Text.Trim().Length > 0;
			var emailAddressEntered = ForgottenPasswordEmailAddressTextBox.Text.Trim().Length > 0;

			args.IsValid = !(userNameEntered && emailAddressEntered);
		}

    /// <summary>
    /// Handles the OnClick event of the SignInButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	  protected void SignInButton_OnClick(object sender, EventArgs e)
	  {
      IdentityConfirmationModal.Hide();
      Response.Redirect(UrlBuilder.LogIn(), true);
	  }
	}
}
