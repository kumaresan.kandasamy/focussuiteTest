﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using BgtSSO;
using Focus.Common;
using Focus.Core;
using Focus.Web.Code;
using Framework.Core;
using Framework.Logging;

namespace Focus.Web.WebAuth
{
	public partial class SSOComplete : PageBase
	{
		private string GetGoto()
		{
			var gotoValue = App.GetSessionValue<string>(Saml.GotoValueCookieKey);
			App.RemoveSessionValue(Saml.GotoValueCookieKey);
			return gotoValue;
		}

		private string GetOriginalUrl()
		{
			var rquestedResourceUrl = App.GetCookieValue(Saml.RquestedResourceUrl);
			App.RemoveCookieValue(Saml.RquestedResourceUrl);
			return rquestedResourceUrl;
		}


		protected void Page_Load(object sender, EventArgs e)
		{
			if (App.User.IsAuthenticated || (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForAssist) && App.Settings.Module == FocusModules.Assist) || (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForTalent) && App.Settings.Module == FocusModules.Talent)) Response.Redirect(UrlBuilder.Default(App.User, App, false));

			var profile = App.GetSessionValue<SSOProfile>(Saml.SSOProfileKey);

			// If there is no profile in the session state then we can't continue because we have no external id to associate the profile with
			if (profile.IsNull()) Response.Redirect(UrlBuilder.Saml());

			// Check if user already exists
			var user = ServiceClientLocator.AccountClient(App).FindUserByExternalId(profile.Id);

			// If the user already exists then log them in.
			if (user.IsNotNull()) SamlSuccess(profile);

			var emailAddressNotProvided = profile.EmailAddress.IsNullOrEmpty();
			var firstNameNotProvided = profile.FirstName.IsNullOrEmpty();
			var lastNameNotProvded = profile.LastName.IsNullOrEmpty();

			if (!(emailAddressNotProvided || firstNameNotProvided || lastNameNotProvded))
			{
				if (App.Settings.Module == FocusModules.Talent)
				{
					App.SetSessionValue(Saml.SSOProfileKey, profile);
					Response.Redirect(UrlBuilder.Register());
				}
				else
					SamlSuccess(profile);
			}

			SSOCompleteTitle.Text = CodeLocalise("SSOCompleteTitle.Text", "Some required information has not been supplied");
			SSOCompleteExplanation.Text = CodeLocalise("SSOCompleteExplanation.Text", "Please enter the following information so that we can create your account");
			SubmitButton.Text = CodeLocalise("SSOCompleteSubmitButton.Text", "Submit");

			if (emailAddressNotProvided)
			{
				EmailRow.Visible = true;
				EmailAddressTextBoxLabel.Text = CodeLocalise("SSOCompleteEmailAddressLabel", "Email address");
				EmailAddressRequiredValidator.ErrorMessage = CodeLocalise("SSOCompleteEmailAddressRequiredValidatorMessage", "An email address is required");
				EmailAddressRegexValidator.ErrorMessage = CodeLocalise("SSOCompleteEmailAddressRegexValidatorMessage", "The email address provided is not in an accepted format.");
				EmailAddressRegexValidator.ValidationExpression = App.Settings.EmailAddressRegExPattern;
			}

			if (firstNameNotProvided)
			{
				FirstNameRow.Visible = true;
				FirstNameTextBoxLabel.Text = CodeLocalise("SSOCompleteFirstNameLabel", "First name");
				FirstNameRequiredValidator.ErrorMessage = CodeLocalise("SSOCompleteFirstNameRequiredValidatorMessage", "A first name is required");
				FirstNameLengthValidator.ErrorMessage = CodeLocalise("SSOCompleteFirstNameLengthValidatorMessage", "First name cannot be more than 50 characters");
			}

			if (lastNameNotProvded)
			{
				LastNameRow.Visible = true;
				LastNameTextBoxLabel.Text = CodeLocalise("SSOCompleteLastNameLabel", "Last name");
				LastNameRequiredValidator.ErrorMessage = CodeLocalise("SSOCompleteLastNameRequiredValidatorMessage", "A last name is required");
				LastNameLengthValidator.ErrorMessage = CodeLocalise("SSOCompleteLastNameLengthValidatorMessage", "Last name cannot be more than 50 characters");
			}
		}

		protected void SubmitExtraInformation(object sender, EventArgs e)
		{
			var profile = App.GetSessionValue<SSOProfile>(Saml.SSOProfileKey);

			if (EmailAddressTextBox.Text.IsNotNullOrEmpty()) profile.EmailAddress = EmailAddressTextBox.Text;

			if (FirstNameTextBox.Text.IsNotNullOrEmpty()) profile.FirstName = FirstNameTextBox.Text;

			if (LastNameTextBox.Text.IsNotNullOrEmpty()) profile.LastName = LastNameTextBox.Text;

			if (App.Settings.Module == FocusModules.Talent)
			{
				App.SetSessionValue(Saml.SSOProfileKey, profile);
				Response.Redirect(UrlBuilder.Register());
			}
			else
				SamlSuccess(profile);
		}

		/// <summary>
		/// Processes a successful SAML response.
		/// </summary>
		private void SamlSuccess(ISSOProfile profile)
		{
			// Login with the profile
			Logger.Debug("Saml : Logging into application using profile: " + profile.Id + " - " + profile.ScreenName);
			var userContext = ServiceClientLocator.AuthenticationClient(App).LogIn(profile, App.Settings.Module);

			App.UserData.Load(userContext.UserData);

			var authTicket = new FormsAuthenticationTicket(1, userContext.UserContext.ScreenName ?? userContext.UserContext.FirstName, DateTime.Now, DateTime.Now.AddMinutes(App.Settings.UserSessionTimeout), false, userContext.UserContext.SerializeUserContext());
			var encryptedTicket = FormsAuthentication.Encrypt(authTicket);

			var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
			Response.Cookies.Add(authCookie);

			Logger.Debug("Saml : Authenticated");

			// When we get the Goto value it will be removed from the cookie.  By only removing it when it is used we can capture a goto that is specified before SAML authentication is requested.
			// This allows us to accept goto values in an initial link and therefore allows us to use them with service provider initiated authentication scenarios.
			//This is the same behaviour for the original url that the user attempted to access.
			var originalUrl = GetOriginalUrl();
			var gotoValue = GetGoto();

			// If we have a goto value then override the user's original choice
			if (!userContext.UserContext.IsMigrated && App.Settings.Module == FocusModules.Talent)
			{
				Response.Redirect(UrlBuilder.Register());
			}
			if (gotoValue.IsNotNullOrEmpty())
			{
				Response.Redirect(UrlBuilder.Goto(gotoValue, App), true);
			}
			else if (originalUrl.IsNotNullOrEmpty())
			{
				Response.Redirect(originalUrl, true);
			}
			else
			{
				Response.Redirect(UrlBuilder.Default(userContext.UserContext, App, userContext.UserContext.LastLoggedInOn == null));
			}
		}
	}
}