﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using directives

using System;
using System.Web;
using System.Web.UI;
using Focus.Web.Code;

#endregion

namespace Focus.Web.WebAuth
{
	public partial class SSOError : Page
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			var base64EncodedBytes = Convert.FromBase64String(HttpUtility.UrlDecode(HttpContext.Current.Request.QueryString.Get("message")));
			ErrorMessage.Text = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
		}

		/// <summary>
		/// Retries the click.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void RetryClick(object sender, EventArgs e)
		{
			Response.Redirect(UrlBuilder.Saml());
		}
	}
}