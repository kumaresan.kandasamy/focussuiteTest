﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Core;
using Focus.Web.Code;
using Focus.Common;

#endregion

namespace Focus.Web.WebAuth
{
	public partial class ResetPassword : PageBase
	{
		private long? UserId
		{
			get { return GetViewStateValue<long?>("ResetPassword:UserId"); }
			set { SetViewStateValue("ResetPassword:UserId", value); }
		}
		
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			Page.Title = CodeLocalise("PageTitle", "Reset Password");

			if (!IsPostBack)
			{
				var validationKey = (Request.QueryString.Count > 0) ? Request.QueryString[0] : string.Empty;

			  ErrorTypes errorType;
        UserId = ServiceClientLocator.AuthenticationClient(App).ValidatePasswordResetValidationKey(validationKey, out errorType);

				if(!UserId.HasValue)
				{
          PasswordFieldsPlaceHolder.Visible = false;
				  InvalidKeyPlaceHolder.Visible = true;

          switch (errorType)
          {
            case ErrorTypes.UserValidationKeyExpired:
              ResetPasswordTitleLabel.DefaultText = "Password reset has expired";
              ResetPasswordTitleLabel.LocalisationKey = "ResetPasswordExpired.Label";
              InvalidKeyLabel.Text = HtmlLocalise("UserValidationKeyExpired.Message", "Your request to reset your password has expired. Click on the link below to return to the login page and click \"Forgot your username or password?\" to request another reset");
              break;

            case ErrorTypes.UserValidationKeyAlreadyUsed:
              ResetPasswordTitleLabel.DefaultText = "Password reset completed";
              ResetPasswordTitleLabel.LocalisationKey = "ResetPasswordCompleted.Label";
              InvalidKeyLabel.Text = HtmlLocalise("UserValidationKeyAlreadyUsed.Message", "Your password has already been reset. Click on the link below to return to the login page and enter your updated password. If you have forgotten it, click \"Forgot your username or password?\" to reset it again");
              break;

            default:
              ResetPasswordTitleLabel.DefaultText = "Password reset failed";
              ResetPasswordTitleLabel.LocalisationKey = "ResetPasswordFailed.Label";
              InvalidKeyLabel.Text = CodeLocalise(errorType);
              break;
          }
				}

				LocaliseUI();
				NewPasswordRegEx.ValidationExpression = RetypedNewPasswordRegEx.ValidationExpression = App.Settings.PasswordRegExPattern;
			}

			ResetPasswordModal.Show();
		}

		/// <summary>
		/// Handles the Click event of the ResetButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ResetButton_Click(object sender, EventArgs e)
		{
			var newPassword = (NewPasswordTextBox.Text ?? string.Empty).Trim();

			try
			{
        var validationKey = (Request.QueryString.Count > 0) ? Request.QueryString[0] : string.Empty;
        ServiceClientLocator.AccountClient(App).ResetPassword(UserId, newPassword, validationKey);

				ConfirmationModal.Show(CodeLocalise("SuccessfulChange.Title", "Password Reset"),
				                       CodeLocalise("SuccessfulChange.Body", "You have successfully reset your password."),
				                       CodeLocalise("SuccessfulChange.Close", "Login"),
				                       closeLink: UrlBuilder.LogIn());
			}
			catch (Exception ex)
			{
				ResetPasswordValidator.IsValid = false;
				ResetPasswordValidator.ErrorMessage = ex.Message;
			}
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			NewPasswordRequired.ErrorMessage = CodeLocalise("NewPassword.RequiredErrorMessage", "New password is required");
			NewPasswordRegEx.ErrorMessage = CodeLocalise("NewPassword.RegExErrorMessage",
                                                   "New password must be 6-20 characters and contain at least one number");
			RetypedNewPasswordRequired.ErrorMessage = CodeLocalise("NewPassword.RequiredErrorMessage",
			                                                       "Retype new password is required");
			RetypedNewPasswordRegEx.ErrorMessage = CodeLocalise("NewPassword.RegExErrorMessage",
                                                          "Retyped new password must be 6-20 characters and contain at least one number");
			NewPasswordCompare.ErrorMessage = CodeLocalise("NewPasswordCompare.CompareErrorMessage", "New passwords must match");
			ResetButton.Text = CodeLocalise("Global.Change.Text", "Reset password");

      ForgotPasswordLink.NavigateUrl = UrlBuilder.LogIn();
      ForgotPasswordLink.Text = HtmlLocalise("ForgottenPasswordLink", "Return to login page");
		}
	}
}