﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SSOClose.aspx.cs" Inherits="Focus.Web.WebAuth.SSOClose" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server" />
	<body onload="Close()" />
</html>

<script type="text/javascript">
	function Close() {
		window.close();
	}
</script>