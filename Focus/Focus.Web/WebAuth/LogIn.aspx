﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LogIn.aspx.cs" Inherits="Focus.Web.WebAuth.LogIn" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content runat="server" ID="HeaderContent" ContentPlaceHolderID="HeaderContent">
	<ul class="navTab"></ul>
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<h1 class="sr-only">Login</h1>
	<asp:HiddenField ID="LogInModalDummyTarget" runat="server" />
	<act:ModalPopupExtender ID="LogInModal" runat="server" 
													TargetControlID="LogInModalDummyTarget" 
													PopupControlID="LogInPanel" 
													BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />

	<asp:Panel ID="LogInPanel" runat="server" CssClass="modal" Style="display:none;" >	
		<table>		
			<tr>
				<th style="width:275"><%= HtmlLocalise("LogIn.Header.Text", "Sign in") %></th>
				<th id="RegisterSpacerCell" runat="server" rowspan="3" style="height: 250px; width:20px;" />					
				<th id="RegisterTitleCell" runat="server" width="275"><%= HtmlLocalise("Register.Header.Text", "Register for an account") %></th>
			</tr>
			<tr>
				<td style="vertical-align:top;">
					<br />
          <p>
						<span class="inFieldLabel"><label for="UserNameTextBox" style="width: 275px"><%= HtmlLocalise("UserName.Label", "username") %></label></span>
						<asp:TextBox ID="UserNameTextBox" runat="server" ClientIDMode="Static" Width="275" AutoCompleteType="Disabled"/>
						<asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserNameTextBox" SetFocusOnError="true" Display="None" />
						<asp:RegularExpressionValidator ID="UserNameRegEx" runat="server" ControlToValidate="UserNameTextBox" SetFocusOnError="true" Display="None" />
					</p>
					<p>
						<span class="inFieldLabel"><label for="PasswordTextBox" style="width: 275px"><%= HtmlLocalise("Password.Label", "password") %></label></span>
						<asp:TextBox ID="PasswordTextBox" runat="server" ClientIDMode="Static" TextMode="Password" Width="275px" AutoCompleteType="Disabled" />						
						<asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="PasswordTextBox" SetFocusOnError="true" Display="None" />
                        <p><span class="instructions"><%= HtmlLocalise("LoginPasswordInstructions.Label", "Password is case-sensitive.")%></span></p>
					</p>
					<p><asp:HyperLink ID="ForgottenPasswordLink" runat="server"><%= HtmlLocalise("ForgottenPassword.Label.NoEdit", "Forgot your username or password?") %></asp:HyperLink></p>			
					<focus:AjaxValidationSummary ID="LogInValidationSummary" runat="server" DisplayMode="List" CssClass="error" />	
				</td>
				<td id="RegisterInstructionsCell" runat="server" style="vertical-align:top;">
					<%= HtmlLocalise("NewToFocus.Label", "New to #FOCUSTALENT#?") %>
					<br/><br />
					<%= HtmlLocalise("Register.Label", "Get started posting jobs and finding qualified applicants.") %>
				</td>
			</tr>
			<tr>
				<td style="text-align:right;">
					<div class="modalButtonBorder">
						<focus:DoubleClickDisableButton ID="LogInButton" runat="server" CssClass="button1" OnClick="LogInButton_Click" />
					</div>
				</td>
				<td id="RegisterButtonCell" runat="server" style="text-align:right;">
					<div class="modalButtonBorder">
						<asp:Button ID="RegisterButton" runat="server" SkinID="Button1" OnClick="RegisterButton_Click" CausesValidation="false" />
					</div>
				</td>
			</tr>
		</table>

		<asp:CustomValidator ID="LoginValidator" runat="server" Display="None" />
	
	</asp:Panel>

	<%-- Confirmation Modal --%>
	<uc:ConfirmationModal ID="Confirmation" runat="server" OnOkCommand="ResendEmail_Click" />

</asp:Content>
