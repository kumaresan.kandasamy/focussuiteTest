﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Unsubscribe.aspx.cs" Inherits="Focus.Web.WebAuth.Unsubscribe" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<h1><focus:LocalisedLabel runat="server" ID="TitleLabel" RenderOuterSpan="False"/></h1>
	<p></p>
	<p><focus:LocalisedLabel runat="server" ID="UnsubscribedConfirmationLabel" RenderOuterSpan="False" /></p>
	<p><focus:LocalisedLabel runat="server" ID="AdditionaTextLabel" RenderOuterSpan="False" /></p>
</asp:Content>
