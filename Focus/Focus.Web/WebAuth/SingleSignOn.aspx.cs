﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Threading;
using System.Web;
using System.Web.Security;

using Focus.Common.Authentication;
using Focus.Common;
using Focus.Core;
using Focus.Web.Code;

#endregion

namespace Focus.Web.WebAuth
{
	public partial class SingleSignOn : PageBase
	{
		/// <summary>
		/// Gets a value indicating whether this instance is single sign on.
		/// </summary>
		protected override bool IsSingleSignOn { get { return true; } }

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			// Logout any previous session and clear it out
			try
			{
				ServiceClientLocator.AuthenticationClient(App).LogOut();
			}
			finally
			{
				var demoShieldValidated = DemoShieldValidated;

				FormsAuthentication.SignOut();
        App.ClearSession();

				DemoShieldValidated = demoShieldValidated;
			}

			var key = new Guid();
			var actionType = SSOActionType.None;

			if (Page.RouteData.Values.ContainsKey("key"))
			{
				Guid.TryParse(Page.RouteData.Values["key"].ToString(), out key);
				actionType = SSOActionType.AccessEmployeeAccount;
			}

			if (Page.RouteData.Values.ContainsKey("validate"))
			{
				Guid.TryParse(Page.RouteData.Values["validate"].ToString(), out key);
				actionType = SSOActionType.CompleteRegistration;
      }

      if (Page.RouteData.Values.ContainsKey("reportssoid"))
      {
        Guid.TryParse(Page.RouteData.Values["reportssoid"].ToString(), out key);
        actionType = SSOActionType.AccessPrintableReport;
      }

      if (Page.RouteData.Values.ContainsKey("accessjobssoid"))
      {
        Guid.TryParse(Page.RouteData.Values["accessjobssoid"].ToString(), out key);
        actionType = SSOActionType.AccessJobAsEmployee;
      }

			if (actionType == SSOActionType.None)
			{
				DemoShieldValidated = false;
				Response.Redirect(UrlBuilder.LogIn());
			}

			try
			{
				var userData = ServiceClientLocator.AuthenticationClient(App).ValidateSingleSignOn(key);

				App.UserData.Load(userData.UserData);

        userData.UserContext.Culture = Thread.CurrentThread.CurrentUICulture.ToString().ToUpperInvariant();

				if (actionType == SSOActionType.CompleteRegistration)
					ServiceClientLocator.AccountClient(App).ActivateAccount(userData.UserContext.UserId);

				var authTicket = new FormsAuthenticationTicket(1, userData.UserContext.FirstName, DateTime.Now, DateTime.Now.AddMinutes(App.Settings.UserSessionTimeout), false, userData.UserContext.SerializeUserContext());
				var encryptedTicket = FormsAuthentication.Encrypt(authTicket);

				var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
				Response.Cookies.Add(authCookie);

        var identity = new FormsIdentity(authTicket);
        var principal = new UserPrincipal(identity, userData.UserContext);
        Context.User = principal;

        if (actionType == SSOActionType.AccessPrintableReport)
        {
          if (Page.RouteData.Values.ContainsKey("savedreportid"))
          {
            long reportId;
            Int64.TryParse(Page.RouteData.Values["savedreportid"].ToString(), out reportId);
            Response.Redirect(UrlBuilder.ReportingOutputReport(reportId), false);
          }
          else
            Response.Redirect(UrlBuilder.Default(App.User, App));
        }
        else if (actionType == SSOActionType.AccessJobAsEmployee)
        {
          if (Page.RouteData.Values.ContainsKey("jobid"))
          {
            long jobId;
            Int64.TryParse(Page.RouteData.Values["jobid"].ToString(), out jobId);
            Response.Redirect(UrlBuilder.JobView(jobId), false);
          }
          else
            Response.Redirect(UrlBuilder.Default(App.User, App));
        }
        else
          Response.Redirect(UrlBuilder.Default(App.User, App), false);
			}
			catch
			{
				DemoShieldValidated = false;
				Response.Redirect(UrlBuilder.LogIn());
			}
		}
	}
}