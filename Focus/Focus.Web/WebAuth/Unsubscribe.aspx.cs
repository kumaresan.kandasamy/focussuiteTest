﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web;
using Focus.Common;
using Focus.Core;
using Framework.Core;

#endregion

namespace Focus.Web.WebAuth
{
	public partial class Unsubscribe : PageBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Page.Title = CodeLocalise("UnsubscribeFromReceivingEmails.Text", "Unsubscribe from receiving emails");

			var title = CodeLocalise("UnsubscribeConfirmation.Title", "Unsubscribe Confirmation");
			var confirmationMessage = CodeLocalise("NoLongerReceiveEmails.Text", "You will no longer receive emails that are not intended for you personally.");
			var additionalText = CodeLocalise("EmailsYouWillReceive.Text", "Please note you will continue to receive email notifications relating to activity on your active, on-hold and draft postings.");

			var url = HttpContext.Current.Request.RawUrl;
			var index = url.IndexOf('?');

			if (index >= 0)
			{
				var queryString = (index < url.Length - 1) ? url.Substring(index + 1) : String.Empty;

				var queryStrings = HttpUtility.ParseQueryString(queryString);

				if (queryStrings.Count == 0)
					return;

				var encryptionId = (long)0;
				var encryptedPersonId = string.Empty;

				foreach (string parameter in queryStrings.Keys)
				{
					if (parameter.IsNotNullOrEmpty())
					{
						var values = queryStrings.GetValues(parameter);

						if (values.IsNotNull() && values.Any())
						{
							if (parameter.EqualsIgnoreCase("key")) // Encryption ID
							{
								encryptionId = Convert.ToInt64(values[0]);
							}
							else if (parameter.EqualsIgnoreCase("value")) // Encrypted Person ID
							{
								encryptedPersonId = values[0];
							}
						}
					}
				}

				if (encryptionId.IsNull() || encryptionId == 0 || encryptedPersonId.IsNullOrEmpty())
				{
					title = CodeLocalise("Error.Title", "Error");
					confirmationMessage = CodeLocalise("Unknown.Error", string.Concat("Unable to unsubscribe user: ", "Missing parameters"));
				}
				else
				{
					var error = ServiceClientLocator.AccountClient(App).UnsubscribeHiringManagerFromEmails(encryptedPersonId, encryptionId, true);

					// If an error has been thrown then display a meaningful message on screen
					if (error != ErrorTypes.Ok)
					{
						title = CodeLocalise("Error.Title", "Error");

						switch (error)
						{
							case ErrorTypes.DecryptedValueNotFound:
								confirmationMessage = CodeLocalise("DecryptedValueNotFound.Error", "Unable to unsubscribe user: Decrypted value not found");
								break;

							case ErrorTypes.PersonNotFound:
								confirmationMessage = CodeLocalise("PersonNotFound.Error", "Unable to unsubscribe user: Person not found in database");
								break;

							default:
								confirmationMessage = CodeLocalise("Unknown.Error", string.Concat("Unable to unsubscribe user: ", error.ToString()));
								break;
						}

						// Clear the additional label if there's an error
						additionalText = string.Empty;
					}
				}
			}
			else
			{
				title = CodeLocalise("Error.Title", "Error");
				confirmationMessage = CodeLocalise("KeyNotProvided.Error", "Unable to unsubscribe user: A key hasn't been provided");
				// Clear the additional label if there's an error
				additionalText = string.Empty;
			}

			TitleLabel.DefaultText = title;
			UnsubscribedConfirmationLabel.DefaultText = confirmationMessage;
			AdditionaTextLabel.DefaultText = additionalText;
		}
	}
}