﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.Security;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using Focus.Core;
using Focus.Web.Code;
using Focus.Web.Code.Controllers;

using Framework.Core;

#endregion

namespace Focus.Web.WebAuth
{
	public partial class LogIn : PageBase
	{
		/// <summary>
		/// Gets the page controller.
		/// </summary>
		/// <value>
		/// The page controller.
		/// </value>
		protected WebAuthController PageController { get { return PageControllerBaseProperty as WebAuthController; } }

		/// <summary>
		/// Registers the page controller.
		/// </summary>
		/// <returns></returns>
		protected override IPageController RegisterPageController()
		{
			return new WebAuthController(App);
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			// If authentication then redirect
			if (App.User.IsAuthenticated)
				Response.Redirect(UrlBuilder.Default(App.User, App));

			// If authentication in talent/assist is disabled, redirect to Single Sign-on page
			if (App.Settings.OAuthEnabled)
				Response.Redirect(UrlBuilder.OAuth());

			if (App.Settings.SamlEnabled || (App.Settings.SamlEnabledForAssist && App.Settings.Module == FocusModules.Assist) || (App.Settings.SamlEnabledForTalent && App.Settings.Module == FocusModules.Talent))
				Response.Redirect(UrlBuilder.Saml());

			// Meta data: helpful for SEO (search engine optimization)
			Page.Title = CodeLocalise("PageTitle", "Sign in");
		  
			UserNameRegEx.ValidationExpression = (App.Settings.Module == FocusModules.Assist && App.Settings.AuthenticateAssistUserViaClient) ? "" : App.Settings.UserNameRegExPattern;
			UserNameRegEx.Enabled = UserNameRegEx.ValidationExpression.IsNotNullOrEmpty();

			if (!IsPostBack)
			{
				UserNameRequired.ErrorMessage = CodeLocalise("UserName.RequiredErrorMessage", "Username is required");
				UserNameRegEx.ErrorMessage = CodeLocalise("UserName.RegExErrorMessage", "Username is not the correct format");
				PasswordRequired.ErrorMessage = CodeLocalise("Password.RequiredErrorMessage", "Password is required");

			  LogInButton.Text = CodeLocalise("Global.LogIn.Text.NoEdit", "Sign in");
				RegisterButton.Text = CodeLocalise("Global.Register.Text.NoEdit", "Register");

        var welcomeModalVisibility = App.GetCookieValue(string.Concat("WelcomeModal-", FormsAuthentication.FormsCookieName), Constants.WelcomeModalStateKeys.ShowModal);
				if (welcomeModalVisibility == Constants.WelcomeModalStateKeys.DoNotShow)
          App.SetCookieValue(string.Concat("WelcomeModal-", FormsAuthentication.FormsCookieName), Constants.WelcomeModalStateKeys.ShowOfficeOnly);
			}

			if (App.Settings.Module == FocusModules.Assist && App.Settings.AuthenticateAssistUserViaClient && !App.Settings.ClientAllowsPasswordReset)
				ForgottenPasswordLink.Visible = false;
			else
				ForgottenPasswordLink.NavigateUrl = UrlBuilder.ForgottenPassword();

      // Hide the Register section if this is assist
			RegisterSpacerCell.Visible = RegisterTitleCell.Visible = RegisterInstructionsCell.Visible = RegisterButtonCell.Visible = MasterPage.IsTalent;
			
			LogInModal.Show();
		}

		/// <summary>
		/// Handles the Click event of the LogInButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void LogInButton_Click(object sender, EventArgs e)
		{
			var userName = (UserNameTextBox.Text ?? string.Empty).Trim();
			var password = (PasswordTextBox.Text ?? string.Empty).Trim();

			var controllerResult = PageController.AttemptLogIn(userName, password);

			if (Handle(controllerResult)) return;

			var warningResult = controllerResult as ErrorResult;

			if (!warningResult.IsNotNull()) return;

			// Log the error
			LoginValidator.IsValid = false;
			LoginValidator.ErrorMessage = warningResult.Message; //.Replace(". ", ".<br/>");
		}

		/// <summary>
		/// Handles the Click event of the ResendEmail control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ResendEmail_Click(object sender, CommandEventArgs e)
		{
			Handle(PageController.ResendEmail((UserNameTextBox.Text ?? string.Empty).Trim()));	
		}

		/// <summary>
		/// Handles the Click event of the RegisterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void RegisterButton_Click(object sender, EventArgs e)
		{
			Handle(PageController.Register());
		}
	}
}