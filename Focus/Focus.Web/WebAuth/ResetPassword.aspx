﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="Focus.Web.WebAuth.ResetPassword" %>

<%@ Register src="../Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">

	<asp:HiddenField ID="ResetPasswordModalDummyTarget" runat="server" />
	<act:ModalPopupExtender ID="ResetPasswordModal" runat="server" 
													TargetControlID="ResetPasswordModalDummyTarget"
													PopupControlID="ResetPasswordPanel"
													PopupDragHandleControlID="ResetPasswordPanel"
													RepositionMode="RepositionOnWindowResizeAndScroll"
													BackgroundCssClass="modalBackground" />

	<asp:Panel ID="ResetPasswordPanel" runat="server" CssClass="modal" Width="325px" >
		<table>
			<tr>
				<th>
          <focus:LocalisedLabel runat="server" ID="ResetPasswordTitleLabel" LocalisationKey="ResetPassword.Label" DefaultText="Reset Password"  />
				</th>
			</tr>
      <asp:PlaceHolder runat="server" ID="PasswordFieldsPlaceHolder">
			  <tr>
				  <td style="vertical-align: top;">
					  <br />
					  <p>
						  <span class="inFieldLabel"><label for="NewPasswordTextBox" style="width: 275px"><%= HtmlLocalise("NewPassword.Label", "new password")%></label></span>				
						  <asp:TextBox ID="NewPasswordTextBox" runat="server" ClientIDMode="Static" TextMode="Password" Width="275" />
						  <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPasswordTextBox" SetFocusOnError="true" Display="None" />
						  <asp:RegularExpressionValidator ID="NewPasswordRegEx" runat="server" ControlToValidate="NewPasswordTextBox" SetFocusOnError="true" Display="None" />
					  </p>
					  <p>
						  <span class="inFieldLabel"><label for="RetypedNewPasswordTextBox" style="width: 275px"><%= HtmlLocalise("RetypedNewPassword.Label", "retype new password")%></label></span>
						  <asp:TextBox ID="RetypedNewPasswordTextBox" runat="server" ClientIDMode="Static" TextMode="Password" Width="275" />
						  <asp:RequiredFieldValidator ID="RetypedNewPasswordRequired" runat="server" ControlToValidate="RetypedNewPasswordTextBox" SetFocusOnError="true" Display="None" />
						  <asp:RegularExpressionValidator ID="RetypedNewPasswordRegEx" runat="server" ControlToValidate="RetypedNewPasswordTextBox" SetFocusOnError="true" Display="None" />
						  <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToValidate="RetypedNewPasswordTextBox" ControlToCompare="NewPasswordTextBox" SetFocusOnError="true" Display="None" />
					  </p>
					  <focus:AjaxValidationSummary ID="ResetPasswordValidationSummary" runat="server" DisplayMode="List" CssClass="error" />
				  </td>
			  </tr>
			  <tr>
				  <td style="text-align:right;">
					  <asp:Button ID="ResetButton" runat="server" SkinID="Button1" OnClick="ResetButton_Click" />
				  </td>
			  </tr>
        <tr>
          <td>
            <asp:CustomValidator ID="ResetPasswordValidator" runat="server" Display="Dynamic" CssClass="error" />
          </td>
        </tr>
      </asp:PlaceHolder>
      <asp:PlaceHolder runat="server" ID="InvalidKeyPlaceHolder" Visible="False">
        <tr>
          <td>
          <asp:Label runat="server" id="InvalidKeyLabel"></asp:Label>
          </td>
        </tr>
        <tr>
          <td>
            <asp:HyperLink runat="server" ID="ForgotPasswordLink"></asp:HyperLink>
          </td>
        </tr>
      </asp:PlaceHolder>
		</table>
	</asp:Panel>

	

	<%-- Confirmation Modal --%>
	<uc:ConfirmationModal ID="ConfirmationModal" runat="server"  />

</asp:Content>