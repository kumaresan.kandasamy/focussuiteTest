﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page058.aspx.cs" Inherits="Focus.Web.Designs.FA28Page058" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<table width="100%">
    <tr>
      <td>
        <p class="pageNav">
          <a href="#"><%= HtmlLocalise("JobOrderDashboard", "Job order dashboard")%></a> | 
          <strong><%= HtmlLocalise("JobDevelopment", "Job development")%></strong> | 
          <a href="#"><%= HtmlLocalise("FindEmployer", "Find employer")%></a> | 
          <a href="#"><%= HtmlLocalise("SendMessages", "Send messages")%></a>
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%">
          <tr>
            <td>
              <a href="#"><%= HtmlLocalise("TopLink1", "return to Job Development")%></a>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="2"><h1><%= HtmlLocalise("TopLink1", "Contact employer about Mickey Smith")%></h1></td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td width="95px"><%= HtmlLocalise("TopLink1", "Employer")%></td>
            <td><%= HtmlLocalise("TopLink1", "Fake Employer, Inc.")%></td>
          </tr>
          <tr>
            <td><%= HtmlLocalise("TopLink1", "Hiring manager")%></td>
            <td><%= HtmlLocalise("TopLink1", "Unknown")%></td>
          </tr>
          <tr>
            <td valign="top"><%= HtmlLocalise("TopLink1", "Contact")%></td>
            <td>
              <table>
                <tr>
                  <td width="392px">State records indicate the following contacts for this employer</td>
                  <td align="right"><asp:Button ID="Button1" runat="server" Text="Add or log contact" class="button1"/></td>
                </tr>
                <tr>
                  <td colspan="2">
                    <table width="100%">
                      <tr>
                        <td width="166px">Showing 1-5 of 5 job seekers  </td>
                        <td align="right">
                          <table class="Pager">
                            <tr>
                              <td>
                                <asp:Label runat="server" ID="lblRecordCount"></asp:Label>
                                <%=HtmlLocalise("Reporting.Results", "result(s) found")%> &nbsp;
                              </td>
                              <td id="tdPageStart" runat="server">
                                <a href="#" id="lnkPagerStart">
                                  <img id="PagerStart" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                                    width="15" height="11" /></a>
                              </td>
                              <td id="tdPageBack" runat="server">
                                <a href="#" id="lnkPagerPrevious">
                                  <img id="PagerBack" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                                    height="11" /></a>
                              </td>
                              <td>
                                PAGE
                              </td>
                              <td>
                                <asp:DropDownList ID="ddlPageNumber" runat="server">
                                </asp:DropDownList>
                              </td>
                              <td>
                                <%=HtmlLocalise("Reporting.Of", "of") %>
                                <asp:Label runat="server" ID="lblMaxPageCount" />
                              </td>
                              <td id="tdPageNext" runat="server">
                                <a href="#" id="lnkPagerNext">
                                  <img id="PagerNext" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                                    width="6" height="11" /></a>
                              </td>
                              <td id="tdPageLast" runat="server">
                                <a href="#" id="lnkPagerEnd">
                                  <img id="PagerEnd" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                                    width="15" height="11" /></a>
                              </td>
                              <td style="padding-left: 15px">
                                <asp:DropDownList ID="ddlPageSizes" runat="server">
                                  <asp:ListItem Text="10" Value="10" />
                                  <asp:ListItem Text="20" Value="20" />
                                  <asp:ListItem Text="50" Value="50" />
                                  <asp:ListItem Text="100" Value="100" />
                                </asp:DropDownList>
                              </td>
                              <td>
                                RESULTS PER PAGE
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="4">
                          <table width="100%" class="DataTable">
                            <thead>
                              <tr>
                                <th width="15%">
                                  <%= HtmlLocalise("RecentPlacementsTH2", "NAME")%>
                                  <img id="Img1" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                                  <img id="Img2" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                                </th>
                                <th width="15%">
                                  <%= HtmlLocalise("RecentPlacementsTH4", "ADDRESS")%>
                                  <img id="Img5" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                                  <img id="Img6" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                                </th>
                                <th width="15%">
                                  <%= HtmlLocalise("RecentPlacementsTH5", "TELEPHONE")%>
                                  <img id="Img3" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                                  <img id="Img4" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                                </th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td valign="top"><%= HtmlLocalise("RecentPlacementsTd1-1", "Nifty Networks")%></td>
                                <td valign="top"><%= HtmlLocalise("RecentPlacementsTd1-2", "Rickey Smith<br />Software Engineer")%></td>
                                <td valign="top"></td>
                              </tr>
                              <tr>
                                <td valign="top"><%= HtmlLocalise("RecentPlacementsTd1-3", "Nifty Networks")%></td>
                                <td valign="top"><%= HtmlLocalise("RecentPlacementsTd1-4", "Rickey Smith<br />Software Engineer")%></td>
                                <td valign="top"></td>
                              </tr>

                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
</table>

</asp:Content>
