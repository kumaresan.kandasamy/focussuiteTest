﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page129.aspx.cs" Inherits="Focus.Web.Designs.FA28Page129" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<table width="100%">
  <tr>
    <td colspan="4"><p class="pageNav"><a href="#">Set system defaults</a> | <strong>Manage staff accounts</strong> | <a href="#">Manage home page announcements</a> | <a href="#">Send feedback</a> </p></td>
  </tr>
  <tr class="multipleAccordionTitle">
    <td colspan="3"><span class="accordionArrow"></span>Job seeker messages</td>
    <td align="right"><asp:Button ID="Button2" runat="server" Text="Save" class="button1" style="margin-top:5px;" /></td>
  </tr>
  <tr class="accordionContent">
    <td valign="top" width="200">
      <asp:DropDownList runat="server" ID="DropDownList11">
        <asp:ListItem>My saved message name</asp:ListItem>
      </asp:DropDownList>
    </td>
    <td width="350"><asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Rows="5" Text="" style="overflow-y:scroll" /></td>
    <td valign="top"><img id="closeLogo" src="/Assets/Images/button_x_close_on.png" onclick="alert('close1')" alt="Main Logo" width="16" height="15"></td>
    <td valign="top" align="right"></td>
  </tr>
  <tr class="multipleAccordionTitle on">
    <td colspan="3"><span class="accordionArrow"></span>Employer messages</td>
    <td align="right"><asp:Button ID="Button1" runat="server" Text="Save" class="button1" style="margin-top:5px;" /></td>
  </tr>
  <tr class="accordionContent open">
    <td valign="top" width="200">
      <asp:DropDownList runat="server" ID="DropDownList1">
        <asp:ListItem>My saved message name</asp:ListItem>
      </asp:DropDownList>
    </td>
    <td width="350"><asp:TextBox ID="TextBox2" runat="server" TextMode="MultiLine" Rows="5" Text="" style="overflow-y:scroll" /></td>
    <td valign="top"><img id="Img1" src="/Assets/Images/button_x_close_on.png" onclick="alert('close1')" alt="Main Logo" width="16" height="15"></td>
    <td valign="top" align="right"></td>
  </tr>

</table>

</asp:Content>
