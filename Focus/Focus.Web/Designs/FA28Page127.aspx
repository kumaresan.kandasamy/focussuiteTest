﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page127.aspx.cs" Inherits="Focus.Web.Designs.FA28Page127" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<table width="100%">
  <tr>
    <td width="140"><h1>Account settings</h1></td>
    <td width="300"></td>
    <td align="right"></td>
  </tr>
  <tr>
    <td colspan="2"><strong>Change password</strong>  * <i>required ﬁelds</i></td>
    <td align="right"><asp:Button ID="Button8" runat="server" Text="Save" class="button3" /></td>
  </tr>
  <tr>
    <td>Current password*</td>
    <td colspan="2">
      <asp:TextBox ID="TextBox2" runat="server" Width="200" />
    </td>
  </tr>
  <tr>
    <td>New password*</td>
    <td colspan="2">
      <asp:TextBox ID="TextBox1" runat="server" Width="200"/>
    </td>
  </tr>
  <tr>
    <td>Retype new password*</td>
    <td colspan="2">
      <asp:TextBox ID="TextBox3" runat="server" Width="200" />
    </td>
  </tr>
</table>

</asp:Content>
