﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page112.aspx.cs" Inherits="Focus.Web.Designs.FA28Page112" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<table width="100%">
  <tr>
    <td><p class="pageNav"><a href="#">Set system defaults</a> | <strong>Manage staff accounts</strong> | <a href="#">Manage home page announcements</a> | <a href="#">Send feedback</a></p></td>
  </tr>
  <tr>
    <td valign="top">
      <asp:Button ID="Button1" runat="server" Text="Save changes" class="button1 right" />
    </td>
  </tr>
  <tr>
    <td><br /></td>
  </tr>
  <tr>
    <td>
      <table class="accordionTable">
        <tr class="multipleAccordionTitle">
          <td><span class="accordionArrow"></span>Search defaults</td>
        </tr>
        <tr class="accordionContent">
          <td>
            <table>
              <tr>
                <td width="150"></td>
                <td width="300"></td>
                <td></td>
              </tr>
              <tr>
                <td valign="top">Search these jobs</td>
                <td colspan="2" align="left">
                  <asp:RadioButton id="Radio1" GroupName="Group1" runat="server"/> jobs matching resume with minimum match score of <asp:TextBox runat="server" ID="TextBox3" Text="" Width="20" /> stars
                </td>
              </tr>
              <tr>
                <td></td>
                <td colspan="2">
                  <asp:RadioButton id="RadioButton1" GroupName="Group1" runat="server"/> all jobs, regardless of match score
                </td>
              </tr>
              <tr>
                <td colspan="3"><br /></td>
              </tr>
              <tr>
                <td>Default search radius</td>
                <td colspan="2">
                <asp:DropDownList runat="server" ID="DropDownList5" Width="47">
                  <asp:ListItem>20</asp:ListItem>
                </asp:DropDownList>
                 miles from job seeker's ZIP code
                </td>
              </tr>
              <tr>
                <td colspan="3"><br /></td>
              </tr>
              <tr>
                <td>Display jobs from</td>
                <td colspan="2">
                  <asp:DropDownList runat="server" ID="DropDownList2" Width="150">
                    <asp:ListItem>-- Select state --</asp:ListItem>
                  </asp:DropDownList>
                  <asp:Button ID="Button2" runat="server" Text="Add" class="button3" />
                </td>
              </tr>
              <tr>
                <td colspan="3"><br /></td>
              </tr>
              <tr>
                <td></td>
                <td rowspan="3">
                  <table class="deletableListItemTable">
                    <tbody>
                      <tr>
                        <td class="column1">Kentucky</td>
                        <td class="column2"><img src="/Assets/Images/button_x_delete_off.png" width="16" height="15" alt="[close]" onclick="alert('deletableListItem1')"></td>
                      </tr>
                      <tr>
                        <td class="column1">Ohio</td>
                        <td class="column2"><img src="/Assets/Images/button_x_delete_off.png" width="16" height="15" alt="[close]" onclick="alert('deletableListItem2')"></td>
                      </tr>
                      <tr>
                        <td class="column1">Ohio</td>
                        <td class="column2"><img src="/Assets/Images/button_x_delete_off.png" width="16" height="15" alt="[close]" onclick="alert('deletableListItem2')"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
                <td>
                  Plus
                  <asp:DropDownList runat="server" ID="DropDownList1">
                    <asp:ListItem>any</asp:ListItem>
                  </asp:DropDownList>
                  radius from 
                </td>
              </tr>
              <tr>
                <td colspan="2"></td>
                <td><asp:RadioButton id="RadioButton2" GroupName="Group1" runat="server"/> selected location(s)</td>
              </tr>
              <tr>
                <td colspan="2"></td>
                <td><asp:RadioButton id="RadioButton3" GroupName="Group1" runat="server"/> this ZIP code <asp:TextBox runat="server" ID="TextBox1" Text="" Width="80" /></td>
              </tr>
              <tr>
                <td colspan="3"><br /></td>
              </tr>
              <tr>
                <td>Expire spidered jobs after</td>
                <td colspan="2">
                  <asp:DropDownList runat="server" ID="DropDownList3" Width="150">
                    <asp:ListItem>60 days</asp:ListItem>
                  </asp:DropDownList>
                </td>
              </tr>
              <tr>
                <td colspan="3"><br /></td>
              </tr>
              <tr>
                <td>Resumes searchable by interested employers?</td>
                <td colspan="2">
                  <asp:DropDownList runat="server" ID="DropDownList4" Width="150">
                    <asp:ListItem>Yes</asp:ListItem>
                  </asp:DropDownList>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr class="multipleAccordionTitle">
          <td><span class="accordionArrow"></span>Alert defaults</td>
        </tr>
        <tr class="accordionContent">
          <td>
            <table width="100%">
              <tr>
                <td width="100">Job alerts</td>
                <td width="160">
                  <asp:DropDownList runat="server" ID="DropDownList6" Width="150">
                    <asp:ListItem>on</asp:ListItem>
                  </asp:DropDownList>
                </td>
                <td width="100">Alert frequency</td>
                <td>
                  <asp:DropDownList runat="server" ID="DropDownList7" Width="150">
                    <asp:ListItem>Weekly</asp:ListItem>
                  </asp:DropDownList>
                </td>
              </tr>
              <tr>
                <td>Job seeker email</td>
                <td colspan="3">
                  <asp:CheckBox runat="server" id="CheckBox1" Text="Send weekly 'other jobs that might interest you' email" TextAlign="Right" />
                </td>
              </tr>
              <tr>
                <td>Staff email alerts</td>
                <td colspan="3">
                  <asp:DropDownList runat="server" Width="100">
                    <asp:ListItem Text="none" />
                    <asp:ListItem Text="text2" />
                  </asp:DropDownList>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr class="multipleAccordionTitle">
          <td><span class="accordionArrow"></span>Approval Defaults</td>
        </tr>
        <tr class="accordionContent">
          <td>
            <table width="100%">
              <tr>
                <td width="150px"></td>
                <td></td>
              </tr>
              <tr>
                <td>New resume approvals</td>
                <td>
                  <asp:DropDownList runat="server" ID="DropDownList8">
                    <asp:ListItem>approval required, but system is available</asp:ListItem>
                  </asp:DropDownList>
                </td>
              </tr>
              <tr>
                <td colspan="2"><br /></td>
              </tr>
              <tr>
                <td>Job posting approvals</td>
                <td>
                  <asp:DropDownList runat="server" ID="DropDownList9">
                    <asp:ListItem>approval required, but system is available</asp:ListItem>
                  </asp:DropDownList>
                </td>
              </tr>
              <tr>
                <td colspan="2"><br /></td>
              </tr>
              <tr>
                <td>Resume referral approvals</td>
                <td>
                  <asp:DropDownList runat="server" ID="DropDownList10">
                    <asp:ListItem>self-referral requires 3+ star match</asp:ListItem>
                  </asp:DropDownList>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr class="multipleAccordionTitle">
          <td><span class="accordionArrow"></span>Flag Defaults</td>
        </tr>
        <tr class="accordionContent">
          <td>
            <table width="100%">
              <tr>
                <td width="180px"></td>
                <td></td>
              </tr>
              <tr>
                <td valign="top">Enable these job seeker flags</td>
                <td>
                  <asp:CheckBox Text="Seeker not logging in" runat="server" />
                  <br />
                  <table>
                    <tr>
                      <td width="12"></td>
                      <td>Flag if </td>
                      <td colspan="2">
                        <%= HtmlInFieldLabel("EmployerNameTextBox2", "Language.InlineLabel", "7", 20)%>
                        <asp:TextBox ID="EmployerNameTextBox2" runat="server" Width="20" ClientIDMode="Static" AutoCompleteType="Disabled" />
                      </td>
                      <td><%= HtmlLocalise("TopLink1", "days since last login (self-service only)")%></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td valign="top"></td>
                <td>
                  <asp:CheckBox ID="CheckBox3" Text="Seeker not clicking on leads" runat="server" />
                  <br />
                  <table>
                    <tr>
                      <td width="12"></td>
                      <td>Flag if fewer than </td>
                      <td colspan="2">
                        <%= HtmlInFieldLabel("EmployerNameTextBox2", "Language.InlineLabel", "7", 20)%>
                        <asp:TextBox ID="TextBox10" runat="server" Width="20" ClientIDMode="Static" AutoCompleteType="Disabled" />
                      </td>
                      <td><%= HtmlLocalise("TopLink1", "leads viewed in")%></td>
                      <td colspan="2">
                        <%= HtmlInFieldLabel("EmployerNameTextBox2", "Language.InlineLabel", "14", 20)%>
                        <asp:TextBox ID="TextBox11" runat="server" Width="20" ClientIDMode="Static" AutoCompleteType="Disabled" />
                      </td>
                      <td><%= HtmlLocalise("TopLink1", "days")%></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td valign="top"></td>
                <td>
                  <asp:CheckBox ID="CheckBox4" Text="Seeker rejecting job offers" runat="server" />
                  <br />
                  <table>
                    <tr>
                      <td width="12"></td>
                      <td>Flag if </td>
                      <td colspan="2">
                        <%= HtmlInFieldLabel("EmployerNameTextBox2", "Language.InlineLabel", "7", 20)%>
                        <asp:TextBox ID="TextBox12" runat="server" Width="20" ClientIDMode="Static" AutoCompleteType="Disabled" />
                      </td>
                      <td><%= HtmlLocalise("TopLink1", "or more job offers rejected in")%></td>
                      <td colspan="2">
                        <%= HtmlInFieldLabel("EmployerNameTextBox2", "Language.InlineLabel", "14", 20)%>
                        <asp:TextBox ID="TextBox13" runat="server" Width="20" ClientIDMode="Static" AutoCompleteType="Disabled" />
                      </td>
                      <td><%= HtmlLocalise("TopLink1", "days")%></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td valign="top"></td>
                <td>
                  <asp:CheckBox ID="CheckBox5" Text="Seeker not reporting for interviews" runat="server" />
                  <br />
                  <table>
                    <tr>
                      <td width="12"></td>
                      <td><%= HtmlLocalise("TopLink1", "Flag if seeker did not report for ")%></td>
                      <td colspan="2">
                        <%= HtmlInFieldLabel("EmployerNameTextBox2", "Language.InlineLabel", "7", 20)%>
                        <asp:TextBox ID="TextBox14" runat="server" Width="20" ClientIDMode="Static" AutoCompleteType="Disabled" />
                      </td>
                      <td><%= HtmlLocalise("TopLink1", "or more interviews in")%></td>
                      <td colspan="2">
                        <%= HtmlInFieldLabel("EmployerNameTextBox2", "Language.InlineLabel", "14", 20)%>
                        <asp:TextBox ID="TextBox15" runat="server" Width="20" ClientIDMode="Static" AutoCompleteType="Disabled" />
                      </td>
                      <td><%= HtmlLocalise("TopLink1", "days")%></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td valign="top"></td>
                <td>
                  <asp:CheckBox ID="CheckBox6" Text="Seeker not responding to employer invitations" runat="server" />
                  <br />
                  <table>
                    <tr>
                      <td width="12"></td>
                      <td><%= HtmlLocalise("TopLink1", "Flag if seeker did not respond to ")%></td>
                      <td colspan="2">
                        <%= HtmlInFieldLabel("EmployerNameTextBox2", "Language.InlineLabel", "7", 20)%>
                        <asp:TextBox ID="TextBox16" runat="server" Width="20" ClientIDMode="Static" AutoCompleteType="Disabled" />
                      </td>
                      <td><%= HtmlLocalise("TopLink1", "or more employer invitations in")%></td>
                      <td colspan="2">
                        <%= HtmlInFieldLabel("EmployerNameTextBox2", "Language.InlineLabel", "14", 20)%>
                        <asp:TextBox ID="TextBox17" runat="server" Width="20" ClientIDMode="Static" AutoCompleteType="Disabled" />
                      </td>
                      <td><%= HtmlLocalise("TopLink1", "days")%></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td valign="top"></td>
                <td>
                  <asp:CheckBox ID="CheckBox7" Text="Seeker not responding to referral suggestions" runat="server" />
                  <br />
                  <table>
                    <tr>
                      <td width="12"></td>
                      <td><%= HtmlLocalise("TopLink1", "Flag if seeker did not respond to ")%></td>
                      <td colspan="2">
                        <%= HtmlInFieldLabel("EmployerNameTextBox2", "Language.InlineLabel", "7", 20)%>
                        <asp:TextBox ID="TextBox18" runat="server" Width="20" ClientIDMode="Static" AutoCompleteType="Disabled" />
                      </td>
                      <td><%= HtmlLocalise("TopLink1", "or more referral suggestions in")%></td>
                      <td colspan="2">
                        <%= HtmlInFieldLabel("EmployerNameTextBox2", "Language.InlineLabel", "14", 20)%>
                        <asp:TextBox ID="TextBox19" runat="server" Width="20" ClientIDMode="Static" AutoCompleteType="Disabled" />
                      </td>
                      <td><%= HtmlLocalise("TopLink1", "days")%></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td valign="top"></td>
                <td>
                  <asp:CheckBox ID="CheckBox8" Text="Seeker self-referring to unqualified jobs" runat="server" />
                  <br />
                  <table>
                    <tr>
                      <td width="12"></td>
                      <td><%= HtmlLocalise("TopLink1", "If matches exist for ")%></td>
                      <td colspan="2">
                        <asp:DropDownList runat="server">
                          <asp:ListItem Text="3 stars" />
                          <asp:ListItem Text="text2" />
                        </asp:DropDownList>
                      </td>
                      <td>or above, flag if seeker self-referred to</td>
                      <td colspan="2">
                        <%= HtmlInFieldLabel("EmployerNameTextBox2", "Language.InlineLabel", "5", 20)%>
                        <asp:TextBox ID="TextBox20" runat="server" Width="20" ClientIDMode="Static" AutoCompleteType="Disabled" />
                      </td>
                      <td>or more jobs that are</td>
                      <td colspan="2">
                        <asp:DropDownList ID="DropDownList19" runat="server">
                          <asp:ListItem Text="3 stars" />
                          <asp:ListItem Text="text2" />
                        </asp:DropDownList>
                      </td>
                      <td>or below in</td>
                      <td colspan="2">
                        <%= HtmlInFieldLabel("EmployerNameTextBox2", "Language.InlineLabel", "14", 20)%>
                        <asp:TextBox ID="TextBox21" runat="server" Width="20" ClientIDMode="Static" AutoCompleteType="Disabled" />
                      </td>
                      <td><%= HtmlLocalise("TopLink1", "days")%></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td valign="top"></td>
                <td>
                  <asp:CheckBox ID="CheckBox9" Text="Showing low-quality matches" runat="server" />
                  <br />
                  <table>
                    <tr>
                      <td width="12"></td>
                      <td><%= HtmlLocalise("TopLink1", "Flag if fewer than ")%></td>
                      <td colspan="2">
                        <%= HtmlInFieldLabel("EmployerNameTextBox2", "Language.InlineLabel", "7", 20)%>
                        <asp:TextBox ID="TextBox22" runat="server" Width="20" ClientIDMode="Static" AutoCompleteType="Disabled" />
                      </td>
                      <td><%= HtmlLocalise("TopLink1", "matches of")%></td>
                      <td>
                        <asp:DropDownList ID="DropDownList20" runat="server">
                          <asp:ListItem Text="3 stars" />
                          <asp:ListItem Text="text2" />
                        </asp:DropDownList>
                      </td>
                      <td><%= HtmlLocalise("TopLink1", "or above in")%></td>
                      <td colspan="2">
                        <%= HtmlInFieldLabel("EmployerNameTextBox2", "Language.InlineLabel", "14", 20)%>
                        <asp:TextBox ID="TextBox23" runat="server" Width="20" ClientIDMode="Static" AutoCompleteType="Disabled" />
                      </td>
                      <td><%= HtmlLocalise("TopLink1", "days")%></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td valign="top"></td>
                <td>
                  <asp:CheckBox ID="CheckBox10" Text="Posting poor-quality resume" runat="server" />
                  <br />
                  <table>
                    <tr>
                      <td width="12"></td>
                      <td><%= HtmlLocalise("TopLink1", "Flag if no matches of")%></td>
                      <td>
                        <asp:DropDownList ID="DropDownList21" runat="server">
                          <asp:ListItem Text="3 stars" />
                          <asp:ListItem Text="text2" />
                        </asp:DropDownList>
                      </td>
                      <td><%= HtmlLocalise("TopLink1", "or above in 14 days")%></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td valign="top"></td>
                <td>
                  <asp:CheckBox ID="CheckBox11" Text="Requiring post-hire followup" runat="server" />
                  <br />
                  <table>
                    <tr>
                      <td width="12"></td>
                      <td><%= HtmlLocalise("TopLink1", "Hire reported by employer, staff or job seeker")%></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr class="multipleAccordionTitle">
          <td><span class="accordionArrow"></span>Message defaults</td>
        </tr>
        <tr class="accordionContent">
          <td>
            <table>
              <tr>
                <td width="160px"></td>
                <td width="400px"></td>
              </tr>
              <tr>
                <td colspan="2"><strong>Focus/Assist default messages</strong></td>
              </tr>
              <tr>
                <td colspan="2"><br /></td>
              </tr>
              <tr>
                <td valign="top">
                  <asp:DropDownList runat="server" ID="DropDownList11">
                    <asp:ListItem>Default message 1</asp:ListItem>
                  </asp:DropDownList>
                </td>
                <td>
                  <asp:TextBox ID="TextBox2" runat="server" TextMode="MultiLine" Rows="5" Text="" style="overflow-y:scroll" />
                </td>
              </tr>
              <tr>
                <td></td>
                <td>
                  <asp:DropDownList runat="server" ID="DropDownList12">
                    <asp:ListItem>insert variable</asp:ListItem>
                  </asp:DropDownList>
                   <asp:Button ID="Button3" runat="server" Text="Add" class="button3" />
                </td>
              </tr>
              <tr>
                <td colspan="2"><br /></td>
              </tr>
              <tr>
                <td colspan="2"><strong>Focus/Career default messages</strong></td>
              </tr>
              <tr>
                <td colspan="2"><br /></td>
              </tr>
              <tr>
                <td valign="top">
                  <asp:DropDownList runat="server" ID="DropDownList13">
                    <asp:ListItem>Default message 1</asp:ListItem>
                  </asp:DropDownList>
                </td>
                <td>
                  <table style="margin-top:-6px">
                    <tr>
                      <td>Send job seeker inactivity warnings</td>
                      <td>
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Text="yes" />
                          <asp:ListItem Text="no" />
                        </asp:RadioButtonList>
                      </td>
                    </tr>
                    </table>
                    <table style="margin-top:-6px">
                    <tr>
                      <td><%= HtmlLocalise("TopLink1", "Send email after ")%></td>
                      <td>
                        <asp:TextBox ID="EmployerNameTextBox" runat="server" Width="20" ClientIDMode="Static" AutoCompleteType="Disabled" />
                      </td>
                      <td>days of inactivity</td>
                    </tr>
                  </table style="margin-top:-6px">
                  <table>
                    <tr>
                      <td><%= HtmlLocalise("TopLink1", "Job seekers must respond within")%></td>
                      <td>
                        <asp:TextBox ID="TextBox24" runat="server" Width="20" ClientIDMode="Static" AutoCompleteType="Disabled" />
                      </td>
                      <td>days</td>
                    </tr>
                  </table>
                  <asp:TextBox ID="TextBox4" runat="server" TextMode="MultiLine" Rows="5" Text="" style="overflow-y:scroll" />
                </td>
              </tr>
              <tr>
                <td></td>
                <td>
                  <asp:DropDownList runat="server" ID="DropDownList14">
                    <asp:ListItem>insert variable</asp:ListItem>
                  </asp:DropDownList>
                   <asp:Button ID="Button4" runat="server" Text="Add" class="button3" />
                </td>
              </tr>
              <tr>
                <td colspan="2"><br /></td>
              </tr>
              <tr>
                <td colspan="2"><strong>Focus/Talent default messages</strong></td>
              </tr>
              <tr>
                <td colspan="2"><br /></td>
              </tr>
              <tr>
                <td valign="top">
                  <asp:DropDownList runat="server" ID="DropDownList15">
                    <asp:ListItem>Default message 1</asp:ListItem>
                  </asp:DropDownList>
                </td>
                <td>
                  <asp:TextBox ID="TextBox5" runat="server" TextMode="MultiLine" Rows="5" Text="" style="overflow-y:scroll" />
                </td>
              </tr>
              <tr>
                <td></td>
                <td>
                  <asp:DropDownList runat="server" ID="DropDownList16">
                    <asp:ListItem>insert variable</asp:ListItem>
                  </asp:DropDownList>
                   <asp:Button ID="Button5" runat="server" Text="Add" class="button3" />
                </td>
              </tr>
              <tr>
                <td colspan="2"><br /></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr class="multipleAccordionTitle">
          <td><span class="accordionArrow"></span>Job order routing defaults</td>
        </tr>
        <tr class="accordionContent">
          <td>
            <table>
              <tr>
                <td colspan="2"><strong>Handle job orders from specific locations</strong></td>
              </tr>
              <tr>
                <td width="160px" valign="top">
                  <asp:RadioButtonList ID="RadioButtonList2" runat="server">
                    <asp:ListItem Text="Statewide" />
                    <asp:ListItem Text="Work locations" />
                  </asp:RadioButtonList>
                </td>
                <td width="400px" style="padding-top:33px">
                  <asp:DropDownList runat="server" ID="DropDownList23">
                    <asp:ListItem>- select an office -</asp:ListItem>
                  </asp:DropDownList>
                   <asp:Button ID="Button14" runat="server" Text="Add" class="button3" />
                </td>
              </tr>
              <tr>
                <td>
                  
                </td>
                <td>
                  <asp:TextBox ID="TextBox25" runat="server" TextMode="MultiLine" Rows="5" Text="" style="overflow-y:scroll" />
                </td>
              </tr>
            </table>

            <table>
              <tr>
                <td width="160px" valign="top">
                  <asp:RadioButtonList ID="RadioButtonList3" runat="server">
                    <asp:ListItem Text="HQ locations" />
                  </asp:RadioButtonList>
                </td>
                <td width="400px">
                  <asp:DropDownList runat="server" ID="DropDownList22">
                    <asp:ListItem>- select a city -</asp:ListItem>
                  </asp:DropDownList>
                   <asp:Button ID="Button15" runat="server" Text="Add" class="button3" />
                </td>
              </tr>
              <tr>
                <td>
                  
                </td>
                <td>
                  <asp:TextBox ID="TextBox26" runat="server" TextMode="MultiLine" Rows="5" Text="" style="overflow-y:scroll" />
                </td>
              </tr>
            </table>

            <table>
              <tr>
                <td width="160px" valign="top">
                  <asp:RadioButtonList ID="RadioButtonList4" runat="server">
                    <asp:ListItem Text="Employer user locations" />
                  </asp:RadioButtonList>
                </td>
                <td width="400px">
                  <asp:DropDownList runat="server" ID="DropDownList24">
                    <asp:ListItem>- select a city -</asp:ListItem>
                  </asp:DropDownList>
                   <asp:Button ID="Button16" runat="server" Text="Add" class="button3" />
                </td>
              </tr>
              <tr>
                <td>
                  
                </td>
                <td>
                  <asp:TextBox ID="TextBox27" runat="server" TextMode="MultiLine" Rows="5" Text="" style="overflow-y:scroll" />
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr class="multipleAccordionTitle">
          <td><span class="accordionArrow"></span>Activity code defaults</td>
        </tr>
        <tr class="accordionContent">
          <td>
            <table>
              <tr>
                <td width="90px">Activity code</td>
                <td width="162px"></td>
                <td width="130px"></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td colspan="3">
                  <asp:DropDownList runat="server" ID="DropDownList18" Width="156">
                    <asp:ListItem>New activity</asp:ListItem>
                  </asp:DropDownList> 
                  <asp:Button ID="Button11" runat="server" Text="Edit" class="button3" style="margin-left:6px;" /> 
                  <asp:Button ID="Button12" runat="server" Text="Delete" class="button1" style="margin-left:6px;" />
                </td>
              </tr>
              <tr>
                <td><br /></td>
              </tr>
              <tr>
                <td></td>
                <td>
                  <span class="inFieldLabel">
                    <label for="ActivityName">Activity name</label>
                    <asp:TextBox runat="server" data-val="true" data-val-required="hexCode1" ID="ActivityName" Width="150" ClientIDMode="Static" />
                  </span>
                </td>
                <td>
                  <span class="inFieldLabel">
                    <label for="CodeNumber">Code number</label>
                    <asp:TextBox runat="server" data-val="true" data-val-required="hexCode1" ID="CodeNumber" Width="120" ClientIDMode="Static" />
                  </span>
                </td>
                <td><asp:Button ID="Button9" runat="server" Text="Save" class="button3" /> </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr class="multipleAccordionTitle">
          <td><span class="accordionArrow"></span>Color defaults <asp:Button ID="Button10" runat="server" Text="Reset colors" class="button3" style="" /></td>
        </tr>
        <tr class="accordionContent">
          <td>
            <table>
              <tr>
                <td>Light color</td>
                <td>
                    <%= HtmlInFieldLabel("Hex1", "Language.InlineLabel", "6-digit-hex-code", 100)%>
                    <asp:TextBox ID="Hex1" runat="server" Width="100" ClientIDMode="Static" AutoCompleteType="Disabled" />
                </td>
              </tr>
              <tr>
                <td>Dark color</td>
                <td>
                    <%= HtmlInFieldLabel("Hex2", "Language.InlineLabel", "6-digit-hex-code", 100)%>
                    <asp:TextBox ID="Hex2" runat="server" Width="100" ClientIDMode="Static" AutoCompleteType="Disabled" />
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr class="multipleAccordionTitle">
          <td><span class="accordionArrow"></span>Logo defaults</td>
        </tr>
        <tr class="accordionContent">
          <td>
            <table>
              <tr>
                <td colspan="2"><i>Logos must be in GIF, JPG, or PNG format at 72dpi. Maximum size 300 pixels wide by 150 pixels high.</i></td>
              </tr>
              <tr>
                <td colspan="2">Select logo to display in:</td>
              </tr>
              <tr>
                <td colspan="2"><br /></td>
              </tr>
              <tr>
                <td width="100px">Focus/Assist</td>
                <td>
                  <span class="LogoUpload">
                    <asp:AsyncFileUpload OnClientUploadError="uploadError"
                        OnClientUploadComplete="uploadComplete" runat="server"
                        ID="file" CssClass="fileUpload" Width="100px" UploaderStyle="Modern"
                        UploadingBackColor="#CCFFFF" ThrobberID="myThrobber" ClientIDMode="static"
                    />
                  </span>
                </td>
              </tr>
              <tr>
                <td>Focus/Talent</td>
                <td>
                  <span class="LogoUpload">
                    <asp:AsyncFileUpload OnClientUploadError="uploadError"
                        OnClientUploadComplete="uploadComplete" runat="server"
                        ID="AsyncFileUpload1" CssClass="fileUpload" Width="100px" UploaderStyle="Modern"
                        UploadingBackColor="#CCFFFF" ThrobberID="myThrobber" ClientIDMode="static"
                    />
                  </span>
                </td>
              </tr>
              <tr>
                <td></td>
                <td>
                  <img id="mainLogo" src="Assets/Images/logo_focustalent.png" alt="Main Logo" width="204" height="40">
                  <img id="closeLogo" src="Assets/Images/button_x_close_on.png" onclick="alert('close1')" alt="Main Logo" width="16" height="15">
                </td>
              </tr>
              <tr>
                <td>Footer logo</td>
                <td><asp:CheckBox runat="server" id="CheckBox2" Text="Display Burning Glass logo in Focus/Assist & Focus/Talent footers" TextAlign="Right" /></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr class="multipleAccordionTitle">
          <td><span class="accordionArrow"></span>Sharing defaults</td>
        </tr>
        <tr class="accordionContent">
          <td>
            <table>
              <tr>
                <td><asp:CheckBox Text="Employer users at the same FEIN may share job postings" runat="server" /></td>
              </tr>
              <tr>
                <td><asp:CheckBox ID="CheckBox12" Text="Staff may share queues, job postings, lists, and message templates" runat="server" /></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr class="multipleAccordionTitle">
          <td><span class="accordionArrow"></span>Preference defaults</td>
        </tr>
        <tr class="accordionContent">
          <td>
            <table>
              <tr>
                <td><asp:RadioButton id="RadioButton8" GroupName="Group1" Text="Employer preferences override system defaults" runat="server"/></td>
              </tr>
              <tr>
                <td><asp:RadioButton id="RadioButton9" GroupName="Group1" Text="System defaults override employer preferences" runat="server"/></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr class="multipleAccordionTitle">
          <td><span class="accordionArrow"></span>Staff access to Labor/Insight</td>
        </tr>
        <tr class="accordionContent">
          <td>
            <table width="100%">
              <tr>
                <td width="100px"><asp:RadioButton id="RadioButton6" GroupName="Group1" Text="Yes" runat="server"/></td>
              </tr>
              <tr>
                <td><asp:RadioButton id="RadioButton7" GroupName="Group1" Text="No" runat="server"/></td>
              </tr>
              <tr>
                <td align="right"><asp:Button ID="Button8" runat="server" Text="Save changes" class="button1" /></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr class="multipleAccordionTitle">
          <td><span class="accordionArrow"></span>Locate account</td>
        </tr>
        <tr class="accordionContent">
          <td>
            <table>
              <tr>
                <td width="90">First name</td>
                <td><asp:TextBox runat="server" ID="TextBox6" Text="" Width="140" /></td>
                <td></td>
              </tr>
              <tr>
                <td colspan="3"><br /></td>
              </tr>
              <tr>
                <td width="80">Last name</td>
                <td><asp:TextBox runat="server" ID="TextBox7" Text="" Width="140" /></td>
                <td></td>
              </tr>
              <tr>
                <td colspan="3"><br /></td>
              </tr>
              <tr>
                <td width="80">Email address</td>
                <td><asp:TextBox runat="server" ID="TextBox8" Text="" Width="140" /></td>
                <td></td>
              </tr>
              <tr>    
                <td colspan="3"><br /></td>
              </tr>
              <tr>
                <td width="80">Ofﬁce</td>
                <td><asp:TextBox runat="server" ID="TextBox9" Text="" Width="140" /></td>
                <td></td>
              </tr>
              <tr>
                <td colspan="3"><br /></td>
              </tr>
              <tr>
                <td colspan="2"></td>
                <td><asp:Button ID="Button13" runat="server" Text="Find" class="button3" /></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</asp:Content>
