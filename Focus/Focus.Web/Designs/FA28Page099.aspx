﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page099.aspx.cs" Inherits="Focus.Web.Designs.FA28Page099" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">


<table width="100%">
    <tr>
      <td>
        <p class="pageNav">
          <a href="#"><%= HtmlLocalise("JobOrderDashboard", "Job seeker reports")%></a> | 
          <strong><%= HtmlLocalise("JobDevelopment", "Job order reports")%></strong> | 
          <a href="#"><%= HtmlLocalise("FindEmployer", "Employer reports")%></a> | 
          <a href="#"><%= HtmlLocalise("SendMessages", "EEO reports")%></a>
          <a href="#"><%= HtmlLocalise("FindEmployer", "Staff activity reports")%></a> | 
          <a href="#"><%= HtmlLocalise("SendMessages", "Lookup")%></a>
          <a href="#"><%= HtmlLocalise("FindEmployer", "Saved reports")%></a> | 
          <a href="#"><%= HtmlLocalise("SendMessages", "Labor/Insight")%></a>
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%">
          <tr>
            <td><a href="#"><%= HtmlLocalise("TopLink1", "return to report")%></a></td>
            <td align="right">
              <asp:DropDownList ID="DropDownList1" runat="server">
                <asp:ListItem Text="Export" />
              </asp:DropDownList>&nbsp;&nbsp;
              <asp:Button ID="Button1" runat="server" Text="print" class="button1 right" />
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td><h1><%= HtmlLocalise("SendMessages", "Employer profile: Nifty Networks")%></h1></td>
    </tr>
    <tr>
      <td>
        <table class="accordionTable">
          <tr class="multipleAccordionTitle">
            <td><span id="namedAnchor1" class="accordionArrow"></span>Contact information</td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Nifty Networks")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "742 Evergreen Terrace")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Springfield, KY 12345")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Phone number: 555-555-1212")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Fax number: 555-555-1234")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Nifty Networks")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Another Address")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Another Location")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Phone number: 555-555-1385")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Fax number: 555-382-3828")%></td>
              </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle">
            <td><span id="namedAnchor2" class="accordionArrow"></span><%=HtmlLocalise("Reporting.Results2", "Logins")%></td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table width="100%">
                <tr>
                  <td colspan="4">Number of Focus/Assist logins for all employer hiring managers in past 7 days: 12</td>
                </tr>
                <tr>
                  <td colspan="4">Time since last login: 42 minutes (<a href="#">hsaxon@niftynetworks.com</a>)</td>
                </tr>
                <tr>
                <td width="180">Showing 1-20 of 100 accounts</td>
                <td width="70">
                  <asp:DropDownList runat="server">
                    <asp:ListItem Text="20" />
                    <asp:ListItem Text="text2" />
                  </asp:DropDownList>
                </td>
                <td>accounts per page</td>
                <td align="right">
                  <table class="Pager">
                    <tr>
                      <td>
                        <asp:Label runat="server" ID="Label1"></asp:Label>
                        <%=HtmlLocalise("Reporting.Results2", "result(s) found")%> &nbsp;
                      </td>
                      <td id="td1" runat="server">
                        <a href="#" id="A1">
                          <img id="Img7" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                            width="15" height="11" /></a>
                      </td>
                      <td id="td2" runat="server">
                        <a href="#" id="A2">
                          <img id="Img8" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                            height="11" /></a>
                      </td>
                      <td>
                        PAGE
                      </td>
                      <td>
                        <asp:DropDownList ID="DropDownList3" runat="server">
                        </asp:DropDownList>
                      </td>
                      <td>
                        <%=HtmlLocalise("Reporting.Of2", "of") %>
                        <asp:Label runat="server" ID="Label2" />
                      </td>
                      <td id="td3" runat="server">
                        <a href="#" id="A3">
                          <img id="Img9" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                            width="6" height="11" /></a>
                      </td>
                      <td id="td4" runat="server">
                        <a href="#" id="A4">
                          <img id="Img10" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                            width="15" height="11" /></a>
                      </td>
                      <td style="padding-left: 15px">
                        <asp:DropDownList ID="DropDownList43" runat="server">
                          <asp:ListItem Text="10" Value="10" />
                          <asp:ListItem Text="20" Value="20" />
                          <asp:ListItem Text="50" Value="50" />
                          <asp:ListItem Text="100" Value="100" />
                        </asp:DropDownList>
                      </td>
                      <td>
                        RESULTS PER PAGE
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              </table>
              <table width="100%">
                <tr>
                  <td colspan="4">
                    <table width="100%" class="DataTable">
                      <thead>
                        <tr>
                          <th width="25%">
                            <%= HtmlLocalise("RecentPlacementsTH21", "NAME")%>
                            <img id="Img11" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img12" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH22", "TITLE")%>
                            <img id="Img13" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img14" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH24", "HIRING AREAS")%>
                            <img id="Img17" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img18" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH24", "EMAIL ADDRESS")%>
                            <img id="Img1" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img2" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH24", "PHONE NUMBER")%>
                            <img id="Img3" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img4" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH24", "LAST LOGIN")%>
                            <img id="Img5" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img6" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Joe Accountholder")%></a></td>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "Hiring manager")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "Marketing, Sales, Support")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "joe@niftynetworks.com")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "555-555-1212")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "Sep 14 2012, 4:02pm")%></td>
                        </tr>
                        <tr>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Joe Accountholder")%></a></td>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "Hiring manager")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "Marketing, Sales, Support")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "joe@niftynetworks.com")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "555-555-1212")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "Sep 14 2012, 4:02pm")%></td>
                        </tr>
                        <tr>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Joe Accountholder")%></a></td>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "Hiring manager")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "Marketing, Sales, Support")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "joe@niftynetworks.com")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "555-555-1212")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "Sep 14 2012, 4:02pm")%></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle">
            <td><span id="Span2" class="accordionArrow"></span><%=HtmlLocalise("Reporting.Results2", "Activity summary")%></td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table>
                <tr>
                  <td colspan="2">
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                      <asp:ListItem Text="Last 7 days" />
                      <asp:ListItem Text="Last 30 days" />
                    </asp:RadioButtonList>
                  </td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Number of job orders open: 15")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Number of job orders edited by staff: 2")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Number of resumes referred: 56")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Number of job seekers interviewed: 15")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Number of job seekers rejected: 14")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Number of job seekers hired: 1")%></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle">
            <td><span id="Span1" class="accordionArrow"></span><%=HtmlLocalise("Reporting.Results2", "Activity log")%></td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table width="100%">
                <tr>
                <td width="100"><%=HtmlLocalise("Reporting.Results2", "Show results for")%></td>
                <td width="107">
                  <asp:DropDownList ID="DropDownList23" runat="server">
                    <asp:ListItem Text="last 30 days" />
                  </asp:DropDownList>
                </td>
                <td width="15"><%=HtmlLocalise("Reporting.Results2", "in")%></td>
                <td width="107">
                  <asp:DropDownList ID="DropDownList24" runat="server">
                    <asp:ListItem Text="descending" />
                  </asp:DropDownList>
                </td>
                <td width="50"><%=HtmlLocalise("Reporting.Results2", "order")%></td>
                <td><a href="#"><%=HtmlLocalise("Reporting.Results2", "Download this log (CSV file)")%></a></td>
                <td align="right">
                  <table class="Pager">
                    <tr>
                      <td>
                        <asp:Label runat="server" ID="Label3"></asp:Label>
                        <%=HtmlLocalise("Reporting.Results2", "result(s) found")%> &nbsp;
                      </td>
                      <td id="td5" runat="server">
                        <a href="#" id="A5">
                          <img id="Img15" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                            width="15" height="11" /></a>
                      </td>
                      <td id="td6" runat="server">
                        <a href="#" id="A6">
                          <img id="Img16" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                            height="11" /></a>
                      </td>
                      <td>
                        PAGE
                      </td>
                      <td>
                        <asp:DropDownList ID="DropDownList4" runat="server">
                        </asp:DropDownList>
                      </td>
                      <td>
                        <%=HtmlLocalise("Reporting.Of2", "of") %>
                        <asp:Label runat="server" ID="Label4" />
                      </td>
                      <td id="td7" runat="server">
                        <a href="#" id="A7">
                          <img id="Img19" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                            width="6" height="11" /></a>
                      </td>
                      <td id="td8" runat="server">
                        <a href="#" id="A8">
                          <img id="Img20" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                            width="15" height="11" /></a>
                      </td>
                      <td style="padding-left: 15px">
                        <asp:DropDownList ID="DropDownList7" runat="server">
                          <asp:ListItem Text="10" Value="10" />
                          <asp:ListItem Text="20" Value="20" />
                          <asp:ListItem Text="50" Value="50" />
                          <asp:ListItem Text="100" Value="100" />
                        </asp:DropDownList>
                      </td>
                      <td>
                        RESULTS PER PAGE
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              </table>
              <table width="100%">
                <tr>
                  <td colspan="4">
                    <table width="100%" class="DataTable">
                      <thead>
                        <tr>
                          <th width="25%">
                            <%= HtmlLocalise("RecentPlacementsTH21", "ACTION DATE/TIME")%>
                            <img id="Img21" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img22" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH22", "USERNAME")%>
                            <img id="Img23" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img24" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH24", "ACTION")%>
                            <img id="Img25" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img26" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "3/13/12 6:55")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "dev@employer.com")%></a></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "SaveJob")%></td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "3/13/12 6:55")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "dev@employer.com")%></a></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "SaveJob")%></td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "3/13/12 6:55")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "dev@employer.com")%></a></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "SaveJob")%></td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "3/13/12 6:55")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "dev@employer.com")%></a></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "SaveJob")%></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle">
            <td><span id="Span6" class="accordionArrow"></span><%=HtmlLocalise("Reporting.Results2", "Referral summary")%></td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table width="100%">
                <tr>
                <td width="100"><%=HtmlLocalise("Reporting.Results2", "Show results for")%></td>
                <td width="107">
                  <asp:DropDownList ID="DropDownList2" runat="server">
                    <asp:ListItem Text="last 30 days" />
                  </asp:DropDownList>
                </td>
                <td width="78"><%=HtmlLocalise("Reporting.Results2", "Order status")%></td>
                <td width="112">
                  <asp:DropDownList ID="DropDownList8" runat="server">
                    <asp:ListItem Text="Order status" />
                  </asp:DropDownList>
                </td>
                <td width="50"><%=HtmlLocalise("Reporting.Results2", "Display")%></td>
                <td>
                  <asp:DropDownList ID="DropDownList11" runat="server">
                    <asp:ListItem Text="All referals" />
                  </asp:DropDownList>
                </td>
                <td align="right">
                  <table class="Pager">
                    <tr>
                      <td>
                        <asp:Label runat="server" ID="Label5"></asp:Label>
                        <%=HtmlLocalise("Reporting.Results2", "result(s) found")%> &nbsp;
                      </td>
                      <td id="td9" runat="server">
                        <a href="#" id="A9">
                          <img id="Img27" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                            width="15" height="11" /></a>
                      </td>
                      <td id="td10" runat="server">
                        <a href="#" id="A10">
                          <img id="Img28" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                            height="11" /></a>
                      </td>
                      <td>
                        PAGE
                      </td>
                      <td>
                        <asp:DropDownList ID="DropDownList9" runat="server">
                        </asp:DropDownList>
                      </td>
                      <td>
                        <%=HtmlLocalise("Reporting.Of2", "of") %>
                        <asp:Label runat="server" ID="Label6" />
                      </td>
                      <td id="td11" runat="server">
                        <a href="#" id="A11">
                          <img id="Img29" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                            width="6" height="11" /></a>
                      </td>
                      <td id="td12" runat="server">
                        <a href="#" id="A12">
                          <img id="Img30" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                            width="15" height="11" /></a>
                      </td>
                      <td style="padding-left: 15px">
                        <asp:DropDownList ID="DropDownList10" runat="server">
                          <asp:ListItem Text="10" Value="10" />
                          <asp:ListItem Text="20" Value="20" />
                          <asp:ListItem Text="50" Value="50" />
                          <asp:ListItem Text="100" Value="100" />
                        </asp:DropDownList>
                      </td>
                      <td>
                        RESULTS PER PAGE
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              </table>
              <table width="100%">
                <tr>
                  <td colspan="4">
                    <table width="100%" class="DataTable">
                      <thead>
                        <tr>
                          <th>
                            <%= HtmlLocalise("RecentPlacementsTH21", "JOB ORDER")%>
                            <img id="Img31" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img32" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th>
                            <%= HtmlLocalise("RecentPlacementsTH22", "DATE")%>
                            <img id="Img33" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img34" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th>
                            <%= HtmlLocalise("RecentPlacementsTH24", "REFERRALS")%>
                            <img id="Img35" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img36" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th>
                            <%= HtmlLocalise("RecentPlacementsTH24", "RATING")%>
                            <img id="Img37" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img38" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th>
                            <%= HtmlLocalise("RecentPlacementsTH24", "NAME")%>
                            <img id="Img39" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img40" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th>
                            <%= HtmlLocalise("RecentPlacementsTH24", "RECENT EMPLOYMENT")%>
                            <img id="Img41" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img42" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th>
                            <%= HtmlLocalise("RecentPlacementsTH24", "YEARS OF EXPERIENCE")%>
                            <img id="Img43" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img44" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "JobTitleGoesHere AndItMightBeLong")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "May 15, 2011")%></a></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "May 14, 2011")%></td>
                          <td>*</td>
                          <td>Firstname Lastname</td>
                          <td>Job title – Employer (dates)</td>
                          <td>12 years</td>
                          <td>
                            <asp:DropDownList runat="server">
                              <asp:ListItem Text="self-referral" />
                              <asp:ListItem Text="text2" />
                            </asp:DropDownList>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "JobTitleGoesHere AndItMightBeLong")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "May 15, 2011")%></a></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "May 14, 2011")%></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "JobTitleGoesHere AndItMightBeLong")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "May 15, 2011")%></a></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "May 14, 2011")%></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle">
            <td><span id="Span7" class="accordionArrow"></span><%=HtmlLocalise("Reporting.Results2", "Job seekers placed at this employer")%></td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table width="100%">
                <tr>
                <td align="right">
                  <table class="Pager">
                    <tr>
                      <td>
                        <asp:Label runat="server" ID="Label7"></asp:Label>
                        <%=HtmlLocalise("Reporting.Results2", "result(s) found")%> &nbsp;
                      </td>
                      <td id="td13" runat="server">
                        <a href="#" id="A13">
                          <img id="Img45" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                            width="15" height="11" /></a>
                      </td>
                      <td id="td14" runat="server">
                        <a href="#" id="A14">
                          <img id="Img46" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                            height="11" /></a>
                      </td>
                      <td>
                        PAGE
                      </td>
                      <td>
                        <asp:DropDownList ID="DropDownList14" runat="server">
                        </asp:DropDownList>
                      </td>
                      <td>
                        <%=HtmlLocalise("Reporting.Of2", "of") %>
                        <asp:Label runat="server" ID="Label8" />
                      </td>
                      <td id="td15" runat="server">
                        <a href="#" id="A15">
                          <img id="Img47" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                            width="6" height="11" /></a>
                      </td>
                      <td id="td16" runat="server">
                        <a href="#" id="A16">
                          <img id="Img48" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                            width="15" height="11" /></a>
                      </td>
                      <td style="padding-left: 15px">
                        <asp:DropDownList ID="DropDownList15" runat="server">
                          <asp:ListItem Text="10" Value="10" />
                          <asp:ListItem Text="20" Value="20" />
                          <asp:ListItem Text="50" Value="50" />
                          <asp:ListItem Text="100" Value="100" />
                        </asp:DropDownList>
                      </td>
                      <td>
                        RESULTS PER PAGE
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              </table>
              <table width="100%">
                <tr>
                  <td colspan="4">
                    <table width="100%" class="DataTable">
                      <thead>
                        <tr>
                          <th width="25%">
                            <%= HtmlLocalise("RecentPlacementsTH21", "NAME")%>
                            <img id="Img49" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img50" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH22", "JOB TITLE")%>
                            <img id="Img51" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img52" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH24", "START DATE")%>
                            <img id="Img53" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img54" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Lastname, Firstname")%></a></td>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "JobTitleGoesHere")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "Jul 25, 2012")%></td>
                        </tr>
                        <tr>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Lastname, Firstname")%></a></td>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "JobTitleGoesHere")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "Jul 25, 2012")%></td>
                        </tr>
                        <tr>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Lastname, Firstname")%></a></td>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "JobTitleGoesHere")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "Jul 25, 2012")%></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle on">
            <td><span id="Span5" class="accordionArrow"></span>Notes and reminders</td>
          </tr>
          <tr class="accordionContent open">
            <td>
              <table width="450px">
                <tr>
                  <td colspan="2">
                    <asp:RadioButton Text="add a note for" runat="server" />
                    <asp:DropDownList ID="DropDownList5" runat="server">
                      <asp:ListItem Text="Harold Saxton" />
                      <asp:ListItem Text="text2" />
                    </asp:DropDownList>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <asp:RadioButton ID="RadioButton3" Text="add a reminder for" runat="server" />
                    <asp:DropDownList ID="DropDownList6" runat="server">
                      <asp:ListItem Text="Harold Saxton" />
                      <asp:ListItem Text="text2" />
                    </asp:DropDownList>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <table>
                      <tr>
                        <td><asp:RadioButton ID="RadioButton1" Text="email" runat="server" /></td>
                        <td><asp:RadioButton ID="RadioButton2" Text="post to dashboard on" runat="server" /></td>
                        <td>
                          <%= HtmlInFieldLabel("MM", "Language.InlineLabel", "MM", 26)%>
                          <asp:TextBox ID="MM" runat="server" Width="26" ClientIDMode="Static" AutoCompleteType="Disabled" />
                        </td>
                       <td>
                          <%= HtmlInFieldLabel("DD", "Language.InlineLabel", "DD", 26)%>
                          <asp:TextBox ID="DD" runat="server" Width="26" ClientIDMode="Static" AutoCompleteType="Disabled" />
                        </td>
                       <td>
                          <%= HtmlInFieldLabel("YY", "Language.InlineLabel", "YY", 26)%>
                          <asp:TextBox ID="YY" runat="server" Width="26" ClientIDMode="Static" AutoCompleteType="Disabled" />
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <%= HtmlInFieldLabel("EmployerNameTextBox", "Language.InlineLabel", "Type note or reminder here", 496)%>
                    <asp:TextBox ID="EmployerNameTextBox" runat="server" Width="100%" ClientIDMode="Static" AutoCompleteType="Disabled" TextMode="MultiLine" Rows="4" />
                  </td>
                </tr>
                <tr>
                  <td colspan="2"><asp:Button ID="Button33" runat="server" Text="Add note or reminder" class="button3" /></td>
                </tr>
                <tr>
                  <td colspan="2" align="right">
                    <table class="Pager">
                        <tr>
                          <td>
                            <asp:Label runat="server" ID="lblRecordCount"></asp:Label>
                            <%=HtmlLocalise("Reporting.Results", "result(s) found")%> &nbsp;
                          </td>
                          <td id="tdPageStart" runat="server">
                            <a href="#" id="lnkPagerStart">
                              <img id="PagerStart" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                                width="15" height="11" /></a>
                          </td>
                          <td id="tdPageBack" runat="server">
                            <a href="#" id="lnkPagerPrevious">
                              <img id="PagerBack" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                                height="11" /></a>
                          </td>
                          <td>
                            PAGE
                          </td>
                          <td>
                            <asp:DropDownList ID="ddlPageNumber" runat="server">
                            </asp:DropDownList>
                          </td>
                          <td>
                            <%=HtmlLocalise("Reporting.Of", "of") %>
                            <asp:Label runat="server" ID="lblMaxPageCount" />
                          </td>
                          <td id="tdPageNext" runat="server">
                            <a href="#" id="lnkPagerNext">
                              <img id="PagerNext" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                                width="6" height="11" /></a>
                          </td>
                          <td id="tdPageLast" runat="server">
                            <a href="#" id="lnkPagerEnd">
                              <img id="PagerEnd" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                                width="15" height="11" /></a>
                          </td>
                          <td style="padding-left: 15px">
                            <asp:DropDownList ID="ddlPageSizes" runat="server">
                              <asp:ListItem Text="10" Value="10" />
                              <asp:ListItem Text="20" Value="20" />
                              <asp:ListItem Text="50" Value="50" />
                              <asp:ListItem Text="100" Value="100" />
                            </asp:DropDownList>
                          </td>
                          <td>
                            RESULTS PER PAGE
                          </td>
                        </tr>
                      </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <div>Firstname Lastname – May 15, 2011</div>
                    <div>This note is from staff member Firstname Lastname.</div><br />
                    <div>Firstname Lastname – May 12, 2011</div>
                    <div>This is another note. It might be much longer. Duis autem vel eum iriure dolor in 
                    hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat 
                    nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit 
                    praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem 
                    ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh 
                    euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</div><br />
                    <div>Firstname Lastname – April 18, 2011</div>
                    <div>This note is from staff member Firstname Lastname.</div>
                  </td>
                </tr>
            </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
</table>

</asp:Content>
