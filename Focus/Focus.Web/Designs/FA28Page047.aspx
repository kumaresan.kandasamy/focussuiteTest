﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page047.aspx.cs" Inherits="Focus.Web.Designs.FA28Page047" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<table width="100%">
    <tr>
      <td>
        <p class="pageNav">
          <a href="#"><%= HtmlLocalise("Label", "Job order dashboard")%></a> | 
          <strong><%= HtmlLocalise("Label", "Job development")%></strong> | 
          <a href="#"><%= HtmlLocalise("Label", "Find employer")%></a> | 
          <a href="#"><%= HtmlLocalise("Label", "Send messages")%></a>
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%">
          <tr>
            <td>
            <a href="#namedAnchor1"><%= HtmlLocalise("Label", "Matches to recent placements")%></a> | 
            <a href="#namedAnchor2"><%= HtmlLocalise("Label", "Spidered jobs with good matches")%></a> | <br /> 
            <a href="#namedAnchor3"><%= HtmlLocalise("Label", "Employers with many spidered jobs")%></a> | 
            <a href="#namedAnchor4"><%= HtmlLocalise("Label", "Employers with many placements")%></a></td>
            <td><asp:Button ID="Button1" runat="server" Text="Create new employer &amp; new job order" class="button1 right" /></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td><br /></td>
    </tr>
    <tr>
      <td>
        <table class="accordionTable">
          <tr class="multipleAccordionTitle">
            <td><span id="namedAnchor1" class="accordionArrow"></span><%= HtmlLocalise("Label", "Matches to recent placements")%></td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table width="100%">
                <tr>
                  <td width="40px"><strong>Filter</strong></td>
                  <td width="162px">
                    <%= HtmlInFieldLabel("EmployerNameTextBox", "Language.InlineLabel", "employer name", 156)%>
                    <asp:TextBox ID="EmployerNameTextBox" runat="server" Width="156" ClientIDMode="Static" AutoCompleteType="Disabled" />
                  </td>
                  <td width="130px"><asp:Button ID="Button11" runat="server" Text="Edit" class="button3"/> </td>
                  <td align="right">
                    <table class="Pager">
                      <tr>
                        <td>
                          <asp:Label runat="server" ID="lblRecordCount"></asp:Label>
                          <%=HtmlLocalise("Reporting.Results", "result(s) found")%> &nbsp;
                        </td>
                        <td id="tdPageStart" runat="server">
                          <a href="#" id="lnkPagerStart">
                            <img id="PagerStart" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                              width="15" height="11" /></a>
                        </td>
                        <td id="tdPageBack" runat="server">
                          <a href="#" id="lnkPagerPrevious">
                            <img id="PagerBack" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                              height="11" /></a>
                        </td>
                        <td>
                          PAGE
                        </td>
                        <td>
                          <asp:DropDownList ID="ddlPageNumber" runat="server">
                          </asp:DropDownList>
                        </td>
                        <td>
                          <%=HtmlLocalise("Reporting.Of", "of") %>
                          <asp:Label runat="server" ID="lblMaxPageCount" />
                        </td>
                        <td id="tdPageNext" runat="server">
                          <a href="#" id="lnkPagerNext">
                            <img id="PagerNext" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                              width="6" height="11" /></a>
                        </td>
                        <td id="tdPageLast" runat="server">
                          <a href="#" id="lnkPagerEnd">
                            <img id="PagerEnd" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                              width="15" height="11" /></a>
                        </td>
                        <td style="padding-left: 15px">
                          <asp:DropDownList ID="ddlPageSizes" runat="server">
                            <asp:ListItem Text="10" Value="10" />
                            <asp:ListItem Text="20" Value="20" />
                            <asp:ListItem Text="50" Value="50" />
                            <asp:ListItem Text="100" Value="100" />
                          </asp:DropDownList>
                        </td>
                        <td>
                          RESULTS PER PAGE
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="4">
                    <table width="100%" class="DataTable">
                      <thead>
                        <tr>
                          <th width="25%">
                            <%= HtmlLocalise("Label", "EMPLOYER")%>
                            <img id="Col1Up" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Col1Down" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("Label", "RECENT PLACEMENT")%>
                            <img id="Img1" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img2" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                          <table>
                            <tr>
                              <td><%= HtmlLocalise("Label", "MATCH")%></td>
                              <td><div style="margin-top:-4px"><%= HtmlTooltipWithArrow("Match.Tooltip", @"Tooltip")%></div></td>
                              <td>
                                <img id="Img3" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                                <img id="Img4" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                              </td>
                            </tr>
                          </table>
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("Label", "JOB SEEKER")%>
                            <img id="Img5" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img6" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%"><%= HtmlLocalise("RecentPlacementsTH5", "ACTIONS")%></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("Label", "Nifty Networks")%></td>
                          <td valign="top"><%= HtmlLocalise("Label", "Rickey Smith<br />Software Engineer")%></td>
                          <td valign="top"><img src="../Assets/Images/starsfour.gif" alt="stars" width="80" height="16" /></td>
                          <td valign="top">
                            <%= HtmlLocalise("Name1", "Mickey Smith")%>
                          </td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "Compare resumes")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "View matches to open positions")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd1-3", "Nifty Networks")%></td>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd1-4", "Rickey Smith<br />Software Engineer")%></td>
                          <td valign="top"><img src="../Assets/Images/starsfour.gif" alt="stars" width="80" height="16" /></td>
                          <td valign="top">
                            <%= HtmlLocalise("Name1", "Mickey Smith")%><br />
                            <a href="#" class="othersLink othersId1">+ hide 0 names</a>
                          </td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "Compare resumes")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "View matches to open positions")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                        <tr class="othersContent othersId1">
                          <td valign="top"></td>
                          <td valign="top"></td>
                          <td valign="top"></td>
                          <td valign="top">
                            <%= HtmlLocalise("Name2", "Another name")%>
                          </td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "Compare resumes")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                        <tr class="othersContent othersId1">
                          <td valign="top"></td>
                          <td valign="top"></td>
                          <td valign="top"></td>
                          <td valign="top">
                            <%= HtmlLocalise("Label", "Another name")%>
                          </td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "Compare resumes")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("Label", "Nifty Networks")%></td>
                          <td valign="top"><%= HtmlLocalise("Label", "Rickey Smith<br />Software Engineer")%></td>
                          <td valign="top"><img src="../Assets/Images/starsfour.gif" alt="stars" width="80" height="16" /></td>
                          <td valign="top">
                            <%= HtmlLocalise("Label", "Mickey Smith")%><br />
                            <a href="#" class="othersLink othersId2">+ hide 0 names</a>
                          </td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "Compare resumes")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "View matches to open positions")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                        <tr class="othersContent othersId2">
                          <td valign="top"></td>
                          <td valign="top"></td>
                          <td valign="top"></td>
                          <td valign="top">
                            <%= HtmlLocalise("Name2", "Another name")%>
                          </td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "Compare resumes")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                        <tr class="othersContent othersId2">
                          <td valign="top"></td>
                          <td valign="top"></td>
                          <td valign="top"></td>
                          <td valign="top">
                            <%= HtmlLocalise("Name3", "Another name")%>
                          </td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "Compare resumes")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                        <tr class="othersContent othersId2">
                          <td valign="top"></td>
                          <td valign="top"></td>
                          <td valign="top"></td>
                          <td valign="top">
                            <%= HtmlLocalise("Name3", "Another name")%>
                          </td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "Compare resumes")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle">
            <td><span id="namedAnchor2" class="accordionArrow"></span><%= HtmlLocalise("Label", "Spidered jobs with good matches")%></td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table width="100%">
                <tr>
                  <td width="40px"><strong>Filter</strong></td>
                  <td width="162px">
                    <%= HtmlInFieldLabel("EmployerNameTextBox2", "Language.InlineLabel", "employer name", 156)%>
                    <asp:TextBox ID="EmployerNameTextBox2" runat="server" Width="156" ClientIDMode="Static" AutoCompleteType="Disabled" />
                  </td>
                  <td width="130px"><asp:Button ID="Button2" runat="server" Text="Edit" class="button3"/> </td>
                  <td align="right">
                    <table class="Pager">
                      <tr>
                        <td>
                          <asp:Label runat="server" ID="Label1"></asp:Label>
                          <%=HtmlLocalise("Reporting.Results2", "result(s) found")%> &nbsp;
                        </td>
                        <td id="td1" runat="server">
                          <a href="#" id="A1">
                            <img id="Img7" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                              width="15" height="11" /></a>
                        </td>
                        <td id="td2" runat="server">
                          <a href="#" id="A2">
                            <img id="Img8" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                              height="11" /></a>
                        </td>
                        <td>
                          PAGE
                        </td>
                        <td>
                          <asp:DropDownList ID="DropDownList1" runat="server">
                          </asp:DropDownList>
                        </td>
                        <td>
                          <%=HtmlLocalise("Reporting.Of2", "of") %>
                          <asp:Label runat="server" ID="Label2" />
                        </td>
                        <td id="td3" runat="server">
                          <a href="#" id="A3">
                            <img id="Img9" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                              width="6" height="11" /></a>
                        </td>
                        <td id="td4" runat="server">
                          <a href="#" id="A4">
                            <img id="Img10" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                              width="15" height="11" /></a>
                        </td>
                        <td style="padding-left: 15px">
                          <asp:DropDownList ID="DropDownList2" runat="server">
                            <asp:ListItem Text="10" Value="10" />
                            <asp:ListItem Text="20" Value="20" />
                            <asp:ListItem Text="50" Value="50" />
                            <asp:ListItem Text="100" Value="100" />
                          </asp:DropDownList>
                        </td>
                        <td>
                          RESULTS PER PAGE
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="4">
                    <table width="100%" class="DataTable">
                      <thead>
                        <tr>
                          <th width="25%">
                            <%= HtmlLocalise("Label", "EMPLOYER NAME")%>
                            <img id="Img11" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img12" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("Label", "JOB TITLE")%>
                            <img id="Img13" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img14" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("Label", "POSTING DATE")%>
                            <img id="Img17" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img18" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%"><%= HtmlLocalise("RecentPlacementsTH5", "ACTIONS")%></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("Label", "Companyname")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "System engineer")%></a></td>
                            <td valign="top"><%= HtmlLocalise("Label", "Feb 10, 2012")%></td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "View matches")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("Label", "Companyname")%></td>
                          <td valign="top">
                            <a href="#"><%= HtmlLocalise("Label", "Another much longer job title here")%></a><br />
                            <a href="#" class="othersLink othersId3">+ 0 others with good matches</a>
                          </td>
                          <td valign="top"><%= HtmlLocalise("Label", "Feb 10, 2012")%></td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "View matches")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                        <tr class="othersContent othersId3">
                          <td valign="top"></td>
                          <td valign="top"></td>
                          <td valign="top">
                            <%= HtmlLocalise("Name22", "Another name")%>
                          </td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "Compare resumes")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                        <tr class="othersContent othersId3">
                          <td valign="top"></td>
                          <td valign="top"></td>
                          <td valign="top">
                            <%= HtmlLocalise("Name23", "Another name")%>
                          </td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "Compare resumes")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle">
            <td><span id="namedAnchor3" class="accordionArrow"></span><%= HtmlLocalise("Label", "Employers with many spidered jobs")%></td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table width="100%">
                <tr>
                  <td width="40px"><strong>Filter</strong></td>
                  <td width="162px">
                    <%= HtmlInFieldLabel("EmployerNameTextBox2", "Language.InlineLabel", "employer name", 156)%>
                    <asp:TextBox ID="TextBox1" runat="server" Width="156" ClientIDMode="Static" AutoCompleteType="Disabled" />
                  </td>
                  <td width="130px"><asp:Button ID="Button3" runat="server" Text="Edit" class="button3"/> </td>
                  <td align="right">
                    <table class="Pager">
                      <tr>
                        <td>
                          <asp:Label runat="server" ID="Label3"></asp:Label>
                          <%=HtmlLocalise("Reporting.Results2", "result(s) found")%> &nbsp;
                        </td>
                        <td id="td5" runat="server">
                          <a href="#" id="A5">
                            <img id="Img15" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                              width="15" height="11" /></a>
                        </td>
                        <td id="td6" runat="server">
                          <a href="#" id="A6">
                            <img id="Img16" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                              height="11" /></a>
                        </td>
                        <td>
                          PAGE
                        </td>
                        <td>
                          <asp:DropDownList ID="DropDownList3" runat="server">
                          </asp:DropDownList>
                        </td>
                        <td>
                          <%=HtmlLocalise("Reporting.Of2", "of") %>
                          <asp:Label runat="server" ID="Label4" />
                        </td>
                        <td id="td7" runat="server">
                          <a href="#" id="A7">
                            <img id="Img19" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                              width="6" height="11" /></a>
                        </td>
                        <td id="td8" runat="server">
                          <a href="#" id="A8">
                            <img id="Img20" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                              width="15" height="11" /></a>
                        </td>
                        <td style="padding-left: 15px">
                          <asp:DropDownList ID="DropDownList4" runat="server">
                            <asp:ListItem Text="10" Value="10" />
                            <asp:ListItem Text="20" Value="20" />
                            <asp:ListItem Text="50" Value="50" />
                            <asp:ListItem Text="100" Value="100" />
                          </asp:DropDownList>
                        </td>
                        <td>
                          RESULTS PER PAGE
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="4">
                    <table width="100%" class="DataTable">
                      <thead>
                        <tr>
                          <th width="25%">
                            <%= HtmlLocalise("Label", "EMPLOYER NAME")%>
                            <img id="Img21" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img22" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("Label", "JOB TITLE")%>
                            <img id="Img23" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img24" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("Label", "POSTING DATE")%>
                            <img id="Img25" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img26" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%"><%= HtmlLocalise("RecentPlacementsTH5", "ACTIONS")%></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("Label", "Companyname")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "System engineer")%></a></td>
                            <td valign="top"><%= HtmlLocalise("Label", "Feb 10, 2012")%></td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "View matches")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("Label", "Companyname")%></td>
                          <td valign="top">
                            <a href="#"><%= HtmlLocalise("Label", "Another much longer job title here")%></a><br />
                            <a href="#" class="othersLink othersId4">+ 0 others not in the system</a>
                          </td>
                          <td valign="top"><%= HtmlLocalise("Label", "Feb 10, 2012")%></td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "View matches")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                        <tr class="othersContent othersId4">
                          <td valign="top"></td>
                          <td valign="top"></td>
                          <td valign="top">
                            <%= HtmlLocalise("Name22", "Another name")%>
                          </td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "Compare resumes")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                        <tr class="othersContent othersId4">
                          <td valign="top"></td>
                          <td valign="top"></td>
                          <td valign="top">
                            <%= HtmlLocalise("Name23", "Another name")%>
                          </td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "Compare resumes")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle on">
            <td><span id="namedAnchor4" class="accordionArrow"></span><%= HtmlLocalise("Label", "Employers with many job placements")%></td>
          </tr>
          <tr class="accordionContent open">
            <td>
              <table width="100%">
                <tr>
                  <td width="40px"></td>
                  <td width="162px">
                  </td>
                  <td width="130px"></td>
                  <td align="right">
                    <table class="Pager">
                      <tr>
                        <td>
                          <asp:Label runat="server" ID="Label5"></asp:Label>
                          <%=HtmlLocalise("Reporting.Results2", "result(s) found")%> &nbsp;
                        </td>
                        <td id="td9" runat="server">
                          <a href="#" id="A9">
                            <img id="Img27" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                              width="15" height="11" /></a>
                        </td>
                        <td id="td10" runat="server">
                          <a href="#" id="A10">
                            <img id="Img28" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                              height="11" /></a>
                        </td>
                        <td>
                          PAGE
                        </td>
                        <td>
                          <asp:DropDownList ID="DropDownList5" runat="server">
                          </asp:DropDownList>
                        </td>
                        <td>
                          <%=HtmlLocalise("Label", "of")%>
                          <asp:Label runat="server" ID="Label6" />
                        </td>
                        <td id="td11" runat="server">
                          <a href="#" id="A11">
                            <img id="Img29" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                              width="6" height="11" /></a>
                        </td>
                        <td id="td12" runat="server">
                          <a href="#" id="A12">
                            <img id="Img30" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                              width="15" height="11" /></a>
                        </td>
                        <td style="padding-left: 15px">
                          <asp:DropDownList ID="DropDownList6" runat="server">
                            <asp:ListItem Text="10" Value="10" />
                            <asp:ListItem Text="20" Value="20" />
                            <asp:ListItem Text="50" Value="50" />
                            <asp:ListItem Text="100" Value="100" />
                          </asp:DropDownList>
                        </td>
                        <td>
                          RESULTS PER PAGE
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="4">
                    <table width="100%" class="DataTable">
                      <thead>
                        <tr>
                          <th width="25%">
                            <%= HtmlLocalise("Label", "EMPLOYER NAME")%>
                            <img id="Img31" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img32" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("Label", "JOB TITLE")%>
                            <img id="Img33" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img34" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("Label", "POSTING DATE")%>
                            <img id="Img35" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img36" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%"><%= HtmlLocalise("RecentPlacementsTH5", "ACTIONS")%></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("Label-2", "Companyname")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Label", "System engineer")%></a></td>
                            <td valign="top"><%= HtmlLocalise("Label", "Feb 10, 2012")%></td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "View matches")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("Label", "Companyname")%></td>
                          <td valign="top">
                            <a href="#"><%= HtmlLocalise("Label", "Another much longer job title here")%></a><br />
                            <a href="#" class="othersLink othersId5">+ 0 more</a>
                          </td>
                          <td valign="top"><%= HtmlLocalise("Label", "Feb 10, 2012")%></td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "View matches")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                        <tr class="othersContent othersId5">
                          <td valign="top"></td>
                          <td valign="top"></td>
                          <td valign="top">
                            <%= HtmlLocalise("Name22", "Another name")%>
                          </td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "Compare resumes")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                        <tr class="othersContent othersId5">
                          <td valign="top"></td>
                          <td valign="top"></td>
                          <td valign="top">
                            <%= HtmlLocalise("Name23", "Another name")%>
                          </td>
                          <td valign="top">
                            <div><a href="#"><%= HtmlLocalise("Label", "Compare resumes")%></a></div>
                            <div><a href="#"><%= HtmlLocalise("Label", "Contact employer")%></a></div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
</table>
</asp:Content>
