﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page107.aspx.cs" Inherits="Focus.Web.Designs.FA28Page107" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<table width="100%">
  <tr>
    <td colspan="2" valign="top">
      <p class="pageNav">
        <a href="#"><%= HtmlLocalise("JobSeekerReports", "Job seeker reports")%></a> | 
        <a href="#"><%= HtmlLocalise("JobOrderReports", "Job order reports")%></a> | 
        <a href="#"><%= HtmlLocalise("EmployerReports", "Employer reports")%></a> | 
        <a href="#"><%= HtmlLocalise("EEOReports", "EEO reports")%></a> | 
        <a href="#"><%= HtmlLocalise("ActivityReports", "Activity reports")%></a> | 
        <strong><%= HtmlLocalise("Lookup", "Lookup")%></strong> | 
        <a href="#"><%= HtmlLocalise("SavedReports", "Saved reports")%></a> | 
        <a href="#"><%= HtmlLocalise("LaborInsight", "Labor/Insight")%></a>
      </p>
    </td>
  </tr>
</table>

<table width="100%" class="accordionTable">
  <tr class="multipleAccordionTitle">
    <td colspan="4"><span class="accordionArrow"></span><%= HtmlLocalise("AccordionHeading1", "FIND A JOB SEEKER")%></td>
  </tr>
  <tr class="accordionContent">
    <td colspan="4">
      <table width="100%">
        <tr>
          <td width="120px"><%= HtmlLocalise("FirstName", "First name")%></td>
          <td width="400px"><asp:TextBox runat="server" ID="TextBox5" Text="" Width="140" /></td>
          <td></td>
          <td align="right"><asp:Button ID="Button5" runat="server" Text="Access job seeker account" class="button2" style="margin-top:5px;" /></td>
        </tr>
          <tr>
            <td><%= HtmlLocalise("LastName", "Last name")%></td>
            <td><asp:TextBox runat="server" ID="TextBox1" Text="" Width="140" /></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2"><div class="HorizontalRule"></div></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td width="90px"><%= HtmlLocalise("SSN", "SSN")%></td>
            <td><asp:TextBox runat="server" ID="TextBox2" Text="" Width="140" /></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2"><div class="HorizontalRule"></div></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td width="90px"><%= HtmlLocalise("EmailAddress", "Email address")%></td>
            <td><asp:TextBox runat="server" ID="TextBox3" Text="" Width="140" /></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2"><div class="HorizontalRule"></div></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td width="90px"><%= HtmlLocalise("PhoneNumber", "Phone number")%></td>
            <td><asp:TextBox runat="server" ID="TextBox4" Text="" Width="140" /></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2"><div class="HorizontalRule"></div></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2" align="right">
              <asp:Button ID="Button1" runat="server" Text="Find" class="button3" style="margin-top:5px;" />
            </td>
            <td></td>
            <td></td>
          </tr>
      </table>
    </td>
  </tr>
  <tr class="multipleAccordionTitle">
    <td colspan="4"><span class="accordionArrow"></span><%= HtmlLocalise("AccordionHeading2", "FIND A LIST OF JOB SEEKERS")%></td>
  </tr>
  <tr class="accordionContent">
    <td colspan="4">
      <table width="100%">
        <tr>
          <td colspan="3">
            <asp:DropDownList runat="server" ID="DropDownList1" Width="200">
                <asp:ListItem>List name</asp:ListItem>
            </asp:DropDownList>
          </td>
        <td align="right"><asp:Button ID="Button3" runat="server" Text="View & manage list members" class="button2" style="margin-top:5px;" /></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr class="multipleAccordionTitle">
    <td colspan="4"><span class="accordionArrow"></span><%= HtmlLocalise("AccordionHeading3", "FIND AN EMPLOYER")%></td>
  </tr>
  <tr class="accordionContent">
    <td colspan="4">
      <table width="100%">
        <tr>
          <td width="120px"><%= HtmlLocalise("ContactFirstName", "Contact First name")%></td>
          <td width="400px"><asp:TextBox runat="server" ID="TextBox7" Text="" Width="140" /></td>
          <td></td>
          <td align="right"><asp:Button ID="Button2" runat="server" Text="Access employer account" class="button2" style="margin-top:5px;" /></td>
        </tr>
          <tr>
            <td width="120px"><%= HtmlLocalise("ContactLastName", "Contact Last name")%></td>
            <td width="400px"><asp:TextBox runat="server" ID="TextBox8" Text="" Width="140" /></td>
            <td></td>
            <td align="right"></td>
          </tr>
          <tr>
            <td colspan="2"><div class="HorizontalRule"></div></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td width="90px"><%= HtmlLocalise("EmployerName", "Employer name")%></td>
            <td><asp:TextBox runat="server" ID="TextBox9" Text="" Width="140" /></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2"><div class="HorizontalRule"></div></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td width="120px"><%= HtmlLocalise("FEIN", "FEIN")%></td>
            <td><asp:TextBox runat="server" ID="TextBox10" Text="" Width="140" /></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2"><div class="HorizontalRule"></div></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td width="120px"><%= HtmlLocalise("ContactEmail", "Contact Email")%></td>
            <td><asp:TextBox runat="server" ID="TextBox11" Text="" Width="140" /></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2"><div class="HorizontalRule"></div></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2" align="right">
              <asp:Button ID="Button4" runat="server" Text="Find" class="button3" style="margin-top:5px;" />
            </td>
            <td></td>
            <td></td>
          </tr>
      </table>
    </td>
  </tr>
  <tr class="multipleAccordionTitle">
    <td colspan="4"><span class="accordionArrow"></span><%= HtmlLocalise("AccordionHeading4", "FIND A JOB ORDER")%></td>
  </tr>
  <tr class="accordionContent">
    <td colspan="4">
      <table width="100%">
        <tr>
          <td><%= HtmlLocalise("ContactFirstName", "Job title")%></td>
          <td colspan="3"><asp:TextBox runat="server" ID="TextBox12" Text="" Width="140" /></td>
          <td></td>
          <td></td>
          <td rowspan="9" align="right" valign="top">
            <asp:Button ID="Button6" runat="server" Text="Send message about selected job" class="button2" style="margin-top:5px;" />
          <br /><br />
          <div style="width:450px;text-align:left;">
            <strong>Job orders </strong>5 found
          </div>
          <br /><br />
          <div style="overflow:scroll;height:300px;width:450px;">
            <table width="100%" class="DataTable">
                <thead>
                  <tr>
                    <th width="25%">
                      <%= HtmlLocalise("RecentPlacementsTH21", "JOB TITLE")%>
                      <img id="Img21" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                      <img id="Img22" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                    </th>
                    <th width="15%">
                      <%= HtmlLocalise("RecentPlacementsTH22", "EMPLOYER")%>
                      <img id="Img23" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                      <img id="Img24" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td valign="top"><asp:RadioButton ID="RadioButton1" Text="Job title goes here" runat="server" GroupName="Employer1" /></td>
                    <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Employer name")%></a></td>
                  </tr>
                  <tr>
                    <td valign="top"><asp:RadioButton ID="RadioButton2" Text="Job title goes here" runat="server" GroupName="Employer1" /></td>
                    <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Employer name")%></a></td>
                  </tr>
                  <tr>
                    <td valign="top"><asp:RadioButton ID="RadioButton3" Text="Job title goes here" runat="server" GroupName="Employer1" /></td>
                    <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Employer name")%></a></td>
                  </tr>
                  <tr>
                    <td valign="top"><asp:RadioButton ID="RadioButton4" Text="Job title goes here" runat="server" GroupName="Employer1" /></td>
                    <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Employer name")%></a></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </td>
        </tr>
        <tr>
          <td><%= HtmlLocalise("ContactFirstName", "Employer name")%></td>
          <td colspan="3"><asp:TextBox runat="server" ID="TextBox14" Text="" Width="140" /></td>
          <td colspan="3"></td>
        </tr>
        <tr>
          <td><%= HtmlLocalise("EmployerName", "Creation date")%></td>
          <td colspan="3"><asp:TextBox runat="server" ID="TextBox13" Text="" Width="140" /></td>
          <td colspan="3"></td>
        </tr>
        <tr>
          <td width="120px"><%= HtmlLocalise("EmployerName", "Creation date")%></td>
          <td width="40px"><i>from</i></td>
          <td width="110px">
            <asp:DropDownList runat="server" ID="DropDownList11" Width="100">
                <asp:ListItem>month</asp:ListItem>
            </asp:DropDownList>
          </td>
          <td width="170px">
            <asp:DropDownList runat="server" ID="DropDownList2" Width="100">
                <asp:ListItem>year</asp:ListItem>
            </asp:DropDownList>
          </td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td><i>to</i></td>
          <td>
            <asp:DropDownList runat="server" ID="DropDownList5" Width="100">
                <asp:ListItem>month</asp:ListItem>
            </asp:DropDownList>
          </td>
          <td>
            <asp:DropDownList runat="server" ID="DropDownList6" Width="100">
                <asp:ListItem>year</asp:ListItem>
            </asp:DropDownList>
          </td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td colspan="4"><div class="HorizontalRule"></div></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td><%= HtmlLocalise("JobId", "Job ID")%></td>
          <td colspan="6"><asp:TextBox runat="server" ID="TextBox6" Text="" Width="140" /></td>
        </tr>
        <tr>
          <td colspan="4"><div class="HorizontalRule"></div></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td align="right" valign="top" colspan="4"><asp:Button ID="Button7" runat="server" Text="Find" class="button3" style="margin-top:5px;" /></td>
          <td></td>
          <td></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr class="multipleAccordionTitle">
    <td colspan="4"><span class="accordionArrow"></span><%= HtmlLocalise("AccordionHeading5", "FIND AN EMPLOYER ACCOUNTHOLDER")%></td>
  </tr>
  <tr class="accordionContent">
    <td colspan="4">
      <table width="100%">
        <tr>
          <td width="120px"><%= HtmlLocalise("ContactFirstName", "Contact First name")%></td>
          <td width="400px"><asp:TextBox runat="server" ID="TextBox15" Text="" Width="140" /></td>
          <td></td>
          <td align="right"><asp:Button ID="Button8" runat="server" Text="Access employer account" class="button2" style="margin-top:5px;" /></td>
        </tr>
          <tr>
            <td width="120px"><%= HtmlLocalise("ContactLastName", "Contact Last name")%></td>
            <td width="400px"><asp:TextBox runat="server" ID="TextBox16" Text="" Width="140" /></td>
            <td></td>
            <td align="right"></td>
          </tr>
          <tr>
            <td colspan="2"><div class="HorizontalRule"></div></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td width="90px"><%= HtmlLocalise("EmployerName", "Employer name")%></td>
            <td><asp:TextBox runat="server" ID="TextBox17" Text="" Width="140" /></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2"><div class="HorizontalRule"></div></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td width="120px"><%= HtmlLocalise("ContactEmail", "Contact Email")%></td>
            <td><asp:TextBox runat="server" ID="TextBox19" Text="" Width="140" /></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2"><div class="HorizontalRule"></div></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2" align="right">
              <asp:Button ID="Button9" runat="server" Text="Find" class="button3" style="margin-top:5px;" />
            </td>
            <td></td>
            <td></td>
          </tr>
      </table>
    </td>
  </tr>
    <tr class="multipleAccordionTitle">
    <td colspan="4"><span class="accordionArrow"></span><%= HtmlLocalise("AccordionHeading6", "FIND A STAFF MEMBER")%></td>
  </tr>
  <tr class="accordionContent">
    <td colspan="4">
      <table>
        <tr>
          <td width="120px"><%= HtmlLocalise("ContactFirstName", "Contact First name")%></td>
          <td width="160px"><asp:TextBox runat="server" ID="TextBox18" Text="" Width="140" /></td>
          <td align="right" rowspan="2" valign="middle"><asp:Button ID="Button10" runat="server" Text="Find" class="button3" style="margin-top:5px;" /></td>
        </tr>
          <tr>
            <td><%= HtmlLocalise("ContactLastName", "Contact Last name")%></td>
            <td><asp:TextBox runat="server" ID="TextBox20" Text="" Width="140" /></td>
            <td></td>
          </tr>
      </table>
    </td>
  </tr>

</table>

</asp:Content>
