﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page131.aspx.cs" Inherits="Focus.Web.Designs.FA28Page131" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<table width="100%">
    <tr>
      <td>
        <p class="pageNav">
          <a href="#"><%= HtmlLocalise("JobOrderDashboard", "Job order dashboard")%></a> | 
          <strong><%= HtmlLocalise("JobDevelopment", "Job development")%></strong> | 
          <a href="#"><%= HtmlLocalise("FindEmployer", "Find employer")%></a> | 
          <a href="#"><%= HtmlLocalise("SendMessages", "Send messages")%></a>
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%">
          <tr>
            <td><h1><%= HtmlLocalise("JobOrderDashboard", "Account settings")%></h1></td>
            <td></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td><br /></td>
    </tr>
    <tr>
      <td>
        <table class="accordionTable">
          <tr class="multipleAccordionTitle">
            <td><span id="namedAnchor1" class="accordionArrow"></span>Sharing invitations</td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table width="100%">
                <tr>
                  <td width="150"></td>
                  <td></td>
                  <td align="right"><asp:Button ID="Button1" runat="server" Text="Send invitation" class="button3"/></td>
                </tr>
                <tr>
                  <td valign="top"><%= HtmlLocalise("JobOrderDashboard", "From this location")%></td>
                  <td>
                    <asp:DropDownList ID="DropDownList1" runat="server">
                      <asp:ListItem Text="- select location -" />
                      <asp:ListItem Text="text2" />
                    </asp:DropDownList>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td valign="top"><%= HtmlLocalise("JobOrderDashboard", "Invite this staff member")%></td>
                  <td>
                    <asp:DropDownList ID="DropDownList2" runat="server">
                      <asp:ListItem Text="- select staff member -" />
                      <asp:ListItem Text="text2" />
                    </asp:DropDownList>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td valign="top"><%= HtmlLocalise("JobOrderDashboard", "To share access to your")%></td>
                  <td>
                    <asp:CheckBoxList runat="server">
                      <asp:ListItem Text="queues" />
                      <asp:ListItem Text="job postings" />
                      <asp:ListItem Text="job seeker lists" />
                      <asp:ListItem Text="message templates" />
                    </asp:CheckBoxList>
                  </td>
                  <td></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle">
            <td><span id="namedAnchor2" class="accordionArrow"></span>Sharing status</td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table width="100%">
                <tr>
                  <td colspan="4">
                    <table width="100%" class="DataTable">
                      <thead>
                        <tr>
                          <th width="25%">
                            <%= HtmlLocalise("RecentPlacementsTH21", "Name")%>
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH22", "What you're sharing with others")%>
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH24", "What's shared with you")%>
                          </th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "Jane Staffer")%></td>
                          <td valign="top"><%= HtmlLocalise("Name31", "Queues, job postings, job seeker lists, employer lists, message templates")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "Queues, job postings")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Action24", "remove sharing privileges  ")%></a></td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "Jane Staffer")%></td>
                          <td valign="top"><%= HtmlLocalise("Name31", "Queues, job postings, job seeker lists, employer lists, message templates")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "Queues, job postings")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Action24", "remove sharing privileges  ")%></a></td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "Jane Staffer")%></td>
                          <td valign="top"><%= HtmlLocalise("Name31", "Queues, job postings, job seeker lists, employer lists, message templates")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "Queues, job postings")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Action24", "remove sharing privileges  ")%></a></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
</table>

</asp:Content>
