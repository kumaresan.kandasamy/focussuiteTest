﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page079.aspx.cs" Inherits="Focus.Web.Designs.FA28Page079" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<asp:Panel ID="ModalPanel" runat="server" CssClass="modal" Style="display:block;width:500px;">

<table width="100%">
    <tr>
      <td><h1><%= HtmlLocalise("Notification", "Notification of Edited Job Posting")%></h1></td>
    </tr>
    <tr>
      <td><%= HtmlLocalise("JobOrderDashboard", "Notify the employer of the edits to their posting.")%></td>
    </tr>
    <tr>
      <td>
        <asp:DropDownList runat="server" ID="DropDownList6" Width="300">
          <asp:ListItem>Default message</asp:ListItem>
        </asp:DropDownList>
      </td>
    </tr>
    <tr>
      <td>

      <div style="overflow:scroll;height:400px;width:100%">
      <table>
        <tr>
            <td>742 Evergreen Terrace</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Springﬁeld, KY 12345</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><br /></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Phone number: 555-555-1212</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Fax number: 555-555-1234</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>URL: http://www.niftynetworks.com</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><br /></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>FEIN #: 12-3456789</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>SEIN #: 00-00000</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><br /></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><strong>Industry classiﬁcation</strong>: Application software programming services, custom computer (541511)</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><br /></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><strong>Contact:</strong></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Harold Saxon</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Nifty Networks</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>742 Evergreen Terrace</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Springﬁeld, KY 12345</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><br /></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Email: h.saxon@niftynetworks.com</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Phone number: 555-555-0000</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Fax number: 555-555-1234</td>
            <td></td>
            <td></td>
        </tr>
      </table>
      </div>

      </td>
    </tr>
    <tr>
      <td><asp:CheckBox id="CheckBox1" Text="email me a copy of this message" TextAlign="Right" runat="server" /></td>
    </tr>
    <tr>
      <td>
        <asp:Button ID="Button1" runat="server" Text="Cancel" class="button4" style="margin-left:0" />
        <asp:Button ID="Button2" runat="server" Text="Email employer" class="button3" />
      </td>
    </tr>
</table>

</asp:Panel>

</asp:Content>
