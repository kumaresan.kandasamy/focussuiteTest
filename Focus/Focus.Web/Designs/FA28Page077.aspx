﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page077.aspx.cs" Inherits="Focus.Web.Designs.FA28Page077" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<asp:Panel ID="ModalPanel" runat="server" CssClass="modal" Style="display:block;width:500px;">

<table width="100%">
    <tr>
      <td><h1><%= HtmlLocalise("Notification", "Deny posting")%></h1></td>
    </tr>
    <tr>
      <td><%= HtmlLocalise("JobOrderDashboard", "This will:")%></td>
    </tr>
    <tr>
      <td>
        <ul>
          <li><%= HtmlLocalise("JobDevelopment", "remove the posting from the approval queue and put it in the employer's 'Closed' tab in Focus/Talent, and")%></li>
          <li><%= HtmlLocalise("JobDevelopment", "email the employer alerting them that their posting has been denied.")%></li>
        </ul>
       </td>
    </tr>
    <tr>
      <td>
        <asp:DropDownList runat="server" ID="DropDownList6" Width="300">
          <asp:ListItem>Default message</asp:ListItem>
        </asp:DropDownList>
      </td>
    </tr>
    <tr>
      <td>
        <div>Dear ContactfirstName ContactlastName,<br /></div>
        <div>The System Engineer posting you recently created has been removed due to the following content:</div>
      </td>
    </tr>
    <tr>
      <td>
        <%= HtmlInFieldLabel("EmployerNameTextBox", "Language.InlineLabel", "Type reasons for denial", 496)%>
        <asp:TextBox ID="EmployerNameTextBox" runat="server" Width="100%" ClientIDMode="Static" AutoCompleteType="Disabled" TextMode="MultiLine" Rows="4" />
      </td>
    </tr>
    <tr>
      <td>
      <div>If you would like to edit this posting to address these issues, you can access it from the Closed Postings tab on your home page.</div><br />
      <div>If you have any questions, please feel free to contact me at 617-555-1212 or email@example.com.</div><br />
            <div>Regards,</div><br />
            <div>Userfirstname Userlastname</div>
      </td>
    </tr>
    <tr>
      <td><asp:CheckBox id="CheckBox1" Text="email me a copy of this message" TextAlign="Right" runat="server" /></td>
    </tr>
    <tr>
      <td>
        <asp:Button ID="Button1" runat="server" Text="Keep posting" class="button4" style="margin-left:0" />
        <asp:Button ID="Button2" runat="server" Text="Deny posting" class="button3" />
      </td>
    </tr>
</table>

</asp:Panel>


</asp:Content>
