﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page124.aspx.cs" Inherits="Focus.Web.Designs.FA28Page124" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<asp:Panel ID="ModalPanel" runat="server" CssClass="modal" Style="display:block;width:500px;">

<table width="100%">
  <tr>
    <td><h1><%= HtmlLocalise("JobOrderDashboard", "Deactivate account")%></h1></td>
  </tr>
  <tr>
    <td>
      <%= HtmlLocalise("JobOrderDashboard", "Deactivate this user's Focus/Assist account?")%>
    </td>
  </tr>
  <tr>
    <td>
      <asp:Button ID="Button1" runat="server" Text="Cancel" class="button4" />
      <asp:Button ID="Button2" runat="server" Text="Inactivate account" class="button3" />
    </td>
  </tr>
</table>

</asp:Panel>

</asp:Content>
