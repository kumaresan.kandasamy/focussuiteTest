﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page128.aspx.cs" Inherits="Focus.Web.Designs.FA28Page128" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<table width="100%">
    <tr>
        <td width="140"><h1>Account settings</h1></td>
        <td width="260"></td>
        <td width="170"></td>
        <td align="right"></td>
    </tr>
    <tr>
        <td colspan="3"><strong>Update my contact Information</strong>  * <i>required ﬁelds</i></td>
        <td align="right"><asp:Button ID="Button8" runat="server" Text="Save" class="button3" /></td>
    </tr>
    <tr>
        <td>First name*</td>
        <td><asp:TextBox ID="TextBox2" runat="server" Width="200" /></td>
        <td>Phone number*</td>
        <td><asp:TextBox ID="TextBox1" runat="server" Width="200" /></td>
    </tr>
    <tr>
        <td>Last name*</td>
        <td><asp:TextBox ID="TextBox3" runat="server" Width="200" /></td>
        <td>Alternate phone number</td>
        <td><asp:TextBox ID="TextBox4" runat="server" Width="200" /></td>
    </tr>
    <tr>
        <td>Middle initial</td>
        <td><asp:TextBox ID="TextBox5" runat="server" Width="30" /></td>
        <td>Fax number</td>
        <td><asp:TextBox ID="TextBox6" runat="server" Width="200" /></td>
    </tr>
    <tr>
        <td>Personal title*</td>
        <td>
            <asp:DropDownList runat="server" ID="DropDownList11">
                <asp:ListItem>Ms.</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>Email address*</td>
        <td><asp:TextBox ID="TextBox8" runat="server" Width="200" /></td>
    </tr>
    <tr>
        <td>Job title</td>
        <td><asp:TextBox ID="TextBox9" runat="server" Width="200" /></td>
        <td></td>
        <td></td>
    </tr>
</table>

</asp:Content>
