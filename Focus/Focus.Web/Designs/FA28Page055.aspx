﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page055.aspx.cs" Inherits="Focus.Web.Designs.FA28Page055" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<p class="pageNav">
		<a href="<%= UrlBuilder.JobSeekerReferrals() %>"><%= HtmlLocalise("ReturnToList.LinkText", "Return to main page")%></a>
	</p>

	<table width="100%">
		<tr>
			<td width="50%"><h1><%= HtmlLocalise("JobSeekerReferral.Header", "Compare resumes")%></h1></td>
			<td width="50%">
				<table class="right">
					<tr>
						<td><asp:button ID="DenyButton" runat="server" CssClass="button4" Text="Contact Employer" /></td>
						<td><asp:button ID="ApproveButton" runat="server" CssClass="button2 right" Text="Cancel" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
        <table>
          <tr>
            <td><h2><%= HtmlLocalise("Resume.Header", "Suggested match")%></h2></td>
            <td>&nbsp;&nbsp;<img src="../Assets/Images/starsfour.gif" alt="stars" width="80" height="16" style="margin-top:-5px" /></td>
          </tr>
        </table>
      </td>
			<td>
        <h2><%= HtmlLocalise("JobPosting.Header", "Recent placement: Software Engineer, Nifty Networks")%></h2>
      </td>
		</tr>
		<tr>
			<td>
				<div style="overflow:scroll;height:400px;width:95%">
					<style type="text/css" xmlns:xs="http://www.w3.org/2001/XMLSchema">
			.font13 { font-family:arial;font-size:13px; }
			.font14 { font-family:arial;font-size:14px; }
			.font16 { font-family:arial;font-size:16px; font-weight:bold; }
		</style>

  
    <font class="font14">
      <table border="0" width="100%">
        <tbody><tr>
          <td>
            <font class="font16">George   J   Bush</font><br><font class="font13">(900) 900-9000 (Home)<br>78 Jefferson St., Apt. 2D<br>Hoboken, NJ  07030<br>Gn10024Sn10024@demo.com</font>
            <hr style="width:100%">
            <table>
              <tbody><tr>
                <td></td>
              </tr>
            </tbody></table>
            <font class="font14"><b>Experience</b></font>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tbody></tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tbody></tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tbody>
                <tr>
                  <td valign="top" align="left">
                    <font class="font13"></font>
                  </td>
                  <td align="right">
                    <font class="font13">Aug. 06 to Oct. 08</font>
                  </td>
                </tr>
                <tr>
                  <td valign="top"><b><font class="font13">Deputy Editor</font><br></b></td>
                </tr>
                <tr>
                  <td valign="top" colspan="2" text-align="justify">
                    <font class="font13">
                      <div style="text-align: justify;">Digital Media, Reed Television Group
* Write, edit, post and determine news order for breaking news stories for BroadcastingCable.com (www.broadcastingcable.com) the Web site for Broadcasting &amp; Cable, a weekly newsmagazine covering the television industry, from 8/07-10/08; and for Multichannel.com (www.multichannel.com), the Web site for Multichannel News, a weekly newsmagazine covering the cable- and satellite-television industries, as well as new-media platforms, from 8/06-8/07.
* Apply principles of search-engine optimization to all breaking news stories on BroadcastingCable.com, including insertion of keywords, maintaining taxonomy system (see below) and researching most-searched versions of various industry-related terms.
* Add elements to breaking news items to help boost traffic, including videos, pictures and links to related stories on BroadcastingCable.com.
* Maintain taxonomy system that determines placement of all stories on the site and handle tagging of all breaking news and print-edition stories.
* Edit and deploy daily and biweekly e-mail newsletters and breaking news e-mail blasts.
* Update and maintain non-news sections of BroadcastingCable.com, including video and photo galleries and event calendar.
* Oversee conversion of content from print edition of B&amp;C to Web site.</div>
                    </font>
                  </td>
                </tr>
                <tr>
                  <td><br></td>
                </tr>
                <tr>
                  <td valign="top" align="left">
                    <font class="font13"></font>
                  </td>
                  <td align="right">
                    <font class="font13">Mar. 95 to Oct. 08</font>
                  </td>
                </tr>
                <tr>
                  <td valign="top"><b><font class="font13"></font><br></b></td>
                </tr>
                <tr>
                  <td valign="top" colspan="2" text-align="justify">
                    <font class="font13">
                      <div style="text-align: justify;"></div>
                    </font>
                  </td>
                </tr>
                <tr>
                  <td><br></td>
                </tr>
                <tr>
                  <td valign="top" align="left">
                    <font class="font13">Reed Television Group</font>
                  </td>
                  <td align="right">
                    <font class="font13">Sept. 02 to Aug. 06</font>
                  </td>
                </tr>
                <tr>
                  <td valign="top"><b><font class="font13">Senior Web Editor</font><br></b></td>
                </tr>
                <tr>
                  <td valign="top" colspan="2" text-align="justify">
                    <font class="font13">
                      <div style="text-align: justify;"></div>
                    </font>
                  </td>
                </tr>
                <tr>
                  <td><br></td>
                </tr>
                <tr>
                  <td valign="top" align="left">
                    <font class="font13">Multichannel.com and BroadcastingCable.com</font>
                  </td>
                  <td align="right">
                    <font class="font13">Aug. 00 to Sept. 02</font>
                  </td>
                </tr>
                <tr>
                  <td valign="top"><b><font class="font13">Web Editor</font><br></b></td>
                </tr>
                <tr>
                  <td valign="top" colspan="2" text-align="justify">
                    <font class="font13">
                      <div style="text-align: justify;"></div>
                    </font>
                  </td>
                </tr>
                <tr>
                  <td><br></td>
                </tr>
                <tr>
                  <td valign="top" align="left">
                    <font class="font13">Multichannel News</font>
                  </td>
                  <td align="right">
                    <font class="font13">Apr. 99 to Aug. 00</font>
                  </td>
                </tr>
                <tr>
                  <td valign="top"><b><font class="font13">Assistant News Editor</font><br></b></td>
                </tr>
                <tr>
                  <td valign="top" colspan="2" text-align="justify">
                    <font class="font13">
                      <div style="text-align: justify;">Edited all stories for print edition of Multichannel News and daily Web-site update.</div>
                    </font>
                  </td>
                </tr>
                <tr>
                  <td><br></td>
                </tr>
                <tr>
                  <td valign="top" align="left">
                    <font class="font13"></font>
                  </td>
                  <td align="right">
                    <font class="font13">Jan. 97 to Apr. 99</font>
                  </td>
                </tr>
                <tr>
                  <td valign="top"><b><font class="font13">Copy Chief</font><br></b></td>
                </tr>
                <tr>
                  <td valign="top" colspan="2" text-align="justify">
                    <font class="font13">
                      <div style="text-align: justify;"></div>
                    </font>
                  </td>
                </tr>
                <tr>
                  <td><br></td>
                </tr>
                <tr>
                  <td valign="top" align="left">
                    <font class="font13">Multichannel News and Multichannel News International</font>
                  </td>
                  <td align="right">
                    <font class="font13">Mar. 95 to Jan. 97</font>
                  </td>
                </tr>
                <tr>
                  <td valign="top"><b><font class="font13">Assistant Managing Editor</font><br></b></td>
                </tr>
                <tr>
                  <td valign="top" colspan="2" text-align="justify">
                    <font class="font13">
                      <div style="text-align: justify;"></div>
                    </font>
                  </td>
                </tr>
                <tr>
                  <td><br></td>
                </tr>
                <tr>
                  <td valign="top" align="left">
                    <font class="font13"></font>
                  </td>
                  <td align="right">
                    <font class="font13">Mar. 94 to Mar. 95</font>
                  </td>
                </tr>
                <tr>
                  <td valign="top"><b><font class="font13">Production Manager</font><br></b></td>
                </tr>
                <tr>
                  <td valign="top" colspan="2" text-align="justify">
                    <font class="font13">
                      <div style="text-align: justify;">Edited, laid out and proofread stories for group of daily, weekly, biweekly and monthly newsletters covering the petroleum industry. Prepared copy for electronic version of PIW.</div>
                    </font>
                  </td>
                </tr>
                <tr>
                  <td><br></td>
                </tr>
                <tr>
                  <td valign="top" align="left">
                    <font class="font13"></font>
                  </td>
                  <td align="right">
                    <font class="font13">June 91 to Mar. 95</font>
                  </td>
                </tr>
                <tr>
                  <td valign="top"><b><font class="font13"></font><br></b></td>
                </tr>
                <tr>
                  <td valign="top" colspan="2" text-align="justify">
                    <font class="font13">
                      <div style="text-align: justify;"></div>
                    </font>
                  </td>
                </tr>
                <tr>
                  <td><br></td>
                </tr>
                <tr>
                  <td valign="top" align="left">
                    <font class="font13"></font>
                  </td>
                  <td align="right">
                    <font class="font13">June 91 to Mar. 94</font>
                  </td>
                </tr>
                <tr>
                  <td valign="top"><b><font class="font13">Production Assistant</font><br></b></td>
                </tr>
                <tr>
                  <td valign="top" colspan="2" text-align="justify">
                    <font class="font13">
                      <div style="text-align: justify;"></div>
                    </font>
                  </td>
                </tr>
                <tr>
                  <td><br></td>
                </tr>
                <tr>
                  <td valign="top" align="left">
                    <font class="font13">Scholastic Inc</font>
                  </td>
                  <td align="right">
                    <font class="font13">June 90 to June 91</font>
                  </td>
                </tr>
                <tr>
                  <td valign="top"><b><font class="font13"></font><br></b></td>
                </tr>
                <tr>
                  <td valign="top" colspan="2" text-align="justify">
                    <font class="font13">
                      <div style="text-align: justify;"></div>
                    </font>
                  </td>
                </tr>
                <tr>
                  <td><br></td>
                </tr>
                <tr>
                  <td valign="top" align="left">
                    <font class="font13"></font>
                  </td>
                  <td align="right">
                    <font class="font13">June 90 to June 91</font>
                  </td>
                </tr>
                <tr>
                  <td valign="top"><b><font class="font13">Researcher</font><br></b></td>
                </tr>
                <tr>
                  <td valign="top" colspan="2" text-align="justify">
                    <font class="font13">
                      <div style="text-align: justify;">Fact-checked stories for monthly magazine covering computer hardware and software for the home and small businesses. Some copy editing and proofreading.</div>
                    </font>
                  </td>
                </tr>
                <tr>
                  <td><br></td>
                </tr>
                <tr>
                  <td valign="top" align="left">
                    <font class="font13"></font>
                  </td>
                  <td align="right">
                    <font class="font13">Sept. 88 to May 90</font>
                  </td>
                </tr>
                <tr>
                  <td valign="top"><b><font class="font13">Housewares Executive</font><br></b></td>
                </tr>
                <tr>
                  <td valign="top" colspan="2" text-align="justify">
                    <font class="font13">
                      <div style="text-align: justify;"></div>
                    </font>
                  </td>
                </tr>
                <tr>
                  <td><br></td>
                </tr>
                <tr>
                  <td valign="top" align="left">
                    <font class="font13"></font>
                  </td>
                  <td align="right">
                    <font class="font13">Sept. 88 to May 90</font>
                  </td>
                </tr>
                <tr>
                  <td valign="top"><b><font class="font13">Editorial Assistant, Housewares Executive</font><br></b></td>
                </tr>
                <tr>
                  <td valign="top" colspan="2" text-align="justify">
                    <font class="font13">
                      <div style="text-align: justify;">Edited, laid out pages, proofread and wrote for weekly newsletter covering the housewares industry.</div>
                    </font>
                  </td>
                </tr>
                <tr>
                  <td><br></td>
                </tr>
              </tbody>
            </table>
            <font class="font14"><b>Education</b></font>
            <table border="0" width="100%" cellpadding="0" cellspacing="0">
              <tbody>
                <tr>
                  <td width="" valign="top" cellpadding="0" cellspacing="0">
                    <font class="font13">1990    New York University, KY</font>
                  </td>
                </tr>
                <tr>
                  <td width="" valign="top">
                    <font class="font13"><b>B.A</b>:	Journalism</font>
                  </td>
                </tr>
                <tr>
                  <td width="" valign="top">
                    <font class="font13"></font>
                  </td>
                </tr>
                <tr>
                  <td><br></td>
                </tr>
              </tbody>
            </table>
            <font class="font13">
              <div style="text-align: justify;">
                <font class="font14"><b>Licenses</b></font><br><font class="font13">Kentucky</font>
                <font class="font13">Class D/Regular Operator</font>
              </div>
            </font><br><font class="font14"><b>Skills</b></font>
            <font class="font13">
              <div style="text-align: justify;">Microsoft Office Suite and Proprietary Web-Publishing Platforms, And Some Experience with Quark Xpress, Adobe Indesign, Adobe Incopy and Photoshop.,adobe, adobe photoshop, broadcast, computer hardware/software knowledge, copy editing, digital media, editing, email technical support, exchanging merchandise, indesign, microsoft office, new media development, newsletters, proofreading, quarkxpress, research, search engine optimization(seo), website production,merchandise exchange<br></div>
            </font><br></td>
        </tr>
      </tbody></table>
    </font>
  

				</div>
			</td>
			<td>
				<div style="overflow:scroll;height:400px;width:95%">
					
  
    <p><b>Software Engineer</b><br>Testerville, KY (40601)<br>Number of openings: 1<br>Application closing date: 31/05/2012<br><br>Full-time position<br>35.00 hours per week<br>Permanent position<br>Salaried "Exempt" position</p>
    <p>* Performed system management.
* Performed integration functions.
</p>
    <p><b>Requirements</b><br>Applicants preferred to have at least a High school diploma/GED or equivalent</p>
    <p><b>How to apply</b><br><a href="http://careerdev.burning-glass.com/career/" target="_blank">Log in to Focus/Career and submit your resume</a></p>
    <p><small>REF/29441/1710668</small></p>
  

				</div>
			</td>
		</tr>
    <tr>
      <td colspan="2"><br /></td>
    </tr>
    <tr>
      <td><asp:CheckBox ID="CheckBox1" Text="do not show this suggestion again" runat="server" /></td>
      <td></td>
    </tr>
	</table>

</asp:Content>
