﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page073.aspx.cs" Inherits="Focus.Web.Designs.FA28Page073" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<asp:Panel ID="ModalPanel" runat="server" CssClass="modal" Style="display:block;width:500px;">

<table width="100%">
    <tr>
      <td><h1><%= HtmlLocalise("Notification", "Notes and reminders")%></h1></td>
      <td><img src="/Assets/images/button_x_close_off.png" onclick="alert('close')" width="16" height="15" alt="" style="float:right;" /></td>
    </tr>
    <tr>
      <td colspan="2">
        <asp:CheckBox Text="add a note for" runat="server" />
        <asp:DropDownList runat="server">
          <asp:ListItem Text="nifty networks" />
          <asp:ListItem Text="text2" />
        </asp:DropDownList>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <asp:CheckBox ID="CheckBox1" Text="add a reminder for" runat="server" />
        <asp:DropDownList ID="DropDownList1" runat="server">
          <asp:ListItem Text="myself" />
          <asp:ListItem Text="text2" />
        </asp:DropDownList>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <table>
          <tr>
            <td><asp:RadioButton ID="RadioButton1" Text="email" runat="server" /></td>
            <td><asp:RadioButton ID="RadioButton2" Text="post to dashboard on" runat="server" /></td>
            <td>
              <%= HtmlInFieldLabel("MM", "Language.InlineLabel", "MM", 26)%>
              <asp:TextBox ID="MM" runat="server" Width="26" ClientIDMode="Static" AutoCompleteType="Disabled" />
            </td>
           <td>
              <%= HtmlInFieldLabel("DD", "Language.InlineLabel", "DD", 26)%>
              <asp:TextBox ID="DD" runat="server" Width="26" ClientIDMode="Static" AutoCompleteType="Disabled" />
            </td>
           <td>
              <%= HtmlInFieldLabel("YY", "Language.InlineLabel", "YY", 26)%>
              <asp:TextBox ID="YY" runat="server" Width="26" ClientIDMode="Static" AutoCompleteType="Disabled" />
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <%= HtmlInFieldLabel("EmployerNameTextBox", "Language.InlineLabel", "Type note or reminder here", 496)%>
        <asp:TextBox ID="EmployerNameTextBox" runat="server" Width="100%" ClientIDMode="Static" AutoCompleteType="Disabled" TextMode="MultiLine" Rows="4" />
      </td>
    </tr>
    <tr>
      <td colspan="2"><asp:Button ID="Button3" runat="server" Text="Add note or reminder" class="button3" /></td>
    </tr>
    <tr>
      <td colspan="2" align="right">
        <table class="Pager">
            <tr>
              <td>
                <asp:Label runat="server" ID="lblRecordCount"></asp:Label>
                <%=HtmlLocalise("Reporting.Results", "result(s) found")%> &nbsp;
              </td>
              <td id="tdPageStart" runat="server">
                <a href="#" id="lnkPagerStart">
                  <img id="PagerStart" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                    width="15" height="11" /></a>
              </td>
              <td id="tdPageBack" runat="server">
                <a href="#" id="lnkPagerPrevious">
                  <img id="PagerBack" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                    height="11" /></a>
              </td>
              <td>
                PAGE
              </td>
              <td>
                <asp:DropDownList ID="ddlPageNumber" runat="server">
                </asp:DropDownList>
              </td>
              <td>
                <%=HtmlLocalise("Reporting.Of", "of") %>
                <asp:Label runat="server" ID="lblMaxPageCount" />
              </td>
              <td id="tdPageNext" runat="server">
                <a href="#" id="lnkPagerNext">
                  <img id="PagerNext" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                    width="6" height="11" /></a>
              </td>
              <td id="tdPageLast" runat="server">
                <a href="#" id="lnkPagerEnd">
                  <img id="PagerEnd" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                    width="15" height="11" /></a>
              </td>
              <td style="padding-left: 15px">
                <asp:DropDownList ID="ddlPageSizes" runat="server">
                  <asp:ListItem Text="10" Value="10" />
                  <asp:ListItem Text="20" Value="20" />
                  <asp:ListItem Text="50" Value="50" />
                  <asp:ListItem Text="100" Value="100" />
                </asp:DropDownList>
              </td>
              <td>
                RESULTS PER PAGE
              </td>
            </tr>
          </table>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <div>Firstname Lastname – May 15, 2011</div>
        <div>This note is from staff member Firstname Lastname.</div><br />
        <div>Firstname Lastname – May 12, 2011</div>
        <div>This is another note. It might be much longer. Duis autem vel eum iriure dolor in 
        hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat 
        nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit 
        praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem 
        ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh 
        euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</div><br />
        <div>Firstname Lastname – April 18, 2011</div>
        <div>This note is from staff member Firstname Lastname.</div>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <asp:Button ID="Button1" runat="server" Text="Cancel" class="button4" />
        <asp:Button ID="Button2" runat="server" Text="Email employer" class="button3" />
      </td>
    </tr>
</table>

</asp:Panel>

</asp:Content>
