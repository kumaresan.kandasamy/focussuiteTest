﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page096.aspx.cs" Inherits="Focus.Web.Designs.FA28Page096" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<table width="100%">
    <tr>
      <td>
        <p class="pageNav">
          <a href="#"><%= HtmlLocalise("JobOrderDashboard", "Job seeker reports")%></a> | 
          <strong><%= HtmlLocalise("JobDevelopment", "Job order reports")%></strong> | 
          <a href="#"><%= HtmlLocalise("FindEmployer", "Employer reports")%></a> | 
          <a href="#"><%= HtmlLocalise("SendMessages", "EEO reports")%></a>
          <a href="#"><%= HtmlLocalise("FindEmployer", "Staff activity reports")%></a> | 
          <a href="#"><%= HtmlLocalise("SendMessages", "Lookup")%></a>
          <a href="#"><%= HtmlLocalise("FindEmployer", "Saved reports")%></a> | 
          <a href="#"><%= HtmlLocalise("SendMessages", "Labor/Insight")%></a>
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%">
          <tr>
            <td><a href="#"><%= HtmlLocalise("TopLink1", "return to report")%></a></td>
            <td align="right">
              <asp:DropDownList runat="server">
                <asp:ListItem Text="Export" />
              </asp:DropDownList>&nbsp;&nbsp;
              <asp:Button ID="Button1" runat="server" Text="print" class="button1 right" />
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td><h1><%= HtmlLocalise("SendMessages", "Hiring manager profile: Harold Saxon / Nifty Networks")%></h1></td>
    </tr>
    <tr>
      <td>
        <table class="accordionTable">
          <tr class="multipleAccordionTitle">
            <td><span id="namedAnchor1" class="accordionArrow"></span>Contact information</td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table>
              <tr>
                <td width="250"><%= HtmlLocalise("SendMessages", "Harold Saxon")%></td>
                <td><a href="#"><%= HtmlLocalise("SendMessages", "Access employer's Focus/Talent account")%></a></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Nifty Networks")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "742 Evergreen Terrace")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Springfield, KY 12345")%></td>
              </tr>
              <tr>
                <td colspan="2"><br /></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Phone number: 555-555-1212")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Fax number: 555-555-1234")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "email: hsaxon@niftynetworks.com")%></td>
              </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle">
            <td><span id="Span1" class="accordionArrow"></span>Sign-ins</td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Days since registration: 120")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Sign-ins since registration: 36")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Sign-ins in past 7 days: 1")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Time since last sign-in: 42 minutes")%></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle">
            <td><span id="Span2" class="accordionArrow"></span>Activity summary</td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table>
              <tr>
                <td colspan="2">
                  <asp:RadioButtonList runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Last 7 days" />
                    <asp:ListItem Text="Last 30 days" />
                  </asp:RadioButtonList>
                </td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Applicants: 250")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Matches: 129")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "3-5–star matches: 79")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "3-5–star matches employer did not view: 26")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Invitations sent to seekers: 28")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Invited seekers who viewed jobs: 23")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Invited seekers who did not view jobs: 5")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Invited seekers who clicked 'How to Apply': 3")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Invited seekers who did not click 'How to Apply': 25")%></td>
              </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle">
            <td><span id="Span3" class="accordionArrow"></span>Referral outcomes</td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Applicants interviewed/pending interviews: 20")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Interviews denied: 1")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Applicants hired: 0")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Applicants rejected: 2")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Applicants who refused offers: 0")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Applicants did not apply: 1 time")%></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle">
            <td><span id="Span4" class="accordionArrow"></span>Survey responses</td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Dissatisfied with resume quality: 3 times")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Satisfied with resume quality: 10 times")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Did not interview applicants: 8 times")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Springfield, KY 12345")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Interviewed applicants: 2 times")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Did not hire applicants: 1 time")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Hired applicants: 0 times")%></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle">
            <td><span id="namedAnchor2" class="accordionArrow"></span>Hiring manager activity log</td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table width="100%">
                <tr>
                <td width="40">Show</td>
                <td width="105">
                  <asp:DropDownList ID="DropDownList1" runat="server">
                    <asp:ListItem Text="all services" />
                  </asp:DropDownList>
                </td>
                <td width="25">for</td>
                <td width="107">
                  <asp:DropDownList ID="DropDownList23" runat="server">
                    <asp:ListItem Text="last 30 days" />
                  </asp:DropDownList>
                </td>
                <td width="30">by</td>
                <td>
                  <asp:DropDownList ID="DropDownList4" runat="server">
                    <asp:ListItem Text="all users" />
                  </asp:DropDownList>
                </td>
                <td align="right">
                  <table class="Pager">
                    <tr>
                      <td>
                        <asp:Label runat="server" ID="Label1"></asp:Label>
                        <%=HtmlLocalise("Reporting.Results2", "result(s) found")%> &nbsp;
                      </td>
                      <td id="td1" runat="server">
                        <a href="#" id="A1">
                          <img id="Img7" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                            width="15" height="11" /></a>
                      </td>
                      <td id="td2" runat="server">
                        <a href="#" id="A2">
                          <img id="Img8" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                            height="11" /></a>
                      </td>
                      <td>
                        PAGE
                      </td>
                      <td>
                        <asp:DropDownList ID="DropDownList2" runat="server">
                        </asp:DropDownList>
                      </td>
                      <td>
                        <%=HtmlLocalise("Reporting.Of2", "of") %>
                        <asp:Label runat="server" ID="Label2" />
                      </td>
                      <td id="td3" runat="server">
                        <a href="#" id="A3">
                          <img id="Img9" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                            width="6" height="11" /></a>
                      </td>
                      <td id="td4" runat="server">
                        <a href="#" id="A4">
                          <img id="Img10" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                            width="15" height="11" /></a>
                      </td>
                      <td style="padding-left: 15px">
                        <asp:DropDownList ID="DropDownList43" runat="server">
                          <asp:ListItem Text="10" Value="10" />
                          <asp:ListItem Text="20" Value="20" />
                          <asp:ListItem Text="50" Value="50" />
                          <asp:ListItem Text="100" Value="100" />
                        </asp:DropDownList>
                      </td>
                      <td>
                        RESULTS PER PAGE
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              </table>
              <table width="100%">
                <tr>
                  <td colspan="4">
                    <table width="100%" class="DataTable">
                      <thead>
                        <tr>
                          <th width="25%">
                            <%= HtmlLocalise("RecentPlacementsTH21", "USER NAME")%>
                            <img id="Img11" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img12" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH22", "DATE")%>
                            <img id="Img13" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img14" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH24", "ACTIVITY/SERVICE")%>
                            <img id="Img17" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img18" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "Lastname, Firstname")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "May 15, 2011")%></a></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "Viewed seeker's jobs, some other service")%></td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "Lastname, Firstname")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "May 15, 2011")%></a></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "Viewed seeker's jobs, some other service")%></td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "Lastname, Firstname")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "May 15, 2011")%></a></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "Viewed seeker's jobs, some other service")%></td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "Lastname, Firstname")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "May 15, 2011")%></a></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "Viewed seeker's jobs, some other service")%></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="4">
                    Add activity/service
                    <asp:DropDownList runat="server">
                      <asp:ListItem Text="- select one -" />
                      <asp:ListItem Text="text2" />
                    </asp:DropDownList>
                    <asp:Button ID="Button2" runat="server" Text="Add" class="button1" />
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle on">
            <td><span id="Span5" class="accordionArrow"></span>Notes and reminders</td>
          </tr>
          <tr class="accordionContent open">
            <td>
              <table width="450px">
                <tr>
                  <td colspan="2">
                    <asp:CheckBox ID="CheckBox1" Text="add a note for" runat="server" />
                    <asp:DropDownList ID="DropDownList3" runat="server">
                      <asp:ListItem Text="Harold Saxton" />
                      <asp:ListItem Text="text2" />
                    </asp:DropDownList>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <asp:CheckBox ID="CheckBox2" Text="add a reminder for" runat="server" />
                    <asp:DropDownList ID="DropDownList5" runat="server">
                      <asp:ListItem Text="Harold Saxton" />
                      <asp:ListItem Text="text2" />
                    </asp:DropDownList>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <table>
                      <tr>
                        <td><asp:RadioButton ID="RadioButton1" Text="email" runat="server" /></td>
                        <td><asp:RadioButton ID="RadioButton2" Text="post to dashboard on" runat="server" /></td>
                        <td>
                          <%= HtmlInFieldLabel("MM", "Language.InlineLabel", "MM", 26)%>
                          <asp:TextBox ID="MM" runat="server" Width="26" ClientIDMode="Static" AutoCompleteType="Disabled" />
                        </td>
                       <td>
                          <%= HtmlInFieldLabel("DD", "Language.InlineLabel", "DD", 26)%>
                          <asp:TextBox ID="DD" runat="server" Width="26" ClientIDMode="Static" AutoCompleteType="Disabled" />
                        </td>
                       <td>
                          <%= HtmlInFieldLabel("YY", "Language.InlineLabel", "YY", 26)%>
                          <asp:TextBox ID="YY" runat="server" Width="26" ClientIDMode="Static" AutoCompleteType="Disabled" />
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <%= HtmlInFieldLabel("EmployerNameTextBox", "Language.InlineLabel", "Type note or reminder here", 496)%>
                    <asp:TextBox ID="EmployerNameTextBox" runat="server" Width="100%" ClientIDMode="Static" AutoCompleteType="Disabled" TextMode="MultiLine" Rows="4" />
                  </td>
                </tr>
                <tr>
                  <td colspan="2"><asp:CheckBox Text="email me a copy of this reminder" runat="server" /></td>
                </tr>
                <tr>
                  <td colspan="2"><asp:Button ID="Button33" runat="server" Text="Add note or reminder" class="button3" /></td>
                </tr>
                <tr>
                  <td colspan="2" align="right">
                    <table class="Pager">
                        <tr>
                          <td>
                            <asp:Label runat="server" ID="lblRecordCount"></asp:Label>
                            <%=HtmlLocalise("Reporting.Results", "result(s) found")%> &nbsp;
                          </td>
                          <td id="tdPageStart" runat="server">
                            <a href="#" id="lnkPagerStart">
                              <img id="PagerStart" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                                width="15" height="11" /></a>
                          </td>
                          <td id="tdPageBack" runat="server">
                            <a href="#" id="lnkPagerPrevious">
                              <img id="PagerBack" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                                height="11" /></a>
                          </td>
                          <td>
                            PAGE
                          </td>
                          <td>
                            <asp:DropDownList ID="ddlPageNumber" runat="server">
                            </asp:DropDownList>
                          </td>
                          <td>
                            <%=HtmlLocalise("Reporting.Of", "of") %>
                            <asp:Label runat="server" ID="lblMaxPageCount" />
                          </td>
                          <td id="tdPageNext" runat="server">
                            <a href="#" id="lnkPagerNext">
                              <img id="PagerNext" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                                width="6" height="11" /></a>
                          </td>
                          <td id="tdPageLast" runat="server">
                            <a href="#" id="lnkPagerEnd">
                              <img id="PagerEnd" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                                width="15" height="11" /></a>
                          </td>
                          <td style="padding-left: 15px">
                            <asp:DropDownList ID="ddlPageSizes" runat="server">
                              <asp:ListItem Text="10" Value="10" />
                              <asp:ListItem Text="20" Value="20" />
                              <asp:ListItem Text="50" Value="50" />
                              <asp:ListItem Text="100" Value="100" />
                            </asp:DropDownList>
                          </td>
                          <td>
                            RESULTS PER PAGE
                          </td>
                        </tr>
                      </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <div>Firstname Lastname – May 15, 2011</div>
                    <div>This note is from staff member Firstname Lastname.</div><br />
                    <div>Firstname Lastname – May 12, 2011</div>
                    <div>This is another note. It might be much longer. Duis autem vel eum iriure dolor in 
                    hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat 
                    nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit 
                    praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem 
                    ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh 
                    euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</div><br />
                    <div>Firstname Lastname – April 18, 2011</div>
                    <div>This note is from staff member Firstname Lastname.</div>
                  </td>
                </tr>
            </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
</table>


</asp:Content>
