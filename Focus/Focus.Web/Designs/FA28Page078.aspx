﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page078.aspx.cs" Inherits="Focus.Web.Designs.FA28Page078" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<p class="pageNav">
  <a href="<%= UrlBuilder.JobSeekerReferrals() %>"><%= HtmlLocalise("ReturnToList.LinkText", "Return to main page")%></a>
</p>

<table width="100%">
  <tr>
			<td width="50%"><h1><%= HtmlLocalise("JobSeekerReferral.Header", "Approve job posting: System Engineer (ID #12345)")%></h1></td>
			<td width="50%">
				<table class="right">
					<tr>
						<td><asp:button ID="DenyButton" runat="server" CssClass="button1" Text="Save &amp; email employer" /></td>
						<td><asp:button ID="Button1" runat="server" CssClass="button1" Text="Deny posting" /></td>
						<td><asp:button ID="ApproveButton" runat="server" CssClass="button5 right" Text="Approve posting" /></td>
					</tr>
				</table>
			</td>
		</tr>
</table>

<table width="100%">
  <tr>
    <td>
    <div style="overflow:scroll;height:400px;width:600px">
      <table>
    <tr>
        <td>742 Evergreen Terrace</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Springﬁeld, KY 12345</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><br /></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Phone number: 555-555-1212</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Fax number: 555-555-1234</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>URL: http://www.niftynetworks.com</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><br /></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>FEIN #: 12-3456789</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>SEIN #: 00-00000</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><br /></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><strong>Industry classiﬁcation</strong>: Application software programming services, custom computer (541511)</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><br /></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><strong>Contact:</strong></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Harold Saxon</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Nifty Networks</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>742 Evergreen Terrace</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Springﬁeld, KY 12345</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><br /></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Email: h.saxon@niftynetworks.com</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Phone number: 555-555-0000</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Fax number: 555-555-1234</td>
        <td></td>
        <td></td>
    </tr>

    <tr>
      <td colspan="3"><asp:CheckBox ID="CheckBox1" Text="Add job post to tax credit list" runat="server" /></td>
    </tr>
    <tr>
      <td colspan="3"><asp:CheckBox ID="CheckBox2" Text="Add job post to job development list" runat="server" /></td>
    </tr>

      </table>
      </div>
    </td>
    <td valign="top" align="right">
      <div class="sidePanel" style="width:300px">
        <h3>Status</h3>
        <p>We have flagged two potentially inappropriate words/phrases in this posting:</p>
        <ul>
          <li><a href="#">Job posting title</a></li>
          <li><a href="#">Job posting title</a></li>
        </ul>
      </div>
    </td>
  </tr>
</table>

</asp:Content>
