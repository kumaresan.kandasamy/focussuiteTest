﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page123.aspx.cs" Inherits="Focus.Web.Designs.FA28Page123" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<asp:Panel ID="ModalPanel" runat="server" CssClass="modal" Style="display:block;width:500px;">

<table width="100%">
  <tr>
    <td><h1><%= HtmlLocalise("JobOrderDashboard", "Reset password")%></h1></td>
  </tr>
  <tr>
    <td>
      <%= HtmlLocalise("JobOrderDashboard", "Reset this user's password and automatically email them a link they can use to change their password?")%>
    </td>
  </tr>
  <tr>
    <td>
      <asp:Button ID="Button1" runat="server" Text="Cancel" class="button4" />
      <asp:Button ID="Button2" runat="server" Text="Reset password" class="button3" />
    </td>
  </tr>
</table>

</asp:Panel>

</asp:Content>
