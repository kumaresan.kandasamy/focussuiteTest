﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page121.aspx.cs" Inherits="Focus.Web.Designs.FA28Page121" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<h1><%=HtmlLocalise("Reporting.Of2", "Locate account")%></h1>

<table width="100%">
  <tr>
    <td width="266" valign="top">
      <div class="subItemsPanel">
        <table>
          <tr>
            <td>
              <%= HtmlInFieldLabel("FirstName", "Language.InlineLabel", "First name", 250)%>
              <asp:TextBox ID="FirstName" runat="server" Width="250" ClientIDMode="Static" AutoCompleteType="Disabled" />
            </td>
          </tr>
          <tr>
            <td>
              <%= HtmlInFieldLabel("LastName", "Language.InlineLabel", "Last name", 250)%>
              <asp:TextBox ID="LastName" runat="server" Width="250" ClientIDMode="Static" AutoCompleteType="Disabled" />
            </td>
          </tr>
          </table>
          <div class="divider"></div>
          <table>
          <tr>
            <td>
              <%= HtmlInFieldLabel("EmailAd", "Language.InlineLabel", "Email address", 250)%>
              <asp:TextBox ID="EmailAd" runat="server" Width="250" ClientIDMode="Static" AutoCompleteType="Disabled" />
            </td>
          </tr>
          </table>
          <div class="divider"></div>
          <table>
          <tr>
            <td>
              <%= HtmlInFieldLabel("Office", "Language.InlineLabel", "Office", 250)%>
              <asp:TextBox ID="Office" runat="server" Width="250" ClientIDMode="Static" AutoCompleteType="Disabled" />
            </td>
          </tr>
          <tr>
            <td colspan="2" align="right"><asp:Button ID="Button13" runat="server" Text="Find" class="button3" style="margin-top:4px;" /></td>
          </tr>
        </table>
      </div>
    </td>
    <td style="width: 100%;"></td>
    <td valign="top">
    <div><strong>Results</strong>&nbsp;&nbsp;25 results found</div>
    <br />
    <table width="750">
      <tr>
        <td width="95"><strong><%= HtmlLocalise("TopLink1", "Smith, Abigail")%></strong></td>
        <td width="95"><%= HtmlLocalise("TopLink1", "State Agency")%></td>
        <td width="115"><%= HtmlLocalise("TopLink1", "foo@example.com")%></td>
        <td width="170"><asp:CheckBox Text="Manager" runat="server" /></td>
        <td align="right"><a href="#"><%= HtmlLocalise("TopLink1", "Reset password")%></a></td>
        <td align="right"><a href="#"><%= HtmlLocalise("TopLink1", "Deactivate account")%></a></td>
      </tr>
    </table>
    <table width="750">
      <tr>
        <td valign="top" width="33%" style="padding-right:6px;">
          <ul class="accordionList">
            <li class="multipleAccordionTitle2"><span class="plusMinusImage"></span><%= HtmlLocalise("Label", "Manage job seekers")%></li>
            <li class="accordionContent2" style="display: list-item;">
              <span class="plusMinusImage"></span>
              <table>
                <tr>
                  <td>
                    <asp:CheckBoxList runat="server" RepeatDirection="Vertical">
                      <asp:ListItem Text="View accounts" />
                      <asp:ListItem Text="Edit accounts" />
                      <asp:ListItem Text="Approve referral requests automatically (bypassing queue)" />
                    </asp:CheckBoxList>
                  </td>
                </tr>
              </table>
            </li>
            <li class="multipleAccordionTitle2"><span class="plusMinusImage"></span><%= HtmlLocalise("Label", "Manage employers")%></li>
            <li class="accordionContent2" style="display: none;">
            <span class="plusMinusImage"></span>
            <table>
                <tr>
                  <td>
                    <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatDirection="Vertical">
                      <asp:ListItem Text="View accounts" />
                      <asp:ListItem Text="Edit accounts" />
                      <asp:ListItem Text="Approve job orders automatically (bypassing queue)" />
                      <asp:ListItem Text="Override TEGL warnings" />
                      <asp:ListItem Text="Handle job development" />
                    </asp:CheckBoxList>
                  </td>
                </tr>
              </table>
            </li>
          </ul>
        </td>
        <td valign="top" width="33%" style="padding-right:6px;">
          <ul class="accordionList">
            <li class="multipleAccordionTitle2"><span class="plusMinusImage"></span><%= HtmlLocalise("Label", "Manage job orders")%></li>
            <li class="accordionContent2" style="display: list-item;">
              <span class="plusMinusImage"></span>
              <table>
                <tr>
                  <td>
                    <asp:CheckBoxList ID="CheckBoxList2" runat="server" RepeatDirection="Vertical">
                      <asp:ListItem Text="View" />
                      <asp:ListItem Text="Edit" />
                    </asp:CheckBoxList>
                  </td>
                </tr>
              </table>
            </li>
            <li class="multipleAccordionTitle2"><span class="plusMinusImage"></span><%= HtmlLocalise("Label", " Manage approval queues")%></li>
            <li class="accordionContent2" style="display: none;">
            <span class="plusMinusImage"></span>
            <div><strong>Job order queue</strong></div>
            <table>
                <tr>
                  <td>
                    <asp:CheckBoxList ID="CheckBoxList3" runat="server" RepeatDirection="Vertical">
                      <asp:ListItem Text="View" />
                      <asp:ListItem Text="Edit" />
                    </asp:CheckBoxList>
                  </td>
                </tr>
              </table>
              <div><strong>New employer account queue</strong></div>
              <table>
                <tr>
                  <td>
                    <asp:CheckBoxList ID="CheckBoxList6" runat="server" RepeatDirection="Vertical">
                      <asp:ListItem Text="View" />
                      <asp:ListItem Text="Edit" />
                    </asp:CheckBoxList>
                  </td>
                </tr>
              </table>
              <div><strong> Referral queue</strong></div>
              <table>
                <tr>
                  <td>
                    <asp:CheckBoxList ID="CheckBoxList7" runat="server" RepeatDirection="Vertical">
                      <asp:ListItem Text="View" />
                      <asp:ListItem Text="Edit" />
                    </asp:CheckBoxList>
                  </td>
                </tr>
              </table>
            </li>
          </ul>
        </td>
        <td valign="top" width="33%" style="padding-right:6px;">
          <ul class="accordionList">
            <li class="multipleAccordionTitle2"><span class="plusMinusImage"></span><%= HtmlLocalise("Label", "Manage report access")%></li>
            <li class="accordionContent2" style="display: list-item;">
              <span class="plusMinusImage"></span>
              <table>
                <tr>
                  <td>
                    <asp:CheckBoxList ID="CheckBoxList4" runat="server" RepeatDirection="Vertical">
                      <asp:ListItem Text="Run job seeker reports" />
                      <asp:ListItem Text="Run employer reports" />
                      <asp:ListItem Text="Run staff activity reports" />
                      <asp:ListItem Text="Access Labor/Insight" />
                    </asp:CheckBoxList>
                  </td>
                </tr>
              </table>
            </li>
            <li class="multipleAccordionTitle2"><span class="plusMinusImage"></span><%= HtmlLocalise("Label", "Manage Focus/Assist")%></li>
            <li class="accordionContent2" style="display: none;">
            <span class="plusMinusImage"></span>
              <div><i>Only system administrators may set system defaults or assign other system administrators.</i></div>
              <div><strong>Account privileges</strong></div>
              <table>
                <tr>
                  <td>
                    <asp:CheckBoxList ID="CheckBoxList8" runat="server" RepeatDirection="Vertical">
                      <asp:ListItem Text="Administer staff accounts" />
                      <asp:ListItem Text="Administer system" />
                      <asp:ListItem Text="Broadcast messages" />
                    </asp:CheckBoxList>
                  </td>
                </tr>
              </table>
              <div><strong>Handle these special job orders</strong></div>
              <table>
                <tr>
                  <td>
                    <asp:CheckBoxList ID="CheckBoxList5" runat="server" RepeatDirection="Vertical">
                      <asp:ListItem Text="Affirmative action" />
                      <asp:ListItem Text="Federal contractor" />
                      <asp:ListItem Text="Foreign labor – agricultural" />
                      <asp:ListItem Text="Foreign labor – non-agricultural" />
                    </asp:CheckBoxList>
                  </td>
                </tr>
              </table>
            </li>
          </ul>
        </td>
      </tr>
    </table>
    <div class="OnPageDivider"></div>
    </td>
  </tr>
</table>



</asp:Content>
