﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page126.aspx.cs" Inherits="Focus.Web.Designs.FA28Page126" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<table width="100%">
  <tr>
    <td width="140"><h1>Contact us</h1></td>
    <td width="500"></td>
    <td align="right"><asp:Button ID="Button8" runat="server" Text="Send email" class="button3" /></td>
  </tr>
  <tr>
    <td>Reason for contact</td>
    <td colspan="2"><asp:RadioButton id="Radio1" GroupName="Group1" Text="I have a comment or question" runat="server"/></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="2"><asp:RadioButton id="RadioButton1" GroupName="Group1" Text="I am reporting an issue" runat="server"/></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="2">
      <asp:DropDownList runat="server" ID="DropDownList5" Width="250">
        <asp:ListItem>- select issue -</asp:ListItem>
      </asp:DropDownList>
    </td>
  </tr>
  <tr>
    <td></td>
    <td colspan="2"><asp:RadioButton id="RadioButton2" GroupName="Group2" Text="I would like to suggest a website to source jobs from" runat="server"/></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="2">
      <asp:TextBox ID="TextBox2" runat="server" TextMode="MultiLine" Rows="5" Text="" Width="400" style="overflow-y:scroll" />
    </td>
  </tr>
</table>

</asp:Content>
