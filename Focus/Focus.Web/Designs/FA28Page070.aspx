﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page070.aspx.cs" Inherits="Focus.Web.Designs.FA28Page070" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<asp:Panel ID="ModalPanel" runat="server" CssClass="modal" Style="display:block;width:500px;">
	<table width="100%">
        <tr>
          <td><h1>Notification of Denied Job Referral Request</h1></td>
          <td><img src="/Assets/images/button_x_close_off.png" onclick="alert('close')" width="16" height="15" alt="" style="float:right;" /></td>
        </tr>
        <tr>
          <td colspan="2">Notify the job seeker their referral request has been denied</td>
        </tr>
        <tr>
          <td colspan="2">
            <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Rows="10" Text="" />
          </td>
        </tr>
        <tr>
            <td><asp:CheckBox id="CheckBox1" Text="email me a copy of this message" TextAlign="Right" runat="server" /></td>
        </tr>
        <tr>
          <td>
            <asp:Button ID="Button1" runat="server" Text="Cancel" class="button4" />
            <asp:Button ID="Button2" runat="server" Text="Email seeker" class="button3" />
          </td>
        </tr>
    </table>
</asp:Panel>


</asp:Content>
