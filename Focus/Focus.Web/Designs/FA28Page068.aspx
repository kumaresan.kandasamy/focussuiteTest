﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page068.aspx.cs" Inherits="Focus.Web.Designs.FA28Page068" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<h3>Approve referral requests</h3>

<table width="100%" style="border-collapse:collapse;border:none;">
  <tr>
    <td>
      <table>
        <tr>
          <td width="85">for this office</td>
          <td width="148">
            <asp:DropDownList ID="DropDownList7" runat="server">
              <asp:ListItem Text="- choose location -" />
            </asp:DropDownList>
          </td>
          <td width="77">managed by</td>
          <td>
            <asp:DropDownList ID="DropDownList8" runat="server">
              <asp:ListItem Text="All staff members" />
            </asp:DropDownList>
          </td>
        </tr>
      </table>
    </td>
    <td align="right">
      <table class="Pager">
        <tr>
          <td>
            <asp:Label runat="server" ID="Label1"></asp:Label>
            <%=HtmlLocalise("Reporting.Results2", "result(s) found")%> &nbsp;
          </td>
          <td id="td1" runat="server">
            <a href="#" id="A1">
              <img id="Img7" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                width="15" height="11" /></a>
          </td>
          <td id="td2" runat="server">
            <a href="#" id="A2">
              <img id="Img8" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                height="11" /></a>
          </td>
          <td>
            PAGE
          </td>
          <td>
            <asp:DropDownList ID="DropDownList1" runat="server">
            </asp:DropDownList>
          </td>
          <td>
            <%=HtmlLocalise("Reporting.Of2", "of") %>
            <asp:Label runat="server" ID="Label2" />
          </td>
          <td id="td3" runat="server">
            <a href="#" id="A3">
              <img id="Img9" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                width="6" height="11" /></a>
          </td>
          <td id="td4" runat="server">
            <a href="#" id="A4">
              <img id="Img10" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                width="15" height="11" /></a>
          </td>
          <td style="padding-left: 15px">
            <asp:DropDownList ID="DropDownList2" runat="server">
              <asp:ListItem Text="10" Value="10" />
              <asp:ListItem Text="20" Value="20" />
              <asp:ListItem Text="50" Value="50" />
              <asp:ListItem Text="100" Value="100" />
            </asp:DropDownList>
          </td>
          <td>
            RESULTS PER PAGE
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<table width="100%" class="DataTable">
  <thead>
    <tr>
      <th width="25%">
        <%= HtmlLocalise("RecentPlacementsTH21", "JOB TITLE/EMPLOYER")%>
        <img id="Img11" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
        <img id="Img12" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
      </th>
      <th width="15%">
        <%= HtmlLocalise("RecentPlacementsTH22", "JOB SEEKER NAME")%>
        <img id="Img13" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
        <img id="Img14" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
      </th>
      <th width="15%">
        <%= HtmlLocalise("RecentPlacementsTH24", "RATING")%>
        <img id="Img17" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
        <img id="Img18" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
      </th>
      <th width="15%">
        <%= HtmlLocalise("RecentPlacementsTH24", "REFERRAL DATE")%>
        <img id="Img1" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
        <img id="Img2" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
      </th><th width="15%">
        <%= HtmlLocalise("RecentPlacementsTH24", "TIME IN QUEUE")%>
        <img id="Img3" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
        <img id="Img4" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
      </th>    </tr>
  </thead>
  <tbody>
    <tr>
      <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Job Title, CompanyOne")%></a></td>
      <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Mickey Smith")%></a></td>
      <td valign="top"><img src="../Assets/Images/starsfour.gif" alt="stars" width="80" height="16" /></td>
      <td valign="top">Apr 10, 2011</td>
      <td valign="top">2 days</td>
    </tr>
    <tr>
      <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Job Title, CompanyOne")%></a></td>
      <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Mickey Smith")%></a></td>
      <td valign="top"><img src="../Assets/Images/starsfour.gif" alt="stars" width="80" height="16" /></td>
      <td valign="top">Apr 10, 2011</td>
      <td valign="top">2 days</td>
    </tr>
    <tr>
      <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Job Title, CompanyOne")%></a></td>
      <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Mickey Smith")%></a></td>
      <td valign="top"><img src="../Assets/Images/starsfour.gif" alt="stars" width="80" height="16" /></td>
      <td valign="top">Apr 10, 2011</td>
      <td valign="top">2 days</td>
    </tr>
  </tbody>
</table>

<br />

<h3>Approve employer account requests</h3>

<table width="100%" style="border-collapse:collapse;border:none;">
  <tr>
    <td>
      <table>
        <tr>
          <td width="85">for this office</td>
          <td width="148">
            <asp:DropDownList ID="DropDownList3" runat="server">
              <asp:ListItem Text="- choose location -" />
            </asp:DropDownList>
          </td>
          <td width="77">managed by</td>
          <td>
            <asp:DropDownList ID="DropDownList4" runat="server">
              <asp:ListItem Text="All staff members" />
            </asp:DropDownList>
          </td>
        </tr>
      </table>
    </td>
    <td align="right">
      <table class="Pager">
        <tr>
          <td>
            <asp:Label runat="server" ID="Label3"></asp:Label>
            <%=HtmlLocalise("Reporting.Results2", "result(s) found")%> &nbsp;
          </td>
          <td id="td5" runat="server">
            <a href="#" id="A5">
              <img id="Img5" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                width="15" height="11" /></a>
          </td>
          <td id="td6" runat="server">
            <a href="#" id="A6">
              <img id="Img6" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                height="11" /></a>
          </td>
          <td>
            PAGE
          </td>
          <td>
            <asp:DropDownList ID="DropDownList9" runat="server">
            </asp:DropDownList>
          </td>
          <td>
            <%=HtmlLocalise("Reporting.Of2", "of") %>
            <asp:Label runat="server" ID="Label4" />
          </td>
          <td id="td7" runat="server">
            <a href="#" id="A7">
              <img id="Img15" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                width="6" height="11" /></a>
          </td>
          <td id="td8" runat="server">
            <a href="#" id="A8">
              <img id="Img16" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                width="15" height="11" /></a>
          </td>
          <td style="padding-left: 15px">
            <asp:DropDownList ID="DropDownList10" runat="server">
              <asp:ListItem Text="10" Value="10" />
              <asp:ListItem Text="20" Value="20" />
              <asp:ListItem Text="50" Value="50" />
              <asp:ListItem Text="100" Value="100" />
            </asp:DropDownList>
          </td>
          <td>
            RESULTS PER PAGE
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="100%" class="DataTable">
  <thead>
    <tr>
      <th width="25%">
        <%= HtmlLocalise("RecentPlacementsTH21", "EMPLOYER NAME")%>
        <img id="Img19" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
        <img id="Img20" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
      </th>
      <th width="15%">
        <%= HtmlLocalise("RecentPlacementsTH22", "CONTACT NAME")%>
        <img id="Img21" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
        <img id="Img22" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
      </th>
      <th width="15%">
        <%= HtmlLocalise("RecentPlacementsTH24", "ACCOUNT CREATION DATE")%>
        <img id="Img23" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
        <img id="Img24" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
      </th>
      <th width="15%">
        <%= HtmlLocalise("RecentPlacementsTH24", "TIME IN QUEUE")%>
        <img id="Img25" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
        <img id="Img26" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
      </th>
      </tr>
  </thead>
  <tbody>
    <tr>
      <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Company Name")%></a></td>
      <td valign="top"><%= HtmlLocalise("Name31", "Firstname Lastname")%></td>
      <td valign="top"><%= HtmlLocalise("Name31", "Apr 10, 2011")%></td>
      <td valign="top"><%= HtmlLocalise("Name31", "2 days")%></td>
    </tr>
    <tr>
      <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Company Name")%></a></td>
      <td valign="top"><%= HtmlLocalise("Name31", "Firstname Lastname")%></td>
      <td valign="top"><%= HtmlLocalise("Name31", "Apr 10, 2011")%></td>
      <td valign="top"><%= HtmlLocalise("Name31", "2 days")%></td>
    </tr>
    <tr>
      <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Company Name")%></a></td>
      <td valign="top"><%= HtmlLocalise("Name31", "Firstname Lastname")%></td>
      <td valign="top"><%= HtmlLocalise("Name31", "Apr 10, 2011")%></td>
      <td valign="top"><%= HtmlLocalise("Name31", "2 days")%></td>
    </tr>
  </tbody>
</table>

<br />

<h3>Approve job postings</h3>

<table width="100%" style="border-collapse:collapse;border:none;">
  <tr>
    <td>
      <table>
        <tr>
          <td width="85">for this office</td>
          <td width="148">
            <asp:DropDownList ID="DropDownList5" runat="server">
              <asp:ListItem Text="- choose location -" />
            </asp:DropDownList>
          </td>
          <td width="77">managed by</td>
          <td>
            <asp:DropDownList ID="DropDownList6" runat="server">
              <asp:ListItem Text="All staff members" />
            </asp:DropDownList>
          </td>
        </tr>
      </table>
    </td>
    <td align="right">
      <table class="Pager">
        <tr>
          <td>
            <asp:Label runat="server" ID="Label5"></asp:Label>
            <%=HtmlLocalise("Reporting.Results2", "result(s) found")%> &nbsp;
          </td>
          <td id="td9" runat="server">
            <a href="#" id="A9">
              <img id="Img27" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                width="15" height="11" /></a>
          </td>
          <td id="td10" runat="server">
            <a href="#" id="A10">
              <img id="Img28" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                height="11" /></a>
          </td>
          <td>
            PAGE
          </td>
          <td>
            <asp:DropDownList ID="DropDownList11" runat="server">
            </asp:DropDownList>
          </td>
          <td>
            <%=HtmlLocalise("Reporting.Of2", "of") %>
            <asp:Label runat="server" ID="Label6" />
          </td>
          <td id="td11" runat="server">
            <a href="#" id="A11">
              <img id="Img29" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                width="6" height="11" /></a>
          </td>
          <td id="td12" runat="server">
            <a href="#" id="A12">
              <img id="Img30" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                width="15" height="11" /></a>
          </td>
          <td style="padding-left: 15px">
            <asp:DropDownList ID="DropDownList12" runat="server">
              <asp:ListItem Text="10" Value="10" />
              <asp:ListItem Text="20" Value="20" />
              <asp:ListItem Text="50" Value="50" />
              <asp:ListItem Text="100" Value="100" />
            </asp:DropDownList>
          </td>
          <td>
            RESULTS PER PAGE
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="100%" class="DataTable">
  <thead>
    <tr>
      <th width="25%">
        <%= HtmlLocalise("RecentPlacementsTH21", "JOB TITLE")%>
        <img id="Img31" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
        <img id="Img32" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
      </th>
      <th width="15%">
        <%= HtmlLocalise("RecentPlacementsTH22", "EMPLOYER NAME")%>
        <img id="Img33" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
        <img id="Img34" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
      </th>
      <th width="15%">
        <%= HtmlLocalise("RecentPlacementsTH24", "POSTING DATE")%>
        <img id="Img35" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
        <img id="Img36" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
      </th>
      <th width="15%">
        <%= HtmlLocalise("RecentPlacementsTH24", "TIME IN QUEUE")%>
        <img id="Img37" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
        <img id="Img38" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
      </th>
      </tr>
  </thead>
  <tbody>
    <tr>
      <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Company Name")%></a></td>
      <td valign="top"><%= HtmlLocalise("Name31", "Firstname Lastname")%></td>
      <td valign="top"><%= HtmlLocalise("Name31", "Apr 10, 2011")%></td>
      <td valign="top"><%= HtmlLocalise("Name31", "2 days")%></td>
    </tr>
    <tr>
      <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Company Name")%></a></td>
      <td valign="top"><%= HtmlLocalise("Name31", "Firstname Lastname")%></td>
      <td valign="top"><%= HtmlLocalise("Name31", "Apr 10, 2011")%></td>
      <td valign="top"><%= HtmlLocalise("Name31", "2 days")%></td>
    </tr>
    <tr>
      <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "Company Name")%></a></td>
      <td valign="top"><%= HtmlLocalise("Name31", "Firstname Lastname")%></td>
      <td valign="top"><%= HtmlLocalise("Name31", "Apr 10, 2011")%></td>
      <td valign="top"><%= HtmlLocalise("Name31", "2 days")%></td>
    </tr>
  </tbody>
</table>

</asp:Content>
