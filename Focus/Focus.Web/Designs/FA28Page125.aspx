﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page125.aspx.cs" Inherits="Focus.Web.Designs.FA28Page125" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<table width="100%">
  <tr>
    <td><p class="pageNav"><a href="#">Set system defaults</a> | <strong>Manage staff accounts</strong> | <a href="#">Manage home page announcements</a> | <a href="#">Send feedback</a> </p></td>
  </tr>
  <tr class="multipleAccordionTitle">
    <td><span class="accordionArrow"></span>Update home page announcements</td>
    <td align="right">
      <asp:Button ID="Button1" runat="server" Text="Update" class="button1" style="margin-top:5px;" />
      <asp:Button ID="Button2" runat="server" Text="Save announcement" class="button1" style="margin-top:5px;" />
    </td>
  </tr>
  <tr class="accordionContent">
    <td>
      <table width="100%">
        <tr>
          <td width="90px">Job seekers</td>
          <td>
            <asp:DropDownList runat="server" ID="DropDownList5" Width="400px">
              <asp:ListItem>New announcement</asp:ListItem>
            </asp:DropDownList>
          </td>
        </tr>
        <tr>
          <td colspan="2"><br /></td>
        </tr>
        <tr>
          <td width="90px"></td>
          <td>
          <span class="inFieldLabel">
            <label for="NameYourSavedAnnouncement">Name your saved announcement</label>
            <asp:TextBox runat="server" data-val="true" data-val-required="hexCode1" ID="NameYourSavedAnnouncement" Width="394px" ClientIDMode="Static" />
          </span>
          </td>
        </tr>
        <tr>
          <td colspan="2"><br /></td>
        </tr>
        <tr>
          <td></td>
          <td>
            <span class="inFieldLabel" style="height:98px">
              <label for="addNewMessageHere">Add new announcement here</label>
              <asp:TextBox runat="server" data-val="true" data-val-required="Add new message here." ID="addNewMessageHere" Width="394px" TextMode="MultiLine" Rows="6" ClientIDMode="Static" />
            </span>
          </td>
        </tr>
        <tr>
          <td colspan="2"><br /></td>
        </tr>
        <tr>
          <td>Employers</td>
          <td>
            <asp:DropDownList runat="server" ID="DropDownList1" Width="400px">
              <asp:ListItem>Select a saved announcement or create new</asp:ListItem>
            </asp:DropDownList>
          </td>
        </tr>
        <tr>
          <td colspan="2"><br /></td>
        </tr>
        <tr>
          <td>Staff members</td>
          <td>
            <asp:DropDownList runat="server" ID="DropDownList2" Width="400px">
              <asp:ListItem>Select a saved announcement or create new</asp:ListItem>
            </asp:DropDownList>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr class="multipleAccordionTitle on">
    <td><span class="accordionArrow"></span>Saved announcements</td>
    <td align="right">
      <asp:Button ID="Button4" runat="server" Text="Save announcement" class="button1" style="margin-top:5px;" />
    </td>
  </tr>
  <tr class="accordionContent open">
    <td>
      <table>
        <tr>
          <td><strong>Job seeker messages</strong></td>
        </tr>
        <tr>
          <td width="160" valign="top">My Saved Message Name</td>
          <td width="400">
            <asp:TextBox ID="TextBox2" runat="server" TextMode="MultiLine" Rows="5" Text="" style="overflow-y:scroll" />
          </td>
          <td width="20" valign="top"><img id="closeLogo" src="Assets/Images/button_x_close_on.png" onclick="alert('close1')" alt="Main Logo" width="16" height="15"></td>
        </tr>
        <tr>
          <td valign="top">Another Saved Message Name</td>
          <td width="400">
            <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Rows="5" Text="" style="overflow-y:scroll" />
          </td>
          <td valign="top"><img id="Img1" src="Assets/Images/button_x_close_on.png" onclick="alert('close1')" alt="Main Logo" width="16" height="15"></td>
        </tr>
        <tr>
          <td><br /></td>
        </tr>
        <tr>
          <td><strong>Employer messages</strong></td>
        </tr>
        <tr>
          <td width="160" valign="top">My Saved Message Name</td>
          <td width="400">
            <asp:TextBox ID="TextBox3" runat="server" TextMode="MultiLine" Rows="5" Text="" style="overflow-y:scroll" />
          </td>
          <td width="20" valign="top"><img id="Img2" src="Assets/Images/button_x_close_on.png" onclick="alert('close1')" alt="Main Logo" width="16" height="15"></td>
        </tr>
      </table>
    </td>
  </tr>

</table>

</asp:Content>
