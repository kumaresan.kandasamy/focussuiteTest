﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page132.aspx.cs" Inherits="Focus.Web.Designs.FA28Page132" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<table width="100%">
    <tr>
      <td>
        <p class="pageNav">
          <a href="#"><%= HtmlLocalise("JobOrderDashboard", "Job order dashboard")%></a> | 
          <strong><%= HtmlLocalise("JobDevelopment", "Job development")%></strong> | 
          <a href="#"><%= HtmlLocalise("FindEmployer", "Find employer")%></a> | 
          <a href="#"><%= HtmlLocalise("SendMessages", "Send messages")%></a>
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%">
          <tr>
            <td><h1><%= HtmlLocalise("JobOrderDashboard", "Account settings")%></h1></td>
            <td></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td><br /></td>
    </tr>
    <tr>
      <td>
        <table width="100%">
          <tr>
            <td><h2>Email alerts</h2></td>
            <td><asp:Button ID="Button1" runat="server" Text="Save" class="button3"/></td>
          </tr>
          <tr>
            <td colspan="2">
              <table>
                <tr>
                  <td width="140">Send email alerts for</td>
                  <td>
                    <asp:CheckBox ID="CheckBox1" Text="new referral requests" runat="server" />&nbsp&nbsp;
                    <asp:CheckBox ID="CheckBox2" Text="employer account requests" runat="server" />&nbsp;&nbsp;
                    <asp:CheckBox ID="CheckBox3" Text="new job postings" runat="server" />
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <table>
                <tr>
                  <td width="140">Frequency</td>
                  <td>
                    <asp:DropDownList runat="server" Width="120">
                      <asp:ListItem Text="daily" />
                      <asp:ListItem Text="text2" />
                    </asp:DropDownList>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
</table>


</asp:Content>
