﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page130.aspx.cs" Inherits="Focus.Web.Designs.FA28Page130" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<table width="100%">
  <tr>
    <td colspan="2">
    <p class="pageNav">
      <a href="#"><%= HtmlLocalise("JobOrderDashboard", "Job order dashboard")%></a> | 
      <strong><%= HtmlLocalise("JobDevelopment", "Job development")%></strong> | 
      <a href="#"><%= HtmlLocalise("FindEmployer", "Find employer")%></a> | 
      <a href="#"><%= HtmlLocalise("SendMessages", "Send messages")%></a>
    </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
    <h1><%= HtmlLocalise("JobOrderDashboard", "Account settings")%></h1>
    </td>
  </tr>
  <tr>
    <td>
    Saved searches for
    <asp:DropDownList runat="server">
      <asp:ListItem Text="job orders" />
      <asp:ListItem Text="text2" />
    </asp:DropDownList>
    </td>
    <td><asp:Button ID="Button1" runat="server" Text="Save" class="button1 right" /></td>
  </tr>
  <tr>
    <td colspan="2"><br /></td>
  </tr>
  <tr>
  <td colspan="2">
    <table width="100%" class="DataTable">
    <thead>
      <tr>
      <th width="25%">
        <%= HtmlLocalise("RecentPlacementsTH1", "Saved search name")%>
      </th>
      <th width="15%">
        <%= HtmlLocalise("RecentPlacementsTH2", "Search criteria")%>
      </th>
      <th width="15%">
        <%= HtmlLocalise("RecentPlacementsTH4", "Status")%>
      </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td valign="top">
          <%= HtmlLocalise("RecentPlacementsTd1-1", "System Engineer - Level 1")%>
          <br />
          <i><%= HtmlLocalise("RecentPlacementsTd1-1", "reated April 9, 2011")%></i>
        </td>
        <td valign="top">
          <div><%= HtmlLocalise("RecentPlacementsTd1-2", "List a criterion here, and it could be long ")%><div>
          <div><%= HtmlLocalise("RecentPlacementsTd1-2", "Another criterion here")%><div>
          <div><%= HtmlLocalise("RecentPlacementsTd1-2", "More criteria, also a little long")%><div>
          <div><%= HtmlLocalise("RecentPlacementsTd1-2", "Another criterion here")%><div>
        </td>
        <td valign="top"><%= HtmlLocalise("RecentPlacementsTd1-2", "On, daily, HTML")%> <a href="#"><%= HtmlLocalise("RecentPlacementsTd1-2", "edit notifications")%></a></td>
      </tr>
      <tr>
        <td valign="top">
          <%= HtmlLocalise("RecentPlacementsTd1-1", "System Engineer - Level 1")%>
          <br />
          <i><%= HtmlLocalise("RecentPlacementsTd1-1", "reated April 9, 2011")%></i>
        </td>
        <td valign="top">
          <div><%= HtmlLocalise("RecentPlacementsTd1-2", "List a criterion here, and it could be long ")%><div>
          <div><%= HtmlLocalise("RecentPlacementsTd1-2", "Another criterion here")%><div>
          <div><%= HtmlLocalise("RecentPlacementsTd1-2", "More criteria, also a little long")%><div>
          <div><%= HtmlLocalise("RecentPlacementsTd1-2", "Another criterion here")%><div>
        </td>
        <td valign="top"><%= HtmlLocalise("RecentPlacementsTd1-2", "On, daily, HTML")%> <a href="#"><%= HtmlLocalise("RecentPlacementsTd1-2", "edit notifications")%></a></td>
      </tr>
      <tr>
        <td valign="top">
          <%= HtmlLocalise("RecentPlacementsTd1-1", "System Engineer - Level 1")%>
          <br />
          <i><%= HtmlLocalise("RecentPlacementsTd1-1", "reated April 9, 2011")%></i>
        </td>
        <td valign="top">
          <div><%= HtmlLocalise("RecentPlacementsTd1-2", "List a criterion here, and it could be long ")%><div>
          <div><%= HtmlLocalise("RecentPlacementsTd1-2", "Another criterion here")%><div>
          <div><%= HtmlLocalise("RecentPlacementsTd1-2", "More criteria, also a little long")%><div>
          <div><%= HtmlLocalise("RecentPlacementsTd1-2", "Another criterion here")%><div>
        </td>
        <td valign="top"><%= HtmlLocalise("RecentPlacementsTd1-2", "On, daily, HTML")%> <a href="#"><%= HtmlLocalise("RecentPlacementsTd1-2", "edit notifications")%></a></td>
      </tr>
    </tbody>
    </table>
  </td>
  </tr>
</table>


</asp:Content>
