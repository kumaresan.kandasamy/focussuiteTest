﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page111.aspx.cs" Inherits="Focus.Web.Designs.FA28Page111" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<table width="100%">
  <tr>
    <td colspan="2" valign="top">
      <p class="pageNav">
        <a href="#"><%= HtmlLocalise("JobSeekerReports", "Job seeker reports")%></a> | 
        <a href="#"><%= HtmlLocalise("JobOrderReports", "Job order reports")%></a> | 
        <a href="#"><%= HtmlLocalise("EmployerReports", "Employer reports")%></a> | 
        <a href="#"><%= HtmlLocalise("EEOReports", "EEO reports")%></a> | 
        <a href="#"><%= HtmlLocalise("ActivityReports", "Activity reports")%></a> | 
        <a href="#"><%= HtmlLocalise("Lookup", "Lookup")%></a> | 
        <strong><%= HtmlLocalise("SavedReports", "Saved reports")%></strong> | 
        <a href="#"><%= HtmlLocalise("LaborInsight", "Labor/Insight")%></a>
      </p>
    </td>
  </tr>
</table>

<table width="100%">
  <tr>
    <td colspan="3"><h1><%= HtmlLocalise("SavedReports", "Saved Reports")%></h1></td>
  </tr>
  <tr>
    <td width="18%" valign="top">
      <div class="subItemsPanel">
        <table width="100%">
          <tr>
            <td colspan="2"><h3><%= HtmlLocalise("ViewBy", "VIEW BY")%></h3></td>
          </tr>
          <tr>
            <td colspan="2">
              <asp:RadioButtonList ID="RadioButtonList1" runat="server" ClientIDMode="Static">
                <asp:ListItem Text="Most recent first" />
                <asp:ListItem Text="Alphabetically by title" />
                <asp:ListItem Text="Category" />
              </asp:RadioButtonList>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <asp:DropDownList runat="server" ID="DropDownList1" Width="200">
                  <asp:ListItem>Job seeker results</asp:ListItem>
              </asp:DropDownList>
            </td>
          </tr>
        </table>
      </div>
    </td>
    <td width="5%"></td>
    <td width="82%" valign="top">
      <asp:TextBox ID="SearchSavedReportsTextBox" runat="server" ClientIDMode="Static" TabIndex="4" MaxLength="100" Width="200" /><!--<= HtmlInFieldLabel("SearchSavedReportsTextBox", "SearchSavedReports.Label", "low", 30)%>-->
      <asp:Button ID="Button1" runat="server" Text="Search" class="button3" />
      <br /><br />
      <div class="HorizontalRule"></div>
      <div class="SavedReports">
        <div class="SavedReport">
          <img class="Thumbnail" src="/Assets/Images/SavedReportPanel.png" alt="Thumbnail" width="133" height="72">
          <img class="closeLogo" id="closeLogo" src="/Assets/Images/button_x_close_off.png" onclick="alert('close1')" alt="Main Logo" width="16" height="15">
          <span class="Date">12/14/10</span>
          <span class="Caption"><a href="#">Report title goes here and may be long</a></span>
        </div>
        <div class="SavedReport">
          <img class="Thumbnail" src="/Assets/Images/SavedReportPanel.png" alt="Thumbnail" width="133" height="72">
          <img class="closeLogo" id="Img1" src="/Assets/Images/button_x_close_off.png" onclick="alert('close1')" alt="Main Logo" width="16" height="15">
          <span class="Date">12/14/10</span>
          <span class="Caption"><a href="#">Report title goes here and may be long</a></span>
        </div>
        <div class="SavedReport">
          <img class="Thumbnail" src="/Assets/Images/SavedReportPanel.png" alt="Thumbnail" width="133" height="72">
          <img class="closeLogo" id="Img2" src="/Assets/Images/button_x_close_off.png" onclick="alert('close1')" alt="Main Logo" width="16" height="15">
          <span class="Date">12/14/10</span>
          <span class="Caption"><a href="#">Report title goes here and may be long</a></span>
        </div>
        <div class="SavedReport">
          <img class="Thumbnail" src="/Assets/Images/SavedReportPanel.png" alt="Thumbnail" width="133" height="72">
          <img class="closeLogo" id="Img3" src="/Assets/Images/button_x_close_off.png" onclick="alert('close1')" alt="Main Logo" width="16" height="15">
          <span class="Date">12/14/10</span>
          <span class="Caption"><a href="#">Report title goes here and may be long</a></span>
        </div>
        <div class="SavedReport">
          <img class="Thumbnail" src="/Assets/Images/SavedReportPanel.png" alt="Thumbnail" width="133" height="72">
          <img class="closeLogo" id="Img4" src="/Assets/Images/button_x_close_off.png" onclick="alert('close1')" alt="Main Logo" width="16" height="15">
          <span class="Date">12/14/10</span>
          <span class="Caption"><a href="#">Report title goes here and may be long</a></span>
        </div>
        <div class="SavedReport">
          <img class="Thumbnail" src="/Assets/Images/SavedReportPanel.png" alt="Thumbnail" width="133" height="72">
          <img class="closeLogo" id="Img5" src="/Assets/Images/button_x_close_off.png" onclick="alert('close1')" alt="Main Logo" width="16" height="15">
          <span class="Date">12/14/10</span>
          <span class="Caption"><a href="#">Report title goes here and may be long</a></span>
        </div>
        <div class="SavedReport">
          <img class="Thumbnail" src="/Assets/Images/SavedReportPanel.png" alt="Thumbnail" width="133" height="72">
          <img class="closeLogo" id="Img6" src="/Assets/Images/button_x_close_off.png" onclick="alert('close1')" alt="Main Logo" width="16" height="15">
          <span class="Date">12/14/10</span>
          <span class="Caption"><a href="#">Report title goes here and may be long</a></span>
        </div>
        <div class="SavedReport">
          <img class="Thumbnail" src="/Assets/Images/SavedReportPanel.png" alt="Thumbnail" width="133" height="72">
          <img class="closeLogo" id="Img7" src="/Assets/Images/button_x_close_off.png" onclick="alert('close1')" alt="Main Logo" width="16" height="15">
          <span class="Date">12/14/10</span>
          <span class="Caption"><a href="#">Report title goes here and may be long</a></span>
        </div>
        <div class="SavedReport">
          <img class="Thumbnail" src="/Assets/Images/SavedReportPanel.png" alt="Thumbnail" width="133" height="72">
          <img class="closeLogo" id="Img8" src="/Assets/Images/button_x_close_off.png" onclick="alert('close1')" alt="Main Logo" width="16" height="15">
          <span class="Date">12/14/10</span>
          <span class="Caption"><a href="#">Report title goes here and may be long</a></span>
        </div>
        <div class="SavedReport">
          <img class="Thumbnail" src="/Assets/Images/SavedReportPanel.png" alt="Thumbnail" width="133" height="72">
          <img class="closeLogo" id="Img9" src="/Assets/Images/button_x_close_off.png" onclick="alert('close1')" alt="Main Logo" width="16" height="15">
          <span class="Date">12/14/10</span>
          <span class="Caption"><a href="#">Report title goes here and may be long</a></span>
        </div>
      </div>
    </td>
  </tr>
</table>


</asp:Content>
