﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page056.aspx.cs" Inherits="Focus.Web.Designs.FA28Page056" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<table width="100%">
    <tr>
      <td>
        <p class="pageNav">
          <a href="#"><%= HtmlLocalise("JobOrderDashboard", "Job order dashboard")%></a> | 
          <strong><%= HtmlLocalise("JobDevelopment", "Job development")%></strong> | 
          <a href="#"><%= HtmlLocalise("FindEmployer", "Find employer")%></a> | 
          <a href="#"><%= HtmlLocalise("SendMessages", "Send messages")%></a>
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%">
          <tr>
            <td>
            <a href="#"><%= HtmlLocalise("TopLink1", "return to Job Development")%></a>
            </td>
            <td><asp:Button ID="Button1" runat="server" Text="Log contact" class="button1 right" /></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>
        <table width="500px">
          <tr>
            <td colspan="2"><h1>Contact employer about Mickey Smith</h1></td>
          </tr>
          <tr>
            <td width="95px">Employer</td>
            <td>Nifty Networks</td>
          </tr>
          <tr>
            <td>Hiring manager</td>
            <td>
              <asp:DropDownList ID="DropDownList7" runat="server">
                <asp:ListItem Text="Harold Saxon" Value="Harold Saxon" />
              </asp:DropDownList>
            </td>
          </tr>
          <tr>
            <td valign="top">Contact</td>
            <td>
              <asp:RadioButton Text="telephone number" runat="server" GroupName="Contact" /> <i>displayed after selecting this option</i>
              <br />
              <asp:RadioButton ID="RadioButton1" Text="email" runat="server" GroupName="Contact" />
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <div style="overflow:scroll;height:150px;">
                <div>Dear firstName lastName,</div>
                <br />
                <div>We recently received a resume for Mickey Smith, who is very similar to your 
                current employee Rickey Smith. A copy of Mickey Smith's resume is attached 
                to this email for your reference.</div>
                <br />
                <div>Regards,</div>
                <br />
                <div>contactFirst contactLast</div>
              </div>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <asp:CheckBoxList runat="server">
                <asp:ListItem Text="attach resume" />
                <asp:ListItem Text="anonymize resume" />
              </asp:CheckBoxList>
            </td>
          </tr>
          <tr>
            <td colspan="2">You may also <a href="#" id="addNote">add a note</a> about this contact.</td>
          </tr>
          <tr class="addNoteContent">
            <td colspan="2">
              <%= HtmlInFieldLabel("AddNoteTextBox", "Language.InlineLabel", "Type note here.", 488)%>
              <asp:TextBox ID="AddNoteTextBox" runat="server" Width="488" ClientIDMode="Static" AutoCompleteType="Disabled" TextMode="MultiLine" Rows="4" />
            </td>
          </tr>
          <tr class="addNoteContent">
            <td colspan="2"><asp:Button ID="Button2" runat="server" Text="Add note" class="button2" /></td>
          </tr>
        </table>
      </td>
    </tr>
</table>

</asp:Content>
