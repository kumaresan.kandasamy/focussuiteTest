﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page071.aspx.cs" Inherits="Focus.Web.Designs.FA28Page071" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<p class="pageNav">
  <a href="<%= UrlBuilder.JobSeekerReferrals() %>"><%= HtmlLocalise("ReturnToList.LinkText", "Return to main page")%></a>
</p>

<table width="100%">
  <tr>
			<td width="50%"><h1><%= HtmlLocalise("JobSeekerReferral.Header", "Approve new employer account request: Nifty Networks")%>&nbsp;<asp:Literal ID="CandidateNameLiteral" runat="server" /></h1></td>
			<td width="50%">
				<table class="right">
					<tr>
						<td><asp:button ID="DenyButton" runat="server" CssClass="button1" Text="Place on hold" /></td>
						<td><asp:button ID="Button1" runat="server" CssClass="button1" Text="Deny employer" /></td>
						<td><asp:button ID="ApproveButton" runat="server" CssClass="button5 right" Text="Approve employer" /></td>
					</tr>
				</table>
			</td>
		</tr>
</table>

<table width="100%">
  <tr>
    <td>
      <div>Nifty Networks</div>
      <div>742 Evergreen Terrace</div>
      <div>Springfield, KY 12345</div>
      <div>Phone number: 555-555-1212</div>
      <div>Fax number: 555-555-1234</div>
      <div>URL: http://www.niftynetworks.com</div>
      <div>FEIN #: 12-3456789</div>
      <div>SEIN #: 00-00000</div>
      <div>Industry classification:  Application software programming services, custom </div>
      <div>computer (541511)</div>
      <div>Contact:</div>
      <div>Harold Saxon</div>
      <div>Nifty Networks</div>
      <div>742 Evergreen Terrace</div>
      <div>Springfield, KY 12345</div>
      <div>Email: h.saxon@niftynetworks.com</div>
      <div>Phone number: 555-555-0000</div>
      <div>Fax number: 555-555-1234</div>
      <br />
      <asp:button ID="Button3" runat="server" CssClass="button1" Text="View / add notes and reminders" style="margin-left:0" />
    </td>
    <td valign="top" align="right">
      <div class="sidePanel" style="width:300px">
        <h3>Pending postings</h3>
        <ul>
          <li><a href="#">Job posting title</a></li>
          <li><a href="#">Job posting title</a></li>
        </ul>
      </div>
    </td>
  </tr>
</table>

</asp:Content>
