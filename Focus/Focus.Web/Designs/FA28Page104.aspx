﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page104.aspx.cs" Inherits="Focus.Web.Designs.FA28Page104" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<table width="100%">
    <tr>
      <td>
        <p class="pageNav">
          <a href="#"><%= HtmlLocalise("JobOrderDashboard", "Job seeker reports")%></a> | 
          <strong><%= HtmlLocalise("JobDevelopment", "Job order reports")%></strong> | 
          <a href="#"><%= HtmlLocalise("FindEmployer", "Employer reports")%></a> | 
          <a href="#"><%= HtmlLocalise("SendMessages", "EEO reports")%></a>
          <a href="#"><%= HtmlLocalise("FindEmployer", "Staff activity reports")%></a> | 
          <a href="#"><%= HtmlLocalise("SendMessages", "Lookup")%></a>
          <a href="#"><%= HtmlLocalise("FindEmployer", "Saved reports")%></a> | 
          <a href="#"><%= HtmlLocalise("SendMessages", "Labor/Insight")%></a>
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%">
          <tr>
            <td><a href="#"><%= HtmlLocalise("TopLink1", "return to report")%></a></td>
            <td align="right">
              <asp:DropDownList ID="DropDownList91" runat="server">
                <asp:ListItem Text="Export" />
              </asp:DropDownList>&nbsp;&nbsp;
              <asp:Button ID="Button1" runat="server" Text="print" class="button1 right" />
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td><h1><%= HtmlLocalise("SendMessages", "Staff profile: Firstname Lastname")%></h1></td>
    </tr>
    <tr>
      <td>
        <table class="accordionTable">
          <tr class="multipleAccordionTitle">
            <td><span id="namedAnchor1" class="accordionArrow"></span>Contact information</td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Nifty Networks")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "742 Evergreen Terrace")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Springfield, KY 12345")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Phone number: 555-555-1212")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Fax number: 555-555-1234")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Nifty Networks")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Another Address")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Another Location")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Phone number: 555-555-1385")%></td>
              </tr>
              <tr>
                <td colspan="2"><%= HtmlLocalise("SendMessages", "Fax number: 555-382-3828")%></td>
              </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle">
            <td><span id="namedAnchor2" class="accordionArrow"></span><%=HtmlLocalise("Reporting.Results2", "Logins")%></td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table width="100%">
                <tr>
                  <td colspan="4">Number of Focus/Assist logins for all employer hiring managers in past 7 days: 12</td>
                </tr>
                <tr>
                  <td colspan="4">Time since last login: 42 minutes (<a href="#">hsaxon@niftynetworks.com</a>)</td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle">
            <td><span id="Span2" class="accordionArrow"></span><%=HtmlLocalise("Reporting.Results2", "Activity summary")%></td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table>
                <tr>
                  <td colspan="2">
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                      <asp:ListItem Text="Last 7 days" />
                      <asp:ListItem Text="Last 30 days" />
                    </asp:RadioButtonList>
                  </td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Number of job orders open: 15")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Number of job orders edited by staff: 2")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Number of resumes referred: 56")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Number of job seekers interviewed: 15")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Number of job seekers rejected: 14")%></td>
                </tr>
                <tr>
                  <td colspan="2"><%= HtmlLocalise("SendMessages", "Number of job seekers hired: 1")%></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle">
            <td><span id="Span1" class="accordionArrow"></span><%=HtmlLocalise("Reporting.Results2", "Activity log")%></td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table width="100%">
                <tr>
                <td width="100"><%=HtmlLocalise("Reporting.Results2", "Show results for")%></td>
                <td width="107">
                  <asp:DropDownList ID="DropDownList23" runat="server">
                    <asp:ListItem Text="last 30 days" />
                  </asp:DropDownList>
                </td>
                <td width="15"><%=HtmlLocalise("Reporting.Results2", "in")%></td>
                <td width="107">
                  <asp:DropDownList ID="DropDownList24" runat="server">
                    <asp:ListItem Text="descending" />
                  </asp:DropDownList>
                </td>
                <td width="50"><%=HtmlLocalise("Reporting.Results2", "order")%></td>
                <td><a href="#"><%=HtmlLocalise("Reporting.Results2", "Download this log (CSV file)")%></a></td>
                <td align="right">
                  <table class="Pager">
                    <tr>
                      <td>
                        <asp:Label runat="server" ID="Label3"></asp:Label>
                        <%=HtmlLocalise("Reporting.Results2", "result(s) found")%> &nbsp;
                      </td>
                      <td id="td5" runat="server">
                        <a href="#" id="A5">
                          <img id="Img15" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                            width="15" height="11" /></a>
                      </td>
                      <td id="td6" runat="server">
                        <a href="#" id="A6">
                          <img id="Img16" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                            height="11" /></a>
                      </td>
                      <td>
                        PAGE
                      </td>
                      <td>
                        <asp:DropDownList ID="DropDownList4" runat="server">
                        </asp:DropDownList>
                      </td>
                      <td>
                        <%=HtmlLocalise("Reporting.Of2", "of") %>
                        <asp:Label runat="server" ID="Label4" />
                      </td>
                      <td id="td7" runat="server">
                        <a href="#" id="A7">
                          <img id="Img19" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                            width="6" height="11" /></a>
                      </td>
                      <td id="td8" runat="server">
                        <a href="#" id="A8">
                          <img id="Img20" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                            width="15" height="11" /></a>
                      </td>
                      <td style="padding-left: 15px">
                        <asp:DropDownList ID="DropDownList7" runat="server">
                          <asp:ListItem Text="10" Value="10" />
                          <asp:ListItem Text="20" Value="20" />
                          <asp:ListItem Text="50" Value="50" />
                          <asp:ListItem Text="100" Value="100" />
                        </asp:DropDownList>
                      </td>
                      <td>
                        RESULTS PER PAGE
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              </table>
              <table width="100%">
                <tr>
                  <td colspan="4">
                    <table width="100%" class="DataTable">
                      <thead>
                        <tr>
                          <th width="25%">
                            <%= HtmlLocalise("RecentPlacementsTH21", "ACTION DATE/TIME")%>
                            <img id="Img21" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img22" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH22", "USERNAME")%>
                            <img id="Img23" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img24" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH24", "ACTION")%>
                            <img id="Img25" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img26" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "3/13/12 6:55")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "dev@employer.com")%></a></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "SaveJob")%></td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "3/13/12 6:55")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "dev@employer.com")%></a></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "SaveJob")%></td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "3/13/12 6:55")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "dev@employer.com")%></a></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "SaveJob")%></td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "3/13/12 6:55")%></td>
                          <td valign="top"><a href="#"><%= HtmlLocalise("Name31", "dev@employer.com")%></a></td>
                          <td valign="top"><%= HtmlLocalise("Action24", "SaveJob")%></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle">
            <td><span id="Span3" class="accordionArrow"></span><%=HtmlLocalise("Reporting.Results2", "Employers assisted")%></td>
          </tr>
          <tr class="accordionContent">
            <td>
              <table width="100%">
                <tr>
                <td width="100"><%=HtmlLocalise("Reporting.Results2", "Show results for")%></td>
                <td width="107">
                  <asp:DropDownList ID="DropDownList1" runat="server">
                    <asp:ListItem Text="last 30 days" />
                  </asp:DropDownList>
                </td>
                <td align="right">
                  <table class="Pager">
                    <tr>
                      <td>
                        <asp:Label runat="server" ID="Label1"></asp:Label>
                        <%=HtmlLocalise("Reporting.Results2", "result(s) found")%> &nbsp;
                      </td>
                      <td id="td1" runat="server">
                        <a href="#" id="A1">
                          <img id="Img1" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                            width="15" height="11" /></a>
                      </td>
                      <td id="td2" runat="server">
                        <a href="#" id="A2">
                          <img id="Img2" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                            height="11" /></a>
                      </td>
                      <td>
                        PAGE
                      </td>
                      <td>
                        <asp:DropDownList ID="DropDownList3" runat="server">
                        </asp:DropDownList>
                      </td>
                      <td>
                        <%=HtmlLocalise("Reporting.Of2", "of") %>
                        <asp:Label runat="server" ID="Label2" />
                      </td>
                      <td id="td3" runat="server">
                        <a href="#" id="A3">
                          <img id="Img3" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                            width="6" height="11" /></a>
                      </td>
                      <td id="td4" runat="server">
                        <a href="#" id="A4">
                          <img id="Img4" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                            width="15" height="11" /></a>
                      </td>
                      <td style="padding-left: 15px">
                        <asp:DropDownList ID="DropDownList5" runat="server">
                          <asp:ListItem Text="10" Value="10" />
                          <asp:ListItem Text="20" Value="20" />
                          <asp:ListItem Text="50" Value="50" />
                          <asp:ListItem Text="100" Value="100" />
                        </asp:DropDownList>
                      </td>
                      <td>
                        RESULTS PER PAGE
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              </table>
              <table width="100%">
                <tr>
                  <td colspan="4">
                    <table width="100%" class="DataTable">
                      <thead>
                        <tr>
                          <th width="25%">
                            <%= HtmlLocalise("RecentPlacementsTH21", "NAME")%>
                            <img id="Img5" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img6" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH22", "DATE")%>
                            <img id="Img7" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img8" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH24", "SERVICE")%>
                            <img id="Img9" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img10" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "ACME Networks")%></td>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "May 15, 2011")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", " Posted job order")%></td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "ACME Networks")%></td>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "May 15, 2011")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", " Posted job order")%></td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "ACME Networks")%></td>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "May 15, 2011")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", " Posted job order")%></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="multipleAccordionTitle on">
            <td><span id="Span4" class="accordionArrow"></span><%=HtmlLocalise("Reporting.Results2", "Job seekers assisted")%></td>
          </tr>
          <tr class="accordionContent open">
            <td>
              <table width="100%">
                <tr>
                <td width="100"><%=HtmlLocalise("Reporting.Results2", "Show results for")%></td>
                <td width="107">
                  <asp:DropDownList ID="DropDownList2" runat="server">
                    <asp:ListItem Text="last 30 days" />
                  </asp:DropDownList>
                </td>
                <td align="right">
                  <table class="Pager">
                    <tr>
                      <td>
                        <asp:Label runat="server" ID="Label5"></asp:Label>
                        <%=HtmlLocalise("Reporting.Results2", "result(s) found")%> &nbsp;
                      </td>
                      <td id="td9" runat="server">
                        <a href="#" id="A9">
                          <img id="Img11" src="/Assets/Images/fa_arrow_leftend_off.png" alt="Pager Start"
                            width="15" height="11" /></a>
                      </td>
                      <td id="td10" runat="server">
                        <a href="#" id="A10">
                          <img id="Img12" src="/Assets/Images/fa_arrow_left_off.png" alt="Pager Back" width="6"
                            height="11" /></a>
                      </td>
                      <td>
                        PAGE
                      </td>
                      <td>
                        <asp:DropDownList ID="DropDownList6" runat="server">
                        </asp:DropDownList>
                      </td>
                      <td>
                        <%=HtmlLocalise("Reporting.Of2", "of") %>
                        <asp:Label runat="server" ID="Label6" />
                      </td>
                      <td id="td11" runat="server">
                        <a href="#" id="A11">
                          <img id="Img13" src="/Assets/Images/fa_arrow_right_off.png" alt="Pager Next"
                            width="6" height="11" /></a>
                      </td>
                      <td id="td12" runat="server">
                        <a href="#" id="A12">
                          <img id="Img14" src="/Assets/Images/fa_arrow_rightend_off.png" alt="Pager End"
                            width="15" height="11" /></a>
                      </td>
                      <td style="padding-left: 15px">
                        <asp:DropDownList ID="DropDownList8" runat="server">
                          <asp:ListItem Text="10" Value="10" />
                          <asp:ListItem Text="20" Value="20" />
                          <asp:ListItem Text="50" Value="50" />
                          <asp:ListItem Text="100" Value="100" />
                        </asp:DropDownList>
                      </td>
                      <td>
                        RESULTS PER PAGE
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              </table>
              <table width="100%">
                <tr>
                  <td colspan="4">
                    <table width="100%" class="DataTable">
                      <thead>
                        <tr>
                          <th width="25%">
                            <%= HtmlLocalise("RecentPlacementsTH21", "NAME")%>
                            <img id="Img17" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img18" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH22", "DATE")%>
                            <img id="Img27" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img28" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                          <th width="15%">
                            <%= HtmlLocalise("RecentPlacementsTH24", "SERVICE")%>
                            <img id="Img29" class="UpArrow" src="/Assets/Images/arrow_sort_up.png" alt="Up" width="12" height="6" />
                            <img id="Img30" class="DownArrow" src="/Assets/Images/arrow_sort_down.png" alt="Down" width="12" height="6" />
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "ACME Networks")%></td>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "May 15, 2011")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", " Posted job order")%></td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "ACME Networks")%></td>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "May 15, 2011")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", " Posted job order")%></td>
                        </tr>
                        <tr>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "ACME Networks")%></td>
                          <td valign="top"><%= HtmlLocalise("RecentPlacementsTd2-2", "May 15, 2011")%></td>
                          <td valign="top"><%= HtmlLocalise("Action24", " Posted job order")%></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
</table>

</asp:Content>