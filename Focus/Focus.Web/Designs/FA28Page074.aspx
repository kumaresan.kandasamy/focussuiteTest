﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page074.aspx.cs" Inherits="Focus.Web.Designs.FA28Page074" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<asp:Panel ID="ModalPanel" runat="server" CssClass="modal" Style="display:block;width:500px;">

<table width="100%">
    <tr>
      <td><h1><%= HtmlLocalise("Notification", "Notification of Denied Employer Account Request")%></h1></td>
    </tr>
    <tr>
      <td><%= HtmlLocalise("JobOrderDashboard", "Notify the employer their account request has been denied.")%></td>
    </tr>
    <tr>
      <td>
        <asp:DropDownList runat="server" ID="DropDownList6" Width="300">
          <asp:ListItem>Default message</asp:ListItem>
        </asp:DropDownList>
      </td>
    </tr>
    <tr>
      <td>
        <div>Dear ContactfirstName ContactlastName,<br /></div>
        <div>We have reviewed your request to register on Focus/Talent. Unfortunately, we cannot approve your request for the following reasons:</div>
      </td>
    </tr>
    <tr>
      <td>
        <%= HtmlInFieldLabel("EmployerNameTextBox", "Language.InlineLabel", "Type reasons for denial", 496)%>
        <asp:TextBox ID="EmployerNameTextBox" runat="server" Width="100%" ClientIDMode="Static" AutoCompleteType="Disabled" TextMode="MultiLine" Rows="4" />
      </td>
    </tr>
    <tr>
      <td><div>If you have any questions, please feel free to contact me at 617-555-1212 or email@example.com.</div><br />
            <div>Regards,</div><br />
            <div>Userfirstname Userlastname</div>
      </td>
    </tr>
    <tr>
      <td><asp:CheckBox id="CheckBox1" Text="email me a copy of this message" TextAlign="Right" runat="server" /></td>
    </tr>
    <tr>
      <td>
        <asp:Button ID="Button1" runat="server" Text="Cancel" class="button4" />
        <asp:Button ID="Button2" runat="server" Text="Email employer" class="button3" />
      </td>
    </tr>
</table>

</asp:Panel>


</asp:Content>
