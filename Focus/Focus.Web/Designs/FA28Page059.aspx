﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FA28Page059.aspx.cs" Inherits="Focus.Web.Designs.FA28Page059" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<asp:Panel ID="ModalPanel" runat="server" CssClass="modal" Style="display:block;width:500px;">
	<table width="100%">
    <tr>
      <td><h1><%= HtmlLocalise("JobOrderDashboard", "Add or log contact")%></h1></td>
      <td><img src="/Assets/images/button_x_close_off.png" onclick="alert('close')" width="16" height="15" alt="" style="float:right;position:relative;left:13px;bottom:18px;" /></td>
    </tr>
    <tr>
      <td colspan="2">
        <div><asp:RadioButton ID="AddOrLogRadio1" ClientIDMode="Static" Text="add contact" runat="server" GroupName="AddOrLog" /></div>
        <div><asp:RadioButton ID="AddOrLogRadio2" ClientIDMode="Static" Text="log contact, but do not save personal information" runat="server" GroupName="AddOrLog" /></div>
      </td>
    </tr>
    <tr>
      <td colspan="2"><i><%= HtmlLocalise("JobOrderDashboard", "*required fields")%></i></td>
    </tr>
    <tr>
      <td><%= HtmlLocalise("JobOrderDashboard", "First name*")%></td>
      <td><asp:TextBox runat="server" Width="200px" /></td>
    </tr>
    <tr>
      <td>Last name*</td>
      <td><asp:TextBox ID="TextBox1" runat="server" Width="200px" /></td>
    </tr>
    <tr>
      <td>Middle initial</td>
      <td><asp:TextBox ID="TextBox2" runat="server" Width="20px" /></td>
    </tr>
    <tr>
      <td>Personal title*</td>
      <td>
        <asp:DropDownList ID="ddlPageSizes" runat="server">
          <asp:ListItem Text="- select title -" Value="100" />
        </asp:DropDownList>
      </td>
    </tr>
    <tr>
      <td>Job title</td>
      <td><asp:TextBox ID="TextBox3" runat="server" Width="200px" /></td>
    </tr>
    <tr>
      <td>City*</td>
      <td><asp:TextBox ID="TextBox4" runat="server" Width="200px" /></td>
    </tr>
    <tr>
      <td>State*</td>
      <td>
        <asp:DropDownList ID="DropDownList1" runat="server">
          <asp:ListItem Text="- select state -" Value="100" />
        </asp:DropDownList>
      </td>
    </tr>
    <tr>
      <td>County*</td>
      <td><asp:TextBox ID="TextBox5" runat="server" Width="200px" /></td>
    </tr>
    <tr>
      <td>ZIP or postal code*</td>
      <td><asp:TextBox ID="TextBox6" runat="server" Width="200px" /></td>
    </tr>
    <tr>
      <td>Country*</td>
      <td>
        <asp:DropDownList ID="DropDownList2" runat="server" Width="130">
          <asp:ListItem Text="- United States -" Value="100" />
        </asp:DropDownList>
      </td>
    </tr>
    <tr>
      <td>Phone number*</td>
      <td>
        <table>
          <tr>
            <td width="140"><asp:TextBox ID="TextBox7" runat="server" Width="130px" /></td>
            <td width="70">
              <%= HtmlInFieldLabel("ExtensionTextBox", "Language.InlineLabel", "extension", 60)%>
              <asp:TextBox ID="ExtensionTextBox" runat="server" Width="60" ClientIDMode="Static" AutoCompleteType="Disabled" />
            </td>
            <td>
              <asp:DropDownList ID="DropDownList3" runat="server" Width="100">
                <asp:ListItem Text="landline" Value="100" />
              </asp:DropDownList>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>Alternate phone number 1</td>
      <td>
        <table>
          <tr>
            <td width="140"><asp:TextBox ID="TextBox8" runat="server" Width="130px" /></td>
            <td>
              <asp:DropDownList ID="DropDownList4" runat="server" Width="100">
                <asp:ListItem Text="fax" Value="100" />
              </asp:DropDownList>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>Alternate phone number 2</td>
      <td>
        <table>
          <tr>
            <td width="140"><asp:TextBox ID="TextBox9" runat="server" Width="130px" /></td>
            <td>
              <asp:DropDownList ID="DropDownList5" runat="server" Width="100">
                <asp:ListItem Text="landline" Value="100" />
              </asp:DropDownList>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>Email address*</td>
      <td><asp:TextBox ID="TextBox10" runat="server" Width="200px" /></td>
    </tr>
    <tr>
      <td valign="top">Areas of hiring responsibility</td>
      <td><asp:TextBox ID="TextBox11" runat="server" Width="200px" TextMode="MultiLine" Rows="3" /></td>
    </tr>
    <tr id="addContactContent">
      <td colspan="2"><asp:CheckBox Text="Create Focus/Talent account for this contact" runat="server" /></td>
    </tr>
    <tr id="logContactContent">
      <td valign="top">Add note</td>
      <td><asp:TextBox ID="TextBox12" runat="server" Width="200px" TextMode="MultiLine" Rows="3" /></td>
    </tr>
  </table>
</asp:Panel>

</asp:Content>
