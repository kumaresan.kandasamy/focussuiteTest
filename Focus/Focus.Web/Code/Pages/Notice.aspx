﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Notice.aspx.cs" Inherits="Focus.Web.Code.Pages.Notice" %>
<%@ Import Namespace="Focus.Core.Models.Assist" %>
<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>

<%@ Register Src="~/Code/Controls/User/EmailDocumentModal.ascx" TagName="EmailDocumentModal" TagPrefix="uc" %>

<asp:Content ID="NoticeContainer" ContentPlaceHolderID="MainContent" runat="server">

	<h1><focus:LocalisedLabel runat="server" ID="PageTitleLabel" DefaultText="Notices"/></h1>
  <div style="display: block;">
    <asp:Repeater ID="DocumentGroupRepeater" runat="server" OnItemDataBound="DocumentGroupRepeater_ItemDataBound">
      <ItemTemplate>
        <div class="staticHeader"><asp:Label runat="server" ID="GroupLabel"></asp:Label></div>
        <span style="display: block;">
          <asp:Repeater ID="DocumentRepeater" runat="server" OnItemDataBound="DocumentRepeater_ItemDataBound" OnItemCommand="DocumentRepeater_ItemCommand">
            <ItemTemplate>
              <div style="display: block; padding-bottom: 10px;">
                <div style="display: block; font-weight: bold;">
                  <%# ((DocumentDto)Container.DataItem).Title%>
                  <asp:ImageButton ID="DownloadDocumentImageButton" runat="server"  ImageUrl="<%# UrlBuilder.DownloadIcon() %>" CommandArgument="<%#((DocumentDto)Container.DataItem).Id %>" CommandName="DownloadDocument"/>
                  <asp:ImageButton ID="EmailDocumentImageButton" runat="server" ImageUrl="<%# UrlBuilder.EmailIcon() %>" CommandArgument="<%#((DocumentDto)Container.DataItem).Id %>" CommandName="EmailDocument"/>
                </div>
                <div style="display: block;"><%# ((DocumentDto)Container.DataItem).Description%>	</div>
              </div>
            </ItemTemplate>
          </asp:Repeater>
        </span>
      </ItemTemplate>
    </asp:Repeater>
  </div>

<uc:EmailDocumentModal ID="EmailDocumentModal"	runat="server" />

  <script type="text/javascript">
    // Remove navigation and login controls
    $(window).load(
      HideControls()
    );

    function HideControls() {
      $("#LoggedInControls").hide();
      $("#HeadLoginView_speechbubblepanel").hide();
    }

    // Close the window. The open method is used to close the window without a prompt
    function Close() {
      window.open('', '_self', '');
      window.close();
    }

  </script>

</asp:Content>