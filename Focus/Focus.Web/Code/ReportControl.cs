﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

#endregion

namespace Focus.Web.Code
{
  public class ReportControl : UserControlBase
  {
    /// <summary>
    /// Builds the office field to cope with multiple offices
    /// </summary>
    /// <param name="officeList">The office names (a string delimited by ||)</param>
    /// <param name="listViewItem">The current item from the list view control.</param>
    protected void BuildOfficeField(string officeList, ListViewItem listViewItem)
    {
      var offices = officeList.Split(new [] { "||" }, StringSplitOptions.RemoveEmptyEntries);

      var officesLiteral = (Literal)listViewItem.FindControl("OfficeLiteral");
      var showAdditionalOfficesLink = (HyperLink)listViewItem.FindControl("ShowAdditionalOfficesLink");
      if (offices.Length < 4)
      {
        officesLiteral.Text = string.Join(", ", offices);
      }
      else
      {
        officesLiteral.Text = string.Concat(string.Join(", ", offices.Take(2)), ",");
        var additionalOfficesLiteral = (Literal)listViewItem.FindControl("AdditionalOfficesLiteral");
        additionalOfficesLiteral.Text = string.Join(", ", offices.Skip(2));

        showAdditionalOfficesLink.Visible = true;
        showAdditionalOfficesLink.Text = CodeLocalise("AdditionalOfficesLink.Text", "+ {0} more", (offices.Length - 2));
      }
    }
  }
}