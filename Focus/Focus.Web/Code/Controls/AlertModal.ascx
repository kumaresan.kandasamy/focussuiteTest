﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AlertModal.ascx.cs" Inherits="Focus.Web.Code.Controls.AlertModal" %>

<asp:HiddenField ID="AlertModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="AlertModalPopup" runat="server" ClientIDMode="Static"
												TargetControlID="AlertModalDummyTarget"
												PopupControlID="AlertModalPanel"
												PopupDragHandleControlID="AlertModalPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground">
	<Animations>
		<OnShown>
			<ScriptAction Script="setButtonFocus('#btnAlertModalClose');" /> 
		</OnShown>
	</Animations>
</act:ModalPopupExtender>

<asp:Panel runat="server" ID="AlertModalPanel" CssClass="modal" style="display:none;" ClientIDMode="Static">
	<asp:Panel runat="server" ID="pnlAlertModalInner">
		<div class="modalHeader">
			<div class="closeIcon" id="closeIcon" runat="server"><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('EmailDocumentModal').hide(); return false;" /></div>
      <h2><focus:LocalisedLabel runat="server" ID="AlertModalTitleLabel" LocalisationKey="AlertModalTitleLabel.Label" DefaultText="Error" CssClass="modalTitle modalTitleLarge" role="heading" aria-level="4"/></h2>
		</div>
		<asp:Label runat="server" ID="lblAlertModalMessage" CssClass="modalMessage"></asp:Label>
		<div style="display: block; padding-top: 20px"><asp:Button ID="btnAlertModalClose" runat="server" SkinID="Button1" ClientIDMode="Static" /></div>
	</asp:Panel>
</asp:Panel>
