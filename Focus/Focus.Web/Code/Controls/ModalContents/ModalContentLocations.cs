﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

namespace Focus.Web.Code.Controls.ModalContents
{
	public static class ModalContentLocations
	{
		public static string JobPreview { get { return GetRelativePath("JobPreviewModalContent.ascx"); } }

		public static string CriminalRecordExclusion { get { return GetRelativePath("CriminalRecordExclusionModalContent.ascx"); } }

		public static string EditPostingReferralConfirmationModal { get { return GetRelativePath( "EditPostingReferralConfirmationContent.ascx" ); } }

		private static string GetRelativePath(string filename)
		{
			return "~/Code/Controls/ModalContents/" + filename;
		}
	}
}