﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobPreviewModalContent.ascx.cs" Inherits="Focus.Web.Code.Controls.ModalContents.JobPreviewModalContact" %>

<div style="float:left;height: 500px;max-height: 500px">
	<asp:Panel ID="JobPreviewModalHeading" runat="server" ClientIDMode="Static" CssClass="modal-header">
		<h1 data-modal="title"><%= HtmlLocalise("Preview.Title", "Preview #POSTINGTYPE# listing")%></h1>
		<p><%= HtmlLocalise("Preview.Description", "#CANDIDATETYPE# will see your listing as follows:")%></p>
  </asp:Panel>
  <table width="550px">
		<tr>
			<td style="border-style:solid; border-width: 1px; padding: 2px;">
				<div style="overflow:scroll;height:400px;" data-modal="text" id="JobPreviewModalDiv" clientidmode="Static">
					<asp:Literal ID="PostingPreviewLiteral" runat="server" />
				</div>
			</td>
		</tr>
		<tr>
			<td style="text-align: left;" align="left">
				<asp:Button ID="PostJobButton" runat="server" SkinID="Button3" CausesValidation="True" Visible="False" OnClick="PostJobClick" />
				<asp:Button ID="NotifyEmployerButton" runat="server" SkinID="Button3" CausesValidation="true" Visible="False" OnClick="NotifyEmployerButton_Click" />
				<asp:Button ID="ReturnToJobButton" runat="server" SkinID="Button3" CausesValidation="true" Visible="False" UseSubmitBehavior="False"/>
			</td>
		</tr>
		<tr>
			<td style="text-align: left;" align="left">
				<asp:Button ID="PrintJobButton" runat="server" SkinID="Button3" CausesValidation="true" Visible="False" UseSubmitBehavior="False" />
			</td>
		</tr>
	</table>
</div>
<div class="closeIcon"><input type="image" src="<%= UrlBuilder.CloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('BddModalPopupBehavior').hide();return false;" /></div>
