﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ConfirmationModalContent.ascx.cs"
	Inherits="Focus.Web.Code.Controls.ModalContents.ConfirmationModalContent" %>

<table style="width:<%= Width %>px">
	<tr>
   <th style="vertical-align: top;">
	   <asp:Literal ID="Title" runat="server"></asp:Literal>
   </th>
  </tr>
  <tr>
   <td style="vertical-align: top; height:<%= Height %>px">
	   <asp:Literal ID="Details" runat="server"></asp:Literal>
   </td>
  </tr>
	<tr>
		<td style="text-align:left;">
		<asp:Button ID="OkButton" runat="server" SkinID="Button1" CausesValidation="false" OnCommand="OkButtonClick"/>
		<asp:Literal ID="OkButtonSpacer" runat="server" EnableViewState="false" Text="  " />
		<asp:Button ID="CancelButton" runat="server" SkinID="Button1" CausesValidation="false" OnCommand="CancelButtonClick"/>
		</td>
	</tr>
</table>