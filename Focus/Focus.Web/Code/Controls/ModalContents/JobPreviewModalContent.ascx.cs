﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Common.Code;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Controllers.Code.Controls.ModalContents;
using Framework.Core;
using Focus.Core;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Services.Core;

#endregion

namespace Focus.Web.Code.Controls.ModalContents
{
	public partial class JobPreviewModalContact : BddModalContentBase
	{

		#region Properties

		/// <summary>
		/// Gets the page controller.
		/// </summary>
		/// <value>
		/// The page controller.
		/// </value>
		protected JobPreviewModalContentController PageController
		{
			get { return PageControllerBaseProperty as JobPreviewModalContentController; }
		}

		/// <summary>
		/// Registers the page controller.
		/// </summary>
		/// <returns></returns>
		public override IPageController RegisterPageController()
		{
			return new JobPreviewModalContentController(App);
		}

		protected bool? ShowPrint
		{
			get { return GetProperty<bool?>("ShowPrint"); }
		}

		protected bool? FromApproval
		{
			get { return GetProperty<bool?>("FromApproval"); }
		}

		protected bool? AllowPost
		{
			get { return GetProperty<bool?>("AllowPost"); }
		}

		protected JobDto Job
		{
			get { return GetProperty<JobDto>("Job"); }
		}

		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
            /** KRP 15.May.2017 - FVN-4623
             * When different user logs in then the session will be cleared. 
             * As a result the Job property will be null. So lets redirect to job order dash page
             * AIM - FVN-4621-Talent:simultaneous editing jobs results in error
            */
            if (Job.IsNull())
            {
                Response.Redirect(UrlBuilder.TalentJobs());
            }
 
			PostingPreviewLiteral.Text = Utilities.InsertPostingFooter(Job.PostingHtml, Job.CriminalBackgroundExclusionRequired, Job.CreditCheckRequired);
			PostJobButton.Visible = AllowPost ?? false;
			PrintJobButton.Visible = ShowPrint ?? false;
			NotifyEmployerButton.Visible = ReturnToJobButton.Visible = FromApproval ?? false;

			LocaliseUI();

			if (FromApproval.GetValueOrDefault())
			{
				PostJobButton.Text = CodeLocalise("ApproveAndPostJobButton.Button", "Approve and Post");
			}
			else if (Job.JobStatus == JobStatuses.OnHold)
			{
				PostJobButton.Text = CodeLocalise("PostOnHoldJobButton.Button", "Save Changes");
			}
			else
			{
				PostJobButton.Text = Job.JobType.IsInternship() 
					? CodeLocalise("PostInternship.Button", "Post internship")
					: CodeLocalise("PostJob.Button", "Post job");
			}

			var url = App.Settings.Module == FocusModules.Assist
				? UrlBuilder.AssistPrintJob(Job.Id, "preview")
				: UrlBuilder.TalentPrintJob(Job.Id);

			PrintJobButton.OnClientClick =
				string.Format(
					"window.open('{0}', '','status=0, toolbar=0, menubar=0, height=600, width=600, location=0, resizable=1, scrollbars=1');return false;",
					url);
		}

		#region Button click handlers

		/// <summary>
		/// Handles the Click event of the PostJobButton control.
		/// </summary>
		protected void PostJobClick(object sender, EventArgs e)
		{
			Handle(PageController.PostJob(Job, FromApproval));
		}

		/// <summary>
		/// Handles the Click event of the NotifyEmployerButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void NotifyEmployerButton_Click(object sender, EventArgs e)
		{
			//ParentModal.Hide();
			Handle( PageController.NotifyEmployer(Job) );
		}

		#endregion

		#region Helper methods

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			PrintJobButton.Text = CodeLocalise("Print.Button", "Print");
			NotifyEmployerButton.Text = CodeLocalise("NotifyEmployerButton.Button", "Notify #BUSINESS#:LOWER");
			ReturnToJobButton.Text = CodeLocalise("ReturnToJobButton.Button", "Return to #POSTINGTYPE#");
		}

		#endregion
	}
}
