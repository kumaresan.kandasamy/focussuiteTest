﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditPostingReferralConfirmationContent.ascx.cs" Inherits="Focus.Web.Code.Controls.ModalContents.EditPostingReferralConfirmationContent" %>

<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<div style="float:left;height: 500px;max-height: 500px">
	<asp:Panel ID="EditPostingReferralConfirmationModalHeading" runat="server" ClientIDMode="Static" CssClass="modal-header">
		<h1 data-modal="title"><%= HtmlLocalise( "Title.Text", "Notification of edited job posting" )%></h1>
		<p><%= HtmlLocalise("Summary.Text", "Notify the #BUSINESS#:LOWER of the edits to their posting.") %></p>
	</asp:Panel>
	<div style="overflow-y: scroll; height: 380px; width: 600px;">
		<asp:Literal ID="EmailBodyLiteral" runat="server" ClientIDMode="Static" />
	</div>
	<p><asp:CheckBox ID="BCCMeCheckBox" runat="server" /></p>
	<p>
		<asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" />
		<asp:Button ID="EditAndSendMessageButton" runat="server" CssClass="button3" OnClick="EditAndSendMessageButton_Clicked" />		
	</p>
</div>

<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />