﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common.Code;

using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Common.Code.ControllerResults;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Core.Views;
using Focus.Web.Controllers.Code.Controls.ModalContents;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.ModalContents
{
	public partial class EditPostingReferralConfirmationContent : BddModalContentBase
	{
		#region Properties

		/// <summary>
		/// Gets the page controller.
		/// </summary>
		/// <value>
		/// The page controller.
		/// </value>
		protected EditPostingReferralConfirmationContentController PageController { get { return PageControllerBaseProperty as EditPostingReferralConfirmationContentController; } }

		/// <summary>
		/// Registers the page controller.
		/// </summary>
		/// <returns></returns>
		public override IPageController RegisterPageController()
		{
			return new EditPostingReferralConfirmationContentController( App );
		}

		private long EmployeeId
		{
			get { return GetViewStateValue<long>( "DenyPostingReferralModal:EmployeeId" ); }
			set { SetViewStateValue( "DenyPostingReferralModal:EmployeeId", value ); }
		}

		protected JobDto Job
		{
			get { return GetProperty<JobDto>( "Job" ); }
		}

		private long JobId
		{
			get { return Job.Id.GetValueOrDefault( 0 ); }
		}

		private string NewPosting
		{
			get { return Job.PostingHtml; }
			//get { return GetProperty<string>( "NewPosting" ); }
		}

		private EmailTemplateView EmailTemplate
		{
			get { return GetViewStateValue<EmailTemplateView>( "EditPostingReferralConfirmationModal:EmailTemplate" ); }
			set { SetViewStateValue( "EditPostingReferralConfirmationModal:EmailTemplate", value ); }
		}

		#endregion

		protected void Page_Load( object sender, EventArgs e )
		{
			if( !IsPostBack || String.IsNullOrWhiteSpace(EmailBodyLiteral.Text) )
			{
				LocaliseUI();

				GetEmailTemplate();
				EmailBodyLiteral.Text = EmailTemplate.Body.Replace( Environment.NewLine, "<br />" );
			}
		}

		/// <summary>
		/// Handles the Clicked event of the EditAndSendMessageButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void EditAndSendMessageButton_Clicked(object sender, EventArgs e)
		{
            Handle(PageController.NotifyEmployer(Job, BCCMeCheckBox.Checked));

			Confirmation.Show(CodeLocalise("ReferralEditedConfirmation.Title", "Posting edited"),
				CodeLocalise("ReferralEditedConfirmation.Body",
					"The posting has been updated and a copy emailed to the #BUSINESS#:LOWER for acceptance."),
				CodeLocalise("CloseModal.Text", "OK"),
				closeLink: UrlBuilder.PostingReferrals());
		}

		/// <summary>
		/// Handles the Clicked event of the CancelButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CancelButton_Clicked( object sender, EventArgs e )
		{
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			EditAndSendMessageButton.Text = CodeLocalise("EditAndSendMessageButton.Text.Text", "Email #BUSINESS#:LOWER");
			CancelButton.Text = CodeLocalise( "Global.Cancel.Text", "Cancel" );
			BCCMeCheckBox.Text = CodeLocalise( "BCCMeCheckBox.Text", "email me a copy of this message" );
		}

		/// <summary>
		/// Gets the email template.
		/// </summary>
		private void GetEmailTemplate()
		{
			var jobPostingReferral = ServiceClientLocator.JobClient( App ).GetJobPostingReferral( JobId );
			if( jobPostingReferral.EmployeeId.HasValue )
				EmployeeId = jobPostingReferral.EmployeeId.Value;
			var userDetails = ServiceClientLocator.AccountClient( App ).GetUserDetails( App.User.UserId );
			var phoneNumber = (userDetails.IsNotNull() && userDetails.PrimaryPhoneNumber.IsNotNull() && userDetails.PrimaryPhoneNumber.Number.IsNotNullOrEmpty()) ? userDetails.PrimaryPhoneNumber.Number : "N/A";

			var jobLinkUrl = string.Concat( App.Settings.TalentApplicationPath, UrlBuilder.JobView( JobId, false ) );
			var jobLinkHtml = string.Format( "<a href='{0}'>{0}</a>", jobLinkUrl );

			var templateValues = new EmailTemplateData
			{
				RecipientName = String.Format( "{0} {1}", jobPostingReferral.EmployeeFirstName, jobPostingReferral.EmployeeLastName ),
				JobTitle = jobPostingReferral.JobTitle,
				JobPosting = NewPosting,
				SenderPhoneNumber = phoneNumber,
				SenderEmailAddress = App.User.EmailAddress,
				SenderName = String.Format( "{0} {1}", App.User.FirstName, App.User.LastName ),
				JobLinkUrls = new List<string> { jobLinkHtml }
			};

			EmailTemplate = ServiceClientLocator.CoreClient( App ).GetEmailTemplatePreview( EmailTemplateTypes.EditPostingReferralConfirmation, templateValues );
		}
	}
}