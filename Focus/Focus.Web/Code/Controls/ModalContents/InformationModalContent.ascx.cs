﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;

using Focus.Common.Code;

#endregion

namespace Focus.Web.Code.Controls.ModalContents
{
	public partial class InformationModalContent : BddModalContentBase
	{
		public string Width { get { return Properties["Width"] as string; } }
		public string Height { get { return Properties["Height"] as string; } }
		public string DisplayCloseIcon { get { return Properties["DisplayCloseIcon"] as string; } }

    protected override void OnInit(System.EventArgs e)
    {
      CancelButtonId = "OkButton";
    }

		public void Page_Load()
		{
			Title.Text = Properties["Title"] as string;
			Details.Text = Properties["Details"] as string;
			OkButton.Text = Properties["OkText"] as string;
            if(Properties.ContainsKey("DisplayCloseIcon"))
                CloseIcon.Visible = Convert.ToBoolean(Properties["DisplayCloseIcon"]);
		}

		protected void OkButtonClick(object sender, CommandEventArgs e)
		{
			Handle(ButtonActions["OkButtonClick"]);
			ParentModal.Hide();
		}
	}
}
