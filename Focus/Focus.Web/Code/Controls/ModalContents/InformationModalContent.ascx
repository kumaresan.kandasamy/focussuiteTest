﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="InformationModalContent.ascx.cs"
	Inherits="Focus.Web.Code.Controls.ModalContents.InformationModalContent" %>

<div class="closeIcon" runat="server" id="CloseIcon" ><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('BddModalPopupBehavior').hide();return false;" /></div>
<table style="width:<%= Width %>px">
	<tr>
   <th style="vertical-align:top;">
	   <asp:Literal ID="Title" runat="server"></asp:Literal>
   </th>
  </tr>
  <tr>
   <td style="vertical-align: top; height:<%= Height %>px">
	   <asp:Literal ID="Details" runat="server"></asp:Literal>
   </td>
  </tr>
	<tr>
		<td style="text-align:left;">
		<asp:Button ID="OkButton" runat="server" SkinID="Button1" CausesValidation="false" OnCommand="OkButtonClick"/>
		</td>
	</tr>
</table>