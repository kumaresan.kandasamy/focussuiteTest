﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="BddModal.ascx.cs"
	Inherits="Focus.Web.Code.Controls.BddModal" %>
<%@ Register TagPrefix="uc" Namespace="DBauer.Web.UI.WebControls" Assembly="DBauer.Web.UI.WebControls.DynamicControlsPlaceholder, Version=2.2.0.0, Culture=neutral, PublicKeyToken=0cf700d027f89147" %>

<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="BddModalPopup" runat="server" ClientIDMode="Static" 
												TargetControlID="ModalDummyTarget"
												PopupControlID="ModalPanel"
												PopupDragHandleControlID="ModalPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" BehaviorID="BddModalPopupBehavior" />

<asp:Panel ID="ModalPanel" runat="server" CssClass="modal conditionallyScrolledModal" Style="display:none;">
	<uc:DynamicControlsPlaceholder ID="ModalContent" ControlsWithoutIDs="DontPersist" runat="server"></uc:DynamicControlsPlaceholder>
</asp:Panel>