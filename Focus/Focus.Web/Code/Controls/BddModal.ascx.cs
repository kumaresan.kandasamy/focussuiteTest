﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;

using Framework.Core;

using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;

#endregion

namespace Focus.Web.Code.Controls
{
	public partial class BddModal : BddModalBase
	{
    /// <summary>
    /// Shows the specified properties.
    /// </summary>
    /// <param name="properties">The properties.</param>
    /// <param name="buttonActions">The button actions.</param>
    /// <param name="contentUrl">The content URL.</param>
    /// <param name="height">The height.</param>
    /// <param name="width">The width.</param>
    /// <param name="y">The y.</param>
    /// <param name="popupDragHandleControlId">The element that serves as the drag handle element for the modal.</param>
    /// <param name="clientSideCancel">Whether to allow the modal to be closed client side</param>
    public override void Show(Dictionary<string, object> properties, Dictionary<string, IControllerResult> buttonActions, string contentUrl, int height, int width, int y, string popupDragHandleControlId, bool clientSideCancel = false)
		{
			Properties = properties;
			ButtonActions = buttonActions;
			BddModalPopup.Y = y;

			// Create instance of the UserControl SimpleControl
			var control = LoadControl(contentUrl) as BddModalContentBase;

			//Add the SimpleControl to Placeholder
			if (control != null)
			{
				control.ID = string.Concat("DymanicallyLoadedModalContent_", contentUrl).Replace("~", "").Replace("/", "-");
				ModalContent.Controls.Clear();
				ModalContent.Controls.Add(control);

        if (clientSideCancel && control.CancelButtonId.IsNotNullOrEmpty())
			  {
          var cancelControl = control.FindControl(control.CancelButtonId);
          if (cancelControl.IsNotNull())
            BddModalPopup.CancelControlID = cancelControl.ClientID;
			  }
			}

			if (popupDragHandleControlId.IsNotNullOrEmpty())
				BddModalPopup.PopupDragHandleControlID = popupDragHandleControlId;

			BddModalPopup.Show();
		}

		/// <summary>
		/// Hides this instance.
		/// </summary>
		public override void Hide()
		{
			BddModalPopup.Hide();
		}
	}
}
