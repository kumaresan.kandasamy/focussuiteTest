﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditCompanyModal.ascx.cs" Inherits="Focus.Web.Code.Controls.User.EditCompanyModal" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>
<%@ Register TagPrefix="uc" TagName="EditBusinessUnit" Src="~/Code/Controls/User/EditBusinessUnit.ascx" %>

<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" BehaviorID="EditCompanyModal"
												TargetControlID="ModalDummyTarget"
												PopupControlID="EditCompanyModalPanel"
												PopupDragHandleControlID="EditCompanyModalPanelHeader"
												RepositionMode="RepositionOnWindowResizeAndScroll" 
												BackgroundCssClass="modalBackground" />
 <asp:Panel ID="EditCompanyModalPanel" runat="server" CssClass="modal" Style="display:none">
   <asp:Panel runat="server" ClientIDMode="Static" ID="EditCompanyModalPanelHeader">
     
   </asp:Panel>
	 <asp:UpdatePanel ID="EditCompanyModalPanelUpdatePanel" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<table role="presentation">
					<tr runat="server" ID="FEINTableRow">
						<td colspan="2"><focus:LocalisedLabel runat="server" ID="FeinLabel" DefaultText="FEIN:"/></td>
					</tr>
					<tr runat="server" ID="SEINTableRow">
						<td class="label"><%= HtmlLabel(StateEmployerIdentificationNumberTextBox,"StateEmployerIdentificationNumber.Label", "State Employer ID")%></td>
						<td>
							<asp:TextBox  TabIndex="2" ID="StateEmployerIdentificationNumberTextBox" MaxLength="11" runat="server"/>
							 <asp:RegularExpressionValidator ID="StateEmployerIdentificationNumberValidator" runat="server" ControlToValidate="StateEmployerIdentificationNumberTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
						</td>
					</tr>
					<tr runat="server" ID="PrimaryNameRow">
						<td style="width:150;"><%= HtmlLabel("PrimaryName.Label", "Primary name") %></td>
						<td>
							<asp:TextBox ID="PrimaryNameTextBox" runat="server" ClientIDMode="Static" TabIndex="3" MaxLength="200" />
							<asp:RequiredFieldValidator ID="PrimaryNameRequired" runat="server" ControlToValidate="PrimaryNameTextBox" SetFocusOnError="true" ValidationGroup="EditCompanyModal" CssClass="error" />
						</td>
					</tr>
        </table>
        
				<asp:Panel ID="EditBusinessUnitPanel" runat="server" CssClass="hidden" Visible="False">
					<uc:EditBusinessUnit runat="server" ID="EditBusinessUnit" ValidationGroup="AddBusinessUnitSelector" />
				</asp:Panel>

        <table role="presentation">
					<tr>
						<td>&nbsp;</td>
						<td style="text-align:right;">
							<asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" Text="Cancel"/>
							<asp:Button ID="SaveButton" runat="server" CssClass="button4" OnClick="SaveButton_Clicked" ValidationGroup="AddBusinessUnitSelector" />
						</td>
					</tr>
				</table>
			</ContentTemplate>	
	</asp:UpdatePanel>
</asp:Panel>

<asp:UpdatePanel ID="ConfirmationUpdatePanel" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" ClientIDMode="AutoID" />
		</ContentTemplate>	
</asp:UpdatePanel>

<script type="text/javascript">
    Sys.Application.add_load(EditCompanyModal_PageLoad);

    function EditCompanyModal_PageLoad(sender, args) {

      $("#FEINTextBox").mask("99-9999999", { placeholder: " " });
      $("#FEINTextBox").mask("99-9999999", { placeholder: " " });
      
	    // Jaws Compatibility
      var modal = $find('EditCompanyModal');

	    if (modal != null) {
		    modal.add_shown(function() {
			    $('#EditCompanyModalPanel').attr('aria-hidden', false);
			    $('.page').attr('aria-hidden', true);
			    $('#FEINTextBox').focus();
		    });
		    modal.add_hiding(function() {
			    $('#EditCompanyModalPanel').attr('aria-hidden', true);
			    $('.page').attr('aria-hidden', false);
		    });
	    }
    }

    function FEINChanged() {
		document.getElementById('WarningMsgLabel').style.display="inline";
	};
</script>
