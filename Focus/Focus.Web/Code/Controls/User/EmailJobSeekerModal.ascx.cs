﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.EmailTemplate;
using Focus.Core.Views;
using Focus.Services.Messages;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class EmailJobSeekerModal : UserControlBase
	{
		public List<long> CandidateIds
		{
			get { return GetViewStateValue<List<long>>("EmailJobSeekerModal:CandidateIds"); }
			set { SetViewStateValue("EmailJobSeekerModal:CandidateIds", value); }
		}

		public string LensPostingId
		{
			get { return GetViewStateValue<string>("EmailCandidateModal:LensPostingId"); }
			set { SetViewStateValue("EmailCandidateModal:LensPostingId", value); }
		}

		public int Score
		{
			get { return GetViewStateValue<int>("EmailCandidateModal:Score"); }
			set { SetViewStateValue("EmailCandidateModal:Score", value); }
		}

		protected long JobId
		{
			get { return GetViewStateValue<long>("EmailCandidateModal:JobId"); }
			set { SetViewStateValue("EmailCandidateModal:JobId", value); }
		}

		private EmailTemplateTypes EmailTemplateType
		{
			get { return GetViewStateValue("EmailCandidateModal:EmailTemplateType", EmailTemplateTypes.CustomMessage); }
			set { SetViewStateValue("EmailCandidateModal:EmailTemplateType", value); }
		}

		public delegate void CompletedHandler(object sender, EmailJobSeekerCompletedEventArgs eventArgs);

		public event CompletedHandler Completed;

		protected bool IsInviteToApply
		{
			get { return GetViewStateValue<bool>( "EmailCandidateModal:IsInviteToApply" ); }
			set { SetViewStateValue( "EmailCandidateModal:IsInviteToApply", value ); }
		}

    protected bool IsVeteran
    {
      get { return GetViewStateValue<bool>("EmailCandidateModal:IsVeteran"); }
      set { SetViewStateValue("EmailCandidateModal:IsVeteran", value); }
    }

    protected ExtendVeteranPriorityOfServiceTypes ExtendVeteranPriority
    {
      get { return GetViewStateValue<ExtendVeteranPriorityOfServiceTypes>("EmailCandidateModal:ExtendVeteranPriority"); }
      set { SetViewStateValue("EmailCandidateModal:ExtendVeteranPriority", value); }
    }

    protected DateTime? VeteranPriorityEndDate
    {
      get { return GetViewStateValue<DateTime?>("EmailCandidateModal:veteranPriorityEndDate"); }
      set { SetViewStateValue("EmailCandidateModal:veteranPriorityEndDate", value); }
    }

    protected bool QueueRecommendation
    {
      get { return GetViewStateValue<bool>("EmailCandidateModal:queueRecommendation"); }
      set { SetViewStateValue("EmailCandidateModal:queueRecommendation", value); }
    }

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
			}
		    EmailSubjectRequired.ValidationGroup = EmailBodyRequired.ValidationGroup = SendMessageButton.ValidationGroup = String.Format("{0}:Validation", ClientID);
		}

		/// <summary>
		/// Shows the specified email.
		/// </summary>
		/// <param name="emailTemplateType">Type of the email template.</param>
		/// <param name="candidateIds">The candidate ids.</param>
		/// <param name="jobIds">The job ids.</param>
		/// <param name="lensPostingIds">The lens posting ids.</param>
		public void Show(EmailTemplateTypes emailTemplateType, List<long> candidateIds, List<long> jobIds = null, List<string> lensPostingIds = null)
		{
			CandidateIds = candidateIds;
			ClearInputs(true);

			var templateValues = new EmailTemplateData();

			switch (emailTemplateType)
			{
				case EmailTemplateTypes.InvitationToApplyForJobThroughFocusCareer:
					EmailTemplateNameDropDown.Visible = false;
					var candidate = ServiceClientLocator.CandidateClient(App).GetCandidate(CandidateIds[0]);

					templateValues = new EmailTemplateData
					{
						// ReSharper disable once AssignNullToNotNullAttribute
						JobLinkUrls = lensPostingIds.Select(x => UrlBuilder.CareerInviteToJobPosting(x)).ToList(),
						RecipientName = candidate.FirstName,
						SenderName = App.User.FirstName
					};
					break;
			}

			EmailTemplateType = emailTemplateType;
			var emailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(emailTemplateType, templateValues);

			EmailSubjectTextBox.Text = emailTemplate.Subject;
			EmailBodyTextBox.Text = emailTemplate.Body;

			EmailJobSeekerModalPopup.Show();
		}

	  /// <summary>
	  /// Shows the specified email.
	  /// </summary>
	  /// <param name="emailTemplateType">Type of the email template.</param>
	  /// <param name="candidateId">The candidate id.</param>
	  /// <param name="jobId">The job id.</param>
	  /// <param name="lensPostingId">The lens posting id.</param>
	  /// <param name="jobTitle">The job title.</param>
    /// <param name="veteranPriorityEndDate">Veteran Priority End Date</param>
    /// <param name="extendVeteranPriority">Extend Veteran Priority</param> 
	  /// <param name="employerName">The employer name.</param>
	  /// <param name="joblink"></param>
	  /// <param name="isInviteToApply"></param>
	  /// <param name="score">The match score</param>
	  /// <param name="veteran">Whether the candidate is a veteran</param>
	  public void Show(EmailTemplateTypes emailTemplateType, long candidateId, long jobId, string lensPostingId, string employerName, 
		  string jobTitle, DateTime? veteranPriorityEndDate, ExtendVeteranPriorityOfServiceTypes? extendVeteranPriority, string joblink = "", 
      bool isInviteToApply = false, int score = 0, bool veteran = false)
		{
			CandidateIds = new List<long> { candidateId };
			LensPostingId = lensPostingId;
			Score = score;
		  IsVeteran = veteran;
			ClearInputs(true);
			var templateValues = new EmailTemplateData();
			switch (emailTemplateType)
			{
				case EmailTemplateTypes.InvitationToApplyForJobThroughFocusCareer:
					EmailTemplateNameDropDown.Visible = false;
					var candidate = ServiceClientLocator.CandidateClient(App).GetCandidate(CandidateIds[0]);
					templateValues = new EmailTemplateData
					{
						JobLinkUrls = new List<string> { UrlBuilder.CareerInviteToJobPosting(lensPostingId) },
						RecipientName = candidate.FirstName,
						SenderName = App.User.FirstName,
						EmployerName = employerName,
						JobTitle = jobTitle
					};
					break;
				case EmailTemplateTypes.RecommendJobSeeker:
					EmailTemplateNameDropDown.Visible = false;
           var jobseeker = ServiceClientLocator.CandidateClient(App).GetCandidatePerson(CandidateIds[0]);
           var job = ServiceClientLocator.JobClient(App).GetJob(jobId);
           if (job.IsNotNull())
           {
					templateValues = new EmailTemplateData
					{
						JobLinkUrls = joblink.IsNotNullOrEmpty() ? new List<string> { joblink } : new List<string> { UrlBuilder.CareerInviteToJobPosting(lensPostingId) },
						RecipientName = jobseeker.FirstName,
						SenderName = App.User.FirstName,
						EmployerName = job.IsConfidential.GetValueOrDefault() ?"[Confidential]":employerName,
						JobTitle = jobTitle
					};
           }
           else
           {
               templateValues = new EmailTemplateData
               {
                   JobLinkUrls = joblink.IsNotNullOrEmpty() ? new List<string> { joblink } : new List<string> { UrlBuilder.CareerInviteToJobPosting(lensPostingId) },
                   RecipientName = jobseeker.FirstName,
                   SenderName = App.User.FirstName,
                   EmployerName = employerName,
                   JobTitle = jobTitle
               };
           }
					break;
			}
			EmailTemplateType = emailTemplateType;
			var emailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(emailTemplateType, templateValues);

			IsInviteToApply = isInviteToApply;
			JobId = jobId;
			EmailSubjectTextBox.Text = emailTemplate.Subject;
			EmailBodyTextBox.Text = emailTemplate.Body;
			EmailJobSeekerModalPopup.Show();

      // Disable the Subject and Body textboxes if a non-veteran is being recommended for a job with a veteran exclusivity period
		  QueueRecommendation = !IsVeteran &&
		                        (extendVeteranPriority.GetValueOrDefault() == ExtendVeteranPriorityOfServiceTypes.ExtendForLifeOfPosting || veteranPriorityEndDate.GetValueOrDefault() > DateTime.Now);

      EmailSubjectTextBox.Enabled = EmailBodyTextBox.Enabled = !QueueRecommendation;
		}

		/// <summary>
		/// Shows the email modal fora single candidate.
		/// </summary>
		/// <param name="candidateId">The candidate id.</param>
		/// <param name="jobId">The job id.</param>
		public void Show(long candidateId, long jobId = 0)
		{
			JobId = jobId;

			CandidateIds = new List<long> { candidateId };

			ClearInputs(true);

			EmailTemplateNameDropDown.Visible = (JobId != 0);

			EmailTemplateType = EmailTemplateTypes.CustomMessage;

			EmailJobSeekerModalPopup.Show();
		}


		public void Show(long candidateId, List<string> lensJobIds)
		{
			JobId = 0;

			ClearInputs(true);
			EmailTemplateNameDropDown.Visible = false;
			CandidateIds = new List<long> { candidateId };

			var candidate = ServiceClientLocator.CandidateClient(App).GetCandidatePerson(candidateId);
			var jobUrls = lensJobIds.Select(x => UrlBuilder.CareerInviteToJobPosting(x)).ToList();

			var templateValues = new EmailTemplateData
			{
				JobLinkUrls = jobUrls,
				RecipientName = candidate.FirstName
			};

			var emailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(EmailTemplateTypes.JobPostingOfInterest, templateValues);
			EmailTemplateType = EmailTemplateTypes.JobPostingOfInterest;
			EmailSubjectTextBox.Text = emailTemplate.Subject;
			EmailBodyTextBox.Text = emailTemplate.Body;

			EmailJobSeekerModalPopup.Show();
		}


		public void Show(long candidateId, List<long> jobIds)
		{
			JobId = 0;

			ClearInputs(true);
			EmailTemplateNameDropDown.Visible = false;
			CandidateIds = new List<long> { candidateId };

			var jobUrls = jobIds.Select(jobId => UrlBuilder.CareerInviteToJobPosting(ServiceClientLocator.JobClient(App).GetJobLensPostingId(jobId))).ToList();

			var templateValues = new EmailTemplateData
			{
				JobLinkUrls = jobUrls
			};

			var emailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(EmailTemplateTypes.CustomMessage, templateValues);
			EmailTemplateType = EmailTemplateTypes.CustomMessage;
			EmailSubjectTextBox.Text = emailTemplate.Subject;
			EmailBodyTextBox.Text = emailTemplate.Body;

			EmailJobSeekerModalPopup.Show();
		}

		/// <summary>
		/// Shows the email modal for multiple candiates.
		/// </summary>
		/// <param name="candidateIds">The candidate ids.</param>
		public void Show(List<long> candidateIds)
		{
			JobId = 0;

			EmailTemplateType = EmailTemplateTypes.CustomMessage;
			ClearInputs(true);
			EmailTemplateNameDropDown.Visible = false;
			CandidateIds = candidateIds;

			EmailJobSeekerModalPopup.Show();
		}

		/// <summary>
		/// Handles the Clicked event of the SendMessageButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SendMessageButton_Clicked(object sender, EventArgs e)
		{
			var emailSubject = EmailSubjectTextBox.TextTrimmed();
			var emailBody = EmailBodyTextBox.TextTrimmed();

			var senderAddress = ServiceClientLocator.CoreClient(App).GetSenderEmailAddressForTemplate(EmailTemplateType);
			var emailList = CandidateIds.Select(personId => new CandidateEmailView
			{
				PersonId = personId,
				CandidateName = "",
				Subject = emailSubject,
				Body = emailBody,
				BccRequestor = false,
				JobId = JobId,
				IsInviteToApply = IsInviteToApply,
				SenderAddress = senderAddress
			}).ToList();

			IsInviteToApply = false;
			JobId = 0;

		  if (QueueRecommendation)
		  {
        // Allow non veterans to be recommended for a Veteran Priority job however delay the email until after the Veteran Priority End Date
        Confirmation.Show(CodeLocalise("RecommendVeteranPosting.Title", "Posting exclusive to veterans"),
            CodeLocalise("ReferralVeteranPosting.Body",
              "This posting is currently only available to veterans to apply. Your recommendation will be saved and sent automatically when the " +
              "exclusivity period is over. No further action is required."),
            CodeLocalise("CloseModal.Text", "OK"));
		  }
		  else
		  {
		    ServiceClientLocator.CandidateClient(App).SendCandidateEmails(emailList, true, BCCMeCheckBox.Checked);

		    EmailJobSeekerModalPopup.Hide();

		    var body = CandidateIds.Count == 1
		      ? CodeLocalise("EmailCandidateConfirmation.Body",
		        "Thank you. We have sent the email to the #CANDIDATETYPE#:LOWER you selected.")
		      : CodeLocalise("EmailCandidatesConfirmation.Body",
		        "Thank you. We have sent the email to the #CANDIDATETYPES#:LOWER you selected.");

		    Confirmation.Show(CodeLocalise("EmailCandidatesConfirmation.Title", "Email #CANDIDATETYPE#:LOWER(s)"), body, CodeLocalise("Global.OK.Text", "OK"));
		  }

		  EmailJobSeekerModalPopup.Hide();

			ClearInputs();

      OnCompleted(new EmailJobSeekerCompletedEventArgs { EmailTemplateType = EmailTemplateType, QueueRecommendation = QueueRecommendation});
		}

		/// <summary>
		/// Handles the Clicked event of the CancelButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CancelButton_Clicked(object sender, EventArgs e)
		{
			EmailJobSeekerModalPopup.Hide();
			OnCompleted(new EmailJobSeekerCompletedEventArgs());
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the EmailTemplateNameDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void EmailTemplateNameDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			var emailTemplateType = EmailTemplateType = EmailTemplateNameDropDown.SelectedValueToEnum<EmailTemplateTypes>();
			ClearInputs();

			if (JobId != 0 && emailTemplateType != EmailTemplateTypes.CustomMessage)
			{
				var jobView = ServiceClientLocator.JobClient(App).GetJobView(JobId);
				var jobLinkUrl = UrlBuilder.CareerInviteToJobPosting(ServiceClientLocator.JobClient(App).GetJobLensPostingId(JobId));

				// If JobId != 0, there will only be one CandidateId
				App.ClearPageError();

				ResumeView candidateResume = null;

				try
				{
					candidateResume = ServiceClientLocator.CandidateClient(App).GetResume(CandidateIds[0]);
				}
				catch (ArgumentNullException ex)
				{
					App.SetPageError(ex.Message);
				}

				var templateValues = new EmailTemplateData
				{
					RecipientName = candidateResume.IsNull() ? null : candidateResume.Name,
					JobTitle = jobView.JobTitle,
					EmployerName = jobView.BusinessUnitName,
					SenderName = string.Format("{0} {1}", App.User.FirstName, App.User.LastName),
					ShowSalutation = true,
					JobLinkUrls = new List<string> { jobLinkUrl }
				};
				var emailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(emailTemplateType, templateValues);
				EmailSubjectTextBox.Text = emailTemplate.Subject;
				EmailBodyTextBox.Text = emailTemplate.Body;
			}
			EmailJobSeekerModalPopup.Show();
		}

		/// <summary>
		/// Raises the <see><cref>E:Completed</cref></see>  event.
		/// </summary>
		/// <param name="eventArgs">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void OnCompleted(EmailJobSeekerCompletedEventArgs eventArgs)
		{
			if (Completed != null)
				Completed(this, eventArgs);
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			EmailSubjectRequired.ErrorMessage = CodeLocalise("EmailSubject.RequiredErrorMessage", "Email subject is required.");
			EmailBodyRequired.ErrorMessage = CodeLocalise("EmailBody.RequiredErrorMessage", "Email text is required.");
			SendMessageButton.Text = CodeLocalise("SendMessageButton.Button", "Send message");
			CancelButton.Text = CodeLocalise("Global.Cancel.Button", "Cancel");
			BCCMeCheckBox.Text = CodeLocalise("BCCMeCheckBox.Text", "email me a copy of this message");

			EmailTemplateNameDropDown.Items.Clear();

			EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.CustomMessage, "Custom message");
			EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.ApproveCandidateReferral, "Approve candidate referral");
			EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.DenyCandidateReferral, "Deny candidate referral");

			//EmailTemplateNameDropDown.Items.AddLocalisedTopDefault("EmailTemplateName.TopDefault", "- select template -");
		}

		/// <summary>
		/// Clears the inputs.
		/// </summary>
		private void ClearInputs(bool resetTemplateDropDown = false)
		{
			if (resetTemplateDropDown)
			{
				EmailTemplateNameDropDown.SelectedItem.Selected = false;
				EmailTemplateNameDropDown.Items.FindByValue(EmailTemplateTypes.CustomMessage.ToString()).Selected = true;
			}

			EmailSubjectTextBox.Text = EmailBodyTextBox.Text = string.Empty;
			BCCMeCheckBox.Checked = false;
		}

	}

	public class EmailJobSeekerCompletedEventArgs : EventArgs
	{
		public EmailTemplateTypes EmailTemplateType { get; set; }
	  public bool QueueRecommendation { get; set; }
	}
}
