﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using BgtSSO;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;
using Focus.Web.Core.Models;
using Focus.Web.WebAuth;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class EmployerRegistration : UserControlBase
	{
		#region Properties

		/// <summary>
		/// Gets or sets the current step.
		/// </summary>
		/// <value>The current step.</value>
		protected int CurrentStep
		{
			get { return App.GetSessionValue("Registration:CurrentStep", 1); }
			set { App.SetSessionValue("Registration:CurrentStep", value); }
		}

		/// <summary>
		/// Gets or sets the password.
		/// </summary>
		/// <value>The password.</value>
		protected string Password
		{
			get { return GetViewStateValue("Registration:Password", string.Empty); }
			set { SetViewStateValue("Registration:Password", value); }
		}

		/// <summary>
		/// Gets or sets a flag indicating if the user has progressed to step 2 before.
		/// </summary>
		/// <value>The current step.</value>
		protected bool PreviouslyProgressedToStep2
		{
			get { return App.GetSessionValue("Registration:PreviouslyProgressedToStep2", false); }
			set { App.SetSessionValue("Registration:PreviouslyProgressedToStep2", value); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether the FEIN is validated
		/// </summary>
		/// <value><c>true</c> if [validated FEIN]; otherwise, <c>false</c>.</value>
		protected bool ValidatedFEIN
		{
			get { return App.GetSessionValue("Registration:validatedFEIN", false); }
			set { App.SetSessionValue("Registration:validatedFEIN", value); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether the FEIN is validated
		/// </summary>
		/// <value><c>true</c> if [validated FEIN]; otherwise, <c>false</c>.</value>
		protected string FEIN
		{
			get { return App.GetSessionValue("Registration:FEIN", ""); }
			set { App.SetSessionValue("Registration:FEIN", value); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether the Employer is already approved.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is already approved; otherwise, <c>false</c>.
		/// </value>
		protected bool IsAlreadyApproved
		{
			get { return App.GetSessionValue("Registration:IsAlreadyApproved", false); }
			set { App.SetSessionValue("Registration:IsAlreadyApproved", value); }
		}

		/// <summary>
		/// Gets or sets the employer external id.
		/// </summary>
		/// <value>The employer external id.</value>
		protected string EmployerExternalId
		{
			get { return GetViewStateValue("Registration:EmployerExternalId", string.Empty); }
			set { SetViewStateValue("Registration:EmployerExternalId", value); }
		}

		/// <summary>
		/// Gets or sets the employer external id.
		/// </summary>
		/// <value>The employer external id.</value>
		protected ApprovalStatuses? InitialApprovalStatus
		{
			get { return GetViewStateValue<ApprovalStatuses?>("Registration:InitialApprovalStatus"); }
			set { SetViewStateValue("Registration:InitialApprovalStatus", value); }
		}

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
        CurrentStep = 1;

        // Set the flag to indicate we have never progressed to step 2 and beyond
        PreviouslyProgressedToStep2 = false;

				ShowCurrentStep();
        
				// Ensure that the CompleteButton is disabled to start with
				// as we want to ensure that the "Accept Terms" checkbox is selected to enable it
				CompleteButton.Enabled = CompleteBottomButton.Enabled = false;

				BottomNavigationTable.Visible = App.Settings.Theme == FocusThemes.Workforce;

				LocaliseUI();
			}
		}

		#region Step navigation methods

		/// <summary>
		/// Shows the current step.
		/// Will also call 
		/// </summary>
		private void ShowCurrentStep()
		{
			Step1.Visible = (CurrentStep == 1);
			Step2.Visible = (CurrentStep == 2);
			Step3.Visible = (CurrentStep == 3);
			Step4.Visible = (CurrentStep == 4);
			Step5.Visible = (CurrentStep == 5);

      Step1.ShowRecaptcha(PreviouslyProgressedToStep2 && App.Settings.RecaptchaAuthorizeAfterFirstCheck);

			if (CurrentStep == 1) Step1ProgressBarItem.Attributes.Add("class", "active");
			else if (CurrentStep > 1) Step1ProgressBarItem.Attributes.Add("class", "visited");

			if (CurrentStep == 2 || CurrentStep == 3) Step2And3ProgressBarItem.Attributes.Add("class", "active");
			else if (CurrentStep > 2) Step2And3ProgressBarItem.Attributes.Add("class", "visited");

			if (CurrentStep == 4) Step4ProgressBarItem.Attributes.Add("class", "active");
			else if (CurrentStep > 4) Step4ProgressBarItem.Attributes.Add("class", "visited");

			if (CurrentStep == 5) Step5ProgressBarItem.Attributes.Add("class", "active");

		  if (Step2.Visible)
        Step2.UpdateBusinessUnitDisplayDetails();

			PreviousButton.Visible = PreviousBottomButton.Visible = (CurrentStep > 1);
			NextButton.Visible = NextBottomButton.Visible = (CurrentStep < 5);
			CompleteButton.Visible = CompleteBottomButton.Visible = (CurrentStep == 5);

			// Set flag to say we have progressed beyond step 1
			if (CurrentStep > 1)
				PreviouslyProgressedToStep2 = true;
		}

		/// <summary>
		/// Handles the Click event of the PreviousButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void PreviousButton_Click(object sender, EventArgs e)
		{
			CurrentStep--;

			if (ValidatedFEIN && CurrentStep == 3)
				CurrentStep--;
			// If this is step 2 then we skip this on going back so we go direct to step 1
			else if (CurrentStep == 2) 
        CurrentStep--;

			ShowCurrentStep();
		}

		/// <summary>
		/// Handles the Click event of the NextButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void NextButton_Click(object sender, EventArgs e)
		{
			if (Page.IsValid)
			{
                if (App.Settings.Module == FocusModules.Talent)
                {
                    if (App.Settings.RecaptchaEnabled && CurrentStep == 1 && !Step1.Validate() && !(PreviouslyProgressedToStep2 && App.Settings.RecaptchaAuthorizeAfterFirstCheck))
                    {
                        Step1.ShowValidationMessages();
                        return;
                    }
                }
				// Any special processing to be done for this step
				if (CurrentStep == 1)
				{
          // Quick get the password from the first step user control because
					// it will be wiped soon as it's a password field!
					Password = Step1.Password;

                    if ((!(App.Settings.SSOEnabled || !App.Settings.SamlEnabledForAssist) && App.Settings.Module == FocusModules.Assist) || (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForTalent) && App.Settings.Module == FocusModules.Talent))
					{
						// Check to see if the username already exists in the system if we're not in OAuth or Saml mode the username is autogenerated and so there is no need to check.
						if (ServiceClientLocator.AccountClient(App).CheckUserExists(new TalentRegistrationModel { AccountUserName = Step1.UserName }).Item1)
						{
							ShowUsernameTakenConfirmation();
							ShowCurrentStep();
							return;
						}
					}

					// See if we can validate the employer using the FEIN
          if (App.Settings.DisplayFEIN == ControlDisplayType.Hidden || Step1.FederalEmployerIdentificationNumber.IsNullOrEmpty())
					{
						ValidatedEmployerView validateEmployer = null;
						if (Step3.EmployerId.GetValueOrDefault() != 0 && Step1.FederalEmployerIdentificationNumber.IsNotNullOrEmpty())
						{
							validateEmployer = ServiceClientLocator.AccountClient(App).ValidateEmployer(string.Empty, Step3.EmployerId);
							Step2.InitialiseStep(validateEmployer, PreviouslyProgressedToStep2);
						}
						else
						{
							ValidatedFEIN = false;
              Step3.EmployerId = null;
							CurrentStep++;
						}
						Step3.InitialiseStep(validateEmployer, "");
					}
					else
					{
						var validatedEmployer = ServiceClientLocator.AccountClient(App).ValidateEmployer(Step1.FederalEmployerIdentificationNumber);

						if (validatedEmployer.IsNotNull() ||
						    (validatedEmployer.IsNotNull() && validatedEmployer.IsValidFederalEmployerIdentificationNumber))
						{
							if (validatedEmployer.CompleteDataFromIntegrationClient || validatedEmployer.CompleteDataFromFocus)
              {
	              if (validatedEmployer.CompleteDataFromIntegrationClient)
	              {
		              EmployerExternalId = validatedEmployer.ExternalId;
		              InitialApprovalStatus = ApprovalStatuses.Approved;
	              }

	              if (Step1.FederalEmployerIdentificationNumber != FEIN)
								{
									Step2.ClearStep();
									Step3.ClearStep();
								}
                Step2.InitialiseStep(validatedEmployer, PreviouslyProgressedToStep2);
                Step3.InitialiseStep(validatedEmployer, Step1.FederalEmployerIdentificationNumber);
								if (Step1.FederalEmployerIdentificationNumber != FEIN)
								{
									Step4.ClearStep();
								}
                ValidatedFEIN = true;
              }
              else // Should only go down here if we have validated the employer from an integration client but they have returned incomplete data
              {
								EmployerExternalId = validatedEmployer.ExternalId;
								InitialApprovalStatus = null;

                Step3.InitialiseStep(validatedEmployer, Step1.FederalEmployerIdentificationNumber);
                ValidatedFEIN = false;
                CurrentStep++;
              }
						}
						else
						{
							// Initialise Step 3 only cos we are on our way to create a new employer / employee account
							EmployerExternalId = null;
							InitialApprovalStatus = null;
							if (Step1.FederalEmployerIdentificationNumber != FEIN)
							{
								Step2.ClearStep();
								Step3.ClearStep();
							}
							Step3.InitialiseStep(null, Step1.FederalEmployerIdentificationNumber);
							if (Step1.FederalEmployerIdentificationNumber != FEIN)
							{
								Step4.ClearStep();
							}
							ValidatedFEIN = false;
							CurrentStep++;
						}
					}

					// If the username is an email address then intialise this to the email address in step 4
					if (Regex.IsMatch(Step1.UserName, App.Settings.EmailAddressRegExPattern))
						Step4.InitialiseStep(Step1.UserName);

					FEIN = Step1.FederalEmployerIdentificationNumber;
				}
				if (CurrentStep == 2)
				{
					var tempModel = new TalentRegistrationModel();
					Step2.UpdateModel(tempModel);
					Step4.BindFromPreviousStep(tempModel);
				}
				if (CurrentStep == 3) // Only update step4 if not a known FEIN
				{
					// We need to preset the employer (user) details so reuse UpdateModel and send to Step4
					var tempModel = new TalentRegistrationModel();
					Step3.UpdateModel(tempModel);
					Step4.BindFromPreviousStep(tempModel);
				}
				if (CurrentStep == 4)
				{
					// We need to make sure that we add onchange event to the 
					// Accept Terms to disable the continue if the terms aren't selected.				
					Step5.InitialiseStep(CompleteButton.ClientID, CompleteBottomButton.ClientID);
				}

				CurrentStep++;
				if (ValidatedFEIN && CurrentStep == 3)
					CurrentStep++;

				ShowCurrentStep();

			}
		}

		#endregion

		#region Registration Completion

		/// <summary>
		/// Handles the Click event of the CompleteButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CompleteButton_Click(object sender, EventArgs e)
		{
			if (Page.IsValid)
			{
				// Create the RegisterAccountRequest and pass it for update to each of the controls
				var model = new TalentRegistrationModel();

				// Update from steps
				Step1.UpdateModel(model);
				Step3.UpdateModel(model);

				// This is a hack but don't have time to rewrite the reg process
				if (ValidatedFEIN)
					Step2.UpdateModel(model);

				Step4.UpdateModel(model);
				Step5.UpdateModel(model);

				// Set the previously stored password
				model.AccountPassword = Password;

				// If Assist user registering the employer FEIN does not need validating
				model.Employer.IsValidFederalEmployerIdentificationNumber =
          (App.Settings.Module == FocusModules.Assist || App.Settings.NewEmployerApproval == TalentApprovalOptions.NoApproval) || ValidatedFEIN;

				model.EmployerExternalId = EmployerExternalId;

                if (App.Settings.SSOEnabled || (App.Settings.SamlEnabledForTalent && App.Settings.Module == FocusModules.Talent) || (App.Settings.Module == FocusModules.Assist && App.Settings.SamlEnabledForAssist))
				{
					var profile = App.GetSessionValue<SSOProfile>(Saml.SSOProfileKey);

					model.EmployeePerson.FirstName = profile.FirstName;
					model.EmployeePerson.LastName = profile.LastName;
					model.EmployeePerson.EmailAddress = model.EmployeeEmailAddress = profile.EmailAddress;
                    model.User = new UserDto { ExternalId = profile.Id, ScreenName = profile.ScreenName };
				}

				model = ServiceClientLocator.AccountClient(App).RegisterTalentUser(model, InitialApprovalStatus);

				App.User = model.UserContext;

				if (App.Settings.Module == FocusModules.Talent)
				{
					FormsAuthentication.SignOut();
					var authTicket = new FormsAuthenticationTicket(1, App.User.FirstName, DateTime.Now, DateTime.Now.AddMinutes(App.Settings.UserSessionTimeout), false, App.User.SerializeUserContext());
					var encryptedTicket = FormsAuthentication.Encrypt(authTicket);

					var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
					Response.Cookies.Add(authCookie);
				}
				
				if (model.RegistrationError == ErrorTypes.UserNameAlreadyExists)
				{
					// Username has been taken between step 1 and registration completion
					ShowUsernameTakenConfirmation();
					Step5.InitialiseStep(CompleteButton.ClientID, CompleteBottomButton.ClientID);
					ShowCurrentStep();
					return;
				}

				OnRegistrationCompleted(new RegistrationCompletedEventArgs(model.AccountUserName, model.AccountPassword, model.Employer.IsValidFederalEmployerIdentificationNumber, model.EmployeeId, model.UserId,
																																		model.EmployeePerson.FirstName, model.EmployeePerson.EmailAddress, IsAlreadyApproved, model.EmployeeApprovalStatus == ApprovalStatuses.Approved));
			}
		}

		/// <summary>
		/// Handles the EmployerChosen event of the Step3 control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="args">The <see cref="EmployerChosenEventArgs"/> instance containing the event data.</param>
		protected void Step3_EmployerChosen(object sender, EmployerChosenEventArgs args)
		{
			// Get employer details
			var validatedEmployer = ServiceClientLocator.AccountClient(App).ValidateEmployer(string.Empty, args.CommandArgument.EmployerId);
			EmployerExternalId = validatedEmployer.ExternalId;
			// Initialise steps 2 and 3
			Step2.InitialiseStepForEducation(validatedEmployer, args.CommandArgument.BusinessUnitId, PreviouslyProgressedToStep2);
			Step3.InitialiseStep(validatedEmployer, Step1.FederalEmployerIdentificationNumber);
			// Set validated to true (This is for education so no FEIN provided)
			ValidatedFEIN = true;

			CurrentStep = 2;
			ShowCurrentStep();
		}

		/// <summary>
		/// Handles the ChangeEmployer event of the Step2 control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Step2_ChangeEmployer(object sender, EventArgs args)
		{
			// USer has to choose a company again so reset Steps 2 and 3
			ValidatedFEIN = false;
			PreviouslyProgressedToStep2 = false;

			//Reset step 3 so fields are blank on next visit
			Step3.EmployerId = null;
			Step3.InitialiseStep(null, Step1.FederalEmployerIdentificationNumber);

			CurrentStep = 3;
			ShowCurrentStep();
		}

		#endregion

		#region Localise UI

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			PreviousButton.Text = PreviousBottomButton.Text = CodeLocalise("PreviousButton.Text.Button", "Previous Step");
			NextButton.Text = NextBottomButton.Text = CodeLocalise("NextButton.Text.Button", "Next Step");
			CompleteButton.Text = CompleteBottomButton.Text = CodeLocalise("CompleteButton.Text.Button", "Complete Registration");
		}

		#endregion

		#region Confirmation

		/// <summary>
		/// Shows the username taken confirmation.
		/// </summary>
		private void ShowUsernameTakenConfirmation()
		{
			Confirmation.Show(CodeLocalise("ErrorValidatingUserName.Title", "Email address is already in use."),
                                                             CodeLocalise("ErrorValidatingUserName.Body", "We have recognized your email address on our system and it shows you have already registered.<br/><br/>Please contact Support at #SUPPORTPHONE# or <a href='mailto:#SUPPORTEMAIL#'>#SUPPORTEMAIL#</a>. Our business hours are between #SUPPORTHOURSOFBUSINESS#"),
															 CodeLocalise("Global.Close.Text", "Close"));                                                                                                                                                                                              
		}

		#endregion

		#region Events

		public event RegistrationCompletedHandler RegistrationCompleted;

		protected virtual void OnRegistrationCompleted(RegistrationCompletedEventArgs e)
		{
			if (RegistrationCompleted != null)
				RegistrationCompleted(this, e);
		}

		#endregion

		#region Delegates

		public delegate void RegistrationCompletedHandler(object o, RegistrationCompletedEventArgs e);

		#endregion

		#region EventArgs

		public class RegistrationCompletedEventArgs : EventArgs
		{
			public readonly string AccountUsername;
			public readonly string AccountPassword;
			public readonly bool IsValidFederalEmployerIdentificationNumber;
			public readonly long EmployeeId;
			public readonly long UserId;
			public readonly string EmployeeFirstName;
			public readonly string EmployeeEmailAddress;
			public readonly bool IsAlreadyApproved;
			public readonly bool EmployeeIsApproved;

			public RegistrationCompletedEventArgs(string accountUsername, string accountPassword, bool isValidFederalEmployerIdentificationNumber, long employeeId, long userId, string employeeFirstName,
																						string employeeEmailAddress, bool isAlreadyApproved, bool employeeIsApproved)
			{
				AccountUsername = accountUsername;
				AccountPassword = accountPassword;
				IsValidFederalEmployerIdentificationNumber = isValidFederalEmployerIdentificationNumber;
				EmployeeId = employeeId;
				UserId = userId;
				EmployeeFirstName = employeeFirstName;
				EmployeeEmailAddress = employeeEmailAddress;
				IsAlreadyApproved = isAlreadyApproved;
				EmployeeIsApproved = employeeIsApproved;
			}
		}

		#endregion

	}
}