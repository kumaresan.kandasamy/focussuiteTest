<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UploadLogoModal.ascx.cs"
    Inherits="Focus.Web.Code.Controls.User.UploadLogoModal" %>
<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" TargetControlID="ModalDummyTarget"
    PopupControlID="ModalPanel" PopupDragHandleControlID="ModalPanel" RepositionMode="RepositionOnWindowResizeAndScroll"
    BackgroundCssClass="modalBackground" />
<asp:Panel ID="ModalPanel" TabIndex="-1" runat="server" CssClass="modal">
    <table style="width: 350px;" role="presentation">
        <tr>
            <td style="vertical-align: top; color: #27506D">
                <h4>
                    <%= HtmlLocalise("Title.Text", "Upload a Logo") %></h4>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 8px;">
                <%= HtmlInFieldLabel("LogoNameTextBox", "LogoName.Label", "Type a logo name", 330)%>
                <asp:TextBox ID="LogoNameTextBox" runat="server" MaxLength="200" Width="330px" ClientIDMode="Static"
                    AutoCompleteType="Disabled" />
                <asp:RequiredFieldValidator ID="LogoNameRequired" runat="server" ControlToValidate="LogoNameTextBox"
                    Display="None" ValidationGroup="UploadLogoModal" />
            </td>
        </tr>
        <tr>
            <td>
                <label>Upload Logo Async FileUpload
                <act:AsyncFileUpload ID="UploadLogoAsyncFileUpload" runat="server" ClientIDMode="Static"
                    CssClass="asyncfileuploadfix fileupload" ThrobberID="UploadThrobberPanel" OnClientUploadStarted="UploadLogoCheckExtension"
                    OnUploadedComplete="UploadLogoAsyncFileUpload_UploadedComplete" /></label>

                <asp:TextBox ID="UploadedLogoFileType" runat="server" ClientIDMode="Static" title="UploadedLogoFileType" />
                <asp:CustomValidator ID="UploadLogoAsyncFileUploadedLogoFileTypeValidator" runat="server"
                    ControlToValidate="UploadedLogoFileType" ValidationGroup="UploadLogoModal" ClientValidationFunction="UploadLogoAsyncFileUploadLogoFileTypeValidator_Validate"
                    Display="None" />
                <asp:TextBox ID="UploadedLogoIsValid" runat="server" ClientIDMode="Static" title="UploadedLogoIsValid" />
                <asp:CustomValidator ID="UploadLogoAsyncFileUploadedLogoIsValidValidator" runat="server"
                    ControlToValidate="UploadedLogoIsValid" ValidationGroup="UploadLogoModal" ClientValidationFunction="UploadLogoAsyncFileUploadedLogoIsValidValidator_Validate"
                    Display="None" />
                <asp:Panel runat="server" ID="UploadThrobberPanel" EnableViewState="false" Style="display: none;"
                    CssClass="updateprogress">
                    <font size="6">&nbsp;&nbsp;&nbsp;Uploading...</font>
                </asp:Panel>
                <div>
                    <i>
                        <%= HtmlLocalise("UploadFormatAndSize.Label", "Logos must be in GIF, JPEG, or PNG format.<br/>Maximum dimensions {0} x {1}. Maximum file size {2}Kbs.", LogoWidth, LogoHeight, MaxFileSize)%></i></div>
            </td>
        </tr>
        <tr>
            <td>
                <focus:AjaxValidationSummary ID="UploadLogoValidationSummary" runat="server" DisplayMode="List"
                    CssClass="error" ValidationGroup="UploadLogoModal" />
                <br />
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">
                <div class="modalButtonBorder">
                    <asp:Button ID="OkButton" runat="server" SkinID="Button3" CausesValidation="true"
                        ValidationGroup="UploadLogoModal" OnClick="OkButton_Click" />
                    <asp:Literal ID="OkButtonSpacer" runat="server" EnableViewState="false" Text="  " />
                    <asp:Button ID="CloseButton" runat="server" SkinID="Button4" CausesValidation="false"
                        OnClick="CloseButton_Click" />
                </div>
            </td>
        </tr>
        <asp:Panel ID="LogoDisplayPanel" runat="server" ClientIDMode="Static" Visible="False">
            <tr>
                <td style="text-align: left;">
                    <asp:Image ID="Logo" runat="server" AlternateText="Logo Image" />
                </td>
            </tr>
            <tr>
                <td style="text-align: left;">
                    <i>
                        <asp:Label ID="ImageText" runat="server"></asp:Label></i>
                </td>
            </tr>
        </asp:Panel>
    </table>
</asp:Panel>
<script type="text/javascript">
    $(document).ready(function () {
        //WIOA adding attribute title to input element after rendering act:AsyncFileUpload
        $('#UploadLogoAsyncFileUpload input[type="file"]').attr('title', 'UploadLogoAsyncFileUpload');
        UploadLogoStartup();
    });

    function UploadLogoStartup() {
        $('.fileupload input:file').customFileInputInModal('<%= HtmlLocalise("Global.Browse.Text", "Browse") %>', '<%= HtmlLocalise("Global.Change.Text", "Change") %>', '<%= HtmlLocalise("UploadLogoFeedback.Text", "Upload a logo") %>');
        $('.inFieldLabel > label, .inFieldLabelAlt > label').inFieldLabels();
    }

    function UploadLogoUploadComplete(sender, args) {
        var options = { type: "POST",
            url: "<%= UrlBuilder.AjaxService() %>/IsUploadedLogoValid",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (response) {
                var results = response.d;
                $('#UploadedLogoIsValid').val(results);
            }
        };

        $.ajax(options);
    }

    function UploadLogoCheckExtension(sender, args) {
        var filename = args.get_fileName();
        var ext = filename.substring(filename.lastIndexOf(".") + 1).toLowerCase();

        $('#UploadedLogoFileType').val(ext);

        return (ext == 'png' || ext == 'jpg' || ext == 'gif');
    }

    function UploadLogoAsyncFileUploadLogoFileTypeValidator_Validate(source, args) {
        var fileType = $('#UploadedLogoFileType').val();
        args.IsValid = (fileType == 'png' || fileType == 'jpg' || fileType == 'gif');
    }

    function UploadLogoAsyncFileUploadedLogoIsValidValidator_Validate(source, args) {
        var isValid = ($('#UploadedLogoIsValid').val() == "1");
        args.IsValid = isValid;
    }

</script>
