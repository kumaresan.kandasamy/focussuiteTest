﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobApplicationRequirementsModal.ascx.cs" Inherits="Focus.Web.Code.Controls.User.JobApplicationRequirementsModal" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:HiddenField ID="JobRequirementsDummyTarget" runat="server" />

<uc:ConfirmationModal ID="Confirmation" runat="server" Height="250px" Width="400px" OnOkCommand="Confirmation_OkCommand" />

<act:ModalPopupExtender ID="JobRequirementsModal" runat="server" BehaviorID="JobRequirementsModal"
	TargetControlID="JobRequirementsDummyTarget" PopupControlID="JobRequirementsModalPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
</act:ModalPopupExtender>

<asp:Panel ID="JobRequirementsModalPanel" TabIndex="-1" runat="server" CssClass="modal" ClientIDMode="Static" Style="z-index:100001; display:none">
	<asp:Panel ID="panJobRequirements" runat="server" CssClass="jobPostingRequirementsModal">
	  <div>
		  <div style="float:left">
        <h2>
          <%= HtmlLocalise("JobPostingRequirementsTitle.Label", "Refer #CANDIDATETYPE#:LOWER for this job") %>
        </h2>
      </div>
		  <div style="float: right">
			  <input type="image" src="<%= UrlBuilder.CloseIcon() %>" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" width="20" height="20" onclick="$find('JobRequirementsModal').hide();return false;"/>
		  </div>
    </div>
    <div style="clear: both"></div>
    <focus:LocalisedLabel runat="server" ID="JobRequirementsInstructionLabel" CssClass="modalMessage" LocalisationKey="JobRequirementsInstruction.Label" DefaultText="Please indicate whether the #CANDIDATETYPE#:LOWER meets the following requirements. Only applicants who meet these requirements will be considered for this job."/>
	  <br /><br />
		<table style="width:100%;">
		  <tr>
        <th><%=HtmlLocalise("Category.Label", "Category") %></th>
        <th><%=HtmlLocalise("Requirement.Label", "Requirement") %></th>
        <th><%=HtmlLocalise("RequirementMet.Label", "Requirement Met?") %></th>
      </tr>
		  <asp:Repeater runat="server" ID="RequirementsRepeater" OnItemDataBound="RequirementsRepeater_ItemDataBound">
        <ItemTemplate>
          <tr style="vertical-align: top">
            <td><asp:Literal ID="CategoryLabel" runat="server"></asp:Literal></td>
            <td><asp:Literal ID="RequirementLabel" runat="server"></asp:Literal></td>
            <td nowrap="nowrap">
              <asp:RadioButton runat="server" ID="RequirementsMetRadioButton" GroupName="Requirements"/>
              <asp:RadioButton runat="server" ID="RequirementsNotMetRadioButton" GroupName="Requirements"/>
              <asp:RadioButton runat="server" ID="RequirementsWaiveRadioButton" GroupName="Requirements"/>
            </td>
          </tr>
        </ItemTemplate>
		  </asp:Repeater>
    </table>
	  <asp:CustomValidator runat="server" ID="RequirementsValidator" ClientValidationFunction="JobApplicationRequirementsModal_Validate"  
      SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="JobApplicationRequirements"/>
	  <br />
		<div style="text-align: center">
			<asp:Button ID="MeetsRequirementsButton" Text="I meet all of these requirements" runat="server" class="button3" OnClick="MeetsRequirementsButton_Click" ValidationGroup="JobApplicationRequirements" />
			<button class="button4" onclick="$find('JobRequirementsModal').hide(); return false;">
			  <%=HtmlLocalise("CancelReferral.Label", "Cancel")%>
			</button>
		</div>
	</asp:Panel>
</asp:Panel>
<asp:HiddenField runat="server" ID="RequirementsCount"/>

<script type="text/javascript">
  function JobApplicationRequirementsModal_Validate(source, arguments) {
    var radioButtons = $("#<%=panJobRequirements.ClientID %>").find("input[type='radio']");
    var checkedCount = 0;
    var totalNeeded = parseInt($("#<%=RequirementsCount.ClientID %>").val(), 10);
    radioButtons.each(function () {
      if ($(this).is(":checked")) {
        checkedCount++;
      }
    });

    arguments.IsValid = checkedCount == totalNeeded;
  }
</script>