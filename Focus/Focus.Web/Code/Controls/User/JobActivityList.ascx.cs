﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Focus.Common.Models;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Job;
using Focus.Core.DataTransferObjects.FocusCore;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
  public partial class JobActivityList : UserControlBase
	{
		/// <summary>
		/// Gets or sets the job model.
		/// </summary>
		/// <value>The job model.</value>
    public JobTypes JobType
		{
      get { return GetViewStateValue("JobActivityList:JobType", JobTypes.None); }
      set { SetViewStateValue("JobActivityList:JobType", value); }
		}

    /// <summary>
    /// Gets or sets the activity criteria.
    /// </summary>
    /// <value>The activity criteria.</value>
    public JobActivityCriteria ActivityCriteria
    {
      get { return GetViewStateValue<JobActivityCriteria>("JobActivityList:ActivityCriteria"); }
      set { SetViewStateValue("JobActivityList:ActivityCriteria", value); }
    }
    /// <summary>
    /// Gets or sets the job id.
    /// </summary>
    /// <value>The job id.</value>
    public long JobId { get; set; }

    private int _activityCount;
	  private string _jobOrderText;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        if (JobId == 0)
          throw new Exception(FormatError(ErrorTypes.InvalidJob, "Invalid job id"));
        ActivityCriteria = new JobActivityCriteria
                             {
                               JobId = JobId,
                               DaysBack = 30,
                               OrderBy = Constants.SortOrders.DateReceivedDesc,
                               FetchOption = CriteriaBase.FetchOptions.PagedList
                             };

        BindDaysBackDropdown();
        BindServiceDropDown();
        BindUserDropDown();
        BindJobActivityList();
        FormatSortingElements();
      }

			_jobOrderText = (App.Settings.Theme == FocusThemes.Workforce || JobType == JobTypes.Job) ? CodeLocalise("JobOrder.Label", "#EMPLOYMENTTYPE#") : CodeLocalise("Internship.Label", "Internship");
    }

    #region Data retrieval
    /// <summary>
    /// Gets the job activity.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="orderBy">The order by.</param>
    /// <param name="startRowIndex">Start index of the row.</param>
    /// <param name="maximumRows">The maximum rows.</param>
    /// <returns></returns>
    public List<JobActionEventViewDto> GetJobActivity(JobActivityCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
    {
      var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

      if (criteria.IsNull())
      {
        _activityCount = 0;
        return null;
      }

      criteria.PageSize = maximumRows;
      criteria.PageIndex = pageIndex;

      var activites = ServiceClientLocator.JobClient(App).GetJobActivities(criteria);
      _activityCount = activites.TotalCount;
      return activites;
    }

    /// <summary>
    /// Gets the referral activity count.
    /// </summary>
    /// <returns></returns>
    public int GetJobActivityCount()
    {
      return _activityCount;
    }

    /// <summary>
    /// Gets the referral activity count.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <returns></returns>
    public int GetJobActivityCount(JobActivityCriteria criteria)
    {
      return _activityCount;
    }

    #endregion

    #region Formatting
    /// <summary>
    /// Formats the sorting elements.
    /// </summary>
    private void FormatSortingElements()
    {

      if (!JobActivityListPager.Visible) return;

      var userNameSortAscButton = (ImageButton)JobActivityLogList.FindControl("UserNameSortAscButton");
      var userNameSortDescButton = (ImageButton)JobActivityLogList.FindControl("UserNameSortDescButton");
      var activityDateSortAscButton = (ImageButton)JobActivityLogList.FindControl("ActivityDateSortAscButton");
      var activityDateSortDescButton = (ImageButton)JobActivityLogList.FindControl("ActivityDateSortDescButton");

      userNameSortAscButton.ImageUrl = activityDateSortAscButton.ImageUrl = UrlBuilder.CategorySortAscImage();
      userNameSortDescButton.ImageUrl = activityDateSortDescButton.ImageUrl = UrlBuilder.CategorySortDescImage();

      userNameSortAscButton.Enabled =
        activityDateSortAscButton.Enabled = userNameSortDescButton.Enabled = activityDateSortDescButton.Enabled = true;

      switch (ActivityCriteria.OrderBy)
      {
        case Constants.SortOrders.NameAsc:
          userNameSortAscButton.ImageUrl = UrlBuilder.ActivityListSortAscImage();
          userNameSortAscButton.Enabled = false;
          break;
        case Constants.SortOrders.NameDesc:
          userNameSortDescButton.ImageUrl = UrlBuilder.ActivityListSortDescImage();
          userNameSortDescButton.Enabled = false;
          break;
        case Constants.SortOrders.ActivityDateAsc:
          activityDateSortAscButton.ImageUrl = UrlBuilder.ActivityListSortAscImage();
          activityDateSortAscButton.Enabled = false;
          break;
        default:
          activityDateSortDescButton.ImageUrl = UrlBuilder.ActivityListSortDescImage();
          activityDateSortDescButton.Enabled = false;
          break;
      }
    }
    #endregion

    #region Bind data
    /// <summary>
    /// Binds the job activity list.
    /// </summary>
    private void BindJobActivityList()
    {
      JobActivityLogList.DataBind();

      // Set visibility of other controls
      JobActivityListPager.Visible = (JobActivityListPager.TotalRowCount > 0);
    }

    /// <summary>
    /// Binds the service drop down.
    /// </summary>
    private void BindServiceDropDown()
    {
		    _jobOrderText = (App.Settings.Theme == FocusThemes.Workforce || JobType == JobTypes.Job) ? CodeLocalise("JobOrder.Label", "#EMPLOYMENTTYPE#") : CodeLocalise("Internship.Label", "Internship");
            ServiceDropDown.Items.Clear();
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise("ServiceDropDown.AllActivities", "All activities")));


            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.PostJob, _jobOrderText + " activated"), ActionTypes.PostJob.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.ApprovePostingReferral, _jobOrderText + " approved"), ActionTypes.ApprovePostingReferral.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.CloseJob, _jobOrderText + " closed"), ActionTypes.CloseJob.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.CreateJob, _jobOrderText + " drafted"), ActionTypes.CreateJob.ToString()));
			ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.SaveJob, _jobOrderText + " edited"), ActionTypes.SaveJob.ToString()));
			ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.HoldJob, _jobOrderText + " on hold"), ActionTypes.HoldJob.ToString()));
      
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.ReactivateJob, _jobOrderText + " reactivated"), ActionTypes.ReactivateJob.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.RefreshJob, _jobOrderText + " refreshed"), ActionTypes.RefreshJob.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.ApproveCandidateReferral, "Referral approved"), ActionTypes.ApproveCandidateReferral.ToString()));
            if (App.Settings.Theme == FocusThemes.Workforce)
                ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AutoApprovedReferralBypass, "Referral auto-approved"), ActionTypes.AutoApprovedReferralBypass.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.UpdateReferralStatusToAutoDenied, "Referral auto-denied"), ActionTypes.UpdateReferralStatusToAutoDenied.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.DenyCandidateReferral, "Referral denied"), ActionTypes.DenyCandidateReferral.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.HoldCandidateReferral, "Referral on hold"), ActionTypes.HoldCandidateReferral.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.ReferralRequest, "Referral pending review"), ActionTypes.ReferralRequest.ToString()));

            if (App.Settings.Theme == FocusThemes.Workforce)
                ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.ApproveCandidateReferral, "Referral reconsidered"), ActionTypes.ReapplyReferralRequest.ToString()));
              
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.SelfReferral, "Self-referral (regular)"), ActionTypes.SelfReferral.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.CreateCandidateApplication, "Staff referral (regular)"), ActionTypes.CreateCandidateApplication.ToString()));
        	ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.InviteJobSeekerToApply, "Job recommendation sent"), ActionTypes.InviteJobSeekerToApply.ToString()));

			if (App.Settings.TalentModulePresent)
			    ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.InviteJobSeekerToApplyThroughTalent, "Invitation to apply sent"), ActionTypes.InviteJobSeekerToApplyThroughTalent.ToString()));

    }

    /// <summary>
    /// Binds the days back dropdown.
    /// </summary>
    private void BindDaysBackDropdown()
    {
      DaysBackDropDown.Items.Clear();
      DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.TheLastDay", "the last day"), "1"));
      DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.The7Days", "last 7 days"), "7"));
      DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.The14Days", "last 14 days"), "14"));
      DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.The30Days", "last 30 days"), "30"));
      DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.The60Days", "last 60 days"), "60"));
      DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.The90Days", "last 90 days"), "90"));
      DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.The180Days", "last 180 days"), "180"));
			DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.AllDays", "all"), "0"));
      DaysBackDropDown.SelectedIndex = 3;
    }

    /// <summary>
    /// Binds the user drop down.
    /// </summary>
    private void BindUserDropDown()
    {
      UserDropDown.Items.Clear();
      UserDropDown.Items.Add(new ListItem(CodeLocalise("UserDropDown.AllUsers", "all users")));
      var users = ServiceClientLocator.JobClient(App).GetJobActivityUsers(JobId);
      if (users.IsNullOrEmpty())
        return;

      foreach (var user in users)
      {
        UserDropDown.Items.Add(new ListItem(user.LastName + ", " + user.FirstName, user.UserId.ToString()));
      }
    }

		/// <summary>
		/// Binds the action description.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="label">The control.</param>
		/// <param name="userNameDisplay">The user name display.</param>
    private void BindActionDescription(JobActionEventViewDto action, Label label, HtmlTableCell userNameDisplay)
    {
      ActionTypes actionType;
      if (!Enum.TryParse(action.ActionName, out actionType)) return;

      switch (actionType)
      {
        case ActionTypes.ApproveCandidateReferral:
          label.Text = CodeLocalise(actionType + ".ActionDescription", "Referral approved");
          break;

				case ActionTypes.DenyCandidateReferral:
					label.Text = CodeLocalise(actionType + ".ActionDescription", "Referral denied");
					break;

                case ActionTypes.HoldCandidateReferral:
                    label.Text = CodeLocalise(actionType + ".ActionDescription", "Referral on hold");
                    break;

				case ActionTypes.UpdateReferralStatusToAutoDenied:
					label.Text = CodeLocalise(actionType + ".ActionDescription", "Referral auto-denied");
					break;

				case ActionTypes.CreateCandidateApplication:
          label.Text = CodeLocalise(actionType + ".ActionDescription", "Staff referral (regular)");
          break;

        case ActionTypes.WaivedRequirements:
          label.Text = action.AdditionalName.IsNotNullOrEmpty()
            ? CodeLocalise(actionType + ".ActionDescription", "Waived requirements for {0}", action.AdditionalName)
            : CodeLocalise(actionType + ".ActionDescription", "Waived requirements");

          break;

        case ActionTypes.SelfReferral:
          label.Text = CodeLocalise(actionType + ".ActionDescription", "Self-referral (regular)");
          break;

        case ActionTypes.ReferralRequest:
          label.Text = CodeLocalise(actionType + ".ActionDescription", "Referral pending review");
          break;

        case ActionTypes.CreateJob:
					label.Text = CodeLocalise(actionType + ".ActionDescription", _jobOrderText + " drafted");
					break;

        case ActionTypes.PostJob:
                    label.Text = CodeLocalise(actionType + ".ActionDescription", _jobOrderText + " activated");
					break;

        case ActionTypes.SaveJob:
					label.Text = CodeLocalise(actionType + ".ActionDescription", _jobOrderText + " edited");
					break;

        case ActionTypes.RefreshJob:
					label.Text = CodeLocalise(actionType + ".ActionDescription", _jobOrderText + " refreshed");
					break;

        case ActionTypes.ReactivateJob:
					label.Text = CodeLocalise(actionType + ".ActionDescription", _jobOrderText + " reactivated");
					break;

        case ActionTypes.ApprovePostingReferral:
					label.Text = CodeLocalise(actionType + ".ActionDescription", _jobOrderText + " approved");
					break;

        case ActionTypes.HoldJob:
					label.Text = CodeLocalise(actionType + ".ActionDescription", _jobOrderText + " on hold");
					break;

        case ActionTypes.CloseJob:
					label.Text = CodeLocalise(actionType + ".ActionDescription", _jobOrderText + " closed");
					break;

				case ActionTypes.ReapplyReferralRequest:
                    label.Text = CodeLocalise(actionType + ".ActionDescription", "Referral reconsidered");
					break;

				case ActionTypes.AutoApprovedReferralBypass:
                    label.Text = CodeLocalise(actionType + ".ActionDescription", "Referral auto-approved {0}", action.AdditionalName);
		      break;

				case ActionTypes.AutoResolvedIssue:
					label.Text = CodeLocalise(actionType + ".ActionDescription", "Flagged issue auto-resolved");
					break;

				case ActionTypes.InviteJobSeekerToApply:
					if (action.EntityIdAdditional02 == 1)
					{
						label.Text = CodeLocalise(actionType + ".ActionDescriptionQueued", "Job recommendation is queued {0}",
																			action.AdditionalName.IsNotNullOrEmpty() ? string.Concat("(", action.AdditionalName, ")") : string.Empty);
					}
					else
					{
						label.Text = CodeLocalise(actionType + ".ActionDescription", "Job recommendation sent {0}",
																			action.AdditionalName.IsNotNullOrEmpty() ? string.Concat("(", action.AdditionalName, ")") : string.Empty);
					}
					break;

				case ActionTypes.InviteJobSeekerToApplyThroughTalent:
		      if (action.EntityIdAdditional02 == 1)
		      {
						label.Text = CodeLocalise(actionType + ".ActionDescriptionQueued", "Invitation to apply is queued {0}",
			                                action.AdditionalName.IsNotNullOrEmpty() ? string.Concat("(", action.AdditionalName, ")") : string.Empty);
		      }
		      else
		      {
			      label.Text = CodeLocalise(actionType + ".ActionDescription", "Invitation to apply sent {0}",
			                                action.AdditionalName.IsNotNullOrEmpty() ? string.Concat("(", action.AdditionalName, ")") : string.Empty);
		      }
		      break;
      }
			
      if (actionType!= ActionTypes.ExternalReferral  && action.AdditionalDetails.IsNotNullOrEmpty())
        label.Text = String.Format("{0} ({1})", label.Text, action.AdditionalDetails);
    }

  #endregion
		
    # region Events

    /// <summary>
    /// Handles the Selecting event of the JobActivityListDataSource control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
    protected void JobActivityListDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
      e.InputParameters["criteria"] = ActivityCriteria;
    }

    /// <summary>
    /// Handles the Sorting event of the JobActivityList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
    protected void JobActivityList_Sorting(object sender, ListViewSortEventArgs e)
    {
      ActivityCriteria.OrderBy = e.SortExpression;
      FormatSortingElements();
    }

    /// <summary>
    /// Handles the LayoutCreated event of the JobActivityList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void JobActivityList_LayoutCreated(object sender, EventArgs e)
    {
      // Set labels
      ((Literal)JobActivityLogList.FindControl("UserName")).Text = CodeLocalise("UserName.Text", "USER NAME");
      ((Literal)JobActivityLogList.FindControl("ActivityDate")).Text = CodeLocalise("ActivityDate.Text", "DATE");
      ((Literal)JobActivityLogList.FindControl("Action")).Text = CodeLocalise("Action.Text", "ACTIVITY/SERVICE");
    }

    protected void DaysBackDropdown_SelectedIndexChanged(object sender, EventArgs e)
    {
      ActivityCriteria.DaysBack = Convert.ToInt32(DaysBackDropDown.SelectedValue);
      JobActivityListPager.ReturnToFirstPage();
      BindJobActivityList();
      FormatSortingElements();
    }

    /// <summary>
    /// Handles the SelectedIndexChanged event of the UserDropDown control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void UserDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (UserDropDown.SelectedIndex == 0)
        ActivityCriteria.UserId = null;
      else
        ActivityCriteria.UserId = Convert.ToInt64(UserDropDown.SelectedValue);

      JobActivityListPager.ReturnToFirstPage();
      BindJobActivityList();
      FormatSortingElements();
    }

    /// <summary>
    /// Handles the SelectedIndexChanged event of the ServiceDropDown control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void ServiceDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (ServiceDropDown.SelectedIndex == 0)
        ActivityCriteria.Action = null;
      else
      {
        ActionTypes actionType;
        Enum.TryParse(ServiceDropDown.SelectedValue, out actionType);
        ActivityCriteria.Action = actionType;
      }

      JobActivityListPager.ReturnToFirstPage();
      BindJobActivityList();
      FormatSortingElements();
    }

    /// <summary>
    /// Handles the ItemDataBound event of the JobActivityList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
    protected void JobActivityList_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
      var action = (JobActionEventViewDto)e.Item.DataItem;
      var literal = (Label)e.Item.FindControl("ActionDescription");
      var userNameDisplay = (HtmlTableCell)e.Item.FindControl("UsernameDisplay");

      BindActionDescription(action, literal, userNameDisplay);
    }


#endregion
		
  }
}