#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Drawing;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Web.Code.Controls.User

{
	public partial class UploadLogoModal : UserControlBase
	{
		#region Properties

		public long BusinessUnitId
		{
			get { return GetViewStateValue<long>("UploadLogo:BusinessUnitId", -1); }
			set { SetViewStateValue("UploadLogo:BusinessUnitId", value); }
		}

		/// <summary>
		/// Gets or sets the width of the logo.
		/// </summary>
		public int LogoWidth
		{
			get { return GetViewStateValue("UploadLogo:LogoWidth", 450); }
			set { SetViewStateValue("UploadLogo:LogoWidth", value); }
		}

		/// <summary>
		/// Gets or sets the height of the logo.
		/// </summary>
		public int LogoHeight
		{
			get { return GetViewStateValue("UploadLogo:LogoHeight", 113); }
			set { SetViewStateValue("UploadLogo:LogoHeight", value); }
		}

		/// <summary>
		/// Gets or sets the size of the max file.
		/// </summary>
		public int MaxFileSize
		{
			get { return GetViewStateValue("UploadLogo:MaxFileSize", 100); }
			set { SetViewStateValue("UploadLogo:MaxFileSize", value); }
		}

    internal byte[] ImageBytes
    {
      get { return GetViewStateValue<byte[]>("UploadLogo:ImageBytes"); }
      set { SetViewStateValue("UploadLogo:ImageBytes", value); }
    }

    
		#endregion

		public delegate void OkClickHandler(object sender, CommandEventArgs eventArgs);
		public event OkClickHandler OkClick;

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			// Localise some buttons
			OkButton.Text = CodeLocalise("OkButton.Text", "Save logo");
			CloseButton.Text = CodeLocalise("CloseButton.Text", "Cancel");

			// Localise some error messages
			LogoNameRequired.ErrorMessage = CodeLocalise("LogoName.Required", "Logo name is required.");
		  UploadLogoAsyncFileUploadedLogoFileTypeValidator.ValidateEmptyText = true;
			UploadLogoAsyncFileUploadedLogoFileTypeValidator.ErrorMessage = CodeLocalise("UploadLogoAsyncFileUpload.InvalidFileType", "Logo must be in GIF, JPEG, or PNG format.");
			UploadLogoAsyncFileUploadedLogoIsValidValidator.ErrorMessage = CodeLocalise("UploadLogoAsyncFileUpload.InvalidFileSize", "Logo has invalid dimensions or is too big.");

			// Hide the spoof textboxes
			UploadedLogoFileType.Style.Add("display", "none");
			UploadedLogoIsValid.Style.Add("display", "none");

      
      if(IsPostBack)
      {
        LogoDisplayPanel.Visible = false;
      }
		}

		/// <summary>
		/// Shows the supload logo modal.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		public void Show(long businessUnitId)
		{
			// Store the Business Unit Id
			BusinessUnitId = businessUnitId;

			// Ensure Logo Name is blank - clean re-entry
			LogoNameTextBox.Text = "";
			
			ModalPopup.Show();

			ScriptManager.RegisterClientScriptBlock(this, typeof(UploadLogoModal), "UploadLogoJavaScript", "function pageLoad() { UploadLogoStartup(); }", true);
		}

		#region Button Events

		/// <summary>
		/// Handles the Click event of the OkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void OkButton_Click(object sender, EventArgs e)
		{
		  var isValid = App.GetSessionValue<bool>(Constants.StateKeys.UploadedLogoIsValid);
      var bytes = App.GetSessionValue<byte[]>(Constants.StateKeys.UploadedLogo);

      if (isValid)
      {
				var logoId = ServiceClientLocator.EmployerClient(App).SaveBusinessUnitLogo(new BusinessUnitLogoDto { BusinessUnitId = BusinessUnitId, Name = LogoNameTextBox.Text, Logo = bytes });

        // Raise the OK event so that the calling code can get what employer logo id was added
        OnOkClick(new CommandEventArgs("OK", logoId));

        // Hide me!
        ModalPopup.Hide();

      }
      else
      {
        var img = ScaleImage(bytes, LogoHeight, LogoWidth);
        var imgBytes = ConvertImageToByteArray(img);
        App.SetSessionValue(Constants.StateKeys.UploadedLogo, imgBytes);
        string base64String = Convert.ToBase64String(imgBytes, 0, imgBytes.Length);
        Logo.ImageUrl = "data:image/png;base64," + base64String;
        Logo.Width = img.Width;
        Logo.Height = img.Height;
        LogoDisplayPanel.Visible = true;

        if(imgBytes.Length < MaxFileSize * 1024)
        {
          ImageText.Text = CodeLocalise("ImageResized.Text",
                                        "The image exceeded the maximum file dimensions/size and has been resized.<br>Please click save again to save the resized image or upload a new image.");
         
          App.SetSessionValue(Constants.StateKeys.UploadedLogoIsValid, true);
         
        }
        else
        {
          ImageText.Text = CodeLocalise("ImageResized.Text",
                                        "The image exceeded the maximum file dimensions/size and has been resized but still exceeds maximum file size.<br>Please choose another image.");
        }
        ModalPopup.Show();

      }

		}

		/// <summary>
		/// Handles the Click event of the CloseButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CloseButton_Click(object sender, EventArgs e)
		{
			// Hide me!
			ModalPopup.Hide();
		}

		#endregion

		#region Raised Events

		/// <summary>
		/// Raises the <see cref="E:OkClick"/> event.
		/// </summary>
		/// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected virtual void OnOkClick(CommandEventArgs eventArgs)
		{
			if (OkClick != null)
				OkClick(this, eventArgs);
		}

		#endregion

		#region Ajax Events

		/// <summary>
		/// Handles the UploadedComplete event of the UploadLogoAsyncFileUpload control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="AjaxControlToolkit.AsyncFileUploadEventArgs"/> instance containing the event data.</param>
		public void UploadLogoAsyncFileUpload_UploadedComplete(object sender, AsyncFileUploadEventArgs e)
		{
			var bytes = UploadLogoAsyncFileUpload.FileBytes;

      if (bytes == null || bytes.Length > (MaxFileSize * 1024) || !IsValidDimensions(bytes))
      {
       App.SetSessionValue(Constants.StateKeys.UploadedLogoIsValid, false);
      }
      else
      {
        App.SetSessionValue(Constants.StateKeys.UploadedLogoIsValid, true);
     
      }

      App.SetSessionValue(Constants.StateKeys.UploadedLogo, bytes);
      UploadLogoAsyncFileUpload.ClearAllFilesFromPersistedStore();
		}

		#endregion

		#region Helper Methods

		/// <summary>
		/// Converts the image to byte array.
		/// </summary>
		/// <param name="image">The image.</param>
		/// <returns></returns>
    public byte[] ConvertImageToByteArray(System.Drawing.Image image)
    {
      using (var ms = new MemoryStream())
      {
        image.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
        // or whatever output format you like
        return ms.ToArray();
      }
    }
		
		/// <summary>
		/// Scales the image.
		/// </summary>
		/// <param name="bytes">The bytes.</param>
		/// <param name="maxHeight">Height of the max.</param>
		/// <param name="maxWidth">Width of the max.</param>
		/// <returns></returns>
    private System.Drawing.Image ScaleImage(byte[] bytes, int maxHeight, int maxWidth)
    {

      using (var memoryStream = new MemoryStream(bytes))
      {
        using (var img = System.Drawing.Image.FromStream(memoryStream))
        {
          var ratio = (double)maxHeight / maxWidth;

          var newWidth = (int)(img.Width * ratio);
          var newHeight = (int)(img.Height * ratio);

          var newImage = new Bitmap(newWidth, newHeight);
          using (var g = Graphics.FromImage(newImage))
          {
            g.DrawImage(img, 0, 0, newWidth, newHeight);
          }
          return newImage;
        }
      } 
    }

		/// <summary>
		/// Determines whether the image has valid dimensions.
		/// </summary>
		/// <param name="bytes">The bytes.</param>
		/// <returns>
		/// 	<c>true</c> if the image dimensions are valid; otherwise, <c>false</c>.
		/// </returns>
		private bool IsValidDimensions(byte[] bytes)
		{
			try
			{
				using (var memoryStream = new MemoryStream(bytes))
				{
					using (var img = System.Drawing.Image.FromStream(memoryStream))
					{
						var width = img.Width;
						var height = img.Height;

						return (width <= LogoWidth && height <= LogoHeight);
					}
				} 
			}

			catch
			{
				return false;
			}
		}

		#endregion
	}
}