﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdateableClientList.ascx.cs" Inherits="Focus.Web.Code.Controls.User.UpdateableClientList" %>

<div runat="server" id="UpdateableClientListWrapper" class="updateableClientList">
	<div>
		<focus:LocalisedLabel runat="server" ID="ItemAutoCompleteTextBoxLabel" AssociatedControlID="ItemAutoCompleteTextBox" LocalisationKey="AutoComplete" DefaultText="Auto complete" CssClass="sr-only"/>
    <focus:AutoCompleteTextBox ID="ItemAutoCompleteTextBox" runat="server" data-bind="value: newListItem().label" AllowFreeInput="False" AutoCompleteType="Disabled" AutoPostBack="False" OnClientItemSelected="focusSuite.updateableClientList.AutoCompleteItemSelected(hiddenSelectedClientId, ui.item)" />
    
		<focus:LocalisedLabel runat="server" ID="ItemTextBoxLabel" AssociatedControlID="ItemTextBox" LocalisationKey="Item" DefaultText="Item" CssClass="sr-only"/>    
		<asp:TextBox runat="server" ID="ItemTextBox" MaxLength="50" data-bind="value: newListItem().value" ></asp:TextBox>
		<focus:LocalisedLabel runat="server" ID="ItemDropDownListLabel" AssociatedControlID="ItemDropDownList" LocalisationKey="Item" DefaultText="Item" CssClass="sr-only"/>    
		<select runat="server" id="ItemDropDownList" data-bind="value: newListItem().value, options: selectableItems, optionsText: 'label', optionsValue: 'value', optionsCaption: dropDownListCaption()" ></select>
		<input type="button" class="button3" runat="server" id="ItemButton" name="ItemButton" title="ItemButton" style="font-size:20px;" data-bind="click: addListItem, value: addButtonText, css: addButtonClass, disable: addButtonDisabled"/>
	</div>
	<div><asp:RegularExpressionValidator ID="ItemTextBoxRegexValidator" runat="server" CssClass="error" ControlToValidate="ItemTextBox" Display="Dynamic"/></div>
	<div><asp:CompareValidator ID="ItemTextBoxCompareValidator" runat="server" ControlToValidate="ItemTextBox" CssClass="error" Display="Dynamic" /></div>
	<div class="error" data-bind="visible: addButtonDisabled" style="display: none"><%=HtmlLocalise("ItemTextBoxMaxItemValidator.ErrorMessage", "You have reached the limit to search by this criteria")%></div>

	<div data-bind="visible: listItems().length > 0" class="selected-items" style="display: none">
		<asp:Label runat="server" ID="SelectionBoxTitleLabel" Visible="False" CssClass="list-title"></asp:Label>
		<ul data-bind="foreach: listItems">
			<li>
				<span class="list-item-default" data-bind="visible: $root.enableDefaultSelection">
					<img data-bind="click: $root.toggleDefaultItem, dynamicDefaultIcon: isDefault, dynamicDefaultIconOptions: { defaultIcon: '<%= UrlBuilder.ButtonDefaultIconOn() %>', defaultToolTip: '<%= HtmlLocalise("Tooltip.IsDefault", "This is the default") %>', setAsDefaultIcon: '<%= UrlBuilder.ButtonDefaultIconOff() %>', setAsDefaultToolTip: '<%= HtmlLocalise("Tooltip.SetDefault", "Set this as the default") %>'}" alt="."  src="<%= UrlBuilder.ButtonDefaultIconOff() %>"/>
				</span>
				<span class="list-item" data-bind="text: label"></span>
				<span class="list-item-delete">
					<asp:Image runat="server" ID="DeleteIcon" data-bind="click: $root.removeListItem, tooltipster: $root.removeIconToolTip" alt="Delete icon"/>
				</span>
		 </li>
		</ul>
		<br class="clear"/>
		<input type="hidden" id="HiddenItemList" runat="server" data-bind="attr: { value: listItemsHiddenField }"/>
	</div>
	<br class="clear"/>
</div>
<asp:PlaceHolder runat="server" ID="Mask">
<script type="text/javascript">
  Sys.Application.add_load(function () {
  	$("#<%=ItemTextBox.ClientID%>").mask("<%=InputMask%>", { placeholder: '<%= (MaskPromptCharacter == (char)0 ? string.Empty : MaskPromptCharacter.ToString()) %>' });
  });
</script>
</asp:PlaceHolder>