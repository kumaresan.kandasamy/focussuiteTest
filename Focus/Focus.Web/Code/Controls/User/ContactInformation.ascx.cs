﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Globalization;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Core.Models;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class ContactInformation : UserControlBase
	{
		#region Properties

		protected UserDetailsModel UserDetails
		{
			get { return GetViewStateValue<UserDetailsModel>("ContactInformation:UserDetails"); }
			set { SetViewStateValue("ContactInformation:UserDetails", value); }
		}

		#endregion


		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
			}

			EmailAddressRegEx.ValidationExpression = App.Settings.EmailAddressRegExPattern;
			PhoneNumberMaskedEdit.Mask = AlternatePhone1MaskedEdit.Mask = AlternatePhone2MaskEdit.Mask = App.Settings.PhoneNumberMaskPattern;
			PhoneRegEx.ValidationExpression = App.Settings.PhoneNumberRegExPattern;
		  AddressPostcodeRegexValidator.ValidationExpression = App.Settings.ExtendedPostalCodeRegExPattern;

		  JobTitleRequired.Enabled = App.Settings.JobTitleRequired;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
		  JobTitleHeader.Text = App.Settings.JobTitleRequired
		                          ? HtmlRequiredLabel(JobTitleTextBox, "JobTitle.Label", "Job title")
		                          : HtmlLabel(JobTitleTextBox, "JobTitle.Label", "Job title");

			FirstNameRequired.ErrorMessage = CodeLocalise("FirstName.RequiredErrorMessage", "First name is required");
			LastNameRequired.ErrorMessage = CodeLocalise("LastName.RequiredErrorMessage", "Last name is required");
			PersonalTitleRequired.ErrorMessage = CodeLocalise("PersonalTitle.RequiredErrorMessage", "Title is required");
			PhoneRegEx.ErrorMessage = CodeLocalise("ContactPhone.RequiredErrorMessage", "A valid phone number is required");
			EmailAddressRequired.ErrorMessage = CodeLocalise("ContactEmailAddress.RequiredErrorMessage", "Email address is required");
      EmailAddressRegEx.ErrorMessage = CodeLocalise("ContactEmailAddress.RegExErrorMessage", "Email address format is invalid");
      AddressPostcodeRegexValidator.ErrorMessage = CodeLocalise("AddressPostcodeRegexValidator.ErrorMessage", "ZIP code must be in a 5 or 9-digit format");
      JobTitleRequired.ErrorMessage = CodeLocalise("JobTitle.RequiredErrorMessage", "Job title is required");
		}

		/// <summary>
		/// Binds the specified user id.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="businessUnitName">Name of the business unit.</param>
		/// <param name="showExternalId"></param>
		/// <param name="showSuffix"></param>
		public UserDetailsModel Bind(long userId, string businessUnitName = "",bool showExternalId = false, bool showSuffix = true)
		{
			BindDropDowns();
		  ResetFields();

			// Get details for user ID
			UserDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(userId);

			#region Bind contact details

		  var personDetails = UserDetails.PersonDetails;
			if (personDetails.IsNotNull())
			{
				if (personDetails.FirstName.IsNotNullOrEmpty()) FirstNameTextBox.Text = personDetails.FirstName;
				if (personDetails.LastName.IsNotNullOrEmpty()) LastNameTextBox.Text = personDetails.LastName;
				if (personDetails.MiddleInitial.IsNotNullOrEmpty()) MiddleInitialTextBox.Text = personDetails.MiddleInitial;
				if (personDetails.TitleId.IsNotNull()) PersonalTitleDropDown.SelectValue(personDetails.TitleId.ToString(CultureInfo.InvariantCulture));
        if (personDetails.SuffixId.IsNotNull() && showSuffix) SuffixDropDown.SelectValue(personDetails.SuffixId.ToString());
				if (personDetails.JobTitle.IsNotNullOrEmpty()) JobTitleTextBox.Text = personDetails.JobTitle;
				if (personDetails.EmailAddress.IsNotNullOrEmpty()) EmailAddressTextBox.Text = personDetails.EmailAddress;
			}

			if (UserDetails.PrimaryPhoneNumber.IsNotNull())
			{
				if (UserDetails.PrimaryPhoneNumber.Number.IsNotNullOrEmpty()) PhoneTextBox.Text = UserDetails.PrimaryPhoneNumber.Number;
				if (UserDetails.PrimaryPhoneNumber.PhoneType.IsNotNull()) PhoneTypeDropDown.SelectValue(UserDetails.PrimaryPhoneNumber.PhoneType.ToString());
				if (UserDetails.PrimaryPhoneNumber.Extension.IsNotNullOrEmpty()) PhoneExtensionTextBox.Text = UserDetails.PrimaryPhoneNumber.Extension;
			}

			if (UserDetails.AlternatePhoneNumber1.IsNotNull())
			{
				if (UserDetails.AlternatePhoneNumber1.Number.IsNotNullOrEmpty()) AlternatePhone1TextBox.Text = UserDetails.AlternatePhoneNumber1.Number;
				if (UserDetails.AlternatePhoneNumber1.PhoneType.IsNotNull()) AlternatePhone1TypeDropDown.SelectValue(UserDetails.AlternatePhoneNumber1.PhoneType.ToString());
			}

			if (UserDetails.AlternatePhoneNumber2.IsNotNull())
			{
				if (UserDetails.AlternatePhoneNumber2.Number.IsNotNullOrEmpty()) AlternatePhone2TextBox.Text = UserDetails.AlternatePhoneNumber2.Number;
				if (UserDetails.AlternatePhoneNumber2.PhoneType.IsNotNull()) AlternatePhone2TypeDropDown.SelectValue(UserDetails.AlternatePhoneNumber2.PhoneType.ToString());
			}

			if (UserDetails.AddressDetails.IsNotNull())
			{
				if (UserDetails.AddressDetails.Line1.IsNotNullOrEmpty()) AddressLine1TextBox.Text = UserDetails.AddressDetails.Line1;
				if (UserDetails.AddressDetails.Line2.IsNotNullOrEmpty()) AddressLine2TextBox.Text = UserDetails.AddressDetails.Line2;
				if (UserDetails.AddressDetails.PostcodeZip.IsNotNullOrEmpty()) AddressPostcodeZipTextBox.Text = UserDetails.AddressDetails.PostcodeZip;
				if (UserDetails.AddressDetails.TownCity.IsNotNullOrEmpty()) AddressTownCityTextBox.Text = UserDetails.AddressDetails.TownCity;
        if (UserDetails.AddressDetails.StateId.IsNotNull() && UserDetails.AddressDetails.StateId > 0) AddressStateDropDownList.SelectedValue = UserDetails.AddressDetails.StateId.ToString(CultureInfo.InvariantCulture);

				var countySelectedValue = UserDetails.AddressDetails.CountyId.IsNotNull() ? UserDetails.AddressDetails.CountyId.ToString() : "";
				AddressCountyDropDownList.SelectValue(countySelectedValue);
				AddressCountyDropDownListCascadingDropDown.SelectedValue = countySelectedValue;

				if (UserDetails.AddressDetails.CountyId.IsNotNull()) AddressCountyDropDownListCascadingDropDown.SelectedValue = UserDetails.AddressDetails.CountyId.ToString();
        if (UserDetails.AddressDetails.CountryId.IsNotNull() && UserDetails.AddressDetails.CountryId > 0) AddressCountryDropDownList.SelectedValue = UserDetails.AddressDetails.CountryId.ToString(CultureInfo.InvariantCulture);
			}

			#endregion

	    if (App.Settings.Module == FocusModules.Assist && App.Settings.ReadOnlyContactInfo && !App.User.IsInRole(Constants.RoleKeys.AssistAccountAdministrator))
	    {
        FirstNameTextBox.ReadOnly =
        LastNameTextBox.ReadOnly =
        MiddleInitialTextBox.ReadOnly =
        JobTitleTextBox.ReadOnly =
        EmailAddressTextBox.ReadOnly =
        PhoneTextBox.ReadOnly =
        PhoneExtensionTextBox.ReadOnly =
        AlternatePhone1TextBox.ReadOnly =
        AlternatePhone2TextBox.ReadOnly =
        AddressLine1TextBox.ReadOnly =
        AddressLine2TextBox.ReadOnly =
        AddressTownCityTextBox.ReadOnly =
        ExternalIdTextBox.ReadOnly =
        AddressPostcodeZipTextBox.ReadOnly = true;
        AddressStateDropDownList.Enabled = 
        AddressCountryDropDownList.Enabled = 
        AddressCountyDropDownListCascadingDropDown.Enabled = 
        AddressCountyDropDownList.Enabled = 
        AlternatePhone2TypeDropDown.Enabled =
        PersonalTitleDropDown.Enabled =
        SuffixDropDown.Enabled =
        PhoneTypeDropDown.Enabled =
        AlternatePhone1TypeDropDown.Enabled = false;
	    }

	    if (showExternalId)
	    {
	      ExternalIdRow.Visible = true;
	      ExternalIdTextBox.Text = UserDetails.UserDetails.ExternalId;
	    }
	    else
        ExternalIdRow.Visible = false;


	    SuffixRow.Visible = showSuffix;

			if (businessUnitName.IsNotNullOrEmpty())
				BusinessUnitNameLabel.Text = businessUnitName;
			else
				BusinessUnitNameRow.Visible = false;

	    return UserDetails;
		}

		/// <summary>
		/// Unbinds the modal.
		/// </summary>
		/// <returns></returns>
		public UserDetailsModel Unbind()
		{
			if (UserDetails.PersonDetails.IsNull()) 
        UserDetails.PersonDetails = new PersonDto();

		  var personDetails = UserDetails.PersonDetails;
			personDetails.FirstName = FirstNameTextBox.TextTrimmed();
			personDetails.LastName = LastNameTextBox.TextTrimmed();
			personDetails.MiddleInitial = MiddleInitialTextBox.TextTrimmed();
      personDetails.TitleId = PersonalTitleDropDown.SelectedValueToLong();
		  personDetails.SuffixId = SuffixDropDown.SelectedValueToNullableLong();
      personDetails.JobTitle = JobTitleTextBox.TextTrimmed();
			personDetails.EmailAddress = EmailAddressTextBox.TextTrimmed();

		  UserDetails.UserDetails.ExternalId = ExternalIdTextBox.TextTrimmed();

			if (UserDetails.PrimaryPhoneNumber.IsNull()) UserDetails.PrimaryPhoneNumber = new PhoneNumberDto { Id = 0 };

			UserDetails.PrimaryPhoneNumber.Number = PhoneTextBox.TextTrimmed();
			UserDetails.PrimaryPhoneNumber.PhoneType = PhoneTypeDropDown.SelectedValueToEnum<PhoneTypes>();
			UserDetails.PrimaryPhoneNumber.IsPrimary = true;
			UserDetails.PrimaryPhoneNumber.Extension = PhoneExtensionTextBox.TextTrimmed();

			if (UserDetails.AlternatePhoneNumber1.IsNull()) UserDetails.AlternatePhoneNumber1 = new PhoneNumberDto { Id = 0 };

			UserDetails.AlternatePhoneNumber1.Number = AlternatePhone1TextBox.TextTrimmed();
			UserDetails.AlternatePhoneNumber1.PhoneType = AlternatePhone1TypeDropDown.SelectedValueToEnum<PhoneTypes>();
			UserDetails.AlternatePhoneNumber1.IsPrimary = false;

			if (UserDetails.AlternatePhoneNumber2.IsNull()) UserDetails.AlternatePhoneNumber2 = new PhoneNumberDto { Id = 0 };

			UserDetails.AlternatePhoneNumber2.Number = AlternatePhone2TextBox.TextTrimmed();
			UserDetails.AlternatePhoneNumber2.PhoneType = AlternatePhone2TypeDropDown.SelectedValueToEnum<PhoneTypes>();
			UserDetails.AlternatePhoneNumber2.IsPrimary = false;

			if (UserDetails.AddressDetails.IsNull()) UserDetails.AddressDetails = new PersonAddressDto();

			UserDetails.AddressDetails.Line1 = AddressLine1TextBox.TextTrimmed();
			UserDetails.AddressDetails.Line2 = AddressLine2TextBox.TextTrimmed();
			UserDetails.AddressDetails.TownCity = AddressTownCityTextBox.TextTrimmed();
			UserDetails.AddressDetails.StateId = AddressStateDropDownList.SelectedValueToLong();
			UserDetails.AddressDetails.CountyId = AddressCountyDropDownList.SelectedValueToLong();
			UserDetails.AddressDetails.CountryId = AddressCountryDropDownList.SelectedValueToLong();
			UserDetails.AddressDetails.PostcodeZip = AddressPostcodeZipTextBox.TextTrimmed();

			return UserDetails;
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void BindDropDowns()
		{
      PersonalTitleDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Titles), null, CodeLocalise("Global.PersonalTitle.TopDefault", "- select -"));
      SuffixDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Suffixes), null, CodeLocalise("SuffixDropDown.TopDefault", "- select suffix -"));

			BindContactTypeDropDown(PhoneTypeDropDown);
			BindContactTypeDropDown(AlternatePhone1TypeDropDown);
			BindContactTypeDropDown(AlternatePhone2TypeDropDown);

			// State
			AddressStateDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States), null, CodeLocalise("Global.State.TopDefault", "- select state -"));

			// Countries
			AddressCountryDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries), null, CodeLocalise("Global.Country.TopDefault", "- select country -"));

			//AddressCountyDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Counties), null, CodeLocalise("Global.County.TopDefault", "- select county -"));
      AddressCountyDropDownList.Items.Insert(0, new ListItem(CodeLocalise("Global.Country.TopDefault", "- select country -"), string.Empty));
			AddressCountyDropDownListCascadingDropDown.PromptText = CodeLocalise("Global.County.TopDefault", "- select county -");
			AddressCountyDropDownListCascadingDropDown.LoadingText = CodeLocalise("Global.County.Progress", "[Loading counties ...]");
			AddressCountyDropDownListCascadingDropDown.PromptValue = string.Empty;
		}

		/// <summary>
		/// Binds the contact type drop down.
		/// </summary>
		/// <param name="dropDownList">The drop down list.</param>
		private void BindContactTypeDropDown(DropDownList dropDownList)
		{
			dropDownList.Items.Clear();

			dropDownList.Items.AddEnum(PhoneTypes.Phone, "landline");
			dropDownList.Items.AddEnum(PhoneTypes.Mobile, "mobile");
			dropDownList.Items.AddEnum(PhoneTypes.Fax, "fax");
			dropDownList.Items.AddEnum(PhoneTypes.Other, "other");
		}

		/// <summary>
		/// Resets the fields on the form
		/// </summary>
		private void ResetFields()
		{
			FirstNameTextBox.Text =
				LastNameTextBox.Text =
				MiddleInitialTextBox.Text =
				JobTitleTextBox.Text =
				EmailAddressTextBox.Text = 
				PhoneTextBox.Text =
				PhoneExtensionTextBox.Text = 
				AlternatePhone1TextBox.Text =
				AlternatePhone2TextBox.Text =
				AddressLine1TextBox.Text =
				AddressLine2TextBox.Text=
				AddressPostcodeZipTextBox.Text =
				AddressTownCityTextBox.Text = string.Empty;

			PersonalTitleDropDown.SelectedIndex = 
				SuffixDropDown.SelectedIndex = 
				PhoneTypeDropDown.SelectedIndex = 
				AlternatePhone1TypeDropDown.SelectedIndex =
				AlternatePhone2TypeDropDown.SelectedIndex =
				AddressStateDropDownList.SelectedIndex =
				AddressCountyDropDownList.SelectedIndex =
				AddressCountryDropDownList.SelectedIndex = 0;
		}
	}
}