<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployerRegistrationStep3.ascx.cs" Inherits="Focus.Web.Code.Controls.User.EmployerRegistrationStep3" %>

<asp:Label ID="FEINExistsInstructionsLabel" runat="server" /><br />
<ul id="ExtraInstructionsList" runat="server" Visible="False">
  <asp:Repeater runat="server" ID="ExtraInstructionsListItems">
    <ItemTemplate>
      <li><%#Container.DataItem%></li>
    </ItemTemplate>
  </asp:Repeater>
</ul>
<br />
<div>
  <p><%= HtmlRequiredFieldsInstruction() %></p>
  <div style="float:left;width: 50%">  
	  <table class="formTable" role="presentation">
	     <asp:PlaceHolder runat="server" ID="LegalNamePlaceHolder">
		    <tr>
			    <td class="label"><%= HtmlRequiredLabel(LegalNameTextBox, "LegalName.Label", "#BUSINESS# legal name")%></td>
			    <td>
				    <asp:TextBox ID="LegalNameTextBox" runat="server" ClientIDMode="Static" Width="98%" TabIndex="1" MaxLength="200" />
				    <asp:RequiredFieldValidator ID="LegalNameRequired" runat="server" ControlToValidate="LegalNameTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			    </td>
		    </tr>
      </asp:PlaceHolder>
		  <tr>
			  <td class="label"><%= HtmlRequiredLabel(CompanyNameTextBox, "CompanyName.Label", "#BUSINESS# name")%></td>
			  <td>
				  <asp:TextBox ID="CompanyNameTextBox" runat="server" ClientIDMode="Static" Width="98%" TabIndex="1" MaxLength="200" />
				  <asp:RequiredFieldValidator ID="CompanyNameRequired" runat="server" ControlToValidate="CompanyNameTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			  </td>
		  </tr>
      <asp:PlaceHolder runat="server" ID="EducationZipCodePlaceHolder">
        <tr>
          <td class="label"><%= HtmlRequiredLabel(EducationAddressPostcodeZipTextBox, "AddressPostcodeZip.Label", "ZIP or postal code")%></td>
          <td>
				    <asp:TextBox ID="EducationAddressPostcodeZipTextBox" runat="server" ClientIDMode="Static" width="90px" TabIndex="3" MaxLength="10" /> 
            <asp:Button runat="server" ID="CheckCompanyButton" TabIndex="4" CausesValidation="True" ValidationGroup="CheckCompany" CssClass="button4" OnClick="CheckBusinessUnitButton_OnClick" />
				    <asp:RequiredFieldValidator ID="EducationAddressPostcodeZipRequired" ClientIDMode="Static" runat="server" ControlToValidate="EducationAddressPostcodeZipTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
            <asp:RequiredFieldValidator ID="EducationAddressPostcodeZipOnCheckRequired" ValidationGroup="CheckCompany" runat="server" ControlToValidate="EducationAddressPostcodeZipTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
            <asp:RegularExpressionValidator ID="EducationAddressPostcodeRegexValidator" runat="server" CssClass="error" ControlToValidate="EducationAddressPostcodeZipTextBox" SetFocusOnError="true" Display="Dynamic" />
			    </td>
        </tr>
      </asp:PlaceHolder>
		  <tr>
			  <td class="label"><%= HtmlRequiredLabel(AddressLine1TextBox, "AddressLine1.Label", "Address")%></td>
			  <td>
				  <asp:TextBox ID="AddressLine1TextBox" runat="server" ClientIDMode="Static"  Width="98%" TabIndex="5" MaxLength="200" />
				  <asp:RequiredFieldValidator ID="AddressLine1Required" runat="server" ControlToValidate="AddressLine1TextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			  </td>
		  </tr>
		  <tr>
			  <%--<td class="label"><%= HtmlLabel("AddressLine2", "")%></td>--%>
              <td class="label"><label for="AddressLine2TextBox" class="hidden">Address Line 2</label></td>
			  <td>
				  <asp:TextBox ID="AddressLine2TextBox" runat="server" ClientIDMode="Static" Width="98%" TabIndex="6" MaxLength="200" />
			  </td>
		  </tr>
      <asp:PlaceHolder runat="server" ID="WorkforceZipCodePlaceHolder" >
		  <tr>
			  <td class="label"><%= HtmlRequiredLabel(AddressPostcodeZipTextBox, "AddressPostcodeZip.Label", "ZIP or postal code")%></td>
			  <td>
				  <asp:TextBox ID="AddressPostcodeZipTextBox" runat="server" ClientIDMode="Static" width="90px" TabIndex="7" MaxLength="10" />
				  <asp:RequiredFieldValidator ID="AddressPostcodeZipRequired" ClientIDMode="Static" runat="server" ControlToValidate="AddressPostcodeZipTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
          <asp:RegularExpressionValidator ID="AddressPostcodeRegexValidator" runat="server" CssClass="error" ControlToValidate="AddressPostcodeZipTextBox" SetFocusOnError="true" Display="Dynamic" />
			  </td>
		  </tr>
      </asp:PlaceHolder>
		  <tr>
			  <td class="label"><%= HtmlRequiredLabel(AddressTownCityTextBox, "AddressTownCity.Label", "City")%></td>
			  <td>
				  <asp:TextBox ID="AddressTownCityTextBox" runat="server" ClientIDMode="Static" Width="98%" TabIndex="8" MaxLength="100" />
				  <asp:RequiredFieldValidator ID="AddressTownCityRequired" runat="server" ControlToValidate="AddressTownCityTextBox" ClientValidationFunction="ValidateZipCode" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			  </td>
		  </tr>
		  <tr>
			  <td class="label"><focus:LocalisedLabel runat="server" ID="StateLabel" AssociatedControlID="AddressStateDropDownList" CssClass="dataInputLabel requiredData" LocalisationKey="State.Label" DefaultText="State"/></td>
			  <td>
				  <asp:DropDownList ID="AddressStateDropDownList" runat="server" ClientIDMode="Static" onchange="UpdateCountry();" Width="100%" TabIndex="9" />
				  <asp:RequiredFieldValidator ID="AddressStateRequired" runat="server" ControlToValidate="AddressStateDropDownList" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			  </td>
		  </tr>
      <asp:PlaceHolder runat="server" ID="CountyRowPlaceHolder">
		    <tr>
			    <td class="label"><%= HtmlRequiredLabel(AddressCountyDropDownList, "AddressCounty.Label", "County")%></td>
			    <td>
            <focus:AjaxDropDownList ID="AddressCountyDropDownList" runat="server" onchange="UpdateStateCountry()" Width="100%" ClientIDMode="Static" TabIndex="10"/>
				    <act:CascadingDropDown ID="AddressCountyDropDownListCascadingDropDown" runat="server" TargetControlID="AddressCountyDropDownList" ParentControlID="AddressStateDropDownList" 
															     ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetCounties" Category="Counties" BehaviorID="AddressCountyDropDownListCascadingDropDown" />
				    <asp:CustomValidator ID="AddressCountyRequired" ValidateEmptyText="True" runat="server" ControlToValidate="AddressCountyDropDownList" SetFocusOnError="true" Display="Dynamic" CssClass="error" ClientValidationFunction="AddressCountyRequiredValidate"/>
			    </td>
		    </tr>
      </asp:PlaceHolder>
		  <tr>
			  <td class="label"><%= HtmlRequiredLabel(AddressCountryDropDownList, "AddressCountry.Label", "Country")%></td>
			  <td>
				  <asp:DropDownList ID="AddressCountryDropDownList" runat="server" onchange="UpdateStateCounty();" ClientIDMode="Static" Width="100%" TabIndex="11" />
				  <asp:RequiredFieldValidator ID="AddressCountryRequired" InitialValue="" runat="server" ControlToValidate="AddressCountryDropDownList" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			  </td>
		  </tr>
      <tr>
        <td class="label"><%= HtmlLabel(PublicTransitAccessible, "PublicTransitAccessible.Label", "Public transit accessible?")%></td>
        <td><asp:CheckBox runat="server" ClientIDMode="Static" ID="PublicTransitAccessible" TabIndex="12"/></td>
      </tr>
		  <tr>
			  <td class="label"><%= HtmlLabel(ContactUrlTextBox, "ContactUrl.Label", "URL")%></td>
			  <td>
				  <asp:TextBox ID="ContactUrlTextBox" runat="server" ClientIDMode="Static" Width="98%" TabIndex="13" MaxLength="1000" />
				  <asp:RegularExpressionValidator ID="ContactUrlRegEx" runat="server" ControlToValidate="ContactUrlTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			  </td>
		  </tr>
		  <tr>
			  <td class="label"><%= HtmlRequiredLabel(ContactPhoneTextBox, "ContactPhone.Label", "Phone number")%></td>
			  <td>
				  <asp:TextBox ID="ContactPhoneTextBox" runat="server" ClientIDMode="Static" Width="50%" TabIndex="14" MaxLength="20" />
          <asp:TextBox ID="ExtensionTextBox" runat="server" ClientIDMode="Static" Width="15%" TabIndex="15" MaxLength="5" /><label for="ExtensionTextBox" class="hidden">Phone Number Extension</label>
				  <act:MaskedEditExtender ID="ExtensionMaskedEdit" runat="server" MaskType="Number" TargetControlID="ExtensionTextBox" PromptCharacter=" " ClearMaskOnLostFocus="false" AutoComplete="false" EnableViewState="true" ClearTextOnInvalid="false" Mask="99999"/>
          <asp:DropDownList ID="PhoneTypeDropDownList" runat="server" ClientIDMode="Static" Width="29%" TabIndex="16" /><label for="PhoneTypeDropDownList" class="hidden">Phone Type</label>
			    <br />
				  <asp:CustomValidator ID="ContactPhoneRequired" runat="server" ControlToValidate="ContactPhoneTextBox" ClientValidationFunction="EmployerRegistrationStep3_ValidatePhone" ValidateEmptyText="True" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			  </td>
		  </tr>
		  <tr>
			  <td class="label"><%= HtmlLabel(ContactAlternatePhoneTextBox, "ContactAlternatePhone.Label", "Alternate phone number 1")%></td>
			  <td>
				  <asp:TextBox ID="ContactAlternatePhoneTextBox" runat="server" ClientIDMode="Static" Width="50%" TabIndex="15" MaxLength="20" />
          <asp:DropDownList ID="AltPhoneTypeDropDownList" runat="server" ClientIDMode="Static" Width="29%" TabIndex="17" /><label for="AltPhoneTypeDropDownList" class="hidden">Alternate Phone 1 Type</label>
			  </td>
		  </tr>
		  <tr>
			  <td class="label"><%= HtmlLabel(ContactAlternate2PhoneTextBox, "ContactAlternatePhone2.Label", "Alternate phone number 2")%></td>
			  <td>
				  <asp:TextBox ID="ContactAlternate2PhoneTextBox" runat="server" ClientIDMode="Static" Width="50%" TabIndex="18" MaxLength="20" />
          <asp:DropDownList ID="AltPhoneType2DropDownList" runat="server" ClientIDMode="Static" Width="29%" TabIndex="19" /><label for="AltPhoneType2DropDownList" class="hidden">Alternate Phone 2 Type</label>
			  </td>
		  </tr>
	  </table>
  </div>
  <div id="EmployerRegistrationStep3Device" style="float:right;width:50%">
    <table class="formTable" role="presentation">
      <tr>
		    <td id="EmployerRegistrationStep3FEIDDevice"><asp:Literal ID="FederalEmployerIdentificationNumberLabel" runat="server"/></td>
			  <td>
				  <asp:TextBox ID="FederalEmployerIdentificationNumberTextBox" runat="server" ClientIDMode="Static" Width="98%" TabIndex="19" MaxLength="10" Enabled="False"/>
				  <asp:RequiredFieldValidator ID="FederalEmployerIdentificationNumberRequired" runat="server" ControlToValidate="FederalEmployerIdentificationNumberTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			  </td>
      </tr>
		  <tr>
		    <td class="label">
			    <asp:Literal ID="StateEmployerIdentificationLabel" runat="server"/>
					<div style="display: inline-block;">
						<asp:Literal runat="server" ID="StateEmployerIdentificationToolTip"></asp:Literal>
					</div>
		    </td>
			  <td>
				  <asp:TextBox ID="StateEmployerIdentificationNumberTextBox" runat="server" ClientIDMode="Static" Width="98%" TabIndex="22" MaxLength="11" />
          <asp:RegularExpressionValidator ID="StateEmployerIdentificationNumberValidator" runat="server" ControlToValidate="StateEmployerIdentificationNumberTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			  </td>
		  </tr>
      <asp:PlaceHolder runat="server" ID="OwnershipTypePlaceHolder">
		    <tr>
			    <td class="label"><%= HtmlRequiredLabel(OwnershipTypeDropDownList, "OwnershipType.Label", "Ownership type")%></td>
			    <td>
				    <asp:DropDownList ID="OwnershipTypeDropDownList" runat="server" ClientIDMode="Static" Width="100%" TabIndex="23" />
				    <asp:RequiredFieldValidator ID="OwnershipTypeRequired" runat="server" ControlToValidate="OwnershipTypeDropDownList" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			    </td>
		    </tr>
      </asp:PlaceHolder>
      <tr>
		    <td class="label"><asp:Literal ID="IndustrialClassificationLabel" runat="server"/></td>
			  <td>
               <table class="formTable" role="presentation">
                    <tr>
                    <td width="65%">
				  <asp:TextBox ID="IndustrialClassificationTextBox" runat="server" ClientIDMode="Static" Width="98%" TabIndex="24" MaxLength="200" AutoCompleteType="Disabled" />
				  <act:AutoCompleteExtender ID="IndustrialClassificationAutoCompleteExtender" runat="server" TargetControlID="IndustrialClassificationTextBox" MinimumPrefixLength="2" 
													  CompletionListCssClass="autocomplete_completionListElement" CompletionInterval="100" CompletionSetCount="15" UseContextKey="true"
													  ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetNaics" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" CompletionListItemCssClass="autocomplete_listItem"  /><br />

				  <asp:RequiredFieldValidator ID="IndustrialClassificationRequired" runat="server" ControlToValidate="IndustrialClassificationTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			    <asp:CustomValidator runat="server" ID="IndustrialClassificationValidator" ControlToValidate="IndustrialClassificationTextBox" ClientValidationFunction="doesNAICSExist"  SetFocusOnError="true" Display="Dynamic" CssClass="error"/>
                <span class="instructions">
            <%= HtmlLocalise("IndustrialClassificationInstructions.Label", "You may enter a NAICS code or title.")%></span>
			  </td>
              <td style="text-align:right">
                    <asp:HyperLink ID="NaicsLink" runat="server" NavigateUrl="https://www.naics.com/search/" Target="_new" ><%= HtmlLocalise("NaicsLink.Label.NoEdit", "NAICS Lookup-Use 2012 NAICS search")%></asp:HyperLink>
              </td>
              </tr>
                </table>
            </td>
      </tr>
		    <tr>
					<td><asp:Literal runat="server" ID="AccountTypeHeader"></asp:Literal></td>
			    <td>
				    <asp:DropDownList ID="AccountTypeDropDownList" runat="server" ClientIDMode="Static" Width="100%" TabIndex="25" />
				    <asp:RequiredFieldValidator ID="AccountTypeRequired" runat="server" ControlToValidate="AccountTypeDropDownList" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			    </td>
		    </tr>
        	<tr id="NoOfEmployeesRow" runat="server">
					  	  <td class="label"><%= HtmlRequiredLabel(NoOfEmployeesDropDownList, "NoOfEmployees.Label", "Number Of Employees")%></td>
			    <td>
				    <asp:DropDownList ID="NoOfEmployeesDropDownList" runat="server" ClientIDMode="Static" Width="100%" TabIndex="26" />
             <asp:RequiredFieldValidator ID="NoOfEmployeesRequired" runat="server" ControlToValidate="NoOfEmployeesDropDownList" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			    </td>
		    </tr>
      <tr>
			  <td class="label"><%= HtmlLabel(CompanyDescriptionTextBox, "CompanyDescription", "#BUSINESS# description")%></td>
			  <td valign="top">
				  <span class="instructions"><%= HtmlLocalise("CompanyDescription.HelpText", "This description will be added to all job postings automatically.")%></span><br />
				  <asp:TextBox ID="CompanyDescriptionTextBox" runat="server" TextMode="MultiLine" Rows="6" ClientIDMode="Static" Width="98%" TabIndex="27" MaxLength="2000" /><br />		
					<asp:CustomValidator runat="server" ID="CompanyDescriptionLengthValidator" ClientValidationFunction="ValidateCompanyDescriptionLength" CssClass="error" ControlToValidate="CompanyDescriptionTextBox" ErrorMessage="Error" SetFocusOnError="True"/>		
			  </td>
      </tr>
    </table>
  </div>
</div>
<script type="text/javascript">
    var zipValidatorName = '#<%=App.Settings.Theme == FocusThemes.Education ? "EducationAddressPostcodeZipRequired" : "AddressPostcodeZipRequired" %>';

    $(document).ready(
        function ()
        {
            $('#ContactPhoneTextBox').mask('(999) 999-9999', { placeholder: ' ' });
            $('#ContactAlternatePhoneTextBox').mask('(999) 999-9999', { placeholder: ' ' });
            $('#ContactAlternate2PhoneTextBox').mask('(999) 999-9999', { placeholder: ' ' });
            $('#AddressPostcodeZipTextBox').mask('<%= App.Settings.ExtendedPostalCodeMaskPattern %>', { placeholder: ' ' });
            $('#EducationAddressPostcodeZipTextBox').mask('<%= App.Settings.ExtendedPostalCodeMaskPattern %>', { placeholder: ' ' });
            $('#AddressPostcodeZipTextBox').change(function() {
               PopulateAddressFieldsForPostalCode(
                 '<%= UrlBuilder.AjaxService() %>',
                 $(this).val(), 
                 'AddressStateDropDownList', 
                 'AddressCountyDropDownList', 
                 '<%=AddressCountyDropDownListCascadingDropDown.BehaviorID%>', 
                 '<%=HtmlLocalise("Global.County.TopDefault", "- select county -") %>',
                 'AddressTownCityTextBox');
            });
            $('#EducationAddressPostcodeZipTextBox').change(function() {
               PopulateAddressFieldsForPostalCode(
                 '<%= UrlBuilder.AjaxService() %>',
                 $(this).val(), 
                 'AddressStateDropDownList', 
                 'AddressCountyDropDownList', 
                 '<%=AddressCountyDropDownListCascadingDropDown.BehaviorID%>', 
                 '<%=HtmlLocalise("Global.County.TopDefault", "- select county -") %>',
                 'AddressTownCityTextBox');
            });
        }
    );
    
		Sys.Application.add_load(Contact_PageLoad);

		function Contact_PageLoad(sender, args) {
			var isCounty = '<%= IsCountyEnabled %>';

			if (isCounty == 'True') {
				var behavior = $find('<%=AddressCountyDropDownListCascadingDropDown.BehaviorID %>');      
				if (behavior != null) {
					behavior.add_populated(function() {
						Contact_UpdateCountyDropDown($('#AddressCountyDropDownList'), $("#AddressStateDropDownList"));
					});
				}
			}

			var countryValue = $('#AddressCountryDropDownList').val();
			var zipCodeLabel = $('#ZipCodeLabel');
			var zipCodeValidator = $(zipValidatorName)[0];

			if (countryValue != <%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>) {
				$(zipCodeLabel).removeClass('requiredData');
				ValidatorEnable(zipCodeValidator, false);
			}
			else
			{
				$(zipCodeLabel).addClass('requiredData');
			}
		}
  
		function ValidateCompanyDescriptionLength(sender, args) {

			var description = document.getElementById(sender.controltovalidate).value;

			args.IsValid = description.length <= 2000;
		}
  
		function Contact_UpdateCountyDropDown(countyDropDown, stateDropDown) {
			if (countyDropDown.children('option:selected').val() != '')
				countyDropDown.next().find(':first-child').text(countyDropDown.children('option:selected').text()).parent().addClass('changed');
			else
				SetCounty(stateDropDown.val() == <%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>);
        
			UpdateStyledDropDown(countyDropDown, stateDropDown.val());
		}

		function ValidateZipCode(sender, args) {
			var countryValue = $('#AddressCountryDropDownList').val();

			if (args.Value.length == 0) {
				sender.innerHTML = "<%= ZIPCodeRequired %>";
				args.IsValid = false;
			}
			else if (countryValue == '<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>' && (args.Value.length != 9 && args.Value.length != 5)) {
				sender.innerHTML = "<%= ZIPCodeLimitError %>";
				args.IsValid = false;
			}
		}

	  function EmployerRegistrationStep3_ValidatePhone(sender, args) {
	    var trimmed = args.Value.trim();
		  if (trimmed == "(   )    -" || trimmed == "") {
		    args.IsValid = false;
		  }
	  }
  
	  function UpdateStateCountry() {
			var countyValue = $('#AddressCountyDropDownList').val();
			var stateValue = $('#AddressStateDropDownList').val();

			if (countyValue == '<%= GetLookupId(Constants.CodeItemKeys.Counties.OutsideUS) %>') {
				if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>')
					SetState(true);
				SetCountry(true);
			}
		}
  
    function doesNAICSExist(source, arguments)
    {
      var exists;

      $.ajax({
          type: "POST",
          contentType: "application/json; charset=utf-8",
          url: "<%= UrlBuilder.AjaxService() %>/CheckNaicsExists", //+ arguments.Value,
          dataType: "json",
          data: '{"naicsText": "' + arguments.Value + '", "childrenOnly": true}',
          async: false,
          success: function (result) {
            exists = (result.d);
          }
      });

      arguments.IsValid = exists;
    }
    
		function County_InvokeCountyCascadeDropdown() {
			$get("<%= AddressCountyDropDownList.ClientID %>")._behaviors[0]._onParentChange(null, null);
		}

    function UpdateStateCounty() {
			var isCounty = '<%= IsCountyEnabled %>';
			var countryValue = $('#AddressCountryDropDownList').val();
			var stateDropdown = $('#AddressStateDropDownList');
			var stateValue = stateDropdown.val();
			var zipLabel = $('#ZipCodeLabel');
			var zipValidator = $(zipValidatorName)[0];

			if (countryValue == '<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>') {
				if (stateValue == '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>') {
       		SetState(false);

       		if (isCounty == 'True')
       			County_InvokeCountyCascadeDropdown();
				}

				$(zipLabel).addClass('requiredData');
				ValidatorEnable(zipValidator, true);
			}
			else {
				if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>') {
       		SetState(true);

       		if (isCounty == 'True')
       			County_InvokeCountyCascadeDropdown();
				}

				$(zipLabel).removeClass('requiredData');
				ValidatorEnable(zipValidator, false);
			}
    }
       
		function SetCounty(outsideUS) {
			var countyDropDown = $('#AddressCountyDropDownList');

			if (outsideUS)
				$(countyDropDown).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.Counties.OutsideUS) %>"]').prop('selected', true);
			else
				$(countyDropDown).children().first().prop('selected', true);
	  
			$(countyDropDown).next().find(':first-child').text($(countyDropDown).children('option:selected').text()).parent().addClass('changed');
		}
			 
		function SetCountry(outsideUS) {
			var countryDropDown = $('#AddressCountryDropDownList');

			if (outsideUS)
				$(countryDropDown).children().first().prop('selected', true);
			else 
				$(countryDropDown).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>"]').prop('selected', true);
	  
			$(countryDropDown).next().find(':first-child').text($(countryDropDown).children('option:selected').text()).parent().addClass('changed');
		}

    function SetState(outsideUS) {
			var stateDropDown = $('#AddressStateDropDownList');

			if (outsideUS)
				$(stateDropDown).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>"]').prop('selected', true);
			else
				$(stateDropDown).children().first().prop('selected', true);

			$(stateDropDown).next().find(':first-child').text($(stateDropDown).children('option:selected').text()).parent().addClass('changed');
    }

    function ValidateZipCode(sender, args) {
      var countryValue = $('#AddressCountryDropDownList').val();

      if (args.Value.length == 0) {
       	sender.innerHTML = "<%= ZIPCodeRequired %>";
       	args.IsValid = false;
      }
      else if (countryValue == '<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>' && (args.Value.length != 9 && args.Value.length != 5)) {
       	sender.innerHTML = "<%= ZIPCodeLimitError %>";
       	args.IsValid = false;
      }
    }
       
		function UpdateCountry() {
			var stateValue = $('#AddressStateDropDownList').val();
			var zipLabel = $('#ZipCodeLabel');
			var zipValidator = $(zipValidatorName)[0];

			if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>') {
				$(zipLabel).addClass('requiredData');
				ValidatorEnable(zipValidator, true);
			}
			else {
				$(zipLabel).removeClass('requiredData');
				ValidatorEnable(zipValidator, false);
			}

			SetCountry(stateValue == '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>');
		}
		
		function AddressCountyRequiredValidate(sender, args) {
    	args.IsValid = args.Value != '';
    }
</script>
<asp:PlaceHolder runat="server" ID="CheckBusinessUnitPlaceHolder" Visible="False">
<asp:HiddenField ID="CheckBusinessUnitModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="CheckBusinessUnitModalPopup" runat="server" ClientIDMode="Static"
												TargetControlID="CheckBusinessUnitModalDummyTarget"
												PopupControlID="CheckBusinessUnitModalPanel"
												PopupDragHandleControlID="CheckBusinessUnitModalPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="CheckBusinessUnitModalPanel" TabIndex="-1" runat="server" CssClass="modal hidden" ClientIDMode="Static">
    <h1><%=HtmlLocalise("BusinessUnitSearch.Header", "BusinessUnit Search") %></h1>
    <asp:PlaceHolder runat="server" ID="CompaniesFoundPlaceHolder">
    <div id="BusinessUnitListDiv">
	<asp:ListView ID="BusinessUnitListView" runat="server" ItemPlaceholderID="BusinessUnitListViewPlaceHolder" DataKeyNames="Id, EmployerId" OnLayoutCreated="BusinessUnitListView_LayoutCreated">
	    <LayoutTemplate>
	        <table width="100%" class="genericTable withHeader">
	            <tr>
	                <th></th>
                    <th><asp:Literal runat="server" ID="BusinessUnitHeader"></asp:Literal></th>
                    <th>&nbsp;&nbsp;</th>
                    <th><asp:Literal runat="server" ID="AddressHeader"></asp:Literal></th>
                    <th>&nbsp;&nbsp;</th>
                    <th><asp:Literal runat="server" ID="ZipHeader"></asp:Literal></th>
	            </tr>
                <tbody id="BusinessUnitListViewPlaceHolder" runat="server"></tbody>
            </table>
	    </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td><asp:RadioButton runat="server" GroupName="Companies" ID="BusinessUnitList" onclick="fnCheckUnCheck(this.id);" /></td>
                <td><%# Eval("BusinessUnitName")%></td>
                <td></td>
                <td><%# Eval("AddressLine1")%></td>
                <td></td>
                <td><%# Eval("PostcodeZip")%></td>
            </tr>
        </ItemTemplate>
    </asp:ListView> 
    </div>
    <asp:CustomValidator runat="server" ID="BusinessUnitSelectedValidator"  ClientValidationFunction="BusinessUnitSelected" CssClass="error" ValidationGroup="SearchBusinessUnit" Display="Dynamic"></asp:CustomValidator> <br/>
    <asp:Button runat="server" CssClass="button3" ID="UseThisBusinessUnitButton" CausesValidation="True" ValidationGroup="SearchBusinessUnit" OnClick="UseThisBusinessUnitButton_Click" />&nbsp;<asp:Button runat="server" CssClass="button3" ID="CreateNewBusinessUnitButton" CausesValidation="False" OnClick="CreateNewBusinessUnitButton_Click" />
    </asp:PlaceHolder>
     <asp:PlaceHolder runat="server" ID="NoBusinessUnitsFoundPlaceHolder">
         <p><asp:Literal runat="server" ID="NoBusinessUnitsFoundLiteral"></asp:Literal></p>
         <asp:Button runat="server" ID="BusinessUnitNotFoundButton" OnClick="CreateNewBusinessUnitButton_Click" CssClass="button3" CausesValidation="False"/>
     </asp:PlaceHolder>
     <div class="closeIcon"><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('CheckBusinessUnitModalPopup').hide();return false;" /></div>
</asp:Panel>
<script type="text/javascript">

	Sys.Application.add_load(BuinessUnitSelect_PageLoad);
	function BuinessUnitSelect_PageLoad(sender, args) {
		// Jaws Compatibility
		var modal = $find('CheckBusinessUnitModalPopup');
		if (modal != null) {
			modal.add_shown(function () {
				$('#CheckBusinessUnitModalPanel').attr('aria-hidden', false);
				$('.page').attr('aria-hidden', true);
				$('#CompanyNameTextBox').focus();
			});

			modal.add_hiding(function () {
				$('#CheckBusinessUnitModalPanel').attr('aria-hidden', true);
				$('.page').attr('aria-hidden', false);
			});
		}
	}
		

	function fnCheckUnCheck(objId) {
        var grd = document.getElementById("BusinessUnitListDiv");

        //Collect A
        var rdoArray = grd.getElementsByTagName("input");

        for (i = 0; i <= rdoArray.length - 1; i++) {
            if (rdoArray[i].type == 'radio') {
                if (rdoArray[i].id != objId) {
                    rdoArray[i].checked = false;
                }
            }
        }
    }

    function BusinessUnitSelected(source, args) {
        var grd = document.getElementById("BusinessUnitListDiv");
        //Collect A
        var rdoArray = grd.getElementsByTagName("input");
        args.IsValid = false;

        for (i = 0; i <= rdoArray.length - 1; i++) {
            if (rdoArray[i].type == 'radio') {
                if (rdoArray[i].checked == true) {
                    args.IsValid = true;
                }
            }
        }
    }

</script>
</asp:PlaceHolder>
