﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ErrorModal.ascx.cs" Inherits="Focus.Web.Code.Controls.User.ErrorModal" %>

<asp:HiddenField ID="ErrorModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ErrorModalPopup" runat="server" ClientIDMode="Static"
												TargetControlID="ErrorModalDummyTarget"
												PopupControlID="ErrorModalPanel"
												PopupDragHandleControlID="ErrorModalPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="ErrorModalPanel" runat="server" CssClass="errorModal modal" Style="display:none;">
	<div style="float:left;">
		<table style="width:<%= Width %>;" role="presentation">
			<tr>
				<td style="vertical-align:top"><b><%= Title %></b></td>
			</tr>
			<tr>
				<td style="vertical-align:top; height:<%= Height %>" ><%= Details %></td>
			</tr>
		</table>
	</div>
	<div class="closeIcon"><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="<%= CloseJavascript %>" /></div>
</asp:Panel>