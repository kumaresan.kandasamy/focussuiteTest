﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobWizardStep5.ascx.cs" Inherits="Focus.Web.Code.Controls.User.JobWizardStep5" %>

<%@ Register src="~/Code/Controls/User/JobWizardLocationSelector.ascx" tagname="JobWizardLocationSelector" tagprefix="uc" %>

<table role="presentation" style="width:100%;">
	<tr style="float: right" id="ExpandCollapseAllRow" runat="server">
		<td><asp:Button ID="ExpandCollapseAllButton" runat="server" class="button3" style="float:right" ClientIDMode="Static"/></td>
	</tr>
	<tr>
		<td>
			<asp:Panel ID="WorkLocationHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
				<table role="presentation" class="accordionTable">
					<tr  class="multipleAccordionTitle">
						<td>
							<asp:Image ID="WorkLocationHeaderImage" runat="server" AlternateText="Work Location Header Image" />
							&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("WorkLocation.Label", "Work location")%></a></span>&nbsp;
							<span class="requiredDataLegend instructions"><%= HtmlLocalise("Global.RequiredFields.Text.NoEdit", "required fields") %></span>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<asp:UpdatePanel runat="server" ID="WorkLocationUpdatePanel" UpdateMode="Conditional">
				<ContentTemplate>
					<div class="singleAccordionContentWrapper">
					<asp:Panel ID="WorkLocationPanel" runat="server" CssClass="singleAccordionContent">
						<table role="presentation" style="width:100%;">
							<tr>
								<td style="width:50%; vertical-align:top;">
									<%= HtmlRequiredLabel(WorkLocationDropDown, "WorkLocations.Label", "Location(s) where employee will report to work")%>
									<br /><br />
									<asp:DropDownList runat="server" ID="WorkLocationDropDown" ClientIDMode="Static" CausesValidation="False" AutoPostBack="True" OnChange="JobWizardStep5_DisableLocationSelectorValidator();" OnSelectedIndexChanged="WorkLocationDropDown_SelectedIndexChanged"/>
									<br /><br /><asp:CheckBox id="PublicTransitAccessibleCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" />
								</td>
								<td  style="width:50%;">
									<uc:JobWizardLocationSelector ID="LocationSelector" runat="server" Visible="false" />
									<asp:CustomValidator ID="LocationSelectorValidator" runat="server" CssClass="error" ClientValidationFunction="validateLocations" ControlToValidate="WorkLocationDropDown" OnServerValidate="LocationSelectorValidator_OnServerValidate" />
								</td>  
							</tr>
                <tr>
                  <td colspan="2">
                      <asp:CheckBox id="HomeBasedWorkerCheckbox" TextAlign="Right" runat="server" />
                  </td>
                </tr>
						</table>
					</asp:Panel>
					</div>
					<act:CollapsiblePanelExtender ID="WorkLocationPanelExtender" runat="server" TargetControlID="WorkLocationPanel" ExpandControlID="WorkLocationHeaderPanel" 
																					CollapseControlID="WorkLocationHeaderPanel" Collapsed="false" ImageControlID="WorkLocationHeaderImage" SuppressPostBack="true" />
				</ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="workLocationDropDown" />
				</Triggers>
			</asp:UpdatePanel>
																								
			<asp:Panel ID="JobConditionsHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
				<table role="presentation" class="accordionTable">
					<tr  class="multipleAccordionTitle">
						<td>
							<asp:Image ID="JobConditionsHeaderImage" runat="server" AlternateText="Job Conditions Header Image" />
							&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("JobConditions.Label", "Job conditions")%></a></span>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<div class="singleAccordionContentWrapper">
			<asp:Panel ID="JobConditionsPanel" runat="server" CssClass="singleAccordionContent">
			<p><asp:CheckBox ID="CourtOrderedAffirmativeActionCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" /></p>
			<p><asp:CheckBox id="FederalContractorCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" OnCheckedChanged="FederalContractorCheckBox_CheckedChanged"/></p>
			<div id="FederalContractorExpirationDateDiv" style="display: none;">
				<table role="presentation">
					<tr>
					  <td><asp:Literal runat="server" id="FederalContractorExpirationDateLiteral"></asp:Literal></td>
						<%--<td>&nbsp;&nbsp;<%= HtmlRequiredLabel(FederalContractorExpirationDateTextBox, "FederalContractorExpirationDate.Label", "Expiration date.")%>&nbsp;&nbsp;</td>--%>
						<td>
							<table role="presentation">
								<tr>
									<td><asp:TextBox ID="FederalContractorExpirationDateTextBox" runat="server" ClientIDMode="Static" /></td>
									<td>
										<table role="presentation">
											<tr>
												<td>
												    <asp:RequiredFieldValidator ID="FederalContractorExpirationDateRequired" runat="server" ControlToValidate="FederalContractorExpirationDateTextBox" CssClass="error" Display="Dynamic" />
                            <asp:CompareValidator ID="FederalContractorExpirationDateIsValid" runat="server" ControlToValidate="FederalContractorExpirationDateTextBox" SetFocusOnError="true" Type="Date" Operator="DataTypeCheck" Display="Dynamic" CssClass="error" ValidateEmptyText="true"/>
                            <asp:CompareValidator ID="FederalContractorExpirationDateGreaterThanTodayCompareValidator" runat="server" ControlToValidate="FederalContractorExpirationDateTextBox" Type="Date" Operator="GreaterThan" CssClass="error" Display="Dynamic" />
												</td>
											</tr>
										</table>
									</td> 
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<act:CalendarExtender ID="FederalContractorExpirationDateCalendarExtender" runat="server" TargetControlID="FederalContractorExpirationDateTextBox"  CssClass="cal_Theme1" />
			</div>
			<asp:CheckBox runat="server" ClientIDMode="Static" ID="ForeignLaborCertificationCheckBox" onclick="EnableDisableForeignLaborRadioButtons($(this), true)" />
			<div style="margin-left: 15px">
				<table>
					<tr>
						<td>
							<asp:RadioButtonList ClientIDMode="Static" ID="ForeignLaborRadioButtonList" runat="server">
								<asp:ListItem Value="ForeignLaborCertificationListItemH2A" onclick="ForeignLaborRadioButtonChanged()"></asp:ListItem>
								<asp:ListItem Value="ForeignLaborCertificationListItemH2B" onclick="ForeignLaborRadioButtonChanged()"></asp:ListItem>
								<asp:ListItem Value="ForeignLaborCertificationListItemOther" onclick="ForeignLaborRadioButtonChanged()"></asp:ListItem>
							</asp:RadioButtonList>
						</td>
						<td style="vertical-align: bottom;">
							<asp:Panel runat="server" ID="AdvertiseFor30DaysPlaceHolder" style="display: none">
								<asp:Label CssClass="error" runat="server" ID="AdvertiseFor30DaysText"></asp:Label><br />
							</asp:Panel>
              <focus:CheckBoxCustomValidator runat="server" ID="FederalForeignCertificationValidator" ControlToValidate="FederalContractorCheckBox" ClientValidationFunction="validateFederalForeignCertification" ValidateEmptyText="True" EnableClientScript="True" SetFocusOnError="True" CssClass="error" Display="Dynamic" />
							<asp:CustomValidator runat="server" ID="ForeignLaborCheckBoxValidator" ControlToValidate="ForeignLaborRadioButtonList" ClientValidationFunction="ValidateForeignLaborRadioButtons" ValidateEmptyText="True" SetFocusOnError="True" CssClass="error" Display="Dynamic" />
							<asp:CustomValidator runat="server" ID="ForeignLaborCheckBoxH2AH2BValidator" ControlToValidate="ForeignLaborRadioButtonList" ClientValidationFunction="ValidateForeignLaborH2AorH2BRadioButtons" ValidateEmptyText="True" SetFocusOnError="True" CssClass="error" Display="Dynamic" />
						</td>
					</tr>
				</table>
			</div>
			
			<br />
			
      </asp:Panel>
			</div>
			<act:CollapsiblePanelExtender ID="JobConditionsPanelExtender" runat="server" TargetControlID="JobConditionsPanel" ExpandControlID="JobConditionsHeaderPanel" 
																					CollapseControlID="JobConditionsHeaderPanel" Collapsed="false" ImageControlID="JobConditionsHeaderImage" 
																					SuppressPostBack="true" />

      <asp:PlaceHolder runat="server" ID="WorkOpportunitiesTaxCreditPlaceHolder">
			  <asp:Panel ID="WorkOpportunitiesTaxCreditHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
				  <table role="presentation" class="accordionTable">
					  <tr  class="multipleAccordionTitle">
						  <td>
							  <asp:Image ID="WorkOpportunitiesTaxCreditHeaderImage" runat="server" AlternateText="Work Opportunities Tax Credit Header Image"/>
							  &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("WorkOpportunitiesTaxCredit.Label", "Interested in hiring individuals who may qualify your business for a tax credit?")%></a></span>
						  </td>
					  </tr>
				  </table>
			  </asp:Panel>
			  <div class="singleAccordionContentWrapper">
			  <asp:Panel ID="WorkOpportunitiesTaxCreditPanel" runat="server" CssClass="singleAccordionContent">
				  <table role="presentation">
					  <tr>
						  <td colspan="3"><%= HtmlLocalise("WorkOpportunitiesTaxCredit.Description", "Hiring qualified individuals from the target groups below may result in tax-credit eligibility. Indicate your interest and our staff will reach out to provide tax-credit assistance.")%></td>
					  </tr>
            <tr>
	            <!-- Row 1 -->
						  <td>
							  <table role="presentation">
								  <tr>
									  <td><asp:CheckBox ID="ExFelonsCheckBox" runat="server" ClientIDMode="Static" /></td>
									  <td>
										  <%= HtmlTooltipster("tooltipWithArrow", "ExFelons.Tooltip", @"Individuals convicted of felonies and/or having conviction dates that are not more than 1 year prior to hire date, or having release dates that are not more than 1 year prior to hire date.")%>
									  </td>
								  </tr>
							  </table>
						  </td>
						  <td>
							  <table role="presentation">
								  <tr>
									  <td><asp:CheckBox ID="SNAPRecipientsCheckBox" runat="server" ClientIDMode="Static" /></td>
									  <td>
										  <%= HtmlTooltipster("tooltipWithArrow", "SNAPRecipientsCheckBox.Tooltip", @"1. Members of families that received Supplemental Nutrition Assistance Program benefits for 6-month period prior to hire date; OR
																																									  <br>2. Able-Bodied Adults without Dependents (ABAWD) who received SNAP benefits for 3 or 5 months prior to hire date and ceased to be eligible for benefits due to program-requirement compliance failure.")%>
									  </td>
									  </tr>
							  </table>
						  </td>
						  <td>
							  <table role="presentation">
								  <tr>
									  <td><asp:CheckBox ID="TANFRecipientsCheckBox" runat="server" ClientIDMode="Static" /></td>
									  <td>
										  <%= HtmlTooltipster("tooltipWithArrow", "TANFRecipients.Tooltip", @"Members of families that received Temporary Assistance to Needy Families benefits for 9 months (consecutive or not) during the 18-month period prior to hire date.")%>
									  </td>
							  </tr>
							  </table>
						  </td>
            </tr>
					  <tr>
						   <!-- Row 2 -->
						   <td>
							  <table role="presentation">
								  <tr>
						 		  <td><asp:CheckBox ID="DesignatedCommunityResidentsCheckBox" runat="server" ClientIDMode="Static" /></td>
									  <td>
										  <%= HtmlTooltipster("tooltipWithArrow", "DesignatedCommunityResidents.Tooltip", @"Individuals 18-39-years old, residing in federally designated Empowerment Zones or Rural Renewal Counties. WOTC-qualified wages are only those earned while employee resides in Target Group D.")%>
									  </td>
										  </tr>
							  </table>
						  </td>
						  <td>
							  <table role="presentation">
								  <tr>
									  <td><asp:CheckBox ID="SSIRecipientsCheckBox" runat="server" ClientIDMode="Static" /></td>
									  <td>
										  <%= HtmlTooltipster("tooltipWithArrow", "SSIRecipients.Tooltip", @"Recipients of Supplemental Security Income benefits for any month ending during the past 60-day period, ending on hire date.")%>
									  </td>
								  </tr>
							  </table>
						  </td>
						  <td>
							  <table role="presentation">
								  <tr>
									  <td><asp:CheckBox ID="VeteransCheckBox" runat="server" ClientIDMode="Static" /></td>
									  <td>
										  <%= HtmlTooltipster("tooltipWithArrow", "Veterans.Tooltip", @"1. Individuals discharged/released from active duty in the US Armed Forces and are members of families that received SNAP for 3 consecutive months during the 15-month period prior to hire date; OR
																																		  <br>2. Individuals discharged/released from active duty in the US Armed Forces and are entitled to compensation for service-connected disabilities (10% or greater); and were, during the 1 year prior to hire date:
																																		  <br>A. Discharged/released from active duty in the US Armed Forces; OR
																																		  <br>B. Unemployed for a period/s, totaling at least 6 months.")%>
									  </td>
								  </tr>
							  </table>
						  </td>
			
					  </tr>
					  <tr>
						   <!-- Row 3 -->
					  <td>
							  <table role="presentation">
								  <tr>
									  <td><asp:CheckBox ID="LongTermFamilyAssistanceRecipientsCheckBox" runat="server" ClientIDMode="Static" /></td>
									  <td>
										  <%= HtmlTooltipster("tooltipWithArrow", "LongTermFamilyAssistanceRecipients.Tooltip", @"Members of families that received Temporary Assistance to Needy Families payments:
																																														  <br>A. For any 18 months (consecutive or not) between August 5, 1997 through hire date and have hire dates not more than 2 years after the end of earliest 18-month period; OR
																																														  <br>B. During 2-year period prior to hire date, ceased to be eligible for benefits due to federal or state law limiting maximum payments; OR
																																														  <br>C. For 18 consecutive months prior to hire date.")%>
									  </td>
								  </tr>
							  </table>
						  </td>
						  <td>
							  <table role="presentation">
								  <tr>
									  <td><asp:CheckBox ID="SummerYouthCheckBox" runat="server" ClientIDMode="Static" /></td>
									  <td>
										  <%= HtmlTooltipster("tooltipWithArrow", "SummerYouthRecipients.Tooltip", @"Individuals 16-17-years old, residing in Empowerment Zones and employed between May 1 and September 15.")%>
									  </td>
								  </tr>
							  </table>
						  </td>
						  <td>
							  <table role="presentation">
								  <tr>
									  <td><asp:CheckBox ID="VocationalRehabilitationCheckBox" runat="server" ClientIDMode="Static" /></td>
									  <td>
										  <%= HtmlTooltipster("tooltipWithArrow", "VocationalRehabilitation.Tooltip", @"Individuals who completed or are completing rehabilitative services from a state-certified agency; an employment network, under the Ticket-to-Work program; or the US Department of Veterans Affairs.")%>
									  </td>			
							  </tr>
							  </table>
						  </td>
					  </tr>					
				  </table>
				  &nbsp;
			  </asp:Panel>
			  </div>
			  <act:CollapsiblePanelExtender ID="WorkOpportunitiesTaxCreditPanelExtender" runat="server" TargetControlID="WorkOpportunitiesTaxCreditPanel" 
																		  ExpandControlID="WorkOpportunitiesTaxCreditHeaderPanel" CollapseControlID="WorkOpportunitiesTaxCreditHeaderPanel" 
																		  Collapsed="false" ImageControlID="WorkOpportunitiesTaxCreditHeaderImage" SuppressPostBack="true" />
      </asp:PlaceHolder>

			<asp:Panel ID="JobPostingFlagsHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
				<table role="presentation" class="accordionTable">
					<tr  class="multipleAccordionTitle">
						<td>
							<asp:Image ID="JobPostingFlagsHeaderImage" runat="server" AlternateText="Job Posting Flags Header Image" />
							&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("JobPostingFlags.Label", "Flag my job posting as")%></a></span>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<div class="singleAccordionContentWrapper">
			<asp:Panel ID="JobPostingFlagsPanel" runat="server" CssClass="singleAccordionContent">
				<table role="presentation">
					<tr>
						<td colspan="2"><%= HtmlLocalise("JobPosting.Description", @"Does your job fit any of the categories below? #FOCUSTALENT# enables interested job seekers to search specifically for opportunities in designated sectors. By selecting any of the categories below, your posting will be flagged accordingly and will turn up in searches for that sector.")%></td>
					</tr>
					<tr>
						<td>
							<table role="presentation">
								<tr>
									<td><asp:CheckBox ID="AdvancedManufacturingCheckBox" runat="server" ClientIDMode="Static" /></td>
									<td><%= HtmlTooltipster("tooltipWithArrow", "EmergingSector.AdvancedManufacturing.ToolTip", "Jobs involving the innovative use of technology to improve products or processes.")%></td>
								</tr>
							</table>
						</td>
						<td>
							<table role="presentation">
								<tr>
									<td><asp:CheckBox ID="HealthInformaticsCheckBox" runat="server" ClientIDMode="Static" /></td>
									<td><%= HtmlTooltipster("tooltipWithArrow", "EmergingSector.HealthInformatics.ToolTip", "Jobs involving the intersecting disciplines of information science, computer science, and healthcare. They deal with the resources, devices, and methods required to optimize the acquisition, storage, retrieval and use of information in healthcare and biomedicine. May also apply to nursing, clinical care, dentistry, pharmacy, public health, occupational therapy, and biomedical research.")%></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table role="presentation">
								<tr>
									<td><asp:CheckBox ID="AerospaceCheckBox" runat="server" ClientIDMode="Static" /></td>
									<td><%= HtmlTooltipster("tooltipWithArrow", "EmergingSector.Aerospace.ToolTip", "Jobs involving research, design, manufacture, operation, or maintenance of aeronautic and astronautic vehicles with diverse commercial, industrial, and military applications.")%></td>
								</tr>
							</table>
						</td>
						<td>
							<table role="presentation">
								<tr>
									<td><asp:CheckBox ID="HealthcareEmployerCheckBox" runat="server" ClientIDMode="Static" /></td>
									<td><%= HtmlTooltipster("tooltipWithArrow", "EmergingSector.Healthcare.ToolTip", "Jobs involving the provision of goods and services to treat patients with curative, preventative, rehabilitative, and palliative care. Today’s healthcare sector has many sub-sectors and depends on interdisciplinary teams of trained professionals and para-professionals that serve various populations.")%></td>
								</tr>
							</table>
						</td>
				  </tr>
				  <tr>
						<td>
							<table role="presentation">
								<tr>
									<td><asp:CheckBox ID="BiotechEmployerCheckBox" runat="server" ClientIDMode="Static" /></td>
									<td><%= HtmlTooltipster("tooltipWithArrow", "EmergingSector.BioTechnology.ToolTip", "Jobs involving the development and modification of useful products from living organisms with applications in genomics, recombinant gene technologies, applied immunology, pharmaceutical therapies, diagnostic testing, medicine, agriculture, and food production.")%></td>
								</tr>
							</table>
						</td>
						<td>
							<table role="presentation">
								<tr>
									<td><asp:CheckBox ID="ITEmployerCheckBox" runat="server" ClientIDMode="Static" /></td>
									<td><%= HtmlTooltipster("tooltipWithArrow", "EmergingSector.InformationTechnology.ToolTip", "Jobs involving the application of computers and telecommunications equipment to store, retrieve, transmit, and manipulate data often in a business context. It encompasses industries associated with computer hardware, software, electronics, semi-conductors, internet, telecom equipment, e-commerce, computer services, and distributive technologies such as television and communications.")%></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table role="presentation">
								<tr>
									<td><asp:CheckBox ID="BusinessServicesCheckBox" runat="server" ClientIDMode="Static" /></td>
									<td><%= HtmlTooltipster("tooltipWithArrow", "EmergingSector.BusinessServices.ToolTip", "Jobs involving the management of business-information technology alignment that promotes a customer-centric approach to service delivery through strategic planning, operations, and continuous improvement.")%></td>
								</tr>
							</table>
						</td>
						<td>
							<table role="presentation">
								<tr>
									<td><asp:CheckBox ID="ResearchDevelopmentCheckBox" runat="server" ClientIDMode="Static" /></td>
									<td><%= HtmlTooltipster("tooltipWithArrow", "EmergingSector.ResearchandDevelopment.ToolTip", "Jobs involving the development of new products, knowledge, processes, and services, differing significantly from one model or industry to the next. R&amp;D activities generally staff engineers and scientists who may work in both corporate or government sectors.")%></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
					 	<td>
							<table role="presentation">
								<tr>
									<td><asp:CheckBox ID="ConsultingCheckBox" runat="server" ClientIDMode="Static" /></td>
									<td><%= HtmlTooltipster("tooltipWithArrow", "EmergingSector.Consulting.ToolTip", "Jobs involving the provision of opinion, advice, counsel, or targeted services, covering a wide range of professions such as biotechnology, engineering, environmental, faculty, financial, franchising, human resources, information technology, legal, management, performance, personal dynamics, politics, public relations, manufacturing, supply-chain.")%></td>
								</tr>
							</table>
						</td>
						<td>
							<table role="presentation">
								<tr>
									<td><asp:CheckBox ID="SMARTCheckBox" runat="server" ClientIDMode="Static" /></td>
									<td><%= HtmlTooltipster("tooltipWithArrow", "EmergingSector.SMART.ToolTip", "Jobs involving science, mathematics, research, technology and engineering. SMART sector jobs are aligned with opportunities in government, military, and corporate markets, and also may be aligned with designated colleges and universities offering SMART curricula.")%></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table role="presentation">
								<tr>
									<td><asp:CheckBox ID="EnergyCheckbox" runat="server" ClientIDMode="Static" /></td>
									<td><%= HtmlTooltipster("tooltipWithArrow", "EmergingSector.Energy.ToolTip", "Jobs involving the extraction, production, manufacture, sale, and maintenance of energy resources, fuels, and systems that fulfill the need for power, including petroleum, natural gas, electrical, hydroelectric, coal, nuclear, solar, wind, and alternative fuels.")%></td>
								</tr>
							</table>
						</td>
						<td>
							<table role="presentation">
								<tr>
									<td><asp:CheckBox ID="TransportationCheckBox" runat="server" ClientIDMode="Static" /></td>
									<td><%= HtmlTooltipster("tooltipWithArrow", "EmergingSector.TransportDistribute.ToolTip", "Jobs involving the planning, management, and movement of people, materials, and products by road, air, rail, and water. These include related professional and technical services such as infrastructure planning and management, logistics, warehousing, and maintenance of equipment and facilities.")%></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table role="presentation">
								<tr>
									<td><asp:CheckBox ID="GreenJobCheckBox" runat="server" ClientIDMode="Static" /></td>
									<td><%= HtmlTooltipster("tooltipWithArrow", "EmergingSector.Green.ToolTip", "Jobs involving the policies, information, materials, technologies and promotion of operations that contribute to minimizing environmental impact; conserving energy; developing alternative, sustainable or high-efficiency energy sources; controlling pollution; developing organic chemical replacements; and recycling or reducing waste.")%></td>
								</tr>
							</table>
						</td>
					</tr>
								
				</table>
				&nbsp;
			</asp:Panel>
			</div>
			<act:CollapsiblePanelExtender ID="JobPostingFlagsPanelExtender" runat="server" TargetControlID="JobPostingFlagsPanel" ExpandControlID="JobPostingFlagsHeaderPanel" 
																					CollapseControlID="JobPostingFlagsHeaderPanel" Collapsed="false" ImageControlID="JobPostingFlagsHeaderImage" 
																					SuppressPostBack="true" />
      
		</td>
	</tr>
</table>

<script type="text/javascript">

//	$(document).ready(function() {
//		ValidatorEnable($('#<%=FederalContractorExpirationDateRequired.ClientID%>')[0], $('#<%=FederalContractorCheckBox.ClientID %>').is(":checked"));
//		ValidatorEnable($('#<%=FederalContractorExpirationDateIsValid.ClientID%>')[0], $('#<%=FederalContractorCheckBox.ClientID %>').is(":checked"));
//		ValidatorEnable($('#<%=FederalContractorExpirationDateGreaterThanTodayCompareValidator.ClientID %>')[0], $('#<%=FederalContractorCheckBox.ClientID %>').is(":checked"));
//	});

  Sys.Application.add_load(JobWizardStep5_PageLoad);

  function JobWizardStep5_PageLoad() {
  	EnableDisableForeignLaborRadioButtons($("#<%=ForeignLaborCertificationCheckBox.ClientID%>"), false);

    bindExpandCollapseAllPanels('<%= ExpandCollapseAllButton.ClientID %>', null); // bind the magic
   }

   function ForeignLaborRadioButtonChanged() {
   	var listItemOtherSelected = $(":radio[value=ForeignLaborCertificationListItemOther]").prop('checked');
	  if (listItemOtherSelected) {
			$('#<%=AdvertiseFor30DaysPlaceHolder.ClientID %>').show();
		} else {
			$('#<%=AdvertiseFor30DaysPlaceHolder.ClientID %>').hide();
		}
	}

  function EnableDisableForeignLaborRadioButtons(chkBox, doValidation) {
	  var isChecked = chkBox.prop('checked');
	  var radioButtons = $('#ForeignLaborRadioButtonList input');

	  radioButtons.prop('disabled', !isChecked);
	  if (!isChecked) {
		  radioButtons.prop('checked', false);
	  }

	  if (doValidation) {
		  ValidatorValidate($("#<%=FederalForeignCertificationValidator.ClientID %>").get(0));
		  ValidatorValidate($("#<%=ForeignLaborCheckBoxH2AH2BValidator.ClientID %>").get(0));
		 }

	  ForeignLaborRadioButtonChanged();
  }

  function ValidateForeignLaborRadioButtons(source, args) {
		args.IsValid = !$('#ForeignLaborCertificationCheckBox').prop('checked');
			
		if (!args.IsValid) {
			args.IsValid = $('#ForeignLaborRadioButtonList input:checked').prop('checked');
		}
	}

	function ValidateForeignLaborH2AorH2BRadioButtons(source, args) {
		var buttonVal = $('#ForeignLaborRadioButtonList input:checked').val();
		args.IsValid = $('#ForeignLaborCertificationCheckBox').prop('checked')
									   && (buttonVal == "ForeignLaborCertificationListItemH2A" || buttonVal == "ForeignLaborCertificationListItemH2B" || $('#ForeignLaborRadioButtonList input:checked').length == 0);
	}
	
	function JobWizardStep5_DisableLocationSelectorValidator() {
		var validator = $("#<%=LocationSelectorValidator.ClientID %>");
		if (validator.length > 0)
			ValidatorEnable(validator[0], false);
	  
		return true;
	}
</script>