﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdateContactInformationModal.ascx.cs" Inherits="Focus.Web.Code.Controls.User.UpdateContactInformationModal" %>

<%@ Register Src="~/Code/Controls/User/ContactInformation.ascx" TagPrefix="uc" TagName="ContactInformation" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ContactModalPopup" runat="server" BehaviorID="UpdateContactInformationModal"
												TargetControlID="ModalDummyTarget"
												PopupControlID="ContactInformationModalPanel"
												PopupDragHandleControlID="ContactInformationModalPanelHeader"
												RepositionMode="RepositionOnWindowResizeAndScroll" 
												BackgroundCssClass="modalBackground" />
<asp:Panel ID="ContactInformationModalPanel" runat="server" CssClass="modal" Style="display:none;width:600px">
  <asp:Panel runat="server" ClientIDMode="Static" ID="ContactInformationModalPanelHeader">
     
  </asp:Panel>
	<div runat="server">
		<p><b><%= HtmlLocalise("UpdateContactInformation.Label", "Update contact information")%></b> <%= HtmlRequiredFieldsInstruction() %></p>
	</div>
	<uc:ContactInformation ID="ContactInformationMain" runat="server" />
	<div runat="server" style="text-align:right;">
		<asp:Button ID="ContactCancelButton" runat="server" CssClass="button4" OnClick="EditContactCancelButton_Clicked" CausesValidation="False" Text="Cancel"/>
		<asp:Button ID="ContactSaveButton" runat="server" CssClass="button4" OnClick="ContactSaveButton_Clicked"  ValidationGroup="ContactInformation" CausesValidation="True" Text="Save"/>
	</div>
</asp:Panel>

<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" ClientIDMode="AutoID" />
		
	
