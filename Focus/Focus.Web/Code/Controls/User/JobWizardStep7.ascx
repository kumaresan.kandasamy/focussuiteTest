﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobWizardStep7.ascx.cs"
    Inherits="Focus.Web.Code.Controls.User.JobWizardStep7" %>
<%@ Register TagPrefix="val" Namespace="xVal.WebForms" Assembly="xVal.WebForms" %>
<%@ Import Namespace="Focus" %>
<script src="<%= ResolveUrl("~/Assets/Scripts/moment.min.js") %>" type="text/javascript"></script>
<table style="width: 100%;" role="presentation">
    <tr style="float: right" id="ExpandCollapseAllRow" runat="server">
        <td>
            <asp:Button ID="ExpandCollapseAllButton" runat="server" class="button3" Style="float: right"
                ClientIDMode="Static" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="JobContactHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
                <table role="presentation" class="accordionTable">
                    <tr class="multipleAccordionTitle">
                        <td>
                            <asp:Image ID="JobContactHeaderImage" runat="server" AlternateText="Job Contact Header Image" />
                            &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("JobContact.Label", "Job contact")%></a></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div id="JobContactPanelWrapper" runat="server" class="singleAccordionContentWrapper">
                <asp:Panel ID="JobContactPanel" runat="server" CssClass="singleAccordionContent">
                    <p>
                        <%= HtmlLocalise("JobContactWarning.Text", "The contact details below are for internal use only and will not be released or displayed to job seekers.")%></p>
                    <table role="presentation">
                        <tr>
                            <td style="width: 140px;">
                                <%= HtmlLabel("JobContactName.Label", "Contact name")%>
                            </td>
                            <td>
                                <asp:Literal ID="JobContactNameLiteral" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= HtmlLabel("JobContactAddress.Label", "Address")%>
                            </td>
                            <td>
                                <asp:Literal ID="JobContactAddressLine1Literal" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Literal ID="JobContactAddressLine2Literal" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= HtmlLabel("JobContactCity.Label", "City")%>
                            </td>
                            <td>
                                <asp:Literal ID="JobContactCityLiteral" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= HtmlLabel("JobContactState.Label", "State")%>
                            </td>
                            <td>
                                <asp:Literal ID="JobContactStateLiteral" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= HtmlLabel("JobContactZip.Label", "ZIP or postal code")%>
                            </td>
                            <td>
                                <asp:Literal ID="JobContactZipLiteral" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= HtmlLabel("JobContactTelephone.Label", "Telephone")%>
                            </td>
                            <td>
                                <asp:Literal ID="JobContactTelephoneLiteral" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= HtmlLabel("EmailAddress.Label", "Email address")%>
                            </td>
                            <td>
                                <asp:Literal ID="JobContactEmailAddressLiteral" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <act:CollapsiblePanelExtender ID="JobContactPanelExtender" runat="server" TargetControlID="JobContactPanel"
                ExpandControlID="JobContactHeaderPanel" CollapseControlID="JobContactHeaderPanel"
                Collapsed="false" ImageControlID="JobContactHeaderImage" SuppressPostBack="true"
                BehaviorID="JobContactPanelBehaviour" />
            <asp:Panel ID="ClosingDateHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
                <table role="presentation" class="accordionTable">
                    <tr class="multipleAccordionTitle">
                        <td>
                            <asp:Image ID="ClosingDateHeaderImage" runat="server" AlternateText="Closing Date Header Image" />
                            &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ClosingDate.Label", "Closing date")%></a></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div class="singleAccordionContentWrapper">
                <asp:Panel ID="ClosingDatePanel" runat="server" CssClass="singleAccordionContent">
                    <table role="presentation">
                        <tr>
                            <td style="width: 175px; height:50px">
                                <%= HtmlRequiredLabel(ClosingDateTextBox, "JobClosingDate.Label", "Job closing date")%>
                            </td>
                            <td>
                                <asp:TextBox ID="ClosingDateTextBox" runat="server" ClientIDMode="Static" />
                            </td>
														<td style="padding-left: 10px">
															<asp:PlaceHolder runat="server" ID="AdvertiseFor30DaysPlaceHolder" Visible="False">
																<asp:Label CssClass="error" runat="server" ID="AdvertiseFor30DaysText"></asp:Label>
																<br />
															</asp:PlaceHolder>
															<val:ModelPropertyValidator ID="ClosingDateValidator" runat="server" CssClass="validator"
																	ControlToValidate="ClosingDateTextBox" Display="Dynamic" PropertyName="JobClosingOn"
																	ModelType="Focus.Web.ViewModels.JobWizardViewModel" />
															<asp:CompareValidator ID="ClosingDateIsValidCompareValidator" runat="server" ControlToValidate="ClosingDateTextBox"
																	Type="Date" Operator="DataTypeCheck" SetFocusOnError="true" Display="Dynamic"
																	CssClass="error" ValidateEmptyText="true" />
															<asp:CompareValidator ID="ClosingDateCompareValidator" runat="server" ErrorMessage="CompareValidator"
																	ControlToValidate="ClosingDateTextBox" Type="Date" Operator="LessThanEqual" CssClass="error"
																	Display="Dynamic" />
															<asp:CompareValidator ID="ClosingDateGreaterThanTodayCompareValidator" runat="server"
																	ErrorMessage="CompareValidator" ControlToValidate="ClosingDateTextBox" Type="Date"
																	Operator="GreaterThan" CssClass="error" Display="Dynamic" />
															<asp:CustomValidator runat="server" ID="ClosingDateCustomValidator" ControlToValidate="ClosingDateTextBox"
																	Display="None" ClientValidationFunction="JobWizardStep7_ClosingDateValidation"
																	ValidationGroup="JobActionModal"></asp:CustomValidator>
														</td>
                        </tr>
                    </table>
                    <act:CalendarExtender ID="ClosingDateCalendarExtender" runat="server" TargetControlID="ClosingDateTextBox" CssClass="cal_Theme1" />
                </asp:Panel>
            </div>
            <act:CollapsiblePanelExtender ID="ClosingDatePanelExtender" runat="server" TargetControlID="ClosingDatePanel"
                ExpandControlID="ClosingDateHeaderPanel" CollapseControlID="ClosingDateHeaderPanel"
                Collapsed="false" ImageControlID="ClosingDateHeaderImage" SuppressPostBack="true"
                BehaviorID="ClosingDatePanelBehaviour" />
            <asp:Panel ID="NumberOfOpeningsHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
                <table role="presentation" class="accordionTable">
                    <tr class="multipleAccordionTitle">
                        <td>
                            <asp:Image ID="NumberOfOpeningsHeaderImage" runat="server" AlternateText="Number Of Openings Header Image" />
                            &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("NumberOfOpeningsPanel.Label", "Number of openings")%></a></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div class="singleAccordionContentWrapper">
                <asp:Panel ID="NumberOfOpeningsPanel" runat="server" CssClass="singleAccordionContent">
                    <table role="presentation">
                        <tr>
                            <td width="175px">
                                <%= HtmlRequiredLabel(NumberOfOpeningsTextBox, "NumberOfOpenings.Label", "Number of openings")%>
                            </td>
                            <td>
                                <asp:TextBox ID="NumberOfOpeningsTextBox" runat="server" ClientIDMode="Static" Width="35px" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="NumberOfOpeningsRequired" runat="server" ControlToValidate="NumberOfOpeningsTextBox"
                                    CssClass="error" Display="Dynamic" />
                                <asp:CompareValidator runat="server" Operator="GreaterThan" ValueToCompare="0" ID="MinimumNumberOfOpeningsValidator"
                                    ControlToValidate="NumberOfOpeningsTextBox" Type="Integer" SetFocusOnError="True"
                                    CssClass="error" Display="Dynamic" />
                                <asp:CompareValidator runat="server" Operator="LessThanEqual" ValueToCompare="200"
                                    ID="MaximumNumberOfOpeningsValidator" ControlToValidate="NumberOfOpeningsTextBox"
                                    Type="Integer" SetFocusOnError="True" CssClass="error" Display="Dynamic" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <act:CollapsiblePanelExtender ID="NumberOfOpeningsPanelExtender" runat="server" TargetControlID="NumberOfOpeningsPanel"
                ExpandControlID="NumberOfOpeningsHeaderPanel" CollapseControlID="NumberOfOpeningsHeaderPanel"
                Collapsed="false" ImageControlID="NumberOfOpeningsHeaderImage" SuppressPostBack="true"
                BehaviorID="NumberOfOpeningsPanelBehaviour" />
            <asp:Panel ID="InterviewContactPreferencesHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
                <table role="presentation" class="accordionTable">
                    <tr class="multipleAccordionTitle">
                        <td>
                            <asp:Image ID="InterviewContactPreferencesHeaderImage" runat="server" AlternateText="Interview Contact Preferences Header Image" />
                            &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("InterviewContactPreferences.Label", "Interview contact preferences")%></a></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div class="singleAccordionContentWrapper">
                <asp:Panel ID="InterviewContactPreferencesPanel" runat="server" CssClass="singleAccordionContent interviewContactPreferences">
                    <p>
                        <%= HtmlRequiredLabel(FocusTalentAccountCheckbox, "InterviewContactPreferences.Text", "Please check all that apply. At least one contact method must be selected.")%></p>
                    <p>
                        <focus:LocalisedLabel runat="server" ID="ReceiveApplicationsLabel" RenderOuterSpan="True"
                            DefaultText="Receive applications through:" />
                    </p>
                    <div>
                        <focus:CheckBoxCustomValidator ID="InterviewContactPreferencesValidator" runat="server"
                            ControlToValidate="FocusTalentAccountCheckbox" ClientValidationFunction="validateInterviewContactPreferences"
                            ValidateEmptyText="True" Display="Dynamic" CssClass="error" />
                    </div>
                    <div class="interviewContactPreferences1">
                        <table role="presentation">
                            <tr>
                                <td style="vertical-align: top; white-space: nowrap;" colspan="2">
                                    <asp:CheckBox ID="FocusTalentAccountCheckbox" TextAlign="Right" runat="server" ClientIDMode="Static" />
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; white-space: nowrap;">
                                    <asp:CheckBox ID="EmailResumeCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" />&nbsp;
                                </td>
                                <td>
                                    <%= HtmlInFieldLabel("EmailAddressTextBox", "EmailAddress.InlineLabel", "email address", null)%>
                                    <asp:TextBox ID="EmailAddressTextBox" runat="server" ClientIDMode="Static" MaxLength="255" />
                                    <span>
                                        <asp:CustomValidator ID="EmailAddressValidator" runat="server" ControlToValidate="EmailAddressTextBox"
                                            ClientValidationFunction="validateEmailAddress" ValidateEmptyText="True" Display="Dynamic"
                                            CssClass="error block" />
                                        <asp:RegularExpressionValidator ID="EmailAddressRegEx" runat="server" ControlToValidate="EmailAddressTextBox"
                                            SetFocusOnError="true" Display="Dynamic" CssClass="error" />
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; white-space: nowrap;">
                                    <asp:CheckBox ID="ApplyOnlineCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" />&nbsp;
                                </td>
                                <td>
                                    <%= HtmlInFieldLabel("ApplyOnlineTextBox", "ApplyOnline.InlineLabel", "enter URL beginning with http:// or https://", null)%>
                                    <asp:TextBox ID="ApplyOnlineTextBox" runat="server" ClientIDMode="Static" MaxLength="4000" />
                                    <span>
                                        <asp:CustomValidator ID="ApplyOnlineValidator" runat="server" ControlToValidate="ApplyOnlineTextBox"
                                            ClientValidationFunction="validateApplyOnline" ValidateEmptyText="True" CssClass="error" />
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; white-space: nowrap;">
                                    <asp:CheckBox ID="MailResumeCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" />&nbsp;
                                </td>
                                <td>
                                    <%= HtmlInFieldLabel("MailResumeTextBox", "MailResume.InlineLabel", "address", null)%>
                                    <asp:TextBox ID="MailResumeTextBox" runat="server" TextMode="MultiLine" Rows="4"
                                        ClientIDMode="Static" />
                                    <span>
                                        <asp:CustomValidator ID="MailResumeValidator" runat="server" ControlToValidate="MailResumeTextBox"
                                            ClientValidationFunction="validateMailResume" ValidateEmptyText="True" Display="Dynamic"
                                            CssClass="error" />
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; white-space: nowrap;">
                                    <asp:CheckBox ID="FaxResumeCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" />&nbsp;
                                </td>
                                <td>
                                    <%= HtmlInFieldLabel("FaxResumeTextBox", "FaxResume.InlineLabel", "enter 10-digit fax resume", null)%>
                                    <asp:TextBox ID="FaxResumeTextBox" runat="server" ClientIDMode="Static" MaxLength="25" />
                                    <span>
                                        <asp:RegularExpressionValidator ID="FaxResumeRegEx" runat="server" ControlToValidate="FaxResumeTextBox"
                                            SetFocusOnError="true" Display="Dynamic" CssClass="error" />
                                    </span>
                                    <act:MaskedEditExtender ID="FaxResumeMaskedEdit" runat="server" MaskType="Number"
                                        TargetControlID="FaxResumeTextBox" PromptCharacter="_" ClearMaskOnLostFocus="false"
                                        AutoComplete="false" EnableViewState="true" ClearTextOnInvalid="false" />
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; white-space: nowrap;">
                                    <asp:CheckBox ID="PhoneForAppointmentCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" />&nbsp;
                                </td>
                                <td>
                                    <%= HtmlInFieldLabel("PhoneForAppointmentTextBox", "PhoneForAppointment.InlineLabel", "enter 10-digit phone number", null)%>
                                    <asp:TextBox ID="PhoneForAppointmentTextBox" runat="server" ClientIDMode="Static"
                                        MaxLength="25" />
                                    <act:MaskedEditExtender ID="PhoneForAppointmentMaskedEdit" runat="server" MaskType="Number"
                                        TargetControlID="PhoneForAppointmentTextBox" PromptCharacter="_" ClearMaskOnLostFocus="false"
                                        AutoComplete="false" EnableViewState="true" ClearTextOnInvalid="false" />
                                    <span>
                                        <asp:RegularExpressionValidator ID="PhoneForAppointmentRegEx" runat="server" ControlToValidate="PhoneForAppointmentTextBox"
                                            SetFocusOnError="true" Display="Dynamic" CssClass="error" />
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; white-space: nowrap;">
                                    <asp:CheckBox ID="InPersonCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" />&nbsp;
                                </td>
                                <td>
                                    <%= HtmlInFieldLabel("InPersonTextBox", "InPerson.InlineLabel", "contact name and address", null)%>
                                    <asp:TextBox ID="InPersonTextBox" runat="server" TextMode="MultiLine" Rows="4" ClientIDMode="Static" MaxLength="2000" />
                                    <span>
																				<asp:RegularExpressionValidator ID="InPersonLengthValidator" runat="server" ControlToValidate="InPersonTextBox" ValidationExpression="^(?:\S|[^\S\r\n]|\r?\n){0,2000}$" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
                                        <asp:CustomValidator ID="InPersonValidator" runat="server" ControlToValidate="InPersonTextBox"
                                            ClientValidationFunction="validateInPerson" ValidateEmptyText="True" CssClass="error" />
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="interviewContactPreferences2">
                        <table role="presentation">
                            <tr>
                                <td style="vertical-align: top; white-space: nowrap;">
                                    <asp:Label runat="server" AssociatedControlID="OtherInstructionsTextBox"><%= HtmlLabel("OtherInstructions.Text", "Other or specific instructions")%></asp:Label>
                                </td>
                                <td style="vertical-align: top; text-align: left;">
                                    <asp:TextBox ID="OtherInstructionsTextBox" runat="server" TextMode="MultiLine" Rows="4"
                                        ClientIDMode="Static" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br class="clear" />
                </asp:Panel>
            </div>
            <act:CollapsiblePanelExtender ID="InterviewContactPreferencesPanelExtender" runat="server"
                TargetControlID="InterviewContactPreferencesPanel" ExpandControlID="InterviewContactPreferencesHeaderPanel"
                CollapseControlID="InterviewContactPreferencesHeaderPanel" Collapsed="false"
                ImageControlID="InterviewContactPreferencesHeaderImage" SuppressPostBack="true"
                BehaviorID="InterviewContactPreferencesPanelBehaviour" />
            <asp:Panel ID="ScreeningPreferencesHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
                <table role="presentation" class="accordionTable">
                    <tr class="multipleAccordionTitle">
                        <td>
                            <asp:Image ID="ScreeningPreferencesHeaderImage" runat="server" AlternateText="Screening Preferences Header Image"/>
                            &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ScreeningPreferences.Label", "Screening preferences")%></a></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div class="singleAccordionContentWrapper">
                <asp:Panel ID="ScreeningPreferencesPanel" runat="server" CssClass="singleAccordionContent">
                    <table role="presentation">
                        <tr id="AllowUnqualifiedApplicationsRow" runat="server">
                          <td>
                              <asp:RadioButton ID="AllowUnqualifiedApplicationsRadioButton" runat="server" GroupName="ScreenPreferences"
                                  ClientIDMode="Static" />
                          </td>
                          <td>
                              <%= HtmlLocalise(ScreeningPreferences.AllowUnqualifiedApplications.ToString(), "Allow interested job seekers to apply")%>
                              <div style="display: inline-block;"><%= HtmlTooltipster("tooltipWithArrow", "AllowUnqualifiedApplications.Tooltip", @"All job seekers can apply regardless of their qualifications. No screening by staff will take place.")%></div>
                          </td>
                        </tr>
                        <tr id="JobSeekersMustHaveMinimumStarMatchRow" runat="server">
                          <td>
                              <asp:RadioButton ID="JobSeekersMustHaveMinimumStarMatchRadioButton" runat="server"
                                  GroupName="ScreenPreferences" ClientIDMode="Static" />
                          </td>
                          <td>
                              <%= HtmlLocalise("JobSeekersWithAMatchScoreOf.Label", "Only job seekers with a match score of")%>&nbsp;<asp:DropDownList
                                  runat="server" ID="ScreeningPreferencesMinimumStarsDropDown" />
                              &nbsp;<%= HtmlLocalise("MayApplyWithoutStaffPermisssion.Label", "may apply without staff permission")%>
                              <div style="display: inline-block;"><%= HtmlTooltipster("tooltipWithArrow", "MayApplyWithoutStaffPermisssion.Tooltip", @"Only job seekers who meet your selected match score (1-5 stars) may apply.")%></div>
                          </td>
                        </tr>
                        <tr id="JobSeekersMinimumStarsToApplyRow" runat="server">
                            <td>
                                <asp:RadioButton ID="JobSeekersMinimumStarsToApplyRadioButton" runat="server" GroupName="ScreenPreferences"
                                    ClientIDMode="Static" />
                            </td>
                            <td>
                                <%= HtmlLocalise("JobSeekersWithAMatchScoreOf.Label", "Only job seekers with a match score of")%>&nbsp;<asp:DropDownList
                                    runat="server" ID="ScreeningPreferencesMinimumStarsToApplyDropDown" />
                                &nbsp;<%= HtmlLocalise("OrAboveMayApply.Label", "or above may apply")%>
                                <div style="display: inline-block;"><%= HtmlTooltipster("tooltipWithArrow", "OrAboveMayApply.Tooltip", @"All job seekers who meet or exceed your selected match score (1-5 stars) may apply. Those below the match score will be screened by staff and referred to you, if qualified.")%></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton ID="JobSeekersMustBeScreenedRadioButton" runat="server" GroupName="ScreenPreferences"
                                    ClientIDMode="Static" />
                            </td>
                            <td>
                                <%= HtmlLocalise(ScreeningPreferences.JobSeekersMustBeScreened.ToString(), "Staff must screen all job seekers before they can apply")%>
                                <div style="display: inline-block;"><%= HtmlTooltipster("tooltipWithArrow", "JobSeekersMustBeScreened.Tooltip", @"All job seekers who wish to apply will be screened by staff and referred to you, if qualified.")%></div>
                            </td>
                        </tr>
                    </table>
                    <asp:PlaceHolder runat="server" ID="PreScreeningServiceRequestPanel">
                        <br />
                        <asp:Literal runat="server" ID="PreScreeningServiceRequestHeader" />
                        <div style="margin-left: 15px">
                            <asp:RadioButtonList runat="server" ID="PreScreeningServiceRequestRadioButtons" />
                            <asp:RequiredFieldValidator ID="PreScreeningServiceRequestValidator" runat="server"
                                ControlToValidate="PreScreeningServiceRequestRadioButtons" CssClass="error" Display="Dynamic" />
                        </div>
                    </asp:PlaceHolder>
                </asp:Panel>
            </div>
            <act:CollapsiblePanelExtender ID="ScreeningPreferencesPanelExtender" runat="server"
                TargetControlID="ScreeningPreferencesPanel" ExpandControlID="ScreeningPreferencesHeaderPanel"
                CollapseControlID="ScreeningPreferencesHeaderPanel" Collapsed="false" ImageControlID="ScreeningPreferencesHeaderImage"
                SuppressPostBack="true" BehaviorID="ScreeningPreferencesPanelBehaviour" />
            <asp:PlaceHolder runat="server" ID="VeteranPriorityPlaceHolder">
                <asp:Panel ID="VeteranPriorityOfServiceHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
                    <table role="presentation" class="accordionTable">
                        <tr class="multipleAccordionTitle">
                            <td>
                                <asp:Image ID="VeteranPriorityOfServiceHeaderImage" runat="server" AlternateText="Veteran Priority Of Service Header Image" />
                                &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("VeteranPriorityOfService.Label", "Veteran priority of service")%></a></span>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div class="singleAccordionContentWrapper">
                    <asp:Panel ID="VeteranPriorityOfServicePanel" runat="server" CssClass="singleAccordionContent">
                        <p>
                            <focus:LocalisedLabel runat="server" ID="VeteranPriorityComplianceLabel" DefaultText="In order to comply with the Jobs for Veterans Act by the US Department of Labor, postings will automatically be available exclusively to veterans for 24 hours as standard. This is providing relevant veterans are found when this job is saved, and those veterans will be alerted to your vacancy." />
                            <br />
                            <br />
														<focus:LocalisedLabel runat="server" ID="VeteranPriorityExlusivityLabel" DefaultText="The options below allow the extension of this exclusivity for a date of your choice, up to and including the life of the posting." />
                        </p>
                        <table role="presentation">
                            <tr>
                                <td colspan="2">
                                    <asp:CheckBox runat="server" ID="ExtendPriorityOfServiceCheckBox" ClientIDMode="Static"
                                        onchange="ExtendVeteranPriorityChanged();" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton runat="server" ID="ExtendPriorityOfServiceUntilRadioButton" GroupName="ExtendPriorityOfServiceRadioButtonList"
                                        ClientIDMode="Static" onchange="ExtendPriorityOfServiceRadioButtonChanged();" />
                                </td>
                                <td>
                                    <%= HtmlLocalise(ExtendVeteranPriorityOfServiceTypes.ExtendUntil.ToString(), "Extend until")%>&nbsp;&nbsp;
                                    <asp:TextBox runat="server" ID="VeteranPriorityExtendUntilDateTextBox" ClientIDMode="Static"></asp:TextBox>
                                    <act:CalendarExtender ID="VeteranPriorityExtendUntilDateCalendarExtender" runat="server"
                                        TargetControlID="VeteranPriorityExtendUntilDateTextBox" BehaviorID="VeteranPriorityExtendUntilDateCalendarExtenderBehaviour"  CssClass="cal_Theme1" />
                                    <asp:CustomValidator ID="ExtendVeteranDateValidator" runat="server" ControlToValidate="VeteranPriorityExtendUntilDateTextBox"
                                        SetFocusOnError="true" Display="Dynamic" CssClass="error" ClientValidationFunction="ValidateExtendVeteranDate"
                                        ClientIDMode="Static" ValidateEmptyText="True" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton runat="server" ID="ExtendPriorityOfServiceForLifeOfPostingRadioButton"
                                        GroupName="ExtendPriorityOfServiceRadioButtonList" ClientIDMode="Static" onchange="ExtendPriorityOfServiceRadioButtonChanged();" />
                                </td>
                                <td>
                                    <%= HtmlLocalise(ExtendVeteranPriorityOfServiceTypes.ExtendForLifeOfPosting.ToString(), "Extend for life of the posting")%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <focus:CheckBoxCustomValidator ID="ExtendPriorityOfServiceValidator" runat="server"
                                        ControlToValidate="ExtendPriorityOfServiceUntilRadioButton" ClientValidationFunction="ValidateExtendVeteranPriority"
                                        ValidateEmptyText="True" CssClass="error" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
                <act:CollapsiblePanelExtender ID="VeteranPriorityOfServicePanelExtender" runat="server"
                    TargetControlID="VeteranPriorityOfServicePanel" ExpandControlID="VeteranPriorityOfServiceHeaderPanel"
                    CollapseControlID="VeteranPriorityOfServiceHeaderPanel" Collapsed="false" ImageControlID="VeteranPriorityOfServiceHeaderImage"
                    SuppressPostBack="true" BehaviorID="VeteranPriorityOfServicePanelBehaviour" />
            </asp:PlaceHolder>
        </td>
    </tr>
</table>
<script type="text/javascript">
    $(document).ready(function () {
        if ($("#<%=VeteranPriorityOfServiceHeaderPanel.ClientID %>").exists()) {
            ExtendVeteranPriorityChanged();
            ExtendPriorityOfServiceRadioButtonChanged();
        }
    });

    Sys.Application.add_load(JobWizardStep7_PageLoad);

    function JobWizardStep7_PageLoad() {

        if ($("#<%= InterviewContactPreferencesPanel.ClientID %>").is(":visible")) {

        		var emailCheckBox = $("#EmailResumeCheckBox");
        		var emailTextBox = $("#EmailAddressTextBox");
            emailCheckBox.change(function () {
            	JobWizardStep7_TextFieldChange(emailCheckBox, emailTextBox);
            });
            JobWizardStep7_TextFieldChange(emailCheckBox, emailTextBox);

            var onlineCheckBox = $("#ApplyOnlineCheckBox");
            var onlineTextBox = $("#ApplyOnlineTextBox");
            onlineCheckBox.change(function () {
            	JobWizardStep7_TextFieldChange(onlineCheckBox, onlineTextBox);
            });
            JobWizardStep7_TextFieldChange(onlineCheckBox, onlineTextBox);

            var mailCheckBox = $("#MailResumeCheckBox");
            var mailTextBox = $("#MailResumeTextBox");
            mailCheckBox.change(function () {
            	JobWizardStep7_TextFieldChange(mailCheckBox, mailTextBox);
            });
            JobWizardStep7_TextFieldChange(mailCheckBox, mailTextBox);

						var faxCheckBox = $("#FaxResumeCheckBox");
						var faxTextBox = $("#FaxResumeTextBox");
            faxCheckBox.change(function () {
            	JobWizardStep7_PhoneNumberChange(faxCheckBox, faxTextBox, '<%= FaxResumeRegEx.ClientID %>', true);
            });
            JobWizardStep7_PhoneNumberChange(faxCheckBox, faxTextBox, '<%= FaxResumeRegEx.ClientID %>', false);

            var phoneCheckBox = $("#PhoneForAppointmentCheckBox");
						var phoneTextBox = $("#PhoneForAppointmentTextBox");
            phoneCheckBox.change(function () {
            	JobWizardStep7_PhoneNumberChange(phoneCheckBox, phoneTextBox, '<%= PhoneForAppointmentRegEx.ClientID %>', true);
            });
            JobWizardStep7_PhoneNumberChange(phoneCheckBox, phoneTextBox, '<%= PhoneForAppointmentRegEx.ClientID %>', false);

            var inpersonCheckBox = $("#InPersonCheckBox");
            var inpersonTextBox = $("#InPersonTextBox");
            inpersonCheckBox.change(function () {
            	JobWizardStep7_TextFieldChange(inpersonCheckBox, inpersonTextBox);
            });
            JobWizardStep7_TextFieldChange(inpersonCheckBox, inpersonTextBox);
	        
            bindExpandCollapseAllPanels('<%= ExpandCollapseAllButton.ClientID %>', null); // bind the magic
        }
    }

		function JobWizardStep7_TextFieldChange(checkBox, textBox) {
			var isChecked = checkBox.is(":checked");
			if (!isChecked) {
				textBox.val("");
			}
			textBox.change();
			textBox.prop('disabled', !isChecked);
		}

		function JobWizardStep7_PhoneNumberChange(phoneCheckBox, phoneTextBox, validatorId, resetField) {
			var isChecked = phoneCheckBox.is(":checked");
			if (!isChecked) {
				phoneTextBox.val("");
			} else if (resetField) {
				var emptyMask = '<%=OldApp_RefactorIfFound.Settings.PhoneNumberMaskPattern.Replace("9", "_")%>';
				phoneTextBox.val(emptyMask);
			}

			phoneTextBox.change();
			phoneTextBox.prop('disabled', !isChecked);
			ValidatorEnable(document.getElementById(validatorId), isChecked);
		}

		function JobWizardStep7_ClosingDateValidation(sender, args) {
    	var dateValid = $("#" + JobWizardStep7_ClientIds["ClosingDateIsValidCompareValidator"])[0].isvalid;

	    var closingDateValidator = $("#" + JobWizardStep7_ClientIds["ClosingDateCompareValidator"]);
	    if (closingDateValidator.exists()) {
	    	ValidatorEnable(closingDateValidator[0], dateValid);
	    }
      ValidatorEnable($("#" + JobWizardStep7_ClientIds["ClosingDateGreaterThanTodayCompareValidator"])[0], dateValid);

      if (dateValid) {
	      if (closingDateValidator.exists()) {
	      	ValidatorValidate(closingDateValidator[0]);
	      }
	      ValidatorValidate($("#" + JobWizardStep7_ClientIds["ClosingDateGreaterThanTodayCompareValidator"])[0]);
      }
    }

    function ExtendVeteranPriorityChanged() {
        var untilRadioButton = $('#ExtendPriorityOfServiceUntilRadioButton');
        var lifeOfPostingRadioButton = $('#ExtendPriorityOfServiceForLifeOfPostingRadioButton');
        var untilDateTextbox = $('#VeteranPriorityExtendUntilDateTextBox');

        if ($('#ExtendPriorityOfServiceCheckBox').is(':checked')) {
            $(untilRadioButton).prop('disabled', false);
            $(lifeOfPostingRadioButton).prop('disabled', false);
        }
        else {
            $(untilDateTextbox).val('').prop('disabled', true);
            $(untilRadioButton).prop('checked', false);
            $(lifeOfPostingRadioButton).prop('checked', false);

            $(untilRadioButton).prop('disabled', true);
            $(lifeOfPostingRadioButton).prop('disabled', true);

            ValidatorValidate($("#" + JobWizardStep7_ClientIds["ExtendPriorityOfServiceValidator"])[0]);
            ValidatorValidate($("#" + JobWizardStep7_ClientIds["ExtendVeteranDateValidator"])[0]);

            var calExtender = $find("VeteranPriorityExtendUntilDateCalendarExtenderBehaviour");
            if (calExtender != null && calExtender.get_isOpen())
                calExtender.hide();
        }
    }

    function ValidateExtendVeteranDate(src, args) {
        if ($('#ExtendPriorityOfServiceCheckBox').is(':checked') && $('#ExtendPriorityOfServiceUntilRadioButton').is(':checked')) {
            var extendUntilDateTextBox = $('#VeteranPriorityExtendUntilDateTextBox');
            var closingDate = $('#ClosingDateTextBox').val();

            if (extendUntilDateTextBox.val() == '') {
                src.innerHTML = '<%= ExtendUntilDateRequiredErrorMessage %>';
                args.IsValid = false;
            }
            else {
                var extendUntilDate = moment(args.Value, "M/D/YYYY", true);

                if (!extendUntilDate.isValid()) {
                    src.innerHTML = '<%= ExtendUntilDateInvalidErrorMessage %>';
                    args.IsValid = false;
                }
                else if (extendUntilDate.isBefore(moment())) {
                    src.innerHTML = '<%= ExtendUntilDateInPastErrorMessage %>';
                    args.IsValid = false;
                }
                else if (closingDate != '') {
                    var closingDateMoment = moment(closingDate, "M/D/YYYY", true);

                    if (extendUntilDate.isAfter(closingDateMoment)) {
                        src.innerHTML = '<%= ExtendUntilDateAfterClosingDateErrorMessage %>';
                        args.IsValid = false;
                    }
                    else if (extendUntilDate.isSame(closingDateMoment, 'day')) {
                        src.innerHTML = "<%= SelectLifeOfPostingErrorMessage %>";
                        args.IsValid = false;
                    }
                }
            }
        }
    }

    function ValidateExtendVeteranPriority(src, args) {
        if ($('#ExtendPriorityOfServiceCheckBox').is(':checked')) {
            if (!$('#ExtendPriorityOfServiceUntilRadioButton').is(':checked') && !$('#ExtendPriorityOfServiceForLifeOfPostingRadioButton').is(':checked')) {
                src.innerHTML = '<%= ExtendVeteranPriorityRequiredErrorMessage %>';
                args.IsValid = false;
            }
        }
    }

    function ExtendPriorityOfServiceRadioButtonChanged() {
        ValidatorEnable(document.getElementById('<%= ExtendVeteranDateValidator.ClientID %>'), $('#ExtendPriorityOfServiceUntilRadioButton').is(':checked'));

        var untilDateTextbox = $('#VeteranPriorityExtendUntilDateTextBox');

        if (!$('#ExtendPriorityOfServiceUntilRadioButton').is(':checked'))
            untilDateTextbox.val('').prop('disabled', true);
        else if ($('#ExtendPriorityOfServiceUntilRadioButton').is(':checked'))
            untilDateTextbox.prop('disabled', false);
    }
</script>
