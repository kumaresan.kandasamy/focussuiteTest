#region Copyright � 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Core;
using Focus.Web.ViewModels;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class JobWizardStep3Path2Part2 : JobWizardControl
	{
    private JobTypes _jobType;
		private bool _resetDescription;

    /// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
      if (!IsPostBack)
        LocaliseUI();
		}

    /// <summary>
    /// Fires when the Reset Description button is clicked to reset the description
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ResetDescriptionButton_OnClick(object sender, EventArgs e)
    {
      // JobDescriptionTextBox.Text = "";
			_resetDescription = true;
      OnGoToStep(new CommandEventArgs("GOTOSTEP", 2));
    }

		#region localise the UI

    /// <summary>
    /// Localises text on the page
    /// </summary>
    private void LocaliseUI()
    {
      ResetDescriptionButton.Text = HtmlLocalise("ResetDescriptionButton.Text", "Restart job description");
			JobDecriptionHtmlValidator.ErrorMessage = CodeLocalise("JobDecriptionHtmlValidator.ErrorMessage", "Job description must not contain HTML mark-up");
		}

    /// <summary>
    /// Localises the UI
    /// </summary>
    private void LocaliseBasedOnJobType()
    {
      if (_jobType.IsInternship())
      {
        TabDescriptionLabel.Text = HtmlLocalise("TabInternshipDescription.Label", "internship description");
        TabSimilarLabel.Text = HtmlLocalise("TabSimilarInternship.Label", "Description for similar internships");
        JobDescriptionRequired.ErrorMessage = CodeLocalise("InternshipDescriptionRequired.ErrorMessage", "Internship description required");
        JobDescriptionInfieldLabel.Text = HtmlInFieldLabel("JobDescriptionTextBox", "InternshipDescription.Label",
                                                           "Type or paste your internship description here.", 350, "inFieldLabelAlt");
      }
      else
      {
        TabDescriptionLabel.Text = HtmlLocalise("TabJobDescription.Label", "job description");
        TabSimilarLabel.Text = HtmlLocalise("TabSimilarJob.Label", "Description for similar jobs");
        JobDescriptionRequired.ErrorMessage = CodeLocalise("JobDescriptionRequired.ErrorMessage", "Job description required");
        JobDescriptionInfieldLabel.Text = HtmlInFieldLabel("JobDescriptionTextBox", "JobDescription.Label",
                                                           "Type or paste your job description here.", 300, "inFieldLabelAlt");
      }
			JobDescriptionValidator.ErrorMessage = CodeLocalise("JobDescriptionValidator.ErrorMessage", "Your description cannot exceed 6000 characters. Please amend");
    }

		#endregion

		#region Bind & Unbind Methods

		/// <summary>
		/// Binds the controls for the step.
		/// </summary>
		/// <param name="model">The model.</param>
		internal void BindStep(JobWizardViewModel model)
		{
      
      
      _jobType = model.Job.JobType;
      model.Job.DescriptionPath = 22;
		  LocaliseBasedOnJobType();

			JobTitleLabel.Text = model.Job.JobTitle;
			JobDescriptionTextBox.Text = model.Job.Description;

		  SimilarJobs.JobType = model.Job.JobType;

		  // Set up Keywords / Statements with the items they will need to populate correctly
			KeywordsStatements.OnetId = model.Job.OnetId;
			KeywordsStatements.JobTitle = model.Job.JobTitle;
		  KeywordsStatements.JobType = model.Job.JobType;
			// Refresh the Keywords and Statements based on the settings
			KeywordsStatements.Refresh();			
		}

		/// <summary>
		/// Unbinds the step.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
		internal JobWizardViewModel UnbindStep(JobWizardViewModel model)
		{
			if (!_resetDescription)
				model.Job.Description = JobDescriptionTextBox.Text.RemoveRogueCharacters();

			model.ResetDescription = _resetDescription;

			return model;
		}

		#endregion
	}
}