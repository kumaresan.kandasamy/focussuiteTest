﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobActivityList.ascx.cs" Inherits="Focus.Web.Code.Controls.User.JobActivityList" %>
<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>
<%@ Register src="~/Code/Controls/User/Pager.ascx" tagname="Pager" tagprefix="uc" %>

<asp:UpdatePanel ID="JobActivityListUpdatePanel" runat="server" UpdateMode="Conditional">
  <ContentTemplate>
    <table style="width:100%;" role="presentation">
      <tr>
        <td style="white-space:nowrap;">
          <%=HtmlLabel(ServiceDropDown,"Show", "Show")%> <asp:DropDownList runat="server" ID="ServiceDropDown" OnSelectedIndexChanged="ServiceDropDown_SelectedIndexChanged" AutoPostBack="True"> </asp:DropDownList> 
          <%=HtmlLabel(DaysBackDropDown,"for", "for")%> <asp:DropDownList runat="server" ID="DaysBackDropDown" OnSelectedIndexChanged="DaysBackDropdown_SelectedIndexChanged" AutoPostBack="True" /> 
          <%=HtmlLabel(UserDropDown,"by", "by")%> <asp:DropDownList runat="server" ID="UserDropDown" OnSelectedIndexChanged="UserDropDown_SelectedIndexChanged" AutoPostBack="True" />
        </td>
      </tr>
      <tr>
        <td style="white-space:nowrap;"><uc:Pager ID="JobActivityListPager" runat="server" PagedControlID="JobActivityLogList" DisplayRecordCount="False" PageSize="10" /></td>
      </tr>
    </table>
    <asp:ListView runat="server" ID="JobActivityLogList" ItemPlaceholderID="ListPlaceHolder" DataSourceID="JobActivityListDataSource" OnSorting="JobActivityList_Sorting" OnLayoutCreated="JobActivityList_LayoutCreated" OnItemDataBound="JobActivityList_ItemDataBound">
      <LayoutTemplate>
        <table style="width:100%;" id="JobActivityLogListTable">
					<tr>
						<th>
							<table role="presentation">
								<tr>
									<td rowspan="2" style="height:16px;"><asp:Literal runat="server" ID="UserName" /></td>
									<td style="height:8px;"><asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="UserNameSortAscButton" runat="server" CommandName="Sort" CommandArgument="name asc" AlternateText="."/></td>
								</tr>
								<tr>
									<td style="height:8px;"><asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="UserNameSortDescButton" runat="server" CommandName="Sort" CommandArgument="name desc" AlternateText="."/></td>
								</tr>
							</table>
						</th>
						<th>
							<table role="presentation">
								<tr>
									<td rowspan="2" style="height:16px"><asp:Literal runat="server" ID="ActivityDate" /></td>
									<td style="height:8px"><asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="ActivityDateSortAscButton" runat="server" CommandName="Sort" CommandArgument="activitydate asc" AlternateText="."/></td>
								</tr>
								<tr>
									<td style="height:8px"><asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="ActivityDateSortDescButton" runat="server" CommandName="Sort" CommandArgument="activitydate desc" AlternateText="."/></td>
								</tr>
							</table>
						</th>
						<th style="text-align:left">
						  <asp:Literal runat="server" ID="Action" />
						</th>
          </tr>
					<tr>
						<td colspan="3"><hr /></td>
					</tr>
          <asp:PlaceHolder ID="ListPlaceHolder" runat="server" />
        </table>
      </LayoutTemplate>
      <ItemTemplate>
        <tr>
          <td runat="server" id="UsernameDisplay"><%# ((JobActionEventViewDto)Container.DataItem).LastName%>, <%# ((JobActionEventViewDto)Container.DataItem).FirstName%></td>
          <td runat="server" id="Date"><%# ((JobActionEventViewDto)Container.DataItem).ActionedOn%></td>
          <td><asp:Label runat="server" ID="ActionDescription"></asp:Label></td>
        </tr>
      </ItemTemplate>
      <EmptyDataTemplate><br/><%=HtmlLocalise("NoJobActivity", "No activity recorded for this #EMPLOYMENTTYPE#:LOWER")%></EmptyDataTemplate>
    </asp:ListView>
    <asp:ObjectDataSource runat="server" ID="JobActivityListDataSource" TypeName="Focus.Web.Code.Controls.User.JobActivityList"  SortParameterName="orderBy" SelectMethod="GetJobActivity" SelectCountMethod="GetJobActivityCount" OnSelecting="JobActivityListDataSource_Selecting" EnablePaging="True" ></asp:ObjectDataSource>
  </ContentTemplate>
</asp:UpdatePanel>
<br/>