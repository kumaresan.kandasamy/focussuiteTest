﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeSearchAvailabilityCriteria.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchAvailabilityCriteria" %>

<asp:Panel ID="AvailabilityHeaderPanel" runat="server" CssClass="singleAccordionTitle noMarginBottom">
	<asp:Image ID="AvailabilityHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" alt="." Height="22" Width="22" />&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Availability.Label", "Availability")%></a></span>
</asp:Panel>
<asp:Panel ID="AvailabilityPanel" runat="server" CssClass="accordionContent">
	<table role="presentation" class="multiLineChecboxTable">
		<tr id="WorkOvertimeRow" runat="server">
			<td><%= HtmlLabel("WorkOvertime.Label", "Willing to work overtime")%></td>
			<td>
				<asp:CheckBox runat="server" ID="WorkOvertimeCheckBox" ClientIDMode="Static" />
			</td>
		</tr>
		<tr id="RelocateRow" runat="server">
			<td><%= HtmlLabel("Relocate.Label", "Willing to relocate")%></td>
			<td>
				<asp:CheckBox runat="server" ID="RelocateCheckBox" ClientIDMode="Static" />
			</td>
		</tr>
		<tr id="WorkShiftsRow" runat="server">
			<td><%= HtmlLabel("NormalWorkShifts.Label", "Normal work shifts")%></td>
			<td>
				<asp:DropDownList runat="server" ID="NormalWorkShiftsDropDown" ClientIDMode="Static" Title="Normal work shifts"/>
			</td>
		</tr>
		<tr id="WorkTypeRow" runat="server">
			<td><%= HtmlLabel("WorkType.Label", "Duration")%></td>
			<td>
				<asp:DropDownList runat="server" ID="WorkTypeDropDown" ClientIDMode="Static" Title="Duration"/>
			</td>
		</tr>
		<tr id="WorkWeekRow" runat="server">
			<td><%= HtmlLabel("WorkWeek.Label", "Work week")%></td>
			<td>
				<asp:DropDownList runat="server" ID="WorkWeekDropDown" ClientIDMode="Static" Title="Work week"/>
			</td>
		</tr>
	</table>
</asp:Panel>
<act:CollapsiblePanelExtender ID="AvailabilityPanelExtender" runat="server" 
														TargetControlID="AvailabilityPanel" 
														ExpandControlID="AvailabilityHeaderPanel" 
														CollapseControlID="AvailabilityHeaderPanel" 
														Collapsed="true" 
														ImageControlID="AvailabilityHeaderImage" 
														CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
														ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
														SuppressPostBack="true"
														BehaviorID="AvailabilityCriteriaBehavior" />