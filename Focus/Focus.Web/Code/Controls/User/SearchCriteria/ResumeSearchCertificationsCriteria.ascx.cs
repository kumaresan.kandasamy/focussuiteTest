﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Common.Extensions;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User.SearchCriteria
{
	public partial class ResumeSearchCertificationsCriteria : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			LocaliseUI();
			ApplyBranding();
		}

		/// <summary>
		/// Handles the Click event of the CertificationsAddButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CertificationsAddButton_Click(object sender, EventArgs e)
		{
			if (CertificationsAddTextBox.TextTrimmed().IsNotNullOrEmpty())
			{
				CertificationsUpdateableList.AddItem(CertificationsAddTextBox.TextTrimmed());
				CertificationsAddTextBox.Text = "";
			}
		}

		/// <summary>
		/// Binds the specified certifications.
		/// </summary>
		/// <param name="certifications">The certifications.</param>
		public void Bind(List<string> certifications)
		{
			if (certifications.IsNotNullOrEmpty())
			{
				CertificationsPanelExtender.Collapsed = false;
				CertificationsUpdateableList.Items = certifications;
			}
		}

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <returns></returns>
		public List<string> Unbind()
		{
			CertificationsAddTextBox.Text = "";
			return CertificationsUpdateableList.Items;
		}

		/// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear()
		{
			CertificationsUpdateableList.Items = null;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			CertificationsAddButton.Text = CodeLocalise("Global.Add.Text.NoEdit", "Add");
			CertificationsAddTextBoxRequired.ErrorMessage = CodeLocalise("CertificationsAddTextBoxRequired.ErrorMessage", "Please enter a certification");
		}

		private void ApplyBranding()
		{
			CertificationsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			CertificationsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			CertificationsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
		}
	}
}