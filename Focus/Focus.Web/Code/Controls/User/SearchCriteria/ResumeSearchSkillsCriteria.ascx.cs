﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User.SearchCriteria
{
	public partial class ResumeSearchSkillsCriteria : UserControlBase
	{
		private List<long> _skillIds;

		/// <summary>
		/// Gets or sets the skill ids.
		/// </summary>
		/// <value>The skill ids.</value>
		public List<long> SkillIds
		{
			get { return SkillsAccordion.SelectedItems; }
			set { _skillIds = value; }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			ApplyBranding();
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		public void Bind()
		{
			if(_skillIds.IsNotNullOrEmpty())
			{
				SkillsCollapsiblePanelExtender.Collapsed = false;
				SkillsAccordion.SelectedItems = _skillIds;
			}
		}

		/// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear()
		{
			SkillsAccordion.SelectedItems = null;
		}

		private void ApplyBranding()
		{
			SkillsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			SkillsCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			SkillsCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}
	}
}