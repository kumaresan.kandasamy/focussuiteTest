﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User.SearchCriteria
{
	public partial class ResumeSearchDrivingLicenceCriteria : UserControlBase
	{
		protected string LicenceEndorsementsJavascriptArray;
		
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			ApplyBranding();
			LicenceEndorsementsJavascriptArray = GetValidLicenceEndorsementsJavascriptArray();
		}

		/// <summary>
		/// Binds the specified criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		public void Bind(CandidateSearchDrivingLicenceCriteria criteria)
		{
			if (criteria.IsNotNull())
			{
				DrivingLicencesPanelExtender.Collapsed = false;
				if (criteria.DrivingLicenceClassId.HasValue)
					DrivingLicenceClassDropDown.SelectValue(criteria.DrivingLicenceClassId.Value.ToString());

				if (criteria.DrivingLicenceEndorsementsIds.IsNotNullOrEmpty())
				{
					foreach (var checkBox in criteria.DrivingLicenceEndorsementsIds.Select(x => DrivingLicenceEndorsementsCheckBoxList.Items.FindByValue(x.ToString())).Where(checkBox => checkBox.IsNotNull()))
					{
						checkBox.Selected = true;
					}
				}
			}
		}

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <returns></returns>
		public CandidateSearchDrivingLicenceCriteria Unbind()
		{
			var drivingLicenceClassId = DrivingLicenceClassDropDown.SelectedValueToLong();
			var drivingLicenceEndorsemantsIds = new List<long>();
			
			foreach(ListItem checkBox in DrivingLicenceEndorsementsCheckBoxList.Items)
			{
				if(checkBox.Selected)
				{
					long id;
					long.TryParse(checkBox.Value, out id);
					if (id != 0) drivingLicenceEndorsemantsIds.Add(id);
				}
			}

			return (drivingLicenceClassId > 0 || drivingLicenceEndorsemantsIds.IsNotNullOrEmpty())
			                             	? new CandidateSearchDrivingLicenceCriteria
			                             	  	{
			                             	  		DrivingLicenceClassId = (drivingLicenceClassId != 0) ? drivingLicenceClassId : (long?) null,
																					DrivingLicenceEndorsementsIds = drivingLicenceEndorsemantsIds
																				} : null;
		}

		/// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear()
		{
			if(DrivingLicenceClassDropDown.Items.Count > 0)
				DrivingLicenceClassDropDown.SelectedIndex = 0;	
			
			foreach (ListItem checkBox in DrivingLicenceEndorsementsCheckBoxList.Items) checkBox.Selected = false;
		}

		/// <summary>
		/// Binds the controls.
		/// </summary>
		public void BindControls()
		{
			BindDrivingLicenceClassDropDown();
			BindDrivingLicenceEndorsementsCheckBoxList();
		}

		/// <summary>
		/// Binds the driving licence class drop down.
		/// </summary>
		private void BindDrivingLicenceClassDropDown()
		{
			DrivingLicenceClassDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceClasses), null, CodeLocalise("DrivingLicenceClass.TopDefault", "- select license class -"));
		}

		/// <summary>
		/// Binds the driving licence endorsements check box list.
		/// </summary>
		private void BindDrivingLicenceEndorsementsCheckBoxList()
		{
			DrivingLicenceEndorsementsCheckBoxList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceEndorsements));
		}

		private void ApplyBranding()
		{
			DrivingLicencesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			DrivingLicencesPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			DrivingLicencesPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}

		/// <summary>
		/// Gets the valid licence endorsements javascript array.
		/// </summary>
		/// <returns></returns>
		private string GetValidLicenceEndorsementsJavascriptArray()
		{
			var javascriptBuilder = new StringBuilder(@"{"""":"""",");

			var endorsementRules = App.Settings.DrivingLicenceEndorsementRules;

			var drivingLicenceLookUps = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceClasses);
			var drivingLicenceEndorsementLookUps = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceEndorsements);

			foreach (var rule in endorsementRules)
			{
				var drivingLicenceKey = string.Format("DrivingLicenceClasses.{0}", rule.LicenceKey);

				var drivingLicenceId = drivingLicenceLookUps.Where(x => x.Key == drivingLicenceKey).Select(x => x.Id).FirstOrDefault();

				if (drivingLicenceId.IsNotNull())
				{
					javascriptBuilder.AppendFormat(@"""{0}"": """, drivingLicenceId);

					foreach (var endorsement in rule.EndorsementKeys)
					{
						var endorsementKey = string.Format("DrivingLicenceEndorsements.{0}", endorsement);

						var endorsementId = drivingLicenceEndorsementLookUps.Where(x => x.Key == endorsementKey).Select(x => x.Id).FirstOrDefault();

						if (endorsementId.IsNotNull())
							javascriptBuilder.AppendFormat("{0},", endorsementId);
					}

					javascriptBuilder.Append(@""",");
				}
			}

			javascriptBuilder.Append("}");

			return javascriptBuilder.ToString().Replace(",}", "}");
		}
	}
}