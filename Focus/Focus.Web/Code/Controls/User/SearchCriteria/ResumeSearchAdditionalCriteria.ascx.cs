﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Framework.Core;

#endregion


namespace Focus.Web.Code.Controls.User.SearchCriteria
{
	public partial class ResumeSearchAdditionalCriteria : UserControlBase
	{
		/// <summary>
		/// Gets or sets the validation group.
		/// </summary>
		/// <value>The validation group.</value>
		public string ValidationGroup { get; set; }

    /// <summary>
    /// Whether to show the military status panel
    /// </summary>
    /// <returns>A boolean indicating whether to show the military status panel or not</returns>
    private bool ShowMilitaryStatus()
    {
      return !App.Settings.HideResumeProfile && App.Settings.ShowMilitaryStatus;
    }
		
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (ValidationGroup.IsNotNullOrEmpty())
				LocationCriteria.ValidationGroup = ValidationGroup;
		}

		protected string Theme
		{
			get { return App.Settings.Theme.ToString(); }
		}

		/// <summary>
		/// Binds the specified criteria.
		/// </summary>
		/// <param name="controlModel">The control model.</param>
		public void Bind(Model controlModel)
		{

			var criteria = controlModel.AdditionalCriteria;
			var theme = App.Settings.Theme;
			var module = App.Settings.Module;
			
			if(criteria.IsNull()) criteria = new CandidateSearchAdditionalCriteria();

			#region Location

			LocationCriteria.Bind(criteria.LocationCriteria);		
			
			#endregion

			#region Education

			var model = new ResumeSearchEducationCriteria.Model();
				
			if (criteria.EducationCriteria.IsNotNull())
			{
				model.EducationLevel = criteria.EducationCriteria.EducationLevel;
				model.LocalisedEducationLevel = criteria.EducationCriteria.EducationLevels;
				model.CurrentlyEnrolledUndergraduate = criteria.EducationCriteria.CurrentlyEnrolledUndergraduate;
				model.MinimumUndergraduateLevel = criteria.EducationCriteria.MinimumUndergraduateLevel;
				model.CurrentlyEnrolledGraduate = criteria.EducationCriteria.CurrentlyEnrolledGraduate;
				model.EnrollmentStatus = criteria.EducationCriteria.EnrollmentStatus;
				model.MonthsUntilExpectedCompletion = criteria.EducationCriteria.MonthsUntilExpectedCompletion;
				model.IncludeAlumni = criteria.EducationCriteria.IncludeAlumni;
			}

			EducationCriteria.Bind(model);

			#endregion

			#region Grade Point Average

			if (theme == FocusThemes.Education)
			{
				GradePointAverageCriteria.MinimumGradePointAverage = (criteria.EducationCriteria.IsNotNull()) ? criteria.EducationCriteria.MinimumGPA : null;
				GradePointAverageCriteria.SearchNotSpecified = (criteria.EducationCriteria.IsNotNull()) && criteria.EducationCriteria.AlsoSearchGPANotSpecified;
				GradePointAverageCriteria.Bind();
			}

			#endregion

			#region Program Of Study

			if (theme == FocusThemes.Education)
			{
				BindProgramOfStudyCriteria(criteria.EducationCriteria);
			}

			#endregion

			#region Languages

      if (criteria.Languages.IsNotNull() && criteria.Languages.Count > 0)
		  {
        LanguageCriteria.Bind(criteria.Languages);
		  }
		  else
		  {
        LanguageCriteria.Bind(criteria.LanguagesWithProficiencies);
		  }

			#endregion

			#region Skills

			if (theme == FocusThemes.Education)
			{
				SkillsCriteria.SkillIds = (criteria.EducationCriteria.IsNotNull()) ? criteria.EducationCriteria.InternshipSkills : null;
				SkillsCriteria.Bind();
			}

			#endregion

			#region Status

			if (ShowMilitaryStatus())
				StatusCriteria.Bind(criteria.StatusCriteria);

			#endregion

			#region Score

			ScoreCriteria.Bind(controlModel.MinimumStarRating);

			#endregion

			#region Industries

			if (App.Settings.SearchByIndustries)
				IndustriesCriteria.Bind(criteria.IndustriesCriteria);

			#endregion

			#region Driving Licence

			if (!App.Settings.HideDrivingLicence)
				DrivingLicenceCriteria.Bind(criteria.DrivingLicenceCriteria);

			#endregion

			#region Licences

			if (theme == FocusThemes.Workforce || module == FocusModules.Assist)
				LicencesCriteria.Bind(criteria.Licences);

			#endregion

			#region Certifications

			CertificationsCriteria.Bind(criteria.Certifications);

			#endregion

			#region Availability

			AvailabilityCriteria.Bind(criteria.AvailabilityCriteria);

			#endregion

			#region Candidate

			if (theme == FocusThemes.Workforce)
				CandidateCriteria.Bind(controlModel.CandidateId);

			#endregion

      #region NCRC Level

      if (theme == FocusThemes.Workforce)
        CareerReadinessCriteria.Bind(criteria.NCRCLevel);

      #endregion
    }

    public void BindProgramOfStudyCriteria(CandidateSearchEducationCriteria educationCriteria)
    {
			if (educationCriteria.IsNotNull() && educationCriteria.ProgramsOfStudy.IsNotNull())
			{
				var programOfStudyList = educationCriteria.ProgramsOfStudy.Select(program => (KeyValuePair<string, string>) program).ToList();

				ProgramOfStudyCriteria.Programs = (programOfStudyList.IsNotNull()) && programOfStudyList.Count > 0 ? programOfStudyList : null;
			}
			else
			{
				ProgramOfStudyCriteria.Programs = null;
			}
			

      ProgramOfStudyCriteria.Bind();
    }

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <param name="jobType">Type of the job.</param>
		/// <returns></returns>
		public Model Unbind(JobTypes? jobType)
		{
			var theme = App.Settings.Theme;
			var module = App.Settings.Module;

			return new Model
			       	{
			       		AdditionalCriteria = new CandidateSearchAdditionalCriteria
			       		                     	{
			       		                     		LocationCriteria =LocationCriteria.Unbind(),
			       		                     		EducationCriteria = UnbindEducationCriteria(jobType),
																				LanguagesWithProficiencies = LanguageCriteria.Unbind(),
			       		                     		StatusCriteria = ShowMilitaryStatus() ? StatusCriteria.Unbind() : null,
																				IndustriesCriteria = (App.Settings.SearchByIndustries) ? IndustriesCriteria.Unbind() : null,
																				DrivingLicenceCriteria = (!App.Settings.HideDrivingLicence) ? DrivingLicenceCriteria.Unbind() : null,
																				Licences = (!(theme == FocusThemes.Education && module == FocusModules.Talent)) ? LicencesCriteria.Unbind() : null,
																				AvailabilityCriteria = (!(theme == FocusThemes.Education && jobType != JobTypes.Job)) ? AvailabilityCriteria.Unbind() : null,
																				Certifications = (!(theme == FocusThemes.Education && module == FocusModules.Talent && jobType != JobTypes.Job)) ? CertificationsCriteria.Unbind() : null,
                                        NCRCLevel = (theme == FocusThemes.Workforce) ? CareerReadinessCriteria.Unbind() : (long?)null
			       		                     	},
								MinimumStarRating = (!(theme == FocusThemes.Education && jobType != JobTypes.Job)) ? ScoreCriteria.Unbind() : 0,
								CandidateId = GetCandidateId()
			       	};
		}

		/// <summary>
		/// Gets the candidate id specified in the search criteria.
		/// </summary>
		/// <returns></returns>
		public long? GetCandidateId()
		{
			return (App.Settings.Theme == FocusThemes.Workforce) ? CandidateCriteria.Unbind() : null;
		}

		/// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear()
		{
			LocationCriteria.Clear();
			EducationCriteria.Clear();
			GradePointAverageCriteria.Clear();
			ProgramOfStudyCriteria.Clear();
			LanguageCriteria.Clear();
			SkillsCriteria.Clear();
			StatusCriteria.Clear();
			ScoreCriteria.Clear();
			DrivingLicenceCriteria.Clear();
			LicencesCriteria.Clear();
			CertificationsCriteria.Clear();
			AvailabilityCriteria.Clear();
			CandidateCriteria.Clear();
      CareerReadinessCriteria.Clear();
		}

		/// <summary>
		/// Binds the controls.
		/// </summary>
		public void BindControls()
		{
			var theme = App.Settings.Theme;
			var module = App.Settings.Module;

			LocationCriteria.BindControls();
			EducationCriteria.BindControls();

			if (theme == FocusThemes.Education)
				GradePointAverageCriteria.BindControls();

			ScoreCriteria.BindControls();

			if (!App.Settings.HideDrivingLicence)
				DrivingLicenceCriteria.BindControls();
			
			AvailabilityCriteria.BindControls();

      if (theme == FocusThemes.Workforce)
        CareerReadinessCriteria.BindControls();

			GradePointAverageCriteria.Visible = (theme == FocusThemes.Education);
			ProgramOfStudyCriteria.Visible = (theme == FocusThemes.Education);
			SkillsCriteria.Visible = (theme == FocusThemes.Education);
		  StatusCriteria.Visible = ShowMilitaryStatus();
			DrivingLicenceCriteria.Visible = (!App.Settings.HideDrivingLicence);
			LicencesCriteria.Visible = (theme == FocusThemes.Workforce || module == FocusModules.Assist);
			CandidateCriteria.Visible = (theme == FocusThemes.Workforce);
			IndustriesCriteriaDiv.Visible = App.Settings.SearchByIndustries;
		  CareerReadinessCriteria.Visible = (theme == FocusThemes.Workforce);
		}

		#region Unbind Education Criteria sections

		/// <summary>
		/// Unbinds the education criteria.
		/// </summary>
		/// <param name="jobType">Type of the job.</param>
		/// <returns></returns>
		private CandidateSearchEducationCriteria UnbindEducationCriteria(JobTypes? jobType)
		{
			var criteria = new CandidateSearchEducationCriteria();
			var theme = App.Settings.Theme;
			
			var educationCriteriaModel = EducationCriteria.Unbind(jobType);

			criteria.EducationLevel = educationCriteriaModel.EducationLevel;
			criteria.EducationLevels = educationCriteriaModel.LocalisedEducationLevel;
			criteria.CurrentlyEnrolledUndergraduate = educationCriteriaModel.CurrentlyEnrolledUndergraduate;
			criteria.MinimumUndergraduateLevel = educationCriteriaModel.MinimumUndergraduateLevel;
			criteria.CurrentlyEnrolledGraduate = educationCriteriaModel.CurrentlyEnrolledGraduate;
			criteria.EnrollmentStatus = educationCriteriaModel.EnrollmentStatus;
			criteria.MonthsUntilExpectedCompletion = educationCriteriaModel.MonthsUntilExpectedCompletion;
			criteria.IncludeAlumni = educationCriteriaModel.IncludeAlumni;
			
			if (theme == FocusThemes.Education)
			{
				criteria.MinimumGPA = GradePointAverageCriteria.MinimumGradePointAverage;
				criteria.AlsoSearchGPANotSpecified = GradePointAverageCriteria.SearchNotSpecified;
			}

			if (theme == FocusThemes.Education)
			{
				if (ProgramOfStudyCriteria.IsNotNull() && ProgramOfStudyCriteria.Programs.IsNotNull())
				{
					if (ProgramOfStudyCriteria.Programs.Count > 0)
						criteria.ProgramsOfStudy = new List<KeyValuePairSerializable<string, string>>();

					foreach (var program in ProgramOfStudyCriteria.Programs)
					{
						criteria.ProgramsOfStudy.Add((KeyValuePairSerializable<string, string>)program);
					}
				}
				else
				{
					criteria.ProgramsOfStudy = null;
				}
				
				if (criteria.ProgramsOfStudy.IsNotNullOrEmpty())
				{
					var allDegreeProgramAreas = criteria.ProgramsOfStudy.Where(p => p.Key.StartsWith("-")).Select(p => p.Key).ToList();
					if (allDegreeProgramAreas.Count > 0)
					{
						criteria.ExtraDegreeLevelEducationIds = new List<string>();
						allDegreeProgramAreas.ForEach(id => criteria.ExtraDegreeLevelEducationIds.AddRange(ServiceClientLocator.ExplorerClient(App).GetProgramAreaDegreeEducationLevels(0 - long.Parse(id)).Select(pad => pad.DegreeEducationLevelId.ToString(CultureInfo.InvariantCulture))));
					}
				}
			}

			if (theme == FocusThemes.Education && jobType != JobTypes.Job)
			{
				criteria.InternshipSkills = SkillsCriteria.SkillIds;
			}

			return criteria;
		}

		#endregion

		public class Model
		{
			public CandidateSearchAdditionalCriteria AdditionalCriteria { get; set; }
			public int MinimumStarRating { get; set; }
			public long? CandidateId { get; set; }
		}
	}
}