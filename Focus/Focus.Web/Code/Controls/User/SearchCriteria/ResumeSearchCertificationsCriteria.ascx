﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeSearchCertificationsCriteria.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchCertificationsCriteria" %>

<%@ Register src="~/Code/Controls/User/UpdateableList.ascx" tagname="UpdateableList" tagprefix="uc" %>

<asp:Panel ID="CertificationsHeaderPanel" runat="server" CssClass="singleAccordionTitle noMarginBottom">
	<asp:Image ID="CertificationsHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" alt="." Height="22" Width="22" />&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Certifications.Label", "Certifications")%></a></span>
</asp:Panel>
<asp:Panel ID="CertificationsPanel" runat="server" CssClass="accordionContent">
	<asp:UpdatePanel ID="CertificationsUpdatePanel" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<%= HtmlInFieldLabel("CertificationsAddTextBox", "CertificationsInlineLabel.Text", "type certification", 250)%>
			<asp:TextBox runat="server" ID="CertificationsAddTextBox" Width="250" AutoCompleteType="Disabled" ClientIDMode="Static" />
			<act:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="CertificationsAddTextBox" MinimumPrefixLength="2" 
																CompletionListCssClass="autocompleteCompletionList" CompletionInterval="100" 
																ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetCertifications" />
			<asp:Button ID="CertificationsAddButton" runat="server" class="button3" ClientIDMode="Static" onclick="CertificationsAddButton_Click"  CausesValidation="True" ValidationGroup="Certification" /> 
      <asp:RequiredFieldValidator ID="CertificationsAddTextBoxRequired" runat="server" ControlToValidate="CertificationsAddTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Certification" />
			<uc:UpdateableList ID="CertificationsUpdateableList" runat="server" RestrictDuplicates="True" />
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Panel>
<act:CollapsiblePanelExtender ID="CertificationsPanelExtender" runat="server" 
														TargetControlID="CertificationsPanel" 
														ExpandControlID="CertificationsHeaderPanel" 
														CollapseControlID="CertificationsHeaderPanel" 
														Collapsed="true" 
														ImageControlID="CertificationsHeaderImage" 
														CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
														ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
														SuppressPostBack="true"
														BehaviorID="CertificationsCriteriaBehavior" />