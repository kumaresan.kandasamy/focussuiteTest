﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeSearchProgramOfStudyCriteria.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchProgramOfStudyCriteria" %>

<%@ Register TagPrefix="uc" TagName="ProgramsOfStudy" Src="~/Code/Controls/User/ProgramOfStudyUpdateableList.ascx" %>

<asp:Panel ID="ProgramOfStudyHeaderPanel" runat="server" CssClass="singleAccordionTitle noMarginBottom">
	<asp:Image ID="ProgramOfStudyHeaderImage" runat="server" AlternateText="Program Of Study Header Image" />&nbsp;&nbsp;<span id="ProgramOfStudyLabel" class="collapsiblePanelHeaderLabel cpHeaderControl" clientidmode="Static"><a href="#"><%= HtmlLocalise("ProgramOfStudy.Label", "Program Of Study")%></a></span>
</asp:Panel>
<div><!-- fixes collapsible panel resizing bug in Chrome and IE10 -->
<asp:Panel ID="ProgramOfStudyPanel" runat="server" CssClass="accordionContent">
	<uc:ProgramsOfStudy ID="ProgramsOfStudy" MaxListSize="6" runat="server"/>
</asp:Panel>
</div>
<act:CollapsiblePanelExtender ID="ProgramOfStudyCollapsiblePanelExtender" runat="server" 
															TargetControlID="ProgramOfStudyPanel" 
															ExpandControlID="ProgramOfStudyHeaderPanel" 
															CollapseControlID="ProgramOfStudyHeaderPanel" 
															Collapsed="true" 
															ImageControlID="ProgramOfStudyHeaderImage" 
															CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
															ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
															SuppressPostBack="true"
															BehaviorID="ProgramOfStudyCriteriaBehavior" />