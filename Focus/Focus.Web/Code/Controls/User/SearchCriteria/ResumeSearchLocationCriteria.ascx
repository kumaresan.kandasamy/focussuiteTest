﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeSearchLocationCriteria.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchLocationCriteria" %>

<asp:Panel ID="LocationHeaderPanel" runat="server" CssClass="singleAccordionTitle noMarginBottom">
	<asp:Image ID="LocationHeaderImage" runat="server" alt="Location image" Height="22" Width="22"/>&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Location.Label", "Location")%></a></span>
</asp:Panel>
<asp:Panel ID="LocationPanel" runat="server" CssClass="accordionContent">
	<focus:LocalisedLabel runat="server" ID="LocationDistanceDropDownLabel" AssociatedControlID="LocationDistanceDropDown" LocalisationKey="LocationDistanceDropDown.Label" DefaultText="Location distance" CssClass="sr-only"/>
	<asp:DropDownList runat="server" ID="LocationDistanceDropDown" ClientIDMode="AutoID" />
  <asp:Panel runat="server" ID="LocationZipCodePanel" style="display: inline-block">
	  &nbsp;<%= HtmlLabel(LocationZIPCodeTextBox,"ZIPCode.Label", "of this ZIP code")%>&nbsp;
	  <asp:TextBox runat="server" ID="LocationZIPCodeTextBox" Text="" Width="100" ClientIDMode="AutoID" MaxLength="5" />
	  <asp:CustomValidator ID="LocationValidator" runat="server" ControlToValidate="LocationZIPCodeTextBox" ClientValidationFunction="validateLocationSearch" ValidateEmptyText="True" SetFocusOnError="True" CssClass="error"/>
  </asp:Panel>
  <br />
  <focus:LocalisedLabel runat="server" CssClass="instructionalText instructionalTextBlock" ID="LocationInstructionLabel"/>
</asp:Panel>
<act:CollapsiblePanelExtender ID="LocationPanelExtender" runat="server" 
														TargetControlID="LocationPanel" 
														ExpandControlID="LocationHeaderPanel" 
														CollapseControlID="LocationHeaderPanel" 
														Collapsed="true" 
														ImageControlID="LocationHeaderImage" 
														SuppressPostBack="true"
														BehaviorID="LocationCriteriaBehavior" />

<script type="text/javascript">

  Sys.Application.add_load(ResumeSearchLocationCriteria_PageLoad);

  function ResumeSearchLocationCriteria_PageLoad() {
    var dropdown = $("#<%=LocationDistanceDropDown.ClientID%>");

    dropdown.change(function () { ResumeSearchLocationCriteria_CheckDropdown($(this)); });
    ResumeSearchLocationCriteria_CheckDropdown(dropdown);
  }

  function ResumeSearchLocationCriteria_CheckDropdown(dropdown) {
    if (dropdown.prop("selectedIndex") == 0) {
      $("#<%=LocationZipCodePanel.ClientID%>").hide();
    } else {
      $("#<%=LocationZipCodePanel.ClientID%>").show();
    }
  }
  
function validateLocationSearch(oSrc, args){
	var isValid = true;
	
	if($("#<%= LocationDistanceDropDown.ClientID %>").val() != ""){
		isValid = (args.Value.length > 0);
	}
	
	args.IsValid = isValid;
}

</script>