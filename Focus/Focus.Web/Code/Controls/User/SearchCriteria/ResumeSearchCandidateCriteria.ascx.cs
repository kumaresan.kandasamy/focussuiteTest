﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

#endregion

namespace Focus.Web.Code.Controls.User.SearchCriteria
{
	public partial class ResumeSearchCandidateCriteria : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			ApplyBranding();
		}

		/// <summary>
		/// Binds the specified candidate id.
		/// </summary>
		/// <param name="candidateId">The candidate id.</param>
		public void Bind(long? candidateId)
		{
			if(candidateId.HasValue)
			{
				CandidatePanelExtender.Collapsed = false;
				CandidateIdTextBox.Text = candidateId.ToString();
			}
		}

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <returns></returns>
		public long? Unbind()
		{
			long candidateId;
			long.TryParse(CandidateIdTextBox.Text, out candidateId);

			return (candidateId > 0) ? candidateId : (long?)null;
		}

		/// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear()
		{
			CandidateIdTextBox.Text = "";
		}

		private void ApplyBranding()
		{
			CandidateHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			CandidatePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			CandidatePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
		}
	}
}