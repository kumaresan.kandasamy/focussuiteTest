﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeSearchSkillsCriteria.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchSkillsCriteria" %>

<%@ Register src="~/Code/Controls/User/ScrollableSkillsAccordion.ascx" tagname="SkillsAccordion" tagprefix="uc" %>

<asp:Panel ID="SkillsHeaderPanel" runat="server" CssClass="singleAccordionTitle noMarginBottom">
	<asp:Image ID="SkillsHeaderImage" runat="server" AlternateText="Skills Header Image" />&nbsp;&nbsp;<span id="SkillsLabel" clientidmode="Static"><%= HtmlLocalise("Skills.Label", "Skills")%></span>
  <%= HtmlInlineTooltipWithArrow("Skills.Tooltip", @"Use to search skills students gained through courses and extra-curricular activities.", "width:200px")%>
</asp:Panel>
<asp:Panel ID="SkillsPanel" runat="server" CssClass="accordionContent">
  <uc:SkillsAccordion runat="Server" ID="SkillsAccordion" WrapperCssClass="skillsAccordionWrapper" AccordionCssClass="skillsAccordion" SummaryCssClass="skillsAccordionSummary"/>
</asp:Panel>
<act:CollapsiblePanelExtender ID="SkillsCollapsiblePanelExtender" runat="server" 
															TargetControlID="SkillsPanel" 
															ExpandControlID="SkillsHeaderPanel" 
															CollapseControlID="SkillsHeaderPanel" 
															Collapsed="true" 
															ImageControlID="SkillsHeaderImage" 
															CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
															ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
															SuppressPostBack="true"
															BehaviorID="SkillsCriteriaBehavior" />