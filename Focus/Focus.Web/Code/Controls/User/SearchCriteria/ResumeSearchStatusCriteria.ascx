﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeSearchStatusCriteria.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchStatusCriteria" %>

<asp:Panel ID="StatusHeaderPanel" runat="server" CssClass="singleAccordionTitle noMarginBottom">
	<asp:Image ID="StatusHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" alt="." Height="22" Width="22" />&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Status.Label", "Military Status")%></a></span>
</asp:Panel>
<asp:Panel ID="StatusPanel" runat="server" CssClass="accordionContent">
	<asp:CheckBox ID="VeteranCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" />
</asp:Panel>
<act:CollapsiblePanelExtender ID="StatusPanelExtender" runat="server" 
														TargetControlID="StatusPanel" 
														ExpandControlID="StatusHeaderPanel" 
														CollapseControlID="StatusHeaderPanel" 
														Collapsed="true" 
														ImageControlID="StatusHeaderImage" 
														CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
														ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
														SuppressPostBack="true"
														BehaviorID="StatusCriteriaBehavior" />
