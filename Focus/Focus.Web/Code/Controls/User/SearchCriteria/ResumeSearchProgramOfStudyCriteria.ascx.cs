﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User.SearchCriteria
{
	public partial class ResumeSearchProgramOfStudyCriteria : UserControlBase
	{
		private List<KeyValuePair<string, string>> _programs;

		/// <summary>
		/// Gets or sets the programs.
		/// </summary>
		/// <value>The programs.</value>
		public List<KeyValuePair<string, string>> Programs
		{
			get { return ProgramsOfStudy.Items; }
			set { _programs = value; }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			ApplyBranding();
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		public void Bind()
		{
			ProgramsOfStudy.Items = _programs;

			if (_programs.IsNotNull())
				ProgramOfStudyCollapsiblePanelExtender.Collapsed = false;
		}

		/// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear()
		{
			ProgramsOfStudy.Clear();
		}

		private void ApplyBranding()
		{
			ProgramOfStudyHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ProgramOfStudyCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ProgramOfStudyCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}
	}
}