﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeSearchDrivingLicenceCriteria.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchDrivingLicenceCriteria" %>

<asp:Panel ID="DrivingLicencesHeaderPanel" runat="server" CssClass="singleAccordionTitle noMarginBottom">
	<asp:Image ID="DrivingLicencesHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" alt="." Width="22" Height="22"/>&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("DrivingLicences.Label", "Driving licenses")%></a></span>
</asp:Panel>
<asp:Panel ID="DrivingLicencesPanel" runat="server" CssClass="accordionContent">
	<focus:LocalisedLabel runat="server" ID="DrivingLicenceClassDropDownLabel" AssociatedControlID="DrivingLicenceClassDropDown" LocalisationKey="DrivingLicenceClassDropDown.Label" DefaultText="Driving licence class" CssClass="sr-only"/>
	<asp:DropDownList runat="server" ID="DrivingLicenceClassDropDown" ClientIDMode="AutoID" />
	<asp:CheckBoxList runat="server" ID="DrivingLicenceEndorsementsCheckBoxList" ClientIDMode="Static" RepeatColumns="2" role="presentation"/>
</asp:Panel>
<act:CollapsiblePanelExtender ID="DrivingLicencesPanelExtender" runat="server" 
														TargetControlID="DrivingLicencesPanel" 
														ExpandControlID="DrivingLicencesHeaderPanel" 
														CollapseControlID="DrivingLicencesHeaderPanel" 
														Collapsed="true" 
														ImageControlID="DrivingLicencesHeaderImage" 
														CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
														ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
														SuppressPostBack="true"
														BehaviorID="DrivingLicenceCriteriaBehavior" />
														
	<script type="text/javascript">

	Sys.Application.add_load(<%= ClientID %>_PageLoad);

	function <%= ClientID %>_PageLoad() {
		$("#<%=DrivingLicenceClassDropDown.ClientID%>").change(function() {
			<%= ClientID %>_ToggleEndorsements();
		});

		<%= ClientID %>_ToggleEndorsements();
	}

	function <%= ClientID %>_ToggleEndorsements() {
		var driverLicenseValue = $("#<%=DrivingLicenceClassDropDown.ClientID%>").val();
		var arrEndorsementTypes = $("#<%=DrivingLicenceEndorsementsCheckBoxList.ClientID%>").find('input');

		var licenceLicenceEndorsements = <%= LicenceEndorsementsJavascriptArray %>;

		var validEndorsements = licenceLicenceEndorsements[driverLicenseValue];

		if (!validEndorsements)
			validEndorsements = '';

		$(arrEndorsementTypes).each(function () {
			if (validEndorsements.indexOf($(this).val()) != -1) {
				$(this).prop('disabled', false).parent().removeAttr('style');
				return true;
			}
			$(this).prop('checked', false);
			$(this).prop('disabled', true).parent().css('color', 'gray');
		});
	}
	</script>