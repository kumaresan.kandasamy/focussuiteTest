﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;

using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User.SearchCriteria
{
  public partial class ResumeSearchCareerReadinessCriteria : UserControlBase
  {
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      LocaliseUI();
      ApplyBranding();
      CareerReadinessHeaderPanel.Visible = CareerReadinessPanel.Visible = App.Settings.EnableNCRCFeatureGroup;
       
    }

    /// <summary>
    /// Binds the specified NCRC level.
    /// </summary>
    /// <param name="careerReadinessLevelId">The NCRC level.</param>
    public void Bind(long? careerReadinessLevelId)
    {
      if (careerReadinessLevelId.IsNotNullOrZero())
      {
        NCRCLevelDropDown.SelectValue(careerReadinessLevelId.ToString());
        CareerReadinessPanelExtender.Collapsed = false;
      }
    }

    /// <summary>
    /// Unbinds this instance.
    /// </summary>
    /// <returns>The value of the selected level</returns>
    public long Unbind()
    {
      return NCRCLevelDropDown.SelectedValueToLong();
    }

    /// <summary>
    /// Clears this instance.
    /// </summary>
    public void Clear()
    {
      if (NCRCLevelDropDown.Items.Count > 0)
        NCRCLevelDropDown.SelectedIndex = 0;
    }

    /// <summary>
    /// Binds the controls.
    /// </summary>
    public void BindControls()
    {
      BindLevelDropDown();
    }

    /// <summary>
    /// Binds the level drop down.
    /// </summary>
    private void BindLevelDropDown()
    {
      NCRCLevelDropDown.Items.Clear();

      NCRCLevelDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.NCRCLevel), null, CodeLocalise("NCRCLevelDropDown.TopDefault", "- Select NCRC level -"));
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      CareerReadinessHeaderLabel.Text = HtmlLocalise("CareerReadiness.Header", "National Career Readiness Certificate&trade;");
    }

    /// <summary>
    /// Applies any branded styles
    /// </summary>
    private void ApplyBranding()
    {
      CareerReadinessHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      CareerReadinessPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
      CareerReadinessPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
    }
  }
}