﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common.Extensions;

#endregion

namespace Focus.Web.Code.Controls.User.SearchCriteria
{
	public partial class ResumeSearchGradePointAverageCriteria : UserControlBase
	{
		private double? _gradePointAverage;
		private bool _searchNotSpecified;

		/// <summary>
		/// Gets the minimum grade point average.
		/// </summary>
		/// <value>The minimum grade point average.</value>
		public double? MinimumGradePointAverage 
		{ 
			get { return (EducationGPAApplicantAtLeastCheckBox.Checked) ? EducationGPADropDownList.SelectedValueToNullableDouble() : null; }
			set { _gradePointAverage = value; }
		}

		/// <summary>
		/// Gets a value indicating whether [search not specified].
		/// </summary>
		/// <value><c>true</c> if [search not specified]; otherwise, <c>false</c>.</value>
		public bool SearchNotSpecified
		{
			get { return EducationGPAApplicantDidNotSpecifyCheckBox.Checked; }
			set { _searchNotSpecified = value; }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			ApplyBranding();
		}

		/// <summary>
		/// Binds the specified grade point average.
		/// </summary>
		public void Bind()
		{
			if (_gradePointAverage.HasValue)
			{
				GradePointAverageCollapsiblePanelExtender.Collapsed = false;

				EducationGPAApplicantAtLeastCheckBox.Checked = true;
				EducationGPADropDownList.SelectValue(_gradePointAverage.ToString());
				EducationGPAApplicantDidNotSpecifyCheckBox.Checked = _searchNotSpecified;
			}
			else
			{
				EducationGPAApplicantAtLeastCheckBox.Checked = false;
				EducationGPAApplicantDidNotSpecifyCheckBox.Checked = false;
			}
		}

		/// <summary>
		/// Clears the controls.
		/// </summary>
		public void Clear()
		{
			if(EducationGPADropDownList.Items.Count > 1)
				EducationGPADropDownList.SelectedIndex = 1;
			
			EducationGPAApplicantAtLeastCheckBox.Checked = false;
			EducationGPAApplicantDidNotSpecifyCheckBox.Checked = false;
		}

		/// <summary>
		/// Binds the controls.
		/// </summary>
		public void BindControls()
		{
			BindEducationGPADropDownList();
		}
		
		/// <summary>
		/// Binds the education GPA drop down list.
		/// </summary>
		private void BindEducationGPADropDownList()
		{
			EducationGPADropDownList.Items.Add("2.75");
			EducationGPADropDownList.Items.Add("3.0");
			EducationGPADropDownList.Items.Add("3.3");
			EducationGPADropDownList.Items.Add("3.5");

			EducationGPADropDownList.SelectedIndex = 1;
		}

		private void ApplyBranding()
		{
			GradePointAverageHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			GradePointAverageCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			GradePointAverageCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}
	}
}