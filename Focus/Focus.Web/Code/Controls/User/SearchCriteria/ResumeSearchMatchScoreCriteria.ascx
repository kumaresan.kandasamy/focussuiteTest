﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeSearchMatchScoreCriteria.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchMatchScoreCriteria" %>

<asp:Panel ID="SearchScoreHeaderPanel" runat="server" CssClass="singleAccordionTitle noMarginBottom">
	<asp:Image ID="SearchScoreHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" AlternateText="." Height="22" Width="22"/>&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("MatchScoreCriteria.Label", "Resume Matching")%></a></span>
</asp:Panel>
<asp:Panel ID="SearchScorePanel" runat="server" CssClass="accordionContent">
	<asp:RadioButton ID="SearchWithStarsRadio" runat="server" GroupName="MatchScoreCriteriaRadios" /><asp:Label AssociatedControlID="SearchWithStarsRadio" runat="server"><%= HtmlLocalise("SearchOnly.Text", "Search only resumes") %>
	<asp:DropDownList runat="server" ID="StarRatingDropDown" /><%= HtmlLocalise("StarsOrHigher.Text","stars or higher") %></asp:Label>
	<br/>
	<asp:RadioButton ID="SearchAllResumesRadio" runat="server" GroupName="MatchScoreCriteriaRadios" Checked="true" /><%= HtmlLabel(SearchAllResumesRadio,"SearchAll.Text", "All resumes with no minimum match rating") %>
</asp:Panel>
<act:CollapsiblePanelExtender ID="SearchScorePanelExtender" runat="server" 
														TargetControlID="SearchScorePanel" 
														ExpandControlID="SearchScoreHeaderPanel" 
														CollapseControlID="SearchScoreHeaderPanel" 
														Collapsed="true" 
														ImageControlID="SearchScoreHeaderImage" 
														CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
														ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
														SuppressPostBack="true"
														BehaviorID="MatchScoreCriteriaBehavior" />