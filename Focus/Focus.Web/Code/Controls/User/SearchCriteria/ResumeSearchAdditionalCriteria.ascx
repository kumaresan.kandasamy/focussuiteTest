﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeSearchAdditionalCriteria.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchAdditionalCriteria" %>

<%@ Register src="~/Code/Controls/User/SearchCriteria/ResumeSearchLocationCriteria.ascx" tagname="LocationCriteria" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/SearchCriteria/ResumeSearchEducationCriteria.ascx" tagname="EducationCriteria" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/SearchCriteria/ResumeSearchGradePointAverageCriteria.ascx" tagname="GradePointAverageCriteria" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/SearchCriteria/ResumeSearchProgramOfStudyCriteria.ascx" tagname="ProgramOfStudyCriteria" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/SearchCriteria/ResumeSearchLanguageCriteria.ascx" tagname="LanguageCriteria" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/SearchCriteria/ResumeSearchSkillsCriteria.ascx" tagname="SkillsCriteria" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/SearchCriteria/ResumeSearchStatusCriteria.ascx" tagname="StatusCriteria" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/SearchCriteria/ResumeSearchMatchScoreCriteria.ascx" tagname="ScoreCriteria" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/SearchCriteria/ResumeSearchDrivingLicenceCriteria.ascx" tagname="DrivingLicenceCriteria" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/SearchCriteria/ResumeSearchIndustriesCriteria.ascx" tagname="IndustriesCriteria" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/SearchCriteria/ResumeSearchLicencesCriteria.ascx" tagname="LicencesCriteria" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/SearchCriteria/ResumeSearchCertificationsCriteria.ascx" tagname="CertificationsCriteria" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/SearchCriteria/ResumeSearchAvailabilityCriteria.ascx" tagname="AvailabilityCriteria" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/SearchCriteria/ResumeSearchCandidateCriteria.ascx" tagname="CandidateCriteria" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/SearchCriteria/ResumeSearchCareerReadinessCriteria.ascx" tagname="CareerReadinessCriteria" tagprefix="uc" %>

<div style="width: 100%; display: block;">
	<div id="leftColumn" style="width: 48%; float: left;"></div>
	<div id="rightColumn" style="width: 48%; float: right;"></div>	
</div>

<div id="LocationCriteriaDiv" style="margin-bottom: 10px">
	<uc:LocationCriteria runat="server" ID="LocationCriteria" />
</div>
<div id="EducationCriteriaDiv" style="margin-bottom: 10px">
	<uc:EducationCriteria runat="server" ID="EducationCriteria" />
</div>
<div id="GradePointAverageCriteriaDiv" style="margin-bottom: 10px">
	<uc:GradePointAverageCriteria runat="server" ID="GradePointAverageCriteria" />
</div>
<div id="ProgramOfStudyCriteriaDiv" style="margin-bottom: 10px">
	<uc:ProgramOfStudyCriteria runat="server" ID="ProgramOfStudyCriteria" />
</div>
<div id="LanguageCriteriaDiv" style="margin-bottom: 10px">
	<uc:LanguageCriteria runat="server" ID="LanguageCriteria" />
</div>
<div id="SkillsCriteriaDiv" style="margin-bottom: 10px">
	<uc:SkillsCriteria runat="server" ID="SkillsCriteria" />
</div>
<div id="StatusCriteriaDiv" style="margin-bottom: 10px">
	<uc:StatusCriteria runat="server" ID="StatusCriteria" />
</div>
<div id="ScoreCriteriaDiv" style="margin-bottom: 10px">
	<uc:ScoreCriteria runat="server" ID="ScoreCriteria" />
</div>
<div id="DrivingLicenceCriteriaDiv" style="margin-bottom: 10px">
	<uc:DrivingLicenceCriteria runat="server" ID="DrivingLicenceCriteria" />
</div>
<div ID="IndustriesCriteriaDiv" style="margin-bottom: 10px" runat="server" ClientIDMode="Static">
	<uc:IndustriesCriteria runat="server" ID="IndustriesCriteria" ClientIDMode="Static" />
</div>
<div id="LicencesCriteriaDiv" style="margin-bottom: 10px">
	<uc:LicencesCriteria runat="server" ID="LicencesCriteria" />
</div>
<div id="CertificationsCriteriaDiv" style="margin-bottom: 10px">
	<uc:CertificationsCriteria runat="server" ID="CertificationsCriteria" />
</div>
<div id="CareerReadinessCriteriaDiv" style="margin-bottom: 10px">
	<uc:CareerReadinessCriteria runat="server" ID="CareerReadinessCriteria" />
</div>
<div id="AvailabilityCriteriaDiv" style="margin-bottom: 10px">
	<uc:AvailabilityCriteria runat="server" ID="AvailabilityCriteria" />
</div>
<div id="CandidateCriteriaDiv" style="margin-bottom: 10px">
	<uc:CandidateCriteria runat="server" ID="CandidateCriteria" />
</div>


<script type="text/javascript">

	Sys.Application.add_load(ResumeSearchAdditionalCriteria_PageLoad);

	function ResumeSearchAdditionalCriteria_PageLoad() {
		var theme = '<%= Theme %>' ;
		if(theme == 'Education') {
			$("#LocationCriteriaDiv").appendTo("#rightColumn");
		}
		else {
			$("#LocationCriteriaDiv").appendTo("#leftColumn");
		}
		
		$("#EducationCriteriaDiv").appendTo("#leftColumn");
		$("#GradePointAverageCriteriaDiv").appendTo("#leftColumn");
		$("#ProgramOfStudyCriteriaDiv").appendTo("#leftColumn");
		
		if(theme == 'Education') {
			$("#LanguageCriteriaDiv").appendTo("#rightColumn");
		}
		else {
			$("#LanguageCriteriaDiv").appendTo("#leftColumn");
		}
		
		$("#SkillsCriteriaDiv").appendTo("#leftColumn");
		$("#StatusCriteriaDiv").appendTo("#leftColumn");
		$("#ScoreCriteriaDiv").appendTo("#leftColumn");

		$("#IndustriesCriteriaDiv").appendTo("#rightColumn");
		$("#DrivingLicenceCriteriaDiv").appendTo("#rightColumn");
		$("#LicencesCriteriaDiv").appendTo("#rightColumn");
		$("#CertificationsCriteriaDiv").appendTo("#rightColumn");
		$("#CareerReadinessCriteriaDiv").appendTo("#rightColumn");
		$("#AvailabilityCriteriaDiv").appendTo("#rightColumn");
		$("#CandidateCriteriaDiv").appendTo("#rightColumn");
	}
	
	function HideSkillsCriteria() {
		$("#SkillsCriteriaDiv").hide();
	}
	
	function ShowSkillsCriteria() {
		$("#SkillsCriteriaDiv").show();
	}

	function HideCertificationsCriteria() {
		$("#CertificationsCriteriaDiv").hide();
	}

	function ShowCertificationsCriteria() {
		$("#CertificationsCriteriaDiv").show();
	}

	function HideScoreCriteria() {
		$("#ScoreCriteriaDiv").hide();
	}

	function ShowScoreCriteria() {
		$("#ScoreCriteriaDiv").show();
	}

	function HideAvailabilityCriteria() {
		$("#AvailabilityCriteriaDiv").hide();
	}

	function ShowAvailabilityCriteria() {
		$("#AvailabilityCriteriaDiv").show();
	}

	function HideGradePointAverageCriteria() {
		$("#GradePointAverageCriteriaDiv").hide();
	}

	function ShowGradePointAverageCriteria() {
		$("#GradePointAverageCriteriaDiv").show();
	}
	
</script>
