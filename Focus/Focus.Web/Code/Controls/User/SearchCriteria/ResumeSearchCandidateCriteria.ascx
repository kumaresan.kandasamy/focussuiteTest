﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeSearchCandidateCriteria.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchCandidateCriteria" %>

<asp:Panel ID="CandidateHeaderPanel" runat="server" CssClass="singleAccordionTitle noMarginBottom">
	<asp:Image ID="CandidateHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" alt="." Height="22" Width="22" />&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Candidate.Label", "Candidate")%></a></span>
	</asp:Panel>
<asp:Panel ID="CandidatePanel" runat="server" CssClass="accordionContent">
	<%= HtmlLocalise("CandidateID.Label", "Candidate Id")%>&nbsp;
	<asp:TextBox runat="server" ID="CandidateIdTextBox" title="Candidate Id TextBox" Width="100" ClientIDMode="Static" />
</asp:Panel>
<act:CollapsiblePanelExtender ID="CandidatePanelExtender" runat="server" 
														TargetControlID="CandidatePanel" 
														ExpandControlID="CandidateHeaderPanel" 
														CollapseControlID="CandidateHeaderPanel" 
														Collapsed="true" 
														ImageControlID="CandidateHeaderImage" 
														CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
														ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
														SuppressPostBack="true"
														BehaviorID="CandidateCriteriaBehavior" />
