﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Common.Extensions;
using Focus.Common.Localisation;
using Focus.Core;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User.SearchCriteria
{
	public partial class ResumeSearchEducationCriteria : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			LocaliseUI();
			ApplyBranding();
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		/// <param name="controlModel">The control model.</param>
		public void Bind(Model controlModel)
		{
			var theme = App.Settings.Theme;

			if ((controlModel.EducationLevel.IsNotNull() && controlModel.EducationLevel != EducationLevels.None) || controlModel.CurrentlyEnrolledUndergraduate || 
				(controlModel.MinimumUndergraduateLevel.IsNotNull() && controlModel.MinimumUndergraduateLevel != SchoolStatus.NA) || controlModel.CurrentlyEnrolledGraduate || controlModel.MonthsUntilExpectedCompletion.HasValue ||
				controlModel.IncludeAlumni)
			{
				EducationCollapsiblePanelExtender.Collapsed = false;
			}

			#region Education Level

			if (controlModel.EducationLevel.IsNotNull() && (theme == FocusThemes.Workforce))
			{
                EducationNoDiplomaCheckBox.Checked = ((controlModel.EducationLevel & EducationLevels.NoDiploma) == EducationLevels.NoDiploma) || ((controlModel.EducationLevel & EducationLevels.HighSchoolDiploma) == EducationLevels.HighSchoolDiploma);
                EducationHighSchoolDiplomaCheckBox.Checked = ((controlModel.EducationLevel & EducationLevels.HighSchoolDiplomaOrEquivalent) == EducationLevels.HighSchoolDiplomaOrEquivalent) || ((controlModel.EducationLevel & EducationLevels.HighSchoolDiploma) == EducationLevels.HighSchoolDiploma);
                EducationNoDegreeCheckBox.Checked = ((controlModel.EducationLevel & EducationLevels.SomeCollegeNoDegree) == EducationLevels.SomeCollegeNoDegree) || ((controlModel.EducationLevel & EducationLevels.HighSchoolDiploma) == EducationLevels.HighSchoolDiploma);
                EducationAssociatesDegreeCheckBox.Checked = ((controlModel.EducationLevel & EducationLevels.AssociatesDegree) == EducationLevels.AssociatesDegree);
                EducationBachelorsDegreeCheckBox.Checked = ((controlModel.EducationLevel & EducationLevels.BachelorsDegree) == EducationLevels.BachelorsDegree);
                EducationMastersDegreeCheckBox.Checked = ((controlModel.EducationLevel & EducationLevels.MastersDegree) == EducationLevels.MastersDegree) || ((controlModel.EducationLevel & EducationLevels.GraduateDegree) == EducationLevels.GraduateDegree);
                EducationDoctorateDegreeCheckBox.Checked = ((controlModel.EducationLevel & EducationLevels.DoctorateDegree) == EducationLevels.DoctorateDegree) || ((controlModel.EducationLevel & EducationLevels.GraduateDegree) == EducationLevels.GraduateDegree);
			}

			#endregion

			#region Enrollment Status

			if (theme == FocusThemes.Education)
			{
				EducationCurrentlyEnrolledUndergraduateCheckBox.Checked = controlModel.CurrentlyEnrolledUndergraduate;

				if (App.Settings.SchoolType == SchoolTypes.FourYear)
				{
					EducationCurrentlyEnrolledGraduateCheckBox.Checked = controlModel.CurrentlyEnrolledGraduate;
					if (controlModel.MinimumUndergraduateLevel.IsNotNull())
						UndergraduateLevelDropDownList.SelectValue(controlModel.MinimumUndergraduateLevel.ToString());
				}
				else
				{
					if (controlModel.MinimumUndergraduateLevel.IsNotNull())
            MinimumUndergraduateLevelCheckBox.Checked = controlModel.CurrentlyEnrolledUndergraduate;
				}

				if(controlModel.EnrollmentStatus.HasValue)
					EnrollmentStatusDropDown.SelectValue(controlModel.EnrollmentStatus.ToString());
			}

			#endregion

			#region Expected Completion
			 
			if (theme == FocusThemes.Education)
			{
				if (controlModel.MonthsUntilExpectedCompletion.HasValue)
				{
					ExpectedCompletionCheckBox.Checked = true;
					ExpectedCompletionDropDown.SelectValue(controlModel.MonthsUntilExpectedCompletion.ToString());
				}
				else
				{
					ExpectedCompletionCheckBox.Checked = false;
				}

				IncludeAlumniCheckBox.Checked = controlModel.IncludeAlumni;
			}

			#endregion

		}

		/// <summary>
		/// Unbinds the specified job type.
		/// </summary>
		/// <param name="jobType">Type of the job.</param>
		/// <returns></returns>
		public Model Unbind(JobTypes? jobType)
		{
			var theme = App.Settings.Theme;
			var module = App.Settings.Module;

			return new Model
			       	{
			       		EducationLevel = GetMinimumEducationLevel(theme),
								LocalisedEducationLevel = GetLocalisedEducationLevel(),
								CurrentlyEnrolledUndergraduate = (theme == FocusThemes.Education && module == FocusModules.Talent && jobType != JobTypes.Job) && EducationCurrentlyEnrolledUndergraduateCheckBox.Checked,
								MinimumUndergraduateLevel = (theme == FocusThemes.Education && module == FocusModules.Talent && jobType != JobTypes.Job)
																						? (App.Settings.SchoolType == SchoolTypes.FourYear) ? UndergraduateLevelDropDownList.SelectedValueToEnum<SchoolStatus>() : (MinimumUndergraduateLevelCheckBox.Checked ? SchoolStatus.Sophomore : SchoolStatus.NA)
																						: SchoolStatus.NA,
								CurrentlyEnrolledGraduate = (theme == FocusThemes.Education && module == FocusModules.Talent && jobType != JobTypes.Job) && EducationCurrentlyEnrolledGraduateCheckBox.Checked,
								MonthsUntilExpectedCompletion = (theme == FocusThemes.Education && jobType == JobTypes.Job && ExpectedCompletionCheckBox.Checked) ? ExpectedCompletionDropDown.SelectedValue.ToInt() : null,
								IncludeAlumni = (theme == FocusThemes.Education && jobType == JobTypes.Job) && IncludeAlumniCheckBox.Checked,
								EnrollmentStatus = (theme == FocusThemes.Education) ? EnrollmentStatusDropDown.SelectedValueToEnum(SchoolStatus.NA) : (SchoolStatus?)null
			       	};
		}


		/// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear()
		{
			#region Education Level

			EducationNoDiplomaCheckBox.Checked = false;
            EducationHighSchoolDiplomaCheckBox.Checked = false;
            EducationNoDegreeCheckBox.Checked = false;
            EducationAssociatesDegreeCheckBox.Checked = false;
            EducationBachelorsDegreeCheckBox.Checked = false;
            EducationMastersDegreeCheckBox.Checked = false;
            EducationDoctorateDegreeCheckBox.Checked = false;

			#endregion

			#region Enrollment Status

			EducationCurrentlyEnrolledUndergraduateCheckBox.Checked = false;
			EducationCurrentlyEnrolledGraduateCheckBox.Checked = false;
			
			if(UndergraduateLevelDropDownList.Items.Count > 0)
				UndergraduateLevelDropDownList.SelectedIndex = 0;

			if (EnrollmentStatusDropDown.Items.Count > 0)
				EnrollmentStatusDropDown.SelectedIndex = 0;
			
			#endregion

			#region Expected Completion

			ExpectedCompletionCheckBox.Checked = true;
			IncludeAlumniCheckBox.Checked = true;

			if (ExpectedCompletionDropDown.Items.Count > 1)
				ExpectedCompletionDropDown.SelectedIndex = 1;

			#endregion

		}

		/// <summary>
		/// Binds the controls.
		/// </summary>
		public void BindControls()
		{
			var theme = App.Settings.Theme;

			#region Eduaction Level

			EducationDegreeCheckBoxes.Visible = (theme == FocusThemes.Workforce && !App.Settings.ShowLocalisedEduLevels);

			if (theme == FocusThemes.Workforce && App.Settings.ShowLocalisedEduLevels)
			{
				EducationLevelSection.Visible = true;
				BindEducationLevelDropdown();
			}
			else
			{
				EducationLevelSection.Visible = false;
			}

			#endregion

			#region Enrolment Status

			if (theme == FocusThemes.Education)
			{
				if (App.Settings.SchoolType == SchoolTypes.FourYear)
				{
					BindUndergraduateLevelDropDownList();
					MinimumUndergraduateLevelRowTwoYear.Visible = false;
				}
				else
				{
					CurrentlyEnrolledGraduateRow.Visible = false;
					MinimumUndergraduateLevelRowFourYear.Visible = false;
				}

				BindEnrollmentStatuDropDown();
			}

			EducationEnrollment.Visible = (theme == FocusThemes.Education);

			#endregion

			#region Expected Completion

			if (theme == FocusThemes.Education)
			{
				BindExpectedCompletionDropDown();
			}

			ExpectedCompletion.Visible = (theme == FocusThemes.Education);

			#endregion
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
            EducationNoDiplomaCheckBox.Text = CodeLocalise("NoDiploma.Label", "No diploma");
            EducationHighSchoolDiplomaCheckBox.Text = CodeLocalise("HighSchoolDiploma.Label", "High school diploma or equivalent");
            EducationNoDegreeCheckBox.Text = CodeLocalise("NoDegree.Label", "Some college, no degree");
            EducationAssociatesDegreeCheckBox.Text = CodeLocalise("AssociatesDegree.Label", "Associate’s or vocational degree");
			EducationBachelorsDegreeCheckBox.Text = CodeLocalise("BachelorsDegree.Label", "Bachelor's degree");
            EducationMastersDegreeCheckBox.Text = CodeLocalise("MastersDegree.Label", "Master's degree");
            EducationDoctorateDegreeCheckBox.Text = CodeLocalise("DoctorateDegree.Label", "Doctorate degree");
		}

		private void ApplyBranding()
		{
			EducationHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			EducationCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			EducationCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
		}

		/// <summary>
		/// Binds the undergraduate level drop down list.
		/// </summary>
		private void BindUndergraduateLevelDropDownList()
		{
			UndergraduateLevelDropDownList.Items.AddEnum(SchoolStatus.Sophomore);
			UndergraduateLevelDropDownList.Items.AddEnum(SchoolStatus.Junior);
			UndergraduateLevelDropDownList.Items.AddEnum(SchoolStatus.Senior);
			UndergraduateLevelDropDownList.Items.AddLocalisedTopDefault("SelectLevel.Dropdown", "- select level -");
		}

		/// <summary>
		/// Binds the expected completion drop down.
		/// </summary>
		private void BindExpectedCompletionDropDown()
		{
			ExpectedCompletionDropDown.Items.Add("3");
			ExpectedCompletionDropDown.Items.Add("6");
			ExpectedCompletionDropDown.Items.Add("12");

			ExpectedCompletionDropDown.SelectedIndex = 1;
		}

		/// <summary>
		/// Binds the enrollment statu drop down.
		/// </summary>
		public void BindEnrollmentStatuDropDown()
		{
			EnrollmentStatusDropDown.Items.Clear();

			if (App.Settings.SchoolType == SchoolTypes.TwoYear)
			{
				EnrollmentStatusDropDown.Items.AddEnum(SchoolStatus.FirstYear, "First year student");
				EnrollmentStatusDropDown.Items.AddEnum(SchoolStatus.SophomoreOrAbove, "Sophomore or above");
				EnrollmentStatusDropDown.Items.AddEnum(SchoolStatus.NonCreditOther, "Non-credit/Other");
				EnrollmentStatusDropDown.Items.AddEnum(SchoolStatus.Alumni, "Alumni");
			}
			else if (App.Settings.SchoolType == SchoolTypes.FourYear)
			{
				EnrollmentStatusDropDown.Items.AddEnum(SchoolStatus.FirstYear, "First year student");
				EnrollmentStatusDropDown.Items.AddEnum(SchoolStatus.Sophomore, "Sophomore");
				EnrollmentStatusDropDown.Items.AddEnum(SchoolStatus.Junior, "Junior");
				EnrollmentStatusDropDown.Items.AddEnum(SchoolStatus.Senior, "Senior");
				EnrollmentStatusDropDown.Items.AddEnum(SchoolStatus.Graduate, "Graduate student");
				EnrollmentStatusDropDown.Items.AddEnum(SchoolStatus.NonCreditOther, "Non-credit/Other");
				EnrollmentStatusDropDown.Items.AddEnum(SchoolStatus.Alumni, "Alumni");
			}


			EnrollmentStatusDropDown.Items.AddLocalisedTopDefault("EnrollmentStatus.TopDefault", "- select enrollment status -");
		}

		/// <summary>
		/// Binds the education level dropdown.
		/// </summary>
		public void BindEducationLevelDropdown()
		{
			if (DropdownClientList.SelectableItems.IsNotNullOrEmpty())
				DropdownClientList.SelectableItems.Clear();

			DropdownClientList.DropDownListCaption = "- select -";

			DropdownClientList.SelectableItems = new List<UpdateableClientListItem>
				{
					new UpdateableClientListItem
						{
							Value = EducationLevel.High_School_Diploma_OR_Equivalent_30.ToString(),
							Label = Localiser.Instance().Localise(EducationLevel.High_School_Diploma_OR_Equivalent_30)
						},
					new UpdateableClientListItem
						{
							Value = EducationLevel.Certificate_Vocational_Program_31.ToString(),
							Label = Localiser.Instance().Localise(EducationLevel.Certificate_Vocational_Program_31)
						},
					new UpdateableClientListItem
						{
							Value = EducationLevel.College_Courses_Completed_33.ToString(),
							Label = Localiser.Instance().Localise(EducationLevel.College_Courses_Completed_33)
						},
					new UpdateableClientListItem
						{
							Value = EducationLevel.Associates_Degree_32.ToString(),
							Label = Localiser.Instance().Localise(EducationLevel.Associates_Degree_32)
						},
					new UpdateableClientListItem
						{
							Value = EducationLevel.Bachelors_OR_Equivalent_24.ToString(),
							Label = Localiser.Instance().Localise(EducationLevel.Bachelors_OR_Equivalent_24)
						},
					new UpdateableClientListItem
						{
							Value = EducationLevel.Masters_Degree_25.ToString(),
							Label = Localiser.Instance().Localise(EducationLevel.Masters_Degree_25)
						},
					new UpdateableClientListItem
						{
							Value = EducationLevel.Doctorate_Degree_26.ToString(),
							Label = Localiser.Instance().Localise(EducationLevel.Doctorate_Degree_26)
						},
					new UpdateableClientListItem
						{
							Value = EducationLevel.Professional_Doctorate_34.ToString(),
							Label = Localiser.Instance().Localise(EducationLevel.Professional_Doctorate_34)
						},
					new UpdateableClientListItem
						{
							Value = EducationLevel.College_Courses_Completed_Beyond_Highest_Degree_35.ToString(),
							Label = Localiser.Instance().Localise(EducationLevel.College_Courses_Completed_Beyond_Highest_Degree_35)
						},
					new UpdateableClientListItem
						{
							Value = EducationLevel.Professional_Development_36.ToString(),
							Label = Localiser.Instance().Localise(EducationLevel.Professional_Development_36)
						},
					new UpdateableClientListItem
						{
							Value = EducationLevel.Other_37.ToString(),
							Label = Localiser.Instance().Localise(EducationLevel.Other_37)
						}
				};
		}

		/// <summary>
		/// Gets the minimum education level.
		/// </summary>
		/// <param name="theme">The theme.</param>
		/// <returns></returns>
		private EducationLevels GetMinimumEducationLevel(FocusThemes theme)
		{
			var educationLevel = EducationLevels.None;

			if (theme == FocusThemes.Workforce)
			{
				if (EducationNoDiplomaCheckBox.Checked) educationLevel = educationLevel | EducationLevels.NoDiploma;
                if (EducationHighSchoolDiplomaCheckBox.Checked) educationLevel = educationLevel | EducationLevels.HighSchoolDiplomaOrEquivalent;
                if (EducationNoDegreeCheckBox.Checked) educationLevel = educationLevel | EducationLevels.SomeCollegeNoDegree;
				if (EducationAssociatesDegreeCheckBox.Checked) educationLevel = educationLevel | EducationLevels.AssociatesDegree;
				if (EducationBachelorsDegreeCheckBox.Checked) educationLevel = educationLevel | EducationLevels.BachelorsDegree;
                if (EducationMastersDegreeCheckBox.Checked) educationLevel = educationLevel | EducationLevels.MastersDegree;
                if (EducationDoctorateDegreeCheckBox.Checked) educationLevel = educationLevel | EducationLevels.DoctorateDegree;
			}

			return educationLevel;
		}

		/// <summary>
		/// Gets the localised education level.
		/// </summary>
		/// <returns></returns>
		private List<EducationLevel> GetLocalisedEducationLevel()
		{
			var educationLevel = new List<EducationLevel>();

			if (App.Settings.Theme == FocusThemes.Workforce && App.Settings.ShowLocalisedEduLevels && DropdownClientList.SelectedItems.IsNotNullOrEmpty() && DropdownClientList.SelectedItems.Count > 0)
			{
				educationLevel.AddRange(DropdownClientList.SelectedItems.Select(item => (EducationLevel) Enum.Parse(typeof (EducationLevel), item.Value)));
			}

			return educationLevel;
		}


		public class Model
		{
			public EducationLevels EducationLevel { get; set; }
			public List<EducationLevel> LocalisedEducationLevel { get; set; }
			public bool CurrentlyEnrolledUndergraduate { get; set; }
			public SchoolStatus MinimumUndergraduateLevel { get; set; }
			public bool CurrentlyEnrolledGraduate { get; set; }
			public int? MonthsUntilExpectedCompletion { get; set; }
			public bool IncludeAlumni { get; set; }
			public SchoolStatus? EnrollmentStatus { get; set; }
		}
	}
}