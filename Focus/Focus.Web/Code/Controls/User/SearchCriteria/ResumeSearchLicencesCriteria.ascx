﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeSearchLicencesCriteria.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchLicencesCriteria" %>

<%@ Register src="~/Code/Controls/User/UpdateableList.ascx" tagname="UpdateableList" tagprefix="uc" %>

<asp:Panel ID="LicencesHeaderPanel" runat="server" CssClass="singleAccordionTitle noMarginBottom">
	<asp:Image ID="LicencesHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" alt="." Height="22" Width="22" />&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Licences.Label", "Occupational licenses")%></a></span>
</asp:Panel>
<asp:Panel ID="LicencesPanel" runat="server" CssClass="accordionContent">
	<asp:UpdatePanel ID="LicencesUpdatePanel" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<%= HtmlInFieldLabel("LicencesAddTextBox", "LicencesInlineLabel.Text", "type license", 250)%>
			<asp:TextBox runat="server" ID="LicencesAddTextBox" Width="250" AutoCompleteType="Disabled" ClientIDMode="Static" />
			<act:AutoCompleteExtender ID="LicencesAddAutoCompleteExtender" runat="server" TargetControlID="LicencesAddTextBox" MinimumPrefixLength="2" 
																CompletionListCssClass="autocompleteCompletionList" CompletionInterval="100" 
																ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetLicences" />
			<asp:Button ID="LicencesAddButton" runat="server" class="button3" ClientIDMode="Static" onclick="LicencesAddButton_Click"  CausesValidation="True" ValidationGroup="OtherLicences" />
      <asp:RequiredFieldValidator ID="OtherLicencesRequired" runat="server" ControlToValidate="LicencesAddTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="OtherLicences" />
			<uc:UpdateableList ID="LicencesUpdateableList" runat="server" RestrictDuplicates="True" />
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Panel>
<act:CollapsiblePanelExtender ID="LicencesPanelExtender" runat="server" 
														TargetControlID="LicencesPanel" 
														ExpandControlID="LicencesHeaderPanel" 
														CollapseControlID="LicencesHeaderPanel" 
														Collapsed="true" 
														ImageControlID="LicencesHeaderImage" 
														CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
														ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
														SuppressPostBack="true"
														BehaviorID="LicencesCriteriaBehavior" />