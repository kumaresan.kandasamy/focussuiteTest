﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Common.Extensions;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User.SearchCriteria
{
	public partial class ResumeSearchLicencesCriteria : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			LocaliseUI();
			ApplyBranding();
		}

		/// <summary>
		/// Handles the Click event of the LicencesAddButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void LicencesAddButton_Click(object sender, EventArgs e)
		{
			if (LicencesAddTextBox.TextTrimmed().IsNotNullOrEmpty())
			{
				LicencesUpdateableList.AddItem(LicencesAddTextBox.TextTrimmed());
				LicencesAddTextBox.Text = "";
			}
		}

		/// <summary>
		/// Binds the specified licences.
		/// </summary>
		/// <param name="licences">The licences.</param>
		public void Bind(List<string> licences)
		{
			if (licences.IsNotNullOrEmpty())
			{
				LicencesPanelExtender.Collapsed = false;
				LicencesUpdateableList.Items = licences;
			}
		}

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <returns></returns>
		public List<string> Unbind()
		{
			return LicencesUpdateableList.Items;
		}

		/// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear()
		{
			LicencesUpdateableList.Items = null;
			LicencesAddTextBox.Text = "";
		}

		private void LocaliseUI()
		{
			OtherLicencesRequired.ErrorMessage = CodeLocalise("OtherLicencesRequired.ErrorMessage", "Please enter a license");
			LicencesAddButton.Text = CodeLocalise("Global.Add.Text.NoEdit", "Add");
		}

		private void ApplyBranding()
		{
			LicencesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			LicencesPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			LicencesPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
		}
	}
}