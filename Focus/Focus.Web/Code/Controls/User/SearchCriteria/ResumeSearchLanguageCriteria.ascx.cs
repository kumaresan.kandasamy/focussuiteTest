﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Common.Extensions;
using Focus.Core.Views;
using Framework.Core;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.Models.Career;

#endregion



namespace Focus.Web.Code.Controls.User.SearchCriteria
{
	public partial class ResumeSearchLanguageCriteria : UserControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
      if (!IsPostBack)
		  {
		    LocaliseUI();
		    ApplyBranding();
			  ApplyTheme();
		    BindLanguageProficiencyDropDown();
		  }
		}

		/// <summary>
		/// Handles the Click event of the LanguagesAddButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void LanguagesAddButton_Click(object sender, EventArgs e)
		{
      // Check for Language
      if (LanguageTextBox.TextTrimmed().IsNotNullOrEmpty())
      {
	      if (App.Settings.Theme == FocusThemes.Workforce)
	      {
					LanguageTextBoxValidator.IsValid = LanguagesUpdateableList.AddItem(new KeyValuePair<string, string[]>(
					LanguageProficiencyDropDown.SelectedValue,
						new string[2]
            {
              LanguageTextBox.TextTrimmed(),
              LanguageTextBox.TextTrimmed() + " (" + LanguageProficiencyDropDown.SelectedItem.Text + ")"
            }
						));  
	      }
	      else
	      {
					LanguageTextBoxValidator.IsValid = LanguagesUpdateableList.AddItem(new KeyValuePair<string, string[]>(
					"0",
						new string[2]
            {
              LanguageTextBox.TextTrimmed(),
              LanguageTextBox.TextTrimmed()
            }
						));  
	      }

        if (LanguageTextBoxValidator.IsValid)
        {
          LanguageTextBox.Text = "";
          LanguageProficiencyDropDown.SelectedIndex = 0;
        }
      }
		}

    /// <summary>
    /// Binds the Language Proficiency drop down.
    /// </summary>
    private void BindLanguageProficiencyDropDown()
    {
			LanguageProficiencyDropDown.Visible = (App.Settings.Theme == FocusThemes.Workforce);

      var languageProficiencies = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.LanguageProficiencies);

	    if (App.Settings.Theme == FocusThemes.Workforce)
	    {
		    var adjustedList = languageProficiencies
			    .Select(l => new LookupItemView
			    {
				    Id = l.Id,
				    Text =
					    l.DisplayOrder < languageProficiencies.Count ? CodeLocalise("OrAbove.Text", "{0} or above", l.Text) : l.Text
			    }).ToList();

		    LanguageProficiencyDropDown.BindLookup(adjustedList, null,
			    CodeLocalise("ResumeSearchLanguageProficiencies.TopDefault", "- any proficiency -"));
	    }
	    else
	    {
				var adjustedList = languageProficiencies
					.Select(l => new LookupItemView
					{
						Id = l.Id,
						Text = l.Text
					}).ToList();

				LanguageProficiencyDropDown.BindLookup(adjustedList, null,
					CodeLocalise("ResumeSearchLanguageProficiencies.TopDefault", ""));
	    }
    }

		/// <summary>
		/// Binds the specified languages.
		/// </summary>
		/// <param name="languages">The languages.</param>
		public void Bind(List<string> languages)
		{
			if (languages.IsNotNullOrEmpty())
			{
				LanguagesPanelExtender.Collapsed = false;
        
        LanguagesUpdateableList.Items =
          languages.Select(l => new KeyValuePair<string, string[]>
            (string.Empty,
              new string[2]
			        {
			          l,
			          string.Empty
			        }
            )).ToList();
			}
		}

    /// <summary>
    /// Binds the specified languages and proficiency level
    /// </summary>
    /// <param name="criteria">The CandidateSearchLanguageCriteria object containing languages and proficiency level</param>
	  public void Bind(CandidateSearchLanguageCriteria criteria)
	  {
	    if (criteria.IsNotNull())
	    {
	      LanguagesPanelExtender.Collapsed = false;

	      var languageProficiencies =
	        ServiceClientLocator.CoreClient(App)
	          .GetLookup(LookupTypes.LanguageProficiencies)
	          .ToDictionary(l => (long?) l.Id, l => l.Text);

	      var nativeProficiencyId = 
	        ServiceClientLocator.CoreClient(App)
	          .GetLookup(LookupTypes.LanguageProficiencies)
	          .Where(x => x.DisplayOrder == languageProficiencies.Count)
	          .Select(np => np.Id).FirstOrDefault();


		    if (App.Settings.Theme == FocusThemes.Workforce)
		    {
			    LanguagesUpdateableList.Items =
				    criteria.LanguageProficiencies.Select(l => new KeyValuePair<string, string[]>
					    (l.Proficiency.ToString(),
						    new string[2]
						    {
							    l.Language,
							    l.Proficiency == 0
								    ? string.Format("{0} ({1})", l.Language,
									    CodeLocalise("ResumeSearchLanguageProficiencies.TopDefault", "- any proficiency -"))
								    : string.Format("{0} ({1})", l.Language,
									    l.Proficiency != nativeProficiencyId
										    ? languageProficiencies[l.Proficiency] + CodeLocalise("OrAbove.Text", " or above")
										    : languageProficiencies[l.Proficiency])
						    }
					    )).ToList();
		    }
		    else
		    {
					LanguagesUpdateableList.Items =
						criteria.LanguageProficiencies.Select(l => new KeyValuePair<string, string[]>
							(l.Proficiency.ToString(),
								new string[2]
						    {
							    l.Language,
							    l.Proficiency == 0
								    ? string.Format("{0}", l.Language)
								    : string.Format("{0} ({1})", l.Language,
									    l.Proficiency != nativeProficiencyId
										    ? languageProficiencies[l.Proficiency] + CodeLocalise("OrAbove.Text", " or above")
										    : languageProficiencies[l.Proficiency])
						    }
							)).ToList();
		    }

		    SearchLanguagesAnd.Checked = criteria.LanguageSearchType; 
        SearchLanguagesOr.Checked = !criteria.LanguageSearchType;

	    }
	  }

	  /// <summary>
	  /// Unbinds this instance.
	  /// </summary>
	  /// <returns></returns>
	  public CandidateSearchLanguageCriteria Unbind()
	  {
	    if (LanguagesUpdateableList.Items.IsNotNullOrEmpty())
	    {
	      var languages = LanguagesUpdateableList.Items.Select(l => new LanguageProficiency()
	      {
	        Language = l.Value[0],
	        Proficiency = l.Key != string.Empty ? Convert.ToInt64(l.Key) : 0
	      }).ToList();

	      return new CandidateSearchLanguageCriteria
	      {
	        LanguageProficiencies = languages,
	        LanguageSearchType = SearchLanguagesAnd.Checked
	      };
	    }

	    return null;

	  }

	  /// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear()
		{
			LanguagesUpdateableList.Items = null;
			LanguageTextBox.Text = "";
		  SearchLanguagesAnd.Checked = true;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			LanguagesAddButton.Text = CodeLocalise("Global.Add.Text.NoEdit", "Add");
			LanguageTextBoxRequired.ErrorMessage = CodeLocalise("LanguageTextBoxRequired.ErrorMessage", "Please enter a language");
            LanguageTextBoxValidator.ErrorMessage = CodeLocalise("LanguageTextBoxValidator.ErrorMessage", "Duplicate languages types not allowed");
            
            //Added for Accessibility 508 compliance
            SearchLanguagesAnd.InputAttributes.Add("Title", "Search Languages And");
            SearchLanguagesOr.InputAttributes.Add("Title", "Search Languages or");
		}


		private void ApplyBranding()
		{
			LanguagesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			LanguagesPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			LanguagesPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}

		/// <summary>
		/// Applies changes based on the them
		/// </summary>
		private void ApplyTheme()
		{
			LanguagesUpdateableList.Width = App.Settings.Theme == FocusThemes.Workforce ? "450" : "256";
		}

    /// <summary>
    /// Sets the Search Type Radio buttons.
    /// </summary>
	  protected void LanguagesUpdatePanel_OnPreRender(object sender, EventArgs e)
	  {
      // Set Search Type Radio buttons
      if (LanguagesUpdateableList.Items.IsNotNull() && LanguagesUpdateableList.Items.Count > 0)
      {
        SearchLanguagesAnd.Enabled = true;
        SearchLanguagesOr.Enabled = true;
      }
      else
      {
        SearchLanguagesAnd.Enabled = false;
        SearchLanguagesOr.Enabled = false;
      }
	  }
	}
}