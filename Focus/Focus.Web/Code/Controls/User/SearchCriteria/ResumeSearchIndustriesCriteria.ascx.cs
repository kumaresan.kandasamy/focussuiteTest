﻿#region Copyright © 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core.Criteria.CandidateSearch;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User.SearchCriteria
{
	public partial class ResumeSearchIndustriesCriteria : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			LocaliseUI();
			ApplyBranding();
		}

		/// <summary>
		/// Handles the Click event of the IndustriesAddButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void IndustriesAddButton_Click(object sender, EventArgs e)
		{
			if (IndustriesAddTextBox.TextTrimmed().IsNotNullOrEmpty())
			{
				var inductryCode = String.Concat(IndustriesAddTextBox.TextTrimmed()
                                                             .TakeWhile(IsNumberOrHyphen)
                                                             .ToList());
				var naic = ServiceClientLocator.CoreClient(App).GetIndustryClassification(inductryCode);
				IndustriesUpdateableList.AddItem(String.Concat(naic.Code, " - ", naic.Name));
				IndustriesAddTextBox.Text = "";
			}
		}

    /// <summary>
    /// Checks if a character is a number or hyphen
    /// </summary>
    /// <param name="character">The character to check</param>
    /// <returns>True if the character is a number or a hyphen, false otherwise</returns>
    private static bool IsNumberOrHyphen(char character)
    {
      return character.Equals('-') || Char.IsNumber(character);
    }

		/// <summary>
		/// Binds the specified licences.
		/// </summary>
		/// <param name="industries">The licences.</param>
		public void Bind(CandidateSearchIndustriesCriteria industries)
		{
			if (industries == null || !industries.IndustryCodes.IsNotNullOrEmpty()) return;
			var items = new List<string>();
			var coreClient = ServiceClientLocator.CoreClient(App);
			IndustriesPanelExtender.Collapsed = false;
			industries.IndustryCodes.ForEach(i =>
			{
				var naic = coreClient.GetIndustryClassification(i.ToString(CultureInfo.InvariantCulture));
				items.Add(string.Concat(naic.Code, " - ", naic.Name));
			});
			IndustriesUpdateableList.Items = items;
		}

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <returns></returns>
		public CandidateSearchIndustriesCriteria Unbind()
		{
			if (IndustriesUpdateableList.Items.IsNullOrEmpty()) return null;
			return new CandidateSearchIndustriesCriteria
			{
				IndustryCodes = IndustriesUpdateableList.Items.Select(i => String.Concat(i.TakeWhile(IsNumberOrHyphen).ToList())).ToList()
			};
		}

		/// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear()
		{
			IndustriesUpdateableList.Items = null;
			IndustriesAddTextBox.Text = "";
		}

		private void LocaliseUI()
		{
			OtherIndustriesRequired.ErrorMessage = CodeLocalise("OtherIndustriesRequired.ErrorMessage", "Please enter an industry");
			IndustriesAddButton.Text = CodeLocalise("Global.Add.Text.NoEdit", "Add");
		}

		private void ApplyBranding()
		{
			IndustriesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			IndustriesPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			IndustriesPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
		}
	}
}