﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeSearchLanguageCriteria.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchLanguageCriteria" %>

<%@ Register src="~/Code/Controls/User/UpdateableLanguagesList.ascx" tagname="UpdateableLanguagesList" tagprefix="uc" %>

<asp:Panel ID="LanguagesHeaderPanel" runat="server" CssClass="singleAccordionTitle noMarginBottom">
	<asp:Image ID="LanguagesHeaderImage" runat="server" alt="." Height="22" Width="22"/>&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Languages.Label", "Languages")%></a></span></asp:Panel>

<asp:Panel ID="LanguagesPanel" runat="server" CssClass="accordionContent">
	<asp:UpdatePanel ID="LanguagesUpdatePanel" runat="server" UpdateMode="Conditional" OnPreRender="LanguagesUpdatePanel_OnPreRender" >
		<ContentTemplate>
      <p>
			  <%= HtmlInFieldLabel("LanguageTextBox", "Language.InlineLabel", "type language", 250)%>
			  <asp:TextBox ID="LanguageTextBox" runat="server" Width="250" ClientIDMode="Static" AutoCompleteType="Disabled" />
			  <act:AutoCompleteExtender ID="LanguageAutoCompleteExtender" runat="server" TargetControlID="LanguageTextBox" MinimumPrefixLength="2" 
																CompletionListCssClass="autocompleteCompletionList" CompletionInterval="100" 
																ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetLanguages" />
        <asp:RequiredFieldValidator ID="LanguageTextBoxRequired" runat="server" ControlToValidate="LanguageTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Languages" />
        <asp:CustomValidator ID="LanguageTextBoxValidator" runat="server" CssClass="error" ControlToValidate="LanguageTextBox" ValidateEmptyText="true" SetFocusOnError="True" ValidationGroup="Languages" />
      </p>
      <p>
        <asp:DropDownList runat="server" ID="LanguageProficiencyDropDown" Width="308px" Title="Language proficiency"/>
			  <asp:Button ID="LanguagesAddButton" runat="server" class="button3" ClientIDMode="Static" onclick="LanguagesAddButton_Click" CausesValidation="True" ValidationGroup="Languages" /> 
      </p>
      <div>
        <uc:UpdateableLanguagesList ID="LanguagesUpdateableList" runat="server" RestrictDuplicates="True" Width="450" />
        <asp:RadioButton ID="SearchLanguagesAnd" runat="server" Checked="true" Enabled="false" ClientIDMode="Static"
          GroupName="LanguageSearchTypeRadios" />
          <label for="SearchLanguagesAnd">
        <%= HtmlLocalise("SearchLanguagesAnd.Text", "#CANDIDATETYPES# with ALL selected languages")%></label>
        <br/>
        <asp:RadioButton ID="SearchLanguagesOr" runat="server" Enabled="false" ClientIDMode="Static"
          GroupName="LanguageSearchTypeRadios" />
          <label for="SearchLanguagesOr">
        <%= HtmlLabel("SearchLanguagesOr.Text", "#CANDIDATETYPES# with ANY selected language")%></label>
      </div>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Panel>

<act:CollapsiblePanelExtender ID="LanguagesPanelExtender" runat="server" 
														TargetControlID="LanguagesPanel" 
														ExpandControlID="LanguagesHeaderPanel" 
														CollapseControlID="LanguagesHeaderPanel" 
														Collapsed="true" 
														ImageControlID="LanguagesHeaderImage" 
														SuppressPostBack="true"
														BehaviorID="LanguageCriteriaBehavior" />
                            
