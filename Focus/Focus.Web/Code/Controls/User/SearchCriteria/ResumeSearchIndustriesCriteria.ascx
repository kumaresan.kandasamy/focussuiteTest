﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeSearchIndustriesCriteria.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchIndustriesCriteria" %>

<%@ Register src="~/Code/Controls/User/UpdateableList.ascx" tagname="UpdateableList" tagprefix="uc" %>

<asp:Panel ID="IndustriesHeaderPanel" runat="server" CssClass="singleAccordionTitle noMarginBottom">
	<asp:Image ID="IndustriesHeaderImage" runat="server" alt="."  ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" />&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Industries.Label", "Industries")%></a></span>
</asp:Panel>
<asp:Panel ID="IndustriesPanel" runat="server" CssClass="accordionContent">
	<asp:UpdatePanel ID="IndustriesUpdatePanel" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<%= HtmlInFieldLabel("IndustriesAddTextBox", "IndustriesInlineLabel.Text", "type industry", 250)%>
			<asp:TextBox runat="server" ID="IndustriesAddTextBox" Width="250" AutoCompleteType="Disabled" ClientIDMode="Static" />
			<act:AutoCompleteExtender ID="IndustriesAddAutoCompleteExtender" runat="server" TargetControlID="IndustriesAddTextBox" MinimumPrefixLength="2" 
																CompletionListCssClass="autocompleteCompletionList" CompletionInterval="100" 
																ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetNaics" />
			<asp:Button ID="IndustriesAddButton" runat="server" class="button3" ClientIDMode="Static" onclick="IndustriesAddButton_Click"  CausesValidation="True" ValidationGroup="OtherIndustries" />
      <asp:RequiredFieldValidator ID="OtherIndustriesRequired" runat="server" ControlToValidate="IndustriesAddTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="OtherIndustries" />
			<asp:CustomValidator runat="server" ID="IndustrialClassificationValidator" ControlToValidate="IndustriesAddTextBox" ClientValidationFunction="doesNAICSExist" ErrorMessage="Industry classification (NAICS) not found"  SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="OtherIndustries"/>
			<uc:UpdateableList ID="IndustriesUpdateableList" runat="server" RestrictDuplicates="True" />
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Panel>
<act:CollapsiblePanelExtender ID="IndustriesPanelExtender" runat="server" 
														TargetControlID="IndustriesPanel" 
														ExpandControlID="IndustriesHeaderPanel" 
														CollapseControlID="IndustriesHeaderPanel" 
														Collapsed="true" 
														ImageControlID="IndustriesHeaderImage" 
														CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
														ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
														SuppressPostBack="true"
														BehaviorID="IndustriesCriteriaBehavior" />
														
<script type="text/javascript">
	function doesNAICSExist(source, arguments)
    {
      var exists;

      $.ajax({
          type: "POST",
          contentType: "application/json; charset=utf-8",
          url: "<%= UrlBuilder.AjaxService() %>/CheckNaicsExists", //+ arguments.Value,
          dataType: "json",
          data: '{"naicsText": "' + arguments.Value + '"}',
          async: false,
          success: function (result) {
            exists = (result.d);
          }
      });

      arguments.IsValid = exists;
    }
</script>