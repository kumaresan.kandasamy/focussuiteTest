﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Framework.Core;

#endregion


namespace Focus.Web.Code.Controls.User.SearchCriteria
{
	public partial class ResumeSearchLocationCriteria : UserControlBase
	{
		/// <summary>
		/// Gets or sets the validation group.
		/// </summary>
		/// <value>The validation group.</value>
		public string ValidationGroup { get; set; }

		/// <summary>
		/// Gets or sets the _distance units.
		/// </summary>
		/// <value>The _distance units.</value>
		private DistanceUnits DistanceUnits
		{
			get { return GetViewStateValue<DistanceUnits>("ResumeSearchLocationCriteria:DistanceUnits"); }
			set { SetViewStateValue("ResumeSearchLocationCriteria:DistanceUnits", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if(ValidationGroup.IsNotNullOrEmpty())
			{
				LocationValidator.ValidationGroup = ValidationGroup;
			}

			LocaliseUI();
			ApplyBranding();
		}

		/// <summary>
		/// Binds the specified criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		public void Bind(CandidateSearchLocationCriteria criteria)
		{
			if (criteria.IsNotNull())
			{
				LocationPanelExtender.Collapsed = false;
				
				if (criteria.DistanceUnits.IsNotNull())
				{
					DistanceUnits = criteria.DistanceUnits;
					BindLocationDistanceDropDown();
				}

				if (criteria.Distance.IsNotNull())
					LocationDistanceDropDown.SelectValue(criteria.Distance.ToString());

				if (criteria.PostalCode.IsNotNullOrEmpty())
					LocationZIPCodeTextBox.Text = criteria.PostalCode;
			}
		}

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <returns></returns>
		public CandidateSearchLocationCriteria Unbind()
		{
			var locationDistance = LocationDistanceDropDown.SelectedValueToLong();

			return (locationDistance != 0)
			       	? new CandidateSearchLocationCriteria
			       	  	{
			       	  		Distance = locationDistance,
			       	  		DistanceUnits = DistanceUnits,
			       	  		PostalCode = LocationZIPCodeTextBox.TextTrimmed()
			       	  	}
			       	: null;
		}

		/// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear()
		{
			if(LocationDistanceDropDown.Items.Count > 0)
				LocationDistanceDropDown.SelectedIndex = 0;
			
			LocationZIPCodeTextBox.Text = "";
		}

		/// <summary>
		/// Binds the controls.
		/// </summary>
		public void BindControls()
		{
			BindLocationDistanceDropDown();
		}

		/// <summary>
		/// Binds the location distance drop down.
		/// </summary>
		private void BindLocationDistanceDropDown()
		{
			LocationDistanceDropDown.Items.Clear();

			var distanceUnit = (DistanceUnits.IsNotNull()) ? DistanceUnits : GetDistanceUnits();
			var distanceUnits = CodeLocalise(distanceUnit, "Undefined");
			LocationDistanceDropDown.Items.Add(new ListItem(CodeLocalise("Global.AnyLocation.Text.NoEdit", "Any location"), string.Empty));
			LocationDistanceDropDown.Items.Add(new ListItem(string.Format("5 {0}", distanceUnits), "5"));
			LocationDistanceDropDown.Items.Add(new ListItem(string.Format("10 {0}", distanceUnits), "10"));
			LocationDistanceDropDown.Items.Add(new ListItem(string.Format("25 {0}", distanceUnits), "25"));
			LocationDistanceDropDown.Items.Add(new ListItem(string.Format("50 {0}", distanceUnits), "50"));
			LocationDistanceDropDown.Items.Add(new ListItem(string.Format("100 {0}", distanceUnits), "100"));
		}

		/// <summary>
		/// Gets the distance units.
		/// </summary>
		private DistanceUnits GetDistanceUnits()
		{
			var distanceUnits = App.Settings.DistanceUnits;
			DistanceUnits = distanceUnits;
			return distanceUnits;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			LocationValidator.ErrorMessage = CodeLocalise("LocationSearch.CustomError", "ZIP code required");
            LocationInstructionLabel.DefaultText = CodeLocalise("LocationInstruction.Label", "Job Seekers who are willing to relocate may appear in search results regardless of the search radius you select.");
		}

		private void ApplyBranding()
		{
			LocationHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			LocationPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			LocationPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
		}
	}
}