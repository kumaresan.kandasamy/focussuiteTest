﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeSearchGradePointAverageCriteria.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchGradePointAverageCriteria" %>

<asp:Panel ID="GradePointAverageHeaderPanel" runat="server" CssClass="singleAccordionTitle noMarginBottom">
	<asp:Image ID="GradePointAverageHeaderImage" runat="server" AlternateText="Grade Point Average Header Image" />&nbsp;&nbsp;<span id="GradePointAverageLabel" class="collapsiblePanelHeaderLabel cpHeaderControl" clientidmode="Static"><a href="#"><%= HtmlLocalise("GPA.Label", "GPA") %></a></span>
</asp:Panel>
<asp:Panel ID="GradePointAveragePanel" runat="server" CssClass="accordionContent">
	<table>
		<tr>
			<td width="8px">
				<label for="EducationGPAApplicantAtLeastCheckBox" style="display:none"><%=HtmlLocalise("ApplicantShouldHaveAtLeastA.Label", "Applicant should have at least a") %></label>
				<asp:CheckBox runat="server" ID="EducationGPAApplicantAtLeastCheckBox" ClientIDMode="Static"/>
			</td>
			<td>
				<label><%=HtmlLocalise("ApplicantShouldHaveAtLeastA.Label", "Applicant should have at least a") %>&nbsp;<asp:DropDownList runat="server" ID="EducationGPADropDownList"/>&nbsp;<%=HtmlLocalise("GPA.Label", "GPA") %></label>
			</td>
		</tr>
		<tr>
			<td>
					<asp:CheckBox runat="server" ID="EducationGPAApplicantDidNotSpecifyCheckBox" ClientIDMode="Static"/>
			</td>
			<td>
			<label for="EducationGPAApplicantDidNotSpecifyCheckBox"><%=HtmlLocalise("AlsoSearchApplicantsWhoDidNotSpecifyGPA.Label", "Also search applicants who did not specify GPA") %></label>
			</td>
		</tr>
	</table>
</asp:Panel>
<act:CollapsiblePanelExtender ID="GradePointAverageCollapsiblePanelExtender" runat="server" 
															TargetControlID="GradePointAveragePanel" 
															ExpandControlID="GradePointAverageHeaderPanel" 
															CollapseControlID="GradePointAverageHeaderPanel" 
															Collapsed="true" 
															ImageControlID="GradePointAverageHeaderImage" 
															CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
															ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
															SuppressPostBack="true"
															BehaviorID="GradePointAverageCriteriaBehavior" />