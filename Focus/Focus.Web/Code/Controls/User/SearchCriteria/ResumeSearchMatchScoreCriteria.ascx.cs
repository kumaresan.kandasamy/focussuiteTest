﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common.Extensions;

#endregion


namespace Focus.Web.Code.Controls.User.SearchCriteria
{
	public partial class ResumeSearchMatchScoreCriteria : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			ApplyBranding();
		}

		/// <summary>
		/// Binds the specified minimum star rating.
		/// </summary>
		/// <param name="minimumStarRating">The minimum star rating.</param>
		public void Bind(int minimumStarRating)
		{
			if (minimumStarRating > 0)
			{
				SearchScorePanelExtender.Collapsed = false;
				SearchWithStarsRadio.Checked = true;
				SearchAllResumesRadio.Checked = false;
				StarRatingDropDown.SelectValue(minimumStarRating.ToString());
			}
			else
			{
				SearchAllResumesRadio.Checked = true;
				SearchWithStarsRadio.Checked = false;
			}
		}

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <returns></returns>
		public int Unbind()
		{
			var minimumStarRating = 0;

			if (SearchWithStarsRadio.Checked)
			{
				minimumStarRating = StarRatingDropDown.SelectedIndex + 1;
			}
			
			return minimumStarRating;
		}

		/// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear()
		{
			SearchAllResumesRadio.Checked = true;
			
			if (StarRatingDropDown.Items.Count > 2)
				StarRatingDropDown.SelectedIndex = 2;
		}

		public void BindControls()
		{
			BindStarRatingDropDown();
		}

		/// <summary>
		/// Binds the star rating drop down.
		/// </summary>
		private void BindStarRatingDropDown()
		{
			StarRatingDropDown.Items.Clear();

			StarRatingDropDown.Items.Add("1");
			StarRatingDropDown.Items.Add("2");
			StarRatingDropDown.Items.Add("3"); 
			StarRatingDropDown.Items.Add("4");
			StarRatingDropDown.Items.Add("5");

			StarRatingDropDown.SelectedIndex = 2;
		}

		private void ApplyBranding()
		{
			SearchScoreHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			SearchScorePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			SearchScorePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}
	}
}