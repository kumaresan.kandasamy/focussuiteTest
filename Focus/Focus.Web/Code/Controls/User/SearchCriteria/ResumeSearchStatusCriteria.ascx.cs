﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core.Criteria.CandidateSearch;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User.SearchCriteria
{
	public partial class ResumeSearchStatusCriteria : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			LocaliseUI();
			ApplyBranding();
		}

		/// <summary>
		/// Binds the specified criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		public void Bind(CandidateSearchStatusCriteria criteria)
		{
			if(criteria.IsNotNull())
			{
				StatusPanelExtender.Collapsed = false;
				VeteranCheckBox.Checked = criteria.Veteran;
			}
		}

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <returns></returns>
		public CandidateSearchStatusCriteria Unbind()
		{
			return new CandidateSearchStatusCriteria {Veteran = VeteranCheckBox.Checked};
		}

		/// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear()
		{
			VeteranCheckBox.Checked = false;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			VeteranCheckBox.Text = CodeLocalise("Veteran.Label", "Veterans or Military Service");
		}

		private void ApplyBranding()
		{
			StatusHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			StatusPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			StatusPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}
	}
}