﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeSearchEducationCriteria.ascx.cs"
    Inherits="Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchEducationCriteria" %>
<%@ Register TagName="UpdateableClientList" TagPrefix="focus" Src="~/Code/Controls/User/UpdateableClientList.ascx" %>
<asp:Panel ID="EducationHeaderPanel" runat="server" CssClass="singleAccordionTitle noMarginBottom">
    <asp:Image ID="EducationHeaderImage" runat="server" alt="." Height="22" Width="22" />
    &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a id="EducationCriteriaLabel"
        href="#"><%= HtmlLocalise("Education.Label", "Education")%></a></span>
</asp:Panel>
<asp:Panel ID="EducationEducationPanel" runat="server" CssClass="accordionContent">
    <div id="EducationDegreeCheckBoxes" runat="server" style="margin-bottom: 5px;">
        <asp:CheckBox ID="EducationNoDiplomaCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" /><br />
        <asp:CheckBox ID="EducationHighSchoolDiplomaCheckBox" TextAlign="Right" runat="server"
            ClientIDMode="Static" /><br />
        <asp:CheckBox ID="EducationNoDegreeCheckBox" TextAlign="Right" runat="server"
            ClientIDMode="Static" /><br />
        <asp:CheckBox ID="EducationAssociatesDegreeCheckBox" TextAlign="Right" runat="server"
            ClientIDMode="Static" /><br />
        <asp:CheckBox ID="EducationBachelorsDegreeCheckBox" TextAlign="Right" runat="server"
            ClientIDMode="Static" /><br />
        <asp:CheckBox ID="EducationMastersDegreeCheckBox" TextAlign="Right" runat="server"
            ClientIDMode="Static" /><br />
        <asp:CheckBox ID="EducationDoctorateDegreeCheckBox" TextAlign="Right" runat="server"
            ClientIDMode="Static" /><br />
        <br />
    </div>
    <div id="EducationLevelSection" runat="server" style="margin-bottom: 5px;">
        <focus:LocalisedLabel runat="server" DefaultText="Education level" RenderOuterSpan="True"
            ID="EducationLevelLocalisedLabel" />
        <focus:UpdateableClientList ID="DropdownClientList" runat="server" InputControlType="DropDownList"
            MaxSize="12" />
    </div>
    <div runat="server" id="ExpectedCompletion" style="margin-bottom: 15px;">
        <table role="presentation">
            <tr>
                <td style="width: 8px">
                    <asp:CheckBox runat="server" ID="ExpectedCompletionCheckBox" ClientIDMode="Static"
                        Checked="true" />
                </td>
                <td>
                    <label>
                        <%= HtmlLocalise("StudentsDueToGraduateWithin.Text", "Students due to graduate within")%>&nbsp;<asp:DropDownList
                            runat="server" ID="ExpectedCompletionDropDown" ClientIDMode="Static" />
                        &nbsp;<%= HtmlLocalise("Months.Text", "months of posting date") %></label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox runat="server" ID="IncludeAlumniCheckBox" ClientIDMode="Static" Checked="true" />
                </td>
                <td>
                    <%= HtmlLocalise("IncludeAlumni.Text", "Include alumni")%>
                </td>
            </tr>
        </table>
    </div>
    <div runat="server" id="EducationEnrollment">
        <table runat="server" id="EducationEnrollmentTable">
            <tr>
                <td style="width: 8px">
                    <asp:CheckBox runat="server" ID="EducationCurrentlyEnrolledUndergraduateCheckBox"
                        ClientIDMode="Static" />
                </td>
                <td>
                    <%= HtmlLocalise("CurrentlyEnrolledUndergraduate.Label", "Currently enrolled - undergraduate student") %>
                </td>
            </tr>
            <tr runat="server" id="MinimumUndergraduateLevelRowFourYear">
                <td>
                </td>
                <td>
                    <asp:Label runat="server" ID="ApplicantMustBeAtLeastLabel" ForeColor="Gray" ClientIDMode="Static"><%= HtmlLocalise("ApplicantMustBeAtLeast.Label", "Applicant must at least be") %></asp:Label>&nbsp;<asp:DropDownList
                        runat="server" ID="UndergraduateLevelDropDownList" Enabled="False" ClientIDMode="Static" />
                    &nbsp;<asp:Label runat="server" ID="EducationLevelLabel" ForeColor="Gray" ClientIDMode="Static"><%= HtmlLocalise("Level.Label", "level") %></asp:Label>
                </td>
            </tr>
            <tr runat="server" id="MinimumUndergraduateLevelRowTwoYear">
                <td>
                    <asp:CheckBox runat="server" ID="MinimumUndergraduateLevelCheckBox" ClientIDMode="Static" />
                </td>
                <td>
                    <asp:Label runat="server" ID="ApplicantMustBeAtLeastSophomoreLabel" ForeColor="Gray"
                        ClientIDMode="Static"><%= HtmlLocalise("ApplicantMustBeAtLeastSophomore.Label", "Applicant must be at least Sophomore level") %></asp:Label>
                </td>
            </tr>
            <tr runat="server" id="CurrentlyEnrolledGraduateRow">
                <td>
                    <asp:CheckBox runat="server" ID="EducationCurrentlyEnrolledGraduateCheckBox" ClientIDMode="Static" />
                </td>
                <td>
                    <%= HtmlLocalise("CurrentlyEnrolledGraduate.Label", "Currently enrolled - graduate student") %>
                </td>
            </tr>
        </table>
        <div runat="server" id="EnrollmentStatusDropDownDiv">
            <%= HtmlLocalise("EnrollmentStatus.Label", "Enrollment status") %><br />
            <asp:DropDownList runat="server" ID="EnrollmentStatusDropDown" Enabled="True" ClientIDMode="Static" />
        </div>
    </div>
</asp:Panel>
<act:CollapsiblePanelExtender ID="EducationCollapsiblePanelExtender" runat="server"
    TargetControlID="EducationEducationPanel" ExpandControlID="EducationHeaderPanel"
    CollapseControlID="EducationHeaderPanel" Collapsed="true" ImageControlID="EducationHeaderImage"
    CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
    SuppressPostBack="true" BehaviorID="EducationCriteriaBehavior" />
<script type="text/javascript">

    Sys.Application.add_load(ResumeSearchEducationCriteria_PageLoad);

    function ResumeSearchEducationCriteria_PageLoad() {
        EnableDisableUndergraduateLevel();

        $('#EducationCurrentlyEnrolledUndergraduateCheckBox').change(function () {
            EnableDisableUndergraduateLevel();
        });
    }

    function EnableDisableUndergraduateLevel() {
        if ($('#EducationCurrentlyEnrolledUndergraduateCheckBox').is(':checked')) {
            $('#UndergraduateLevelDropDownList').attr('disabled', false);
            $('#ApplicantMustBeAtLeastLabel').css('color', 'Black');
            $('#ApplicantMustBeAtLeastSophomoreLabel').css('color', 'Black');
            $('#EducationLevelLabel').css('color', 'Black');
            $('#MinimumUndergraduateLevelCheckBox').attr('disabled', false);
        }
        else {
            $('#UndergraduateLevelDropDownList').val("");
            $('#UndergraduateLevelDropDownList').attr('disabled', true);
            $('#ApplicantMustBeAtLeastLabel').css('color', 'Gray');
            $('#ApplicantMustBeAtLeastSophomoreLabel').css('color', 'Gray');
            $('#EducationLevelLabel').css('color', 'Gray');
            $('#MinimumUndergraduateLevelCheckBox').attr('disabled', true);
            $('#MinimumUndergraduateLevelCheckBox').attr('checked', false);
        };
    }

    function ShowExpectedCompletionSection() {
        $('#<%= ExpectedCompletion.ClientID %>').show();
    }

    function HideExpectedCompletionSection() {
        $('#<%= ExpectedCompletion.ClientID %>').hide();
    }

    function ShowEnrollmentStatusDropDown() {
        $('#<%= EducationEnrollmentTable.ClientID %>').hide();
        $('#<%= EnrollmentStatusDropDownDiv.ClientID %>').show();
    }

    function HideEnrollmentStatusDropDown() {
        $('#<%= EducationEnrollmentTable.ClientID %>').show();
        $('#<%= EnrollmentStatusDropDownDiv.ClientID %>').hide();
    }

    function SetEducationCriteriaTitle(title) {
        $('#EducationCriteriaLabel').text(title);
    }

</script>
