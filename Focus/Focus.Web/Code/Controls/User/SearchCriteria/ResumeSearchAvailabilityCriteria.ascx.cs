﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User.SearchCriteria
{
	public partial class ResumeSearchAvailabilityCriteria : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			ApplyBranding();

            //Accessibility 508 compliance
            WorkOvertimeCheckBox.InputAttributes.Add("Title", "Willing to work overtime");
            RelocateCheckBox.InputAttributes.Add("Title", "Willing to relocate");

		}

		/// <summary>
		/// Binds the specified criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		public void Bind(CandidateSearchAvailabilityCriteria criteria)
		{
			var theme = App.Settings.Theme;

			var displayWorkOvertimeSection = (theme == FocusThemes.Workforce);
		  var displayRelocateSection = (theme == FocusThemes.Workforce);
			var displayWorkShiftsSection = (theme == FocusThemes.Workforce);
			var displayWorkTypeSection = (theme == FocusThemes.Workforce);

			if(criteria.IsNotNull())
			{
				AvailabilityPanelExtender.Collapsed = false;

				if (criteria.WorkOvertime.HasValue && displayWorkOvertimeSection)
					WorkOvertimeCheckBox.Checked = criteria.WorkOvertime.Value;

        if (criteria.WillingToRelocate.HasValue && displayRelocateSection)
			    RelocateCheckBox.Checked = criteria.WillingToRelocate.Value;

				if (criteria.NormalWorkShiftId.HasValue && displayWorkShiftsSection)
					NormalWorkShiftsDropDown.SelectValue(criteria.NormalWorkShiftId.Value.ToString());

				if (criteria.WorkTypeId.HasValue && displayWorkTypeSection)
					WorkTypeDropDown.SelectValue(criteria.WorkTypeId.Value.ToString());

				if (criteria.WorkWeekId.HasValue)
					WorkWeekDropDown.SelectValue(criteria.WorkWeekId.Value.ToString());
			}
		}

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <returns></returns>
		public CandidateSearchAvailabilityCriteria Unbind()
		{
			var theme = App.Settings.Theme;

			var workOverTime = ((theme == FocusThemes.Workforce) && WorkOvertimeCheckBox.Checked) ? true : (bool?)null;
		  var willingToRelocate = ((theme == FocusThemes.Workforce) && RelocateCheckBox.Checked) ? true : (bool?) null;
			var normalWorkShiftId = (theme == FocusThemes.Workforce) ? NormalWorkShiftsDropDown.SelectedValueToLong() : 0;
			var workTypeId = (theme == FocusThemes.Workforce) ? WorkTypeDropDown.SelectedValueToLong() : 0;
			var workWeekId = WorkWeekDropDown.SelectedValueToLong();

			return (workOverTime.HasValue || willingToRelocate.HasValue || normalWorkShiftId > 0 || workTypeId > 0 || workWeekId > 0)
																	? new CandidateSearchAvailabilityCriteria
																	{
																		WorkOvertime = workOverTime,
                                    WillingToRelocate = willingToRelocate,
																		NormalWorkShiftId = (normalWorkShiftId != 0) ? normalWorkShiftId : (long?)null,
																		WorkTypeId = (workTypeId != 0) ? workTypeId : (long?)null,
																		WorkWeekId = (workWeekId != 0) ? workWeekId : (long?)null
																	}
																	: null;
		}

		/// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear()
		{
			WorkOvertimeCheckBox.Checked = false;
		  RelocateCheckBox.Checked = false;
			
			if(NormalWorkShiftsDropDown.Items.Count > 0)
				NormalWorkShiftsDropDown.SelectedIndex = 0;
			
			if(WorkTypeDropDown.Items.Count > 0)
				WorkTypeDropDown.SelectedIndex = 0;
			
			if(WorkWeekDropDown.Items.Count > 0)
				WorkWeekDropDown.SelectedIndex = 0;
		}

		/// <summary>
		/// Binds the controls.
		/// </summary>
		public void BindControls()
		{
			var theme = App.Settings.Theme;

			var displayWorkOvertimeSection = (theme == FocusThemes.Workforce);
		  var displayRelocateSection = (theme == FocusThemes.Workforce);
			var displayWorkShiftsSection = (theme == FocusThemes.Workforce);
			var displayWorkTypeSection = (theme == FocusThemes.Workforce);

			if (displayWorkShiftsSection)
				BindNormalWorkShiftsDropDown();

			if (displayWorkTypeSection)
				BindWorkTypeDropDown();

			BindWorkWeekDropDown();

			WorkOvertimeRow.Visible = displayWorkOvertimeSection;
		  RelocateRow.Visible = displayRelocateSection;
			WorkShiftsRow.Visible = displayWorkShiftsSection;
			WorkTypeRow.Visible = displayWorkTypeSection;
		}

		/// <summary>
		/// Binds the normal work shifts drop down.
		/// </summary>
		private void BindNormalWorkShiftsDropDown()
		{
			NormalWorkShiftsDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.WorkShifts), null, CodeLocalise("WorkShifts.TopDefault", "- select work shift -"));
		}

		/// <summary>
		/// Binds the work type drop down.
		/// </summary>
		private void BindWorkTypeDropDown()
		{
			WorkTypeDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Durations), null, CodeLocalise("WorkTypes.TopDefault", "- select duration -"));
		}

		/// <summary>
		/// Binds the work week drop down.
		/// </summary>
		private void BindWorkWeekDropDown()
		{
			WorkWeekDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.WorkWeeks), null, CodeLocalise("WorkWeeks.TopDefault", "- select work week -"));
		}

		private void ApplyBranding()
		{
			AvailabilityHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			AvailabilityPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			AvailabilityPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}
	}
}