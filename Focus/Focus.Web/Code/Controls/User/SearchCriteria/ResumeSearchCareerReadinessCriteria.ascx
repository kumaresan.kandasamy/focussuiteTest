﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeSearchCareerReadinessCriteria.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchCareerReadinessCriteria" %>

<asp:Panel ID="CareerReadinessHeaderPanel" runat="server" CssClass="singleAccordionTitle noMarginBottom">
	<asp:Image ID="CareerReadinessHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" alt="." Height="22" Width="22"/>&nbsp;
  <span class="collapsiblePanelHeaderLabel cpHeaderControl">
    <a href="#">
      <asp:Label runat="server" ID="CareerReadinessHeaderLabel"></asp:Label>
    </a>
  </span>
  <div style="display:inline-block;"><%= HtmlTooltipster("tooltipWithArrow", "CareerReadiness.Tooltip", @"The NCRC is a portable workplace readiness credential recognized by participating #BUSINESSES#:LOWER across the US. It requires at least a Bronze level in assessments for Applied Mathematics, Locating Information, and Reading for Information. Job seekers must take standardized tests.")%></div>
</asp:Panel>
<asp:Panel ID="CareerReadinessPanel" runat="server" CssClass="accordionContent">
	<asp:UpdatePanel ID="CareerReadinessUpdatePanel" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
		  <p>
			<%= HtmlLabel(NCRCLevelDropDown, "NCRCLevelDropDown.Label", "Candidates who have attained the National Career Readiness Certificate at the following level(s)")%>
		  </p>
      <p>
			<asp:DropDownList runat="server" ID="NCRCLevelDropDown" />
      </p>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Panel>
<act:CollapsiblePanelExtender ID="CareerReadinessPanelExtender" runat="server" 
														TargetControlID="CareerReadinessPanel" 
														ExpandControlID="CareerReadinessHeaderPanel" 
														CollapseControlID="CareerReadinessHeaderPanel" 
														Collapsed="true" 
														ImageControlID="CareerReadinessHeaderImage" 
														CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
														ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
														SuppressPostBack="true"
														BehaviorID="CareerReadinessCriteriaBehavior" />