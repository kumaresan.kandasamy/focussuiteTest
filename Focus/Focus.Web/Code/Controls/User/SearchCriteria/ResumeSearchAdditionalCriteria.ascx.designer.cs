﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Focus.Web.Code.Controls.User.SearchCriteria {
    
    
    public partial class ResumeSearchAdditionalCriteria {
        
        /// <summary>
        /// LocationCriteria control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchLocationCriteria LocationCriteria;
        
        /// <summary>
        /// EducationCriteria control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchEducationCriteria EducationCriteria;
        
        /// <summary>
        /// GradePointAverageCriteria control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchGradePointAverageCriteria GradePointAverageCriteria;
        
        /// <summary>
        /// ProgramOfStudyCriteria control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchProgramOfStudyCriteria ProgramOfStudyCriteria;
        
        /// <summary>
        /// LanguageCriteria control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchLanguageCriteria LanguageCriteria;
        
        /// <summary>
        /// SkillsCriteria control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchSkillsCriteria SkillsCriteria;
        
        /// <summary>
        /// StatusCriteria control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchStatusCriteria StatusCriteria;
        
        /// <summary>
        /// ScoreCriteria control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchMatchScoreCriteria ScoreCriteria;
        
        /// <summary>
        /// DrivingLicenceCriteria control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchDrivingLicenceCriteria DrivingLicenceCriteria;
        
        /// <summary>
        /// IndustriesCriteriaDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl IndustriesCriteriaDiv;
        
        /// <summary>
        /// IndustriesCriteria control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchIndustriesCriteria IndustriesCriteria;
        
        /// <summary>
        /// LicencesCriteria control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchLicencesCriteria LicencesCriteria;
        
        /// <summary>
        /// CertificationsCriteria control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchCertificationsCriteria CertificationsCriteria;
        
        /// <summary>
        /// CareerReadinessCriteria control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchCareerReadinessCriteria CareerReadinessCriteria;
        
        /// <summary>
        /// AvailabilityCriteria control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchAvailabilityCriteria AvailabilityCriteria;
        
        /// <summary>
        /// CandidateCriteria control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.SearchCriteria.ResumeSearchCandidateCriteria CandidateCriteria;
    }
}
