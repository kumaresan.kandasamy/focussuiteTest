﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Core;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
  public partial class JobApplicationRequirementsModal : UserControlBase
	{
    public delegate void ReferredHandler(object sender, JobApplicationReferredEventArgs eventArgs);
    public event ReferredHandler Referred;

    /// <summary>
    /// Gets or sets the person id.
    /// </summary>
    /// <value>The person id.</value>
    public long PersonId
    {
      get { return GetViewStateValue<long>("JobApplicationRequirementsModal:PersonId", 0); }
      set { SetViewStateValue("JobApplicationRequirementsModal:PersonId", value); }
    }
    /// <summary>
    /// Gets or sets the person id.
    /// </summary>
    /// <value>The person id.</value>
    public long JobId
    {
      get { return GetViewStateValue<long>("JobApplicationRequirementsModal:JobId", 0); }
      set { SetViewStateValue("JobApplicationRequirementsModal:JobId", value); }
    }

    /// <summary>
    /// Gets or sets the score.
    /// </summary>
    /// <value>The person id.</value>
    public int Score
    {
      get { return GetViewStateValue("JobApplicationRequirementsModal:Score", 0); }
      set { SetViewStateValue("JobApplicationRequirementsModal:Score", value); }
    }

    /// <summary>
    /// Loads the page
    /// </summary>
    /// <param name="sender">Page raising the event</param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
        LocaliseUI();
    }

    #region Events

    /// <summary>
    /// Fires when an item is bound to the repeater control
    /// </summary>
    /// <param name="source">The repeater control</param>
    /// <param name="e">Arguments with details of the item being bound</param>
    protected void RequirementsRepeater_ItemDataBound(object source, RepeaterItemEventArgs e)
    {
      // Do nothing if this isn't a Item or AlternateItem
      if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
        return;

      var requirement = (Requirement) e.Item.DataItem;

      // Get access to each control
      var categoryLabel = (Literal)e.Item.FindControl("CategoryLabel");
      categoryLabel.Text = requirement.Category;

      var requirementLabel = (Literal)e.Item.FindControl("RequirementLabel");
      requirementLabel.Text = requirement.Description;

      var yesButton = (RadioButton)e.Item.FindControl("RequirementsMetRadioButton");
      yesButton.Text = HtmlLocalise("Global.Yes", "Yes");

      var noButton = (RadioButton)e.Item.FindControl("RequirementsNotMetRadioButton");
      noButton.Text = HtmlLocalise("Global.No", "No");

      var waiveButton = (RadioButton) e.Item.FindControl("RequirementsWaiveRadioButton");
      waiveButton.Text = HtmlLocalise("Global.Waive", "Waive");
    }

    /// <summary>
    /// Handles the Click event of the btnIMeetReq control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void MeetsRequirementsButton_Click(object sender, EventArgs e)
    {
      //OnReferred(new EventArgs());
      var noCount = 0;
      var waiveCount = 0;
      var yesCount = 0;
      var waivedRequirements = new List<string>();

      foreach (RepeaterItem item in RequirementsRepeater.Items)
      {
        var yesButton = (RadioButton)item.FindControl("RequirementsMetRadioButton");
        if (yesButton.Checked)
          yesCount++;

        var noButton = (RadioButton)item.FindControl("RequirementsNotMetRadioButton");
        if (noButton.Checked)
          noCount++;

        var waiveButton = (RadioButton)item.FindControl("RequirementsWaiveRadioButton");
        if (waiveButton.Checked)
        {
          waiveCount++;
          var categoryLiteral = (Literal)item.FindControl("CategoryLabel");
          waivedRequirements.Add(categoryLiteral.Text);
        }
      }

      if (noCount + waiveCount + yesCount == RequirementsRepeater.Items.Count)
      {
        if (noCount > 0)
        {
          JobRequirementsModal.Hide();

          Confirmation.Show(CodeLocalise("RequirementsNotMet.Header", "Requirements not met"),
            CodeLocalise("RequirementsNotMet.Body", "You have indicated that this #CANDIDATETYPE#:LOWER does not meet all of the requirements, as a result this #CANDIDATETYPE#:LOWER cannot be referred. If the questions were answered in error, please go back and amend your responses to complete this referral."),
						CodeLocalise("Global.Cancel", "Cancel"),
            okText: CodeLocalise("RequirementsNotMet.CloseText", "Return to requirements"),
            okCommandName: "RETURN");

          return;
        }

        OnReferred(new JobApplicationReferredEventArgs(PersonId, Score, waivedRequirements){JobId = JobId});
      }
    }

    /// <summary>
    /// Handles the OkCommand event of the Confirmation control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="CommandEventArgs" /> instance containing the event data.</param>
		protected void Confirmation_OkCommand(object sender, CommandEventArgs e)
    {
      if (e.CommandName == "RETURN")
        JobRequirementsModal.Show();
    }

    #endregion

    /// <summary>
    /// Localises the controls on the page
    /// </summary>
    private void LocaliseUI()
    {
      MeetsRequirementsButton.Text = HtmlLocalise("CompleteReferral.Button", "Complete Referral");
      RequirementsValidator.Text = HtmlLocalise("RequirementsValidator.Text", "Please check all the requirements");
    }

    /// <summary>
    /// Shows the modal
    /// </summary>
    /// <param name="jobId">The Id of the job</param>
    /// <param name="personId">The Id of the person being refered</param>
    /// <param name="score">The matching score for the candidate</param>
    public void Show(long jobId, long personId, int score)
    {
      PersonId = personId;
      Score = score;
      JobId = jobId;

      var requirements = new List<Requirement>();
      var job = ServiceClientLocator.JobClient(App).GetJob(jobId);

      // Minimum Education Level
      if (job.MinimumEducationLevelRequired.GetValueOrDefault(false) && job.MinimumEducationLevel.GetValueOrDefault(EducationLevels.None) != EducationLevels.None)
      {
        var educationLevel = CodeLocalise(job.MinimumEducationLevel, true);

        var requirement = new Requirement
        {
          Category = HtmlLocalise("Education.Category", "Education level"),
          Description = HtmlLocalise("EducationLevel.Requirement", "Must have at least a {0} level degree.", educationLevel)
        };
        requirements.Add(requirement);
      }

      // Programs of Study Required
      if (job.ProgramsOfStudyRequired.GetValueOrDefault(false))
      {
        var programOfStudys = ServiceClientLocator.JobClient(App).GetJobProgramsOfStudy(jobId).Select(jp => jp.ProgramOfStudy).ToList();
        if (programOfStudys.Any())
        {
          var requirement = new Requirement
          {
            Category = HtmlLocalise("ProgramOfStudys.Category", "Program of study"),
            Description = HtmlLocalise("ProgramOfStudys.Requirement", "Must have studied the following: {0}", string.Join(", ", programOfStudys))
          };
          requirements.Add(requirement);
        }
      }

      // Minimum Experience Required
      if (job.MinimumExperienceRequired.GetValueOrDefault(false) && (job.MinimumExperience.HasValue || job.MinimumExperienceMonths.HasValue))
      {
        var experienceText = new StringBuilder();
        var years = job.MinimumExperience.GetValueOrDefault(0);
        var months = job.MinimumExperienceMonths.GetValueOrDefault(0);

        if (years > 0)
        {
          experienceText.Append(years).Append(" ");
          experienceText.Append(years == 1 && months > 0 ? HtmlLocalise("Global.Year", "year") : HtmlLocalise("Global.Years", "years"));

          if (months > 0)
            experienceText.Append(" ").Append(HtmlLocalise("Global.And", " and ")).Append(" ");
        }

        if (months > 0)
          experienceText.Append(job.MinimumExperienceMonths).Append(HtmlLocalise("Global.Months", "months"));

        var requirement = new Requirement
        {
          Category = HtmlLocalise("MinimumExperience.Category", "Minimum experience"),
          Description = HtmlLocalise("MinimumExperience.Requirement", "Must have at least {0} of experience.", experienceText.ToString())
        };
        requirements.Add(requirement);
      }

      // Minimum Age Required
      if (job.MinimumAgeRequired.GetValueOrDefault(false) && job.MinimumAge.GetValueOrDefault(0) > 1)
      {
        var requirement = new Requirement
        {
          Category = HtmlLocalise("MinimumAge.Category", "Minimum age"),
          Description = HtmlLocalise("MinimumAge.Requirement", "Must be at least {0} years of age.", job.MinimumAge)
        };
        requirements.Add(requirement);
      }

      //NCRC
      if (job.CareerReadinessLevelRequired.GetValueOrDefault(false) && job.CareerReadinessLevel.HasValue)
      {
          var ncrclevel = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.NCRCLevel).First(l => l.Id == job.CareerReadinessLevel);
          var requirement = new Requirement
          {
              Category = HtmlLocalise("NCRC.Category", "NCRC"),
              Description = HtmlLocalise("NCRC.Requirement", "Must hold a {0} National Career Readiness Certificate", ncrclevel.Text)
          };
          requirements.Add(requirement);
      }

      // Driving Licence Required
      if (job.DrivingLicenceRequired.GetValueOrDefault(false) && job.DrivingLicenceClassId.HasValue)
      {
        var drivingLicense = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceClasses).First(l => l.Id == job.DrivingLicenceClassId);
        var drivingLicenseText = new StringBuilder();
        drivingLicenseText.Append(HtmlLocalise("DrivingLicence.Requirement", "Must have a '{0}' license", drivingLicense.Text));

	      var drivingLicenceEndorsements = ServiceClientLocator.JobClient(App).GetJobDrivingLicenceEndorsements(jobId);

				if (drivingLicenceEndorsements.IsNotNullOrEmpty())
        {
	        var endorsementLookups = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceEndorsements);

	        var endorsementStrings = drivingLicenceEndorsements.Select(endorsement => endorsementLookups.Where(x => x.Id == endorsement.DrivingLicenceEndorsementId).Select(x => x.Text).FirstOrDefault()).Where(lookupText => lookupText.IsNotNullOrEmpty()).ToList();

	        var endorsementDescription = endorsementStrings.Count() == 1
            ? HtmlLocalise("Endorsement.Requirement", " with the following endorsments: {0}", endorsementStrings[0])
            : HtmlLocalise("Endorsements.Requirement", " with the following endorsments: {0}", string.Join(", ", endorsementStrings));

          if (endorsementStrings.Any())
            drivingLicenseText.Append(endorsementDescription);
        }

        var requirement = new Requirement
        {
          Category = HtmlLocalise("DrivingLicence.Category", "Driving license"),
          Description = drivingLicenseText.ToString()
        };
        requirements.Add(requirement);
      }

      // Occupational Licences Required
      if (job.LicencesRequired.GetValueOrDefault(false))
      {
        var licences = ServiceClientLocator.JobClient(App).GetJobLicences(jobId).Select(jl => jl.Licence).ToList();
        if (licences.Any())
        {
          var licenceDescription = licences.Count() == 1 
            ? HtmlLocalise("Licence.Requirement", "Must hold the following license: {0}", licences[0])
            : HtmlLocalise("Licences.Requirement", "Must hold the following licenses: {0}", string.Join(", ", licences));

          var requirement = new Requirement
          {
            Category = HtmlLocalise("Licences.Category", "Occupational licence"),
            Description = licenceDescription
          };
          requirements.Add(requirement);
        }
      }

      // Certifications Required
      if (job.CertificationRequired.GetValueOrDefault(false))
      {
        var certificates = ServiceClientLocator.JobClient(App).GetJobCertificates(jobId).Select(jc => jc.Certificate).ToList();
        if (certificates.Any())
        {
          var certificateDescription = certificates.Count() == 1
            ? HtmlLocalise("Certificate.Requirement", "Must hold the following certificate: {0}", certificates[0])
            : HtmlLocalise("Certificates.Requirement", "Must hold the following certificates: {0}", string.Join(", ", certificates));

          var requirement = new Requirement
          {
            Category = HtmlLocalise("Certificates.Category", "Certificate"),
            Description = certificateDescription
          };
          requirements.Add(requirement);
        }
      }

      // Languages Required
      if (job.LanguagesRequired.GetValueOrDefault(false))
      {
        var allLanguages = ServiceClientLocator.JobClient(App).GetJobLanguages(jobId).ToList();

				var languagesWithNoProficiency = allLanguages.Where(l => l.LanguageProficiencyId.IsNullOrZero()).ToList();
				if (languagesWithNoProficiency.Any())
        {
          var languages = languagesWithNoProficiency.Select(x => x.Language).ToList();
					var languageDescription = languagesWithNoProficiency.Count() == 1
						? HtmlLocalise("Language.Requirement", "Must be proficient in the following language: {0}", languagesWithNoProficiency[0])
            : HtmlLocalise("Languages.Requirement", "Must be proficient in the following languages: {0}", string.Join(", ", languages));

          var requirement = new Requirement
          {
            Category = HtmlLocalise("Languages.Category", "Language"),
            Description = languageDescription
          };
          requirements.Add(requirement);
        }

				var languagesWithProficiencies = allLanguages.Where(l => l.LanguageProficiencyId.IsNotNullOrZero()).ToList();
				languagesWithProficiencies.ForEach(language =>
				{
					var proficiency = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.LanguageProficiencies).First(l => l.Id == language.LanguageProficiencyId).Text;
					var languageDescription = HtmlLocalise("LanguageWithProficiency.Requirement", "You are required to have knowledge of the following language and proficiency level: {0} {1}", language.Language, proficiency);

					var requirement = new Requirement
					{
						Category = HtmlLocalise("Languages.Category", "Language"),
						Description = languageDescription
					};
					requirements.Add(requirement);
				});
      }

      // Special Requirements Required
      var specialRequirements = ServiceClientLocator.JobClient(App).GetJobSpecialRequirements(jobId).Select(jsr => jsr.Requirement.ToSentenceCase()).ToList();
      if (specialRequirements.Any())
      {
        var specialRequirementDescription = specialRequirements.Count() == 1
          ? HtmlLocalise("SpecialRequirement.Requirement", "Must meet the following mandatory requirement of this job: {0}", specialRequirements[0])
          : HtmlLocalise("SpecialRequirements.Requirement", "Must meet the following mandatory requirements of this job: {0}", string.Join(", ", specialRequirements));

        var requirement = new Requirement
        {
          Category = HtmlLocalise("SpecialRequirements.Category", "Special"),
          Description = specialRequirementDescription
        };
        requirements.Add(requirement);
      }

      if (requirements.Any())
      {
        RequirementsRepeater.DataSource = requirements;
        RequirementsRepeater.DataBind();

        RequirementsCount.Value = requirements.Count().ToString(CultureInfo.InvariantCulture);

        JobRequirementsModal.Show();
      }
      else
      {
        OnReferred(new JobApplicationReferredEventArgs(PersonId, Score){JobId = jobId});
      }
    }

    /// <summary>
    /// Raises the <see cref="E:Completed"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected virtual void OnReferred(JobApplicationReferredEventArgs eventArgs)
    {
      if (Referred != null)
        Referred(this, eventArgs);
    }

    public struct Requirement
    {
      public string Category;
      public string Description;
    }
  }

  #region EventArgs

  /// <summary>
  /// Requirements that have been waived
  /// </summary>
  public class JobApplicationReferredEventArgs : EventArgs
  {
    public long PersonId;
    public int Score;
    public long JobId { get; set; }

    public readonly List<string> WaivedRequirments;

    public JobApplicationReferredEventArgs(long personId, int score)
    {
      PersonId = personId;
      Score = score;
      WaivedRequirments = new List<string>();
    }

    public JobApplicationReferredEventArgs(long personId, int score, List<string> waivedRequirments)
    {
      PersonId = personId;
      Score = score;
      WaivedRequirments = waivedRequirments;
    }
  }

  #endregion
}