﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;
using Focus.Core;
using Focus.Core.Models;
using Focus.Web.ViewModels;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class JobWizardStep2 : UserControlBase
	{
		public delegate void PathSelectedHandler(object sender, CommandEventArgs eventArgs);
		public event PathSelectedHandler PathSelected;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      
    }

    /// <summary>
    /// Initialises the step.
    /// </summary>
    /// <param name="model">The model.</param>
    internal void BindStep(JobWizardViewModel model)
    {
      if (model.Job.JobType == JobTypes.InternshipPaid || model.Job.JobType == JobTypes.InternshipUnpaid)
      {
        Path1Label.Text = CodeLocalise("JobWizardStep2.InternshipDescription",
                                       "<strong>1. I have an internship description:</strong> click to add your internship description and continue");
        Path2Label.Text = CodeLocalise("JobWizardStep2.InternshipNoDescription",
                                       "<strong>2. I don't have an internship description:</strong> we'll help you create a complete description");
        
      }
      else
      {
        Path1Label.Text = CodeLocalise("JobWizardStep2.JobDescription",
                                         "<strong>1. I have a job description:</strong> click to add your job description and continue");
        Path2Label.Text = CodeLocalise("JobWizardStep2.JobNoDescription",
                                       "<strong>2. I don't have a job description:</strong> we'll help you create a complete description");
      }
    }

	  /// <summary>
		/// Handles the Click event of the Path1Button control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Path1Button_Click(object sender, EventArgs e)
	  {
      OnPathSelected(new CommandEventArgs("PATH-SELECTED", 1));
		}

		/// <summary>
		/// Handles the Click event of the Path2Button control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Path2Button_Click(object sender, EventArgs e)
		{
		  OnPathSelected(new CommandEventArgs("PATH-SELECTED", 21));
		}

		/// <summary>
		/// Raises the <see cref="E:PathSelected"/> event.
		/// </summary>
		/// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected virtual void OnPathSelected(CommandEventArgs eventArgs)
		{
			if (PathSelected != null)
				PathSelected(this, eventArgs);
		}
	}
}