﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
    public partial class EmailEmployeeModal : UserControlBase
    {
        internal long EmployeeId
        {
            get { return GetViewStateValue<long>("EmailEmployeeModal:EmployeeId"); }
            set { SetViewStateValue("EmailEmployeeModal:EmployeeId", value); }
        }

        internal List<long> EmployeeIds
        {
            get { return GetViewStateValue<List<long>>("EmailEmployeeModal:EmployeeIds"); }
            set { SetViewStateValue("EmailEmployeeModal:EmployeeIds", value); }
        }


        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LocaliseUI();
            }

            EmailSubjectRequired.ValidationGroup = EmailBodyRequired.ValidationGroup = SendMessageButton.ValidationGroup = String.Format("{0}:Validation", ClientID);
        }

        /// <summary>
        /// Shows the specified employee id.
        /// </summary>
        /// <param name="employeeId">The employee id.</param>
        public void Show(long employeeId)
        {
            EmployeeId = employeeId;
            ClearInputs();

            EmailEmployeeModalPopup.Show();
        }

        /// <summary>
        /// Shows the specified employee id.
        /// </summary>
        /// <param name="employeeIds">The employee ids.</param>
        public void Show(List<long> employeeIds)
        {
            EmployeeIds = employeeIds;
            ClearInputs();

            EmailEmployeeModalPopup.Show();
        }

        /// <summary>
        /// Handles the Clicked event of the SendMessageButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void SendMessageButton_Clicked(object sender, EventArgs e)
        {
            var emailSubject = EmailSubjectTextBox.TextTrimmed();
            var emailBody = EmailBodyTextBox.TextTrimmed();

            // We will use the custom email template
            var senderAddress = ServiceClientLocator.CoreClient(App).GetSenderEmailAddressForTemplate(EmailTemplateTypes.CustomMessage);

            if (EmployeeIds == null)
            {
                ServiceClientLocator.EmployeeClient(App).EmailEmployee(EmployeeId, emailSubject, emailBody, BCCMeCheckBox.Checked, senderAddress: senderAddress);
            }
            else
            {
                var employeeIdList = new List<long>();
                foreach (var employeeid in EmployeeIds)
                {
                    //ServiceClientLocator.EmployeeClient(App).EmailEmployee(employeeid, emailSubject, emailBody, BCCMeCheckBox.Checked, senderAddress: senderAddress);
                    employeeIdList.Add(employeeid);

                }
                ServiceClientLocator.EmployeeClient(App).EmailEmployees(employeeIdList, emailSubject, emailBody, BCCMeCheckBox.Checked, senderAddress: senderAddress);
            }

            EmailEmployeeModalPopup.Hide();

            var body = EmployeeIds.IsNull() || EmployeeIds.Count == 1
                      ? CodeLocalise("EmailEmployeeConfirmation.Body", "Thank you. We have sent the email to the #BUSINESS#:LOWER you selected.")
                      : CodeLocalise("EmailEmployeesConfirmation.Body", "Thank you. We have sent the email to the #BUSINESS#:LOWER(es) you selected.");

            Confirmation.Show(CodeLocalise("EmailEmployeesConfirmation.Title", "Email #BUSINESS#:LOWER(es)"),
                        body,
                        CodeLocalise("Global.Ok.Text", "Ok"));

            ClearInputs();
        }

        /// <summary>
        /// Handles the Clicked event of the CancelButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CancelButton_Clicked(object sender, EventArgs e)
        {
            EmailEmployeeModalPopup.Hide();
        }

        /// <summary>
        /// Localises the UI.
        /// </summary>
        private void LocaliseUI()
        {
            EmailSubjectRequired.ErrorMessage = CodeLocalise("EmailSubject.RequiredErrorMessage", "Email subject is required.");
            EmailBodyRequired.ErrorMessage = CodeLocalise("EmailBody.RequiredErrorMessage", "Email text is required.");
            SendMessageButton.Text = CodeLocalise("SendMessageButton.Button", "Send message");
            CancelButton.Text = CodeLocalise("Global.Cancel.Button", "Cancel");
            BCCMeCheckBox.Text = CodeLocalise("BCCMeCheckBox.Text", "email me a copy of this message");
        }

        /// <summary>
        /// Clears the inputs.
        /// </summary>
        private void ClearInputs()
        {
            EmailSubjectTextBox.Text = EmailBodyTextBox.Text = string.Empty;
            BCCMeCheckBox.Checked = false;
        }
    }
}