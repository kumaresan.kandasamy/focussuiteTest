<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobWizardLocationSelector.ascx.cs" Inherits="Focus.Web.Code.Controls.User.JobWizardLocationSelector" %>
<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>
<%@ Import Namespace="Framework.Core" %>
<% if (Mode == LocationSelectorMode.Standard)
   { %>
<%= HtmlInFieldLabel("LocationTextBox", "Location.InlineLabel", "enter city, state; or enter ZIP", 250) %>
<asp:TextBox runat="server" ID="LocationTextBox" Width="250" ClientIDMode="Static" />
<asp:Button ID="AddButton" runat="server" class="button3" OnClick="AddButton_Click" ValidationGroup="JobWizardLocationSelector"/>
<div>
	<asp:CustomValidator runat="server" ID="LocationTextBoxValidator" ControlToValidate="LocationTextBox" SetFocusOnError="True" CssClass="error" Display="Dynamic" OnServerValidate="LocationTextBoxFormatValidator_ServerValidate" ValidationGroup="JobWizardLocationSelector" />
</div>
<div><i><%= HtmlLocalise("LocationExample.Text", "example: Trenton, NJ") %></i></div>
<div><i><%= HtmlLocalise("ZipExample.Text", "example: 08640") %></i></div>
<% }
   else if (Mode == LocationSelectorMode.FullAddressSingle)
   {
       if (JobLocations.IsNotNullOrEmpty())
       { %>
   <table style="width:400px">
       <caption>Full location address;</caption>
       <tbody>
       <tr style="vertical-align: top">
           <th style="width:50%;text-align:left;"><%=HtmlLocalise("Global.Line1", "Line 1")%>:</th>
           <td><%= JobLocations[0].AddressLine1 %></td>
       </tr>
       <tr style="vertical-align: top">
           <th style="text-align:left"><%=HtmlLocalise("Global.City", "City")%>:</th>
           <td><%= JobLocations[0].City %></td>
       </tr>
       <%
       if (App.Settings.CountyEnabled)
       { 
       %>
       <tr style="vertical-align: top">
           <th style="text-align:left" data-header="County"><%=HtmlLocalise("Global.County", "County")%>:</th>
           <td data-field="County"><%= JobLocations[0].County %></td>
       </tr>
       <% 
       }  
       %>
       <tr style="vertical-align: top">
           <th style="text-align:left"><%=HtmlLocalise("Global.State", "State")%>:</th>
           <td><%= JobLocations[0].State %></td>
       </tr>
       <tr style="vertical-align: top">
           <th style="text-align:left"><%=HtmlLocalise("Global.ZipCode", "Zip code")%>:</th>
           <td><%= JobLocations[0].Zip %></td>
       </tr>
       </tbody>
   </table>
<% }
    }

else // LocationSelectorMode.FullAddressMany
   { %>
    <!-------------------------------------------------->
		
		<asp:Panel runat="server" DefaultButton="AddFullAddressButton">
			<table>
			<tr>
			<td class="label"><%= HtmlRequiredLabel(AddressLine1TextBox, "AddressLine1.Label", "Address")%></td>
			<td>
				<asp:TextBox ID="AddressLine1TextBox" runat="server" Width="98%" TabIndex="2" MaxLength="200" />
				<asp:RequiredFieldValidator  ValidationGroup="JobWizardLocationSelectorFull" ID="AddressLine1Required" runat="server" ControlToValidate="AddressLine1TextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			</td>
		</tr>
				<tr>
			<td class="label"><%= HtmlRequiredLabel(AddressPostcodeZipTextBox, "AddressPostcodeZip.Label", "ZIP or postal code")%></td>
			<td>
				<asp:TextBox ID="AddressPostcodeZipTextBox" runat="server"  Width="90px" TabIndex="3" MaxLength="10" />
				<asp:RequiredFieldValidator  ValidationGroup="JobWizardLocationSelectorFull" ID="AddressPostcodeZipRequired" runat="server" ControlToValidate="AddressPostcodeZipTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
				<asp:RegularExpressionValidator  ValidationGroup="JobWizardLocationSelectorFull" ID="AddressPostcodeRegexValidator" runat="server" CssClass="error" ControlToValidate="AddressPostcodeZipTextBox" SetFocusOnError="true" Display="Dynamic" />
			</td>
		</tr>
		<tr>
			<td class="label"><%= HtmlRequiredLabel(AddressTownCityTextBox, "AddressTownCity.Label", "City")%></td>
			<td>
				<asp:TextBox ID="AddressTownCityTextBox" runat="server" Width="98%" TabIndex="4" MaxLength="100" />
				<asp:RequiredFieldValidator  ValidationGroup="JobWizardLocationSelectorFull" ID="AddressTownCityRequired" runat="server" ControlToValidate="AddressTownCityTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			</td>
		</tr>
    <asp:PlaceHolder runat="server" ID="CountyPlaceHolder">
		<tr>
			<td class="label"><%= HtmlRequiredLabel(AddressCountyDropDownList, "AddressCounty.Label", "County")%></td>
			<td>
        <focus:AjaxDropDownList ID="AddressCountyDropDownList" runat="server"  Width="75%" TabIndex="5" onchange="JobWizardLocationSelector_UpdateStateCountry()"/>
			  <act:CascadingDropDown ID="AddressCountyDropDownListCascadingDropDown" runat="server" TargetControlID="AddressCountyDropDownList" ParentControlID="AddressStateDropDownList" 
															  ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetCounties" Category="Counties" BehaviorID="AddressCountyDropDownListCascadingDropDown" />
        <asp:RequiredFieldValidator  ValidationGroup="JobWizardLocationSelectorFull" ID="AddressCountyRequired" runat="server" ControlToValidate="AddressCountyDropDownList" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			</td>
		</tr>
    </asp:PlaceHolder>
		<tr>
			<td class="label"><%= HtmlRequiredLabel(AddressStateDropDownList, "AddressState.Label", "State")%></td>
			<td>
				<asp:DropDownList ID="AddressStateDropDownList" runat="server" Width="100%" TabIndex="6"/>
				<asp:RequiredFieldValidator  ValidationGroup="JobWizardLocationSelectorFull" ID="AddressStateRequired" runat="server" ControlToValidate="AddressStateDropDownList" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			</td>
		</tr>
			</table>
			<!-------------------------------------------------->

			<asp:Button runat="server" ID="AddFullAddressButton" CssClass="button3" OnClick="AddFullAddressButton_OnClick" ValidationGroup="JobWizardLocationSelectorFull"/>
		    <div>
		        <asp:CustomValidator  ID="LocationValidator" ControlToValidate="AddressTownCityTextBox" SetFocusOnError="True"  runat="server" CssClass="error" Display="Dynamic" OnServerValidate="LocationValidator_ServerValidate" ValidationGroup="JobWizardLocationSelectorFull" />
		    </div>
		</asp:Panel>
<% }
   if (Mode != LocationSelectorMode.FullAddressSingle)
   { %>
<br />

<table id="locationsTable" runat="server" Visible="false" style="width:500px" role="presentation">
	<tr>
		<td class="focusTable" style="width:250px"><%= HtmlLocalise("LocationColumn.Title", "Location")%></td>
		<td class="focusTable" style="width: 200px; text-align:center;"><%= HtmlLocalise("PublicTransitColumn.Title", "Public transit accessible")%></td>
		<td class="focusTable" style="width:50px;text-align:center;"><%= HtmlLocalise("DeleteColumn.Title", "Delete")%></td>
	</tr>
</table>
<asp:ListView ID="ItemsList" runat="server" onitemdatabound="ItemsList_DataBound" ItemPlaceholderID="ItemListViewPlaceHolder">
	<LayoutTemplate>
		<table style="width:500px"><asp:PlaceHolder ID="ItemListViewPlaceHolder" runat="server" /></table> 
	</LayoutTemplate>
	<ItemTemplate>
		<tr>
			<td style="width:250px"><%# ((JobLocationDto)Container.DataItem).Location %>
			</td>
			<td style="width: 200px; text-align: center;"><asp:CheckBox ID="PublicTransitCheckbox" runat="server" /></td>
			<td style="width:50px;text-align:center;">
				<asp:ImageButton ID="ItemsListRemoveImageButton" runat="server"  OnCommand="ItemsListRemoveButton_Command" CommandArgument="<%# Container.DataItemIndex %>" 
													CommandName="Remove" ImageUrl="<%# UrlBuilder.ButtonDeleteIcon() %>" CausesValidation="false" />
			</td>
		</tr>
	</ItemTemplate>
</asp:ListView>
<% } %>
<asp:HiddenField ID="LocationSelectorHasRowsHidden" runat="server" Value="false"	ClientIDMode="Static" />
<script type="text/javascript">
  Sys.Application.add_load(function() {
    <%=ClientID%>_JobWizardLocationSelectorDocumentReady();
  });

  var prm = Sys.WebForms.PageRequestManager.getInstance();

  prm.add_endRequest(function () {
    <%=ClientID%>_JobWizardLocationSelectorDocumentReady();

    // Apply jQuery to infield labels
    $(".inFieldLabel > label, .inFieldLabelAlt > label").inFieldLabels();
  });

  function <%=ClientID%>_JobWizardLocationSelectorDocumentReady() {
    $("#<%=AddressPostcodeZipTextBox.ClientID%>").mask("<%= App.Settings.ExtendedPostalCodeMaskPattern %>", { placeholder: " " });
		$("#<%= AddressPostcodeZipTextBox.ClientID %>").change(function() {
		  PopulateAddressFieldsForPostalCode(
		    '<%= UrlBuilder.AjaxService() %>',
		    $(this).val(),
		    '<%=AddressStateDropDownList.ClientID %>',
		    '<%=AddressCountyDropDownList.ClientID %>',
		    '<%=AddressCountyDropDownListCascadingDropDown.BehaviorID%>', 
		    '<%=HtmlLocalise("Global.County.TopDefault", "- select county -") %>',
		    '<%=AddressTownCityTextBox.ClientID %>');
		});
  }
  
	function JobWizardLocationSelector_UpdateStateCountry() {
		var countyValue = $('#<%=AddressCountyDropDownList.ClientID %>').val();
		var stateValue = $('#<%=AddressStateDropDownList.ClientID %>').val();

		if (countyValue == '<%= GetLookupId(Constants.CodeItemKeys.Counties.OutsideUS) %>') {
			if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>')
				JobWizardLocationSelector_SetState(true);
		}
	}
	
	function JobWizardLocationSelector_SetState(outsideUS) {
		var stateDropDown = $('#<%=AddressStateDropDownList.ClientID %>');

		if (outsideUS)
			$(stateDropDown).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>"]').prop('selected', true);
		else
			$(stateDropDown).children().first().prop('selected', true);

		$(stateDropDown).next().find(':first-child').text($(stateDropDown).children('option:selected').text()).parent().addClass('changed');
  }
</script>