﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;

using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
  public partial class ReferralRequestSentModal : UserControlBase
  {
    public delegate void AutoApprovedHandler(object sender, EventArgs eventArgs);
    public event AutoApprovedHandler AutoApproved;

    /// <summary>
    /// Gets or sets the job seeker's name
    /// </summary>
    public string JobSeekerName
    {
      get { return GetViewStateValue("ReferralRequestSentModal:JobSeekerName", string.Empty); }
      set { SetViewStateValue("ReferralRequestSentModal:JobSeekerName", value); }
    }

    /// <summary>
    /// Gets or sets the application id
    /// </summary>
    public long CandidateApplicationId
    {
      get { return GetViewStateValue("ReferralRequestSentModal:CandidateApplicationId", (long)0); }
      set { SetViewStateValue("ReferralRequestSentModal:CandidateApplicationId", value); }
    }

    /// <summary>
    /// Gets or sets the application approval status
    /// </summary>
    public ApprovalStatuses ApprovalStatus
    {
      get { return GetViewStateValue("ReferralRequestSentModal:ApprovalStatuses", ApprovalStatuses.None); }
      set { SetViewStateValue("ReferralRequestSentModal:ApprovalStatuses", value); }
    }

    /// <summary>
    /// Fires when the page is being loaded
    /// </summary>
    /// <param name="sender">Page raising the event</param>
    /// <param name="e">Standard event arguments</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        LocaliseUI();
      }

      AutoApprovalModalPopup.CancelControlID = CancelButton.ClientID;
    }

    /// <summary>
    /// Shows the modal
    /// </summary>
    /// <param name="personId">The person identifier.</param>
    /// <param name="jobId">The job identifier.</param>
    /// <param name="jobSeekerName">The job seeker's name to display in the modal.</param>
    /// <param name="application">The application.</param>
    /// <returns></returns>
    public bool Show(long personId, long jobId = 0, string jobSeekerName = null, ApplicationDto application = null)
    {
      if (App.Settings.Theme == FocusThemes.Education)
        return false;

      // Check the status of any existing application
      if (application.IsNull())
        application = ServiceClientLocator.CandidateClient(App).GetApplication(personId, jobId);

      if (application.IsNull() || application.Id.IsNullOrZero())
        return false;

      // Only show the modal for pending, denied or reconsider statuses
      if (application.ApprovalStatus.IsIn(ApprovalStatuses.WaitingApproval, ApprovalStatuses.Reconsider, ApprovalStatuses.Rejected))
      {
        if (jobSeekerName.IsNull())
        {
          var candidate = ServiceClientLocator.CandidateClient(App).GetCandidatePerson(personId);
          JobSeekerName = string.Concat(candidate.FirstName, " ", candidate.LastName);
        }
        else
        {
          JobSeekerName = jobSeekerName.Replace("\n", " ").Trim();  
        }

        CandidateApplicationId = application.Id.GetValueOrDefault(0);
        ApprovalStatus = application.ApprovalStatus;

        Title.Text = ApprovalStatus == ApprovalStatuses.Rejected
          ? HtmlLocalise("Title.Reviewed.Text", "Resume review already taken place")
          : HtmlLocalise("Title.InProgress.Text", "Resume review in progress");

        Details.Text = ApprovalStatus == ApprovalStatuses.Rejected
          ? HtmlLocalise("Details.Reviewed.Text", "{0} has already submitted a referral request to this position which was previously denied. By clicking OK, the job seeker will be notified that the decision regarding their referral request has been reversed and they will be displayed as an applicant for this job posting. If you abandon this action, the job seeker's request will remain as denied.", JobSeekerName)
          : HtmlLocalise("Details.InProgress.Text", "{0} has already submitted a referral request to this position which is currently under review. By clicking OK, the job seeker will be notified that their referral request has been successful and they will be displayed on your Applicants tab. If you abandon this action, the job seeker's request will remain under review.", JobSeekerName);

        AutoApprovalModalPopup.Show();

        // Indicate the modal was shown
        return true;
      }

      // Indicate the modal was not shown
      return false;
    }

    /// <summary>
    /// Localises the text on the page
    /// </summary>
    private void LocaliseUI()
    {
      OkButton.Text = HtmlLocalise("Global.OK", "OK");
      CancelButton.Text = HtmlLocalise("Global.Cancel", "Cancel");
    }

    /// <summary>
    /// Fires when the OK button is click to perform the referral
    /// </summary>
    /// <param name="sender">Button raising the event</param>
    /// <param name="e">Standard event arguments</param>
    protected void OkButton_OnClick(object sender, EventArgs e)
    {
      var candidateApplication = ServiceClientLocator.CandidateClient(App).GetJobSeekerReferral(CandidateApplicationId);
      Utilities.ApproveCandidateReferral(candidateApplication, true, EmailTemplateTypes.AutoApprovedJobSeekerReferral, ConfirmationModal);

      AutoApprovalModalPopup.Hide();

      OnAutoApproved(new EventArgs());
    }

    /// <summary>
    /// Raised the Auto-Approved event
    /// </summary>
    /// <param name="eventArgs">Standard event arguments</param>
    protected virtual void OnAutoApproved(EventArgs eventArgs)
    {
      if (AutoApproved != null)
        AutoApproved(this, eventArgs);
    }
  }
}