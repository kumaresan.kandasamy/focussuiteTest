﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobWizardStep4.ascx.cs"
    Inherits="Focus.Web.Code.Controls.User.JobWizardStep4" %>
<%@ Register Src="~/Code/Controls/User/UpdateableList.ascx" TagName="UpdateableList"
    TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/UpdateableLanguagesList.ascx" TagName="UpdateableLanguagesList"
    TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/LanguageProficiencyDefinitionsModal.ascx"
    TagName="LanguageProficiencyDefinitionsModal" TagPrefix="uc" %>
<table role="presentation" style="width: 100%;">
    <tr style="float: right" id="ExpandCollapseAllRow" runat="server">
        <td>
            <asp:Button ID="ExpandCollapseAllButton" runat="server" class="button3" Style="float: right"
                ClientIDMode="Static" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="MinimumEducationHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
                <table role="presentation" class="accordionTable">
                    <tr class="multipleAccordionTitle">
                        <td>
                            <asp:Image ID="MinimumEducationHeaderImage" runat="server" AlternateText="Minimum Education Header Image" />
                            &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><asp:HyperLink
                                ID="MinimumEducationLiteral" runat="server" href="#" /></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div class="singleAccordionContentWrapper">
                <asp:Panel ID="MinimumEducationPanel" runat="server" CssClass="singleAccordionContent">
                    <table role="presentation" style="width: 100%;">
                        <tr>
                            <td>
                                <%= AddRequirementLabel%>
                                <asp:CheckBox ID="HideEducationCheckBox" runat="server" />
                            </td>
                        </tr>
                        <tr id="MinimumEducationRow" runat="server">
                            <td>
                                <%= HtmlRequiredLabel(MinimumEducationDropDownList, "MinimumEducation.Label", "Applicants must have at least this level of education")%>&nbsp;&nbsp;
                                <asp:DropDownList ID="MinimumEducationDropDownList" runat="server" />
                                <asp:RequiredFieldValidator ID="MinimumEducationRequired" runat="server" ControlToValidate="MinimumEducationDropDownList"
                                    CssClass="error" />
                            </td>
                        </tr>
                        <tr id="StudentEnrolledRow" runat="server">
                            <td>
                                <table role="presentation">
                                    <tr>
                                        <td>
                                            <%= HtmlRequiredLabel(StudentEnrolledRadioButtonList, "StduentEnrolled.Label", "Student must be currently enrolled in college")%>
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="StudentEnrolledRadioButtonList" runat="server" ClientIDMode="Static"
                                                RepeatDirection="Horizontal" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="MinimumCollegeYearsRow" runat="server">
                            <td>
                                <%= HtmlRequiredLabel(MinimumCollegeYearsDropDown, "MinimumCollegeYears.Label", "Student must have completed at least this many year(s) of college")%>&nbsp;&nbsp;
                                <asp:DropDownList ID="MinimumCollegeYearsDropDown" runat="server" ClientIDMode="Static" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButtonList ID="EducationCriteriaRadioButtonList" runat="server" ClientIDMode="Static" />
                                <focus:DependentRequiredFieldValidator ID="EducationCriteriaPreferenceRequired" runat="server"
                                    ControlToValidate="EducationCriteriaRadioButtonList" DependentControl="MinimumEducationDropDownList"
                                    CssClass="error" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <asp:PlaceHolder runat="server" ID="BackgroundCheckPlaceHolder" Visible="false">
                <asp:Panel ID="BackgroundCheckHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
                    <table role="presentation" class="accordionTable">
                        <tr class="multipleAccordionTitle">
                            <td>
                                <asp:Image ID="BackgroundCheckHeaderImage" runat="server" AlternateText="Background Check Header Image" />
                                &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("BackgroundCheck.Label", "Background check requirements")%></a></span>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div class="singleAccordionContentWrapper">
                    <asp:Panel ID="BackgroundCheckPanel" runat="server" CssClass="singleAccordionContent">
                        <table role="presentation">
                            <asp:PlaceHolder runat="server" ID="CreditCheckPlaceHolder" Visible="false">
                                <tr>
                                    <td style="width: 300px">
                                        <%=HtmlRequiredLabel("CreditCheckLabel.Label", "Will a credit background check on the applicant be required?")%>
                                        <%--<focus:LocalisedLabel runat="server" ID="CreditCheckLabel" DefaultText="Will a credit check on the applicant be required?" />--%>
                                    </td>
                                    <td style="width: 200px">
                                        <asp:RadioButtonList runat="server" ID="CreditCheckRadioButtonList" RepeatDirection="Horizontal"
                                            CellPadding="15" CellSpacing="15" onchange="CreditCheckRadioButtonChanged();" />
                                        <asp:CustomValidator ID="CreditCheckValidator" runat="server" CssClass="error" SetFocusOnError="true" Enabled="false"
                                            ClientValidationFunction="ValidateCreditCheck" />
                                    </td>
                                    <td id="TEGLNoticeToEmployersCell">
                                        <focus:LocalisedLabel runat="server" ID="TEGLNoticeToEmployersLabel" RenderOuterSpan="True"
                                            CssClass="information-panel" DefaultText="" />
                                    </td>
                                </tr>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" ID="CriminalBackgroundCheckPlaceHolder" Visible="false">
                                <tr>
                                    <td style="width: 300px">
                                        <%=HtmlRequiredLabel("CriminalBackgroundCheckLabel.Label", "Will a criminal background check on the applicant be required?")%>
                                        <%--<focus:LocalisedLabel runat="server" ID="CriminalBackgroundCheckLabel" DefaultText="Will a criminal background check on the applicant be required?" />--%>
                                    </td>
                                    <td style="width: 200px">
                                        <asp:RadioButtonList runat="server" ID="CriminalBackgroundCheckRadioButtonList" RepeatDirection="Horizontal"
                                            CellPadding="15" CellSpacing="15" onchange="CriminalBackgroundCheckRadioButtonChanged();" />
                                        <asp:CustomValidator ID="CriminalBackgroundValidator" runat="server" CssClass="error" Enabled="false"
                                            SetFocusOnError="true" ClientValidationFunction="ValidateCriminalCheck" />
                                    </td>
                                    <td id="CriminalBackgroundCheckNoticeToEmployersCell">
                                        <focus:LocalisedLabel runat="server" ID="CriminalBackgroundCheckNoticeToEmployersLabel"
                                            RenderOuterSpan="True" CssClass="information-panel" DefaultText="" />
                                    </td>
                                </tr>
                            </asp:PlaceHolder>
                        </table>
                    </asp:Panel>
                </div>
                <act:CollapsiblePanelExtender ID="BackgroundCheckPanelExtender" runat="server" TargetControlID="BackgroundCheckPanel"
                    ExpandControlID="BackgroundCheckHeaderPanel" CollapseControlID="BackgroundCheckHeaderPanel"
                    Collapsed="false" ImageControlID="BackgroundCheckHeaderImage" SuppressPostBack="true"
                    BehaviorID="BackgroundCheckPanelBehaviour" />
            </asp:PlaceHolder>
            <act:CollapsiblePanelExtender ID="MinimumEducationPanelExtender" runat="server" TargetControlID="MinimumEducationPanel"
                ExpandControlID="MinimumEducationHeaderPanel" CollapseControlID="MinimumEducationHeaderPanel"
                Collapsed="false" ImageControlID="MinimumEducationHeaderImage" SuppressPostBack="true" />
            <asp:Panel ID="WorkWeekHeaderPanel" runat="server" CssClass="singleAccordionTitle">
                <table role="presentation" class="accordionTable">
                    <tr class="multipleAccordionTitle">
                        <td>
                            <asp:Image ID="WorkWeekHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>"
                                AlternateText="." />
                            &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Availablity.Label", "Availability")%></a></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div class="singleAccordionContentWrapper">
                <asp:Panel ID="WorkWeekPanel" runat="server" CssClass="singleAccordionContent">
                    <table role="presentation">
                        <tr>
                            <td>
                                <%= AddRequirementLabel%>
                                <asp:CheckBox ID="HideWorkWeekCheckBox" runat="server" ClientIDMode="Static" />
                            </td>
                        </tr>
                    </table>
                    <table role="presentation">
                        <tr>
                            <td>
                                <%= HtmlLabel("WorkWeek.Label", "Work week")%>&nbsp;&nbsp;
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="WorkWeekDropDown" ClientIDMode="Static" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <act:CollapsiblePanelExtender ID="WorkWeekPanelExtender" runat="server" TargetControlID="WorkWeekPanel"
                ExpandControlID="WorkWeekHeaderPanel" CollapseControlID="WorkWeekHeaderPanel"
                Collapsed="true" ImageControlID="WorkWeekHeaderImage" SuppressPostBack="true" />
            <asp:Panel ID="MinimumExperienceHeaderPanel" runat="server" CssClass="singleAccordionTitle">
                <table role="presentation" class="accordionTable">
                    <tr class="multipleAccordionTitle">
                        <td>
                            <asp:Image ID="MinimumExperienceHeaderImage" runat="server" AlternateText="Minimum Experience Header Image" />
                            &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("MinimumExperience.Label", "Minimum experience")%></a></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div class="singleAccordionContentWrapper">
                <asp:Panel ID="MinimumExperiencePanel" runat="server" CssClass="singleAccordionContent">
                    <table role="presentation">
                        <tr>
                            <td>
                                <%= AddRequirementLabel %>
                                <asp:CheckBox ID="HideExperienceCheckBox" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= HtmlLabel("MinimumExperience.Label", "Experience in years")%>
                            </td>
                            <td>
                                <asp:TextBox ID="MinimumExperienceTextBox" runat="server" Width="20" MaxLength="2"
                                    ClientIDMode="Static" />&nbsp;&nbsp;<asp:DropDownList runat="server" ID="MinimumExperienceMonthsDropDown"
                                        Width="50">
                                    </asp:DropDownList>
                                &nbsp;<%= HtmlLabel("MinimumExperienceMonths.Label", "months")%>
                            </td>
                            <td>
                                <asp:CompareValidator runat="server" Operator="DataTypeCheck" ID="MinimumExperienceValidator"
                                    ControlToValidate="MinimumExperienceTextBox" Type="Integer" SetFocusOnError="True"
                                    CssClass="error" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:RadioButtonList runat="server" ID="MinimumExperienceRadioButtonList" ClientIDMode="Static" />
                                <focus:DependentRequiredFieldValidator ID="MinimumExperiencePreferenceRequired" runat="server"
                                    ControlToValidate="MinimumExperienceRadioButtonList" DependentControl="MinimumExperienceTextBox"
                                    DependentInitialValue="" CssClass="error" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <act:CollapsiblePanelExtender ID="MinimumExperiencePanelExtender" runat="server"
                TargetControlID="MinimumExperiencePanel" ExpandControlID="MinimumExperienceHeaderPanel"
                CollapseControlID="MinimumExperienceHeaderPanel" Collapsed="true" ImageControlID="MinimumExperienceHeaderImage"
                SuppressPostBack="true" />
            <asp:PlaceHolder runat="server" ID="MinimumAgePlaceHolder">
                <asp:Panel ID="MinimumAgeHeaderPanel" runat="server" CssClass="singleAccordionTitle">
                    <table role="presentation" class="accordionTable">
                        <tr class="multipleAccordionTitle">
                            <td>
                                <asp:Image ID="MinimumAgeHeaderImage" runat="server" AlternateText="Minimum Age Header Image" />
                                &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("MinimumAge.Label", "Minimum age")%></a></span>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div class="singleAccordionContentWrapper">
                    <asp:Panel ID="MinimumAgePanel" runat="server" CssClass="singleAccordionContent">
                        <asp:UpdatePanel ID="MinimunAgeUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table role="presentation" style="width: 100%;">
                                    <tr>
                                        <td colspan="2">
                                            <%= AddRequirementLabel%>
                                            <asp:CheckBox ID="HideMinimumAgeCheckBox" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px;">
                                            <%= HtmlLabel(MinimumAgeTextBox, "MinimumAge.Label", "Minimum age")%>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="MinimumAgeTextBox" runat="server" Width="20" MaxLength="2" ClientIDMode="Static" />&nbsp;&nbsp;<i><%= HtmlLocalise("MinimumAge.HelpText", "Must be a legal/statutory requirement for performing this job. This may not be used to indicate a preferred level of experience or maturity.")%></i><asp:CompareValidator
                                                runat="server" Operator="DataTypeCheck" ID="MinimumAgeValidator" ControlToValidate="MinimumAgeTextBox"
                                                Type="Integer" SetFocusOnError="True" CssClass="error" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%= HtmlLabel(MinimumAgeReasonDropDown, "MinimumAgeReason.Label", "Reason")%>
                                        </td>
                                        <td>
                                            <asp:DropDownList runat="server" ID="MinimumAgeReasonDropDown" Width="230" />
                                            <asp:CustomValidator ID="MinimumAgeReasonRequired" runat="server" ControlToValidate="MinimumAgeReasonDropDown"
                                                ValidateEmptyText="True" ClientValidationFunction="validateMinimumAgeReasonDropdown"
                                                SetFocusOnError="true" Display="Dynamic" CssClass="error" />
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="MinimumAgeReasonTextBox" runat="server" Width="250" ClientIDMode="Static"
                                                MaxLength="500" />
                                            <asp:CustomValidator ID="MinimumAgeReasonValidator" runat="server" CssClass="error"
                                                ControlToValidate="MinimumAgeReasonTextBox" ClientValidationFunction="validateMinimumAgeReason"
                                                ValidateEmptyText="true" SetFocusOnError="True" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="MinimumAgeTextBox" EventName="TextChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </asp:Panel>
                </div>
                <act:CollapsiblePanelExtender ID="MinimumAgePanelExtender" runat="server" TargetControlID="MinimumAgePanel"
                    ExpandControlID="MinimumAgeHeaderPanel" CollapseControlID="MinimumAgeHeaderPanel"
                    Collapsed="true" ImageControlID="MinimumAgeHeaderImage" SuppressPostBack="true" />
            </asp:PlaceHolder>
            <asp:Panel ID="DriverLicencesHeaderPanel" runat="server" CssClass="singleAccordionTitle">
                <table role="presentation" class="accordionTable">
                    <tr class="multipleAccordionTitle">
                        <td>
                            <asp:Image ID="DriverLicencesHeaderImage" runat="server" AlternateText="Driver Licences Header Image" />
                            &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("DriverLicences.Label", "Driver’s license")%></a></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div class="singleAccordionContentWrapper">
                <asp:Panel ID="DriverLicencesPanel" runat="server" CssClass="singleAccordionContent">
                    <table role="presentation" style="width: 100%;">
                        <tr>
                            <td colspan="2">
                                <%= AddRequirementLabel%>
                                <asp:CheckBox ID="HideDriversLicenceCheckBox" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">
                                <%= HtmlLabel("DriverLicenceClass.Label", "License class")%>
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="DrivingLicenceClassDropDown" ClientIDMode="Static"
                                    onchange="return DriverLicenseTypeChange();" />
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">
                                <%= HtmlLabel("DriverLicenceEndorsements.Label", "Endorsement type")%>
                            </td>
                            <td>
                                <asp:CheckBoxList runat="server" ID="DrivingLicenceEndorsementsCheckBoxList" ClientIDMode="Static"
                                    RepeatColumns="2" CssClass="checkBoxListTable" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:RadioButtonList runat="server" ID="DrivingLicenceRadioButtonList" ClientIDMode="Static" />
                                <focus:DependentRequiredFieldValidator ID="DrivingLicencePreferenceRequired" runat="server"
                                    ControlToValidate="DrivingLicenceRadioButtonList" DependentControl="DrivingLicenceClassDropDown"
                                    DependentInitialValue="" CssClass="error" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <act:CollapsiblePanelExtender ID="DriverLicencesPanelExtender" runat="server" TargetControlID="DriverLicencesPanel"
                ExpandControlID="DriverLicencesHeaderPanel" CollapseControlID="DriverLicencesHeaderPanel"
                Collapsed="true" ImageControlID="DriverLicencesHeaderImage" SuppressPostBack="true" />
            <asp:Panel ID="LicensesHeaderPanel" runat="server" CssClass="singleAccordionTitle">
                <table role="presentation" class="accordionTable">
                    <tr class="multipleAccordionTitle">
                        <td>
                            <asp:Image ID="LicensesHeaderImage" runat="server" AlternateText="Licenses Header Image" />
                            &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Licenses.Label", "Occupational licenses")%></a></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div class="singleAccordionContentWrapper">
                <asp:Panel ID="LicensesPanel" runat="server" CssClass="singleAccordionContent">
                    <asp:UpdatePanel ID="LicencesUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table role="presentation">
                                <tr>
                                    <td colspan="2">
                                        <%= AddRequirementLabel%>
                                        <asp:CheckBox ID="HideLicencesCheckBox" runat="server" />
                                    </td>
                                </tr>
                                <tr style="vertical-align: top;">
                                    <td style="width: 100px;">
                                        <%= HtmlLocalise("LicenseType.Label", "License type")%>
                                    </td>
                                    <td style="width: 380px;">
                                        <%--<%= HtmlInFieldLabel("LicenceTypeTextBox", "LicenceType.InlineLabel", "enter license", 250)%>--%>
                                        <%--<asp:TextBox ID="LicenceTypeTextBox" runat="server" Width="250" ClientIDMode="Static" AutoCompleteType="Disabled" />
									<act:AutoCompleteExtender ID="LicencesAutoCompleteExtender" runat="server" TargetControlID="LicenceTypeTextBox" MinimumPrefixLength="2" 
																						CompletionListCssClass="autocompleteCompletionList" CompletionInterval="100" 
																						ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetLicences" />--%>
                                        <focus:AutoCompleteTextBox ID="LicenceTypeTextBox" runat="server" ClientIDMode="Static"
                                            Width="250" AutoCompleteType="Disabled" />
                                        <asp:Button ID="AddLicenceButton" runat="server" class="button1" ClientIDMode="Static"
                                            OnClick="AddLicenceButton_Click" CausesValidation="False" /><br />
                                        <asp:CustomValidator ID="LicenceTypeValidator" runat="server" CssClass="error" ControlToValidate="LicenceTypeTextBox"
                                            ValidateEmptyText="true" SetFocusOnError="True" />
                                    </td>
                                    <td>
                                        <uc:UpdateableList ID="LicencesUpdateableList" runat="server" RestrictDuplicates="True" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:RadioButtonList runat="server" ID="LicenceTypeCriteriaRadioButtonList" />
                                        <asp:CustomValidator ID="LicencesUpdateableListValidator" runat="server" CssClass="error"
                                            ControlToValidate="LicenceTypeCriteriaRadioButtonList" ValidateEmptyText="True"
                                            ClientValidationFunction="JobWizardStep4_ValidateLicensesPreference" Display="Dynamic" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
            <act:CollapsiblePanelExtender ID="LicensesPanelExtender" runat="server" TargetControlID="LicensesPanel"
                ExpandControlID="LicensesHeaderPanel" CollapseControlID="LicensesHeaderPanel"
                Collapsed="true" ImageControlID="LicensesHeaderImage" SuppressPostBack="true" />
            <asp:Panel ID="CertiﬁcationsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
                <table role="presentation" class="accordionTable">
                    <tr class="multipleAccordionTitle">
                        <td>
                            <asp:Image ID="CertiﬁcationsHeaderImage" runat="server" AlternateText="Certiﬁcations Header Image"/>
                            &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Certiﬁcations.Label", "Certiﬁcations")%></a></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div class="singleAccordionContentWrapper">
                <asp:Panel ID="CertiﬁcationsPanel" runat="server" CssClass="singleAccordionContent">
                    <asp:UpdatePanel ID="CertiﬁcationsUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table role="presentation">
                                <tr>
                                    <td colspan="2">
                                        <%= AddRequirementLabel%>
                                        <asp:CheckBox ID="HideCertificationsCheckBox" runat="server" />
                                    </td>
                                </tr>
                                <tr style="vertical-align: top;">
                                    <td style="width: 100px;">
                                        <%= HtmlLocalise("CertiﬁcationType.Label", "Certiﬁcation type")%>
                                    </td>
                                    <td style="width: 380px;">
                                        <%= HtmlInFieldLabel("CertiﬁcationTypeTextBox", "CertiﬁcationType.InlineLabel", "enter certiﬁcation", 250)%>
                                        <asp:TextBox ID="CertiﬁcationTypeTextBox" runat="server" Width="250" ClientIDMode="Static"
                                            AutoCompleteType="Disabled" />
                                        <act:AutoCompleteExtender ID="CertiﬁcationTypeAutoCompleteExtender" runat="server"
                                            TargetControlID="CertiﬁcationTypeTextBox" MinimumPrefixLength="2" CompletionListCssClass="autocompleteCompletionList"
                                            CompletionInterval="100" ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetCertifications" />
                                        <asp:Button ID="AddCertiﬁcationButton" runat="server" class="button1" OnClick="AddCertiﬁcationButton_Click"
                                            CausesValidation="False" /><br />
                                        <asp:CustomValidator ID="CertiﬁcationTypeTextBoxValidator" runat="server" CssClass="error"
                                            ControlToValidate="CertiﬁcationTypeTextBox" ValidateEmptyText="true" SetFocusOnError="True" />
                                    </td>
                                    <td>
                                        <uc:UpdateableList ID="CertiﬁcationsUpdateableList" runat="server" RestrictDuplicates="True" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:RadioButtonList runat="server" ID="CertiﬁcationTypeCriteriaRadioButtonList" />
                                        <asp:CustomValidator ID="CertiﬁcationsUpdateableListValidator" runat="server" CssClass="error"
                                            ControlToValidate="CertiﬁcationTypeCriteriaRadioButtonList" ValidateEmptyText="True"
                                            ClientValidationFunction="JobWizardStep4_ValidateCertificationsPreference" Display="Dynamic" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
            <act:CollapsiblePanelExtender ID="CertiﬁcationsPanelExtender" runat="server" TargetControlID="CertiﬁcationsPanel"
                ExpandControlID="CertiﬁcationsHeaderPanel" CollapseControlID="CertiﬁcationsHeaderPanel"
                Collapsed="true" ImageControlID="CertiﬁcationsHeaderImage" SuppressPostBack="true" />
            <asp:Panel ID="LanguagesHeaderPanel" runat="server" CssClass="singleAccordionTitle">
                <table role="presentation" class="accordionTable">
                    <tr class="multipleAccordionTitle">
                        <td>
                            <asp:Image ID="LanguagesHeaderImage" runat="server" AlternateText="Languages Header Image" />
                            &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Languages.Label", "Languages")%></a></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div class="singleAccordionContentWrapper">
                <asp:Panel ID="LanguagesPanel" runat="server" CssClass="singleAccordionContent">
                    <asp:UpdatePanel ID="LanguagesUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table role="presentation">
                                <tr>
                                    <td colspan="2">
                                        <%= AddRequirementLabel%>
                                        <asp:CheckBox ID="HideLanguagesCheckBox" runat="server" />
                                    </td>
                                </tr>
                                <tr style="vertical-align: top;">
                                    <td style="width: 440px;">
                                        <table role="presentation">
                                            <tr style="vertical-align: top;">
                                                <td style="width: 120px;">
                                                    <%= HtmlLocalise("LanguageType.Label", "Language type")%>
                                                </td>
                                                <td>
                                                    <%= HtmlInFieldLabel("LanguageTextBox", "Language.InlineLabel", "enter language", 250)%>
                                                    <asp:TextBox ID="LanguageTextBox" runat="server" Width="254px" ClientIDMode="Static"
                                                        AutoCompleteType="Disabled" />
                                                    <act:AutoCompleteExtender ID="LanguageAutoCompleteExtender" runat="server" TargetControlID="LanguageTextBox"
                                                        MinimumPrefixLength="2" CompletionListCssClass="autocompleteCompletionList" CompletionInterval="100"
                                                        ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetLanguages" />
                                                    <asp:CustomValidator ID="LanguageTextBoxValidator" runat="server" CssClass="error"
                                                        ControlToValidate="LanguageTextBox" ValidateEmptyText="true" SetFocusOnError="True" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="AddLanguageButtonEdu" runat="server" class="button1" OnClick="AddLanguageButton_Click"
                                                        CausesValidation="false" Visible="False" /><br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label runat="server" ID="LanguageProficiencyDropDownLabel" AssociatedControlID="LanguageProficiencyDropDown"><%= HtmlLocalise("LanguageProficiency.Label", "Proficiency")%></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="LanguageProficiencyDropDown" Width="260px" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="AddLanguageButton" runat="server" class="button1" OnClick="AddLanguageButton_Click"
                                                        CausesValidation="false" /><br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td />
                                                <td colspan="2">
                                                    <asp:CustomValidator ID="LanguageProficiencyRequired" runat="server" ControlToValidate="LanguageProficiencyDropDown"
                                                        ValidateEmptyText="True" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <asp:HyperLink runat="server" ID="TestPostDisplay" NavigateUrl="javascript:$find('LanguageProficiencyDefinitionsPopup').show(); "><%= HtmlLocalise("LanguageProficiencyDefinitions.Label", "Language proficiency definitions")%></asp:HyperLink>
                                                    <uc:LanguageProficiencyDefinitionsModal ID="LanguageProficiencies" runat="server" />
                                                </td>
                                        </table>
                                    </td>
                                    <td style="padding-left: 30px;">
                                        <uc:UpdateableLanguagesList ID="LanguagesUpdateableList" runat="server" RestrictDuplicates="True" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:RadioButtonList runat="server" ID="LanguageTypeCriteriaRadioButtonList" />
                                        <asp:CustomValidator ID="LanguagesUpdateableListValidator" runat="server" CssClass="error"
                                            ControlToValidate="LanguageTypeCriteriaRadioButtonList" ValidateEmptyText="True"
                                            ClientValidationFunction="JobWizardStep4_ValidateLanguagesPreference" Display="Dynamic" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="AddLanguageButton" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="AddLanguageButtonEdu" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
            <act:CollapsiblePanelExtender ID="LanguagesPanelExtender" runat="server" TargetControlID="LanguagesPanel"
                ExpandControlID="LanguagesHeaderPanel" CollapseControlID="LanguagesHeaderPanel"
                Collapsed="true" ImageControlID="LanguagesHeaderImage" SuppressPostBack="true" />
            <asp:PlaceHolder runat="server" ID="CareerReadinessPlaceHolder">
                <asp:Panel ID="CareerReadinessHeaderPanel" runat="server" CssClass="singleAccordionTitle">
                    <table role="presentation" class="accordionTable">
                        <tr class="multipleAccordionTitle">
                            <td>
                                <asp:Image ID="CareerReadinessHeaderImage" runat="server" AlternateText="Career Readiness Header Image" />
                                &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("CareerReadiness.Label", "National Career Readiness Certificate&#8482;")%></a></span>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div class="singleAccordionContentWrapper">
                    <asp:Panel ID="CareerReadinessPanel" runat="server" CssClass="singleAccordionContent">
                        <asp:UpdatePanel ID="CareerReadinessUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table role="presentation">
                                    <tr>
                                        <td colspan="2">
                                            <span id="CareerReadinessRequirementLabel">
                                                <%= AddRequirementLabel%></span>
                                            <asp:CheckBox ID="HideCareerReadinessCheckBox" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <%= HtmlLocalise("CareerReadinessCheckBox.Label", "National Career Readiness Certificate - requires at least a Bronze level in three WorkKeys assessments: Applied Mathematics, Locating Information and Reading for Information.")%>
                                        </td>
                                    </tr>
                                    <tr style="vertical-align: top;">
                                        <td style="width: 200px;">
                                            <%= HtmlLabel(CareerReadinessLevelDropdown, "CareerReadinessLevel.Header", "Desired NCRC level")%>
                                        </td>
                                        <td>
                                            <asp:DropDownList runat="server" ID="CareerReadinessLevelDropdown" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:RadioButtonList runat="server" ID="CareerReadinessTypeCriteriaRadioButtonList" />
                                            <focus:DependentRequiredFieldValidator ID="CareerReadinessTypeCriteriaPreferenceRequired"
                                                runat="server" ControlToValidate="CareerReadinessTypeCriteriaRadioButtonList"
                                                DependentControl="CareerReadinessLevelDropdown" DependentInitialValue="" CssClass="error" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                </div>
                <act:CollapsiblePanelExtender ID="CareerReadinessPanelExtender" runat="server" TargetControlID="CareerReadinessPanel"
                    ExpandControlID="CareerReadinessHeaderPanel" CollapseControlID="CareerReadinessHeaderPanel"
                    Collapsed="true" ImageControlID="CareerReadinessHeaderImage" SuppressPostBack="true" />
            </asp:PlaceHolder>
            <asp:Panel ID="SpecialRequirementsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
                <table role="presentation" class="accordionTable">
                    <tr class="multipleAccordionTitle">
                        <td>
                            <asp:Image ID="SpecialRequirementsHeaderImage" runat="server" AlternateText="Special Requirements Header Image" />
                            &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("SpecialRequirements.Label", "Special requirements")%></a></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div class="singleAccordionContentWrapper">
                <asp:Panel ID="SpecialRequirementsPanel" runat="server" CssClass="singleAccordionContent">
                    <table role="presentation">
                        <tr>
                            <td>
                                <%= AddRequirementLabel%>
                                <asp:CheckBox ID="HideSpecialRequirementsCheckBox" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <%= HtmlLocalise("SpecialRequirementsLine1.Label", "You may also specify any other mandatory requirements for your job. Applicants who cannot affirm that they meet the criteria will be automatically disqualified.")%>
                    <br />
                    <%= HtmlLocalise("SpecialRequirementsLine2.Label", "Samples: \"Lift at least 35 pounds\", \"Know C++ and other object-oriented languages.\"")%>
                    <br />
                    <br />
                    <asp:UpdatePanel ID="SpecialRequirementsUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table role="presentation" style="width: 100%;">
                                <asp:Repeater ID="SpecialRequirementsApplicantRepeater" runat="server" OnItemDataBound="SpecialRequirementsApplicantRepeater_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width: 10%;">
                                                <%# Container.ItemIndex + 1 %>.
                                                <%= HtmlLocalise("SpecialRequirementsApplicant.Label", "Applicant must")%>
                                            </td>
                                            <td style="width: 90%;">
                                                <asp:TextBox ID="ApplicantMustTextBox" runat="server" Width="90%" MaxLength="400" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Button ID="AddAnotherRequirementButton" class="button1" runat="server" OnClick="AddAnotherRequirementButton_Click" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
            <act:CollapsiblePanelExtender ID="SpecialRequirementsPanelExtender" runat="server"
                TargetControlID="SpecialRequirementsPanel" ExpandControlID="SpecialRequirementsHeaderPanel"
                CollapseControlID="SpecialRequirementsHeaderPanel" Collapsed="true" ImageControlID="SpecialRequirementsHeaderImage"
                SuppressPostBack="true" />
        </td>
    </tr>
</table>
<script type="text/javascript">

    Sys.Application.add_load(JobWizardStep4_PageLoad);

    function JobWizardStep4_PageLoad() {
        var button = $("#<%= ExpandCollapseAllButton.ClientID %>");
        if (button.length > 0)
            bindExpandCollapseAllPanels('<%= ExpandCollapseAllButton.ClientID %>', null); // bind the magic
    }
		 
</script>
<asp:PlaceHolder runat="server" ID="MinimumAgeValidationScript">
    <script type="text/javascript">
        $(document).ready(function () {
            HandleMinimumAgeReasonDropDown();
            $('#<%=MinimumAgeReasonDropDown.ClientID %>').change(HandleMinimumAgeReasonDropDown);

            HandleMinimumAgeTextBox();
            $('#<%=MinimumAgeTextBox.ClientID %>').blur(HandleMinimumAgeTextBox);
            $('#<%=MinimumAgeTextBox.ClientID %>').keyup(function () {
                if ($('#<%=MinimumAgeTextBox.ClientID %>').val().length > 0) {
                    HandleMinimumAgeTextBox();
                }
            });
        });

        function HandleMinimumAgeReasonDropDown() {
            if ($('#<%=MinimumAgeReasonDropDown.ClientID %>')[0].selectedIndex == 5) {
                document.getElementById("<%=MinimumAgeReasonValidator.ClientID %>").enabled = true;
                $('#<%=MinimumAgeReasonTextBox.ClientID %>').show();
            } else {
                ValidatorEnable($("#<%= MinimumAgeReasonValidator.ClientID %>")[0], false);
                $('#<%=MinimumAgeReasonTextBox.ClientID %>').val("");
                $('#<%=MinimumAgeReasonTextBox.ClientID %>').hide();
            }
        }

        function HandleMinimumAgeTextBox() {
            if ($('#<%=MinimumAgeTextBox.ClientID %>').val().length > 0) {
                $('#<%=MinimumAgeReasonDropDown.ClientID %>').removeAttr('disabled');
            } else {
                $('#<%=MinimumAgeReasonDropDown.ClientID %>').attr('disabled', true);
                $('#<%=MinimumAgeReasonDropDown.ClientID %>')[0].selectedIndex = 0;
                $("#<%=MinimumAgeReasonRequired.ClientID %>").hide();
                HandleMinimumAgeReasonDropDown();
            }
        }

        function validateMinimumAgeReasonDropdown(oSrc, args) {
            var dropDown = $('#<%=MinimumAgeReasonDropDown.ClientID %>');
            if (dropDown.is(":disabled")) {
                args.IsValid = true;
            } else {
                args.IsValid = ($('#<%=MinimumAgeReasonDropDown.ClientID %>').prop("selectedIndex") > 0);
            }
        }

        function validateMinimumAgeReason(oSrc, args) {
            var isValid = true;

            if ($('#<%=MinimumAgeTextBox.ClientID %>').val().trim() != '') {
                if ($('#<%=MinimumAgeReasonDropDown.ClientID %>').val() == 'Other')
                    isValid = ($('#<%=MinimumAgeReasonTextBox.ClientID %>').val().trim() != '');
                else
                    isValid = true;
            }
            args.IsValid = isValid;
        }

    </script>
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="DrivingLicenseScript">
    <script type="text/javascript">
      Sys.Application.add_load(JobWizardStep4_Driving_Licence_PageLoad);

      function JobWizardStep4_Driving_Licence_PageLoad() {
        DriverLicenseTypeChange();
      }
    
      function DriverLicenseTypeChange() {
        var driverLicense = $('#DrivingLicenceClassDropDown');
        var endorsementTypes = $('#DrivingLicenceEndorsementsCheckBoxList');
			
        var driverLicenseValue = $(driverLicense).val();
        var arrEndorsementTypes = $(endorsementTypes).find('input');
			
        var licenceLicenceEndorsements = <%= LicenceEndorsementsJavascriptArray %>;

        var validEndorsements = licenceLicenceEndorsements[driverLicenseValue];

        if (!validEndorsements)
          validEndorsements = '';

        $(arrEndorsementTypes).each(function () {
          if (validEndorsements.indexOf($(this).val()) != -1) {
            $(this).prop('disabled', false).parent().removeAttr('style');
            return true;
          }
          $(this).prop('checked', false);
          $(this).prop('disabled', true).parent().css('color', 'gray');
        });

        return false;
      }
    </script>
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="LanguageScript">
    <script type="text/javascript">
        Sys.Application.add_load(JobWizardStep4_Language_PageLoad);

        function JobWizardStep4_Language_PageLoad() {
            $('#<%=LanguageTextBox.ClientID %>').blur(HandleLanguageTextBox);
            $('#<%=LanguageTextBox.ClientID %>').keyup(HandleLanguageTextBox);
            HandleLanguageTextBox();
        }

        function HandleLanguageTextBox() {
            var dropdown = $('#<%=LanguageProficiencyDropDown.ClientID %>');
            if (dropdown.exists()) {
                if ($('#<%=LanguageTextBox.ClientID %>').val().length > 0) {
                    dropdown.attr('disabled', false);
                } else {
                    dropdown.attr('disabled', true);
                    dropdown[0].selectedIndex = 0;
                }
            }
        }
    </script>
</asp:PlaceHolder>
<script type="text/javascript">
    $(document).ready(function () {
        if ($("#<%=BackgroundCheckHeaderPanel.ClientID %>").exists()) {
            CreditCheckRadioButtonChanged();
            CriminalBackgroundCheckRadioButtonChanged();
        }
    });

    function CreditCheckRadioButtonChanged() {
        var warningLabelCell = $('#TEGLNoticeToEmployersCell');

        var radioList = '<%= CreditCheckRadioButtonList.ClientID%>';

        if ($('#' + radioList + ' input:checked').val() == 'N') {
            // Make warning label hide only when no selected
            warningLabelCell.hide();
        }
        else
            warningLabelCell.show();
    }

    function CriminalBackgroundCheckRadioButtonChanged() {
        var criminalBackgroundWarningLabelCell = $('#CriminalBackgroundCheckNoticeToEmployersCell');

        var criminalBackgroundRadioList = '<%= CriminalBackgroundCheckRadioButtonList.ClientID%>';

        if ($('#' + criminalBackgroundRadioList + ' input:checked').val() == 'N')
        // Make warning label hide only when no selected
            criminalBackgroundWarningLabelCell.hide();
        else
            criminalBackgroundWarningLabelCell.show();
    }

    function ValidateCriminalCheck(src, args) {
        var criminalBackgroundRadioList = '<%= CriminalBackgroundCheckRadioButtonList.ClientID%>';
        if ($('#' + criminalBackgroundRadioList + ' input:checked').val() == 'N' || $('#' + criminalBackgroundRadioList + ' input:checked').val() == 'Y')
            args.IsValid = true;
        else
            args.IsValid = false;

    }

    function ValidateCreditCheck(src, args) {
        var radioList = '<%= CreditCheckRadioButtonList.ClientID%>';
        if ($('#' + radioList + ' input:checked').val() == 'N' || $('#' + radioList + ' input:checked').val() == 'Y')
            args.IsValid = true;
        else
            args.IsValid = false;

    }

    function JobWizardStep4_ValidateLicensesPreference(src, args) {
        var licensesValue = $("#<%=LicencesUpdateableList.ItemCountClientId %>").val();
        args.IsValid = licensesValue.length == 0 || licensesValue == "0" || args.Value.length > 0;
    }

    function JobWizardStep4_ValidateCertificationsPreference(src, args) {
        var certificationsValue = $("#<%=CertiﬁcationsUpdateableList.ItemCountClientId %>").val();
        args.IsValid = certificationsValue.length == 0 || certificationsValue == "0" || args.Value.length > 0;
    }

    function JobWizardStep4_ValidateLanguagesPreference(src, args) {
        var languageValue = $("#<%=LanguagesUpdateableList.ItemListClientId %>").val();
        args.IsValid = languageValue.length == 0 || languageValue == "[]" || args.Value.length > 0;
    }
</script>
