#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;
using Focus.Web.Core.Models;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class EmployerRegistrationStep3 : UserControlBase
	{
		#region Properties

		protected string IsCountyEnabled = "";
		protected string ZIPCodeRequired = "";
		protected string ZIPCodeLimitError = "";

		/// <summary>
		/// Gets or sets the employer id.
		/// </summary>
		/// <value>The employer id.</value>
		public long? EmployerId
		{
			get { return GetViewStateValue<long?>("RegisterStep2:EmployerId"); }
			set { SetViewStateValue("RegisterStep2:EmployerId", value); }
		}

		/// <summary>
		/// Gets or sets the address id.
		/// </summary>
		/// <value>The address id.</value>
		protected long? AddressId
		{
			get { return GetViewStateValue<long?>("RegisterStep2:AddressId"); }
			set { SetViewStateValue("RegisterStep2:AddressId", value); }
		}

		/// <summary>
		/// Gets or sets the business unit id.
		/// </summary>
		/// <value>The business unit id.</value>
		protected long? BusinessUnitId
		{
			get { return GetViewStateValue<long?>("RegisterStep2:BusinessUnitId"); }
			set { SetViewStateValue("RegisterStep2:BusinessUnitId", value); }
		}

		/// <summary>
		/// Gets or sets the company description id.
		/// </summary>
		/// <value>The company description id.</value>
		protected long? CompanyDescriptionId
		{
			get { return GetViewStateValue<long?>("RegisterStep2:CompanyDescriptionId"); }
			set { SetViewStateValue("RegisterStep2:CompanyDescriptionId", value); }
		}

		/// <summary>
		/// Gets or sets the county id.
		/// </summary>
		/// <value>The county id.</value>
		protected long? CountyId
		{
			//get { return GetViewStateValue<long?>("RegisterStep2:CountyId"); }
			//set { SetViewStateValue("RegisterStep2:CountyId", value); }
			get
			{
				if (Session["RegisterStep2:CountyId"].IsNotNull())
				{
					long tmp = 0;
					if (long.TryParse(Session["RegisterStep2:CountyId"].ToString(), out tmp))
					{
						return tmp;
					}
				}

				return null;
			}

			set { Session["RegisterStep2:CountyId"] = value; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is already approved.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is already approved; otherwise, <c>false</c>.
		/// </value>
		protected bool IsAlreadyApproved
		{
			get { return App.GetSessionValue("Registration:IsAlreadyApproved", false); }
			set { App.SetSessionValue("Registration:IsAlreadyApproved", value); }
		}

		/// <summary>
		/// Gets a value indicating whether this instance is workforce.
		/// </summary>
		/// <value>
		/// <c>true</c> if this instance is workforce; otherwise, <c>false</c>.
		/// </value>
		private bool IsWorkforce
		{
			get { return App.Settings.Theme == FocusThemes.Workforce; }
		}

		#endregion


		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				Bind();
				LocaliseUI();
				ContactUrlRegEx.ValidationExpression = App.Settings.UrlRegExPattern;
				EducationAddressPostcodeRegexValidator.ValidationExpression =
				AddressPostcodeRegexValidator.ValidationExpression = App.Settings.ExtendedPostalCodeRegExPattern;
				NoOfEmployeesRow.Visible = NoOfEmployeesRequired.Enabled = App.Settings.ShowEmployeeNumbers;
        StateEmployerIdentificationNumberValidator.ValidationExpression = App.Settings.SEINRegExPattern;
      }

			HandleAccountTypeDisplay();

			FederalEmployerIdentificationNumberRequired.Enabled = (App.Settings.DisplayFEIN == ControlDisplayType.Mandatory);

			IsCountyEnabled = App.Settings.CountyEnabled ? "True" : "False";
		}

    /// <summary>
    /// Handles the PreRender event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_PreRender(object sender, EventArgs e)
    {
      if (App.Settings.SEINMaskPattern.IsNotNullOrEmpty())
      {
        var script = string.Format("$('#StateEmployerIdentificationNumberTextBox').mask('{0}', {{ placeholder: ' ' }});", App.Settings.SEINMaskPattern);
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SEINMaskPattern", script, true);
      }
    }

	  private void HandleAccountTypeDisplay()
	  {
      AccountTypeRequired.Enabled = false;
      if (App.Settings.AccountTypeDisplayType == ControlDisplayType.Mandatory)
      {
        AccountTypeRequired.Enabled = true;
      }
      else if (App.Settings.AccountTypeDisplayType == ControlDisplayType.Hidden)
      {
        AccountTypeRequired.Enabled = false;
        AccountTypeHeader.Visible = false;
        AccountTypeDropDownList.Visible = false;
      }
      if (App.Settings.Theme == FocusThemes.Education)
      {
        AccountTypeRequired.Enabled = false;
        AccountTypeHeader.Visible = false;
        AccountTypeDropDownList.Visible = false;
      }
	  }

		/// <summary>
		/// Clears the step
		/// </summary>
		public void ClearStep()
		{
			LegalNameTextBox.Text =
				CompanyNameTextBox.Text =
				EducationAddressPostcodeZipTextBox.Text =
				AddressLine1TextBox.Text =
				AddressLine2TextBox.Text =
				AddressPostcodeZipTextBox.Text =
				AddressTownCityTextBox.Text =
				ContactUrlTextBox.Text = 
				ContactPhoneTextBox.Text = 
				ExtensionTextBox.Text = 
				ContactAlternatePhoneTextBox.Text =
				ContactAlternate2PhoneTextBox.Text = 
				FederalEmployerIdentificationNumberTextBox.Text = 
				StateEmployerIdentificationNumberTextBox.Text = 
				IndustrialClassificationTextBox.Text = 
				CompanyDescriptionTextBox.Text = "";

			AddressStateDropDownList.SelectedIndex =
				AddressCountyDropDownList.SelectedIndex =
				AddressCountryDropDownList.SelectedIndex =
				PhoneTypeDropDownList.SelectedIndex =
				AltPhoneTypeDropDownList.SelectedIndex =
				AltPhoneType2DropDownList.SelectedIndex =
				OwnershipTypeDropDownList.SelectedIndex =
				AccountTypeDropDownList.SelectedIndex =
				NoOfEmployeesDropDownList.SelectedIndex = 0;

			PublicTransitAccessible.Checked = false;
		}

	  /// <summary>
		/// Initialises the step with the specified FederalEmployerIdentificationNumber
		/// </summary>
		/// <param name="validatedEmployer">The validated employer.</param>
		/// <param name="federalEmployerIdentificationNumber">The federal employer identification number.</param>
		internal void InitialiseStep(ValidatedEmployerView validatedEmployer, string federalEmployerIdentificationNumber)
		{
			if (validatedEmployer.IsNull())
				validatedEmployer = new ValidatedEmployerView();

	    var hasFEIN = App.Settings.DisplayFEIN == ControlDisplayType.Mandatory || (App.Settings.DisplayFEIN == ControlDisplayType.Optional && federalEmployerIdentificationNumber.IsNotNullOrEmpty());
      if (hasFEIN)
			{
				FEINExistsInstructionsLabel.Text = CodeLocalise("EmployerWasFound.Label",
																												"Based on the Federal Employer ID Number you entered ({0}), our records indicate that neither your #BUSINESS#:LOWER nor any of its business units or hiring managers have registered with our system previously.",
																												federalEmployerIdentificationNumber);

				ExtraInstructionsList.Visible = true;

				var listItems = new List<string>
        {
          CodeLocalise("EmployerWasNotFound.Item1", "If you have entered an incorrect FEIN, please click \"Previous step\" to edit."),
          App.Settings.SupportDaysResponse == 1 
            ? CodeLocalise("EmployerWasNotFound.Item2b", "If you are a new or an out-of-state #BUSINESS#:LOWER, our staff must confirm your FEIN to approve your registration. This normally takes 1 business day.")
            : CodeLocalise("EmployerWasNotFound.Item2a", "If you are a new or an out-of-state #BUSINESS#:LOWER, our staff must confirm your FEIN to approve your registration. This normally takes {0} business days.", App.Settings.SupportDaysResponse),
          CodeLocalise("EmployerWasNotFound.Item3", "If you need help completing your registration, contact our support team at #SUPPORTEMAIL# or #SUPPORTPHONE#. Our business hours are #SUPPORTHOURSOFBUSINESS#", federalEmployerIdentificationNumber)
        };
				ExtraInstructionsListItems.DataSource = listItems;
				ExtraInstructionsListItems.DataBind();
			}
			else
			{
				FEINExistsInstructionsLabel.Text = CodeLocalise("FindCompanyHelpText.Label",
																												"Please enter your #BUSINESS#:LOWER address details below. To check your #BUSINESS#:LOWER already exists in our system, enter your zipcode and click Find #BUSINESS#.");

				ExtraInstructionsList.Visible = false;
			}

      FederalEmployerIdentificationNumberLabel.Visible =
	      FederalEmployerIdentificationNumberTextBox.Visible =
	      FederalEmployerIdentificationNumberRequired.Visible = hasFEIN;

	    StateEmployerIdentificationNumberTextBox.Visible =
	      StateEmployerIdentificationLabel.Visible =
	      IndustrialClassificationLabel.Visible =
	      IndustrialClassificationTextBox.Visible =
	      IndustrialClassificationRequired.Visible =
	      WorkforceZipCodePlaceHolder.Visible =
	      CountyRowPlaceHolder.Visible =
	      OwnershipTypePlaceHolder.Visible =
	      LegalNamePlaceHolder.Visible = IsWorkforce;

			EducationZipCodePlaceHolder.Visible = !IsWorkforce;

      if (!IsWorkforce)
      {
        CheckCompanyButton.Visible =
          EducationAddressPostcodeZipOnCheckRequired.Visible = !hasFEIN;
      }

			// Employer
			EmployerId = validatedEmployer.Id;
			CompanyNameTextBox.Text = validatedEmployer.Name;
		  LegalNameTextBox.Text = validatedEmployer.LegalName;

			FederalEmployerIdentificationNumberTextBox.Text = federalEmployerIdentificationNumber;
			StateEmployerIdentificationNumberTextBox.Text = validatedEmployer.StateEmployerIdentificationNumber;

		  if (validatedEmployer.OwnershipTypeId.IsNotNull())
			  OwnershipTypeDropDownList.SelectValue(validatedEmployer.OwnershipTypeId.ToString());
		  else
			  OwnershipTypeDropDownList.SelectedIndex = 0;

		  if (validatedEmployer.AccountTypeId.IsNotNull())
			  AccountTypeDropDownList.SelectValue(validatedEmployer.AccountTypeId.ToString());
		  else
			  AccountTypeDropDownList.SelectedIndex = 0;

			IndustrialClassificationTextBox.Text = validatedEmployer.IndustrialClassification;
			ContactUrlTextBox.Text = validatedEmployer.Url;
			ContactPhoneTextBox.Text = validatedEmployer.PrimaryPhone;
			ExtensionTextBox.Text = validatedEmployer.PrimaryPhoneExtension;
			PhoneTypeDropDownList.SelectedValue = validatedEmployer.PrimaryPhoneType;
			ContactAlternatePhoneTextBox.Text = validatedEmployer.AlternatePhone1;
			AltPhoneTypeDropDownList.SelectedValue = validatedEmployer.AlternatePhone1Type;
			ContactAlternate2PhoneTextBox.Text = validatedEmployer.AlternatePhone2;
			AltPhoneType2DropDownList.SelectedValue = validatedEmployer.AlternatePhone2Type;

			// Employer Address
			AddressId = validatedEmployer.AddressId;

			AddressLine1TextBox.Text = validatedEmployer.AddressLine1;
			AddressLine2TextBox.Text = validatedEmployer.AddressLine2;
			AddressTownCityTextBox.Text = validatedEmployer.AddressTownCity;

			AddressStateDropDownList.SelectValue(validatedEmployer.AddressStateId.IsNotNull()
																						 ? validatedEmployer.AddressStateId.ToString()
																						 : ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(
																							 x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).
																								 SingleOrDefault().ToString());
			AddressCountyDropDownList.SelectValue(validatedEmployer.AddressCountyId.IsNotNull()
				? validatedEmployer.AddressCountyId.ToString()
				: CountyId.HasValue ? CountyId.ToString() : string.Empty);

			//CountyId = validatedEmployer.AddressCountyId;

			AddressCountyDropDownListCascadingDropDown.PromptText = CodeLocalise("Global.County.TopDefault",
																																					 "- select county -");
			AddressCountyDropDownListCascadingDropDown.LoadingText = CodeLocalise("Global.County.Progress",
																																						"[Loading counties ...]");
			AddressCountyDropDownListCascadingDropDown.PromptValue = string.Empty;

			if (validatedEmployer.AddressCountyId.HasValue)
				AddressCountyDropDownListCascadingDropDown.SelectedValue = validatedEmployer.AddressCountyId.Value.ToString();

			if (IsWorkforce) AddressPostcodeZipTextBox.Text = validatedEmployer.AddressPostcodeZip;
			else EducationAddressPostcodeZipTextBox.Text = validatedEmployer.AddressPostcodeZip;

			AddressCountryDropDownList.SelectValue(validatedEmployer.AddressCountryId.IsNotNull()
																							 ? validatedEmployer.AddressCountryId.ToString()
																							 : ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries).Where(
																								 x => x.Key == "Country.US").Select(x => x.Id).SingleOrDefault().
																									 ToString());

			// Employer Business Unit
			BusinessUnitId = validatedEmployer.BusinessUnitId;

			// Employer Description
			CompanyDescriptionId = validatedEmployer.DescriptionId;
			CompanyDescriptionTextBox.Text = validatedEmployer.Description;
		}

		/// <summary>
		/// Updates the model.
		/// </summary>
		/// <param name="model">The model.</param>
		internal void UpdateModel(TalentRegistrationModel model)
		{
			model.Employer = new EmployerDto
												 {
													 Id = EmployerId,
													 Name = CompanyNameTextBox.TextTrimmed(),
                           LegalName = LegalNameTextBox.TextTrimmed(),
													 FederalEmployerIdentificationNumber =
														 FederalEmployerIdentificationNumberTextBox.TextTrimmed(),
													 StateEmployerIdentificationNumber = StateEmployerIdentificationNumberTextBox.TextTrimmed(),
													 Url = ContactUrlTextBox.TextTrimmed(defaultValue: null),
													 OwnershipTypeId = OwnershipTypeDropDownList.SelectedValueToLong(),
													 AccountTypeId = AccountTypeDropDownList.SelectedValueToNullableLong(),
													 PrimaryPhone = ContactPhoneTextBox.TextTrimmed(),
													 PrimaryPhoneExtension = ExtensionTextBox.TextTrimmed(),
													 PrimaryPhoneType = PhoneTypeDropDownList.SelectedValue,
													 AlternatePhone1 = ContactAlternatePhoneTextBox.TextTrimmed(),
													 AlternatePhone1Type = AltPhoneTypeDropDownList.SelectedValue,
													 AlternatePhone2 = ContactAlternate2PhoneTextBox.TextTrimmed(),
													 AlternatePhone2Type = AltPhoneType2DropDownList.SelectedValue,
													 IndustrialClassification = IndustrialClassificationTextBox.TextTrimmed(),
													 NoOfEmployees = NoOfEmployeesDropDownList.SelectedValueToLong()
												 };

			if (App.Settings.Module == FocusModules.Assist || App.Settings.NewEmployerApproval == TalentApprovalOptions.NoApproval)
				IsAlreadyApproved = true;

			if (IsAlreadyApproved)
				model.Employer.ApprovalStatus = ApprovalStatuses.Approved;

			model.EmployerAddress = new EmployerAddressDto
																{
																	Id = AddressId,
																	Line1 = AddressLine1TextBox.TextTrimmed(),
																	Line2 = AddressLine2TextBox.TextTrimmed(),
																	TownCity = AddressTownCityTextBox.TextTrimmed(),
																	PostcodeZip =
																		IsWorkforce
																			? AddressPostcodeZipTextBox.TextTrimmed()
																			: EducationAddressPostcodeZipTextBox.TextTrimmed(),
																	StateId = AddressStateDropDownList.SelectedValueToLong(),
																	CountyId =
																		AddressCountyDropDownList.SelectedValueToLong() == 0
																			? CountyId == 0 ? (long?)null : CountyId
																			: AddressCountyDropDownList.SelectedValueToLong(),
																	CountryId = AddressCountryDropDownList.SelectedValueToLong(),
																	IsPrimary = true,
																	EmployerId = (EmployerId ?? 0),
																	PublicTransitAccessible = PublicTransitAccessible.Checked
																};


			model.BusinessUnit = new BusinessUnitDto
														 {
															 Id = BusinessUnitId,
															 EmployerId = (EmployerId ?? 0),
															 Name = CompanyNameTextBox.TextTrimmed(),
                               LegalName = LegalNameTextBox.TextTrimmed(),
															 Url = ContactUrlTextBox.TextTrimmed(defaultValue: null),
															 OwnershipTypeId = OwnershipTypeDropDownList.SelectedValueToLong(),
															 AccountTypeId = AccountTypeDropDownList.SelectedValueToNullableLong(),
															 NoOfEmployees = NoOfEmployeesDropDownList.SelectedValueToLong(),
															 PrimaryPhone = ContactPhoneTextBox.TextTrimmed(),
															 PrimaryPhoneExtension = ExtensionTextBox.TextTrimmed(),
															 PrimaryPhoneType = PhoneTypeDropDownList.SelectedValue,
															 AlternatePhone1 = ContactAlternatePhoneTextBox.TextTrimmed(),
															 AlternatePhone1Type = AltPhoneTypeDropDownList.SelectedValue,
															 AlternatePhone2 = ContactAlternate2PhoneTextBox.TextTrimmed(),
															 AlternatePhone2Type = AltPhoneType2DropDownList.SelectedValue,
															 IndustrialClassification = IndustrialClassificationTextBox.TextTrimmed()
														 };

			model.BusinessUnitDescription = new BusinessUnitDescriptionDto
																				{
																					Id = CompanyDescriptionId,
																					Title = CodeLocalise("CompanyDescription", "#BUSINESS# description"),
																					Description = CompanyDescriptionTextBox.TextTrimmed().RemoveRogueCharacters()
																				};

			model.BusinessUnitAddress = new BusinessUnitAddressDto
																		{
																			Id = AddressId,
																			Line1 = AddressLine1TextBox.TextTrimmed(),
																			Line2 = AddressLine2TextBox.TextTrimmed(),
																			TownCity = AddressTownCityTextBox.TextTrimmed(),
																			PostcodeZip =
																				IsWorkforce
																					? AddressPostcodeZipTextBox.TextTrimmed()
																					: EducationAddressPostcodeZipTextBox.TextTrimmed(),
																			StateId = AddressStateDropDownList.SelectedValueToLong(),
																			CountyId =
																				AddressCountyDropDownList.SelectedValueToLong() == 0
																					? CountyId == 0 ? (long?)null : CountyId
																					: AddressCountyDropDownList.SelectedValueToLong(),
																			CountryId = AddressCountryDropDownList.SelectedValueToLong(),
																			IsPrimary = true,
																			BusinessUnitId = (EmployerId ?? 0),
																			PublicTransitAccessible = PublicTransitAccessible.Checked
																		};

			if (EmployerId.GetValueOrDefault() == 0)
			{
				// New employer so set description and BU to primary
				model.BusinessUnit.IsPrimary = true;
				model.BusinessUnitDescription.IsPrimary = true;
			}
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			// Ownership
			OwnershipTypeDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.OwnershipTypes), null,
																					 CodeLocalise("Global.OwnershipType.TopDefault", "- select ownership -"));

			// State
			AddressStateDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States), null,
																					CodeLocalise("Global.State.TopDefault", "- select state -"));

			// County
			AddressCountyDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Counties), null,
																					 CodeLocalise("Global.County.TopDefault", "- select county -"));

			// Country
			AddressCountryDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries), null,
																						CodeLocalise("Global.Country.TopDefault", "- select country -"));

			// Phone
			BindContactTypeDropDown(PhoneTypeDropDownList);
			BindContactTypeDropDown(AltPhoneTypeDropDownList);
			BindContactTypeDropDown(AltPhoneType2DropDownList);

			// Account type
			AccountTypeDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.AccountTypes), null,
																					CodeLocalise("Global.AccountType.TopDefault", "- select account type -"));

			// No Of Employees
			BindNoOfEmployeesDropdown(NoOfEmployeesDropDownList);
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			CompanyNameRequired.ErrorMessage = CodeLocalise("CompanyName.RequiredErrorMessage", "#BUSINESS# name is required.");
			LegalNameRequired.ErrorMessage = CodeLocalise("LegalName.RequiredErrorMessage", "#BUSINESS# legal name is required.");
      FederalEmployerIdentificationNumberLabel.Text = HtmlRequiredLabel(FederalEmployerIdentificationNumberTextBox, "FederalEmployerIdentificationNumber.Label", "Federal Employer ID");
      FederalEmployerIdentificationNumberRequired.ErrorMessage = CodeLocalise("FederalEmployerIdentificationNumber.RequiredErrorMessage", "Federal Employer ID is required.");
      StateEmployerIdentificationNumberValidator.ErrorMessage = App.Settings.SEINRegExPattern == "^\\d*$"
        ? CodeLocalise("StateEmployerIdentificationNumberValidator.NumericErrorMessage", "State Employer ID must consist of only numbers.")
        : CodeLocalise("StateEmployerIdentificationNumberValidator.ErrorMessage", "State Employer ID is not in the correct format.");

      OwnershipTypeRequired.ErrorMessage = CodeLocalise("OwnershipType.RequiredErrorMessage", "Ownership type is required.");

      StateEmployerIdentificationLabel.Text = HtmlLabel(StateEmployerIdentificationNumberTextBox,"StateEmployerIdentificationNumber.Label", "State Employer ID");

			var defaultState = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States, 0, App.Settings.DefaultStateKey).FirstOrDefault();
			var defaultTooltipText = defaultState.IsNotNull() 
				? string.Format("If you have an SEIN that is not for the State of {0}, you are not required to provide it", defaultState.Text)
				: "If you have an SEIN that is not for the state, you are not required to provide it";
			StateEmployerIdentificationToolTip.Text = HtmlTooltipster("tooltipWithArrow", "SEIN.Tooltip", defaultTooltipText);

      IndustrialClassificationLabel.Text = HtmlRequiredLabel(IndustrialClassificationTextBox, "IndustrialClassification.Label", "Industry classification");

			if (App.Settings.AccountTypeDisplayType == ControlDisplayType.Mandatory)
			{
				AccountTypeHeader.Text = HtmlRequiredLabel(AccountTypeDropDownList, "AccountType.Label", "Account Type");
				AccountTypeRequired.ErrorMessage = CodeLocalise("AccountType.RequiredErrorMessage", "Account type is required.");
			}
			else
			{
				AccountTypeHeader.Text = HtmlLabel(AccountTypeDropDownList, "AccountType.Label", "Account Type");
			}

			NoOfEmployeesRequired.ErrorMessage = CodeLocalise("NoOfEmployees.RequiredErrorMessage", "Number Of Employees is required.");

			AddressLine1Required.ErrorMessage = CodeLocalise("AddressLine1.RequiredErrorMessage",
																											 "Address (line 1) is required.");
			IndustrialClassificationRequired.ErrorMessage = CodeLocalise("IndustrialClassification.RequiredErrorMessage",
																																	 "Industrial classification is required.");
			AddressTownCityRequired.ErrorMessage = CodeLocalise("AddressTownCity.RequiredErrorMessage", "City is required.");
			AddressStateRequired.ErrorMessage = CodeLocalise("AddressState.RequiredErrorMessage", "State is required.");
			AddressCountyRequired.ErrorMessage = CodeLocalise("AddressCounty.RequiredErrorMessage", "County is required.");
			AddressPostcodeZipRequired.ErrorMessage =
			EducationAddressPostcodeZipRequired.ErrorMessage =
			EducationAddressPostcodeZipOnCheckRequired.ErrorMessage = CodeLocalise("AddressPostcodeZip.RequiredErrorMessage", "ZIP or postal code is required.");

			AddressPostcodeRegexValidator.ErrorMessage =
			EducationAddressPostcodeRegexValidator.ErrorMessage = CodeLocalise("AddressPostcodeRegexValidator.ErrorMessage", "ZIP code must be in a 5 or 9-digit format");

			AddressCountryRequired.ErrorMessage = CodeLocalise("AddressCountry.RequiredErrorMessage", "Country is required.");
			ContactPhoneRequired.ErrorMessage = CodeLocalise("ContactPhone.RequiredErrorMessage", "Phone number is required.");
			IndustrialClassificationValidator.ErrorMessage = CodeLocalise("IndustrialClassification.NotFoundErrorMessage",
																																		"Industry classification (NAICS) not found.");
			ContactUrlRegEx.ErrorMessage = CodeLocalise("ContactUrlRegEx.ErrorMessage",
																									"Invalid url. Please ensure http:// or https:// is present, as appropriate to your url.");
			CheckCompanyButton.Text = CodeLocalise("CheckCompanyButton.Button", "Find #BUSINESS#:LOWER");

		  if (!IsWorkforce)
		  {
				UseThisBusinessUnitButton.Text = CodeLocalise("UseThisCompany.Button", "Use this #BUSINESS#:LOWER");
				CreateNewBusinessUnitButton.Text = CodeLocalise("CreateNewCompanyButton.Button", "Create new #BUSINESS#:LOWER");
		    BusinessUnitSelectedValidator.ErrorMessage = CodeLocalise("ChooseCompany.ErrorMessage",
																																	"Please choose a #BUSINESS#:LOWER or choose to create a new one.");
		    BusinessUnitNotFoundButton.Text = CodeLocalise("Continue.Button", "Continue");
				NoBusinessUnitsFoundLiteral.Text = CodeLocalise("NoCompaniesFound.Text", "There are no matching #BUSINESSES#:LOWER found in the application");
		  }

		  CompanyDescriptionLengthValidator.ErrorMessage = CodeLocalise("CompanyDescriptionLengthValidator.ErrorMessage", "Description must be 2000 characters or less");
		}

		/// <summary>
		/// Binds the contact type drop down.
		/// </summary>
		/// <param name="dropDownList">The drop down list.</param>
		private void BindContactTypeDropDown(DropDownList dropDownList)
		{
			dropDownList.Items.Clear();

			dropDownList.Items.AddEnum(PhoneTypes.Phone, "landline");
			dropDownList.Items.AddEnum(PhoneTypes.Mobile, "mobile");
			dropDownList.Items.AddEnum(PhoneTypes.Fax, "fax");
			dropDownList.Items.AddEnum(PhoneTypes.Other, "other");
		}

		/// <summary>
		/// Binds the no of employees dropdown.
		/// </summary>
		/// <param name="dropdownlist">The dropdownlist.</param>
		private void BindNoOfEmployeesDropdown(ListControl dropdownlist)
		{
			dropdownlist.Items.Clear();
			dropdownlist.Items.AddLocalised("Global.NoOfEmployees.TopDefault", "- select number of employees -", "");
			dropdownlist.Items.AddLocalised("NoOfEmployeesRange1.Text", "1-49", "1");
			dropdownlist.Items.AddLocalised("NoOfEmployeesRange2.Text", "50-99", "2");
			dropdownlist.Items.AddLocalised("NoOfEmployeesRange3.Text", "100-499", "3");
			dropdownlist.Items.AddLocalised("NoOfEmployeesRange4.Text", "500-999", "4");
			dropdownlist.Items.AddLocalised("NoOfEmployeesRange5.Text", "1000-4999", "5");
			dropdownlist.Items.AddLocalised("NoOfEmployeesRange6.Text", "5000+", "6");
		}

		#region Find existing employer

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="eventArgs">The <see cref="EmployerChosenEventArgs"/> instance containing the event data.</param>
		public delegate void ExistingEmployerChosenHandler(object sender, EmployerChosenEventArgs eventArgs);
		/// <summary>
		/// Occurs when [employer chosen].
		/// </summary>
		public event ExistingEmployerChosenHandler EmployerChosen;

		/// <summary>
		/// Handles the OnClick event of the CheckCompanyButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void CheckBusinessUnitButton_OnClick(object sender, EventArgs e)
		{
			// Get data, if any and show modal
			BindCompanyList();
			CheckBusinessUnitPlaceHolder.Visible = true;
			CheckBusinessUnitModalPopup.Show();
			CheckBusinessUnitModalPanel.CssClass = "modal";
		}

		/// <summary>
		/// Handles the LayoutCreated event of the CompanyListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void BusinessUnitListView_LayoutCreated(object sender, EventArgs e)
		{
			// Set table headings
			((Literal)BusinessUnitListView.FindControl("BusinessUnitHeader")).Text = CodeLocalise("Company.Header", "#BUSINESS#");
			((Literal)BusinessUnitListView.FindControl("AddressHeader")).Text = CodeLocalise("Address.Header", "Address");
			((Literal)BusinessUnitListView.FindControl("ZipHeader")).Text = CodeLocalise("Zip.Header", "Zip code");
		}

		/// <summary>
		/// Handles the Click event of the UseThisCompanyButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void UseThisBusinessUnitButton_Click(object sender, EventArgs e)
		{
			// Get the chosen employer
			var employerId = (from company in BusinessUnitListView.Items
												let radioButton = (RadioButton)company.FindControl("BusinessUnitList")
												where radioButton.Checked
												select company).Select(item => BusinessUnitListView.DataKeys[item.DisplayIndex]["EmployerId"].AsLong()).
				SingleOrDefault();

			// Get the chosen employer
			var businessUnitId = (from company in BusinessUnitListView.Items
														let radioButton = (RadioButton)company.FindControl("BusinessUnitList")
														where radioButton.Checked
														select company).Select(item => BusinessUnitListView.DataKeys[item.DisplayIndex]["Id"].AsLong()).
				SingleOrDefault();

			// Trigger event
			OnEmployerChosen(new EmployerChosenEventArgs { CommandArgument = new EmployerChosenCommandArgument { EmployerId = employerId, BusinessUnitId = businessUnitId } });

			// Tidy up
			HideCheckBusinessUnitModal();

		}

		/// <summary>
		/// Handles the Click event of the CreateNewEmployerButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void CreateNewBusinessUnitButton_Click(object sender, EventArgs e)
		{
			// Simply hide the modal and let the user continue
			HideCheckBusinessUnitModal();
		}

		/// <summary>
		/// Hides the check company modal.
		/// </summary>
		private void HideCheckBusinessUnitModal()
		{
			CheckBusinessUnitPlaceHolder.Visible = false;
			CheckBusinessUnitModalPopup.Hide();
			CheckBusinessUnitModalPanel.CssClass = "modal hidden";
		}

		/// <summary>
		/// Binds the company list.
		/// </summary>
		private void BindCompanyList()
		{
			// Get the search results
			var businessUnits = ServiceClientLocator.EmployerClient(App).SearchBusinessUnits(EducationAddressPostcodeZipTextBox.TextTrimmed());

			// Companies found so show and bind list
			if (businessUnits.IsNotNullOrEmpty())
			{
				NoBusinessUnitsFoundPlaceHolder.Visible = false;
				CompaniesFoundPlaceHolder.Visible = true;
				BusinessUnitListView.DataSource = businessUnits;
				BusinessUnitListView.DataBind();
			}
			else
			{
				// No companies found so show correct panel
				NoBusinessUnitsFoundPlaceHolder.Visible = true;
				CompaniesFoundPlaceHolder.Visible = false;
			}
		}

		/// <summary>
		/// Raises the <see cref="E:EmployerChosen" /> event.
		/// </summary>
		/// <param name="eventArgs">The <see cref="EmployerChosenEventArgs"/> instance containing the event data.</param>
		protected virtual void OnEmployerChosen(EmployerChosenEventArgs eventArgs)
		{
			if (EmployerChosen != null)
				EmployerChosen(this, eventArgs);
		}


		#endregion
	}

	[Serializable]
	public class EmployerChosenCommandArgument
	{
		public long EmployerId { get; set; }
		public long BusinessUnitId { get; set; }
	}
	[Serializable]
	public class EmployerChosenEventArgs
	{
		public string CommandName { get; set; }
		public EmployerChosenCommandArgument CommandArgument { get; set; }
	}
}