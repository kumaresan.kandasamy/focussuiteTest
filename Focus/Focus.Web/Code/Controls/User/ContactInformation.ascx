﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactInformation.ascx.cs" Inherits="Focus.Web.Code.Controls.User.ContactInformation" %>

<table style="width:100%;" role="presentation" >
	<tr runat="server" id="BusinessUnitNameRow">
		<td style="width:250px;"><%= HtmlLabel("BusinessUnit.Label", "Business unit") %></td>
		<td><asp:Label runat="server" ID="BusinessUnitNameLabel"/></td>
	</tr>
	<tr>
		<td style="width:250px;"><%= HtmlRequiredLabel(FirstNameTextBox, "FirstName.Label", "First name") %></td>
		<td>
			<asp:TextBox ID="FirstNameTextBox" runat="server" TabIndex="1" MaxLength="50" />
			<asp:RequiredFieldValidator ID="FirstNameRequired" runat="server" ControlToValidate="FirstNameTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ContactInformation"/>
		</td>
	</tr>
	<tr>
		<td><%= HtmlRequiredLabel(LastNameTextBox, "LastName.Label", "Last name")%></td>
		<td>
			<asp:TextBox ID="LastNameTextBox" runat="server" TabIndex="2" MaxLength="50" />
			<asp:RequiredFieldValidator ID="LastNameRequired" runat="server" ControlToValidate="LastNameTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error"  ValidationGroup="ContactInformation"/>
		</td>
	</tr>
	<tr>
		<td><%= HtmlLabel(MiddleInitialTextBox,"MiddleInitial.Label", "Middle initial")%></td>
		<td>
		  <asp:TextBox ID="MiddleInitialTextBox" runat="server"  Width="95px" TabIndex="3" MaxLength="5"/>
		</td>
	</tr>
	<tr>
		<td><%= HtmlRequiredLabel(PersonalTitleDropDown, "PersonalTitle.Label", "Title")%></td>
		<td>
			<asp:DropDownList ID="PersonalTitleDropDown" runat="server" Width="100px" TabIndex="4" />
			<asp:RequiredFieldValidator ID="PersonalTitleRequired" runat="server" ControlToValidate="PersonalTitleDropDown" SetFocusOnError="true" Display="Dynamic" CssClass="error"  ValidationGroup="ContactInformation"/>
		</td>
	</tr>
	<tr id="SuffixRow" runat="server">
		<td><%= HtmlLabel(SuffixDropDown, "SuffixDropDown.Label", "Suffix")%></td>
		<td>
			<asp:DropDownList ID="SuffixDropDown" runat="server" Width="150px" TabIndex="4" />
		</td>
	</tr>
	<tr>
		<td><asp:Literal runat="server" ID="JobTitleHeader"></asp:Literal></td>
		<td>
		  <asp:TextBox ID="JobTitleTextBox" runat="server" TabIndex="5" MaxLength="200" Width="75%"/>
 			<asp:RequiredFieldValidator ID="JobTitleRequired" runat="server" ControlToValidate="JobTitleTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error"  ValidationGroup="ContactInformation"/>
		</td>
	</tr>
  	<tr id=ExternalIdRow runat="server">
		<td><% = HtmlLabel(ExternalIdTextBox, "ExternalId.Label", "External Id")%></td>
        <td>
		  <asp:TextBox ID="ExternalIdTextBox" runat="server" TabIndex="5" MaxLength="200" Width="75%"/>
 		</td>
	</tr>
</table>
<table role="presentation" style="width:100%;">
	<tr>
		<td style="width:250px;"><%= HtmlLabel(AddressLine1TextBox,"AddressLine1.Label", "Address Line 1")%></td>
		<td>
			<asp:TextBox ID="AddressLine1TextBox" runat="server"  Width="75%" TabIndex="5" MaxLength="200" />
		</td>
	</tr>
	<tr>
		<td><%= HtmlLabel(AddressLine2TextBox,"AddressLine2.Label", "Address Line 2")%></td>
		<td>
			<asp:TextBox ID="AddressLine2TextBox" runat="server" Width="75%" TabIndex="6" MaxLength="200" />
		</td>
	</tr>
	<tr>
		<td><%= HtmlLabel(AddressPostcodeZipTextBox,"AddressPostcodeZip.Label", "ZIP or postal code")%></td>
		<td>
			<asp:TextBox ID="AddressPostcodeZipTextBox" runat="server" Width="90px" TabIndex="7" MaxLength="10" />
			<asp:RegularExpressionValidator ID="AddressPostcodeRegexValidator" runat="server" CssClass="error" ControlToValidate="AddressPostcodeZipTextBox" ValidationGroup="ContactInformation" SetFocusOnError="true" Display="Dynamic" />
		</td>
	</tr>
	<tr>
		<td><%= HtmlLabel(AddressTownCityTextBox,"AddressTownCity.Label", "City")%></td>
		<td>
			<asp:TextBox ID="AddressTownCityTextBox" runat="server" Width="50%" TabIndex="8" MaxLength="100" />
		</td>
	</tr>
	<tr>
		<td><%= HtmlLabel(AddressStateDropDownList,"AddressState.Label", "State")%></td>
		<td>
			<asp:DropDownList ID="AddressStateDropDownList" runat="server" Width="75%" TabIndex="9" ClientIDMode="Static" onchange="UpdateCountry();"/>
		</td>
	</tr>
      <asp:PlaceHolder runat="server" ID="CountyRowPlaceHolder">
	<tr>
		<td><%= HtmlLabel(AddressCountyDropDownList,"AddressCounty.Label", "County")%></td>
		<td>
      <focus:AjaxDropDownList ID="AddressCountyDropDownList" runat="server"  Width="75%" TabIndex="10" ClientIDMode="Static" onchange="UpdateStateCountry()"/>
			<act:CascadingDropDown ID="AddressCountyDropDownListCascadingDropDown" runat="server" TargetControlID="AddressCountyDropDownList" ParentControlID="AddressStateDropDownList" 
															ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetCounties" Category="Counties" BehaviorID="AddressCountyDropDownListCascadingDropDown" />
		</td>
	</tr>
      </asp:PlaceHolder>
	<tr>
		<td><%= HtmlLabel(AddressCountryDropDownList,"AddressCountry.Label", "Country")%></td>
		<td>
			<asp:DropDownList ID="AddressCountryDropDownList" runat="server" Width="75%" TabIndex="11" onchange="UpdateStateCounty();" ClientIDMode="Static"/>
		</td>
	</tr>
</table>
<table style="width:100%;" role="presentation">
	<tr>
		<td style="width:248px;"><%= HtmlRequiredLabel(PhoneTextBox, "ContactPhone.Label", "Phone number")%></td>
		<td>
			<table role="presentation">
				<tr>
					<td>
						<asp:TextBox ID="PhoneTextBox" runat="server" TabIndex="12" MaxLength="20" />
						<act:MaskedEditExtender ID="PhoneNumberMaskedEdit" runat="server" MaskType="Number" TargetControlID="PhoneTextBox" PromptCharacter="_" ClearMaskOnLostFocus="false" AutoComplete="false" EnableViewState="true" ClearTextOnInvalid="false" />
					</td>
					<td style="padding-left:2px;">
						<%= HtmlInFieldLabel("PhoneExtensionTextBox", "PhoneExtension.Label", "extension", 62)%>
						<asp:TextBox ID="PhoneExtensionTextBox" runat="server" TabIndex="13" MaxLength="10" Width="67" ClientIDMode="Static"/>
						<act:MaskedEditExtender ID="PhoneExtensionMaskedEdit" runat="server" MaskType="Number" TargetControlID="PhoneExtensionTextBox" PromptCharacter=" " ClearMaskOnLostFocus="false" AutoComplete="false" EnableViewState="true" ClearTextOnInvalid="false" Mask="99999"/>
						<focus:LocalisedLabel runat="server" ID="PhoneTypeDropDownLabel" AssociatedControlID="PhoneTypeDropDown" LocalisationKey="PhoneType.Label" DefaultText="Phone type" CssClass="sr-only"/>
						<asp:DropDownList ID="PhoneTypeDropDown" runat="server" TabIndex="14" />	
					</td>
					<td><asp:RegularExpressionValidator ID="PhoneRegEx" runat="server" ControlToValidate="PhoneTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ContactInformation" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table role="presentation" style="width:100%;">
	<tr>
		<td style="width:250px;"><%= HtmlLabel(AlternatePhone1TextBox,"AlternatePhone1.Label", "Alternate phone number 1")%></td>
		<td>
			<asp:TextBox ID="AlternatePhone1TextBox" runat="server" TabIndex="15" MaxLength="20" />
			<act:MaskedEditExtender ID="AlternatePhone1MaskedEdit" runat="server" MaskType="Number" TargetControlID="AlternatePhone1TextBox" PromptCharacter="_" ClearMaskOnLostFocus="false" AutoComplete="false" EnableViewState="true" ClearTextOnInvalid="false" />
			<focus:LocalisedLabel runat="server" ID="AlternatePhone1TypeDropDownLabel" AssociatedControlID="AlternatePhone1TypeDropDown" LocalisationKey="AlternatePhone1.Label" DefaultText="Alternate phone number 1" CssClass="sr-only"/>
			<asp:DropDownList ID="AlternatePhone1TypeDropDown" runat="server" TabIndex="16"/>	
		</td>
	</tr>
	<tr>
		<td><%= HtmlLabel(AlternatePhone2TextBox,"AlternatePhone2.Label", "Alternate phone number 2")%></td>
		<td>
			<asp:TextBox ID="AlternatePhone2TextBox" runat="server" TabIndex="17" MaxLength="20" />
			<act:MaskedEditExtender ID="AlternatePhone2MaskEdit" runat="server" MaskType="Number" TargetControlID="AlternatePhone2TextBox" PromptCharacter="_" ClearMaskOnLostFocus="false" AutoComplete="false" EnableViewState="true" ClearTextOnInvalid="false" />
			<focus:LocalisedLabel runat="server" ID="AlternatePhone2TypeDropDownLabel" AssociatedControlID="AlternatePhone2TypeDropDown" LocalisationKey="AlternatePhone2.Label" DefaultText="Alternate phone number 2" CssClass="sr-only"/>
			<asp:DropDownList ID="AlternatePhone2TypeDropDown" runat="server" TabIndex="18" />
		</td>
	</tr>
	<tr>
		<td><%= HtmlRequiredLabel(EmailAddressTextBox, "EmailAddress.Label", "Email address")%></td>
		<td>
			<asp:TextBox ID="EmailAddressTextBox" runat="server" TabIndex="19" MaxLength="200" />
			<asp:RequiredFieldValidator ID="EmailAddressRequired" runat="server" ControlToValidate="EmailAddressTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ContactInformation" />
			<asp:RegularExpressionValidator ID="EmailAddressRegEx" runat="server" ControlToValidate="EmailAddressTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ContactInformation" />
		</td>
	</tr>
</table>
	
<script type="text/javascript">
	$(document).ready(
	function () {
		$("#<%= AddressPostcodeZipTextBox.ClientID %>").mask("<%= App.Settings.ExtendedPostalCodeMaskPattern %>", { placeholder: " " });
		$("#<%= AddressPostcodeZipTextBox.ClientID %>").change(function() {
		  PopulateAddressFieldsForPostalCode(
		    '<%= UrlBuilder.AjaxService() %>',
		    $(this).val(),
		    '<%=AddressStateDropDownList.ClientID %>',
		    '<%=AddressCountyDropDownList.ClientID %>',
		    '<%=AddressCountyDropDownListCascadingDropDown.BehaviorID%>', 
		    '<%=HtmlLocalise("Global.County.TopDefault", "- select county -") %>',
		    '<%=AddressTownCityTextBox.ClientID %>');
		});
	}
);
	
	Sys.Application.add_load(ContactInformation_PageLoad);

	function ContactInformation_PageLoad(sender, args) {
		var behavior = $find('<%=AddressCountyDropDownListCascadingDropDown.BehaviorID %>');      
		if (behavior != null) {
			behavior.add_populated(function() {
				ContactInformation_UpdateCountyDropDown($('#AddressCountyDropDownList'), $("#AddressStateDropDownList"));
			});
		}
	}
		
	function ContactInformation_UpdateCountyDropDown(countyDropDown, stateDropDown) {
		if (countyDropDown.children('option:selected').val() != '')
			countyDropDown.next().find(':first-child').text(countyDropDown.children('option:selected').text()).parent().addClass('changed');
		else
			SetCounty(stateDropDown.val() == '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>');
        
		UpdateStyledDropDown(countyDropDown, stateDropDown.val());
	}
		
	function SetCounty(outsideUS) {
		var countyDropDown = $('#AddressCountyDropDownList');

		if (outsideUS)
			$(countyDropDown).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.Counties.OutsideUS) %>"]').prop('selected', true);
		else
			$(countyDropDown).children().first().prop('selected', true);
	  
		$(countyDropDown).next().find(':first-child').text($(countyDropDown).children('option:selected').text()).parent().addClass('changed');
	}
	
	function UpdateStateCountry() {
		var countyValue = $('#AddressCountyDropDownList').val();
		var stateValue = $('#AddressStateDropDownList').val();

		if (countyValue == '<%= GetLookupId(Constants.CodeItemKeys.Counties.OutsideUS) %>') {
			if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>')
				SetState(true);
			SetCountry(true);
		}
	}
	
	function UpdateStateCounty() {
		var countryValue = $('#AddressCountryDropDownList').val();
		var stateDropdown = $('#AddressStateDropDownList');
		var stateValue = stateDropdown.val();

		if (countryValue == '<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>') {
			if (stateValue == '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>') {
       	SetState(false);

     		County_InvokeCountyCascadeDropdown();
			}
		}
		else {
			if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>') {
       	SetState(true);

     		County_InvokeCountyCascadeDropdown();
			}
		}
  }
	
	function UpdateCountry() {
		var stateValue = $('#AddressStateDropDownList').val();
		SetCountry(stateValue == '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>');
	}

	function SetState(outsideUS) {
		var stateDropDown = $('#AddressStateDropDownList');

		if (outsideUS)
			$(stateDropDown).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>"]').prop('selected', true);
		else
			$(stateDropDown).children().first().prop('selected', true);

		$(stateDropDown).next().find(':first-child').text($(stateDropDown).children('option:selected').text()).parent().addClass('changed');
  }
	
	function SetCountry(outsideUS) {
		var countryDropDown = $('#AddressCountryDropDownList');

		if (outsideUS)
			$(countryDropDown).children().first().prop('selected', true);
		else 
			$(countryDropDown).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>"]').prop('selected', true);
	  
		$(countryDropDown).next().find(':first-child').text($(countryDropDown).children('option:selected').text()).parent().addClass('changed');
	}
	
	function SetCounty(outsideUS) {
		var countyDropDown = $('#AddressCountyDropDownList');

		if (outsideUS)
			$(countyDropDown).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.Counties.OutsideUS) %>"]').prop('selected', true);
		else
			$(countyDropDown).children().first().prop('selected', true);
	  
		$(countyDropDown).next().find(':first-child').text($(countyDropDown).children('option:selected').text()).parent().addClass('changed');
	}
	
	function County_InvokeCountyCascadeDropdown() {
		$get("<%= AddressCountyDropDownList.ClientID %>")._behaviors[0]._onParentChange(null, null);
	}
</script>