﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;

using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class SecurityQuestions : UserControlBase
	{
		private const int NumberOfQuestions = 3;
		private bool _lastQuestionHasValue;

	  public SecurityQuestionsPageMode PageMode { get; set; }

		public string ValidationGroup
		{
			get { return GetViewStateValue<string>("SecurityQuestions:ValidationGroup"); }
			set { SetViewStateValue("SecurityQuestions:ValidationGroup", value); }
		}

		/// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
			SetupPageMode();
			
			if (IsPostBack) 
				return;

			LocaliseUi();
    }

		/// <summary>
		/// Binds the controls on the page
		/// </summary>
		/// <param name="showSaveAllButton">Whether to show the Save All button and do the save in the control</param>
		public void Bind(bool showSaveAllButton = false)
		{
			var allQuestions = new UserSecurityQuestionDto[NumberOfQuestions];
			var numberOfExistingQuestions = 0;

			if (PageMode == SecurityQuestionsPageMode.Update)
			{
				var questions = ServiceClientLocator.AccountClient(App).GetUserSecurityQuestion();
				foreach (var question in questions)
				{
					allQuestions[numberOfExistingQuestions] = question;
					numberOfExistingQuestions++;
				}
			}

			for (var index = numberOfExistingQuestions; index < NumberOfQuestions; index++)
			{
				allQuestions[index] = new UserSecurityQuestionDto();
			}

			_lastQuestionHasValue = false;

			SecurityQuestionRepeater.DataSource = allQuestions;
			SecurityQuestionRepeater.DataBind();

			if (showSaveAllButton)
			{
				SaveAllButton.ValidationGroup = ValidationGroup;
				ButtonsPanel.Visible = true;
			}
		}

		/// <summary>
		/// Set up controls based on the page mode
		/// </summary>
		private void SetupPageMode()
		{
			HelpText1.Visible = PageMode == SecurityQuestionsPageMode.Register;
			ClientJavaScriptCode.Visible = PageMode == SecurityQuestionsPageMode.Register;
		}
    
    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUi()
    {
        HeaderLiteral.Text = CodeLocalise("HeaderLiteral.Text", "Confirming your identity for password resets");
        HelpTextLiteral1.Text = CodeLocalise("HelpTextLiteral1.Text", "If you forget your password and request password help to send a reset email, we will ask you to confirm your identity by answering a combination of the questions below. You must select three different questions and provide answers to each.");
        QuestionRequired.ErrorMessage = CodeLocalise("SecurityQuestion.RequiredErrorMessage", "Question is required"); 
        AnswerRequired.ErrorMessage = CodeLocalise("SecurityAnswer.RequiredErrorMessage", "Answer is required");
			UpdateSecurityQuestion.Text = SaveAllButton.Text = CodeLocalise("SecurityQuestion.SaveSecurityQuestion", "Save");
			CancelUpdateSecurityQuestion.Text = CodeLocalise( "SecurityQuestion.CancelUpdateSecurityQuestion", "Close" );
		}

		/// <summary>
		/// Binds the drop-down in the modal
		/// </summary>
		/// <param name="itemToEdit">The question number being editted</param>
		private void BindModalSecurityQuestionDropDown(int itemToEdit)
		{
			var questions = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.SecurityQuestions);

			QuestionDropDown.Items.Clear();
			BindExplorerDropDown(QuestionDropDown, questions, null, CodeLocalise( "SecurityQuestion.TopDefault", "- select a question -" ));

			for (var itemNumber = 1; itemNumber <= NumberOfQuestions; itemNumber++)
			{
				var item = SecurityQuestionRepeater.Items[itemNumber - 1];
				var questionDropDown = (DropDownList)item.FindControl("SecurityQuestionDropDown");

				if (itemToEdit != itemNumber && questionDropDown.SelectedValue.IsNotNullOrEmpty())
				{
					QuestionDropDown.Items.Remove(QuestionDropDown.Items.FindByValue(questionDropDown.SelectedValue));
				}
			}
			QuestionDropDown.SelectedValue = string.Empty;
		}

		/// <summary>
		/// Binds the question drop-down
		/// </summary>
		/// <param name="dropDownList">The dropdown</param>
		/// <param name="lookupItems">The list of questions</param>
		/// <param name="currentValue">The selected value</param>
		/// <param name="topDefault">Whether to add a "Add new" option at the top</param>
    private static void BindExplorerDropDown(DropDownList dropDownList, IList<LookupItemView> lookupItems, string currentValue, string topDefault)
    {
      dropDownList.DataSource = lookupItems;
      dropDownList.DataValueField = "Id";
      dropDownList.DataTextField = "Text";
      dropDownList.DataBind();

      if (!String.IsNullOrEmpty(currentValue) && dropDownList.Items.FindByValue(currentValue) != null) dropDownList.SelectValue(currentValue);
      if (!String.IsNullOrEmpty(topDefault)) dropDownList.Items.Insert(0, new ListItem(topDefault, string.Empty));
    }

		/// <summary>
		/// Fires when an item is bound to the repeater control
		/// </summary>
		/// <param name="source">The repeater control</param>
		/// <param name="e">Arguments with details of the item being bound</param>
		protected void SecurityQuestionRepeater_ItemDataBound(object source, RepeaterItemEventArgs e)
		{
			var questionNumber = e.Item.ItemIndex + 1;

			var validationGroup = ValidationGroup;
			if (ValidationGroup.IsNullOrEmpty())
				validationGroup = PageMode == SecurityQuestionsPageMode.Register ? "" : string.Concat("SecurityQuestionAnswer", questionNumber);

			var questionBox = (HtmlControl) e.Item.FindControl("QuestionBox");
			questionBox.Attributes.Add("class", PageMode == SecurityQuestionsPageMode.Register ? "questbox narrow" : "questbox");

			var questionNumberLiteral = (Literal)e.Item.FindControl("SecurityQuestionNumber");
			questionNumberLiteral.Text = CodeLocalise("QuestionNumber.Text", "Security question {0}", questionNumber);

			// Question Drop Down
			var questionDropDown = (DropDownList)e.Item.FindControl("SecurityQuestionDropDown");
			var questions = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.SecurityQuestions);
			questionDropDown.Attributes.Add("data-number", questionNumber.ToString(CultureInfo.InvariantCulture));
			questionDropDown.Enabled = PageMode == SecurityQuestionsPageMode.Register;
			BindExplorerDropDown(questionDropDown, questions, null, CodeLocalise("SecurityQuestion.TopDefault", "- select a question -"));

			// Question Validator
			var questionRequired = (RequiredFieldValidator)e.Item.FindControl("SecurityQuestionRequired");
			questionRequired.Visible = PageMode == SecurityQuestionsPageMode.Register;
			questionRequired.ErrorMessage = CodeLocalise("SecurityQuestion.RequiredErrorMessage", "Question is required");
			questionRequired.ValidationGroup = validationGroup;

			// Answer Textbox
			var answerTextBox = (TextBox)e.Item.FindControl("SecurityAnswerTextBox");
		  answerTextBox.TextMode = PageMode == SecurityQuestionsPageMode.Register ||
		                           (PageMode == SecurityQuestionsPageMode.Update && App.User.IsShadowingUser)
		    ? TextBoxMode.SingleLine
		    : TextBoxMode.Password;

			// Answer Validator
			var answerRequired = (RequiredFieldValidator)e.Item.FindControl("SecurityAnswerRequired");
			answerRequired.ErrorMessage = PageMode == SecurityQuestionsPageMode.Register ? CodeLocalise("SecurityAnswer.RequiredErrorMessage", "Answer is required") : CodeLocalise("SecurityAnswer.WrongAnswerErrorMessage", "Incorrect Security Answer");
			answerRequired.ValidationGroup = validationGroup;

			// Update button
			var updateButton = (Button)e.Item.FindControl("UpdateSecurityQuestionAnswer");
			updateButton.Text = CodeLocalise("SecurityQuestion.UpdateSecurityQuestion", "Update");
			updateButton.Visible = PageMode == SecurityQuestionsPageMode.Update;
			updateButton.CommandName = "UPDATE";
			updateButton.CommandArgument = questionNumber.ToString(CultureInfo.InvariantCulture);
			updateButton.ValidationGroup = validationGroup;

			if (PageMode == SecurityQuestionsPageMode.Update)
			{
				var question = (UserSecurityQuestionDto)e.Item.DataItem;

				questionDropDown.SelectedValue = question.SecurityQuestionId.ToString();
				answerRequired.Enabled = questionDropDown.SelectedValue.IsNotNullOrEmpty();
				updateButton.Enabled = _lastQuestionHasValue || questionNumber == 1 || questionDropDown.SelectedValue.IsNotNullOrEmpty();

        // Display and disable the security answer if Assist user is updating the security questions/answers on another user's behalf
			  if (App.User.IsShadowingUser)
			  {
			    answerTextBox.Text = question.SecurityAnswerEncrypted;
			    answerTextBox.Enabled = false;
			  }

				_lastQuestionHasValue = (questionDropDown.SelectedValue.IsNotNullOrEmpty());
			}
		}

		/// <summary>
		/// Unbinds all the questions
		/// </summary>
		/// <returns>A list of questions/answers</returns>
		public List<UserSecurityQuestionDto> Unbind()
		{
			var userSecurityQuestions = new List<UserSecurityQuestionDto>();
			var questionIndex = 1;

			foreach (RepeaterItem item in SecurityQuestionRepeater.Items)
			{
				var questionDropDown = (DropDownList)item.FindControl("SecurityQuestionDropDown");
				var answerTextBox = (TextBox)item.FindControl("SecurityAnswerTextBox");

				if (questionDropDown.SelectedValue != string.Empty)
				{
					userSecurityQuestions.Add(new UserSecurityQuestionDto
					{
						QuestionIndex = questionIndex,
						SecurityQuestion = null,
						SecurityQuestionId = questionDropDown.SelectedValueToLong(),
						SecurityAnswerEncrypted = answerTextBox.Text
					});

					questionIndex++;
				}
			}

			return userSecurityQuestions;
		}

		protected void UpdateSecurityQuestionButton_Click( object sender, EventArgs e )
		{
			if (AnswerTextBox.Text.IsNullOrWhitespace())
			{
				AnswerRequired.Visible = true;
				AnswerRequired.IsValid = false;
				return;
			}
			if( QuestionDropDown.SelectedValue == string.Empty )
			{
				QuestionRequired.Visible = true;
				QuestionRequired.IsValid = false;
			}
			var questionId = QuestionDropDown.SelectedValueToInt();
			var answer = AnswerTextBox.TextTrimmed();
			
			ServiceClientLocator.AccountClient(App).ChangeSecurityQuestion(null, questionId, answer, Convert.ToInt32(EditIndex.Value));

			Bind();

			ModalPopup.Hide();
		}

		/// <summary>
		/// Cancels the modal
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void CancelUpdateSecurityQuestionButton_Click( object sender, EventArgs e )
		{
			ModalPopup.Hide();
		}

		/// <summary>
		/// Fires when the Update button is clicked for a question
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void UpdateSecurityQuestionAnswer_OnCommand(object sender, CommandEventArgs e)
		{
			var buttonNumber = e.CommandArgument.AsInt32();
			var item = SecurityQuestionRepeater.Items[buttonNumber - 1];

			var questionDropDown = (DropDownList)item.FindControl("SecurityQuestionDropDown");
			var answerTextBox = (TextBox)item.FindControl("SecurityAnswerTextBox");
			var answerRequired = (RequiredFieldValidator)item.FindControl("SecurityAnswerRequired");

			var securityQuestion = ServiceClientLocator.AccountClient(App).GetUserSecurityQuestion(buttonNumber);

			if (securityQuestion != null && securityQuestion.Any())
			{
				if (securityQuestion.First().SecurityAnswerEncrypted != answerTextBox.Text)
				{
					answerRequired.Visible = true;
					answerRequired.IsValid = false;
					return;
				}
			}

			answerRequired.Visible = false;
			answerRequired.IsValid = true;

			BindModalSecurityQuestionDropDown(buttonNumber);

			QuestionBoxHeaderModal.InnerHtml = CodeLocalise("SecurityQuestion.QuestionBoxHeaderModal", "Security question {0}", buttonNumber);
			if (questionDropDown.Items.FindByValue(questionDropDown.SelectedValue).IsNotNull())
				QuestionDropDown.SelectedValue = questionDropDown.SelectedValue;

			AnswerTextBox.Text = string.Empty;
			EditIndex.Value = buttonNumber.ToString(CultureInfo.InvariantCulture);

			ModalPopup.Show();
		}

		/// <summary>
		/// Fires when the save button is clicked to save all question
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void SaveAllButton_Click(object sender, EventArgs e)
		{
			var questions = Unbind();
			ServiceClientLocator.AccountClient(App).ChangeSecurityQuestions(questions);
		}
	}
}