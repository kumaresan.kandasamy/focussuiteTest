﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Common.Helpers;
using Focus.Common;
using Framework.Core;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class ScrollableSkillsAccordion : UserControlBase
	{
		/// <summary>
		/// Gets or sets the CSS class to apply to the outer wrapper of the control.
		/// </summary>
		[CssClassProperty]
		public string WrapperCssClass { get; set; }

		/// <summary>
		/// Gets or sets the CSS class to apply to the scrollable accordion wrapper.
		/// </summary>
		[CssClassProperty]
		public string AccordionCssClass { get; set; }

		/// <summary>
		/// Gets or sets the CSS class to apply to the summary list box wrapper.
		/// </summary>
		[CssClassProperty]
		public string SummaryCssClass { get; set; }

		/// <summary>
		/// Gets or sets the data source for the control.
		/// </summary>
		public List<EducationInternshipCategoryDto> InternshipCategories { get; set; }

    /// <summary>
    /// Gets or sets the job Id.
    /// </summary>
    public long JobId { get; set; }

	  private List<long> _skillIds;
    private List<string> _skillNames;
 
    public List<string> SelectedNames
    {
      get
      {
        return _skillNames ?? (_skillNames = SkillsNamesHiddenField.Value.Split(',').Where(id => id.Length > 0).ToList());
      }
    }

    public List<long> SelectedItems
    {
      get
      {
        if (_skillIds == null)
        {
          var skills = SkillsIdsHiddenField.Value.Split(',');
          _skillIds = skills.Where(id => id.Length > 0).Select(id => Convert.ToInt64(id)).ToList();
        }
        return _skillIds;
      }
      set
      {
        _skillIds = value;
        SkillsIdsHiddenField.Value = (_skillIds.IsNotNullOrEmpty()) ? string.Join(",", _skillIds.Select(s => s.ToString(CultureInfo.InvariantCulture))) : "";
      }
    }
		
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>

    protected void Page_Load(object sender, EventArgs e)
    {
      if (WrapperCssClass.IsNotNullOrEmpty())
        scrollableSkillsAccordionWrapper.Attributes.Add("class", WrapperCssClass);

      if (AccordionCssClass.IsNotNullOrEmpty())
        scrollableSkillsAccordion.Attributes.Add("class", AccordionCssClass);

      if (SummaryCssClass.IsNotNullOrEmpty())
        scrollableSkillsSummary.Attributes.Add("class", SummaryCssClass);

		  if (!IsPostBack)
		    BindControl();

      RegisterJavascript();
     
    }

		/// <summary>
		/// Binds the control.
		/// </summary>
    private void BindControl()
		{
			if (InternshipCategories.IsNullOrEmpty())
			{
				InternshipCategories = ServiceClientLocator.CoreClient(App).GetEducationInternshipCategories(true);
			}

			if (InternshipCategories.IsNullOrEmpty()) return;
			SkillCategoryRepeater.DataSource = InternshipCategories;
			SkillCategoryRepeater.DataBind();
      
		}

		/// <summary>
		/// Handles the ItemDataBound event of the SkillCategoryRepeater control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
	  protected void SkillCategoryRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;
			var category = e.Item.DataItem as EducationInternshipCategoryDto;
			var childRepeater = e.Item.FindControl("SkillRepeater") as Repeater;
			if (childRepeater.IsNull()) return;
			if (category.CategorySkills.IsNullOrEmpty()) return;
			childRepeater.DataSource = category.CategorySkills;
			childRepeater.DataBind();
		}

		/// <summary>
		/// Handles the ItemDataBound event of the SkillRepeater control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void SkillRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;
			var skill = e.Item.DataItem as EducationInternshipSkillDto;
			var checkBoxList = e.Item.FindControl("SkillCheckBoxList") as CheckBoxList;
			if (checkBoxList.IsNull()) return;
			checkBoxList.Items.Clear();

		  var listItem = new ListItem
		  {
        Text = skill.Name,
        Value = skill.Id.ToString(),
		    Selected = SelectedItems.Contains(skill.Id.GetValueOrDefault(0))
		  };
		  checkBoxList.Items.Add(listItem);
		}

    /// <summary>
    /// Registers the javascript .
    /// </summary>
    protected void RegisterJavascript()
    {
      var script = @"<script type=""text/javascript"">
        var " + scrollableSkillsAccordion.ClientID + @"_Details = new ScrollableSkillsAccordion(
          '" + scrollableSkillsAccordion.ClientID + @"',
          '" + scrollableSkillsSummary.ClientID + @"',
          '" + SkillsIdsHiddenField.ClientID + @"',
          '" + SkillsNamesHiddenField.ClientID + @"',
		      '" + ResolveUrl( UrlBuilder.ButtonDeleteIcon() ) + @"'
        );

        function " + scrollableSkillsAccordion.ClientID + @"_Load() {
          " + scrollableSkillsAccordion.ClientID + @"_Details.SetUpCheckBoxes();
        }
  
        Sys.Application.add_load(function() {
		      SetupAccordionNew();
		      " + scrollableSkillsAccordion.ClientID + @"_Load();
        });
  
      </script>";

      Page.ClientScript.RegisterStartupScript(GetType(), "StartupScript", script);
      ScriptManager.RegisterClientScriptInclude(this, GetType(), "scrollableSkillsAccordionScriptInclude", UrlHelper.GetCacheBusterUrl("~/Assets/Scripts/scrollable-skills-accordion.js"));
    }
	}
}