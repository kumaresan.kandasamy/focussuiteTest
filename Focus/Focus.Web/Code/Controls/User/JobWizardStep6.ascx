﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobWizardStep6.ascx.cs"
	Inherits="Focus.Web.Code.Controls.User.JobWizardStep6" %>
<%@ Register TagPrefix="ajaxtoolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.50731.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<table style="width:100%;" role="presentation">
	<tr style="float: right" id="ExpandCollapseAllRow" runat="server">
		<td>
			<asp:Button ID="ExpandCollapseAllButton" runat="server" class="button3" Style="float: right"
				ClientIDMode="Static" />
		</td>
	</tr>
	<tr>
		<td>
			<asp:Panel ID="SalaryHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
				<table class="accordionTable" role="presentation">
					<tr class="multipleAccordionTitle">
						<td>
							<asp:Image ID="SalaryHeaderImage" runat="server" AlternateText="Salary Header Image"/>
							&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Salary.Label", "Salary")%></a></span>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<div class="singleAccordionContentWrapper">
				<asp:Panel ID="SalaryPanel" runat="server" CssClass="singleAccordionContent">
					<table role="presentation">
						<tr>
							<td style="vertical-align:top;">
								<%= HtmlLabel("SalaryRange.Label", "Salary range")%>&nbsp;
							</td>
							<td>
								<asp:TextBox runat="server" ID="MinimumSalaryTextBox" ClientIDMode="Static" MaxLength="10" />
                                <ajaxtoolkit:FilteredTextBoxExtender ID="minSalary" runat="server" TargetControlID="MinimumSalaryTextBox" FilterType="Custom" ValidChars="0123456789." />
								<br />
								<i>
									<%= HtmlLocalise("Minimum.Text", "minimum")%></i>
							</td>
							<td>
								<asp:TextBox runat="server" ID="MaximumSalaryTextBox" ClientIDMode="Static" MaxLength="10" />
                                <ajaxtoolkit:FilteredTextBoxExtender ID="maxSalary" runat="server" TargetControlID="MaximumSalaryTextBox" FilterType="Custom" ValidChars="0123456789." />
								<br />
								<i>
									<focus:LocalisedLabel runat="server" ID="MaximumActualLabel" DefaultText="maximum/actual"/>
								</i>
							</td>
							<td style="vertical-align:top;">
								<asp:DropDownList runat="server" ID="SalaryFrequencyDropDown" ClientIDMode="Static" />
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<asp:CustomValidator runat="server" ClientValidationFunction="JobWizardStep6_ValidateMinimumSalaryAmount" 
									ID="MinimumSalaryValueValidator" ControlToValidate="MinimumSalaryTextBox" 
									CssClass="error" Display="Dynamic" />
								<asp:CompareValidator runat="server" Operator="GreaterThanEqual" ID="MinimumSalaryMaximumSalaryValueValidator"
									ControlToValidate="MaximumSalaryTextBox" ControlToCompare="MinimumSalaryTextBox"
									Type="Double" SetFocusOnError="True" CssClass="error" Display="Dynamic" />
                <asp:CustomValidator runat="server" ID="MinimumSalaryActualValidator" ControlToValidate="MaximumSalaryTextBox" 
                    SetFocusOnError="True" CssClass="error" Display="Dynamic"
                    ValidateEmptyText="True" ClientValidationFunction="JobWizardStep6_ValidateMinimumSalaryRange" />
								<asp:CustomValidator runat="server" ClientValidationFunction="JobWizardStep6_ValidateMaximumSalaryAmount" 
									ID="MaximumSalaryValueValidator" ControlToValidate="MaximumSalaryTextBox" 
									CssClass="error" Display="Dynamic" />
								<asp:CustomValidator runat="server" ID="SalaryFrequencyDropDownValidator" ControlToValidate="SalaryFrequencyDropDown"
									ClientValidationFunction="validateSalaryFrequencyDropDown" ValidateEmptyText="True"
									SetFocusOnError="True" CssClass="error" Display="Dynamic" />
								
							</td>
						</tr>
            <asp:PlaceHolder runat="server" ID="MeetsMinimumWageRequirementPlaceHolder">
              <tr>
							  <td colspan="4">
							    <asp:CheckBox ID="MeetsMinimumWageRequirementCheckBox" runat="server"/>
                  <%= HtmlRequiredLabel(MeetsMinimumWageRequirementCheckBox, "MeetsMinimumWageRequirementCheckBox.Text", "I confirm that this job posting meets the state minimum-wage requirement")%>
                  <br />
							  </td>
              </tr>
              <tr>
							  <td colspan="4">
							    <focus:CheckBoxCustomValidator runat="server" ID="MeetsMinimumWageRequirementValidator" ControlToValidate="MeetsMinimumWageRequirementCheckBox" ClientValidationFunction="JobWizardStep6_ValidateMeetsMinimumWageRequirement" ValidateEmptyText="True" EnableClientScript="True" SetFocusOnError="True" CssClass="error" Display="Dynamic" />
							  </td>
              </tr>
            </asp:PlaceHolder>
						<tr>
							<td colspan="4">
								<asp:CheckBox ID="HideSalaryCheckbox" runat="server" ClientIDMode="Static" /><br />
								<asp:CheckBox runat="server" ID="OtherIncomeConditionsCheckbox" ClientIDMode="Static"
									Text="Other income conditions" onclick="EnableDisableSalaryRadioButtons(this)" />
								<div style="margin-left: 15px">
									<asp:RadioButtonList ID="IncomeConditionsRadioButtonList" ClientIDMode="Static" runat="server">
										<asp:ListItem runat="server">Commission</asp:ListItem>
										<asp:ListItem runat="server">Commission and salary</asp:ListItem>
									</asp:RadioButtonList>
								</div>
								<asp:CustomValidator runat="server" ID="RadioButtonValidator" Text="Please select a salary condition."
									ControlToValidate="IncomeConditionsRadioButtonList" ClientValidationFunction="ValidateIncomeConditionsRadioButtons"
									ValidateEmptyText="True" SetFocusOnError="True" CssClass="error" Display="Dynamic" />
							</td>
						</tr>
					</table>
          
          <asp:PlaceHolder runat="server" ID="WorkNetLinkPlaceHolder">
            <p>
              <asp:HyperLink ID="WorkNetLinkHyperLink" runat="server" Target="_tab"></asp:HyperLink>
            </p>
          </asp:PlaceHolder>
				</asp:Panel>
			</div>
			<act:CollapsiblePanelExtender ID="SalaryPanelExtender" runat="server" TargetControlID="SalaryPanel"
				ExpandControlID="SalaryHeaderPanel" CollapseControlID="SalaryHeaderPanel" Collapsed="false"
				ImageControlID="SalaryHeaderImage" SuppressPostBack="true" />
			<asp:Panel ID="HoursHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
				<table role="presentation" class="accordionTable">
					<tr class="multipleAccordionTitle">
						<td>
							<asp:Image ID="HoursHeaderImage" runat="server" AlternateText="Hours Header Image"/>
							&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Hours.Label", "Hours")%></a></span>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<div class="singleAccordionContentWrapper">
				<asp:Panel ID="HoursPanel" runat="server" CssClass="singleAccordionContent">
					<table role="presentation" class="multiLineChecboxTable">
						<tr>
							<td class="twoColumn1">
								<%= HtmlLabel("NormalWorkDays.Label", "Normal work days")%>
							</td>
							<td class="twoColumn2">
							    <asp:CheckBox ID="WorkWeekdaysCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" onclick="setCheckedStateOnWeekdayCheckboxes()"/>
							</td>
                            <td>
								<asp:CheckBox ID="WorkMonCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" onclick="setCheckedWeekdaysAndWeekendStateOnDayToggle()" />
							</td>
							<td>
								<asp:CheckBox ID="WorkTueCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" onclick="setCheckedWeekdaysAndWeekendStateOnDayToggle()" />
							</td>
							<td>
								<asp:CheckBox ID="WorkWedCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" onclick="setCheckedWeekdaysAndWeekendStateOnDayToggle()" />
							</td>
							<td>
								<asp:CheckBox ID="WorkThuCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" onclick="setCheckedWeekdaysAndWeekendStateOnDayToggle()" />
							</td>
							<td>
								<asp:CheckBox ID="WorkFriCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" onclick="setCheckedWeekdaysAndWeekendStateOnDayToggle()" />
							</td>
						</tr>
						<tr>
							<td>
							</td>
                            <td>
							    <asp:CheckBox ID="WorkWeekendCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" onclick="setCheckedStateOnWeekendCheckboxes()"/>
							</td>
							<td>
								<asp:CheckBox ID="WorkSatCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" onclick="setCheckedWeekdaysAndWeekendStateOnDayToggle()"/>
							</td>
							<td>
								<asp:CheckBox ID="WorkSunCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" onclick="setCheckedWeekdaysAndWeekendStateOnDayToggle()"/>
							</td>
							<td colspan="3">
							</td>
						</tr>
                        <tr>
							<td colspan="1">
							</td>
							<td>
								<asp:CheckBox ID="WorkVariesCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" />
							</td>
							<td colspan="4">
							</td>
                        </tr>
						<tr>
							<td>
								<asp:Literal runat="server" ID="HoursLabel"></asp:Literal>
							</td>
							<td colspan="6" style="vertical-align: top">
								<asp:DropDownList runat="server" ID="EmploymentStatusDropDown" ClientIDMode="Static" />
								<br />
								<asp:RequiredFieldValidator runat="server" ID="EmploymentStatusValidator" ControlToValidate="EmploymentStatusDropDown" 
                    SetFocusOnError="True" CssClass="error" Display="Dynamic"></asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td>
								<asp:Literal runat="server" ID="JobTypeLabel"></asp:Literal>
							</td>
							<td colspan="5">
								<asp:DropDownList runat="server" ID="JobTypeDropDown" ClientIDMode="Static" />
								<br />
								<asp:RequiredFieldValidator runat="server" ID="JobTypeValidator" ControlToValidate="JobTypeDropDown" 
                    SetFocusOnError="True" CssClass="error" Display="Dynamic"></asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td style="vertical-align: top">
							  <asp:Literal runat="server" ID="HoursPerWeekHeader"></asp:Literal>
							</td>
							<td colspan="6">
							  <asp:PlaceHolder runat="server" ID="MinimumHoursPerWeekPanel" Visible="False">
							    <div style="display:inline-block">
                    <asp:TextBox runat="server" ID="MinimumHoursPerWeekTextBox" Text="" ClientIDMode="Static" />
                    <br />
								    <i>
									    <asp:Literal runat="server" ID="MinimumHoursLabel"></asp:Literal>
								    </i>
                  </div>
							  </asp:PlaceHolder>
                <div style="display:inline-block">
								  <asp:TextBox runat="server" ID="HoursPerWeekTextBox" Text="" ClientIDMode="Static" />
                  <asp:PlaceHolder runat="server" ID="MaximumActualLabelPanel">
                    <br />
								    <i>
									    <asp:Literal runat="server" ID="MaximumHoursLabel"></asp:Literal>
								    </i>
                  </asp:PlaceHolder>
                </div>
                <br />
                <asp:PlaceHolder runat="server" ID="MinimumHoursValidatorsPanel" Visible="False">
								  <asp:RangeValidator ID="MinimumHoursPerWeekRangeValidator" runat="server" ControlToValidate="MinimumHoursPerWeekTextBox"
									  CssClass="error" MaximumValue="168" MinimumValue="0" Type="Double" SetFocusOnError="True" Display="Dynamic" />
                  <asp:CustomValidator runat="server" ID="HoursPerWeekCustomValidator" ControlToValidate="HoursPerWeekTextBox" 
                    SetFocusOnError="True" CssClass="error" Display="Dynamic"
                    ValidateEmptyText="True" ClientValidationFunction="JobWizardStep6_ValidateHoursPerWeek" />
                  <asp:CustomValidator runat="server" ID="MinimumHoursPerWeekCustomValidator" ControlToValidate="MinimumHoursPerWeekTextBox" 
                    SetFocusOnError="True" CssClass="error" Display="None"
                    ValidateEmptyText="True" ClientValidationFunction="JobWizardStep6_ValidateMinimumHoursPerWeek" />
                  <asp:CompareValidator runat="server" Operator="LessThan" ID="HoursPerWeekCompareValidator"
									  ControlToValidate="MinimumHoursPerWeekTextBox" ControlToCompare="HoursPerWeekTextBox"
									  Type="Double" SetFocusOnError="True" CssClass="error" />
                </asp:PlaceHolder>
								<asp:RequiredFieldValidator runat="server" ID="HoursPerWeekRequiredValidator" Visible="False" Enabled="False" ControlToValidate="HoursPerWeekTextBox" 
                    SetFocusOnError="True" CssClass="error" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:CustomValidator runat="server" ID="HoursPerWeekEmploymentValidator" ControlToValidate="HoursPerWeekTextBox" 
                  SetFocusOnError="True" CssClass="error" Display="Dynamic"
                  ValidateEmptyText="True" ClientValidationFunction="JobWizardStep6_ValidateHoursForEmploymentStatus" />
							</td>
						</tr>
						<tr>
							<td>
								<%= HtmlLabel("NormalWorkShifts.Label", "Normal work shifts")%>
							</td>
							<td colspan="5">
								<asp:DropDownList runat="server" ID="NormalWorkShiftsDropDown" ClientIDMode="Static" />
							</td>
						</tr>
						<tr>
							<td>
								<%= HtmlLabel("OvertimeRequired.Label", "Overtime required?")%>
							</td>
							<td>
								<asp:CheckBox ID="OvertimeRequiredCheckBox" runat="server" ClientIDMode="Static" />
							</td>
						</tr>
						<tr>
							<td>
								<%= HtmlLabel("JobStatus.Label", "Job status")%>
							</td>
							<td colspan="5">
								<asp:DropDownList runat="server" ID="JobStatusDropDown" ClientIDMode="Static" />
							</td>
						</tr>
					</table>
				</asp:Panel>
			</div>
			<act:CollapsiblePanelExtender ID="HoursPanelExtender" runat="server" TargetControlID="HoursPanel"
				ExpandControlID="HoursHeaderPanel" CollapseControlID="HoursHeaderPanel" Collapsed="false"
				ImageControlID="HoursHeaderImage" SuppressPostBack="true" />
			<asp:Panel ID="BenefitsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
				<table role="presentation" class="accordionTable">
					<tr class="multipleAccordionTitle">
						<td>
							<asp:Image ID="BenefitsHeaderImage" runat="server" AlternateText="Benefits Header Image" />
							&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Benefits.Label", "Benefits")%></a></span>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<div class="singleAccordionContentWrapper">
				<asp:Panel ID="BenefitsPanel" runat="server" CssClass="singleAccordionContent">
					<table role="presentation" style="width:100%;" class="TableWithDividers">
						<tr class="topRow">
							<td style="width:20%;">
								<label>
									<%=HtmlLabel("BenefitsQuestion.Label", "Does your posting have benefits as part of the package?")%></label>
							</td>
							<td>
								<asp:RadioButton ID="YesBenefitsRadioButton" runat="server" ClientIDMode="Static" onclick="EnableDisableBenefitOptionsCheckboxes(false)" GroupName="NoBenefits" />
								<label for="YesBenefitsRadioButton"><%=HtmlLabel("YesBenefits.Label", "Yes")%></label>
								<asp:RadioButton ID="NoBenefitsRadioButton" runat="server" ClientIDMode="Static" onclick="EnableDisableBenefitOptionsCheckboxes(true)" GroupName="NoBenefits" />
								<label for="NoBenefitsRadioButton"><%=HtmlLabel("NoBenefits.Label", "No")%></label>
							  <asp:CustomValidator runat="server" ID="BenefitsValidator" Text="Please select the benefits appropriate to your posting" ClientValidationFunction="ValidateBenefits" CssClass="error" Display="Dynamic" />
              </td>
						</tr>
            <tr>
              <td style="width:20%;border-top: none">
								<label>
									<%=HtmlLabel("DisplayBenefits.Label", "Display benefits on job posting?")%></label>
							</td>
							<td style="border-top: none">
								<asp:RadioButton ID="YesShowBenefitsRadioButton" runat="server" ClientIDMode="Static" GroupName="ShowBenefits" />
								<label for="YesShowBenefitsRadioButton"><%=HtmlLabel("YesShowBenefits.Label", "Yes")%></label>
								<asp:RadioButton ID="NoShowBenefitsRadioButton" runat="server" ClientIDMode="Static" GroupName="ShowBenefits" />
								<label for="NoShowBenefitsRadioButton"><%=HtmlLabel("NoShowBenefits.Label", "No")%></label>
              </td>
            </tr>
						<tr>
							<td style="width:20%; vertical-align:top;">
								<%= HtmlLabel("Leave.Label", "Leave")%>
							</td>
							<td style="vertical-align:top;">
								<asp:CheckBox ID="PaidHolidaysCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static"
									CssClass="BenefitsOption" />
								<br />
								<asp:CheckBox ID="SickCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static"
									CssClass="BenefitsOption" />
							</td>
							<td style="vertical-align:top;">
								<asp:CheckBox ID="VacationCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static"
									CssClass="BenefitsOption" />
								<br />
								<asp:CheckBox ID="MedicalCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static"
									CssClass="BenefitsOption" />
							</td>
							<td style="vertical-align:top;">
								<asp:CheckBox ID="LeaveSharingCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static"
									CssClass="BenefitsOption" />
							</td>
						</tr>
						<tr>
							<td style="width:20%; vertical-align:top;">
								<%= HtmlLabel("Retirement.Label", "Retirement")%>
							</td>
							<td style="vertical-align:top;">
								<asp:CheckBox ID="FourZeroThreeBPlanCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static"
									CssClass="BenefitsOption" />
								<br />
								<asp:CheckBox ID="PensionPlanCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static"
									CssClass="BenefitsOption" />
							</td>
							<td style="vertical-align:top;">
								<asp:CheckBox ID="FourZeroOneKCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static"
									CssClass="BenefitsOption" />
								<br />
								<asp:CheckBox ID="ProﬁtSharingCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static"
									CssClass="BenefitsOption" />
							</td>
							<td style="vertical-align:top;">
								<asp:CheckBox ID="DeferredCompensationCheckBox" TextAlign="Right" runat="server"
									ClientIDMode="Static" CssClass="BenefitsOption" />
							</td>
						</tr>
						<tr>
							<td style="width:20%; vertical-align:top;">
								<%= HtmlLabel("Insurance.Label", "Insurance")%>
							</td>
							<td style="vertical-align:top;">
								<asp:CheckBox ID="DentalCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static"
									CssClass="BenefitsOption" />
								<br />
								<asp:CheckBox ID="DisabilityCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static"
									CssClass="BenefitsOption" />
								<br />
								<asp:CheckBox ID="DomesticPartnerCoverageCheckBox" TextAlign="Right" runat="server"
									ClientIDMode="Static" CssClass="BenefitsOption" />
							</td>
							<td style="vertical-align:top;">
								<asp:CheckBox ID="HealthCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static"
									CssClass="BenefitsOption" />
								<br />
								<asp:CheckBox ID="HealthSavingsCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static"
									CssClass="BenefitsOption" />
							</td>
							<td style="vertical-align:top;">
								<asp:CheckBox ID="LifeCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static"
									CssClass="BenefitsOption" />
								<br />
								<asp:CheckBox ID="VisionCheckBox" Text="Vision" TextAlign="Right" runat="server"
									ClientIDMode="Static" CssClass="BenefitsOption" />
							</td>
						</tr>
						<tr>
							<td style="width:20%; vertical-align:top;">
								<%= HtmlLabel("Miscellaneous.Label", "Miscellaneous")%>
							</td>
							<td style="vertical-align:top;">
								<asp:CheckBox ID="BeneﬁtsNegotiableCheckBox" TextAlign="Right" ClientIDMode="Static"
									runat="server" CssClass="BenefitsOption" />
								<br />
								<asp:CheckBox ID="ChildCareCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static"
									CssClass="BenefitsOption" />
							</td>
							<td style="vertical-align:top;">
								<asp:CheckBox ID="TuitionAssistanceCheckBox" Text="Tuition assistance" TextAlign="Right"
									runat="server" ClientIDMode="Static" CssClass="BenefitsOption" />
								<br />
								<asp:CheckBox ID="RelocationCheckBox" Text="Relocation" TextAlign="Right" runat="server"
									CssClass="BenefitsOption" />
							</td>
							<td style="vertical-align:top;">
								<asp:CheckBox ID="ClothingAllowanceCheckBox" Text="Clothing/Uniform" TextAlign="Right"
									runat="server" ClientIDMode="Static" CssClass="BenefitsOption" />
								<br />
								<asp:CheckBox ID="OtherCheckBox" Text="Other" TextAlign="Right" runat="server" ClientIDMode="Static"
									CssClass="BenefitsOption" />
								<br />
								&nbsp;&nbsp;&nbsp;&nbsp;
								<%= HtmlInFieldLabel("OtherTextBox", "OtherTextBox.InlineLabel", "please specify", 100)%>
								<asp:TextBox runat="server" ID="OtherTextBox" Width="100%" ClientIDMode="Static" MaxLength="3000" />
								<asp:CustomValidator runat="server" ID="OtherFieldValidator" Text="Please enter value."
									ControlToValidate="OtherTextBox" ClientValidationFunction="ValidateOtherTextBox"
									ValidateEmptyText="True" SetFocusOnError="True" CssClass="error" Display="Dynamic" />
							</td>
						</tr>
					</table>
				</asp:Panel>
			</div>
			<act:CollapsiblePanelExtender ID="BenefitsPanelExtender" runat="server" TargetControlID="BenefitsPanel"
				ExpandControlID="BenefitsHeaderPanel" CollapseControlID="BenefitsHeaderPanel" Collapsed="true"
				ImageControlID="BenefitsHeaderImage" SuppressPostBack="true" />
		</td>
	</tr>
</table>
<script type="text/javascript">
	Sys.Application.add_load(JobWizardStep6_PageLoad);

	var JobWizardStep6_SalaryFrequencyDropDown;
	var JobWizardStep6_MinimumSalaryTextBox;
	var JobWizardStep6_MaximumSalaryTextBox;
	
	function JobWizardStep6_PageLoad() {
	  JobWizardStep6_SalaryFrequencyDropDown = $("#<%= SalaryFrequencyDropDown.ClientID %>");
	  JobWizardStep6_MinimumSalaryTextBox = $("#<%=MinimumSalaryTextBox.ClientID %>");
	  JobWizardStep6_MaximumSalaryTextBox = $("#<%=MaximumSalaryTextBox.ClientID %>");
	  
		var otherCheckBox = $("#<%=OtherCheckBox.ClientID %>");

		if ($('#NoBenefitsRadioButton').is(':checked')) {
			EnableDisableBenefitOptionsCheckboxes(true);
		}

		if (otherCheckBox.length > 0) {
			JobWizardStep6_HandleOtherBenefits(otherCheckBox);
			otherCheckBox.click(function () {
				JobWizardStep6_HandleOtherBenefits($(this));
			});
		}

		$("#" + JobWizardStep6_NameValueCollection["EmploymentStatusDropDownClientId"]).change(function () {
		  ValidatorValidate($("#" + JobWizardStep6_NameValueCollection["HoursPerWeekEmploymentValidatorClientId"])[0]);
		});
		bindExpandCollapseAllPanels('<%= ExpandCollapseAllButton.ClientID %>', null); // bind the magic
	}

	function setCheckedStateOnWeekdayCheckboxes() {
		if ($('#WorkWeekdaysCheckBox').prop('checked') == true) {
			$('#WorkMonCheckBox').prop('checked', true);
			$('#WorkTueCheckBox').prop('checked', true);
			$('#WorkWedCheckBox').prop('checked', true);
			$('#WorkThuCheckBox').prop('checked', true);
			$('#WorkFriCheckBox').prop('checked', true);
		} else if ($('#WorkWeekdaysCheckBox').prop('checked') == false) {
			$('#WorkMonCheckBox').prop('checked', false);
			$('#WorkTueCheckBox').prop('checked', false);
			$('#WorkWedCheckBox').prop('checked', false);
			$('#WorkThuCheckBox').prop('checked', false);
			$('#WorkFriCheckBox').prop('checked', false);
		}
	}

	function setCheckedStateOnWeekendCheckboxes() {
		if ($('#WorkWeekendCheckBox').prop('checked') == true) {
			$('#WorkSatCheckBox').prop('checked', true);
			$('#WorkSunCheckBox').prop('checked', true);
		} else if ($('#WorkWeekendCheckBox').prop('checked') == false) {
			$('#WorkSatCheckBox').prop('checked', false);
			$('#WorkSunCheckBox').prop('checked', false);
		}
	}

	function setCheckedWeekdaysAndWeekendStateOnDayToggle() {
		if ($('#WorkSatCheckBox').prop('checked') == false || $('#WorkSatCheckBox').prop('checked') == false) {
			$('#WorkWeekendCheckBox').prop('checked', false);
		}
		if ($('#WorkMonCheckBox').prop('checked') == false ||
			$('#WorkTueCheckBox').prop('checked') == false ||
			$('#WorkWedCheckBox').prop('checked') == false ||
			$('#WorkThuCheckBox').prop('checked') == false ||
			$('#WorkFriCheckBox').prop('checked') == false) {
			$('#WorkWeekdaysCheckBox').prop('checked', false);
		}
	}

	function validateSalaryFrequencyDropDown(src, args) {
		args.IsValid = true;

		var MinimumSalaryTextBox = $("#<%= MinimumSalaryTextBox.ClientID %>").val();
		var MaximumSalaryTextBox = $("#<%= MaximumSalaryTextBox.ClientID %>").val();
		var salaryFrequencyDropDown = $("#<%= SalaryFrequencyDropDown.ClientID %>").val();

		if (salaryFrequencyDropDown == "" && (TrimText(MinimumSalaryTextBox) + TrimText(MaximumSalaryTextBox)) != "")
		  args.IsValid = false;

		ValidatorValidate($("#<%=MinimumSalaryValueValidator.ClientID %>")[0]);
		ValidatorValidate($("#<%=MaximumSalaryValueValidator.ClientID %>")[0]);
}

	function JobWizardStep6_HandleOtherBenefits(otherCheckBox) {

		var otherTextBox = $("#<%=OtherTextBox.ClientID %>");
		var infieldLabel = $("label[for='" + otherTextBox.attr("id") + "']");

		if (otherCheckBox.is(":checked")) {
			otherTextBox.prop("disabled", false);
			infieldLabel.show();
		} else {
			otherTextBox.prop("disabled", true);
			infieldLabel.hide();
		}
	}

	function ValidateOtherTextBox(source, args) {
		args.IsValid = !$('#OtherCheckBox').prop('checked');

		if (!args.IsValid) {
			args.IsValid = $('#OtherTextBox').val().length > 0;
		}
	}

	function EnableDisableSalaryRadioButtons(chkBox) {
		$('#IncomeConditionsRadioButtonList input').prop('disabled', !$(chkBox).prop('checked'));
		$('#IncomeConditionsRadioButtonList input').prop('checked', false);
	}

	function ValidateIncomeConditionsRadioButtons(source, args) {
		args.IsValid = !$('#OtherIncomeConditionsCheckbox').prop('checked');

		if (!args.IsValid) {
			args.IsValid = $('#IncomeConditionsRadioButtonList input:checked').prop('checked');
		}
	}

	function EnableDisableBenefitOptionsCheckboxes(bool) {
		$('.BenefitsOption input').prop('disabled', bool);
		if (bool) {
			$('.BenefitsOption input').prop('checked', false);
		}
	}

	function ValidateBenefits(source, args) {

		if ($('#NoBenefitsRadioButton').is(':checked')) {
			args.IsValid = true;
		} else {
			if ($('#YesBenefitsRadioButton').is(':checked') && $(".BenefitsOption :checkbox:checked").length == 0) {
				args.IsValid = false;
			}
		}
	}

	function JobWizardStep6_ValidateMeetsMinimumWageRequirement(oSrc, args) {
		args.IsValid = $("#<%=MeetsMinimumWageRequirementCheckBox.ClientID%>").is(":checked");
	}

	function JobWizardStep6_ValidateHoursPerWeek(oSrc, args) {
		var minCheckBox = $("#<%=MinimumHoursPerWeekTextBox.ClientID %>");
		var maxCheckBox = $("#<%=HoursPerWeekTextBox.ClientID %>");

		args.IsValid = maxCheckBox.val().trim().length > 0 || minCheckBox.val().trim().length == 0;
	}
	function JobWizardStep6_ValidateMinimumHoursPerWeek(oSrc, args) {
		var validator = $("#<%=HoursPerWeekCustomValidator.ClientID %>");
		if (validator.length > 0) {
			ValidatorValidate($("#<%=HoursPerWeekCustomValidator.ClientID %>")[0]);
		}
	}

	function JobWizardStep6_ValidateHoursForEmploymentStatus(oSrc, args) {
	  var hoursBoxVal = $("#" + JobWizardStep6_NameValueCollection["HoursPerWeekTextBoxClientId"]).val();
    if (hoursBoxVal != "") {
	    if (!hoursBoxVal.match(/^\d+.?\d*$/)) {
	      args.IsValid = false;
	      oSrc.innerHTML = JobWizardStep6_NameValueCollection["AllHoursPerWeekMessage"];
	    } else {
	      var hoursBoxFloatVal = parseFloat(hoursBoxVal);
	      var partTimeLimit = parseInt(JobWizardStep6_NameValueCollection["PartTimeEmploymentLimit"], 10);
	      var employmentStatusIndex = $("#" + JobWizardStep6_NameValueCollection["EmploymentStatusDropDownClientId"]).prop("selectedIndex");
	      if (employmentStatusIndex == 0 || partTimeLimit == 0) {
	        if (hoursBoxFloatVal < 0 || hoursBoxFloatVal > 168) {
	          args.IsValid = false;
	          oSrc.innerHTML = JobWizardStep6_NameValueCollection["AllHoursPerWeekMessage"];
	        }
	      } else if (employmentStatusIndex == 1) {
	        if (hoursBoxFloatVal < partTimeLimit || hoursBoxFloatVal > 168) {
	          args.IsValid = false;
	          oSrc.innerHTML = JobWizardStep6_NameValueCollection["FullTimeHoursPerWeekMessage"];
	        }
	      } else {
	        if (hoursBoxFloatVal < 0 || hoursBoxFloatVal >= partTimeLimit) {
	          args.IsValid = false;
	          oSrc.innerHTML = JobWizardStep6_NameValueCollection["PartTimeHoursPerWeekMessage"];
	        }
	      }
	    }
	  }
	}

	function JobWizardStep6_ValidateMinimumSalaryRange(oSrc, args) {
	  args.IsValid = JobWizardStep6_MaximumSalaryTextBox.val().trim().length > 0 || JobWizardStep6_MinimumSalaryTextBox.val().trim().length == 0;
	}

	function JobWizardStep6_ValidateMinimumSalaryAmount(oSrc, args) {
	  var value = JobWizardStep6_MinimumSalaryTextBox.val();
	  
	  args.IsValid = JobWizardStep6_SalaryFrequencyDropDown == null
	                   || JobWizardStep6_SalaryFrequencyDropDown.val() == ""
	                   || (JobWizardStep6_IsDecimal(value) && parseFloat(value) >= parseFloat(JobWizardStep6_CodeValues["MinThreshold" + JobWizardStep6_SalaryFrequencyDropDown.val()]));

	  if (!args.IsValid) {
	    $(oSrc).html(JobWizardStep6_CodeValues["ErrorMinimumSalary" + JobWizardStep6_SalaryFrequencyDropDown.val()]);
	  }
	}
	
	function JobWizardStep6_ValidateMaximumSalaryAmount(oSrc, args) {
	  var value = JobWizardStep6_MaximumSalaryTextBox.val();
	  
	  args.IsValid = JobWizardStep6_SalaryFrequencyDropDown == null
	                   || JobWizardStep6_SalaryFrequencyDropDown.val() == ""
	                   || (JobWizardStep6_IsDecimal(value) && parseFloat(value) <= parseFloat(JobWizardStep6_CodeValues["MaxThreshold" + JobWizardStep6_SalaryFrequencyDropDown.val()]));

	  if (!args.IsValid) {
	    $(oSrc).html(JobWizardStep6_CodeValues["ErrorMaximumSalary" + JobWizardStep6_SalaryFrequencyDropDown.val()]);
	  }
  }

  function JobWizardStep6_IsDecimal(val) {
    var ex = /^[0-9]*(\.[0-9]+)?$/;
    return ex.test(val);
  }

</script>
