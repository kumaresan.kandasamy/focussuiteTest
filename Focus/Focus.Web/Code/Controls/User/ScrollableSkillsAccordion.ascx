﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScrollableSkillsAccordion.ascx.cs" Inherits="Focus.Web.Code.Controls.User.ScrollableSkillsAccordion" %>
<span id="scrollableSkillsAccordionWrapper" runat="server">
	<span id="scrollableSkillsAccordion" runat="server" style="overflow-y:scroll;">
		<asp:Repeater runat="server" ID="SkillCategoryRepeater" OnItemDataBound="SkillCategoryRepeater_ItemDataBound">
			<HeaderTemplate><span class="accordionNew"></HeaderTemplate>
			<ItemTemplate>
				<span class="accordionSectionNew">
					<span class="accordionTitleNew"><%# Eval("Name") %></span>
					<span class="accordionContentNew">
						<asp:Repeater runat="server" ID="SkillRepeater" OnItemDataBound="SkillRepeater_ItemDataBound">
							<HeaderTemplate>
								<ul>
									<li>
										<span>
											<input type="checkbox" value="all" id="CheckAll" runat="server"/><focus:LocalisedLabel runat="server" ID="CheckAllLabel" AssociatedControlID="CheckAll" CssClass="embolden" LocalisationKey="CheckAll.Label" DefaultText="Select all" />
										</span>
									</li>
							</HeaderTemplate>
							<ItemTemplate>
								<li>
									<asp:CheckBoxList runat="server" ID="SkillCheckBoxList" RepeatLayout="Flow"></asp:CheckBoxList>
								</li>
							</ItemTemplate>
							<FooterTemplate></ul></FooterTemplate>
						</asp:Repeater>
					</span>
				</span>
			</ItemTemplate>
			<FooterTemplate></span></FooterTemplate>
		</asp:Repeater>
   </span>
  <span id="scrollableSkillsSummary" runat="server">
		<p class="instructionalText embolden">Your choices:</p>
		<table class="deletableListItemTable" id="selectedSkills">
		</table>
    <asp:HiddenField runat="server" ID="SkillsIdsHiddenField"/>
    <asp:HiddenField runat="server" ID="SkillsNamesHiddenField" />
   </span>
</span>
