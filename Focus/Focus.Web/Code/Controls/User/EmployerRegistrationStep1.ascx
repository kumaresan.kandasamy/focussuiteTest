<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployerRegistrationStep1.ascx.cs" Inherits="Focus.Web.Code.Controls.User.EmployerRegistrationStep1" %>
<%--<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha.Web.UI.Controls" Assembly="Recaptcha.Web" %> 
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>--%>
<%@ Register Src="~/Code/Controls/User/SecurityQuestions.ascx" TagName="SecurityQuestions"
  TagPrefix="uc" %>
  <script src="https://www.google.com/recaptcha/api.js"></script>
<div style="width: 100%;">
  <table class="formTable" role="presentation">
    <tr>
      <td colspan="2" style="width: 45%"/>
      <td style="width: 55%" />
    </tr>
    <tr>
      <td colspan="2">
        <div id="NotificationDiv" runat="server">
          <%= HtmlLocalise("Notification.Text", "Please note: you'll need your #BUSINESS#:LOWER's Federal Employer Identification Number (FEIN) to register for #FOCUSTALENT#.")%></div>
      </td>
      <td rowspan="8">
        <div class="formTable" style="border-left: solid 2px #ececec; width:99%; padding-left: 8px;">
					<uc:SecurityQuestions ID="SecurityQuestionsPanel" runat="server" PageMode="Register"  />
        </div>
      </td>
    </tr>
    <tr>
      <td colspan="3">
        <%= HtmlRequiredFieldsInstruction() %>
      </td>
    </tr>
    <asp:PlaceHolder runat="server" ID="Step1RegistrationPlaceHolder">
      <tr style="vertical-align:top;">
        <%--<td class="label"><%= HtmlRequiredLabel("UserName.Label", "Email address")%></td>--%>
        <td class="label" style="white-space: nowrap">
          <%= HtmlRequiredLabel(UserNameTextBox, "UserName.Label", "Email address")%>
        </td>
        <td>
          <asp:TextBox ID="UserNameTextBox" runat="server" ClientIDMode="Static" Width="98%"
            TabIndex="1" MaxLength="100" AutoCompleteType="Disabled" />
          <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserNameTextBox"
            SetFocusOnError="true" Display="Dynamic" CssClass="error" />
          <asp:RegularExpressionValidator ID="UserNameRegEx" runat="server" ControlToValidate="UserNameTextBox"
            SetFocusOnError="true" Display="Dynamic" CssClass="error" />
        </td>
      </tr>
      <tr style="vertical-align:top;">
        <td class="label" style="white-space: nowrap">
          <%= HtmlRequiredLabel(ConfirmUserNameTextBox, "ConfirmUserName.Label", "Confirm email address")%>
        </td>
        <td>
          <asp:TextBox ID="ConfirmUserNameTextBox" runat="server" ClientIDMode="Static" Width="98%"
            TabIndex="2" MaxLength="100" AutoCompleteType="Disabled" />
          <asp:RequiredFieldValidator ID="ConfirmUserNameRequired" runat="server" ControlToValidate="ConfirmUserNameTextBox"
            SetFocusOnError="true" Display="Dynamic" CssClass="error" />
          <asp:CompareValidator ID="ConfirmUserNameCompare" runat="server" ControlToValidate="ConfirmUserNameTextBox"
            ControlToCompare="UserNameTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
        </td>
      </tr>
      <tr style="vertical-align:top;">
        <td class="label" style="white-space: nowrap">
          <%= HtmlRequiredLabel(PasswordTextBox, "Password.Label", "Password")%>
        </td>
        <td>
          <asp:TextBox ID="PasswordTextBox" runat="server" ClientIDMode="Static" Width="98%"
            TextMode="Password" TabIndex="3" MaxLength="20" /><br />
          <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="PasswordTextBox"
            SetFocusOnError="true" Display="Dynamic" CssClass="error" />
          <asp:RegularExpressionValidator ID="PasswordRegEx" runat="server" ControlToValidate="PasswordTextBox"
            SetFocusOnError="true" Display="Dynamic" CssClass="error" />
          <span class="instructions">
            <%= HtmlLocalise("PasswordInstructions.Label", "6-20 characters; must include at least one number; password is case-sensitive.")%></span>
        </td>
      </tr>
      <tr style="vertical-align:top;">
        <td class="label" style="white-space: nowrap">
          <%= HtmlRequiredLabel(ConfirmPasswordTextBox, "ConfirmPassword.Label", "Confirm password")%>
        </td>
        <td>
          <asp:TextBox ID="ConfirmPasswordTextBox" runat="server" ClientIDMode="Static" Width="98%"
            TextMode="Password" TabIndex="4" MaxLength="20" />
          <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" ControlToValidate="ConfirmPasswordTextBox"
            SetFocusOnError="true" Display="Dynamic" CssClass="error" />
          <asp:CompareValidator ID="ConfirmPasswordCompare" runat="server" ControlToValidate="ConfirmPasswordTextBox"
            ControlToCompare="PasswordTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
        </td>
      </tr>
    </asp:PlaceHolder>
    <tr id="FEINRow" runat="server" style="vertical-align:top;">
      <td class="label" style="white-space: nowrap">
        <%= HtmlLabel(FederalEmployerIdentificationNumberTextBox, "FederalEmployerIdentificationNumber.Label", "Federal Employer ID (FEIN)", App.Settings.DisplayFEIN == ControlDisplayType.Mandatory)%>
      </td>
      <td>
        <asp:TextBox ID="FederalEmployerIdentificationNumberTextBox" runat="server" ClientIDMode="Static"
          Width="98%" TabIndex="5" MaxLength="10" />
        <asp:RequiredFieldValidator ID="FederalEmployerIdentificationNumberRequired" runat="server"
          ControlToValidate="FederalEmployerIdentificationNumberTextBox" SetFocusOnError="true"
          Display="Dynamic" CssClass="error" />
        <asp:RegularExpressionValidator ID="FederalEmployerIdentificationNumberRegEx" runat="server"
          ControlToValidate="FederalEmployerIdentificationNumberTextBox" SetFocusOnError="true"
          Display="Dynamic" CssClass="error" />
        <span class="instructions">
          <%= HtmlLocalise("FederalEmployerIdentificationNumber.Label", "Two digits followed by - then another seven digits.")%></span>
        <asp:LinkButton runat="server" ID="FeinHelpLinkButton" OnClick="FeinHelpLinkButton_Clicked"
          CausesValidation="False"><%= HtmlLocalise("FeinHelpLinkButton.Text", "Need an FEIN?")%></asp:LinkButton>
      </td>
    </tr>
    <tr id="FEINConfirmRow" runat="server" style="vertical-align:top;">
      <td class="label" style="white-space: nowrap">
        <%= HtmlLabel(ConfirmFederalEmployerIdentificationNumberTextBox, "ConfirmFederalEmployerIdentificationNumber.Label", "Confirm (FEIN)", App.Settings.DisplayFEIN == ControlDisplayType.Mandatory)%>
      </td>
      <td>
        <asp:TextBox ID="ConfirmFederalEmployerIdentificationNumberTextBox" runat="server"
          ClientIDMode="Static" Width="98%" TabIndex="6" MaxLength="10" />
        <asp:RequiredFieldValidator ID="ConfirmFederalEmployerIdentificationNumberRequired"
          runat="server" ControlToValidate="ConfirmFederalEmployerIdentificationNumberTextBox"
          SetFocusOnError="true" Display="Dynamic" CssClass="error" />
        <asp:CompareValidator ID="ConfirmFederalEmployerIdentificationNumberCompare" runat="server"
          ControlToValidate="ConfirmFederalEmployerIdentificationNumberTextBox" ControlToCompare="FederalEmployerIdentificationNumberTextBox"
          SetFocusOnError="true" Display="Dynamic" CssClass="error" />
      </td>
    </tr>
    <tr id="RecaptchaRow" clientidmode="Static" runat="server" visible="False">
      <td style="white-space: nowrap" class="label" colspan="2">
        <%= HtmlRequiredLabel("g-recaptcha-response", "Recaptcha.Label", "Authenticate user")%>
				<br/>
				<div class="g-recaptcha" data-sitekey='<%= PublicKey%>'></div>
                 <%--<asp:CustomValidator ID="RecaptchaValidator" runat="server" SetFocusOnError="true"
          Display="Dynamic" CssClass="error" ClientValidationFunction="EmployerRegistration_ValidatRecaptcha"
          ValidateEmptyText="true" />--%>
        <asp:Label runat="server" ID="RecaptchaErrorMessage" CssClass="error" Visible="false"
          ClientIDMode="Static" />
      </td>
    </tr>
  </table>
</div>
<script type="text/javascript">
  $(document).ready(
		function () {
		  $("#FederalEmployerIdentificationNumberTextBox").mask("99-9999999", { placeholder: " " });
		  $("#ConfirmFederalEmployerIdentificationNumberTextBox").mask("99-9999999", { placeholder: " " });

		  //Recaptcha Accessibility
		  if ($('#recaptcha_reload').length > 0) {
		    //AccessibleRecaptcha('#recaptcha_reload');
		    //AccessibleRecaptcha('#recaptcha_switch_audio');
		    //AccessibleRecaptcha('#recaptcha_whatsthis');

		    // Change Style to Match Talent
		    $('#recaptcha_privacy > a').css({ color: '#BFBFBF' });
		  }
		}
	);
  // Changes recaptcha buttons to be more accessible
  function AccessibleRecaptcha(id) {
    var attrs = {};

    $.each($(id)[0].attributes, function (idx, attr) {
      attrs[attr.nodeName] = attr.nodeValue;
    });

    $(id).replaceWith(function () {
      return $("<input />", attrs).append($(id).contents());
    });
    $(id).prop("type", "image");
    $(id).parent().attr("onclick", "return false;");
    $(id).parent().attr("onkeypress", "return false;");
  }

  function EmployerRegistration_ValidatRecaptcha(source, arguments) {
    arguments.IsValid = $("#g-recaptcha-response").val().trim().length > 0;
    $("#<%=RecaptchaErrorMessage.ClientID %>").hide();
  }
</script>
