﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobWizardEducation.ascx.cs" Inherits="Focus.Web.Code.Controls.User.JobWizardEducation" %>
<%@ Register src="~/Code/Controls/User/JobWizardStep1.ascx" tagname="JobWizardStep1" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobWizardStep2.ascx" tagname="JobWizardStep2" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobWizardStep3Path1.ascx" tagname="JobWizardStep3Path1" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobWizardStep3Path2Part1.ascx" tagname="JobWizardStep3Path2Part1" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobWizardStep3Path2Part2.ascx" tagname="JobWizardStep3Path2Part2" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobWizardStep4.ascx" tagname="JobWizardStep4" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/Education/JobWizardEducationStep5.ascx" tagname="JobWizardStep5" tagprefix="uc" %>

<table width="100%">
	<tr>
		<td><h1><%= HtmlLocalise("CreateJobOrInternship.Header", "Create a new job or internship to post") %></h1></td>
		<td align="right">
			<asp:Button ID="PreviewButtonTop" runat="server" SkinID="Button2" OnClick="PreviewButton_Click" UseSubmitBehavior="False" Visible="false" />
			<asp:Literal ID="PreviewSpacerTop" runat="server" Visible="false">&nbsp;&nbsp;</asp:Literal>
			<asp:Button ID="PreviousButtonTop" runat="server" SkinID="Button3" OnClick="PreviousButton_Click" CausesValidation="false" UseSubmitBehavior="False" />&nbsp;&nbsp;
			<asp:Button ID="NextButtonTop" runat="server" SkinID="Button2" OnClick="NextButton_Click" UseSubmitBehavior="False" />
		</td>
	</tr>
</table>

<%-- Progress Bar --%> 
<div class="progressBarWrap">
  <table class="progressBar" role="presentation"> 
	  <tr>
		  <td class="first" width="16%" nowrap="nowrap"><span ID="Step1ProgressBarItem" runat="server" EnableViewState="False"><asp:LinkButton ID="Step1ProgressBarItemLinkButton" runat="server" OnCommand="StepProgressBarLinkButton_Command" CommandArgument="1" /><asp:Literal ID="Step1ProgressBarItemLiteral" runat="server" /></span></td>
		  <td width="16%" nowrap="nowrap"><span ID="Step2Step3ProgressBarItem" runat="server" EnableViewState="False"><asp:LinkButton ID="Step2Step3ProgressBarItemLinkButton" runat="server" OnCommand="StepProgressBarLinkButton_Command" CommandArgument="2" /><asp:Literal ID="Step2Step3ProgressBarItemLiteral" runat="server" /></span></td>
		  <td width="16%" nowrap="nowrap"><span ID="Step4ProgressBarItem" runat="server" EnableViewState="False"><asp:LinkButton ID="Step4ProgressBarItemLinkButton" runat="server" OnCommand="StepProgressBarLinkButton_Command" CommandArgument="4" /><asp:Literal ID="Step4ProgressBarItemLiteral" runat="server" /></span></td>
		  <td  class="last" width="16%" nowrap="nowrap"><span ID="Step5ProgressBarItem" runat="server" EnableViewState="False"><asp:LinkButton ID="Step5ProgressBarItemLinkButton" runat="server" OnCommand="StepProgressBarLinkButton_Command" CommandArgument="5" /><asp:Literal ID="Step5ProgressBarItemLiteral" runat="server" /></span></td>
		</tr>
  </table>
</div>

<div style="clear:both;">
	<uc:JobWizardStep1 ID="Step1" runat="server" />
	<uc:JobWizardStep2 ID="Step2" runat="server" OnPathSelected="Step2_PathSelected" />
	<uc:JobWizardStep3Path1 ID="Step3Path1" runat="server" OnGoToStep="Step3Path1_OnGoToStep" />
	<uc:JobWizardStep3Path2Part1 ID="Step3Path2Part1" runat="server" />
	<uc:JobWizardStep3Path2Part2 ID="Step3Path2Part2" runat="server" OnGoToStep="Step3Path2Part2_OnGoToStep" />
	<uc:JobWizardStep4 ID="Step4" runat="server" />				
	<uc:JobWizardStep5 ID="Step5" runat="server" />
</div>

<br />
<br />

<table style="width: 100%;" role="presentation">
	<tr>
		<td/>
		<td style="text-align: right;">
			<asp:Button ID="PreviewButtonBottom" runat="server" SkinID="Button2" OnClick="PreviewButton_Click" UseSubmitBehavior="False" Visible="false" />
			<asp:Literal ID="PreviewSpacerBottom" runat="server" Visible="false">&nbsp;&nbsp;</asp:Literal>
			<asp:Button ID="PreviousButtonBottom" runat="server" SkinID="Button3" OnClick="PreviousButton_Click" CausesValidation="false" UseSubmitBehavior="False" />&nbsp;&nbsp;
			<asp:Button ID="NextButtonBottom" runat="server" SkinID="Button2" OnClick="NextButton_Click" UseSubmitBehavior="False" />
		</td>
	</tr>
</table>