﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Focus.Web.Code.Controls.User {
    
    
    public partial class JobWizardEducation {
        
        /// <summary>
        /// PreviewButtonTop control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button PreviewButtonTop;
        
        /// <summary>
        /// PreviewSpacerTop control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal PreviewSpacerTop;
        
        /// <summary>
        /// PreviousButtonTop control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button PreviousButtonTop;
        
        /// <summary>
        /// NextButtonTop control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button NextButtonTop;
        
        /// <summary>
        /// Step1ProgressBarItem control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl Step1ProgressBarItem;
        
        /// <summary>
        /// Step1ProgressBarItemLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton Step1ProgressBarItemLinkButton;
        
        /// <summary>
        /// Step1ProgressBarItemLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Step1ProgressBarItemLiteral;
        
        /// <summary>
        /// Step2Step3ProgressBarItem control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl Step2Step3ProgressBarItem;
        
        /// <summary>
        /// Step2Step3ProgressBarItemLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton Step2Step3ProgressBarItemLinkButton;
        
        /// <summary>
        /// Step2Step3ProgressBarItemLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Step2Step3ProgressBarItemLiteral;
        
        /// <summary>
        /// Step4ProgressBarItem control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl Step4ProgressBarItem;
        
        /// <summary>
        /// Step4ProgressBarItemLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton Step4ProgressBarItemLinkButton;
        
        /// <summary>
        /// Step4ProgressBarItemLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Step4ProgressBarItemLiteral;
        
        /// <summary>
        /// Step5ProgressBarItem control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl Step5ProgressBarItem;
        
        /// <summary>
        /// Step5ProgressBarItemLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton Step5ProgressBarItemLinkButton;
        
        /// <summary>
        /// Step5ProgressBarItemLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Step5ProgressBarItemLiteral;
        
        /// <summary>
        /// Step1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.JobWizardStep1 Step1;
        
        /// <summary>
        /// Step2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.JobWizardStep2 Step2;
        
        /// <summary>
        /// Step3Path1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.JobWizardStep3Path1 Step3Path1;
        
        /// <summary>
        /// Step3Path2Part1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.JobWizardStep3Path2Part1 Step3Path2Part1;
        
        /// <summary>
        /// Step3Path2Part2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.JobWizardStep3Path2Part2 Step3Path2Part2;
        
        /// <summary>
        /// Step4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.JobWizardStep4 Step4;
        
        /// <summary>
        /// Step5 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.Education.JobWizardEducationStep5 Step5;
        
        /// <summary>
        /// PreviewButtonBottom control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button PreviewButtonBottom;
        
        /// <summary>
        /// PreviewSpacerBottom control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal PreviewSpacerBottom;
        
        /// <summary>
        /// PreviousButtonBottom control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button PreviousButtonBottom;
        
        /// <summary>
        /// NextButtonBottom control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button NextButtonBottom;
    }
}
