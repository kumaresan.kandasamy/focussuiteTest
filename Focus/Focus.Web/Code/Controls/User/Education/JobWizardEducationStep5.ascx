﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobWizardEducationStep5.ascx.cs" Inherits="Focus.Web.Code.Controls.User.Education.JobWizardEducationStep5" %>
<%@ Register src="~/Code/Controls/User/JobWizardLocationSelector.ascx" tagname="JobWizardLocationSelector" tagprefix="uc" %>
<table style="width:100%;" role="presentation">
	<tr>
		<td>
			<asp:Panel ID="WorkLocationHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
				<table role="presentation" class="accordionTable">
					<tr  class="multipleAccordionTitle">
						<td>
							<asp:Image ID="WorkLocationHeaderImage" runat="server" />
							&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("WorkLocation.Label", "Work location")%></a></span>&nbsp;<%= HtmlRequiredFieldsInstruction() %>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<asp:UpdatePanel runat="server" ID="WorkLocationUpdatePanel" UpdateMode="Conditional">
				<ContentTemplate>
					<div class="singleAccordionContentWrapper">
					<asp:Panel ID="WorkLocationPanel" runat="server" CssClass="singleAccordionContent">
						<table style="width:100%;" role="presentation">
							<tr>
								<td style="width:50%; vertical-align:top;">
									<%= HtmlRequiredLabel(WorkLocationDropDown, "WorkLocations.Label", "Location(s) where employee will report to work")%>
									<br/><br />
									<asp:DropDownList runat="server" ID="WorkLocationDropDown" ClientIDMode="Static" CausesValidation="False" AutoPostBack="True" OnChange="JobWizardStep5_DisableLocationSelectorValidator()" OnSelectedIndexChanged="WorkLocationDropDown_SelectedIndexChanged"/>
									<br /><br /><asp:CheckBox id="PublicTransitAccessibleCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" />
								</td>
								<td  style="width:50%;">
									<uc:JobWizardLocationSelector ID="LocationSelector" runat="server" Visible="false" ClientIDMode="Static"/>
									<asp:CustomValidator ID="LocationSelectorValidator" runat="server" CssClass="error" ClientValidationFunction="validateLocations" ControlToValidate="WorkLocationDropDown" />
								</td>  
							</tr>
						</table>
					</asp:Panel>
					</div>
					<act:CollapsiblePanelExtender ID="WorkLocationPanelExtender" runat="server" TargetControlID="WorkLocationPanel" ExpandControlID="WorkLocationHeaderPanel" 
																					CollapseControlID="WorkLocationHeaderPanel" Collapsed="false" ImageControlID="WorkLocationHeaderImage" 
																					SuppressPostBack="true"/>
				</ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="workLocationDropDown" />
				</Triggers>
			</asp:UpdatePanel>
	
  <asp:Panel ID="SalaryHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
				<table role="presentation" class="accordionTable">
					<tr  class="multipleAccordionTitle">
						<td>
							<asp:Image ID="SalaryHeaderImage" runat="server" />
							&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Salary.Label", "Salary")%></a></span>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<div class="singleAccordionContentWrapper">
				<asp:Panel ID="SalaryPanel" runat="server" CssClass="singleAccordionContent">
					<table role="presentation">
						<tr>
							<td style="vertical-align:top;"><%= HtmlLabel("Salary.Text", "Salary")%>&nbsp;</td>
							<td colspan="3">
								<asp:Label runat="server" ID="SalaryTypeLabel"/>
							</td>
						</tr>
						<asp:PlaceHolder runat="server" ID="SalaryRangeRow">
						<tr>
							<td style="vertical-align:top;"><%= HtmlLabel("SalaryRange.Label", "Salary range")%>&nbsp;</td>
							<td>
								<asp:TextBox runat="server" ID="MinimumSalaryTextBox" ClientIDMode="Static" MaxLength="10"/>
								<br />
								<i><%= HtmlLocalise("Minimum.Text", "minimum")%></i>
							</td>
							<td>
								<asp:TextBox runat="server" ID="MaximumSalaryTextBox" ClientIDMode="Static" MaxLength="10" />
								<br />
								<i><focus:LocalisedLabel runat="server" ID="MaximumLabel" DefaultText="maximum"/></i>
							</td>
							<td style="vertical-align:top;">
								<asp:DropDownList runat="server" ID="SalaryFrequencyDropDown" ClientIDMode="Static" />
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<asp:CustomValidator runat="server" ClientValidationFunction="JobWizardEducationStep5_ValidateMinimumSalaryAmount" ID="MinimumSalaryValueValidator" ControlToValidate="MinimumSalaryTextBox" CssClass="error" Display="Dynamic" />
								<asp:CompareValidator runat="server" Operator="GreaterThanEqual" ID="MinimumSalaryMaximumSalaryValueValidator" ControlToValidate="MaximumSalaryTextBox" ControlToCompare="MinimumSalaryTextBox" Type="Double" SetFocusOnError="True" CssClass="error" Display="Dynamic" />
								<asp:CustomValidator runat="server" ClientValidationFunction="JobWizardEducationStep5_ValidateMaximumSalaryAmount" ID="MaximumSalaryValueValidator" ControlToValidate="MaximumSalaryTextBox" CssClass="error" Display="Dynamic" />								<asp:CustomValidator runat="server" ID="SalaryFrequencyDropDownValidator" ControlToValidate="SalaryFrequencyDropDown" ClientValidationFunction="validateSalaryFrequencyDropDown" ValidateEmptyText="True" SetFocusOnError="True" CssClass="error" Display="Dynamic" />
								<focus:MultipleFieldsValidator ID="MinimumSalaryMaximumSalaryValueBothRequiredValidator" ControlsToValidate="MinimumSalaryTextBox,MaximumSalaryTextBox" CssClass="error" Display="Dynamic" Condition="AND" runat="server"></focus:MultipleFieldsValidator>
							</td>
						</tr>
						</asp:PlaceHolder>
						<tr id="OtherSalaryRow" runat="server">
							<td style="vertical-align:top;"><%= HtmlLabel("OtherSalary.Label", "Other salary")%>&nbsp;</td>
							<td colspan="3">
								<asp:TextBox runat="server" TextMode="MultiLine" Columns="5" Rows="5" ID="OtherSalaryTextBox" ClientIDMode="Static" MaxLength="10"/>
							</td>
						</tr>
            <asp:PlaceHolder runat="server" ID="MeetsMinimumWageRequirementPlaceHolder">
              <tr>
							  <td colspan="4">
							    <asp:CheckBox ID="MeetsMinimumWageRequirementCheckBox" runat="server"/><br />
							  </td>
              </tr>
              <tr>
							  <td colspan="4">
							    <focus:CheckBoxCustomValidator runat="server" ID="MeetsMinimumWageRequirementValidator" ControlToValidate="MeetsMinimumWageRequirementCheckBox" ClientValidationFunction="JobWizardEducationStep5_ValidateMeetsMinimumWageRequirement" ValidateEmptyText="True" EnableClientScript="True" SetFocusOnError="True" CssClass="error" Display="Dynamic" />
							  </td>
              </tr>
            </asp:PlaceHolder>
						<tr>
							<td colspan="4">
								<asp:CheckBox ID="HideSalaryCheckbox" runat="server" ClientIDMode="Static" /><br />
                                <asp:CustomValidator runat="server" ID="CheckSalaryFilledInValidator" ControlToValidate="MinimumSalaryTextBox" SetFocusOnError="True" Display="Dynamic" CssClass="error" ClientValidationFunction="validateSalaryWhenOnPosting" ValidateEmptyText="True"></asp:CustomValidator>
								<asp:CheckBox id="CommissionBasedCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" />
							</td>
						</tr>
					</table>
				</asp:Panel>
			</div>
			<act:CollapsiblePanelExtender ID="SalaryPanelExtender" runat="server" TargetControlID="SalaryPanel" ExpandControlID="SalaryHeaderPanel" 
																					CollapseControlID="SalaryHeaderPanel" Collapsed="false" ImageControlID="SalaryHeaderImage" 
																					SuppressPostBack="true" />
                                          
                                          
     <asp:Panel ID="HoursHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
				<table role="presentation" class="accordionTable">
					<tr  class="multipleAccordionTitle">
						<td>
							<asp:Image ID="HoursHeaderImage" runat="server" />
							&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Hours.Label", "Hours")%></a></span>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<div class="singleAccordionContentWrapper">
				<asp:Panel ID="HoursPanel" runat="server" CssClass="singleAccordionContent">
					<table role="presentation">
						<tr>
							<td><%= HtmlLabel("StartDate.Label", "Start date") %></td>
							<td><asp:TextBox ID="StartDateTextBox" runat="server"  ClientIDMode="Static" /></td>
							<td>
								<table role="presentation">
									<tr>
										<td>
											<asp:CompareValidator ID="StartDateCompareValidator" runat="server" ErrorMessage="CompareValidator" ControlToValidate="StartDateTextBox" Type="Date" Operator="GreaterThan" CssClass="error" Display="Dynamic" />
										</td>
									</tr>
									<tr>
										<td><asp:CustomValidator ID="StartDateValidator" runat="server" ControlToValidate="StartDateTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ClientValidationFunction="ValidateDate" ValidateEmptyText="true" /></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td><%= HtmlLabel("EndDate.Label", "End date") %></td>
							<td><asp:TextBox ID="EndDateTextBox" runat="server"  ClientIDMode="Static" /></td>
							<td>
								<table role="presentation">
									<tr>
										<td>
  										<asp:CompareValidator ID="EndDateCompareValidator" runat="server" ErrorMessage="CompareValidator" ControlToValidate="EndDateTextBox" ControlToCompare="StartDateTextBox" Type="Date" Operator="GreaterThanEqual" CssClass="error" Display="Dynamic" />
										</td>
									</tr>
									<tr>
										<td><asp:CustomValidator ID="EndDateValidator" runat="server" ControlToValidate="EndDateTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ClientValidationFunction="ValidateDate" ValidateEmptyText="true" /></td>
									</tr>
								</table>
							</td>
						</tr>
						 <tr>
							<td style="vertical-align: top">
								<asp:Literal runat="server" ID="HoursPerWeekHeader"></asp:Literal>
							</td>
							<td colspan="3">
							  <asp:PlaceHolder runat="server" ID="MinimumHoursPerWeekPanel" Visible="False">
							    <div style="display:inline-block">
                    <asp:TextBox runat="server" ID="MinimumHoursPerWeekTextBox" Text="" ClientIDMode="Static" />
                    <br />
								    <i><%= HtmlLocalise("Minimum.Text", "minimum")%></i>
                  </div>
							  </asp:PlaceHolder>
                <div style="display:inline-block">
								  <asp:TextBox runat="server" ID="HoursPerWeekTextBox" Text="" ClientIDMode="Static" />
                  <asp:PlaceHolder runat="server" ID="MaximumActualLabelPanel">
                    <br />
								    <i><%= HtmlLocalise("MaximumActual.Text", "maximum/actual")%></i>
                  </asp:PlaceHolder>
                </div>
                <asp:PlaceHolder runat="server" ID="MinimumHoursValidatorsPanel" Visible="False">
                  <br />
								  <asp:RangeValidator ID="MinimumHoursPerWeekRangeValidator" runat="server" ControlToValidate="MinimumHoursPerWeekTextBox"
									  CssClass="error" MaximumValue="168" MinimumValue="0" Type="Double" SetFocusOnError="True" Display="Dynamic" />
                  <asp:CustomValidator runat="server" ID="HoursPerWeekCustomValidator" ControlToValidate="HoursPerWeekTextBox" 
                    SetFocusOnError="True" CssClass="error" Display="Dynamic"
                    ValidateEmptyText="True" ClientValidationFunction="JobWizardEducationStep5_ValidateHoursPerWeek" />
                  <asp:CustomValidator runat="server" ID="MinimumHoursPerWeekCustomValidator" ControlToValidate="MinimumHoursPerWeekTextBox" 
                    SetFocusOnError="True" CssClass="error" Display="None"
                    ValidateEmptyText="True" ClientValidationFunction="JobWizardEducationStep5_ValidateMinimumHoursPerWeek" />
                  <asp:CompareValidator runat="server" Operator="LessThan" ID="HoursPerWeekCompareValidator"
									  ControlToValidate="MinimumHoursPerWeekTextBox" ControlToCompare="HoursPerWeekTextBox"
									  Type="Double" SetFocusOnError="True" CssClass="error" />
                </asp:PlaceHolder>
								<asp:RangeValidator ID="HoursPerWeekRangeValidator" runat="server" ControlToValidate="HoursPerWeekTextBox"
									CssClass="error" MaximumValue="168" MinimumValue="0" Type="Double" SetFocusOnError="True" />
							</td>
						</tr>
						</table>
					 <act:CalendarExtender ID="StartDateCalendarExtender" runat="server" TargetControlID="StartDateTextBox" />
					 <act:CalendarExtender ID="EndDateCalendarExtender" runat="server" TargetControlID="EndDateTextBox" />
				</asp:Panel>
			</div>
			<act:CollapsiblePanelExtender ID="HoursPanelExtender" runat="server" TargetControlID="HoursPanel" ExpandControlID="HoursHeaderPanel" 
																					CollapseControlID="HoursHeaderPanel" Collapsed="false" ImageControlID="HoursHeaderImage" 
																					SuppressPostBack="true" />
                                          
      <asp:Panel ID="ApplicationClosingDateHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
				<table role="presentation" class="accordionTable">
					<tr  class="multipleAccordionTitle">
						<td>
							<asp:Image ID="ApplicationClosingDateHeaderImage" runat="server" />
							&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ApplicationClosingDate.Label", "Application closing date")%></a></span>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<div class="singleAccordionContentWrapper">
				<asp:Panel ID="ApplicationClosingDatePanel" runat="server" CssClass="singleAccordionContent">
					<table role="presentation">
					<tr>
							<td style="width:140px;"><%= HtmlRequiredLabel(ApplicationClosingDateTextBox, "ApplicationClosingDate.Label", "Application closing date")%></td>
							<td><asp:TextBox ID="ApplicationClosingDateTextBox" runat="server"  ClientIDMode="Static" /></td>
							<td>
								<table role="presentation">
									<tr>
										<td><asp:RequiredFieldValidator ID="ApplicationClosingDateRequired" runat="server" ControlToValidate="ApplicationClosingDateTextBox" CssClass="error" Display="Dynamic" /></td>
									</tr>
									<tr>
										<td><asp:CustomValidator ID="ClosingDateValidator" runat="server" ControlToValidate="ApplicationClosingDateTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ClientValidationFunction="ValidateDate" ValidateEmptyText="true" /></td>
									</tr>
									<tr>
										<td><asp:CompareValidator ID="ApplicationClosingDateCompareValidator" runat="server" ErrorMessage="CompareValidator" ControlToValidate="ApplicationClosingDateTextBox" Type="Date" Operator="LessThanEqual" CssClass="error" Display="Dynamic" /></td>
									</tr>
									<tr>
										<td><asp:CompareValidator ID="ApplicationClosingDateGreaterThanTodayCompareValidatior" runat="server" ErrorMessage="CompareValidator" ControlToValidate="ApplicationClosingDateTextBox" Type="Date" Operator="GreaterThan" CssClass="error" Display="Dynamic" /></td>
									</tr>
								</table>
							</td>
						</tr>
					 </table>
				 <act:CalendarExtender ID="ApplicationClosingDateCalendarExtender" runat="server" TargetControlID="ApplicationClosingDateTextBox" />
				</asp:Panel>
			</div>
			<act:CollapsiblePanelExtender ID="ApplicationClosingDatePanelExtender" runat="server" TargetControlID="ApplicationClosingDatePanel" ExpandControlID="ApplicationClosingDateHeaderPanel" 
																					CollapseControlID="ApplicationClosingDateHeaderPanel" Collapsed="false" ImageControlID="ApplicationClosingDateHeaderImage" 
																					SuppressPostBack="true" />
                                          
     	<asp:Panel ID="NumberOfOpeningsHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
				<table role="presentation" class="accordionTable">
					<tr  class="multipleAccordionTitle">
						<td>
							<asp:Image ID="NumberOfOpeningsHeaderImage" runat="server" />
							&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("NumberOfOpeningsPanel.Label", "Number of openings")%></a></span>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<div class="singleAccordionContentWrapper">
				<asp:Panel ID="NumberOfOpeningsPanel" runat="server" CssClass="singleAccordionContent">
					<table role="presentation">
						<tr>
							<td style="width:140px;"><%= HtmlRequiredLabel(NumberOfOpeningsTextBox, "NumberOfOpenings.Label", "Number of openings")%></td>
							<td><asp:TextBox ID="NumberOfOpeningsTextBox" runat="server"  ClientIDMode="Static" Width="30px" /></td>
							<td>
								<asp:RequiredFieldValidator ID="NumberOfOpeningsRequired" runat="server" ControlToValidate="NumberOfOpeningsTextBox" CssClass="error" Display="Dynamic" />
								<asp:CompareValidator runat="server" Operator="GreaterThan" ValueToCompare="0" ID="MinimumNumberOfOpeningsValidator" ControlToValidate="NumberOfOpeningsTextBox" Type="Integer" SetFocusOnError="True" CssClass="error" Display="Dynamic" />
								<asp:CompareValidator runat="server" Operator="LessThanEqual" ValueToCompare="200" ID="MaximumNumberOfOpeningsValidator" ControlToValidate="NumberOfOpeningsTextBox" Type="Integer" SetFocusOnError="True" CssClass="error" Display="Dynamic" />
								<act:MaskedEditExtender ID="NumberOfOpeningsMaskedEdit" runat="server" MaskType="Number" TargetControlID="NumberOfOpeningsTextBox" PromptCharacter=" " ClearMaskOnLostFocus="false" AutoComplete="false" EnableViewState="true" ClearTextOnInvalid="false" Mask="999"/>
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<asp:CheckBox ID="HideOpeningsCheckbox" runat="server" ClientIDMode="Static" /><br />
							</td>
            </tr>
					</table>
				</asp:Panel>
			</div>
			<act:CollapsiblePanelExtender ID="NumberOfOpeningsPanelExtender" runat="server" TargetControlID="NumberOfOpeningsPanel" ExpandControlID="NumberOfOpeningsHeaderPanel" 
																					CollapseControlID="NumberOfOpeningsHeaderPanel" Collapsed="false" ImageControlID="NumberOfOpeningsHeaderImage" 
																					SuppressPostBack="true" />
                                          	<asp:Panel ID="InterviewContactPreferencesHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
				<table role="presentation" class="accordionTable">
					<tr  class="multipleAccordionTitle">
						<td>
							<asp:Image ID="InterviewContactPreferencesHeaderImage" runat="server" />&nbsp;&nbsp;<%= HtmlLocalise("InterviewContactPreferences.Label", "Interview contact preferences")%>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<div class="singleAccordionContentWrapper">
				<asp:Panel ID="InterviewContactPreferencesPanel" runat="server" CssClass="singleAccordionContent">
				<p><%= HtmlRequiredLabel(InterviewContactPreferences,"InterviewContactPreferences.Text", "Please check all that apply. At least one contact method must be selected.")%></p>
				<table role="presentation" style="width:100%;" id="InterviewContactPreferences" runat="server">
					<tr>
						<td style="vertical-align:top; white-space:nowrap;" colspan="4"><asp:RadioButton runat="server" GroupName="InterviewPreferences" ID="InterviewPreferenceFocusTalent" ClientIDMode="Static"/>&nbsp;&nbsp;<focus:CheckBoxCustomValidator ID="InterviewContactPreferencesValidator" runat="server" ControlToValidate="InterviewPreferenceFocusTalent" ClientValidationFunction="validateInterviewContactPreferences" 
																							ValidateEmptyText="True" CssClass="error"/></td>
					</tr>
					<tr>
						<td style="vertical-align:top; white-space:nowrap;" colspan="4" ><asp:RadioButton runat="server" GroupName="InterviewPreferences" ID="InterviewPreferenceOther" ClientIDMode="Static"/></td>
					</tr>
					<tr>
						<td style="vertical-align:top; white-space:nowrap;" ><asp:CheckBox id="EmailResumeCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" />&nbsp;</td>
						<td>
							<%= HtmlInFieldLabel("EmailAddressTextBox", "EmailAddress.InlineLabel", "email address", 300)%>
							<asp:TextBox ID="EmailAddressTextBox" runat="server" Width="300" ClientIDMode="Static" MaxLength="255" />
							<asp:CustomValidator ID="EmailAddressValidator" runat="server" ControlToValidate="EmailAddressTextBox" ClientValidationFunction="validateEmailAddress" ValidateEmptyText="True" Display="Dynamic" CssClass="error" />
              <asp:RegularExpressionValidator ID="EmailAddressRegEx" runat="server" ControlToValidate="EmailAddressTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
						</td>
						<td rowspan="6" style="vertical-align:top;white-space:nowrap;"><%= HtmlLabel("OtherInstructions.Text", "Other or specific instructions")%>&nbsp;</td>
						<td rowspan="6" style="width:100%; vertical-align:top; text-align:left"><asp:TextBox ID="OtherInstructionsTextBox" runat="server" TextMode="MultiLine" Rows="4" ClientIDMode="Static" Width="300" /></td>

					</tr>
					<tr>
						<td style="vertical-align:top; white-space:nowrap;" ><asp:CheckBox id="ApplyOnlineCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" />&nbsp;</td>
						<td>
							<%= HtmlInFieldLabel("ApplyOnlineTextBox", "ApplyOnline.InlineLabel", "enter URL beginning with http:// or https://", 300)%>
							<asp:TextBox ID="ApplyOnlineTextBox" runat="server" Width="300" ClientIDMode="Static" MaxLength="255" />
							<asp:CustomValidator ID="ApplyOnlineValidator" runat="server" ControlToValidate="ApplyOnlineTextBox" ClientValidationFunction="validateApplyOnline" ValidateEmptyText="True" CssClass="error" />
						</td>
					</tr>
					<tr>
						<td style="vertical-align:top;  white-space:nowrap;" ><asp:CheckBox id="MailResumeCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" />&nbsp;</td>
						<td>
							<%= HtmlInFieldLabel("MailResumeTextBox", "MailResume.InlineLabel", "address", 300)%>
							<asp:TextBox ID="MailResumeTextBox" runat="server" Width="300" TextMode="MultiLine" rows="4" ClientIDMode="Static" />
							<asp:CustomValidator ID="MailResumeValidator" runat="server" ControlToValidate="MailResumeTextBox" ClientValidationFunction="validateMailResume" ValidateEmptyText="True" CssClass="error" />
						</td>
					</tr>
					<tr>
						<td style="vertical-align:top; white-space:nowrap;"><asp:CheckBox id="FaxResumeCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" />&nbsp;</td>
						<td>
							<%= HtmlInFieldLabel("FaxResumeTextBox", "FaxResume.InlineLabel", "fax resume", 300)%>
							<asp:TextBox ID="FaxResumeTextBox" runat="server" Width="300" ClientIDMode="Static" MaxLength="25" />
							<asp:CustomValidator ID="FaxResumeValidator" runat="server" ControlToValidate="FaxResumeTextBox" ClientValidationFunction="validateFaxResume" ValidateEmptyText="True" CssClass="error" />
						</td>
					</tr>
					<tr>
						<td style="vertical-align:top; white-space:nowrap;"><asp:CheckBox id="PhoneForAppointmentCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" />&nbsp;</td>
						<td>
							<%= HtmlInFieldLabel("PhoneForAppointmentTextBox", "PhoneForAppointment.InlineLabel", "phone number", 300)%>
							<asp:TextBox ID="PhoneForAppointmentTextBox" runat="server" Width="300" ClientIDMode="Static" MaxLength="25" />
							<asp:CustomValidator ID="PhoneForAppointmentValidator" runat="server" ControlToValidate="PhoneForAppointmentTextBox" ClientValidationFunction="validatePhoneForAppointment" ValidateEmptyText="True" 
																		CssClass="error" />
						</td>
					</tr>
					<tr>
						<td style="vertical-align:top; white-space:nowrap;"><asp:CheckBox id="InPersonCheckBox" TextAlign="Right" runat="server" ClientIDMode="Static" />&nbsp;</td>
						<td>
							<%= HtmlInFieldLabel("InPersonTextBox", "InPerson.InlineLabel", "contact name and address", 300)%>
							<asp:TextBox ID="InPersonTextBox" runat="server" Width="300" TextMode="MultiLine" rows="4" ClientIDMode="Static" />
							<asp:CustomValidator ID="InPersonValidator" runat="server" ControlToValidate="InPersonTextBox" ClientValidationFunction="validateInPerson" ValidateEmptyText="True" CssClass="error" />
						</td>
					</tr>
				</table>
				</asp:Panel>
			</div>
			<act:CollapsiblePanelExtender ID="InterviewContactPreferencesPanelExtender" runat="server" TargetControlID="InterviewContactPreferencesPanel" ExpandControlID="InterviewContactPreferencesHeaderPanel" 
																					CollapseControlID="InterviewContactPreferencesHeaderPanel" Collapsed="false" ImageControlID="InterviewContactPreferencesHeaderImage" 
																					SuppressPostBack="true" />
		
			<asp:Panel ID="ScreeningPreferencesHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
				<table role="presentation" class="accordionTable">
					<tr  class="multipleAccordionTitle">
						<td>
							<asp:Image ID="ScreeningPreferencesHeaderImage" runat="server" />
							&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ScreeningPreferences.Label", "Screening preferences")%></a></span>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<div class="singleAccordionContentWrapper">
				<asp:Panel ID="ScreeningPreferencesPanel" runat="server" CssClass="singleAccordionContent">
					<table role="presentation">
						<tr id="AllowUnqualifiedApplicationsRow" runat="server">
							<td>
							  <asp:RadioButton ID="AllowUnqualifiedApplicationsRadioButton" runat="server" GroupName="ScreenPreferences" />
							</td>
              <td>
                <%= HtmlLocalise(ScreeningPreferences.AllowUnqualifiedApplications.ToString(), "Allow interested #CANDIDATETYPES#:LOWER to apply")%>
                <div style="display: inline-block;"><%= HtmlTooltipster("tooltipWithArrow", "AllowUnqualifiedApplications.Tooltip", @"All #CANDIDATETYPES#:LOWER can apply regardless of their qualifications. No screening by staff will take place.")%></div>
              </td>
						</tr>
						<tr id="JobSeekersMustHaveMinimumStarMatchRow" runat="server">
							<td>
							  <asp:RadioButton ID="JobSeekersMustHaveMinimumStarMatchRadioButton" runat="server" GroupName="ScreenPreferences" />
							</td>
              <td>
						    <%= HtmlLocalise("JobSeekersWithAMatchScoreOf.Label", "Only #CANDIDATETYPES#:LOWER with a match score of")%>&nbsp;<asp:DropDownList runat="server" ID="ScreeningPreferencesMinimumStarsDropDown"/>&nbsp;<%= HtmlLocalise("MayApplyWithoutStaffPermisssion.Label", "may apply without staff permission")%>
                <div style="display: inline-block;"><%= HtmlTooltipster("tooltipWithArrow", "MayApplyWithoutStaffPermisssion.Tooltip", @"Only #CANDIDATETYPES#:LOWER who meet your selected match score (1-5 stars) may apply.")%></div>
						  </td>
						</tr>
					  <tr id="JobSeekersMinimumStarsToApplyRow" runat="server">
						  <td>
						    <asp:RadioButton ID="JobSeekersMinimumStarsToApplyRadioButton" runat="server" GroupName="ScreenPreferences" ClientIDMode="Static" />
						  </td>
              <td>
                <%= HtmlLocalise("JobSeekersWithAMatchScoreOf.Label", "Only #CANDIDATETYPES#:LOWER with a match score of")%>&nbsp;<asp:DropDownList runat="server" ID="ScreeningPreferencesMinimumStarsToApplyDropDown"/>&nbsp;<%= HtmlLocalise("OrAboveMayApply.Label", "or above may apply")%>
                <div style="display: inline-block;"><%= HtmlTooltipster("tooltipWithArrow", "OrAboveMayApply.Tooltip", @"All #CANDIDATETYPES#:LOWER who meet or exceed your selected match score (1-5 stars) may apply. Those below the match score will be screened by staff and referred to you, if qualified.")%></div>
              </td>
					  </tr>
						<tr>
							<td>
							  <asp:RadioButton ID="JobSeekersMustBeScreenedRadioButton" runat="server" GroupName="ScreenPreferences" />
							</td>
              <td>
                <%= HtmlLocalise(ScreeningPreferences.JobSeekersMustBeScreened.ToString(), "Staff must screen all #CANDIDATETYPES#:LOWER before they can apply")%>
                <div style="display: inline-block;"><%= HtmlTooltipster("tooltipWithArrow", "JobSeekersMustBeScreened.Tooltip", @"All #CANDIDATETYPES#:LOWER who wish to apply will be screened by staff and referred to you, if qualified.")%></div>
              </td>
						</tr>
					</table>
				</asp:Panel>
			</div>
			<act:CollapsiblePanelExtender ID="ScreeningPreferencesPanelExtender" runat="server" TargetControlID="ScreeningPreferencesPanel" ExpandControlID="ScreeningPreferencesHeaderPanel" 
																					CollapseControlID="ScreeningPreferencesHeaderPanel" Collapsed="false" ImageControlID="ScreeningPreferencesHeaderImage" 
																					SuppressPostBack="true" />
      
		</td>
	</tr>
</table>
<script type="text/javascript">
  var JobWizardEducationStep5_SalaryFrequencyDropDown;
  var JobWizardEducationStep5_MinimumSalaryTextBox;
  var JobWizardEducationStep5_MaximumSalaryTextBox;

  Sys.Application.add_load(JobWizardEducationStep5_PageLoad);

  var JobWizardEducationStep5_SalaryFrequencyDropDown;
  var JobWizardEducationStep5_MinimumSalaryTextBox;
  var JobWizardEducationStep5_MaximumSalaryTextBox;

  function JobWizardEducationStep5_PageLoad() {
    JobWizardEducationStep5_SalaryFrequencyDropDown = $("#<%= SalaryFrequencyDropDown.ClientID %>");
    JobWizardEducationStep5_MinimumSalaryTextBox = $("#<%=MinimumSalaryTextBox.ClientID %>");
    JobWizardEducationStep5_MaximumSalaryTextBox = $("#<%=MaximumSalaryTextBox.ClientID %>");
	}

	Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
    $('.inFieldLabel > label').inFieldLabels();
  });
  function validateSalaryFrequencyDropDown(src, args) {
    args.IsValid = true;

    var MinimumSalaryTextBox = $("#<%= MinimumSalaryTextBox.ClientID %>").val();
    var MaximumSalaryTextBox = $("#<%= MaximumSalaryTextBox.ClientID %>").val();
    var salaryFrequencyDropDown = $("#<%= SalaryFrequencyDropDown.ClientID %>").val();

    if (salaryFrequencyDropDown == "" && (TrimText(MinimumSalaryTextBox) + TrimText(MaximumSalaryTextBox)) != "")
      args.IsValid = false;

    ValidatorValidate($("#<%=MinimumSalaryValueValidator.ClientID %>")[0]);
    ValidatorValidate($("#<%=MaximumSalaryValueValidator.ClientID %>")[0]);
  }
  function validateSalaryWhenOnPosting(src, args) {
    args.IsValid = true;

    var minimumSalaryTextBoxValue = $("#<%= MinimumSalaryTextBox.ClientID %>").val();
    var showSalaryOnPosting = $('#<%= HideSalaryCheckbox.ClientID %>').is(':checked');
      
    if (showSalaryOnPosting && minimumSalaryTextBoxValue == "")
      args.IsValid = false;
  }

  $(document).ready(function () {
    $("#InterviewPreferenceFocusTalent").change(function () {
      ToggleInterviewPreferences();
    });
    $("#InterviewPreferenceOther").change(function () {
      ToggleInterviewPreferences();
    });
  });

  function ToggleInterviewPreferences() {
    var interviewThroughTalentOnly = $("#InterviewPreferenceFocusTalent").is(":checked");
    // Reset interview settings
    if (interviewThroughTalentOnly) {
      $("#EmailResumeCheckBox").prop('checked', false);
      $("#ApplyOnlineCheckBox").prop('checked', false);
      $("#MailResumeCheckBox").prop('checked', false);
      $("#FaxResumeCheckBox").prop('checked', false);
      $("#PhoneForAppointmentCheckBox").prop('checked', false);
      $("#InPersonCheckBox").prop('checked', false);

      $("#OtherInstructionsTextBox").val("");
      $("#EmailAddressTextBox").val("");
      $("#ApplyOnlineTextBox").val("");
      $("#MailResumeTextBox").val("");
      $("#FaxResumeTextBox").val("");
      $("#PhoneForAppointmentTextBox").val("");
      $("#InPersonTextBox").val("");

      // Trigger change event to reset labels
      $("#OtherInstructionsTextBox").change();
      $("#EmailAddressTextBox").change();
      $("#ApplyOnlineTextBox").change();
      $("#MailResumeTextBox").change();
      $("#FaxResumeTextBox").change();
      $("#PhoneForAppointmentTextBox").change();
      $("#InPersonTextBox").change();
    }

    // Set interview preferences according to radio button values
    $("#EmailResumeCheckBox").prop('disabled', interviewThroughTalentOnly);
    $("#ApplyOnlineCheckBox").prop('disabled', interviewThroughTalentOnly);
    $("#MailResumeCheckBox").prop('disabled', interviewThroughTalentOnly);
    $("#FaxResumeCheckBox").prop('disabled', interviewThroughTalentOnly);
    $("#PhoneForAppointmentCheckBox").prop('disabled', interviewThroughTalentOnly);
    $("#InPersonCheckBox").prop('disabled', interviewThroughTalentOnly);

    $("#OtherInstructionsTextBox").prop('disabled', interviewThroughTalentOnly);
    $("#EmailAddressTextBox").prop('disabled', interviewThroughTalentOnly);
    $("#ApplyOnlineTextBox").prop('disabled', interviewThroughTalentOnly);
    $("#MailResumeTextBox").prop('disabled', interviewThroughTalentOnly);
    $("#FaxResumeTextBox").prop('disabled', interviewThroughTalentOnly);
    $("#PhoneForAppointmentTextBox").prop('disabled', interviewThroughTalentOnly);
    $("#InPersonTextBox").prop('disabled', interviewThroughTalentOnly);
  }

  function ValidateDate(sender, args) {
    if (args.Value.trim().length > 0 && args.Value != "__/__/____") {
      var dateRegEx = /^(0?[1-9]|1[012])[- \/.](0?[1-9]|[12][0-9]|3[01])[- \/.](19|20\d\d)$/;

      if ("<%= CurrentCultureName %>" == "en-GB") {
        dateRegEx = /^(0?[1-9]|[12][0-9]|3[01])[- \/.](0?[1-9]|1[012])[- \/.](19|20\d\d)$/;

        if (dateRegEx.test(args.Value) == false) {
          sender.innerHTML = "<%= DateErrorMessage %>";
          args.IsValid = false;
        } else {

          var match = dateRegEx.exec(args.Value);
          if (match != null) {
            // At this point, $3 holds the year, $2 the month and $1 the day of the date entered
            if (match[1] == 31 && (match[2] == 4 || match[2] == 6 || match[2] == 9 || match[2] == 11)) {
              sender.innerHTML = "<%= DateErrorMessage %>";
              args.IsValid = false; // 31st of a month with 30 days
            } else if (match[1] >= 30 && match[2] == 2) {
              sender.innerHTML = "<%= DateErrorMessage %>";
              args.IsValid = false; // February 30th or 31st
            } else if (match[2] == 2 && match[1] == 29 && !(match[3] % 4 == 0 && (match[3] % 100 != 0 || match[3] % 400 == 0))) {
              sender.innerHTML = "<%= DateErrorMessage %>";
              args.IsValid = false; // February 29th outside a leap year
            }
          }
        }
      }
      else {
        if (dateRegEx.test(args.Value) == false) {
          sender.innerHTML = "<%= DateErrorMessage %>";
          args.IsValid = false;
        } else {
          // At this point, $3 holds the year, $2 the day and $1 the month of the date entered
          var match = dateRegEx.exec(args.Value);
          if (match != null) {
            // At this point, $3 holds the year, $2 the month and $1 the day of the date entered
            if (match[2] == 31 && (match[1] == 4 || match[1] == 6 || match[1] == 9 || match[1] == 11)) {
              sender.innerHTML = "<%= DateErrorMessage %>";
              args.IsValid = false; // 31st of a month with 30 days
            } else if (match[2] >= 30 && match[1] == 2) {
              sender.innerHTML = "<%= DateErrorMessage %>";
              args.IsValid = false; // February 30th or 31st
            } else if (match[1] == 2 && match[2] == 29 && !(match[3] % 4 == 0 && (match[3] % 100 != 0 || match[3] % 400 == 0))) {
              sender.innerHTML = "<%= DateErrorMessage %>";
              args.IsValid = false; // February 29th outside a leap year
            }
          }
        }
      }
    }
  }

  function JobWizardStep5_DisableLocationSelectorValidator() {
    var validator = $("#<%=LocationSelectorValidator.ClientID %>");
    if (validator.length > 0)
      ValidatorEnable(validator[0], false);

    return true;
  }

  function JobWizardEducationStep5_ValidateMeetsMinimumWageRequirement(oSrc, args) {
    args.IsValid = $("#<%=MeetsMinimumWageRequirementCheckBox.ClientID%>").is(":checked");
  }

  function JobWizardEducationStep5_ValidateHoursPerWeek(oSrc, args) {
    var minCheckBox = $("#<%=MinimumHoursPerWeekTextBox.ClientID %>");
    var maxCheckBox = $("#<%=HoursPerWeekTextBox.ClientID %>");

    args.IsValid = maxCheckBox.val().trim().length > 0 || minCheckBox.val().trim().length == 0;
  }
  function JobWizardEducationStep5_ValidateMinimumHoursPerWeek(oSrc, args) {
    ValidatorValidate($("#<%=HoursPerWeekCustomValidator.ClientID %>")[0]);
  }

  function JobWizardEducationStep5_ValidateMinimumSalaryAmount(oSrc, args) {
    var value = JobWizardEducationStep5_MinimumSalaryTextBox.val();

    args.IsValid = JobWizardEducationStep5_SalaryFrequencyDropDown == null
	                   || JobWizardEducationStep5_SalaryFrequencyDropDown.val() == ""
	                   || (JobWizardEducationStep5_IsDecimal(value) && parseFloat(value) >= parseFloat(JobWizardEducationStep5_CodeValues["MinThreshold" + JobWizardEducationStep5_SalaryFrequencyDropDown.val()]));

    if (!args.IsValid) {
      $(oSrc).html(JobWizardEducationStep5_CodeValues["ErrorMinimumSalary" + JobWizardEducationStep5_SalaryFrequencyDropDown.val()]);
    }
  }

  function JobWizardEducationStep5_ValidateMaximumSalaryAmount(oSrc, args) {
    var value = JobWizardEducationStep5_MaximumSalaryTextBox.val();

    args.IsValid = JobWizardEducationStep5_SalaryFrequencyDropDown == null
	                   || JobWizardEducationStep5_SalaryFrequencyDropDown.val() == ""
	                   || (JobWizardEducationStep5_IsDecimal(value) && parseFloat(value) <= parseFloat(JobWizardEducationStep5_CodeValues["MaxThreshold" + JobWizardEducationStep5_SalaryFrequencyDropDown.val()]));

    if (!args.IsValid) {
      $(oSrc).html(JobWizardEducationStep5_CodeValues["ErrorMaximumSalary" + JobWizardEducationStep5_SalaryFrequencyDropDown.val()]);
    }
  }

  function JobWizardEducationStep5_IsDecimal(val) {
    var ex = /^[0-9]*(\.[0-9]+)?$/;
    return ex.test(val);
  }
</script>