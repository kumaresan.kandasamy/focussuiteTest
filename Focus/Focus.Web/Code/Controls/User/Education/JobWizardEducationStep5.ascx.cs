﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Web.ViewModels;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User.Education
{
  public partial class JobWizardEducationStep5 : JobWizardControl
	//TODO: Merge this back into the main job wizard control so we don't have to maintain 2 different objects
  {
    private const string _newOrMultipleLocationsValue = "NewOrMultipleLocations";
    private const string _allLocationsValue = "AllLocations";

    private List<JobLocationDto> _jobLocations
    {
      get { return GetViewStateValue<List<JobLocationDto>>("JobWizardStep5:JobLocations"); }
      set { SetViewStateValue("JobWizardStep5:JobLocations", value); }
    }

    private List<JobLocationDto> _allJobLocations
    {
      get { return GetViewStateValue<List<JobLocationDto>>("JobWizardStep5:AllJobLocations"); }
      set { SetViewStateValue("JobWizardStep5:AllJobLocations", value); }
    }

    private JobLocationDto MainSiteLocation
    {
      get { return GetViewStateValue<JobLocationDto>("JobWizardStep5:MainSiteLocation"); }
      set { SetViewStateValue("JobWizardStep5:MainSiteLocation", value); }
    }

		protected string DateErrorMessage
		{
			get { return GetViewStateValue<string>("JobWizardStep5:DateErrorMessage"); }
			set { SetViewStateValue("JobWizardStep5:DateErrorMessage", value); }
		}

		protected string CurrentCultureName
		{
			get { return GetViewStateValue<string>("JobWizardStep5:CurrentCultureName"); }
			set { SetViewStateValue("JobWizardStep5:CurrentCultureName", value); }
		}

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        LocaliseUI();
	      ApplyBranding();
        ApplyTheme();
        ScreeningPreferencesHeaderPanel.Visible = ScreeningPreferencesPanel.Visible = App.Settings.JobSeekerReferralsEnabled;
				CurrentCultureName = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
      }

			InterviewContactPreferencesHeaderPanel.Visible = InterviewContactPreferencesPanel.Visible = !App.Settings.HideInterviewContactPreferences;

      StartDateCompareValidator.ValueToCompare = DateTime.Today.ToShortDateString();

      EmailAddressRegEx.ValidationExpression = App.Settings.EmailAddressRegExPattern;

      RegisterJavascript();
    }

    protected void WorkLocationDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
      var selectedJobLocationType = WorkLocationDropDown.SelectedValueToEnum(JobLocationTypes.MultipleLocations);

      PublicTransitAccessibleCheckBox.Visible = (selectedJobLocationType == JobLocationTypes.MainSite);

      if (selectedJobLocationType == JobLocationTypes.MultipleLocations)
      {
        LocationSelector.Visible = true;
        LocationSelectorValidator.Enabled = true;
        var jobLocations = _jobLocations;
        if (WorkLocationDropDown.SelectedValue == _allLocationsValue) jobLocations = _allJobLocations;
        LocationSelector.Bind(jobLocations);
      }
      else
      {
        LocationSelector.Visible = false;
        LocationSelectorValidator.Enabled = false;
      }
    }

    #region BindStep & UnbindStep

    /// <summary>
    /// Binds the controls for the step.
    /// </summary>
    /// <param name="model">The model.</param>
    internal void BindStep(JobWizardViewModel model)
    {
      if(model.Job.JobType != model.OriginalJobType)
        UpdateSalaryDetails(model);

      _jobLocations = model.JobLocations;
      _allJobLocations = model.AllJobLocations;

      BindControls(model);

      if (model.Job.StartDate.HasValue) StartDateTextBox.Text = model.Job.StartDate.GetValueOrDefault().ToShortDateString();
      if (model.Job.EndDate.HasValue) EndDateTextBox.Text = model.Job.EndDate.GetValueOrDefault().ToShortDateString();
	    ApplicationClosingDateTextBox.Text = model.JobClosingOn;
      if (model.Job.MinSalary.IsNotNull()) MinimumSalaryTextBox.Text = model.Job.MinSalary.GetValueOrDefault().ToString("F2");
      if (model.Job.MaxSalary.IsNotNull()) MaximumSalaryTextBox.Text = model.Job.MaxSalary.GetValueOrDefault().ToString("F2");
      if (model.Job.OtherSalary.IsNotNullOrEmpty()) OtherSalaryTextBox.Text = model.Job.OtherSalary;
      if (model.Job.SalaryFrequencyId.IsNotNull()) SalaryFrequencyDropDown.SelectValue(model.Job.SalaryFrequencyId.GetValueOrDefault().ToString(CultureInfo.InvariantCulture));
      MeetsMinimumWageRequirementCheckBox.Checked = model.Job.MeetsMinimumWageRequirement;

      if (model.Job.IsCommissionBased.IsNotNull()) CommissionBasedCheckBox.Checked = model.Job.IsCommissionBased.GetValueOrDefault();

      MinimumSalaryTextBox.Enabled =
        MaximumSalaryTextBox.Enabled =
        SalaryFrequencyDropDown.Enabled =
        OtherSalaryTextBox.Enabled =
        CommissionBasedCheckBox.Enabled =
        HideSalaryCheckbox.Enabled = OtherSalaryTextBox.Enabled = (model.Job.JobType != JobTypes.InternshipUnpaid);

      if (model.Job.HideSalaryOnPosting.HasValue)
      {
        HideSalaryCheckbox.Checked = !model.Job.HideSalaryOnPosting.GetValueOrDefault();
      }
      
      if (model.Job.HideOpeningsOnPosting.HasValue) { HideOpeningsCheckbox.Checked = model.Job.HideOpeningsOnPosting.GetValueOrDefault(); }

      if (model.Job.HoursPerWeek.IsNotNull()) HoursPerWeekTextBox.Text = model.Job.HoursPerWeek.GetValueOrDefault().ToString("F");
      if (model.Job.MinimumHoursPerWeek.IsNotNull()) MinimumHoursPerWeekTextBox.Text = model.Job.MinimumHoursPerWeek.GetValueOrDefault().ToString("F");

      NumberOfOpeningsTextBox.Text = model.Job.NumberOfOpenings.IsNotNull() ? model.Job.NumberOfOpenings.ToString() : "1";
      if (model.Job.JobLocationType == JobLocationTypes.MultipleLocations || model.Job.JobLocationType == JobLocationTypes.OtherLocation)
      {
        WorkLocationDropDown.SelectValue(_newOrMultipleLocationsValue);
        LocationSelector.Visible = true;
        LocationSelectorValidator.Enabled = true;
        if (_jobLocations.IsNotNullOrEmpty()) LocationSelector.Bind(_jobLocations);
      }
      else
      {
        WorkLocationDropDown.SelectValue(model.Job.JobLocationType.ToString());
        LocationSelector.Visible = false;
        LocationSelectorValidator.Enabled = false;
      }

      PublicTransitAccessibleCheckBox.Visible = (model.Job.JobLocationType == JobLocationTypes.MainSite);
      SalaryTypeLabel.Text = model.Job.JobType == JobTypes.Job || model.Job.JobType == JobTypes.InternshipPaid
                               ? CodeLocalise("PaidPosition.Text", "paid position")
                               : CodeLocalise("UnpaidPosition.Text", "unpaid position");

     
      InterviewPreferenceFocusTalent.Checked = ((model.Job.InterviewContactPreferences & ContactMethods.FocusTalent) == ContactMethods.FocusTalent);
      EmailResumeCheckBox.Checked = ((model.Job.InterviewContactPreferences & ContactMethods.Email) == ContactMethods.Email);
      ApplyOnlineCheckBox.Checked = ((model.Job.InterviewContactPreferences & ContactMethods.Online) == ContactMethods.Online);
      MailResumeCheckBox.Checked = ((model.Job.InterviewContactPreferences & ContactMethods.Mail) == ContactMethods.Mail);
      FaxResumeCheckBox.Checked = ((model.Job.InterviewContactPreferences & ContactMethods.Fax) == ContactMethods.Fax);
      InPersonCheckBox.Checked = ((model.Job.InterviewContactPreferences & ContactMethods.InPerson) == ContactMethods.InPerson);
      PhoneForAppointmentCheckBox.Checked = ((model.Job.InterviewContactPreferences & ContactMethods.Telephone) == ContactMethods.Telephone);

      if (EmailResumeCheckBox.Checked || ApplyOnlineCheckBox.Checked || MailResumeCheckBox.Checked || FaxResumeCheckBox.Checked || InPersonCheckBox.Checked || PhoneForAppointmentCheckBox.Checked)
      {
        InterviewPreferenceFocusTalent.Checked = false;
        InterviewPreferenceOther.Checked = true;
      }
      else
      {
        InterviewPreferenceOther.Checked = false;
        InterviewPreferenceFocusTalent.Checked = true;
      }

      EmailResumeCheckBox.Enabled =
        EmailAddressTextBox.Enabled =
        ApplyOnlineCheckBox.Enabled =
        ApplyOnlineTextBox.Enabled =
        MailResumeCheckBox.Enabled =
        MailResumeTextBox.Enabled =
        FaxResumeCheckBox.Enabled =
        FaxResumeTextBox.Enabled =
        PhoneForAppointmentCheckBox.Enabled =
        PhoneForAppointmentTextBox.Enabled =
        InPersonCheckBox.Enabled =
        InPersonTextBox.Enabled = OtherInstructionsTextBox.Enabled = InterviewPreferenceOther.Checked;

      if (model.Job.InterviewEmailAddress.IsNotNullOrEmpty()) EmailAddressTextBox.Text = model.Job.InterviewEmailAddress;
      if (model.Job.InterviewApplicationUrl.IsNotNullOrEmpty()) ApplyOnlineTextBox.Text = model.Job.InterviewApplicationUrl;
      if (model.Job.InterviewMailAddress.IsNotNullOrEmpty()) MailResumeTextBox.Text = model.Job.InterviewMailAddress;
      if (model.Job.InterviewFaxNumber.IsNotNullOrEmpty()) FaxResumeTextBox.Text = model.Job.InterviewFaxNumber;
      if (model.Job.InterviewPhoneNumber.IsNotNullOrEmpty()) PhoneForAppointmentTextBox.Text = model.Job.InterviewPhoneNumber;
      if (model.Job.InterviewDirectApplicationDetails.IsNotNullOrEmpty()) InPersonTextBox.Text = model.Job.InterviewDirectApplicationDetails;
      if (model.Job.InterviewOtherInstructions.IsNotNullOrEmpty()) OtherInstructionsTextBox.Text = model.Job.InterviewOtherInstructions;

      
      BindStepScreeningPreferences(model, 
                                   AllowUnqualifiedApplicationsRadioButton, 
                                   JobSeekersMustBeScreenedRadioButton,
                                   JobSeekersMustHaveMinimumStarMatchRadioButton,
                                   ScreeningPreferencesMinimumStarsDropDown,
                                   JobSeekersMustHaveMinimumStarMatchRow,
                                   JobSeekersMinimumStarsToApplyRadioButton,
                                   ScreeningPreferencesMinimumStarsToApplyDropDown,
                                   JobSeekersMinimumStarsToApplyRow,
                                   AllowUnqualifiedApplicationsRow);

      // TODO: at the moment business unit address is null but this could be due to moving the code from existing control
      // TODO: needs investigation
      if (!model.BusinessUnitAddress.IsNotNull()) return;
      if (model.Job.JobLocationType == JobLocationTypes.MainSite && _jobLocations.IsNotNullOrEmpty())
        PublicTransitAccessibleCheckBox.Checked = (_jobLocations[0].IsPublicTransitAccessible.IsNotNull() &&
                                                   _jobLocations[0].IsPublicTransitAccessible);
      else
        PublicTransitAccessibleCheckBox.Checked = (model.BusinessUnitAddress.PublicTransitAccessible.IsNotNull() &&
                                                   model.BusinessUnitAddress.PublicTransitAccessible);

      var expiryDate = GetJobExpiryDate(App, model.Job.PostedOn, model.Job.FederalContractor.GetValueOrDefault(false));
      ApplicationClosingDateCompareValidator.ValueToCompare = expiryDate.ToShortDateString();
      ApplicationClosingDateCompareValidator.ErrorMessage = App.Settings.DaysForJobPostingLifetime == 0
        ? CodeLocalise("ClosingDateCompareValidator.ErrorMessage", "Closing date cannot be longer than one year away")
        : CodeLocalise("ClosingDateCompareValidator.Lifetime.ErrorMessage", "Closing date cannot exceed {0}", expiryDate.ToShortDateString());

      ApplicationClosingDateGreaterThanTodayCompareValidatior.ValueToCompare = App.Settings.DaysForMinimumJobClosingDate == 0
                                                                                 ? DateTime.Today.ToShortDateString()
                                                                                 : model.Job.CreatedOn.AddDays(App.Settings.DaysForMinimumJobClosingDate - 1).ToShortDateString();
                                                                                 
      RegisterCodeValuesJson("JobWizardEducationStep5_ScriptValues", "JobWizardEducationStep5_CodeValues", InitialiseClientSideCodeValuesForSalary());
    }

		/// <summary>
		/// Unbinds the step.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
    internal JobWizardViewModel UnbindStep(JobWizardViewModel model)
    {
      model.JobClosingOn = ApplicationClosingDateTextBox.TextTrimmed();
      model.Job.StartDate = StartDateTextBox.TextTrimmed().ToDate();
      model.Job.EndDate = EndDateTextBox.TextTrimmed().ToDate();
      model.Job.NumberOfOpenings = int.Parse(NumberOfOpeningsTextBox.TextTrimmed());
      model.Job.HideOpeningsOnPosting = HideOpeningsCheckbox.Checked;
      model.Job.MinSalary = MinimumSalaryTextBox.AsNullableDecimal();
      model.Job.MaxSalary = MaximumSalaryTextBox.AsNullableDecimal();
      model.Job.OtherSalary = OtherSalaryTextBox.Text;
      model.Job.SalaryFrequencyId = SalaryFrequencyDropDown.SelectedValueToLong();
      model.Job.HideSalaryOnPosting = !HideSalaryCheckbox.Checked;
      model.Job.MeetsMinimumWageRequirement = MeetsMinimumWageRequirementCheckBox.Checked;

      model.Job.IsCommissionBased = CommissionBasedCheckBox.Checked;

      model.Job.HoursPerWeek = HoursPerWeekTextBox.AsNullableDecimal();
      model.Job.MinimumHoursPerWeek = MinimumHoursPerWeekTextBox.AsNullableDecimal();
     
      if (WorkLocationDropDown.SelectedValueToEnum(JobLocationTypes.MultipleLocations) == JobLocationTypes.MultipleLocations)
      {
        _jobLocations = LocationSelector.Unbind();

        // Ensure all locations are associated with the job
        foreach (var jobLocation in _jobLocations)
        {
          jobLocation.JobId = model.Job.Id.GetValueOrDefault();
        }

        model.Job.JobLocationType = (_jobLocations.Count > 1) ? JobLocationTypes.MultipleLocations : JobLocationTypes.OtherLocation;

        model.JobLocations = _jobLocations;
      }
      else
      {
        model.Job.JobLocationType = WorkLocationDropDown.SelectedValueToEnum<JobLocationTypes>();
      }

      if (model.Job.JobLocationType == JobLocationTypes.MainSite)
      {
        var locations = new List<JobLocationDto>
				                {
				                	new JobLocationDto
				                	{
				                		JobId = model.Job.Id.GetValueOrDefault(),
                            City = MainSiteLocation.City,
                            County = MainSiteLocation.County,
                            State = MainSiteLocation.State,
                            Zip = MainSiteLocation.Zip,
								            Location = MainSiteLocation.Location,
														IsPublicTransitAccessible = PublicTransitAccessibleCheckBox.Checked
				                	}
				                };

        model.JobLocations = locations;
      }

      if (model.Job.JobLocationType == JobLocationTypes.NoFixedLocation)
        model.JobLocations = new List<JobLocationDto>();

			if (!App.Settings.HideInterviewContactPreferences)
			{
				var interviewContactPreferences = ContactMethods.None;
				if (InterviewPreferenceFocusTalent.Checked) interviewContactPreferences = interviewContactPreferences | ContactMethods.FocusTalent;
				if (EmailResumeCheckBox.Checked) interviewContactPreferences = interviewContactPreferences | ContactMethods.Email;
				if (ApplyOnlineCheckBox.Checked) interviewContactPreferences = interviewContactPreferences | ContactMethods.Online;
				if (MailResumeCheckBox.Checked) interviewContactPreferences = interviewContactPreferences | ContactMethods.Mail;
				if (FaxResumeCheckBox.Checked) interviewContactPreferences = interviewContactPreferences | ContactMethods.Fax;
				if (InPersonCheckBox.Checked) interviewContactPreferences = interviewContactPreferences | ContactMethods.InPerson;
				if (PhoneForAppointmentCheckBox.Checked) interviewContactPreferences = interviewContactPreferences | ContactMethods.Telephone;
				model.Job.InterviewContactPreferences = interviewContactPreferences;
			}

      model.Job.InterviewEmailAddress = EmailAddressTextBox.TextTrimmed();
      model.Job.InterviewApplicationUrl = ApplyOnlineTextBox.TextTrimmed();
      model.Job.InterviewMailAddress = MailResumeTextBox.TextTrimmed();
      model.Job.InterviewFaxNumber = FaxResumeTextBox.TextTrimmed();
      model.Job.InterviewPhoneNumber = PhoneForAppointmentTextBox.TextTrimmed();
      model.Job.InterviewDirectApplicationDetails = InPersonTextBox.TextTrimmed();
      model.Job.InterviewOtherInstructions = OtherInstructionsTextBox.TextTrimmed();

      if (App.Settings.JobSeekerReferralsEnabled && !App.Settings.HideScreeningPreferences)
      {
        if (JobSeekersMustBeScreenedRadioButton.Checked)
          model.Job.ScreeningPreferences = ScreeningPreferences.JobSeekersMustBeScreened;

        else if (JobSeekersMustHaveMinimumStarMatchRadioButton.Checked && App.Settings.Theme == FocusThemes.Workforce)
          model.Job.ScreeningPreferences = (ScreeningPreferences)Enum.Parse(typeof(ScreeningPreferences), ScreeningPreferencesMinimumStarsDropDown.SelectedValue);

        else if (JobSeekersMinimumStarsToApplyRadioButton.Checked)
          model.Job.ScreeningPreferences = (ScreeningPreferences)Enum.Parse(typeof(ScreeningPreferences), ScreeningPreferencesMinimumStarsToApplyDropDown.SelectedValue);

        else
          model.Job.ScreeningPreferences = ScreeningPreferences.AllowUnqualifiedApplications;
      }

     
      return model;
    }

    #endregion

    #region Bind Methods

		/// <summary>
		/// Binds the controls.
		/// </summary>
		/// <param name="model">The model.</param>
    private void BindControls(JobWizardViewModel model)
    {
      BindScreeningPreferencesMinimumStarsDropDown(ScreeningPreferencesMinimumStarsDropDown);
      BindScreeningPreferencesMinimumStarsToApplyDropDown(ScreeningPreferencesMinimumStarsToApplyDropDown);
      MainSiteLocation = BuildWorkLocation(model.BusinessUnitAddress);
      BindWorkLocationDropDown(MainSiteLocation.Location);
    }

		/// <summary>
		/// Binds the work location drop down.
		/// </summary>
		/// <param name="companyAddress">The company address.</param>
    private void BindWorkLocationDropDown(string companyAddress)
    {
      WorkLocationDropDown.Items.Clear();
      if(companyAddress.IsNotNullOrEmpty())
        WorkLocationDropDown.Items.AddEnum(JobLocationTypes.MainSite, companyAddress);
      WorkLocationDropDown.Items.Add(new ListItem(CodeLocalise("NewOrMultipleLocations.Text", "New or multiple locations"), _newOrMultipleLocationsValue));
      WorkLocationDropDown.Items.Add(new ListItem(CodeLocalise("AllLocations.Text", "All currently listed locations"), _allLocationsValue));
      SalaryFrequencyDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Frequencies), null, CodeLocalise("SalaryFrequency.TopDefault", "- select frequency -"));

			if (App.Settings.NoFixedLocation)
				WorkLocationDropDown.Items.AddEnum(JobLocationTypes.NoFixedLocation, "No fixed location");

      // AJ Hidden "Jobs available throughout the state" until it can be handled in Career
      // WorkLocationDropDown.Items.AddEnum(JobLocationTypes.NoFixedLocation, "Jobs available throughout the state");
    }
    
    #endregion

    #region Helper Methods

    /// <summary>
    ///  Resets controls depending on change in job type
    /// </summary>
    /// <param name="model">The model.</param>
    internal void UpdateSalaryDetails(JobWizardViewModel model)
    {
      switch (model.Job.JobType)
      {
        case JobTypes.InternshipUnpaid:
          MinimumSalaryTextBox.Text =
            MaximumSalaryTextBox.Text = OtherSalaryTextBox.Text = model.Job.OtherSalary = string.Empty;
          model.Job.IsCommissionBased = CommissionBasedCheckBox.Checked = false;
          model.Job.HideSalaryOnPosting = true;
          HideSalaryCheckbox.Checked = false;
          SalaryFrequencyDropDown.SelectedIndex = -1;
          model.Job.SalaryFrequencyId = null;
          model.Job.MaxSalary = model.Job.MinSalary = null;
          break;
      }

    }

    /// <summary>
    /// Registers the javascript.
    /// </summary>
    private void RegisterJavascript()
    {
      // Build the JS
      var js = @"function pageLoad() {
	$(document).ready(function()
	{
		
		// Apply jQuery to infield labels
		$("".inFieldLabel > label"").inFieldLabels();
	});
}

function validateLocations(oSrc, args){
	var isValid = true;
	if(args.Value == """ + _newOrMultipleLocationsValue + @""" || args.Value == """ + _allLocationsValue + @"""){
		isValid = ($(""#LocationSelectorHasRowsHidden"").val() == 'true')
	}
 	args.IsValid = isValid;
 
}

function validateInterviewContactPreferences(oSrc, args)
{
	args.IsValid = ($(""#" + InterviewPreferenceFocusTalent.ClientID + @""").is("":checked"") ||
                  ( $(""#" + InterviewPreferenceOther.ClientID + @""").is("":checked"") && (
									$(""#" + EmailResumeCheckBox.ClientID + @""").is("":checked"") ||
									$(""#" + ApplyOnlineCheckBox.ClientID + @""").is("":checked"") ||
									$(""#" + MailResumeCheckBox.ClientID + @""").is("":checked"") ||
									$(""#" + FaxResumeCheckBox.ClientID + @""").is("":checked"") ||
									$(""#" + PhoneForAppointmentCheckBox.ClientID + @""").is("":checked"") ||
									$(""#" + InPersonCheckBox.ClientID + @""").is("":checked""))));
 
}

function validateEmailAddress(oSrc, args)
{
	if($(""#" + EmailResumeCheckBox.ClientID + @""").is("":checked""))
	{
		args.IsValid = ($(""#" + EmailAddressTextBox.ClientID + @""").val() != '');
	}
	else
	{
		args.IsValid = true;
	}
 
}

function validateApplyOnline(oSrc, args)
{
	if($(""#" + ApplyOnlineCheckBox.ClientID + @""").is("":checked""))
	{
		args.IsValid = ($(""#" + ApplyOnlineTextBox.ClientID + @""").val() != '');
	}
	else
	{
		args.IsValid = true;
	}
 
}

function validateMailResume(oSrc, args)
{
	if($(""#" + MailResumeCheckBox.ClientID + @""").is("":checked""))
	{
		args.IsValid = ($(""#" + MailResumeTextBox.ClientID + @""").val() != '');
	}
	else
	{
		args.IsValid = true;
	}
 
}

function validateFaxResume(oSrc, args)
{
	if($(""#" + FaxResumeCheckBox.ClientID + @""").is("":checked""))
	{
		args.IsValid = ($(""#" + FaxResumeTextBox.ClientID + @""").val() != '');
	}
	else
	{
		args.IsValid = true;
	}
 
}

function validatePhoneForAppointment(oSrc, args)
{
	if($(""#" + PhoneForAppointmentCheckBox.ClientID + @""").is("":checked""))
	{
		args.IsValid = ($(""#" + PhoneForAppointmentTextBox.ClientID + @""").val() != '');
	}
	else
	{
		args.IsValid = true;
	}

}

function validateInPerson(oSrc, args)
{
	if($(""#" + InPersonCheckBox.ClientID + @""").is("":checked""))
	{
		args.IsValid = ($(""#" + InPersonTextBox.ClientID + @""").val() != '');
	}
	else
	{
		args.IsValid = true;
	}
 
}
";

      ScriptManager.RegisterClientScriptBlock(this, typeof(JobWizardStep5), "JobWizardStep5JavaScript", js, true);
    }

    #endregion

    #region Localise the UI

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      //ScreeningPreferencesLabel.Text = CodeLocalise("ScreeningPreferencesLabel.Text",
                                                   // "Allow interested #CANDIDATETYPE# may apply");

      PublicTransitAccessibleCheckBox.Text = CodeLocalise("PublicTransitAccessibleCheckBox.Text", "Public transit accessible");
      LocationSelectorValidator.ErrorMessage = CodeLocalise("LocationSelectorValidator.ErrorMessage", "At least one location is required");
      HideSalaryCheckbox.Text = CodeLocalise("HideSalaryCheckbox.Text", "Display salary in my posting.");
      HideOpeningsCheckbox.Text = CodeLocalise("HideOpeningsCheckbox.Text", "Hide number of openings from displaying in my posting");
      CommissionBasedCheckBox.Text = CodeLocalise("CommissionBasedCheckBox.Text", "This is a commission-based position.");

      ApplicationClosingDateRequired.ErrorMessage = CodeLocalise("ClosingDateRequired.ErrorMessage", "Closing date required");

      NumberOfOpeningsRequired.ErrorMessage = CodeLocalise("NumberOfOpeningsRequired.ErrorMessage", "Number of openings required");
      MinimumNumberOfOpeningsValidator.ErrorMessage = CodeLocalise("MinimumNumberOfOpeningsValidator.ErrorMessage", "Number of openings must be a number greater than 0");
			MaximumNumberOfOpeningsValidator.ErrorMessage = CodeLocalise("MaximumNumberOfOpeningsValidator.ErrorMessage", "Number of openings must be a number less than or equal to 200");

      if (App.Settings.HasMinimumHoursPerWeek && App.Settings.Theme == FocusThemes.Workforce)
      {
        HoursPerWeekHeader.Text = HtmlLabel("HoursPerWeekRange.Label", "Hours per week range");
        HoursPerWeekRangeValidator.ErrorMessage = string.Concat(CodeLocalise("MaximumHoursPerWeek.RangeError", "Maximum/actual hours must be a number between 0 and 168"), "<br />");
        MinimumHoursPerWeekRangeValidator.ErrorMessage = string.Concat(CodeLocalise("MinimumHoursPerWeek.RangeError", "Minimum hours must be a number between 0 and 168"), "<br />");
        HoursPerWeekCompareValidator.ErrorMessage = string.Concat(CodeLocalise("MinimumHoursPerWeek.CompareError", "Maximum hours must have a value greater than the minimum hours"), "<br />");
        HoursPerWeekCustomValidator.ErrorMessage = string.Concat(CodeLocalise("MinimumHoursPerWeek.CustomError", "Actual hours should be entered into the maximum hours field"), "<br />");
      }
      else
      {
        HoursPerWeekHeader.Text = HtmlLabel("HoursPerWeek.Label", "Hours per week");
        HoursPerWeekRangeValidator.ErrorMessage = string.Concat(CodeLocalise("HoursPerWeek.RangeError", "Enter a number between 0 and 168"), "<br />"); ;
      }

      InterviewPreferenceFocusTalent.Text = CodeLocalise("InterviewPreferenceFocusTalent.Text", "Receive applications through my #FOCUSTALENT# account");
      InterviewPreferenceOther.Text = CodeLocalise("InterviewPreferenceOther.Text", "Receive applications through these methods instead") + ":";
      EmailResumeCheckBox.Text = CodeLocalise("EmailResumeCheckBox.Text", "Email resume to");
      ApplyOnlineCheckBox.Text = CodeLocalise("ApplyOnlineCheckBox.Text", "Apply online to job URL");
      MailResumeCheckBox.Text = CodeLocalise("MailResumeCheckBox.Text", "Mail resume to");
      FaxResumeCheckBox.Text = CodeLocalise("FaxResumeCheckBox.Text", "Fax resume to");
      PhoneForAppointmentCheckBox.Text = CodeLocalise("PhoneForAppointmentCheckBox.Text", "Call for an appointment");
      InPersonCheckBox.Text = CodeLocalise("InPersonCheckBox.Text", "Send seeker(s) for in-person interview");
      StartDateCompareValidator.ErrorMessage = CodeLocalise("StartDateCompareValidator.ErrorMessage", "Start date must be in the future");
      EndDateCompareValidator.ErrorMessage = CodeLocalise("EndDateCompareValidator.ErrorMessage", "End date must not be before the start date");

      ApplicationClosingDateGreaterThanTodayCompareValidatior.ErrorMessage = App.Settings.DaysForMinimumJobClosingDate == 0
        ? CodeLocalise("ApplicationClosingDateGreaterThanTodayCompareValidatior.ErrorMessage", "Closing date must be in the future")
        : CodeLocalise("ApplicationClosingDateGreaterThanTodayCompareValidatiorMore.ErrorMessage", "Closing date must be at least {0} days in the future", App.Settings.DaysForMinimumJobClosingDate);

      InterviewContactPreferencesValidator.ErrorMessage = CodeLocalise("InterviewContactPreferencesValidator.ErrorMessage", "One completed contact method must be selected");
      EmailAddressValidator.ErrorMessage = CodeLocalise("EmailAddressValidator.ErrorMessage", "Email address is required");
      ApplyOnlineValidator.ErrorMessage = CodeLocalise("ApplyOnlineValidator.ErrorMessage", "URL is required");
      MailResumeValidator.ErrorMessage = CodeLocalise("MailResumeValidator.ErrorMessage", "Mail to address is required");
      FaxResumeValidator.ErrorMessage = CodeLocalise("FaxResumeValidator.ErrorMessage", "Fax number is required");
      PhoneForAppointmentValidator.ErrorMessage = CodeLocalise("PhoneForAppointmentValidator.ErrorMessage", "Phone number is required");
      InPersonValidator.ErrorMessage = CodeLocalise("InPersonValidator.ErrorMessage", "Contact details are required");
      EmailAddressRegEx.ErrorMessage = CodeLocalise("EmailAddress.RegExErrorMessage", "Email address format is invalid");

      MinimumSalaryMaximumSalaryValueValidator.ErrorMessage = CodeLocalise("MinimumSalaryMaximumSalaryTextBox.Error", "Maximum salary must have a value greater than the minimum salary<br/>");
      SalaryFrequencyDropDownValidator.ErrorMessage = CodeLocalise("SalaryFrequencyDropDown.Error", "You must select a salary frequency<br/>");
      MinimumSalaryMaximumSalaryValueBothRequiredValidator.ErrorMessage = CodeLocalise("MinimumSalaryMaximumSalaryValueBothRequired.Error", "You must enter both a minimum and maximum salary and not just one of them<br/>");
      CheckSalaryFilledInValidator.ErrorMessage =
        CodeLocalise("CheckSalaryFilledInValidator.Error",
                     "A salary value must be provided if it is to be shown in the job posting.") + "</br>";

      MeetsMinimumWageRequirementCheckBox.Text = CodeLocalise("MeetsMinimumWageRequirementCheckBox.Text", "I confirm that this job posting meets the state minimum-wage requirement");
      MeetsMinimumWageRequirementValidator.ErrorMessage = CodeLocalise("MeetsMinimumWageRequirementValidator.ErrorMessage", "Minimum wage confirmation is required");
    }

	  private void ApplyBranding()
	  {
		  WorkLocationHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			WorkLocationPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
		  WorkLocationPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

		  SalaryHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
		  SalaryPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
		  SalaryPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

		  HoursHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			HoursPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			HoursPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			ApplicationClosingDateHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ApplicationClosingDatePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ApplicationClosingDatePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			NumberOfOpeningsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			NumberOfOpeningsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			NumberOfOpeningsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			InterviewContactPreferencesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			InterviewContactPreferencesPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			InterviewContactPreferencesPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			ScreeningPreferencesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ScreeningPreferencesPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ScreeningPreferencesPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
	  }

    /// <summary>
    /// Applies the theme.
    /// </summary>
    private void ApplyTheme()
    {
      MeetsMinimumWageRequirementPlaceHolder.Visible = App.Settings.MeetsMinimumWageRequirementCheckBox;

      // Note: Requirement is HoursPerWeek not shown for Education
      var hasMinimumHours = App.Settings.HasMinimumHoursPerWeek && App.Settings.Theme == FocusThemes.Workforce;

      MinimumHoursPerWeekPanel.Visible =
        MaximumActualLabelPanel.Visible =
        MinimumHoursValidatorsPanel.Visible = hasMinimumHours;

      if (hasMinimumHours)
        HoursPerWeekRangeValidator.Display = ValidatorDisplay.Dynamic;
    }
	  #endregion

  }
}