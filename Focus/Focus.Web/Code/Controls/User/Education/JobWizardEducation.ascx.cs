﻿#region Copyright © 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Core;
using Focus.Web.ViewModels;
using Framework.Core;
using Focus.Web.Controllers.WebTalent;
using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using System.Diagnostics;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class JobWizardEducation : UserControlBase
	// TODO: Complete controller for JobWizardEducation
	//TODO: merge with jobWizard so we don't have 2 controls
	{
		private const int FinalStep = 5;
		private JobWizardViewModel _model;

		#region Properties

		public long JobId { get; set; }

		public long HiringManagerId { get; set; }

		public bool FromApproval { get; set; }

		/// <summary>
		/// Gets or sets the current step.
		/// </summary>
		/// <value>The current step.</value>
		protected int CurrentStep
		{
			get { return GetViewStateValue("JobEdit:CurrentStep", 1); }
			set { SetViewStateValue("JobEdit:CurrentStep", value); }
		}

		/// <summary>
		/// Gets the model.
		/// </summary>
		/// <value>The model.</value>
		protected JobWizardViewModel Model
		{
			get { return _model ?? (_model = GetViewStateValue("JobEdit:Model", new JobWizardViewModel())); }
			set { _model = value; }
		}

		/// <summary>
		/// Records the steps visited
		/// </summary>
		protected List<int> StepsToSave
		{
			get { return GetViewStateValue("JobEdit:StepsToSave", (List<int>)null); }
			set { SetViewStateValue("JobEdit:StepsToSave", value); }
		}

		/// <summary>
		/// Gets the page controller.
		/// </summary>
		/// <value>
		/// The page controller.
		/// </value>
		protected JobWizardEducationController PageController { get { return PageControllerBaseProperty as JobWizardEducationController; } }

		/// <summary>
		/// Registers the page controller.
		/// </summary>
		/// <returns></returns>
		public override IPageController RegisterPageController()
		{
			return new JobWizardEducationController(App);
		}

		#endregion

		#region Page Load & PreRender

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			var source = Page.RouteData.Values["source"];
			if (source != null)
				FromApproval = (string.Compare(source.ToString(), "approval", StringComparison.OrdinalIgnoreCase) == 0);

			if (!IsPostBack)
			{
				var result = PageController.GetViewModel(HiringManagerId, JobId, FromApproval);
				if (Handle(result)) return;

				var viewModelResult = result as ViewModelResult;
				Debug.Assert(!viewModelResult.IsNull());

				Model = viewModelResult.ViewModel as JobWizardViewModel;

				if (Model.IsNull())
					throw new Exception("Could not get view model");

				// Move the step on if applicable but only if in draft
				if ((JobId > 0) && Model.Job.JobStatus == JobStatuses.Draft)
					CurrentStep = (Model.Job.WizardStep != 3 || (Model.Job.WizardStep == 3 && Model.Job.WizardPath == 1) ? Model.Job.WizardStep + 1 : Model.Job.WizardStep);
				else
					CurrentStep = 1;

				if (CurrentStep > FinalStep) CurrentStep = FinalStep;

				ShowCurrentStep();
			}
		}

		/// <summary>
		/// Handles the PreRender event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_PreRender(object sender, EventArgs e)
		{
			// Persist the Model
			SetViewStateValue("JobEdit:Model", Model);
		}

		#endregion

		#region Step navigation methods

		/// <summary>
		/// Shows the current step.
		/// </summary>
		private void ShowCurrentStep(bool previous = false)
		{
			if ((App.Settings.Theme == FocusThemes.Education && !previous) && CurrentStep == 2 &&
					Model.Job.Description.IsNotNullOrEmpty() && Model.Job.DescriptionPath.IsNotNull() &&
					Model.Job.DescriptionPath != 0)
			{
				CurrentStep = 3;
				Model.Job.WizardPath = (int)Model.Job.DescriptionPath;
			}

      if (CurrentStep == 3 && Model.Job.WizardPath.IsNotIn(1, 21, 22))
        Model.Job.WizardPath = (Model.Job.WizardPath > 22) ? 22 : 1;

			Step1.Visible = (CurrentStep == 1);
			Step2.Visible = (CurrentStep == 2);
			Step3Path1.Visible = (CurrentStep == 3 && Model.Job.WizardPath == 1);
			Step3Path2Part1.Visible = (CurrentStep == 3 && Model.Job.WizardPath == 21);
			Step3Path2Part2.Visible = (CurrentStep == 3 && Model.Job.WizardPath == 22);
			Step4.Visible = (CurrentStep == 4);
			Step5.Visible = (CurrentStep == 5);

			//PreviousButtonTop.Visible = PreviousButtonBottom.Visible = (CurrentStep > 1 && Model.Job.JobStatus == JobStatuses.Draft);
			PreviousButtonTop.Visible = PreviousButtonBottom.Visible = App.Settings.Theme == FocusThemes.Workforce && !FromApproval;

			NextButtonTop.Visible = NextButtonBottom.Visible = (CurrentStep != 2);

			PreviewButtonTop.Visible = PreviewSpacerTop.Visible = (CurrentStep > 2);
			PreviewButtonBottom.Visible = PreviewSpacerBottom.Visible = (CurrentStep > 2);

			if (CurrentStep == 1) Step1.BindStep(Model);
			if (CurrentStep == 2) Step2.BindStep(Model);

			if (CurrentStep == 3 && Model.Job.WizardPath == 1) Step3Path1.BindStep(Model);
			if (CurrentStep == 3 && Model.Job.WizardPath == 21) Step3Path2Part1.BindStep(Model);
			if (CurrentStep == 3 && Model.Job.WizardPath == 22) Step3Path2Part2.BindStep(Model);
			if (CurrentStep == 4) Step4.BindStep(Model);
			if (CurrentStep == 5) Step5.BindStep(Model);

			SetProgressBar();

			LocaliseUI();
		}

		/// <summary>
		/// Sets the progress bar.
		/// </summary>
		private void SetProgressBar()
		{
			SetStepActiveOrVisited(1, Step1ProgressBarItem);
			SetStepActiveOrVisited(2, Step2Step3ProgressBarItem);
			if (CurrentStep == 3) SetStepActiveOrVisited(3, Step2Step3ProgressBarItem);
			SetStepActiveOrVisited(4, Step4ProgressBarItem);
			SetStepActiveOrVisited(5, Step5ProgressBarItem);

			var maxVisitedStep = (Model.Job.WizardStep == 1 && Model.Job.UpdatedOn == DateTime.MinValue) ? 0 : Model.Job.WizardStep + 1;

			// Hide button for currrent step or if step has not previously been reached
			Step1ProgressBarItemLinkButton.Visible = (CurrentStep != 1);
			Step2Step3ProgressBarItemLinkButton.Visible = maxVisitedStep >= 2 && (!(CurrentStep == 2 || CurrentStep == 3));
			Step4ProgressBarItemLinkButton.Visible = maxVisitedStep >= 4 && CurrentStep != 4;
			Step5ProgressBarItemLinkButton.Visible = maxVisitedStep >= 5 && CurrentStep != 5;
			Step1ProgressBarItemLiteral.Visible = !Step1ProgressBarItemLinkButton.Visible;
			Step2Step3ProgressBarItemLiteral.Visible = !Step2Step3ProgressBarItemLinkButton.Visible;
			Step4ProgressBarItemLiteral.Visible = !Step4ProgressBarItemLinkButton.Visible;
			Step5ProgressBarItemLiteral.Visible = !Step5ProgressBarItemLinkButton.Visible;

			Step1ProgressBarItemLinkButton.Text = Step1ProgressBarItemLiteral.Text = CodeLocalise("ProgressBar.Step1.LinkText", "Title & #BUSINESS#");
      Step2Step3ProgressBarItemLinkButton.Text = Step2Step3ProgressBarItemLiteral.Text = CodeLocalise("ProgressBar.Step2Step3.LinkText", "Description");
      Step4ProgressBarItemLinkButton.Text = Step4ProgressBarItemLiteral.Text = CodeLocalise("ProgressBar.Step4.LinkText", "Requirements");
      Step5ProgressBarItemLinkButton.Text = Step5ProgressBarItemLiteral.Text = CodeLocalise("ProgressBar.Step5.LinkText", "Recruitment Information");

		}

		/// <summary>
		/// Sets the step active or visited.
		/// </summary>
		/// <param name="step">The step.</param>
		/// <param name="stepControl">The step control.</param>
		private void SetStepActiveOrVisited(int step, HtmlGenericControl stepControl)
		{
			if (CurrentStep == step) stepControl.Attributes.Add("class", "active");
			else
			{
				var maxVisitedStep = (Model.Job.WizardStep == 1 && Model.Job.UpdatedOn == DateTime.MinValue) ? 0 : Model.Job.WizardStep + 1;
				if (maxVisitedStep >= step) stepControl.Attributes.Add("class", "visited");
			}
		}

		/// <summary>
		/// Handles the Click event of the PreviousButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void PreviousButton_Click(object sender, EventArgs e)
		{
			CurrentStep--;
      AdjustCurrentStep();
			ShowCurrentStep(true);
		}

		/// <summary>
		/// Handles the Click event of the NextButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void NextButton_Click(object sender, EventArgs e)
		{
			if (Page.IsValid)
        GoNext(adjustStep:true);
		}

		/// <summary>
		/// Handles the Command event of the StepProgressBarLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void StepProgressBarLinkButton_Command(object sender, CommandEventArgs eventArgs)
		{
      UnbindStep();
      CurrentStep = int.Parse(eventArgs.CommandArgument.ToString());

		  AdjustCurrentStep();
      ShowCurrentStep();
		}

    /// <summary>
    /// Go to a specific step
    /// </summary>
    /// <param name="step">The step</param>
    private void GotoStep(int step)
    {
      CurrentStep = step;

      ShowCurrentStep();
    }

		/// <summary>
		/// Unbinds the steps and populates the model
		/// </summary>
		private void UnbindStep()
		{
			if (StepsToSave.IsNull())
				StepsToSave = new List<int>();

			if (!StepsToSave.Contains(CurrentStep))
				StepsToSave.Add(CurrentStep);

			if (CurrentStep == 1) Model = Step1.UnbindStep(Model);

			if (CurrentStep == 3)
			{
				if (Model.Job.WizardPath == 1) Model = Step3Path1.UnbindStep(Model);
				if (Model.Job.WizardPath == 21) Model = Step3Path2Part1.UnbindStep(Model);
				if (Model.Job.WizardPath == 22) Model = Step3Path2Part2.UnbindStep(Model);
			}

			if (CurrentStep == 4) Model = Step4.UnbindStep(Model);
			if (CurrentStep == 5) Model = Step5.UnbindStep(Model);
		}

    /// <summary>
    /// Goes to the next step or step path / part
    /// </summary>
    /// <param name="wizardPath">The wizard path.</param>
    /// <param name="adjustStep">Whether to adjust the job description step</param>
    private void GoNext(int wizardPath = 0, bool adjustStep = false)
		{
			UnbindStep();

			var isNewAddress = false;

			// Unbind each step
			if (CurrentStep == 1)
			{
				Model.BusinessUnitAddress = ServiceClientLocator.EmployerClient(App).GetPrimaryBusinessUnitAddress(Model.Job.BusinessUnitId.Value);
				isNewAddress = true;
			}

			if (CurrentStep == 2)
			{
				Model.Job.WizardPath = wizardPath;
				Model.Job.Tasks = null;
			}

			// Update some system data
			if (CurrentStep > Model.Job.WizardStep) Model.Job.WizardStep = CurrentStep;
			Model.Job.UpdatedBy = App.User.UserId;

			if (!Handle(PageController.SaveJob(Model, StepsToSave, FromApproval, CurrentStep, FinalStep, isNewAddress)))
			{
				if (CurrentStep < FinalStep) CurrentStep++;
			}
      else if (Model.Job.WizardPath == 21) Model.Job.WizardPath++;

      if (adjustStep)
        AdjustCurrentStep();

			ShowCurrentStep();
			SetProgressBar();
			StepsToSave.Clear();
		}

    /// <summary>
    /// Whether to automatically adjust the current step
    /// </summary>
    private void AdjustCurrentStep()
    {
      if (CurrentStep == 2 && Model.Job.Description.IsNotNullOrEmpty())
        CurrentStep = 3;
    }

		#endregion

		#region Complete & Preview events

		/// <summary>
		/// Handles the PathSelected event of the Step2 control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void Step2_PathSelected(object sender, CommandEventArgs e)
		{
			GoNext((int)e.CommandArgument);
		}

		/// <summary>
		/// Handles the Click event of the PreviewButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void PreviewButton_Click(object sender, EventArgs e)
		{
			var previewModel = CloneModel();

			// Unbind current step
			if (CurrentStep == 1) previewModel = Step1.UnbindStep(previewModel);

			if (CurrentStep == 3 && Model.Job.WizardPath == 1) previewModel = Step3Path1.UnbindStep(previewModel);
			if (CurrentStep == 3 && Model.Job.WizardPath == 21) previewModel = Step3Path2Part1.UnbindStep(previewModel);
			if (CurrentStep == 3 && Model.Job.WizardPath == 22) previewModel = Step3Path2Part2.UnbindStep(previewModel);

			if (CurrentStep == 4) previewModel = Step4.UnbindStep(previewModel);
			if (CurrentStep == 5) previewModel = Step5.UnbindStep(previewModel);

			Handle(PageController.PreviewPosting(previewModel.Job));

			SetProgressBar();
		}

		/// <summary>
		/// Handles the PostCommand event of the JobPreview control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void JobPreview_PostCommand(object sender, EventArgs e)
		{
			ShowCurrentStep();
		}

		#endregion

		#region Localise UI

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			PreviewButtonTop.Text = PreviewButtonBottom.Text = CodeLocalise("PreviewButton.Text", "Preview");
			PreviousButtonTop.Text = PreviousButtonBottom.Text = CodeLocalise("PreviousButton.Text", "Previous Step");

			if (CurrentStep < 5 && Model.Job.JobStatus == JobStatuses.Draft && !FromApproval)
				NextButtonTop.Text = NextButtonBottom.Text = CodeLocalise("NextButtonDraft.Text", "Save Draft / Move to Next Step");
			else if (CurrentStep == 2 || (CurrentStep == 3 && Model.Job.WizardPath == 21)) // Editing an non-draft job's description
				NextButtonTop.Text = NextButtonBottom.Text = CodeLocalise("NextButton.Text", "Move to Next Step");
			else // Non draft jobs and on the last step of the wizard
				NextButtonTop.Text = NextButtonBottom.Text = CodeLocalise("NextButtonPost.Text", "Save and Post");


		}

		#endregion

		#region Other methods

		private JobWizardViewModel CloneModel()
		{
			var serializedModel = Model.Serialize();
			return (JobWizardViewModel)serializedModel.Deserialize(typeof(JobWizardViewModel));
		}

		#endregion

    protected void Step3Path1_OnGoToStep(object sender, CommandEventArgs eventargs)
    {
      Model = Step3Path1.UnbindStep(Model);
      GotoStep((int)eventargs.CommandArgument);
    }

    protected void Step3Path2Part2_OnGoToStep(object sender, CommandEventArgs eventargs)
    {
      Model = Step3Path2Part2.UnbindStep(Model);
      GotoStep((int)eventargs.CommandArgument);
    }
	}


}
