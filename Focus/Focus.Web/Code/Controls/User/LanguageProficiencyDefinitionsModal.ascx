﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LanguageProficiencyDefinitionsModal.ascx.cs" Inherits="Focus.Web.Code.Controls.User.LanguageProficiencyDefinitionsModal" %>

<asp:HiddenField ID="LanguageProficiencyDefinitionsPopupDummyTarget" runat="server" />
<act:ModalPopupExtender ID="LanguageProficiencyDefinitionsPopup" runat="server" BehaviorID="LanguageProficiencyDefinitionsPopup"
												TargetControlID="LanguageProficiencyDefinitionsPopupDummyTarget"
												PopupControlID="LanguageProficiencyDefinitionsPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground"
												CancelControlID="ReturnHomeButton"
												Y="50" />
                        
<asp:Panel ID="LanguageProficiencyDefinitionsPanel" runat="server" CssClass="modal" Width="700px" ClientIDMode="Static" Style="display: none;">
  <div style="float: left;">
    <table role="presentation">
      <tr>
        <td>
            <table id="LanguageProficiencyTable" class="LanguageProficiencyTable" role="presentation">
              <tr>
                <td>
                  <b><%= HtmlLocalise("LanguageProficiencyCodeTitle.Label", "Proficiency Code")%></b>
                </td>
                <td>
                  <b><%= HtmlLocalise("LanguageProficiencySpeakingDefinitionsTitle.Label", "Speaking Definitions")%></b>
                </td>
                <td>
                  <b><%= HtmlLocalise("LanguageProficiencyReadingDefinitionsTitle.Label", "Reading Definitions")%></b>
                </td>
              </tr>
              <tr>
                <td>
                  <%= HtmlLocalise("LanguageProficiencyCode1.Label", @"1 - Elementary Proficiency")%>                  
                </td>
                <td>
                  <%= HtmlLocalise("LanguageProficiencySpeakingDefinitions1.Label", @"Able to satisfy routine travel needs and minimum courtesy requirements.")%>
                </td>
                <td>
                  <%= HtmlLocalise("LanguageProficiencyReadingDefinitions1.Label", @"Able to read some personal and place names, street signs, office and shop designations, numbers and isolated words and phrases.")%>                  
                </td>
              </tr>
              <tr>
                <td>
                  <%= HtmlLocalise("LanguageProficiencyCode2.Label", @"2 - Limited Working Proficiency")%>                     
                </td>
                <td>
                  <%= HtmlLocalise("LanguageProficiencySpeakingDefinitions2.Label", @"Able to satisfy routine social demands and limited work requirements.")%>                  
                </td>
                <td>
                  <%= HtmlLocalise("LanguageProficiencyReadingDefinitions2.Label", @"Able to read simple prose, in a form equivalent to typescript or printing, on subjects within a familiar context.")%>                  
                </td>
              </tr>
              <tr>
                <td>
                  <%= HtmlLocalise("LanguageProficiencyCode3.Label", @"3 - Minimum Professional Proficiency")%>                   
                </td>
                <td>
                  <%= HtmlLocalise("LanguageProficiencySpeakingDefinitions3.Label", @"Able to speak the language with sufficient structural accuracy and vocabulary to participate effectively in most formal and informal conversations on practical, social, and professional topics.")%>     
                </td>
                <td>                  
                  <%= HtmlLocalise("LanguageProficiencyReadingDefinitions2.Label", @"Able to read standard newspaper items addressed to the general reader, routine correspondence, reports, and technical materials in the individual's special field.")%>   
                </td>
              </tr>
              <tr>
                <td>
                  <%= HtmlLocalise("LanguageProficiencyCode4.Label", @"4 - Full Professional Proficiency")%>
                </td>
                <td>                  
                  <%= HtmlLocalise("LanguageProficiencySpeakingDefinitions4.Label", @"Able to use the language fluently and accurately on all levels pertinent to professional needs.")%>
                </td>
                <td>
                  <%= HtmlLocalise("LanguageProficiencyReadingDefinitions4.Label", @"Able to read all styles and forms of the language pertinent to professional needs.")%>                
                </td>
              </tr>
              <tr>
                <td>                  
                  <%= HtmlLocalise("LanguageProficiencyCode5.Label", @"5 - Native or Bilingual Proficiency")%>
                </td>
                <td>                  
                  <%= HtmlLocalise("LanguageProficiencySpeakingDefinitions5.Label", @"Equivalent to that of an educated native speaker.")%>
                </td>
                <td>                  
                  <%= HtmlLocalise("LanguageProficiencyReadingDefinitions5.Label", @"Equivalent to that of an educated native.")%>
                </td>
              </tr>
            </table>
        </td>
      </tr>
      <tr>
        <td style="text-align: right;">
          <div class="modalButtonBorder" style="text-align: right; float: right;">
						<asp:Button ID="CloseLanguageProficiencyModal" runat="server" SkinID="Button1" OnClientClick="$find('LanguageProficiencyDefinitionsPopup').hide(); return false;" CausesValidation="false" Text="OK"/>
					</div>
        </td>
      </tr>
    </table>
  </div>
  <div class="closeIcon">
    <input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Button", "Close") %>" onclick="$find('LanguageProficiencyDefinitionsPopup').hide(); return false;"/>
  </div>
</asp:Panel>
