﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobWizardStep2.ascx.cs" Inherits="Focus.Web.Code.Controls.User.JobWizardStep2" %>

<asp:LinkButton ID="Path1Button" runat="server" CssClass="button7" OnClick="Path1Button_Click">
  <asp:Label runat="server" ID="Path1Label"></asp:Label>
</asp:LinkButton>
<asp:LinkButton ID="Path2Button" runat="server" CssClass="button7" OnClick="Path2Button_Click">
  <asp:Label runat="server" ID="Path2Label"></asp:Label>
</asp:LinkButton>

