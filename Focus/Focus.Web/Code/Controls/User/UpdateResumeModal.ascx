﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdateResumeModal.ascx.cs" Inherits="Focus.Web.Code.Controls.User.UpdateResumeModal" %>

<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:HiddenField ID="UpdateResumeModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="UpdateResumeModalPopup" runat="server" ClientIDMode="Static"
												TargetControlID="UpdateResumeModalDummyTarget"
												PopupControlID="UpdateResumeModalPanel" 
												PopupDragHandleControlID="UpdateResumeModalPanelHeader"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" />
<asp:Panel ID="UpdateResumeModalPanel" TabIndex="-1" runat="server" CssClass="modal" ClientIDMode="Static" Style="display:none">
	<div style="float:left;width:100%;">
		<asp:Panel runat="server" ID="UpdateResumeModalPanelHeader">
		  <h1><%= HtmlLocalise("UpdateResume.Label", "Update Resume") %></h1>
      <p><%= HtmlLocalise("UpdateResumeDescription.Label", "Your #CANDIDATETYPE#'s completed resumes will receive these updates")%></p>
      <table role="presentation">
        <tr>
          <td><%= App.Settings.HideEnrollmentStatus? "" : HtmlLabel(EnrollmentStatusDDL,"EnrollmentStatus", "Enrollment Status")%></td>
          <td>
            <asp:DropDownList ID="EnrollmentStatusDDL" runat="server" Title="Enrollment Status"></asp:DropDownList>
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <asp:RequiredFieldValidator ID="EnrollmentStatusRequiredValidator" runat="server" ControlToValidate="EnrollmentStatusDDL" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education"></asp:RequiredFieldValidator>
          </td>
        </tr>
        <tr>
          <td><%= HtmlLabel(EducationStatusDDL,"EducationLevel", "Education Level")%></td>
          <td>
            <asp:DropDownList ID="EducationStatusDDL" runat="server" ></asp:DropDownList>
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <asp:RequiredFieldValidator ID="EducationStatusRequiredValidator" runat="server" ControlToValidate="EducationStatusDDL" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education"></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="custEnrolmentEducationLevel" runat="server" ControlToValidate="EducationStatusDDL" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="ValidateEnrolmentEducationLevel" ValidateEmptyText="true" />
          </td>
         </tr>
         <tr>
          <td><%= HtmlLabel(EmploymentStatusDDL,"EmploymentStatus", "Employment Status")%></td>
          <td><asp:DropDownList ID="EmploymentStatusDDL" runat="server" ></asp:DropDownList></td>
        </tr>
        <tr>
        <td></td>
          <td>
            <asp:RequiredFieldValidator ID="EmploymentStatusRequireddValidator" runat="server" ControlToValidate="EmploymentStatusDDL" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education"></asp:RequiredFieldValidator>
          </td>
        </tr>
      </table>
      <div style="float:right">
             <asp:Button ID="UpdateDefaultResume" runat="server" OnClick="UpdateResumeAction_Click" Text="Update" ValidationGroup="Education" CssClass="button3" />
      </div>
		</asp:Panel>
    </div>
	<div class="closeIcon"><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('UpdateResumeModalPopup').hide();return false;" /></div>
</asp:Panel>

<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />

<script type="text/javascript">

  function ValidateEnrolmentEducationLevel(src, args) {
    var schoolStatus = $('#<%= EnrollmentStatusDDL.ClientID %> :selected').text();
    var education = $('#<%= EducationStatusDDL.ClientID %>');
    var educationLevel = $(education).val();

    if (educationLevel == "")
      return;

    educationLevel = educationLevel.substr(educationLevel.length - 2);
    
    if ((schoolStatus == 'In school, Post H.S.' || schoolStatus == 'Not attending school, H.S. Graduate') && educationLevel <= 12) {
      src.innerHTML = "<%= EnrollmentStatusErrorMessage %>";
      args.IsValid = false;
    }
    else if ((schoolStatus == 'In School, H.S. or less' || schoolStatus == 'Not attending school, H.S. Drop out' || schoolStatus == 'In School, Alternative School') && educationLevel > 12) {
      src.innerHTML = "<%= EducationLevelErrorMessage %>";
      args.IsValid = false;
    }
  }

</script>