﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobWizardStep3Path1.ascx.cs" Inherits="Focus.Web.Code.Controls.User.JobWizardStep3Path1" %>

<%@ Register src="~/Code/Controls/User/JobWizardKeywordsStatements.ascx" tagname="JobWizardKeywordsStatements" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobWizardSimilarJobs.ascx" tagname="JobWizardSimilarJobs" tagprefix="uc" %>

<table style="width:100%;" role="presentation">
  <tr>
    <td style="width:70%;"></td>
    <td style="width:4%;"></td>
    <td style="width:26%;"></td>
  </tr>
	<tr>
		<td colspan="2"><h1><asp:Label ID="JobTitleLabel" runat="server" /></h1></td>
    <td style="text-align: right">
      <asp:Button runat="server" ID="ResetDescriptionButton" CssClass="button3" OnClick="ResetDescriptionButton_OnClick" />
    </td>
	</tr>
	<tr>
		<td style="vertical-align:top;">
      <ul class="navTab localNavTab">
	      <li id="TabDescription" class="active"><span id="TabDescriptionLink" tabindex="0" role="tab"><asp:Literal runat="server" ID="TabDescriptionLabel"></asp:Literal></span></li>
          <li id="TabSimilar"><span id="TabSimilarLink" tabindex="-1" role="tab"><asp:Literal runat="server" ID="TabSimilarLabel"></asp:Literal></span></li>
      </ul>
      <div id="tabbedContent">
				<div id="JobDescriptionTab">					
					<asp:UpdatePanel ID="JobDescriptionUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:RequiredFieldValidator ID="JobDescriptionRequired" runat="server" ControlToValidate="JobDescriptionTextBox" SetFocusOnError="true" CssClass="error" Display="Dynamic" />
              <asp:RegularExpressionValidator ID="JobDescriptionValidator" runat="server" ControlToValidate="JobDescriptionTextBox" ValidationExpression="^(?:\S|[^\S\r\n]|\r?\n){0,6000}$" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							<asp:CustomValidator runat="server" ID="JobDecriptionHtmlValidator" ControlToValidate="JobDescriptionTextBox" CssClass="error" SetFocusOnError="true" Display="Dynamic" 							     ClientValidationFunction="JobWizardStep3Path1_CheckHTMLTag"></asp:CustomValidator>
							<asp:Literal runat="server" ID="JobDescriptionInfieldLabel"></asp:Literal>
							<asp:TextBox ID="JobDescriptionTextBox" runat="server" TextMode="MultiLine" Rows="25" ClientIDMode="Static" onkeyup="event.cancelBubble=true;" />
						</ContentTemplate>
						<Triggers>
							<asp:AsyncPostBackTrigger ControlID="JobDescriptionDropDownList" EventName="SelectedIndexChanged" />
						</Triggers>
					</asp:UpdatePanel>
					<br />
				</div>
				<div id="SimilarJobsTab">
					<uc:JobWizardSimilarJobs ID="SimilarJobs" runat="server" TargetControlId="JobDescriptionTextBox" OnAddEventJavascriptMethod="FocusOnJobDescription()" />
				</div>
			</div>
		</td>
		<td></td>
		<td style="vertical-align:top;">
			<br />
			<br />
			<br />
			<div id="OpenOrUploadSection">
				<div id="OpenOrUploadOverlay"></div>
					<asp:DropDownList ID="JobDescriptionDropDownList" runat="server" Width="300" AutoPostBack="true" OnSelectedIndexChanged="JobDescriptionDropDownList_SelectedIndexChanged" />
				  <br /><br />
          <asp:Label runat="server" ID="JobDescriptionHelp" Visible="False"></asp:Label>
          <asp:PlaceHolder runat="server" ID="UploadPlaceHolder">
					  <div><%= HtmlLocalise("JobDescriptionOr.Label", "Or")%></div>
					  <br />
					  <div class="asyncFileUploadButton">
						  <act:AsyncFileUpload ID="UploadJobDescriptionAsyncFileUpload" runat="server" ClientIDMode="Static" OnClientUploadError="UploadJobDescriptionUploadError" 
																   OnUploadedComplete="UploadJobDescriptionAsyncFileUpload_UploadedComplete" OnClientUploadComplete="UploadJobDescriptionUploadComplete" ThrobberID="UploadThrobberPanel" OnClientUploadStarted="UploadJobDescriptionCheckExtension"/>
						  <%=UploadText %>
					  </div>
					  <asp:Panel runat="server" ID="UploadThrobberPanel" EnableViewState="false" style="display:none;" CssClass="updateprogress" >
                        <font size="6">&nbsp;&nbsp;&nbsp;Uploading...</font>
					  </asp:Panel>
					  <div><i><%= HtmlLocalise("FileFormats.Label", ".pdf, .doc, .docx, .odf, .htm, .html, .rtf formats")%></i></div>
          </asp:PlaceHolder>
					<br />							
			</div>
			<br />
			<div id="KeywordsStatementsSection" class="opacity">
				<div id="KeywordsStatementsOverlay" class="overlay"></div>
				<uc:JobWizardKeywordsStatements ID="KeywordsStatements" runat="server" TargetControlId="JobDescriptionTextBox" DefaultOpenSection="Statements" />
			</div>	
		</td>
	</tr>
</table>

<script type="text/javascript">
	$(document).ready(function () {
		$("#JobDescriptionTextBox").keyup(HideOrShowKeywordsStatements);
		$("#JobDescriptionTextBox").on('cut paste', function () { window.setTimeout(HideOrShowKeywordsStatements, 1); });
		HideOrShowKeywordsStatements();
		$("#SimilarJobsTab").hide();

		$('body').keyup(function (e) {
			if (e.keyCode == 39) {
				$('#TabSimilarLink').focus();
				ShowSimilarJobsTab();
			} else if (e.keyCode == 37) {
				$("#TabDescriptionLink").focus();
				ShowJobDescriptionTab();
			}
		});

		$("#TabDescriptionLink").click(ShowJobDescriptionTab);
		$("#TabSimilarLink").click(ShowSimilarJobsTab);

		$('.inFieldLabel > label, .inFieldLabelAlt > label').inFieldLabels();


		$('#MainContent_WizardEducation_NextButtonTop,#MainContent_WizardEducation_NextButtonBottom')
      .bind('click', function (event) {
      	if ($('#<%=JobDescriptionRequired.ClientID %>').css("display") != 'none') {
      		ShowJobDescriptionTab();
      	}
      });
	});

  var prm = Sys.WebForms.PageRequestManager.getInstance();

  prm.add_endRequest(function () {
  	$('.inFieldLabel > label, .inFieldLabelAlt > label').inFieldLabels();
    $("#JobDescriptionTextBox").keyup(HideOrShowKeywordsStatements);
    $("#JobDescriptionTextBox").on('cut paste', function () { window.setTimeout(HideOrShowKeywordsStatements, 1); });
    HideOrShowKeywordsStatements();
  });

   function DisableOverlayControls(id) {
   	var overlayInputs = $("#" +id +" :input");
   	overlayInputs.prop('disabled', true);
   } 

  function EnableOverlayControls(id) {
		var overlayInputs = $("#" +id +" :input");
		overlayInputs.prop('disabled', false);
	}

  function ShowJobDescriptionTab() {
	  $("#TabDescription").addClass("active");
		$("#TabSimilar").removeClass("active");
		$("#JobDescriptionTab").show();
		$("#SimilarJobsTab").hide();
		HideOrShowOnTabSelect(true);
	}

	function ShowSimilarJobsTab() {
	  $("#TabDescription").removeClass("active");
		$("#TabSimilar").addClass("active");
		$("#JobDescriptionTab").hide();
		$("#SimilarJobsTab").show();
		HideOrShowOnTabSelect(false);
	}

	function HideOrShowKeywordsStatements() {
		if ($("#JobDescriptionTextBox").val().length > 0)
		{
			$("#KeywordsStatementsSection").removeClass("opacity");
			$("#KeywordsStatementsOverlay").removeClass("overlay");
			EnableOverlayControls("KeywordsStatementsOverlay");
		}
		else
		{
			$("#KeywordsStatementsSection").addClass("opacity");
			$("#KeywordsStatementsOverlay").addClass("overlay");
			DisableOverlayControls("KeywordsStatementsOverlay");
		}
	}

	function HideOrShowOnTabSelect(show)
	{
		if (show)
		{
			HideOrShowKeywordsStatements();
			$("#OpenOrUploadSection").removeClass("opacity");
			$("#OpenOrUploadOverlay").removeClass("overlay");
			EnableOverlayControls("OpenOrUploadSection");
		}
		else
		{
			$("#KeywordsStatementsSection").addClass("opacity");
			$("#KeywordsStatementsOverlay").addClass("overlay");
			$("#OpenOrUploadSection").addClass("opacity");
			$("#OpenOrUploadOverlay").addClass("overlay");
			DisableOverlayControls("OpenOrUploadSection");
		}
	}

	function FocusOnJobDescription()
	{
		HideOrShowKeywordsStatements();
		ShowJobDescriptionTab();
	}

	function UploadJobDescriptionUploadComplete(sender, args)
	{
		var options = { type: "POST",
			url: "<%= UrlBuilder.AjaxService() %>/GetUploadedJobDescription",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			async: true,
			success: function (response)
			{
				var results = response.d;
				$("#JobDescriptionTextBox").val(results);
				HideOrShowKeywordsStatements();
				$('.inFieldLabel > label, .inFieldLabelAlt > label').inFieldLabels();
			}
		};

		$.ajax(options);
	}

	function UploadJobDescriptionUploadError(sender, args)
	{
		// TODO: Need to make this display a confirmation box
		alert('Error uploading ' + args.get_fileName() + ' - ' + args.get_errorMessage());
  }

  function UploadJobDescriptionCheckExtension(sender, args) {
  	if (sender._inputFile.files != null && sender._inputFile.files[0].size >= 4000000) {
  		var sizeError = new Error();
  		sizeError.name = "Upload error";
  		sizeError.message = "File too large. Maximum size is 4MB";
  		throw (sizeError);
  		
  		//alert("File too large. Maximum size is 4MB");
  		//sender._stopLoad();
  		//return false;
  	}
	  
    var fileName = args.get_fileName().toString();
    var ext = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
    if (ext != 'pdf' && ext != 'doc' && ext != 'docx' && ext != 'odf' && ext != 'html' && ext != 'htm' && ext != 'rtf') {
      var err = new Error();
      err.name = 'Upload error';
      err.message = 'Only .pdf, .doc, .docx, .odf, .htm, .html, .rtf formats can be uploaded.';
      throw (err);
    }
    return true;
  }

  function JobWizardStep3Path1_CheckHTMLTag(sender, args) {
  	var patt = new RegExp("<[A-Za-z]");
  	args.IsValid = !patt.test(args.Value);
  }
</script>

