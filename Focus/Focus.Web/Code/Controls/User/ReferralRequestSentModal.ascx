﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReferralRequestSentModal.ascx.cs" Inherits="Focus.Web.Code.Controls.User.ReferralRequestSentModal" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>

<asp:HiddenField ID="AutoApprovalDummyTarget" runat="server" />

<act:ModalPopupExtender ID="AutoApprovalModalPopup" runat="server" 
  BehaviorID="AutoApprovalModal"
	TargetControlID="AutoApprovalDummyTarget" 
  PopupControlID="AutoApprovalModalPanel" 
  BackgroundCssClass="modalBackground" 
  RepositionMode="RepositionOnWindowResizeAndScroll">
</act:ModalPopupExtender>

<asp:Panel ID="AutoApprovalModalPanel" TabIndex="-1" runat="server" CssClass="modal" Style="z-index:100001; display:none">
	<table style="width:400px;">
		<tr>
			<th style="vertical-align:top" data-modal="title">
			  <asp:Literal runat="server" ID="Title" />
			</th>
		</tr>
		<tr>
			<td style="vertical-align:top; height:150px;" data-modal="title;body">
			  <asp:Literal runat="server" ID="Details" />
			</td>
		</tr>
		<tr>
			<td style="text-align: center">
				<asp:Button ID="OkButton" runat="server" SkinID="Button1" CausesValidation="false" OnClick="OkButton_OnClick" />
				&nbsp;
				<asp:Button ID="CancelButton" runat="server" SkinID="Button1" CausesValidation="false" />
			</td>
		</tr>
	</table>
</asp:Panel>

<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />

<script type="text/javascript">
  Sys.Application.add_load(function () {
    var popup = $find('<%=AutoApprovalModalPopup.ClientID %>');
    if (popup != null) {
      popup.add_shown(function() { setButtonFocus('#<%=CancelButton.ClientID %>');
      });
    }
  });
</script>
