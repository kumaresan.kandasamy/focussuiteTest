﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageUploader.ascx.cs" Inherits="Focus.Web.Code.Controls.User.ImageUploader" %>

<table style="width:100%;" role="presentation" >
	<tr>
		<td>
			<div id="UploaderDiv">
			    <focus:LocalisedLabel runat="server" ID="UploadImageAsyncFileUploadLabel" AssociatedControlID="UploadImageAsyncFileUpload" LocalisationKey="UploadImageAsyncFileUploadLabel.Label" DefaultText="Upload Image Async File Upload" CssClass="sr-only"/>
			    <label  class="visuallyhidden">Search: 
				<act:AsyncFileUpload ID="UploadImageAsyncFileUpload"  runat="server" ClientIDMode="Static" CssClass="asyncfileuploadfix fileupload" ThrobberID="UploadThrobberPanel"
																	OnClientUploadStarted="UploadImageCheckExtension" OnClientUploadComplete="UploadImageUploadComplete" Title="UploadImageAsyncFileUpload"
																	OnUploadedComplete="UploadImageAsyncFileUpload_UploadedComplete" /></label>
				</div>
				<br />
				<div id="ImageDiv"><asp:Image ID="UploadedImage" runat="server" ClientIDMode="Static" AlternateText="Uploaded image" Width="10" Height="10"/></div>
				<focus:LocalisedLabel runat="server" ID="UploadedImageFileTypeLabel" AssociatedControlID="UploadedImageFileType" LocalisationKey="UploadedImageFileType.Label" DefaultText="Uploaded image type" CssClass="sr-only"/>
				<asp:TextBox ID="UploadedImageFileType" runat="server" ClientIDMode="Static" />
				<asp:CustomValidator ID="UploadImageAsyncFileUploadedImageFileTypeValidator" runat="server" ControlToValidate="UploadedImageFileType" 
													ClientValidationFunction="UploadImageAsyncFileUploadImageFileTypeValidator_Validate" CssClass="error" />
				<asp:TextBox ID="UploadedImageIsValid" runat="server" ClientIDMode="Static" Tooltip="Is image valid"/><br/>
				<asp:CustomValidator ID="UploadImageAsyncFileUploadedImageIsValidValidator" runat="server" ControlToValidate="UploadedImageIsValid"  
													ClientValidationFunction="UploadImageAsyncFileUploadedImageIsValidValidator_Validate" ClientIDMode="Static" CssClass="error" Display="Dynamic" />
			
			<asp:Panel runat="server" ID="UploadThrobberPanel" EnableViewState="false" style="display:none;" CssClass="updateprogress" >
			<font size="6">&nbsp;&nbsp;&nbsp;Uploading...</font>
			</asp:Panel>
		</td>
	</tr>
</table>	
<script type="text/javascript">
  Sys.Application.add_load(UploadImageStartup);
</script>
UploadImageAsyncFileUpload