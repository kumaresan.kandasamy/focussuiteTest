﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecurityQuestionsModal.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SecurityQuestionsModal" %>
<%@ Register Src="~/Code/Controls/User/SecurityQuestions.ascx" TagName="SecurityQuestions" TagPrefix="uc" %>

<asp:HiddenField ID="QuestionsModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="QuestionsModal" runat="server" BehaviorID="QuestionsModal"
												TargetControlID="QuestionsModalDummyTarget"
												PopupControlID="QuestionsModalPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground"
												Y="50" />

<asp:Panel ID="QuestionsModalPanel" runat="server" CssClass="modal" Width="850px" ClientIDMode="Static" Style="display:none;" >
	<div style="float:left;">
		<uc:SecurityQuestions ID="SecurityQuestionsPanel" runat="server" PageMode="Register" />
	</div>
</asp:Panel>