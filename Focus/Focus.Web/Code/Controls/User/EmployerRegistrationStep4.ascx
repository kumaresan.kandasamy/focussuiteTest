<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployerRegistrationStep4.ascx.cs"
    Inherits="Focus.Web.Code.Controls.User.EmployerRegistrationStep4" %>
<div>
    <table class="formTable" role="presentation">
        <tr>
            <td width="15%" />
            <td width="30%" />
            <td width="75px" />
            <td width="15%" />
            <td width="30%" />
        </tr>
        <tr>
            <td colspan="2">
                <%= HtmlRequiredFieldsInstruction() %>
            </td>
            <td />
            <td colspan="2">
            </td>
        </tr>
        <tr>
            <td class="label">
                <%= HtmlRequiredLabel(FirstNameTextBox, "FirstName.Label", "First name")%>
            </td>
            <td>
                <asp:TextBox ID="FirstNameTextBox" runat="server" ClientIDMode="Static" Width="98%"
                    TabIndex="1" MaxLength="50" />
                <asp:RequiredFieldValidator ID="FirstNameRequired" runat="server" ControlToValidate="FirstNameTextBox"
                    SetFocusOnError="true" Display="Dynamic" CssClass="error" />
            </td>
            <td />
            <td rowspan="13">
            </td>
            <td rowspan="13">
            </td>
        </tr>
        <tr>
            <td class="label">
                <%= HtmlRequiredLabel(LastNameTextBox, "LastName.Label", "Last name")%>
            </td>
            <td>
                <asp:TextBox ID="LastNameTextBox" runat="server" ClientIDMode="Static" Width="98%"
                    TabIndex="2" MaxLength="50" />
                <asp:RequiredFieldValidator ID="LastNameRequiredField" runat="server" ControlToValidate="LastNameTextBox"
                    SetFocusOnError="true" Display="Dynamic" CssClass="error" />
            </td>
            <td />
        </tr>
        <tr>
            <td class="label">
                <%= HtmlLabel(MiddleInitialTextBox, "MiddleInitial.Label", "Middle initial")%>
            </td>
            <td>
                <asp:TextBox ID="MiddleInitialTextBox" runat="server" ClientIDMode="Static" Width="95px"
                    TabIndex="3" MaxLength="5" />
                <div style="float: right">
                    <%--<focus:LocalisedLabel runat="server" ID="SuffixLabel" DefaultText="Suffix" LocalisationKey="Suffix.Label"
                                          AssociatedControlID="SuffixDropDownList" />--%>
                    <%= HtmlLabel(SuffixDropDownList, "Suffix.Label", "Suffix")%>
                    <asp:DropDownList runat="server" ID="SuffixDropDownList" ClientIDMode="Static" Width="140px" />
                </div>
            </td>
            
        </tr>
        <tr>
            <td class="label">
                <%= HtmlRequiredLabel(PersonalTitleDropDownList, "PersonalTitle.Label", "Title")%>
            </td>
            <td>
                <asp:DropDownList ID="PersonalTitleDropDownList" runat="server" ClientIDMode="Static"
                    Width="100px" TabIndex="4" />
                <asp:RequiredFieldValidator ID="PersonalTitleRequired" runat="server" ControlToValidate="PersonalTitleDropDownList"
                    SetFocusOnError="true" Display="Dynamic" CssClass="error" />
            </td>
            <td />
        </tr>
        <tr>
            <td class="label">
                <focus:LocalisedLabel runat="server" ID="JobTitleLabel" DefaultText="Job title" LocalisationKey="JobTitle.Label"
                    AssociatedControlID="JobTitleTextBox" />
            </td>
            <td>
                <asp:TextBox ID="JobTitleTextBox" runat="server" ClientIDMode="Static" Width="98%"
                    TabIndex="5" MaxLength="200" />
                <asp:RequiredFieldValidator ID="JobTitleRequired" runat="server" ControlToValidate="JobTitleTextBox"
                    SetFocusOnError="true" Display="Dynamic" CssClass="error" Enabled="False" />
            </td>
            <td />
        </tr>
        <tr>
            <td class="label">
                <%= HtmlRequiredLabel(AddressLine1TextBox, "AddressLine1.Label", "Address")%>
            </td>
            <td>
                <asp:TextBox ID="AddressLine1TextBox" runat="server" ClientIDMode="Static" Width="98%"
                    TabIndex="6" MaxLength="200" />
                <asp:RequiredFieldValidator ID="AddressLine1Required" runat="server" ControlToValidate="AddressLine1TextBox"
                    SetFocusOnError="true" Display="Dynamic" CssClass="error" />
            </td>
            <td />
        </tr>
        <tr>
            <td class="label">
                <label for="AddressLine2TextBox" class="hidden">Address Line 2</label>
            </td>
            <td>
                <asp:TextBox ID="AddressLine2TextBox" runat="server" ClientIDMode="Static" Width="98%"
                    TabIndex="6" MaxLength="200" />
            </td>
            <td />
        </tr>
        <tr>
            <td class="label">
                <%= HtmlRequiredLabel(AddressPostcodeZipTextBox, "AddressPostcodeZip.Label", "ZIP or postal code")%>
            </td>
            <td>
                <asp:TextBox ID="AddressPostcodeZipTextBox" runat="server" ClientIDMode="Static"
                    Width="90px" TabIndex="6" MaxLength="10" />
                <asp:RequiredFieldValidator ID="AddressPostcodeZipRequired" runat="server" ControlToValidate="AddressPostcodeZipTextBox"
                    SetFocusOnError="true" Display="Dynamic" CssClass="error" />
                <asp:RegularExpressionValidator ID="AddressPostcodeRegexValidator" runat="server"
                    CssClass="error" ControlToValidate="AddressPostcodeZipTextBox" SetFocusOnError="true"
                    Display="Dynamic" />
            </td>
            <td />
        </tr>
        <tr>
            <td class="label">
                <%= HtmlRequiredLabel(AddressTownCityTextBox, "AddressTownCity.Label", "City")%>
            </td>
            <td>
                <asp:TextBox ID="AddressTownCityTextBox" runat="server" ClientIDMode="Static" Width="98%"
                    TabIndex="7" MaxLength="200" />
                <asp:RequiredFieldValidator ID="AddressCityRequired" runat="server" ControlToValidate="AddressTownCityTextBox"
                    SetFocusOnError="true" Display="Dynamic" CssClass="error" />
            </td>
            <td />
        </tr>
        <tr>
            <td class="label">
                <%= HtmlRequiredLabel(AddressStateDropDownList, "AddressState.Label", "State")%>
            </td>
            <td>
                <asp:DropDownList ID="AddressStateDropDownList" runat="server" ClientIDMode="Static"
                    Width="100%" TabIndex="8" onchange="UpdateCountry()" />
                <asp:RequiredFieldValidator ID="AddressStateRequired" runat="server" ControlToValidate="AddressStateDropDownList"
                    SetFocusOnError="true" Display="Dynamic" CssClass="error" />
            </td>
            <td />
        </tr>
        <tr id="CountyRow" runat="server">
            <td class="label">
                <%= HtmlRequiredLabel(AddressCountyDropDownList, "AddressCounty.Label", "County")%>
            </td>
            <td>
                <focus:AjaxDropDownList ID="AddressCountyDropDownList" runat="server" Width="100%"
                    ClientIDMode="Static" onchange="UpdateStateCountry()" TabIndex="9" />
                <act:CascadingDropDown ID="AddressCountyDropDownListCascadingDropDown" runat="server"
                    TargetControlID="AddressCountyDropDownList" ParentControlID="AddressStateDropDownList"
                    ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetCounties" Category="Counties"
                    BehaviorID="AddressCountyDropDownListCascadingDropDown" />
                <asp:CustomValidator ID="AddressCountyRequired" ValidateEmptyText="True" runat="server"
                    ControlToValidate="AddressCountyDropDownList" SetFocusOnError="true" Display="Dynamic"
                    CssClass="error" ClientValidationFunction="AddressCountyRequiredValidate" />
            </td>
            <td />
        </tr>
        <tr>
            <td class="label">
                <%= HtmlRequiredLabel(AddressCountryDropDownList, "AddressCountry.Label", "Country")%>
            </td>
            <td>
                <asp:DropDownList ID="AddressCountryDropDownList" runat="server" ClientIDMode="Static"
                    onchange="UpdateStateCounty();" Width="100%" TabIndex="10" />
                <asp:RequiredFieldValidator ID="AddressCountryRequired" runat="server" ControlToValidate="AddressCountryDropDownList"
                    SetFocusOnError="true" Display="Dynamic" CssClass="error" />
            </td>
            <td />
        </tr>
        <tr>
            <td class="label">
                <%= HtmlRequiredLabel(ContactPhoneTextBox, "ContactPhone.Label", "Phone number")%>
            </td>
            <td nowrap="nowrap">
                <asp:TextBox ID="ContactPhoneTextBox" runat="server" ClientIDMode="Static" Width="50%"
                    TabIndex="11" MaxLength="20" />
                <asp:TextBox ID="ExtensionTextBox" runat="server" ClientIDMode="Static" Width="15%"
                    TabIndex="12" MaxLength="5" /><label for="ExtensionTextBox" class="hidden">Phone Extension</label>
                <act:MaskedEditExtender ID="ExtensionMaskedEdit" runat="server" MaskType="Number"
                    TargetControlID="ExtensionTextBox" PromptCharacter=" " ClearMaskOnLostFocus="false"
                    AutoComplete="false" EnableViewState="true" ClearTextOnInvalid="false" Mask="99999" />
                <asp:DropDownList ID="PhoneTypeDropDownList" runat="server" ClientIDMode="Static"
                    Width="30%" TabIndex="13" /><label for="PhoneTypeDropDownList" class="hidden">phone Type</label>
                <asp:CustomValidator ID="ContactPhoneRequired" runat="server" ControlToValidate="ContactPhoneTextBox"
                    ClientValidationFunction="EmployerRegistrationStep4_ValidatePhone" ValidateEmptyText="True"
                    SetFocusOnError="true" Display="Dynamic" CssClass="error" />
            </td>
            <td />
        </tr>
        <tr>
            <td class="label">
                <%= HtmlLabel(ContactAlternatePhoneTextBox, "ContactAlternatePhone.Label", "Alternate phone number 1")%>
            </td>
            <td>
                <asp:TextBox ID="ContactAlternatePhoneTextBox" runat="server" ClientIDMode="Static"
                    Width="50%" TabIndex="14" MaxLength="20" />
                <asp:DropDownList ID="AltPhoneTypeDropDownList" runat="server" ClientIDMode="Static"
                    Width="30%" TabIndex="15" /><label for="AltPhoneTypeDropDownList" class="hidden">Alternate phone 1 Type</label>
            </td>
            <td />
        </tr>
        <tr>
            <td class="label">
                <%= HtmlLabel(ContactAlternate2PhoneTextBox, "ContactFax.Label", "Alternate phone number 2")%>
            </td>
            <td>
                <asp:TextBox ID="ContactAlternate2PhoneTextBox" runat="server" ClientIDMode="Static"
                    Width="50%" TabIndex="16" MaxLength="20" />
                <asp:DropDownList ID="AltPhoneType2DropDownList" runat="server" ClientIDMode="Static"
                    Width="30%" TabIndex="17" /><label for="AltPhoneType2DropDownList" class="hidden">Alternate phone 2 Type</label>
            </td>
            <td />
        </tr>
        <tr id="emailRow" runat="server">
            <td class="label">
                <%= HtmlRequiredLabel(ContactEmailAddressTextBox, "ContactEmailAddress.Label", "Email address")%>
            </td>
            <td>
                <asp:TextBox ID="ContactEmailAddressTextBox" runat="server" ClientIDMode="Static"
                    Width="98%" TabIndex="17" MaxLength="200" Enabled="False" />
                <asp:RequiredFieldValidator ID="ContactEmailAddressRequired" runat="server" ControlToValidate="ContactEmailAddressTextBox"
                    SetFocusOnError="true" Display="Dynamic" CssClass="error" />
                <asp:RegularExpressionValidator ID="ContactEmailAddressRegEx" runat="server" ControlToValidate="ContactEmailAddressTextBox"
                    SetFocusOnError="true" Display="Dynamic" CssClass="error" />
            </td>
            <td />
        </tr>
    </table>
</div>
<script type="text/javascript">
	$(document).ready(
		function() {
			$('#ContactPhoneTextBox').mask('(999) 999-9999', { placeholder: ' ' });
			$('#ContactAlternatePhoneTextBox').mask('(999) 999-9999', { placeholder: ' ' });
			$('#ContactAlternate2PhoneTextBox').mask('(999) 999-9999', { placeholder: ' ' });
			$('#AddressPostcodeZipTextBox').mask('<%= App.Settings.ExtendedPostalCodeMaskPattern %>', { placeholder: ' ' });
			$('#AddressPostcodeZipTextBox').change(function() {
			  PopulateAddressFieldsForPostalCode(
			    '<%= UrlBuilder.AjaxService() %>',
		      $(this).val(), 
			    'AddressStateDropDownList',
			    'AddressCountyDropDownList',
			    '<%=AddressCountyDropDownListCascadingDropDown.BehaviorID%>', 
			    '<%=HtmlLocalise("Global.County.TopDefault", "- select county -") %>',
			    'AddressTownCityTextBox');
			});
			UpdateCountry();
		});
		
	Sys.Application.add_load(Contact_PageLoad);

	function Contact_PageLoad(sender, args) {
		var isCounty = '<%= IsCountyEnabled %>';

		if (isCounty == 'True') {
			var behavior = $find('<%=AddressCountyDropDownListCascadingDropDown.BehaviorID %>');      
			if (behavior != null) {
				behavior.add_populated(function() {
					Contact_UpdateCountyDropDown($('#AddressCountyDropDownList'), $("#AddressStateDropDownList"));
				});
			}
		}
	}
		
	function Contact_UpdateCountyDropDown(countyDropDown, stateDropDown) {
			if (countyDropDown.children('option:selected').val() != '')
				countyDropDown.next().find(':first-child').text(countyDropDown.children('option:selected').text()).parent().addClass('changed');
			else
				SetCounty(stateDropDown.val() == <%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>);
        
			UpdateStyledDropDown(countyDropDown, stateDropDown.val());
		}
  
	  function UpdateStateCountry() {
			var countyValue = $('#AddressCountyDropDownList').val();
			var stateValue = $('#AddressStateDropDownList').val();

			if (countyValue == '<%= GetLookupId(Constants.CodeItemKeys.Counties.OutsideUS) %>') {
				if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>')
					SetState(true);
				
				SetCountry(true);
			}
		}
	  
		function County_InvokeCountyCascadeDropdown() {
			$get("<%= AddressCountyDropDownList.ClientID %>")._behaviors[0]._onParentChange(null, null);
		}

    function UpdateStateCounty() {
			var isCounty = '<%= IsCountyEnabled %>';
			var countryValue = $('#AddressCountryDropDownList').val();
			var stateDropdown = $('#AddressStateDropDownList');
			var stateValue = stateDropdown.val();

			if (countryValue == '<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>') {
				if (stateValue == '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>') {
       		SetState(false);

       		if (isCounty == 'True')
       			County_InvokeCountyCascadeDropdown();
				}
			}
			else {
				if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>') {
       		SetState(true);

       		if (isCounty == 'True')
       			County_InvokeCountyCascadeDropdown();
				}
			}
    }
       
		function SetCounty(outsideUS) {
			var countyDropDown = $('#AddressCountyDropDownList');

			if (outsideUS)
				$(countyDropDown).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.Counties.OutsideUS) %>"]').prop('selected', true);
			else
				$(countyDropDown).children().first().prop('selected', true);
	  
			$(countyDropDown).next().find(':first-child').text($(countyDropDown).children('option:selected').text()).parent().addClass('changed');
		}
			 
		function SetCountry(outsideUS) {
			var countryDropDown = $('#AddressCountryDropDownList');

			if (outsideUS)
				$(countryDropDown).children().first().prop('selected', true);
			else 
				$(countryDropDown).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>"]').prop('selected', true);
	  
			$(countryDropDown).next().find(':first-child').text($(countryDropDown).children('option:selected').text()).parent().addClass('changed');
		}

    function SetState(outsideUS) {
			var stateDropDown = $('#AddressStateDropDownList');

			if (outsideUS)
				$(stateDropDown).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>"]').prop('selected', true);
			else
				$(stateDropDown).children().first().prop('selected', true);

			$(stateDropDown).next().find(':first-child').text($(stateDropDown).children('option:selected').text()).parent().addClass('changed');
    }
       
		function UpdateCountry() {
			var stateValue = $('#AddressStateDropDownList').val();

			if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>') {
				$("#AddressPostcodeZipTextBox").mask("<%= App.Settings.ExtendedPostalCodeMaskPattern %>", { placeholder: " " });
			}
			else {
				$("#AddressPostcodeZipTextBox").unmask();
			}

			SetCountry(stateValue == '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>');
		}
		
		function AddressCountyRequiredValidate(sender, args) {
    	args.IsValid = args.Value != '';
    }
		
	  function EmployerRegistrationStep4_ValidatePhone(sender, args) {
	    var trimmed = args.Value.trim();
		  if (trimmed == "(   )    -" || trimmed == "") {
		    args.IsValid = false;
		  }
	  }
</script>
