﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecurityQuestions.ascx.cs"
    Inherits="Focus.Web.Code.Controls.User.SecurityQuestions" %>
<h2 class="questheader">
    <asp:Literal runat="server" ID="HeaderLiteral"></asp:Literal>
    <br/>
</h2>
<span class="instructions">
    <asp:Label runat="server" ID="InstructionsLabel" Text="answers are case-sensitive"/>
</span>
<br/>
<asp:Panel Style="padding-right: 5px;" runat="server" ID="HelpText1" Visible="False">
    <asp:Literal runat="server" ID="HelpTextLiteral1"></asp:Literal>&nbsp;&nbsp;
</asp:Panel>
<br />
<asp:UpdatePanel ID="UpdateSecurityQuestionUpdatePanel" runat="server" UpdateMode="Conditional"
    Class="questpanel">
    <ContentTemplate>
        <asp:Repeater runat="server" ID="SecurityQuestionRepeater" OnItemDataBound="SecurityQuestionRepeater_ItemDataBound">
            <ItemTemplate>
                <div class="questbox" id="QuestionBox" runat="server">
                    <h4 class="questboxheader">
                        <asp:Literal runat="server" ID="SecurityQuestionNumber"></asp:Literal>
                    </h4>
                    <table role="presentation">
                        <tr style="height: 25px; vertical-align: top">
                            <td style="white-space: nowrap">
                                <%= HtmlRequiredLabel(QuestionDropDown, "TalentSecurityQuestion.Label", "Question")%>
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="SecurityQuestionDropDown" title="Security Question DropDown"
                                    Style="margin-left: 1px;" Enabled="False" CssClass="SecurityQuestionDropDown" />
                                <br />
                                <asp:RequiredFieldValidator ID="SecurityQuestionRequired" runat="server" ControlToValidate="SecurityQuestionDropDown"
                                    ValidationGroup="SecurityQuestionAnswer" SetFocusOnError="true" Display="Dynamic"
                                    CssClass="error" />
                            </td>
                        </tr>
                        <tr style="height: 25px; vertical-align: top">
                            <td style="white-space: nowrap">
                                <%= HtmlRequiredLabel(AnswerTextBox, "TalentSecurityAnswer.Label", "Answer")%>
                            </td>
                            <td>
                                <asp:TextBox ID="SecurityAnswerTextBox" runat="server" title="Security Answer TextBox"
                                    TextMode="SingleLine" MaxLength="100" CssClass="SecurityAnswerTextBox"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="SecurityAnswerRequired" runat="server" ControlToValidate="SecurityAnswerTextBox"
                                    ValidationGroup="SecurityQuestionAnswer" SetFocusOnError="true" Display="Dynamic"
                                    CssClass="error" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td style="padding-top: 8px">
                                <asp:Button runat="server" CssClass="button2 right" OnCommand="UpdateSecurityQuestionAnswer_OnCommand"
                                    ValidationGroup="SecurityQuestionAnswer" ID="UpdateSecurityQuestionAnswer" />
                            </td>
                        </tr>
                    </table>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <asp:PlaceHolder runat="server" ID="ButtonsPanel" Visible="False">
            <div style="width: 100%; text-align: right">
                <br />
                <asp:Button runat="server" CssClass="button1" ID="SaveAllButton" OnClick="SaveAllButton_Click" />
            </div>
        </asp:PlaceHolder>
        <asp:HiddenField ID="ModalDummyTarget" runat="server" />
        <act:ModalPopupExtender ID="ModalPopup" runat="server" BehaviorID="SecurityQuestions"
            TargetControlID="ModalDummyTarget" PopupControlID="UpdateSecurityQuestionModalPanel"
            PopupDragHandleControlID="UpdateSecurityQuestionModalPanelHeader" RepositionMode="RepositionOnWindowResizeAndScroll"
            BackgroundCssClass="modalBackground" />
        <asp:Panel ID="UpdateSecurityQuestionModalPanel" runat="server" CssClass="modal"
            Style="display: none;">
            <div>
                <asp:Panel runat="server" ID="UpdateSecurityQuestionModalPanelHeader">
                    <h4 class="questboxheader" runat="server" id="QuestionBoxHeaderModal">
                        UpdateSecurityQuestion</h4>
                    <div class="closeIcon">
                        <asp:Literal runat="server" ID="LocaliseClose"></asp:Literal><input type="image"
                            src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text.NoEdit", "Close") %>"
                            onclick="$find('SecurityQuestions').hide(); return false;" /></div>
                </asp:Panel>
                <asp:HiddenField runat="server" ID="EditIndex" />
                <table role="presentation">
                    <tr style="height: 25px;">
                        <td style="width: 35%;">
                            <%= HtmlRequiredLabel(QuestionDropDown, "TalentSecurityQuestion.Label", "Question")%>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="QuestionDropDown" Width="250" Style="margin-left: 1px;" />
                            <asp:RequiredFieldValidator ID="QuestionRequired" runat="server" ControlToValidate="QuestionDropDown"
                                SetFocusOnError="true" ValidationGroup="PopupModal" Display="Dynamic" CssClass="error" />
                        </td>
                    </tr>
                    <tr style="height: 25px;">
                        <td style="width: 35%;">
                            <%= HtmlRequiredLabel(AnswerTextBox, "TalentSecurityAnswer.Label", "Answer")%>
                        </td>
                        <td>
                            <asp:TextBox ID="AnswerTextBox" runat="server" Width="244" MaxLength="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="AnswerRequired" runat="server" Visible="False" ControlToValidate="AnswerTextBox"
                                SetFocusOnError="true" ValidationGroup="PopupModal" Display="Dynamic" CssClass="error" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right">
                            <asp:Button runat="server" CssClass="button2" ID="UpdateSecurityQuestion" ValidationGroup="PopupModal"
                                OnClick="UpdateSecurityQuestionButton_Click" />
                            <asp:Button runat="server" CssClass="button2 right" ID="CancelUpdateSecurityQuestion"
                                OnClick="CancelUpdateSecurityQuestionButton_Click" ValidationGroup="None" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:Panel runat="server" ID="ClientJavaScriptCode" Visible="False">
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {
            var questions = $(".SecurityQuestionDropDown");
            questions.change(function () {
                var selectedValues = new Array();

                questions.each(function () {
                    var val = $(this).val();
                    if ($(this).val() != "") {
                        selectedValues.push(val);
                    }
                });

                questions.each(function () {
                    $(this).find("option").each(function () {
                        $(this).show();
                        $(this).removeAttr('disabled', 'disabled');
                    });

                    var val = $(this).val();
                    for (var index = 0; index < selectedValues.length; index++) {
                        if (selectedValues[index] != val) {
                            $(this).find("option[value=" + selectedValues[index] + "]").attr('disabled', 'disabled').hide();
                        }
                    }
                });
            });
        });
    </script>
</asp:Panel>
