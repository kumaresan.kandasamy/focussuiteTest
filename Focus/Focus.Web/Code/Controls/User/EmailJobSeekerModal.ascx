﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailJobSeekerModal.ascx.cs" Inherits="Focus.Web.Code.Controls.User.EmailJobSeekerModal" %>

<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="EmailJobSeekerModalPopup" runat="server" BehaviorID="EmailJobSeekerModalPanel"
                        TargetControlID="ModalDummyTarget"
                        PopupControlID="EmailJobSeekerModalPanel"
                        PopupDragHandleControlID="EmailJobSeekerModalPanel"
                        RepositionMode="RepositionOnWindowResizeAndScroll"
                        BackgroundCssClass="modalBackground"
	/>

<asp:Panel ID="EmailJobSeekerModalPanel" runat="server" CssClass="modal" style="display:none;">
	<h2><%= HtmlLocalise("Title.Text", "Email #CANDIDATETYPE#:LOWER(s)") %></h2>
  <table style="width:100%;" role="presentation">
    <tr>
      <td><asp:DropDownList runat="server" ID="EmailTemplateNameDropDown" AutoPostBack="True" OnSelectedIndexChanged="EmailTemplateNameDropDown_SelectedIndexChanged" Visible="False" /></td>
    </tr>
      <tr>
          <td><%= HtmlInFieldLabel(EmailSubjectTextBox.ClientID, "EmailSubject.InlineLabel", "type your subject here", 350)%>
        
              <asp:TextBox ID="EmailSubjectTextBox" runat="server" MaxLength="200" Width="350px" /></td>
      </tr>
      <tr>
          <td><%= HtmlInFieldLabel(EmailBodyTextBox.ClientID, "EmailBody.InlineLabel", "type your message here", 350)%>
              <asp:TextBox ID="EmailBodyTextBox" runat="server" TextMode="MultiLine" Rows="10" Width="350px" /></td>
      </tr>
    <tr>
      <td><asp:CheckBox ID="BCCMeCheckBox" runat="server" /></td>
    </tr>
    <tr>
      <td><asp:RequiredFieldValidator ID="EmailSubjectRequired" runat="server" ControlToValidate="EmailSubjectTextBox" SetFocusOnError="true"  
	                               CssClass="error" /><br/>
     <asp:RequiredFieldValidator ID="EmailBodyRequired" runat="server" ControlToValidate="EmailBodyTextBox" SetFocusOnError="true"  
																 CssClass="error"/></td>
    </tr>
    <tr>
      <td><asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" />
		<asp:Button ID="SendMessageButton" runat="server" CssClass="button3" OnClick="SendMessageButton_Clicked" /></td>
    </tr>
    <tr>
      <td></td>
    </tr>
    <tr>
      <td></td>
    </tr>
    <tr>
      <td></td>
    </tr>
  </table>
</asp:Panel>

<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" ClientIDMode="AutoID" />

