﻿<%@ Control Language="C#" Title="Confirmation Modal" AutoEventWireup="true" CodeBehind="ConfirmationModal.ascx.cs" Inherits="Focus.Web.Code.Controls.User.ConfirmationModal" %>

<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" 
												TargetControlID="ModalDummyTarget"
												PopupControlID="ModalPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="ModalPanel" runat="server" CssClass="modal" Style="display:none;">
	<table style="width:<%= Width %>;" role="presentation">
		<tr>
			<td class="modalHeading" style="vertical-align:top"><h5><%= Title %></h5></td>
		</tr>
		<tr>
			<td style="vertical-align:top;height:<%= Height %>;">
				<%= Details %>
				<asp:PlaceHolder runat="server" ID="ConfirmationTextRow" Visible="False">
					<br />
					<br />
					<asp:Literal ID="TextBoxHeader" runat="server"></asp:Literal>
					<asp:TextBox ID="ConfirmationTextBox" runat="server" TextMode="MultiLine" Rows="5" Width="100%"></asp:TextBox>
					<br/>
					<asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmationTextBox" ID="ConfirmationTextBoxValidator" Enabled="False" CssClass="error" />
				</asp:PlaceHolder>
			</td>
		</tr>
		<tr>
			<td style="text-align:left">
				<asp:Button ID="OkButton" runat="server" SkinID="Button1" CausesValidation="false" OnCommand="OkButton_Command" Text="OK"/>
				<asp:Button ID="CloseButton" runat="server" SkinID="Button1" CausesValidation="false" OnCommand="CloseButton_Command" Text="Close"/>
			</td>
		</tr>
	</table>
</asp:Panel>
<script type="text/javascript">
	Sys.Application.add_load(function () {
	  var popup = $find('<%=ModalPopup.ClientID %>');
	  if (popup != null) {
		  popup.add_shown(function () {
			  setButtonFocus('#<%=CloseButton.ClientID %>');
		  });
    }
	});
</script>
