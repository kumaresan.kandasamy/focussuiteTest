﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Common.Helpers;
using Focus.Common.Models;
using Focus.Core;
using Framework.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

#endregion

namespace Focus.Web.Code.Controls.User
{
  /// <summary>
  /// Allows the user to select a list of items, with the selection being done purely client side
  /// </summary>
  [ToolboxData("<{0}:UpdateableClientList runat=server></{0}:UpdateableClientList>")]
  public partial class UpdateableClientList : UserControlBase
	{
		#region Page properties

		private List<UpdateableClientListItem> _selectedItems;

			/// <summary>
    /// The maximum number of items that can be added.
    /// </summary>
		[Browsable(true), Category("Behavior"), Description("The maximum number of items that can be added.")]
    public int MaxSize
    {
      get { return GetViewStateValue<int>("UpdateableClientList:MaxSize", 1000); }
      set { SetViewStateValue("UpdateableClientList:MaxSize", value); }
    }

    /// <summary>
    /// An optional mask for the textbox field.
    /// </summary>
		[Browsable(true), Category("Layout"), Description("An optional mask for the textbox field.")]
    public string InputMask
    {
      get { return GetViewStateValue<string>("UpdateableClientList:InputMask"); }
      set { SetViewStateValue("UpdateableClientList:InputMask", value); }
    }

		/// <summary>
		/// An optional width for the control
		/// </summary>
		[Browsable(true), Category("Layout"), Description("The width")]
		public string Width
		{
			get { return GetViewStateValue<string>("UpdateableClientList:Width"); }
			set { SetViewStateValue("UpdateableClientList:Width", value); }
		}

		/// <summary>
		/// An optional prompt character for the textbox field.
		/// </summary>
		[Browsable(true), Category("Layout"), Description("An optional prompt character for the textbox field.")]
		public char MaskPromptCharacter
		{
			get { return GetViewStateValue<char>("UpdateableClientList:MaskPromptCharacter", '_'); }
			set { SetViewStateValue("UpdateableClientList:MaskPromptCharacter", value); }
		}

		/// <summary>
		/// Gets or sets the regex validator expression.
		/// </summary>
		/// <value>
		/// The regex validator expression.
		/// </value>
		[Browsable(true), Category("Validation"), Description("Gets or sets the regex validator expression.")]
		public string ValidatorRegex
		{
			get { return GetViewStateValue<string>("UpdateableClientList:ValidatorRegex"); }
			set { SetViewStateValue("UpdateableClientList:ValidatorRegex", value); }
		}

		/// <summary>
		/// Gets or sets the regex validator error message.
		/// </summary>
		/// <value>
		/// The regex validator error message.
		/// </value>
		[Browsable(true), Category("Validation"), Description("Gets or sets the validator regex error message.")]
	  public string ValidatorRegexErrorMessage
	  {
			get { return GetViewStateValue<string>("UpdateableClientList:ValidatorRegexErrorMessage", String.Empty); }
			set { SetViewStateValue("UpdateableClientList:ValidatorRegexErrorMessage", value); }
	  }

		/// <summary>
		/// Gets or sets the compare validator value.
		/// </summary>
		/// <value>
		/// The compare validator value.
		/// </value>
		[Browsable(true), Category("Validation"), Description("Gets or sets the compare validator value.")]
	  public string ValidatorCompareValue
	  {
			get { return GetViewStateValue<string>("UpdateableClientList:ValidatorCompareValue"); }
			set { SetViewStateValue("UpdateableClientList:ValidatorCompareValue", value); }
	  }

		/// <summary>
		/// Gets or sets the type of the compare validator.
		/// </summary>
		/// <value>
		/// The type of the compare validator.
		/// </value>
		[Browsable(true), Category("Validation"), Description("Gets or sets the type of the validator compare.")]
	  public ValidationDataType ValidatorCompareType
	  {
			get { return GetViewStateValue<ValidationDataType>("UpdateableClientList:ValidatorCompareType"); }
			set { SetViewStateValue("UpdateableClientList:ValidatorCompareType", value); }
	  }

		/// <summary>
		/// Gets or sets the compare validator operator.
		/// </summary>
		/// <value>
		/// The compare validator operator.
		/// </value>
		[Browsable(true), Category("Validation"), Description("Gets or sets the compare validator operator.")]
		public ValidationCompareOperator ValidatorCompareOperator
		{
			get { return GetViewStateValue<ValidationCompareOperator>("UpdateableClientList:ValidatorCompareOperator"); }
			set { SetViewStateValue("UpdateableClientList:ValidatorCompareOperator", value); }
		}

		/// <summary>
		/// Gets or sets the compare validator error message.
		/// </summary>
		/// <value>
		/// The compare validator error message.
		/// </value>
		[Browsable(true), Category("Validation"), Description("Gets or sets the compare validator error message.")]
		public string ValidatorCompareErrorMessage
		{
			get { return GetViewStateValue<string>("UpdateableClientList:ValidatorCompareErrorMessage", String.Empty); }
			set { SetViewStateValue("UpdateableClientList:ValidatorCompareErrorMessage", value); }
		}

		/// <summary>
		/// Gets or sets the validator mask.
		/// </summary>
		/// <value>
		/// The validator mask.
		/// </value>
		[Browsable(true), Category("Validation"), Description("Gets or sets the validator mask.")]
		public string ValidatorMask
	  {
			get { return GetViewStateValue<string>("UpdateableClientList:ValidatorMask"); }
			set { SetViewStateValue("UpdateableClientList:ValidatorMask", value); }
	  }

		/// <summary>
		/// Gets or sets the validation group.
		/// </summary>
		/// <value>
		/// The validation group.
		/// </value>
		[Browsable(true), Category("Validation"), Description("Gets or sets the validation group.")]
	  public string ValidationGroup
	  {
			get { return GetViewStateValue<string>("UpdateableClientList:ValidationGroup"); }
			set { SetViewStateValue("UpdateableClientList:ValidationGroup", value); }
	  }

    public bool AllowNonAdded { get; set; }
		
    /// <summary>
    /// Gets or sets the list of selected items.
    /// </summary>
		[Browsable(false), Category("Data"), Description("Contains the list of selected items.")]
    public List<UpdateableClientListItem> SelectedItems
    {
      get
      {
				if (_selectedItems.IsNullOrEmpty())
	      {
          if (MaxSize != 1 && (!AllowNonAdded || HiddenItemList.Value != "[]" || ItemTextBox.Text.IsNullOrEmpty()))
	        {
	          var jsonSerializerSettings = new JsonSerializerSettings
	          {
	            ContractResolver = new CamelCasePropertyNamesContractResolver()
	          };
	          _selectedItems = JsonConvert.DeserializeObject<List<UpdateableClientListItem>>(HiddenItemList.Value, jsonSerializerSettings);
	        }
          else if (ItemTextBox.Text.IsNotNullOrEmpty())
	        {
	          _selectedItems = new List<UpdateableClientListItem>
	          {
              new UpdateableClientListItem
              {
                Label = ItemTextBox.Text,
                Value = ItemTextBox.Text,
                IsDefault = true,
                IsNonAddedItem = (MaxSize != 1)
              }
	          };
	        }
	      }
	      return _selectedItems;
      }
      set
      {
        _selectedItems = value;
      }
    }

		/// <summary>
		/// Gets the list of selected items serialized as a JSON string.
		/// </summary>
	  public string SelectedItemsJson
	  {
		  get { 
				var jsonSerializerSettings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
				return JsonConvert.SerializeObject(SelectedItems, Formatting.None, jsonSerializerSettings);
		  }
	  }

	  /// <summary>
		/// Gets or sets the title to display above the selection box.
		/// </summary>
		[Browsable(true), Category("Layout"), Description("Gets or sets the title to display above the selection box.")]
	  public string SelectionBoxTitle
	  {
			get { return GetViewStateValue<string>("UpdateableClientList:SelectionBoxTitle"); }
			set { SetViewStateValue("UpdateableClientList:SelectionBoxTitle", value); }
	  }

		/// <summary>
		/// Gets or sets the input control type.
		/// </summary>
		[Browsable(true), Category("Behavior"), Description("Gets or sets the input control type.")]
	  public InputControlType InputControlType
	  {
		  get { return GetViewStateValue<InputControlType>("UpdateableClientList:InputControlType", InputControlType.TextBox); }
			set { SetViewStateValue("UpdateableClientList:InputControlType", value); }
	  }

		/// <summary>
		/// Gets or sets the initial items in the dropdown list (if in use).
		/// </summary>
		[Browsable(false), Category("Data"), Description("Gets or sets the initial items in the dropdown list (if in use).")]
	  public List<UpdateableClientListItem> SelectableItems
	  {
			get { return GetViewStateValue<List<UpdateableClientListItem>>("UpdateableClientList:SelectableItems"); }
			set { SetViewStateValue("UpdateableClientList:SelectableItems", value);}
	  }

		/// <summary>
		/// Gets the <see cref="SelectableItems"/> serialised as a JSON string.
		/// </summary>
		[Browsable(false), Category("Data"), Description("Gets SelectableItems serialised as a JSON string.")]
	  public string SelectableItemsJson
	  {
		  get
		  {
			  var jsonSerializerSettings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
				return JsonConvert.SerializeObject(SelectableItems, Formatting.None, jsonSerializerSettings);
		  }
	  }

		/// <summary>
		/// Gets or sets the dropdown list caption eg. Please select...
		/// </summary>
		[Browsable(true), Category("Layout"), Description("Gets or sets the dropdown list caption eg. Please select...")]
	  public string DropDownListCaption
	  {
		  get { return GetViewStateValue<string>("UpdateableClientList:DropDownListCaption", "Please select..."); }
			set { SetViewStateValue("UpdateableClientList:DropDownListCaption", value);}
	  }

		/// <summary>
		/// Gets or sets the service method that provides data for the auto complete textbox.
		/// </summary>
		[Browsable(true), Category("Behavior"), Description("Gets or sets the auto complete service method.")]
		public string AutoCompleteServiceMethod
		{
			get { return GetViewStateValue<string>("UpdateableClientList:AutoCompleteServiceMethod", ItemAutoCompleteTextBox.ServiceMethod); }
			set { SetViewStateValue("UpdateableClientList:AutoCompleteServiceMethod", value); }
		}

		/// <summary>
		/// Gets or sets whether the selection of a default item is enabled.
		/// </summary>
		[Browsable(true), Category("Behavior"), Description("Gets or sets whether the selection of a default item is enabled.")]
	  public bool EnableDefaultSelection
	  {
		  get { return GetViewStateValue<bool>("UpdateableClientList:EnableDefaultSelection"); }
			set { SetViewStateValue("UpdateableClientList:EnableDefaultSelection", value); }
	  }

		#endregion

		/// <summary>
		/// Clears the selected items.
		/// </summary>
	  public void Clear()
	  {
		  HiddenItemList.Value = string.Empty;
		  _selectedItems = null;
	  }

		/// <summary>
		/// Sets the selected items.
		/// </summary>
		/// <param name="items"></param>
	  public void SetSelectedItems(List<UpdateableClientListItem> items)
	  {
		  SelectedItems = items;
	  }

		/// <summary>
    /// Handles the loading of the page to initialise various controls
    /// </summary>
    /// <param name="sender">The page raising the event</param>
    /// <param name="e">Standard event arguments</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
			{
				Localise();
        ApplyTheme();

        if (MaxSize > 1)
					ItemButton.ValidationGroup = ItemTextBoxRegexValidator.ValidationGroup = ItemTextBoxCompareValidator.ValidationGroup = string.Concat(ID, "_ValidationGroup");

				if (SelectionBoxTitle.IsNotNullOrEmpty())
				{
					SelectionBoxTitleLabel.Text = SelectionBoxTitle;
					SelectionBoxTitleLabel.Visible = true;
				}
      }

			

			ScriptManager.RegisterClientScriptInclude(this, GetType(), "Knockout", ResolveUrl("~/Assets/Scripts/knockout-3.1.0.js"));
      ScriptManager.RegisterClientScriptInclude(this, GetType(), "UpdateableClientListInclude", UrlHelper.GetCacheBusterUrl("~/Assets/Scripts/Focus.UpdateableClientList.js"));
    }

		/// <summary>
		/// Build the JavaScript to initialise the Knockout viewmodel.
		/// </summary>
		/// <returns></returns>
	  private string BuildInitJS()
	  {
		  return string.Format(@"focusSuite.updateableClientList.BindUpdateableClientListViewModel({{
			mode: '{0}',
			validationGroup: '{1}',
			addItemText: '{2}',
			addAnotherItemText: '{3}',
			maxItems: {4},
			buttonClass: 'button3',
			buttonDisabledClass: 'button1Disabled',
			initialItems: {5},
			selectableItems: {6},
			dropDownListCaption: '{7}',
			enableDefaultSelection: {8},
			deleteItemToolTip: '{9}'
	}}, '{10}');", InputControlType.ToString(), ItemButton.ValidationGroup,
			  HtmlLocalise("Global.Add", "+"), HtmlLocalise("Global.AddAnother", "+"), MaxSize, SelectedItemsJson,
			  SelectableItemsJson, DropDownListCaption, EnableDefaultSelection.ToString().ToLowerInvariant(),
				HtmlLocalise("Tooltip.Remove", "Remove"), UpdateableClientListWrapper.ClientID);
	  }

    /// <summary>
    /// Handles the pre-render of the page
    /// </summary>
    /// <param name="sender">The page raising the event</param>
    /// <param name="e">Standard event arguments</param>
    protected void Page_PreRender(object sender, EventArgs e)
    {
			if (Width.IsNotNullOrEmpty())
			{
				UpdateableClientListWrapper.Style.Add("width", Width);	
			}

      if (MaxSize == 1)
        ItemButton.Visible = false;

			HiddenItemList.Value = SelectedItems.IsNotNullOrEmpty()
		    ? SelectedItemsJson
		    : string.Empty;

	    if (InputControlType.IsIn(InputControlType.TextBox, InputControlType.AutoCompleteTextBox))
	    {
		    ItemDropDownListLabel.Visible = ItemDropDownList.Visible = false;
	      if (InputControlType == InputControlType.TextBox)
	      {
	        Mask.Visible = InputMask.IsNotNullOrEmpty();
					ItemAutoCompleteTextBoxLabel.Visible = ItemAutoCompleteTextBox.Visible = false;
	      }
	      else
	      {
					ItemTextBoxLabel.Visible = ItemTextBox.Visible = false;
	        ItemTextBoxRegexValidator.ControlToValidate = ItemTextBoxCompareValidator.ControlToValidate = ItemAutoCompleteTextBox.ClientID;
	      }

	      if (ValidatorRegex.IsNullOrEmpty())
					ItemTextBoxRegexValidator.Visible = ItemTextBoxRegexValidator.Enabled = false;
				else
				{
					ItemTextBoxRegexValidator.ValidationExpression = ValidatorRegex;

					if (ValidationGroup.IsNotNullOrEmpty())
						ItemButton.ValidationGroup = ItemTextBoxRegexValidator.ValidationGroup = ValidationGroup;
				}

				if (ValidatorCompareValue.IsNullOrEmpty())
					ItemTextBoxCompareValidator.Visible = ItemTextBoxCompareValidator.Enabled = false;
				else
				{
					ItemTextBoxCompareValidator.ValueToCompare = ValidatorCompareValue;
					ItemTextBoxCompareValidator.Type = ValidatorCompareType;
					ItemTextBoxCompareValidator.Operator = ValidatorCompareOperator;
					ItemTextBoxCompareValidator.ErrorMessage = ValidatorCompareErrorMessage;

					if (ValidationGroup.IsNotNullOrEmpty())
						ItemButton.ValidationGroup = ItemTextBoxCompareValidator.ValidationGroup = ValidationGroup;
				}
				SetupAutoComplete();
	    }
	    else
	    {
        ItemTextBox.Visible = ItemAutoCompleteTextBoxLabel.Visible = ItemAutoCompleteTextBox.Visible = ItemTextBox.Visible = 
					ItemTextBoxCompareValidator.Visible = ItemTextBoxRegexValidator.Visible = false;
	    }

			ScriptManager.RegisterStartupScript(this, GetType(), this.ID + ":UpdateableClientListInit", BuildInitJS(), true);
      

			if (SelectedItems.IsNotNull() && SelectedItems.Count >= MaxSize)
			{
				ItemButton.Attributes["class"] = "button1disabled";
				ItemButton.Disabled = true;
			}

			ScriptManager.RegisterStartupScript(this, GetType(), this.ID + ":UpdateableClientListInit", BuildInitJS(), true);
    }

    /// <summary>
    /// Localises various bits of text on the page
    /// </summary>
    private void Localise()
    {
			ItemTextBoxRegexValidator.ErrorMessage = CodeLocalise("ItemTextBoxRegexValidator.ErrorMessage", ValidatorRegexErrorMessage);
    }

		private void SetupAutoComplete()
		{
			if (AutoCompleteServiceMethod.IsNotNullOrEmpty())
			{
				ItemAutoCompleteTextBox.ServiceUrl = UrlBuilder.AjaxService();
				ItemAutoCompleteTextBox.ServiceMethod = AutoCompleteServiceMethod;
				ItemAutoCompleteTextBox.InFieldLabelText = "";
				ItemAutoCompleteTextBox.ServiceMethodSearchKey = "prefixText";
				ItemAutoCompleteTextBox.ResultSize = 10;
			}
		}

    /// <summary>
    /// Applies various UI themes
    /// </summary>
    private void ApplyTheme()
    {
      DeleteIcon.ImageUrl = UrlBuilder.ButtonCloseIcon();
    }
	}

	/// <summary>
	/// An enumeration of input control types.
	/// </summary>
	public enum InputControlType
	{
		/// <summary>
		/// Control renders with a text box.
		/// </summary>
		TextBox,
		/// <summary>
		/// Control renders with a dropdown list.
		/// </summary>
		DropDownList,
    /// <summary>
    /// Control renders with an auto-complete text box
    /// </summary>
    AutoCompleteTextBox
	}

	/// <summary>
	/// A class representing a single data item in the <see cref="UpdateableClientList"/> control.
	/// </summary>
	[Serializable]
	public class UpdateableClientListItem
	{
		/// <summary>
		/// Gets or sets the text to display for the item in the UI.
		/// </summary>
		public string Label { get; set; }

		/// <summary>
		/// Gets or sets the value of the item.
		/// </summary>
		public string Value { get; set; }

		/// <summary>
		/// Gets or sets whether the item is the default.
		/// </summary>
		public bool IsDefault { get; set; }

    /// <summary>
    /// Whether the item can be included in the selected items even if not explicitly added
    /// </summary>
    /// <remarks>
    /// This only applies where MaxSize is not 1
    /// </remarks>
    public bool IsNonAddedItem { get; set; }
	}
}