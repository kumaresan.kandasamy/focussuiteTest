﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Common.ServiceClients;
using Focus.Core;
using Focus.Data.Core;
#endregion

namespace Focus.Web.Code.Controls.User
{
  public partial class JobOrders : UserControlBase
  {
    private int _jobsCount;
    private JobStatuses? _jobStatus;
    private string _problemWithDatesDetails, _activeDetails, _onHoldDetails, _closingOnDetails, _draftDetails;

    protected void Page_Load(object sender, EventArgs e)
    {
      _problemWithDatesDetails = CodeLocalise("JobRow.ProblemWithDates.Label", "Problem displaying the dates!");
      _closingOnDetails = CodeLocalise("JobRow.ClosingOn.Label", "{0:MMM dd, yyyy}");
      //_onHoldDetails = CodeLocalise("JobRow.OnHold.Label", "Posted {0:MMM dd, yyyy}, on hold since {1:MMM dd, yyyy}");
      // _closedDetails = CodeLocalise("JobRow.Closed.Label", "Posted {0:MMM dd, yyyy}, closed on {1:MMM dd, yyyy}");
      //_draftDetails = CodeLocalise("JobRow.Expires.Label", "Draft last saved on {0:MMM dd, yyyy}");

      if (!IsPostBack)
        LocaliseUI();

      ResultCount.Text = CodeLocalise("ResultCount.Text", "{0}", SearchResultListPager.TotalRowCount.ToString());
    }

    /// <summary>
    /// Handles the Selecting event of the SearchResultDataSource control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
    protected void SearchResultDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
      //e.InputParameters["criteria"] = _employeeCriteria;
    }

    /// <summary>
    /// Determines whether [is assist employers administrator].
    /// </summary>
    /// <returns>
    /// 	<c>true</c> if [is assist employers administrator]; otherwise, <c>false</c>.
    /// </returns>
    protected bool IsAssistEmployersAdministrator()
    {
      return App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator);
    }

    /// <summary>
    /// Handles the CheckedChanged event of the SelectorRadioButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SelectorCheckBox_CheckedChanged(object sender, EventArgs e)
    {
     
    }

    /// <summary>
    /// Handles the CheckedChanged event of the SelectorRadioButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SelectorCheckBoxs_CheckedChanged(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Filters the job list by action.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void FilterJobOrdersButton_Clicked(object sender, EventArgs e)
    {

    }
    

    /// <summary>
    /// Gets the jobs.
    /// </summary>
    /// <param name="orderBy">The order by.</param>
    /// <param name="startRowIndex">Start index of the row.</param>
    /// <param name="maximumRows">The maximum rows.</param>
    /// <returns></returns>
    public List<JobView> GetJobs(string orderBy, int startRowIndex, int maximumRows)
    {
      var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));
      var jobs = JobClient.Instance.GetJobs(JobStatuses.Active, pageIndex, maximumRows, orderBy);

      _jobsCount = jobs.TotalCount;
     return jobs;
    }

    /// <summary>
    /// Gets the jobs count.
    /// </summary>
    /// <returns></returns>
    public int GetJobsCount()
    {
      return _jobsCount;
    }

    /// <summary>
    /// Formats the posted and expiry dates.
    /// </summary>
    /// <param name="job">The job summary.</param>
    /// <returns></returns>
    protected string FormatDates(JobView job)
    {
      var formattedDates = _problemWithDatesDetails;

      switch (job.JobStatus)
      {
        case JobStatuses.Active:
          if (job.ClosingOn.HasValue)
            formattedDates = string.Format(_closingOnDetails, job.ClosingOn.Value);
          break;

        //case JobStatuses.OnHold:
        //  if (job.PostedOn.HasValue && job.HeldOn.HasValue)
        //    formattedDates = string.Format(_onHoldDetails, job.PostedOn.Value, job.HeldOn.Value);
        //  break;

        //case JobStatuses.Closed:
        //  if (job.PostedOn.HasValue && job.ClosedOn.HasValue)
        //    formattedDates = string.Format(_closedDetails, job.PostedOn.Value, job.ClosedOn.Value);
        //  break;

        //case JobStatuses.Draft:
        //  formattedDates = string.Format(_draftDetails, job.UpdatedOn);
        //  break;
      }

      return formattedDates;
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {

      FilterJobOrdersButton.Text = CodeLocalise("FilterJobOrdersButton.Text", "Go");
     
    }
  }
}