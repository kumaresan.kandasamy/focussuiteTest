﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Services.Core;
using Focus.Web.Code;
using Focus.Web.Core.Models;
using Constants = Focus.Core.Constants;
using DistanceUnits = Focus.Core.Models.Career.DistanceUnits;

using Framework.Core;
using Framework.DataAccess;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class SearchJobPostings : UserControlBase
	{
		public event EventHandler SearchJobPostingsClick;

		#region Properties

		public long JobSeekerId
		{
			get { return GetViewStateValue<long>("JobSeekerProfile:JobSeekerId"); }
			set { SetViewStateValue("JobSeekerProfile:JobSeekerId", value); }
		}

		protected JobSeekerProfileModel JobSeekerProfileModel
		{
			get { return GetViewStateValue<JobSeekerProfileModel>("JobSeekerProfile:JobSeekerProfileModel"); }
			set { SetViewStateValue("JobSeekerProfile:JobSeekerProfileModel", value); }
		}

		protected ResumeModel Resume
		{
			get { return GetViewStateValue<ResumeModel>("JobSeeker:Resume"); }
			set { SetViewStateValue("JobSeeker:Resume", value); }
		}

		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			var searchCriteria = App.GetSessionValue<Focus.Core.Models.Career.SearchCriteria>("Career:SearchCriteria");

			if (JobSeekerId > 0)
			{
				GetJobSeekerProfileData();
				if (JobSeekerProfileModel.ResumeId != null)
				{
					if (!IsPostBack)
						Resume = ServiceClientLocator.ResumeClient(App).GetResume(JobSeekerProfileModel.ResumeId);

					if (searchCriteria.IsNull())
					{
						if (Resume.IsNotNull())
							Utilities.SaveCriteriaAgainstResume(Resume, MatchingType.Resume);
					}
				}
			}

			if (IsPostBack) return;
			LocaliseUI();
			ApplyTheme();
			ApplyBranding();
			HideControl();
			BindDropdowns();
		  BindSearchExpression();
		}

		/// <summary>
		/// Builds the search expression.
		/// </summary>
		/// <returns></returns>
		public Focus.Core.Models.Career.SearchCriteria BuildSearchExpression()
		{
			var criteria = new Focus.Core.Models.Career.SearchCriteria();

			#region Job Matching Criteria

			criteria.RequiredResultCriteria = new RequiredResultCriteria
			{
				DocumentsToSearch = DocumentType.Posting,
				MinimumStarMatch = rbStarMatch.Checked ? ddlStarMatch.SelectedValueToEnum(StarMatching.ZeroStar) : StarMatching.None,
				MaximumDocumentCount = App.Settings.MaximumNoDocumentToReturnInSearch
			};

			#endregion

			#region Job Location Criteria

			var joblocation = new JobLocationCriteria();

			if (cbJobsInMyState.Checked)
			{
				joblocation.OnlyShowJobInMyState = true;
				joblocation.Area = new AreaCriteria { StateId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault() };
			}

			if (rbArea.Checked && txtZipCode.TextTrimmed() != "")
			{
				var lookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses).FirstOrDefault(x => x.Id == ddlRadius.SelectedValueToLong());
				var radiusExternalId = (lookup.IsNotNull()) ? lookup.ExternalId : "0";
				var radiusValue = radiusExternalId.ToLong();

				var radius = new RadiusCriteria
				{
					Distance = (radiusValue.HasValue) ? radiusValue.Value : 0,
					DistanceUnits = DistanceUnits.Miles,
					PostalCode = txtZipCode.TextTrimmed()
				};
				joblocation.Radius = radius;

				if (cbJobsInMyState.Checked)
				{
					var state = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault();

					if (!App.Settings.NationWideInstance)
					{
						if (App.User.IsAuthenticated && App.UserData.HasProfile && App.UserData.Profile.PostalAddress.IsNotNull() && App.UserData.Profile.PostalAddress.StateId.HasValue)
							state = App.UserData.Profile.PostalAddress.StateId.Value;
					}
					var area = new AreaCriteria
					{
						StateId = state
					};
					joblocation.Area = area;
				}
			}
			else if (rbMSAState.Checked && (ddlState.SelectedValue != "" || ddlMSA.SelectedValue != ""))
			{
				var stateId = new long();

				if (App.Settings.NationWideInstance)
				{
					stateId = ddlState.SelectedValueToLong();
				}
				else
				{
					if (!cbJobsInMyState.Checked)
						stateId = ddlState.SelectedValueToLong();
				}

				var area = new AreaCriteria
				{
					StateId = stateId,
					MsaId = ddlMSA.SelectedValueToLong()
				};
				joblocation.Area = area;

				var lookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).FirstOrDefault(x => x.Id == ddlState.SelectedValueToLong());

				if (lookup.IsNotNull())
					joblocation.SelectedStateInCriteria = lookup.ExternalId;
			}
			joblocation.OnlyShowJobInMyState = cbJobsInMyState.Checked;

			//if (HomeBasedJobPostingsRadioButton.Checked)
			//	joblocation.HomeBasedJobPostings = true;

			if (!rbArea.Checked && !rbMSAState.Checked)
				BuildDefaultSearchLocationCriteria(joblocation, App);

			criteria.JobLocationCriteria = joblocation;

			#endregion

			#region Keyword Criteria

			if (SearchTermTextBox.TextTrimmed() != "")
			{
				var searchLocation = ddlSearchLocation.SelectedValueToEnum(PostingKeywordScopes.Anywhere);
				criteria.KeywordCriteria = new KeywordCriteria(SearchTermTextBox.TextTrimmed()) { SearchLocation = searchLocation };
			}

			#endregion

			#region Posting Age Criteria

			var postingAgeLookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.PostingAges).FirstOrDefault(x => x.Id == ddlPostingAge.SelectedValueToLong());
			var selectedPostingAge = (postingAgeLookup.IsNotNull()) ? postingAgeLookup.ExternalId : String.Empty;

			if (selectedPostingAge.IsNotNullOrEmpty())
			{
				criteria.PostingAgeCriteria = new PostingAgeCriteria
				{
					PostingAgeInDays = selectedPostingAge.AsInt()
				};
			}

			#endregion

			#region Education Level Criteria

			var selectedEducation = (from ListItem checkbox in cblEducationRequired.Items where checkbox.Selected select checkbox.Value.ToLong() into checkboxValue where checkboxValue.HasValue select checkboxValue.Value).ToList();

			if (selectedEducation.IsNotNullOrEmpty())
			{
				criteria.EducationLevelCriteria = new EducationLevelCriteria
				{
					IncludeJobsWithoutEducationRequirement = cbWithoutEducationInfo.Checked,
					RequiredEducationIds = selectedEducation
				};
			}

			#endregion

			#region Salary Level Criteria

			if (minimumSalaryTextBox.Text.AsFloat() > 0)
			{
				criteria.SalaryCriteria = new SalaryCriteria
				{
					IncludeJobsWithoutSalaryInformation = cbWithoutSalaryInfo.Checked,
					MinimumSalary = minimumSalaryTextBox.Text.AsFloat()
				};
			}

			#endregion

			#region Intership Criteria

      // Only set internship criteria if a selectlist item has been selected 
            if (ddlInternshipRequired.SelectedValueToEnum(FilterTypes.NoFilter) != FilterTypes.NoFilter)
		  {
		    var selectedIntership = ddlInternshipRequired.SelectedValueToEnum(FilterTypes.NoFilter);
		    if (selectedIntership.IsNotNull())
		    {
		      criteria.InternshipCriteria = new InternshipCriteria
		      {
		        Internship = selectedIntership
		      };
		    }
		  }
		  else
		  {
		    criteria.InternshipCriteria = null;
		  }


		  #endregion

			#region Home Based Jobs Criteria

          if (ddlHomeBasedJobsRequired.SelectedValueToEnum(FilterTypes.NoFilter) != FilterTypes.NoFilter)
			{
				var selectedHomeBasedJobs = ddlHomeBasedJobsRequired.SelectedValueToEnum(FilterTypes.NoFilter);
				if (selectedHomeBasedJobs.IsNotNull())
				{
					criteria.HomeBasedJobsCriteria = new HomeBasedJobsCriteria
					{
						HomeBasedJobs = selectedHomeBasedJobs
					};
				}
			}
			else
			{
				criteria.HomeBasedJobsCriteria = null;
			}


			#endregion

			#region Commission Based Jobs Criteria

          if (ddlCommissionBasedJobsRequired.SelectedValueToEnum(FilterTypes.NoFilter) != FilterTypes.NoFilter)
			{
				var selectedCommissionBasedJobs = ddlCommissionBasedJobsRequired.SelectedValueToEnum(FilterTypes.NoFilter);
				if (selectedCommissionBasedJobs.IsNotNull())
				{
					criteria.CommissionBasedJobsCriteria = new CommissionBasedJobsCriteria
					{
						CommissionBasedJobs = selectedCommissionBasedJobs
					};
				}
			}
			else
			{
				criteria.CommissionBasedJobsCriteria = null;
			}


			#endregion

			#region Salary And Commission Based Jobs Criteria

          if (ddlSalaryAndCommissionBasedJobsRequired.SelectedValueToEnum(FilterTypes.NoFilter) != FilterTypes.NoFilter)
			{
				var selectedSalaryAndCommissionBasedJobs = ddlSalaryAndCommissionBasedJobsRequired.SelectedValueToEnum(FilterTypes.NoFilter);
				if (selectedSalaryAndCommissionBasedJobs.IsNotNull())
				{
					criteria.SalaryAndCommissionBasedJobsCriteria = new SalaryAndCommissionBasedJobsCriteria
					{
						SalaryAndCommissionBasedJobs = selectedSalaryAndCommissionBasedJobs
					};
				}
			}
			else
			{
				criteria.SalaryAndCommissionBasedJobsCriteria = null;
			}


			#endregion

			#region Occupation and Industry Criteria

			var industryId = ddlIndustry.SelectedValueToLong();

			var industryDetailId = ddlIndustryDetail.SelectedValueToLong();
			var industryDetailIds = (industryDetailId.IsNotNull() && industryDetailId > 0) ? new List<long> { industryDetailId } : null;

			if (industryId.IsNotNull() && industryId > 0)
			{
				criteria.IndustryCriteria = new IndustryCriteria
				{
					IndustryId = industryId,
					IndustryDetailIds = industryDetailIds
				};
			}

			#endregion

			#region Emerging Sector Criteria

			var selectedSectors = new List<long>();
			string sectors = "";

			foreach (ListItem checkbox in cblEmergingSectors.Items)
			{
				if (checkbox.Selected)
				{
					var sectorId = checkbox.Value.ToLong();
					if (sectorId.HasValue)
					{
						selectedSectors.Add(sectorId.Value);
						sectors += checkbox.Text + ", ";
					}
				}
			}

			if (selectedSectors.Count > 0)
			{
				sectors = sectors.Trim();
				if (sectors.EndsWith(","))
					sectors = sectors.Substring(0, sectors.Length - 1);
				criteria.JobSectorCriteria = new JobSectorCriteria
				{
					RequiredJobSectorIds = selectedSectors,
					RequiredJobSectors = sectors
				};
			}

			#endregion

			#region Physical abilities Criteria

			var selectedPhysicalabilities = new List<long>();
			string abilitiesList = "";

			foreach (ListItem checkbox in cblDisabilityCategories.Items)
			{
				if (checkbox.Selected)
				{
					var abilityId = checkbox.Value.ToLong();
					if (abilityId.HasValue)
					{
						selectedPhysicalabilities.Add(abilityId.Value);
						abilitiesList += checkbox.Text + ", ";
					}
				}
			}

			if (selectedPhysicalabilities.Count > 0)
			{
				abilitiesList = abilitiesList.Trim();
				if (abilitiesList.EndsWith(","))
					abilitiesList = abilitiesList.Substring(0, abilitiesList.Length - 1);

				criteria.PhysicalAbilityCriteria = new PhysicalAbilityCriteria
				{
					PhysicalAbilityIds = selectedPhysicalabilities,
					PhysicalAbilities = abilitiesList
				};
			}

			#endregion

			#region Exclude Past Job Criteria

			if (rbStarMatch.Checked)
			{
				if (JobSeekerProfileModel.IsNotNull() && JobSeekerProfileModel.ResumeId.IsNotNull())
				{
					criteria.ReferenceDocumentCriteria = new ReferenceDocumentCriteria
					{
						DocumentId = JobSeekerProfileModel.ResumeId.ToString(),
						DocumentType = DocumentType.Resume
					};
				}

				var selectedJobIds = (from ListItem checkbox in cblExcludeJobs.Items
															where checkbox.Selected
															select new Focus.Core.Models.Career.Job { JobId = checkbox.Value.AsNullableGuid(), Description = checkbox.Text }).ToList();

				if (selectedJobIds.Count > 0)
				{
					criteria.ReferenceDocumentCriteria.ExcludedJobs = selectedJobIds;
				}
			}

			#endregion

			#region ROnet Criteria

			var careerAreaId = ddlJobFamily.SelectedValueToLong();

			if (careerAreaId.IsNotNull() && careerAreaId > 0)
			{
				var roNet = ddlOccupation.SelectedValue;

				var roNets = new List<string>();

				if (roNet.IsNullOrEmpty())
					roNets = null;
				else if (roNet.Substring(0, 1) == "-" + careerAreaId)
					roNets = ServiceClientLocator.ExplorerClient(App).GetCareerAreaJobs(careerAreaId).Select(x => x.ROnet).ToList();
				else if (roNet.Substring(0, 1) != "-")
					roNets.Add(roNet);

				criteria.ROnetCriteria = new ROnetCriteria
				{
					CareerAreaId = careerAreaId,
					ROnets = roNets
				};
			}

			#endregion

			#region Reference Document Criteria

			if (rbStarMatch.Checked && JobSeekerProfileModel.IsNotNull() && criteria.ReferenceDocumentCriteria.IsNull())
			{
				criteria.ReferenceDocumentCriteria = new ReferenceDocumentCriteria
				{
					DocumentId = JobSeekerProfileModel.ResumeId.ToString(),
					DocumentType = DocumentType.Resume
				};
			}

			#endregion

			#region Work Availability

			criteria.WorkDaysCriteria = SearchCriteriaWorkDays.BuildSearchExpression();

			#endregion

      #region Language Proficienies

		  criteria.LanguageCriteria = searchCriteriaLanguage.Unbind();

      #endregion

      return criteria;
		}

		protected void btnSearchJobPostings_Click(object sender, EventArgs e)
		{
			if (SearchJobPostingsClick != null)
				SearchJobPostingsClick(this, EventArgs.Empty);
		}

		/// <summary>
		/// Gets the job seeker profile data.
		/// </summary>
		private void GetJobSeekerProfileData()
		{
			JobSeekerProfileModel = ServiceClientLocator.CandidateClient(App).GetJobSeekerProfile(JobSeekerId);
		}

		/// <summary>
		/// Applies the theme.
		/// </summary>
		private void ApplyTheme()
		{
			//if (App.Settings.Theme == FocusThemes.Education || !App.Settings.TalentModulePresent)
			//	HomeBasedJobPostingsPlaceHolder.Visible = false;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			if (JobSeekerId > 0)
			{
				SearchJobPostingsLabel.Text = CodeLocalise("SearchJobPostingsFor.Label",
																									 String.Format("Search job postings for {0} {1}",
																																 JobSeekerProfileModel.JobSeekerProfileView.FirstName,
																																 JobSeekerProfileModel.JobSeekerProfileView.LastName));
			}
			else
			{
				SearchJobPostingsLabel.Text = CodeLocalise("SearchJobPostings.Label", "Search job postings");
			}

			RadiusValidator.ErrorMessage = CodeLocalise("RadiusLocation.RequiredErrorMessage", "Both radius and 5 digit zip code is required");
			cbJobsInMyState.Text = CodeLocalise("OnlyShow.Label", "Only show in-state jobs");
			rbMSAState.Text = CodeLocalise("SearchJobPostings.rbMSAState.Text", "Search this state/city");
			//HomeBasedJobPostingsRadioButton.Text = CodeLocalise("HomeBasedJobPostingsCheckBox.Label", "Search home-based job postings");
		}

		/// <summary>
		/// Applies the branding.
		/// </summary>
		private void ApplyBranding()
		{
			JobMatchingHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			JobMatchingPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			JobMatchingPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			PostingDateHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			PostingDatePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			PostingDatePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			EducationLevelHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			EducationLevelPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			EducationLevelPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			SalaryLevelHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			SalaryLevelPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			SalaryLevelPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			InternshipsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			InternshipsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			InternshipsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			JobTypesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			JobTypesPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			JobTypesPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

      LanguagesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      LanguagesPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
      LanguagesPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			OccupationsAndIndustryHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			OccupationsAndIndustryPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			OccupationsAndIndustryPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			EmergingSectorsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			EmergingSectorsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			EmergingSectorsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			PhysicalAbilitiesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			PhysicalAbilitiesPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			PhysicalAbilitiesPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			ExcludeJobsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ExcludeJobsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ExcludeJobsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}

		/// <summary>
		/// Binds the dropdowns.
		/// </summary>
		private void BindDropdowns()
		{
			try
			{
				BindStarMatchDropDown(StarMatching.ThreeStar);
				ddlRadius.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses), null, CodeLocalise("Radius.TopDefault.NoEdit", "- select radius -"));
				BindPosingKeywordScopeDropDown(PostingKeywordScopes.Anywhere);

				var postingAgeLookups = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.PostingAges);
				var defaultPostingAgeLookup = postingAgeLookups.FirstOrDefault(x => x.ExternalId == Defaults.ConfigurationItemDefaults.PostingAgeInDaysForAssist.ToString(CultureInfo.InvariantCulture));
				ddlPostingAge.BindLookup(postingAgeLookups, (defaultPostingAgeLookup.IsNotNull()) ? defaultPostingAgeLookup.Id.ToString(CultureInfo.InvariantCulture) : "30", null);
				cblEducationRequired.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.RequiredEducationLevels));
				BindInternshipFilterDropDown();
				BindHomeBasedJobsFilterDropDown();
				BindCommissionBasedJobsFilterDropDown();
				BindSalaryAndCommissionBasedJobsFilterDropDown();

				var lookupDictionary = ServiceClientLocator.CoreClient(App).GetLocalisationDictionary();
				var emergingSectors = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.EmergingSectors);

				//var emergingSectorToolTips = emergingSectors.Select(es =>
				//  lookupDictionary.ContainsKey(string.Concat(es.Key, ".ToolTip")) ? lookupDictionary[string.Concat(es.Key, ".ToolTip")] : "").ToList();

				// TODO: hmmm , test this and possibly simplify
				var emergingSectorToolTips = (from emergingSector in emergingSectors
																			let localisationDictionaryEntry = lookupDictionary.GetValue(string.Concat(emergingSector.Key, ".ToolTip"))
																			where localisationDictionaryEntry != null
																			where localisationDictionaryEntry.IsNotNull()
																			select localisationDictionaryEntry.LocalisedValue).ToList();

				cblEmergingSectors.BindLookup(emergingSectors, emergingSectorToolTips);

				cblDisabilityCategories.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.PhysicalAbilities));

				if (Resume.IsNotNull() && Resume.ResumeMetaInfo.ResumeId.IsNotNull())
				{
					var distinctJobs = ServiceClientLocator.ResumeClient(App).GetDistinctJobs((long)Resume.ResumeMetaInfo.ResumeId);

					if (distinctJobs.IsNotNullOrEmpty())
					{
						cblExcludeJobs.DataSource = distinctJobs;
						cblExcludeJobs.DataValueField = "Key";
						cblExcludeJobs.DataTextField = "Value";
						cblExcludeJobs.DataBind();

						ExcludeJobsHeaderPanel.Visible = ExcludeJobsContentPanel.Visible = true;
					}
					else
						ExcludeJobsHeaderPanel.Visible = ExcludeJobsContentPanel.Visible = false;
				}

				BindJobFamilyDropDown();

				ddlOccupation.Items.AddLocalisedTopDefault("Occupations.SelectOccupation.TopDefault", "- select occupation -");
				ddlIndustry.BindDictionary(ServiceClientLocator.CoreClient(App).GetIndustryClassificationLookup(0), null, CodeLocalise("Industry.TopDefault", "- select industry -"), "0");
				ddlIndustryDetail.Items.AddLocalisedTopDefault("OccupationDetails.SelectOccupationDetails.TopDefault", "- select occupation details -");

				var nearbyStates = (from s in ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States) where App.Settings.NearbyStateKeys.Contains(s.Key) select s).ToList();
				ddlState.BindLookup(nearbyStates, null, CodeLocalise("State.TopDefault.NoEdit", "- select state -"), "0");
				ddlMSA.Items.AddLocalisedTopDefault("City.SelectCity.TopDefault", "- select city -");
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

		/// <summary>
		/// Binds the job family drop down.
		/// </summary>
		private void BindJobFamilyDropDown()
		{
			ddlJobFamily.DataSource = ServiceClientLocator.ExplorerClient(App).GetCareerAreas();
			ddlJobFamily.DataTextField = "Name";
			ddlJobFamily.DataValueField = "Id";
			ddlJobFamily.DataBind();
			ddlJobFamily.Items.AddLocalisedTopDefault("JobFamily.TopDefault", "- select job family -", "0");
		}

		/// <summary>
		/// Binds the star match drop down.
		/// </summary>
		/// <param name="selectedValue">The selected value.</param>
		private void BindStarMatchDropDown(StarMatching selectedValue)
		{
			ddlStarMatch.Items.Clear();

			ddlStarMatch.Items.AddEnum(StarMatching.OneStar, "1-star match");
			ddlStarMatch.Items.AddEnum(StarMatching.TwoStar, "2-star match");
			ddlStarMatch.Items.AddEnum(StarMatching.ThreeStar, "3-star match");
			ddlStarMatch.Items.AddEnum(StarMatching.FourStar, "4-star match");
			ddlStarMatch.Items.AddEnum(StarMatching.FiveStar, "5-star match");

			if (selectedValue.IsNotNull())
				ddlStarMatch.SelectValue(selectedValue.ToString());
		}

		/// <summary>
		/// Binds the internship filter drop down.
		/// </summary>
		private void BindInternshipFilterDropDown()
		{
			ddlInternshipRequired.Items.Clear();

			ddlInternshipRequired.Items.AddEnum(FilterTypes.Exclude, "Exclude internships");
			ddlInternshipRequired.Items.AddEnum(FilterTypes.NoFilter, "Search for all jobs (including internships)");
			ddlInternshipRequired.Items.AddEnum(FilterTypes.ShowOnly, "Search only for internships");

			ddlInternshipRequired.SelectValue(FilterTypes.NoFilter.ToString());
		}

		/// <summary>
		/// Binds the home based jobs filter drop down.
		/// </summary>
		private void BindHomeBasedJobsFilterDropDown()
		{
			ddlHomeBasedJobsRequired.Items.Clear();

			ddlHomeBasedJobsRequired.Items.AddEnum(FilterTypes.Exclude, "Exclude Home-based jobs");
			ddlHomeBasedJobsRequired.Items.AddEnum(FilterTypes.NoFilter, "Search for all jobs (including home-based jobs)");
			ddlHomeBasedJobsRequired.Items.AddEnum(FilterTypes.ShowOnly, "Search only for Home-based jobs");
			ddlHomeBasedJobsRequired.SelectValue(FilterTypes.NoFilter.ToString());
		}

		/// <summary>
		/// Binds the commission based jobs filter drop down.
		/// </summary>
		private void BindCommissionBasedJobsFilterDropDown()
		{
			ddlCommissionBasedJobsRequired.Items.Clear();

			ddlCommissionBasedJobsRequired.Items.AddEnum(FilterTypes.Exclude, "Exclude Commission-only jobs");
			ddlCommissionBasedJobsRequired.Items.AddEnum(FilterTypes.NoFilter, "Search for all jobs (including Commission-only jobs)");
			ddlCommissionBasedJobsRequired.Items.AddEnum(FilterTypes.ShowOnly, "Search only for Commission-only jobs");
			ddlCommissionBasedJobsRequired.SelectValue(FilterTypes.NoFilter.ToString());
		}

		/// <summary>
		/// Binds the salary and commission based jobs filter drop down.
		/// </summary>
		private void BindSalaryAndCommissionBasedJobsFilterDropDown()
		{
			ddlSalaryAndCommissionBasedJobsRequired.Items.Clear();

			ddlSalaryAndCommissionBasedJobsRequired.Items.AddEnum(FilterTypes.Exclude, "Exclude Commission + salary jobs");
			ddlSalaryAndCommissionBasedJobsRequired.Items.AddEnum(FilterTypes.NoFilter, "Search for all jobs (including Commission + salary jobs)");
			ddlSalaryAndCommissionBasedJobsRequired.Items.AddEnum(FilterTypes.ShowOnly, "Search only for Commission + salary jobs");
			ddlSalaryAndCommissionBasedJobsRequired.SelectValue(FilterTypes.NoFilter.ToString());
		}

		/// <summary>
		/// Binds the posing keyword scope drop down.
		/// </summary>
		/// <param name="selectedValue">The selected value.</param>
		private void BindPosingKeywordScopeDropDown(PostingKeywordScopes selectedValue)
		{
			ddlSearchLocation.Items.Clear();

			ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.Anywhere, "Anywhere");
			ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.JobDescription, "Job Description");
			ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.Employer, "#BUSINESS#");
			ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.JobTitle, "Job Title");

			if (selectedValue.IsNotNull())
				ddlSearchLocation.SelectValue(selectedValue.ToString());
		}

		/// <summary>
		/// Hides the control.
		/// </summary>
		private void HideControl()
		{
			ddlStarMatch.Enabled = true;
			ExcludeJobsHeaderPanel.Visible = ExcludeJobsContentPanel.Visible = false;

			if (Resume.IsNotNull())
				ExcludeJobsHeaderPanel.Visible = ExcludeJobsContentPanel.Visible = (bool)Resume.ResumeMetaInfo.IsDefault;
			else
				ddlStarMatch.Enabled = rbStarMatch.Enabled = false;

			SearchCriteriaWorkDays.Visible = App.Settings.WorkAvailabilitySearch;
		}

		/// <summary>
		/// Binds the search expression.
		/// </summary>
		private void BindSearchExpression()
		{
			var searchCriteria = App.GetSessionValue<Focus.Core.Models.Career.SearchCriteria>("Career:SearchCriteria");
			if (searchCriteria.IsNotNull())
			{
				var searchExpression = searchCriteria;
				//if (searchExpression.JobLocationCriteria.IsNotNull())
				//  BindPreferedLocation();

				#region Job Matching Criteria

				if (searchExpression.RequiredResultCriteria.IsNotNull())
				{
					var minimumscore = searchExpression.RequiredResultCriteria.MinimumStarMatch;
					if (minimumscore != StarMatching.None && minimumscore != StarMatching.ZeroStar)
					{
						rbStarMatch.Checked = true;
						rbWithoutMatch.Checked = false;

						ddlStarMatch.SelectValue(minimumscore.ToString());

						JobMatchingHeaderPanel.CssClass += " collapsableAccordionTitle on";
						JobMatchingContentPanel.CssClass += " collapsableAccordionContent open";
            JobMatchingPanelExtender.Collapsed = false;
					}
					else
					{
						rbWithoutMatch.Checked = true;
						rbStarMatch.Checked = false;
					}				  
				}
				else
				{
					SetSearchDefaults(jobMatching: true);
				}

				#endregion

				#region Job Location Criteria

				if (searchExpression.JobLocationCriteria.IsNotNull())
				{
					var locationCriteria = searchExpression.JobLocationCriteria;
					cbJobsInMyState.Checked = locationCriteria.OnlyShowJobInMyState;

					var radius = locationCriteria.Radius;
					if (radius.IsNotNull())
					{
						rbArea.Checked = true;

						var lookup =
							ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses)
												.FirstOrDefault(x => x.ExternalId == radius.Distance.ToString());

						if (lookup.IsNotNull())
							ddlRadius.SelectValue(lookup.Id.ToString());

						txtZipCode.Text = radius.PostalCode;
						if (locationCriteria.OnlyShowJobInMyState)
							rbMSAState.Checked = false;
					}

					var area = locationCriteria.Area;
					if (radius.IsNull() && area.IsNotNull())
					{
						rbMSAState.Checked = true;

						if (area.MsaId.HasValue)
						{
							ddlMSA.SelectValue(area.MsaId.ToString());
							ccdMSA.SelectedValue = area.MsaId.ToString();
						}

						if (area.StateId.HasValue)
						{
							ddlState.SelectValue((area.StateId.ToString()));
						}
					}

					//if (radius.IsNull() && area.IsNull() && locationCriteria.HomeBasedJobPostings.IsNotNull())
					//	HomeBasedJobPostingsRadioButton.Checked = locationCriteria.HomeBasedJobPostings;

					if (area.IsNull() && radius.IsNull())
						rbArea.Checked = true;
				}
				else
				{
					SetSearchDefaults(searchRadius: true);
					BindPreferedLocation();
				}

				#endregion

				#region Keyword Criteria

				if (searchExpression.KeywordCriteria.IsNotNull())
				{
					ddlSearchLocation.SelectValue((searchExpression.KeywordCriteria.SearchLocation.IsNotNull())
																					? searchExpression.KeywordCriteria.SearchLocation.ToString()
																					: "");
					SearchTermTextBox.Text = searchExpression.KeywordCriteria.KeywordText;
				}

				#endregion

				#region Posting Age Criteria

				if (searchExpression.PostingAgeCriteria.IsNotNull() &&
						searchExpression.PostingAgeCriteria.PostingAgeInDays.IsNotNull())
				{
					var lookup =
						ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.PostingAges)
											.FirstOrDefault(
												x => x.ExternalId == searchExpression.PostingAgeCriteria.PostingAgeInDays.ToString());

					if (lookup.IsNotNull())
						ddlPostingAge.SelectValue(lookup.Id.ToString());

					if (searchExpression.PostingAgeCriteria.PostingAgeInDays != App.Settings.JobSearchPostingDate)
					{
						PostingDateHeaderPanel.CssClass += " collapsableAccordionTitle on";
						PostingDateContentPanel.CssClass += " collapsableAccordionContent open";
					  PostingDatePanelExtender.Collapsed = false;
					}
				}

				#endregion

				#region Education Level Criteria

				if (searchExpression.EducationLevelCriteria.IsNotNull())
				{
					cbWithoutEducationInfo.Checked =
						searchExpression.EducationLevelCriteria.IncludeJobsWithoutEducationRequirement;

					foreach (
						var checkbox in
							searchExpression.EducationLevelCriteria.RequiredEducationIds.SelectMany(
								requiredEducationId =>
								cblEducationRequired.Items.Cast<ListItem>()
																		.Where(checkbox => checkbox.Value == requiredEducationId.ToString())))
					{
						checkbox.Selected = true;
					}

					EducationLevelHeaderPanel.CssClass += " collapsableAccordionTitle on";
					EducationLevelContentPanel.CssClass += " collapsableAccordionContent open";
				  EducationLevelPanelExtender.Collapsed = false;
				}

				#endregion

				#region Salary Level Criteria

				if (searchExpression.SalaryCriteria.IsNotNull())
				{
					cbWithoutSalaryInfo.Checked = searchExpression.SalaryCriteria.IncludeJobsWithoutSalaryInformation;
					minimumSalaryTextBox.Text = searchExpression.SalaryCriteria.MinimumSalary.ToString();
					SalaryLevelHeaderPanel.CssClass += " collapsableAccordionTitle on";
					SalaryLevelContentPanel.CssClass += " collapsableAccordionContent open";
				  SalaryLevelPanelExtender.Collapsed = false;
				}

				#endregion

				#region Intership Criteria

				if (searchExpression.InternshipCriteria.IsNotNull())
				{
					var intership = searchExpression.InternshipCriteria.Internship;

					if (intership.IsNotNull())
						ddlInternshipRequired.SelectValue(intership.ToString());

					InternshipsHeaderPanel.CssClass += " collapsableAccordionTitle on";
					InternshipsContentPanel.CssClass += " collapsableAccordionContent open";
				  InternshipsPanelExtender.Collapsed = false;
				}

				#endregion

				#region Job Types Criteria

				if (searchExpression.HomeBasedJobsCriteria.IsNotNull())
				{
					var homeBasedJob = searchExpression.HomeBasedJobsCriteria.HomeBasedJobs;

					if (homeBasedJob.IsNotNull())
						ddlHomeBasedJobsRequired.SelectValue(homeBasedJob.ToString());

					JobTypesHeaderPanel.CssClass += " collapsableAccordionTitle on";
					JobTypesContentPanel.CssClass += " collapsableAccordionContent open";
					JobTypesPanelExtender.Collapsed = false;
				}

				if (searchExpression.CommissionBasedJobsCriteria.IsNotNull())
				{
					var commissionBasedJob = searchExpression.CommissionBasedJobsCriteria.CommissionBasedJobs;

					if (commissionBasedJob.IsNotNull())
						ddlCommissionBasedJobsRequired.SelectValue(commissionBasedJob.ToString());

					JobTypesHeaderPanel.CssClass += " collapsableAccordionTitle on";
					JobTypesContentPanel.CssClass += " collapsableAccordionContent open";
					JobTypesPanelExtender.Collapsed = false;
				}

				if (searchExpression.SalaryAndCommissionBasedJobsCriteria.IsNotNull())
				{
					var salaryAndCommissionBasedJob = searchExpression.SalaryAndCommissionBasedJobsCriteria.SalaryAndCommissionBasedJobs;

					if (salaryAndCommissionBasedJob.IsNotNull())
						ddlSalaryAndCommissionBasedJobsRequired.SelectValue(salaryAndCommissionBasedJob.ToString());

					JobTypesHeaderPanel.CssClass += " collapsableAccordionTitle on";
					JobTypesContentPanel.CssClass += " collapsableAccordionContent open";
					JobTypesPanelExtender.Collapsed = false;
				}

				#endregion

				#region Occupation and Industry Criteria

				if (searchExpression.IndustryCriteria.IsNotNull())
				{
					if (searchExpression.IndustryCriteria.IndustryId.HasValue)
						ddlIndustry.SelectValue(searchExpression.IndustryCriteria.IndustryId.ToString());

					if (searchExpression.IndustryCriteria.IndustryDetailIds.IsNotNull() && searchExpression.IndustryCriteria.IndustryDetailIds.Count() == 1)
					{
						if (searchExpression.IndustryCriteria.IndustryDetailIds[0].IsNotNull())
						{
							ddlIndustry.SelectValue(searchExpression.IndustryCriteria.IndustryDetailIds[0].ToString());
							ccdIndustryDetail.SelectedValue = searchExpression.IndustryCriteria.IndustryDetailIds[0].ToString();
						}
					}
					OccupationsAndIndustryHeaderPanel.CssClass += " collapsableAccordionTitle on";
					OccupationsAndIndustryContentPanel.CssClass += " collapsableAccordionContent open";
				  OccupationsAndIndustryPanelExtender.Collapsed = false;
				}

				#endregion

				#region Emerging Sectors Criteria

				if (searchExpression.JobSectorCriteria.IsNotNull())
				{
					foreach (var sectorId in searchExpression.JobSectorCriteria.RequiredJobSectorIds)
					{
						foreach (var checkbox in cblEmergingSectors.Items.Cast<ListItem>())
						{
							if (sectorId.ToString() == checkbox.Value)
								checkbox.Selected = true;
						}
					}

					EmergingSectorsHeaderPanel.CssClass += " collapsableAccordionTitle on";
					EmergingSectorsContentPanel.CssClass += " collapsableAccordionContent open";
				  EmergingSectorsPanelExtender.Collapsed = false;
				}

				#endregion

				#region Physical abilities Criteria

				if (searchExpression.PhysicalAbilityCriteria.IsNotNull())
				{
					foreach (var abilityId in searchExpression.PhysicalAbilityCriteria.PhysicalAbilityIds)
					{
						foreach (var checkbox in cblDisabilityCategories.Items.Cast<ListItem>())
						{
							if (abilityId.ToString() == checkbox.Value)
								checkbox.Selected = true;
						}
					}
					PhysicalAbilitiesHeaderPanel.CssClass += " collapsableAccordionTitle on";
					PhysicalAbilitiesContentPanel.CssClass += " collapsableAccordionContent open";
				  PhysicalAbilitiesPanelExtender.Collapsed = false;
				}

				#endregion

				#region Exclude Job Criteria

				if (searchExpression.ReferenceDocumentCriteria.IsNotNull())
				{
					foreach (ListItem checkbox in cblExcludeJobs.Items)
					{
						var excludejob = searchExpression.ReferenceDocumentCriteria.ExcludedJobs;

						if (excludejob.IsNotNull())
							foreach (var job in excludejob.Where(job => job.JobId == checkbox.Value.AsNullableGuid()))
							{
								checkbox.Selected = true;
							}
					}
					ExcludeJobsHeaderPanel.CssClass += " collapsableAccordionTitle on";
					ExcludeJobsContentPanel.CssClass += " collapsableAccordionContent open";
          ExcludeJobsPanelExtender.Collapsed = false;
				}

				#endregion

				#region ROnet Criteria

				if (searchExpression.ROnetCriteria.IsNotNull())
				{
					if (searchExpression.ROnetCriteria.CareerAreaId.IsNotNull())
					{
						ddlJobFamily.SelectValue(searchExpression.ROnetCriteria.CareerAreaId.ToString());
					}

					if (searchCriteria.ROnetCriteria.ROnets.IsNotNullOrEmpty())
					{
						ddlOccupation.SelectValue(searchCriteria.ROnetCriteria.ROnets[0]);
						ccdOccupation.SelectedValue = searchExpression.ROnetCriteria.ROnets[0].ToString(CultureInfo.InvariantCulture);
					}

				  OccupationsAndIndustryPanelExtender.Collapsed = false;

				}

				#endregion

				#region Work Availability

			  if (searchCriteria.WorkDaysCriteria.IsNotNull())
			  {
			    SearchCriteriaWorkDays.Bind(searchCriteria.WorkDaysCriteria);
			  }

			  #endregion

        #region Language Proficiencies

			  if (searchCriteria.LanguageCriteria.IsNotNull())
			  {
			    searchCriteriaLanguage.Bind(searchCriteria.LanguageCriteria);
			    LanguagesHeaderPanel.CssClass += " collapsableAccordionTitle on";
			    LanguagesContentPanel.CssClass += " collapsableAccordionContent open";
			    LanguagesPanelExtender.Collapsed = false;
			  }

			  #endregion
			}
			else
			{
				SetSearchDefaults(true, true);
				BindPreferedLocation();
			}
		}

		/// <summary>
		/// Set default search settings
		/// </summary>
		/// <param name="jobMatching">if set to <c>true</c> then default job matching fields</param>
		/// <param name="searchRadius">if set to <c>true</c> then default radius fields</param>
		private void SetSearchDefaults(bool jobMatching = false, bool searchRadius = false)
		{
			if (jobMatching)
			{
				rbWithoutMatch.Checked = App.Settings.CareerSearchShowAllJobs || !rbStarMatch.Enabled;
				rbStarMatch.Checked = !rbWithoutMatch.Checked;
				ddlStarMatch.SelectedIndex = App.Settings.CareerSearchMinimumStarMatchScore - 1;
			}

			if (searchRadius && App.Settings.CareerSearchDefaultRadiusId > 0)
			{
				rbArea.Checked = true;
				ddlRadius.SelectedValue = App.Settings.CareerSearchDefaultRadiusId.ToString(CultureInfo.InvariantCulture);
				if (App.UserData.Profile.IsNotNull() && App.UserData.Profile.PostalAddress.IsNotNull() && App.UserData.Profile.PostalAddress.Zip.IsNotNullOrEmpty())
					txtZipCode.Text = App.UserData.Profile.PostalAddress.Zip;
			}
		}

		/// <summary>
		/// Binds the prefered location.
		/// </summary>
		private void BindPreferedLocation()
		{
			if (Resume.IsNull() && JobSeekerProfileModel.IsNotNull())
				Resume = ServiceClientLocator.ResumeClient(App).GetResume(JobSeekerProfileModel.ResumeId);

			if (Resume.IsNotNull() && Resume.Special.IsNotNull())
			{
				var preference = Resume.Special.Preferences;
				if (preference.IsNotNull())
				{
					cbJobsInMyState.Checked = preference.SearchInMyState.HasValue && preference.SearchInMyState.Value;

					if (preference.MSAPreference.IsNotNull() && preference.MSAPreference.Any())
					{
						ddlState.SelectValue(preference.MSAPreference[0].StateId.ToString());
						ddlMSA.SelectValue(preference.MSAPreference[0].MSAId.ToString());
						ccdMSA.SelectedValue = preference.MSAPreference[0].MSAId.ToString();
						rbMSAState.Checked = true;
					}

					if (preference.ZipPreference.IsNotNull() && preference.ZipPreference.Any())
					{
						if (preference.ZipPreference[0].RadiusId.IsNotNull())
						{
							ddlRadius.SelectValue(preference.ZipPreference[0].RadiusId.ToString());
							txtZipCode.Text = preference.ZipPreference[0].Zip;
							rbArea.Checked = true;
						}
					}
				}
			}

			if (!rbMSAState.Checked && !rbArea.Checked && !App.Settings.NationWideInstance)
			{
				var stateId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault();

				ddlState.SelectValue(stateId.ToString());
				rbMSAState.Checked = true;
			}
		}

		/// <summary>
		/// Sets up default search criteria if user has not specified any (and if App Settings are set accordingly)
		/// </summary>
		/// <param name="jobLocation">The job location criteria</param>
		/// <param name="app">The application.</param>
		private void BuildDefaultSearchLocationCriteria(JobLocationCriteria jobLocation, IApp app)
		{
			if (App.Settings.CareerSearchCentrePointType == SearchCentrePointOptions.Zipcode && App.Settings.CareerSearchRadius > 0 && App.Settings.CareerSearchZipcode.Length > 0)
			{
				var radius = new RadiusCriteria
				{
					Distance = App.Settings.CareerSearchRadius,
					DistanceUnits = DistanceUnits.Miles,
					PostalCode = App.Settings.CareerSearchZipcode
				};
				jobLocation.Radius = radius;
			}
			else if (App.Settings.CareerSearchCentrePointType == SearchCentrePointOptions.SelectedLocations && App.Settings.NearbyStateKeys.Any())
			{
				var lookups = ServiceClientLocator.CoreClient(app).GetLookup(LookupTypes.States).Where(x => App.Settings.NearbyStateKeys.Contains(x.Key)).Select(x => x.ExternalId).ToList();

				if (lookups.IsNotNullOrEmpty())
					jobLocation.SearchMultipleStates = lookups;
			}
		}
	}
}