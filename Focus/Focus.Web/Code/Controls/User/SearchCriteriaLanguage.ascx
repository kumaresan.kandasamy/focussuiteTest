﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchCriteriaLanguage.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SearchCriteriaLanguage" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<%@ Register Src="UpdateableLanguagesList.ascx" TagName="UpdateableLanguageList" TagPrefix="uc" %>
<%@ Register Src="LanguageProficiencyDefinitionsModal.ascx" TagName="LangProficiencyDefinitionModal" TagPrefix="uc" %>

<asp:UpdatePanel ID="UpdatePanelSearchCriteriaLanguage" runat="server" UpdateMode="Conditional" OnPreRender="UpdatePanelSearchCriteriaLanguage_OnPreRender">
  <ContentTemplate>
    
    <div class="dataInputField">
      <div class="instructionalText">
        <focus:LocalisedLabel runat="server" ID="LanguagesInstructionsLabel" RenderOuterSpan="False"
                              LocalisationKey="LanguagesInstructions.Label" DefaultText="Indicate all languages in which you are proficient."/>
        <asp:LinkButton ID="lnkLanguageProficiences" runat="server" OnClick="lnkLanguageProficiences_Click">
          <focus:LocalisedLabel runat="server" ID="PreviewLanguageProficiences" RenderOuterSpan="False"
                                LocalisationKey="PreviewLanguageProficiences.LinkText" DefaultText="(Language proficiency definitions)"/>
        </asp:LinkButton>
      </div>
    </div>
    
    <div class="dataInputRow">
      <div class="dataInputGroupedItemHorizontal" style="margin-bottom: 10px;">
        <div class="dataInputField">
				  <focus:LocalisedLabel runat="server" ID="LanguageTypeLabel" AssociatedControlID="LanguageTextBox"
					  CssClass="requiredData" LocalisationKey="Language.Label" DefaultText="Language" />
				  <span class="inFieldLabel">
					  <focus:LocalisedLabel runat="server" ID="LanguageLabel" AssociatedControlID="LanguageTextBox"
						  LocalisationKey="Language.Label" DefaultText="Language" Width="250" />
				  </span>
          <div id="language-outer">
				    <asp:TextBox ID="LanguageTextBox" runat="server" Width="250" ClientIDMode="Static" />      
          </div>
          <asp:CustomValidator ID="LanguageTextBoxValidator" runat="server" CssClass="error" ControlToValidate="LanguageTextBox" ValidateEmptyText="true" SetFocusOnError="True" />
				  <ajaxtoolkit:AutoCompleteExtender ID="aceLanguage" runat="server" CompletionSetCount="20"
					  CompletionListCssClass="autocomplete_completionListElement" CompletionListItemCssClass="autocomplete_listItem"
					  CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" ServiceMethod="GetLanguages"
					  ServicePath="~/Services/AjaxService.svc" TargetControlID="LanguageTextBox" CompletionInterval="5"
					  UseContextKey="false" EnableCaching="true" MinimumPrefixLength="3" OnClientShown="fixAutoCompleteExtender">
				  </ajaxtoolkit:AutoCompleteExtender>
			  </div>
      </div>
      <div class="dataInputGroupedItemHorizontal">
        <div class="dataInputField">
				  <focus:LocalisedLabel runat="server" ID="LanguageProficiencyLabel" AssociatedControlID="ddlLanguageProficiency"
					  CssClass="requiredData" LocalisationKey="LanguageProficiency.Label" DefaultText="Language Proficiency" />
				  <br />
				  <div id="language-proficiency-outer">
					  <asp:DropDownList runat="server" ID="ddlLanguageProficiency" Enabled="false" Width="250"/>
					  <div class="dataInputGroupedItemHorizontal"></div>
						  <asp:LinkButton ID="LanguageAddButton" runat="server" ClientIDMode="Static" Text="+ Add"
						  Style="margin-left: 10px;" OnClick="LanguageAddButton_Click" ValidationGroup="LanguageProficiency" />
					  <div class="dataInputGroupedItemHorizontal"></div>
				  </div>
				  <asp:CustomValidator ID="LanguageProficiencyRequired" runat="server" ControlToValidate="ddlLanguageProficiency"
					  CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="LanguageProficiency"
					  ValidateEmptyText="True" ClientValidationFunction="ValidateLanguageProficiency" />
			  </div>
      </div>
    </div>

    <div class="dataInputRow">      
      <asp:CustomValidator ID="ProficiencyMissing" runat="server" CssClass="error" SetFocusOnError="true"
                           Display="Dynamic" ValidationGroup="Education" ValidateEmptyText="True" OnServerValidate="ProficiencyMissing_OnServerValidate"/>
    </div>
    <div>
      <asp:Panel ID="LanguagePanel" CssClass="resumeEducationLanguageList" runat="server" ScrollBars="Auto">
        <uc:UpdateableLanguageList ID="LanguagesUpdateableList" runat="server" RestrictDuplicates="true" Width="420" />
      </asp:Panel>
    </div>
    <div>
      <asp:RadioButton ID="SearchLanguagesAnd" runat="server" Checked="true" Enabled="false"
                       GroupName="LanguageSearchTypeRadios" />
      <%= HtmlLabel(SearchLanguagesAnd,"SearchLanguagesAnd.Text", "Jobs with ALL selected languages") %>
      <br/>
      <asp:RadioButton ID="SearchLanguagesOr" runat="server" Enabled="false"
                       GroupName="LanguageSearchTypeRadios" />
      <%= HtmlLabel(SearchLanguagesOr,"SearchLanguagesOr.Text", "Jobs with ANY selected language") %>
    </div>
  </ContentTemplate>
  <Triggers>
    <asp:AsyncPostBackTrigger ControlID="LanguageAddButton" EventName="Click"/>
  </Triggers>
</asp:UpdatePanel>
<uc:LangProficiencyDefinitionModal ID="LangProficiencyModal" runat="server" />
	
<script type="text/javascript">

  $().ready(function() {
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(styleLanguageProficiencyControls);
  });

  Sys.Application.add_load(LanguageProficiency_PageLoad);

  function LanguageProficiency_PageLoad() {

  	var langBox = $("[id$='LanguageTextBox']");
  	var langProf = $("[id$='ddlLanguageProficiency']");

  	langBox.val("");
	  langProf.prop("selectedIndex", 0);

  	var lang = langBox.on("input", function () {
      if (lang.val().length > 0) {
        langProf.removeAttr('disabled');
      } else {
        langProf.prop('disabled', true);
      }
    });
  };

  function styleLanguageProficiencyControls() {
    SetLanguageProficiencyUIState();
    //$("[id$='ddlLanguageProficiency']").customSelect();
    $("[id$='LanguageLabel']").inFieldLabels();
  }

  function SetLanguageProficiencyUIState() {
    var lang = $("#<%= LanguageTextBox.ClientID %>");
    var langProf = $("[id$='ddlLanguageProficiency']");
    if ($(langProf).exists() && $(langProf).is(':disabled') && lang.val().length == 0) {
      langProf.prop('disabled', true);
    } else {
      langProf.removeAttr('disabled');
    }
  }

  function ValidateLanguageProficiency(src, args) {
    var lang = $("#<%= LanguageTextBox.ClientID %>");
    var proficiency = $("[id$='ddlLanguageProficiency']");
    if (lang.val().length > 0 && !proficiency.is(":disabled")) {
      if (proficiency.children().first().is(':selected')) {
        args.IsValid = false;
      }
    }
  }
</script>
