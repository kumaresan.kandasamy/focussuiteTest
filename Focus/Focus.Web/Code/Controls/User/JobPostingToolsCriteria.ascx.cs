﻿using System;
using Framework.Core;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Career;
using System.Xml;
using System.Collections.Generic;
using Framework.Exceptions;
using Mindscape.LightSpeed.Querying;

namespace Focus.Web.Code.Controls.User
{
	public partial class JobPostingToolsCriteria : UserControlBase
	{

		#region properties

		public int ViewCount { get; set; }
		public long JobSeekerId { get; set; }
		public string LensPostingId { get; set; }
		public long JobId { get; set; }
		public long? ReferralId { get; set; }
		public string BackUrl { get; set; }

		private int? Score
		{
			get { return GetViewStateValue<int?>("JobPostingToolsCriteria:Score"); }
			set { SetViewStateValue("JobPostingToolsCriteria:Score", value); }
		}

		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			JobId = ServiceClientLocator.PostingClient(App).GetJobIdFromPosting(LensPostingId).GetValueOrDefault();
			ViewCountLabel.Text = HtmlLocalise("ViewedJob.Label", "This job has been viewed " + ViewCount.ToString() + " times");

			if (App.Settings.Module == FocusModules.Talent)
			{
				EmailPlaceHolder.Visible = ReferPlaceHolder.Visible = ToolLinksPlaceHolder.Visible = ViewReferralPlaceHolder.Visible = false;
			}
			else
			{
				if (JobSeekerId == 0 || ServiceClientLocator.ResumeClient(App).GetDefaultResumeId(JobSeekerId, true) == 0)
				{
					EmailPlaceHolder.Visible = ReferPlaceHolder.Visible = ViewReferralPlaceHolder.Visible = false;
				}
				else
				{

					var application = ServiceClientLocator.CandidateClient(App).GetApplication(JobSeekerId, JobId);

					ReferralId = application.Id;

					ViewReferralPlaceHolder.Visible = application.IsNotNull() &&
					                                  (application.ApprovalStatus.IsIn(ApprovalStatuses.Rejected)
					                                   || application.ApprovalStatus.IsIn(ApprovalStatuses.Reconsider)
					                                   || application.ApprovalStatus.IsIn(ApprovalStatuses.WaitingApproval))
						;
					ReferPlaceHolder.Visible = !ViewReferralPlaceHolder.Visible;

					//AmIGoodMatchPlaceHolder.Visible = EmailPlaceHolder.Visible = ReferPlaceHolder.Visible = ViewReferralPlaceHolder.Visible = false;
					//Response.RedirectToRoute("AssistJobMatching", new { jobid = LensPostingId, fromURL = "AssistJobSearchResults", jobseekerid = JobSeekerId });


				}
                   

                if (App.Settings.Module == FocusModules.Assist && JobSeekerId != 0 && App.Settings.UnderAgeJobSeekerRestrictionThreshold > 0)
                {
                    var person = ServiceClientLocator.CandidateClient(App).GetCandidatePerson(JobSeekerId);

                    long jsPersonId = person.Id == null ? 0 : (long)Convert.ToDouble(person.Id.ToString());
                    var jsUser = ServiceClientLocator.AccountClient(App).GetUserDetails(0, jsPersonId, false);
                    bool underAgeFlag = false;

                    if (person.Age < App.Settings.UnderAgeJobSeekerRestrictionThreshold )
                    {
                        EmailJobButton.Visible = false;
                        ReferJobButton.Visible = false;
                        underAgeFlag=true;
                    }
                    //FVN-6960 ss#497 - Inactive job seeker referral
                    if (!jsUser.UserDetails.Enabled)
                    {
                        EmailJobButton.Visible = false;
                        ReferJobButton.Visible = false;
                    }
                    else if(!underAgeFlag)
                    {
                        
                        EmailJobButton.Visible = true;
                        ReferJobButton.Visible = true;
                    }
                }

				ReferJobButton.Text = CodeLocalise("ReferJobButton.Text", "Refer job");
				ViewReferralButton.Text = CodeLocalise("ViewReferralButton.Text", "View referral request");
				JobSeekerAlreadyReferredLabel.Text = CodeLocalise("JobSeekerAlreadyReferred.Text", "#CANDIDATETYPE# has already been referred to this job.");


			}

		}

		/// <summary>
		/// Handles the Click event of the AmIGoodMatchHyperLink control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AmIGoodMatchHyperLink_Click(object sender, EventArgs e)
		{
			if (App.User.IsAuthenticated)
			{
				// Log self service
				if (JobSeekerId != 0)
					ServiceClientLocator.CoreClient(App).SaveSelfService(ActionTypes.ViewJobInsights, actingOnBehalfOfPersonId: JobSeekerId);
				// Redirect
				Response.RedirectToRoute("AssistJobMatching", new { jobid = LensPostingId, fromURL = "AssistJobSearchResults", jobseekerid = JobSeekerId });
			}
		}

		/// <summary>
		/// Handles the Click event of the FindMoreJobsHyperLink control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void FindMoreJobsHyperLink_Click(object sender, EventArgs e)
		{
			var criteria = App.GetSessionValue<Focus.Core.Models.Career.SearchCriteria>("AssistSearchCriteria");
			criteria = new Focus.Core.Models.Career.SearchCriteria
			{
				JobLocationCriteria = (criteria.IsNotNull()) ? criteria.JobLocationCriteria : null,
				ReferenceDocumentCriteria = new ReferenceDocumentCriteria
				{
					DocumentType = DocumentType.Posting,
					DocumentId = LensPostingId
				}
			};

			App.SetSessionValue("Career:SearchCriteria", criteria);

			Response.RedirectToRoute("AssistJobSearchResults");
		}

		/// <summary>
		/// Handles the Click event of the DoNotDisplayHyperLink control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DoNotDisplayHyperLink_Click(object sender, EventArgs e)
		{
			if (App.User.IsAuthenticated)
			{
				try
				{
					if (LensPostingId.IsNotNullOrEmpty())
						ServiceClientLocator.OrganizationClient(App).SetDoNotDisplay(LensPostingId);

					Response.Redirect(BackUrl);
				}
				catch (ApplicationException ex)
				{
					MasterPage.ShowError(AlertTypes.Error, ex.Message);
				}
			}
			//else
			//  ConfirmationModal.Show();
		}

		/// <summary>
		/// Handles the Click event of the EmailJob control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void EmailJob_Click(object sender, EventArgs e)
        {
            if (App.User.IsAuthenticated)
            {
                DateTime? extendVeteranPriorityDate = null;
                ExtendVeteranPriorityOfServiceTypes veteranPriorityOfService = ExtendVeteranPriorityOfServiceTypes.None;

                var posting = ServiceClientLocator.PostingClient(App).GetJobPosting(LensPostingId);

                var person = ServiceClientLocator.CandidateClient(App).GetCandidatePerson(JobSeekerId);

                var jobId = posting.PostingInfo.FocusJobId.GetValueOrDefault(0);

                var employerName = posting.PostingInfo.Employer;
                var jobTitle = posting.PostingInfo.Title;

                if (jobId > 0)
                {
                    var job = ServiceClientLocator.JobClient(App).GetJob(jobId);
                    var employer = ServiceClientLocator.EmployerClient(App).GetEmployer(job.EmployerId);

                    jobTitle = job.JobTitle;
                    employerName = employer.Name;
                    if (job.ExtendVeteranPriority != null) veteranPriorityOfService = job.ExtendVeteranPriority.Value;
                    extendVeteranPriorityDate = job.VeteranPriorityEndDate;

                    var autoApprovalAvailable = jobId.IsNotNull() && AutoApprovalModal.Show(JobSeekerId, jobId);

                    if (!autoApprovalAvailable)
                    {
                        var isExistingApplicant = ServiceClientLocator.CandidateClient(App).IsCandidateExistingJobInviteeOrApplicant(jobId, JobSeekerId);

                        if (isExistingApplicant.Exists && isExistingApplicant.IsApplicationPendingApproval)
                        {
                            var application = ServiceClientLocator.CandidateClient(App).GetApplication(JobSeekerId, jobId);

                            var candidateName = (application.ApplicationStatus != ApplicationStatusTypes.NotApplicable &&
                                                    application.ApplicationStatus != ApplicationStatusTypes.SelfReferred &&
                                                    person.FirstName.IsNotNullOrEmpty() && person.LastName.IsNotNullOrEmpty() && jobId != 0)
                                ? person.FirstName+ ""+ person.LastName
                                : CodeLocalise("TheJobSeeker.Text", "the job seeker");


                            ConfirmationModal.Show(CodeLocalise("PendingApplication.Title", "Pending application"),
                                CodeLocalise("PendingApplicationApproved.Body", "An application has already been received from " + candidateName + " and was waiting to be approved."),
                                CodeLocalise("Global.Close.Text", "Close"));
                            return;
                        }
                        else if (isExistingApplicant.Exists)
                        {
                            ConfirmationModal.Show(CodeLocalise("ApplicantExists.Title", "Error"),
                                        CodeLocalise("ApplicantExists.Body", "This job seeker is already an invitee or applicant to this job"),
                                        CodeLocalise("Global.Close.Text", "Close"),height:50);
                            return;
                        }
                        else if (ServiceClientLocator.ResumeClient(App).GetDefaultResumeId(JobSeekerId, true) == 0)
                        {
                            ConfirmationModal.Show(CodeLocalise("ApplicantResumeNotExists.Title", "Error"),
                                       CodeLocalise("ApplicantResumeNotExists.Body", "This job seeker currently does not have a completed resume"),
                                       CodeLocalise("Global.Close.Text", "Close"),height:50);
                            return;
                        }
                        else
                        {
                            // Job available only to veterans for the life of posting
                            if (job.ExtendVeteranPriority.IsNotNull() && (!person.IsVeteran.GetValueOrDefault() && (job.ExtendVeteranPriority.Value.Equals(ExtendVeteranPriorityOfServiceTypes.ExtendForLifeOfPosting))))
                            {
                                ConfirmationModal.Show(CodeLocalise("ReferralVeteranPosting.Title", "Posting exclusive to veterans"),
                                    CodeLocalise("ReferralVeteranPosting.Body",
                                    "This job is set to exclude non-veteran invitations or recommendations for the lifetime of the posting." +
                                    "You cannot invite or recommend that this job seeker apply."),
                                    CodeLocalise("CloseModal.Text", "OK"));
                                return;
                            }

                            EmailCandidate.Show(EmailTemplateTypes.RecommendJobSeeker, JobSeekerId, jobId, LensPostingId, employerName, jobTitle, extendVeteranPriorityDate, veteranPriorityOfService, "", true, veteran: person.IsVeteran.GetValueOrDefault());
                        }
                    }

                }
                else if (LensPostingId.IsNotNullOrEmpty())
                {
                    EmailCandidate.Show(EmailTemplateTypes.RecommendJobSeeker, JobSeekerId, jobId, LensPostingId, employerName, jobTitle, extendVeteranPriorityDate, veteranPriorityOfService, "", true, veteran:person.IsVeteran.GetValueOrDefault());
                }
            }
        }

		protected void ReferJobButton_Link(object sender, EventArgs e)
		{
			bool alienRegistrationExpire = false;

			long defaultResumeId = ServiceClientLocator.ResumeClient(App).GetDefaultResumeId(JobSeekerId, true);
			var defaultResume = ServiceClientLocator.ResumeClient(App).GetResume(defaultResumeId);

			if (defaultResume.IsNotNull() && defaultResume.ResumeContent.Profile.IsNotNull())
			{
				var userProfile = defaultResume.ResumeContent.Profile;

				if (!(userProfile.IsUSCitizen ?? true))
				{
					if (userProfile.AlienExpires.HasValue)
					{
						if (userProfile.AlienExpires < DateTime.Now)
						{
							alienRegistrationExpire = true;
						}
					}
				}
			}

			// if non US and registraton expired then do not allow posting
			if (alienRegistrationExpire)
			{
				ReferralConfirmationModal.Show(CodeLocalise("ReferralQueued.Title", "Unable to refer"),
					CodeLocalise("ReferralQueued.Body",
						"The selected job seeker has a lapsed alien registration expiry date on their default resume and therefore you are unable to refer them to this posting."),
					CodeLocalise("Global.Close.Text", "Close"));
				ConfirmationUpdatePanel.Update();

			}
			else
			{
				// is lens id same as LensPostingId? If so remove! PM
				var lensId = Page.RouteData.Values["jobid"].ToString();
				int originId;
				int rank;

				Int32.TryParse(Page.Request.QueryString["rank"], out rank);

				if (Int32.TryParse(Page.Request.QueryString["originid"], out originId) &&
				    originId != 999)
				{
					var jobId = ServiceClientLocator.PostingClient(App).GetJobIdFromPosting(lensId).GetValueOrDefault();

					if (jobId != 0)
					{
						var application = ServiceClientLocator.CandidateClient(App).GetApplication(JobSeekerId, jobId);
						if (application.IsNull() || application.ApprovalStatus != ApprovalStatuses.Approved)
						{
							var autoApprovalAvailable = AutoApprovalModal.Show(JobSeekerId, jobId);

							if (!autoApprovalAvailable)
								JobApplicationRequirementsModal.Show(jobId, JobSeekerId, rank);
						}
						else
						{
							JobSeekerAlreadyReferredLabel.Visible = true;
						}
					}
					else
					{
						EmailCandidate.Show(EmailTemplateTypes.InvitationToApplyForJobThroughFocusCareer, new List<long> { JobSeekerId },
							lensPostingIds: new List<string> { lensId });
					}
				}
				else
				{
					var postingId = ServiceClientLocator.PostingClient(App).GetPostingId(lensId).GetValueOrDefault(0);
					if (postingId == 0)
					{
						EmailCandidate.Show(EmailTemplateTypes.InvitationToApplyForJobThroughFocusCareer, new List<long> { JobSeekerId },
							lensPostingIds: new List<string> { lensId });
					}
					else
					{
						var application = ServiceClientLocator.CandidateClient(App).GetApplicationForPosting(JobSeekerId, postingId);
						if (application.IsNull() || application.ApprovalStatus != ApprovalStatuses.Approved)
						{
							var autoApprovalAvailable = AutoApprovalModal.Show(JobSeekerId, application: application);
							if (!autoApprovalAvailable)
							{
								var actionType = ActionTypes.ExternalReferral;

								if (App.User.IsShadowingUser || App.User.PersonId != JobSeekerId)
									actionType = ActionTypes.ExternalStaffReferral;

								var score = GetScore(defaultResumeId);
								var alreadyReferred = ServiceClientLocator.CandidateClient(App).RegisterSelfReferral(postingId, JobSeekerId, score, actionType);

								if (alreadyReferred)
								{
									JobSeekerAlreadyReferredLabel.Visible = true;
								}
								else
								{
									ConfirmationModal.Show(CodeLocalise("ReferralCompleted.Heading", "Referral completed"),
										CodeLocalise("ReferralCompleted.Text",
											"The candidate has been successfully referred for the job"),
										CodeLocalise("Global.Ok", "Ok"));
								}
							}
						}
						else
						{
							JobSeekerAlreadyReferredLabel.Visible = true;
						}
					}
				}
			}
		}

		protected void JobApplicationRequirementsModal_Referred(object sender, JobApplicationReferredEventArgs e)
		{
			try
			{
				var isQueued = ServiceClientLocator.CandidateClient(App).ReferCandidate(e.PersonId, e.JobId, e.Score, e.WaivedRequirments, true);
				if(isQueued)
                {
                    var job = ServiceClientLocator.JobClient(App).GetJob(e.JobId);
                    Utilities.SendResumeIfJobContactByEmail(e.JobId, e.PersonId, job.VeteranPriorityEndDate);
                }
                else
                    Utilities.SendResumeIfJobContactByEmail(e.JobId, e.PersonId);

				if (isQueued)
					ConfirmationModal.Show(CodeLocalise("ReferralQueued.Title", "Referral queued"),
						CodeLocalise("ReferralQueued.Body",
							"You have referred a non-veteran for a job that is currently being held for 24 hours for veteran priority.<br /><br />The referral will be queued and released once the 24 hour period has lapsed."),
						CodeLocalise("Global.Ok.Text", "Ok"));
				else
					ConfirmationModal.Show(CodeLocalise("ReferralCompleted.Heading", "Referral completed"),
						CodeLocalise("ReferralCompleted.Text",
							"The candidate has been successfully referred for the job"),
						CodeLocalise("Global.Ok", "Ok"));
			}
			catch (ServiceCallException ex)
			{
				if (ex.ErrorCode == (int)ErrorTypes.ResumeNotFound)
				{
					ConfirmationModal.Show(CodeLocalise("ReferralFailed.Heading", "Referral failed"),
						CodeLocalise("ReferralFailed.Text",
							"Unable to refer candidate as they no longer have a resume"),
						CodeLocalise("Global.Ok", "Ok"));
				}
				else
				{
					throw;
				}
			}
		}

		/// <summary>
		/// Fires when the View Referral Request button is clicked to redirected to the referral request page
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void ViewReferralButton_OnClick(object sender, EventArgs e)
		{
			if (ReferralId.IsNotNullOrZero())
			{
				//SetReturnUrl();

				Response.Redirect(UrlBuilder.JobSeekerReferral(ReferralId));
			}
		}

		protected void EmailCandidateModal_Completed(object sender, EmailJobSeekerCompletedEventArgs args)
		{
			var emailType = args.EmailTemplateType;

			switch (emailType)
			{
				case EmailTemplateTypes.InvitationToApplyForJobThroughFocusCareer:
					ServiceClientLocator.PostingClient(App).GetJobPosting(LensPostingId); // Ensure posting is in our db

					ServiceClientLocator.CandidateClient(App).RegisterHowToApply(LensPostingId, JobSeekerId);
					break;
				case EmailTemplateTypes.RecommendJobSeeker:
					var score = GetScore();
					ServiceClientLocator.CandidateClient(App).RegisterJobPostingOfInterestSent(LensPostingId, JobSeekerId, score, App.User.UserId , App.Settings.Module == FocusModules.Assist,args.QueueRecommendation);
					break;
			}
			/*
			ClearCheckBoxes();
			// Redirect instead?
			 */
		}

		/// <summary>
		/// Gets the matching score for the job
		/// </summary>
		/// <param name="defaultResumeId">The id of the job seeker's resume</param>
		/// <returns>The matching score</returns>
		private int GetScore(long? defaultResumeId = null)
		{
			if (Score.IsNull())
			{
				if (defaultResumeId.IsNull())
				{
					defaultResumeId = ServiceClientLocator.ResumeClient(App).GetDefaultResumeId(JobSeekerId, true);
				}

				Score = (LensPostingId.IsNotNullOrEmpty()) ? ServiceClientLocator.OrganizationClient(App).GetMatchScore(defaultResumeId.GetValueOrDefault(), LensPostingId) : 0;
			}

			return Score.GetValueOrDefault();
		}
	}
}
