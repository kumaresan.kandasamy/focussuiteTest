﻿using System;
using System.IO;
using System.Web.UI;
using AjaxControlToolkit;
using Focus.Core;
using Framework.Core;
using Focus.Services;
using Focus.Common.Extensions;

namespace Focus.Web.Code.Controls.User
{
	public partial class ImageUploader : UserControlBase
	{
		#region Properties

		/// <summary>
		/// Gets or sets the width of the image.
		/// </summary>
		public int ImageWidth
		{
			get { return GetViewStateValue("UploadImage:ImageWidth", 450); }
			set { SetViewStateValue("UploadImage:ImageWidth", value); }
		}

		/// <summary>
		/// Gets or sets the height of the image.
		/// </summary>
		public int ImageHeight
		{
			get { return GetViewStateValue("UploadImage:ImageHeight", 113); }
			set { SetViewStateValue("UploadImage:ImageHeight", value); }
		}

		/// <summary>
		/// Gets or sets the size of the max file.
		/// </summary>
		public int MaxFileSize
		{
			get { return GetViewStateValue("UploadImage:MaxFileSize", 100); }
			set { SetViewStateValue("UploadImage:MaxFileSize", value); }
		}

		/// <summary>
		/// Gets or sets the validation group.
		/// </summary>
		/// <value>The validation group.</value>
		public string ValidationGroup
		{
			get { return GetViewStateValue("UploadImage:ValidationGroup", string.Empty); }
			set { SetViewStateValue("UploadImage:ValidationGroup", value); }
		}

		/// <summary>
		/// Gets the Image.
		/// </summary>
		/// <value>The Image.</value>
		public byte[] Image
		{
			get { return App.GetSessionValue<byte[]>(Constants.StateKeys.UploadedImage); }
		}

		#endregion

		/// <summary>
		/// Clears this control.
		/// </summary>
		public void Clear()
		{
			App.RemoveSessionValue(Constants.StateKeys.UploadedImage);
		}

		/// <summary>
		/// Determines whether this control has file.
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if this control has file; otherwise, <c>false</c>.
		/// </returns>
		public bool HasFile()
		{
			var fileBytes = App.GetSessionValue<byte[]>(Constants.StateKeys.UploadedImage);

			return (fileBytes != null && fileBytes.Length > 0);
		}

		/// <summary>
		/// Sets the preview image.
		/// </summary>
		/// <param name="imageUrl">The image URL.</param>
		public void SetPreviewImage(string imageUrl)
		{
			UploadedImage.ImageUrl = imageUrl;
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			// Set the validation group
			if (ValidationGroup.IsNotNullOrEmpty())
			{
				UploadImageAsyncFileUploadedImageFileTypeValidator.ValidationGroup = ValidationGroup;
				UploadImageAsyncFileUploadedImageIsValidValidator.ValidationGroup = ValidationGroup;
			}

			// Localise some error messages
			UploadImageAsyncFileUploadedImageFileTypeValidator.ErrorMessage = CodeLocalise("UploadImageAsyncFileUpload.InvalidFileType", "Image must be in GIF, JPEG, or PNG format.");
			UploadImageAsyncFileUploadedImageIsValidValidator.ErrorMessage = CodeLocalise("UploadImageAsyncFileUpload.InvalidFileSize", "Image has invalid dimensions or is too big.");

			// Hide the spoof textboxes
			UploadedImageFileType.Style.Add("display", "none");
			UploadedImageIsValid.Style.Add("display", "none");

			var logoPreviewUrl = UrlBuilder.ViewLogo(Constants.StateKeys.UploadedImage);

			//UploadProgressImage.ImageUrl = UrlBuilder.ProgressImage();

			ScriptManager.RegisterClientScriptBlock(this, typeof(ImageUploader), "UploadImageJavaScript", @"

  var ImageUploader_Counter = 0;

	function UploadImageStartup() {"
		+ @"$('.fileupload input:file').customFileInput('" + CodeLocalise("Global.Browse.Text", "Browse") + @"', '" + CodeLocalise("Global.Change.Text", "Change") + @"', '" + CodeLocalise("UploadImageFeedback.Text", "Upload an image") + @"');
			var input = $('.fileupload input:file');
			$('<label class=""sr-only"">File Upload</label>').html($(this).html()).attr('for', input.attr('id')).insertBefore(input);	
}

	function UploadImageUploadComplete(sender, args) {
		var options = { type: ""POST"",
			url: """ + UrlBuilder.AjaxService() + @"/IsUploadedImageValid"",
			contentType: ""application/json; charset=utf-8"",
			dataType: ""json"",
			async: true,
			success: function (response) {
				var results = response.d;
				$('#UploadedImageIsValid').val(results);
				if (results == '1'){
          ImageUploader_Counter++;
					$('#UploadedImage').attr('src', '" + logoPreviewUrl + @"' + ImageUploader_Counter);
          $('#" + UploadImageAsyncFileUploadedImageIsValidValidator.ClientID + @"').hide();
				} else {
          $('#" + UploadImageAsyncFileUploadedImageIsValidValidator.ClientID + @"').show();
        }
			}
		};

		$.ajax(options);

		
	}

	function UploadImageCheckExtension(sender, args) {
		var filename = args.get_fileName();
		var ext = filename.substring(filename.lastIndexOf(""."") + 1).toLowerCase();

		$('#UploadImageFileType').val(ext);

		return (ext == 'png' || ext == 'jpg' || ext == 'gif');
	}

	function UploadImageAsyncFileUploadImageFileTypeValidator_Validate(source, args) {
		var fileType = $('#UploadImageFileType').val();
		args.IsValid = (fileType == 'png' || fileType == 'jpg' || fileType == 'gif');
	}

	function UploadImageAsyncFileUploadedImageIsValidValidator_Validate(source, args) {
		var isValid = ($('#UploadedImageIsValid').val() == ""1"");
		args.IsValid = isValid;
	}", true);
		}

		#region Ajax Events

		/// <summary>
		/// Handles the UploadedComplete event of the UploadImageAsyncFileUpload control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="AjaxControlToolkit.AsyncFileUploadEventArgs"/> instance containing the event data.</param>
		public void UploadImageAsyncFileUpload_UploadedComplete(object sender, AsyncFileUploadEventArgs e)
		{
			var bytes = UploadImageAsyncFileUpload.FileBytes;

			if (bytes == null || bytes.Length > (MaxFileSize * 1024) || !IsValidDimensions(bytes))
				App.SetSessionValue(Constants.StateKeys.UploadedImageIsValid, false);
			else
			{
				App.SetSessionValue(Constants.StateKeys.UploadedImageIsValid, true);
				App.SetSessionValue(Constants.StateKeys.UploadedImage, bytes);
			}

			UploadImageAsyncFileUpload.ClearAllFilesFromPersistedStore();

		}

		#endregion

		#region Helper Methods

		/// <summary>
		/// Determines whether the image has valid dimensions.
		/// </summary>
		/// <param name="bytes">The bytes.</param>
		/// <returns>
		/// 	<c>true</c> if the image dimensions are valid; otherwise, <c>false</c>.
		/// </returns>
		private bool IsValidDimensions(byte[] bytes)
		{
			try
			{
				using (var memoryStream = new MemoryStream(bytes))
				{
					using (var img = System.Drawing.Image.FromStream(memoryStream))
					{
						var width = img.Width;
						var height = img.Height;

						return (width <= ImageWidth && height <= ImageHeight);
					}
				}
			}

			catch
			{
				return false;
			}
		}

		#endregion
	}
}
