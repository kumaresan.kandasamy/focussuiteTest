﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageModal.ascx.cs" Inherits="Focus.Web.Code.Controls.User.PageModal" %>

<asp:HiddenField ID="PageModalDummyTarget" runat="server" />
<act:modalpopupextender ID="PageModalPopup" runat="server" ClientIDMode="Static"
												TargetControlID="PageModalDummyTarget" 
												PopupControlID="PageModalPanel" 
												Drag="true" 
												BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
</act:modalpopupextender>

<div id="PageModalPanel" class="modal" style="display: none; height:90%; width:90%;">
	<div class="closeIcon"><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="PageModalCancelScript();return false;" /></div>
	<div style="height:100%; width:99%;">
		<iframe id="PageModalFrame" src="about:blank" frameborder="0" style="height:100%; width:100%;" title="">xx</iframe>
	</div>
</div>

<script type="text/javascript">

	function PageModalShow(url)
	{
		var frame = $get('PageModalFrame');
		frame.src = url;
	}

	function PageModalCancelScript()
	{
		var frame = $get('PageModalFrame');
		frame.src = "about:blank";
		$find('PageModalPopup').hide();
		return false;
	}
		
</script>