﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.UI;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.Employer;
using Framework.Core;

using Focus.Core.Models.Assist;
using Focus.Common;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class OfficeFilter : UserControlBase
	{
		#region Properties

		private bool UserIsManager
		{
			get { return GetViewStateValue<bool>("OfficeFilter:UserIsManager"); }
			set { SetViewStateValue("OfficeFilter:UserIsManager", value); }
		}

		private bool UserStatewideManager
		{
			get { return GetViewStateValue<bool>("OfficeFilter:UserStatewideManager"); }
			set { SetViewStateValue("OfficeFilter:UserStatewideManager", value); }
		}

		private List<long?> UserOfficeIds
		{
			get { return GetViewStateValue<List<long?>>("OfficeFilter:UserOfficeIds"); }
			set { SetViewStateValue("OfficeFilter:UserOfficeIds", value); }
		}

		private bool IsEnabled 
		{
      get { return IsEnabledHiddenField.Value.Equals("true", StringComparison.OrdinalIgnoreCase); }
      set { IsEnabledHiddenField.Value = value.ToString(); }
		}

	  public OfficeFilterModel OfficeFilterModel { get; set; }

	  #endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				IsEnabled = true;
				var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId);
				UserIsManager = userDetails.PersonDetails.Manager.IsNotNull() && (bool) userDetails.PersonDetails.Manager;
				UserStatewideManager = ServiceClientLocator.EmployerClient(App).IsPersonStatewide(Convert.ToInt64(App.User.PersonId));
				UserOfficeIds = ServiceClientLocator.EmployerClient(App).GetOfficesList(new OfficeCriteria {PersonId = App.User.PersonId}).Select(x => x.Id).ToList();

				InitialisePage();
			}

      JavaScriptHelpers();
		  Enable(IsEnabled);
		}

		/// <summary>
		/// Initialises the page.
		/// Use this method to make controls visible/hidden, populate controls etc on initial load
		/// </summary>
		protected void InitialisePage()
		{
			#region Show/hide controls

			AssignmentFilterPlaceHolder.Visible = !UserIsManager;
			StaffMemberFilterPlaceHolder.Visible = UserIsManager;

			#endregion

			#region Populate controls

			BindOfficeGroupDropDown();
			BindOfficesDropdown((OfficeGroup)Enum.Parse(typeof(OfficeGroup), OfficeGroupDropDownList.SelectedValue, true));

			#endregion
		}

    /// <summary>
    /// Registers some variables to be used in JavaScript
    /// </summary>
    private void JavaScriptHelpers()
    {
      ScriptManager.RegisterClientScriptBlock(this, GetType(), "EnableFunctionality",
				@"var OfficeFilter_Variables = new Array();

          function OfficeFilter_Enable(clientId, enable) {
	          var controls = [
	            $('#' + OfficeFilter_Variables[clientId]['OfficeGroupDropDownList']),
	            $('#' + OfficeFilter_Variables[clientId]['AssignmentsDropDownList']),
	            $('#' + OfficeFilter_Variables[clientId]['StaffMembersDropDownList']),
	            $('#' + OfficeFilter_Variables[clientId]['OfficesDropDownList'])
	          ];

	          for (var i = 0; i < 4; i++) {
	            controls[i].prop('disabled', !enable);
							if (!enable){
							controls[i].addClass('selectDisabled');
							} else {
							controls[i].removeClass('selectDisabled');
							}
	            if (i > 0 && !enable)
	              controls[i].prop('selectedIndex', 0);
	          }

            if (enable) {
              if (controls[0].val() == 'StatewideOffices') {
                controls[3].prop('disabled', !OfficeFilter_Variables[clientId]['UserStatewideManager']);
              }
            }

	          $('#' + OfficeFilter_Variables[clientId]['IsEnabledHiddenField']).val(enable.toString().toLowerCase());
	        }", true);

      var variables = new NameValueCollection
		  {
		    {"OfficeGroupDropDownList", OfficeGroupDropDownList.ClientID},
        {"AssignmentsDropDownList", AssignmentsDropDownList.ClientID},
        {"StaffMembersDropDownList", StaffMembersDropDownList.ClientID},
        {"OfficesDropDownList", OfficesDropDownList.ClientID},
        {"IsEnabledHiddenField", IsEnabledHiddenField.ClientID}
		  };

      var variableName = string.Format("OfficeFilter_Variables[\"{0}\"]", ClientID);
      RegisterCodeValuesJson("OfficeFilter_Variables", variableName, variables);
    }

		/// <summary>
		/// Unbinds the filter.
		/// </summary>
		public OfficeFilterModel Unbind()
		{
			return new OfficeFilterModel
				{
					OfficeGroup = OfficeGroupDropDownList.SelectedValue.IsNotNullOrEmpty() ? (OfficeGroup)Enum.Parse(typeof(OfficeGroup), OfficeGroupDropDownList.SelectedValue, true) : (OfficeGroup?)(null),
					OfficeId = OfficesDropDownList.SelectedValue.IsNotNullOrEmpty() ? Convert.ToInt64(OfficesDropDownList.SelectedValue): (long?)null,
					StaffMemberId = UserIsManager && StaffMembersDropDownList.SelectedValue.IsNotNullOrEmpty() ? Convert.ToInt64(StaffMembersDropDownList.SelectedValue) : (long?)(null),
					AssignmentType = !UserIsManager && AssignmentsDropDownList.SelectedValue.IsNotNullOrEmpty()? (AssignmentType)Enum.Parse(typeof(AssignmentType), AssignmentsDropDownList.SelectedValue, true) : (AssignmentType?)(null)
				};
		}

		/// <summary>
		/// Enables the specified enable.
		/// </summary>
		/// <param name="enable">if set to <c>true</c> [enable].</param>
		public void Enable(bool enable)
		{
			OfficeGroupDropDownList.Enabled = 
        AssignmentsDropDownList.Enabled = 
        StaffMembersDropDownList.Enabled =
        OfficesDropDownList.Enabled = enable;

		  if (enable)
		  {
        var officeGroup = (OfficeGroup) Enum.Parse(typeof (OfficeGroup), OfficeGroupDropDownList.SelectedValue, true);

		    if (officeGroup == OfficeGroup.StatewideOffices)
		      OfficesDropDownList.Enabled = UserStatewideManager;
		  }

		  IsEnabled = enable;

			if (!enable)
			{
				if (AssignmentsDropDownList.Visible)
					AssignmentsDropDownList.SelectedIndex = 0;

				if (StaffMembersDropDownList.Visible)
					StaffMembersDropDownList.SelectedIndex = 0;

				if (OfficesDropDownList.Visible)
					OfficesDropDownList.SelectedIndex = 0;
			}
		}

		/// <summary>
		/// Returns whether the office filter is enabled
		/// </summary>
		/// <returns></returns>
		public bool FilterEnabled()
		{
			return IsEnabled;
		}

		/// <summary>
		/// Handles the SelectedValueChanged event of the OfficeGroupDropDownList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void OfficeGroupDropDownList_SelectedIndexChanged(object sender, EventArgs e)
		{
			var officeGroup = (OfficeGroup) Enum.Parse(typeof (OfficeGroup), OfficeGroupDropDownList.SelectedValue, true);

			if (officeGroup == OfficeGroup.StatewideOffices)
			{
				OfficesDropDownList.Enabled = UserStatewideManager;
				BindStaffMembersDropDown();
			}
			else
			{
				OfficesDropDownList.Enabled = true;
				AssignmentsDropDownList.Enabled = true;
				BindStaffMembersDropDown(UserOfficeIds);
			}

			BindOfficesDropdown((OfficeGroup)Enum.Parse(typeof(OfficeGroup), OfficeGroupDropDownList.SelectedValue, true));

			#region Clear other drop downs

			OfficesDropDownList.SelectedValue = "";
		  AssignmentsDropDownList.SelectedValue = AssignmentType.AllAssignments.ToString();
			StaffMembersDropDownList.SelectedValue = "";

			#endregion
		}

    /// <summary>
    /// Handles the SelectedValueChanged event of the OfficeDropDownList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void OfficeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
      // This is here because the selectedIndexChanged event wasn't firing when first changed.  This is a total hack - blame Jim!
      if (OfficesDropDownList.SelectedValue.IsNotNullOrEmpty())
        BindStaffMembersDropDown(new List<long?> { OfficesDropDownList.SelectedValueToLong() });
      else
        BindStaffMembersDropDown();
    }

		/// <summary>
		/// Binds the office group drop down.
		/// </summary>
		private void BindOfficeGroupDropDown()
		{
			OfficeGroupDropDownList.Items.Clear();

			OfficeGroupDropDownList.Items.AddEnum(OfficeGroup.MyOffices, CodeLocalise("MyOffices.Text.NoEdit", "My offices"));
			OfficeGroupDropDownList.Items.AddEnum(OfficeGroup.StatewideOffices, CodeLocalise("OfficesStatewide.Text.NoEdit", "Offices statewide"));

		  if (OfficeFilterModel != null && OfficeFilterModel.OfficeGroup.HasValue)
		  {
		    OfficeGroupDropDownList.SelectedValue = OfficeFilterModel.OfficeGroup.ToString();
		  }
		  else
		  {
        OfficeGroupDropDownList.SelectedValue = OfficeGroup.MyOffices.ToString();
		  }
		  
			if (UserIsManager)
				BindStaffMembersDropDown(UserOfficeIds);
			else
				BindAssignmentsDropDown();
		}

		/// <summary>
		/// Binds the staff members drop down.
		/// </summary>
		private void BindStaffMembersDropDown(List<long?> officeIds = null)
		{
		  var originalStaffMember = StaffMembersDropDownList.SelectedValue;

			StaffMembersDropDownList.Items.Clear();
      StaffMembersDropDownList.SelectedValue = null;

			var staffMembers = ServiceClientLocator.EmployerClient(App).GetOfficeStaffMembersList(new StaffMemberCriteria
			{
				OfficeIds = officeIds
			}).Select(x => new { x.Id, DisplayText = x.FirstName + " " + x.LastName });

			StaffMembersDropDownList.DataSource = staffMembers;
			StaffMembersDropDownList.DataValueField = "Id";
			StaffMembersDropDownList.DataTextField = "DisplayText";
			StaffMembersDropDownList.DataBind();
			
			StaffMembersDropDownList.Items.AddLocalisedTopDefault("StaffMembersDropDownList.TopDefault.NoEdit", "- select -");

		  StaffMembersDropDownList.SelectValue(originalStaffMember);
		}

		/// <summary>
		/// Binds the assignments drop down.
		/// </summary>
		private void BindAssignmentsDropDown()
		{
			AssignmentsDropDownList.Items.Clear();

			AssignmentsDropDownList.Items.AddEnum(AssignmentType.AllAssignments, CodeLocalise("AllAssignments.Text.NoEdit", "All assignments"));
			AssignmentsDropDownList.Items.AddEnum(AssignmentType.MyAssignments, CodeLocalise("MyAssignments.Text.NoEdit", "My assignments"));

		  if (OfficeFilterModel != null && OfficeFilterModel.AssignmentType.HasValue)
		  {
        AssignmentsDropDownList.SelectedValue = OfficeFilterModel.AssignmentType.ToString();
		  }
		  else
		  {
        AssignmentsDropDownList.SelectedValue = AssignmentType.AllAssignments.ToString();
		  }			
		}

		/// <summary>
		/// Binds the offices dropdown.
		/// </summary>
		private void BindOfficesDropdown(OfficeGroup officeGroup)
		{
			OfficesDropDownList.Items.Clear();

			var offices = ServiceClientLocator.EmployerClient(App).GetOfficesList(new OfficeCriteria { OfficeGroup = officeGroup });
			OfficesDropDownList.DataSource = offices;
			OfficesDropDownList.DataValueField = "Id";
			OfficesDropDownList.DataTextField = "OfficeName";
			OfficesDropDownList.DataBind();

			OfficesDropDownList.Items.AddLocalisedTopDefault("Offices.Select.NoEdit", "- select -");

      if (OfficeFilterModel != null && OfficeFilterModel.OfficeId.HasValue)
      {
        OfficesDropDownList.SelectedValue = OfficeFilterModel.OfficeId.ToString();
      }
		}
	}
}