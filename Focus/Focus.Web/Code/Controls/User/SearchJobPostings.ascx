﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchJobPostings.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SearchJobPostings" %>
<%@ Register Src="~/Code/Controls/User/SearchCriteriaWorkDays.ascx" TagName="SearchCriteriaWorkDays" TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/SearchCriteriaLanguage.ascx" TagName="SearchCriteriaLanguage" TagPrefix="uc" %>

<%@ Register TagPrefix="ajaxtoolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.50731.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<tr>
	<td colspan="3">
		<table style="width: 100%;" class="savedSearchTop" role="presentation">
			<tr>
				<td>&nbsp;
				</td>
				<td style="width: 100%;">
					<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
						<Triggers>
							<asp:AsyncPostBackTrigger ControlID="btnSearchJobPostings" EventName="Click" />
						</Triggers>
						<ContentTemplate>
							<div style="text-align: left">
								<h1>
									<asp:Label ID="SearchJobPostingsLabel" runat="server" /></h1>
							</div>
							<div style="text-align: right;">
								<asp:Button ID="btnSearchJobPostings" Text="Go &#187;" runat="server" class="button3" OnClick="btnSearchJobPostings_Click" ValidationGroup="Location" />
							</div>

							<table style="width: 100%;" role="presentation">
								<%--SEARCH CRITERIA--%>
								<tr>
									<td style="width: 635px; vertical-align: top;">
										<table role="presentation">
											<tr>
												<td style="width: 300px;">
													<%= HtmlInFieldLabel("SearchTermTextBox", "SearchTermTextBox.Label", "search for these words", 168)%>
													<asp:TextBox ID="SearchTermTextBox" runat="server" ClientIDMode="Static" Width="278px" />
												</td>
												<td style="width: 34px;">
													<span class="boldedText">
														<%= HtmlLabel(ddlSearchLocation,"in.Label", "in")%>
													</span>
												</td>
												<td>
													<asp:DropDownList ID="ddlSearchLocation" runat="server" Width="150px">
														<asp:ListItem>complete job posting</asp:ListItem>
													</asp:DropDownList>
												</td>
												<td style="vertical-align: top; padding: 5px 0 0 15px;">
													<div class="tooltip">
														<div class="tooltipWithArrow helpMessage">
															<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("ToolTip1.Label", "<b>Search tips</b><br/>By default, we will search for all words or phrases you enter in the search box.<br/>• To search for an exact phrase, enclose it in quotation marks.<br/>• Use NOT or a minus sign (-) to exclude a term from your search.<br/>• Use OR to search for one of any set of words or phrases you specify.")%>"></div>
														</div>
													</div>
												</td>
											</tr>
										</table>
									</td>
									<td style="width: 12px;"></td>
									<td style="width: 635px; vertical-align: top;">
										<table role="presentation">
											<tr>
												<td style="width: 200px;">
													<asp:RadioButton ID="rbArea" Text="Search within this area" runat="server" ClientIDMode="Static"
														GroupName="Search" onclick="Hide_Radius_StateMSA(this)" />
												</td>
												<td style="width: 150px;">
													<focus:LocalisedLabel runat="server" ID="ddlRadiusLabel" AssociatedControlID="ddlRadius" LocalisationKey="Radius" DefaultText="Radius" CssClass="sr-only" />
													<asp:DropDownList ID="ddlRadius" runat="server" Width="150px" ClientIDMode="Static" onchange="CheckRadius();"
														CssClass="RadiusClass">
													</asp:DropDownList>
												</td>
												<td style="width: 25px;">&nbsp;of
												</td>
												<td>
													<%= HtmlInFieldLabel("txtZipCode", "ZipCodeBox.Label", "ZIP Code", 128)%>
													<asp:TextBox ID="txtZipCode" runat="server" ClientIDMode="Static" Width="128px" MaxLength="9" onblur="CheckRadius();" />
													<ajaxtoolkit:FilteredTextBoxExtender ID="ftbeZip" runat="server" TargetControlID="txtZipCode" FilterType="Numbers" />
												</td>
											</tr>

											<tr>
												<td colspan="4">
													<asp:CustomValidator ID="RadiusValidator" runat="server" SetFocusOnError="true" Display="Dynamic"
														CssClass="error" ValidationGroup="Location" ClientValidationFunction="ValidateRadius"
														ValidateEmptyText="true" />
												</td>
											</tr>
											<tr>
												<td>
													<asp:CheckBox ID="cbJobsInMyState" runat="server" CssClass="checkBoxList" ClientIDMode="Static" />
												</td>
												<td colspan="3" style="vertical-align: top; padding-top: 5px;">
													<div class="tooltip">
														<div class="tooltipWithArrow helpMessage">
															<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("ToolTip3.Label", "If your search area is near a state line, the radius criteria may deliver results for jobs in your neighboring state(s).  Check this box to limit search results only to jobs in your state. <br/> Note:  If you opt to limit your search to in-state jobs, make sure that the ZIP code and radius you select includes areas within the state.  Otherwise, you will receive no matches.")%>"></div>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="4" style="height: 7px;"></td>
											</tr>
											<tr>
												<td style="vertical-align: top">
													<asp:RadioButton ID="rbMSAState" ClientIDMode="Static" Text="Search this state/city"
														runat="server" GroupName="Search" onclick="Hide_Radius_StateMSA(this)" />
												</td>
												<td colspan="3" style="vertical-align: top;">
													<focus:LocalisedLabel runat="server" ID="ddlStateLabel" AssociatedControlID="ddlState" LocalisationKey="State" DefaultText="State" CssClass="sr-only" />
													<asp:DropDownList ID="ddlState" runat="server" Width="320px" ClientIDMode="Static" onchange="CheckMSA();"
														CssClass="StateClass">
														<asp:ListItem Value="">- select a state -</asp:ListItem>
													</asp:DropDownList>
													<div style="height: 10px;">
													</div>
													<focus:LocalisedLabel runat="server" ID="ddlMSALabel" AssociatedControlID="ddlMSA" LocalisationKey="MSA" DefaultText="MSA" CssClass="sr-only" />
													<focus:AjaxDropDownList ID="ddlMSA" runat="server" Width="320px" onchange="CheckMSA();"
														ClientIDMode="Static" TabIndex="10" Enabled="False" />
													<ajaxtoolkit:CascadingDropDown ID="ccdMSA" ClientIDMode="Static" runat="server" Category="MSA" LoadingText="[Loading City...]"
														ParentControlID="ddlState" ServiceMethod="GetMSA" ServicePath="~/Services/AjaxService.svc"
														BehaviorID="ccdMSABehaviorID" TargetControlID="ddlMSA">
													</ajaxtoolkit:CascadingDropDown>
												</td>
											</tr>
											<tr>
												<td colspan="4" style="height: 7px;"></td>
											</tr>

											<%--											<asp:PlaceHolder runat="server" ID="HomeBasedJobPostingsPlaceHolder">
												<tr>
													<td colspan="4">
														<asp:RadioButton ID="HomeBasedJobPostingsRadioButton" ClientIDMode="Static" Text="Test" runat="server" GroupName="Search" onclick="Hide_Radius_StateMSA(this)" />
														<div class="inlineTooltip">
															<div class="tooltipWithArrow helpMessage">
																<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("HomeBasedJobPostingsToolTip.Label", "Selecting this option means only jobs posted directly from this website that have been highlighted as suitable for home-based workers will be displayed in your search results. Spidered jobs from other #BUSINESSES#:LOWER will be excluded.")%>"></div>
															</div>
														</div>
													</td>
												</tr>
											</asp:PlaceHolder>--%>
										</table>
								</tr>
								<tr>
									<td colspan="3">
										<div class="horizontalRule" style="margin-bottom: 15px;"></div>
									</td>
								</tr>
								<tr>
									<td style="vertical-align: top; width: 635px;">
										<div class="singleAccordionContentWrapper">
											<asp:Panel ID="JobMatchingHeaderPanel" runat="server" CssClass="singleAccordionTitle singleAccordionStyle2">
												<table role="presentation">
													<tr>
														<td style="width: 30px;">
															<span>
																<asp:Image ID="JobMatchingHeaderImage" runat="server" AlternateText="." Height="22" Width="22" /></span>
														</td>
														<td class="accordionLabel">
															<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("AccordionHeading2.Label", "JOB MATCHING")%></a></span>
														</td>
														<td>
															<div class="tooltip">
																<div class="tooltipWithArrow helpMessage">
																	<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("ToolTip2.Label", "Select the star match (1-5) to find jobs based on how closely this resume matches. A 5-star match will find only jobs to which you are most qualified. Other star match selections find all jobs with that score or higher, but not those with lower-score matches. If you wish to see all available jobs without considering this resume, select the second search option.")%>"></div>
																</div>
															</div>
														</td>
													</tr>
												</table>
											</asp:Panel>
											<asp:Panel ID="JobMatchingContentPanel" runat="server" CssClass="singleAccordionContent singleAccordionStyle2">
												<table role="presentation">
													<tr class="first">
														<td>
															<asp:RadioButton ID="rbStarMatch" Text="Jobs that are at least a" runat="server"
																GroupName="Match" onclick="EnableDisableExcludeJob();"
																ClientIDMode="Static" />
															<asp:DropDownList ID="ddlStarMatch" runat="server" ClientIDMode="Static" Width="140">
															</asp:DropDownList>
															<%= HtmlLabel(ddlStarMatch,"starmatch.Label", "for this resume")%>
														</td>
													</tr>
													<tr class="last">
														<td>
															<focus:LocalisedLabel runat="server" ID="rbWithoutMatchLabel" AssociatedControlID="rbWithoutMatch" LocalisationKey="rbWithoutMatch.Label" DefaultText="Without match radio button" CssClass="sr-only" />
															<asp:RadioButton ID="rbWithoutMatch" Text="All jobs, without matching this resume"
																Checked="true" onclick="ResetStar();EnableDisableExcludeJob();" runat="server"
																GroupName="Match" ClientIDMode="Static" />
														</td>
													</tr>
												</table>
											</asp:Panel>
										</div>
										<act:CollapsiblePanelExtender ID="JobMatchingPanelExtender" runat="server" TargetControlID="JobMatchingContentPanel"
											ExpandControlID="JobMatchingHeaderPanel" CollapseControlID="JobMatchingHeaderPanel"
											Collapsed="True" ImageControlID="JobMatchingHeaderImage" SuppressPostBack="true" />

										<div class="singleAccordionContentWrapper">
											<asp:Panel ID="PostingDateHeaderPanel" runat="server" CssClass="singleAccordionTitle singleAccordionStyle2">
												<table role="presentation">
													<tr>
														<td style="width: 30px;">
															<span>
																<asp:Image ID="PostingDateHeaderImage" runat="server" AlternateText="." Height="22" Width="22" /></span>
														</td>
														<td class="accordionLabel">
															<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("AccordionHeading2.Label", "POSTING DATE")%></a></span>
														</td>
														<td>
															<div class="tooltip">
																<div class="tooltipWithArrow helpMessage">
																	<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("ToolTip2.Label", "Select a job-posting date to find jobs based on when a #BUSINESS#:LOWER posted the job. To see all jobs, select 'More than 30 days.' To narrow your search results, decrease the time frame for the posting date.")%>"></div>
																</div>
															</div>
														</td>
													</tr>
												</table>
											</asp:Panel>
											<asp:Panel ID="PostingDateContentPanel" runat="server" CssClass="singleAccordionContent singleAccordionStyle2">
												<table role="presentation">
													<tr class="first last">
														<td style="width: 135px;">Display jobs added
														</td>
														<td>
															<focus:LocalisedLabel runat="server" ID="ddlPostingAgeLabel" AssociatedControlID="ddlPostingAge" LocalisationKey="PostingAge" DefaultText="Posting age" CssClass="sr-only" />
															<asp:DropDownList ID="ddlPostingAge" runat="server" Width="168">
															</asp:DropDownList>
														</td>
													</tr>
												</table>
											</asp:Panel>
										</div>
										<act:CollapsiblePanelExtender ID="PostingDatePanelExtender" runat="server" TargetControlID="PostingDateContentPanel"
											ExpandControlID="PostingDateHeaderPanel" CollapseControlID="PostingDateHeaderPanel"
											Collapsed="True" ImageControlID="PostingDateHeaderImage" SuppressPostBack="true" />

										<div class="singleAccordionContentWrapper">
											<asp:Panel ID="EducationLevelHeaderPanel" runat="server" CssClass="singleAccordionTitle singleAccordionStyle2">
												<table role="presentation">
													<tr>
														<td style="width: 30px;">
															<span>
																<asp:Image ID="EducationLevelHeaderImage" runat="server" AlternateText="." Height="22" Width="22" /></span>
														</td>
														<td class="accordionLabel">
															<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("AccordionHeading3.Label", "EDUCATION LEVEL")%></a></span>
														</td>
														<td>
															<div class="tooltip">
																<div class="tooltipWithArrow helpMessage">
																	<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("ToolTip3.Label", "To find jobs with a specific education level, select the appropriate check box to display only openings for that level.  Remember that some #BUSINESSES#:LOWER may not provide an educational level requirement. The last check box will include those jobs in your search results.")%>"></div>
																</div>
															</div>
														</td>
													</tr>
												</table>
											</asp:Panel>
											<asp:Panel ID="EducationLevelContentPanel" runat="server" CssClass="singleAccordionContent singleAccordionStyle2">
												<table role='presentation'>
													<tr class="first">
														<td>
															<asp:CheckBoxList ID="cblEducationRequired" runat="server" class="checkBoxList" role="presentation">
															</asp:CheckBoxList>
														</td>
													</tr>
													<tr class="last">
														<td colspan="3">
															<asp:CheckBox ID="cbWithoutEducationInfo" runat="server" CssClass="checkBoxList"
																Text="Also show jobs without education information" Checked="true" />
														</td>
													</tr>
												</table>
											</asp:Panel>
										</div>
										<act:CollapsiblePanelExtender ID="EducationLevelPanelExtender" runat="server" TargetControlID="EducationLevelContentPanel"
											ExpandControlID="EducationLevelHeaderPanel" CollapseControlID="EducationLevelHeaderPanel"
											Collapsed="True" ImageControlID="EducationLevelHeaderImage" SuppressPostBack="true" />

										<div class="singleAccordionContentWrapper">
											<asp:Panel ID="SalaryLevelHeaderPanel" runat="server" CssClass="singleAccordionTitle singleAccordionStyle2">
												<table role="presentation">
													<tr>
														<td style="width: 30px;">
															<span>
																<asp:Image ID="SalaryLevelHeaderImage" runat="server" AlternateText="." Height="22" Width="22" /></span>
														</td>
														<td class="accordionLabel">
															<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("AccordionHeading4.Label", "SALARY LEVEL")%></a></span>
														</td>
														<td>
															<div class="tooltip">
																<div class="tooltipWithArrow helpMessage">
																	<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("ToolTip4.Label", "To find jobs with at a specific salary level, enter the salary minimum you hope to find. Keep in mind that many #BUSINESSES#:LOWER prefer to negotiate salaries and do not provide this information in their job descriptions. The last check box will include jobs in your search results that do not list salary information.")%>"></div>
																</div>
															</div>
														</td>
													</tr>
												</table>
											</asp:Panel>
											<asp:Panel ID="SalaryLevelContentPanel" runat="server" CssClass="singleAccordionContent singleAccordionStyle2">
												<table role="presentation">
													<tr class="first">
														<td style="width: 14px;">$</td>
														<td style="width: 140px;">
															<%= HtmlInFieldLabel("minimumSalaryTextBox", "minimumSalaryTextBox.Label", "minimum", 128)%>
															<asp:TextBox ID="minimumSalaryTextBox" runat="server" ClientIDMode="Static" Width="128px"
																MaxLength="10" onBlur="this.value=formatCurrency(this.value);" />
															<ajaxtoolkit:FilteredTextBoxExtender ID="fteWages" runat="server" TargetControlID="minimumSalaryTextBox"
																FilterType="Custom" ValidChars=",.0123456789" />
														</td>
														<td>
															<%= HtmlLocalise("PerYear.Label", "per year")%>
														</td>
													</tr>
													<tr class="last">
														<td colspan="3">
															<asp:CheckBox ID="cbWithoutSalaryInfo" runat="server" CssClass="checkBoxList" Text="Also show jobs without salary information"
																Checked="true" />
														</td>
													</tr>
												</table>
											</asp:Panel>
										</div>
										<act:CollapsiblePanelExtender ID="SalaryLevelPanelExtender" runat="server" TargetControlID="SalaryLevelContentPanel"
											ExpandControlID="SalaryLevelHeaderPanel" CollapseControlID="SalaryLevelHeaderPanel"
											Collapsed="True" ImageControlID="SalaryLevelHeaderImage" SuppressPostBack="true" />

										<div class="singleAccordionContentWrapper">
											<asp:Panel ID="InternshipsHeaderPanel" runat="server" CssClass="singleAccordionTitle singleAccordionStyle2">
												<table role="presentation">
													<tr>
														<td style="width: 30px;">
															<span>
																<asp:Image ID="InternshipsHeaderImage" runat="server" AlternateText="." Height="22" Width="22" /></span>
														</td>
														<td class="accordionLabel">
															<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("AccordionHeading5.Label", "INTERNSHIPS")%></a></span>
														</td>
														<td>
															<div class="tooltip">
																<div class="tooltipWithArrow helpMessage">
																	<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("ToolTip5.Label", "The availability of paid and unpaid internships varies from one area to another. You can select whether or not to include or exclude these jobs.")%>"></div>
																</div>
															</div>
														</td>
													</tr>
												</table>
											</asp:Panel>
											<asp:Panel ID="InternshipsContentPanel" runat="server" CssClass="singleAccordionContent singleAccordionStyle2">
												<table role="presentation">
													<tr class="first last">
														<td style="width: 135px;">Display these jobs</td>
														<td>
															<focus:LocalisedLabel runat="server" ID="ddlInternshipRequiredLabel" AssociatedControlID="ddlInternshipRequired" LocalisationKey="InternshipRequired" DefaultText="Internship required" CssClass="sr-only" />
															<asp:DropDownList ID="ddlInternshipRequired" runat="server" Width="320">
																<asp:ListItem>all employment opportunities</asp:ListItem>
															</asp:DropDownList>
														</td>
													</tr>
												</table>
											</asp:Panel>
										</div>
										<act:CollapsiblePanelExtender ID="InternshipsPanelExtender" runat="server" TargetControlID="InternshipsContentPanel"
											ExpandControlID="InternshipsHeaderPanel" CollapseControlID="InternshipsHeaderPanel"
											Collapsed="True" ImageControlID="InternshipsHeaderImage" SuppressPostBack="true" />
										
										<div class="singleAccordionContentWrapper">
											<asp:Panel ID="LanguagesHeaderPanel" runat="server" CssClass="singleAccordionTitle singleAccordionStyle2">
												<table role="presentation">
													<tr>
														<td style="width: 30px;">
															<span>
																<asp:Image ID="LanguagesHeaderImage" runat="server" AlternateText="." Height="22" Width="22" /></span>
														</td>
														<td class="accordionLabel">
															<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("AccordionHeading9.Label", "LANGUAGES")%></a></span>
														</td>
														<td>
															<div class="tooltip">
																<div class="tooltipWithArrow helpMessage">
																	<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("ToolTip9.Label", "Add a language and proficiency level to find jobs requiring language skills. When adding multiple languages, you can choose whether you want the search to return jobs containing all or any of your selected languages.")%>"></div>
																</div>
															</div>
														</td>
													</tr>
												</table>
											</asp:Panel>
											<asp:Panel ID="LanguagesContentPanel" runat="server" CssClass="singleAccordionContent singleAccordionStyle2">
												<table role="presentation" >
													<tr class="first last">
														<td>
															<uc:SearchCriteriaLanguage ID="searchCriteriaLanguage" InstructionalText="Display jobs with these selected language and proficiency levels."
																LanguageProficiencyDropdownWidth="300" LanguageTextboxRightMargin="20px" ProficiencyLevelAndBelow="True" ReverseProficiencyOrder="true" runat="server" />
														</td>
													</tr>
												</table>
											</asp:Panel>
										</div>
										<act:CollapsiblePanelExtender ID="LanguagesPanelExtender" runat="server" TargetControlID="LanguagesContentPanel"
											ExpandControlID="LanguagesHeaderPanel" CollapseControlID="LanguagesHeaderPanel"
											Collapsed="True" ImageControlID="LanguagesHeaderImage" SuppressPostBack="true" />

									</td>
									<td style="width: 12px;"></td>
									<td style="width: 635px; vertical-align: top;">

										<uc:SearchCriteriaWorkDays ID="SearchCriteriaWorkDays" runat="server" />

										<div class="singleAccordionContentWrapper">
											<asp:Panel ID="OccupationsAndIndustryHeaderPanel" runat="server" CssClass="singleAccordionTitle singleAccordionStyle2">
												<table role="presentation">
													<tr>
														<td style="width: 30px;">
															<span>
																<asp:Image ID="OccupationsAndIndustryHeaderImage" runat="server" AlternateText="." Height="22" Width="22" /></span>
														</td>
														<td class="accordionLabel">
															<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("AccordionHeading6.Label", "OCCUPATION AND INDUSTRY")%></a></span>
														</td>
														<td>
															<div class="tooltip">
																<div class="tooltipWithArrow helpMessage">
																	<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("ToolTip6.Label", "Select an occupation and/or industry from the menus. An occupation describes a specific type of job; an industry describes what a #BUSINESS#:LOWER does overall.")%>"></div>
																</div>
															</div>
														</td>
													</tr>
												</table>
											</asp:Panel>
											<asp:Panel ID="OccupationsAndIndustryContentPanel" runat="server" CssClass="singleAccordionContent singleAccordionStyle2">
												<table role="presentation">
													<tr class="first">
														<td colspan="2"><%= HtmlLocalise("LookingFor.Label", "What are you looking for?")%></td>
													</tr>
													<tr>
														<td style="width: 80px;"><%= HtmlLabel(ddlJobFamily, "Occupation", "Occupation")%></td>
														<td>
															<asp:DropDownList ID="ddlJobFamily" runat="server" Width="370px" ClientIDMode="Static">
																<asp:ListItem>- select job family -</asp:ListItem>
															</asp:DropDownList>
															<br />
															<br />
															<focus:LocalisedLabel runat="server" ID="ddlOccupationLabel" AssociatedControlID="ddlOccupation" LocalisationKey="Occupation" DefaultText="Occupation" CssClass="sr-only" />
															<focus:AjaxDropDownList ID="ddlOccupation" runat="server" Width="370px" ClientIDMode="Static" TabIndex="8" />
															<ajaxtoolkit:CascadingDropDown ID="ccdOccupation" runat="server" Category="Occupation"
																LoadingText="[Loading occupation...]" ParentControlID="ddlJobFamily" ServiceMethod="GetJobsByCareerArea"
																ServicePath="~/Services/AjaxService.svc" BehaviorID="ccdOccupationBehaviorID" TargetControlID="ddlOccupation">
															</ajaxtoolkit:CascadingDropDown>
														</td>
													</tr>
													<tr>
														<td colspan="2" style="height: 10px;"></td>
													</tr>
													<tr class="last">
														<td style="width: 80px; vertical-align: top;">Industry</td>
														<td>
															<focus:LocalisedLabel runat="server" ID="ddlIndustryLabel" AssociatedControlID="ddlIndustry" LocalisationKey="Industry.Label" DefaultText="Industry" CssClass="sr-only" />
															<asp:DropDownList ID="ddlIndustry" runat="server" Width="370px" ClientIDMode="Static">
																<asp:ListItem>- select industry -</asp:ListItem>
															</asp:DropDownList>
															<br />
															<br />
															<focus:LocalisedLabel runat="server" ID="ddlIndustryDetailLabel" AssociatedControlID="ddlIndustryDetail" LocalisationKey="ddlIndustryDetail.Label" DefaultText="Industry detail" CssClass="sr-only" />
															<focus:AjaxDropDownList ID="ddlIndustryDetail" runat="server" Width="370px" ClientIDMode="Static" TabIndex="10" />
															<ajaxtoolkit:CascadingDropDown ID="ccdIndustryDetail" runat="server" Category="Industry"
																LoadingText="[Loading industry details...]" ParentControlID="ddlIndustry" ServiceMethod="GetIndustryDetail"
																ServicePath="~/Services/AjaxService.svc" BehaviorID="ccdIndustryBehaviorID" TargetControlID="ddlIndustryDetail">
															</ajaxtoolkit:CascadingDropDown>
														</td>
													</tr>
												</table>
											</asp:Panel>
										</div>
										<act:CollapsiblePanelExtender ID="OccupationsAndIndustryPanelExtender" runat="server" TargetControlID="OccupationsAndIndustryContentPanel"
											ExpandControlID="OccupationsAndIndustryHeaderPanel" CollapseControlID="OccupationsAndIndustryHeaderPanel"
											Collapsed="True" ImageControlID="OccupationsAndIndustryHeaderImage" SuppressPostBack="true" />

										<div class="singleAccordionContentWrapper">
											<asp:Panel ID="EmergingSectorsHeaderPanel" runat="server" CssClass="singleAccordionTitle singleAccordionStyle2">
												<table role="presentation">
													<tr>
														<td style="width: 30px;">
															<span>
																<asp:Image ID="EmergingSectorsHeaderImage" runat="server" AlternateText="." Height="22" Width="22" /></span>
														</td>
														<td class="accordionLabel">
															<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("AccordionHeading7.Label", "EMERGING/HIGH-GROWTH SECTORS")%></a></span>
														</td>
														<td>
															<div class="tooltip">
																<div class="tooltipWithArrow helpMessage">
																	<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("ToolTip7.Label", "Select an emerging may be high-growth sector. These are industries that are hiring and expanding rapidly.")%>"></div>
																</div>
															</div>
														</td>
													</tr>
												</table>
											</asp:Panel>
											<asp:Panel ID="EmergingSectorsContentPanel" runat="server" CssClass="singleAccordionContent singleAccordionStyle2">
												<table role="presentation">
													<tr class="first last">
														<td>
															<focus:ToolTipCheckBoxList ID="cblEmergingSectors" runat="server" class="checkBoxList" RepeatColumns="2"
																Width="100%" role="presentation">
															</focus:ToolTipCheckBoxList>
														</td>
													</tr>
												</table>
											</asp:Panel>
										</div>
										<act:CollapsiblePanelExtender ID="EmergingSectorsPanelExtender" runat="server" TargetControlID="EmergingSectorsContentPanel"
											ExpandControlID="EmergingSectorsHeaderPanel" CollapseControlID="EmergingSectorsHeaderPanel"
											Collapsed="True" ImageControlID="EmergingSectorsHeaderImage" SuppressPostBack="true" />

										<div class="singleAccordionContentWrapper">
											<asp:Panel ID="PhysicalAbilitiesHeaderPanel" runat="server" CssClass="singleAccordionTitle singleAccordionStyle2">
												<table role="presentation">
													<tr>
														<td style="width: 30px;">
															<span>
																<asp:Image ID="PhysicalAbilitiesHeaderImage" runat="server" AlternateText="." Height="22" Width="22" /></span>
														</td>
														<td class="accordionLabel">
															<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("AccordionHeading8.Label", "#CANDIDATETYPE#:UPPER ABILITIES")%></a></span>
														</td>
														<td>
															<div class="tooltip">
																<div class="tooltipWithArrow helpMessage">
																	<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("ToolTip8.Label", "If you have issues that affect your ability or preference in doing a job, use this filter to omit jobs that may present challenging requirements from your search results.")%>"></div>
																</div>
															</div>
														</td>
													</tr>
												</table>
											</asp:Panel>
											<asp:Panel ID="PhysicalAbilitiesContentPanel" runat="server" CssClass="singleAccordionContent singleAccordionStyle2">
												<table role="presentation">
													<tr class="first">
														<td>
															<%= HtmlLocalise("PhysicalAbilities.Label", "Do not show jobs that typically involve the following abilities:")%>
														</td>
													</tr>
													<tr class="last">
														<td>
															<asp:CheckBoxList ID="cblDisabilityCategories" runat="server" class="checkBoxList" role="presentation"
																RepeatColumns="2" Width="100%">
															</asp:CheckBoxList>
														</td>
													</tr>
												</table>
											</asp:Panel>
										</div>
										<act:CollapsiblePanelExtender ID="PhysicalAbilitiesPanelExtender" runat="server" TargetControlID="PhysicalAbilitiesContentPanel"
											ExpandControlID="PhysicalAbilitiesHeaderPanel" CollapseControlID="PhysicalAbilitiesHeaderPanel"
											Collapsed="True" ImageControlID="PhysicalAbilitiesHeaderImage" SuppressPostBack="true" />

										<div class="singleAccordionContentWrapper">
											<asp:Panel ID="JobTypesHeaderPanel" runat="server" CssClass="singleAccordionTitle singleAccordionStyle2">
												<table role="presentation">
													<tr>
														<td style="width: 30px;">
															<span>
																<asp:Image ID="JobTypesHeaderImage" runat="server" AlternateText="." Height="22" Width="22" /></span>
														</td>
														<td class="accordionLabel">
															<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("AccordionHeading12.Label", "TARGET/EXCLUDE BY JOB TYPE")%></a></span>
														</td>
														<td>
															<div class="tooltip">
																<div class="tooltipWithArrow helpMessage">
																	<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("ToolTip5.Label", "The availability of different type of jobs varies from one area to another. You can select whether or not to include or exclude these jobs.")%>"></div>
																</div>
															</div>
														</td>
													</tr>
												</table>
											</asp:Panel>
											<asp:Panel ID="JobTypesContentPanel" runat="server" CssClass="singleAccordionContent singleAccordionStyle2">
												<table role="presentation">
													<tr class="first last">
														<td style="width: 135px;">Commission-only jobs</td>
														<td>
															<focus:LocalisedLabel runat="server" ID="ddlCommissionBasedJobsRequiredLabel" AssociatedControlID="ddlCommissionBasedJobsRequired" LocalisationKey="CommissionBasedJobsRequired" DefaultText="Commission Based Jobs required" CssClass="sr-only" />
															<asp:DropDownList ID="ddlCommissionBasedJobsRequired" runat="server" Width="320">
																<asp:ListItem>all employment opportunities</asp:ListItem>
															</asp:DropDownList>
														</td>
													</tr>
												</table>
												<table role="presentation">
													<tr class="first last">
														<td style="width: 135px;">Commission + salary jobs</td>
														<td>
															<focus:LocalisedLabel runat="server" ID="ddlSalaryAndCommissionBasedJobsRequiredLabel" AssociatedControlID="ddlSalaryAndCommissionBasedJobsRequired" LocalisationKey="SalaryAndCommissionBasedJobsRequired" DefaultText="Salary And Commission Based Jobs required" CssClass="sr-only" />
															<asp:DropDownList ID="ddlSalaryAndCommissionBasedJobsRequired" runat="server" Width="320">
																<asp:ListItem>all employment opportunities</asp:ListItem>
															</asp:DropDownList>
														</td>
													</tr>
												</table>
												<table role="presentation">
													<tr class="first last">
														<td style="width: 135px;">Home-based jobs</td>
														<td>
															<focus:LocalisedLabel runat="server" ID="ddlHomeBasedJobsRequiredLabel" AssociatedControlID="ddlHomeBasedJobsRequired" LocalisationKey="HomeBasedJobsRequired" DefaultText="Home Based Jobs required" CssClass="sr-only" />
															<asp:DropDownList ID="ddlHomeBasedJobsRequired" runat="server" Width="320">
																<asp:ListItem>all employment opportunities</asp:ListItem>
															</asp:DropDownList>
														</td>
													</tr>
												</table>
											</asp:Panel>
										</div>
										<act:CollapsiblePanelExtender ID="JobTypesPanelExtender" runat="server" TargetControlID="JobTypesContentPanel"
											ExpandControlID="JobTypesHeaderPanel" CollapseControlID="JobTypesHeaderPanel"
											Collapsed="True" ImageControlID="JobTypesHeaderImage" SuppressPostBack="true" />
										<div class="singleAccordionContentWrapper">
											<asp:Panel ID="ExcludeJobsHeaderPanel" runat="server" CssClass="singleAccordionTitle singleAccordionStyle2">
												<table role="presentation">
													<tr>
														<td style="width: 30px;">
															<span>
																<asp:Image ID="ExcludeJobsHeaderImage" runat="server" AlternateText="." Height="22" Width="22" /></span>
														</td>
														<td class="accordionLabel">
															<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("AccordionHeading3.Label", "EXCLUDE JOBS")%></a></span>
														</td>
														<td>
															<div class="tooltip">
																<div class="tooltipWithArrow helpMessage">
																	<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("ExcludeJobs.Tooltip", "If you want to search for jobs that match some of the jobs shown on your resume, select those you wish to exclude from your search results. This helps to narrow your search results only to jobs that are still of interest to you.")%>"></div>
																</div>
															</div>
														</td>
													</tr>
												</table>
											</asp:Panel>
											<asp:Panel ID="ExcludeJobsContentPanel" runat="server" CssClass="singleAccordionContent singleAccordionStyle2">
												<table role="presentation">
													<tr class="first">
														<td>
															<%= HtmlLocalise("ExcludeJob.Label", "Search based on this resume but exclude these jobs from being factored into the search results:")%>
														</td>
													</tr>
													<tr class="last">
														<td>
															<asp:CheckBoxList ID="cblExcludeJobs" runat="server" class="checkBoxList" Width="100%" role="presentation">
															</asp:CheckBoxList>
														</td>
													</tr>
												</table>
											</asp:Panel>
										</div>
										<act:CollapsiblePanelExtender ID="ExcludeJobsPanelExtender" runat="server" TargetControlID="ExcludeJobsContentPanel"
											ExpandControlID="ExcludeJobsHeaderPanel" CollapseControlID="ExcludeJobsHeaderPanel"
											Collapsed="True" ImageControlID="ExcludeJobsHeaderImage" SuppressPostBack="true" />
									</td>
								</tr>
							</table>

						</ContentTemplate>
					</asp:UpdatePanel>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="3">
		<div class="horizontalRule">
		</div>
	</td>
</tr>

<script type="text/javascript">

	var prm = Sys.WebForms.PageRequestManager.getInstance();
	prm.add_endRequest(endRequestHandler);

	Sys.Application.add_load(function () {
		var behavior = $find('<%=ccdOccupation.BehaviorID %>');
		if (behavior != null) {
			behavior.add_populated(function () {
				$('#ddlOccupation').next().find(':first-child').text($('#ddlOccupation option:selected').text()).parent().addClass('changed');
			});
		}
		var indBehavior = $find('<%=ccdIndustryDetail.BehaviorID %>');
  	if (indBehavior != null) {
  		indBehavior.add_populated(function () {
  			if ($('#ddlIndustry').val() != '0') {
  				$('#ddlIndustryDetail').next().find(':first-child').text($("#ddlIndustryDetail option:selected").text()).parent().addClass('changed');
  			}
  			else {
  				$('#ddlIndustryDetail').attr('disabled', 'disabled');
  			}
  		});
  	}
  	var msaBehavior = $find('<%=ccdMSA.BehaviorID %>');
    if (msaBehavior != null) {
    	msaBehavior.add_populated(function () {
    		$('#ddlMSA').next().find(':first-child').text($("#ddlMSA option:selected").text()).parent().addClass('changed');
    		if ($('#rbArea').is(':checked')) {
    			$('#ddlMSA').attr('disabled', 'disabled');
    		}
    	});
    }
    var minimumSalaryTextBox = $('#minimumSalaryTextBox');
    if ($(minimumSalaryTextBox).val() != '') {
    	$(minimumSalaryTextBox).val(formatCurrency($(minimumSalaryTextBox).val()));
    }

    var rbArea = $('#rbMSAState');
    var rbRadius = $('#rbArea');

    if ($(rbArea).is(':checked')) {
    	Hide_Radius_StateMSA($(rbArea)[0]);
    }
    else if ($(rbRadius).is(':checked')) {
    	Hide_Radius_StateMSA($(rbRadius)[0]);
    }

    EnableDisableExcludeJob();
	});


	function endRequestHandler(sender, args) {
		$('span.inFieldLabel > label').inFieldLabels();
	}

	//TODO: Martha - check below (low) function replaced 07.03.2013 - backup available
	function Hide_Radius_StateMSA(sender) {
		var rbArea = document.getElementById("rbArea");
		var rbMSAState = document.getElementById("rbMSAState");
		var ddlState = document.getElementById("ddlState");
		var ddlMSA = document.getElementById("ddlMSA");
		var ddlRadius = document.getElementById("ddlRadius");
		var txtZipCode = document.getElementById("txtZipCode");
		var cbJobsInMyState = $('#cbJobsInMyState');
		if (sender == rbArea) {
			ddlRadius.disabled = false;
			txtZipCode.disabled = false;

			$(ddlMSA).empty();
			var opt = document.createElement("option");
			opt.value = "";
			opt.text = "- select city -";
			ddlMSA.options.add(opt);


			$(ddlState).get(0).selectedIndex = 0;
			$(ddlState).next().find(':first-child').text($("#ddlState option:selected").text()).parent().addClass('changed');
			$(ddlMSA).get(0).selectedIndex = 0;
			$(ddlMSA).next().find(':first-child').text($("#ddlMSA option:selected").text()).parent().addClass('changed');
			$(ddlState).attr('disabled', 'disabled');
			$(ddlMSA).attr('disabled', 'disabled');
			cbJobsInMyState.prop('disabled', false);
		}
		else {
			$(ddlState).removeAttr('disabled');
			$(ddlMSA).removeAttr('disabled');
			ddlRadius.disabled = true;
			txtZipCode.disabled = true;
			$(ddlRadius).get(0).selectedIndex = 0;
			$(ddlRadius).next().find(':first-child').text($("#ddlRadius option:selected").text()).parent().addClass('changed');
			txtZipCode.value = "";
			cbJobsInMyState.prop('disabled', true);
			cbJobsInMyState.prop('checked', false);
		}
	}

	function CheckRadius() {
		var rbArea = document.getElementById("rbArea");
		var ddlRadius = document.getElementById("ddlRadius");
		var Radius = ddlRadius.options[ddlRadius.selectedIndex].value;
		var txtZipCode = document.getElementById("txtZipCode");
		if (Radius != "" || txtZipCode.value != "") {
			$('#rbSearchArea').attr('checked', 'checked');
			Hide_Radius_StateMSA(rbArea);
		}
	}
	function CheckMSA() {
		var rbSearchMSA = document.getElementById("rbMSAState");
		var state = $('#ddlState').val();
		var msa = $('#ddlMSA').val();

		if (state != "" | msa != "") {
			$('#rbSearchMSA').attr('checked', 'checked');
			Hide_Radius_StateMSA(rbSearchMSA);
		}
	}

	function ResetStar() {
		$("#ddlStarMatch").get(0).selectedIndex = 2;
		$(".StarMatchClass").next().find(':first-child').text($("#ddlStarMatch option:selected").text()).parent().addClass('changed');
	}

	function EnableDisableExcludeJob() {
		var rbStarMatch = document.getElementById("rbStarMatch");
		var rbWithoutMatch = document.getElementById("rbWithoutMatch");
		var excludeJobBox = document.getElementById("ExcludeJobsPanel");
		if (rbStarMatch.checked && excludeJobBox != null) {
			var elements = excludeJobBox.getElementsByTagName("input");
			for (var i = 0; i < elements.length; i++)
				if (elements[i].type == "checkbox") {
					elements[i].disabled = false;
				}
			//excludeJobBox.style.visibility = "visible";
			//excludeJobBox.style.display = "";
		}
		else if (rbWithoutMatch.checked && excludeJobBox != null) {
			var elements = excludeJobBox.getElementsByTagName("input");
			for (var i = 0; i < elements.length; i++)
				if (elements[i].type == "checkbox") {
					//elements[i].checked = false;
					elements[i].disabled = true;
				}
			//excludeJobBox.style.visibility = "hidden";
			//excludeJobBox.style.display = "none";
		}
		//Commented out as it was causing problems (FVN-2691)
		//    if ($("#MainContent_ExcludeJobsContentPanel").length == 0) {
		//      $(".singleAccordionContentWrapper").last().removeClass("singleAccordionContentWrapper");
		//    }
	}

	function validateWages(sender, args) {
		var wages = document.getElementById("minimumSalaryTextBox").value;
		var wagesRegEx = /^\d{1,3}(?:\,?\d{3,3})*(?:\.\d{2,2})?$/;
		if (wagesRegEx.test(wages) == false) {
			args.IsValid = false;
		}
	}
	function ValidateRadius(sender, args) {
		var radius = $('#ddlRadius').val();
		var zipCode = $('#txtZipCode').val().trim();
		args.IsValid = !($("#rbArea").is(':checked')) || (radius.length > 0 && zipCode.length == 5);
	}
	//		function ValidateArea(sender, args) {
	//			var ddlState = document.getElementById("ddlState");
	//			var State = ddlState.options[ddlState.selectedIndex].value;
	//			var ddlMSA = document.getElementById("ddlMSA");
	//			var MSA = ddlMSA.options[ddlMSA.selectedIndex].value;
	//			if (!(((State == "none" || State == "") && MSA == "") || ((State != "none" || State != "") && MSA != ""))) {
	//				args.IsValid = false;
	//			}
	//		}
</script>
