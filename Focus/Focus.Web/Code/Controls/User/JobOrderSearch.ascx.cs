﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;

#endregion

namespace Focus.Web.Code.Controls.User
{
  public partial class JobOrderSearch : UserControlBase
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        LocaliseUI();
        BindCreationDates();
      }
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
    
    }


    /// <summary>
    /// Binds the creation dates.
    /// </summary>
    private void BindCreationDates()
    {
      FromMonthDropDownList.Items.Clear(); FromYearDropDownList.Items.Clear();
      ToMonthDropDownList.Items.Clear(); ToYearDropDownList.Items.Clear();

      var months = new[] { CodeLocalise("Global.January.Label", "January"), CodeLocalise("Global.February.Label", "February"), CodeLocalise("Global.March.Label", "March"),
													 CodeLocalise("Global.April.Label", "April"), CodeLocalise("Global.May.Label", "May"), CodeLocalise("Global.June.Label", "June"),
													 CodeLocalise("Global.July.Label", "July"), CodeLocalise("Global.August.Label", "August"), CodeLocalise("Global.September.Label", "September"),
													 CodeLocalise("Global.October.Label", "October"), CodeLocalise("Global.November.Label", "November"), CodeLocalise("Global.December.Label", "December")};

      for (var i = 0; i < 12; i++)
      {
        FromMonthDropDownList.Items.Add(new ListItem(months[i], (i + 1).ToString()));
        ToMonthDropDownList.Items.Add(new ListItem(months[i], (i + 1).ToString()));
      }

      for (var i = 2005; i <= DateTime.Now.Year; i++)
      {
        FromYearDropDownList.Items.Add(new ListItem((i).ToString(), (i).ToString()));
        ToYearDropDownList.Items.Add(new ListItem((i).ToString(), (i).ToString()));
      }

      FromMonthDropDownList.Items.AddLocalisedTopDefault("Month.TopDefault", "month");
      FromYearDropDownList.Items.AddLocalisedTopDefault("Year.TopDefault", "year");

      ToMonthDropDownList.Items.AddLocalisedTopDefault("Month.TopDefault", "month");
      ToYearDropDownList.Items.AddLocalisedTopDefault("Year.TopDefault", "year");

    }
  }
}