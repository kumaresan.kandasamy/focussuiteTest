<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SaveSearchActionModal.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SaveSearchActionModal" %>

<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:HiddenField ID="SaveSearchActionModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="SaveSearchActionModalPopup" runat="server" ClientIDMode="Static"
												TargetControlID="SaveSearchActionModalDummyTarget"
												PopupControlID="SaveSearchActionModalPanel" 
												PopupDragHandleControlID="SaveSearchActionModalPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="SaveSearchActionModalPanel" TabIndex="-1" runat="server" CssClass="modal" Style="display:none">
	<div style="float:left;">
	<table style="width:400px;" role="presentation">
		<tr>
			<td class="modalHeading" style="vertical-align:top;"><h5><%= Title %></h5></td>
		</tr>
		<tr>
			<td>
				<table role="presentation">
					<tr style="vertical-align: top">
						<td style="width:140px;white-space:nowrap"><%= HtmlRequiredLabel(NameTextBox, "Name.Label", "Name my search")%></td>
						<td>
						  <asp:TextBox ID="NameTextBox" runat="server" />
              <br />
							<asp:RequiredFieldValidator ID="NameRequired" runat="server" ControlToValidate="NameTextBox" SetFocusOnError="true" CssClass="error" ValidationGroup="SaveSearchActionModal" />
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<asp:CheckBox ID="AlertMeCheckBox" runat="server" Checked="true" ClientIDMode="AutoID" />
              <%= HtmlLabel(AlertMeCheckBox,"AlertMe.Label", "Alert me of new talent matching my criteria")%>
						</td>
					</tr>
          <tr>
						<td colspan="2">
              <table role="presentation">
								<tr>
									<td>&nbsp;</td>
									<td>
										<asp:RadioButton ID="DailyAlertFrequencyRadioButton" runat="server" GroupName="AlertFrequency" Checked="True" />
										<%= HtmlLabel(DailyAlertFrequencyRadioButton, EmailAlertFrequencies.Daily, "Daily")%>
									</td>
									<td>
										<asp:RadioButton ID="HTMLEmailFormatRadioButton" runat="server" GroupName="EmailFormat" Checked="True" />
										<%= HtmlLabel(HTMLEmailFormatRadioButton,EmailFormats.HTML, "HTML")%>
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>
										<asp:RadioButton ID="WeeklyAlertFrequencyRadioButton" runat="server" GroupName="AlertFrequency" />
										<%= HtmlLabel(WeeklyAlertFrequencyRadioButton,EmailAlertFrequencies.Weekly, "Weekly")%>
									</td>
									<td>
										<asp:RadioButton ID="TextOnlyEmailFormatRadioButton" runat="server" GroupName="EmailFormat" />
										<%= HtmlLabel(TextOnlyEmailFormatRadioButton,EmailFormats.TextOnly, "Text only")%>
									</td>
								</tr>
                <tr>
                  <td colspan="3">&nbsp;</td>
                </tr>
							</table>
						</td>
					</tr>
					<tr style="vertical-align: top">
						<td style="white-space:nowrap"><%= HtmlRequiredLabel(EmailAddressTextBox, "EmailAddress.Label", "Email address")%></td>
						<td>
							<asp:TextBox ID="EmailAddressTextBox" runat="server" ClientIDMode="Static"/>
              <br />
							<asp:RequiredFieldValidator ID="EmailAddressRequired" runat="server" ControlToValidate="EmailAddressTextBox" SetFocusOnError="true" CssClass="error" ValidationGroup="SaveSearchActionModal" Display="Dynamic" />
							<asp:RegularExpressionValidator ID="EmailAddressRegEx" runat="server" ControlToValidate="EmailAddressTextBox" SetFocusOnError="true" CssClass="error" ValidationGroup="SaveSearchActionModal" Display="Dynamic" />
              <br />
						</td>
					</tr>
				</table>
               <%--31.May.2017 - KRP - FVN-5010 Temporary hack to avoid saving search of 1st user to 2nd user
                Create a hidden to store the user id. While saving the search, check whether the field value is matching with
                current user id--%> 
                <asp:HiddenField ID="UserIDHidden" runat="server"/>
			</td>
		</tr>
		<tr>
			<td style="text-align: left;">
				<asp:Button ID="OkButton" runat="server" SkinID="Button3" CausesValidation="true" ValidationGroup="SaveSearchActionModal" OnClick="OkButton_Click" />
			</td>
		</tr>
	</table>
	</div>
	<div class="closeIcon"><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text.Button", "Close") %>" onclick="$find('SaveSearchActionModalPopup').hide();return false;" /></div>
</asp:Panel>

<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />

<script type="text/javascript">

  $(document).ready(function () {
    var alertMeCheckBox = $("#<%=AlertMeCheckBox.ClientID%>");
    var isAlertMeChecked = alertMeCheckBox.is(':checked');
    EnableEmailValidator(isAlertMeChecked);
    EnableDisableEmail(isAlertMeChecked);

    alertMeCheckBox.change(function () {
      var alertMeChecked = alertMeCheckBox.is(':checked');
      EnableEmailValidator(alertMeChecked);
      EnableDisableEmail(alertMeChecked);
    });
  });

	function EnableEmailValidator(enable) {
		ValidatorEnable($("#<%= EmailAddressRequired.ClientID %>")[0], enable);
	}

	function EnableDisableEmail(enable) {
		if (enable)
			$("#EmailAddressTextBox").prop('disabled', false);
		else
			$("#EmailAddressTextBox").val('').prop('disabled', true);
	}

</script>
