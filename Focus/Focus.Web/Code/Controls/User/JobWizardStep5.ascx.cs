﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Web.ViewModels;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class JobWizardStep5 : JobWizardControl
	{
		private const string NewOrMultipleLocationsValue = "NewOrMultipleLocations";
		private const string AllLocationsValue = "AllLocations";

		private List<JobLocationDto> _jobLocations
		{
			get { return GetViewStateValue<List<JobLocationDto>>("JobWizardStep5:JobLocations"); }
			set { SetViewStateValue("JobWizardStep5:JobLocations", value); }
		}

		private List<JobLocationDto> AllJobLocations
		{
			get { return GetViewStateValue<List<JobLocationDto>>("JobWizardStep5:AllJobLocations"); }
			set { SetViewStateValue("JobWizardStep5:AllJobLocations", value); }
		}

	  private JobLocationDto MainSiteLocation
	  {
      get { return GetViewStateValue<JobLocationDto>("JobWizardStep5:MainSiteLocation"); }
      set { SetViewStateValue("JobWizardStep5:MainSiteLocation", value); }
	  }

		private JobWizardViewModel Model
		{
			get { return GetViewStateValue<JobWizardViewModel>("JobWizardStep5:ViewModel"); }
			set { SetViewStateValue("JobWizardStep5:ViewModel", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
			}

		  ApplyFederalContractorDateSettings();
			ApplyBranding();
			RegisterJavascript();
			ApplyTheme();
		}

    /// <summary>
    /// Handles the Federal Contractor Date Settings
    /// </summary>
    private void ApplyFederalContractorDateSettings()
	  {
      FederalContractorExpirationDateGreaterThanTodayCompareValidator.ValueToCompare = DateTime.Today.ToShortDateString();
      if (App.Settings.FederalContratorExpiryDateMandatory)
      {
        FederalContractorExpirationDateLiteral.Text = HtmlRequiredLabel(FederalContractorExpirationDateTextBox, "FederalContractorExpirationDate.Label", "Expiration date");
        //FederalContractorExpirationDateRequired.Enabled = true;
      }
      else
      {
        FederalContractorExpirationDateLiteral.Text = CodeLocalise("FederalContractorExpirationDate.Label", "Expiration date");
        FederalContractorExpirationDateRequired.Enabled = false;
      }
	  }

		/// <summary>
		/// Handles the CheckedChanged event of the FederalContractorCheckBox control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void FederalContractorCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			EnableFederalContractorValidators(FederalContractorCheckBox.Checked);
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the WorkLocationDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void WorkLocationDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			var selectedJobLocationType = WorkLocationDropDown.SelectedValueToEnum(JobLocationTypes.MultipleLocations);

			PublicTransitAccessibleCheckBox.Visible = (selectedJobLocationType == JobLocationTypes.MainSite);
			if (!App.Settings.ShowWorkLocationFullAddress)
			{
				if (selectedJobLocationType == JobLocationTypes.MultipleLocations)
				{
					LocationSelector.Visible = true;
					var jobLocations = _jobLocations;
					if (WorkLocationDropDown.SelectedValue == AllLocationsValue) jobLocations = AllJobLocations;
					LocationSelector.Bind(jobLocations);
				}
				else
				{
					LocationSelector.Visible = false;
				}
			}
			else
			{
				// show full postal addresses
				LocationSelector.Visible = true;
				var jobLocations = _jobLocations;
				LocationSelector.Mode = JobWizardLocationSelector.LocationSelectorMode.FullAddressSingle;
				if (WorkLocationDropDown.SelectedValue == AllLocationsValue)
				{
					jobLocations = AllJobLocations;
					LocationSelector.Mode = JobWizardLocationSelector.LocationSelectorMode.FullAddressMany;
				}

				if (selectedJobLocationType == JobLocationTypes.MultipleLocations)
				{
					LocationSelector.Mode = JobWizardLocationSelector.LocationSelectorMode.FullAddressMany;	
				}

				if (selectedJobLocationType == JobLocationTypes.MainSite)
				{
					var stateName =
						ServiceClientLocator.CoreClient(App)
							.GetLookup(LookupTypes.States)
							.Where(x => x.Id == Model.BusinessUnitAddress.StateId)
							.Select(x => x.Text)
							.SingleOrDefault();

          var countyName = App.Settings.CountyEnabled
                             ? ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Counties).Where(x => x.Id == Model.BusinessUnitAddress.CountyId).Select(x => x.Text).SingleOrDefault()
                             : null;

					jobLocations = new List<JobLocationDto>()
					{
						new JobLocationDto()
						{
							AddressLine1 = Model.BusinessUnitAddress.Line1,
							City = Model.BusinessUnitAddress.TownCity,
              County = countyName,
							State = stateName,
							Zip = Model.BusinessUnitAddress.PostcodeZip,
							IsPublicTransitAccessible = Model.BusinessUnitAddress.PublicTransitAccessible
						}
					};
				}

				LocationSelector.Bind(jobLocations);
			}

      LocationSelectorValidator.Enabled = selectedJobLocationType.IsIn(JobLocationTypes.MultipleLocations, JobLocationTypes.OtherLocation);
		}

		#region BindStep & UnbindStep

		/// <summary>
		/// Binds the controls for the step.
		/// </summary>
		/// <param name="model">The model.</param>
		internal void BindStep(JobWizardViewModel model)
		{
			_jobLocations = model.JobLocations;
			AllJobLocations = model.AllJobLocations;
			Model = model;

			BindControls(model);

			if (model.Job.JobLocationType == JobLocationTypes.MultipleLocations ||
			    model.Job.JobLocationType == JobLocationTypes.OtherLocation)
			{
				WorkLocationDropDown.SelectValue(NewOrMultipleLocationsValue);
				LocationSelector.Visible = true;
				if (_jobLocations.IsNotNullOrEmpty()) LocationSelector.Bind(_jobLocations);
			}
			else
			{
				WorkLocationDropDown.SelectValue(model.Job.JobLocationType.ToString());
				LocationSelector.Visible = false;
			}

			if (App.Settings.ShowWorkLocationFullAddress)
			{
				LocationSelector.Visible = true;
				var jobLocations = _jobLocations;
				LocationSelector.Mode = JobWizardLocationSelector.LocationSelectorMode.FullAddressSingle;
				if (model.Job.JobLocationType == JobLocationTypes.OtherLocation)
				{
					LocationSelector.Mode = JobWizardLocationSelector.LocationSelectorMode.FullAddressMany;
				}

				if (model.Job.JobLocationType == JobLocationTypes.MultipleLocations)
				{
					LocationSelector.Mode = JobWizardLocationSelector.LocationSelectorMode.FullAddressMany;
				}

				if (model.Job.JobLocationType == JobLocationTypes.MainSite)
				{
					var stateName =
						ServiceClientLocator.CoreClient(App)
							.GetLookup(LookupTypes.States)
							.Where(x => x.Id == model.BusinessUnitAddress.StateId)
							.Select(x => x.Text)
							.SingleOrDefault();

				  var countyName = App.Settings.CountyEnabled
				                     ? ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Counties).Where(x => x.Id == Model.BusinessUnitAddress.CountyId).Select(x => x.Text).SingleOrDefault()
				                     : null;

					jobLocations = new List<JobLocationDto>()
					{
						new JobLocationDto
						{
							AddressLine1 = model.BusinessUnitAddress.Line1,
							City = model.BusinessUnitAddress.TownCity,
              County = countyName,
							State = stateName,
							Zip = model.BusinessUnitAddress.PostcodeZip,
							IsPublicTransitAccessible = model.BusinessUnitAddress.PublicTransitAccessible
						}
					};
				}

				LocationSelector.Bind(jobLocations);
			}

		  LocationSelectorValidator.Enabled = (model.Job.JobLocationType == JobLocationTypes.MultipleLocations ||model.Job.JobLocationType == JobLocationTypes.OtherLocation);

			PublicTransitAccessibleCheckBox.Visible = (model.Job.JobLocationType == JobLocationTypes.MainSite);

      if (model.Job.JobLocationType == JobLocationTypes.MainSite && _jobLocations.IsNotNullOrEmpty())
				PublicTransitAccessibleCheckBox.Checked = (_jobLocations[0].IsPublicTransitAccessible.IsNotNull() && _jobLocations[0].IsPublicTransitAccessible);
			else
				PublicTransitAccessibleCheckBox.Checked = (model.BusinessUnitAddress.PublicTransitAccessible.IsNotNull() && model.BusinessUnitAddress.PublicTransitAccessible);

      HomeBasedWorkerCheckbox.Visible = (App.Settings.Theme == FocusThemes.Workforce);
		  HomeBasedWorkerCheckbox.Checked = model.Job.SuitableForHomeWorker.GetValueOrDefault();

      FederalContractorCheckBox.Checked = model.Job.FederalContractor.GetValueOrDefault();
			EnableFederalContractorValidators(FederalContractorCheckBox.Checked);

			if (model.Job.FederalContractorExpiresOn.IsNotNull() && model.Job.FederalContractorExpiresOn > DateTime.MinValue)
				FederalContractorExpirationDateTextBox.Text = model.Job.FederalContractorExpiresOn.GetValueOrDefault().ToShortDateString();

			AdvertiseFor30DaysPlaceHolder.Style.Remove( "display" );
			if( !model.Job.ForeignLabourCertificationOther.GetValueOrDefault())
			{
					AdvertiseFor30DaysPlaceHolder.Style.Add( "display", "none" );
			}
			
		  if (model.Job.ForeignLabourCertificationH2A.GetValueOrDefault() || model.Job.ForeignLabourCertificationH2B.GetValueOrDefault() || model.Job.ForeignLabourCertificationOther.GetValueOrDefault())
		  {
		    ForeignLaborCertificationCheckBox.Checked = true;
        ForeignLaborRadioButtonList.Enabled = true;

        ForeignLaborRadioButtonList.Items[0].Selected = model.Job.ForeignLabourCertificationH2A.GetValueOrDefault();
        ForeignLaborRadioButtonList.Items[1].Selected = model.Job.ForeignLabourCertificationH2B.GetValueOrDefault();
        ForeignLaborRadioButtonList.Items[2].Selected = model.Job.ForeignLabourCertificationOther.GetValueOrDefault();
		  }
		  else
		  {
        ForeignLaborCertificationCheckBox.Checked = false;
        ForeignLaborRadioButtonList.Enabled = false;

        ForeignLaborRadioButtonList.Items[0].Selected = 
          ForeignLaborRadioButtonList.Items[1].Selected = 
          ForeignLaborRadioButtonList.Items[2].Selected = false;
		  }

			var expiryDate = GetJobExpiryDate(App, model.Job.PostedOn, model.Job.FederalContractor.GetValueOrDefault(false), model.Job.ForeignLabourCertificationH2A.GetValueOrDefault(false),
        model.Job.ForeignLabourCertificationH2B.GetValueOrDefault(false), model.Job.ForeignLabourCertificationOther.GetValueOrDefault(false));
			ForeignLaborCheckBoxH2AH2BValidator.Enabled = !model.Job.ClosingOn.HasValue || model.Job.ClosingOn.GetValueOrDefault() > expiryDate;
			ForeignLaborCheckBoxH2AH2BValidator.ErrorMessage = CodeLocalise("ForeignLaborCheckBoxH2AH2BValidator.ErrorMessage", "To perform this update, your job's expiry date cannot exceed {0}.", expiryDate.ToShortDateString());

		  CourtOrderedAffirmativeActionCheckBox.Checked = model.Job.CourtOrderedAffirmativeAction.GetValueOrDefault();

			if (model.Job.WorkOpportunitiesTaxCreditHires.IsNotNull())
			{
				LongTermFamilyAssistanceRecipientsCheckBox.Checked = ((model.Job.WorkOpportunitiesTaxCreditHires & WorkOpportunitiesTaxCreditCategories.LongTermFamilyAssistanceRecipient) == WorkOpportunitiesTaxCreditCategories.LongTermFamilyAssistanceRecipient);
				TANFRecipientsCheckBox.Checked = ((model.Job.WorkOpportunitiesTaxCreditHires & WorkOpportunitiesTaxCreditCategories.TemporaryAssistanceToNeedyFamilyRecipients) == WorkOpportunitiesTaxCreditCategories.TemporaryAssistanceToNeedyFamilyRecipients);
				DesignatedCommunityResidentsCheckBox.Checked = ((model.Job.WorkOpportunitiesTaxCreditHires & WorkOpportunitiesTaxCreditCategories.DesignatedCommunityResidents) == WorkOpportunitiesTaxCreditCategories.DesignatedCommunityResidents);
				SNAPRecipientsCheckBox.Checked = ((model.Job.WorkOpportunitiesTaxCreditHires & WorkOpportunitiesTaxCreditCategories.SNAPRecipients) == WorkOpportunitiesTaxCreditCategories.SNAPRecipients);

				VocationalRehabilitationCheckBox.Checked = ((model.Job.WorkOpportunitiesTaxCreditHires & WorkOpportunitiesTaxCreditCategories.VocationalRehabilitation) == WorkOpportunitiesTaxCreditCategories.VocationalRehabilitation);
				SSIRecipientsCheckBox.Checked = ((model.Job.WorkOpportunitiesTaxCreditHires & WorkOpportunitiesTaxCreditCategories.SupplementSecurityIncomeRecipients) == WorkOpportunitiesTaxCreditCategories.SupplementSecurityIncomeRecipients);
				ExFelonsCheckBox.Checked = ((model.Job.WorkOpportunitiesTaxCreditHires & WorkOpportunitiesTaxCreditCategories.ExFelons) == WorkOpportunitiesTaxCreditCategories.ExFelons);
				SummerYouthCheckBox.Checked = ((model.Job.WorkOpportunitiesTaxCreditHires & WorkOpportunitiesTaxCreditCategories.SummerYouth) == WorkOpportunitiesTaxCreditCategories.SummerYouth);
				VeteransCheckBox.Checked = ((model.Job.WorkOpportunitiesTaxCreditHires & WorkOpportunitiesTaxCreditCategories.Veterans) == WorkOpportunitiesTaxCreditCategories.Veterans);
			}

			if (model.Job.PostingFlags.IsNotNull())
			{
				HealthcareEmployerCheckBox.Checked = ((model.Job.PostingFlags & JobPostingFlags.HealthcareEmployer) == JobPostingFlags.HealthcareEmployer);
				AdvancedManufacturingCheckBox.Checked = ((model.Job.PostingFlags & JobPostingFlags.AdvancedManufacturingEmployer) == JobPostingFlags.AdvancedManufacturingEmployer);
				TransportationCheckBox.Checked = ((model.Job.PostingFlags & JobPostingFlags.TransportationEmployer) == JobPostingFlags.TransportationEmployer);
				BiotechEmployerCheckBox.Checked = ((model.Job.PostingFlags & JobPostingFlags.BiotechEmployer) == JobPostingFlags.BiotechEmployer);
				GreenJobCheckBox.Checked = ((model.Job.PostingFlags & JobPostingFlags.GreenJob) == JobPostingFlags.GreenJob);
				ITEmployerCheckBox.Checked = ((model.Job.PostingFlags & JobPostingFlags.ITEmployer) == JobPostingFlags.ITEmployer);
				AerospaceCheckBox.Checked = ((model.Job.PostingFlags & JobPostingFlags.Aerospace) == JobPostingFlags.Aerospace);
				HealthInformaticsCheckBox.Checked = ((model.Job.PostingFlags & JobPostingFlags.HealthInformatics) == JobPostingFlags.HealthInformatics);
				SMARTCheckBox.Checked = ((model.Job.PostingFlags & JobPostingFlags.SMART) == JobPostingFlags.SMART);
				BusinessServicesCheckBox.Checked = ((model.Job.PostingFlags & JobPostingFlags.BusinessServices) == JobPostingFlags.BusinessServices);
				ConsultingCheckBox.Checked = ((model.Job.PostingFlags & JobPostingFlags.Consulting) == JobPostingFlags.Consulting);
				ResearchDevelopmentCheckBox.Checked = ((model.Job.PostingFlags & JobPostingFlags.ResearchandDevelopment) == JobPostingFlags.ResearchandDevelopment);
				EnergyCheckbox.Checked = ((model.Job.PostingFlags & JobPostingFlags.Energy) == JobPostingFlags.Energy);
			}
		}

		/// <summary>
		/// Unbinds the step.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
		internal JobWizardViewModel UnbindStep(JobWizardViewModel model)
		{
			if (WorkLocationDropDown.SelectedValueToEnum<JobLocationTypes>(JobLocationTypes.MultipleLocations) == JobLocationTypes.MultipleLocations)
			{
				model.Job.JobLocationType = JobLocationTypes.MultipleLocations;
				_jobLocations = LocationSelector.Unbind();

				// Ensure all locations are associated with the job
				if (_jobLocations.IsNotNullOrEmpty())
				{
					foreach (var jobLocation in _jobLocations)
					{
						jobLocation.JobId = model.Job.Id.GetValueOrDefault();
					}

					model.Job.JobLocationType = (_jobLocations.Count > 1)
						? JobLocationTypes.MultipleLocations
						: JobLocationTypes.OtherLocation;

					model.JobLocations = _jobLocations;
				}
			}
			else
			{
				model.Job.JobLocationType = WorkLocationDropDown.SelectedValueToEnum<JobLocationTypes>();
			}

			if (model.Job.JobLocationType == JobLocationTypes.MainSite)
			{
				var locations = new List<JobLocationDto>
				{
					new JobLocationDto
					{
						JobId = model.Job.Id.GetValueOrDefault(),
            City = MainSiteLocation.City,
            County = MainSiteLocation.County,
            State = MainSiteLocation.State,
            Zip = MainSiteLocation.Zip,
						Location = MainSiteLocation.Location,
						IsPublicTransitAccessible = PublicTransitAccessibleCheckBox.Checked
					}
				};

				model.JobLocations = locations;
			}

			if (model.Job.JobLocationType == JobLocationTypes.NoFixedLocation)
				model.JobLocations = new List<JobLocationDto>();

		  model.Job.SuitableForHomeWorker = HomeBasedWorkerCheckbox.Checked;

			model.Job.FederalContractor = FederalContractorCheckBox.Checked;
			model.Job.FederalContractorExpiresOn = (model.Job.FederalContractor.GetValueOrDefault()) ? model.Job.FederalContractorExpiresOn = FederalContractorExpirationDateTextBox.TextTrimmed().ToDate() : null;

		  if (ForeignLaborCertificationCheckBox.Checked)
		  {
		    model.Job.ForeignLabourCertificationH2A = ForeignLaborRadioButtonList.Items[0].Selected;
		    model.Job.ForeignLabourCertificationH2B = ForeignLaborRadioButtonList.Items[1].Selected;
		    model.Job.ForeignLabourCertificationOther = ForeignLaborRadioButtonList.Items[2].Selected;
		  }
		  else
		  {
		    model.Job.ForeignLabourCertificationH2A =
		      model.Job.ForeignLabourCertificationH2B =
		      model.Job.ForeignLabourCertificationOther = false;
		  }
		  model.Job.CourtOrderedAffirmativeAction = CourtOrderedAffirmativeActionCheckBox.Checked;

			var workOpportunitiesTaxCreditHires = WorkOpportunitiesTaxCreditCategories.None;
			if (TANFRecipientsCheckBox.Checked) workOpportunitiesTaxCreditHires = workOpportunitiesTaxCreditHires | WorkOpportunitiesTaxCreditCategories.TemporaryAssistanceToNeedyFamilyRecipients;
			if (DesignatedCommunityResidentsCheckBox.Checked) workOpportunitiesTaxCreditHires = workOpportunitiesTaxCreditHires | WorkOpportunitiesTaxCreditCategories.DesignatedCommunityResidents;
			if (SNAPRecipientsCheckBox.Checked)  workOpportunitiesTaxCreditHires = workOpportunitiesTaxCreditHires | WorkOpportunitiesTaxCreditCategories.SNAPRecipients;
			if (LongTermFamilyAssistanceRecipientsCheckBox.Checked) workOpportunitiesTaxCreditHires = workOpportunitiesTaxCreditHires | WorkOpportunitiesTaxCreditCategories.LongTermFamilyAssistanceRecipient;
			if (VocationalRehabilitationCheckBox.Checked) workOpportunitiesTaxCreditHires = workOpportunitiesTaxCreditHires | WorkOpportunitiesTaxCreditCategories.VocationalRehabilitation;
			if (SSIRecipientsCheckBox.Checked) workOpportunitiesTaxCreditHires = workOpportunitiesTaxCreditHires | WorkOpportunitiesTaxCreditCategories.SupplementSecurityIncomeRecipients;
			if (ExFelonsCheckBox.Checked) workOpportunitiesTaxCreditHires = workOpportunitiesTaxCreditHires | WorkOpportunitiesTaxCreditCategories.ExFelons;
			if (SummerYouthCheckBox.Checked) workOpportunitiesTaxCreditHires = workOpportunitiesTaxCreditHires | WorkOpportunitiesTaxCreditCategories.SummerYouth;
			if (VeteransCheckBox.Checked) workOpportunitiesTaxCreditHires = workOpportunitiesTaxCreditHires | WorkOpportunitiesTaxCreditCategories.Veterans;

			model.Job.WorkOpportunitiesTaxCreditHires = workOpportunitiesTaxCreditHires;

			var jobPostingFlags = JobPostingFlags.None;
			if (HealthcareEmployerCheckBox.Checked) jobPostingFlags = jobPostingFlags | JobPostingFlags.HealthcareEmployer;
			if (AdvancedManufacturingCheckBox.Checked) jobPostingFlags = jobPostingFlags | JobPostingFlags.AdvancedManufacturingEmployer;
			if (TransportationCheckBox.Checked) jobPostingFlags = jobPostingFlags | JobPostingFlags.TransportationEmployer;
			if (BiotechEmployerCheckBox.Checked) jobPostingFlags = jobPostingFlags | JobPostingFlags.BiotechEmployer;
			if (GreenJobCheckBox.Checked) jobPostingFlags = jobPostingFlags | JobPostingFlags.GreenJob;
			if (ITEmployerCheckBox.Checked) jobPostingFlags = jobPostingFlags | JobPostingFlags.ITEmployer;
			if (SMARTCheckBox.Checked) jobPostingFlags = jobPostingFlags | JobPostingFlags.SMART;
			if (ResearchDevelopmentCheckBox.Checked) jobPostingFlags = jobPostingFlags | JobPostingFlags.ResearchandDevelopment;
			if (AerospaceCheckBox.Checked) jobPostingFlags = jobPostingFlags | JobPostingFlags.Aerospace;
			if (HealthInformaticsCheckBox.Checked) jobPostingFlags = jobPostingFlags | JobPostingFlags.HealthInformatics;
			if (BusinessServicesCheckBox.Checked) jobPostingFlags = jobPostingFlags | JobPostingFlags.BusinessServices;
			if (ConsultingCheckBox.Checked) jobPostingFlags = jobPostingFlags | JobPostingFlags.Consulting;
			if (EnergyCheckbox.Checked) jobPostingFlags = jobPostingFlags | JobPostingFlags.Energy;

			model.Job.PostingFlags = jobPostingFlags;

			return model;
		}

		#endregion

		#region Bind Methods

		/// <summary>
		/// Binds the controls.
		/// </summary>
		/// <param name="model">The model.</param>
		private void BindControls(JobWizardViewModel model)
		{
			Model = model;
		  MainSiteLocation = BuildWorkLocation(model.BusinessUnitAddress);
      BindWorkLocationDropDown(MainSiteLocation.Location);
		}

		/// <summary>
		/// Binds the work location drop down.
		/// </summary>
		/// <param name="companyAddress">The company address.</param>
		private void BindWorkLocationDropDown(string companyAddress)
		{
			WorkLocationDropDown.Items.Clear();

			WorkLocationDropDown.Items.AddEnum(JobLocationTypes.MainSite, companyAddress);
			WorkLocationDropDown.Items.Add(new ListItem(CodeLocalise("NewOrMultipleLocations.Text", "New or multiple locations"), NewOrMultipleLocationsValue));
			WorkLocationDropDown.Items.Add(new ListItem(CodeLocalise("AllLocations.Text", "All currently listed locations"), AllLocationsValue));

			if (App.Settings.NoFixedLocation)
				WorkLocationDropDown.Items.AddEnum(JobLocationTypes.NoFixedLocation, "No fixed location");
		}

		#endregion

		#region Helper Methods

		/// <summary>
		/// Registers the javascript.
		/// </summary>
		private void RegisterJavascript()
		{
			// Build the JS
			var js =
        @"function pageLoad() {
	      $(document).ready(function()
	      {
          if ($(""#" + FederalContractorExpirationDateIsValid.ClientID + @""").exists()) {
				    ValidatorEnable($(""#" + FederalContractorExpirationDateIsValid.ClientID + @""")[0], $(""#" + FederalContractorCheckBox.ClientID + @""").is("":checked""));
				    ValidatorEnable($(""#" + FederalContractorExpirationDateGreaterThanTodayCompareValidator.ClientID + @""")[0], $(""#" + FederalContractorCheckBox.ClientID + @""").is("":checked""));
			    }
        ";

		  if (App.Settings.FederalContratorExpiryDateMandatory)
		  {
        js += @"if ($(""#" + FederalContractorExpirationDateRequired.ClientID + @""").exists()) {
            ValidatorEnable($(""#" + FederalContractorExpirationDateRequired.ClientID + @""")[0], $(""#" + FederalContractorCheckBox.ClientID + @""").is("":checked""));
          }
          ";
		  }

		  js += @"// Add onclick handler to terms checkbox 
		      $(""#" + FederalContractorCheckBox.ClientID + @""").click(function()
          {	
		        // If checked
		        if ($(""#" + FederalContractorCheckBox.ClientID + @""").is("":checked""))
            {
			        $(""#FederalContractorExpirationDateDiv"").show();
            }
		        else
            {
			        $(""#FederalContractorExpirationDateDiv"").hide();
            }";

		  if (App.Settings.FederalContratorExpiryDateMandatory)
		  {
        js += @"if ($(""#" + FederalContractorExpirationDateRequired.ClientID + @""").exists()) {
            ValidatorEnable($(""#" + FederalContractorExpirationDateRequired.ClientID + @""")[0], $(""#" + FederalContractorCheckBox.ClientID + @""").is("":checked""));
          }";
		  }

      js += @"ValidatorEnable($(""#" + FederalContractorExpirationDateIsValid.ClientID + @""")[0], $(""#" + FederalContractorCheckBox.ClientID + @""").is("":checked""));
		        ValidatorEnable($(""#" + FederalContractorExpirationDateGreaterThanTodayCompareValidator.ClientID + @""")[0], $(""#" + FederalContractorCheckBox.ClientID + @""").is("":checked""));
          });

		      if ($(""#" + FederalContractorCheckBox.ClientID + @""").is("":checked""))
          {
			      $(""#FederalContractorExpirationDateDiv"").show();
          }
		      else
          {
			      $(""#FederalContractorExpirationDateDiv"").hide();
		      }

		      // Apply jQuery to infield labels
		      $("".inFieldLabel > label"").inFieldLabels();
	      });
      }

      function validateFederalContractorExpirationDate(){
      }

      function validateLocations(oSrc, args){
	      var isValid = true;
	      if(args.Value == """ + NewOrMultipleLocationsValue + @""" || args.Value == """ + AllLocationsValue + @"""){
		      isValid = ($(""#LocationSelectorHasRowsHidden"").val() == 'true');
	      }
	      args.IsValid = isValid;
      }

      function validateJobConditions (oSrc, args){
        args.IsValid = ($(""#FederalContractorCheckBox"").is("":checked"") ||
									      $(""#ForeignLaborCertificationCheckBox"").is("":checked"") ||
                         $(""#ForeignLaborCertificationCheckBoxH2A"").is("":checked"") ||
                          $(""#ForeignLaborCertificationCheckBoxH2B"").is("":checked"") ||
                           $(""#ForeignLaborCertificationCheckBoxOther"").is("":checked"") ||
									          $(""#CourtOrderedAffirmativeActionCheckBox"").is("":checked""));
      }

      function validateFederalForeignCertification (oSrc, args){
        var contractorChecked = $(""#" + FederalContractorCheckBox.ClientID + @""").is("":checked"");
        var certChecked = $(""#" + ForeignLaborCertificationCheckBox.ClientID + @""").is("":checked"");

	      args.IsValid = !(contractorChecked && certChecked);
      }";

			ScriptManager.RegisterClientScriptBlock(this, typeof (JobWizardStep5), "JobWizardStep5JavaScript", js, true);
		}

		#endregion

		#region Localise the UI

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			PublicTransitAccessibleCheckBox.Text = CodeLocalise("PublicTransitAccessibleCheckBox.Text", "Public transit accessible");
      HomeBasedWorkerCheckbox.Text = CodeLocalise("HomeBasedWorkerCheckbox.Text", "Suitable for home-based job seekers?");

			FederalContractorCheckBox.Text = CodeLocalise("FederalContractorCheckBox.Text", "Federal contractor");
			ForeignLaborCertificationCheckBox.Text = CodeLocalise("ForeignLaborCertiﬁcationCheckbox.Text", "Foreign labor certification types");
			ForeignLaborRadioButtonList.Items[0].Text = CodeLocalise("ForeignLaborCertiﬁcationH2A.Text", "Foreign labor certiﬁcation H2A agriculture");
			ForeignLaborRadioButtonList.Items[1].Text = CodeLocalise("ForeignLaborCertiﬁcationH2B.Text", "Foreign labor certiﬁcation H2B non-agriculture");
			ForeignLaborRadioButtonList.Items[2].Text = CodeLocalise("ForeignLaborCertiﬁcationOther.Text", "Foreign labor certiﬁcation other");

			ForeignLaborCheckBoxValidator.ErrorMessage = CodeLocalise("ForeignLaborCheckBoxValidator.ErrorMessage", "Please select a foreign labor certification type.");

			CourtOrderedAffirmativeActionCheckBox.Text = CodeLocalise("CourtOrderedAfﬁrmativeActionCheckBox.Text", "Court-ordered affirmative action");

			ExFelonsCheckBox.Text = CodeLocalise("ExFelonsCheckBox.Text", "Ex-felons");
			DesignatedCommunityResidentsCheckBox.Text = CodeLocalise("DesignatedCommunityResidentsCheckBox.Text", "EZ and RRC residents");
			LongTermFamilyAssistanceRecipientsCheckBox.Text = CodeLocalise("LongTermFamilyAssistanceRecipientsCheckBox.Text", "TANF (long-term) recipients");
			SSIRecipientsCheckBox.Text = CodeLocalise("SSIRecipientsCheckBox.Text", "SSI recipients");
			SummerYouthCheckBox.Text = CodeLocalise("SummerYouthCheckBox.Text", "Summer youth");
			SNAPRecipientsCheckBox.Text = CodeLocalise("SNAPRecipientsCheckBox.Text", "SNAP recipients");
			TANFRecipientsCheckBox.Text = CodeLocalise("TANFRecipientsCheckBox.Text", "TANF (short-term) recipients");

			VocationalRehabilitationCheckBox.Text = CodeLocalise("VocationalRehabilitationCheckBox.Text", "Vocational rehabilitation participants");
			VeteransCheckBox.Text = CodeLocalise("VeteransCheckBox.Text", "Veterans");

			AdvancedManufacturingCheckBox.Text = CodeLocalise("AdvancedManufacturing.Text", "Advanced Manufacturing");
			AerospaceCheckBox.Text = CodeLocalise("AerospaceCheckBox.Text", "Aerospace");
			HealthcareEmployerCheckBox.Text = CodeLocalise("HealthcareEmployerCheckBox.Text", "Healthcare");
			BusinessServicesCheckBox.Text = CodeLocalise("BusinessServicesCheckBox.Text", "Business Services");
			ConsultingCheckBox.Text = CodeLocalise("ConsultingCheckBox.Text", "Consulting");
			EnergyCheckbox.Text = CodeLocalise("EnergyCheckbox.Text", "Energy");
			HealthInformaticsCheckBox.Text = CodeLocalise("HealthInformaticsCheckBox.Text ", "Health Infomatics");
			ResearchDevelopmentCheckBox.Text = CodeLocalise("ResearchDevelopmentCheckBox.Text", "Research & Development");
			SMARTCheckBox.Text = CodeLocalise("SMARTCheckBox.Text", "SMART");
			TransportationCheckBox.Text = CodeLocalise("TransportationCheckBox.Text", "Transportation & Distribution");
			BiotechEmployerCheckBox.Text = CodeLocalise("BiotechEmployerCheckBox.Text", "Biotechnology");
			GreenJobCheckBox.Text = CodeLocalise("GreenJobCheckBox.Text", "Green");
			ITEmployerCheckBox.Text = CodeLocalise("ITEmployerCheckBox.Text", "Information Technology");

      FederalContractorExpirationDateTextBox.ToolTip = CodeLocalise("ClosingDateTextBox.ToolTip", "Please enter the expiration date as mm/dd/yyyy");
      FederalContractorExpirationDateRequired.ErrorMessage = CodeLocalise("FederalContractorExpirationDateValidator.ErrorMessage", "Federal contractor expiration date required");
			FederalContractorExpirationDateIsValid.ErrorMessage = CodeLocalise("FederalContractorExpirationDateTextBox.ErrorMessage", "Valid date is required");
			FederalContractorExpirationDateGreaterThanTodayCompareValidator.ErrorMessage = CodeLocalise("FederalContractorExpirationDateGreaterThanTodayCompareValidator.ErrorMessage", "Expiry date must be in the future");

			LocationSelectorValidator.ErrorMessage = CodeLocalise("LocationSelectorValidator.ErrorMessage", "At least one location is required");
			FederalForeignCertificationValidator.ErrorMessage = CodeLocalise("FederalForeignCertificationValidator.ErrorMessage", "Federal contractor/Foreign certification combination you have selected is not allowed");

      ExpandCollapseAllButton.Text = HtmlLocalise("ExpandAllButton.Text", "Expand all");

			JobConditionsHeaderPanel.Visible = !App.Settings.HideJobConditions;
			JobConditionsPanel.Visible = !App.Settings.HideJobConditions;

			AdvertiseFor30DaysText.Text = HtmlLocalise( "AdvertiseFor30DaysText.Text", "To comply with Federal Law, this job posting must be advertised for a <b>full 30 days</b>" );
		}

		#endregion

		/// <summary>
		/// Applies the branding.
		/// </summary>
		private void ApplyBranding()
		{
			WorkLocationHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			WorkLocationPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			WorkLocationPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			JobConditionsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			JobConditionsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			JobConditionsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			WorkOpportunitiesTaxCreditHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			WorkOpportunitiesTaxCreditPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			WorkOpportunitiesTaxCreditPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			JobPostingFlagsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			JobPostingFlagsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			JobPostingFlagsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}

		/// <summary>
		/// Applies the theme.
		/// </summary>
		private void ApplyTheme()
		{
			ExpandCollapseAllRow.Visible = App.Settings.Theme == FocusThemes.Workforce;

      WorkOpportunitiesTaxCreditPlaceHolder.Visible = !App.Settings.HideWOTC;
		}

		/// <summary>
		/// Enables the federal contractor validators.
		/// </summary>
		/// <param name="enable">if set to <c>true</c> [enable].</param>
		private void EnableFederalContractorValidators(bool enable)
		{
		  FederalContractorExpirationDateRequired.Enabled = App.Settings.FederalContratorExpiryDateMandatory && enable;
      FederalContractorExpirationDateIsValid.Enabled = FederalContractorExpirationDateGreaterThanTodayCompareValidator.Enabled = enable;
		}

		protected void LocationSelectorValidator_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			args.IsValid = (WorkLocationDropDown.SelectedValueToEnum(JobLocationTypes.MultipleLocations) != JobLocationTypes.MultipleLocations) || LocationSelector.JobLocationCount() > 0;
		}
	}
}