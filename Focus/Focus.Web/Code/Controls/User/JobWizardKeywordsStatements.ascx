  <%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobWizardKeywordsStatements.ascx.cs" Inherits="Focus.Web.Code.Controls.User.JobWizardKeywordsStatements" %>

<h1>
  <%= JobType.IsInternship()
        ? HtmlLocalise("Header", "Additional internship details")
        : HtmlLocalise("Header", "Additional job details")%>
</h1>
<i>
  <%= JobType.IsInternship()
        ? HtmlLocalise("SubHeader", "Other skills or knowledge sets for this internship include:")
        : HtmlLocalise("SubHeader", "Other skills or knowledge sets for this job include:")%>
</i>
<br />
<br />

<asp:UpdatePanel ID="KeywordsStatementsUpdatePanel" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
		<asp:Panel ID="AccordionPanel" runat="server">
			<act:Accordion ID="MyAccordion" runat="server" CssClass="accordion" HeaderCssClass="accordionTitle off"	HeaderSelectedCssClass="accordionTitle on"
										 ContentCssClass="accordionContent3" FadeTransitions="false" FramesPerSecond="40" TransitionDuration="150" AutoSize="None"	RequireOpenedPane="false" 
										 SuppressHeaderPostbacks="true" SelectedIndex="0">
				<Panes>
					<act:AccordionPane ID="KeywordsAccordionPane" runat="server">
						<Header><span class="plusMinusImage"></span><span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("KeywordsAccordionPane.Title", "Keywords")%></a></span></Header>
						<Content>
							<asp:Panel runat="server" ID="KeywordsPanel" Height="125px" ScrollBars="Auto">
								<asp:CheckBoxList ID="KeywordsCheckBoxList" runat="server" />						
							</asp:Panel>
						</Content>
					</act:AccordionPane>
					<act:AccordionPane ID="StatementsAccordionPane" runat="server">
						<Header><span class="plusMinusImage"></span><span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("StatementsAccordionPane.Title", "Statements")%></a></span></Header>
						<Content>
							<asp:Panel ID="StatementsPanel" runat="server" Height="125px" ScrollBars="Auto">
								<asp:CheckBoxList ID="StatementsCheckBoxList" runat="server" CellPadding="2" CellSpacing="5"/>
							</asp:Panel>
						</Content>
					</act:AccordionPane>
				</Panes>
			</act:Accordion>
		</asp:Panel>		
	</ContentTemplate>
	<Triggers>
		<asp:AsyncPostBackTrigger ControlID="RefreshButton" EventName="Click" />		
	</Triggers>
</asp:UpdatePanel>
<br />
<div style="float:left"><asp:Button ID="AddButton" runat="server" class="button3" /></div>
<div style="float:right"><asp:Button ID="RefreshButton" runat="server" class="button3" OnClick="RefreshButton_Click" /></div>
<div style="clear:both"></div>


