﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models;

using Framework.Core;

#endregion


namespace Focus.Web.Code.Controls.User
{
	public partial class BusinessUnitSelector : UserControlBase
	{
		private bool AllowExistingBusinessUnitSelection
		{
			get { return GetViewStateValue("BusinessUnitSelector:AllowExistingBusinessUnitSelection", true); }
			set { SetViewStateValue("BusinessUnitSelector:AllowExistingBusinessUnitSelection", value); }
		}

		private long EmployerId
		{
			get { return GetViewStateValue<long>("BusinessUnitSelector:EmployerId"); }
			set { SetViewStateValue("BusinessUnitSelector:EmployerId", value); }
		}

		private BusinessUnitModel _model
		{
			get { return GetViewStateValue<BusinessUnitModel>("BusinessUnitSelector: Model"); }
			set { SetViewStateValue("BusinessUnitSelector: Model", value); }
		}

		private string LegalName
		{
			get { return GetViewStateValue<string>("BusinessUnitSelector: LegalName"); }
			set { SetViewStateValue("BusinessUnitSelector: LegalName", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
				LocaliseUI();

			HandleAccountTypeDisplay();
		  LegalNamePlaceHolder.Visible = App.Settings.Theme == FocusThemes.Workforce;
		}

		private void HandleAccountTypeDisplay()
		{
			if (App.Settings.AccountTypeDisplayType == ControlDisplayType.Hidden || App.Settings.Theme == FocusThemes.Education)
			{
				AccountTypeROLabel.Visible = false;
				AccountTypeRow.Visible = false;
			}
		}

		/// <summary>
		/// Shows the specified employer id.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <param name="allowExistingBusinessUnitSelection">Whether to show a business unit selector</param>
		/// <param name="legalName">Any existing legal name</param>
		public void Show(long employerId, bool allowExistingBusinessUnitSelection = true, string legalName = null)
		{
			EmployerId = employerId;
			AllowExistingBusinessUnitSelection = allowExistingBusinessUnitSelection;

			if (EmployerId < 1 && AllowExistingBusinessUnitSelection)
				throw new Exception("Employer ID required for this selector");

			if (!AllowExistingBusinessUnitSelection)
			{
				_model = new BusinessUnitModel
				{
					BusinessUnit = new BusinessUnitDto { EmployerId = EmployerId },
					BusinessUnitAddress = new BusinessUnitAddressDto(),
					BusinessUnitDescription = new BusinessUnitDescriptionDto { Title = CodeLocalise("EmployerDescription", "#BUSINESS# description") }
				};

				NewBusinessUnitPanel.CssClass = "";
				BusinessUnitEditor.Bind(employerId: EmployerId, legalName: legalName);
			}
			else
			{
				LegalName = legalName;
			}
		
			Bind();
			Reset();

			BusinessUnitSelectorModalPopup.Show();
			BusinessUnitSelectorModalPanel.CssClass = "modal";
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the BusinessUnitDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void BusinessUnitDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			var businessUnitId = BusinessUnitDropDown.SelectedValueToLong();

			if (businessUnitId <= 0)
			{
				_model = new BusinessUnitModel
				{
					BusinessUnit = new BusinessUnitDto { EmployerId = EmployerId },
					BusinessUnitAddress = new BusinessUnitAddressDto(),
					BusinessUnitDescription = new BusinessUnitDescriptionDto { Title = CodeLocalise("EmployerDescription", "#BUSINESS# description") }
				};

				ExistingBusinessUnitPanel.Visible = false;
				SelectButton.Visible = false;
				
				// We are adding a new business unit so clear out values
				ClearValues();

				if (businessUnitId == -1)
				{
					NewBusinessUnitPanel.Visible = true;
					NewBusinessUnitPanel.CssClass = "";
					BusinessUnitEditor.Bind(employerId: EmployerId, legalName: LegalName);
					AddButton.Visible = true;
				}
				else
				{
					NewBusinessUnitPanel.Visible = false;
					AddButton.Visible = false;
				}
			}
			else
			{
				_model = new BusinessUnitModel
				{
					BusinessUnit = ServiceClientLocator.EmployerClient(App).GetBusinessUnit(businessUnitId),
					BusinessUnitAddress = ServiceClientLocator.EmployerClient(App).GetPrimaryBusinessUnitAddress(businessUnitId),
					BusinessUnitDescription = ServiceClientLocator.EmployerClient(App).GetPrimaryBusinessUnitDescription(businessUnitId)
				};
				SetDisplayValues();
				NewBusinessUnitPanel.Visible = false;
				ExistingBusinessUnitPanel.Visible = true;
				SelectButton.Visible = true;
				AddButton.Visible = false;
			}

		  IndustrialClassificationRow.Visible = App.Settings.Theme == FocusThemes.Workforce;
		}

		/// <summary>
		/// Handles the Clicked event of the CancelButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CancelButton_Clicked(object sender, EventArgs e)
		{
			BusinessUnitSelectorModalPopup.Hide();
		}

		/// <summary>
		/// Handles the Clicked event of the SelectButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SelectButton_Clicked(object sender, EventArgs e)
		{
			BusinessUnitSelectorModalPopup.Hide();
			OnBusinessUnitSelected(new BusinessUnitSelectedEventArgs(_model));
		}

		protected void AddButton_Clicked(object sender, EventArgs e)
		{
			if (!Page.IsValid)
				return;
			
			_model = BusinessUnitEditor.Unbind();
			BusinessUnitSelectorModalPopup.Hide();
			OnBusinessUnitSelected(new BusinessUnitSelectedEventArgs(_model));
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			BindBusinessUnitDropDown();
		}

		/// <summary>
		/// Binds the business unit drop down.
		/// </summary>
		private void BindBusinessUnitDropDown()
		{
			if (EmployerId.IsNull() || EmployerId < 1) return;

			var businessUnits = ServiceClientLocator.EmployerClient(App).GetBusinessUnits(EmployerId);

			foreach (var businessUnit in businessUnits.Where(businessUnit => businessUnit.IsPrimary))
			{
				businessUnit.Name = String.Format("{0} ({1})", businessUnit.Name, CodeLocalise("Global.Primary.Text", "Primary"));
			}

			BusinessUnitDropDown.DataSource = businessUnits;
			BusinessUnitDropDown.DataValueField = "Id";
			BusinessUnitDropDown.DataTextField = "Name";
			BusinessUnitDropDown.DataBind();
			BusinessUnitDropDown.Items.AddLocalisedTopDefault("Global.Company.TopDefault", "- select a #BUSINESS#:LOWER -");
			BusinessUnitDropDown.Items.Add(new ListItem(CodeLocalise("AddNewEmployer.Text", "Add new #BUSINESS#:LOWER"), "-1"));
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			CancelButton.Text = CodeLocalise("CancelButton.Button", "Cancel");
			SelectButton.Text = CodeLocalise("SelectButton.Button", "Select");
			AddButton.Text = CodeLocalise("AddButton.Button", "Add & Select");
		}

		/// <summary>
		/// Clears the values.
		/// </summary>
		private void ClearValues()
		{
			NameROLabel.Text = "";
		  LegalNameLabel.Text = "";
			AddressLine1ROLabel.Text = "";
			AddressLine2ROLabel.Text = "";
			AddressCityROLabel.Text = "";
			AddressStateROLabel.Text = "";
			AddressCountyROLabel.Text = "";
			AddressPostcodeROLabel.Text = "";
			AddressCountryROLabel.Text = "";
			UrlROLabel.Text = "";
			PhoneNumberROLabel.Text = "";
			AlternatePhoneNumber1ROLabel.Text = "";
			AlternatePhoneNumber1ROLabel.Text = "";
			OwnershipTypeROLabel.Text = "";
			IndustrialClassificationROLabel.Text = "";
			CompanyDescriptionLabel.Text = "";
		}

		/// <summary>
		/// Sets the display values.
		/// </summary>
		private void SetDisplayValues()
		{
			NameROLabel.Text = _model.BusinessUnit.Name;
		  LegalNameLabel.Text = _model.BusinessUnit.LegalName;
			AddressLine1ROLabel.Text = _model.BusinessUnitAddress.Line1;
			AddressLine2ROLabel.Text = _model.BusinessUnitAddress.Line2;
			AddressCityROLabel.Text = _model.BusinessUnitAddress.TownCity;
			if (_model.BusinessUnitAddress.StateId.IsNotNull()) AddressStateROLabel.Text = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Id == _model.BusinessUnitAddress.StateId).Select(x => x.Text).SingleOrDefault();
			if (_model.BusinessUnitAddress.CountyId.IsNotNull()) AddressCountyROLabel.Text = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Counties).Where(x => x.Id == _model.BusinessUnitAddress.CountyId).Select(x => x.Text).SingleOrDefault();
			AddressPostcodeROLabel.Text = _model.BusinessUnitAddress.PostcodeZip;
			if (_model.BusinessUnitAddress.CountryId.IsNotNull()) AddressCountryROLabel.Text = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries).Where(x => x.Id == _model.BusinessUnitAddress.CountryId).Select(x => x.Text).SingleOrDefault();
			UrlROLabel.Text = _model.BusinessUnit.Url;
			PhoneNumberROLabel.Text = _model.BusinessUnit.PrimaryPhone;
			if (_model.BusinessUnit.PrimaryPhoneExtension.IsNotNullOrEmpty())
				PhoneNumberROLabel.Text += CodeLocalise("Extension.Text", " Ext: {0}", _model.BusinessUnit.PrimaryPhoneExtension);
			PhoneNumberROLabel.Text += string.Concat(" (", _model.BusinessUnit.PrimaryPhoneType, ")");
			if (_model.BusinessUnit.AlternatePhone1.IsNotNullOrEmpty())
				AlternatePhoneNumber1ROLabel.Text = string.Concat(_model.BusinessUnit.AlternatePhone1, " (", _model.BusinessUnit.AlternatePhone1Type, ")");
			if (_model.BusinessUnit.AlternatePhone2.IsNotNullOrEmpty())
				AlternatePhoneNumber1ROLabel.Text = string.Concat(_model.BusinessUnit.AlternatePhone2, " (", _model.BusinessUnit.AlternatePhone2Type, ")");
			if (_model.BusinessUnit.OwnershipTypeId.IsNotNull()) OwnershipTypeROLabel.Text = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.OwnershipTypes).Where(x => x.Id == _model.BusinessUnit.OwnershipTypeId).Select(x => x.Text).SingleOrDefault();
			if (_model.BusinessUnit.AccountTypeId.IsNotNull())
			{
				AccountTypeROLabel.Text = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.AccountTypes).Where(x => x.Id == _model.BusinessUnit.AccountTypeId).Select(x => x.Text).SingleOrDefault();
			}

			IndustrialClassificationROLabel.Text = _model.BusinessUnit.IndustrialClassification;
			CompanyDescriptionLabel.Text = _model.BusinessUnitDescription.IsNull() ? string.Empty : _model.BusinessUnitDescription.Description;
		}

		/// <summary>
		/// Resets this instance.
		/// </summary>
		private void Reset()
		{
			ExistingBusinessUnitPanel.Visible = false;
			ClearValues();
      
      if (BusinessUnitDropDown.Items.Count > 0)
			  BusinessUnitDropDown.SelectedIndex = 0;

			SelectButton.Visible = false;
			if(AllowExistingBusinessUnitSelection)
			{
				NewBusinessUnitPanel.Visible = false;
				AddButton.Visible = false;
			}
			else
			{
				SelectorPanel.Visible = false;
				NewBusinessUnitPanel.Visible = true;
				AddButton.Visible = true;
			}
		}

		#region Events

		public event BusinessUnitSelectedHandler BusinessUnitSelected;

		/// <summary>
		/// Raises the <see cref="E:BusinessUnitSelected"/> event.
		/// </summary>
		/// <param name="e">The <see cref="Focus.Web.Code.Controls.User.BusinessUnitSelector.BusinessUnitSelectedEventArgs"/> instance containing the event data.</param>
		protected virtual void OnBusinessUnitSelected(BusinessUnitSelectedEventArgs e)
		{
			if (BusinessUnitSelected != null)
				BusinessUnitSelected(this, e);
		}

		#endregion

		#region Delegates

		public delegate void BusinessUnitSelectedHandler(object o, BusinessUnitSelectedEventArgs e);
		
		#endregion

		#region EventArgs

		/// <summary>
		/// 
		/// </summary>
		public class BusinessUnitSelectedEventArgs : EventArgs
		{
			public readonly BusinessUnitModel Model;

			public BusinessUnitSelectedEventArgs(BusinessUnitModel model)
			{
				Model = model;
			}
		}

		#endregion
		
	}
}