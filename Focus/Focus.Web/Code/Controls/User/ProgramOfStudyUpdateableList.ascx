﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProgramOfStudyUpdateableList.ascx.cs" Inherits="Focus.Web.Code.Controls.User.ProgramOfStudyUpdateableList" %>
<%@ Register TagPrefix="uc" TagName="UpdateablePairs" Src="~/Code/Controls/User/UpdateablePairs.ascx" %>

<asp:UpdatePanel runat="server" ID="SelectProgramsOfStudyUpdatePanel" UpdateMode="Conditional">
<ContentTemplate>
  <div id="ProgramListDiv" runat="server" class="vertical">
		<asp:Literal runat="server" ID="ProgramsOfStudyMessage"></asp:Literal>
    <asp:PlaceHolder runat="server" ID="CustomValidatorPanels" Visible="False">
      <asp:CustomValidator runat="server" ID="ProgramOfStudyNotRequiredValidator" EnableClientScript="true" CssClass="error" ClientValidationFunction="ProgramOfStudyNotRequired_ValidateNotChecked" Display="Dynamic" />
      <asp:CustomValidator runat="server" ID="ProgramsOfStudyUpdateablePairsValidator" EnableClientScript="true" CssClass="error" ClientValidationFunction="ProgramsOfStudyUpdateablePairs_ValidateSelected" Display="Dynamic" />
    </asp:PlaceHolder>
		<uc:UpdateablePairs ID="ProgramsOfStudyUpdateablePairs" runat="server" Width="470px" RestrictDuplicates="True"/>
	</div>
	<div id="ProgramDropdownDiv" runat="server" class="vertical" style="width: 540px">
	  <span id="ProgramOfStudySelector" clientidmode="Static">
			<table width="100%">
			  <asp:PlaceHolder runat="server" ID="ProgramsOfStudyRequiredPanel" Visible="False">
			    <tr>
			      <td>
			        <asp:CheckBox runat="server" ID="ProgramOfStudyNotRequiredCheckBox"/>
    	      </td>
			    </tr>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="ProgramAreaPlaceHolder">
				<tr>
					<td>
						<asp:DropDownList runat="server" ID="StudyProgramAreaDropDownList" Width="480" />
					</td>
					<td></td>
				</tr>
        </asp:PlaceHolder>
				<tr>
					<td>
						<asp:DropDownList runat="server" ID="StudyProgramAreaDegreesDropDownList" Width="480" />
            <br />
					  <asp:label ID="ProgramAreaMessageLabel" runat="server" CssClass="error" EnableViewState="False"></asp:label>
					</td>
					<td style="vertical-align: top"><asp:Button ID="AddProgramOfStudyButton" runat="server" class="button3" ClientIDMode="Static" onClick="AddProgramOfStudyButton_Click" CausesValidation="False" /></td>
				</tr>
			</table>
		</span>
	  <asp:HiddenField runat="server" ClientIDMode="Static" ID="ProgramOfStudyDegreeId" Value=""/><br/>
  </div>
	
  <script type="text/javascript">
    $(document).ready(function() {
      if ($("#<%=StudyProgramAreaDropDownList.ClientID %>").exists()) {
        <%=StudyProgramAreaDropDownList.ClientID %>_ControlLoad();
      }
    });
    
    $(document).ready(function () {
      if ($("#<%=StudyProgramAreaDropDownList.ClientID %>").exists()) {
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(<%=StudyProgramAreaDropDownList.ClientID %>_ControlLoad);
      }
    });

    function ProgramOfStudyNotRequired_ValidateNotChecked(sender, args) {
      var degreesSelected = $("#<%=ProgramListDiv.ClientID %>").find("input[type='image']").length;
      args.IsValid = !degreesSelected || !$("#<%=ProgramOfStudyNotRequiredCheckBox.ClientID %>").is(':checked');
    }
    
    function ProgramsOfStudyUpdateablePairs_ValidateSelected(sender, args) {
      var degreesSelected = $("#<%=ProgramListDiv.ClientID %>").find("input[type='image']").length;
      args.IsValid = degreesSelected || $("#<%=ProgramOfStudyNotRequiredCheckBox.ClientID %>").is(':checked');
    }
  </script>
</ContentTemplate>
</asp:UpdatePanel>
					
						
