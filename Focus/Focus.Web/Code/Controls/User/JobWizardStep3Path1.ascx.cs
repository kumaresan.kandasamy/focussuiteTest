﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Web.ViewModels;

using AjaxControlToolkit;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
  public partial class JobWizardStep3Path1 : JobWizardControl
	{
	  private JobTypes _jobType;

    /// <summary>
    /// Text for the Upload button
    /// </summary>
	  protected string UploadText
	  {
      get
      {
        return _jobType.IsInternship()
                 ? HtmlLocalise("UploadInternshipDescriptionButton.Text", "Upload an internship description")
                 : HtmlLocalise("UploadJobDescriptionButton.Text", "Upload a job description");
      }
	  }

	  private bool _resetDescription;

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
		  if (!IsPostBack)
		    LocaliseUI();

			ApplyBranding();

		  UploadPlaceHolder.Visible = App.Settings.Theme == FocusThemes.Workforce;
		  JobDescriptionHelp.Visible = App.Settings.Theme != FocusThemes.Workforce;
		}

		/// <summary>
		/// Apply client-specific branding as required.
		/// </summary>
		private void ApplyBranding()
		{
			//UploadProgressImage.ImageUrl = UrlBuilder.ProgressImage();
		}

		/// <summary>
		/// Handles the PreRender event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_PreRender(object sender, EventArgs e)
		{
			RegisterJavascript();
		}

    /// <summary>
    /// Fires when the Reset Description button is clicked to reset the description
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ResetDescriptionButton_OnClick(object sender, EventArgs e)
    {
      // JobDescriptionTextBox.Text = "";
			_resetDescription = true;
      OnGoToStep(new CommandEventArgs("GOTOSTEP", 2));
    }

		/// <summary>
		/// Registers the javascript.
		/// </summary>
		private void RegisterJavascript()
		{
			// Build the JS
			const string js = @"function pageLoad() { $('.inFieldLabel > label, .inFieldLabelAlt > label').inFieldLabels(); }";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "JobWizardStep3Path1Javascript", js, true);
		}

		#region localise the UI

    /// <summary>
    /// Localises text on the page
    /// </summary>
    private void LocaliseUI()
    {
      ResetDescriptionButton.Text = HtmlLocalise("ResetDescriptionButton.Text", "Restart job description");
			JobDecriptionHtmlValidator.ErrorMessage = CodeLocalise("JobDecriptionHtmlValidator.ErrorMessage", "Job description must not contain HTML mark-up");
		}

		/// <summary>
		/// Localises the UI
		/// </summary>
		private void LocaliseBasedOnJobType()
		{
      if (_jobType.IsInternship())
      {
        TabDescriptionLabel.Text = HtmlLocalise("TabInternshipDescription.Label", "internship description");
        TabSimilarLabel.Text = HtmlLocalise("TabSimilarInternship.Label", "Description for similar internships");
        JobDescriptionRequired.ErrorMessage = CodeLocalise("InternshipDescriptionRequired.ErrorMessage", "Internship description required");
        JobDescriptionInfieldLabel.Text = HtmlInFieldLabel("JobDescriptionTextBox", "InternshipDescription.Label",
                                                           "Type or paste your internship description here.", 350, "inFieldLabelAlt");
      }
      else
      {
        TabDescriptionLabel.Text = HtmlLocalise("TabJobDescription.Label", "job description");
        TabSimilarLabel.Text = HtmlLocalise("TabSimilarJob.Label", "Description for similar jobs");
        JobDescriptionRequired.ErrorMessage = CodeLocalise("JobDescriptionRequired.ErrorMessage", "Job description required");
        JobDescriptionInfieldLabel.Text = HtmlInFieldLabel("JobDescriptionTextBox", "JobDescription.Label",
                                                           "Type or paste your job description here.", 300, "inFieldLabelAlt");
      }

      JobDescriptionHelp.Text = HtmlLocalise("JobDescriptionHelp.Text", "The job description and preferred requirements can be entered or pasted into the job description text area.");
      JobDescriptionValidator.ErrorMessage = CodeLocalise("JobDescriptionValidator.ErrorMessage", "Your description cannot exceed 6000 characters. Please amend");
		}

		#endregion

		#region Bind & Unbind Methods

		/// <summary>
		/// Binds the controls for the step.
		/// </summary>
		/// <param name="model">The model.</param>
		internal void BindStep(JobWizardViewModel model)
		{
		  _jobType = model.Job.JobType;
		  model.Job.DescriptionPath = 1;
      LocaliseBasedOnJobType();

			JobTitleLabel.Text = model.Job.JobTitle;
			JobDescriptionTextBox.Text = model.ResetDescription ? string.Empty : model.Job.Description;

			if (model.ResetDescription)
				model.ResetDescription = false;

		  var jobTypes = _jobType.IsInternship()
		                   ? new List<JobTypes> { JobTypes.InternshipPaid, JobTypes.InternshipUnpaid }
		                   : new List<JobTypes> { JobTypes.Job };

      JobDescriptionDropDownList.DataSource = ServiceClientLocator.EmployerClient(App).GetEmployerJobs(model.Job.EmployerId, jobTypes);
			JobDescriptionDropDownList.DataValueField = "Id";
			JobDescriptionDropDownList.DataTextField = "Text";
			JobDescriptionDropDownList.DataBind();

      if (_jobType.IsInternship())
        JobDescriptionDropDownList.Items.AddLocalisedTopDefault("JobDescriptionDropDownList.TopDefault", "- select existing internship description -");
      else
        JobDescriptionDropDownList.Items.AddLocalisedTopDefault("InternshipDescriptionDropDownList.TopDefault", "- select existing job description -");

			// Set up Keywords / Statements with the items they will need to populate correctly
			KeywordsStatements.OnetId = model.Job.OnetId;
			KeywordsStatements.JobTitle = model.Job.JobTitle;
		  KeywordsStatements.JobType = model.Job.JobType;

		  if (model.Job.JobType != JobTypes.Job && model.Job.Id != null)
		  {
        var skills = ServiceClientLocator.JobClient(App).GetEducationInternshipSkills((long) model.Job.Id);
		    KeywordsStatements.SkillsIds = skills.Select(x => x.EducationInternshipSkillId).ToList();
		  }

		  // Refresh the Keywords and Statements based on the settings
			KeywordsStatements.Refresh();

		  SimilarJobs.JobType = model.Job.JobType;
		  SimilarJobs.ClearList();
		}

		/// <summary>
		/// Unbinds the step.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
		internal JobWizardViewModel UnbindStep(JobWizardViewModel model)
		{
			if (!_resetDescription)
				model.Job.Description = JobDescriptionTextBox.Text.RemoveRogueCharacters();

			model.ResetDescription = _resetDescription;

			return model;
		}

		#endregion

		#region Ajax Events

		/// <summary>
		/// Handles the SelectedIndexChanged event of the JobDescriptionDropDownList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void JobDescriptionDropDownList_SelectedIndexChanged(object sender, EventArgs e)
		{
			var jobdescriptionId = JobDescriptionDropDownList.SelectedValueToNullableLong();
			if (jobdescriptionId == null) return;

			JobDescriptionTextBox.Text = ServiceClientLocator.JobClient(App).GetJobDescription(jobdescriptionId.Value);
		}

		/// <summary>
		/// Handles the UploadedComplete event of the UploadJobDescriptionAsyncFileUpload control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="AjaxControlToolkit.AsyncFileUploadEventArgs"/> instance containing the event data.</param>
		public void UploadJobDescriptionAsyncFileUpload_UploadedComplete(object sender, AsyncFileUploadEventArgs e)
		{
			var bytes = UploadJobDescriptionAsyncFileUpload.FileBytes;
			var fileName = UploadJobDescriptionAsyncFileUpload.FileName;
			var contentType = UploadJobDescriptionAsyncFileUpload.ContentType;

			try
			{
			  var text = ServiceClientLocator.SearchClient(App).ExtractJobDescription(bytes, fileName, contentType);
        if (contentType.Equals("application/pdf") || fileName.ToLowerInvariant().EndsWith(".rtf"))
			  {
			    text = Regex.Replace(text, @"([^\r\n\-])\r\n(\w)", "$1 $2");
          text = Regex.Replace(text, @"(\-)\r\n(\w)", "$1$2");
          text = Regex.Replace(text, @"(\w)\r\n(\-)", "$1$2");
			  }

			  if (text.EndsWith("\r\n"))
			    text = text.TrimEnd(new[] {'\r', '\n'});

			  App.SetSessionValue(Constants.StateKeys.UploadedJobDescription, text);
			}
			catch (Exception)
			{
				App.RemoveSessionValue(Constants.StateKeys.UploadedJobDescription);
			}			
		}

		#endregion
	}
}