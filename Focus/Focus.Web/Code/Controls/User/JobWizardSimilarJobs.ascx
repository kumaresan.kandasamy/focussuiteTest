<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobWizardSimilarJobs.ascx.cs" Inherits="Focus.Web.Code.Controls.User.JobWizardSimilarJobs" %>
<%@ Import Namespace="Focus.Core.Views" %>
<i>
  <asp:Literal runat="server" ID="Instructions1Literal"></asp:Literal>
  <asp:Literal runat="server" ID="Instructions2Literal"></asp:Literal>
</i>
<br />
<br />
<asp:Literal runat="server" ID="SearchLiteral"></asp:Literal>&nbsp;&nbsp;
<asp:TextBox runat="server" ID="SearchTextBox" Width="240" />&nbsp;&nbsp;
<asp:Button ID="SearchButton" runat="server" class="button3" OnClick="SearchButton_Click" CausesValidation="false" />
<br />
<br />

<asp:UpdatePanel ID="SimilarJobsUpdatePanel" runat="server">
	<ContentTemplate>
		<table id="SimilarJobsHeaderTable" runat="server" width="100%" visible="false">
			<tr>
				<td style="width: 100%;"><asp:Literal runat="server" ID="JobTitleColumn"></asp:Literal></td>
			</tr>
		</table>

		<asp:ListView ID="SimilarJobsListView" runat="server" DataSourceID="SimilarJobsDataSource" ItemPlaceholderID="SimilarJobsListViewPlaceHolder"
									OnItemDataBound="SimilarJobsListView_ItemDataBound" Visible="False" >
			<LayoutTemplate>
				<asp:PlaceHolder ID="SimilarJobsListViewPlaceHolder" runat="server" />
			</LayoutTemplate>

			<ItemTemplate>
				<asp:Panel ID="SimilarJobsHeaderPanel" runat="server" CssClass="singleAccordionTitle">					
					<div style="width:100%">
						<asp:Image ID="SimilarJobsHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" AlternateText="."/>
            <a href="#">
              <span data-column="JobTitle"><%# ((SimilarJobView)Container.DataItem).JobTitle%></span>
            </a>
					</div>
				</asp:Panel>
				<asp:Panel ID="SimilarJobsPanel" runat="server">
					<div style="width:100%">
						<asp:TextBox ID="SimilarJobDescriptionTextBox" runat="server" CssClass="similarJobDescriptionTextBox" TextMode="MultiLine" ReadOnly="true" Rows="10" ClientIDMode="Static" Text='<%# ((SimilarJobView)Container.DataItem).JobDescription%>' />
						<br />
            <asp:Label runat="server" ID="SelectionErrorLabel" class="error addSelectionValidator" style="display: none"></asp:Label>
            <br />
						<asp:Button ID="AddSelectionButton" runat="server" class="button3" />
					  <br />
					</div>
				</asp:Panel>
				<act:CollapsiblePanelExtender ID="SimilarJobsPanelExtender" runat="server" TargetControlID="SimilarJobsPanel" ExpandControlID="SimilarJobsHeaderPanel" 
																				CollapseControlID="SimilarJobsHeaderPanel" Collapsed="true" ImageControlID="SimilarJobsHeaderImage" 
																				CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
																				SuppressPostBack="true" />
			</ItemTemplate>

			<EmptyDataTemplate>
				<%= (!Page.IsPostBack ? HtmlLocalise("SimilarJobsListView.EmptyText", "") : HtmlLocalise("SimilarJobsListView.NotFoundText", "The search didn't find any matching #POSTINGTYPES#.").ToSentenceCase()) %>
			</EmptyDataTemplate>

		</asp:ListView>
		<asp:ObjectDataSource ID="SimilarJobsDataSource" runat="server" TypeName="Focus.Web.Code.Controls.User.JobWizardSimilarJobs" 
      SelectMethod="GetSimilarJobs" OnSelecting="SimilarJobsDataSource_Selecting  ">
			<SelectParameters>
				<asp:ControlParameter ControlID="SearchTextBox" ConvertEmptyStringToNull="true" Type="String" PropertyName="Text" Name="search" />
       </SelectParameters>
		</asp:ObjectDataSource>			
	</ContentTemplate>

	<Triggers>
		<asp:AsyncPostBackTrigger ControlID="SearchButton" EventName="Click" />
	</Triggers>
</asp:UpdatePanel>

<script type="text/javascript">
	$(document).ready(function () {
		$('body').mouseup(function () {
			GetSelectedText('similarJobDescriptionTextBox');
		});
	});

function JobWizardSimilarJobs_HideSelectionValidators() {
  $(".addSelectionValidator").hide();
}
</script>
