﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Framework.Core;

namespace Focus.Web.Code.Controls.User
{
  public partial class SearchCriteriaLanguage : UserControlBase
	{
		public string ServicePath
		{
			set { aceLanguage.ServicePath = value; }
		}

		public string ServiceMethod
		{
			set { aceLanguage.ServiceMethod = value; }
		}
    
    public string LanguageTextboxWidth { get; set; }
    public string LanguageProficiencyDropdownWidth { get; set; }
    public string LanguageTextboxRightMargin { get; set; }
    public bool ProficiencyLevelAndBelow { get; set; }
    public string InstructionalText { get; set; }
    public bool ReverseProficiencyOrder { get; set; }

    /// <summary>
    /// Handles the Load event of the control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	  protected void Page_Load(object sender, EventArgs e)
		{
	    if (!Page.IsPostBack)
	    {
	      BindProficiencyDropDown();
	      LocaliseUi();
	      SetControls();
	    }
		}

    /// <summary>
    /// Localises the UI.
    /// </summary>
		private void LocaliseUi()
		{
      LanguageTextBoxValidator.ErrorMessage = CodeLocalise("LanguageTextBoxValidator.ErrorMessage", "Duplicate languages types not allowed");
			LanguageProficiencyRequired.ErrorMessage = CodeLocalise("LanguageProficiency.RequiredErrorMessage", "Language proficiency level missing");
			ProficiencyMissing.ErrorMessage = CodeLocalise("LanguageProficiencyMissing.Error","Please update your languages to indicate your proficiency.");
		}

    /// <summary>
    /// Set the public control properties
    /// </summary>
    private void SetControls()
    {
      // Control Widths
      LanguageTextBox.Width = LanguageTextboxWidth == null ? 250 : Unit.Parse(LanguageTextboxWidth);
      LanguageLabel.Width = LanguageTextBox.Width;
      ddlLanguageProficiency.Width = LanguageProficiencyDropdownWidth == null
        ? 250
        : Unit.Parse(LanguageProficiencyDropdownWidth);
      LanguageProficiencyLabel.Width = ddlLanguageProficiency.Width;

      // Margin between Language and Proficiency controls 
      if (LanguageTextboxRightMargin.IsNotNullOrEmpty())
        LanguageTextBox.Style.Add("margin-right", LanguageTextboxRightMargin);

      // Instruction Text
      if (InstructionalText.IsNotNullOrEmpty()) LanguagesInstructionsLabel.DefaultText = InstructionalText;
    }

    /// <summary>
    /// Unbind the Languages Updateable List control (get items).
    /// </summary>
    public LanguageCriteria Unbind()
    {
      if (LanguagesUpdateableList.Items.IsNotNullOrEmpty())
      {
        var languages = LanguagesUpdateableList.Items.Select(l => new LanguageProficiency()
        {
          Language = l.Value[0],
          Proficiency = l.Key != string.Empty ? Convert.ToInt64(l.Key) : 0
        }).ToList();

        return new LanguageCriteria
        {
          LanguagesWithProficiencies = languages,
          LanguageSearchType = SearchLanguagesAnd.Checked
        };
      }

      return null;

    }

    /// <summary>
    /// Binds the Languages Updateable List control.
    /// </summary>
    /// <param name="languageInfo">LanguageCriteria Class</param>
    public void Bind(LanguageCriteria languageInfo)
    {
      if (!languageInfo.LanguagesWithProficiencies.IsNullOrEmpty())
      {
        var proficiencies =
          ServiceClientLocator.CoreClient(App)
            .GetLookup(LookupTypes.LanguageProficiencies)
            .ToDictionary(l => (long?) l.Id, l => l.Text);

        LanguagesUpdateableList.Items =
          languageInfo.LanguagesWithProficiencies.Select(l => new KeyValuePair<string, string[]>
            (l.Proficiency.ToString(),
              new string[2]
              {
                l.Language,
                l.Proficiency.IsNotNull()
                  ? ProficiencyLevelAndBelow
                    ? string.Format("{0} - {1}", l.Language,
                      CodeLocalise("OrBelow.Text", "{0} or below", proficiencies[l.Proficiency]))
                    : string.Format("{0} - {1}", l.Language, proficiencies[l.Proficiency])
                  : l.Language
              }
            )).ToList();

        // Radio Buttons
        SearchLanguagesAnd.Checked = languageInfo.LanguageSearchType;
        SearchLanguagesOr.Checked = !languageInfo.LanguageSearchType;
      }
    }

    /// <summary>
    /// Handles the Click event of the AddLanguageButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void LanguageAddButton_Click(object sender, EventArgs e)
		{
			if (LanguageTextBox.TextTrimmed().IsNotNullOrEmpty())
			{
				if (ddlLanguageProficiency.SelectedIndex > 0)
				{
          LanguageTextBoxValidator.IsValid = LanguagesUpdateableList.AddItem(new KeyValuePair<string, string[]>(
            ddlLanguageProficiency.SelectedValue,
            new string[2]
            {
              LanguageTextBox.TextTrimmed(),
              string.Format("{0} - {1}", LanguageTextBox.TextTrimmed(), ddlLanguageProficiency.SelectedItem.Text)
            }
            ));

          // Duplicate language
          if (LanguageTextBoxValidator.IsValid)
          {
            LanguageTextBox.Text = "";
            ddlLanguageProficiency.SelectedIndex = 0;
          }
				}
				else
				{
          // Language Proficiency required
          LanguageTextBox.Text = "";
          LanguageProficiencyRequired.IsValid = false;				  
				}
			}
		}

    /// <summary>
    /// Binds the Language Proficiency drop down control.
    /// </summary>
		private void BindProficiencyDropDown()
		{
		  var languageProficiencies = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.LanguageProficiencies)
		    .Select(l => new LookupItemView
		    {
		      Id = l.Id,
		      Text = ProficiencyLevelAndBelow && l.DisplayOrder > 1 ? CodeLocalise("OrBelow.Text", "{0} or below", l.Text) : l.Text
		    }).ToList();

		  if (ReverseProficiencyOrder) languageProficiencies.Reverse();

			ddlLanguageProficiency.DataTextField = "Text";
			ddlLanguageProficiency.DataValueField = "Id";
			ddlLanguageProficiency.DataSource = languageProficiencies;
			ddlLanguageProficiency.DataBind();
			ddlLanguageProficiency.Items.Insert(0, new ListItem("- select -", "0"));
		}

    /// <summary>
    /// Open the languages proficiencies modal.
    /// </summary>
    protected void lnkLanguageProficiences_Click(object sender, EventArgs e)
    {
      LangProficiencyModal.Show();
    }

    /// <summary>
    /// Server side validation the selection of a proficiency level
    /// </summary>
		protected void ProficiencyMissing_OnServerValidate(object source, ServerValidateEventArgs args)
		{
      args.IsValid = LanguagesUpdateableList.Items.IsNull() || !LanguagesUpdateableList.Items.Any(item => item.Value.IsNullOrEmpty());
		}

    /// <summary>
    /// Sets the Search Type Radio buttons.
    /// </summary>
    protected void UpdatePanelSearchCriteriaLanguage_OnPreRender(object sender, EventArgs e)
    {
      // Set Search Type Radio buttons
      if (LanguagesUpdateableList.Items.IsNotNull() && LanguagesUpdateableList.Items.Count > 0)
      {
        SearchLanguagesAnd.Enabled = true;
        SearchLanguagesOr.Enabled = true;
      }
      else
      {
        SearchLanguagesAnd.Enabled = false;
        SearchLanguagesOr.Enabled = false;
      }
    }
	}
}