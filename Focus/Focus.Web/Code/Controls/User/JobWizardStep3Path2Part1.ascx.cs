#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.EducationInternship;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.ViewModels;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class JobWizardStep3Path2Part1 : UserControlBase
	{

    private string _jobTasks;
    /// <summary>
    /// Gets the model.
    /// </summary>
    /// <value>The model.</value>
    protected string JobTasks
    {
      get { return _jobTasks ?? (_jobTasks = GetViewStateValue("JobWizard:JobTasks", "")); }
      set { SetViewStateValue("JobWizard:JobTasks", value); }
    }

    /// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
		
    }

    /// <summary>
    /// Handles the Click event of the UpdateOccupationsButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void OccupationsRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (OccupationsRadioButtonList.SelectedValue != "Internship")
      {
        TasksPanel.Visible = true;
        ItemsRow.Visible = true;
        ApplicantRow.Visible = true;
        SkillsAccordionPanel.Visible = false;
        var jobTasks = GetJobTasks(new JobWizardViewModel { Job = new JobDto() },
                                   OccupationsRadioButtonList.SelectedValue.ToLong());
        TasksRepeater.DataSource = jobTasks;
        TasksRepeater.DataBind();
        JobTasks = jobTasks.Serialize();

      }
      else
      {
        TasksPanel.Visible = false;
        ItemsRow.Visible = false;
        ApplicantRow.Visible = false;
        SkillsAccordionPanel.Visible = true;
      }

    }

   #region Bind & Unbind Methods

    /// <summary>
		/// Binds the controls for the step.
		/// </summary>
		/// <param name="model">The model.</param>
		internal void BindStep(JobWizardViewModel model)
    {
      var jobTasks = new List<JobTask>();
			JobTitleLabel.Text = model.Job.JobTitle;
      
      if (model.Job.JobType != model.OriginalJobType)
        OccupationsRadioButtonList.SelectedIndex = -1;

      if (App.Settings.Theme == FocusThemes.Education && model.Job.JobType != JobTypes.Job)
      {
        // Bind the occupations that are possible matches to the job titles
        OccupationsRadioButtonList.DataSource = ServiceClientLocator.SearchClient(App).GetOnets(model.Job.JobTitle, 5);
        OccupationsRadioButtonList.DataValueField = "Id";
        OccupationsRadioButtonList.DataTextField = "Occupation";
        OccupationsRadioButtonList.DataBind();
        OccupationsRadioButtonList.Items.Add(new ListItem(CodeLocalise("JobTitles.Internship", "Internship")));

        if (model.JobEducationInternshipSkills.IsNotNullOrEmpty() || IsInternJobTitle(model.Job.JobTitle))
        {
          if(model.JobEducationInternshipSkills.IsNotNullOrEmpty())
            SkillsAccordion.SelectedItems =
              model.JobEducationInternshipSkills.Select(x => x.EducationInternshipSkillId).ToList();
          OccupationsRadioButtonList.SelectedIndex = 5;
          TasksPanel.Visible = false;
          ItemsRow.Visible = false;
          ApplicantRow.Visible = false;
          SkillsAccordionPanel.Visible = true;
        }
        else
        {
          TasksPanel.Visible = true;
          SkillsAccordionPanel.Visible = false;
          ItemsRow.Visible = model.Job.OnetId.IsNotNull();
          ApplicantRow.Visible = model.Job.OnetId.IsNotNull();

          if (model.Job.OnetId.IsNotNull())
          {
            var occupation = OccupationsRadioButtonList.Items.FindByValue(model.Job.OnetId.ToString());
            if (occupation.IsNotNull()) occupation.Selected = true;
          }
        }

        InstructionsLabel.Text = HtmlLocalise("InternshipInstructions.Text", "Check all items that apply to your internship listing.");
      }
      else
      {
        TasksPanel.Visible = true;
        SkillsAccordionPanel.Visible = false;

        InstructionsLabel.Text = HtmlLocalise("JobInstructions.Text", "Check all items that apply to your job listing.");
      }

      if (model.Job.Tasks.IsNullOrEmpty() && model.Job.OnetId.IsNotNull())
		  {
		    model.Job.Tasks = GetJobTasks(model).Serialize();
		    jobTasks = (List<JobTask>) model.Job.Tasks.Deserialize(typeof (List<JobTask>));
		  }

      JobTasks = jobTasks.Serialize();
		  TasksRepeater.DataSource = jobTasks;
		  TasksRepeater.DataBind();
    
		}
    
	  /// <summary>
	  /// Gets the job tasks.
	  /// </summary>
	  /// <param name="model">The model.</param>
	  /// <param name="onetId"></param>
	  private List<JobTask> GetJobTasks(JobWizardViewModel model, long? onetId = null)
		{
			var jobTasks = new List<JobTask>();
			
			if (model.Job.OnetId.IsNull() && onetId.IsNull())
				return jobTasks;

			try
			{
        var jobTaskViewItems = ServiceClientLocator.JobClient(App).GetJobTasks((onetId.IsNotNull() ? (long)onetId : model.Job.OnetId.Value), JobTaskScopes.Job);
        
				foreach (var jobTaskViewItem in jobTaskViewItems)
				{
					JobTask jobTask = null;

					switch (jobTaskViewItem.JobTaskType)
					{
						case JobTaskTypes.YesNo:
							jobTask = new YesNoJobTask { Id = jobTaskViewItem.JobTaskId, Prompt = jobTaskViewItem.Prompt, Response = jobTaskViewItem.Response };
							break;

						case JobTaskTypes.Open:
							jobTask = new OpenJobTask { Id = jobTaskViewItem.JobTaskId, Prompt = jobTaskViewItem.Prompt, Response = jobTaskViewItem.Response };
							break;

						case JobTaskTypes.MultiOption:
							jobTask = new MultiJobTask { Id = jobTaskViewItem.JobTaskId, Prompt = jobTaskViewItem.Prompt, Response = jobTaskViewItem.Response };

							foreach (var jobTaskMultiOptionPrompt in jobTaskViewItem.MultiOptionPrompts)
								((MultiJobTask)jobTask).Options.Add(new MultiJobTaskOption { Prompt = jobTaskMultiOptionPrompt });

							break;
					}

					jobTasks.Add(jobTask);
				}
			}
			catch
			{ }

			return jobTasks;
		}

		#region Tasks Binding

	 
	  protected void TasksRepeater_ItemDataBound(object source, RepeaterItemEventArgs e)
		{
      // Do nothing if this isn't a Item or AlternateItem
			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			// Get access to each control
			var jobTaskId = (HiddenField)e.Item.FindControl("JobTaskId");
			var taskSelectedCheckBox = (CheckBox)e.Item.FindControl("TaskSelectedCheckBox");
			var taskPrompt = (Literal)e.Item.FindControl("TaskPrompt");
			var multiOptionsRow = (HtmlTableRow)e.Item.FindControl("MultiOptionsRow");
			var multiOptionsCheckBoxList = (CheckBoxList)e.Item.FindControl("MultiOptionsCheckBoxList");
			var openRow = (HtmlTableRow)e.Item.FindControl("OpenRow");
			var openTextBox = (TextBox)e.Item.FindControl("OpenTextBox");
      var openTextBoxValidator = (CustomValidator)e.Item.FindControl("OpenTextBoxValidator");

			// Get the JobTask
			var jobTask = ((JobTask)e.Item.DataItem);

			// Set the controls			
			jobTaskId.Value = jobTask.Id.ToString();
			taskSelectedCheckBox.Checked = jobTask.Selected;
			taskPrompt.Text = jobTask.Prompt;

			// Process a Multi Job Task
			if (jobTask is MultiJobTask)
			{
				// Add the check box items
				multiOptionsCheckBoxList.Items.Clear();

				foreach (var option in ((MultiJobTask)jobTask).Options)
				{
					var listItem = new ListItem(option.Prompt) {Selected = option.Selected};
          listItem.Attributes.Add("data-disable", (jobTask.Selected ? "0" : "1"));

					multiOptionsCheckBoxList.Items.Add(listItem);
				}

				// Hide the selected check box and open row
				openRow.Visible = openTextBox.Enabled = false;

				// Set the onclick and onkeypress events for the task selected checkbox
				taskSelectedCheckBox.Attributes["onclick"] = "EnableDisableMultiOptionsTask('" + taskSelectedCheckBox.ClientID + "','" + multiOptionsCheckBoxList.ClientID + "');";
				taskSelectedCheckBox.Attributes["onkeypress"] = "EnableDisableMultiOptionsTask('" + taskSelectedCheckBox.ClientID + "','" + multiOptionsCheckBoxList.ClientID + "');";
				
				return;
			}

			// Process an Open Job Task
			if (jobTask is OpenJobTask)
			{
				openTextBox.Text = ((OpenJobTask)jobTask).Text;
				if (!jobTask.Selected) openTextBox.Attributes.Add("disabled", "disabled");

				// Hide the selected check box and the multi row
				multiOptionsRow.Visible = multiOptionsCheckBoxList.Enabled = false;

				// Set the onclick event for the task selected checkbox
				taskSelectedCheckBox.Attributes["onclick"] = "EnableDisableOpenTask('" + taskSelectedCheckBox.ClientID + "','" + openTextBox.ClientID + "');";
				taskSelectedCheckBox.Attributes["onkeypress"] = "EnableDisableOpenTask('" + taskSelectedCheckBox.ClientID + "','" + openTextBox.ClientID + "');";

			  openTextBoxValidator.ErrorMessage = CodeLocalise("OpenTextBox.Error",
			                                                   "Please enter some text or untick the checkbox.");
        // Set validatior
        Page.ClientScript.RegisterExpandoAttribute(openTextBoxValidator.ClientID, "txtId", openTextBox.ClientID);

				return;
			}

			// Last chance saloon so it must be Yes / No so hide everything else
			multiOptionsRow.Visible = multiOptionsCheckBoxList.Enabled = false;
			openRow.Visible = openTextBox.Enabled = false;
      
		}

		#endregion

		/// <summary>
		/// Unbinds the step.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
		internal JobWizardViewModel UnbindStep(JobWizardViewModel model)
		{
  		var jobDescription = new StringBuilder();

		  if (App.Settings.Theme == FocusThemes.Education && model.Job.OnetId.IsNull() &&
		      OccupationsRadioButtonList.SelectedValue != "Internship")
		    model.Job.OnetId = OccupationsRadioButtonList.SelectedValue.ToLong();

		  if (App.Settings.Theme == FocusThemes.Workforce || OccupationsRadioButtonList.SelectedValue != "Internship")
		  {
		    // Get out of dodge if we don't have a model to update
        if (JobTasks.IsNullOrEmpty())
		      return model;

        var jobTasks = (List<JobTask>)JobTasks.Deserialize(typeof(List<JobTask>));
		    foreach (RepeaterItem item in TasksRepeater.Items)
		    {
		      // Get the task
		      var jobTaskId = Convert.ToInt32(((HiddenField) item.FindControl("JobTaskId")).Value);
		      var jobTask = jobTasks.Single(x => x.Id == jobTaskId);

		      // Set the Selected value
		      var taskSelectedCheckBox = (CheckBox) item.FindControl("TaskSelectedCheckBox");
		      jobTask.Selected = taskSelectedCheckBox.Checked;

		      if (jobTask is YesNoJobTask && jobTask.Selected)
		      {
		        jobDescription.AppendLine("* " + jobTask.Response);
		        continue;
		      }

		      // Process a Multi Job Task
		      if (jobTask is MultiJobTask)
		      {
		        var multiOptionsCheckBoxList = (CheckBoxList) item.FindControl("MultiOptionsCheckBoxList");
		        ((MultiJobTask) jobTask).Options = new List<MultiJobTaskOption>();

		        var noneSelected = true;
		        var options = new StringBuilder();

		        foreach (ListItem listItem in multiOptionsCheckBoxList.Items)
		        {
		          ((MultiJobTask) jobTask).Options.Add(new MultiJobTaskOption
		                                                 {
		                                                   Prompt = listItem.Text,
		                                                   Selected = listItem.Selected
		                                                 });
		          if (listItem.Selected)
		          {
		            noneSelected = false;
		            options.Append(listItem.Text + ", ");
		          }
		        }

		        // If none are selected then de-select this task
		        if (noneSelected)
		          taskSelectedCheckBox.Checked = jobTask.Selected = false;
		        else
		        {
		          var optionsText = options.ToString();

		          if (optionsText.LastIndexOf(", ") > 0)
		            optionsText = optionsText.Substring(0, optionsText.LastIndexOf(", ")) + ".";

		          jobDescription.AppendLine("* " + jobTask.Response + " " + optionsText);
		        }

		        // Mark the items as disabled or not 
		        foreach (ListItem listItem in multiOptionsCheckBoxList.Items)
		          listItem.Attributes.Add("data-disable", (jobTask.Selected ? "0" : "1"));

		        continue;
		      }

		      // Process an Open Job Task
		      if (jobTask is OpenJobTask)
		      {
		        var openTextBox = (TextBox) item.FindControl("OpenTextBox");
		        ((OpenJobTask) jobTask).Text = openTextBox.TextTrimmed();

		        // if there is no text then de-select this task
		        taskSelectedCheckBox.Checked = jobTask.Selected = ((OpenJobTask) jobTask).Text.IsNotNullOrEmpty();

		        // Ensure that the textbox is disabled
		        if (!jobTask.Selected)
		          openTextBox.Attributes.Add("disabled", "disabled");
		        else
		          openTextBox.Attributes.Remove("disabled");

		        if (jobTask.Selected)
		        {
		          var response = "";

		          if (jobTask.Response.IsNotNullOrEmpty())
		            response = jobTask.Response.Trim() + " ";

		          jobDescription.AppendLine("* " + response + ((OpenJobTask) jobTask).Text);
		        }
		      }
		    }

		    model.Job.Tasks = jobTasks.Serialize();
		  }
		  else
		  {
		    if (SkillsAccordion.SelectedItems.Any())
		    {
		      model.JobEducationInternshipSkills =
		        SkillsAccordion.SelectedItems.Select(item => new JobEducationInternshipSkillDto
		                                                       {
		                                                         EducationInternshipSkillId = item,
		                                                         JobId = model.Job.Id.Value
		                                                       }).ToList();
		      
          var statements =
		        ServiceClientLocator.CoreClient(App).GetEducationInternshipStatements(new EducationInternshipStatementCriteria
		                                                               {
		                                                                 EducationInternshipSkillIds
		                                                                   =
		                                                                   SkillsAccordion
		                                                                   .SelectedItems
		                                                               });
		      foreach (var statement in statements)
		        jobDescription.AppendLine("* " + statement.PresentTenseStatement);
		    }
		  }

		  model.Job.Description = jobDescription.ToString();

			return model;
		}

		#endregion

    /// <summary>
    /// Checks if job title contains intern words
    /// </summary>
    /// <param name="jobTitle">The job title.</param>
    /// <returns>
    /// True or false
    /// </returns>
    private bool IsInternJobTitle(string jobTitle)
    {
      var internshipWords = new List<string>
		                          {
		                            CodeLocalise("JobTitle.Intern", "intern"),
		                            CodeLocalise("JobTitle.Interns", "interns"),
		                            CodeLocalise("JobTitle.Internship", "internship"),
		                            CodeLocalise("JobTitle.Apprentice", "apprentice")
		                          };

      return internshipWords.Any(internshipWord => Regex.IsMatch(jobTitle, internshipWord, RegexOptions.IgnoreCase));
    } 

    #region Job Task

		[Serializable]
		[XmlInclude(typeof(YesNoJobTask))]
		[XmlInclude(typeof(MultiJobTask))]
		[XmlInclude(typeof(OpenJobTask))]
		public class JobTask
		{
			public long Id { get; set; }
			public bool Selected { get; set; }
			public string Prompt { get; set; }
			public string Response { get; set; }
		}

		[Serializable]
		public class YesNoJobTask : JobTask
		{ }

		[Serializable]
		public class MultiJobTask : JobTask
		{
			public MultiJobTask()
			{
				Options = new List<MultiJobTaskOption>();
			}

			public List<MultiJobTaskOption> Options { get; set; }
		}

		[Serializable]
		public class MultiJobTaskOption
		{
			public string Prompt { get; set; }
			public bool Selected { get; set; }
		}

		[Serializable]
		public class OpenJobTask : JobTask
		{
			public string Text { get; set; }
		}

		#endregion
	}
}