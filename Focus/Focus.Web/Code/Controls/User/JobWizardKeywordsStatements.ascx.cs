#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class JobWizardKeywordsStatements : UserControlBase
	{
		/// <summary>
		/// Gets or sets the Onet id.
		/// </summary>
		public long? OnetId { get; set; }

	  /// <summary>
	  /// Gets or sets the job title.
	  /// </summary>
	  /// <value>The job title.</value>
	  public string JobTitle
	  {
      get { return GetViewStateValue<string>("JobWizardKeywordsStatements:JobTitle"); }
      set { SetViewStateValue("JobWizardKeywordsStatements:JobTitle", value); }
	  }

    /// <summary>
    /// Gets or sets the skills ids.
    /// </summary>
    /// <value>The skills ids.</value>
    public List<long> SkillsIds { get; set; }

	  /// <summary>
	  /// Gets or sets the job type.
	  /// </summary>
	  /// <value>The job type.</value>
	  public JobTypes JobType
	  {
      get { return GetViewStateValue<JobTypes>("JobWizardKeywordsStatements:JobType"); }
      set { SetViewStateValue("JobWizardKeywordsStatements:JobType", value); }
	  }
	
		/// <summary>
		/// Gets or sets the target control id.
		/// </summary>
		public string TargetControlId { get; set; }

		/// <summary>
		/// Gets or sets the default open section.
		/// </summary>
		public Section DefaultOpenSection { get; set; }

		/// <summary>
		/// Gets or sets the height of the section.
		/// </summary>
		/// <value>The height of the section.</value>
		public string SectionHeight { get; set; }
	
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();

				if (SectionHeight.IsNotNullOrEmpty())
					KeywordsPanel.Height = StatementsPanel.Height = new Unit(SectionHeight);

				var keywordsHeadingMatch = CodeLocalise("KeywordsHeadingMatch", "* Knowledge sets include:");
				var js = string.Format("return AddSelectedKeywordsOrStatements('{0}', '{1}', '{2}', '{3}');", KeywordsCheckBoxList.ClientID, 
																																																			StatementsCheckBoxList.ClientID,
																																																			GetControlRenderId(TargetControlId), 
																																																			keywordsHeadingMatch);
				AddButton.Attributes.Add("onclick", js);
				AddButton.Attributes.Add("onkeypress", js);
			}

			// Set the default open pane to be either Keywords or Statements
			if (KeywordsCheckBoxList.Items.Count > 0 && StatementsCheckBoxList.Items.Count > 0)
				MyAccordion.SelectedIndex = DefaultOpenSection == Section.Keywords ? 0 : 1;

    }

		/// <summary>
		/// Refreshes the keywords and statements
		/// </summary>
		public void Refresh()
		{
			BindKeywords();
      if(OnetId.IsNotNull())
        BindStatements();
		  
      StatementsPanel.Visible = App.Settings.Theme == FocusThemes.Workforce
		                              ? (StatementsAccordionPane.Visible = true)
		                              : (StatementsAccordionPane.Visible = (OnetId.IsNotNull()));
		}

		/// <summary>
		/// Binds the Keywords.
		/// Can be refreshed as the description changes
		/// </summary>
		private void BindKeywords()
		{
			if (TargetControlId.IsNullOrEmpty()) return;
			var targetControl = Parent.FindControlRecursive(TargetControlId) as TextBox;
			var jobDescription = (targetControl.IsNotNull() ? targetControl.Text : "");

      KeywordsCheckBoxList.DataSource = ServiceClientLocator.SearchClient(App).ClarifyText(string.Concat(JobTitle, "\n ", jobDescription), App.Settings.MinimumScoreForClarify, App.Settings.MaximumSearchCountForClarify, "keywords");
			KeywordsCheckBoxList.DataBind();

      ReSelectAlreadySelectedDetails(KeywordsCheckBoxList, jobDescription);

		}

    /// <summary>
		/// Binds the Statements.
		/// Once bound it will never be re-bound
		/// </summary>
		private void BindStatements()
		{
      StatementsCheckBoxList.DataSource = ServiceClientLocator.SearchClient(App).GetOnetTasks((long) OnetId);
      StatementsCheckBoxList.DataValueField = "Id";
      StatementsCheckBoxList.DataTextField = "Task";
		  StatementsCheckBoxList.DataBind();
		}

		#region Events

		/// <summary>
		/// Handles the Click event of the RefreshButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		public void RefreshButton_Click(object sender, EventArgs e)
		{
			BindKeywords();
		}

		#endregion

		#region Localise UI

		/// <summary>
		/// Localises the UI.
		/// </summary>
		public void LocaliseUI()
		{
			AddButton.Text = CodeLocalise("AddButton.Text", "< Add");
			RefreshButton.Text = CodeLocalise("RefreshButton.Text", "Refresh");
		}

		#endregion


    /// <summary>
    /// Reselects the already selected details.
    /// </summary>
    /// <param name="list">The list.</param>
    /// <param name="jobDescription">The job description.</param>
    private void ReSelectAlreadySelectedDetails(ListControl list, string jobDescription)
    {
      if (jobDescription.IsNotNullOrEmpty())
      {
        for (int i = 0; i < list.Items.Count; i++)
        {
          if (jobDescription.Contains(list.Items[i].Text))
          {
            list.Items[i].Selected = true;
            list.Items[i].Enabled = false;
          }
        }
      }
    }

		public enum Section
		{
			Keywords,
			Statements
		}
	}
}