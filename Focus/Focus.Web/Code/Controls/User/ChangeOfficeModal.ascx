﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangeOfficeModal.ascx.cs" Inherits="Focus.Web.Code.Controls.User.ChangeOfficeModal" %>

<asp:HiddenField ID="ChangeOfficeDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ChangeOffice" runat="server" BehaviorID="ChangeOfficeModal"
                        TargetControlID="ChangeOfficeDummyTarget"
                        PopupControlID="ChangeOfficePanel" 
                        PopupDragHandleControlID="ModalHeaderPanel"
                        RepositionMode="RepositionOnWindowResizeAndScroll"
                        BackgroundCssClass="modalBackground" />
												

<asp:Panel ID="ChangeOfficePanel" runat="server" CssClass="modal" Width="275px" ClientIDMode="Static" Style="display:none;" >
	<div>
		<asp:Panel ID="OfficeSelectorPanel" runat="server" Visible="True">
		  <asp:Panel runat="server" ID="ModalHeaderPanel" CssClass="modal-header">
  			<strong id="OfficeSelectorTitle" runat="server"><%= HtmlLocalise("OfficeSelectorTitle.Text", "Change office") %></strong>
      </asp:Panel>
      <br/><%= HtmlLabel(OfficeDropDownList,"OfficeSelectorBody.Text", "I am currently working in")%>
			<div style="margin-top:20px;">
				<asp:DropDownList ID="OfficeDropDownList" runat="server" ValidationGroup="ChangeOfficeValidation"/>
				<asp:Button ID="SaveCurrentOfficeButton" CausesValidation="True" ValidationGroup="ChangeOfficeValidation" class="button1" runat="server" Text="Save" OnClick="SaveCurrentOfficeClick"/>
			  <br />
				<asp:RequiredFieldValidator ID="ChangeOfficeDropDownValidator" CssClass="error" ControlToValidate="OfficeDropDownList" ValidationGroup="ChangeOfficeValidation" runat="server"/>
			</div>
		</asp:Panel>
	</div>
	<div class="closeIcon" id="closeIcon" runat="server"><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('ChangeOfficeModal').hide(); return false;" /></div>
</asp:Panel>
