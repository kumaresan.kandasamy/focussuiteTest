﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobOrderSearch.ascx.cs" Inherits="Focus.Web.Code.Controls.User.JobOrderSearch" %>
<div id="SearchPanelTop">
  <table width="100%">
      <tr>
          <td colspan="3">
            <asp:Panel ID="JobOrderSearchHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
              <table class="accordionTable">
					      <tr>
						      <td>
							      <asp:Image ID="JobOrderSearchHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" AlternateText="."/>
							      &nbsp;&nbsp;<%= HtmlLocalise("FindAJobOrder.Label", "Find a #EMPLOYMENTTYPE#")%>
						      </td>
					      </tr>
				      </table>
		        </asp:Panel>
           </td>
       </tr>
			 <tr>
			    <td>
			       <asp:Panel ID="JobOrderSearchPanel" runat="server">
			     <table width="100%">
			       <tr>
              <td>
                <table>
                 <tr>
                    <td width="100"><%= HtmlLocalise("JobID.Text", "#POSTINGTYPE# ID")%></td>
			              <td>
				              <asp:TextBox runat="server" ID="JobIDTextBox" ClientIDMode="Static" Width="250"/>
			              </td>
                 </tr>
               </table> 
            </td>
         <td>
            <table>
             <tr>
               <td width="100"><%= HtmlLocalise("EmployerName.Text", "#BUSINESS#:LOWER name")%></td>
			        <td >
				        <asp:TextBox runat="server" ID="EmployerNameTextBox" ClientIDMode="Static" Width="250"/>
			        </td>
             </tr>
           </table>
         </td>
         <td>
            <table>
             <tr>
               <td width="100"><%= HtmlLabel("CreationDate.Label", "Creation date") %>&nbsp;</td>
				       <td width="30"><i><%= HtmlLabel("CreationDateFrom.Label", "from") %></i>&nbsp;</td>
				        <td><asp:DropDownList ID="FromMonthDropDownList" runat="server" />&nbsp;</td>
				        <td><asp:DropDownList ID="FromYearDropDownList" runat="server" /></td>
             </tr>
           </table>
         </td>
			</tr>    
               <tr>
         <td>
           <table>
             <tr>
                <td width="100"><%= HtmlLocalise("JobTitle.Text", "#POSTINGTYPE# title")%></td>
			          <td>
				          <asp:TextBox runat="server" ID="JobTitleTextBox" ClientIDMode="Static" Width="250"/>
			          </td>
             </tr>
           </table>
         </td>
         <td>
            <table>
             <tr>
               <td width="100"><%= HtmlLocalise("DBATradeName.Text", "DBA/trade name")%></td>
			        <td >
				        <asp:TextBox runat="server" ID="DBATradeNameTextBox" ClientIDMode="Static" Width="250"/>
			        </td>
             </tr>
           </table>
         </td>
         <td>
            <table>
             <tr>
               <td width="100">&nbsp;</td>
				       <td width="30"><i><%= HtmlLabel("CreationDateTo.Label", "to") %></i>&nbsp;</td>
				        <td><asp:DropDownList ID="ToMonthDropDownList" runat="server" />&nbsp;</td>
				        <td><asp:DropDownList ID="ToYearDropDownList" runat="server" /></td>
             </tr>
           </table>
         </td>
			</tr>    
               <tr>
         <td colspan="3">
           <table>
             <tr>
                <td width="100"><%= HtmlLocalise("HiringManagerUsername.Text", "Hiring manager username")%></td>
			          <td>
				          <asp:TextBox runat="server" ID="HiringManagerUsernameTextBox" ClientIDMode="Static" Width="250"/>
			          </td>
             </tr>
           </table>
         </td>
       </tr>   
               <tr>
                <td colspan="3">&nbsp;</td>
               </tr>     
             </table>
             </asp:Panel>
          </td> 
       </tr> 
     <act:CollapsiblePanelExtender ID="JobOrderSearchPanelExtender" runat="server" TargetControlID="JobOrderSearchPanel" ExpandControlID="JobOrderSearchHeaderPanel" 
																			  CollapseControlID="JobOrderSearchHeaderPanel" Collapsed="false" ImageControlID="JobOrderSearchHeaderImage" 
																			  CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
																			  SuppressPostBack="true" />
  </table>
</div>