#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Core.Models;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class EmployerRegistrationStep5 : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				LocaliseUI();
		}

		/// <summary>
		/// Initialises the step.
		/// </summary>
		/// <param name="completeButtonClientID">The complete button client ID.</param>
		/// <param name="completeBottomButtonClientId">The complete bottom button client id.</param>
		internal void InitialiseStep(string completeButtonClientID, string completeBottomButtonClientId)
		{
			// Reset the check box
			AcceptTermsOfUserCheckBox.Checked = false;

			// Build the JS 
			var js = @"
$(document).ready(function()
{
	// Add onclick handler to terms checkbox 
	$(""#" + AcceptTermsOfUserCheckBox.ClientID + @""").click(function(){
		
	// If checked
	if ($(""#" + AcceptTermsOfUserCheckBox.ClientID + @""").is("":checked"")){
		$(""#" + completeButtonClientID + @""").removeAttr('disabled').removeClass('aspNetDisabled');
		$(""#" + completeBottomButtonClientId + @""").removeAttr('disabled').removeClass('aspNetDisabled');
}
	else{
		$(""#" + completeButtonClientID + @""").attr('disabled', 'disabled').addClass('aspNetDisabled');
		$(""#" + completeBottomButtonClientId + @""").attr('disabled', 'disabled').addClass('aspNetDisabled');
}
	});
});
";			
			ScriptManager.RegisterClientScriptBlock(this, typeof(EmployerRegistrationStep5), "CompleteButtonEnable", js, true);
		}

		/// <summary>
		/// Updates the model.
		/// </summary>
		/// <param name="model">The model.</param>
		internal void UpdateModel(TalentRegistrationModel model)
		{
			if (model.Employer == null) model.Employer = new EmployerDto();
			model.Employer.TermsAccepted = AcceptTermsOfUserCheckBox.Checked;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			TermsOfUseEducationPanel.Visible = App.Settings.Theme == FocusThemes.Education;
			TermsOfUseWorkforcePanel.Visible = App.Settings.Theme != FocusThemes.Education;
			AcceptTermsOfUserCheckBox.Text = App.Settings.Module == FocusModules.Talent ? CodeLocalise("TalentTermsOfUse.Label", "I accept these terms of use.") : CodeLocalise("AssistTermsOfUse.Label", "I have informed the #BUSINESS#:LOWER of these terms of use.");
		}
	}
}