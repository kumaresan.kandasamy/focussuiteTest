﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditBusinessUnit.ascx.cs"
    Inherits="Focus.Web.Code.Controls.User.EditBusinessUnit" %>
<table role="presentation">
    <tr>
        <td colspan="2" width="200px">
            <%= HtmlRequiredFieldsInstruction() %>
        </td>
    </tr>
    <asp:PlaceHolder runat="server" ID="LegalNamePlaceHolder">
        <tr>
            <td>
                <%= HtmlRequiredLabel(LegalNameTextBox, "LegalName.Label", "Business legal name")%>
            </td>
            <td>
                <asp:TextBox ID="LegalNameTextBox" runat="server" Width="400" TabIndex="1" MaxLength="200" />
                <asp:RequiredFieldValidator ID="LegalNameRequired" runat="server" ControlToValidate="LegalNameTextBox"
                    SetFocusOnError="true" Display="Dynamic" CssClass="error" />
            </td>
        </tr>
    </asp:PlaceHolder>
    <tr>
        <td>
            <%= HtmlRequiredLabel(BusinessUnitNameTextBox, "EmployerName.Label", "#BUSINESS# name")%>
        </td>
        <td>
            <asp:TextBox ID="BusinessUnitNameTextBox" runat="server" Width="400" TabIndex="1"
                MaxLength="200" />
            <asp:RequiredFieldValidator ID="EmployerNameRequired" runat="server" ControlToValidate="BusinessUnitNameTextBox"
                SetFocusOnError="true" Display="Dynamic" CssClass="error" />
        </td>
    </tr>
    <tr>
        <td class="label">
            <%= HtmlRequiredLabel(AddressLine1TextBox, "AddressLine1.Label", "Address")%>
        </td>
        <td>
            <asp:TextBox ID="AddressLine1TextBox" runat="server" Width="98%" TabIndex="2" MaxLength="200" />
            <asp:RequiredFieldValidator ID="AddressLine1Required" runat="server" ControlToValidate="AddressLine1TextBox"
                SetFocusOnError="true" Display="Dynamic" CssClass="error" />
        </td>
    </tr>
    <tr>
<%--        <td class="label">
            <%= HtmlLabel(AddressLine2TextBox,"AddressLine2", "")%>
        </td>--%>
        <td class="label">
            <label for="AddressLine2TextBox" class="hidden">Address Line 2</label>
        </td>
        <td>
            <asp:TextBox ID="AddressLine2TextBox" title="Address Line 2 TextBox" runat="server"
                Width="98%" TabIndex="3" MaxLength="200" />
        </td>
    </tr>
    <tr>
        <td class="label">
            <%= HtmlRequiredLabel(AddressTownCityTextBox, "AddressTownCity.Label", "City")%>
        </td>
        <td>
            <asp:TextBox ID="AddressTownCityTextBox" runat="server" Width="98%" TabIndex="4"
                MaxLength="100" />
            <asp:RequiredFieldValidator ID="AddressTownCityRequired" runat="server" ControlToValidate="AddressTownCityTextBox"
                SetFocusOnError="true" Display="Dynamic" CssClass="error" />
        </td>
    </tr>
    <tr>
        <td class="label">
            <%= HtmlRequiredLabel(AddressPostcodeZipTextBox, "AddressPostcodeZip.Label", "ZIP or postal code")%>
        </td>
        <td>
            <asp:TextBox ID="AddressPostcodeZipTextBox" runat="server" Width="90px" TabIndex="5"
                MaxLength="10" />
            <asp:RequiredFieldValidator ID="AddressPostcodeZipRequired" runat="server" ControlToValidate="AddressPostcodeZipTextBox"
                SetFocusOnError="true" Display="Dynamic" CssClass="error" />
            <asp:RegularExpressionValidator ID="AddressPostcodeRegexValidator" runat="server"
                CssClass="error" ControlToValidate="AddressPostcodeZipTextBox" SetFocusOnError="true"
                Display="Dynamic" />
        </td>
    </tr>
    <tr>
        <td class="label">
            <%= HtmlRequiredLabel(AddressStateDropDownList, "AddressState.Label", "State")%>
        </td>
        <td>
            <asp:DropDownList ID="AddressStateDropDownList" runat="server" Width="100%" TabIndex="6"
                onchange="focusSuite.editBusinessUnit.updateCountry();" />
            <asp:RequiredFieldValidator ID="AddressStateRequired" runat="server" ControlToValidate="AddressStateDropDownList"
                SetFocusOnError="true" Display="Dynamic" CssClass="error" />
        </td>
    </tr>
    <tr id="CountyRow" runat="server">
        <td class="label">
            <%= HtmlRequiredLabel(AddressCountyDropDownList, "AddressCounty.Label", "County")%>
        </td>
        <td>
            <focus:AjaxDropDownList ID="AddressCountyDropDownList" runat="server" Width="100%"
                TabIndex="7" onchange="focusSuite.editBusinessUnit.updateStateCountry()" />
            <act:CascadingDropDown ID="AddressCountyDropDownListCascadingDropDown" runat="server"
                TargetControlID="AddressCountyDropDownList" ParentControlID="AddressStateDropDownList"
                ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetCounties" Category="Counties" />
            <asp:RequiredFieldValidator ID="AddressCountyRequired" runat="server" ControlToValidate="AddressCountyDropDownList"
                SetFocusOnError="true" Display="Dynamic" CssClass="error" />
        </td>
    </tr>
    <tr>
        <td class="label">
            <%= HtmlRequiredLabel(AddressCountryDropDownList, "AddressCountry.Label", "Country")%>
        </td>
        <td>
            <asp:DropDownList ID="AddressCountryDropDownList" runat="server" Width="100%" TabIndex="8"
                onchange="focusSuite.editBusinessUnit.updateStateCounty();" />
            <asp:RequiredFieldValidator ID="AddressCountryRequired" runat="server" ControlToValidate="AddressCountryDropDownList"
                SetFocusOnError="true" Display="Dynamic" CssClass="error" />
        </td>
    </tr>
    <tr>
        <td class="label">
            <%= HtmlLocalise("PublicTransitAccessible.Label", "Public transit accessible?")%>
        </td>
        <td>            
            <asp:CheckBox runat="server" ID="PublicTransitAccessible" TabIndex="9" aria-label="PublicTransitAccessible"/>
            <label for="MainContent_BusinessUnitEditor_PublicTransitAccessible" class="hidden">Public transit accessible?</label>
        </td>
    </tr>
    <tr>
        <td class="label">
            <%= HtmlLabel(ContactUrlTextBox,"ContactUrl.Label", "URL")%>
        </td>
        <td>
            <asp:TextBox ID="ContactUrlTextBox" runat="server" title="Contact Url TextBox" Width="98%"
                TabIndex="10" MaxLength="1000" />
            <asp:RegularExpressionValidator ID="ContactUrlRegEx" runat="server" ControlToValidate="ContactUrlTextBox"
                SetFocusOnError="true" Display="Dynamic" CssClass="error" />
        </td>
    </tr>
    <tr>
        <td class="label">
            <%= HtmlRequiredLabel(ContactPhoneTextBox, "ContactPhone.Label", "Phone number")%>
        </td>
        <td>
            <asp:TextBox ID="ContactPhoneTextBox" runat="server" Width="50%" TabIndex="11" MaxLength="20" />
            <asp:TextBox aria-label="Extension TextBox" ID="ExtensionTextBox" title="Extension TextBox" runat="server" Width="15%"
                TabIndex="12" MaxLength="5" />
            <act:MaskedEditExtender ID="ExtensionMaskedEdit" runat="server" MaskType="Number"
                TargetControlID="ExtensionTextBox" PromptCharacter=" " ClearMaskOnLostFocus="false"
                AutoComplete="false" EnableViewState="true" ClearTextOnInvalid="false" Mask="99999" />
            <asp:DropDownList aria-label="Phone Type" ID="PhoneTypeDropDownList" runat="server" Width="90px" TabIndex="13"
                Title="Phone Type" />
            <asp:RequiredFieldValidator ID="ContactPhoneRequired" runat="server" ControlToValidate="ContactPhoneTextBox"
                SetFocusOnError="true" Display="Dynamic" CssClass="error" />
        </td>
    </tr>
    <tr>
        <td class="label">
            <%= HtmlLabel(ContactAlternatePhoneTextBox,"ContactAlternatePhone.Label", "Alternate phone number 1")%>
        </td>
        <td>
            <asp:TextBox ID="ContactAlternatePhoneTextBox" title="Contact Alternate Phone TextBox"
                runat="server" Width="50%" TabIndex="14" MaxLength="20" />
            <asp:DropDownList aria-label="Alt Phone Type" ID="AltPhoneTypeDropDownList" runat="server" Width="90px" TabIndex="15"
                Title="Alternate phone number 1" />
        </td>
    </tr>
    <tr>
        <td class="label">
            <%= HtmlLabel(ContactAlternate2PhoneTextBox,"ContactAlternatePhone2.Label", "Alternate phone number 2")%>
        </td>
        <td>
            <asp:TextBox ID="ContactAlternate2PhoneTextBox" title="Contact Alternate 2 Phone TextBox"
                runat="server" Width="50%" TabIndex="16" MaxLength="20" />
            <asp:DropDownList aria-label="Alt Phone Type 2" ID="AltPhoneType2DropDownList" runat="server" Width="90px" TabIndex="17"
                Title="Alternate phone number 2" />
        </td>
    </tr>
    <asp:PlaceHolder ID="OwnershipTypePlaceHolder" runat="server">
        <tr>
            <td class="label">
                <%= HtmlRequiredLabel(OwnershipTypeDropDownList, "OwnershipType.Label", "Ownership type")%>
            </td>
            <td>
                <asp:DropDownList ID="OwnershipTypeDropDownList" runat="server" Width="200px" TabIndex="18" />
                <asp:RequiredFieldValidator ID="OwnershipTypeRequired" runat="server" ControlToValidate="OwnershipTypeDropDownList"
                    SetFocusOnError="true" Display="Dynamic" CssClass="error" />
            </td>
        </tr>
    </asp:PlaceHolder>
    <tr id="IndustryClassificationRow" runat="server">
        <td class="label">
            <%= HtmlRequiredLabel(IndustrialClassificationTextBox, "IndustrialClassification.Label", "Industry classification")%>
        </td>
        <td width="50%">
                    <asp:TextBox ID="IndustrialClassificationTextBox" runat="server" Width="98%" TabIndex="19"
                        MaxLength="200" AutoCompleteType="Disabled" />
                    <act:AutoCompleteExtender ID="IndustrialClassificationAutoCompleteExtender" runat="server"
                        TargetControlID="IndustrialClassificationTextBox" MinimumPrefixLength="2" CompletionListCssClass="autocomplete_completionListElement"
                        CompletionInterval="100" CompletionSetCount="15" UseContextKey="true" ServicePath="~/Services/AjaxService.svc"
                        ServiceMethod="GetNaics" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                        CompletionListItemCssClass="autocomplete_listItem" />
                    <br />
                    <asp:RequiredFieldValidator ID="IndustrialClassificationRequired" runat="server"
                        ControlToValidate="IndustrialClassificationTextBox" SetFocusOnError="true" Display="Dynamic"
                        CssClass="error" />
                    <asp:CustomValidator runat="server" ID="IndustrialClassificationValidator" ControlToValidate="IndustrialClassificationTextBox"
                        ClientValidationFunction="focusSuite.editBusinessUnit.doesNAICSExist" SetFocusOnError="true"
                        Display="Dynamic" CssClass="error" />
        </td>
        <td width="30%" style="text-align: left">
            <asp:HyperLink ID="NaicsLink" runat="server" NavigateUrl="https://www.naics.com/search/" Width="50%" Visible="false"
                Target="_new"><%= HtmlLocalise("NaicsLink.Label.NoEdit", "NAICS Lookup-Use 2012 NAICS search")%></asp:HyperLink>
        </td>
 </tr>
<tr>
    <td>
        <asp:Literal runat="server" ID="AccountTypeHeader"></asp:Literal>
    </td>
    <td>
        <asp:DropDownList aria-label="Account Type" ID="AccountTypeDropDownList" runat="server" Width="100%" TabIndex="25" />
        <asp:RequiredFieldValidator ID="AccountTypeRequired" runat="server" ControlToValidate="AccountTypeDropDownList"
            SetFocusOnError="true" Display="Dynamic" CssClass="error" />
    </td>
</tr>
<tr id="NoOfEmployeesRow" runat="server">
    <td class="label">
        <%= HtmlRequiredLabel(NoOfEmployeesDropDownList, "NoOfEmployees.Label", "Number Of Employees")%>
    </td>
    <td>
        <asp:DropDownList ID="NoOfEmployeesDropDownList" runat="server" Width="100%" TabIndex="25" />
        <asp:RequiredFieldValidator ID="NoOfEmployeesValidator" runat="server" ControlToValidate="NoOfEmployeesDropDownList"
            SetFocusOnError="true" Display="Dynamic" CssClass="error" />
    </td>
</tr>
<tr id="CompanyDescriptionRow" runat="server">
    <td class="label">
        <%= HtmlLabel(CompanyDescriptionTextBox,"CompanyDescription", "#BUSINESS# description")%>
    </td>
    <td>
        <asp:TextBox ID="CompanyDescriptionTextBox" TextMode="MultiLine" MaxLength="2000" title="Company description text box"
            runat="server" Rows="6" Width="98%" TabIndex="21" />
        <br />
        <asp:CustomValidator runat="server" ID="CompanyDescriptionLengthValidator" ClientValidationFunction="EditBusinessUnit_ValidateCompanyDescriptionLength"
            CssClass="error" ControlToValidate="CompanyDescriptionTextBox" ErrorMessage="Error"
            SetFocusOnError="True" />
    </td>
</tr>
</table>
<script language="javascript" type="text/javascript">
    function EditBusinessUnit_ValidateCompanyDescriptionLength(sender, args) {
        var description = $("#" + sender.controltovalidate).val();

        args.IsValid = description.length <= 2000;
    }
</script>
