#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;
using BgtSSO;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Core.Models;
using Focus.Web.WebAuth;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class EmployerRegistrationStep4 : UserControlBase
	{
		protected string IsCountyEnabled = "";

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				Bind();
				LocaliseUI();
			  ShowHideControls();
			}

			ContactEmailAddressRegEx.ValidationExpression = App.Settings.EmailAddressRegExPattern;
      AddressPostcodeRegexValidator.ValidationExpression = App.Settings.ExtendedPostalCodeRegExPattern;

			if (App.Settings.JobTitleRequired)
			{
				JobTitleLabel.CssClass = "requiredData";
				JobTitleRequired.Enabled = true;
			}

			IsCountyEnabled = App.Settings.Theme == FocusThemes.Workforce ? "True" : "False";

            if ((!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForAssist) && App.Settings.Module == FocusModules.Assist) || (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForTalent) && App.Settings.Module == FocusModules.Talent)) return;
            var profile = App.GetSessionValue<SSOProfile>(Saml.SSOProfileKey);

            FirstNameTextBox.Text = profile.FirstName;
            LastNameTextBox.Text = profile.LastName;
		}

		/// <summary>
		/// Initialises the step with the specified FederalEmployerIdentificationNumber
		/// </summary>
		/// <param name="emailAddress">The email address.</param>
		internal void InitialiseStep(string emailAddress)
		{
			ContactEmailAddressTextBox.Text = emailAddress;
		}

		/// <summary>
		/// Clears down the step
		/// </summary>
		internal void ClearStep()
		{
			PersonalTitleDropDownList.SelectedIndex = 0;
			FirstNameTextBox.Text = "";
			MiddleInitialTextBox.Text = "";
			LastNameTextBox.Text = "";
			JobTitleTextBox.Text = "";
			ContactEmailAddressTextBox.Text = "";
			SuffixDropDownList.SelectedIndex = 0;
			AddressLine1TextBox.Text = "";
			AddressLine2TextBox.Text = "";
			AddressTownCityTextBox.Text = "";
			AddressPostcodeZipTextBox.Text = "";
			AddressCountryDropDownList.SelectedIndex = 0;
			AddressStateDropDownList.SelectedIndex = 0;
			ContactPhoneTextBox.Text = "";
			ExtensionTextBox.Text = "";
			PhoneTypeDropDownList.SelectedIndex = 0;
			ContactAlternatePhoneTextBox.Text = "";
			AltPhoneTypeDropDownList.SelectedIndex = 0; 
			ContactAlternate2PhoneTextBox.Text = "";
			AltPhoneType2DropDownList.SelectedIndex = 0;
		}

		/// <summary>
		/// Updates the model.
		/// </summary>
		/// <param name="model">The model.</param>
		internal void UpdateModel(TalentRegistrationModel model)
		{
			model.EmployeePerson = new PersonDto
			{
				TitleId = PersonalTitleDropDownList.SelectedValueToLong(),
				FirstName = FirstNameTextBox.TextTrimmed(),
				MiddleInitial = MiddleInitialTextBox.TextTrimmed(),
				LastName = LastNameTextBox.TextTrimmed(),
				JobTitle = JobTitleTextBox.TextTrimmed(),
				EmailAddress = ContactEmailAddressTextBox.TextTrimmed(),
        SuffixId = SuffixDropDownList.SelectedValueToNullableLong()
			};

			model.EmployeePhone = ContactPhoneTextBox.TextTrimmed();
		    model.EmployeePhoneExtension = ExtensionTextBox.TextTrimmed();
		    model.EmployeePhoneType = PhoneTypeDropDownList.SelectedValue;
			model.EmployeeAlternatePhone1 = ContactAlternatePhoneTextBox.TextTrimmed();
		    model.EmployeeAlternatePhone1Type = AltPhoneTypeDropDownList.SelectedValue;
            model.EmployeeAlternatePhone2 = ContactAlternate2PhoneTextBox.TextTrimmed();
		    model.EmployeeAlternatePhone2Type = AltPhoneType2DropDownList.SelectedValue;
			model.EmployeeEmailAddress = ContactEmailAddressTextBox.TextTrimmed();

			model.EmployeeAddress = new PersonAddressDto
			{
				Line1 = AddressLine1TextBox.TextTrimmed(),
				Line2 = AddressLine2TextBox.TextTrimmed(),
				TownCity = AddressTownCityTextBox.TextTrimmed(),
        CountyId = AddressCountyDropDownList.SelectedValueToLong() == 0 ? (long?)null : AddressCountyDropDownList.SelectedValueToLong(),
				PostcodeZip = AddressPostcodeZipTextBox.TextTrimmed(),
				StateId = AddressStateDropDownList.SelectedValueToLong(),
				CountryId = AddressCountryDropDownList.SelectedValueToLong(),
				IsPrimary = true
			};
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			// PersonalTitle
			PersonalTitleDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Titles), null, CodeLocalise("Global.PersonalTitle.TopDefault", "- select -"));
      SuffixDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Suffixes), null, CodeLocalise("SuffixDropDownList.TopDefault", "- select suffix -"));

			// State
			AddressStateDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States), null, CodeLocalise("Global.State.TopDefault", "- select state -"));
      AddressStateDropDownList.SelectValue(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).SingleOrDefault().ToString());

			// Countries
			AddressCountryDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries), null, CodeLocalise("Global.Country.TopDefault", "- select country -"));
      // Set default country
      AddressCountryDropDownList.SelectValue(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries).Where(x => x.Key == "Country.US").Select(x => x.Id).SingleOrDefault().ToString());

      // Phone type
      BindContactTypeDropDown(PhoneTypeDropDownList);
      BindContactTypeDropDown(AltPhoneTypeDropDownList);
      BindContactTypeDropDown(AltPhoneType2DropDownList);

      AddressCountyDropDownListCascadingDropDown.PromptText = CodeLocalise("Global.County.TopDefault", "- select county -");
      AddressCountyDropDownListCascadingDropDown.LoadingText = CodeLocalise("Global.County.Progress", "[Loading counties ...]");
      AddressCountyDropDownListCascadingDropDown.PromptValue = string.Empty; 
            
      //FVN-4929 - commenting the code. caused issue during SAML sso employer registration
      //if ((!(App.Settings.SSOEnabled || !App.Settings.SamlEnabledForAssist) && App.Settings.Module == FocusModules.Assist) || (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForTalent) && App.Settings.Module == FocusModules.Talent)) return;
      //var profile = App.GetSessionValue<SSOProfile>(Saml.SSOProfileKey);
      //FirstNameTextBox.Text = profile.FirstName;
      //LastNameTextBox.Text = profile.LastName;
		}

		/// <summary>
		/// Shows the hide controls.
		/// </summary>
    private void ShowHideControls()
    {
      emailRow.Visible = false;
      CountyRow.Visible = AddressCountyRequired.Enabled = App.Settings.Theme == FocusThemes.Workforce;

      FirstNameTextBox.Enabled = (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForAssist) && App.Settings.Module == FocusModules.Assist) || (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForTalent) && App.Settings.Module == FocusModules.Talent);
      LastNameTextBox.Enabled = (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForAssist) && App.Settings.Module == FocusModules.Assist) || (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForTalent) && App.Settings.Module == FocusModules.Talent);
    }

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			FirstNameRequired.ErrorMessage = CodeLocalise("FirstName.RequiredErrorMessage", "First name is required.");
			LastNameRequiredField.ErrorMessage = CodeLocalise("LastName.RequiredErrorMessage", "Last name is required.");
			PersonalTitleRequired.ErrorMessage = CodeLocalise("PersonalTitle.RequiredErrorMessage", "Title is required.");
			AddressLine1Required.ErrorMessage = CodeLocalise("AddressLine1.RequiredErrorMessage", "Address is required.");
			AddressCityRequired.ErrorMessage = CodeLocalise("AddressTownCity.RequiredErrorMessage", "City is required.");
			AddressStateRequired.ErrorMessage = CodeLocalise("AddressState.RequiredErrorMessage", "State is required.");
			AddressCountyRequired.ErrorMessage = CodeLocalise("AddressCounty.RequiredErrorMessage", "County is required.");
			AddressPostcodeZipRequired.ErrorMessage = CodeLocalise("AddressPostcodeZip.RequiredErrorMessage", "ZIP or postal code is required.");
      AddressPostcodeRegexValidator.ErrorMessage = CodeLocalise("AddressPostcodeRegexValidator.ErrorMessage", "ZIP code must be in a 5 or 9-digit format");
			AddressCountryRequired.ErrorMessage = CodeLocalise("AddressCountry.RequiredErrorMessage", "Country is required.");
			ContactPhoneRequired.ErrorMessage = CodeLocalise("ContactPhone.RequiredErrorMessage", "Phone number is required.");
			ContactEmailAddressRequired.ErrorMessage = CodeLocalise("ContactEmailAddress.RequiredErrorMessage", "Email address is required.");
      ContactEmailAddressRegEx.ErrorMessage = CodeLocalise("ContactEmailAddress.RegExErrorMessage", "Email address format is invalid.");
			JobTitleRequired.ErrorMessage = CodeLocalise("JobTitleRequired.RequiredErrorMessage", "Job title is required");
		}

    /// <summary>
    /// Binds the contact type drop down.
    /// </summary>
    /// <param name="dropDownList">The drop down list.</param>
    private void BindContactTypeDropDown(DropDownList dropDownList)
    {
      dropDownList.Items.Clear();

      dropDownList.Items.AddEnum(PhoneTypes.Phone, "landline");
      dropDownList.Items.AddEnum(PhoneTypes.Mobile, "mobile");
      dropDownList.Items.AddEnum(PhoneTypes.Fax, "fax");
      dropDownList.Items.AddEnum(PhoneTypes.Other, "other");
    }


    ///// <summary>
    ///// Initialise the address fields from the previous step
    ///// </summary>
    ///// <param name="model">The model.</param>
    internal void BindFromPreviousStep(TalentRegistrationModel model)
    {

        if (AddressLine1TextBox.TextTrimmed().IsNotNullOrEmpty()) // User has edited this step so don't reset
            return;

        if (model.BusinessUnitAddress.IsNull())
            return;
        AddressLine1TextBox.Text = model.BusinessUnitAddress.Line1;
        AddressLine2TextBox.Text = model.BusinessUnitAddress.Line2;
        AddressTownCityTextBox.Text = model.BusinessUnitAddress.TownCity;
        AddressPostcodeZipTextBox.Text = model.BusinessUnitAddress.PostcodeZip;
        AddressCountryDropDownList.SelectedValue = model.BusinessUnitAddress.CountryId.ToString();
        AddressStateDropDownList.SelectedValue = model.BusinessUnitAddress.StateId.ToString();
        // Counties is a look up so we need to populate before setting
        AddressCountyDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Counties, model.BusinessUnitAddress.StateId), null, "- select county -");
        AddressCountyDropDownListCascadingDropDown.SelectedValue = model.BusinessUnitAddress.CountyId.ToString();
        ContactPhoneTextBox.Text = model.BusinessUnit.PrimaryPhone;
        ExtensionTextBox.Text = model.BusinessUnit.PrimaryPhoneExtension;
        if (model.BusinessUnit.PrimaryPhoneType.IsNotNullOrEmpty())
            PhoneTypeDropDownList.SelectedValue = model.BusinessUnit.PrimaryPhoneType;
        ContactAlternatePhoneTextBox.Text = model.BusinessUnit.AlternatePhone1;
        if (model.BusinessUnit.AlternatePhone1Type.IsNotNullOrEmpty())
            AltPhoneTypeDropDownList.SelectedValue = model.BusinessUnit.AlternatePhone1Type;
        ContactAlternate2PhoneTextBox.Text = model.BusinessUnit.AlternatePhone2;
        if (model.BusinessUnit.AlternatePhone2Type.IsNotNullOrEmpty())
            AltPhoneType2DropDownList.SelectedValue = model.BusinessUnit.AlternatePhone2Type;
    }
	}
}