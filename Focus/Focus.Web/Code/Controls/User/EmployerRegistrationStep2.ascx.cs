#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;
using Focus.Web.Core.Models;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class EmployerRegistrationStep2 : UserControlBase
	{
		#region Properties

		/// <summary>
		/// Gets or sets the employer id.
		/// </summary>
		/// <value>The employer id.</value>
		protected long? EmployerId
		{
			get { return GetViewStateValue<long?>("RegisterStep2:EmployerId"); }
			set { SetViewStateValue("RegisterStep2:EmployerId", value); }
		}

		/// <summary>
		/// Gets or sets the address id.
		/// </summary>
		/// <value>The address id.</value>
		protected long? AddressId
		{
			get { return GetViewStateValue<long?>("RegisterStep2:AddressId"); }
			set { SetViewStateValue("RegisterStep2:AddressId", value); }
		}

		/// <summary>
		/// Gets or sets the business unit id.
		/// </summary>
		/// <value>The business unit id.</value>
		protected long? BusinessUnitId
		{
			get { return GetViewStateValue<long?>("RegisterStep2:BusinessUnitId"); }
			set { SetViewStateValue("RegisterStep2:BusinessUnitId", value); }
		}

		/// <summary>
		/// Gets or sets the company description id.
		/// </summary>
		/// <value>The company description id.</value>
		protected long? CompanyDescriptionId
		{
			get { return GetViewStateValue<long?>("RegisterStep2:CompanyDescriptionId"); }
			set { SetViewStateValue("RegisterStep2:CompanyDescriptionId", value); }
		}

		/// <summary>
		/// Gets or sets the county id.
		/// </summary>
		/// <value>The county id.</value>
		protected long? CountyId
		{
			//get { return GetViewStateValue<long?>("RegisterStep2:CountyId"); }
			//set { SetViewStateValue("RegisterStep2:CountyId", value); }
			get
			{
				if (Session["RegisterStep2:CountyId"].IsNotNull())
				{
					long tmp = 0;
					if (long.TryParse(Session["RegisterStep2:CountyId"].ToString(), out tmp))
					{
						return tmp;
					}
				}

				return null;
			}

			set { Session["RegisterStep2:CountyId"] = value; }
		}

		/// <summary>
		/// Gets or sets the list of Business Unit objects for this employer
		/// </summary>
		/// <value>The business units for this employer</value>
		protected List<BusinessUnitDto> BusinessUnits
		{
			get { return GetViewStateValue<List<BusinessUnitDto>>("RegisterStep2:BusinessUnits"); }
			set { SetViewStateValue("RegisterStep2:BusinessUnits", value); }
		}

		/// <summary>
		/// Gets or sets the Business unit object
		/// </summary>
		/// <value>The business unit selected</value>
		protected BusinessUnitDto BusinessUnit
		{
			get { return GetViewStateValue<BusinessUnitDto>("RegisterStep2:BusinessUnit"); }
			set { SetViewStateValue("RegisterStep2:BusinessUnit", value); }
		}

		/// <summary>
		/// Gets or sets the Business unit address object
		/// </summary>
		/// <value>The address details of the business unit selected</value>
		protected BusinessUnitAddressDto BusinessUnitAddress
		{
			get { return GetViewStateValue<BusinessUnitAddressDto>("RegisterStep2:BusinessUnitAddress"); }
			set { SetViewStateValue("RegisterStep2:BusinessUnitAddress", value); }
		}

		/// <summary>
		/// Gets or sets the Business unit description object
		/// </summary>
		/// <value>The description of the business unit selected</value>
		protected BusinessUnitDescriptionDto BusinessUnitDescription
		{
			get { return GetViewStateValue<BusinessUnitDescriptionDto>("RegisterStep2:BusinessUnitDescription"); }
			set { SetViewStateValue("RegisterStep2:BusinessUnitDescription", value); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is already approved.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is already approved; otherwise, <c>false</c>.
		/// </value>
		protected bool IsAlreadyApproved
		{
			get { return App.GetSessionValue("Registration:IsAlreadyApproved", false); }
			set { App.SetSessionValue("Registration:IsAlreadyApproved", value); }
		}

	  private ValidatedEmployerView ValidatedEmployerDetails
	  {
      get { return GetViewStateValue<ValidatedEmployerView>("RegisterStep2:ValidatedEmployerDetails"); }
      set { SetViewStateValue("RegisterStep2:ValidatedEmployerDetails", value); }
	  }

    public delegate void ChangeEmployerHandler(object sender, EventArgs eventArgs);
    public event ChangeEmployerHandler ChangeEmployer;

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			HandleAccountTypeDisplay();
		  NoOfEmployeesLabel.Visible =
      NoOfEmployeesCellLabel.Visible = App.Settings.ShowEmployeeNumbers;

		  if (!IsPostBack)
		    LocaliseUI();
		}

		private void HandleAccountTypeDisplay()
		{
			if (App.Settings.AccountTypeDisplayType == ControlDisplayType.Hidden)
			{
				AccountTypeLabel.Visible = false;
				AccountTypeCell.Visible = false;
			}
		}

		/// <summary>
		/// Initialises the step with the specified Validated Employer
		/// </summary>
		/// <param name="validatedEmployer">The validated employer.</param>
		/// <param name="previouslyProgressedToStep2">if set to <c>true</c> [previously progressed to step2].</param>
		public void InitialiseStep(ValidatedEmployerView validatedEmployer, bool previouslyProgressedToStep2)
		{
		  ValidatedEmployerDetails = validatedEmployer;

			var allowTalentCompanyInformationUpdate = App.Settings.AllowTalentCompanyInformationUpdate;

			var federalEmployerIdentificationNumber = CodeLocalise("Global.Unknown.Label", "Unknown");
			var employerName = CodeLocalise("Global.Unknown.Label", "Unknown");
			var primaryPhone = CodeLocalise("Global.Unknown.Label", "Unknown");

			if (validatedEmployer.IsNotNull())
			{
				federalEmployerIdentificationNumber = validatedEmployer.FederalEmployerIdentificationNumber;
				employerName = validatedEmployer.Name;
				primaryPhone = validatedEmployer.PrimaryPhone;
			}

      ExtraInstructionsList.Visible = false;

		  if (allowTalentCompanyInformationUpdate)
		  {
		    FEINExistsInstructionsLabel.Text = CodeLocalise("ConfirmCorrectEmployer.Text",
																												"Please use the drop down below to select an existing #BUSINESS#:LOWER already linked to the Federal Employee ID number that you have entered ({0}) or select �New #BUSINESS#� to enter your #BUSINESS#:LOWER details. ", federalEmployerIdentificationNumber);
		  }
		  else
		  {
		    if (previouslyProgressedToStep2)
		    {
		      FEINExistsInstructionsLabel.Text = CodeLocalise("ConfirmCorrectEmployerAlternate.Text",
		                                                      "Based on the Federal Employer ID Number you entered ({0}), our records indicate that you are associated with {1}",
		                                                      federalEmployerIdentificationNumber, employerName);
          ExtraInstructionsList.Visible = true;

			    var listItems = new List<string>();

					if (App.Settings.Theme == FocusThemes.Workforce)
					{
						listItems.Add(CodeLocalise("EmployerWasFound.Item1", "If you have entered an incorrect FEIN, please click \"Previous step\" to edit."));
						listItems.Add(CodeLocalise("EmployerWasFound.Item2", "If the FEIN is correct and you are a new hiring manager for this #BUSINESS#:LOWER, please click \"Next step\" to complete your Contact Information."));
						listItems.Add(CodeLocalise("EmployerWasFound.Item3", "If the FEIN is correct, but you represent a new or existing business unit under this #BUSINESS#:LOWER, please select or create the business unit from the \"Select #BUSINESS#:LOWER\" drop-down below."));
						listItems.Add(CodeLocalise("EmployerWasFound.Item4", "If you need help completing your registration, contact our support team at #SUPPORTEMAIL# or #SUPPORTPHONE#. Our business hours are #SUPPORTHOURSOFBUSINESS#", federalEmployerIdentificationNumber));
					}
					else if (App.Settings.Theme == FocusThemes.Education)
					{
						listItems.Add(CodeLocalise("EmployerWasFound.Item1", "Please click \"Change #BUSINESS#:LOWER\" to edit."));
						listItems.Add(CodeLocalise("EmployerWasFound.Item2", "If you are a new hiring manager for this #BUSINESS#:LOWER, please click \"Next step\" to complete your Contact Information."));
						listItems.Add(CodeLocalise("EmployerWasFound.Item4", "If you need help completing your registration, contact our support team at #SUPPORTEMAIL# or #SUPPORTPHONE#. Our business hours are #SUPPORTHOURSOFBUSINESS#", federalEmployerIdentificationNumber));
					}

          ExtraInstructionsListItems.DataSource = listItems;
          ExtraInstructionsListItems.DataBind();
		    }
		    else
		    {
		      FEINExistsInstructionsLabel.Text = CodeLocalise("ConfirmCorrectEmployer.Text",
																													"Please use the drop down below to select an existing #BUSINESS#:LOWER already linked to the Federal Employee ID number that you have entered ({0}) or select �New #BUSINESS#� to enter your #BUSINESS#:LOWER details. ", federalEmployerIdentificationNumber);
		    }
		  }
		  IndustrialClassificationTitleLabel.Text = CodeLocalise("IndustrialClassification.Label", "Industry classification");
			BindBusinessUnitDropDown(validatedEmployer.Id);

		  IndustrialClassificationTitleLabel.Visible =
		    IndustrialClassificationLabel.Visible =
        StateEmployerIdentificationNumberTitleLabel.Visible = 
        StateEmployerIdentificationNumberLabel.Visible =
        AccountTypeTitleLabel.Visible = 
        AccountTypeLabel.Visible = 
        OwnershipTypeLabel.Visible = 
        OwnershipTypeTitleLabel.Visible = (App.Settings.Theme == FocusThemes.Workforce);

			if (App.Settings.Module == FocusModules.Talent)
			{
				FEINPanel.Visible = false;
				Panel2.Visible = false;
			}

			EmployerId = validatedEmployer.Id;
		}

		/// <summary>
		/// Clears the step
		/// </summary>
		public void ClearStep(bool clearBusinessUnits = true)
		{
			if (clearBusinessUnits)
			{
				BusinessUnit = null;
				BusinessUnits = null;
				BusinessUnitAddress = null;
				BusinessUnitDescription = null;
			}

			LegalNameLabel.Text =
				BusinessUnitLabel.Text =
				AddressLine1Label.Text =
				AddressLine2Label.Text =
				AddressTownCityLabel.Text =
				AddressStateLabel.Text =
				AddressCountyLabel.Text =
				AddressPostcodeZipLabel.Text =
				AddressCountryLabel.Text =
				ContactUrlLabel.Text =
				ContactPhoneLabel.Text =
				ContactAlternatePhoneLabel.Text =
				ContactAlternatePhone2Label.Text =
				FederalEmployerIdentificationNumberLabel.Text =
				StateEmployerIdentificationNumberLabel.Text =
				OwnershipTypeLabel.Text =
				IndustrialClassificationLabel.Text =
				AccountTypeLabel.Text =
				NoOfEmployeesLabel.Text =
				CompanyDescriptionLabel.Text = "";
		}

    /// <summary>
    /// Ensure address fields are pre-filled when returning to the step
    /// </summary>
    public void UpdateBusinessUnitDisplayDetails()
    {
      var businessUnitId = Convert.ToInt64((BusinessUnitTradeNameDropDownList.SelectedValue));
      if (businessUnitId > 0 || businessUnitId < -1)
        FillAddressDetails();
    }

		/// <summary>
		/// Initialises the step for education.
		/// </summary>
		/// <param name="validatedEmployer">The validated employer.</param>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <param name="previouslyProgressedToStep2">if set to <c>true</c> [previously progressed to step2].</param>
    public void InitialiseStepForEducation(ValidatedEmployerView validatedEmployer, long businessUnitId, bool previouslyProgressedToStep2)
    {
      InitialiseStep(validatedEmployer, previouslyProgressedToStep2);

      ChangeCompanyButton.Visible = true;
			ChangeCompanyButton.Text = CodeLocalise("ChangeCompany.Button", "Change #BUSINESS#:LOWER");
      BusinessUnitSelctorRow.Visible = 
        IndustrialClassificationTitleLabel.Visible = 
        IndustrialClassificationLabel.Visible = 
        CountyRow.Visible = 
        NoOfEmployeesRow.Visible = 
        FEINExistsInstructionsLabel.Visible =
        LegalNameRow.Visible = false;

      GetBusinessUnitDetails(businessUnitId);
      FillAddressDetails();
    }

	  /// <summary>
		/// Bind the Business Units to the drop down list
		/// </summary>
		/// <param name="employerId"> The Employer ID </param>
		/// <returns></returns>
		private bool BindBusinessUnitDropDown(long employerId)
		{
			var success = false;

			try
			{
				var employerClient = ServiceClientLocator.EmployerClient(App);
			  var validatedEmployer = false;

				var businessUnitsForDropDown = new Dictionary<long, string>();

				if (employerId > 0)
					BusinessUnits = employerClient.GetBusinessUnits(employerId);

			  if (employerId == 0 && ValidatedEmployerDetails.IsNotNull() && ValidatedEmployerDetails.Id == 0)
			  {
			    businessUnitsForDropDown.Add(-2, ValidatedEmployerDetails.Name);
			    validatedEmployer = true;
			  }

			  if (BusinessUnits.IsNotNull())
        {
  				foreach (var businessUnit in BusinessUnits)
  				{
  				  var businessUnitId = businessUnit.Id.GetValueOrDefault(0);
					  if (businessUnitId > 0)
					  {
						  var businessUnitAddress = employerClient.GetPrimaryBusinessUnitAddress(businessUnitId);
						  businessUnitsForDropDown.Add(businessUnitId, String.Format("{0} ({1})", businessUnit.Name, businessUnitAddress.Line1));
					  }
					  else
					  {
						  businessUnitsForDropDown.Add(businessUnitId, String.Format("{0} ({1})", businessUnit.Name, BusinessUnitAddress.Line1));
					  }
				  }        
        }

				BusinessUnitTradeNameDropDownList.DataSource = businessUnitsForDropDown;
				BusinessUnitTradeNameDropDownList.DataValueField = "Key";
				BusinessUnitTradeNameDropDownList.DataTextField = "Value";
				BusinessUnitTradeNameDropDownList.DataBind();

        if (validatedEmployer)
        {
          GetBusinessUnitDetailsFromValidatedEmployer();
          BusinessUnitChanged();
        }
        else
        {
					BusinessUnitTradeNameDropDownList.Items.AddLocalisedTopDefault("Global.BusinessUnitTradeName.TopDefault", "- Select #BUSINESS# -", "0");
					BusinessUnitTradeNameDropDownList.Items.Add(new ListItem(CodeLocalise("Global.EmployerName.TopDefault", "- New #BUSINESS# -"), "-1"));
        }

				success = true;
			}
			catch (Exception)
			{}

			return success;
		}

		/// <summary>
		/// Handles the dropdownlist index changing
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void BusinessUnitTradeNameDropDownList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			var businessUnitId = Convert.ToInt64((BusinessUnitTradeNameDropDownList.SelectedValue));

			try
			{
				if (businessUnitId > 0)
					GetBusinessUnitDetails(businessUnitId);
				else if (businessUnitId == -2)
					GetBusinessUnitDetailsFromValidatedEmployer();
				
				if (businessUnitId == 0)
					ClearStep(false);
				else
					BusinessUnitChanged(ValidatedEmployerDetails.IsNotNull() ? ValidatedEmployerDetails.LegalName : null);
			}
			catch (Exception)
			{}
			
		}

		/// <summary>
		/// Functionality for when the Business unit drop down list changes.  Triggered from SelectedIndexChanged and also after a Business Unit has been added
		/// </summary>
		/// <returns> Success or failure </returns>
		private bool BusinessUnitChanged(string legalName = null)
		{
			var success = false;

			try
			{
				var businessUnitId = Convert.ToInt64((BusinessUnitTradeNameDropDownList.SelectedValue));

        if (businessUnitId != -1)
          FillAddressDetails();
        else
          SelectBusinessUnitModal.Show(Convert.ToInt64(EmployerId), false, legalName);

				success = true;
			}
			catch (Exception)
			{}

			return success;
		}

		/// <summary>
		/// Gets the business unit details.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
    private void GetBusinessUnitDetails(long businessUnitId)
    {
      BusinessUnit = ServiceClientLocator.EmployerClient(App).GetBusinessUnit(businessUnitId);

      if (BusinessUnit.Id.IsNull())
        throw new Exception("Business unit ID is null");

      BusinessUnitAddress = ServiceClientLocator.EmployerClient(App).GetPrimaryBusinessUnitAddress(Convert.ToInt64(BusinessUnit.Id));

      var businessUnitDescriptions = ServiceClientLocator.EmployerClient(App).GetBusinessUnitDescriptions(Convert.ToInt64(BusinessUnit.Id));
      BusinessUnitDescription = businessUnitDescriptions.Find(desc => desc.IsPrimary);
    }

    private void GetBusinessUnitDetailsFromValidatedEmployer()
    {
      BusinessUnit = new BusinessUnitDto
      {
        AlternatePhone1 = ValidatedEmployerDetails.AlternatePhone1,
        AlternatePhone1Type = ValidatedEmployerDetails.AlternatePhone1Type,
        AlternatePhone2 = ValidatedEmployerDetails.AlternatePhone2,
        AlternatePhone2Type = ValidatedEmployerDetails.AlternatePhone2Type,
        IndustrialClassification = ValidatedEmployerDetails.IndustrialClassification,
        IsPrimary = true,
        Name = ValidatedEmployerDetails.Name,
				LegalName = ValidatedEmployerDetails.LegalName,
				OwnershipTypeId = ValidatedEmployerDetails.OwnershipTypeId.GetValueOrDefault(0),
				AccountTypeId = ValidatedEmployerDetails.AccountTypeId.GetValueOrDefault(0),
        IsPreferred = false,
        PrimaryPhone = ValidatedEmployerDetails.PrimaryPhone,
        PrimaryPhoneExtension = ValidatedEmployerDetails.PrimaryPhoneExtension,
        PrimaryPhoneType = ValidatedEmployerDetails.PrimaryPhoneType,
        Url = ValidatedEmployerDetails.Url,
        NoOfEmployees = ValidatedEmployerDetails.NoOfEmployees.GetValueOrDefault(0)
      };

      if (BusinessUnit.Url.IsNotNull() && BusinessUnit.Url.Length == 0)
        BusinessUnit.Url = null;

      BusinessUnitAddress = new BusinessUnitAddressDto
      {
        CountryId = ValidatedEmployerDetails.AddressCountryId.GetValueOrDefault(0),
        CountyId = ValidatedEmployerDetails.AddressCountyId,
        IsPrimary = true,
        Line1 = ValidatedEmployerDetails.AddressLine1,
        Line2 = ValidatedEmployerDetails.AddressLine2,
        Line3 = ValidatedEmployerDetails.AddressLine3,
        PostcodeZip = ValidatedEmployerDetails.AddressPostcodeZip,
        StateId = ValidatedEmployerDetails.AddressStateId.GetValueOrDefault(0),
        TownCity = ValidatedEmployerDetails.AddressTownCity
      };

      BusinessUnitDescription = new BusinessUnitDescriptionDto
      {
        Description = ValidatedEmployerDetails.Description,
        IsPrimary = true,
				Title = CodeLocalise("BusinessUnitDescriptionTitle.Text", "#BUSINESS# description")
      };
    }

		/// <summary>
		/// Fills the address fields with the details of the business unit
		/// </summary>
		/// <returns> Success or failure</returns>
		private bool FillAddressDetails()
		{
			bool success = false;

			try
			{
				//set properties
				EmployerId = BusinessUnit.EmployerId;
				AddressId = BusinessUnitAddress.Id;
				CountyId = BusinessUnitAddress.CountyId;
				BusinessUnitId = Convert.ToInt64(BusinessUnit.Id);

				var employer = ServiceClientLocator.EmployerClient(App);
				if (EmployerId.IsNull())
					throw new Exception("Employer ID is null");

			  if (EmployerId > 0)
			  {
			    var employerDetails = employer.GetEmployer(Convert.ToInt64(EmployerId));

			    // Employer
			    FederalEmployerIdentificationNumberLabel.Text = employerDetails.FEIN;
			    StateEmployerIdentificationNumberLabel.Text = employerDetails.StateEmployerIdentificationNumber;

          // Is employer Already approved
          IsAlreadyApproved = employerDetails.IsAlreadyApproved;
			  }
			  else if (ValidatedEmployerDetails.IsNotNull())
			  {
          FederalEmployerIdentificationNumberLabel.Text = ValidatedEmployerDetails.FederalEmployerIdentificationNumber;
          StateEmployerIdentificationNumberLabel.Text = ValidatedEmployerDetails.StateEmployerIdentificationNumber;

			    IsAlreadyApproved = true;
			  }

			  if (BusinessUnit.OwnershipTypeId.IsNotNull())
					OwnershipTypeLabel.Text =
							ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.OwnershipTypes).Where(
									x => x.Id == BusinessUnit.OwnershipTypeId).Select(x => x.Text).SingleOrDefault();

				if (BusinessUnit.AccountTypeId.IsNotNull())
					AccountTypeLabel.Text =
							ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.AccountTypes).Where(
									x => x.Id == BusinessUnit.AccountTypeId).Select(x => x.Text).SingleOrDefault();

			  if (BusinessUnit.NoOfEmployees.IsNotNull())
			  {
			    switch (BusinessUnit.NoOfEmployees)
			    {
            case 1:
              NoOfEmployeesLabel.Text = CodeLocalise("NoOfEmployeesRange1.Text", "1-49");
            break;
            case 2:
              NoOfEmployeesLabel.Text = CodeLocalise("NoOfEmployeesRange2.Text", "50-99");
            break;
            case 3:
              NoOfEmployeesLabel.Text = CodeLocalise("NoOfEmployeesRange3.Text", "100-499");
            break;
            case 4:
              NoOfEmployeesLabel.Text = CodeLocalise("NoOfEmployeesRange4.Text", "500-999");
			      break;
            case 5:
              NoOfEmployeesLabel.Text = CodeLocalise("NoOfEmployeesRange5.Text", "1000-4999");
            break;
            case 6:
              NoOfEmployeesLabel.Text = CodeLocalise("NoOfEmployeesRange6.Text", "5000+");
            break;
            default:
			        NoOfEmployeesLabel.Text = string.Empty;
			      break;
			    }
			  }


			  IndustrialClassificationLabel.Text = BusinessUnit.IndustrialClassification;
				ContactUrlLabel.Text = BusinessUnit.Url;
				ContactPhoneLabel.Text = BusinessUnit.PrimaryPhone;
				if (BusinessUnit.PrimaryPhoneExtension.IsNotNullOrEmpty())
					ContactPhoneLabel.Text += CodeLocalise("Extension.Text", " Ext: {0}",
																								 BusinessUnit.PrimaryPhoneExtension);
        ContactPhoneLabel.Text += string.Concat(" (", DisplayPhoneType(BusinessUnit.PrimaryPhoneType), ")");
				if (BusinessUnit.AlternatePhone1.IsNotNullOrEmpty())
					ContactAlternatePhoneLabel.Text = string.Concat(BusinessUnit.AlternatePhone1, " (",
																													DisplayPhoneType(BusinessUnit.AlternatePhone1Type), ")");
				if (BusinessUnit.AlternatePhone2.IsNotNullOrEmpty())
					ContactAlternatePhone2Label.Text = string.Concat(BusinessUnit.AlternatePhone2, " (",
																													 DisplayPhoneType(BusinessUnit.AlternatePhone2Type), ")");

				// Employer Address
				AddressLine1Label.Text = BusinessUnitAddress.Line1;
				AddressLine2Label.Text = BusinessUnitAddress.Line2;
				AddressTownCityLabel.Text = BusinessUnitAddress.TownCity;
				if (BusinessUnitAddress.StateId.IsNotNull())
					AddressStateLabel.Text =
							ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Id == BusinessUnitAddress.StateId)
									.Select(x => x.Text).SingleOrDefault();
				if (BusinessUnitAddress.CountyId.IsNotNull())
				{
					AddressCountyLabel.Text =
						ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Counties).Where(
							x => x.Id == BusinessUnitAddress.CountyId).Select(x => x.Text).SingleOrDefault();
					CountyId = BusinessUnitAddress.CountyId;
				}

				AddressPostcodeZipLabel.Text = BusinessUnitAddress.PostcodeZip;
				if (BusinessUnitAddress.CountryId.IsNotNull())
					AddressCountryLabel.Text =
							ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries).Where(
									x => x.Id == BusinessUnitAddress.CountryId).Select(x => x.Text).SingleOrDefault();

				// Employer Business Unit
				BusinessUnitLabel.Text = BusinessUnit.Name;
			  LegalNameLabel.Text = BusinessUnit.LegalName;

				// Employer Description
				if (BusinessUnitDescription.IsNotNull())
				{
					CompanyDescriptionLabel.Text = BusinessUnitDescription.Description;
					CompanyDescriptionId = BusinessUnitDescription.Id;
				}

				success = true;
			}
			catch (Exception)
			{
			}

			return success;
		}

		/// <summary>
		/// Handles the BusinessUnitSelected event of the SelectBusinessUnitModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Web.Code.Controls.User.BusinessUnitSelector.BusinessUnitSelectedEventArgs"/> instance containing the event data.</param>
		protected void SelectBusinessUnitModal_BusinessUnitSelected(object sender, BusinessUnitSelector.BusinessUnitSelectedEventArgs e)
		{
			var model = e.Model;

			if (e.Model.IsNotNull())
			{
				BusinessUnit = model.BusinessUnit;
				BusinessUnitAddress = model.BusinessUnitAddress;
				BusinessUnitDescription = model.BusinessUnitDescription;

				BusinessUnit.Id = -999;
        
        if (BusinessUnits.IsNull())
          BusinessUnits = new List<BusinessUnitDto>();

				BusinessUnits.Add(BusinessUnit);

				//Re-bind the Business unit dropdownlist
				BindBusinessUnitDropDown(0);

				EmployerRegistrationStep2UpdatePanel.Update();

				BusinessUnitTradeNameDropDownList.SelectedValue = BusinessUnit.Id.ToString();
				BusinessUnitChanged();
			}
		}

    /// <summary>
    /// Handles the OnClick event of the ChangeCompanyButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void ChangeCompanyButton_OnClick(object sender, EventArgs e)
    {
      OnEmployerChanged(new EventArgs());
    }

    /// <summary>
    /// Raises the <see cref="E:EmployerChanged" /> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected virtual void OnEmployerChanged(EventArgs eventArgs)
    {
      if (ChangeEmployer != null)
        ChangeEmployer(this, eventArgs);
    }

		/// <summary>
		/// Updates the model.
		/// </summary>
		/// <param name="model">The model.</param>
		internal void UpdateModel(TalentRegistrationModel model)
		{
			// don't let step 2 overwrite step 3 account type if it has a value, since step 3 is later in some scenarios
			// in some scenarios (where the FEIN is already in use and you are adding a business unit) step 2 is later
			// so don't overwrite step 2 if it has a value

			if (model.BusinessUnit != null && model.BusinessUnit.AccountTypeId == 0) model.BusinessUnit.AccountTypeId = null;
			if (BusinessUnit != null && BusinessUnit.AccountTypeId == 0) BusinessUnit.AccountTypeId = null;

			model.BusinessUnit = BusinessUnit;
			model.BusinessUnitDescription = (BusinessUnitDescription.IsNotNull()) ? BusinessUnitDescription : new BusinessUnitDescriptionDto { Title = CodeLocalise("CompanyDescription", "#BUSINESS# description") };
			model.BusinessUnitAddress = BusinessUnitAddress;
		}

    /// <summary>
    /// Displays the (localised) type of the phone.
    /// </summary>
    /// <param name="phoneType">Type of the phone.</param>
    /// <returns></returns>
    private string DisplayPhoneType(string phoneType)
    {
      if (Enum.IsDefined(typeof (PhoneTypes), phoneType))
      {
        var phoneEnum = (PhoneTypes)Enum.Parse(typeof(PhoneTypes), phoneType);
        return (phoneEnum == PhoneTypes.Phone) ? CodeLocalise("Global.PhoneTypes.Landline", "Landline") : CodeLocalise(phoneEnum, true);
      }

      return phoneType;
    }

    /// <summary>
    /// Localises the UI
    /// </summary>
    private void LocaliseUI()
    {
      AccountTypeTitleLabel.Text = HtmlLabel("AccountType.Label", "Account type");
      StateEmployerIdentificationNumberTitleLabel.Text = HtmlLabel("StateEmployerIdentificationNumber.Label", "State Employer ID");
      OwnershipTypeTitleLabel.Text = HtmlLabel("OwnershipType.Label", "Ownership type");
			BusinessUnitTradeNameRequired.ErrorMessage = CodeLocalise("BusinessUnitTradeNameRequired.RequiredErrorMessage", "Please select a #BUSINESS#:LOWER.");
    }

    /// <summary>
    /// Server validation of the business unit drop-down
    /// </summary>
    /// <param name="source"></param>
    /// <param name="args"></param>
	  protected void BusinessUnitTradeNameRequired_OnServerValidate(object source, ServerValidateEventArgs args)
    {
	    var val = BusinessUnitTradeNameDropDownList.SelectedValue;
			args.IsValid = val.Length > 0 && val != "0" && val != "-1";

			if (val == "-1")
				BusinessUnitTradeNameDropDownList.SelectValue("0");
	  }
	}
}