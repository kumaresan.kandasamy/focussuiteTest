﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobWizard.ascx.cs" Inherits="Focus.Web.Code.Controls.User.JobWizard" %>

<%@ Register src="~/Code/Controls/User/JobWizardStep1.ascx" tagname="JobWizardStep1" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobWizardStep2.ascx" tagname="JobWizardStep2" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobWizardStep3Path1.ascx" tagname="JobWizardStep3Path1" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobWizardStep3Path2Part1.ascx" tagname="JobWizardStep3Path2Part1" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobWizardStep3Path2Part2.ascx" tagname="JobWizardStep3Path2Part2" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobWizardStep4.ascx" tagname="JobWizardStep4" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobWizardStep5.ascx" tagname="JobWizardStep5" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobWizardStep6.ascx" tagname="JobWizardStep6" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobWizardStep7.ascx" tagname="JobWizardStep7" tagprefix="uc" %>

<table style="width: 100%;" role="presentation">
	<tr>
		<td><h2><%= HtmlLocalise("Create.Header", "Create a new job to post") %></h2></td>
		<td style="text-align: right;">
			<asp:Button ID="PreviewButtonTop" runat="server" SkinID="Button2" OnClick="PreviewButton_Click" UseSubmitBehavior="False" Visible="false" />
			<asp:Literal ID="PreviewSpacerTop" runat="server" Visible="false">&nbsp;&nbsp;</asp:Literal>
			<asp:Button ID="PreviousButtonTop" runat="server" SkinID="Button3" OnClick="PreviousButton_Click" CausesValidation="false" UseSubmitBehavior="False" />&nbsp;&nbsp;
			<asp:Button ID="NextButtonTop" runat="server" SkinID="Button2" OnClick="NextButton_Click" UseSubmitBehavior="True" />
		</td>
	</tr>
</table>

<%-- Progress Bar --%> 
<div class="progressBarWrap">
  <table class="progressBar" role="presentation"> 
	  <tr>
		  <td class="first" style="width: 16%; white-space: nowrap"><span ID="Step1ProgressBarItem" runat="server" EnableViewState="False"><asp:LinkButton ID="Step1ProgressBarItemLinkButton" runat="server" OnCommand="StepProgressBarLinkButton_Command" CommandArgument="1" /><asp:Literal ID="Step1ProgressBarItemLiteral" runat="server" /></span></td>
		  <td style="width: 16%; white-space: nowrap"><span ID="Step2Step3ProgressBarItem" runat="server" EnableViewState="False"><asp:LinkButton ID="Step2Step3ProgressBarItemLinkButton" runat="server" OnCommand="StepProgressBarLinkButton_Command" CommandArgument="2" /><asp:Literal ID="Step2Step3ProgressBarItemLiteral" runat="server" /></span></td>
		  <td style="width: 16%; white-space: nowrap"><span ID="Step4ProgressBarItem" runat="server" EnableViewState="False"><asp:LinkButton ID="Step4ProgressBarItemLinkButton" runat="server" OnCommand="StepProgressBarLinkButton_Command" CommandArgument="4" /><asp:Literal ID="Step4ProgressBarItemLiteral" runat="server" /></span></td>
		  <td style="width: 16%; white-space: nowrap"><span ID="Step5ProgressBarItem" runat="server" EnableViewState="False"><asp:LinkButton ID="Step5ProgressBarItemLinkButton" runat="server" OnCommand="StepProgressBarLinkButton_Command" CommandArgument="5" /><asp:Literal ID="Step5ProgressBarItemLiteral" runat="server" /></span></td>
		  <td style="width: 16%; white-space: nowrap"><span ID="Step6ProgressBarItem" runat="server" EnableViewState="False"><asp:LinkButton ID="Step6ProgressBarItemLinkButton" runat="server" OnCommand="StepProgressBarLinkButton_Command" CommandArgument="6" /><asp:Literal ID="Step6ProgressBarItemLiteral" runat="server" /></span></td>
		  <td class="last" style="width: 16%; white-space: nowrap"><span ID="Step7ProgressBarItem" runat="server" EnableViewState="False"><asp:LinkButton ID="Step7ProgressBarItemLinkButton" runat="server" OnCommand="StepProgressBarLinkButton_Command" CommandArgument="7" /><asp:Literal ID="Step7ProgressBarItemLiteral" runat="server" /></span></td>
	  </tr>
  </table>
</div>

<div style="clear:both;">
	<uc:JobWizardStep1 ID="Step1" runat="server" />
	<uc:JobWizardStep2 ID="Step2" runat="server" OnPathSelected="Step2_PathSelected" />
	<uc:JobWizardStep3Path1 ID="Step3Path1" runat="server" OnGoToStep="Step3Path1_OnGoToStep" />
	<uc:JobWizardStep3Path2Part1 ID="Step3Path2Part1" runat="server" />
	<uc:JobWizardStep3Path2Part2 ID="Step3Path2Part2" runat="server" OnGoToStep="Step3Path2Part2_OnGoToStep" />
	<uc:JobWizardStep4 ID="Step4" runat="server" />				
	<uc:JobWizardStep5 ID="Step5" runat="server" />
	<uc:JobWizardStep6 ID="Step6" runat="server" />
	<uc:JobWizardStep7 ID="Step7" runat="server" />
</div>

<br />
<br />

<table style="width: 100%;" role="presentation">
	<tr>
		<td/>
		<td style="text-align: right;">
			<asp:Button ID="PreviewButtonBottom" runat="server" SkinID="Button2" OnClick="PreviewButton_Click" UseSubmitBehavior="False" Visible="false" />
			<asp:Literal ID="PreviewSpacerBottom" runat="server" Visible="false">&nbsp;&nbsp;</asp:Literal>
			<asp:Button ID="PreviousButtonBottom" runat="server" SkinID="Button3" OnClick="PreviousButton_Click" CausesValidation="false" UseSubmitBehavior="False" />&nbsp;&nbsp;
			<asp:Button ID="NextButtonBottom" runat="server" SkinID="Button2" OnClick="NextButton_Click" UseSubmitBehavior="False" />
		</td>
	</tr>
</table>