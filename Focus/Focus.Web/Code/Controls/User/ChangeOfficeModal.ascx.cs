﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common.Extensions;
using Focus.Common;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class ChangeOfficeModal : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				LocaliseUI();
		}

		/// <summary>
		/// Binds the offices.
		/// </summary>
		private void BindOffices()
		{
			// Clear items from list and add a default selection
			OfficeDropDownList.Items.Clear();

			if (App.User.PersonId == null) return;

			var offices = ServiceClientLocator.EmployerClient(App).GetOfficesPersonManages(App.User.PersonId.Value, true);
			var currentOfficeForPerson = ServiceClientLocator.EmployerClient(App).GetCurrentOfficeForPerson(App.User.PersonId.Value);

			OfficeDropDownList.DataSource = offices;
			OfficeDropDownList.DataValueField = "Id";
			OfficeDropDownList.DataTextField = "OfficeName";
			OfficeDropDownList.DataBind();
			OfficeDropDownList.Items.AddLocalisedTopDefault("Offices.Select.NoEdit", "- select -");

			if (currentOfficeForPerson != null && currentOfficeForPerson.StartTime.Date == DateTime.Today)
				OfficeDropDownList.SelectValue(currentOfficeForPerson.OfficeId.ToString());
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			ChangeOfficeDropDownValidator.ErrorMessage = CodeLocalise("ChangeOfficeDropDownValidator.ErrorMessage", "Please select a valid office from the list.");
		}

		/// <summary>
		/// Shows the modal.
		/// </summary>
		/// <param name="top">The top.</param>
		public void Show(int top = 210)
		{
			ChangeOffice.Y = top;
			ChangeOffice.Show();
			BindOffices();
		}

		protected void SaveCurrentOfficeClick(object sender, EventArgs e)
		{
			if (App.User.PersonId != null)
				ServiceClientLocator.EmployerClient(App).SaveCurrentOfficeForPerson(OfficeDropDownList.SelectedValueToLong(), App.User.PersonId.Value);
		}
	}
}