#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Views;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class JobWizardSimilarJobs : UserControlBase
	{
    private string _addSelectionButtonText;
    private string _noSelectionErrorText;
    private string _addSelectionButtonOnClientClick;
	  private string _targetControlRenderId;

		/// <summary>
		/// Gets or sets the target control id.
		/// </summary>
		public string TargetControlId { get; set; }

		/// <summary>
		/// Gets or sets the on add event javascript method.
		/// </summary>
		/// <value>The on add event javascript method.</value>
		public string OnAddEventJavascriptMethod { get; set; }

    /// <summary>
    /// Gets or sets the job type.
    /// </summary>
    /// <value>The type.</value>
    public JobTypes JobType
    {
      get
      {
        return GetViewStateValue<JobTypes>("JobWizardSimilarJobs:JobType");
      }
      set
      {
        SetViewStateValue("JobWizardSimilarJobs:JobType", value);
        ApplyTheme();
      }
    }

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				LocaliseUI();

      _addSelectionButtonText = CodeLocalise("AddSelectionButton.Text", "Add selected text to description");
		  _noSelectionErrorText = HtmlLocalise("NoSelectionError.Text", "Please highlight required text using your cursor to select it");
		  _targetControlRenderId = GetControlRenderId(TargetControlId);
      _addSelectionButtonOnClientClick = "JobWizardSimilarJobs_HideSelectionValidators(); return AddSelectectTextAreaText('{0}','{1}','{2}');";
		}

		#region ObjectDataSource Methods

		/// <summary>
		/// Gets the similar jobs.
		/// </summary>
		/// <returns></returns>
		public List<SimilarJobView> GetSimilarJobs(string search, JobTypes jobType)
		{
      //JobTypes type;
      //Enum.TryParse(jobType, out type);
			return ServiceClientLocator.JobClient(App).GetSimilarJobs(search, jobType);
		}

		/// <summary>
		/// Handles the ItemDataBound event of the JobsListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void SimilarJobsListView_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var addSelectionButton = (Button)e.Item.FindControl("AddSelectionButton");
			addSelectionButton.Text = _addSelectionButtonText;

      var selectionErrorLabel = (Label)e.Item.FindControl("SelectionErrorLabel");
      selectionErrorLabel.Text = _noSelectionErrorText;

      addSelectionButton.OnClientClick = string.Format(_addSelectionButtonOnClientClick, _targetControlRenderId, OnAddEventJavascriptMethod, selectionErrorLabel.ClientID);

			// This is a hack to fix the fact the ListView doesn't 
			// bind the <%# ... %> tags so we have to place it outside
      SimilarJobsHeaderTable.Visible = true;
		}

    /// <summary>
    /// Handles the Selecting event of the SimilarJobsDataSource control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
    protected void SimilarJobsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
      e.InputParameters["jobType"] = JobType;
    }

    /// <summary>
    /// Clears out any previously selected items
    /// </summary>
    public void ClearList()
    {
      SimilarJobsListView.Items.Clear();

      SimilarJobsHeaderTable.Visible = false;
      SimilarJobsListView.Visible = false;

      SearchTextBox.Text = "";
    }

		#endregion

		#region Ajax events

		/// <summary>
		/// Handles the Click event of the SearchButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SearchButton_Click(object sender, EventArgs e)
		{
			// This is a hack to fix the fact the ListView doesn't 
			// bind the <%# ... %> tags so we have to place it outside
      SimilarJobsHeaderTable.Visible = false;
		  SimilarJobsListView.Visible = true;

			SimilarJobsListView.DataBind();
		}

		#endregion

		#region Localise the UI

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			SearchButton.Text = CodeLocalise("Global.Go.Text.NoEdit", "Go");
		}

    /// <summary>
    /// Applies text based on the job type
    /// </summary>
    private void ApplyTheme()
    {
      if (JobType.IsInternship())
      {
        Instructions1Literal.Text = HtmlLocalise("Instructions1Internship.Text", "Find an internship description that is similar to yours.").ToSentenceCase();
        Instructions2Literal.Text = HtmlLocalise("Instructions2Internship.Text", " Select it below and click \"Go\" to copy it into your internship description.");
        SearchLiteral.Text = HtmlLocalise("SearchInternship.Text", "Search internship listings").ToSentenceCase();
        JobTitleColumn.Text = HtmlLocalise("InternshipTitleColumn.Title", "Internship Title");
      }
      else
      {
        Instructions1Literal.Text = HtmlLocalise("Instructions1Job.Text", "Find an job description that is similar to yours.").ToSentenceCase();
        Instructions2Literal.Text = HtmlLocalise("Instructions2Job.Text", " Select it below and click \"Go\" to copy it into your job description.");
        SearchLiteral.Text = HtmlLocalise("SearchJob.Text", "Search job listings").ToSentenceCase();
        JobTitleColumn.Text = HtmlLocalise("JobTitleColumn.Title", "Job Title");
      }

    }

		#endregion
	}
}