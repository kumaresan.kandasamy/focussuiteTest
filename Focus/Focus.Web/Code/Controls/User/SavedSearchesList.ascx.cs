﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.Criteria.SavedSearch;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.WebTalent.Controls;
using Framework.Core;

#endregion


namespace Focus.Web.Code.Controls.User
{
  public partial class SavedSearchesList : UserControlBase
	{
    private int _savedSearchCount;

    private SavedSearchCriteria SavedSearchCriteria
    {
      get { return GetViewStateValue<SavedSearchCriteria>("SavedSearches:SearchCriteria"); }
      set { SetViewStateValue("SavedSearches:SearchCriteria", value); }
    }

    private long _savedSearchId
    {
      get { return GetViewStateValue<long>("SavedSearches:SavedSearchID"); }
      set { SetViewStateValue("SavedSearches:SavedSearchID", value); }
    }

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        Bind();
      }
    }
		
    /// <summary>
    /// Handles the Command event of the SearchRowAction control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected void SearchRowAction_Command(object sender, CommandEventArgs e)
    {
      int rowId;
      long savedSearchId;
      
			int.TryParse(e.CommandArgument.ToString(), out rowId);
      long.TryParse(SavedSearches.DataKeys[rowId]["Id"].ToString(), out savedSearchId);
      var name = SavedSearches.DataKeys[rowId]["Name"].ToString();

			if (savedSearchId == 0) throw new Exception("Search Id required");

      switch (e.CommandName)
      {
        case "Edit":
					SaveSearchAction.Show(savedSearchId);
          break;

        case "Delete":
          _savedSearchId = savedSearchId;

          DeleteConfirmationModal.Show(CodeLocalise("DeleteConfirmation.Title", "Delete Saved Search"),
                                CodeLocalise("DeleteConfirmation.Body", "Are you sure you want to delete {0}?", name),
                                CodeLocalise("Global.Cancel.Text", "Cancel"),
                                okText: CodeLocalise("DeleteConfirmation.Ok.Text", "Delete saved search"), okCommandName: "Delete", okCommandArgument: savedSearchId.ToString()); 
          break;
      }

      // Any command that has the OK button doesn't need to be bound here
      // as it is done in the Ok Command event handler
      SavedSearches.DataBind();
    }

    /// <summary>
    /// Handles the OkCommand event of the Confirmation control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected void Confirmation_OkCommand(object sender, CommandEventArgs e)
    {
      long savedSearchId = 0;
      long.TryParse(e.CommandArgument.ToString(),out savedSearchId);

      ServiceClientLocator.SearchClient(App).DeleteCandidateSavedSearch(savedSearchId);
      SavedSearches.DataBind();
    }

		/// <summary>
		/// Handles the ItemDeleting event of the SavedSearchesList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewDeleteEventArgs"/> instance containing the event data.</param>
    protected void SavedSearchesList_ItemDeleting(object sender , ListViewDeleteEventArgs e)
    {
      //needs implementing , even though it isnt doing anything
    }

    /// <summary>
    /// Handles the OkCommand event of the DeleteConfirmationModal control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected void DeleteConfirmationModal_OkCommand(object sender, CommandEventArgs e)
    {
      ServiceClientLocator.SearchClient(App).DeleteCandidateSavedSearch(_savedSearchId);
      Bind();
     
    }

		/// <summary>
		/// Handles the Completed event of the SaveSearchAction control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SaveSearchAction_Completed(object sender, EventArgs e)
    {
     Bind();
    }

		/// <summary>
		/// Gets the saved searches.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
    public List<SavedSearchDto> GetSavedSearches(SavedSearchCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
    {
      var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

      criteria.PageSize = maximumRows;
      criteria.PageIndex = pageIndex;
      criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;
     
      var savedSearches = ServiceClientLocator.SearchClient(App).GetCandidateSavedSearch(criteria);
      _savedSearchCount = savedSearches.TotalCount;
      return savedSearches;
    }

    /// <summary>
    /// Handles the Selecting event of the SearchResultDataSource control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
    protected void SearchResultDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
      if (SavedSearchCriteria.IsNull())
      {
        SavedSearchCriteria = new SavedSearchCriteria{OrderBy = "name asc" };
      }

      e.InputParameters["criteria"] = SavedSearchCriteria;
    }

		/// <summary>
		/// Binds this instance.
		/// </summary>
    private void Bind()
    {
      SavedSearches.DataBind();
      ResultCount.Text = CodeLocalise("ResultCount.Text", "{0} results found", SearchResultListPager.TotalRowCount.ToString());

      SearchResultListPager.Visible = SavedSearches.Items.Count > 0;
    }

    /// <summary>
    /// Gets the saved search count.
    /// </summary>
    /// <returns></returns>
    public int GetSavedSearchCount()
    {
      return _savedSearchCount;
    }

    /// <summary>
    /// Gets the jobs count.
    /// </summary>
    /// <returns></returns>
    public int GetSavedSearchCount(SavedSearchCriteria criteria)
    {
      return _savedSearchCount;
    }

    #region List View Binding

		/// <summary>
		/// Formats the alert settings.
		/// </summary>
		/// <param name="savedSearch">The saved search.</param>
		/// <returns></returns>
    protected string FormatAlertSettings(SavedSearchDto savedSearch)
    {
      var result = new StringBuilder("");

      result.AppendFormat("{0}, ", savedSearch.AlertEmailRequired ? CodeLocalise("Global.On.Text", "On") : CodeLocalise("Global.Off.Text", "Off"));

      if (savedSearch.AlertEmailFrequency.IsNotNull()) result.AppendFormat("{0}, ", CodeLocalise(savedSearch.AlertEmailFrequency));

      if (savedSearch.AlertEmailFormat.IsNotNull()) result.AppendFormat("{0}, ", CodeLocalise(savedSearch.AlertEmailFormat));

      var resultString = result.ToString();
      return resultString.Substring(0, resultString.Length - 2);
    }

    /// <summary>
    /// Handles the OnItemDataBound event of the saved searches list.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ListViewItemEventArgs"/> instance containing the event data.</param>
    protected void SavedSearchesList_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
      var url = (HtmlAnchor) e.Item.FindControl("SavedSearch_Url");
      if (App.Settings.Module == FocusModules.Assist)
      {
        url.HRef = UrlBuilder.AssistPool(TalentPoolListTypes.AllResumes, 0,
                                         ((SavedSearchDto) e.Item.DataItem).Id.ToString());
      }
      else
      {
       url.HRef = UrlBuilder.TalentPool(PoolResult.Tabs.AllResumes, null, ((SavedSearchDto) e.Item.DataItem).Id);
      }
    
      var criteriaSummary = (CandidateSearchCriteriaSummary)e.Item.FindControl("CriteriaSummary");

      // Deserialize the string in the dto
      var criteria = ((SavedSearchDto)e.Item.DataItem).SearchCriteria;
      if (criteria.IsNotNullOrEmpty())
      {
        var searchCriteria = new CandidateSearchCriteria();
        searchCriteria = (CandidateSearchCriteria)criteria.Deserialize(searchCriteria.GetType());
        criteriaSummary.UpdateSearchCriteria(searchCriteria);
      }
    }

    #endregion
  }
  
}