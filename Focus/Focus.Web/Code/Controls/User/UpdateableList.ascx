﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdateableList.ascx.cs" Inherits="Focus.Web.Code.Controls.User.UpdateableList" %>

<asp:Panel runat="server" ID="ItemsListPanel">
<asp:ListView ID="ItemsList" runat="server" >
	<LayoutTemplate>
		<table class="deletableListItemTable">
			<tr runat="server" ID="itemPlaceHolder"></tr>
		</table>
	</LayoutTemplate>
	<ItemTemplate>
		<tr>
			<td class="column1"><%# Container.DataItem %>
			</td>
			<td class="column2">
				<asp:ImageButton ID="ItemsListRemoveImageButton" runat="server"  OnCommand="ItemsListRemoveButton_Command" CommandArgument="<%# Container.DataItemIndex %>" 
													CommandName="Remove" ImageUrl="<%# UrlBuilder.ButtonDeleteIcon() %>" CausesValidation="False" />
			</td>
		</tr>
	</ItemTemplate>
</asp:ListView>
<asp:HiddenField runat="server" ID="ItemCount"/>
</asp:Panel>