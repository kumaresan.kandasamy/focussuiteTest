﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchCriteriaWorkDays.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SearchCriteriaWorkDays" %>

<div class="singleAccordionContentWrapper">
	<asp:Panel ID="WorkDaysHeaderPanel" runat="server" CssClass="singleAccordionTitle singleAccordionStyle2">
		<table role="presentation">
			<tr>
				<td style="width:30px;">
					<span><asp:Image ID="WorkDaysHeaderImage" runat="server" alt="." Height="22" Width="22"/></span>
				</td>
				<td class="accordionLabel">
					<span class="collapsiblePanelHeaderLabel cpHeaderControl">
					  <a href="#">
					    <%= HtmlLocalise("WorkAvailabiltiy.Label", "WORK AVAILABILITY")%>
					  </a>
					</span>
				</td>
				<td>
					<div class="tooltip">
						  <asp:Literal runat="server" ID="ToolTipster"></asp:Literal>
					</div>
				</td>
			</tr>
		</table>
	</asp:Panel>
  <asp:Panel ID="WorkDaysContentPanel" runat="server" CssClass="singleAccordionContent singleAccordionStyle2">
	  <table role="presentation" style="width:100%;">
		  <tr class="first">
			  <td>
				  <%= HtmlLocalise("NormalWorkDays.Label", "Normal work days")%>&nbsp;
			  </td>
        <td>
          <asp:CheckBoxList runat="server" ID="WeekDaysCheckBoxList" RepeatDirection="Horizontal" CssClass="daysOfWeekCheckBoxList" role="presentation"/>
        </td>
		  </tr>
		  <tr>
			  <td>
				  &nbsp;
			  </td>
			  <td>
          <asp:CheckBoxList runat="server" ID="WeekendDaysCheckBoxList" RepeatDirection="Horizontal" CssClass="daysOfWeekCheckBoxList" role="presentation"/>
			  </td>
		  </tr>
		  <tr id="VariesTableRow" runat="server">
			  <td>
				  &nbsp;
			  </td>
			  <td>
          <asp:CheckBox runat="server" ID="VariesCheckBox" />
			  </td>
		  </tr>
      <asp:PlaceHolder runat="server" ID="WorkWeeksPlaceHolder">
        <tr>
          <td class="last">
					  <%= HtmlLabel(WorkWeekDropDown, "NormalWorkWeek.Label", "Hours")%>&nbsp;
				  </td>
          <td>
            <asp:DropDownList runat="server" ID="WorkWeekDropDown" Width="230px"/>
          </td>
          </tr>
      </asp:PlaceHolder>
    </table>
	</asp:Panel>
</div>
<act:CollapsiblePanelExtender ID="WorkDaysPanelExtender" runat="server" TargetControlID="WorkDaysContentPanel"
        ExpandControlID="WorkDaysHeaderPanel" CollapseControlID="WorkDaysHeaderPanel"
        Collapsed="True" ImageControlID="WorkDaysHeaderImage" SuppressPostBack="true" />

<script type="text/javascript">
  Sys.Application.add_load(function () {
    focusSuite.searchCriteriaWorkDays.BindSearchCriteriaWorkDaysOnClick('<%=WeekDaysCheckBoxList.ClientID %>', '<%=WeekendDaysCheckBoxList.ClientID %>');
  });
</script>