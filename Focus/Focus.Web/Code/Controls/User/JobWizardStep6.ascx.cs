﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Web.ViewModels;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class JobWizardStep6 : JobWizardControl
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				LocaliseUI();

			ApplyBranding();
			ApplyTheme();

      var clientValues = new NameValueCollection
		  {
		    {"HoursPerWeekTextBoxClientId", HoursPerWeekTextBox.ClientID},
		    {"EmploymentStatusDropDownClientId", EmploymentStatusDropDown.ClientID},
		    {"HoursPerWeekEmploymentValidatorClientId", HoursPerWeekEmploymentValidator.ClientID},
        {"PartTimeEmploymentLimit", App.Settings.PartTimeEmploymentLimit.ToString(CultureInfo.InvariantCulture)},
        {"AllHoursPerWeekMessage", App.Settings.HasMinimumHoursPerWeek
                                     ? CodeLocalise("MaximumHoursPerWeek.RangeError", "Maximum/actual hours must be a number between 0 and 168")
                                     : CodeLocalise("HoursPerWeek.RangeError", "Enter a number between 0 and 168")},
        {"PartTimeHoursPerWeekMessage", App.Settings.HasMinimumHoursPerWeek 
                                     ? CodeLocalise("PartTimeMaxHoursPerWeekMessage.RangeError", "Maximum/actual hours must be a number less than {0} for part-time jobs", App.Settings.PartTimeEmploymentLimit)
                                     : CodeLocalise("PartTimeHoursPerWeekMessage.RangeError", "Hours must be a number less than {0} for part-time jobs", App.Settings.PartTimeEmploymentLimit)},
        {"FullTimeHoursPerWeekMessage", App.Settings.HasMinimumHoursPerWeek 
                                     ? CodeLocalise("FullTimeHoursPerWeekMessage.RangeError", "Maximum/actual hours must be a number between {0} and 168 for full-time jobs", App.Settings.PartTimeEmploymentLimit)
                                     : CodeLocalise("FullTimeHoursPerWeekMessage.RangeError", "Hours must be a number between {0} and 168 for full-time jobs", App.Settings.PartTimeEmploymentLimit)}
		  };
      RegisterCodeValuesJson("JobWizardStep6_NameValueCollection", "JobWizardStep6_NameValueCollection", clientValues);
		}

		#region Bind Methods

		/// <summary>
		/// Binds the controls for the step.JobId
		/// </summary>
		/// <param name="model">The model.</param>
		internal void BindStep(JobWizardViewModel model)
		{
			BindControls();

			if (model.Job.MinSalary.IsNotNull()) MinimumSalaryTextBox.Text = model.Job.MinSalary.GetValueOrDefault().ToString("F2");
			if (model.Job.MaxSalary.IsNotNull()) MaximumSalaryTextBox.Text = model.Job.MaxSalary.GetValueOrDefault().ToString("F2");
			if (model.Job.SalaryFrequencyId.IsNotNull()) SalaryFrequencyDropDown.SelectValue(model.Job.SalaryFrequencyId.GetValueOrDefault().ToString(CultureInfo.InvariantCulture));
			if (model.Job.HideSalaryOnPosting.HasValue) {HideSalaryCheckbox.Checked = model.Job.HideSalaryOnPosting.GetValueOrDefault();}
      MeetsMinimumWageRequirementCheckBox.Checked = model.Job.MeetsMinimumWageRequirement;

      if (model.Job.EmploymentStatusId.IsNotNull()) EmploymentStatusDropDown.SelectValue(model.Job.EmploymentStatusId.GetValueOrDefault().ToString(CultureInfo.InvariantCulture));

      if (model.Job.HoursPerWeek.IsNotNull()) HoursPerWeekTextBox.Text = model.Job.HoursPerWeek.GetValueOrDefault().ToString("F");
      if (model.Job.MinimumHoursPerWeek.IsNotNull()) MinimumHoursPerWeekTextBox.Text = model.Job.MinimumHoursPerWeek.GetValueOrDefault().ToString("F");

			if (model.Job.OverTimeRequired.IsNotNull()) OvertimeRequiredCheckBox.Checked = model.Job.OverTimeRequired.GetValueOrDefault();

			if (model.Job.IsCommissionBased.IsNotNull()) IncomeConditionsRadioButtonList.Items[0].Selected = model.Job.IsCommissionBased.GetValueOrDefault();
			if (model.Job.IsSalaryAndCommissionBased.IsNotNull()) IncomeConditionsRadioButtonList.Items[1].Selected = model.Job.IsSalaryAndCommissionBased.GetValueOrDefault();
			OtherIncomeConditionsCheckbox.Checked = (IncomeConditionsRadioButtonList.Items[0].Selected ||
			                                         IncomeConditionsRadioButtonList.Items[1].Selected);
			IncomeConditionsRadioButtonList.Enabled = OtherIncomeConditionsCheckbox.Checked;

			if(model.Job.NormalWorkDays.IsNotNull())
			{
				WorkMonCheckBox.Checked = ((model.Job.NormalWorkDays & DaysOfWeek.Monday) == DaysOfWeek.Monday);
				WorkTueCheckBox.Checked = ((model.Job.NormalWorkDays & DaysOfWeek.Tuesday) == DaysOfWeek.Tuesday);
				WorkWedCheckBox.Checked = ((model.Job.NormalWorkDays & DaysOfWeek.Wednesday) == DaysOfWeek.Wednesday);
				WorkThuCheckBox.Checked = ((model.Job.NormalWorkDays & DaysOfWeek.Thursday) == DaysOfWeek.Thursday);
				WorkFriCheckBox.Checked = ((model.Job.NormalWorkDays & DaysOfWeek.Friday) == DaysOfWeek.Friday);
				WorkSatCheckBox.Checked = ((model.Job.NormalWorkDays & DaysOfWeek.Saturday) == DaysOfWeek.Saturday);
				WorkSunCheckBox.Checked = ((model.Job.NormalWorkDays & DaysOfWeek.Sunday) == DaysOfWeek.Sunday);

			    WorkWeekdaysCheckBox.Checked = WorkMonCheckBox.Checked
			                                   && WorkTueCheckBox.Checked
			                                   && WorkWedCheckBox.Checked
			                                   && WorkThuCheckBox.Checked
			                                   && WorkFriCheckBox.Checked;

			    WorkWeekendCheckBox.Checked = WorkSatCheckBox.Checked
			                                   && WorkSunCheckBox.Checked;
			}

			WorkVariesCheckBox.Checked = model.Job.WorkDaysVary.GetValueOrDefault();
			if (model.Job.NormalWorkShiftsId.IsNotNull()) NormalWorkShiftsDropDown.SelectValue(model.Job.NormalWorkShiftsId.GetValueOrDefault().ToString());
			if (model.Job.JobTypeId.IsNotNull()) JobTypeDropDown.SelectValue(model.Job.JobTypeId.ToString());
			if (model.Job.JobStatusId.IsNotNull()) JobStatusDropDown.SelectValue(model.Job.JobStatusId.GetValueOrDefault().ToString());

			if(model.Job.LeaveBenefits.IsNotNull())
			{
				PaidHolidaysCheckBox.Checked = ((model.Job.LeaveBenefits & LeaveBenefits.PaidHolidays) == LeaveBenefits.PaidHolidays);
				VacationCheckBox.Checked = ((model.Job.LeaveBenefits & LeaveBenefits.Vacation) == LeaveBenefits.Vacation);
				LeaveSharingCheckBox.Checked = ((model.Job.LeaveBenefits & LeaveBenefits.LeaveSharing) == LeaveBenefits.LeaveSharing);
				SickCheckBox.Checked = ((model.Job.LeaveBenefits & LeaveBenefits.Sick) == LeaveBenefits.Sick);
				MedicalCheckBox.Checked = ((model.Job.LeaveBenefits & LeaveBenefits.Medical) == LeaveBenefits.Medical);
			}

			if(model.Job.RetirementBenefits.IsNotNull())
			{
				PensionPlanCheckBox.Checked = ((model.Job.RetirementBenefits & RetirementBenefits.PensionPlan) == RetirementBenefits.PensionPlan);
				FourZeroOneKCheckBox.Checked = ((model.Job.RetirementBenefits & RetirementBenefits.Four01K) == RetirementBenefits.Four01K);
				ProﬁtSharingCheckBox.Checked = ((model.Job.RetirementBenefits & RetirementBenefits.ProfitSharing) == RetirementBenefits.ProfitSharing);
				FourZeroThreeBPlanCheckBox.Checked = ((model.Job.RetirementBenefits & RetirementBenefits.Four03BPlan) == RetirementBenefits.Four03BPlan);
				DeferredCompensationCheckBox.Checked = ((model.Job.RetirementBenefits & RetirementBenefits.DeferredCompensation) == RetirementBenefits.DeferredCompensation);
			}

			if(model.Job.InsuranceBenefits.IsNotNull())
			{
				DentalCheckBox.Checked = ((model.Job.InsuranceBenefits & InsuranceBenefits.Dental) == InsuranceBenefits.Dental);
				HealthCheckBox.Checked = ((model.Job.InsuranceBenefits & InsuranceBenefits.Health) == InsuranceBenefits.Health);
				LifeCheckBox.Checked = ((model.Job.InsuranceBenefits & InsuranceBenefits.Life) == InsuranceBenefits.Life);
				DisabilityCheckBox.Checked = ((model.Job.InsuranceBenefits & InsuranceBenefits.Disability) == InsuranceBenefits.Disability);
				HealthSavingsCheckBox.Checked = ((model.Job.InsuranceBenefits & InsuranceBenefits.HealthSavings) == InsuranceBenefits.HealthSavings);
				VisionCheckBox.Checked = ((model.Job.InsuranceBenefits & InsuranceBenefits.Vision) == InsuranceBenefits.Vision);
				DomesticPartnerCoverageCheckBox.Checked = ((model.Job.InsuranceBenefits & InsuranceBenefits.DomesticPartnerCoverage) == InsuranceBenefits.DomesticPartnerCoverage);
			}

			if(model.Job.MiscellaneousBenefits.IsNotNull())
			{
				BeneﬁtsNegotiableCheckBox.Checked = ((model.Job.MiscellaneousBenefits & MiscellaneousBenefits.BenefitsNegotiable) == MiscellaneousBenefits.BenefitsNegotiable);
				TuitionAssistanceCheckBox.Checked = ((model.Job.MiscellaneousBenefits & MiscellaneousBenefits.TuitionAssistance) == MiscellaneousBenefits.TuitionAssistance);
				ClothingAllowanceCheckBox.Checked = ((model.Job.MiscellaneousBenefits & MiscellaneousBenefits.ClothingAllowance) == MiscellaneousBenefits.ClothingAllowance);
				ChildCareCheckBox.Checked = ((model.Job.MiscellaneousBenefits & MiscellaneousBenefits.ChildCare) == MiscellaneousBenefits.ChildCare);
				RelocationCheckBox.Checked = ((model.Job.MiscellaneousBenefits & MiscellaneousBenefits.Relocation) == MiscellaneousBenefits.Relocation);
				OtherCheckBox.Checked = ((model.Job.MiscellaneousBenefits & MiscellaneousBenefits.Other) == MiscellaneousBenefits.Other);
			}

			if (model.Job.OtherBenefitsDetails.IsNotNullOrEmpty()) OtherTextBox.Text = model.Job.OtherBenefitsDetails;

			if ((model.Job.MiscellaneousBenefits.IsNull() || model.Job.MiscellaneousBenefits == MiscellaneousBenefits.None) &&
			    (model.Job.InsuranceBenefits.IsNull() || model.Job.InsuranceBenefits == InsuranceBenefits.None) &&
			    (model.Job.RetirementBenefits.IsNull() || model.Job.RetirementBenefits == RetirementBenefits.None) &&
			    (model.Job.LeaveBenefits.IsNull() || model.Job.LeaveBenefits == LeaveBenefits.None) &&
			    model.Job.OtherBenefitsDetails.IsNullOrEmpty())
			{
				NoBenefitsRadioButton.Checked = true;
			}
			else
			{
				YesBenefitsRadioButton.Checked = true;
			}

      if (model.Job.DisplayBenefits.IsNotNull() && model.Job.DisplayBenefits)
		    YesShowBenefitsRadioButton.Checked = true;
      else
        NoShowBenefitsRadioButton.Checked = true;

		  if (App.Settings.ShowWorkNetLink && App.Settings.WorkNetLink.IsNotNullOrEmpty())
		  {
		    var showLink = true;
        var onetId = model.Job.OnetId;
        if (onetId.HasValue)
        {
					var socs = ServiceClientLocator.ExplorerClient(App).GetSOCForOnet(onetId.Value);

          var postalCodeView = ExtractPostalCodeFromLocation(model.JobLocations);
          var county = postalCodeView.IsNotNull() && postalCodeView.CountyKey.IsNotNullOrEmpty()
            ? ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Counties, 0, postalCodeView.CountyKey).FirstOrDefault(c => c.ExternalId.IsNotNullOrEmpty())
            : null;

          if (socs.Any() && county.IsNotNull())
          {
            var socCode = socs.First().Soc2010Code.Replace("-", "");
            var fipsCode = county.ExternalId.Substring(2);

            var link = string.Format(App.Settings.WorkNetLink, fipsCode, Server.UrlEncode(county.Text), socCode);
            WorkNetLinkHyperLink.NavigateUrl = link;
          }
          else
          {
            showLink = false;
          }
        }
        else
        {
          showLink = false;
        }

        WorkNetLinkPlaceHolder.Visible = showLink;
		  }

      RegisterCodeValuesJson("JobWizardStep6ScriptValues", "JobWizardStep6_CodeValues", InitialiseClientSideCodeValuesForSalary());
		}

    /// <summary>
    /// Extracts the postal code from the job locations
    /// </summary>
    /// <param name="jobLocations">The job's locations</param>
    /// <returns>The Postal Code View</returns>
    private PostalCodeViewDto ExtractPostalCodeFromLocation(IEnumerable<JobLocationDto> jobLocations)
    {
      var jobLocation = jobLocations.FirstOrDefault();
      if (jobLocation.IsNull())
        return null;

      var location = jobLocation.Location;

      // Split the relevent information	
      string postalCode = null;
      var lastIndexOfOpenBracket = location.LastIndexOf("(", StringComparison.Ordinal);
      if (lastIndexOfOpenBracket > 0)
      {
        var lastIndexOfCloseBracket = location.LastIndexOf(")", StringComparison.Ordinal);
        postalCode = location.Substring(lastIndexOfOpenBracket + 1, (lastIndexOfCloseBracket - (lastIndexOfOpenBracket + 1)));
      }
      else
      {
        var commaPos = location.LastIndexOf(",", System.StringComparison.Ordinal);
        if (commaPos > 0)
          postalCode = location.Substring(commaPos + 1);
      }

      return postalCode.IsNotNullOrEmpty()
				? ServiceClientLocator.CoreClient(App).EstablishLocation(postalCode, "", "")
        : null;
    }

		/// <summary>
		/// Unbinds the step.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
		internal JobWizardViewModel UnbindStep(JobWizardViewModel model)
		{
			model.Job.MinSalary = MinimumSalaryTextBox.AsNullableDecimal();
			model.Job.MaxSalary = MaximumSalaryTextBox.AsNullableDecimal();
			model.Job.SalaryFrequencyId = SalaryFrequencyDropDown.SelectedValueToLong();
			model.Job.HideSalaryOnPosting = HideSalaryCheckbox.Checked;
		  model.Job.MeetsMinimumWageRequirement = MeetsMinimumWageRequirementCheckBox.Checked;

			model.Job.EmploymentStatusId = EmploymentStatusDropDown.SelectedValueToLong();
			
      model.Job.HoursPerWeek = HoursPerWeekTextBox.AsNullableDecimal();
      model.Job.MinimumHoursPerWeek = MinimumHoursPerWeekTextBox.AsNullableDecimal();

			model.Job.OverTimeRequired = OvertimeRequiredCheckBox.Checked;

			model.Job.IsCommissionBased = IncomeConditionsRadioButtonList.Items[0].Selected;
			model.Job.IsSalaryAndCommissionBased = IncomeConditionsRadioButtonList.Items[1].Selected;

            var normalWorkDays = DaysOfWeek.None;
            if (WorkMonCheckBox.Checked) normalWorkDays = normalWorkDays | DaysOfWeek.Monday;
            if (WorkTueCheckBox.Checked) normalWorkDays = normalWorkDays | DaysOfWeek.Tuesday;
            if (WorkWedCheckBox.Checked) normalWorkDays = normalWorkDays | DaysOfWeek.Wednesday;
            if (WorkThuCheckBox.Checked) normalWorkDays = normalWorkDays | DaysOfWeek.Thursday;
            if (WorkFriCheckBox.Checked) normalWorkDays = normalWorkDays | DaysOfWeek.Friday;
            if (WorkSatCheckBox.Checked) normalWorkDays = normalWorkDays | DaysOfWeek.Saturday;
            if (WorkSunCheckBox.Checked) normalWorkDays = normalWorkDays | DaysOfWeek.Sunday;
            model.Job.NormalWorkDays = normalWorkDays;

			model.Job.WorkDaysVary = WorkVariesCheckBox.Checked;

			model.Job.NormalWorkShiftsId = NormalWorkShiftsDropDown.SelectedValueToLong();
		  model.Job.JobTypeId = JobTypeDropDown.SelectedValueToLong();
			model.Job.JobStatusId = JobStatusDropDown.SelectedValueToLong();

			var leaveBenefits = LeaveBenefits.None;
			if (PaidHolidaysCheckBox.Checked) leaveBenefits = leaveBenefits | LeaveBenefits.PaidHolidays;
			if (VacationCheckBox.Checked) leaveBenefits = leaveBenefits | LeaveBenefits.Vacation;
			if (LeaveSharingCheckBox.Checked) leaveBenefits = leaveBenefits | LeaveBenefits.LeaveSharing;
			if (SickCheckBox.Checked) leaveBenefits = leaveBenefits | LeaveBenefits.Sick;
			if (MedicalCheckBox.Checked) leaveBenefits = leaveBenefits | LeaveBenefits.Medical;
			model.Job.LeaveBenefits = leaveBenefits;

			var retirementBenefits = RetirementBenefits.None;
			if (PensionPlanCheckBox.Checked) retirementBenefits = retirementBenefits | RetirementBenefits.PensionPlan;
			if (FourZeroOneKCheckBox.Checked) retirementBenefits = retirementBenefits | RetirementBenefits.Four01K;
			if (ProﬁtSharingCheckBox.Checked) retirementBenefits = retirementBenefits | RetirementBenefits.ProfitSharing;
			if (FourZeroThreeBPlanCheckBox.Checked) retirementBenefits = retirementBenefits | RetirementBenefits.Four03BPlan;
			if (DeferredCompensationCheckBox.Checked) retirementBenefits = retirementBenefits | RetirementBenefits.DeferredCompensation;
			model.Job.RetirementBenefits = retirementBenefits;

			var insuranceBenefits = InsuranceBenefits.None;
			if (DentalCheckBox.Checked) insuranceBenefits = insuranceBenefits | InsuranceBenefits.Dental;
			if (HealthCheckBox.Checked) insuranceBenefits = insuranceBenefits | InsuranceBenefits.Health;
			if (LifeCheckBox.Checked) insuranceBenefits = insuranceBenefits | InsuranceBenefits.Life;
			if (DisabilityCheckBox.Checked) insuranceBenefits = insuranceBenefits | InsuranceBenefits.Disability;
			if (HealthSavingsCheckBox.Checked) insuranceBenefits = insuranceBenefits | InsuranceBenefits.HealthSavings;
			if (VisionCheckBox.Checked) insuranceBenefits = insuranceBenefits | InsuranceBenefits.Vision;
			if (DomesticPartnerCoverageCheckBox.Checked) insuranceBenefits = insuranceBenefits | InsuranceBenefits.DomesticPartnerCoverage;
			model.Job.InsuranceBenefits = insuranceBenefits;

			var miscellaneousBenefits = MiscellaneousBenefits.None;
			if (BeneﬁtsNegotiableCheckBox.Checked) miscellaneousBenefits = miscellaneousBenefits | MiscellaneousBenefits.BenefitsNegotiable;
			if (TuitionAssistanceCheckBox.Checked) miscellaneousBenefits = miscellaneousBenefits | MiscellaneousBenefits.TuitionAssistance;
			if (ClothingAllowanceCheckBox.Checked) miscellaneousBenefits = miscellaneousBenefits | MiscellaneousBenefits.ClothingAllowance;
			if (ChildCareCheckBox.Checked) miscellaneousBenefits = miscellaneousBenefits | MiscellaneousBenefits.ChildCare;
			if (RelocationCheckBox.Checked) miscellaneousBenefits = miscellaneousBenefits | MiscellaneousBenefits.Relocation;
			if (OtherCheckBox.Checked) miscellaneousBenefits = miscellaneousBenefits | MiscellaneousBenefits.Other;
			model.Job.MiscellaneousBenefits = miscellaneousBenefits;

			model.Job.OtherBenefitsDetails = OtherTextBox.TextTrimmed();

		  model.Job.DisplayBenefits = YesShowBenefitsRadioButton.Checked;

			return model;
		}

		/// <summary>
		/// Binds the controls.
		/// </summary>
		private void BindControls()
		{
			SalaryFrequencyDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Frequencies), null, CodeLocalise("SalaryFrequency.TopDefault", "- select frequency -"));
			EmploymentStatusDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.EmploymentStatuses), null, CodeLocalise("EmploymentStatus.TopDefault", "- select employment status -"), "");
			NormalWorkShiftsDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.WorkShifts), null, CodeLocalise("Global.WorkShifts.TopDefault", "- select work shift -"));
			JobTypeDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.JobTypes), null, CodeLocalise("JobType.TopDefault", "- select job type -"));
			JobStatusDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.JobStatuses), null, CodeLocalise("JobStatus.TopDefault", "- select job status -"));
		}

		#endregion

		#region Localise the UI

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			IncomeConditionsRadioButtonList.Items[0].Text = CodeLocalise("CommissionBasedCheckBox.Text", "This is a commission-based position.");
			IncomeConditionsRadioButtonList.Items[1].Text = CodeLocalise("SalaryAndCommissionCheckBox.Text", "This is a salary + commission-based position");
			WorkMonCheckBox.Text = CodeLocalise("Global.WeekDays.Monday.ShortFormat", "Mon");
			WorkTueCheckBox.Text = CodeLocalise("Global.WeekDays.Tuesday.ShortFormat", "Tue");
			WorkWedCheckBox.Text = CodeLocalise("Global.WeekDays.Wednesday.ShortFormat", "Wed");
			WorkThuCheckBox.Text = CodeLocalise("Global.WeekDays.Thursday.ShortFormat", "Thu");
			WorkFriCheckBox.Text = CodeLocalise("Global.WeekDays.Friday.ShortFormat", "Fri");
			WorkSatCheckBox.Text = CodeLocalise("Global.WeekDays.Saturday.ShortFormat", "Sat");
            WorkSunCheckBox.Text = CodeLocalise("Global.WeekDays.Sunday.ShortFormat", "Sun");
            WorkWeekdaysCheckBox.Text = CodeLocalise("Global.WeekDays.Weekdays.ShortFormat", "Weekdays");
            WorkWeekendCheckBox.Text = CodeLocalise("Global.WeekDays.Weekends.ShortFormat", "Weekends");
			WorkVariesCheckBox.Text = CodeLocalise("Varies.Text", "Varies");

			EmploymentStatusValidator.ErrorMessage = CodeLocalise("EmploymentStatusValidator.ErrorMessage", "Hours are required");
			JobTypeValidator.ErrorMessage = CodeLocalise("JobTypeValidator.ErrorMessage", "Job type is required");

			if (App.Settings.MandatoryHours)
			{
				HoursLabel.Text = HtmlRequiredLabel(EmploymentStatusDropDown, "EmploymentStatus.Label", "Hours");
				EmploymentStatusValidator.Enabled = true;
			}
			else
			{
				HoursLabel.Text = HtmlLabel("EmploymentStatus.Label", "Hours");
				EmploymentStatusValidator.Enabled = false;
			}

			if (App.Settings.MandatoryJobType)
			{
				JobTypeLabel.Text = HtmlRequiredLabel(JobTypeDropDown, "JobType.Label", "Job type");
				JobTypeValidator.Enabled = true;
			}
			else
			{
				JobTypeLabel.Text = HtmlLabel("JobType.Label", "Job type");
				JobTypeValidator.Enabled = false;
			}

      if (App.Settings.HasMinimumHoursPerWeek)
      {
				MinimumHoursLabel.Text = HtmlLocalise("Minimum.Text", "minimum");
	      MaximumHoursLabel.Text = App.Settings.MandatoryHoursPerWeek
																	 ? HtmlRequiredLabel(HoursPerWeekHeader, "MaximumActual.Text", "maximum/actual")
																	 : HtmlLocalise("MaximumActual.Text", "maximum/actual");

				HoursPerWeekHeader.Text = HtmlLabel("HoursPerWeekRange.Label", "Hours per week range");
				MinimumHoursPerWeekRangeValidator.ErrorMessage = string.Concat(CodeLocalise("MinimumHoursPerWeek.RangeError", "Minimum hours must be a number between 0 and 168"), "<br />");
        HoursPerWeekCompareValidator.ErrorMessage = string.Concat(CodeLocalise("MinimumHoursPerWeek.CompareError", "Maximum hours must have a value greater than the minimum hours"), "<br />");
        HoursPerWeekCustomValidator.ErrorMessage = string.Concat(CodeLocalise("MinimumHoursPerWeek.CustomError", "Actual hours should be entered into the maximum hours field"), "<br />");
				HoursPerWeekRequiredValidator.ErrorMessage = string.Concat(CodeLocalise("HoursPerWeekRequiredValidator.WithMin.Error", "Maximum/actual hours per week must be entered"), "<br />");
			}
      else
      {
        HoursPerWeekHeader.Text = App.Settings.MandatoryHoursPerWeek
					? HtmlRequiredLabel(HoursPerWeekTextBox, "HoursPerWeek.Label", "Hours per week")
					: HtmlLabel("HoursPerWeek.Label", "Hours per week");

				HoursPerWeekRequiredValidator.ErrorMessage = string.Concat(CodeLocalise("HoursPerWeekRequiredValidator.WithNoMin.Error", "Hours per week must be entered"), "<br />");
			}

			PaidHolidaysCheckBox.Text = CodeLocalise("PaidHolidaysCheckBox.Text", "Paid holidays");
			SickCheckBox.Text = CodeLocalise("SickCheckBox.Text", "Sick");
			VacationCheckBox.Text = CodeLocalise("VacationCheckBox.Text", "Vacation/Paid time off");
			MedicalCheckBox.Text = CodeLocalise("MedicalCheckBox.Text", "Medical");
			LeaveSharingCheckBox.Text = CodeLocalise("LeaveSharingCheckBox.Text", "Leave sharing");
			PensionPlanCheckBox.Text = CodeLocalise("PensionPlanCheckBox.Text", "Pension plan");
			FourZeroOneKCheckBox.Text = CodeLocalise("FourZeroOneKCheckBox.Text", "401K");
			ProﬁtSharingCheckBox.Text = CodeLocalise("ProﬁtSharingCheckBox.Text", "Proﬁt sharing");
			DomesticPartnerCoverageCheckBox.Text = CodeLocalise("DomesticPartnerCoverage.Text", "Domestic Partner Coverage");
			DeferredCompensationCheckBox.Text = CodeLocalise("DeferredCompensation.Text", "Deferred Compensation");
			FourZeroThreeBPlanCheckBox.Text = CodeLocalise("FourZeroThreeBPlan.Text", "403B Plan");
			DentalCheckBox.Text = CodeLocalise("DentalCheckBox.Text", "Dental");
			DisabilityCheckBox.Text = CodeLocalise("DisabilityCheckBox.Text", "Disability");
			HealthCheckBox.Text = CodeLocalise("HealthCheckBox.Text", "Health");
			HealthSavingsCheckBox.Text = CodeLocalise("HealthSavingsCheckBox.Text", "Health savings");
			LifeCheckBox.Text = CodeLocalise("LifeCheckBox.Text", "Life");
			VisionCheckBox.Text = CodeLocalise("VisionCheckBox.Text", "Vision");
			BeneﬁtsNegotiableCheckBox.Text = CodeLocalise("BeneﬁtsNegotiableCheckBox.Text", "Beneﬁts negotiable");
			ChildCareCheckBox.Text = CodeLocalise("ChildCareCheckBox.Text", "Child care");
			TuitionAssistanceCheckBox.Text = CodeLocalise("TuitionAssistanceCheckBox.Text", "Tuition assistance");
			RelocationCheckBox.Text = CodeLocalise("RelocationCheckBox.Text", "Relocation");
			ClothingAllowanceCheckBox.Text = CodeLocalise("ClothingAllowanceCheckBox.Text", "Clothing/Uniform");
			OtherCheckBox.Text = CodeLocalise("OtherCheckBox.Text", "Other");
			HideSalaryCheckbox.Text = CodeLocalise("HideSalaryCheckbox.Text", "Hide salary range from job seekers.");      
			MinimumSalaryMaximumSalaryValueValidator.ErrorMessage = CodeLocalise("MinimumSalaryMaximumSalaryTextBox.Error", "Maximum salary must have a value greater than the minimum salary<br/>");
      MinimumSalaryActualValidator.ErrorMessage = string.Concat(CodeLocalise("MinimumSalaryActual.CustomError", "Use the max salary field to enter an actual salary"), "<br />");
    
			SalaryFrequencyDropDownValidator.ErrorMessage = CodeLocalise("SalaryFrequencyDropDown.Error", "You must select a salary frequency<br/>");
      ExpandCollapseAllButton.Text = HtmlLocalise("ExpandAllButton.Text", "Expand all");

      MeetsMinimumWageRequirementValidator.ErrorMessage = CodeLocalise("MeetsMinimumWageRequirementValidator.ErrorMessage", "Minimum wage confirmation is required");

      WorkNetLinkHyperLink.Text = CodeLocalise("WorkNetLinkHyperLink.Text", "Wage and Career Info");
		}

		#endregion

		/// <summary>
		/// Applies the branding.
		/// </summary>
		private void ApplyBranding()
		{
			SalaryHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			SalaryPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			SalaryPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			HoursHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			HoursPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			HoursPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			BenefitsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			BenefitsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			BenefitsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}

		/// <summary>
		/// Applies the theme.
		/// </summary>
		private void ApplyTheme()
		{
			ExpandCollapseAllRow.Visible = App.Settings.Theme == FocusThemes.Workforce;
		  MeetsMinimumWageRequirementPlaceHolder.Visible = App.Settings.MeetsMinimumWageRequirementCheckBox;

      MinimumHoursPerWeekPanel.Visible =
        MaximumActualLabelPanel.Visible =
        MinimumHoursValidatorsPanel.Visible = App.Settings.HasMinimumHoursPerWeek;

      if (App.Settings.HasMinimumHoursPerWeek)
        HoursPerWeekCompareValidator.Display = ValidatorDisplay.Dynamic;

      WorkNetLinkPlaceHolder.Visible = App.Settings.ShowWorkNetLink && App.Settings.WorkNetLink.IsNotNullOrEmpty();

			HoursPerWeekRequiredValidator.Visible = HoursPerWeekRequiredValidator.Enabled = App.Settings.MandatoryHoursPerWeek;

			if (App.Settings.HasMinimumHoursPerWeek)
				HoursPerWeekCustomValidator.Visible = HoursPerWeekCustomValidator.Enabled = !App.Settings.MandatoryHoursPerWeek;
		}
  }
}