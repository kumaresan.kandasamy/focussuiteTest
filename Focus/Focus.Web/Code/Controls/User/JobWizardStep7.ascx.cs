﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Common.Code;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Web.Controllers.WebTalent;
using Focus.Web.ViewModels;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class JobWizardStep7 : JobWizardControl
	{
		#region Properties

		/// <summary>
		/// Gets the page controller.
		/// </summary>
		/// <value>
		/// The page controller.
		/// </value>
		protected JobWizardStep7Controller PageController { get { return PageControllerBaseProperty as JobWizardStep7Controller; } }

		protected string ExtendVeteranPriorityRequiredErrorMessage = "";
		protected string ExtendUntilDateRequiredErrorMessage = "";
		protected string ExtendUntilDateInvalidErrorMessage = "";
		protected string ExtendUntilDateInPastErrorMessage = "";
		protected string ExtendUntilDateAfterClosingDateErrorMessage = "";
		protected string SelectLifeOfPostingErrorMessage = "";

		#endregion

		/// <summary>
		/// Registers the page controller.
		/// </summary>
		/// <returns></returns>
		public override IPageController RegisterPageController()
		{
			return new JobWizardStep7Controller(App);
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			LocaliseUI();

			if (!IsPostBack)
			{
				ScreeningPreferencesHeaderPanel.Visible = ScreeningPreferencesPanel.Visible = App.Settings.JobSeekerReferralsEnabled;
				InterviewContactPreferencesHeaderPanel.Visible = InterviewContactPreferencesPanel.Visible = !App.Settings.HideInterviewContactPreferences;
			}

			if (!App.Settings.HideInterviewContactPreferences)
			{
				RegisterJavascript();

				EmailAddressRegEx.ValidationExpression = App.Settings.EmailAddressRegExPattern;
				FaxResumeRegEx.ValidationExpression = PhoneForAppointmentRegEx.ValidationExpression = App.Settings.PhoneNumberRegExPattern;
				PhoneForAppointmentMaskedEdit.Mask = FaxResumeMaskedEdit.Mask = App.Settings.PhoneNumberMaskPattern;

				FaxResumeRegEx.Enabled = FaxResumeCheckBox.Checked;
				PhoneForAppointmentRegEx.Enabled = PhoneForAppointmentCheckBox.Checked;
			}
			
			ApplyBranding();
			ApplyTheme();

      var clientIds = new NameValueCollection
		  {
		    {"ClosingDateIsValidCompareValidator", ClosingDateIsValidCompareValidator.ClientID},
		    {"ClosingDateCompareValidator", ClosingDateCompareValidator.ClientID},
		    {"ClosingDateGreaterThanTodayCompareValidator", ClosingDateGreaterThanTodayCompareValidator.ClientID},
				{"ExtendPriorityOfServiceValidator", ExtendPriorityOfServiceValidator.ClientID},
				{"ExtendVeteranDateValidator", ExtendVeteranDateValidator.ClientID}
		  };
      RegisterCodeValuesJson("JobWizardStep7_ClientIds", "JobWizardStep7_ClientIds", clientIds);
		}

		#region Bind Methods

		/// <summary>
		/// Binds the controls for the step.
		/// </summary>
		/// <param name="model">The model.</param>
		internal void BindStep(JobWizardViewModel model)
		{
			BindScreeningPreferencesMinimumStarsDropDown(ScreeningPreferencesMinimumStarsDropDown);
      BindScreeningPreferencesMinimumStarsToApplyDropDown(ScreeningPreferencesMinimumStarsToApplyDropDown);

			// If user has single signed on to the application
			if (App.User.IsShadowingUser || App.Settings.Module == FocusModules.Assist)
			{
				if (model.JobContact.IsNotNull())
				{
					var firstname = (model.JobContact.FirstName.IsNotNullOrEmpty()) ? model.JobContact.FirstName : string.Empty;
					var lastname = (model.JobContact.LastName.IsNotNullOrEmpty()) ? model.JobContact.LastName : string.Empty;
					JobContactNameLiteral.Text = string.Format("{0} {1}", firstname, lastname);
					JobContactEmailAddressLiteral.Text = (model.JobContact.EmailAddress.IsNotNullOrEmpty())
						? model.JobContact.EmailAddress
						: string.Empty;
				}

				if (model.JobContactAddress.IsNotNull())
				{
					if (model.JobContactAddress.Line1.IsNotNullOrEmpty())
						JobContactAddressLine1Literal.Text = model.JobContactAddress.Line1;
					if (model.JobContactAddress.Line2.IsNotNullOrEmpty())
						JobContactAddressLine2Literal.Text = model.JobContactAddress.Line2;
					if (model.JobContactAddress.TownCity.IsNotNullOrEmpty())
						JobContactCityLiteral.Text = model.JobContactAddress.TownCity;
					if (model.JobContactAddress.StateId.IsNotNull())
						JobContactStateLiteral.Text =
							ServiceClientLocator.CoreClient(App)
								.GetLookup(LookupTypes.States)
								.Where(x => x.Id == model.JobContactAddress.StateId)
								.Select(x => x.Text)
								.SingleOrDefault();
					if (model.JobContactAddress.PostcodeZip.IsNotNullOrEmpty())
						JobContactZipLiteral.Text = model.JobContactAddress.PostcodeZip;
				}

				if (model.JobContactPrimaryContactDetails.IsNotNull())
				{
					if (model.JobContactPrimaryContactDetails.Number.IsNotNullOrEmpty())
						JobContactTelephoneLiteral.Text = Regex.Replace(model.JobContactPrimaryContactDetails.Number,
							App.Settings.PhoneNumberStrictRegExPattern,
							App.Settings.PhoneNumberFormat);
				}
			}
			else
			{
				JobContactHeaderPanel.Visible = JobContactPanel.Visible = JobContactPanelWrapper.Visible = false;
			}

			ClosingDateTextBox.Text = model.JobClosingOn;
			if (model.Job.NumberOfOpenings.IsNotNull())
				NumberOfOpeningsTextBox.Text = model.Job.NumberOfOpenings.GetValueOrDefault().ToString();

			if (!App.Settings.HideInterviewContactPreferences)
			{
				FocusTalentAccountCheckbox.Checked = (model.Job.InterviewContactPreferences.IsNull() ||((model.Job.InterviewContactPreferences & ContactMethods.FocusTalent) == ContactMethods.FocusTalent));

				EmailResumeCheckBox.Checked = ((model.Job.InterviewContactPreferences & ContactMethods.Email) ==
																			 ContactMethods.Email);
				ApplyOnlineCheckBox.Checked = ((model.Job.InterviewContactPreferences & ContactMethods.Online) ==
																			 ContactMethods.Online);
				MailResumeCheckBox.Checked = ((model.Job.InterviewContactPreferences & ContactMethods.Mail) == ContactMethods.Mail);
				FaxResumeCheckBox.Checked = ((model.Job.InterviewContactPreferences & ContactMethods.Fax) == ContactMethods.Fax);
				InPersonCheckBox.Checked = ((model.Job.InterviewContactPreferences & ContactMethods.InPerson) ==
																		ContactMethods.InPerson);
				PhoneForAppointmentCheckBox.Checked = ((model.Job.InterviewContactPreferences & ContactMethods.Telephone) ==
																							 ContactMethods.Telephone);

				if (model.Job.InterviewEmailAddress.IsNotNullOrEmpty()) EmailAddressTextBox.Text = model.Job.InterviewEmailAddress;
				if (model.Job.InterviewApplicationUrl.IsNotNullOrEmpty())
					ApplyOnlineTextBox.Text = model.Job.InterviewApplicationUrl;
				if (model.Job.InterviewMailAddress.IsNotNullOrEmpty()) MailResumeTextBox.Text = model.Job.InterviewMailAddress;
				if (model.Job.InterviewFaxNumber.IsNotNullOrEmpty())
					FaxResumeTextBox.Text = Regex.Replace(model.Job.InterviewFaxNumber, App.Settings.PhoneNumberStrictRegExPattern,
						App.Settings.PhoneNumberFormat);
				if (model.Job.InterviewPhoneNumber.IsNotNullOrEmpty())
					PhoneForAppointmentTextBox.Text = Regex.Replace(model.Job.InterviewPhoneNumber,
						App.Settings.PhoneNumberStrictRegExPattern,
						App.Settings.PhoneNumberFormat);
				if (model.Job.InterviewDirectApplicationDetails.IsNotNullOrEmpty())
					InPersonTextBox.Text = model.Job.InterviewDirectApplicationDetails;
				if (model.Job.InterviewOtherInstructions.IsNotNullOrEmpty())
					OtherInstructionsTextBox.Text = model.Job.InterviewOtherInstructions;
			}
			else
			{
				model.Job.InterviewContactPreferences = ContactMethods.None;
			}

			model.ShowScreeningPreferencesPanel = App.Settings.HideScreeningPreferences;

			if (model.ShowScreeningPreferencesPanel == false)
			{
				BindStepScreeningPreferences(model,
																		AllowUnqualifiedApplicationsRadioButton,
																		JobSeekersMustBeScreenedRadioButton,
																		JobSeekersMustHaveMinimumStarMatchRadioButton,
																		ScreeningPreferencesMinimumStarsDropDown,
																		JobSeekersMustHaveMinimumStarMatchRow,
                                    JobSeekersMinimumStarsToApplyRadioButton,
                                    ScreeningPreferencesMinimumStarsToApplyDropDown,
                                    JobSeekersMinimumStarsToApplyRow,
																		AllowUnqualifiedApplicationsRow);
			}
			else
			{
				ScreeningPreferencesHeaderPanel.Visible = false;
				ScreeningPreferencesPanel.Visible = false;
			}

		  var expiryDate = GetJobExpiryDate(App, model.Job.PostedOn, model.Job.FederalContractor.GetValueOrDefault(false),
		    model.Job.ForeignLabourCertificationH2A.GetValueOrDefault(false),
		    model.Job.ForeignLabourCertificationH2B.GetValueOrDefault(false),
		    model.Job.ForeignLabourCertificationOther.GetValueOrDefault(false));

      ClosingDateCompareValidator.ValueToCompare = expiryDate.ToShortDateString();
      ClosingDateCompareValidator.ErrorMessage = CodeLocalise("ClosingDateCompareValidator.Lifetime.ErrorMessage", "Closing date cannot exceed {0}", expiryDate.ToShortDateString());

			var minClosingDate = App.Settings.DaysForMinimumJobClosingDate == 0
				                     ? DateTime.Today
				                     : model.Job.CreatedOn.Date.AddDays(App.Settings.DaysForMinimumJobClosingDate - 1);

			ClosingDateGreaterThanTodayCompareValidator.ErrorMessage = App.Settings.DaysForMinimumJobClosingDate == 0 || minClosingDate < DateTime.Today
				? CodeLocalise("ClosingDateGreaterThanTodayCompareValidatior.ErrorMessage", "Closing date must be in the future")
				: CodeLocalise("ClosingDateGreaterThanTodayCompareValidatiorMore.ErrorMessage", "Closing date must be at least {0} days in the future", App.Settings.DaysForMinimumJobClosingDate);

			if (minClosingDate < DateTime.Today)
				minClosingDate = DateTime.Today;

      ClosingDateGreaterThanTodayCompareValidator.ValueToCompare = minClosingDate.ToShortDateString();

			// Turn off validator if the expiry date is DateTime.MaxValue
      ClosingDateCompareValidator.Visible = ClosingDateCompareValidator.Enabled = (expiryDate != DateTime.MaxValue);
      
      PreScreeningServiceRequestRadioButtons.Items.Clear();
      PreScreeningServiceRequestRadioButtons.Items.Add(new ListItem(CodeLocalise("Global.Yes", "Yes"), "1"));
      PreScreeningServiceRequestRadioButtons.Items.Add(new ListItem(CodeLocalise("Global.Yes", "No"), "0"));
			AdvertiseFor30DaysPlaceHolder.Visible = model.Job.ForeignLabourCertificationOther.GetValueOrDefault();
			PreScreeningServiceRequestRadioButtons.SelectedIndex = model.Job.PreScreeningServiceRequest.GetValueOrDefault(false) ? 0 : 1;

			if (App.Settings.VeteranPriorityServiceEnabled && App.Settings.ExtendedVeteranPriorityofService)
			{
				if (model.Job.ExtendVeteranPriority.IsNotNull() && model.Job.ExtendVeteranPriority != ExtendVeteranPriorityOfServiceTypes.None)
				{
					ExtendPriorityOfServiceCheckBox.Checked = true;

					if (model.Job.ExtendVeteranPriority == ExtendVeteranPriorityOfServiceTypes.ExtendUntil)
					{
						ExtendPriorityOfServiceUntilRadioButton.Checked = true;

						if (model.Job.VeteranPriorityEndDate.IsNotNull())
						{
							var dateTimeFormat = CultureInfo.CurrentUICulture.DateTimeFormat;
							VeteranPriorityExtendUntilDateTextBox.Text = model.Job.VeteranPriorityEndDate.Value.Date.ToString(dateTimeFormat.ShortDatePattern);
						}
					}
					else
					{
						ExtendPriorityOfServiceForLifeOfPostingRadioButton.Checked = true;
					}
				}
			}
		}

		/// <summary>
		/// Unbinds the step.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
		internal JobWizardViewModel UnbindStep(JobWizardViewModel model)
		{
			if (model.JobClosingOn.IsNotNullOrEmpty() && model.JobClosingOn != ClosingDateTextBox.TextTrimmed())
				model.Job.ClosingOnUpdated = true;

			model.JobClosingOn = ClosingDateTextBox.TextTrimmed();
			model.Job.NumberOfOpenings = int.Parse(NumberOfOpeningsTextBox.TextTrimmed());

			if (!App.Settings.HideInterviewContactPreferences)
			{
				var interviewContactPreferences = ContactMethods.None;
				if (FocusTalentAccountCheckbox.Checked) interviewContactPreferences = interviewContactPreferences | ContactMethods.FocusTalent;
				if (EmailResumeCheckBox.Checked) interviewContactPreferences = interviewContactPreferences | ContactMethods.Email;
				if (ApplyOnlineCheckBox.Checked) interviewContactPreferences = interviewContactPreferences | ContactMethods.Online;
				if (MailResumeCheckBox.Checked) interviewContactPreferences = interviewContactPreferences | ContactMethods.Mail;
				if (FaxResumeCheckBox.Checked) interviewContactPreferences = interviewContactPreferences | ContactMethods.Fax;
				if (InPersonCheckBox.Checked) interviewContactPreferences = interviewContactPreferences | ContactMethods.InPerson;
				if (PhoneForAppointmentCheckBox.Checked)
					interviewContactPreferences = interviewContactPreferences | ContactMethods.Telephone;
				model.Job.InterviewContactPreferences = interviewContactPreferences;

				model.Job.InterviewEmailAddress = EmailResumeCheckBox.Checked ? EmailAddressTextBox.TextTrimmed() : string.Empty;
				model.Job.InterviewApplicationUrl = ApplyOnlineCheckBox.Checked ? ApplyOnlineTextBox.TextTrimmed() : string.Empty;
				model.Job.InterviewMailAddress = MailResumeCheckBox.Checked ? MailResumeTextBox.TextTrimmed() : string.Empty;
				model.Job.InterviewFaxNumber = FaxResumeCheckBox.Checked ? FaxResumeTextBox.TextTrimmed() : string.Empty;
				model.Job.InterviewPhoneNumber = PhoneForAppointmentCheckBox.Checked
					? PhoneForAppointmentTextBox.TextTrimmed()
					: string.Empty;
				model.Job.InterviewDirectApplicationDetails = InPersonCheckBox.Checked
					? InPersonTextBox.TextTrimmed()
					: string.Empty;
				model.Job.InterviewOtherInstructions = OtherInstructionsTextBox.TextTrimmed();
			}
			else
			{
				model.Job.InterviewContactPreferences = ContactMethods.None;
			}

			if (!App.Settings.JobSeekerReferralsEnabled || App.Settings.HideScreeningPreferences) return model;

			if (JobSeekersMustBeScreenedRadioButton.Checked)
				model.Job.ScreeningPreferences = ScreeningPreferences.JobSeekersMustBeScreened;

			else if (JobSeekersMustHaveMinimumStarMatchRadioButton.Checked)
				model.Job.ScreeningPreferences =
					(ScreeningPreferences)
						Enum.Parse(typeof(ScreeningPreferences), ScreeningPreferencesMinimumStarsDropDown.SelectedValue);

      else if (JobSeekersMinimumStarsToApplyRadioButton.Checked)
        model.Job.ScreeningPreferences =
          (ScreeningPreferences)
            Enum.Parse(typeof(ScreeningPreferences), ScreeningPreferencesMinimumStarsToApplyDropDown.SelectedValue);

			else
				model.Job.ScreeningPreferences = ScreeningPreferences.AllowUnqualifiedApplications;

		  if (PreScreeningServiceRequestRadioButtons.SelectedIndex >= 0)
		    model.Job.PreScreeningServiceRequest = (PreScreeningServiceRequestRadioButtons.SelectedValue == "1");
		  else
		    model.Job.PreScreeningServiceRequest = null;

			if (ExtendPriorityOfServiceCheckBox.Checked)
			{
				if (ExtendPriorityOfServiceUntilRadioButton.Checked)
				{
					var veteranPriorityEndDate = VeteranPriorityExtendUntilDateTextBox.TextTrimmed().ToDate();

					// Extend priority to midnight of the day selected
					if (veteranPriorityEndDate.HasValue)
						veteranPriorityEndDate = veteranPriorityEndDate.Value.AddDays(1).AddSeconds(-1);

					model.Job.VeteranPriorityEndDate = veteranPriorityEndDate;
					model.Job.ExtendVeteranPriority = ExtendVeteranPriorityOfServiceTypes.ExtendUntil;
				}
				else
				{
					var veteranPriorityEndDate = Convert.ToDateTime(ClosingDateTextBox.TextTrimmed().ToDate());
					var ts = new TimeSpan(23, 59, 59);
					veteranPriorityEndDate = veteranPriorityEndDate.Date + ts;

					model.Job.VeteranPriorityEndDate = veteranPriorityEndDate;

					model.Job.ExtendVeteranPriority = ExtendVeteranPriorityOfServiceTypes.ExtendForLifeOfPosting;
				}
			}
			else
			{
				// Only reset date if swtiching off field manually
				if (model.Job.ExtendVeteranPriority.GetValueOrDefault(ExtendVeteranPriorityOfServiceTypes.None) != ExtendVeteranPriorityOfServiceTypes.None)
				{
					model.Job.VeteranPriorityEndDate = new DateTime(1900, 01, 01);	
				}
				model.Job.ExtendVeteranPriority = ExtendVeteranPriorityOfServiceTypes.None;
				
			}

			return model;
		}

		#endregion

		#region Localise the UI

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			ClosingDateTextBox.ToolTip = CodeLocalise("ClosingDateTextBox.ToolTip",
																								"Please enter the closing date as mm/dd/yyyy");
			ClosingDateValidator.ErrorMessage = CodeLocalise("ClosingDateRequired.ErrorMessage", "Closing date required");
			ClosingDateIsValidCompareValidator.ErrorMessage = CodeLocalise("ClosingDateIsValidCompareValidator.ErrorMessage",
																																			"Valid Date is required");

			NumberOfOpeningsRequired.ErrorMessage = CodeLocalise("NumberOfOpeningsRequired.ErrorMessage",
																														"Number of openings required");
			MinimumNumberOfOpeningsValidator.ErrorMessage = CodeLocalise("MinimumNumberOfOpeningsValidator.ErrorMessage",
																																		"Number of openings must be a number greater than 0");
		  MaximumNumberOfOpeningsValidator.ValueToCompare = App.Settings.MaximumNumberJobOpenings.ToString();
			MaximumNumberOfOpeningsValidator.ErrorMessage = CodeLocalise("MaximumNumberOfOpeningsValidator.ErrorMessage",
                                                                    "Number of openings must be a number less than or equal to {0}", App.Settings.MaximumNumberJobOpenings);
       
			FocusTalentAccountCheckbox.Text = CodeLocalise("FocusTalentAccountCheckbox.Text", "My Focus/Talent account");
			EmailResumeCheckBox.Text = CodeLocalise("EmailResumeCheckBox.Text", "Email resume to");
			ApplyOnlineCheckBox.Text = CodeLocalise("ApplyOnlineCheckBox.Text", "Apply online to job URL");
			MailResumeCheckBox.Text = CodeLocalise("MailResumeCheckBox.Text", "Mail resume to");
			FaxResumeCheckBox.Text = CodeLocalise("FaxResumeCheckBox.Text", "Fax resume to");
			PhoneForAppointmentCheckBox.Text = CodeLocalise("PhoneForAppointmentCheckBox.Text", "Call for an appointment");
			InPersonCheckBox.Text = CodeLocalise("InPersonCheckBox.Text", "Send seeker(s) for in-person interview");
			ExtendPriorityOfServiceCheckBox.Text = CodeLocalise("ExtendPriorityOfServiceCheckBox.Text", "I wish to extend the veteran priority of service for this posting");

			InterviewContactPreferencesValidator.ErrorMessage = CodeLocalise(
				"InterviewContactPreferencesValidator.ErrorMessage", "One completed contact method must be selected");
			EmailAddressValidator.ErrorMessage = CodeLocalise("EmailAddressValidator.ErrorMessage", "Email address is required");
			MailResumeValidator.ErrorMessage = CodeLocalise("MailResumeValidator.ErrorMessage", "Mail to address is required");
			InPersonValidator.ErrorMessage = CodeLocalise("InPersonValidator.ErrorMessage", "Contact details are required");
			InPersonLengthValidator.ErrorMessage = CodeLocalise("InPersonLengthValidator.ErrorMessage", "Contact details cannot exceed 2000 characters. ");
			EmailAddressRegEx.ErrorMessage = CodeLocalise("EmailAddress.RegExErrorMessage", "Email address format is invalid");
			FaxResumeRegEx.ErrorMessage = CodeLocalise("FaxResume.RegExErrorMessage", "A valid fax number is required");
			PhoneForAppointmentRegEx.ErrorMessage = CodeLocalise("PhoneForAppointment.RegExErrorMessage",
																														"A valid phone number is required");

      ExpandCollapseAllButton.Text = HtmlLocalise("ExpandAllButton.Text", "Expand all");

      PreScreeningServiceRequestHeader.Text = CodeLocalise("PreScreeningServiceRequestHeader.Text", "Would you be interested in hearing about your job center's services?");
		  PreScreeningServiceRequestValidator.ErrorMessage = CodeLocalise("PreScreeningServiceRequestValidator.ErrorMessage", "Please select either Yes or No");

			ExtendVeteranPriorityRequiredErrorMessage = CodeLocalise("ExtendVeteranPriority.RequiredErrorMessage", "Extension period is required");
			ExtendUntilDateRequiredErrorMessage = CodeLocalise("ExtendUntilDate.RequiredErrorMessage", "Extension date is required");
			ExtendUntilDateInvalidErrorMessage = CodeLocalise("ExtendUntilDate.ErrorMessage", "Extension date is not a valid date");
			ExtendUntilDateInPastErrorMessage = CodeLocalise("ExtendUntilDateInPast.ErrorMessage", "Extension date must be in the future");
			ExtendUntilDateAfterClosingDateErrorMessage = CodeLocalise("ExtendUntilDateInPast.ErrorMessage", "Extension date must be prior to job closing date");
			SelectLifeOfPostingErrorMessage = CodeLocalise("SelectLifeOfPosting.ErrorMessage", "Extension date is equal to the job\'s closing date. Select Extend for life of the posting to continue");

			AdvertiseFor30DaysText.Text = HtmlLocalise( "AdvertiseFor30DaysText.Text", "To comply with Federal Law, this job posting must be advertised for a <b>full 30 days</b>" );
		}

		#endregion

		/// <summary>
		/// Applies the branding.
		/// </summary>
		private void ApplyBranding()
		{
			JobContactHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			JobContactPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			JobContactPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			ClosingDateHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ClosingDatePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ClosingDatePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			NumberOfOpeningsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			NumberOfOpeningsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			NumberOfOpeningsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			InterviewContactPreferencesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			InterviewContactPreferencesPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			InterviewContactPreferencesPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			ScreeningPreferencesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ScreeningPreferencesPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ScreeningPreferencesPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			VeteranPriorityOfServiceHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			VeteranPriorityOfServicePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			VeteranPriorityOfServicePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}

		/// <summary>
		/// Applies the theme.
		/// </summary>
		private void ApplyTheme()
		{
			ExpandCollapseAllRow.Visible = App.Settings.Theme == FocusThemes.Workforce;

		  PreScreeningServiceRequestPanel.Visible = (App.Settings.PreScreeningServiceRequest && App.Settings.Theme == FocusThemes.Workforce);

			VeteranPriorityPlaceHolder.Visible = App.Settings.ExtendedVeteranPriorityofService && App.Settings.VeteranPriorityServiceEnabled && App.Settings.Theme == FocusThemes.Workforce;
		}

		/// <summary>
		/// Registers the javascript.
		/// </summary>
		private void RegisterJavascript()
		{
			// Build the JS
			var js = @"function validateInterviewContactPreferences(oSrc, args)
{
	args.IsValid = ($(""#" + FocusTalentAccountCheckbox.ClientID + @""").is("":checked"") ||
									$(""#" + EmailResumeCheckBox.ClientID + @""").is("":checked"") ||
									$(""#" + ApplyOnlineCheckBox.ClientID + @""").is("":checked"") ||
									$(""#" + MailResumeCheckBox.ClientID + @""").is("":checked"") ||
									$(""#" + FaxResumeCheckBox.ClientID + @""").is("":checked"") ||
									$(""#" + PhoneForAppointmentCheckBox.ClientID + @""").is("":checked"") ||
									$(""#" + InPersonCheckBox.ClientID + @""").is("":checked""));
}

function validateEmailAddress(oSrc, args)
{
	if($(""#" + EmailResumeCheckBox.ClientID + @""").is("":checked""))
	{
		args.IsValid = ($(""#" + EmailAddressTextBox.ClientID + @""").val() != '');
	}
	else
	{
		args.IsValid = true;
	}
}

function validateApplyOnline(oSrc, args)
{
	if($(""#" + ApplyOnlineCheckBox.ClientID + @""").is("":checked""))
	{
		var val = $(""#" + ApplyOnlineTextBox.ClientID + @""").val();
		if (val != '')
		{
			var re = new RegExp(/" + App.Settings.UrlRegExPattern.Replace("/",@"\/") + @"/);
			if (val.match(re)) 
			{
				args.IsValid = true;
			}
			else
			{
				args.IsValid = false;
				oSrc.textContent = """ +
								CodeLocalise("ApplyOnlineValidator.InvalidMessage", "Invalid url. Please ensure http:// or https:// is present, as appropriate to your url") + @""";
			}
		}
		else
		{
			args.IsValid = false;
			oSrc.textContent = """ + CodeLocalise("ApplyOnlineValidator.RequiredMessage", "URL is required") + @""";
		}
	}
	else
	{
		args.IsValid = true;
	}
}

function validateMailResume(oSrc, args)
{
	if($(""#" + MailResumeCheckBox.ClientID + @""").is("":checked""))
	{
		args.IsValid = ($(""#" + MailResumeTextBox.ClientID + @""").val() != '');
	}
	else
	{
		args.IsValid = true;
	}
}

function validateFaxResume(oSrc, args)
{
	if($(""#" + FaxResumeCheckBox.ClientID + @""").is("":checked""))
	{
		var number = $(""#" + FaxResumeTextBox.ClientID + @""").val();
		if (number.length == 0)
		{
			args.IsValid = false;  
			oSrc.textContent = """ + CodeLocalise("FaxResumeValidator.ErrorMessage", "Fax number is required") + @""";
		} 
		else if (/" + App.Settings.PhoneNumberRegExPattern +
								@"/.test(number) && number.replace(/[0-9]/gi, '').length != number.length - 10)
		{
			args.IsValid = false;  
			oSrc.textContent = """ + CodeLocalise("FaxResumeValidator.ErrorMessage2", "Fax number must be 10 digits") + @""";
		}
		else
		{
			args.IsValid = true;
		}
	}
	else
	{
		args.IsValid = true;
	}
}

function validateInPerson(oSrc, args)
{
	if($(""#" + InPersonCheckBox.ClientID + @""").is("":checked""))
	{
		args.IsValid = ($(""#" + InPersonTextBox.ClientID + @""").val() != '');
	}
	else
	{
		args.IsValid = true;
	}
}
";

			ScriptManager.RegisterClientScriptBlock(this, typeof(JobWizardStep7), "JobWizardStep7JavaScript", js, true);
		}
	}

}