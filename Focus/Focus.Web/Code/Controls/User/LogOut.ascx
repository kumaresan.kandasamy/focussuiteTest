﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LogOut.ascx.cs" Inherits="Focus.Web.Code.Controls.User.LogOut" %>

<asp:HiddenField ID="LogOutModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="LogOutModal" runat="server" BehaviorID="LogOutModal"
												TargetControlID="LogOutModalDummyTarget"
												PopupControlID="LogOutPanel"
												PopupDragHandleControlID="LogOutPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground"
												CancelControlID="ReturnHomeButton" />

<asp:Panel ID="LogOutPanel" runat="server" CssClass="modal" Width="325px" ClientIDMode="Static" Style="display:none;">
	<div style="float:left;">
		<table role="presentation">
			<tr>
				<td style="width:325px;"><%= HtmlLocalise("Header.Text", "Sign out")%></td>
			</tr>
			<tr>
				<td style="vertical-align:top">
					<p><%= HtmlLocalise("Notification.Text", "Are you sure you want to sign out?")%></p>
					<br />
					<br />
				</td>
			</tr>
			<tr>
				<td>
					<div class="modalButtonBorder">
						<asp:Button ID="CompleteLogOutButton" runat="server" SkinID="Button1" OnClick="CompleteLogOutButton_Click" CausesValidation="false" Text="Log Out"/>
						&nbsp;&nbsp;&nbsp;
						<asp:Button ID="ReturnHomeButton" runat="server" SkinID="Button1" OnClientClick="return false;" UseSubmitBehavior="False" ClientIDMode="Static" Text="Home"/>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<div class="closeIcon"><asp:Literal runat="server" ID="LocaliseClose"></asp:Literal><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text.NoEdit", "Close") %>" onclick="$find('LogOutModal').hide(); return false;" /></div>

</asp:Panel>