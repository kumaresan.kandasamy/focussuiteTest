﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class UpdateableList : UserControlBase
	{
		public string ItemCountClientId
		{
			get { return ItemCount.ClientID; }
		}

		public string Width { get; set; }

		/// <summary>
		/// Gets or sets the items.
		/// </summary>
		/// <value>The items.</value>
		public List<string> Items
		{
			get { return GetViewStateValue<List<string>>("UpdateableList:" + ID + ":Items"); }
			set
			{
				SetViewStateValue("UpdateableList:" + ID + ":Items", value);
				Bind();
			}
		}
    /// <summary>
    /// Gets or sets a value indicating whether to restrict duplicates in the collection.
    /// </summary>
    /// <value><c>true</c> if [restrict duplicates]; otherwise, <c>false</c>.</value>
    public bool RestrictDuplicates { get; set; }

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				ItemsListPanel.Width = Width == null ? 254 : Unit.Parse(Width);

				Bind();
			}
		}

    /// <summary>
    /// Adds the item.
    /// </summary>
    /// <param name="item">The item.</param>
    /// <returns>Duplicate validation check</returns>
		public bool AddItem(string item)
		{
			// Would be nice to be able to save an id also
			if (Items == null)
				Items = new List<string>();
      if (RestrictDuplicates && Items.Contains(item, StringComparer.OrdinalIgnoreCase))
        return false;

			Items.Add(item);

			Bind();
		  return true;
		}

		/// <summary>
		/// Handles the Command event of the ItemsListRemoveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ItemsListRemoveButton_Command(object sender, CommandEventArgs e)
		{
			var rowNo = Convert.ToInt32(e.CommandArgument);
			Items.RemoveAt(rowNo);
			Bind();
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			ItemsList.DataSource = Items;
			ItemsList.DataBind();

			ItemCount.Value = Items.IsNull() ? "0" : Items.Count.ToString(CultureInfo.InvariantCulture);
		}
	}
}