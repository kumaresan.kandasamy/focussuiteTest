﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.ViewModels;

using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class JobWizardStep4 : UserControlBase
	{
		private JobTypes JobType
		{
			get { return GetViewStateValue<JobTypes>("JobWizardKeywordsStatements:JobType"); }
			set { SetViewStateValue("JobWizardKeywordsStatements:JobType", value); }
		}

		private List<JobProgramOfStudyDto> _jobTier2ProgramsOfStudy
		{
			get { return GetViewStateValue<List<JobProgramOfStudyDto>>("JobWizardKeywordsStatements:JobTier2ProgramsOfStudy"); }
			set { SetViewStateValue("JobWizardKeywordsStatements:JobTier2ProgramsOfStudy", value); }
		}

		protected string AddRequirementLabel
		{
			get
			{
				return JobType.IsInternship()
					? HtmlLabel("AddRequirement.Label", "Add this requirement to my internship description")
					: HtmlLabel("AddRequirement.Label", "Add this requirement to my job description");
			}
		}

		protected string Theme
		{
			get { return App.Settings.Theme.ToString(); }
		}

		protected string LicenceEndorsementsJavascriptArray;

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			LocaliseUI();

			InitialiseBackgroundCheck();
			ApplyBranding();
			RegisterJavascript();
			ApplyTheme();

			LicenceEndorsementsJavascriptArray = GetValidLicenceEndorsementsJavascriptArray();

		}

		/// <summary>
		/// Handles the Click event of the AddLicenceButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddLicenceButton_Click(object sender, EventArgs e)
		{
			if (LicenceTypeTextBox.TextTrimmed().IsNotNullOrEmpty())
			{
				LicenceTypeValidator.IsValid = LicencesUpdateableList.AddItem(LicenceTypeTextBox.TextTrimmed());
				LicenceTypeTextBox.Text = "";
			}
		}

		/// <summary>
		/// Handles the Click event of the AddProgramOfStudyButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddProgramOfStudyButton_Click(object sender, EventArgs e)
		{
			if (LicenceTypeTextBox.TextTrimmed().IsNotNullOrEmpty())
			{
				if (LicencesUpdateableList.Items.Count > 5)
				{
					LicenceTypeValidator.IsValid = false;
					LicenceTypeValidator.Text = HtmlLocalise("LicenceType.MoreThanSix", "more than 6");
					return;
				}

				if (LicencesUpdateableList.Items.Contains(LicenceTypeTextBox.TextTrimmed()))
				{
					LicenceTypeValidator.IsValid = false;
					LicenceTypeValidator.Text = HtmlLocalise("LicsenceType.Exists", "already exists");
					return;
				}

				LicenceTypeValidator.IsValid = LicencesUpdateableList.AddItem(LicenceTypeTextBox.TextTrimmed());
				LicenceTypeTextBox.Text = "";
			}
		}

		/// <summary>
		/// Handles the Click event of the AddCertiﬁcationButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddCertiﬁcationButton_Click(object sender, EventArgs e)
		{
			if (CertiﬁcationTypeTextBox.TextTrimmed().IsNotNullOrEmpty())
			{
				CertiﬁcationTypeTextBoxValidator.IsValid = CertiﬁcationsUpdateableList.AddItem(CertiﬁcationTypeTextBox.TextTrimmed());
				CertiﬁcationTypeTextBox.Text = "";
			}
		}

		/// <summary>
		/// Handles the Click event of the AddLanguageButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddLanguageButton_Click(object sender, EventArgs e)
		{
			// Check for Language
			if (LanguageTextBox.TextTrimmed().IsNotNullOrEmpty())
			{
				// Check for proficiency
				if (LanguageProficiencyDropDown.Visible && LanguageProficiencyDropDown.SelectedIndex == 0)
				{
					LanguageProficiencyRequired.IsValid = false;
				}
				else // Add the proficiency id and language and to the Updateable list control or show a duplicate error
				{

					string selectedValue;
					var selectedText = string.Empty;

					if (LanguageProficiencyDropDown.SelectedIndex == 0)
					{
						selectedValue = "0";
					}
					else
					{
						selectedValue = LanguageProficiencyDropDown.SelectedValue;
						selectedText = " (" + LanguageProficiencyDropDown.SelectedItem.Text + ")";
					}

					LanguageTextBoxValidator.IsValid =
							LanguagesUpdateableList.AddItem(new KeyValuePair<string, string[]>(
									selectedValue,
											new string[2]
					      {
					        LanguageTextBox.TextTrimmed(),
					        LanguageTextBox.TextTrimmed() + selectedText
					      }
											));

					if (LanguageTextBoxValidator.IsValid)
					{
						LanguageTextBox.Text = "";
						if (LanguageProficiencyDropDown.Visible)
							LanguageProficiencyDropDown.SelectedIndex = 0;
					}
				}
			}
		}

		/// <summary>
		/// Handles the Click event of the AddAnotherRequirementButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddAnotherRequirementButton_Click(object sender, EventArgs e)
		{
			BindSpecialRequirementsRepeater(UnbindSpecialRequirementsRepeater());
		}

		#region Bind Methods

		/// <summary>
		/// Binds the controls for the step.
		/// </summary>
		/// <param name="model">The model.</param>
		internal void BindStep(JobWizardViewModel model)
		{
			JobType = model.Job.JobType;
			MinimumAgeReasonValidator.Enabled = false;

			BindControls(model);

			if (model.Job.HideMinimumAgeOnPosting.IsNotNull()) HideMinimumAgeCheckBox.Checked = !model.Job.HideMinimumAgeOnPosting;
			if (model.Job.HideCertificationsOnPosting.IsNotNull()) HideCertificationsCheckBox.Checked = !model.Job.HideCertificationsOnPosting;
			if (model.Job.HideDriversLicenceOnPosting.IsNotNull()) HideDriversLicenceCheckBox.Checked = !model.Job.HideDriversLicenceOnPosting;
			if (model.Job.HideEducationOnPosting.IsNotNull()) HideEducationCheckBox.Checked = !model.Job.HideEducationOnPosting;
			if (model.Job.HideExperienceOnPosting.IsNotNull()) HideExperienceCheckBox.Checked = !model.Job.HideExperienceOnPosting;
			if (model.Job.HideLanguagesOnPosting.IsNotNull()) HideLanguagesCheckBox.Checked = !model.Job.HideLanguagesOnPosting;
			if (model.Job.HideLicencesOnPosting.IsNotNull()) HideLicencesCheckBox.Checked = !model.Job.HideLicencesOnPosting;
			if (model.Job.HideSpecialRequirementsOnPosting.IsNotNull()) HideSpecialRequirementsCheckBox.Checked = !model.Job.HideSpecialRequirementsOnPosting;
			if (model.Job.HideWorkWeekOnPosting.IsNotNull()) HideWorkWeekCheckBox.Checked = !model.Job.HideWorkWeekOnPosting;
			if (model.Job.HideCareerReadinessOnPosting.IsNotNull()) HideCareerReadinessCheckBox.Checked = !model.Job.HideCareerReadinessOnPosting;


			if (model.Job.StudentEnrolled.IsNotNull())
				StudentEnrolledRadioButtonList.SelectValue(model.Job.StudentEnrolled.ToString());
			if (model.Job.MinimumCollegeYears.IsNotNull() && MinimumCollegeYearsDropDown.Items.FindByValue(model.Job.MinimumCollegeYears.ToString()).IsNotNull())
				MinimumCollegeYearsDropDown.SelectedValue = model.Job.MinimumCollegeYears.ToString();
			MinimumEducationDropDownList.SelectValue(model.Job.MinimumEducationLevel.ToString());
			if (model.Job.MinimumEducationLevelRequired.IsNotNull()) EducationCriteriaRadioButtonList.SelectValue(model.Job.MinimumEducationLevelRequired.ToString());
			if (model.Job.MinimumExperience.IsNotNull()) MinimumExperienceTextBox.Text = model.Job.MinimumExperience.GetValueOrDefault().ToString();
			if (model.Job.MinimumExperienceRequired.IsNotNull()) MinimumExperienceRadioButtonList.SelectValue(model.Job.MinimumExperienceRequired.ToString());
			if (model.Job.MinimumExperienceMonths.IsNotNull() && MinimumExperienceMonthsDropDown.Items.FindByValue(model.Job.MinimumExperienceMonths.ToString()).IsNotNull())
				MinimumExperienceMonthsDropDown.SelectedValue = model.Job.MinimumExperienceMonths.ToString();

			if (model.Job.DrivingLicenceClassId.IsNotNull()) DrivingLicenceClassDropDown.SelectValue(model.Job.DrivingLicenceClassId.Value.ToString());

			if (model.JobDrivingLicenceEndorsements.IsNotNullOrEmpty())
			{
				foreach (ListItem checkbox in DrivingLicenceEndorsementsCheckBoxList.Items)
				{
					var checkboxValue = checkbox.ValueToLong();

					if (checkboxValue.HasValue)
						checkbox.Selected = (model.JobDrivingLicenceEndorsements.Any(x => x.DrivingLicenceEndorsementId == checkboxValue));
				}
			}

			if (model.Job.DrivingLicenceRequired.IsNotNull()) DrivingLicenceRadioButtonList.SelectValue(model.Job.DrivingLicenceRequired.ToString());

			if (model.JobLicences.IsNotNull()) LicencesUpdateableList.Items = model.JobLicences.Select(jobLicence => jobLicence.Licence).ToList();
			if (model.Job.LicencesRequired.IsNotNull()) LicenceTypeCriteriaRadioButtonList.SelectValue(model.Job.LicencesRequired.ToString());

			if (model.JobCertificates.IsNotNull())
			{
				CertiﬁcationsUpdateableList.Items =
					model.JobCertificates.Select(jobCertificate => jobCertificate.Certificate).ToList();
			}
			else
			{
				if (CertiﬁcationsUpdateableList.Items.IsNotNull())
					CertiﬁcationsUpdateableList.Items.Clear();
			}
			if (model.Job.CertificationRequired.IsNotNull()) CertiﬁcationTypeCriteriaRadioButtonList.SelectValue(model.Job.CertificationRequired.ToString());

			// if (model.JobLanguages.IsNotNull()) LanguagesUpdateableList.Items = model.JobLanguages.Select(jobLanguge => jobLanguge.Language).ToList();      
			if (model.JobLanguages.IsNotNull())
			{
				var languageProficiencies = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.LanguageProficiencies).ToDictionary(l => (long?)l.Id, l => l.Text);
				LanguagesUpdateableList.Items =
					model.JobLanguages.Select(l => new KeyValuePair<string, string[]>
																			(l.LanguageProficiencyId.ToString(),
																				new string[2]
                                        {
                                          l.Language,
                                          (l.LanguageProficiencyId != null & l.LanguageProficiencyId > 0) ? l.Language + " (" + languageProficiencies[l.LanguageProficiencyId] + ")" : l.Language
                                        }
																			)).ToList();
			}

			if (model.Job.LanguagesRequired.IsNotNull()) LanguageTypeCriteriaRadioButtonList.SelectValue(model.Job.LanguagesRequired.ToString());

			if (model.Job.CareerReadinessLevel.IsNotNull()) CareerReadinessLevelDropdown.SelectValue(model.Job.CareerReadinessLevel.Value.ToString(CultureInfo.InvariantCulture));
			if (model.Job.CareerReadinessLevelRequired.IsNotNull()) CareerReadinessTypeCriteriaRadioButtonList.SelectValue(model.Job.CareerReadinessLevelRequired.ToString());

			SetThemeVisibility(model);

			if (model.Job.MinimumAge.IsNotNull()) MinimumAgeTextBox.Text = model.Job.MinimumAge.GetValueOrDefault().ToString(CultureInfo.CurrentUICulture);
			if (model.Job.MinimumAgeReasonValue.IsNotNull()) MinimumAgeReasonDropDown.SelectValue(((MinimumAgeReason)model.Job.MinimumAgeReasonValue).ToString());
			if (model.Job.MinimumAgeReason.IsNotNullOrEmpty()) MinimumAgeReasonTextBox.Text = model.Job.MinimumAgeReason;

            if (App.Settings.ShowCreditCheck)
            {
                BindCreditCheckRadioButtonList();
                CreditCheckValidator.Enabled = true;

                if (model.Job.CreditCheckRequired.IsNotNull())
                    CreditCheckRadioButtonList.SelectedValue = model.Job.CreditCheckRequired.GetValueOrDefault() ? "Y" : "N";
            }

            if (App.Settings.ShowCriminalBackgroundCheck)
            {
                BindCriminalBackgroundCheckRadioButtonList();
                CriminalBackgroundValidator.Enabled = true;

                if (model.Job.CriminalBackgroundExclusionRequired.IsNotNull())
                    CriminalBackgroundCheckRadioButtonList.SelectedValue = model.Job.CriminalBackgroundExclusionRequired.GetValueOrDefault() ? "Y" : "N";
            }
		}

		/// <summary>
		/// Sets the visibility of various form elements depending on theme.
		/// </summary>
		private void SetThemeVisibility(JobWizardViewModel model)
		{
			MinimumAgePlaceHolder.Visible =
				MinimumAgeValidationScript.Visible = !(App.Settings.HideMinimumAge || App.Settings.Theme == FocusThemes.Education);

			MinimumExperienceHeaderPanel.Visible = App.Settings.Theme != FocusThemes.Education;
			MinimumExperiencePanel.Visible = App.Settings.Theme != FocusThemes.Education;
			CertiﬁcationsPanel.Visible = CertiﬁcationsHeaderPanel.Visible = ((App.Settings.Theme == FocusThemes.Workforce) ||
																		(App.Settings.Theme == FocusThemes.Education && model.Job.JobType == JobTypes.Job));

			if (model.Job.JobType == JobTypes.InternshipUnpaid || model.Job.JobType == JobTypes.InternshipPaid)
			{
				MinimumEducationLiteral.Text = CodeLocalise("EducationCriteria.Text", "Education Criteria");
				StudentEnrolledRow.Visible = MinimumCollegeYearsRow.Visible = true;
				MinimumEducationRow.Visible = false;
			}
			else
			{
				MinimumEducationLiteral.Text = CodeLocalise("DegreeLevel.Text", "Degree level");
				StudentEnrolledRow.Visible = MinimumCollegeYearsRow.Visible = false;
				MinimumEducationRow.Visible = true;
			}

			DriverLicencesHeaderPanel.Visible =
				DriverLicencesPanel.Visible =
				DrivingLicenseScript.Visible = !App.Settings.HideDrivingLicence;

			LicensesHeaderPanel.Visible = LicensesPanel.Visible = App.Settings.Theme == FocusThemes.Workforce || App.Settings.ShowLicenseRequirement || model.Job.JobType == JobTypes.Job;
			WorkWeekHeaderPanel.Visible = WorkWeekPanel.Visible = WorkWeekPanelExtender.Visible ==
																 (App.Settings.Theme == FocusThemes.Education && model.Job.JobType == JobTypes.Job);

		}

		/// <summary>
		/// Unbinds the step.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
		internal JobWizardViewModel UnbindStep(JobWizardViewModel model)
		{
			model.Job.MinimumEducationLevel = MinimumEducationDropDownList.SelectedValueToEnum<EducationLevels>();
			model.Job.MinimumEducationLevelRequired = (EducationCriteriaRadioButtonList.SelectedValue == true.ToString());
			model.Job.StudentEnrolled = (StudentEnrolledRadioButtonList.SelectedValue == true.ToString());
			if (MinimumCollegeYearsDropDown.SelectedValue.IsNotNullOrEmpty())
				model.Job.MinimumCollegeYears = Convert.ToInt32(MinimumCollegeYearsDropDown.SelectedValue);
			model.Job.MinimumExperience = null;
			if (MinimumExperienceTextBox.TextTrimmed().IsNotNullOrEmpty()) model.Job.MinimumExperience = int.Parse(MinimumExperienceTextBox.TextTrimmed());
			model.Job.MinimumExperienceMonths = null;
			if (MinimumExperienceMonthsDropDown.SelectedValue.IsNotNullOrEmpty())
				model.Job.MinimumExperienceMonths = Convert.ToInt32(MinimumExperienceMonthsDropDown.SelectedValue);

			if (MinimumExperienceRadioButtonList.SelectedValue.IsNotNullOrEmpty())
				model.Job.MinimumExperienceRequired = (MinimumExperienceRadioButtonList.SelectedValue == true.ToString());

			model.Job.MinimumAge = null;
			model.Job.MinimumAgeReasonValue = (int?)MinimumAgeReasonDropDown.SelectedValueToEnum<MinimumAgeReason>();

			if (MinimumAgeTextBox.TextTrimmed().IsNotNullOrEmpty()) model.Job.MinimumAge = int.Parse(MinimumAgeTextBox.TextTrimmed());
			model.Job.MinimumAgeReason = MinimumAgeReasonTextBox.TextTrimmed();
			model.Job.MinimumAgeRequired = MinimumAgeTextBox.TextTrimmed().IsNotNullOrEmpty();

			model.Job.DrivingLicenceClassId = (DrivingLicenceClassDropDown.SelectedIndex > 0) ? model.Job.DrivingLicenceClassId = DrivingLicenceClassDropDown.SelectedValueToLong() : null;
			model.JobDrivingLicenceEndorsements = new List<JobDrivingLicenceEndorsementDto>();

			foreach (var endorsementId in DrivingLicenceEndorsementsCheckBoxList.Items.Cast<ListItem>().Where(checkbox => checkbox.Selected).Select(checkbox => checkbox.ValueToLong()).Where(endorsementId => endorsementId.HasValue))
			{
				model.JobDrivingLicenceEndorsements.Add(new JobDrivingLicenceEndorsementDto { JobId = model.Job.Id.GetValueOrDefault(), DrivingLicenceEndorsementId = endorsementId.Value });
			}

			if (DrivingLicenceRadioButtonList.SelectedValue.IsNotNullOrEmpty())
				model.Job.DrivingLicenceRequired = (DrivingLicenceRadioButtonList.SelectedValue == true.ToString());

			model.JobLicences = (LicencesUpdateableList.Items.IsNotNullOrEmpty())
															? LicencesUpdateableList.Items.Select(item => new JobLicenceDto
																																							{
																																								JobId = model.Job.Id.GetValueOrDefault(),
																																								Licence = item
																																							}).ToList()
															: new List<JobLicenceDto>();

			if (LicenceTypeCriteriaRadioButtonList.SelectedValue.IsNotNullOrEmpty())
				model.Job.LicencesRequired = (LicenceTypeCriteriaRadioButtonList.SelectedValue == true.ToString());

			model.JobCertificates = (CertiﬁcationsUpdateableList.Items.IsNotNullOrEmpty())
																	? CertiﬁcationsUpdateableList.Items.Select(item => new JobCertificateDto
																																											{
																																												JobId = model.Job.Id.GetValueOrDefault(),
																																												Certificate = item
																																											}).ToList()
																	: new List<JobCertificateDto>();

			if (CertiﬁcationTypeCriteriaRadioButtonList.SelectedValue.IsNotNullOrEmpty())
				model.Job.CertificationRequired = (CertiﬁcationTypeCriteriaRadioButtonList.SelectedValue == true.ToString());

			model.JobLanguages = (LanguagesUpdateableList.Items.IsNotNull())
														? LanguagesUpdateableList.Items.Select(item => new JobLanguageDto
																																						{
																																							JobId = model.Job.Id.GetValueOrDefault(),
																																							Language = item.Value[0],
																																							LanguageProficiencyId = item.Key.IsNotNullOrEmpty() ? Convert.ToInt64(item.Key) : (long?)null
																																						}).ToList()
																		: new List<JobLanguageDto>();

			if (LanguageTypeCriteriaRadioButtonList.SelectedValue.IsNotNullOrEmpty())
				model.Job.LanguagesRequired = (LanguageTypeCriteriaRadioButtonList.SelectedValue == true.ToString());

			model.Job.CareerReadinessLevel = (CareerReadinessLevelDropdown.SelectedIndex > 0)
				? CareerReadinessLevelDropdown.SelectedValueToLong()
				: (long?)null;

			if (CareerReadinessTypeCriteriaRadioButtonList.SelectedValue.IsNotNullOrEmpty())
				model.Job.CareerReadinessLevelRequired = (CareerReadinessTypeCriteriaRadioButtonList.SelectedValue == true.ToString());

			var specialRequirements = UnbindSpecialRequirementsRepeater();
			model.JobSpecialRequirements = (specialRequirements.IsNotNullOrEmpty())
																			? specialRequirements.Select(specialRequirement => new JobSpecialRequirementDto
																																													{
																																														JobId =
																																																model.Job.Id.GetValueOrDefault(),
																																														Requirement = specialRequirement
																																													}).ToList()
																			: new List<JobSpecialRequirementDto>();

			model.Job.HideMinimumAgeOnPosting = !HideMinimumAgeCheckBox.Checked;
			model.Job.HideCertificationsOnPosting = !HideCertificationsCheckBox.Checked;
			model.Job.HideDriversLicenceOnPosting = !HideDriversLicenceCheckBox.Checked;
			model.Job.HideEducationOnPosting = !HideEducationCheckBox.Checked;
			model.Job.HideExperienceOnPosting = !HideExperienceCheckBox.Checked;
			model.Job.HideLanguagesOnPosting = !HideLanguagesCheckBox.Checked;
			model.Job.HideLicencesOnPosting = !HideLicencesCheckBox.Checked;
			model.Job.HideSpecialRequirementsOnPosting = !HideSpecialRequirementsCheckBox.Checked;
			model.Job.HideCareerReadinessOnPosting = !HideCareerReadinessCheckBox.Checked;

			if (App.Settings.ShowCreditCheck)
				model.Job.CreditCheckRequired = CreditCheckRadioButtonList.SelectedValue == "Y";

			if (App.Settings.ShowCriminalBackgroundCheck)
				model.Job.CriminalBackgroundExclusionRequired = CriminalBackgroundCheckRadioButtonList.SelectedValue == "Y";

			return model;
		}

		/// <summary>
		/// Binds the controls.
		/// </summary>
		private void BindControls(JobWizardViewModel model)
		{
			BindMinimumEducationDropDown();
			BindDrivingLicenceClassDropDown();
			BindDrivingLicenceEndorsementsCheckBoxList();
			BindMinimumExperienceMonthsDropDown();
			BindMinimumAgeReasonDropDown();
			BindLanguageProficiencyDropDown();
			BindCareerReadinessLevelDropDown();
			BindRequirementRequiredRadioButtonList(EducationCriteriaRadioButtonList);
			BindRequirementRequiredRadioButtonList(DrivingLicenceRadioButtonList);
			BindRequirementRequiredRadioButtonList(LicenceTypeCriteriaRadioButtonList);
			BindRequirementRequiredRadioButtonList(LanguageTypeCriteriaRadioButtonList);
			BindRequirementRequiredRadioButtonList(CareerReadinessTypeCriteriaRadioButtonList);

			var specialRequirements = (model.JobSpecialRequirements.IsNotNull())
																	? model.JobSpecialRequirements.Select(jobSpecialRequirement => jobSpecialRequirement.Requirement).ToList()
																	: new List<string>();
			BindSpecialRequirementsRepeater(specialRequirements);

			if (App.Settings.Theme == FocusThemes.Education)
			{
				BindMinimumCollegeYearsDropDown();
				BindStudentEnrolledRadioButtonList();
				if (model.Job.JobType == JobTypes.Job)
					BindWorkWeekDropDown(model);

			}
			else
			{
				BindRequirementRequiredRadioButtonList(MinimumExperienceRadioButtonList);
				BindRequirementRequiredRadioButtonList(CertiﬁcationTypeCriteriaRadioButtonList);
			}
		}

		private void BindWorkWeekDropDown(JobWizardViewModel model)
		{
			WorkWeekDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.WorkWeeks), null, CodeLocalise("WorkWeeks.TopDefault", "- select work week -"));
			if (model.Job.WorkWeekId.HasValue)
				WorkWeekDropDown.SelectValue(model.Job.WorkWeekId.Value.ToString());
		}

		/// <summary>
		/// Binds the minimum education drop down.
		/// </summary>
		private void BindMinimumEducationDropDown()
		{
			MinimumEducationDropDownList.Items.Clear();
			MinimumEducationDropDownList.Items.Add(new ListItem(CodeLocalise("MinimumEducation.TopDefault", "- select minimum education level -"), ""));
			MinimumEducationDropDownList.Items.AddEnum(EducationLevels.None, "No specific requirement");
			MinimumEducationDropDownList.Items.AddEnum(EducationLevels.NoDiploma, "No Diploma");
			MinimumEducationDropDownList.Items.AddEnum(EducationLevels.HighSchoolDiplomaOrEquivalent, "High school diploma or equivalent");
			MinimumEducationDropDownList.Items.AddEnum(EducationLevels.SomeCollegeNoDegree, "Some college, no degree");
			MinimumEducationDropDownList.Items.AddEnum(EducationLevels.AssociatesDegree, "Associate’s or vocational degree");
			MinimumEducationDropDownList.Items.AddEnum(EducationLevels.BachelorsDegree, "Bachelor's degree");
			MinimumEducationDropDownList.Items.AddEnum(EducationLevels.MastersDegree, "Master's degree");
			MinimumEducationDropDownList.Items.AddEnum(EducationLevels.DoctorateDegree, "Doctorate degree");
		}

		/// <summary>
		/// Binds the National Career Readiness drop down.
		/// </summary>
		private void BindCareerReadinessLevelDropDown()
		{
			var items = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.NCRCLevel);

			CareerReadinessLevelDropdown.BindLookup(items, null, CodeLocalise("NCRCLevel.TopDefault", "- select NCRC level -"));
		}

		/// <summary>
		/// Binds the minimum age reason drop down.
		/// </summary>
		private void BindMinimumAgeReasonDropDown()
		{
			MinimumAgeReasonDropDown.Items.Clear();

			MinimumAgeReasonDropDown.Items.Add(new ListItem(CodeLocalise("MinimunAgeReasonDropDown.TopDefault", "- choose minimum age reason -"), string.Empty));
			MinimumAgeReasonDropDown.Items.Add(new ListItem(CodeLocalise(MinimumAgeReason.AlcoholTobacco, "Alcohol/tobacco sales"), MinimumAgeReason.AlcoholTobacco.ToString()));
			MinimumAgeReasonDropDown.Items.Add(new ListItem(CodeLocalise(MinimumAgeReason.ChildLaborLaws, "Child labor laws"), MinimumAgeReason.ChildLaborLaws.ToString()));
			MinimumAgeReasonDropDown.Items.Add(new ListItem(CodeLocalise(MinimumAgeReason.CommercialDriversLicense, "Commercial driver's license (CDL)"), MinimumAgeReason.CommercialDriversLicense.ToString()));
			MinimumAgeReasonDropDown.Items.Add(new ListItem(CodeLocalise(MinimumAgeReason.InsuranceBondingRequirements, "Insurance/bonding requirements"), MinimumAgeReason.InsuranceBondingRequirements.ToString()));
			MinimumAgeReasonDropDown.Items.Add(new ListItem(CodeLocalise(MinimumAgeReason.Other, "Other"), MinimumAgeReason.Other.ToString()));
		}

		/// <summary>
		/// Binds the minimum college years drop down.
		/// </summary>
		private void BindMinimumCollegeYearsDropDown()
		{
			MinimumCollegeYearsDropDown.Items.Clear();
			MinimumCollegeYearsDropDown.Items.Add(new ListItem("1", "1"));
			MinimumCollegeYearsDropDown.Items.Add(new ListItem("2", "2"));
			MinimumCollegeYearsDropDown.Items.Add(new ListItem("3", "3"));
			MinimumCollegeYearsDropDown.Items.Add(new ListItem("4", "4"));
		}

		/// <summary>
		/// Binds the driving licence class drop down.
		/// </summary>
		private void BindDrivingLicenceClassDropDown()
		{
			DrivingLicenceClassDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceClasses), null, CodeLocalise("DrivingLicenceClass.TopDefault", "- select license class -"));
		}

		/// <summary>
		/// Binds the driving licence endorsements check box list.
		/// </summary>
		private void BindDrivingLicenceEndorsementsCheckBoxList()
		{
			DrivingLicenceEndorsementsCheckBoxList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceEndorsements).OrderBy(lv => lv.Text).ToList());
		}

		/// <summary>
		/// Binds the Language Proficiency drop down.
		/// </summary>
		private void BindLanguageProficiencyDropDown()
		{
			LanguageProficiencyDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.LanguageProficiencies), null, CodeLocalise("LanguageProficiencies.TopDefault", "- select -"));
		}

		/// <summary>
		/// Binds the requirement required radio button list.
		/// </summary>
		/// <param name="radioButtonList">The radio button list.</param>
		private void BindRequirementRequiredRadioButtonList(RadioButtonList radioButtonList)
		{
			radioButtonList.Items.Clear();
			radioButtonList.Items.Add(new ListItem(CodeLocalise("RequirementPreferred.Text", "Applicants who meet specified criteria are preferred, but allow all"), false.ToString()));
			radioButtonList.Items.Add(
							new ListItem(
								CodeLocalise("JobRequirementRequired.Text",
														 "Only show application information to people who affirm they meet these criteria"),
								true.ToString()));

			if (App.Settings.RequirementsPreferenceDefault != RequirementsPreference.None)
			{
				radioButtonList.SelectValue((App.Settings.RequirementsPreferenceDefault != RequirementsPreference.AllowAll).ToString());
			}
		}

		/// <summary>
		/// Binds the student enrolled radio button.
		/// </summary>
		private void BindStudentEnrolledRadioButtonList()
		{
			StudentEnrolledRadioButtonList.Items.Clear();
			StudentEnrolledRadioButtonList.Items.Add(new ListItem(CodeLocalise("Global.Yes.Label", "Yes"), true.ToString()));
			StudentEnrolledRadioButtonList.Items.Add(new ListItem(CodeLocalise("Global.No.Label", "No"), false.ToString()));
			StudentEnrolledRadioButtonList.SelectValue(true.ToString());
		}

		/// <summary>
		/// Binds the special requirements repeater.
		/// </summary>
		private void BindSpecialRequirementsRepeater(List<string> specialRequirements)
		{
			if (specialRequirements.Count < 3)
			{
				while (specialRequirements.Count < 3)
				{
					specialRequirements.Add(string.Empty);
				}
			}
			else
			{
				// Add an empty string
				specialRequirements.Add(string.Empty);
			}

			SpecialRequirementsApplicantRepeater.DataSource = specialRequirements;
			SpecialRequirementsApplicantRepeater.DataBind();
		}

		/// <summary>
		/// Binds the minimum experience in months drop down.
		/// </summary>
		private void BindMinimumExperienceMonthsDropDown()
		{
			MinimumExperienceMonthsDropDown.Items.Clear();
			MinimumExperienceMonthsDropDown.Items.Add(new ListItem(string.Empty, string.Empty));
			for (var i = 1; i < 12; i++)
			{
				MinimumExperienceMonthsDropDown.Items.Add(new ListItem(i.ToString(), i.ToString()));
			}
		}

		/// <summary>
		/// Binds the credit check radio button list.
		/// </summary>
		private void BindCreditCheckRadioButtonList()
		{
			var yesText = CodeLocalise("Global.Yes.Label", "Yes");
			var noText = CodeLocalise("Global.No.Label", "No");

			CreditCheckRadioButtonList.Items.Clear();

			CreditCheckRadioButtonList.Items.Add(new ListItem(yesText, "Y"));
			CreditCheckRadioButtonList.Items.Add(new ListItem(noText, "N"));
		}

		/// <summary>
		/// Binds the criminal background check radio button list.
		/// </summary>
		private void BindCriminalBackgroundCheckRadioButtonList()
		{
			var yesText = CodeLocalise("Global.Yes.Label", "Yes");
			var noText = CodeLocalise("Global.No.Label", "No");

			CriminalBackgroundCheckRadioButtonList.Items.Clear();

			CriminalBackgroundCheckRadioButtonList.Items.Add(new ListItem(yesText, "Y"));
			CriminalBackgroundCheckRadioButtonList.Items.Add(new ListItem(noText, "N"));
		}

		/// <summary>
		/// Unbinds the special requirements repeater.
		/// </summary>
		/// <returns></returns>
		private List<string> UnbindSpecialRequirementsRepeater()
		{
			return (from RepeaterItem item in SpecialRequirementsApplicantRepeater.Items select (TextBox)item.FindControl("ApplicantMustTextBox") into applicantMustTextBox where applicantMustTextBox.IsNotNull() && applicantMustTextBox.TextTrimmed().IsNotNullOrEmpty() select applicantMustTextBox.TextTrimmed()).ToList();
		}

		/// <summary>
		/// Handles the ItemDataBound event of the SpecialRequirementsApplicantRepeater control.
		/// </summary>
		/// <param name="source">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void SpecialRequirementsApplicantRepeater_ItemDataBound(object source, RepeaterItemEventArgs e)
		{
			// Do nothing if this isn't a Item or AlternateItem
			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			var applicantMustTextBox = (TextBox)e.Item.FindControl("ApplicantMustTextBox");
			applicantMustTextBox.Text = ((string)e.Item.DataItem);
		}

		/// <summary>
		/// Gets the valid licence endorsements javascript array.
		/// </summary>
		/// <returns></returns>
		private string GetValidLicenceEndorsementsJavascriptArray()
		{
			if (LicenceEndorsementsJavascriptArray.IsNotNullOrEmpty())
				return LicenceEndorsementsJavascriptArray;

			var javascriptBuilder = new StringBuilder(@"{"""":"""",");

			var endorsementRules = App.Settings.DrivingLicenceEndorsementRules;

			var drivingLicenceLookUps = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceClasses);
			var drivingLicenceEndorsementLookUps = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceEndorsements);

			foreach (var rule in endorsementRules)
			{
				var drivingLicenceKey = string.Format("DrivingLicenceClasses.{0}", rule.LicenceKey);

				var drivingLicenceId = drivingLicenceLookUps.Where(x => x.Key == drivingLicenceKey).Select(x => x.Id).FirstOrDefault();

				if (drivingLicenceId.IsNotNull())
				{
					javascriptBuilder.AppendFormat(@"""{0}"": """, drivingLicenceId);

					foreach (var endorsement in rule.EndorsementKeys)
					{
						var endorsementKey = string.Format("DrivingLicenceEndorsements.{0}", endorsement);

						var endorsementId = drivingLicenceEndorsementLookUps.Where(x => x.Key == endorsementKey).Select(x => x.Id).FirstOrDefault();

						if (endorsementId.IsNotNull())
							javascriptBuilder.AppendFormat("{0},", endorsementId);
					}

					javascriptBuilder.Append(@""",");
				}
			}

			javascriptBuilder.Append("}");

			return javascriptBuilder.ToString().Replace(",}", "}");
		}

		#endregion

		#region Localise the UI

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			AddLicenceButton.Text = CodeLocalise("AddLicenseButton.Text", "Add");
			AddCertiﬁcationButton.Text = CodeLocalise("AddCertiﬁcationButton.Text", "Add");
			AddLanguageButton.Text = AddLanguageButtonEdu.Text = CodeLocalise("AddLanguageButton.Text", "Add");
			AddAnotherRequirementButton.Text = CodeLocalise("AddAnotherRequirementButton.Text", "Add more requirements");
			MinimumEducationRequired.ErrorMessage = CodeLocalise("MinimumEducationRequired.ErrorMessage", "Minimum education level required");
            CreditCheckValidator.ErrorMessage = CodeLocalise("CreditCheckRequired.ErrorMessage", "Credit check is required");
            CriminalBackgroundValidator.ErrorMessage = CodeLocalise("CriminalBackgroundRequired.ErrorMessage", "Criminal check is required");

			EducationCriteriaPreferenceRequired.ErrorMessage =
					MinimumExperiencePreferenceRequired.ErrorMessage =
					DrivingLicencePreferenceRequired.ErrorMessage =
					CareerReadinessTypeCriteriaPreferenceRequired.ErrorMessage =
					LanguagesUpdateableListValidator.ErrorMessage =
					CertiﬁcationsUpdateableListValidator.ErrorMessage =
					LicencesUpdateableListValidator.ErrorMessage = CodeLocalise("PreferenceRequired.ErrorMessage", "Please select whether this is a preferred or mandatory requirement");

			MinimumAgeReasonRequired.ErrorMessage = CodeLocalise("MinimumAgeReasonRequired.ErrorMessage", "Minimum age reason is required");
			MinimumAgeReasonValidator.ErrorMessage = CodeLocalise("MinimumAgeReasonValidator.ErrorMessage", "Reason is required");
			MinimumAgeValidator.ErrorMessage = CodeLocalise("MinimumAgeValidator.ErrorMessage", "Minimum age must be numeric or left blank");
			MinimumExperienceValidator.ErrorMessage = CodeLocalise("MinimumExperienceValidator.ErrorMessage", "Minimum experience must be numeric or left blank");
			LicenceTypeValidator.ErrorMessage = CodeLocalise("LicenceTypeValidator.ErrorMessage", "Duplicate licences not allowed");
			CertiﬁcationTypeTextBoxValidator.ErrorMessage = CodeLocalise("CertiﬁcationTypeTextBoxValidator.ErrorMessage", "Duplicate certifications not allowed");
			LanguageTextBoxValidator.ErrorMessage = CodeLocalise("LanguageTextBoxValidator.ErrorMessage", "Duplicate languages types not allowed");
			LanguageProficiencyRequired.ErrorMessage = CodeLocalise("LanguageProficiencyRequired.ErrorMessage", "Language proficiency level missing");

			LicenceTypeTextBox.ServiceMethod = "GetLicences";
			LicenceTypeTextBox.ServiceUrl = ResolveUrl("~/Services/AjaxService.svc");
			LicenceTypeTextBox.InFieldLabelText = CodeLocalise("LicenceType.InlineLabel", "enter license");
			LicenceTypeTextBox.ServiceMethodSearchKey = "prefixText";
			LicenceTypeTextBox.ResultSize = 10;

			ExpandCollapseAllButton.Text = HtmlLocalise("ExpandAllButton.Text", "Expand all");

			if (App.Settings.ShowCriminalBackgroundCheck || App.Settings.ShowCreditCheck)
			{
				var category = Convert.ToInt64(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DocumentCategories).Where(x => x.Key.Equals("DocumentCategories.LegalNotice")).Select(x => x.Id).FirstOrDefault());
				var module = App.Settings.Module == FocusModules.Assist ? DocumentFocusModules.Assist : DocumentFocusModules.Talent;

				if (App.Settings.ShowCreditCheck)
				{
					var group = Convert.ToInt64(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DocumentGroups).Where(x => x.Key.Equals("DocumentGroups.CreditCheck")).Select(x => x.Id).FirstOrDefault());
					var noticeUrl = UrlBuilder.Notice(Uri.EscapeDataString(CodeLocalise("LegalNotice.Title", "Legal Notices")), category, group, module);

					TEGLNoticeToEmployersLabel.DefaultText = CodeLocalise("CreditCheckExclusionWarning.Text", "We advise not to automatically exclude job seekers based on their credit history unless able to show that a credit history restriction is related to the job posted and consistent with business needs. While permitted to use credit reports in hiring and other decisions, this type of screening requirement may unjustifiably limit the employment opportunities of applicants in protected groups and may therefore violate federal civil rights laws.<br/><br/><a href= '{0}' target='_blank'>Please click for more information</a>", noticeUrl);
				}

				if (App.Settings.ShowCriminalBackgroundCheck)
				{
					var group = Convert.ToInt64(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DocumentGroups).Where(x => x.Key.Equals("DocumentGroups.CriminalBackgroundCheck")).Select(x => x.Id).FirstOrDefault());
					var noticeUrl = UrlBuilder.Notice(Uri.EscapeDataString(CodeLocalise("LegalNotice.Title", "Legal Notices")), category, group, module);

					CriminalBackgroundCheckNoticeToEmployersLabel.DefaultText = CodeLocalise("CriminalBackgroundCheckExclusionWarning.Text", "We advise not to automatically exclude job seekers based on their criminal background unless able to show that a criminal background restriction is related to the job posted and consistent with business needs. While permitted to use criminal background reports in hiring and other decisions, this type of screening requirement may unjustifiably limit the employment opportunities of applicants in protected groups and may therefore violate federal civil rights laws.<br/><br/><a href= '{0}' target='_blank'>Please click for more information</a>", noticeUrl);
				}
			}
		}

		/// <summary>
		/// Applies the branding.
		/// </summary>
		private void ApplyBranding()
		{
			MinimumEducationHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			MinimumEducationPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			MinimumEducationPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();

			BackgroundCheckHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			BackgroundCheckPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			BackgroundCheckPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			WorkWeekHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			WorkWeekPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			WorkWeekPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();

			MinimumExperienceHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			MinimumExperiencePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			MinimumExperiencePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();

			MinimumAgeHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			MinimumAgePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			MinimumAgePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();

			DriverLicencesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			DriverLicencesPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			DriverLicencesPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();

			LicensesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			LicensesPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			LicensesPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();

			CertiﬁcationsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			CertiﬁcationsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			CertiﬁcationsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();

			LanguagesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			LanguagesPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			LanguagesPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();

			SpecialRequirementsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			SpecialRequirementsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			SpecialRequirementsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();

			CareerReadinessHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			CareerReadinessPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			CareerReadinessPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
		}

		/// <summary>
		/// Applies the theme.
		/// </summary>
		private void ApplyTheme()
		{
			ExpandCollapseAllRow.Visible =
	CareerReadinessPlaceHolder.Visible = (App.Settings.Theme == FocusThemes.Workforce);

			TestPostDisplay.Visible = LanguageProficiencies.Visible =
			AddLanguageButton.Visible = LanguageProficiencyDropDown.Visible = LanguageProficiencyDropDownLabel.Visible = (App.Settings.Theme == FocusThemes.Workforce);

			AddLanguageButtonEdu.Visible = (App.Settings.Theme == FocusThemes.Education);
		}

		/// <summary>
		/// Initialises the background check panel.
		/// </summary>
		private void InitialiseBackgroundCheck()
		{
			BackgroundCheckPlaceHolder.Visible = App.Settings.ShowCriminalBackgroundCheck || App.Settings.ShowCreditCheck;
			CreditCheckPlaceHolder.Visible = App.Settings.ShowCreditCheck;
			CriminalBackgroundCheckPlaceHolder.Visible = App.Settings.ShowCriminalBackgroundCheck;
		}

		#endregion

		#region Registering Javascript

		/// <summary>
		/// Registers the javascript.
		/// </summary>
		private void RegisterJavascript()
		{
			// Build the JS
			var js = @"function pageLoad() {
				$(document).ready(function()
				{
					// Apply jQuery to infield labels
					$("".inFieldLabel > label"").inFieldLabels();
				});
			}";

			var sManager = ScriptManager.GetCurrent(Page);
			var scriptKey = "JobWizardStep4JavaScript";
			if (sManager != null && sManager.IsInAsyncPostBack)
			{
				//if a MS AJAX request, use the Scriptmanager class
				ScriptManager.RegisterStartupScript(Page, Page.GetType(), scriptKey, js, true);
			}
			else
			{
				//if a standard postback, use the standard ClientScript method
				js = string.Concat("Sys.Application.add_load(function(){", js, "});");
				Page.ClientScript.RegisterStartupScript(Page.GetType(), scriptKey, js, true);
			}
		}

		#endregion

	}
}