#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Common.Code;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Code.Controllers.WebAssist;
using Focus.Web.Core.Models;
using Framework.Core;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class EmployerRegistrationStep1 : UserControlBase
	{
		public override IPageController RegisterPageController()
		{
			return new EmployerRegistrationStep1Controller(App);
		}

		protected EmployerRegistrationStep1Controller PageController
		{
			get { return PageControllerBaseProperty as EmployerRegistrationStep1Controller; }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			// Need to do this during the load because recaptcha loses this when set in code behind
			if (App.Settings.RecaptchaEnabled)
			{
				PrivateKey = App.Settings.RecaptchaPrivateKey;
				PublicKey = App.Settings.RecaptchaPublicKey;
			}

			SecurityQuestionsPanel.Visible = App.Settings.EnableTalentPasswordSecurity;

			if (IsPostBack) return;

			UserNameRegEx.ValidationExpression = App.Settings.UserNameRegExPattern;
			UserNameRegEx.Enabled = UserNameRegEx.ValidationExpression.IsNotNullOrEmpty();
			PasswordRegEx.ValidationExpression = App.Settings.PasswordRegExPattern;
			FederalEmployerIdentificationNumberRegEx.ValidationExpression = App.Settings.FEINRegExPattern;

			if (App.Settings.DisplayFEIN.IsIn(ControlDisplayType.Hidden, ControlDisplayType.Optional))
				NotificationDiv.Style.Add("display", "none");

			if (App.Settings.DisplayFEIN == ControlDisplayType.Hidden)
			{
				FEINRow.Visible = false;
				FEINConfirmRow.Visible = false;
			}
			else
			{
				FederalEmployerIdentificationNumberRequired.Enabled = ConfirmFederalEmployerIdentificationNumberRequired.Enabled = (App.Settings.DisplayFEIN == ControlDisplayType.Mandatory);
			}

            Step1RegistrationPlaceHolder.Visible = (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForAssist) && App.Settings.Module == FocusModules.Assist) || (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForTalent) && App.Settings.Module == FocusModules.Talent);

			if (App.Settings.EnableTalentPasswordSecurity)
				SecurityQuestionsPanel.Bind();

			LocaliseUI();
		}

        /// <summary>
        /// Gets the Public key for Recaptcha.
        /// </summary>
        /// <value>The the Public key for Recaptcha.</value>
        public  string PublicKey { get; set;}

        /// <summary>
        /// Gets the Private key for Recaptcha.
        /// </summary>
        /// <value>The the Private key for Recaptcha.</value>
        internal string PrivateKey { get; set; }



		/// <summary>
		/// Shows any non-standard validation messages
		/// </summary>
		internal void ShowValidationMessages()
		{
            RecaptchaErrorMessage.Visible = RecaptchaRow.Visible;
		}


        public bool Validate()
        {
            string Response = Request["g-recaptcha-response"];//Getting Response String Append to Post Method
            bool Valid = false;
            //Request to Google Server
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create
            (" https://www.google.com/recaptcha/api/siteverify?secret= " + PrivateKey + "&response=" + Response);
            //"https://www.google.com/recaptcha/api/siteverify?secret=" + ReCaptcha_Secret + "&response=" + response;
            try
            {
                //Google recaptcha Response
                using (WebResponse wResponse = req.GetResponse())
                {

                    using (StreamReader readStream = new StreamReader(wResponse.GetResponseStream()))
                    {
                        string jsonResponse = readStream.ReadToEnd();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        CaptchaResponse data = js.Deserialize<CaptchaResponse>(jsonResponse);// Deserialize Json

                        Valid = Convert.ToBoolean(data.Success);
                    }
                }

                return Valid;
            }
            catch (WebException ex)
            {
                throw ex;
            }
        }


        /// <summary>
		/// Gets the federal employer identification number.
		/// </summary>
		/// <value>The federal employer identification number.</value>
		internal string FederalEmployerIdentificationNumber
		{
			get { return FederalEmployerIdentificationNumberTextBox.Text; }
		}

		/// <summary>
		/// Gets the username.
		/// </summary>
		/// <value>The username.</value>
		internal string UserName
		{
			get { return UserNameTextBox.TextTrimmed(); }
		}

		/// <summary>
		/// Gets or sets the password.
		/// </summary>
		/// <value>The password.</value>
		internal string Password
		{
			get { return PasswordTextBox.TextTrimmed(); }
			set { PasswordTextBox.Text = value; }
		}

		/// <summary>
		/// Updates the model.
		/// </summary>
		/// <param name="model">The model.</param>
		internal void UpdateModel(TalentRegistrationModel model)
		{
            if (App.Settings.SSOEnabled || (App.Settings.SamlEnabledForTalent && App.Settings.Module == FocusModules.Talent) || (App.Settings.Module == FocusModules.Assist && App.Settings.SamlEnabledForAssist)) return;
			model.AccountUserName = UserNameTextBox.TextTrimmed();
			model.AccountPassword = PasswordTextBox.TextTrimmed();

			/*
			var userSecurityQuestions = new List<UserSecurityQuestionDto>();
			var questionIndex = 1;

			if( !SecurityQuestionsPanel.Question1.IsNullOrWhitespace() || SecurityQuestionsPanel.QuestionId1.HasValue )
			{
				userSecurityQuestions.Add(new UserSecurityQuestionDto
				{
					QuestionIndex = questionIndex++,
					SecurityQuestion = SecurityQuestionsPanel.Question1,
					SecurityQuestionId = SecurityQuestionsPanel.QuestionId1,
					SecurityAnswerEncrypted = SecurityQuestionsPanel.Answer1
				});
			}
			if( !SecurityQuestionsPanel.Question2.IsNullOrWhitespace() || SecurityQuestionsPanel.QuestionId2.HasValue )
			{
				userSecurityQuestions.Add(new UserSecurityQuestionDto
				{
					QuestionIndex = questionIndex++,
					SecurityQuestion = SecurityQuestionsPanel.Question2,
					SecurityQuestionId = SecurityQuestionsPanel.QuestionId2,
					SecurityAnswerEncrypted = SecurityQuestionsPanel.Answer2
				});
			}
			if( !SecurityQuestionsPanel.Question3.IsNullOrWhitespace() || SecurityQuestionsPanel.QuestionId3.HasValue )
			{
				userSecurityQuestions.Add(new UserSecurityQuestionDto
				{
					QuestionIndex = questionIndex,
					SecurityQuestion = SecurityQuestionsPanel.Question3,
					SecurityQuestionId = SecurityQuestionsPanel.QuestionId3,
					SecurityAnswerEncrypted = SecurityQuestionsPanel.Answer3
				});
			}
			*/
			model.UserSecurityQuestions = SecurityQuestionsPanel.Unbind().ToArray();
		}

		/// <summary>
		/// Shows the recaptcha.
		/// </summary>
        /// <param name="showCaptcha">if set to <c>true</c> [previously progressed automatic step2].</param>
		internal void ShowRecaptcha(bool showCaptcha)
		{
			// Show recaptcha if this control is visible, recaptcha is enabled and the module is Talent (Assist users will already be logged in)
            var show = (Visible && !showCaptcha) && App.Settings.RecaptchaEnabled && App.Settings.Module == FocusModules.Talent;
            //Recaptcha.SkipRecaptcha = !show;
			RecaptchaRow.Visible = show;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			UserNameRequired.ErrorMessage = CodeLocalise("UserName.RequiredErrorMessage", "Email address is required");
			UserNameRegEx.ErrorMessage = CodeLocalise("UserName.RegExErrorMessage", "Email address format is invalid");
			ConfirmUserNameRequired.ErrorMessage = CodeLocalise("ConfirmUserName.RequiredErrorMessage", "Confirm email address is required");
			ConfirmUserNameCompare.ErrorMessage = CodeLocalise("ConfirmUserNameCompare.CompareErrorMessage", "Email addresses must match");
			PasswordRequired.ErrorMessage = CodeLocalise("Password.RequiredErrorMessage", "Password is required");
			PasswordRegEx.ErrorMessage = CodeLocalise("PasswordTextBox.RegExErrorMessage", "Password must be 6-20 characters and contain at least 1 number.");
			ConfirmPasswordRequired.ErrorMessage = CodeLocalise("ConfirmPassword.RequiredErrorMessage", "Confirm password is required");
			ConfirmPasswordCompare.ErrorMessage = CodeLocalise("ConfirmPasswordCompare.CompareErrorMessage", "Passwords must match");
			FederalEmployerIdentificationNumberRequired.ErrorMessage = CodeLocalise("FederalEmployerIdentificationNumber.RequiredErrorMessage", "Federal Employer ID (FEIN) required.");
			ConfirmFederalEmployerIdentificationNumberRequired.ErrorMessage = CodeLocalise("ConfirmFederalEmployerIdentificationNumber.RequiredErrorMessage", "Confirm (FEIN) is required.");
			ConfirmFederalEmployerIdentificationNumberCompare.ErrorMessage = CodeLocalise("ConfirmFederalEmployerIdentificationNumber.CompareErrorMessage", "Federal Employer ID's (FEIN's) must match.");
			FederalEmployerIdentificationNumberRegEx.ErrorMessage = CodeLocalise("UserName.FederalEmployerIdentificationNumberRegEx", "Federal Employer ID (FEIN) is invalid");
			//RecaptchaValidator.ErrorMessage = CodeLocalise("RecaptchaValidator.RequiredErrorMessage", "Enter what you see in order to continue");
			RecaptchaErrorMessage.Text = CodeLocalise("RecaptchaErrorMessage.Text", "What you have entered is incorrect, try again");
		}

		/// <summary>
		/// Handles the Clicked event of the FeinHelpLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void FeinHelpLinkButton_Clicked(object sender, EventArgs e)
		{
			var result = PageController.ShowNeedFeinModal();

			Handle(result);

		}
	}

    class CaptchaResponse
    {
        [JsonProperty("success")]
        public string Success { get; set; }

        [JsonProperty("error-codes")]
        public List<string> ErrorCodes { get; set; }
    }
}
