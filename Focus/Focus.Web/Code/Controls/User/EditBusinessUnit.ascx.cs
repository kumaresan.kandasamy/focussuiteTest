﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Common.Helpers;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models;

using Framework.Core;

#endregion


namespace Focus.Web.Code.Controls.User
{
	public partial class EditBusinessUnit : UserControlBase
	{
		private BusinessUnitModel _model
		{
			get { return GetViewStateValue<BusinessUnitModel>("EditBusinessUnit: Model"); }
			set { SetViewStateValue("EditBusinessUnit: Model", value); }
		}

		public string ValidationGroup
		{
			get { return GetViewStateValue("EditBusinessUnit:ValidationGroup", string.Empty); }
			set { SetViewStateValue("EditBusinessUnit:ValidationGroup", value); }
		}

		public bool ValidationEnabled
		{
			get { return GetViewStateValue<bool>("EditBusinessUnit:ValidationEnabled", true); }
			set { SetViewStateValue("EditBusinessUnit:ValidationEnabled", value); }
		}


		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				SetValidation();
                //AIM-FVN-6819 - Fixed the dropdown binding issue in ManageCompany page
				//BindControls();
				LocaliseUI();
			  ApplyTheme();

        ContactUrlRegEx.ValidationExpression = App.Settings.UrlRegExPattern;
        AddressPostcodeRegexValidator.ValidationExpression = App.Settings.ExtendedPostalCodeRegExPattern;

				AddressCountyDropDownListCascadingDropDown.BehaviorID = AddressCountyDropDownListCascadingDropDown.ClientID;
				IndustrialClassificationAutoCompleteExtender.BehaviorID = IndustrialClassificationAutoCompleteExtender.ClientID;
        CountyRow.Visible = 
          AddressCountyRequired.Enabled = 
          LegalNamePlaceHolder.Visible = 
          App.Settings.Theme == FocusThemes.Workforce;

        NoOfEmployeesValidator.Enabled = NoOfEmployeesRow.Visible = App.Settings.ShowEmployeeNumbers;
			}

			HandleAccountTypeDisplay();

			if (!Page.ClientScript.IsClientScriptIncludeRegistered(GetType(), "EditBusinessUnitScript"))
			{
				ScriptManager.RegisterClientScriptInclude(this, GetType(), "EditBusinessUnitScript", UrlHelper.GetCacheBusterUrl("~/Assets/Scripts/Focus.EditBusinessUnit.js"));
			}

			ScriptManager.RegisterStartupScript(this, GetType(), "InitBusinessUnitScript", 
				string.Format("focusSuite.editBusinessUnit.init({{PhoneTextBox: '{0}',AlternatePhoneTextBox: '{1}',AlternatePhone2TextBox: '{2}',PostcodeZipTextBox: '{3}',AjaxServiceUrl: '{4}',StateDropdownList: '{5}',CountyDropdownList: '{6}',CountyDropdownListCascadingDropdown: '{7}',CountyDefaultText: '{8}',TownCityTextBox: '{9}',CompanyDescriptionTextBox: '{10}',CountyDropdownListCascadingDropdownBehaviour: '{11}',StateZZ: '{12}',CountyOutsideUS: '{13}',CountryUS: '{14}', ControlClientId: '{15}', ExtendedPostalCodeMaskPattern: '{16}', CountryDropDownList: '{17}'}});", 
          ContactPhoneTextBox.ClientID, 
          ContactAlternatePhoneTextBox.ClientID, 
          ContactAlternate2PhoneTextBox.ClientID, 
          AddressPostcodeZipTextBox.ClientID, 
          UrlBuilder.AjaxService(), 
          AddressStateDropDownList.ClientID,
  				AddressCountyDropDownList.ClientID, 
          AddressCountyDropDownListCascadingDropDown.ClientID, 
          CodeLocalise("Global.County.TopDefault", "- select county -"), 
          AddressTownCityTextBox.ClientID, 
          CompanyDescriptionTextBox.ClientID,
          AddressCountyDropDownListCascadingDropDown.BehaviorID, 
          GetLookupId(Constants.CodeItemKeys.States.ZZ), 
          GetLookupId(Constants.CodeItemKeys.Counties.OutsideUS), 
          GetLookupId(Constants.CodeItemKeys.Countries.US), 
          ClientID, 
          App.Settings.ExtendedPostalCodeMaskPattern, 
          AddressCountryDropDownList.ClientID), true);
		}

		private void HandleAccountTypeDisplay()
		{
      
			AccountTypeRequired.Enabled = false;
		    if (App.Settings.AccountTypeDisplayType == ControlDisplayType.Mandatory)
		    {
		      AccountTypeRequired.Enabled = true;
		    }
		    else if (App.Settings.AccountTypeDisplayType == ControlDisplayType.Hidden)
		    {
		      AccountTypeRequired.Enabled = false;
		      AccountTypeHeader.Visible = false;
		      AccountTypeDropDownList.Visible = false;
		    }
        if (App.Settings.Theme == FocusThemes.Education)
        {
          AccountTypeRequired.Enabled = false;
          AccountTypeHeader.Visible = false;
          AccountTypeDropDownList.Visible = false;
        }
		}

		/// <summary>
		/// Binds the specified business unit id.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <param name="employerId">The employer id.</param>
		/// <param name="showCompanyDescription">if set to <c>true</c> [show company description].</param>
		/// <param name="legalName">Any existing legal name</param>
		public void Bind(long businessUnitId = 0, long employerId = 0, bool showCompanyDescription = true, string legalName = null)
		{
            //AIM-FVN-6819 - Fixed the dropdown binding issue in ManageCompany page
            BindControls();
			ClearValues();

			if(businessUnitId == 0)
			{
				var businessUnitAddress = new BusinessUnitAddressDto {IsPrimary = true};

				_model = new BusinessUnitModel
				         	{
				         		BusinessUnit = new BusinessUnitDto { EmployerId = employerId, LegalName = legalName },
										BusinessUnitAddress = businessUnitAddress,
										BusinessUnitDescription = new BusinessUnitDescriptionDto { Title = CodeLocalise("CompanyDescription", "#BUSINESS# description"), IsPrimary = true }
									};
                NaicsLink.Visible = true;
			}
			else
			{
				_model = new BusinessUnitModel
				         	{
				         		BusinessUnit = ServiceClientLocator.EmployerClient(App).GetBusinessUnit(businessUnitId),
				         		BusinessUnitAddress = ServiceClientLocator.EmployerClient(App).GetPrimaryBusinessUnitAddress(businessUnitId),
										BusinessUnitDescription = ServiceClientLocator.EmployerClient(App).GetPrimaryBusinessUnitDescription(businessUnitId),
										LockVersion = ServiceClientLocator.EmployerClient(App).GetBusinessUnit(businessUnitId).LockVersion
				         	};
				
			}
			
			SetInitialValues();

			CompanyDescriptionRow.Visible = showCompanyDescription;
		  IndustryClassificationRow.Visible = App.Settings.Theme == FocusThemes.Workforce;
		}

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <returns></returns>
		public BusinessUnitModel Unbind()
		{
			_model.BusinessUnit.Name = BusinessUnitNameTextBox.TextTrimmed();
		  _model.BusinessUnit.LegalName = LegalNameTextBox.TextTrimmed();
			_model.BusinessUnitAddress.Line1 = AddressLine1TextBox.TextTrimmed();
			_model.BusinessUnitAddress.Line2 = AddressLine2TextBox.TextTrimmed();
			_model.BusinessUnitAddress.TownCity = AddressTownCityTextBox.TextTrimmed();
			_model.BusinessUnitAddress.PostcodeZip = AddressPostcodeZipTextBox.TextTrimmed();
			_model.BusinessUnitAddress.StateId = AddressStateDropDownList.SelectedValueToLong();
			_model.BusinessUnitAddress.CountyId = AddressCountyDropDownList.SelectedValueToLong();
			_model.BusinessUnitAddress.CountryId = AddressCountryDropDownList.SelectedValueToLong();
			_model.BusinessUnitAddress.PublicTransitAccessible = PublicTransitAccessible.Checked;
			_model.BusinessUnit.Url = ContactUrlTextBox.TextTrimmed(defaultValue: null);
			_model.BusinessUnit.OwnershipTypeId = OwnershipTypeDropDownList.SelectedValueToLong();
			_model.BusinessUnit.AccountTypeId = AccountTypeDropDownList.SelectedValueToLong();
			_model.BusinessUnit.PrimaryPhone = ContactPhoneTextBox.TextTrimmed();
			_model.BusinessUnit.PrimaryPhoneExtension = ExtensionTextBox.TextTrimmed();
			_model.BusinessUnit.PrimaryPhoneType = PhoneTypeDropDownList.SelectedValue;
			_model.BusinessUnit.AlternatePhone1 = ContactAlternatePhoneTextBox.TextTrimmed();
			_model.BusinessUnit.AlternatePhone1Type = AltPhoneTypeDropDownList.SelectedValue;
			_model.BusinessUnit.AlternatePhone2 = ContactAlternate2PhoneTextBox.TextTrimmed();
			_model.BusinessUnit.AlternatePhone2Type = AltPhoneType2DropDownList.SelectedValue;
			_model.BusinessUnit.IndustrialClassification = IndustrialClassificationTextBox.TextTrimmed();
		  _model.BusinessUnit.NoOfEmployees = NoOfEmployeesDropDownList.SelectedValueToLong();
      if (_model.BusinessUnitDescription.IsNotNull())
			  _model.BusinessUnitDescription.Description = CompanyDescriptionTextBox.TextTrimmed().TruncateString(2000);
			
			return _model;
		}


		/// <summary>
		/// Binds the controls.
		/// </summary>
		private void BindControls()
		{
			// Ownership
			OwnershipTypeDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.OwnershipTypes), null, CodeLocalise("Global.OwnershipType.TopDefault", "- select ownership -"));

			// AccountType
			AccountTypeDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.AccountTypes), null, CodeLocalise("Global.AccountType.TopDefault", "- select account type -"));

			// State
			AddressStateDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States), null, CodeLocalise("Global.State.TopDefault", "- select state -"));

			// Countries
			AddressCountryDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries), null, CodeLocalise("Global.Country.TopDefault", "- select country -"));
			//AddressCountyDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Counties), null, CodeLocalise("Global.County.TopDefault", "- select county -"));
      AddressCountyDropDownList.Items.Insert(0, new ListItem(CodeLocalise("Global.Country.TopDefault", "- select country -"), string.Empty));
      BindContactTypeDropDown(PhoneTypeDropDownList);
			BindContactTypeDropDown(AltPhoneTypeDropDownList);
			BindContactTypeDropDown(AltPhoneType2DropDownList);
		  BindNoOfEmployeesDropdown(NoOfEmployeesDropDownList);
		}

		/// <summary>
		/// Binds the contact type drop down.
		/// </summary>
		/// <param name="dropDownList">The drop down list.</param>
		private void BindContactTypeDropDown(DropDownList dropDownList)
		{
			dropDownList.Items.Clear();

			dropDownList.Items.AddEnum(PhoneTypes.Phone, "landline");
			dropDownList.Items.AddEnum(PhoneTypes.Mobile, "mobile");
			dropDownList.Items.AddEnum(PhoneTypes.Fax, "fax");
			dropDownList.Items.AddEnum(PhoneTypes.Other, "other");
		}

    private void BindNoOfEmployeesDropdown(ListControl dropdownlist)
    {
      dropdownlist.Items.Clear();
      dropdownlist.Items.AddLocalised("Global.NoOfEmployees.TopDefault", "- select number of employees -", "");
      dropdownlist.Items.AddLocalised("NoOfEmployeesRange1.Text", "1-49", "1");
      dropdownlist.Items.AddLocalised("NoOfEmployeesRange2.Text", "50-99", "2");
      dropdownlist.Items.AddLocalised("NoOfEmployeesRange3.Text", "100-499", "3");
      dropdownlist.Items.AddLocalised("NoOfEmployeesRange4.Text", "500-999", "4");
      dropdownlist.Items.AddLocalised("NoOfEmployeesRange5.Text", "1000-4999", "5");
      dropdownlist.Items.AddLocalised("NoOfEmployeesRange6.Text", "5000+", "6");
    }

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			EmployerNameRequired.ErrorMessage = CodeLocalise("EmployerName.RequiredErrorMessage", "#BUSINESS# name is required.");
      LegalNameRequired.ErrorMessage = CodeLocalise("LegalNameRequired.RequiredErrorMessage", "Business legal name is required.");
			OwnershipTypeRequired.ErrorMessage = CodeLocalise("OwnershipType.RequiredErrorMessage", "Ownership type is required.");
			AddressLine1Required.ErrorMessage = CodeLocalise("AddressLine1.RequiredErrorMessage", "Address (line 1) is required.");
			IndustrialClassificationRequired.ErrorMessage = CodeLocalise("IndustrialClassification.RequiredErrorMessage", "Industrial classification is required.");
			AddressTownCityRequired.ErrorMessage = CodeLocalise("AddressTownCity.RequiredErrorMessage", "City is required.");
			AddressStateRequired.ErrorMessage = CodeLocalise("AddressState.RequiredErrorMessage", "State is required.");
			AddressCountyRequired.ErrorMessage = CodeLocalise("AddressCounty.RequiredErrorMessage", "County is required.");
			AddressPostcodeZipRequired.ErrorMessage = CodeLocalise("AddressPostcodeZip.RequiredErrorMessage", "ZIP or postal code is required.");
      AddressPostcodeRegexValidator.ErrorMessage = CodeLocalise("AddressPostcodeRegexValidator.ErrorMessage", "ZIP code must be in a 5 or 9-digit format");
      AddressCountryRequired.ErrorMessage = CodeLocalise("AddressCountry.RequiredErrorMessage", "Country is required.");
			ContactPhoneRequired.ErrorMessage = CodeLocalise("ContactPhone.RequiredErrorMessage", "Phone number is required.");
			IndustrialClassificationValidator.ErrorMessage = CodeLocalise("IndustrialClassification.NotFoundErrorMessage", "Industry classification (NAICS) not found.");
			ContactUrlRegEx.ErrorMessage = CodeLocalise("ContactUrlRegEx.ErrorMessage", "Invalid url. Please ensure http:// or https:// is present, as appropriate to your url.");
      NoOfEmployeesValidator.ErrorMessage = CodeLocalise("NoOfEmployees.RequiredErrorMessage", "Number Of Employees is required.");
			CompanyDescriptionLengthValidator.ErrorMessage = CodeLocalise("CompanyDescriptionLengthValidator.ErrorMessage", "Description must be 2000 characters or less");
			
			if (App.Settings.AccountTypeDisplayType == ControlDisplayType.Mandatory)
			{
				AccountTypeRequired.ErrorMessage = CodeLocalise("AccountType.RequiredErrorMessage", "Account type is required.");
				AccountTypeHeader.Text = HtmlRequiredLabel(AccountTypeDropDownList, "AccountType.Label", "Account Type");
			}
			else
			{
				AccountTypeHeader.Text = HtmlLabel(AccountTypeDropDownList, "AccountType.Label", "Account Type");
			}

            //Accessiblity 508 compliance
            PublicTransitAccessible.InputAttributes.Add("Title", "Public transit accessible");
		}

    /// <summary>
    /// Shows/hides controls based on the theme
    /// </summary>
    private void ApplyTheme()
    {
      OwnershipTypePlaceHolder.Visible = App.Settings.Theme == FocusThemes.Workforce;
    }

		/// <summary>
		/// Sets the validation.
		/// </summary>
		private void SetValidation()
		{
			if(ValidationGroup.IsNotNullOrEmpty())
			{
				EmployerNameRequired.ValidationGroup = 
          LegalNameRequired.ValidationGroup =
          OwnershipTypeRequired.ValidationGroup = 
          AddressLine1Required.ValidationGroup = 
          IndustrialClassificationRequired.ValidationGroup =
					AddressTownCityRequired.ValidationGroup = 
          AddressStateRequired.ValidationGroup = 
          AddressCountyRequired.ValidationGroup = 
          AddressPostcodeZipRequired.ValidationGroup =
					AddressPostcodeZipTextBox.ValidationGroup = 
					AddressCountryRequired.ValidationGroup = 
          ContactPhoneRequired.ValidationGroup = 
          IndustrialClassificationValidator.ValidationGroup = 
          ContactUrlRegEx.ValidationGroup = 
					AccountTypeRequired.ValidationGroup = 
          NoOfEmployeesValidator.ValidationGroup =
					CompanyDescriptionLengthValidator.ValidationGroup = ValidationGroup;
			}

			EmployerNameRequired.Enabled = 
        LegalNameRequired.Enabled = 
        OwnershipTypeRequired.Enabled = 
        AddressLine1Required.Enabled = 
        IndustrialClassificationRequired.Enabled = 
        AddressTownCityRequired.Enabled = 
				AddressStateRequired.Enabled = 
        AddressCountyRequired.Enabled = 
        AddressPostcodeZipRequired.Enabled = 
				AddressPostcodeZipTextBox.Enabled = 
        AddressCountryRequired.Enabled = 
        ContactPhoneRequired.Enabled = 
				IndustrialClassificationValidator.Enabled = 
        ContactUrlRegEx.Enabled =
				CompanyDescriptionLengthValidator.Enabled = ValidationEnabled;
		}

		/// <summary>
		/// Clears the values.
		/// </summary>
		private void ClearValues()
		{
			BusinessUnitNameTextBox.Text = "";
		  LegalNameTextBox.Text = "";
		  LegalNameTextBox.Enabled =
		    LegalNameRequired.Enabled = true;
			OwnershipTypeDropDownList.SelectedIndex = 0;

			IndustrialClassificationTextBox.Text = "";
			ContactUrlTextBox.Text = "";
			ContactPhoneTextBox.Text = "";
			ExtensionTextBox.Text = "";
			PhoneTypeDropDownList.SelectedIndex = 0;
			ContactAlternatePhoneTextBox.Text = "";
			AltPhoneTypeDropDownList.SelectedIndex = 0;
			ContactAlternate2PhoneTextBox.Text = "";
			AltPhoneType2DropDownList.SelectedIndex = 0;
		  NoOfEmployeesDropDownList.SelectedIndex = 0;

			// Employer Address
			AddressLine1TextBox.Text = "";
			AddressLine2TextBox.Text = "";
			AddressTownCityTextBox.Text = "";

			AddressStateDropDownList.SelectedIndex = 0;

			AddressCountyDropDownListCascadingDropDown.PromptText = CodeLocalise("Global.County.TopDefault", "- select county -");
			AddressCountyDropDownListCascadingDropDown.LoadingText = CodeLocalise("Global.County.Progress", "[Loading counties ...]");
			AddressCountyDropDownListCascadingDropDown.PromptValue = string.Empty;

			AddressCountyDropDownList.SelectedIndex = 0;
			AddressCountyDropDownListCascadingDropDown.SelectedValue = "";

			AddressPostcodeZipTextBox.Text = "";

			AddressCountryDropDownList.SelectValue(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries).Where(x => x.Key == "Country.US").Select(x => x.Id).SingleOrDefault().ToString());

			CompanyDescriptionTextBox.Text = "";
		}

		/// <summary>
		/// Sets the initial values.
		/// </summary>
		private void SetInitialValues()
		{
			BusinessUnitNameTextBox.Text = (_model.BusinessUnit.Name.IsNotNullOrEmpty() ? _model.BusinessUnit.Name : "");
      LegalNameTextBox.Text = _model.BusinessUnit.LegalName.IsNotNullOrEmpty() ? _model.BusinessUnit.LegalName : "";
		  LegalNameTextBox.Enabled =
        LegalNameRequired.Enabled = App.Settings.Module == FocusModules.Assist || App.User.IsShadowingUser || (_model.BusinessUnit.Id.IsNullOrZero() && _model.BusinessUnit.LegalName.IsNullOrEmpty());
			OwnershipTypeDropDownList.SelectValue(_model.BusinessUnit.OwnershipTypeId.IsNotNull() ? _model.BusinessUnit.OwnershipTypeId.ToString() : null);
			AccountTypeDropDownList.SelectValue(_model.BusinessUnit.AccountTypeId.IsNotNull() 
																				? _model.BusinessUnit.AccountTypeId.ToString() 
																				: null);
			IndustrialClassificationTextBox.Text = _model.BusinessUnit.IndustrialClassification;
			ContactUrlTextBox.Text = _model.BusinessUnit.Url;
			ContactPhoneTextBox.Text = _model.BusinessUnit.PrimaryPhone;
			ExtensionTextBox.Text = _model.BusinessUnit.PrimaryPhoneExtension;
			PhoneTypeDropDownList.SelectedValue = _model.BusinessUnit.PrimaryPhoneType;
			ContactAlternatePhoneTextBox.Text = _model.BusinessUnit.AlternatePhone1;
			AltPhoneTypeDropDownList.SelectedValue = _model.BusinessUnit.AlternatePhone1Type;
			ContactAlternate2PhoneTextBox.Text = _model.BusinessUnit.AlternatePhone2;
			AltPhoneType2DropDownList.SelectedValue = _model.BusinessUnit.AlternatePhone2Type;
      NoOfEmployeesDropDownList.SelectValue(_model.BusinessUnit.NoOfEmployees.IsNotNull() ? _model.BusinessUnit.NoOfEmployees.ToString() : null);

			// Address
			AddressLine1TextBox.Text = _model.BusinessUnitAddress.Line1;
			AddressLine2TextBox.Text = _model.BusinessUnitAddress.Line2;
			AddressTownCityTextBox.Text = _model.BusinessUnitAddress.TownCity;

			AddressStateDropDownList.SelectValue(_model.BusinessUnitAddress.StateId.IsNotNull() ? _model.BusinessUnitAddress.StateId.ToString() : ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).SingleOrDefault().ToString());
			
			AddressCountyDropDownListCascadingDropDown.PromptText = CodeLocalise("Global.County.TopDefault", "- select county -");
			AddressCountyDropDownListCascadingDropDown.LoadingText = CodeLocalise("Global.County.Progress", "[Loading counties ...]");
			AddressCountyDropDownListCascadingDropDown.PromptValue = string.Empty;

			var countySelectedValue = _model.BusinessUnitAddress.CountyId.IsNotNull() ? _model.BusinessUnitAddress.CountyId.ToString() : "";
			AddressCountyDropDownList.SelectValue(countySelectedValue);
			AddressCountyDropDownListCascadingDropDown.SelectedValue = countySelectedValue;

			AddressPostcodeZipTextBox.Text = _model.BusinessUnitAddress.PostcodeZip;

			AddressCountryDropDownList.SelectValue(_model.BusinessUnitAddress.CountryId.IsNotNull() ? _model.BusinessUnitAddress.CountryId.ToString() : ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries).Where(x => x.Key == "Country.US").Select(x => x.Id).SingleOrDefault().ToString());

			// Company Description
      if (_model.BusinessUnitDescription.IsNotNull())
			  CompanyDescriptionTextBox.Text = _model.BusinessUnitDescription.Description;
		}
	}
}