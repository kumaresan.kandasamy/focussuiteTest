<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobWizardStep3Path2Part2.ascx.cs" Inherits="Focus.Web.Code.Controls.User.JobWizardStep3Path2Part2" %>

<%@ Register src="~/Code/Controls/User/JobWizardKeywordsStatements.ascx" tagname="JobWizardKeywordsStatements" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobWizardSimilarJobs.ascx" tagname="JobWizardSimilarJobs" tagprefix="uc" %>

<table style="width:100%;" role="presentation">
  <tr>
    <td style="width:70%;"></td>
    <td style="width:4%;"></td>
    <td style="width:26%;"></td>
  </tr>
	<tr>
		<td colspan="2"><h1><asp:Label ID="JobTitleLabel" runat="server" /></h1></td>
    <td style="text-align: right">
      <asp:Button runat="server" ID="ResetDescriptionButton" CssClass="button3" OnClick="ResetDescriptionButton_OnClick" />
    </td>
	</tr>
	<tr>
		<td style="vertical-align:top;">
      <ul class="navTab localNavTab">
          <li id="TabDescription" class="active"><span id="TabDescriptionLink"><asp:Literal runat="server" ID="TabDescriptionLabel"></asp:Literal></span></li>
          <li id="TabSimilar"><span id="TabSimilarLink"><asp:Literal runat="server" ID="TabSimilarLabel"></asp:Literal></span></li>
      </ul>
      <div id="tabbedContent">
				<div id="JobDescriptionTab">					
					<asp:RequiredFieldValidator ID="JobDescriptionRequired" runat="server" ControlToValidate="JobDescriptionTextBox" SetFocusOnError="true" CssClass="error"  Display="Dynamic"/>
					<asp:RegularExpressionValidator ID="JobDescriptionValidator" runat="server" ControlToValidate="JobDescriptionTextBox" ValidationExpression="^(?:\S|[^\S\r\n]|\r?\n){0,6000}$" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
					<asp:CustomValidator runat="server" ID="JobDecriptionHtmlValidator" ControlToValidate="JobDescriptionTextBox" CssClass="error" SetFocusOnError="true" Display="Dynamic" ClientValidationFunction="JobWizardStep3Path2Part2_CheckHTMLTag"></asp:CustomValidator>
					<asp:Literal runat="server" ID="JobDescriptionInfieldLabel"></asp:Literal>
					<asp:TextBox ID="JobDescriptionTextBox" runat="server" TextMode="MultiLine" Rows="25" ClientIDMode="Static" />
					<br />
				</div>
				<div id="SimilarJobsTab">
					<uc:JobWizardSimilarJobs ID="SimilarJobs" runat="server" TargetControlId="JobDescriptionTextBox" OnAddEventJavascriptMethod="FocusOnJobDescription()" />
				</div>
			</div>
		</td>
		<td></td>
		<td style="vertical-align:top;">
			<br />
			<br />
			<br />
			<div id="KeywordsStatementsSection" class="opacity">
				<div id="KeywordsStatementsOverlay" class="overlay"></div>
				<uc:JobWizardKeywordsStatements ID="KeywordsStatements" runat="server" TargetControlId="JobDescriptionTextBox" DefaultOpenSection="Statements" SectionHeight="240px" />
			</div>	
		</td>
	</tr>
</table>

<script type="text/javascript">
	$(document).ready(function ()
	{
		$("#JobDescriptionTextBox").keyup(HideOrShowKeywordsStatements);
		HideOrShowKeywordsStatements();
		$("#SimilarJobsTab").hide();

		$("#TabDescriptionLink").click(ShowJobDescriptionTab);
		$("#TabSimilarLink").click(ShowSimilarJobsTab);

		$('.inFieldLabel > label, .inFieldLabelAlt > label').inFieldLabels();
	});

	function ShowJobDescriptionTab()
	{
		$("#TabDescription").addClass("active");
		$("#TabSimilar").removeClass("active");
		$("#JobDescriptionTab").show();
		$("#SimilarJobsTab").hide();
		HideOrShowOnTabSelect(true);
	}

	function ShowSimilarJobsTab()
	{
		$("#TabDescription").removeClass("active");
		$("#TabSimilar").addClass("active");
		$("#JobDescriptionTab").hide();
		$("#SimilarJobsTab").show();
		HideOrShowOnTabSelect(false);
	}

	function HideOrShowKeywordsStatements()
	{
		if ($("#JobDescriptionTextBox").val().length > 0)
		{
			$("#KeywordsStatementsSection").removeClass("opacity");
			$("#KeywordsStatementsOverlay").removeClass("overlay");
		}
		else
		{
			$("#KeywordsStatementsSection").addClass("opacity");
			$("#KeywordsStatementsOverlay").addClass("overlay");
		}
	}

	function HideOrShowOnTabSelect(show)
	{
		if (show)
			HideOrShowKeywordsStatements();
		else
		{
			$("#KeywordsStatementsSection").addClass("opacity");
			$("#KeywordsStatementsOverlay").addClass("overlay");
		}
	}

	function FocusOnJobDescription()
	{
		HideOrShowKeywordsStatements();
		ShowJobDescriptionTab();
	}

	function JobWizardStep3Path2Part2_CheckHTMLTag(sender, args) {
		var patt = new RegExp("<[A-Za-z]");
		args.IsValid = !patt.test(args.Value);
	}
</script>

