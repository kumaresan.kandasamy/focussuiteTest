﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Focus.Common.Controls.Server;
using Focus.Common.Extensions;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class Pager : UserControlBase
	{
		#region Private Properties

		private const string CssClassName = "CssClass";

		#endregion

		#region Public Properties

		public string PagedControlId
		{
			get { return MainDataPager.PagedControlID; }
			set { MainDataPager.PagedControlID = value; }
		}

		public int PageSize
		{
			get { return MainDataPager.PageSize; }
			set { MainDataPager.PageSize = value; }
		}

    public DropDownList PageSizerControl
    {
      get { return PageSizer; }
    }

		public int CurrentPage
		{
			get
			{
				var page = 1;
				if (MainDataPager.Controls.Count > 0)
				{
					var ddl = (DropDownList)MainDataPager.Controls[0].FindControl("PageJumper");
					page = ddl.SelectedValue != "" ? Convert.ToInt32(ddl.SelectedValue) : page;
				}
				return page;
			}
		}

		public string CssClass
		{
			get { return (ViewState[CssClassName] == null) ? "pager-table" : (string)ViewState[CssClassName]; }
			set { ViewState[CssClassName] = value; }
		}

		public int TotalRowCount
		{
			get { return MainDataPager.TotalRowCount; }
		}

		public bool DisplayRecordCount { get; set; }

		#endregion

		#region Methods

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack && PageSize != 0)
				PageSizer.SelectedValue = PageSize.ToString(CultureInfo.CurrentUICulture);

			if (!IsPostBack && Page.RouteData.Values["page"] != null)
			{
				int page;

				if (int.TryParse(Page.RouteData.Values["page"].ToString(), out page))
				{
					var startRowIndex = (page - 1) * MainDataPager.PageSize;
					MainDataPager.SetPageProperties(startRowIndex, MainDataPager.PageSize, true);
				}
			}

		}

    /// <summary>
        /// Handles the PreRender event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_PreRender(object sender, EventArgs e)
        {
            if (MainDataPager.Controls.Count > 0)
            {
                var previousButton = (LinkButton)MainDataPager.Controls[0].FindControl("PreviousButton");
                if (previousButton != null && previousButton.Enabled == false)
                    previousButton.Attributes["href"] = "#";

                var nextButton = (LinkButton)MainDataPager.Controls[0].FindControl("NextButton");
                if (nextButton != null && nextButton.Enabled == false)
                    nextButton.Attributes["href"] = "#";
            }
        }

		/// <summary>
		/// Handles the OnPagerCommand event of the MainDataPager control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.DataPagerCommandEventArgs"/> instance containing the event data.</param>
		protected void MainDataPager_OnPagerCommand(object sender, DataPagerCommandEventArgs e)
		{
			var pager = e.Item.Pager;

			// Check which button raised the event
			switch (e.CommandName)
			{
				case "FirstPage":
					e.NewStartRowIndex = 0;
					e.NewMaximumRows = pager.MaximumRows;
				break;

				case "Next":
					var newIndex = pager.StartRowIndex + pager.PageSize;
					if (newIndex <= e.TotalRowCount)
					{
						e.NewStartRowIndex = newIndex;
						e.NewMaximumRows = pager.MaximumRows;
					}
				break;

				case "Previous":
					e.NewStartRowIndex = pager.StartRowIndex - pager.PageSize;
					e.NewMaximumRows = pager.MaximumRows;
				break;
			}

			OnDataPaged(new DataPagedEventArgs());

		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the PageSizer control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void PageSizer_SelectedIndexChanged(object sender, EventArgs e)
		{
			var ddlPageSizer = (DropDownList)sender;
			var pageSize = Convert.ToInt32(ddlPageSizer.SelectedValue);

			MainDataPager.SetPageProperties(0, pageSize, true);
			OnDataPaged(new DataPagedEventArgs());
		}

    /// <summary>
    /// Handles the PreRender event of the PageJumper control.
    /// Used to build the page jumper ddl
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void PageJumper_PreRender(object sender, EventArgs e)
    {
      var currentPage = (MainDataPager.StartRowIndex / MainDataPager.PageSize) + 1;
      var totalPages = (int)Math.Ceiling((double)MainDataPager.TotalRowCount / MainDataPager.PageSize);

      // Populate the DropDownList if needed
      var ddl = (DropDownList)sender;
      ddl.Items.Clear();

	    var pageJumperLabel = (LocalisedLabel)MainDataPager.Controls[0].FindControl("PageJumperLabel");

      // Add a list item for each page
      if (totalPages == 0) totalPages = 1;

      // If we are requesting a page above the total number of pages take them back to the last page
      if (currentPage > totalPages)
        currentPage = totalPages;

      if (totalPages <= App.Settings.PagerDropdownLimit)
      {
        pageJumperLabel.Visible = ddl.Visible = true;
        for (var i = 1; i <= totalPages; i++)
          ddl.Items.Add(i.ToString(CultureInfo.CurrentUICulture));
      }
      else
      {
				pageJumperLabel.Visible = ddl.Visible = false;
        ddl.Items.Add(currentPage.ToString(CultureInfo.CurrentUICulture));
      }

      ddl.Items.FindByValue(currentPage.ToString(CultureInfo.CurrentUICulture)).Selected = true;

      RecordCount.Text = (DisplayRecordCount ? CodeLocalise("ResultCount.Text", "{0} results found", TotalRowCount.ToString(CultureInfo.CurrentUICulture)) : "&nbsp;&nbsp;&nbsp;&nbsp;");
    }

    /// <summary>
    /// Handles the PreRender event of the PageTextBox control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void PageNumberTextBox_OnPreRender(object sender, EventArgs e)
    {
      var currentPage = (MainDataPager.StartRowIndex / MainDataPager.PageSize) + 1;
      var totalPages = (int)Math.Ceiling((double)MainDataPager.TotalRowCount / MainDataPager.PageSize);

      if (totalPages == 0)
        totalPages = 1;

      if (currentPage > totalPages)
        currentPage = totalPages;

			var pageNoTextBoxLabel = (LocalisedLabel)MainDataPager.Controls[0].FindControl("PageNumberTextBoxLabel");

      var textBox = (TextBox)sender;
      if (totalPages > App.Settings.PagerDropdownLimit)
      {
        var maxChars = totalPages.ToString(CultureInfo.InvariantCulture).Length;
				pageNoTextBoxLabel.Visible = textBox.Visible = true;
        textBox.Width = Unit.Pixel(maxChars * 8);
        textBox.MaxLength = maxChars;
      }
      else
      {
				pageNoTextBoxLabel.Visible =  textBox.Visible = false;
      }
      textBox.Text = currentPage.ToString(CultureInfo.InvariantCulture);
    }

		/// <summary>
		/// Handles the SelectedIndexChanged event of the PageJumper control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void PageJumper_SelectedIndexChanged(object sender, EventArgs e)
		{
			var ddlPageJumper = (DropDownList)sender;
			var pageNo = Convert.ToInt32(ddlPageJumper.SelectedValue);
			var startRowIndex = (pageNo - 1) * MainDataPager.PageSize;

			MainDataPager.SetPageProperties(startRowIndex, MainDataPager.PageSize, true);
			OnDataPaged(new DataPagedEventArgs());
		}

    /// <summary>
    /// Handles the OnTextChanged event of the PageNumberTextBox control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void PageNumberTextBox_OnTextChanged(object sender, EventArgs e)
    {
      var pageBox = (TextBox)sender;
      var pageNo = pageBox.Text.AsInt32();

      var totalPages = (int)Math.Ceiling((double)MainDataPager.TotalRowCount / MainDataPager.PageSize);
      if (pageNo < 1)
        pageNo = 1;
      else if (pageNo > totalPages)
        pageNo = totalPages;

      var startRowIndex = (pageNo - 1) * MainDataPager.PageSize;

      MainDataPager.SetPageProperties(startRowIndex, MainDataPager.PageSize, true);
      OnDataPaged(new DataPagedEventArgs());
    }

		/// <summary>
		/// Returns to first page.
		/// </summary>
		public void ReturnToFirstPage()
		{
			MainDataPager.SetPageProperties(0, MainDataPager.PageSize, true);
			OnDataPaged(new DataPagedEventArgs());
		}

    /// <summary>
    /// Returns to page.
    /// </summary>
    /// <param name="pageno">The pageno.</param>
    /// <param name="pagesize">The pagesize.</param>
    public void ReturnToPage(int pageno = 0, int pagesize = 0)
    {
      pageno = pageno * pagesize;
      MainDataPager.SetPageProperties(pageno, pagesize, true);
      PageSizer.SelectedValue = pagesize.ToString();
    }

		#endregion

		#region Events

		/// <summary>
		/// Occurs when [data paged].
		/// </summary>
		public event DataPagedHandler DataPaged;

		/// <summary>
		/// Raises the <see cref="E:DataPaged"/> event.
		/// </summary>
		/// <param name="e">The <see cref="Focus.Web.Code.Controls.User.Pager.DataPagedEventArgs"/> instance containing the event data.</param>
		protected virtual void OnDataPaged(DataPagedEventArgs e)
		{
			if (DataPaged != null)
				DataPaged(this, e);
		}

		#endregion

		#region Delegates

		public delegate void DataPagedHandler(object o, DataPagedEventArgs e);
		
		#endregion

		#region EventArgs

		/// <summary>
		/// 
		/// </summary>
		public class DataPagedEventArgs : EventArgs
		{

		}

		#endregion
	}
}