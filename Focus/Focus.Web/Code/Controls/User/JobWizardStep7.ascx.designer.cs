//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Focus.Web.Code.Controls.User {
    
    
    public partial class JobWizardStep7 {
        
        /// <summary>
        /// ExpandCollapseAllRow control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow ExpandCollapseAllRow;
        
        /// <summary>
        /// ExpandCollapseAllButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ExpandCollapseAllButton;
        
        /// <summary>
        /// JobContactHeaderPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel JobContactHeaderPanel;
        
        /// <summary>
        /// JobContactHeaderImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image JobContactHeaderImage;
        
        /// <summary>
        /// JobContactPanelWrapper control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl JobContactPanelWrapper;
        
        /// <summary>
        /// JobContactPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel JobContactPanel;
        
        /// <summary>
        /// JobContactNameLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal JobContactNameLiteral;
        
        /// <summary>
        /// JobContactAddressLine1Literal control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal JobContactAddressLine1Literal;
        
        /// <summary>
        /// JobContactAddressLine2Literal control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal JobContactAddressLine2Literal;
        
        /// <summary>
        /// JobContactCityLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal JobContactCityLiteral;
        
        /// <summary>
        /// JobContactStateLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal JobContactStateLiteral;
        
        /// <summary>
        /// JobContactZipLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal JobContactZipLiteral;
        
        /// <summary>
        /// JobContactTelephoneLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal JobContactTelephoneLiteral;
        
        /// <summary>
        /// JobContactEmailAddressLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal JobContactEmailAddressLiteral;
        
        /// <summary>
        /// JobContactPanelExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.CollapsiblePanelExtender JobContactPanelExtender;
        
        /// <summary>
        /// ClosingDateHeaderPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ClosingDateHeaderPanel;
        
        /// <summary>
        /// ClosingDateHeaderImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image ClosingDateHeaderImage;
        
        /// <summary>
        /// ClosingDatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ClosingDatePanel;
        
        /// <summary>
        /// ClosingDateTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ClosingDateTextBox;
        
        /// <summary>
        /// AdvertiseFor30DaysPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder AdvertiseFor30DaysPlaceHolder;
        
        /// <summary>
        /// AdvertiseFor30DaysText control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label AdvertiseFor30DaysText;
        
        /// <summary>
        /// ClosingDateValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xVal.WebForms.ModelPropertyValidator ClosingDateValidator;
        
        /// <summary>
        /// ClosingDateIsValidCompareValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CompareValidator ClosingDateIsValidCompareValidator;
        
        /// <summary>
        /// ClosingDateCompareValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CompareValidator ClosingDateCompareValidator;
        
        /// <summary>
        /// ClosingDateGreaterThanTodayCompareValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CompareValidator ClosingDateGreaterThanTodayCompareValidator;
        
        /// <summary>
        /// ClosingDateCustomValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator ClosingDateCustomValidator;
        
        /// <summary>
        /// ClosingDateCalendarExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.CalendarExtender ClosingDateCalendarExtender;
        
        /// <summary>
        /// ClosingDatePanelExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.CollapsiblePanelExtender ClosingDatePanelExtender;
        
        /// <summary>
        /// NumberOfOpeningsHeaderPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel NumberOfOpeningsHeaderPanel;
        
        /// <summary>
        /// NumberOfOpeningsHeaderImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image NumberOfOpeningsHeaderImage;
        
        /// <summary>
        /// NumberOfOpeningsPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel NumberOfOpeningsPanel;
        
        /// <summary>
        /// NumberOfOpeningsTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox NumberOfOpeningsTextBox;
        
        /// <summary>
        /// NumberOfOpeningsRequired control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator NumberOfOpeningsRequired;
        
        /// <summary>
        /// MinimumNumberOfOpeningsValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CompareValidator MinimumNumberOfOpeningsValidator;
        
        /// <summary>
        /// MaximumNumberOfOpeningsValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CompareValidator MaximumNumberOfOpeningsValidator;
        
        /// <summary>
        /// NumberOfOpeningsPanelExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.CollapsiblePanelExtender NumberOfOpeningsPanelExtender;
        
        /// <summary>
        /// InterviewContactPreferencesHeaderPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel InterviewContactPreferencesHeaderPanel;
        
        /// <summary>
        /// InterviewContactPreferencesHeaderImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image InterviewContactPreferencesHeaderImage;
        
        /// <summary>
        /// InterviewContactPreferencesPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel InterviewContactPreferencesPanel;
        
        /// <summary>
        /// ReceiveApplicationsLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel ReceiveApplicationsLabel;
        
        /// <summary>
        /// InterviewContactPreferencesValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.CheckBoxCustomValidator InterviewContactPreferencesValidator;
        
        /// <summary>
        /// FocusTalentAccountCheckbox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox FocusTalentAccountCheckbox;
        
        /// <summary>
        /// EmailResumeCheckBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox EmailResumeCheckBox;
        
        /// <summary>
        /// EmailAddressTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox EmailAddressTextBox;
        
        /// <summary>
        /// EmailAddressValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator EmailAddressValidator;
        
        /// <summary>
        /// EmailAddressRegEx control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RegularExpressionValidator EmailAddressRegEx;
        
        /// <summary>
        /// ApplyOnlineCheckBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox ApplyOnlineCheckBox;
        
        /// <summary>
        /// ApplyOnlineTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ApplyOnlineTextBox;
        
        /// <summary>
        /// ApplyOnlineValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator ApplyOnlineValidator;
        
        /// <summary>
        /// MailResumeCheckBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox MailResumeCheckBox;
        
        /// <summary>
        /// MailResumeTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox MailResumeTextBox;
        
        /// <summary>
        /// MailResumeValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator MailResumeValidator;
        
        /// <summary>
        /// FaxResumeCheckBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox FaxResumeCheckBox;
        
        /// <summary>
        /// FaxResumeTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox FaxResumeTextBox;
        
        /// <summary>
        /// FaxResumeRegEx control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RegularExpressionValidator FaxResumeRegEx;
        
        /// <summary>
        /// FaxResumeMaskedEdit control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.MaskedEditExtender FaxResumeMaskedEdit;
        
        /// <summary>
        /// PhoneForAppointmentCheckBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox PhoneForAppointmentCheckBox;
        
        /// <summary>
        /// PhoneForAppointmentTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox PhoneForAppointmentTextBox;
        
        /// <summary>
        /// PhoneForAppointmentMaskedEdit control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.MaskedEditExtender PhoneForAppointmentMaskedEdit;
        
        /// <summary>
        /// PhoneForAppointmentRegEx control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RegularExpressionValidator PhoneForAppointmentRegEx;
        
        /// <summary>
        /// InPersonCheckBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox InPersonCheckBox;
        
        /// <summary>
        /// InPersonTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox InPersonTextBox;
        
        /// <summary>
        /// InPersonLengthValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RegularExpressionValidator InPersonLengthValidator;
        
        /// <summary>
        /// InPersonValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator InPersonValidator;
        
        /// <summary>
        /// OtherInstructionsTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox OtherInstructionsTextBox;
        
        /// <summary>
        /// InterviewContactPreferencesPanelExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.CollapsiblePanelExtender InterviewContactPreferencesPanelExtender;
        
        /// <summary>
        /// ScreeningPreferencesHeaderPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ScreeningPreferencesHeaderPanel;
        
        /// <summary>
        /// ScreeningPreferencesHeaderImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image ScreeningPreferencesHeaderImage;
        
        /// <summary>
        /// ScreeningPreferencesPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ScreeningPreferencesPanel;
        
        /// <summary>
        /// AllowUnqualifiedApplicationsRow control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow AllowUnqualifiedApplicationsRow;
        
        /// <summary>
        /// AllowUnqualifiedApplicationsRadioButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton AllowUnqualifiedApplicationsRadioButton;
        
        /// <summary>
        /// JobSeekersMustHaveMinimumStarMatchRow control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow JobSeekersMustHaveMinimumStarMatchRow;
        
        /// <summary>
        /// JobSeekersMustHaveMinimumStarMatchRadioButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton JobSeekersMustHaveMinimumStarMatchRadioButton;
        
        /// <summary>
        /// ScreeningPreferencesMinimumStarsDropDown control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ScreeningPreferencesMinimumStarsDropDown;
        
        /// <summary>
        /// JobSeekersMinimumStarsToApplyRow control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow JobSeekersMinimumStarsToApplyRow;
        
        /// <summary>
        /// JobSeekersMinimumStarsToApplyRadioButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton JobSeekersMinimumStarsToApplyRadioButton;
        
        /// <summary>
        /// ScreeningPreferencesMinimumStarsToApplyDropDown control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ScreeningPreferencesMinimumStarsToApplyDropDown;
        
        /// <summary>
        /// JobSeekersMustBeScreenedRadioButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton JobSeekersMustBeScreenedRadioButton;
        
        /// <summary>
        /// PreScreeningServiceRequestPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder PreScreeningServiceRequestPanel;
        
        /// <summary>
        /// PreScreeningServiceRequestHeader control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal PreScreeningServiceRequestHeader;
        
        /// <summary>
        /// PreScreeningServiceRequestRadioButtons control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList PreScreeningServiceRequestRadioButtons;
        
        /// <summary>
        /// PreScreeningServiceRequestValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator PreScreeningServiceRequestValidator;
        
        /// <summary>
        /// ScreeningPreferencesPanelExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.CollapsiblePanelExtender ScreeningPreferencesPanelExtender;
        
        /// <summary>
        /// VeteranPriorityPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder VeteranPriorityPlaceHolder;
        
        /// <summary>
        /// VeteranPriorityOfServiceHeaderPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel VeteranPriorityOfServiceHeaderPanel;
        
        /// <summary>
        /// VeteranPriorityOfServiceHeaderImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image VeteranPriorityOfServiceHeaderImage;
        
        /// <summary>
        /// VeteranPriorityOfServicePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel VeteranPriorityOfServicePanel;
        
        /// <summary>
        /// VeteranPriorityComplianceLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel VeteranPriorityComplianceLabel;
        
        /// <summary>
        /// VeteranPriorityExlusivityLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel VeteranPriorityExlusivityLabel;
        
        /// <summary>
        /// ExtendPriorityOfServiceCheckBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox ExtendPriorityOfServiceCheckBox;
        
        /// <summary>
        /// ExtendPriorityOfServiceUntilRadioButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton ExtendPriorityOfServiceUntilRadioButton;
        
        /// <summary>
        /// VeteranPriorityExtendUntilDateTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox VeteranPriorityExtendUntilDateTextBox;
        
        /// <summary>
        /// VeteranPriorityExtendUntilDateCalendarExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.CalendarExtender VeteranPriorityExtendUntilDateCalendarExtender;
        
        /// <summary>
        /// ExtendVeteranDateValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator ExtendVeteranDateValidator;
        
        /// <summary>
        /// ExtendPriorityOfServiceForLifeOfPostingRadioButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton ExtendPriorityOfServiceForLifeOfPostingRadioButton;
        
        /// <summary>
        /// ExtendPriorityOfServiceValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.CheckBoxCustomValidator ExtendPriorityOfServiceValidator;
        
        /// <summary>
        /// VeteranPriorityOfServicePanelExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.CollapsiblePanelExtender VeteranPriorityOfServicePanelExtender;
    }
}
