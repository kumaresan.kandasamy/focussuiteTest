#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class SaveSearchActionModal : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				NameRequired.ErrorMessage = CodeLocalise("Name.RequiredError", "Search name required");
				EmailAddressRequired.ErrorMessage = CodeLocalise("EmailAddress.RequiredError", "Email address required");
				EmailAddressRegEx.ErrorMessage = CodeLocalise("EmailAddress.RegexError", "Invalid email address");
			}
		}

		public delegate void CompletedHandler(object sender, EventArgs eventArgs);
		public event CompletedHandler Completed;

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value>The title.</value>
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the search criteria.
		/// </summary>
		/// <value>The search criteria.</value>
		protected CandidateSearchCriteria SearchCriteria
		{
			get { return GetViewStateValue<CandidateSearchCriteria>("SaveSearchActionModal:SearchCriteria"); }
			set { SetViewStateValue("SaveSearchActionModal:SearchCriteria", value); }
		}

		/// <summary>
		/// Gets or sets the saved search id.
		/// </summary>
		/// <value>The saved search id.</value>
		private long SavedSearchId
		{
			get { return GetViewStateValue<long>("SaveSearchActionModal:SavedSearchId"); }
			set { SetViewStateValue("SaveSearchActionModal:SavedSearchId", value); }
		}

		/// <summary>
		/// Shows the specified saved search id.
		/// </summary>
		/// <param name="savedSearchId">The saved search id.</param>
		/// <param name="searchCriteria">The search criteria.</param>
		public void Show(long savedSearchId, CandidateSearchCriteria searchCriteria = null)
		{
			SavedSearchId = savedSearchId;
            UserIDHidden.Value = App.User.UserId.ToString(); //FVN-6966 Talent search AIP
			// Get saved search from the database
			var savedSearchView = ServiceClientLocator.SearchClient(App).GetCandidateSavedSearch(savedSearchId);
			if (savedSearchView.IsNotNull())
			{
        if (searchCriteria.IsNotNull())
        {
          SearchCriteria = searchCriteria;
        }
        else
        {
          SearchCriteria = savedSearchView.Criteria;
        }

			  if (savedSearchView.SavedSearch.IsNotNull())
				{
					if (savedSearchView.SavedSearch.Name.IsNotNullOrEmpty()) NameTextBox.Text = savedSearchView.SavedSearch.Name;
					if (savedSearchView.SavedSearch.AlertEmailRequired.IsNotNull()) AlertMeCheckBox.Checked = savedSearchView.SavedSearch.AlertEmailRequired;
					if (savedSearchView.SavedSearch.AlertEmailFrequency.IsNotNull()) WeeklyAlertFrequencyRadioButton.Checked = (savedSearchView.SavedSearch.AlertEmailFrequency == EmailAlertFrequencies.Weekly);
					if (savedSearchView.SavedSearch.AlertEmailFormat.IsNotNull()) TextOnlyEmailFormatRadioButton.Checked = (savedSearchView.SavedSearch.AlertEmailFormat == EmailFormats.TextOnly);
					EmailAddressTextBox.Text = (savedSearchView.SavedSearch.AlertEmailAddress.IsNotNullOrEmpty()) ? savedSearchView.SavedSearch.AlertEmailAddress : App.User.EmailAddress;
				}
			}

			InitialiseControl();
			OnCompleted(new EventArgs());
		}

		/// <summary>
		/// Shows the specified action type.
		/// </summary>
		/// <param name="searchCriteria">The search criteria.</param>
		public void Show(CandidateSearchCriteria searchCriteria)
		{
			SearchCriteria = searchCriteria;
            UserIDHidden.Value = App.User.UserId.ToString();
			InitialiseControl();
		  NameTextBox.Text = "";
			EmailAddressTextBox.Text = App.User.EmailAddress;
			
			OnCompleted(new EventArgs());
		}

		/// <summary>
		/// Handles the Click event of the OkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void OkButton_Click(object sender, EventArgs e)
		{
            /** 31.May.2017 - Check whether userid has changed i.e. different user has logged in any other tabs
             */
            if (UserIDHidden.Value == App.User.UserId.ToString() || App.Settings.Module == FocusModules.Assist)
		    {
                var name = NameTextBox.TextTrimmed();
                ServiceClientLocator.SearchClient(App).SaveCandidateSearch(SearchCriteria, SavedSearchId, name, AlertMeCheckBox.Checked,
                                                                                                    (DailyAlertFrequencyRadioButton.Checked) ? EmailAlertFrequencies.Daily : EmailAlertFrequencies.Weekly,
                                                                                                    (HTMLEmailFormatRadioButton.Checked) ? EmailFormats.HTML : EmailFormats.TextOnly,
                                                                                                    EmailAddressTextBox.TextTrimmed());

                Confirmation.Show(CodeLocalise("Confirmation.Title", "Search saved"),
                                                    CodeLocalise("Confirmation.Details", "Confirmation that {0} search has been saved.", name),
                                                    CodeLocalise("Global.Close.Text", "Close"));
                OnCompleted(new EventArgs());
                SaveSearchActionModalPopup.Hide();
            }
            else
            {
                OnCompleted(new EventArgs());
                SaveSearchActionModalPopup.Hide();
                Response.Redirect(UrlBuilder.TalentJobs());

            }


		}

		/// <summary>
		/// Raises the <see cref="E:Completed"/> event.
		/// </summary>
		/// <param name="eventArgs">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void OnCompleted(EventArgs eventArgs)
		{
			if (Completed != null)
				Completed(this, eventArgs);
		}

		/// <summary>
		/// Initialises the control.
		/// </summary>
		private void InitialiseControl()
		{
			EmailAddressRegEx.ValidationExpression = App.Settings.EmailAddressRegExPattern;
			Title = CodeLocalise("Title", "Save search & notify me of new talent");
			OkButton.Text = CodeLocalise("Ok.Text.NoEdit", "Save search");
			
			SaveSearchActionModalPopup.Show();
		}
	}
}
