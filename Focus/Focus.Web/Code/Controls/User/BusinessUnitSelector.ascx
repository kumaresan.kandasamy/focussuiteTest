﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BusinessUnitSelector.ascx.cs" Inherits="Focus.Web.Code.Controls.User.BusinessUnitSelector" %>

<%@ Register TagPrefix="uc" TagName="EditBusinessUnit" Src="~/Code/Controls/User/EditBusinessUnit.ascx" %>

<asp:HiddenField ID="BusinessUnitSelectorModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="BusinessUnitSelectorModalPopup" runat="server" BehaviorID="BusinessUnitSelectorModal"
												TargetControlID="BusinessUnitSelectorModalDummyTarget"
												PopupControlID="BusinessUnitSelectorModalPanel"
												PopupDragHandleControlID="BusinessUnitSelectorModalHeader"
												RepositionMode="RepositionOnWindowResizeAndScroll" 
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="BusinessUnitSelectorModalPanel" runat="server" CssClass="modal hidden" >
	<asp:Panel CssClass="modal-header" ID="BusinessUnitSelectorModalHeader" runat="server" style="height:16px;position:relative;top: -10px;">
		<div class="closeIcon" id="closeIcon" runat="server"><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('BusinessUnitSelectorModal').hide(); return false;" /></div>
	</asp:Panel>
	<asp:UpdatePanel ID="BusinessUnitSelectorUpdatePanel" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<asp:Panel ID="SelectorPanel" runat="server">	
				<table role="presentation">
					<tr>
						<td class="label" width="200px"><%= HtmlLabel("BusinessUnit.Label", "Select a #BUSINESS#:LOWER")%></td>
						<td><asp:DropDownList aria-label="Business Unit" ID="BusinessUnitDropDown" runat="server" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="BusinessUnitDropDown_SelectedIndexChanged" /><br /></td>
					</tr>
				</table>
			</asp:Panel>
			<asp:Panel ID="ExistingBusinessUnitPanel" runat="server" Visible="false">
				<table role="presentation">
          <asp:PlaceHolder runat="server" ID="LegalNamePlaceHolder">
					<tr>
						<td class="label" width="200px"><%= HtmlLabel("LegalName.Label", "Business legal name")%></td>
						<td><asp:label ID="LegalNameLabel" runat="server" /></td>
					</tr>
          </asp:PlaceHolder>				  
					<tr>
						<td class="label" width="200px"><%= HtmlLabel("NameRO.Label", "#BUSINESS# name")%></td>
						<td><asp:label ID="NameROLabel" runat="server" /></td>
					</tr>
					<tr>
						<td class="label"><%= HtmlLabel("AddressLine1RO.Label", "Address")%></td>
						<td><asp:label ID="AddressLine1ROLabel" runat="server" /></td>
					</tr>
					<tr>
						<td class="label"></td>
						<td><asp:label ID="AddressLine2ROLabel" runat="server" /></td>
					</tr>
					<tr>
						<td class="label"><%= HtmlLabel("AddressCityRO.Label", "City")%></td>
						<td><asp:label ID="AddressCityROLabel" runat="server" /></td>
					</tr>
					<tr>
						<td class="label"><%= HtmlLabel("AddressStateRO.Label", "State")%></td>
						<td><asp:label ID="AddressStateROLabel" runat="server" /></td>
					</tr>
					<tr>
						<td class="label"><%= HtmlLabel("AddressCountyRO.Label", "County")%></td>
						<td><asp:label ID="AddressCountyROLabel" runat="server" /></td>
					</tr>
					<tr>
						<td class="label"><%= HtmlLabel("AddressPostcodeRO.Label", "ZIP or postal code")%></td>
						<td><asp:label ID="AddressPostcodeROLabel" runat="server" /></td>
					</tr>
					<tr>
						<td class="label"><%= HtmlLabel("AddressCountryRO.Label", "Country")%></td>
						<td><asp:label ID="AddressCountryROLabel" runat="server" /></td>
					</tr>
					<tr>
						<td class="label"><%= HtmlLabel("Url.Label", "URL")%></td>
						<td><asp:label ID="UrlROLabel" runat="server" /></td>
					</tr>
					<tr>
						<td class="label"><%= HtmlLabel("PhoneNumber.Label", "Phone number")%></td>
						<td><asp:label ID="PhoneNumberROLabel" runat="server" /></td>
					</tr>
					<tr>
						<td class="label"><%= HtmlLabel("AlternatePhoneNumber1.Label", "Alternate phone number 1")%></td>
						<td><asp:label ID="AlternatePhoneNumber1ROLabel" runat="server" /></td>
					</tr>
					<tr>
						<td class="label"><%= HtmlLabel("AlternatePhoneNumber2.Label", "Alternate phone number 2")%></td>
						<td><asp:label ID="AlternatePhoneNumber2ROLabel" runat="server" /></td>
					</tr>
					<tr>
						<td class="label"><%= HtmlLabel("OwnershipType.Label", "Ownership type")%></td>
						<td><asp:label ID="OwnershipTypeROLabel" runat="server" /></td>
					</tr>
					<tr id="IndustrialClassificationRow" runat="server">
						<td class="label"><%= HtmlLabel("IndustrialClassification.Label", "Industrial classification")%></td>
						<td><asp:label ID="IndustrialClassificationROLabel" runat="server" /></td>
					</tr>
					<tr id="AccountTypeRow" runat="server">
						<td class="label"><%= HtmlLabel("AccountType.Label", "Account type")%></td>
						<td><asp:label ID="AccountTypeROLabel" runat="server" /></td>
					</tr>
					<tr>
						<td class="label"><%= HtmlLabel("CompanyDescription", "#BUSINESS# description")%></td>
						<td><asp:label ID="CompanyDescriptionLabel" runat="server" /></td>
					</tr>
				</table>
			</asp:Panel>
			<asp:Panel ID="NewBusinessUnitPanel" runat="server" CssClass="hidden">
				<uc:EditBusinessUnit runat="server" ID="BusinessUnitEditor" ValidationGroup="AddBusinessUnitSelector" />
			</asp:Panel>
			<div align="right">
				<asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" CausesValidation="false" />
				<asp:Button ID="SelectButton" runat="server" CssClass="button4" OnClick="SelectButton_Clicked" CausesValidation="false" />
				<asp:Button ID="AddButton" runat="server" CssClass="button4" OnClick="AddButton_Clicked" ValidationGroup="AddBusinessUnitSelector" />
			</div>
		</ContentTemplate>
		<Triggers>
			<asp:AsyncPostBackTrigger ControlID="BusinessUnitDropDown" EventName="SelectedIndexChanged" />
		</Triggers>
	</asp:UpdatePanel>
</asp:Panel>

