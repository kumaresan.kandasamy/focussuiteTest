﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Focus.Web.Code.Controls.User
{
	public partial class SecurityQuestionsModal : UserControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		/// <summary>
		/// Shows the modal
		/// </summary>
		/// <param name="showSaveAllButton">Whether to show the save all button (and let the control handle saving)</param>
		/// <param name="validationGroup">The validation group</param>
		public void Show(bool showSaveAllButton, string validationGroup = "SecurityQuestionModal")
		{
			SecurityQuestionsPanel.ValidationGroup = validationGroup;
			SecurityQuestionsPanel.Bind(showSaveAllButton);

			QuestionsModal.Show();
		}
	}
}