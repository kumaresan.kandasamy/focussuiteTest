﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserReport.ascx.cs" Inherits="Focus.Web.Code.Controls.User.Reporting.UserReport" ClassName="UserReport" %>
<div class="SavedReport">
  <asp:HyperLink ID="ReportLink" runat="server">
    <asp:Image ID="ReportImage" runat="server" Width="133" Height="72" CssClass="Thumbnail" AlternateText="Report Image" />
  </asp:HyperLink>
  <asp:ImageButton runat="server" ID="DeleteIcon" ImageUrl="<%# UrlBuilder.ButtonCloseIcon() %>" Width="16" Height="15" runat="server" CssClass="closeLogo" CommandName="Delete" OnCommand="DeleteIcon_OnCommand" />
  <div style="clear: both"></div>
  <span class="Date"><asp:Label ID="ReportDateLabel" runat="server" Text="Label"></asp:Label></span>
  <span class="Caption"><asp:Label ID="ReportCaptionLabel" runat="server"></asp:Label></span>
</div>