﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Focus.Common;

#endregion

namespace Focus.Web.Code.Controls.User.Reporting
{
  public partial class UserReport : UserControlBase
	{
    public delegate void ReportDeletedHandler(object sender, ReportDeletedEventArgs eventArgs);
    public event ReportDeletedHandler ReportDeletedEvent;

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
		{
			LocaliseUI();
		}

	  private void LocaliseUI()
	  {
		  DeleteIcon.AlternateText = CodeLocalise("Global.Delete", "Delete");
	  }

	  /// <summary>
		/// Sets the panel params.
		/// </summary>
		/// <param name="reportId">The report id.</param>
		/// <param name="reportName">Name of the report.</param>
		/// <param name="reportSaveDate">The report save date.</param>
		/// <param name="isGrid">if set to <c>true</c> [is grid].</param>
		/// <param name="showDelete">if set to <c>true</c> [show delete].</param>
    public void SetPanelParams(long reportId, string reportName, DateTime reportSaveDate, bool isGrid, bool showDelete)
    {
      ReportLink.NavigateUrl = UrlBuilder.ReportingSavedReport(reportId);

			ReportImage.ImageUrl = isGrid ? Page.ResolveUrl(UrlBuilder.SavedReportGridImage()) : Page.ResolveUrl(UrlBuilder.SavedReportChartImage());
		  
      DeleteIcon.CommandArgument = reportId.ToString(CultureInfo.InvariantCulture);
		  DeleteIcon.Visible = showDelete;

      ReportDateLabel.Text = reportSaveDate.Date.ToShortDateString();
      ReportCaptionLabel.Text = reportName;
      ReportImage.AlternateText = reportName;
    }

    protected void DeleteIcon_OnCommand(object sender, CommandEventArgs e)
    {
      var reportId = long.Parse(e.CommandArgument.ToString());
      ServiceClientLocator.ReportClient(App).DeleteReport(reportId);

      var eventArgs = new ReportDeletedEventArgs
      {
        ReportId = reportId
      };

      OnReportDeleted(eventArgs);
    }

    /// <summary>
    /// Raises the <see cref="ReportDeletedEventArgs"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected virtual void OnReportDeleted(ReportDeletedEventArgs eventArgs)
    {
      if (ReportDeletedEvent != null)
        ReportDeletedEvent(this, eventArgs);
    }
  }

  public class ReportDeletedEventArgs : EventArgs
  {
    public long ReportId { get; set; }
  }
}