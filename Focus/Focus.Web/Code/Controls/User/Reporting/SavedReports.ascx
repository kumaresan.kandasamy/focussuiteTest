﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SavedReports.ascx.cs" Inherits="Focus.Web.Code.Controls.User.Reporting.SavedReports" %>
<%@ Register src="UserReport.ascx" tagName="UserReport" tagPrefix="uc" %>

<h2 id="SavedReportsHeader" runat="server"><asp:Literal runat="server" ID="SavedReportsHeaderLiteral"/></h2>
<h3 id="ShortHeader" runat="server"><asp:Literal runat="server" ID="ShortHeaderLiteral"/></h3>
<asp:Panel runat="server" ID="pnlNoResults" Width="100%" HorizontalAlign="Center"><%=HtmlLocalise("Reporting.NoReportsFound", "No reports found")%></asp:Panel>

<asp:Repeater runat="server" ID="SavedReportRepeater" OnItemDataBound="SavedReportRepeater_OnItemDataBound">
  <ItemTemplate>
    <uc:UserReport runat="server" ID="UserReport" OnReportDeletedEvent="UserReport_OnReportDeletedEvent" />
  </ItemTemplate>
</asp:Repeater>