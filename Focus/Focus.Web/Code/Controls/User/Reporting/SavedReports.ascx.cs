﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.Report;

#endregion

namespace Focus.Web.Code.Controls.User.Reporting
{
  public partial class SavedReports : UserControlBase
	{
    public bool? DashboardReports { get; set; }
    public bool AllowDelete { get; set; }
    public bool ShowShortHeader { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();
        BindSavedReports();

        ShortHeader.Visible = ShowShortHeader;
        SavedReportsHeader.Visible = !ShowShortHeader;
      }
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      SavedReportsHeaderLiteral.Text = CodeLocalise("SavedReportsHeaderLiteral.Text", "Saved reports");
      ShortHeaderLiteral.Text = CodeLocalise("ShortHeaderLiteral.Text", "Reports");
    }

    /// <summary>
    /// Adds saved reports to the panel
    /// </summary>
    private void BindSavedReports()
    {
      var reports = ServiceClientLocator.ReportClient(App).GetSavedReportList(DashboardReports);

      var filteredReports = reports.AsQueryable();
      
      if (!App.User.IsInAnyRole(Constants.RoleKeys.AssistJobSeekerReports, Constants.RoleKeys.AssistJobSeekerReportsViewOnly))
        filteredReports = filteredReports.Where(report => report.ReportType != ReportType.JobSeeker);

      if (!App.User.IsInAnyRole(Constants.RoleKeys.AssistJobOrderReports, Constants.RoleKeys.AssistJobOrderReportsViewOnly))
        filteredReports = filteredReports.Where(report => report.ReportType != ReportType.JobOrder);

      if (!App.User.IsInAnyRole(Constants.RoleKeys.AssistEmployerReports, Constants.RoleKeys.AssistEmployerReportsViewOnly))
        filteredReports = filteredReports.Where(report => report.ReportType != ReportType.Employer);

      SavedReportRepeater.DataSource = filteredReports;
      SavedReportRepeater.DataBind();

      pnlNoResults.Visible = (reports.Count == 0);
    }

    /// <summary>
    /// Binds a user report icon to the repeated
    /// </summary>
    /// <param name="sender">Repeater control raising the event</param>
    /// <param name="e">Repeater event arguments</param>
    protected void SavedReportRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      var reportControl = (UserReport)e.Item.FindControl("UserReport");
      var savedReport = (SavedReportDto) e.Item.DataItem;

      reportControl.SetPanelParams(savedReport.Id.GetValueOrDefault(0), savedReport.Name, savedReport.ReportDate, savedReport.ReportDisplayType == ReportDisplayType.Table, AllowDelete);
    }

    /// <summary>
    /// Fires when a report has been deleted to re-bind the repeater
    /// </summary>
    /// <param name="sender">User Report control</param>
    /// <param name="eventargs">Report Deleted Arguments</param>
    protected void UserReport_OnReportDeletedEvent(object sender, ReportDeletedEventArgs eventargs)
    {
      BindSavedReports();
    }
  }
}