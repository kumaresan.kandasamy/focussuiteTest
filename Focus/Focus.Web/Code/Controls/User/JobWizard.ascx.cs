﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using Focus.Common;
using Focus.Core;
using Focus.Web.Controllers.Code.Controls.User;
using Focus.Web.ViewModels;

using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class JobWizard : UserControlBase
	{
		private const int FinalStep = 7;
		private JobWizardViewModel _model;
		private bool _isNewAddress = false;

		#region Properties

		public long JobId { get; set; }

		public long HiringManagerId { get; set; }

		public bool FromApproval { get; set; }

		/// <summary>
		/// Gets or sets the current step.
		/// </summary>
		/// <value>The current step.</value>
		protected int CurrentStep
		{
			get { return GetViewStateValue("JobEdit:CurrentStep", 1); }
			set { SetViewStateValue("JobEdit:CurrentStep", value); }
		}

		/// <summary>
		/// Gets the page controller.
		/// </summary>
		/// <value>
		/// The page controller.
		/// </value>
		protected JobWizardController PageController
		{
			get { return PageControllerBaseProperty as JobWizardController; }
		}

		/// <summary>
		/// Registers the page controller.
		/// </summary>
		/// <returns></returns>
		public override IPageController RegisterPageController()
		{
			return new JobWizardController(App);
		}

		/// <summary>
		/// Gets the model.
		/// </summary>
		/// <value>The model.</value>
		protected JobWizardViewModel Model
		{
			get { return _model ?? (_model = (GetViewStateValue("JobEdit:Model", new JobWizardViewModel()))); }
			set { _model = value; }
		}

		/// <summary>
		/// Records the steps visited
		/// </summary>
		protected List<int> StepsToSave
		{
			get { return GetViewStateValue("JobEdit:StepsToSave", (List<int>) null); }
			set { SetViewStateValue("JobEdit:StepsToSave", value); }
		}

		#endregion

		#region Page Load & PreRender

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			var source = Page.RouteData.Values["source"];
			if (source != null)
				FromApproval = (string.Compare(source.ToString(), "approval", StringComparison.OrdinalIgnoreCase) == 0);

			if (!IsPostBack)
			{
				var result = PageController.GetViewModel(HiringManagerId, JobId, FromApproval);
				if (Handle(result)) return;

				var viewModelResult = result as ViewModelResult;
				Debug.Assert(!viewModelResult.IsNull());

				Model = viewModelResult.ViewModel as JobWizardViewModel;

				if (Model.IsNull())
					throw new Exception("Could not get view model");

				// Move the step on if applicable but only if in draft
				if ((JobId > 0) && Model.Job.JobStatus == JobStatuses.Draft && !FromApproval)
				{
					CurrentStep = (Model.Job.WizardStep != 3 || (Model.Job.WizardStep == 3 && Model.Job.WizardPath == 1)
						               ? Model.Job.WizardStep + 1
						               : Model.Job.WizardStep);
				}
				else
				{
					var currentStep = Page.RouteData.Values["step"];
					CurrentStep = currentStep.IsNull() ? 1 : int.Parse(currentStep.ToString());
				}

				if (CurrentStep > FinalStep) CurrentStep = FinalStep;

				ShowCurrentStep();
			}
		}

		/// <summary>
		/// Handles the PreRender event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_PreRender(object sender, EventArgs e)
		{
			// Persist the Model
			SetViewStateValue("JobEdit:Model", Model);
		}

		#endregion

		#region Step navigation methods

		/// <summary>
		/// Shows the current step.
		/// </summary>
		private void ShowCurrentStep()
		{
			if (CurrentStep == 3 && Model.Job.WizardPath.IsNotIn(1, 21, 22))
				Model.Job.WizardPath = (Model.Job.WizardPath > 22) ? 22 : 1;

			Step1.Visible = (CurrentStep == 1);
			Step2.Visible = (CurrentStep == 2);
			Step3Path1.Visible = (CurrentStep == 3 && Model.Job.WizardPath == 1);
			Step3Path2Part1.Visible = (CurrentStep == 3 && Model.Job.WizardPath == 21);
			Step3Path2Part2.Visible = (CurrentStep == 3 && Model.Job.WizardPath == 22);
			Step4.Visible = (CurrentStep == 4);
			Step5.Visible = (CurrentStep == 5);
			Step6.Visible = (CurrentStep == 6);
			Step7.Visible = (CurrentStep == 7);

			PreviousButtonTop.Visible =
				PreviousButtonBottom.Visible = (CurrentStep > 1 && Model.Job.JobStatus == JobStatuses.Draft) && !FromApproval;
			NextButtonTop.Visible = NextButtonBottom.Visible = (CurrentStep != 2);

			PreviewButtonTop.Visible = PreviewSpacerTop.Visible = (CurrentStep > 2);
			PreviewButtonBottom.Visible = PreviewSpacerBottom.Visible = (CurrentStep > 2);

			if (CurrentStep == 1) Step1.BindStep(Model);
			if (CurrentStep == 2) Step2.BindStep(Model);
			if (CurrentStep == 3 && Model.Job.WizardPath == 1) Step3Path1.BindStep(Model);
			if (CurrentStep == 3 && Model.Job.WizardPath == 21) Step3Path2Part1.BindStep(Model);
			if (CurrentStep == 3 && Model.Job.WizardPath == 22) Step3Path2Part2.BindStep(Model);
			if (CurrentStep == 4) Step4.BindStep(Model);
			if (CurrentStep == 5) Step5.BindStep(Model);
			if (CurrentStep == 6) Step6.BindStep(Model);
			if (CurrentStep == 7) Step7.BindStep(Model);

			SetProgressBar();

			LocaliseUI();
		}

		/// <summary>
		/// Sets the progress bar.
		/// </summary>
		private void SetProgressBar()
		{
			SetStepActiveOrVisited(1, Step1ProgressBarItem);
			SetStepActiveOrVisited(2, Step2Step3ProgressBarItem);
			if (CurrentStep == 3) SetStepActiveOrVisited(3, Step2Step3ProgressBarItem);
			SetStepActiveOrVisited(4, Step4ProgressBarItem);
			SetStepActiveOrVisited(5, Step5ProgressBarItem);
			SetStepActiveOrVisited(6, Step6ProgressBarItem);
			SetStepActiveOrVisited(7, Step7ProgressBarItem);

			var maxVisitedStep = (Model.Job.WizardStep == 1 && Model.Job.UpdatedOn == DateTime.MinValue)
				? 0
				: Model.Job.WizardStep + 1;

			// Hide button for currrent step or if step has not previously been reached
			Step1ProgressBarItemLinkButton.Visible = (CurrentStep != 1);
			Step2Step3ProgressBarItemLinkButton.Visible = maxVisitedStep >= 2 && (!(CurrentStep == 2 || CurrentStep == 3));
			Step4ProgressBarItemLinkButton.Visible = maxVisitedStep >= 4 && CurrentStep != 4;
			Step5ProgressBarItemLinkButton.Visible = maxVisitedStep >= 5 && CurrentStep != 5;
			Step6ProgressBarItemLinkButton.Visible = maxVisitedStep >= 6 && CurrentStep != 6;
			Step7ProgressBarItemLinkButton.Visible = maxVisitedStep >= 7 && CurrentStep != 7;

			Step1ProgressBarItemLiteral.Visible = !Step1ProgressBarItemLinkButton.Visible;
			Step2Step3ProgressBarItemLiteral.Visible = !Step2Step3ProgressBarItemLinkButton.Visible;
			Step4ProgressBarItemLiteral.Visible = !Step4ProgressBarItemLinkButton.Visible;
			Step5ProgressBarItemLiteral.Visible = !Step5ProgressBarItemLinkButton.Visible;
			Step6ProgressBarItemLiteral.Visible = !Step6ProgressBarItemLinkButton.Visible;
			Step7ProgressBarItemLiteral.Visible = !Step7ProgressBarItemLinkButton.Visible;

			Step1ProgressBarItemLinkButton.Text =
				Step1ProgressBarItemLiteral.Text = CodeLocalise("ProgressBar.Step1.Text", "Title & #BUSINESS#");
			Step2Step3ProgressBarItemLinkButton.Text =
				Step2Step3ProgressBarItemLiteral.Text = CodeLocalise("ProgressBar.Step2Step3.Text", "Description");
			Step4ProgressBarItemLinkButton.Text =
				Step4ProgressBarItemLiteral.Text = CodeLocalise("ProgressBar.Step4.Text", "Requirements");
			Step5ProgressBarItemLinkButton.Text =
				Step5ProgressBarItemLiteral.Text = CodeLocalise("ProgressBar.Step5.Text", "Details");
			Step6ProgressBarItemLinkButton.Text =
				Step6ProgressBarItemLiteral.Text = CodeLocalise("ProgressBar.Step6.Text", "Salary and Benefits");
			Step7ProgressBarItemLinkButton.Text =
				Step7ProgressBarItemLiteral.Text = CodeLocalise("ProgressBar.Step7.Text", "Recruitment Information");
		}

		/// <summary>
		/// Sets the step active or visited.
		/// </summary>
		/// <param name="step">The step.</param>
		/// <param name="stepControl">The step control.</param>
		private void SetStepActiveOrVisited(int step, HtmlGenericControl stepControl)
		{
			if (CurrentStep == step) stepControl.Attributes.Add("class", "active");
			else
			{
				var maxVisitedStep = (Model.Job.WizardStep == 1 && Model.Job.UpdatedOn == DateTime.MinValue)
					? 0
					: Model.Job.WizardStep + 1;
				if (maxVisitedStep >= step) stepControl.Attributes.Add("class", "visited");
			}
		}

		/// <summary>
		/// Handles the Click event of the PreviousButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void PreviousButton_Click(object sender, EventArgs e)
		{
			CurrentStep--;
			AdjustCurrentStep();
			ShowCurrentStep();
		}

		/// <summary>
		/// Handles the Click event of the NextButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void NextButton_Click(object sender, EventArgs e)
		{
			if (Page.IsValid)
				GoNext(adjustStep: true);
		}

		/// <summary>
		/// Handles the Command event of the StepProgressBarLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void StepProgressBarLinkButton_Command(object sender, CommandEventArgs eventArgs)
		{
			var step = int.Parse(eventArgs.CommandArgument.ToString());
			if (CurrentStep != step && CurrentStep != 2 && !(CurrentStep == 3 && Model.Job.WizardPath == 21))
				UnbindStep(0);

			CurrentStep = step;
			AdjustCurrentStep();
			ShowCurrentStep();
		}

		/// <summary>
		/// Go to a specific step
		/// </summary>
		/// <param name="step">The step</param>
		private void GotoStep(int step)
		{
			CurrentStep = step;

			ShowCurrentStep();
		}

		/// <summary>
		/// Unbinds the steps and populates the model
		/// </summary>
		private void UnbindStep(int wizardPath)
		{
			if (StepsToSave.IsNull())
				StepsToSave = new List<int>();

			if (!StepsToSave.Contains(CurrentStep))
				StepsToSave.Add(CurrentStep);

			switch (CurrentStep)
			{
				case 1:
					Model = Step1.UnbindStep(Model);
					Model.BusinessUnitAddress =
						ServiceClientLocator.EmployerClient(App).GetPrimaryBusinessUnitAddress(Model.Job.BusinessUnitId.Value);
					_isNewAddress = true;
					break;

				case 2:
					Model.Job.WizardPath = wizardPath;
					Model.Job.Tasks = null;
					break;

				case 3:
					if (Model.Job.WizardPath == 1) Model = Step3Path1.UnbindStep(Model);
					if (Model.Job.WizardPath == 21) Model = Step3Path2Part1.UnbindStep(Model);
					if (Model.Job.WizardPath == 22) Model = Step3Path2Part2.UnbindStep(Model);
					break;

				case 4:
					Model = Step4.UnbindStep(Model);
					break;

				case 5:
					Model = Step5.UnbindStep(Model);
					break;

				case 6:
					Model = Step6.UnbindStep(Model);
					break;

				case 7:
					Model = Step7.UnbindStep(Model);
					break;
			}

			// Update some system data
			if (CurrentStep > Model.Job.WizardStep) Model.Job.WizardStep = CurrentStep;
			Model.Job.UpdatedBy = App.User.UserId;

			// Changes have been made so set the Approval status to none so that it goes through approval checks
      if (!FromApproval && Model.Job.ApprovalStatus != ApprovalStatuses.Rejected)
				Model.Job.ApprovalStatus = ApprovalStatuses.None;
		}

		/// <summary>
		/// Goes to the next step or step path / part
		/// </summary>
		/// <param name="wizardPath">The wizard path.</param>
		/// <param name="adjustStep">Whether to adjust the job description step</param>
		private void GoNext(int wizardPath = 0, bool adjustStep = false)
		{
			UnbindStep(wizardPath);
			if (!Handle(PageController.SaveJob(Model, StepsToSave, FromApproval, CurrentStep, FinalStep, _isNewAddress)))
			{
				if (CurrentStep < FinalStep) CurrentStep++;
			}
			else if (Model.Job.WizardPath == 21) Model.Job.WizardPath++;

			if (adjustStep)
				AdjustCurrentStep();

			ShowCurrentStep();
			SetProgressBar();
			StepsToSave.Clear();
		}

		/// <summary>
		/// Whether to automatically adjust the current step
		/// </summary>
		private void AdjustCurrentStep()
		{
			if (CurrentStep == 2 && Model.Job.Description.IsNotNullOrEmpty())
				CurrentStep = 3;
		}

		#endregion

		#region Complete & Preview events

		/// <summary>
		/// Handles the PathSelected event of the Step2 control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void Step2_PathSelected(object sender, CommandEventArgs e)
		{
			GoNext((int) e.CommandArgument);
		}

		/// <summary>
		/// Handles the Click event of the PreviewButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void PreviewButton_Click(object sender, EventArgs e)
		{
			var previewModel = CloneModel();

			// Unbind current step
			if (CurrentStep == 1) previewModel = Step1.UnbindStep(previewModel);

			if (CurrentStep == 3 && Model.Job.WizardPath == 1) previewModel = Step3Path1.UnbindStep(previewModel);
			if (CurrentStep == 3 && Model.Job.WizardPath == 21) previewModel = Step3Path2Part1.UnbindStep(previewModel);
			if (CurrentStep == 3 && Model.Job.WizardPath == 22) previewModel = Step3Path2Part2.UnbindStep(previewModel);

			if (CurrentStep == 4) previewModel = Step4.UnbindStep(previewModel);
			if (CurrentStep == 5) previewModel = Step5.UnbindStep(previewModel);
			if (CurrentStep == 6) previewModel = Step6.UnbindStep(previewModel);
			if (CurrentStep == 7) previewModel = Step7.UnbindStep(previewModel);

			previewModel.Job.PostingHtml = ServiceClientLocator.JobClient(App)
				.GeneratePosting(previewModel.Job, previewModel.JobLocations, previewModel.JobLanguages,
					previewModel.JobCertificates, previewModel.JobLicences, previewModel.JobSpecialRequirements,
					previewModel.JobProgramsOfStudy, previewModel.JobDrivingLicenceEndorsements);

			Handle(PageController.PreviewPosting(previewModel.Job));

			SetProgressBar();
		}

		protected void JobPreview_PostCommand(object sender, EventArgs e)
		{
			ShowCurrentStep();
		}

		#endregion

		#region Localise UI

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			PreviewButtonTop.Text = PreviewButtonBottom.Text = CodeLocalise("PreviewButton.Text", "Preview");
			PreviousButtonTop.Text = PreviousButtonBottom.Text = CodeLocalise("PreviousButton.Text", "Previous Step");

			if (CurrentStep < 7 && Model.Job.JobStatus == JobStatuses.Draft && !FromApproval)
				NextButtonTop.Text = NextButtonBottom.Text = CodeLocalise("NextButtonDraft.Text", "Save Draft / Move to Next Step");
			else if (CurrentStep == 2 || (CurrentStep == 3 && Model.Job.WizardPath == 21)) // Editing an non-draft job's description
				NextButtonTop.Text = NextButtonBottom.Text = CodeLocalise("NextButton.Text", "Move to Next Step");
			else // Non draft jobs and on the last step of the wizard
				NextButtonTop.Text = NextButtonBottom.Text = CodeLocalise("NextButtonPost.Text", "Save and Post");

		}

		#endregion

		#region Other methods

		/// <summary>
		/// Clones the model.
		/// </summary>
		/// <returns></returns>
		private JobWizardViewModel CloneModel()
		{
			var serializedModel = Model.Serialize();
			return (JobWizardViewModel) serializedModel.Deserialize(typeof (JobWizardViewModel));
		}

		#endregion

		protected void Step3Path1_OnGoToStep(object sender, CommandEventArgs eventargs)
		{
			Model = Step3Path1.UnbindStep(Model);
			GotoStep((int) eventargs.CommandArgument);
		}

		protected void Step3Path2Part2_OnGoToStep(object sender, CommandEventArgs eventargs)
		{
			Model = Step3Path2Part2.UnbindStep(Model);
			GotoStep((int) eventargs.CommandArgument);
		}
	}
}
