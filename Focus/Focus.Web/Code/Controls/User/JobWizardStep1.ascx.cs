﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.ViewModels;
using Framework.Core;
using Focus.Common.Extensions;
using Focus.Common;

#endregion

namespace Focus.Web.Code.Controls.User
{
  public partial class JobWizardStep1 : UserControlBase
	{
    protected const string ConfidentialCompany = "-1";
    protected const string AddNewCompanyDescription = "-1";

    /// <summary>
    /// Gets or sets the Employer Id.
    /// </summary>
    protected long CurrentEmployerId
    {
      get { return GetViewStateValue<long>("JobWizardStep1:EmployerId", -1); }
      set { SetViewStateValue("JobWizardStep1:EmployerId", value); }
    }

    /// <summary>
    /// Gets or sets the business unit id.
    /// </summary>
    /// <value>The business unit id.</value>
    protected long? BusinessUnitId
    {
      get { return GetViewStateValue<long?>("JobWizardStep1:BusinessUnitId"); }
      set { SetViewStateValue("JobWizardStep1:BusinessUnitId", value); }
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
        LocaliseUI();

      CompanyDescriptionTitlePanel.Style.Add("display", (CompanyDescriptionDropDownList.SelectedValue == AddNewCompanyDescription) ? "" : "none");
    }

    #region BindStep & UnbindStep

    /// <summary>
    /// Initialises the step.
    /// </summary>
    /// <param name="model">The model.</param>
    internal void BindStep(JobWizardViewModel model)
    {
      CurrentEmployerId = model.Job.EmployerId;
      BusinessUnitId = model.Job.BusinessUnitId;

      if (App.Settings.Theme != FocusThemes.Education)
        JobTitleAutoCompleteExtender.ContextKey = model.Job.EmployerId.ToString();

      if (!IsPostBack && model.Job.Id == null)
      {
        OccupationsPanel.Style.Add("display", "none");
        CompanyInformationPanel.Style.Add("display", "none");
      }
      else
      {
        OccupationsPanel.Style.Add("display", "");
        CompanyInformationPanel.Style.Add("display", "");
      }

      BindStaticControls();
      BindControls(model);

      CompanyLogoUploadButton.Visible = (BusinessUnitId.HasValue && BusinessUnitId.Value > 0);
    }

    /// <summary>
    /// Unbinds the step.
    /// </summary>
    /// <param name="model">The model.</param>
    /// <returns></returns>
    internal JobWizardViewModel UnbindStep(JobWizardViewModel model)
    {
      model.Job.JobTitle = JobTitleTextBox.TextTrimmed(200);

      var onetId = "";
      var ronetId = "";

      switch (App.Settings.Theme)
      {
        case FocusThemes.Education:
          ronetId = OccupationsRadioButtonList.SelectedValue;
          if (ronetId.IsNullOrEmpty())
            ronetId = ServiceClientLocator.SearchClient(App).GetROnetByCode(GetId()).ToString();
          break;
        default:
          onetId = OccupationsRadioButtonList.SelectedValue;
          if (onetId.IsNullOrEmpty())
            onetId = GetId();
          break;
      }

      if (App.Settings.Theme != FocusThemes.Education && (!model.Job.OnetId.HasValue || model.Job.OnetId.Value != onetId.ToLong()))
      {
        // If changed then set it and dump the current tasks 
        model.Job.OnetId = onetId.ToLong();
        model.Job.Tasks = null;
      }

      if (App.Settings.Theme == FocusThemes.Education && (!model.Job.ROnetId.HasValue || model.Job.ROnetId.Value != ronetId.ToLong()))
      {
        model.Job.Tasks = null;
        model.Job.ROnetId = ronetId.ToLong();
        model.Job.OnetId = ServiceClientLocator.SearchClient(App).GetOnetByROnet((long)ronetId.ToLong());
        model.JobProgramsOfStudy = new List<JobProgramOfStudyDto>();
        model.ResetProgramsOfStudy = true;
      }

      model.Job.BusinessUnitId = CompanyDropDown.SelectedValue != ConfidentialCompany ? CompanyDropDown.SelectedValueToNullableLong() : null;
      model.Job.BusinessUnitDescriptionId = CompanyDescriptionDropDownList.SelectedValueToNullableLong();
      model.Job.EmployerDescriptionPostingPosition = CompanyDescriptionPostingPositionDropDownList.SelectedValueToEnum(EmployerDescriptionPostingPositions.BelowJobPosting);

      model.Job.BusinessUnitLogoId = CompanyLogoDropDownList.SelectedValueToNullableLong();
      model.Job.IsConfidential = ConfidentialCompanyCheckBox.Checked;

      model.OriginalJobType = model.Job.JobType;
      model.Job.JobType = App.Settings.Theme == FocusThemes.Workforce ? JobTypes.Job : JobTypeDropDownList.SelectedValueToEnum(JobTypes.Job);

      if (model.OriginalJobType == JobTypes.None)
        model.OriginalJobType = model.Job.JobType;

      return model;
    }

    #endregion

    #region Bind Methods

    /// <summary>
    /// Binds the controls.
    /// </summary>
    /// <param name="model">The model.</param>
    private void BindControls(JobWizardViewModel model)
    {
      // Bind the Job Title
      JobTitleTextBox.Text = model.Job.JobTitle;

      // Bind the company names
      CompanyDropDown.DataSource = ServiceClientLocator.EmployeeClient(App).GetEmployeesBusinessUnits(model.Job.EmployeeId.Value);
      CompanyDropDown.DataValueField = "BusinessUnitId";
      CompanyDropDown.DataTextField = "BusinessUnitName";
      CompanyDropDown.DataBind();
			CompanyDropDown.Items.AddLocalisedTopDefault("Global.Company.TopDefault", "- select a #BUSINESS#:LOWER -");

      if (App.Settings.ConfidentialEmployersEnabled)
      {
        ConfidentialCompanyCheckBox.Visible = true;
        ConfidentialCompanyCheckBox.Text = CodeLocalise("Company.Confidential.Text", "Confidential - Name will not be displayed");
      }
      else
      {
	      ConfidentialCompanyCheckBox.Visible = false;
      }
			
			if (model.Job.IsConfidential.HasValue)
				ConfidentialCompanyCheckBox.Checked = model.Job.IsConfidential.Value;

      // If we have a business unit we use it, however if we are returning to this step and we need to handle the ConfidentialCompany selection.
      if (model.Job.BusinessUnitId.HasValue)
        CompanyDropDown.SelectValue(model.Job.BusinessUnitId.Value.ToString());
      else if (model.Job.Id.HasValue && !model.Job.BusinessUnitId.HasValue && App.Settings.ConfidentialEmployersEnabled)
        CompanyDropDown.SelectValue(ConfidentialCompany);
      else
        CompanyDropDown.SelectValue("");

      // Bind the company description names
      BindCompanyDescriptions(model.Job.BusinessUnitDescriptionId);

      // Bind the company description posting positions
      CompanyDescriptionPostingPositionDropDownList.SelectValue(model.Job.EmployerDescriptionPostingPosition.ToString(), EmployerDescriptionPostingPositions.BelowJobPosting.ToString());

      // Bind all the Occupation Families
      switch (App.Settings.Theme)
      {
        case FocusThemes.Workforce:
					// Only show Job Families which have active occupations
					var onetJobFamilies = ServiceClientLocator.SearchClient(App).GetOnets(true).Select(onet => onet.JobFamilyId).Distinct();
          var jobFamilies = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.JobFamily).Where(family => onetJobFamilies.Contains(family.Id)).ToList();

					OccupationsFamilyDropDownList.DataSource = jobFamilies;
          OccupationsFamilyDropDownList.DataTextField = "Text";
          OccupationsDropDownListCascadingDropDown.ServiceMethod = "GetOccupations";
          break;
        default:
          OccupationsFamilyDropDownList.DataSource = ServiceClientLocator.ExplorerClient(App).GetCareerAreas();
          OccupationsFamilyDropDownList.DataTextField = "Name";
          OccupationsDropDownListCascadingDropDown.ServiceMethod = "GetFilteredJobsByCareerArea";
         
          break;
      }

      OccupationsFamilyDropDownList.DataValueField = "Id";
      OccupationsFamilyDropDownList.DataBind();
      OccupationsFamilyDropDownList.Items.AddLocalisedTopDefault("Global.OccupationFamily.TopDefault", "- select an occupation family -");

      // Add prompt to Occupations cascader
      OccupationsDropDownListCascadingDropDown.PromptText = CodeLocalise("Global.Occupations.TopDefault", "- select an occupation -");
      OccupationsDropDownListCascadingDropDown.LoadingText = CodeLocalise("Global.Occupations.Progress", "[Loading occupations ...]");
      OccupationsDropDownListCascadingDropDown.PromptValue = string.Empty;

      if (model.Job.OnetId.HasValue)
      {
        OccupationsRadioButtonList.DataSource = App.Settings.Theme == FocusThemes.Education
                                                  ? (object)ServiceClientLocator.SearchClient(App).GetROnets((long)model.Job.OnetId,
                                                                                             model.Job.JobTitle, 5)
                                                  : ServiceClientLocator.SearchClient(App).GetOnets((long)model.Job.OnetId,
                                                                                   model.Job.JobTitle, 5);
        OccupationsRadioButtonList.DataValueField = "Id";
        OccupationsRadioButtonList.DataTextField = "Occupation";
        OccupationsRadioButtonList.DataBind();

        if (App.Settings.Theme == FocusThemes.Education)
        {
          if (model.Job.ROnetId.HasValue)
            OccupationsRadioButtonList.SelectValue(model.Job.ROnetId.Value.ToString());
        }
        else
          OccupationsRadioButtonList.SelectValue(model.Job.OnetId.Value.ToString());
      }

      // Bind the company logo
      BindLogos(model.Job.BusinessUnitLogoId);

      // Bind the job type
      if (App.Settings.Theme != FocusThemes.Education)
      {
        JobTypeHeaderRow.Visible = false;
        JobTypeRow.Visible = false;

        NumberingForJobTitle.Text = @"1.";
        NumberingForCompany.Text = @"2.";
        NumberingForLogo.Text = @"3.";
      }
      else
      {
        BindJobType(model.Job.JobType.IsNotNull() ? model.Job.JobType : JobTypes.Job);
        JobTypeDropDownList.Enabled = !model.Job.PostedOn.HasValue;
      }
    }

    /// <summary>
    /// Binds the company descriptions.
    /// </summary>
    /// <param name="selectedDescriptionId">The current description id.</param>
    private void BindCompanyDescriptions(long? selectedDescriptionId)
    {
      // Bind the company description names
      var employerDescriptions = ServiceClientLocator.EmployerClient(App).GetBusinessUnitDescriptions(BusinessUnitId.HasValue ? BusinessUnitId.Value : 0);

      // Update the primary employer description to show "... - default" 
      foreach (var employerDescription in employerDescriptions)
        if (employerDescription.IsPrimary)
        {
          employerDescription.Description = string.Format("{0} - {1}", employerDescription, CodeLocalise("DefaultEmployerDescription.Suffix", "default"));
          break;
        }

      CompanyDescriptionDropDownList.DataSource = employerDescriptions;
      CompanyDescriptionDropDownList.DataValueField = "Id";
      CompanyDescriptionDropDownList.DataTextField = "Title";
      CompanyDescriptionDropDownList.DataBind();
      CompanyDescriptionDropDownList.Items.AddLocalisedTopDefault("Global.Company.TopDefault", "- select a description -");
      if (BusinessUnitId.HasValue && BusinessUnitId.Value > 0) CompanyDescriptionDropDownList.Items.AddLocalised("CompanyDescription.AddNew.Text.NoEdit", "Add a new description", AddNewCompanyDescription);
      CompanyDescriptionDropDownList.SelectValue((selectedDescriptionId.HasValue ? selectedDescriptionId.Value.ToString() : ""));

      if (selectedDescriptionId.HasValue && selectedDescriptionId > 0)
      {
        try
        {
          CompanyDescriptionTextBox.Text = ServiceClientLocator.EmployerClient(App).GetBusinessUnitDescription(selectedDescriptionId.Value).Description;
        }
        catch { }
      }
    }


    /// <summary>
    /// Binds the job type drop down liat.
    /// </summary>
    private void BindJobType(JobTypes jobType)
    {
      JobTypeDropDownList.Items.Clear();
      JobTypeDropDownList.Items.AddLocalisedTopDefault("JobWizard.JobType.TopDefault", "- select job type -");
      JobTypeDropDownList.Items.AddEnum(JobTypes.Job, "Paid job");
      JobTypeDropDownList.Items.AddEnum(JobTypes.InternshipPaid, "Paid internship");
      JobTypeDropDownList.Items.AddEnum(JobTypes.InternshipUnpaid, "Unpaid internship");

      JobTypeDropDownList.SelectValue(jobType.ToString());
    }

    /// <summary>
    /// Binds the logos.
    /// </summary>
    /// <param name="selectedLogoId">The selected logo id.</param>
    private void BindLogos(long? selectedLogoId)
    {
      // Bind the company description names
      var employerLogos = ServiceClientLocator.EmployerClient(App).GetBusinessUnitLogos(BusinessUnitId.HasValue ? BusinessUnitId.Value : 0);

      CompanyLogoDropDownList.DataSource = employerLogos;
      CompanyLogoDropDownList.DataValueField = "Id";
      CompanyLogoDropDownList.DataTextField = "Name";
      CompanyLogoDropDownList.DataBind();
      CompanyLogoDropDownList.Items.AddLocalisedTopDefault("Global.Company.TopDefault", "- select a logo -");
      CompanyLogoDropDownList.SelectValue((selectedLogoId.HasValue ? selectedLogoId.Value.ToString() : ""));
    }

    /// <summary>
    /// Binds the static controls.
    /// </summary>
    private void BindStaticControls()
    {
			CompanyDescriptionPostingPositionDropDownList.Items.Clear();
      CompanyDescriptionPostingPositionDropDownList.Items.AddLocalisedTopDefault("Global.CompanyDescriptionPostingPosition.TopDefault", "- select a position -");
      CompanyDescriptionPostingPositionDropDownList.Items.AddEnum(EmployerDescriptionPostingPositions.BelowJobPosting, "Below job posting");
      CompanyDescriptionPostingPositionDropDownList.Items.AddEnum(EmployerDescriptionPostingPositions.AboveJobPosting, "Above job posting");
			CompanyDescriptionPostingPositionDropDownList.Items.AddEnum(EmployerDescriptionPostingPositions.DoNotShow, "Do not show #BUSINESS#:LOWER description");
    }


    #endregion

    #region Ajax events

    /// <summary>
    /// Handles the Click event of the UpdateOccupationsButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void UpdateOccupationsButton_Click(object sender, EventArgs e)
    {
      #region Display onet and mapped ronet - this is to make testing easier and was requested that it is left in the code base by Helen Gill
      //TestROnetInformation.Text = "";
      //var ronets = ServiceClientLocator.SearchClient(App).GetROnets(JobTitleTextBox.TextTrimmed(200), 5);
      //foreach (var ronet in ronets)
      //  TestROnetInformation.Text += string.Format("OnetCode:{0} ROnet:{1}</br>", ronet.OnetCode,
      //                                            ServiceClientLocator.SearchClient(App).GetROnetCodeById((long)ronet.Id));
      #endregion

      // Bind the occupations that are possible matches to the job title
      OccupationsRadioButtonList.DataSource = App.Settings.Theme == FocusThemes.Education
                                                ? (object)
                                                  ServiceClientLocator.SearchClient(App).GetROnets(JobTitleTextBox.TextTrimmed(200), 5)
                                                : ServiceClientLocator.SearchClient(App).GetOnets(JobTitleTextBox.TextTrimmed(200), 5);
      OccupationsRadioButtonList.DataValueField = "Id";
      OccupationsRadioButtonList.DataTextField = "Occupation";
      OccupationsRadioButtonList.DataBind();
    }

    /// <summary>
    /// Handles the Click event of the CompanyDescriptionSaveButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void CompanyDescriptionSaveButton_Click(object sender, EventArgs e)
    {
      if (BusinessUnitId == null) return;

      var employerDescription = ServiceClientLocator.EmployerClient(App).SaveBusinessUnitDescription(new BusinessUnitDescriptionDto
      {
        BusinessUnitId = BusinessUnitId.Value,
        Title =
					CompanyDescriptionTitleTextBox.Text.RemoveRogueCharacters(),
        Description =
					CompanyDescriptionTextBox.Text.RemoveRogueCharacters()
      });
      CompanyDescriptionTitlePanel.Style.Add("display", "none");
      CompanyDescriptionTitleTextBox.Text = "";

      // Bind the company description names
      BindCompanyDescriptions(employerDescription.Id);
    }

    /// <summary>
    /// Handles the Click event of the CompanyLogoUploadButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void CompanyLogoUploadButton_Click(object sender, EventArgs e)
    {
      if (BusinessUnitId == null) return;
      UploadLogo.Show(BusinessUnitId.Value);
    }

    /// <summary>
    /// Handles the OkClick event of the UploadLogo control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected void UploadLogo_OkClick(object sender, CommandEventArgs e)
    {
      var logoId = (long)e.CommandArgument;
      BindLogos(logoId);
    }

    #endregion

    /// <summary>
    /// Handles the SelectedIndexChanged event of the CompanyDropDown control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void CompanyDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
      BusinessUnitId = CompanyDropDown.SelectedValueToLong();
      BindCompanyDescriptions(0);
      BindLogos(0);
      CompanyLogoUploadButton.Visible = (BusinessUnitId.Value > 0);
    }

    #region Localise the UI

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      UpdateOccupationsButton.Text = CodeLocalise("Global.Find.Text.NoEdit", "Find");
      CompanyDescriptionSaveButton.Text = CodeLocalise("CompanyDescriptionSaveButton.Text", "Save description");
      CompanyLogoUploadButton.Text = CodeLocalise("CompanyLogoUploadButton.Text", "Upload a new logo");

      JobTitleRequired.ErrorMessage = CodeLocalise("JobTitle.Required", "Job title is required");
      InternshipJobTitleRequired.ErrorMessage = CodeLocalise("InternshipJobTitle.Required", "Internship title is required");
			CompanyRequired.ErrorMessage = CodeLocalise("Company.Required", "#BUSINESS# is required");
      OccupationsValidator.ErrorMessage = CodeLocalise("Occupation.Required", "Occupation is required. If unavailable from the selection list, enter a job title and click \"Find\"");
      CompanyDescriptionTitleRequired.ErrorMessage = CodeLocalise("CompanyDescriptionTitle.Required", "Description name is required");
      CompanyDescriptionRequired.ErrorMessage = CodeLocalise("CompanyDescription.Required", "Description is required");
      CompanyDescriptionCustomValidator.ErrorMessage = CodeLocalise("CompanyDescriptionCustomValidator.Required", "Please save the new description");

    }

    #endregion

    #region Helper methods

    /// <summary>
    /// Gets the onet id.
    /// </summary>
    /// <returns>The onetId</returns>
    internal string GetId()
    {
      var onetId = OccupationsDropDownListCascadingDropDown.SelectedValue;
      if (onetId.IsNotNullOrEmpty() && onetId.IndexOf(":::") > 0)
        onetId = onetId.Substring(0, onetId.IndexOf(":::"));

      return onetId;
    }
    #endregion
  }
}
