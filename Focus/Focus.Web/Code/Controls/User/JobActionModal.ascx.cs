#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class JobActionModal : UserControlBase
	{
		#region Properties

		private const string YesValue = "Y";
		private const string NoValue = "N";

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value>The title.</value>
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the job id.
		/// </summary>
		/// <value>The job id.</value>
		protected long JobId
		{
			get { return GetViewStateValue<long>("JobActionModal:JobId"); }
			set { SetViewStateValue("JobActionModal:JobId", value); }
		}

		/// <summary>
		/// Gets or sets the job ids.
		/// </summary>
		/// <value>The job id.</value>
		protected List<long> JobIds
		{
			get { return GetViewStateValue<List<long>>("JobActionModal:JobIds"); }
			set { SetViewStateValue("JobActionModal:JobIds", value); }
		}

		/// <summary>
		/// Gets or sets the job title. 
		/// </summary>
		/// <value>The job title.</value>
		protected string JobTitle
		{
			get { return GetViewStateValue<string>("JobActionModal:JobTitle"); }
			set { SetViewStateValue("JobActionModal:JobTitle", value); }
		}

		/// <summary>
		/// Gets or sets the job type. 
		/// </summary>
		/// <value>The job title.</value>
		protected JobTypes JobType
		{
			get { return GetViewStateValue<JobTypes>("JobActionModal:JobType"); }
			set { SetViewStateValue("JobActionModal:JobType", value); }
		}

		/// <summary>
		/// Gets or sets the type of the action.
		/// </summary>
		/// <value>The type of the action.</value>
		protected ActionTypes ActionType
		{
			get { return GetViewStateValue<ActionTypes>("JobActionModal:ActionType"); }
			set { SetViewStateValue("JobActionModal:ActionType", value); }
		}

		#endregion

		#region Event handlers

		public delegate void CompletedHandler(object sender, EventArgs eventArgs);

		public event CompletedHandler Completed;

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				var yesText = CodeLocalise("Global.Yes.Label", "Yes");
				var noText = CodeLocalise("Global.No.Label", "No");

				JobInterviewAnswerRadioButtonList.Items.Add(new ListItem(yesText, YesValue));
				JobInterviewAnswerRadioButtonList.Items.Add(new ListItem(noText, NoValue));
				//JobInterviewAnswerRadioButtonList.SelectedValue = YesValue;

				JobHireAnswerRadioButtonList.Items.Add(new ListItem(yesText, YesValue));
				JobHireAnswerRadioButtonList.Items.Add(new ListItem(noText, NoValue));
				//JobHireAnswerRadioButtonList.SelectedValue = YesValue;

				ClosingDateCompareValidator.ValueToCompare =
					ClosingDateGreaterThanTodayCompareValidator.ValueToCompare = DateTime.Now.ToShortDateString();

				JobExpirationLabel.Text = JobType.IsInternship()
				? HtmlLocalise("InternshipExpirationDateText.Label", "New internship expiration date")
				: HtmlLocalise("JobExpirationDateText.Label", "New job expiration date");

				Question1Label.Text = CodeLocalise("Question1Text.Label", "Were you satisfied with the quality of the applicants you found or received from us?");
			}

			var clientIds = new NameValueCollection
			{
				{ "ClosingDateIsValidCompareValidator", ClosingDateIsValidCompareValidator.ClientID },
				{ "ClosingDateCompareValidator", ClosingDateCompareValidator.ClientID },
				{ "ClosingDateGreaterThanTodayCompareValidator", ClosingDateGreaterThanTodayCompareValidator.ClientID },
			};
			RegisterCodeValuesJson("JobActionModal_ClientIds", "JobActionModal_ClientIds", clientIds);
		}

		/// <summary>
		/// Shows the modal.
		/// </summary>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobTitle">The job title.</param>
		/// <param name="jobType">The type of job (job or internship)</param>
		/// <param name="jobIds">Array of jobIds.</param>
		/// <param name="numberOfOpenings">The number of openings.</param>
		public void Show(ActionTypes actionType, long jobId, string jobTitle, JobTypes jobType, List<long> jobIds = null, int? numberOfOpenings = null)
		{
			JobId = jobId;
			JobIds = jobIds;
			JobTitle = jobTitle;
			JobType = jobType;
			ActionType = actionType;
            var appNamePlaceholder = App.CurrentModuleNamePlaceHolder;

			NumberOfPostingsRow.Visible = false;
			PaddingRow.Visible = false;

			var model = ServiceClientLocator.JobClient(App).GetJob(jobId);

			switch (ActionType)
			{
				case ActionTypes.RefreshJob:
					Title = CodeLocalise("RefreshJobTitle.Text", "Refresh posting for {0}", JobTitle);
					OkButton.Text = CodeLocalise("RefreshJobOk.Button", "Refresh posting");
					CloseButton.Text = CodeLocalise("RefreshJobClose.Button", "Cancel");
					string jobSurveyHeaderText;
					if (OldApp_RefactorIfFound.Settings.Module == FocusModules.Talent)
					{
						jobSurveyHeaderText = JobType.IsInternship()
							? CodeLocalise("RefreshInternshipSurveyHeader.Text", "Before refreshing this internship, please provide feedback to help our customer service initiative and to gather outcomes on the internship applicants we referred.")
							: CodeLocalise("RefreshJobSurveyHeader.Text", "Before refreshing this job, please provide feedback to help our customer service initiative and to gather outcomes on the job applicants we referred.");
					}
					else
					{
						jobSurveyHeaderText = JobType.IsInternship()
							? CodeLocalise("RefreshInternshipSurveyHeader.Text", "Before refreshing this internship, ask #BUSINESS#:LOWER to provide feedback for customer service initiative and to help us gather outcomes on the internship applicants we referred.")
							: CodeLocalise("RefreshJobSurveyHeader.Text", "Before refreshing this job, ask #BUSINESS#:LOWER to provide feedback for customer service initiative and to help us gather outcomes on the job applicants we referred.");
					}
					JobSurveyHeader.Text = jobSurveyHeaderText;
					SurveyRow.Visible = ExpirationDateRow.Visible = true;

                    Question1Label.Text = CodeLocalise("Question1Text.Label", "Were you satisfied with the quality of the applicants you found or received via {0}?", Constants.PlaceHolders.FocusTalent);
                    Question2Label.Text = CodeLocalise("Question2Text.Label", "Have you interviewed any of the applicants you found or received via {0}?", Constants.PlaceHolders.FocusTalent);
                    Question3Label.Text = CodeLocalise("Question3Text.Label", "Did you hire any of the applicants you found or received via {0}?", Constants.PlaceHolders.FocusTalent);
					BindExpirationDate(jobId, model, true);
					JobExpirationLabel.Text = JobType.IsInternship()
						? HtmlLocalise("NewInternshipExpirationDateText.Label", "New internship expiration date")
						: HtmlLocalise("NewJobExpirationDateText.Label", "New job expiration date");

					ExpirationDateValidator.ErrorMessage = CodeLocalise("ExpirationDateValidator.ErrorMessage", "Expiration date required.");
					ExpirationDateRow.Visible = true;
					BindApplicantsList();
					NumberOfPostingsRow.Visible = true;
					NumberOfPostingsTextBox.Text = numberOfOpenings.IsNotNull() ? numberOfOpenings.ToString() : string.Empty;

					break;

				case ActionTypes.ReactivateJob:
					Title = CodeLocalise("RefreshJobTitle.Text", "Reactivate posting for {0}", JobTitle);
					OkButton.Text = CodeLocalise("RefreshJobOk.Button", "Reactivate posting");
					CloseButton.Text = CodeLocalise("RefreshJobClose.Button", "Cancel");
					SurveyRow.Visible = false;
					JobExpirationLabel.Text = JobType.IsInternship()
						? HtmlLocalise("CurrentInternshipExpirationDateText.Label", "Current internship expiration date")
						: HtmlLocalise("CurrentJobExpirationDateText.Label", "Current job expiration date");
					ExpirationDateValidator.ErrorMessage = CodeLocalise("ExpirationDateValidator.ErrorMessage", "Expiration date required.");
					BindExpirationDate(jobId, model, false);
					ExpirationDateRow.Visible = true;
					ApplicantsRow.Visible = false;
					PaddingRow.Visible = true;

					break;

				case ActionTypes.CloseJob:
					Title = CodeLocalise("CloseJobTitle.Text", "Close posting for {0}", JobTitle);
					OkButton.Text = CodeLocalise("CloseJobOk.Button", "Close posting");
					CloseButton.Text = CodeLocalise("CloseJobClose.Button", "Cancel");
					JobSurveyHeader.Text = JobType.IsInternship()
                        ? CodeLocalise("CloseInternshipSurveyHeader.Text", "Before closing your internship, please help us improve {0} by answering a few questions.", Constants.PlaceHolders.FocusTalent)
                        : CodeLocalise("CloseJobSurveyHeader.Text", "Before closing your job, please help us improve {0} by answering a few questions.", Constants.PlaceHolders.FocusTalent);
					SurveyRow.Visible = true;
					ExpirationDateRow.Visible = false;
					BindApplicantsList();

                    Question1Label.Text = CodeLocalise("Question1Text.Label", "Were you satisfied with the quality of the applicants you found or received via {0}?", Constants.PlaceHolders.FocusTalent);
                    Question2Label.Text = CodeLocalise("Question2Text.Label", "Have you interviewed any of the applicants you found or received via {0}?", Constants.PlaceHolders.FocusTalent);
                    Question3Label.Text = CodeLocalise("Question3Text.Label", "Did you hire any of the applicants you found or received via {0}?", Constants.PlaceHolders.FocusTalent);

					AdvertiseFor30DaysPlaceHolder.Visible = model.ForeignLabourCertificationOther.GetValueOrDefault();
					break;
			}

			JobSatisfiedAnswerDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.SatisfactionLevels), null, null);
            JobSatisfiedAnswerDropDownList.Items.Insert(0, new ListItem("- Select response -", string.Empty));

			JobExpirationLabel.Text = JobType.IsInternship()
				? HtmlLocalise("InternshipExpirationDateText.Label", "New internship expiration date")
				: HtmlLocalise("JobExpirationDateText.Label", "New job expiration date");

			// Reset the survey question answers each time the modal is opened
			InitialiseUI();

			ModalPopup.Show();
		}

		/// <summary>
		/// Handles the Click event of the OkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void OkButton_Click(object sender, EventArgs e)
		{
			switch (ActionType)
			{
				case ActionTypes.RefreshJob:
				{
					UpdateApplicantsStatuses();

                    var postingSurvey = new PostingSurveyDto
                        {
                            JobId = JobId,
                            SurveyType = SurveyTypes.JobRefreshSurvey,
                            SatisfactionLevel = JobSatisfiedAnswerDropDownList.SelectedIndex != 0 ? (long?)JobSatisfiedAnswerDropDownList.SelectedValueToLong() : null,
                            DidInterview = JobInterviewAnswerRadioButtonList.SelectedValue != "" ? (bool?)(JobInterviewAnswerRadioButtonList.SelectedValue == YesValue) : null,
                            DidOffer = null,
                            DidHire = JobHireAnswerRadioButtonList.SelectedValue != "" ? (bool?)(JobHireAnswerRadioButtonList.SelectedValue == YesValue) : null
                        };

                        DateTime newExpirationDate;
                        DateTime.TryParse(ExpirationDateTextBox.Text, out newExpirationDate);
                        var newNumberOfOpenings = Convert.ToInt32(NumberOfPostingsTextBox.TextTrimmed());

                        ServiceClientLocator.JobClient(App).RefreshJob(JobId, newExpirationDate, postingSurvey, newNumberOfOpenings);

					break;
				}

				case ActionTypes.ReactivateJob:
				{
					DateTime newExpirationDate;
					DateTime.TryParse(ExpirationDateTextBox.Text, out newExpirationDate);

					if (JobIds.IsNullOrEmpty())
					{
						ServiceClientLocator.JobClient(App).ReactivateJob(JobId, newExpirationDate);
					}
					else
					{
						foreach (var jobId in JobIds)
						{
							ServiceClientLocator.JobClient(App).ReactivateJob(jobId, newExpirationDate);
						}
					}

					break;
				}

				case ActionTypes.CloseJob:
				{
					UpdateApplicantsStatuses();
           

    				    var postingSurvey = new PostingSurveyDto
					    {
						    JobId = JobId,
						    SurveyType = SurveyTypes.JobCloseSurvey,
						    SatisfactionLevel = JobSatisfiedAnswerDropDownList.SelectedIndex != 0 ? (long?)JobSatisfiedAnswerDropDownList.SelectedValueToLong() : null,
						    DidInterview = JobInterviewAnswerRadioButtonList.SelectedValue != ""? (bool?)(JobInterviewAnswerRadioButtonList.SelectedValue == YesValue) : null,
						    DidOffer = null,
						    DidHire = JobHireAnswerRadioButtonList.SelectedValue != ""? (bool?)(JobHireAnswerRadioButtonList.SelectedValue == YesValue) : null
					    };

					    ServiceClientLocator.JobClient(App).CloseJob(JobId, postingSurvey);

					break;
				}
			}

			OnCompleted(new EventArgs());
			ModalPopup.Hide();
		}

		/// <summary>
		/// Handles the Click event of the CloseButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CloseButton_Click(object sender, EventArgs e)
		{
			OnCompleted(new EventArgs());
			ModalPopup.Hide();
		}

		/// <summary>
		/// Raises the <see cref="Completed"/> event.
		/// </summary>
		/// <param name="eventArgs">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void OnCompleted(EventArgs eventArgs)
		{
			if (Completed != null)
				Completed(this, eventArgs);
		}

		/// <summary>
		/// Handles the ItemDataBound event of the ApplicantsRepeater control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void ApplicantsRepeater_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var applicationStatusDropDownList = (DropDownList)e.Item.FindControl("ApplicationStatusDropDown");
			//var applicationStatusValidator = (RequiredFieldValidator)e.Item.FindControl("ApplicationStatusRequired");
			BindStatus(applicationStatusDropDownList, ((ApplicationViewDto)e.Item.DataItem).CandidateApplicationStatus);
		}

		/// <summary>
		/// Binds the status.
		/// </summary>
		/// <param name="statusDropDownList">The status drop down list.</param>
		/// <param name="status">The status.</param>
		/// <param name="validator">The validator.</param>
		private void BindStatus(DropDownList statusDropDownList, ApplicationStatusTypes status)
		{
			statusDropDownList.Items.Clear();

			statusDropDownList.Items.Add(new ListItem(CodeLocalise("ApplicationStatus.TopDefault", "- choose outcome -"), string.Empty));
			statusDropDownList.Items.AddEnum(ApplicationStatusTypes.DidNotApply);
			statusDropDownList.Items.AddEnum(ApplicationStatusTypes.FailedToShow);
			statusDropDownList.Items.AddEnum(ApplicationStatusTypes.FailedToReportToJob);
			statusDropDownList.Items.AddEnum(ApplicationStatusTypes.FailedToRespondToInvitation);
			statusDropDownList.Items.AddEnum(ApplicationStatusTypes.Hired);
			statusDropDownList.Items.AddEnum(ApplicationStatusTypes.InterviewDenied);
			statusDropDownList.Items.AddEnum(ApplicationStatusTypes.JobAlreadyFilled);
			statusDropDownList.Items.AddEnum(ApplicationStatusTypes.NewApplicant);
			statusDropDownList.Items.AddEnum(ApplicationStatusTypes.NotHired);
			statusDropDownList.Items.AddEnum(ApplicationStatusTypes.NotQualified);
            statusDropDownList.Items.Add(new ListItem(CodeLocalise("ApplicationStatusTypes.RefusedOffer", "Refused job"), ApplicationStatusTypes.RefusedOffer.ToString()));
            statusDropDownList.Items.AddEnum(ApplicationStatusTypes.RefusedReferral);

			// Bind validator to drop down
			//validator.ErrorMessage = CodeLocalise("ApplicationStatus.ErrorMessage", "Please set the referral outcome");

			if (statusDropDownList.Items.FindByValue(status.ToString()) != null)
				statusDropDownList.SelectValue(status.ToString());
		}

		/// <summary>
		/// Binds the expiration date fields.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="model">The job model.</param>
		/// <param name="suggestDate">Whether to suggest a new closing date.</param>
		private void BindExpirationDate(long jobId, JobDto model, bool suggestDate)
		{
			var jobDates = ServiceClientLocator.JobClient(App).GetJobDates(jobId);

			var currentExpiryDate = jobDates.ClosingOn.IsNull() ? DateTime.Today : jobDates.ClosingOn.Value;

			ClosingDateIsValidCompareValidator.ErrorMessage = CodeLocalise("ClosingDateIsValidCompareValidator.ErrorMessage", "Valid date is required");

			ClosingDateGreaterThanTodayCompareValidator.ErrorMessage = CodeLocalise("ClosingDateGreaterThanTodayCompareValidatior.ErrorMessage", "Date must be in the future");
			ClosingDateGreaterThanTodayCompareValidator.ValueToCompare = DateTime.Today.ToShortDateString();

			var expiryDate = JobWizardControl.GetJobExpiryDate(App, jobDates.PostedOn, jobDates.FederalContractor.GetValueOrDefault(false),
				model.ForeignLabourCertificationH2A.GetValueOrDefault(false),
				model.ForeignLabourCertificationH2B.GetValueOrDefault(false),
				model.ForeignLabourCertificationOther.GetValueOrDefault(false));

			ClosingDateCompareValidator.ValueToCompare = expiryDate.ToShortDateString();
			ClosingDateCompareValidator.ErrorMessage = CodeLocalise("ClosingDateCompareValidator.Lifetime.ErrorMessage", "Closing date cannot exceed {0}", expiryDate.ToShortDateString());

			ClosingDateCompareValidator.Visible =
				ClosingDateCompareValidator.Enabled = ClosingDateCompareValidator.Enabled = (expiryDate != DateTime.MaxValue); 

			// If refreshing a job
			if (suggestDate)
			{
				var suggestedExpiryDate = DateTime.Now.Date.AddDays(App.Settings.DaysForDefaultJobClosingDate);
				if (suggestedExpiryDate > expiryDate)
				{
					suggestedExpiryDate = expiryDate;
				}
				ExpirationDateTextBox.Text = suggestedExpiryDate.ToShortDateString();
			}
			else
			{
				ExpirationDateTextBox.Text = currentExpiryDate.ToShortDateString();
			}
			/*
			ExpirationDateDropDown.Items.Clear();

			ExpirationDateDropDown.Items.Add(new ListItem(CodeLocalise("ExpirationDate.TopDefault", "- select date -"), ""));

		  var expiryDate = App.Settings.DaysForJobPostingLifetime > 0
		                     ? JobWizardControl.GetJobExpiryDate(jobPostingDate, App.Settings.DaysForJobPostingLifetime)
		                     : DateTime.Now.Date.AddDays(App.Settings.MaximumNumberOfDaysCanExtendJobBy);

		  var date = DateTime.Now.Date;
			while (date <= expiryDate)
			{
				ExpirationDateDropDown.Items.Add(new ListItem(date.ToShortDateString(), date.Date.ToString()));
			  date = date.AddDays(1);
			}
      */
		}

		/// <summary>
		/// Binds the applicants list.
		/// </summary>
		private void BindApplicantsList()
		{
			ApplicantsListView.DataSource = ServiceClientLocator.JobClient(App).GetJobApplicants(JobId);
			ApplicantsListView.DataBind();
			ApplicantsRow.Visible = (ApplicantsListView.Items.Count > 0);
		}

		/// <summary>
		/// Updates the applicants statuses.
		/// </summary>
		private void UpdateApplicantsStatuses()
		{
			foreach (var item in ApplicantsListView.Items)
			{
				try
				{
					long candidateApplicationId;
					long.TryParse(ApplicantsListView.DataKeys[item.DataItemIndex]["Id"].ToString(), out candidateApplicationId);

					var applicationStatusDropDown = (DropDownList)item.FindControl("ApplicationStatusDropDown");

                    if (applicationStatusDropDown.SelectedValue != string.Empty)
                    {
                        var newStatus = applicationStatusDropDown.SelectedValueToEnum<ApplicationStatusTypes>();

                        if (candidateApplicationId.IsNotNull() && candidateApplicationId > 0)
                            ServiceClientLocator.CandidateClient(App).UpdateApplicationStatus(candidateApplicationId, newStatus);
                    }
				}
				catch {}
			}
		}

		/// <summary>
		/// Initialises the UI.
		/// </summary>
		private void InitialiseUI()
		{
			JobSatisfiedAnswerDropDownList.SelectedIndex = 0;
            JobInterviewAnswerRadioButtonList.SelectedIndex = -1;
            JobHireAnswerRadioButtonList.SelectedIndex = -1;

			NumberOfPostingsValidator.ErrorMessage = CodeLocalise("NumberOfPostingsValidator.Error", "Number of openings must be entered");
			NumberOfPostingsRegEx.ValidationExpression = @"^[0-9]*$";
            NumberOfPostingsRegEx.ErrorMessage = CodeLocalise("NumberOfPostings.NumberOfPostingsRegEx", "Number of openings must be a numeric value");
            NumberOfPostingsTextBoxRangeValidator.ErrorMessage = CodeLocalise("NumberOfPostingsTextBoxRangeValidator.Error", "Number of openings must be between 1 and 200");

            AdvertiseFor30DaysText.Text = HtmlLocalise("AdvertiseFor30DaysText.Text", "To comply with Federal Law, this job openings must be advertised for a <b>full 30 days</b>");
		}
	}
}
