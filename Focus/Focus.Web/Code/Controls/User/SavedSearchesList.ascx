﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SavedSearchesList.ascx.cs" Inherits="Focus.Web.Code.Controls.User.SavedSearchesList" %>
<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>
<%@ Register src="~/Code/Controls/User/SaveSearchActionModal.ascx" tagname="SaveSearchActionModal" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/CandidateSearchCriteriaSummary.ascx" tagname="CandidateSearchCriteriaSummary" tagprefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<%@ Register src="~/Code/Controls/User/Pager.ascx" tagname="Pager" tagprefix="uc" %>

	<b><%= HtmlLocalise("ManageSavedSearches.Label", "Saved searches")%></b>
	<br/><br/>
	<table class="genericTable" role="presentation">
	  <tr>
	   <td><strong><asp:literal ID="ResultCount" runat="server" /></strong>&nbsp;</td>
     <td colspan="4">
	      <div style="width:100%;">
		    <uc:Pager ID="SearchResultListPager" runat="server" PagedControlID="SavedSearches" PageSize="10" />
	      </div>
      </td>
    </tr>
    <tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr style="border-bottom: 1px solid">
			<td style="width:20%;"><%= HtmlLocalise("SavedSearcheName.ColumnTitle", "Saved search name")%></td>
			<td style="width:40%;"><%= HtmlLocalise("SearchCriteria.ColumnTitle", "Search criteria")%></td>
			<td style="width:20%;"><%= HtmlLocalise("Status.ColumnTitle", "Status")%></td>
			<td style="width:10%;"></td>
      <td style="width:10%;"></td>
		</tr>
			<asp:ListView ID="SavedSearches" runat="server" OnItemDataBound="SavedSearchesList_OnItemDataBound" ItemPlaceholderID="SearchResultPlaceHolder" 
      DataSourceID="SavedSearchesDataSource" DataKeyNames="Id, Name">
		  
			<LayoutTemplate>
				<asp:PlaceHolder ID="SearchResultPlaceHolder" runat="server" />
			</LayoutTemplate>
			<ItemTemplate>
				<tr style="vertical-align:top;">
					<td>
						<a id="SavedSearch_Url" runat="server" href="" title="Saved search name" ><%# ((SavedSearchDto)Container.DataItem).Name%></a><br/>
						<%--<i>Created on April 9, 2011</i>--%>
					</td>
					<td><uc:CandidateSearchCriteriaSummary ID="CriteriaSummary" runat="server" /></td>
					<td><%# FormatAlertSettings((SavedSearchDto)Container.DataItem)%></td>
					<td>
						<asp:LinkButton ID="EditLinkButton" runat="server" CommandName="Edit" CommandArgument="<%# Container.DisplayIndex %>" OnCommand="SearchRowAction_Command" ><%# HtmlLocalise("EditNotifications.LinkText", "Edit notifications")%></asp:LinkButton>
					</td>
          <td>
						<asp:LinkButton ID="DeleteLinkButton" runat="server"  CommandName="Delete" CommandArgument="<%# Container.DisplayIndex %>" OnCommand="SearchRowAction_Command" ><%# HtmlLocalise("DeleteSavedSearch.LinkText", "Delete search")%></asp:LinkButton>
					</td>
				</tr>	
      </ItemTemplate>
			<EmptyDataTemplate>
			  <tr style="vertical-align:top;">
				  <td colspan="4">
				    <br />
			      <%= HtmlLocalise("SavedSearchesList.NoReult", "You have no saved searches")%>
				  </td>
        </tr>
			</EmptyDataTemplate>
		</asp:ListView>
    <asp:ObjectDataSource ID="SavedSearchesDataSource" DeleteMethod="Delete_SavedSearch"  runat="server" TypeName="Focus.Web.Code.Controls.User.SavedSearchesList" EnablePaging="true" 
														SelectMethod="GetSavedSearches" OnSelecting="SearchResultDataSource_Selecting" SelectCountMethod="GetSavedSearchCount" SortParameterName="orderBy">
      </asp:ObjectDataSource>
	</table>
	<uc:SaveSearchActionModal ID="SaveSearchAction" runat="server" OnCompleted="SaveSearchAction_Completed"/>
  <uc:ConfirmationModal ID="DeleteConfirmationModal" runat="server" OnOkCommand="DeleteConfirmationModal_OkCommand" />
