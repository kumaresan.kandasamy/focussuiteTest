﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Focus.Web.Code.Controls.User
{
	public partial class UpdateablePairs : UserControlBase
	{
		public string Width { get; set; }

		/// <summary>
		/// Gets or sets the items.
		/// </summary>
		/// <value>The items.</value>
		public List<KeyValuePair<string,string>> Items
		{
			get { return GetViewStateValue<List<KeyValuePair<string, string>>>("UpdateablePair:" + ID + ":Items"); }
			set
			{
				SetViewStateValue("UpdateablePair:" + ID + ":Items", value);
				Bind();
			}
		}

		/// <summary>
		/// Gets or sets the size of the max list.
		/// </summary>
		/// <value>The size of the max list.</value>
		public int MaxListSize { get; set; }

    /// <summary>
    /// Gets or sets the minimum size of the list.
    /// </summary>
    /// <value>The minimum size of the list.</value>
    public int MinListSize { get; set; }
		
		/// <summary>
		/// Gets or sets a value indicating whether to restrict duplicates in the collection.
		/// </summary>
		/// <value><c>true</c> if [restrict duplicates]; otherwise, <c>false</c>.</value>
		public bool RestrictDuplicates { get; set; }

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				ItemsListPanel.Width = Width == null ? 254 : Unit.Parse(Width);

				Bind();
			}
		}
		
		/// <summary>
		/// Adds the item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
    public UpdateablePairsAddResult AddItem(KeyValuePair<string, string> item)
		{
			if (Items == null)
				Items = new List<KeyValuePair<string, string>>();

			if (RestrictDuplicates && Items.Contains(item))
        return UpdateablePairsAddResult.Duplicate;

			if (MaxListSize > 0 && Items.Count >= MaxListSize)
        return UpdateablePairsAddResult.LimitReached;

			Items.Add(item);

			Bind();
      return UpdateablePairsAddResult.Success;
		}

		/// <summary>
		/// Handles the Command event of the ItemsListRemoveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ItemsListRemoveButton_Command(object sender, CommandEventArgs e)
		{
			var rowNo = Convert.ToInt32(e.CommandArgument);
			Items.RemoveAt(rowNo);
			Bind();
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			ItemsList.DataSource = Items;
			ItemsList.DataBind();
		}
	}

  public enum UpdateablePairsAddResult
  {
    Success,
    Duplicate,
    LimitReached
  }
}