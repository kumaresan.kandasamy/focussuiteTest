﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using Framework.Core;

using Focus.Core;
using Focus.Core.Views;
using Focus.Common.Extensions;
using Focus.Common;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class ResumePreviewModal : UserControlBase
	{
		private ResumeView Candidate
		{
			get { return GetViewStateValue<ResumeView>("ResumePreviewModal:Candidate"); }
			set { SetViewStateValue("ResumePreviewModal:Candidate", value); }
		}

		/// <summary>
		/// Handles the Command event of the ResumeAction control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ResumeAction_Command(object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Email":
					OnResumePreviewEmailCandidateClick(new ResumePreviewEmailCandidateClickEventArgs(Candidate.Id, Candidate.Name, Candidate.Status, Candidate));
					ResumePreviewModalPopup.Hide();
					break;

				case "EmailResume":
					OnResumePreviewEmailResumeCandidateClick(new ResumePreviewEmailResumeCandidateClickEventArgs(Candidate.Id));
					ResumePreviewModalPopup.Hide();
					break;

				case "CandidatesLikeThis":
					OnResumePreviewCandidatesLikeMeClick(new ResumePreviewCandidatesLikeMeEventArgs(Candidate.Id));
					ResumePreviewModalPopup.Hide();
					break;

				case "Note":
					var candidateNote = ServiceClientLocator.CandidateClient(App).GetPersonNote(Candidate.Id, App.User.EmployerId);
					if (candidateNote.IsNotNull()) NotesTextBox.Text = candidateNote.Note;
					EditNotePanel.Visible = true;
					ResumePreviewModalPopup.Show();
					break;

				case "SaveNote":
					ServiceClientLocator.CandidateClient(App).SavePersonNote(Candidate.Id, App.User.EmployerId, NotesTextBox.TextTrimmed());
					Confirmation.Show(CodeLocalise("SaveNoteConfirmation.Title", "Note saved"),
						CodeLocalise("SaveNoteConfirmation.Body", "Note has been saved to candidate."),
						CodeLocalise("Global.Close.Text", "Close"));
					Candidate.EmployerNote = NotesTextBox.TextTrimmed();
					NotePanel.Attributes["class"] = Candidate.EmployerNote.IsNotNullOrEmpty() ? "tooltip left Noted" : "tooltip left Note";
					EditNotePanel.Visible = false;
					OnResumePreviewNoteSaved(new EventArgs());
					ResumePreviewModalPopup.Show();
					break;

				case "CloseNote":
					EditNotePanel.Visible = false;
					ResumePreviewModalPopup.Show();
					break;

				case "Flag":
					ServiceClientLocator.CandidateClient(App).ToggleCandidateFlag(Candidate.Id);
					Candidate.Flagged = !Candidate.Flagged;
					FlagPanel.Attributes["class"] = Candidate.Flagged ? "tooltip left Flagged" : "tooltip left Flag";
					OnResumePreviewFlagToggled(new EventArgs());
					ResumePreviewModalPopup.Show();
					break;
			}
		}

		/// <summary>
		/// Binds the NCRC image.
		/// </summary>
		/// <param name="ncrcImage">The NCRC icon image.</param>
		/// <param name="ncrcLevel">The candidate's NCRC level.</param>
		/// <param name="ncrcLevelId">The (lokup) ID for the candidates NCRC level.</param>
		private void BindNcrcImage(Image ncrcImage, NcrcLevelTypes? ncrcLevel, long? ncrcLevelId)
		{
			if (App.Settings.Theme != FocusThemes.Workforce || ((ncrcLevel == NcrcLevelTypes.None || ncrcLevel.IsNull()) && ncrcLevelId.IsNull())) return;
			ncrcImage.Visible = true;
			if (ncrcLevel == NcrcLevelTypes.Bronze || ncrcLevelId == GetLookupId("NCRCLevel.Bronze"))
			{
				ncrcImage.ImageUrl = UrlBuilder.NcrcBronzeIconSmall();
				ncrcImage.AlternateText = ncrcImage.ToolTip = CodeLocalise("Ncrc.Bronze.Tooltip", "Bronze NCRC");
			}
			else if (ncrcLevel == NcrcLevelTypes.Silver || ncrcLevelId == GetLookupId("NCRCLevel.Silver"))
			{
				ncrcImage.ImageUrl = UrlBuilder.NcrcSilverIconSmall();
				ncrcImage.AlternateText = ncrcImage.ToolTip = CodeLocalise("Ncrc.Silver.Tooltip", "Silver NCRC");
			}
			else if (ncrcLevel == NcrcLevelTypes.Gold || ncrcLevelId == GetLookupId("NCRCLevel.Gold"))
			{
				ncrcImage.ImageUrl = UrlBuilder.NcrcGoldIconSmall();
				ncrcImage.AlternateText = ncrcImage.ToolTip = CodeLocalise("Ncrc.Gold.Tooltip", "Gold NCRC");
			}
			else if (ncrcLevel == NcrcLevelTypes.Platinum || ncrcLevelId == GetLookupId("NCRCLevel.Platinum"))
			{
				ncrcImage.ImageUrl = UrlBuilder.NcrcPlatinumIconSmall();
				ncrcImage.AlternateText = ncrcImage.ToolTip = CodeLocalise("Ncrc.Platinum.Tooltip", "Platinum NCRC");
			}
		}

		/// <summary>
		/// Shows the resume for the candidate and job.
		/// </summary>
		/// <param name="candidate">The candidate.</param>
		/// <param name="jobId">The job id.</param>
		/// <param name="showEmailPanel">Whether to show the email icon.</param>
		/// <param name="approvedUser">Whether the current user is approved.</param>
		public void Show(ResumeView candidate, long jobId, bool showEmailPanel = true, bool approvedUser = true)
		{
			Candidate = candidate;

			var returnFullResume = App.Settings.Module == FocusModules.Assist || App.User.IsShadowingUser || Candidate.IsApplicantForEmployer.GetValueOrDefault() || (App.Settings.ShowContactDetails == ShowContactDetails.Full && candidate.ContactInfoVisible) || App.Settings.ShowContactDetails == ShowContactDetails.Partial;

			var resumeHtml = ServiceClientLocator.CandidateClient(App).GetResumeAsHtml(Candidate.Id, returnFullResume, jobId: jobId);

			if (resumeHtml.IsNull())
			{
				ShowError();
				return;
			}

			// If we're in talent then mark the application as viewed for job issue calculation
			if (App.Settings.Module == FocusModules.Talent && !App.User.IsShadowingUser)
			{
				ServiceClientLocator.CandidateClient(App).MarkApplicationViewed(jobId, candidate.Id);
				if (candidate.ApplicationApprovalStatus == ApprovalStatuses.Approved)
					ServiceClientLocator.JobClient(App).ResolveJobIssues(jobId, new List<JobIssuesFilter> { JobIssuesFilter.NotViewingReferrals }, null, true);
			}

			GetScoreImage(Candidate.Score);

			NotesTextBox.Text = "";
			EditNotePanel.Visible = false;
			lblPreviewFormattedOutput.Text = resumeHtml;

			FlagPanel.Attributes["class"] = Candidate.Flagged ? "tooltip left Flagged" : "tooltip left Flag";

			NotePanel.Attributes["class"] = Candidate.EmployerNote.IsNotNullOrEmpty() ? "tooltip left Noted" : "tooltip left Note";

			VeteranImage.ToolTip = CodeLocalise("VeteranImage.ToolTip", "Veteran");
			VeteranImage.Visible = Candidate.IsVeteran;
			BindNcrcImage(NcrcImage, Candidate.NcrcLevel, Candidate.NcrcLevelId);

			// Hide the email button if WF and no job selected or Edu and an unapproved user
			if ((App.Settings.Theme == FocusThemes.Workforce) || (App.Settings.Theme == FocusThemes.Education && !approvedUser))
			{
				EmailPanel.Visible = jobId != 0 || showEmailPanel;
			}
			else
			{
				EmailPanel.Visible = true;
			}
			PrintPanel.Visible = candidate.ApplicationId != 0 && App.Settings.Theme == FocusThemes.Workforce;

			EmailCustom.Visible = App.Settings.Theme == FocusThemes.Workforce && jobId != 0 && showEmailPanel && Candidate.IsApplicantForEmployer.GetValueOrDefault();

			CandidateNameLabel.Text = (Candidate.NameIsMasked) ? "#" + Candidate.Id.ToString() : Candidate.Name;

			ResumePreviewModalPopup.Show();
		}

		/// <summary>
		/// Shows the resume for a candidate specified by resume ID.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <param name="candidate">The candidate.</param>
		/// <param name="showEmailPanel">Whether to show the email icon.</param>
		public void Show(long resumeId, ResumeView candidate, bool showEmailPanel = true)
		{
			Candidate = candidate;

			var resumeHtml = ServiceClientLocator.CandidateClient(App).GetResumeByIdAsHtml(resumeId);

			if (resumeHtml.IsNull())
			{
				ShowError();
				return;
			}

			ResumeScoreImageCell.Visible = false;

			NotesTextBox.Text = "";
			EditNotePanel.Visible = false;
			lblPreviewFormattedOutput.Text = resumeHtml;

			FlagPanel.Attributes["class"] = Candidate.Flagged ? "tooltip left Flagged" : "tooltip left Flag";

			NotePanel.Attributes["class"] = Candidate.EmployerNote.IsNotNullOrEmpty() ? "tooltip left Noted" : "tooltip left Note";

			VeteranImage.ToolTip = CodeLocalise("VeteranImage.ToolTip", "Veteran");
			VeteranImage.Visible = Candidate.IsVeteran;

			EmailPanel.Visible = showEmailPanel;
			EmailCustom.Visible = showEmailPanel && App.Settings.Theme == FocusThemes.Workforce && Candidate.IsApplicantForEmployer.GetValueOrDefault();

			CandidateNameLabel.Text = (Candidate.NameIsMasked) ? "#" + Candidate.Id.ToString() : Candidate.Name;

			ResumePreviewModalPopup.Show();
		}

		/// <summary>
		/// Gets the score image.
		/// </summary>
		/// <param name="score">The score.</param>
		private void GetScoreImage(int score)
		{
			var minimumStarScores = App.Settings.StarRatingMinimumScores;

			ResumeScoreImage.ToolTip = score.ToString();

			if (score >= minimumStarScores[5])
				ResumeScoreImage.ImageUrl = UrlBuilder.AssistFiveStarRatingImage();
			else if (score >= minimumStarScores[4])
				ResumeScoreImage.ImageUrl = UrlBuilder.AssistFourStarRatingImage();
			else if (score >= minimumStarScores[3])
				ResumeScoreImage.ImageUrl = UrlBuilder.AssistThreeStarRatingImage();
			else if (score >= minimumStarScores[2])
				ResumeScoreImage.ImageUrl = UrlBuilder.AssistTwoStarRatingImage();
			else if (score >= minimumStarScores[1])
				ResumeScoreImage.ImageUrl = UrlBuilder.AssistOneStarRatingImage();
			else
				ResumeScoreImage.ImageUrl = UrlBuilder.AssistZeroStarRatingImage();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			FlagPanel.Visible = NotePanel.Visible = (App.Settings.Module == FocusModules.Talent);
			EmailButtonLabel.Text = CodeLocalise("Email.ToolTip", "Email");
			FlagButtonLabel.Text = CodeLocalise("Flag.ToolTip", "Toggle Flag");
			NoteButtonLabel.Text = CodeLocalise("Note.ToolTip", "Add/Edit Note");
			CandidatesLikeThisButtonLabel.Text = CodeLocalise("CandidatesLikeThis.ToolTip", "Candidates Like This");
		}

		private void ApplyBranding()
		{
			VeteranImage.ImageUrl = UrlBuilder.VeteranImage();
		}

		#region Events

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
				ApplyBranding();

				//Bind a zero score to this image to prevent the image having an empty src attribute
				GetScoreImage(0);
			}
		}

		public event ResumePreviewEmailCandidateClickHandler ResumePreviewEmailCandidateClick;


		/// <summary>
		/// Raises the <see cref="E:ResumePreviewEmailCandidateClick"/> event.
		/// </summary>
		/// <param name="e">The <see cref="Focus.Web.Code.Controls.User.ResumePreviewModal.ResumePreviewEmailCandidateClickEventArgs"/> instance containing the event data.</param>
		protected virtual void OnResumePreviewEmailCandidateClick(ResumePreviewEmailCandidateClickEventArgs e)
		{
			if (ResumePreviewEmailCandidateClick != null)
				ResumePreviewEmailCandidateClick(this, e);
		}


		public event ResumePreviewEmailResumeCandidateClickHandler ResumePreviewEmailResumeCandidateClick;

		/// <summary>
		/// Raises the <see cref="E:ResumePreviewEmailCandidateClick"/> event.
		/// </summary>
		/// <param name="e">The <see cref="Focus.Web.Code.Controls.User.ResumePreviewModal.ResumePreviewEmailCandidateClickEventArgs"/> instance containing the event data.</param>
		protected virtual void OnResumePreviewEmailResumeCandidateClick(ResumePreviewEmailResumeCandidateClickEventArgs e)
		{
			if (ResumePreviewEmailResumeCandidateClick != null)
				ResumePreviewEmailResumeCandidateClick(this, e);
		}

		public event ResumePreviewCandidatesLikeMeClickHandler ResumePreviewCandidatesLikeMeClick;

		/// <summary>
		/// Raises the <see cref="E:ResumePreviewCandidatesLikeMeClick"/> event.
		/// </summary>
		/// <param name="e">The <see cref="ResumePreviewModal.ResumePreviewCandidatesLikeMeEventArgs"/> instance containing the event data.</param>
		protected virtual void OnResumePreviewCandidatesLikeMeClick(ResumePreviewCandidatesLikeMeEventArgs e)
		{
			if (ResumePreviewCandidatesLikeMeClick != null)
				ResumePreviewCandidatesLikeMeClick(this, e);
		}

		public event ResumePreviewFlagToggledHandler ResumePreviewFlagToggled;

		protected virtual void OnResumePreviewFlagToggled(EventArgs e)
		{
			if (ResumePreviewFlagToggled != null)
				ResumePreviewFlagToggled(this, e);
		}

		public event ResumePreviewNoteSavedHandler ResumePreviewNoteSaved;

		protected virtual void OnResumePreviewNoteSaved(EventArgs e)
		{
			if (ResumePreviewNoteSaved != null)
				ResumePreviewNoteSaved(this, e);
		}

		#endregion

		#region Delegates

		public delegate void ResumePreviewEmailCandidateClickHandler(object o, ResumePreviewEmailCandidateClickEventArgs e);

		public delegate void ResumePreviewEmailResumeCandidateClickHandler(object o, ResumePreviewEmailResumeCandidateClickEventArgs e);

		public delegate void ResumePreviewCandidatesLikeMeClickHandler(object o, ResumePreviewCandidatesLikeMeEventArgs e);

		public delegate void ResumePreviewFlagToggledHandler(object o, EventArgs e);

		public delegate void ResumePreviewNoteSavedHandler(object o, EventArgs e);

		#endregion

		#region EventArgs

		public class ResumePreviewEmailCandidateClickEventArgs : EventArgs
		{
			public readonly long CandidateId;
			public readonly string CandidateName;
			public readonly ApplicationStatusTypes ApplicationStatus;
			public readonly ResumeView Candidate;

			public ResumePreviewEmailCandidateClickEventArgs(long candidateId, string candidateName, ApplicationStatusTypes applicationStatus, ResumeView candidate)
			{
				CandidateId = candidateId;
				CandidateName = candidateName;
				ApplicationStatus = applicationStatus;
				Candidate = candidate;
			}
		}

		public class ResumePreviewCandidatesLikeMeEventArgs : EventArgs
		{
			public readonly long CandidateId;

			public ResumePreviewCandidatesLikeMeEventArgs(long candidateId)
			{
				CandidateId = candidateId;
			}
		}

		public class ResumePreviewEmailResumeCandidateClickEventArgs : EventArgs
		{
			public readonly long ResumeId;

			public ResumePreviewEmailResumeCandidateClickEventArgs(long resumeId)
			{
				ResumeId = resumeId;
			}
		}

		#endregion
	}
}
