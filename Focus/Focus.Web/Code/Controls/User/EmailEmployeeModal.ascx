﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailEmployeeModal.ascx.cs" Inherits="Focus.Web.Code.Controls.User.EmailEmployeeModal" %>

<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="EmailEmployeeModalPopup" runat="server" BehaviorID="EmailEmployeeModalPanel"
												TargetControlID="ModalDummyTarget"
												PopupControlID="EmailEmployeeModalPanel" 
												PopupDragHandleControlID="EmailEmployeeModalPanel"
												BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />

<asp:Panel ID="EmailEmployeeModalPanel" runat="server" CssClass="modal" Style="display:none">
		<h2><%= HtmlLocalise("Title.Text", "Email #BUSINESS#:LOWER(es)")%></h2>
  <p>
    <%= HtmlInFieldLabel(EmailSubjectTextBox.ClientID, "EmailSubject.InlineLabel", "type your subject here", 350)%>
		<asp:TextBox ID="EmailSubjectTextBox" runat="server" MaxLength="200" Width="350px" />
  </p>
	<p>
		<%= HtmlInFieldLabel(EmailBodyTextBox.ClientID, "EmailBody.InlineLabel", "type your message here", 350)%>
		<asp:TextBox ID="EmailBodyTextBox" runat="server" TextMode="MultiLine" Rows="10" Width="350px"  />
	</p>
	<p><asp:CheckBox ID="BCCMeCheckBox" runat="server" /></p>
	<p><asp:RequiredFieldValidator ID="EmailSubjectRequired" runat="server" ControlToValidate="EmailSubjectTextBox" SetFocusOnError="true" CssClass="error" /><br/>
	<asp:RequiredFieldValidator ID="EmailBodyRequired" runat="server" ControlToValidate="EmailBodyTextBox" SetFocusOnError="true" CssClass="error" /></p>
	<p>
		<asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" Text="Cancel" />
		<asp:Button ID="SendMessageButton" runat="server" CssClass="button3" OnClick="SendMessageButton_Clicked" Text="Send" />		
	</p>
</asp:Panel>

<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" ClientIDMode="AutoID" />

<script type="text/javascript">
  Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
    $('.inFieldLabel > label').inFieldLabels();
  });
</script>