﻿using System;
using Focus.Core;
using Focus.Services;
using Framework.Core;

namespace Focus.Web.Code.Controls.User
{
	public partial class ErrorModal : UserControlBase
	{
		public string Title { get; set; }
		public string Details { get; set; }
		public string Height { get; set; }
		public string Width { get; set; }
		protected string CloseJavascript { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Width = "400px";
            Height = "20px";
        }

		/// <summary>
		/// Shows the specified close link.
		/// </summary>
		/// <param name="closeLink">The close link.</param>
		/// <param name="height">The height.</param>
		/// <param name="width">The width.</param>
		/// <param name="top">The top.</param>
		public void Show(string closeLink = "", int height = 20, int width = 400, int top = 210)
		{
			var lastError = App.GetSessionValue<string>(Constants.StateKeys.PageError);
			
			if(lastError.IsNullOrEmpty()) throw new Exception("The last handled error could not be found.");

			Show(lastError, closeLink, height, width);
		}

		/// <summary>
		/// Shows the specified error message.
		/// </summary>
		/// <param name="errorMessage">The error message.</param>
		/// <param name="closeLink">The close link.</param>
		/// <param name="height">The height.</param>
		/// <param name="width">The width.</param>
		public void Show(string errorMessage, string closeLink = "", int height = 20, int width = 400)
		{
			Title = CodeLocalise("Error.Title","Error");
			Details = errorMessage;
			
			Height = string.Format("{0}px", height);
			Width = string.Format("style='width: {0}px;'", width);

			CloseJavascript = closeLink.IsNotNullOrEmpty() ? string.Format("window.location='{0}'", closeLink) 
																											: string.Format("$find('{0}').hide(); return false;", ErrorModalPopup.ClientID);
			
			ErrorModalPopup.Show();
		}
	}
}