﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion


#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Focus.Common.Code;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Services.Core.Extensions;
using Focus.Web.Code;

using DistanceUnits = Focus.Core.Models.Career.DistanceUnits;

using Framework.Core;

#endregion


namespace Focus.Web.Code.Controls.User
{
	public partial class JobPostingSearchResults : UserControlBase
	{
		#region Properties

		private int _searchResultCount;

		public long JobSeekerId
		{
			get { return GetViewStateValue<long>("JobSearchResults:JobSeekerId"); }
			set { SetViewStateValue("JobSearchResults:JobSeekerId", value); }
		}

		protected List<PostingSearchResultView> Result
		{
			get { return App.GetSessionValue<List<PostingSearchResultView>>("JobSearchResults:Result"); }
			set { App.SetSessionValue("JobSearchResults:Result", value); }
		}

		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
				BindDropdowns();

				// If coming from explorer we may have specific explorer criteria
				var criteria = App.GetSessionValue("Career:SearchCriteria", new Focus.Core.Models.Career.SearchCriteria());

				BindKeywordAndLocationCriteria(criteria);
				LoadList(criteria);
				BindSearchResultList();
			}

			JobSeekerAlreadyReferredLabel.Visible = false;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			RadiusValidator.ErrorMessage = CodeLocalise("RadiusLocation.RequiredErrorMessage", "Both radius and 5 digit zip code is required");
			JobSeekerAlreadyReferredLabel.Text = CodeLocalise("JobSeekerAlreadyReferred.Text", "#CANDIDATETYPE# has already been referred to this job.");
		}

		/// <summary>
		/// Binds the dropdowns.
		/// </summary>
		private void BindDropdowns()
		{
			ddlRadius.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses), null, CodeLocalise("Radius.TopDefault", "- select radius -"));
			BindPosingKeywordScopeDropDown(PostingKeywordScopes.Anywhere);
		}

		/// <summary>
		/// Binds the posing keyword scope drop down.
		/// </summary>
		/// <param name="selectedValue">The selected value.</param>
		private void BindPosingKeywordScopeDropDown(PostingKeywordScopes selectedValue)
		{
			ddlSearchLocation.Items.Clear();

			ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.Anywhere, "Anywhere");
			ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.JobDescription, "Job Description");
			ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.Employer, "#BUSINESS#");
			ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.JobTitle, "Job Title");

			if (selectedValue.IsNotNull())
				ddlSearchLocation.SelectValue(selectedValue.ToString());
		}

		/// <summary>
		/// Binds the keyword and location criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void BindKeywordAndLocationCriteria(Focus.Core.Models.Career.SearchCriteria criteria)
		{
			if (criteria.JobLocationCriteria.IsNotNull())
			{
				var radius = criteria.JobLocationCriteria.Radius;
				if (radius.IsNotNull())
				{
					long? radiusId = 0;

					if (radius.Distance.IsNotNull())
					{
						radiusId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses).Where(x => x.ExternalId == radius.Distance.ToString(CultureInfo.CurrentUICulture)).Select(x => x.Id).FirstOrDefault();
					}

					if (radiusId > 0)
						ddlRadius.SelectValue(radiusId.GetValueOrDefault().ToString(CultureInfo.CurrentUICulture));

					txtZipCode.Text = radius.PostalCode;
				}
				else
					txtZipCode.Text = "";
			}

			if (criteria.KeywordCriteria.IsNotNull())
			{
				var keywordlocation = criteria.KeywordCriteria.SearchLocation;

				if (keywordlocation.IsNotNull())
					ddlSearchLocation.SelectValue(keywordlocation.ToString());

				SearchTermTextBox.Text = criteria.KeywordCriteria.KeywordText;
			}
		}

		/// <summary>
		/// Loads the list.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void LoadList(Focus.Core.Models.Career.SearchCriteria criteria)
		{
			Result = ServiceClientLocator.SearchClient(App).GetSearchResults(criteria);
		}

		/// <summary>
		/// Binds the search result list.
		/// </summary>
		private void BindSearchResultList()
		{
			SearchResultListView.DataBind();
			TopPager.Visible = BottomPager.Visible = (TopPager.TotalRowCount > 0);
			var displayContext = App.GetSessionValue<PageDisplayContext>("Career:DisplayContext");
			if (displayContext.IsNotNull() && displayContext.displaypreference.IsNotNull() && !IsPostBack)
			{
				// Reset paging back to first page
				displayContext.displaypreference.PageNumber = 0;
				App.SetSessionValue("Career:DisplayContext", displayContext);
			}
		}

		#region ObjectDataSource Method

		/// <summary>
		/// Gets the search results.
		/// </summary>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<PostingSearchResultView> GetSearchResults(string orderBy, int startRowIndex, int maximumRows)
		{
			var searchResults = new List<PostingSearchResultView>();

			if (Result.IsNotNull())
			{
				var sortDescending = false;

				if (orderBy.EndsWith(" DESC"))
				{
					sortDescending = true;
					orderBy = orderBy.Substring(0, orderBy.LastIndexOf(" DESC"));
				}

				switch (orderBy)
				{
					case "Rating":
						Result = sortDescending ? Result.OrderByDescending(x => x.Rank).ToList() : Result.OrderBy(x => x.Rank).ToList();
						break;

					case "Date":
						Result = sortDescending ? Result.OrderByDescending(x => x.JobDate).ToList() : Result.OrderBy(x => x.JobDate).ToList();
						break;

					case "JobTitle":
						Result = sortDescending ? Result.OrderByDescending(x => x.JobTitle).ToList() : Result.OrderBy(x => x.JobTitle).ToList();
						break;

					case "Employer":
						Result = sortDescending ? Result.OrderByDescending(x => x.Employer).ToList() : Result.OrderBy(x => x.Employer).ToList();
						break;

					case "JobLocation":
						Result = sortDescending ? Result.OrderByDescending(x => x.Location).ToList() : Result.OrderBy(x => x.Location).ToList();
						break;

				}

				searchResults = (startRowIndex == 0) ? Result.Take(maximumRows).ToList() : Result.Skip(startRowIndex).Take(maximumRows).ToList();
				_searchResultCount = Result.Count();
			}

			return searchResults;
		}

		/// <summary>
		/// Gets the referrals count.
		/// </summary>
		/// <returns></returns>
		public int GetSearchResultCount()
		{
			return _searchResultCount;
		}

		#endregion

		/// <summary>
		/// Handles the Sorting event of the SearchResultListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ListViewSortEventArgs" /> instance containing the event data.</param>
		protected void SearchResultListView_Sorting(object sender, ListViewSortEventArgs e)
		{
			var sortImageUrl = e.SortDirection == SortDirection.Ascending ? UrlBuilder.TriangleUpIcon() : UrlBuilder.TriangleDownIcon();
			var ratingSortImage = (Image)SearchResultListView.FindControl("RatingSortImage");
			var dateSortImage = (Image)SearchResultListView.FindControl("DateSortImage");
			var jobTitleSortImage = (Image)SearchResultListView.FindControl("JobTitleSortImage");
			var employerSortImage = (Image)SearchResultListView.FindControl("EmployerSortImage");
			var jobLocationSortImage = (Image)SearchResultListView.FindControl("JobLocationSortImage");

			switch (e.SortExpression)
			{
				case "Rating":
					ratingSortImage.ImageUrl = sortImageUrl;
					ratingSortImage.Visible = true;
					dateSortImage.Visible = false;
					jobTitleSortImage.Visible = false;
					employerSortImage.Visible = false;
					jobLocationSortImage.Visible = false;
					break;

				case "Date":
					dateSortImage.ImageUrl = sortImageUrl;
					ratingSortImage.Visible = false;
					dateSortImage.Visible = true;
					jobTitleSortImage.Visible = false;
					employerSortImage.Visible = false;
					jobLocationSortImage.Visible = false;
					break;

				case "JobTitle":
					jobTitleSortImage.ImageUrl = sortImageUrl;
					ratingSortImage.Visible = false;
					dateSortImage.Visible = false;
					jobTitleSortImage.Visible = true;
					employerSortImage.Visible = false;
					jobLocationSortImage.Visible = false;
					break;

				case "Employer":
					employerSortImage.ImageUrl = sortImageUrl;
					ratingSortImage.Visible = false;
					dateSortImage.Visible = false;
					jobTitleSortImage.Visible = false;
					employerSortImage.Visible = true;
					jobLocationSortImage.Visible = false;
					break;

				case "JobLocation":
					jobLocationSortImage.ImageUrl = sortImageUrl;
					ratingSortImage.Visible = false;
					dateSortImage.Visible = false;
					jobTitleSortImage.Visible = false;
					employerSortImage.Visible = false;
					jobLocationSortImage.Visible = true;
					break;
			}
		}

		/// <summary>
		/// Handles the PagePropertiesChanging event of the SearchResultListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.PagePropertiesChangingEventArgs"/> instance containing the event data.</param>
		protected void SearchResultListView_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
		{
			var displayContext = App.GetSessionValue("Career:DisplayContext", new PageDisplayContext());
			if (displayContext.IsNotNull() && displayContext.displaypreference.IsNotNull())
			{
				displayContext.displaypreference.PageNumber = e.StartRowIndex / e.MaximumRows;
				displayContext.displaypreference.PageSize = e.MaximumRows;
			}
			else
			{
				displayContext = new PageDisplayContext
				{
					displaypreference = new DisplayPreference
					{
						PageNumber = e.StartRowIndex / e.MaximumRows,
						PageSize = e.MaximumRows
					}
				};
			}

			App.SetSessionValue("Career:DisplayContext", displayContext);
		}

		/// <summary>
		/// Handles the PageSizeChange event of the TopPager control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void TopPager_PageSizeChange(object sender, EventArgs e)
		{
			var ddlPageSizer = (DropDownList)sender;
			var pageSize = Convert.ToInt32(ddlPageSizer.SelectedValue);
			var displayContext = App.GetSessionValue("Career:DisplayContext", new PageDisplayContext());
			if (displayContext.IsNotNull() && displayContext.displaypreference.IsNotNull())
				displayContext.displaypreference.PageSize = pageSize;
			App.SetSessionValue("Career:DisplayContext", displayContext);
		}

		/// <summary>
		/// Gets the stars.
		/// </summary>
		/// <param name="score">The score.</param>
		/// <returns></returns>
		protected string GetStars(int score)
		{
			var starRating = score.ToStarRating(App.Settings.StarRatingMinimumScores);
			var output = string.Empty;
			for (var j = 1; j <= starRating; j++) output += string.Format("<img src='{0}' alt='Star Icon On' />", UrlBuilder.StarIconOn());
			for (var j = 1; j <= 5 - starRating; j++) output += string.Format("<img src='{0}' alt='Star Icon Off' />", UrlBuilder.StarIconOff());

			return output;
		}

		/// <summary>
		/// Handles the Click event of the lnkReviewSearchCriteria control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void lnkReviewSearchCriteria_Click(object sender, EventArgs e)
		{
			var searchcriteria = App.GetSessionValue<Focus.Core.Models.Career.SearchCriteria>("Career:SearchCriteria");
			lblReviewSearchCriteriaInfo.Text = CommonUtilities.GetCriteria(searchcriteria);
			CriteriaCloseButton.Text = CodeLocalise("Global.Close.Text", "Close");
			ChangeCriteriaButton.Text = CodeLocalise("ChangeCriteria.Text", "Change criteria");
			ReviewSearchCriteriaModalPopup.Show();
		}

		/// <summary>
		/// Handles the Click event of the ChangeCriteriaButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ChangeCriteriaButton_Click(object sender, EventArgs e)
		{
			if (App.Settings.Module == FocusModules.Assist)
				Response.Redirect(UrlBuilder.AssistSearchJobPostings(JobSeekerId));
			else if (App.Settings.Module == FocusModules.Talent)
				Response.Redirect(UrlBuilder.TalentJobSearch(JobSeekerId));
		}



		/// <summary>
		/// Handles the ItemDataBound event of the SearchResultListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ListViewItemEventArgs" /> instance containing the event data.</param>
		protected void SearchResultListView_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var posting = (PostingSearchResultView)e.Item.DataItem;

			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				var jobtitle = (HyperLink)e.Item.FindControl("JobTitleHyperLink");
				var amIGoodMatchHyperLink = (LinkButton)e.Item.FindControl("AmIGoodMatchHyperLink");
				var findMoreJobsHyperLink = (LinkButton)e.Item.FindControl("FindMoreJobsHyperLink");
				var doNotDisplayHyperLink = (LinkButton)e.Item.FindControl("DoNotDisplayHyperLink");

				string routeUrlName = App.Settings.Module == FocusModules.Assist ? "AssistJobPosting" : "TalentJobPosting";
				
				jobtitle.NavigateUrl = GetRouteUrl(routeUrlName,
					new
					{
						jobid = posting.Id,
						fromURL = (App.Settings.Module == FocusModules.Assist ? "AssistJobSearchResultsPage" : "TalentJobSearchResultsPage"),
						jobseekerid = JobSeekerId,
						page = TopPager.CurrentPage,
						rank = posting.Rank,
						originid = posting.OriginId
					});
				
				


				amIGoodMatchHyperLink.CommandArgument = posting.Id;  // NavigateUrl = GetRouteUrl("JobMatching", new { jobid = posting.ID, fromURL = "JobSearchResults" });
				findMoreJobsHyperLink.CommandArgument = posting.Id;
				doNotDisplayHyperLink.CommandArgument = posting.Id;

				var amIGoodMatchTrigger = new AsyncPostBackTrigger { ControlID = amIGoodMatchHyperLink.UniqueID };
				SearchResultUpdatePanel.Triggers.Add(amIGoodMatchTrigger);

				var findMoreJobsTrigger = new AsyncPostBackTrigger { ControlID = findMoreJobsHyperLink.UniqueID };
				SearchResultUpdatePanel.Triggers.Add(findMoreJobsTrigger);

				var doNotDisplayTrigger = new AsyncPostBackTrigger { ControlID = doNotDisplayHyperLink.UniqueID };
				SearchResultUpdatePanel.Triggers.Add(doNotDisplayTrigger);

				//apply row shading to Talent jobs to make them stand out from spidered jobs
				// if posting.OriginId = 999 then it's a spidered job, else it's Talent
				if (posting.OriginId.IsTalentOrigin(App.Settings))
				{
					((HtmlTableRow)e.Item.FindControl("JobSearchResultRow")).Attributes.Add("class", "nonSpideredJobSearchResult");
				}

				((HtmlTableCell)e.Item.FindControl("ActionCell")).Visible = App.Settings.Module == FocusModules.Assist;
			}
		}

		/// <summary>
		/// Handles the Click event of the AmIGoodMatchHyperLink control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void AmIGoodMatchHyperLink_Click(object sender, EventArgs e)
		{
			if (App.User.IsAuthenticated)
			{
				var lbtnName = (LinkButton)sender;
				var lensPostingId = lbtnName.CommandArgument;

				var post = (lensPostingId.IsNotNullOrEmpty()) ? ServiceClientLocator.PostingClient(App).GetJobPosting(lensPostingId) : null;

				if (post.IsNotNull() && post.PostingXml.IsNotNullOrEmpty())
					App.SetSessionValue("Career:XMLPosting", post.PostingXml);

				// Log self service
				if (JobSeekerId != 0)
					ServiceClientLocator.CoreClient(App).SaveSelfService(ActionTypes.ViewJobInsights, actingOnBehalfOfPersonId: JobSeekerId);


				Response.RedirectToRoute("AssistJobMatching", new { jobid = lbtnName.CommandArgument, fromURL = "AssistJobSearchResults", jobseekerid = JobSeekerId });
			}
			else
				ConfirmationModal.Show("", "", "", "", "", ""); //populate params

		}

		/// <summary>
		/// Handles the Click event of the FindMoreJobsHyperLink control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void FindMoreJobsHyperLink_Click(object sender, EventArgs e)
		{

			var criteria = App.GetSessionValue<Focus.Core.Models.Career.SearchCriteria>("Career:SearchCriteria");
			var lbtnName = (LinkButton)sender;

			criteria = new Focus.Core.Models.Career.SearchCriteria
			{
				JobLocationCriteria = criteria.JobLocationCriteria,
				ReferenceDocumentCriteria = new ReferenceDocumentCriteria
				{
					DocumentType = DocumentType.Posting,
					DocumentId = lbtnName.CommandArgument
				},
				SearchType = PostingSearchTypes.PostingsLikeThis
			};

			App.SetSessionValue("Career:SearchCriteria", criteria);
			LoadList(criteria);
			BindSearchResultList();
		}

		/// <summary>
		/// Handles the Click event of the DoNotDisplayHyperLink control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DoNotDisplayHyperLink_Click(object sender, EventArgs e)
		{
			if (App.User.IsAuthenticated)
			{
				var lbtnName = (LinkButton)sender;

				try
				{
					var lensPostingId = lbtnName.CommandArgument;

					if (lensPostingId.IsNotNullOrEmpty() && ServiceClientLocator.OrganizationClient(App).SetDoNotDisplay(lensPostingId))
					{
						Result = Result.Where(x => x.Id != lbtnName.CommandArgument.ToString(CultureInfo.CurrentUICulture)).ToList();
						BindSearchResultList();
					}
				}
				catch (ApplicationException ex)
				{
					MasterPage.ShowError(AlertTypes.Error, ex.Message);
				}
			}
			else
				ConfirmationModal.Show("", "", "", "", "", ""); //populate params
		}

		/// <summary>
		/// Handles the Click event of the btnGO control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void btnGO_Click(object sender, EventArgs e)
		{
			if (JobSeekerId > 0)
				ServiceClientLocator.CandidateClient(App).RegisterFindJobsForSeeker(JobSeekerId);

			var criteria = App.GetSessionValue<Focus.Core.Models.Career.SearchCriteria>("Career:SearchCriteria", null);

			#region Job Location Criteria

			//criteria.Remove(criteria.Where(x => x is JobLocationCriterion).FirstOrDefault());
			if (txtZipCode.TextTrimmed() != "")
			{
				if (criteria.JobLocationCriteria.IsNull())
					criteria.JobLocationCriteria = new JobLocationCriteria();

				var joblocation = criteria.JobLocationCriteria;

				// Reset any state/area criteria
				joblocation.Area = null;
				joblocation.SelectedStateInCriteria = null;

				var lookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses).FirstOrDefault(x => x.Id == ddlRadius.SelectedValueToLong());
				if (lookup != null)
				{
					var radiusExternalId = (lookup.IsNotNull()) ? lookup.ExternalId : "0";
					var radius = radiusExternalId.ToLong();

					joblocation.Radius = new RadiusCriteria
					{
						Distance = (radius.HasValue) ? radius.Value : 0,
						DistanceUnits = DistanceUnits.Miles,
						PostalCode = txtZipCode.TextTrimmed()
					};
				}
			}
			else
			{
				var joblocation = criteria.JobLocationCriteria;

				if (joblocation.IsNotNull())
					joblocation.Radius = null;
			}

			#endregion

			#region Keyword Criteria

			criteria.KeywordCriteria = null;

			if (SearchTermTextBox.TextTrimmed() != "")
			{
				var searchLocation = ddlSearchLocation.SelectedValueToEnum(PostingKeywordScopes.Anywhere);
				criteria.KeywordCriteria = new KeywordCriteria(SearchTermTextBox.TextTrimmed()) { SearchLocation = searchLocation };
			}

			#endregion

			App.SetSessionValue("Career:SearchCriteria", criteria);
			LoadList(criteria);
			BindSearchResultList();
		}

		/// <summary>
		/// Handles the OkCommand event of the ReviewSearchCriteriaConfirmationModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		public void ReviewSearchCriteriaConfirmationModal_OkCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "REVIEW_CRITERIA")
				Response.RedirectToRoute("JobSearchCriteria", new { Control = "searchjobpostings" });
		}

		/// <summary>
		/// Handles the OnLayoutCreated event of the SearchResultListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void SearchResultListView_OnLayoutCreated(object sender, EventArgs e)
		{
			((ListView)sender).FindControl("ActionHeader").Visible = App.Settings.Module == FocusModules.Assist;
		}
	}
}