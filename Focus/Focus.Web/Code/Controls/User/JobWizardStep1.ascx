﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobWizardStep1.ascx.cs" Inherits="Focus.Web.Code.Controls.User.JobWizardStep1" %>

<%@ Register src="~/Code/Controls/User/UploadLogoModal.ascx" tagname="UploadLogoModal" tagprefix="uc" %>


<table style="width:100%;" role="presentation">
	<tr>
		<td style="width:48%; vertical-align:top;">
			<table style="width:100%;" role="presentation">
			  <tr  id="JobTypeHeaderRow" runat="server">
					<td><h2>1. <%= HtmlLocalise("JobType.Header", "Job Type")%></h2></td>
				</tr>
				<tr id="JobTypeRow" runat="server">
					<td>
						<table style="width:100%;" role="presentation">
							<tr>
							  <td>
								<div><%= HtmlLabel(JobTypeDropDownList,"OccupationsOr.Label", "Or select from the following")%></div>
							   <asp:DropDownList ID="JobTypeDropDownList" runat="server" Width="400" ClientIDMode="Static" />
                 </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
					  <h2><asp:literal runat="server" ID="NumberingForJobTitle">2.</asp:literal> 
              <span id="JobTitleHeader"><%= HtmlLocalise("JobTitle.Header", "Job Title")%></span>
              <span id="InternshipTitleHeader" style="display:none"><%= HtmlLocalise("InternshipTitle.Header", "Internship Title")%></span>
            </h2>
					</td>
				</tr>
				<tr>
					<td>
						<table style="width:100%;" role="presentation">
							<tr>
								<td style="padding-right:4px; width:95%;">
									<%= HtmlInFieldLabel("JobTitleTextBox", "JobTitle.Label", "Type new job title here then click find", 450)%>
									<asp:TextBox ID="JobTitleTextBox" runat="server" Width="100%" ClientIDMode="Static" AutoCompleteType="Disabled" />
									<act:AutoCompleteExtender ID="JobTitleAutoCompleteExtender" BehaviorID="JobTitleBehaviour" runat="server" TargetControlID="JobTitleTextBox" MinimumPrefixLength="3" 
																						CompletionListCssClass="autocompleteCompletionList"  CompletionInterval="100" CompletionSetCount="15" UseContextKey="true"
																						ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetJobTitles" CompletionListHighlightedItemCssClass="autocompleteHighlightedListitem" CompletionListItemCssClass="autocompleteListitem" /><br />
									<asp:CustomValidator ID="JobTitleRequired" runat="server" ClientValidationFunction="ValidateJobTitle" ControlToValidate="JobTitleTextBox" ValidateEmptyText="True" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
									<asp:CustomValidator ID="InternshipJobTitleRequired" runat="server" ClientValidationFunction="ValidateInternshipTitle" ControlToValidate="JobTitleTextBox" ValidateEmptyText="True" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								</td>
								<td style="vertical-align:top; padding-left:4px">
									<asp:Button ID="UpdateOccupationsButton" runat="server" SkinID="Button2" ClientIDMode="Static" OnClick="UpdateOccupationsButton_Click" CausesValidation="false" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<asp:Panel ID="OccupationsPanel" runat="server" ClientIDMode="Static">
							<div><%= HtmlLabel("OccupationSelection.Label", "Which of the following best describes your job posting?")%></div>
							<br />
							<asp:UpdatePanel ID="OccupationsUpdatePanel" runat="server" UpdateMode="Conditional">
								<ContentTemplate>
								  <asp:Literal runat="server" ID="TestROnetInformation"></asp:Literal>
									<asp:RadioButtonList ID="OccupationsRadioButtonList" runat="server" ClientIDMode="Static" role="presentation"/>
								</ContentTemplate>
								<Triggers>
									<asp:AsyncPostBackTrigger ControlID="UpdateOccupationsButton" EventName="Click" />
								</Triggers>
							</asp:UpdatePanel>
							<br />
							<br />
							<div><%= HtmlLabel(OccupationsFamilyDropDownList,"OccupationsOr.Label", "Or select from the following")%></div>
							<asp:DropDownList ID="OccupationsFamilyDropDownList" runat="server" Width="400" ClientIDMode="Static" /><br /> 
							<br />
							<focus:LocalisedLabel runat="server" ID="OccupationsDropDownListLabel" AssociatedControlID="OccupationsDropDownList" LocalisationKey="OccupationsDropDown.Label" DefaultText="Occupations" CssClass="sr-only"/>
							<focus:AjaxDropDownList ID="OccupationsDropDownList" runat="server"  Width="400" ClientIDMode="Static" />
							<act:CascadingDropDown ID="OccupationsDropDownListCascadingDropDown" runat="server" TargetControlID="OccupationsDropDownList" ParentControlID="OccupationsFamilyDropDownList" 
																		 ServicePath="~/Services/AjaxService.svc" Category="Occupations" /> 
							<br />
							<br />
						</asp:Panel>
						<asp:CustomValidator runat="server" id="OccupationsValidator" ClientValidationFunction="ValidateOccupations" Display="Dynamic" CssClass="error" />
					</td>
				</tr>
       </table>
    </td>
		<td style="width:4%;"></td>
		<td style="width:48%; vertical-align:top;">
			<asp:Panel ID="CompanyInformationPanel" runat="server" ClientIDMode="Static">
				<asp:UpdatePanel ID="CompanyInformationUpdatePanel" runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<table style="width:100%;" role="presentation">
							<tr>
								<td><h2><asp:literal runat="server" ID="NumberingForCompany">3.</asp:literal> <%= HtmlLocalise("CompanyInformation.Header", "#BUSINESS# Information")%></h2></td>
							</tr>
							<tr>
								<td>
									<focus:LocalisedLabel runat="server" ID="CompanyDropDownLabel" AssociatedControlID="CompanyDropDown" LocalisationKey="CompanyDropDown.Label" DefaultText="Company" CssClass="sr-only"/>
									<asp:DropDownList ID="CompanyDropDown" runat="server" Width="100%" ClientIDMode="Static" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="CompanyDropDown_SelectedIndexChanged" /><br />
									<asp:RequiredFieldValidator ID="CompanyRequired" runat="server" ControlToValidate="CompanyDropDown" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
									<br />
								</td>	
							</tr>
              <tr>
								<td>
									<asp:CheckBox ID="ConfidentialCompanyCheckBox" runat="server" ClientIDMode="Static" CausesValidation="false" /><br />
									<br />
								</td>	
							</tr>
							<tr>
								<td>
									<div><%= HtmlLabel(CompanyDescriptionDropDownList,"CompanyDescription.Label", "#BUSINESS# Description")%></div>
									<focus:AjaxDropDownList runat="server" ID="CompanyDescriptionDropDownList" Width="350" ClientIDMode="Static"/>
									<asp:Panel id="CompanyDescriptionTitlePanel" runat="server" ClientIDMode="Static">
										<br />
										<%= HtmlInFieldLabel("CompanyDescriptionTitleTextBox", "CompanyDescriptionTitle.Label", "Type description name", 350)%>
										<asp:TextBox ID="CompanyDescriptionTitleTextBox" runat="server" Width="350" ClientIDMode="Static" MaxLength="200" />&nbsp;
										<asp:Button ID="CompanyDescriptionSaveButton" runat="server" OnClick="CompanyDescriptionSaveButton_Click" class="button3 right" ValidationGroup="AddCompanyDescription" />
										<br />
										<asp:RequiredFieldValidator ID="CompanyDescriptionTitleRequired" runat="server" ControlToValidate="CompanyDescriptionTitleTextBox" SetFocusOnError="true" Display="Static" CssClass="error" ValidationGroup="AddCompanyDescription" />
									</asp:Panel>
									<br />
									<br />
									<div><%= HtmlLabel(CompanyDescriptionPostingPositionDropDownList,"CompanyDescriptionPostion.Label", "Show Description")%></div>
									<focus:AjaxDropDownList ID="CompanyDescriptionPostingPositionDropDownList" runat="server" Width="350" ClientIDMode="Static" />
									<br />
									<br />
									<focus:LocalisedLabel runat="server" ID="CompanyDescriptionTextBoxLabel" AssociatedControlID="CompanyDescriptionTextBox" LocalisationKey="ComapnyDescription.Label" DefaultText="CompanyDescription" CssClass="sr-only"/>
									<asp:TextBox ID="CompanyDescriptionTextBox" runat="server" TextMode="MultiLine" Rows="10" ClientIDMode="Static" /><br />
									<asp:RequiredFieldValidator ID="CompanyDescriptionRequired" runat="server" ControlToValidate="CompanyDescriptionTextBox" SetFocusOnError="true" CssClass="error" ValidationGroup="AddCompanyDescription" Display="Dynamic" />
									<asp:CustomValidator runat="server" id="CompanyDescriptionCustomValidator" ControlToValidate="CompanyDescriptionTextBox" ValidateEmptyText="true" ClientValidationFunction="ValidateCompanyDescriptionSaved" Display="Dynamic" CssClass="error" />
									<br />

									<h2><asp:literal runat="server" ID="NumberingForLogo">4.</asp:literal> <%= HtmlLocalise("CompanyLogo.Header", "#BUSINESS# Logo")%></h2>
									<div><%= HtmlLabel(CompanyLogoDropDownList,"CompanyLogo.Instructions", "Select a #BUSINESS#:LOWER logo to use with your job listing")%></div>
									<asp:DropDownList runat="server" ID="CompanyLogoDropDownList" Width="350" ClientIDMode="Static" />
									<asp:Button ID="CompanyLogoUploadButton" runat="server" class="button3 right" OnClick="CompanyLogoUploadButton_Click" ClientIDMode="Static" CausesValidation="false" />

									<uc:UploadLogoModal ID="UploadLogo" runat="server" OnOkClick="UploadLogo_OkClick" />
								</td>
							</tr>
						</table>
					</ContentTemplate>
					<Triggers>
						<asp:AsyncPostBackTrigger ControlID="CompanyDropDown" EventName="SelectedIndexChanged" />
						<asp:AsyncPostBackTrigger ControlID="CompanyDescriptionSaveButton" EventName="Click" />
						<asp:AsyncPostBackTrigger ControlID="CompanyLogoUploadButton" EventName="Click" />
					</Triggers>
				</asp:UpdatePanel>
			</asp:Panel>
		</td>
	</tr>
</table>

<script type="text/javascript">
  var prm = Sys.WebForms.PageRequestManager.getInstance();
  if (!prm.get_isInAsyncPostBack()) {
    prm.add_pageLoaded(HandleCompanyDescriptionData);
  }

  prm.add_endRequest(SetupAccordionNew);
  
  Sys.Extended.UI.AutoCompleteBehavior.prototype.set_enabled = function (value) {
    try {
        $removeHandler(this.get_element(), "keydown", this._keyDownHandler);
    } catch (error) {} //just escape error if handler already removed
    this._timer.set_enabled( !! value);
    if (value) {
        this._keyDownHandler = Function.createDelegate(this, this._onKeyDown);
    } else {
        this._keyDownHandler = Function.createDelegate(this, function () {});
    }
    $addHandler(this.get_element(), "keydown", this._keyDownHandler);
};

  $(document).ready(function() {
	  WebForm_AutoFocus("Y");
    var companyDropDown = $("#CompanyDropDown").val();
    $("#CompanyLogoDropDownList").attr('disabled', (companyDropDown == <%= ConfidentialCompany %>));
    $("#CompanyLogoUploadButton").attr('disabled', (companyDropDown == <%= ConfidentialCompany %>));

    $("#UpdateOccupationsButton").attr('disabled', ($("#JobTitleTextBox").val().length < 3));

    $("#CompanyDescriptionTitlePanel").hide();


    if ($("#JobTypeDropDownList").length > 0) {
      if ($("#JobTypeDropDownList")[0].selectedIndex > 1) {
        $("#JobTitleHeader").hide();
        $("#InternshipTitleHeader").show();
        $("label[for='JobTitleTextBox']").text('<%=CodeLocalise("InternshipTitle.Label", "Type new internship title here") %>');
        $("#CompanyInformationPanel").show();
        $("#OccupationsPanel").hide();
        $("#JobTitleTextBox").attr('AutoCompleteType', 'disabled');
        $("#UpdateOccupationsButton").hide();
        ValidatorEnable($("#<%= OccupationsValidator.ClientID %>")[0], false);
      }
    }else {
	    var internshipTitle = $("#<%= InternshipJobTitleRequired.ClientID %>");
	    if (internshipTitle.length > 0) {
				ValidatorEnable(internshipTitle[0], false);		    
	    }
    }


    $("#JobTitleTextBox").keyup(function() {
      var jobTitle = $(this).val();
      $("#UpdateOccupationsButton").attr('disabled', (jobTitle.length < 3));
    });

    $("#UpdateOccupationsButton").click(function() {
      $("#OccupationsPanel").show();
    });
    
   $('#<%= OccupationsPanel.ClientID %>').on('click', 'input[type=radio]', function() {
      $("#OccupationsFamilyDropDownList").val("");
      $("#OccupationsDropDownList").val("");
      $("#OccupationsDropDownList").attr('disabled', true);
      $("#CompanyInformationPanel").show();
    });

    $("#OccupationsFamilyDropDownList").change(function() {
      $('input[name="<%= OccupationsRadioButtonList.UniqueID %>"]').attr('checked', false);
    });

    
      if ($("#JobTypeDropDownList").length > 0) {
        $("#JobTypeDropDownList").change(function() {
          if ($("#JobTypeDropDownList")[0].selectedIndex > 1) {
            $("#JobTitleHeader").hide();
            $("#InternshipTitleHeader").show();
            $("label[for='JobTitleTextBox']").text('<%=CodeLocalise("InternshipTitle.Label", "Type new internship title here") %>');

            $("#CompanyInformationPanel").show();
            $("#OccupationsPanel").hide();
            $find("JobTitleBehaviour").set_enabled(false);
            ValidatorEnable($("#<%= OccupationsValidator.ClientID %>")[0], false);
            $("#UpdateOccupationsButton").hide();
          } else {
            $("#JobTitleHeader").show();
            $("#InternshipTitleHeader").hide();
            $("label[for='JobTitleTextBox']").text('<%=CodeLocalise("JobTitle.Label", "Type new job title here then click find") %>')
            if (($("input[id^='OccupationsRadioButtonList_']:checked").val()) || ($("#OccupationsFamilyDropDownList").get(0).selectedIndex != "0")) {
              $("#OccupationsPanel").show();
              $("#CompanyInformationPanel").show();
            } else {
              $("#CompanyInformationPanel").hide();
            }

            $find("JobTitleBehaviour").set_enabled(true);
            document.getElementById("<%=OccupationsValidator.ClientID %>").enabled = true;
            $("#UpdateOccupationsButton").show();
          }

        });
      }
    
    $("#OccupationsDropDownList").change(function() {
      $('input[name="<%= OccupationsRadioButtonList.UniqueID %>"]').attr('checked', false);
      $("#CompanyInformationPanel").show();
    });

    $("#CompanyDropDown").change(function() {
      $("#CompanyLogoDropDownList").attr('disabled', ($(this).val() == <%= ConfidentialCompany %>));
      if ($(this).val() == <%= ConfidentialCompany %>) $("#CompanyLogoDropDownList").val('');
      $("#CompanyLogoUploadButton").attr('disabled', ($(this).val() == <%= ConfidentialCompany %>));
    });
  });

  function ValidateJobTitle(source, arguments) {

    if ($("#JobTypeDropDownList").length>0) {
      if ($("#JobTypeDropDownList")[0].selectedIndex <= 1 && $("#JobTitleTextBox").val().trim().length == 0) {
        arguments.IsValid = false;
      } else {
        arguments.IsValid = true;
      }
    } else {
      if ($("#JobTitleTextBox").val().trim().length == 0) {
        arguments.IsValid = false;
      } else {
        arguments.IsValid = true;
      }
    }
  }
  
  function ValidateInternshipTitle(source, arguments) {
    if ($("#JobTypeDropDownList")[0].selectedIndex > 1 && $("#JobTitleTextBox").val().trim().length == 0) {
      arguments.IsValid = false;
    }
    else {
       arguments.IsValid = true;
    }
  }

  function ValidateOccupations(source, arguments) {
    if (($("input[id^='OccupationsRadioButtonList_']:checked").val()) || ($("#OccupationsFamilyDropDownList").get(0).selectedIndex != "0" && $("#OccupationsDropDownList").get(0).selectedIndex != "0")) {
      arguments.IsValid = true;
    } else {
      arguments.IsValid = false;
    }
  }
  
  function ValidateCompanyDescriptionSaved(source, arguments) {
    arguments.IsValid = ($("#CompanyDescriptionDropDownList").val() != <%= AddNewCompanyDescription %>);
  }
  
  function HandleCompanyDescriptionData(source, arguments) {
      $("#CompanyDescriptionDropDownList").change(function() {
        if ($(this).val() == <%= AddNewCompanyDescription %>) {
          $("#CompanyDescriptionTitlePanel").show();
          $("#CompanyDescriptionTitleTextBox").val("");
        } else {
          $("#CompanyDescriptionTitlePanel").hide();

          if ($(this).val() != "") {
            // if this is a selection of an employer description then go and get it
            var options = {
              type: "POST",
              url: "<%= UrlBuilder.AjaxService() %>/GetBusinessUnitDescription",
              contentType: "application/json; charset=utf-8",
              dataType: "json",
              async: true,
              data: '{"businessUnitDescriptionId": "' + $(this).val() + '"}',
              success: function(response) {
                var results = response.d;
                $("#CompanyDescriptionTextBox").val(results);
              }
            };
            $.ajax(options);
          } else {
	          $("#CompanyDescriptionTextBox").val("");
          }
	        
        }
        return false;
      });
  }

	// FVN-2851
  window.WebForm_AutoFocus = window.WebForm_AutoFocus || (function(obj) {
  });
</script>

