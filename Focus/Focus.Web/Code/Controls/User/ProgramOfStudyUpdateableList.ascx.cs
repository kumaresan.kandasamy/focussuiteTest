﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Common.Extensions;
using Focus.Common;

using Framework.Core;

#endregion


namespace Focus.Web.Code.Controls.User
{
	public partial class ProgramOfStudyUpdateableList : UserControlBase
	{
		/// <summary>
		/// Gets or sets the items.
		/// </summary>
		/// <value>The items.</value>
		public List<KeyValuePair<string, string>> Items
		{
			get { return ProgramsOfStudyUpdateablePairs.Items; }
			set { ProgramsOfStudyUpdateablePairs.Items = value; }
		}

		/// <summary>
		/// Gets or sets the size of the max list.
		/// </summary>
		/// <value>The size of the max list.</value>
		public int MaxListSize
		{
			get { return ProgramsOfStudyUpdateablePairs.MaxListSize; }
			set { ProgramsOfStudyUpdateablePairs.MaxListSize = value; }
		}

    /// <summary>
    /// Whether or not the Program of Study Not Required box is checked or not
    /// </summary>
    public bool IsProgramOfStudyNotRequiredChecked
    {
      get { return ProgramOfStudyNotRequiredCheckBox.Checked; }
      set { ProgramOfStudyNotRequiredCheckBox.Checked = value; }
    }

    public bool HideAllDegreesOption { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is job wizard.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is job wizard; otherwise, <c>false</c>.
		/// </value>
		public bool IsJobWizard { get; set; }

    /// <summary>
    /// Whether the programs of study are required
    /// </summary>
    public bool ProgramsOfStudyRequired
    {
      get
      {
        return GetViewStateValue<bool>(string.Concat(ClientID,":ProgramsOfStudyRequired"));
      }
      set
      {
        SetViewStateValue(string.Concat(ClientID, ":ProgramsOfStudyRequired"), value);
      }
    }

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				LocaliseUI();
			  SetVisibility();

			  if (App.Settings.ShowProgramArea)
			    BindStudyProgramAreaDropDown();
			  else
			    BindAllDegreesDropDown();
			}

			else
			{
				// need to rebind the degree drop down as we are not using ajaxtoolkit cascading drop downs
        if (App.Settings.ShowProgramArea) 
          BindStudyProgramAreaDegreesDropDown();
			}

      if (App.Settings.ShowProgramArea)
			  RegisterJavascript();
		}

    /// <summary>
    /// Handles the PreRender event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_PreRender(object sender, EventArgs e)
    {
      ApplyValidation();
    }

		/// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear()
		{
			StudyProgramAreaDropDownList.SelectedIndex = -1;
		  StudyProgramAreaDegreesDropDownList.SelectedIndex = -1;
			Items = null;
		}

		/// <summary>
		/// Binds the study program area degrees drop down.
		/// 
		/// </summary>
		private void BindStudyProgramAreaDegreesDropDown()
		{
			if (!String.IsNullOrEmpty(StudyProgramAreaDropDownList.SelectedValue))
			{
			  var programAreaId = StudyProgramAreaDropDownList.SelectedValueToLong();
        var degrees = ServiceClientLocator.ExplorerClient(App).GetProgramAreaDegreeEducationLevels(programAreaId);

				StudyProgramAreaDegreesDropDownList.DataSource = degrees;
				StudyProgramAreaDegreesDropDownList.DataTextField = "DegreeEducationLevelName";
				StudyProgramAreaDegreesDropDownList.DataValueField = "DegreeEducationLevelId";
				StudyProgramAreaDegreesDropDownList.DataBind();
        if (!HideAllDegreesOption) StudyProgramAreaDegreesDropDownList.Items.AddLocalisedTopDefault("Global.Degree.AllDegrees", "All Degrees", string.Concat("-", StudyProgramAreaDropDownList.SelectedValue));
				StudyProgramAreaDegreesDropDownList.Items.AddLocalisedTopDefault("Global.Degree.TopDefault", "- select degree -");

				var selectedDegree = Request.Form[StudyProgramAreaDegreesDropDownList.UniqueID];
			  if (selectedDegree.IsNotNullOrEmpty())
			  {
			    var selectedDegreeId = long.Parse(selectedDegree);
          if (degrees.Any(d => d.DegreeEducationLevelId == selectedDegreeId) || selectedDegreeId == 0 - programAreaId)
			      StudyProgramAreaDegreesDropDownList.SelectedValue = selectedDegree;
			  }
			}
		}

    /// <summary>
    /// Binds all degrees to the drop down.
    /// </summary>
    private void BindAllDegreesDropDown()
    {
      var degrees = ServiceClientLocator.ExplorerClient(App).GetProgramAreaDegreeEducationLevels(null);

      StudyProgramAreaDegreesDropDownList.DataSource = degrees;
      StudyProgramAreaDegreesDropDownList.DataTextField = "DegreeEducationLevelName";
      StudyProgramAreaDegreesDropDownList.DataValueField = "DegreeEducationLevelId";
      StudyProgramAreaDegreesDropDownList.DataBind();

      StudyProgramAreaDegreesDropDownList.Items.AddLocalisedTopDefault("Global.Degree.TopDefault", "- select degree -");
    }

		/// <summary>
		/// Binds the program of study dropdowns.
		/// </summary>
		private void BindStudyProgramAreaDropDown()
		{
			StudyProgramAreaDropDownList.DataSource = ServiceClientLocator.ExplorerClient(App).GetProgramAreas();
			StudyProgramAreaDropDownList.DataTextField = "Name";
			StudyProgramAreaDropDownList.DataValueField = "Id";
			StudyProgramAreaDropDownList.DataBind();
			StudyProgramAreaDropDownList.Items.AddLocalisedTopDefault("Global.ProgramArea.TopDefault", "- select department -");

			StudyProgramAreaDegreesDropDownList.Items.AddLocalisedTopDefault("Global.Degree.TopDefault", "- select degree -");
		}

		/// <summary>
		/// Registers the javascript.
		/// </summary>
		private void RegisterJavascript()
		{
			var js = String.Format(
        @"
function {0}_ControlLoad() {{
  $('#{0}').change(function () {{
		var selectedIndex = ($(this).prop('selectedIndex') > 0) ? 1 : -1;

		LoadCascadingDropDownList($(this).val(),'{1}','{2}','{3}','{4}','{5}', selectedIndex);
	}});
  $('#{1}').change(function () {{
		$('#{6}').text('');
	}});
}}",
   StudyProgramAreaDropDownList.ClientID, StudyProgramAreaDegreesDropDownList.ClientID, String.Concat(UrlBuilder.AjaxService(), HideAllDegreesOption ? "/GetDegreeEducationLevelsByProgramAreaWithoutAll" : "/GetDegreeEducationLevelsByProgramArea"), "Degrees",
				CodeLocalise("Global.Area.TopDefault", "- select degree -"),
				CodeLocalise("LoadingAreas.Text", "[Loading degrees...]"),
        ProgramAreaMessageLabel.ClientID);

        Page.ClientScript.RegisterStartupScript(GetType(), ClientID + "StudyProgramAreaDropDownListOnChange", js, true);
		}

    /// <summary>
    /// Sets the visibility of various controls
    /// </summary>
    private void SetVisibility()
    {
      if (!App.Settings.ShowProgramArea)
        ProgramAreaPlaceHolder.Visible = false;
    }
		
		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			AddProgramOfStudyButton.Text =  CodeLocalise("Global.Add.Text", "Add");

			if (IsJobWizard)
			{
        ProgramsOfStudyMessage.Text = string.Format("<em>{0}.</em><br/>", CodeLocalise("ProgramsOfStudyAssigned.Label", "This posting has been assigned to the following degree areas.<br>You can add or remove degrees as appropriate"));
				ProgramListDiv.Attributes["class"] = "horizontal";
				ProgramDropdownDiv.Attributes["class"] = "horizontal";
			}
		}

    /// <summary>
    /// Sets up validation
    /// </summary>
    private void ApplyValidation()
    {
      if (ProgramsOfStudyRequired)
      {
        ProgramOfStudyNotRequiredCheckBox.Text = HtmlLocalise("ProgramOfStudyNotRequiredCheckBox.Text", "No specific program of study required for this posting");
        ProgramOfStudyNotRequiredValidator.ErrorMessage = CodeLocalise("ProgramOfStudyNotRequiredValidator.Error", "No programs of study should be selected if it has been indicated none are required");
        ProgramsOfStudyUpdateablePairsValidator.ErrorMessage = CodeLocalise("ProgramOfStudyNotRequiredValidator.Error", "At least one program of study is required unless indicated otherwise");
        ProgramsOfStudyRequiredPanel.Visible = true;
        CustomValidatorPanels.Visible = true;
      }
      else
      {
        ProgramsOfStudyRequiredPanel.Visible = false;
        CustomValidatorPanels.Visible = false;
      }
    }

		/// <summary>
		/// Handles the Click event of the AddProgramOfStudyButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddProgramOfStudyButton_Click(object sender, EventArgs e)
		{
      if (StudyProgramAreaDegreesDropDownList.SelectedIndex != 0)
      {
        string displayText;

        if (App.Settings.ShowProgramArea)
          displayText = StudyProgramAreaDegreesDropDownList.SelectedValueToNullableLong(0) < 0
                          ? string.Concat(StudyProgramAreaDropDownList.SelectedItem.ToString(), " - ", StudyProgramAreaDegreesDropDownList.SelectedItem.ToString())
                          : StudyProgramAreaDegreesDropDownList.SelectedItem.ToString();
        else
          displayText = StudyProgramAreaDegreesDropDownList.SelectedItem.ToString();

        var success = ProgramsOfStudyUpdateablePairs.AddItem(new KeyValuePair<string, string>(StudyProgramAreaDegreesDropDownList.SelectedValue, displayText));
        switch (success)
        {
          case UpdateablePairsAddResult.Duplicate:
            ProgramAreaMessageLabel.Text = HtmlLocalise("UpdateablePair.AddItem.Duplicate", "The program already exists in the list");
            break;

          case UpdateablePairsAddResult.LimitReached:
            //ProgramAreaMessageLabel.Text = HtmlLocalise("UpdateablePair.AddItem.Duplicate", "A maximum of {0} programs can be added", MaxListSize);
            break;

          default:
            ProgramAreaMessageLabel.Text = "";
            break;
        }
      }
      else
      {
        ProgramAreaMessageLabel.Text = HtmlLocalise("UpdateablePair.AddItem.SelectOne", "Please select a degree");
      }
		}
	}
}