﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumePreviewModal.ascx.cs" Inherits="Focus.Web.Code.Controls.User.ResumePreviewModal" %>

<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:HiddenField ID="ResumePreviewModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ResumePreviewModalPopup" runat="server" ClientIDMode="Static"
												TargetControlID="ResumePreviewModalDummyTarget"
												PopupControlID="ResumePreviewModalPanel" 
												PopupDragHandleControlID="ResumePreviewModalPanelHeader"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" />
<asp:Panel ID="ResumePreviewModalPanel" TabIndex="-1" runat="server" CssClass="modal" ClientIDMode="Static" Width="90%" Style="display:none">
	<div style="float:left;width:100%;">
		<asp:Panel runat="server" ID="ResumePreviewModalPanelHeader">
		<table role="presentation" style="width:100%;">
			<tr>
				<td colspan="2">
					<h2>
						<%= HtmlLocalise("Preview.Title", "Resume for ") %><asp:Label runat="server" ID="CandidateNameLabel"></asp:Label>
						<asp:image ID="VeteranImage" runat="server" Visible="false" ImageUrl="<%# UrlBuilder.VeteranImage() %>" Height="15" Width="17" AlternateText="Veteran"/>
						<asp:Image runat="server" ID="NcrcImage" CssClass="tooltipImage toolTipNew ncrcListIcon" Visible="False" AlternateText="Ncrc Image"/>
					</h2>
				</td>
			</tr>
			<tr>
				<td runat="server" ID="ResumeScoreImageCell" style="width:100px;">
					<asp:image ID="ResumeScoreImage" runat="server" alt="Resume score image" Width="80" Height="16"/>
				</td>
				<td>
					<asp:panel ID="EmailPanel" runat="server" class="tooltip left Email">
						<asp:LinkButton ID="EmailButton" runat="server" OnCommand="ResumeAction_Command" CommandName="Email" aria-label="Email Job Seeker">
							<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("EmailJobseeker.ToolTip", "Email job seeker")%>"></div>
                            <asp:Label ID="EmailButtonLabel" runat="server" CssClass="sr-only" />
						</asp:LinkButton>
					</asp:panel>

					<asp:panel ID="FlagPanel" runat="server" class="tooltip left Flag">
						<asp:LinkButton ID="FlagButton" runat="server" OnCommand="ResumeAction_Command" CommandName="Flag" aria-label="Flag">
							<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("Flag.ToolTip", "Toggle Flag")%>"></div>
                            <asp:Label ID="FlagButtonLabel" runat="server" CssClass="sr-only" />
						</asp:LinkButton>
					</asp:panel>

					<asp:panel ID="NotePanel" runat="server" class="tooltip left Note">
						<asp:LinkButton ID="NoteButton" runat="server" OnCommand="ResumeAction_Command" CommandName="Note" aria-label="Note">
							<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("Note.ToolTip", "Add/Edit Note")%>"></div>
                            <asp:Label ID="NoteButtonLabel" runat="server" CssClass="sr-only" />
						</asp:LinkButton>
					</asp:panel>

					<asp:panel ID="CandidatesLikeThisPanel" runat="server" class="tooltip left MoreCandidatesLikeThis">
						<asp:LinkButton ID="CandidatesLikeThisButton" runat="server" OnCommand="ResumeAction_Command" CommandName="CandidatesLikeThis" aria-label="Candidates Like This">
							<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("CandidatesLikeThis.ToolTip", "Candidates Like This")%>"></div>
                            <asp:Label ID="CandidatesLikeThisButtonLabel" runat="server" CssClass="sr-only" />
						</asp:LinkButton>
					</asp:panel>
					
					<asp:panel ID="EmailCustom" runat="server" class="tooltip right Email">
						<asp:LinkButton ID="EmailResumeButton" runat="server" OnCommand="ResumeAction_Command" CommandName="EmailResume" aria-label="Email Resume">
							<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("EmailResume.ToolTip", "Email Resume")%>"></div>
							<asp:Label ID="EmailResumeLabel"   text="Email Resume" runat="server" CssClass="sr-only" />
						</asp:LinkButton>
					</asp:panel>
					
					<asp:panel ID="PrintPanel" runat="server" class="tooltip right Print">
						<img src="<%= UrlBuilder.PrintIcon() %>" alt="Print" width="30" height="24" TabIndex="-1" onkeypress="return PrintResume();"  onclick="return PrintResume();" title="Print Resume" />
					</asp:panel>
				</td>
			</tr>
			</table>
			</asp:Panel>
			<table role="presentation" style="width:100%;">
			<tr>
				<td></td>
				<td>
					<asp:panel ID="EditNotePanel" runat="server" Visible="false" Width="100%" >
						<br/>
						<table role="presentation" style="width:100%;">
							<tr>
								<td><asp:TextBox ID="NotesTextBox" Width="99%" MaxLength="399" runat="server" /></td>
								<td style="width:20px; text-align:center;">
										<asp:LinkButton ID="SaveNoteButton" runat="server" OnCommand="ResumeAction_Command" CommandName="SaveNote">
											<img src="<%= UrlBuilder.SaveIcon() %>" alt="<%= HtmlLocalise("SaveNote.Text", "Save note") %>" width="16px" />
										</asp:LinkButton>
								</td>
								<td style="width:20px;">
									<asp:LinkButton ID="CloseNoteButton" runat="server" OnCommand="ResumeAction_Command" CommandName="CloseNote">
										<img src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" />
									</asp:LinkButton>
								</td>
							</tr>
						</table>
						<br/>
					</asp:panel>
				</td>
			</tr>
			<tr>
				<td  colspan="2" style="border-style:solid; border-width: 1px; padding: 5px;">
					<div style="overflow:scroll;height:400px;width:100%;">
						<asp:Label ID="lblPreviewFormattedOutput" runat="server" />
					</div>
				</td>
			</tr>
		</table>
	</div>
	<div class="closeIcon"><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text.NoEdit", "Close") %>" onclick="$find('ResumePreviewModalPopup').hide();return false;" /></div>
</asp:Panel>

<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />
<script language="javascript" type="text/javascript">
	function PrintResume()
	{
		var displaySetting = 'left=150px, top=70px, width=850px, height=850px, addressbar=no, location=no, scrollbars=yes, status=no, resizable=no';
		var contentInnerHtml = $('#<%=lblPreviewFormattedOutput.ClientID %>').html();
		var documentPrint = window.open('PrintPreview', '_blank', displaySetting);
		documentPrint.document.open();
		documentPrint.document.write("<html><head><");
		documentPrint.document.write("script>function printResume() { "); //
		documentPrint.document.write("  if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) { ");
		documentPrint.document.write("    window.PPClose = false; ");
		documentPrint.document.write("    window.onbeforeunload = function () { ");
		documentPrint.document.write("      if (window.PPClose === false) { ");
		documentPrint.document.write("        return 'Leaving this page will block the parent window!\\nPlease select \"Stay on this Page\" option and use the\\nCancel button instead to close the Print Preview Window.\\n';");
		documentPrint.document.write("      }");
		documentPrint.document.write("    };");
		documentPrint.document.write("    window.print();");
		documentPrint.document.write("    window.PPClose = true;");
		documentPrint.document.write("    window.close();");
		documentPrint.document.write("  } else {");
		documentPrint.document.write("    window.print();");
		documentPrint.document.write("    window.close();");
		documentPrint.document.write("  }");
		documentPrint.document.write("}</");
		documentPrint.document.write("script></head>");
		documentPrint.document.write("<body onload='printResume();'>");
		documentPrint.document.write(contentInnerHtml);
		documentPrint.document.write("</body></html>");
		documentPrint.document.close();
		return false;
	}
</script>
