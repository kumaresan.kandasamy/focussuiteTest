﻿using System.Web.UI.WebControls;
using Focus.Common.Code;
using Framework.Core;

namespace Focus.Web.Code.Controls.User
{
	/// <summary>
	/// Standard confirmation modal user control
	/// </summary>
	public partial class ConfirmationModal : UserControlBase, IConfirmationModal
	{
		private const string CommandPrefix = "COMMAND:";
		private const string LinkPrefix = "LINK:";

		private bool _textBoxSet = false;

		public string Title { get; set; }
		public string Details { get; set; }
		public string Height { get; set; }
		public string Width { get; set; }

		public string ConfirmationText
		{
			get { return ConfirmationTextBox.Text.Trim(); }
		}

		public delegate void CloseCommandHandler(object sender, CommandEventArgs eventArgs);
		public event CloseCommandHandler CloseCommand;

		public delegate void OkCommandHandler(object sender, CommandEventArgs eventArgs);
		public event OkCommandHandler OkCommand;

		/// <summary>
		/// Shows the modal.
		/// </summary>
		/// <param name="title">The title.</param>
		/// <param name="details">The details.</param>
		/// <param name="closeText">The close text.</param>
		/// <param name="closeCommandName">Name of the close command.</param>
		/// <param name="closeLink">The close link.</param>
		/// <param name="okText">The ok text.</param>
		/// <param name="okCommandName">Name of the ok command.</param>
		/// <param name="okCommandArgument">The ok command argument.</param>
		/// <param name="height">The height.</param>
		/// <param name="width">The width.</param>
		/// <param name="y">The y.</param>
		public void Show(string title, string details, string closeText, string closeCommandName = "", string closeLink = "", string okText = null, string okCommandName = "", string okCommandArgument = "", int height = 200, int width = 400, int y = -1)
		{
			Title = title;
			Details = details;

			CloseButton.Text = string.IsNullOrEmpty(closeText) ? "Close" : closeText;

		  CloseButton.CommandName = string.Empty;
		  ModalPopup.CancelControlID = string.Empty;

			if (closeCommandName.IsNotNullOrEmpty())
				CloseButton.CommandName = CommandPrefix + closeCommandName;
			else if (closeLink.IsNotNullOrEmpty())
				CloseButton.CommandName = LinkPrefix + closeLink;
			else
				ModalPopup.CancelControlID = CloseButton.ClientID;

			if (okText.IsNotNullOrEmpty())
			{
				OkButton.Text = okText;
				OkButton.CommandName = CommandPrefix + okCommandName;
				OkButton.CommandArgument = okCommandArgument;
                OkButton.Visible = true;
			}
			else
				OkButton.Visible = false;

			Height = string.Format("{0}px", height);
			Width = string.Format("{0}px", width);

			if (y > -1)
				ModalPopup.Y = y;

			// If text box has not been explicitly set before the show, ensure any previous uses of the modal do not carry over into this one
			if (!_textBoxSet)
			{
				ConfirmationTextRow.Visible = false;
				ConfirmationTextBoxValidator.Enabled = false;
				ConfirmationTextBoxValidator.ValidationGroup = OkButton.ValidationGroup = "";
				OkButton.CausesValidation = false;
			}

			ModalPopup.Show();
		}

		/// <summary>
		/// Sets the text box details prior to showing the modal
		/// </summary>
		/// <param name="title">The title above the text box</param>
		/// <param name="rows">The number of rows for the text box</param>
		/// <param name="mandatory">Whether the text box is mandatory</param>
		/// <param name="validationMessage">The validation message if the text box is mandatory</param>
		/// <param name="validationGroup">The validation group if the text box is mandatory</param>
		public void SetTextBoxDetails(string title, int rows, bool mandatory, string validationMessage = "", string validationGroup = "")
		{
			_textBoxSet = true;

			ConfirmationTextRow.Visible = true;
			ConfirmationTextBox.Rows = rows;
			TextBoxHeader.Text = mandatory 
				? HtmlRequiredLabel(ConfirmationTextBox, title, title) 
				: HtmlLabel(ConfirmationTextBox, title, title);

			if (mandatory)
			{
				ConfirmationTextBoxValidator.Text = validationMessage;
				ConfirmationTextBoxValidator.Enabled = true;
				ConfirmationTextBoxValidator.ValidationGroup = OkButton.ValidationGroup = validationGroup;
				OkButton.CausesValidation = true;
			}
		}

		/// <summary>
		/// Hides this instance.
		/// </summary>
		public void Hide()
		{
			ModalPopup.Hide();
		}

		/// <summary>
		/// Handles the Command event of the CloseButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void CloseButton_Command(object sender, CommandEventArgs eventArgs)
		{
			var command = GetCommand(eventArgs);
			var link = GetLink(eventArgs);
			ModalPopup.Hide();

			if (command.IsNotNullOrEmpty())
				OnCloseCommand(new CommandEventArgs(command, null));
			else if (link.IsNotNullOrEmpty())
				Response.Redirect(link, true);
		}

		/// <summary>
		/// Handles the Command event of the OkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void OkButton_Command(object sender, CommandEventArgs eventArgs)
		{
      ModalPopup.Hide();
      
      var command = GetCommand(eventArgs);

			if (command.IsNotNullOrEmpty())
				OnOkCommand(new CommandEventArgs(command, eventArgs.CommandArgument));
		}

		/// <summary>
		/// Raises the <see cref="E:CloseCommand"/> event.
		/// </summary>
		/// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected virtual void OnCloseCommand(CommandEventArgs eventArgs)
		{
			if (CloseCommand != null)
				CloseCommand(this, eventArgs);
		}

		/// <summary>
		/// Raises the <see cref="E:OkCommand"/> event.
		/// </summary>
		/// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected virtual void OnOkCommand(CommandEventArgs eventArgs)
		{
			if (OkCommand != null)
				OkCommand(this, eventArgs);
		}

		/// <summary>
		/// Gets the command.
		/// </summary>
		/// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		/// <returns></returns>
		private static string GetCommand(CommandEventArgs eventArgs)
		{
			if (eventArgs.IsNull() || eventArgs.CommandName.IsNullOrEmpty())
				return null;

			var commandFull = eventArgs.CommandName;

			if (commandFull.StartsWith(CommandPrefix))
				return commandFull.Substring(8);

			return null;
		}

		/// <summary>
		/// Gets the link.
		/// </summary>
		/// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		/// <returns></returns>
		private static string GetLink(CommandEventArgs eventArgs)
		{
			if (eventArgs.IsNull() || eventArgs.CommandName.IsNullOrEmpty())
				return null;

			var commandFull = eventArgs.CommandName;

			if (commandFull.StartsWith(LinkPrefix))
				return commandFull.Substring(5);

			return null;
		}
	}
}