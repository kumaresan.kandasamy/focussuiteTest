﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using directives

using System;
using System.Collections.Specialized;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class EmailDocument : UserControlBase
	{
		#region Properties

		private DocumentDto _document
		{
			get { return GetViewStateValue<DocumentDto>("EmailDocumentModal:Document"); }
			set { SetViewStateValue("EmailDocumentModal:Document", value); }
		}

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			RegisterCodeValuesJson("emailDocumentModalScriptValues", "emailDocumentModalCodeValues", InitialiseClientSideCodeValues());
			if (!IsPostBack)
				LocaliseUI();
		}

		/// <summary>
		/// Shows the specified document.
		/// </summary>
		/// <param name="documentId">The document id.</param>
		/// <param name="isResume"></param>
		public void Show(long documentId,bool isResume = false)
		{
			InitialiseFields();
			if (!isResume)
			{
				_document = ServiceClientLocator.CoreClient(App).GetDocument(documentId);
				AttachmentNameLabel.DefaultText = _document.Title;
			}
			else
			{
				EmailDocumentTitleLabel.DefaultText = CodeLocalise("EmailResume.Text", "Email Resume");
				var resumeHtml = ServiceClientLocator.CandidateClient(App).GetResumeAsHtml(documentId, true, false);
				byte[] pdfData;
				Utilities.Export2PDF(resumeHtml, out pdfData, "");
				_document = new DocumentDto
				{
					FileName = "Resume.pdf",
					File = pdfData
				};
				AttachmentNameLabel.DefaultText = CodeLocalise("Resume.Text", "Resume");
			}
			EmailDocumentModal.Show();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			EmailBodyRequired.ErrorMessage = CodeLocalise("EmailBody.RequiredErrorMessage", "Body is required");
			EmailSubjectRequired.ErrorMessage = CodeLocalise("EmailSubject.RequiredErrorMessage", "Subject is required");
			SendButton.Attributes.Add("tabindex", "0");
		}

		/// <summary>
		/// Handles the Click event of the SendButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void SendButton_Click(object sender, EventArgs e)
		{
			try
			{
				ServiceClientLocator.CoreClient(App).SendEmail(ToEmailAddressTextBox.Text.Trim(), DocumentEmailSubjectTextBox.Text.Trim(), DocumentEmailBodyTextBox.TextTrimmed(), isHtml: true, attachment: _document.File, attachmentName: _document.FileName);
				MasterPage.ShowModalAlert(AlertTypes.Info, "Your email has been successfully sent.", "Thank you");
			}
			catch (Exception ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}

			EmailDocumentModal.Hide();
		}

		/// <summary>
		/// Initialises the client side code values.
		/// </summary>
		/// <returns></returns>
		private NameValueCollection InitialiseClientSideCodeValues()
		{
			var validationMessages = new NameValueCollection
				          {
					          { "emailAddressRequired", CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required") },
										{ "emailAddressCharLimit", CodeLocalise("EmailAddressCharLimit.ErrorMessage", "Email address must be at least 6 characters") },
										{ "emailAddressErrorMessage", CodeLocalise("EmailAddress.ErrorMessage", "Email address is not correct format")}
				          };
			return validationMessages;
		}

		/// <summary>
		/// Initialises the fields.
		/// </summary>
		private void InitialiseFields()
		{
			ToEmailAddressTextBox.Text = string.Empty;
			DocumentEmailSubjectTextBox.Text = string.Empty;
			DocumentEmailBodyTextBox.Text = string.Empty;
			AttachmentNameLabel.DefaultText = string.Empty;
		}
	}
}