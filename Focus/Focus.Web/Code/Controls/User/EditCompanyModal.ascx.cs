﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using directives

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models;
using Focus.Web.Core.Models;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
  public partial class EditCompanyModal : UserControlBase
	{
    #region Properties

    /// <summary>
    /// Whether to show a confirmation on saving
    /// </summary>
    public bool ShowConfirmationOnSave { get; set; }

    /// <summary>
    /// Whether to close the window on saving
    /// </summary>
    public bool CloseWindowOnSave { get; set; }

    /// <summary>
    /// Gets or sets the employer id.
    /// </summary>
    /// <value>The user id.</value>
    protected long EmployerId
    {
      get { return GetViewStateValue<long>("UpdateCompany:EmployerId"); }
      set { SetViewStateValue("UpdateCompany:EmployerId", value); }
    }

		protected int? LockVersion
		{
			get { return GetViewStateValue<int?>("UpdateCompany:LockVersion"); }
			set { SetViewStateValue("UpdateCompany:LockVersion", value); }
		}

		/// <summary>
		/// Gets or sets the business unit id.
		/// </summary>
  	protected long BusinessUnitId
  	{
			get { return GetViewStateValue<long>("UpdateCompany:BusinessUnitId"); }
			set { SetViewStateValue("UpdateCompany:BusinessUnitId", value); }
  	}

		/// <summary>
		/// Gets or sets the list of Business Unit objects for this employer
		/// </summary>
		/// <value>The business units for this employer</value>
		protected List<BusinessUnitDto> BusinessUnits
		{
			get { return GetViewStateValue<List<BusinessUnitDto>>("UpdateCompany:BusinessUnits"); }
			set { SetViewStateValue("UpdateCompany:BusinessUnits", value); }
		}

		
		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        LocaliseUI();
				StateEmployerIdentificationNumberValidator.ValidationExpression = App.Settings.SEINRegExPattern;
      }
    }

		/// <summary>
		/// Handles the PreRender event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_PreRender(object sender, EventArgs e)
		{
			if (App.Settings.SEINMaskPattern.IsNotNullOrEmpty())
			{
				var script = string.Format("$('#{0}').mask('{1}', {{ placeholder: ' ' }});", StateEmployerIdentificationNumberTextBox.ClientID, App.Settings.SEINMaskPattern);
				ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SEINMaskPattern", script, true);
			}
		}

    /// <summary>
    /// Shows the specified employer id/business unit id combo.
    /// </summary>
    /// <param name="employerId">The employer id.</param>
		/// /// <param name="businessUnitId">The business unit id.</param>
    /// <param name="top">The top.</param>
		public void Show(long employerId, long businessUnitId, int? lockVersion = null )
    {
      EmployerId = employerId;
    	BusinessUnitId = businessUnitId;
			LockVersion = lockVersion;

    	EditBusinessUnitPanel.Visible = false;

      var model = ServiceClientLocator.EmployerClient(App).GetEmployer(EmployerId);

	    if (lockVersion.IsNull() && model.LockVersion.IsNotNull())
		    LockVersion = model.LockVersion;
      
      PrimaryNameTextBox.Text = model.Name;

			var feinRegEx = new Regex(App.Settings.FEINRegExPattern);

			if (feinRegEx.IsMatch(model.FEIN))
				FeinLabel.DefaultText = CodeLocalise("FeinLabel.DefaultText", "FEIN: " + model.FEIN);
			else
				FeinLabel.DefaultText = CodeLocalise("FeinLabel.DefaultText", "No FEIN stored for this company");

      StateEmployerIdentificationNumberTextBox.Text = model.StateEmployerIdentificationNumber;

			if (BusinessUnitId > 0)
			{
				EditBusinessUnitPanel.CssClass = "";
				EditBusinessUnitPanel.Visible = true;
				EditBusinessUnit.Bind(businessUnitId: Convert.ToInt64(businessUnitId), employerId: EmployerId);
			}

    	ModalPopup.Show();
    }


		/// <summary>
		/// Shows the specified employer id.  Because no business unit ID is passed in the primary business unit will always be shown in this scenario
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <param name="top">The top.</param>
		public void Show(long employerId)
		{

			
			
			Show(employerId);
		}

    public void Hide()
    {
      ModalPopup.Hide();
    }

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <returns></returns>
		public EmployerDetailsModel Unbind()
		{
			var model = GetModel();

			try
			{
				model.Name = PrimaryNameTextBox.TextTrimmed();
				model.StateEmployerIdentificationNumber = StateEmployerIdentificationNumberTextBox.TextTrimmed();
			}
			catch (Exception)
			{}
			return model;
		}

    /// <summary>
    /// Handles the Clicked event of the OKButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SaveButton_Clicked(object sender, EventArgs e)
    {
			var employerClient = ServiceClientLocator.EmployerClient(App);
      	
      var employerDetailsModel = Unbind();

	    employerDetailsModel.LockVersion = LockVersion;

	    try
	    {

		    employerClient.SaveEmployerDetails(employerDetailsModel);

		    OnCompanyUpdated(new CompanyUpdatedEventArgs(employerDetailsModel));

		    var businessUnitModel = new BusinessUnitModel();

		    if (BusinessUnitId > 0)
		    {
			    businessUnitModel = EditBusinessUnit.Unbind();
			    employerClient.SaveBusinessUnitModel(businessUnitModel);
			    OnBusinessUnitUpdated(new BusinessUnitUpdatedEventArgs(businessUnitModel));
		    }

		    if (ShowConfirmationOnSave)
		    {
				Confirmation.Show(CodeLocalise("EditEmployerConfirmation.Title", "#BUSINESS# account amended"),
													CodeLocalise("EditEmployerConfirmation.Body", "The #BUSINESS#:LOWER account has been amended."),
				    CodeLocalise("CloseModal.Text", "OK"));
			    ConfirmationUpdatePanel.Update();
		    }

		    if (CloseWindowOnSave)
			    Hide();
	    }
	    catch (Exception ex)
	    {
				Confirmation.Show(CodeLocalise("CompanyUpdateError.Title", "Company update failed"),
						CodeLocalise("CompanyUpdateError.Body", ex.Message),
						CodeLocalise("CloseModal.Text", "OK"));
				ConfirmationUpdatePanel.Update();
				Hide();
		  
	    }

    }

    /// <summary>
    /// Handles the Clicked event of the CancelButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void CancelButton_Clicked(object sender, EventArgs e)
    {
      Hide();
    }

    /// <summary>
    /// Binds the contact type drop down.
    /// </summary>
    /// <param name="dropDownList">The drop down list.</param>
    private void BindContactTypeDropDown(DropDownList dropDownList)
    {
      dropDownList.Items.Clear();

      dropDownList.Items.AddEnum(PhoneTypes.Phone, "landline");
      dropDownList.Items.AddEnum(PhoneTypes.Mobile, "mobile");
      dropDownList.Items.AddEnum(PhoneTypes.Fax, "fax");
      dropDownList.Items.AddEnum(PhoneTypes.Other, "other");
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
			StateEmployerIdentificationNumberValidator.ErrorMessage = App.Settings.SEINRegExPattern == "^\\d*$"
				? CodeLocalise("StateEmployerIdentificationNumberValidator.NumericErrorMessage", "State employer ID must consist of only numbers.")
				: CodeLocalise("StateEmployerIdentificationNumberValidator.ErrorMessage", "State employer ID is not in the correct format.");

      CancelButton.Text = CodeLocalise("Global.Cancel.Button", "Cancel");
      SaveButton.Text = CodeLocalise("Global.Save.Button", "Save");

      if (App.Settings.Theme == FocusThemes.Education)
        FEINTableRow.Visible = SEINTableRow.Visible = false;
      else
        PrimaryNameRow.Visible = false;
    }

    /// <summary>
    /// Gets the model.
    /// </summary>
    /// <returns></returns>
    private EmployerDetailsModel GetModel()
    {
      return ServiceClientLocator.EmployerClient(App).GetEmployer(EmployerId);
    }

    /// <summary>
    /// Initialises the control values.
    /// </summary>
    private void InitialiseControlValues()
    {
      
    }


		#region Events

		public event CompanyUpdatedHandler CompanyUpdated;

		/// <summary>
		/// Raises the <see cref="E:BusinessUnitSelected"/> event.
		/// </summary>
		/// <param name="e">The <see cref="Focus.Web.Code.Controls.User.BusinessUnitSelector.BusinessUnitSelectedEventArgs"/> instance containing the event data.</param>
		protected virtual void OnCompanyUpdated(CompanyUpdatedEventArgs e)
		{
			if (CompanyUpdated != null)
				CompanyUpdated(this, e);
		}

		public event BusinessUnitUpdatedHandler BusinessUnitUpdated;

		/// <summary>
		/// Raises the <see cref="E:BusinessUnitSelected"/> event.
		/// </summary>
		/// <param name="e">The <see cref="Focus.Web.Code.Controls.User.BusinessUnitSelector.BusinessUnitSelectedEventArgs"/> instance containing the event data.</param>
		protected virtual void OnBusinessUnitUpdated(BusinessUnitUpdatedEventArgs e)
		{
			if (BusinessUnitUpdated != null)
				BusinessUnitUpdated(this, e);
		}

		#endregion

		#region Delegates

		public delegate void CompanyUpdatedHandler(object o, CompanyUpdatedEventArgs e);
		public delegate void BusinessUnitUpdatedHandler(object o, BusinessUnitUpdatedEventArgs e);

		#endregion

		#region EventArgs

		/// <summary>
		/// 
		/// </summary>
		public class CompanyUpdatedEventArgs : EventArgs
		{
			public readonly EmployerDetailsModel Model;

			public CompanyUpdatedEventArgs(EmployerDetailsModel model)
			{
				Model = model;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public class BusinessUnitUpdatedEventArgs : EventArgs
		{
			public readonly BusinessUnitModel Model;

			public BusinessUnitUpdatedEventArgs(BusinessUnitModel model)
			{
				Model = model;
			}
		}

		#endregion
  }
}