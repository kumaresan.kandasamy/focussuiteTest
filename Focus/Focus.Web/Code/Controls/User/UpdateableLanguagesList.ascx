﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdateableLanguagesList.ascx.cs" Inherits="Focus.Web.Code.Controls.User.UpdateableLanguagesList" %>

<asp:Panel runat="server" ID="ItemsListPanel">
<asp:ListView ID="ItemsList" runat="server" >
	<LayoutTemplate>
		<table class="deletableListItemTable">
			<tr runat="server" ID="itemPlaceHolder"></tr>
		</table>
	</LayoutTemplate>
	<ItemTemplate>
		<tr>
			<td class="column1"><%# ((KeyValuePair<string,string[]>)Container.DataItem).Value[1] %>
			</td>
			<td class="column2">
				<asp:ImageButton ID="ItemsListRemoveImageButton" runat="server"  OnCommand="ItemsListRemoveButton_Command" CommandArgument="<%# Container.DataItemIndex %>" 
													CommandName="Remove" ImageUrl="<%# UrlBuilder.ButtonDeleteIcon() %>" CausesValidation="False" />
			</td>
		</tr>
	</ItemTemplate>
</asp:ListView>
<asp:HiddenField runat="server" ID="ItemList"/>
</asp:Panel>