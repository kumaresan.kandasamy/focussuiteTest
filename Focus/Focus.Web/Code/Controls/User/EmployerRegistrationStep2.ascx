<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployerRegistrationStep2.ascx.cs" Inherits="Focus.Web.Code.Controls.User.EmployerRegistrationStep2" %>
<%@ Register TagPrefix="uc" TagName="BusinessUnitSelector" Src="~/Code/Controls/User/BusinessUnitSelector.ascx" %>

<asp:Label ID="FEINExistsInstructionsLabel" runat="server" /><br />
<ul id="ExtraInstructionsList" runat="server" Visible="False">
  <asp:Repeater runat="server" ID="ExtraInstructionsListItems">
    <ItemTemplate>
      <li><%#Container.DataItem%></li>
    </ItemTemplate>
  </asp:Repeater>
</ul>
<br />
<asp:Panel ID="ReadOnlyPanel" runat="server">
	<asp:UpdatePanel ID="EmployerRegistrationStep2UpdatePanel" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
		  <div style="float:left;width: 50%">
		    <table class="formTable" role="presentation">
				  <tr id="BusinessUnitSelctorRow" runat="server">
					  <td class="label"><%= HtmlLabel("BusinessUnitTradeName.Label", "Select #BUSINESS#")%></td>
						<td>
							<asp:DropDownList aria-label="Business Unit TradeName" ID="BusinessUnitTradeNameDropDownList" runat="server" ClientIDMode="Static" Width="100%" TabIndex="1" AutoPostBack="true" OnSelectedIndexChanged="BusinessUnitTradeNameDropDownList_SelectedIndexChanged" CausesValidation="False" />
              <asp:CustomValidator ID="BusinessUnitTradeNameRequired" runat="server" ClientIDMode="Static" SetFocusOnError="true" Display="Dynamic" CssClass="error" OnServerValidate="BusinessUnitTradeNameRequired_OnServerValidate" />
						</td>
				  </tr>
          <tr id="LegalNameRow" runat="server">
					  <td class="label"><%= HtmlLabel("LegalName.Label", "#BUSINESS# legal name")%></td>
					  <td>
						  <asp:Label ID="LegalNameLabel" runat="server" Width="98%" EnableViewState="false" />
					  </td>
          </tr>
          <tr>
					  <td class="label"><%= HtmlLabel("BusinessUnit.Label", "#BUSINESS# name")%></td>
					  <td>
						  <asp:Label ID="BusinessUnitLabel" runat="server" Width="98%" EnableViewState="false" />
					  </td>
          </tr>
          <tr>
					  <td class="label"><%= HtmlLabel("AddressLine1.Label", "Address")%></td>
					  <td>
						  <asp:Label ID="AddressLine1Label" runat="server"  Width="98%" EnableViewState="false" />
					  </td>
          </tr>
          <tr>
					  <td class="label"><%= HtmlLabel("AddressLine2.Label", "")%></td>
					  <td>
						  <asp:Label ID="AddressLine2Label" runat="server" Width="98%" EnableViewState="false" />
					  </td>
          </tr>
          <tr>
					  <td class="label"><%= HtmlLabel("AddressTownCity.Label", "City")%></td>
					  <td>
						  <asp:Label ID="AddressTownCityLabel" runat="server" Width="98%" EnableViewState="false" />
					  </td>
          </tr>
          <tr>
					  <td class="label"><%= HtmlLabel("AddressState.Label", "State")%></td>
					  <td>
						  <asp:Label ID="AddressStateLabel" runat="server" Width="100%" EnableViewState="false" />
					  </td>
          </tr>
          <tr id="CountyRow" runat="server">
					  <td class="label"><%= HtmlLabel("AddressCounty.Label", "County")%></td>
					  <td>
						  <asp:Label ID="AddressCountyLabel" runat="server" Width="98%" EnableViewState="false"/>
					  </td>
          </tr>
          <tr>
					  <td class="label"><%= HtmlLabel("AddressPostcodeZip.Label", "ZIP or postal code")%></td>
					  <td>
						  <asp:Label ID="AddressPostcodeZipLabel" runat="server"  Width="150px" EnableViewState="false" />
					  </td>
          </tr>
				  <tr>
					  <td class="label"><%= HtmlLabel("AddressCountry.Label", "Country")%></td>
					  <td>
						  <asp:Label ID="AddressCountryLabel" runat="server" Width="100%" EnableViewState="false" />
					  </td>
				  </tr>
				  <tr>
					  <td class="label"><%= HtmlLabel("ContactUrl.Label", "URL")%></td>
					  <td>
						  <asp:Label ID="ContactUrlLabel" runat="server" Width="98%" EnableViewState="false" />
					  </td>
				  </tr>
				  <tr>
					  <td class="label"><%= HtmlLabel("ContactPhone.Label", "Phone number")%></td>
					  <td>
						  <asp:Label ID="ContactPhoneLabel" runat="server" Width="98%" EnableViewState="false" />
					  </td>
				  </tr>
				  <tr>
					  <td class="label"><%= HtmlLabel("ContactAlternatePhone.Label", "Alternate phone number 1")%></td>
					  <td>
						  <asp:Label ID="ContactAlternatePhoneLabel" runat="server" Width="98%" EnableViewState="false" />
					  </td>
				  </tr>
				  <tr>
					  <td class="label"><%= HtmlLabel("ContactAlternatePhone2.Label", "Alternate phone number 2")%></td>
					  <td>
						  <asp:Label ID="ContactAlternatePhone2Label" runat="server" Width="98%" EnableViewState="false" />
					  </td>
				  </tr>
        </table>
		  </div>
      <div style="float:right;width: 50%">
			  <table class="formTable" role="presentation">
			    <tr>
			      <td colspan="2">&nbsp;</td>
			    </tr>
			  	<tr>
					  <asp:Panel ID="FEINPanel" runat="server">
						  <td class="label"><%= HtmlLabel("FederalEmployerIdentificationNumber.Label", "Federal Employer ID")%></td>
						  <td>
							  <asp:Label ID="FederalEmployerIdentificationNumberLabel" runat="server" Width="98%" EnableViewState="false" />
						  </td>
					  </asp:Panel>
				  </tr>
				  <tr>
					  <asp:Panel ID="Panel2" runat="server">
						  <td class="label"><asp:Literal runat="server" ID="StateEmployerIdentificationNumberTitleLabel"></asp:Literal></td>
						  <td>
							  <asp:Label ID="StateEmployerIdentificationNumberLabel" runat="server" Width="98%" EnableViewState="false" />
						  </td>
					  </asp:Panel>
				  </tr>
				  <tr>
					  <td class="label"><asp:Literal runat="server" ID="OwnershipTypeTitleLabel"></asp:Literal></td>
					  <td>
						  <asp:Label ID="OwnershipTypeLabel" runat="server" Width="100%" EnableViewState="false" />
					  </td>
				  </tr>
			  	<tr>
					  <td class="label"><asp:Literal runat="server" ID="IndustrialClassificationTitleLabel"></asp:Literal></td>
					  <td>
						  <asp:Label ID="IndustrialClassificationLabel" runat="server" Width="98%" EnableViewState="false" />
					  </td>
				  </tr>
			  	<tr>
					  <td id="AccountTypeCell" class="label" runat="server"><asp:Literal runat="server" ID="AccountTypeTitleLabel"></asp:Literal></td>
					  <td>
						  <asp:Label ID="AccountTypeLabel" runat="server" Width="100%" EnableViewState="false" />
					  </td>
				  </tr>
				  <tr id="NoOfEmployeesRow" runat="server">
					  <td id="NoOfEmployeesCellLabel" class="label" runat="server" ><%= HtmlLabel("NoOfEmployees.Label", "Number Of Employees")%></td>
					  <td>
						  <asp:Label ID="NoOfEmployeesLabel" runat="server" Width="100%" EnableViewState="false" />
					  </td>
				  </tr>
		  		<tr>
          	<td class="label"><%= HtmlLabel("CompanyDescription.Label", "#BUSINESS# description")%></td>
						<td valign="top">
							<asp:Label ID="CompanyDescriptionLabel" runat="server" Width="98%" EnableViewState="false" />			
						</td>
 				  </tr>
	  		</table>
      </div>
    </ContentTemplate>
	</asp:UpdatePanel>
    <asp:Button runat="server" ID="ChangeCompanyButton" CssClass="button3" Visible="False" OnClick="ChangeCompanyButton_OnClick"/>
</asp:Panel>
<uc:BusinessUnitSelector ID="SelectBusinessUnitModal" runat="server" OnBusinessUnitSelected="SelectBusinessUnitModal_BusinessUnitSelected" />
