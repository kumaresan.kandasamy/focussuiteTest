﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobPostingDetails.ascx.cs" Inherits="Focus.Web.Code.Controls.User.JobPostingDetails" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationBookMarkModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<%@ Register src="~/Code/Controls/User/JobPostingToolsCriteria.ascx" tagPrefix="uc" tagName="JobPostingToolsCriteria" %>

<div>
	<asp:Label ID="testvalue" runat="server" CssClass="instructionalText"></asp:Label>
	<asp:HyperLink ID="BackHyperLink" runat="server" onclick="javascript:showProcessing();">
		<focus:LocalisedLabel runat="server" ID="JobResultsLabel" RenderOuterSpan="False" LocalisationKey="JobResults.Label" DefaultText="&#171; Job results"/>
	</asp:HyperLink>
	
	<div class="jobPostingSummary">
		<div class="jobPostingSummaryMain">
			<h1><asp:Label ID="lblPostingTitle" runat="server"></asp:Label></h1>
			<asp:Label ID="VeteranDetails" runat="server" Visible="False"></asp:Label>
			<p>
				<asp:Label ID="lblCustomerJobID" runat="server"></asp:Label><br/>
				<asp:Label ID="lblJobID" runat="server" />
			</p>
			<asp:PlaceHolder runat="server" id="NoApplyHolder" Visible="False">
        <p>
          <asp:Label runat="server" ID="NoApplyLabel"></asp:Label>
        </p>
      </asp:PlaceHolder>
		</div>
		<div class="jobPostingSummaryReport">
			<asp:PlaceHolder runat="server" ID="ReportJobPlaceHolder" Visible="False">
				<focus:LocalisedLabel runat="server" ID="ReportJobLabel" AssociatedControlID="ddlReportSpamOrClosed" LocalisationKey="ReportJob.Label" DefaultText="Report job"/>
				<div class="FocusCareer blueDropDown">
					<asp:DropDownList ID="ddlReportSpamOrClosed" runat="server" Width="130px" ClientIDMode="Static">
						<asp:ListItem Value="SPAM">as spam</asp:ListItem>
						<asp:ListItem Value="CLOSED">as closed</asp:ListItem>
					</asp:DropDownList>
				</div>
				<asp:Button ID="btnReportSpamOrClosed" Text="Go &#187;" runat="server" class="buttonLevel2" OnClick="btnReportSpamOrClosed_Click" />
      </asp:PlaceHolder>
			
	</div>
		<div class="jobPostingTools">
				<uc:JobPostingToolsCriteria ID="JobPostingToolsCriteria" runat="server" />
		</div>
	</div>
	
	<div>
		<iframe id="PostingDetail" runat="server" width="100%" height="500px" ClientIDMode="Static" title="PostingDetail">Posting Detail</iframe>
	</div>
	
	<uc:ConfirmationBookMarkModal ID="ConfirmationBookMarkModal" runat="server" />
	<asp:HiddenField ID="HowToApplyDummyTarget" runat="server" />
	<asp:HiddenField ID="HiddenEmailJob" runat="server" />
	<asp:HiddenField runat="server" ID="hdnActions" ClientIDMode="Static" />
	<asp:HiddenField runat="server" ID="hdnActionsYes" ClientIDMode="Static" />
	
</div>

	<script type="text/javascript">

		var prm = Sys.WebForms.PageRequestManager.getInstance();

		prm.add_endRequest(endRequestHandler);

		function endRequestHandler(sender, args) {
			$('span.inFieldLabel > label').inFieldLabels();
		}

		function ApplyStyleToDropDowns() {
			$('#ddlReportSpamOrClosed').customSelect();
		}

		function PrintPosting(section) {
			var displaySetting = 'left=150px,top=70px,width=850px,height=850px,addressbar=no,location=no,scrollbars=yes,status=no,resizable=no';
			var contentInnerHtml = '';

			if (section === 'POSTING') {
				var jobDetails = '<strong>Customer Job ID:</strong> ' + '<%= CustomerJobId %>' + '<br /><strong>' + '<%= BrandName %>' + ' Job ID:</strong> ' + '<%= LensPostingId %>' + '<br />';
				contentInnerHtml = jobDetails + $('#PostingDetail').contents().find('body').html();
			}
			else if (section === 'JOB_INFO')
				contentInnerHtml = $('#lblReferralDetails').html();

			var documentPrint = window.open('', '_blank', displaySetting);
			documentPrint.document.open();
			documentPrint.document.write('<html><head></head>');
			documentPrint.document.write('<body onload="self.close();">');
			documentPrint.document.write(contentInnerHtml);
			documentPrint.document.write('</body></html>');
			documentPrint.print();
			documentPrint.document.close();
			return false;
		}



		function checkInclude(radioClicked) {
			var radioButtonList = $('#' + radioClicked.id);
			var radioButtonListSubId = radioClicked.id.substring(radioClicked.id.lastIndexOf('_') + 1);
			var hdnActions = $('#hdnActions');
			var hdnActionsYes = $('#hdnActionsYes');

			if ($(radioButtonList).find('input[value="0"]').is(':checked')) {
				if ($(hdnActions).val().indexOf("'" + radioButtonListSubId + "'") < 0)
					$(hdnActions).val($(hdnActions).val() + "'" + radioButtonListSubId + "'#");
			}
			else {
				if ($(hdnActions).val().indexOf("'" + radioButtonListSubId + "'") >= 0)
					$(hdnActions).val($(hdnActions).val().replace("'" + radioButtonListSubId + "'", ""));
			}

			if ($(radioButtonList).find('input[value="1"]').is(':checked')) {
				if ($(hdnActionsYes).val().indexOf("'" + radioButtonListSubId + "'") < 0)
					$(hdnActionsYes).val($(hdnActionsYes).val() + "'" + radioButtonListSubId + "'#");
			}
			else {
				if ($(hdnActionsYes).val().indexOf("'" + radioButtonListSubId + "'") >= 0)
					$(hdnActionsYes).val($(hdnActionsYes).val().replace("'" + radioButtonListSubId + "'", ""));
			}
			return true;
		}

		function JobPosting_DisableJobSeekerLink(careerUrl) {
			var link = $("#<%=PostingDetail.ClientID %>").contents().find("a[href^='" + careerUrl + "']");
			if (link.length > 0) {
				link.each(function () {
					var $t = jQuery(this);
					$t.after($t.text());
					$t.remove();
				});
			}
		}
	</script>