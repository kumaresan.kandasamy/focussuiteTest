﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common;
using Focus.Web.Core.Models;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class UpdateContactInformationModal : UserControlBase
	{
		#region Properties

		
		/// <summary>
		/// LockVersion to prevent overriting by multiple users
		/// </summary>
		protected int? LockVersion
		{
			get { return GetViewStateValue<int?>("UpdateContact:LockVersion"); }
			set { SetViewStateValue("UpdateContact:LockVersion", value); }
		}

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				Localise();
			}
		}

		/// <summary>
		/// Shows the specified user id.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="lockVersion">lock version</param>
		/// <param name="businessUnitName">business unit name</param>
		public void Show(long userId = 0, string businessUnitName = "", int? lockVersion = null)
		{
			
			LockVersion = lockVersion;

			if (userId > 0)
			{
				var details = ContactInformationMain.Bind(userId, businessUnitName);
				if (lockVersion.IsNull() && details.LockVersion.IsNotNull())
					LockVersion = details.LockVersion;
			}


			ContactModalPopup.Show();
		}

		/// <summary>
		/// Handles the Clicked event of the CancelButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void EditContactCancelButton_Clicked(object sender, EventArgs e)
		{
			Hide();
		}

		/// <summary>
		/// Handles the Clicked event of the SaveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void ContactSaveButton_Clicked(object sender, EventArgs e)
		{

			var userDetails = ContactInformationMain.Unbind();
			userDetails.LockVersion = LockVersion;
			try
			{
				ServiceClientLocator.AccountClient(App).UpdateContactDetails(userDetails);
				OnContactUpdated(new ContactUpdatedEventArgs(userDetails));
				Hide();
			}
			catch (Exception)
			{

				var message = CodeLocalise("HiringManaferUpdateError.EditError", "There was a problem editing the record.<br /><br />Another user has recently updated this record. Please exit the record and return to it to see the changes made.");

				Confirmation.Show(CodeLocalise("HiringManaferUpdateError.Text", "Edit hiring contact"),
													message,
													CodeLocalise("CloseModal.Text", "OK"),
													closeLink: UrlBuilder.EmployerReferrals());
													Hide();
			}
			
		}

		/// <summary>
		/// Hides this instance.
		/// </summary>
		private void Hide()
		{
			ContactModalPopup.Hide();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void Localise()
		{
			ContactCancelButton.Text = CodeLocalise("Global.Cancel.Label", "Cancel");
			ContactSaveButton.Text = CodeLocalise("Global.Save.Label", "Save");
		}

		public event ContactUpdatedHandler ContactUpdated;

		/// <summary>
		/// Raises the <see cref="E:ContactUpdated" /> event.
		/// </summary>
		/// <param name="e">The <see cref="ContactUpdatedEventArgs"/> instance containing the event data.</param>
		protected virtual void OnContactUpdated(ContactUpdatedEventArgs e)
		{
			if (ContactUpdated != null)
				ContactUpdated(this, e);
		}

		public delegate void ContactUpdatedHandler(object o, ContactUpdatedEventArgs e);

		/// <summary>
		/// 
		/// </summary>
		public class ContactUpdatedEventArgs : EventArgs
		{
			public readonly UserDetailsModel Model;

			public ContactUpdatedEventArgs(UserDetailsModel model)
			{
				Model = model;
			}
		}
	}
}	