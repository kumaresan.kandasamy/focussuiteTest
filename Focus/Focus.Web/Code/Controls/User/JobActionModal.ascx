<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobActionModal.ascx.cs"
    Inherits="Focus.Web.Code.Controls.User.JobActionModal" %>
<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>
<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" TargetControlID="ModalDummyTarget"
    PopupControlID="ModalPanel" RepositionMode="RepositionOnWindowResizeAndScroll"
    BackgroundCssClass="modalBackground" />
<asp:Panel ID="ModalPanel" TabIndex="-1" runat="server" CssClass="modal conditionallyScrolledModal"
    Style="display: none;">
    <table style="width: 450px;">
        <tr>
            <th style="vertical-align: top;">
                <%=Title%>
            </th>
        </tr>
        <tr id="ExpirationDateRow" runat="server">
            <td style="vertical-align: top;">
                <table role="presentation">
                    <tr>
                        <td style="white-space: nowrap;">
                            <asp:Label runat="server" ID="JobExpirationLabel" AssociatedControlID="ExpirationDateTextBox"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="ExpirationDateTextBox" runat="server" Width="100px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator runat="server" ID="ExpirationDateValidator" ControlToValidate="ExpirationDateTextBox"
                                SetFocusOnError="True" CssClass="error" ValidationGroup="JobActionModal" Display="Dynamic" />
                            <asp:CompareValidator ID="ClosingDateIsValidCompareValidator" runat="server" ControlToValidate="ExpirationDateTextBox"
                                Type="Date" Operator="DataTypeCheck" SetFocusOnError="true" Display="Dynamic"
                                CssClass="error" ValidationGroup="JobActionModal" />
                            <asp:CompareValidator ID="ClosingDateCompareValidator" runat="server" ErrorMessage="CompareValidator"
                                ControlToValidate="ExpirationDateTextBox" Type="Date" Operator="LessThanEqual"
                                CssClass="error" Display="Dynamic" ValidationGroup="JobActionModal" />
                            <asp:CompareValidator ID="ClosingDateGreaterThanTodayCompareValidator" runat="server"
                                ErrorMessage="CompareValidator" ControlToValidate="ExpirationDateTextBox" Type="Date"
                                Operator="GreaterThan" CssClass="error" Display="Dynamic" ValidationGroup="JobActionModal" />
                            <asp:CustomValidator runat="server" ID="ClosingDateCustomValidator" ControlToValidate="ExpirationDateTextBox"
                                Display="None" ClientValidationFunction="JobActionModal_ClosingDateValidation"
                                ValidationGroup="JobActionModal"></asp:CustomValidator>
                            <br />
                        </td>
                    </tr>
                    <tr id="NumberOfPostingsRow" runat="server" visible="False">
                        <td style="white-space: nowrap;">
                            <%= HtmlLocalise("NumberOfPostings.Label", "Number of Openings")%>
                        </td>
                        <td>
                            <asp:TextBox ID="NumberOfPostingsTextBox" runat="server" ClientIDMode="static" MaxLength="3"
                                Width="30px" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:RequiredFieldValidator runat="server" ID="NumberOfPostingsValidator" ControlToValidate="NumberOfPostingsTextBox"
                                SetFocusOnError="True" CssClass="error" ValidationGroup="JobActionModal" Display="Dynamic" />
                            <asp:RegularExpressionValidator ID="NumberOfPostingsRegEx" runat="server" ControlToValidate="NumberOfPostingsTextBox"
                                SetFocusOnError="true" Display="Dynamic" ValidationGroup="JobActionModal" CssClass="error" />
                            <asp:RangeValidator ID="NumberOfPostingsTextBoxRangeValidator" runat="server" ControlToValidate="NumberOfPostingsTextBox"
                                CssClass="error" MaximumValue="200" MinimumValue="1" Type="Integer" ValidationGroup="JobActionModal"
                                SetFocusOnError="True" />
                        </td>
                    </tr>
                </table>
                <act:CalendarExtender ID="ClosingDateCalendarExtender" runat="server" TargetControlID="ExpirationDateTextBox" CssClass="cal_Theme1" />
            </td>
        </tr>
    </table>
    <table style="width: 450px;" role="presentation">
        <tr id="SurveyRow" runat="server">
            <td style="vertical-align: top; height: 300px;">
                <asp:Literal ID="JobSurveyHeader" runat="server" />
                <br />
                <br />
                <table role="presentation">
                    <tr>
                        <td style="vertical-align: top;">
                            1.
                        </td>
                        <td style="padding-bottom: 12px;">
                            <asp:Label runat="server" ID="Question1Label" AssociatedControlID="JobSatisfiedAnswerDropDownList"></asp:Label>
	                        <asp:DropDownList ID="JobSatisfiedAnswerDropDownList" runat="server"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            2.
                        </td>
                        <td style="padding-bottom: 12px;">
                            <asp:Literal ID="Question2Label" runat="server" /><br />
                            <fieldset >
                                <legend>Interview Answers</legend>
                                <asp:RadioButtonList ID="JobInterviewAnswerRadioButtonList" runat="server" RepeatDirection="Horizontal"
                                    role="presentation" CssClass="RadioButtonList"/>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            3.
                        </td>
                        <td style="padding-bottom: 12px;">
                            <asp:Literal ID="Question3Label" runat="server" /><br />
                            <fieldset>
                                <legend>Job Hire Answers</legend>
                                <asp:RadioButtonList ID="JobHireAnswerRadioButtonList" runat="server" RepeatDirection="Horizontal"
                                    role="presentation" CssClass="RadioButtonList"/>
                            </fieldset>
                        </td>
                    </tr>
										<tr runat="server" ID="AdvertiseFor30DaysPlaceHolder" Visible="False">
											<td colspan="2">
												<asp:Label CssClass="error" runat="server" ID="AdvertiseFor30DaysText"></asp:Label>
											</td>
										</tr>
                    <tr id="ApplicantsRow" runat="server">
                        <td style="vertical-align: top;">
                            4.
                        </td>
                        <td style="padding-bottom: 12px;">
                            <%= HtmlLocalise("ApplicantsUpdate.Text", "Please update referral outcomes for the job seekers below:")%>
                            <br />
                            <br />
                            <%= HtmlLabel(FilterTextBox,"ApplicantsFilter.Text", "Filter Applicants") %>
                            <asp:TextBox ID="FilterTextBox" runat="server" AutoCompleteType="Disabled" />
                            <br />
                            <br />
                            <asp:Panel ID="ApplicantsPanel" runat="server" CssClass="scrollableArea	">
                                <asp:ListView ID="ApplicantsListView" runat="server" OnItemDataBound="ApplicantsRepeater_ItemDataBound"
                                    ItemPlaceholderID="ApplicantsListPlaceHolder" DataKeyNames="Id">
                                    <LayoutTemplate>
                                        <table class="applicantsList">
                                            <asp:PlaceHolder ID="ApplicantsListPlaceHolder" runat="server" />
                                        </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td class="applicantName">
                                                <%# ((ApplicationViewDto)Container.DataItem).CandidateFirstName %>
                                                <%# ((ApplicationViewDto)Container.DataItem).CandidateLastName %>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ApplicationStatusDropDown" runat="server" ClientIDMode="Predictable" />
                                            </td>
                                            <%--<td>
                                                <asp:RequiredFieldValidator runat="server" ID="ApplicationStatusRequired" CssClass="error"
                                                    Display="Dynamic" SetFocusOnError="True" ControlToValidate="ApplicationStatusDropDown"
                                                    ClientIDMode="Predictable" ValidationGroup="JobActionModal"></asp:RequiredFieldValidator>
                                            </td>--%>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="PaddingRow" runat="server" visible="False">
            <td>
                <div style="height: 74px">
                    &nbsp;</div>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">
                <asp:Button ID="OkButton" runat="server" SkinID="Button3" CausesValidation="true" Title="OkButton"
                    ValidationGroup="JobActionModal" OnClick="OkButton_Click" />
                <asp:Literal ID="OkButtonSpacer" runat="server" EnableViewState="false" Text="  " />
                <asp:Button ID="CloseButton" runat="server" SkinID="Button4" Title="CloseButton" CausesValidation="false" 
                    OnClick="CloseButton_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>
<script type="text/javascript">
    function pageLoad() {
        $(document).ready(function () {
            $('#FilterTextBox').keyup(function () {
                var filter = $(this).val();
                $('.applicantsList .applicantName').each(function () {
                    if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                        $(this).parent().addClass("hidden");
                    } else {
                        $(this).parent().removeClass("hidden");
                    }
                });
            });
        });
    };

    function JobActionModal_ClosingDateValidation(sender, args) {
      var dateValid = $("#" + JobActionModal_ClientIds["ClosingDateIsValidCompareValidator"])[0].isvalid;

      var closingDateValidator = $("#" + JobActionModal_ClientIds["ClosingDateCompareValidator"]);
	    if (closingDateValidator.exists()) {
	    	ValidatorEnable(closingDateValidator[0], dateValid);
	    }
	    ValidatorEnable($("#" + JobActionModal_ClientIds["ClosingDateGreaterThanTodayCompareValidator"])[0], dateValid);

      if (dateValid) {
	      if (closingDateValidator.exists()) {
	      	ValidatorValidate(closingDateValidator[0]);
	      }
	      ValidatorValidate($("#" + JobActionModal_ClientIds["ClosingDateGreaterThanTodayCompareValidator"])[0]);
      }
    }
</script>
