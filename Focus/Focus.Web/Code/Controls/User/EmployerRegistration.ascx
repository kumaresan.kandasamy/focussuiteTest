﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployerRegistration.ascx.cs" Inherits="Focus.Web.Code.Controls.User.EmployerRegistration" %>

<%@ Register src="~/Code/Controls/User/EmployerRegistrationStep1.ascx" tagname="RegisterStep1" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/EmployerRegistrationStep2.ascx" tagname="RegisterStep2" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/EmployerRegistrationStep3.ascx" tagname="RegisterStep3" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/EmployerRegistrationStep4.ascx" tagname="RegisterStep4" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/EmployerRegistrationStep5.ascx" tagname="RegisterStep5" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<table width="100%" role="presentation">
	<tr>
		<td><h1><%= HtmlLocalise("Header.Text", "Register for an account")%></h1></td>
		<td align="right">
			<asp:Button ID="PreviousButton" runat="server" SkinID="Button3" OnClick="PreviousButton_Click" CausesValidation="false" />&nbsp;&nbsp;
			<asp:Button ID="NextButton" runat="server" SkinID="Button2" OnClick="NextButton_Click" />
      <focus:DoubleClickDisableButton ID="CompleteButton" runat="server" CssClass="button2" OnClick="CompleteButton_Click" />
		</td>
	</tr>
</table>

<%-- Progress Bar --%> 
<div class="progressBarWrap">
<table class="progressBar" role="presentation"> 
	<tr>
		<td class="first"><span ID="Step1ProgressBarItem" runat="server" EnableViewState="False"><%= HtmlLocalise("ProgressBar.Step1.Text", "Account Setup")%></span></td>
		<td><span ID="Step2And3ProgressBarItem" runat="server" EnableViewState="False"><%= HtmlLocalise("ProgressBar.Step2And3.Text", "Corporate Information")%></span></td>
		<td><span ID="Step4ProgressBarItem" runat="server" EnableViewState="False"><%= HtmlLocalise("ProgressBar.Step4.Text", "Contact Information")%></span></td>
		<td class="last"><span ID="Step5ProgressBarItem" runat="server" EnableViewState="False"><%= HtmlLocalise("ProgressBar.Step5.Text", "Terms of Use")%></span></td>
	</tr>
</table>
</div>
		
<asp:Panel style="clear:both;" id="EmployerRegistrationStepsPanel" runat="server" DefaultButton="NextButton">
	<uc:RegisterStep1 ID="Step1" runat="server" />
    <uc:RegisterStep2 ID="Step2" runat="server" OnChangeEmployer="Step2_ChangeEmployer" />
	<uc:RegisterStep3 ID="Step3" runat="server" OnEmployerChosen="Step3_EmployerChosen" />
	<uc:RegisterStep4 ID="Step4" runat="server" />				
	<uc:RegisterStep5 ID="Step5" runat="server" />
</asp:Panel>

<table width="100%" id="BottomNavigationTable" runat="server" role="presentation">
	<tr>
		<td align="right">
	    <asp:Button ID="PreviousBottomButton" runat="server" SkinID="Button3" OnClick="PreviousButton_Click" CausesValidation="false" />&nbsp;&nbsp;
	    <asp:Button ID="NextBottomButton" runat="server" SkinID="Button2" OnClick="NextButton_Click" />
	    <focus:DoubleClickDisableButton ID="CompleteBottomButton" runat="server" CssClass="button2" OnClick="CompleteButton_Click" />
		</td>
	</tr>
</table>

<%-- Confirmation Modal --%>
<uc:ConfirmationModal ID="Confirmation" runat="server" Height="250px" Width="400px" />

<script type="text/javascript">
  $(document).ready(function () {
    $('form').keypress(function (event) {
      if ($("#<%=CompleteButton.ClientID%>").exists()) {
        return false;
      }
      return true;
    });
  });
</script>