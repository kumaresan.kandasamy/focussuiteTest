﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfficeFilter.ascx.cs" Inherits="Focus.Web.Code.Controls.User.OfficeFilter" %>


<table style="border-collapse:collapse;" role="presentation">
	<tr>
		<td style="height:30px;width:120px;"><%= HtmlLabel(OfficeGroupDropDownList,"Office.Label", "For these offices")%></td>
		<td style="width:326px;">
		  <asp:DropDownList ID="OfficeGroupDropDownList" Visible="true" runat="server" CausesValidation="false" OnSelectedIndexChanged="OfficeGroupDropDownList_SelectedIndexChanged" AutoPostBack="True" Width="200"/>
		</td>
		<td>
			<asp:PlaceHolder runat="server" ID="AssignmentFilterPlaceHolder">
				<span style="display: inline-block; width: 90px;"><%= HtmlLabel(AssignmentsDropDownList,"AssignmentsDropDownList.Label", "Show")%></span>
				<asp:DropDownList ID="AssignmentsDropDownList" Visible="true" runat="server" CausesValidation="false"/>
			</asp:PlaceHolder>
			<asp:PlaceHolder runat="server" ID="StaffMemberFilterPlaceHolder">
				<span style="display: inline-block; width: 90px;"><%= HtmlLabel(StaffMembersDropDownList,"StaffMembersDropDownList.Label", "Handled by")%></span>
        <asp:DropDownList ID="StaffMembersDropDownList" Visible="true" runat="server" CausesValidation="false" />
			</asp:PlaceHolder>
		</td>
	</tr> 
	<tr id="OfficesRow" clientidmode="Static" runat="server">
		<td style="height:30px;"><%= HtmlLabel(OfficesDropDownList,"AssignedOffices.Label", "Offices")%></td>
		<td colspan="2">
			<asp:DropDownList ID="OfficesDropDownList" Visible="true" runat="server" CausesValidation="false" OnSelectedIndexChanged="OfficeDropDownList_SelectedIndexChanged" AutoPostBack="True" Width="160px"/>
		</td>
	</tr>
</table>
<asp:HiddenField runat="server" ID="IsEnabledHiddenField"/>

<script type="text/javascript">
	Sys.Application.add_load(OfficeFilter_PageLoad);

	function OfficeFilter_PageLoad() {
		var cdd = $find("OfficesCascadingDropDownBehavior");
		if (cdd != null)
			cdd.add_populated(onPopulated);
	}

	function onPopulated() {
		var ddl = document.getElementById('OfficesDropDownList');
		var count = ddl.options.length;

		if (count == 2) {
			ddl.disabled = true;
			ddl.selectedIndex = 1;
		}
	}
</script>
