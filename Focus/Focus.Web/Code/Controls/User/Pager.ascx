﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Pager.ascx.cs" Inherits="Focus.Web.Code.Controls.User.Pager" %>
<div style="border: 0;" class="<%= CssClass %>" role="presentation">
	<div class="pager-sizer">
		<%= HtmlLocalise("Display", "Display")%>&nbsp;<asp:DropDownList ID="PageSizer" ClientIDMode="AutoID"
			runat="server" CssClass="pager-sizer-list" CausesValidation="false" AutoPostBack="true"
			OnSelectedIndexChanged="PageSizer_SelectedIndexChanged">
			<asp:ListItem Text="10" Value="10" Selected="True" />
			<asp:ListItem Text="25" Value="25" />
			<asp:ListItem Text="50" Value="50" />
			<asp:ListItem Text="100" Value="100" />
		</asp:DropDownList>
		&nbsp;<%= HtmlLabel(PageSizer,"RecordsPerPage", "records per page")%>&nbsp;
		<asp:Literal runat="server" EnableViewState="false" ID="RecordCount" />&nbsp;
			<%= HtmlLocalise("Page", "Page")%>&nbsp;:&nbsp;
			<asp:DataPager ID="MainDataPager" runat="server" PageSize="10">
				<Fields>
					<asp:TemplatePagerField OnPagerCommand="MainDataPager_OnPagerCommand">
						<PagerTemplate>
							<asp:LinkButton ID="PreviousButton" ClientIDMode="AutoID" runat="server" CausesValidation="false"
								CommandName="Previous" CssClass='<%# (Container.StartRowIndex > 0) ? "pager-button" : "pager-button-disabled" %>'
								Enabled='<%# (Container.StartRowIndex > 0) %>'>&laquo; <%= HtmlLocalise("Previous", "Previous")%></asp:LinkButton>
							<focus:LocalisedLabel runat="server" ID="PageJumperLabel" AssociatedControlID="PageJumper"
								LocalisationKey="Page" DefaultText="Page" CssClass="sr-only" />
							<asp:DropDownList ID="PageJumper" ClientIDMode="AutoID" runat="server" CausesValidation="false"
								AutoPostBack="true" CssClass="pager-jumper" OnPreRender="PageJumper_PreRender"
								OnSelectedIndexChanged="PageJumper_SelectedIndexChanged" />
							<focus:LocalisedLabel runat="server" ID="PageNumberTextBoxLabel" AssociatedControlID="PageNumberTextBox"
								LocalisationKey="Page" DefaultText="Page" CssClass="sr-only" />
							<asp:TextBox ID="PageNumberTextBox" ClientIDMode="AutoID" runat="server" AutoPostBack="True"
								OnTextChanged="PageNumberTextBox_OnTextChanged" OnPreRender="PageNumberTextBox_OnPreRender"
								Text=""></asp:TextBox>
							&nbsp;<%= HtmlLocalise("Of", "of")%>&nbsp;<%# Math.Ceiling ((double)Container.TotalRowCount / Container.PageSize).ToString() %>
							<asp:LinkButton ID="NextButton" Title="Next button" ClientIDMode="AutoID" runat="server"
								CausesValidation="false" CommandName="Next" CssClass='<%# ((Container.StartRowIndex + Container.PageSize) < Container.TotalRowCount) ? "pager-button" : "pager-button-disabled" %>'
								Enabled='<%# ((Container.StartRowIndex + Container.PageSize) < Container.TotalRowCount) %>'><%= HtmlLocalise("Next", "Next")%> &raquo;</asp:LinkButton>
						</PagerTemplate>
					</asp:TemplatePagerField>
				</Fields>
			</asp:DataPager>
	</div>
</div>
