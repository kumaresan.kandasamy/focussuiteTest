﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Xml;
using System.Net;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;

using Framework.Core;

using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Services.Core.Extensions;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class JobPostingDetails:  UserControlBase
	{
		#region properties

		protected string EmailAddressRequired = "";
		protected string EmailAddressCharLimit = "";
		protected string EmailAddressErrorMessage = "";
		protected string FromUrl = "";
		protected string CustomerJobId = string.Empty;
		protected string BrandName = string.Empty;
		protected string LensPostingId = string.Empty;

		public int? CurrentPageNumber { get; set; }
		public long? JobseekerId { get; set; }

		private long? OriginId
		{
			get { return GetViewStateValue<long?>("JobPosting:OriginId"); }
			set { SetViewStateValue("JobPosting:OriginId", value); }
		}

		private bool ReferralEligible
		{
			get { return GetViewStateValue("JobPosting:ReferralEligible", false); }
			set { SetViewStateValue("JobPosting:ReferralEligible", value); }
		}

		private int ReferralScore
		{
			get { return GetViewStateValue("JobPosting:ReferralScore", 0); }
			set { SetViewStateValue("JobPosting:ReferralScore", value); }
		}

		private string ReferralMessage
		{
			get { return GetViewStateValue("JobPosting:ReferralMessage", ""); }
			set { SetViewStateValue("JobPosting:ReferralMessage", value); }
		}

		private string JobTitle
		{
			get { return GetViewStateValue("JobPosting:JobTitle", ""); }
			set { SetViewStateValue("JobPosting:JobTitle", value); }
		}

	
		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			string parentFormUrl = Page.RouteData.Values["fromURL"].ToString();
			string currentPageNumber = (CurrentPageNumber ?? 1).ToString();
			string jobseekerId = (JobseekerId ?? 0).ToString();

			BackHyperLink.NavigateUrl = GetRouteUrl(parentFormUrl,
				new { jobseekerid = jobseekerId, page = (currentPageNumber == "0" ? "1" : currentPageNumber) });
			if (jobseekerId.IsNotNullOrEmpty())
			{
				JobPostingToolsCriteria.JobSeekerId = Convert.ToInt64(jobseekerId);
			}

			LensPostingId = Page.RouteData.Values["jobid"].ToString();
			JobPostingToolsCriteria.LensPostingId = LensPostingId;
			JobPostingToolsCriteria.BackUrl = BackHyperLink.NavigateUrl;
			
			
			// Logs the candidate action
			ServiceClientLocator.CandidateClient(App).ViewJobDetails(App.User.PersonId, LensPostingId);

			LocaliseUI();

            if (!IsPostBack)
            {
                DisplayJobPosting(LensPostingId);
            }
            else
            {
                DisplayJob(LensPostingId);
            }
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			EmailAddressRequired = CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required");
			EmailAddressCharLimit = CodeLocalise("EmailAddressCharLimit.ErrorMessage", "Email address must be at least 6 characters");
			EmailAddressErrorMessage = CodeLocalise("EmailAddress.ErrorMessage", "Email address format is invalid");
		}
		
		/// <summary>
		/// Handles the Click event of the DoNotDisplayHyperLink control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DoNotDisplayHyperLink_Click(object sender, EventArgs e)
		{
			if (App.User.IsAuthenticated)
			{
				try
				{
					if (LensPostingId.IsNotNullOrEmpty())
						ServiceClientLocator.OrganizationClient(App).SetDoNotDisplay(LensPostingId);

					Response.Redirect(BackHyperLink.NavigateUrl);
				}
				catch (ApplicationException ex)
				{
					MasterPage.ShowError(AlertTypes.Error, ex.Message);
				}
			}
			//else
			//  ConfirmationModal.Show();
		}

		/// <summary>
		/// Displays the job posting.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		private void DisplayJobPosting(string lensPostingId)
		{
			App.RemoveSessionValue("Career:XMLPosting");
			App.RemoveSessionValue("Career:XMLRequirement");
			App.RemoveSessionValue("Career:XMLEosPosting");
			App.RemoveSessionValue("Career:DisplayPosting");

			try
			{
				var post = ServiceClientLocator.PostingClient(App).GetJobPosting(lensPostingId);
        var focusJobId = post.PostingInfo.FocusJobId;
        var focusPostingId = post.PostingInfo.FocusPostingId;

        JobPostingToolsCriteria.ViewCount = post.PostingInfo.ViewedCount;

				if (post.PostingInfo.IsNotNull())
				{
					OriginId = post.PostingInfo.PostingOriginId;

					CustomerJobId = post.PostingInfo.CustomerJobId;

					BrandName = App.Settings.BrandName;
					lblCustomerJobID.Text = (CustomerJobId.IsNotNullOrEmpty() && (CustomerJobId != "-999")) ? string.Format(CodeLocalise("CustomerJobId.Label", "Customer Job ID {0}"), CustomerJobId) : String.Empty;
          lblJobID.Text = string.Format(CodeLocalise("CustomerPostingId.Label", "{0} Posting ID {1}"), App.Settings.BrandName, focusPostingId);
          if (focusJobId != null) lblJobID.Text += string.Format(CodeLocalise("CustomerJobId.Label", " (Job ID {0})"), focusJobId);

					lblPostingTitle.Text = post.PostingInfo.Title;
					JobTitle = post.PostingInfo.Title;
				}

				if (post.PostingXml.IsNotNullOrEmpty())
					App.SetSessionValue("Career:XMLPosting", post.PostingXml);

				if (App.Settings.JobRequirements)
				{
					if (post.RequirementsXml.IsNotNullOrEmpty())
						App.SetSessionValue("Career:XMLRequirement", post.RequirementsXml);

					if (post.EosPostingXml.IsNotNullOrEmpty())
						App.SetSessionValue("Career:XMLEosPosting", post.EosPostingXml);
				}

				var postingHtml = post.PostingHtml;
				var jobId = ServiceClientLocator.PostingClient(App).GetJobIdFromPosting(LensPostingId);

				if (jobId.HasValue && jobId > 0)
				{
					var job = ServiceClientLocator.JobClient(App).GetJob(Convert.ToInt64(jobId));

					postingHtml = Utilities.InsertPostingFooter(postingHtml, job.CriminalBackgroundExclusionRequired, job.CreditCheckRequired);

					if (job.VeteranPriorityEndDate.IsNotNull() && job.VeteranPriorityEndDate.Value > DateTime.Now)
					{
						VeteranDetails.Text = CodeLocalise("VeteranDetails.Text", "This posting is currently available to veteran job seekers only until {0} {1}.", job.VeteranPriorityEndDate.Value.ToShortDateString(), job.VeteranPriorityEndDate.Value.ToShortTimeString());
						VeteranDetails.Visible = true;
					}
				}

				App.SetSessionValue("Career:DisplayPosting", postingHtml.AddAltTagWithValueForImg("Posting images"));

				if (App.User.IsAuthenticated)
				{
					ServiceClientLocator.PostingClient(App).AddViewedPosting(new ViewedPostingDto { LensPostingId = lensPostingId });
				}

				if (App.Settings.Module == FocusModules.Assist)
				{
					PostingDetail.Attributes["src"] = GetRouteUrl("AssistJobDisplay", new {jobid = lensPostingId});
				}
				else if (App.Settings.Module == FocusModules.Talent)
				{
					PostingDetail.Attributes["src"] = GetRouteUrl("TalentJobDisplay", new { jobid = lensPostingId });
				}
				
				if (OriginId.HasValue && OriginId.IsTalentOrigin(App.Settings))
					PostingDetail.Attributes["onload"] = string.Format("JobPosting_DisableJobSeekerLink('{0}')", App.Settings.CareerApplicationPath);
			}
			catch (ApplicationException ex)
			{
				ConfirmationBookMarkModal.Show("", ex.Message + "  We are taking you to previous page.", "Ok", "REDIRECT_PREVIOUS", height: 50);
			}
		}

        private void DisplayJob(string lensPostingId)
        {
            App.RemoveSessionValue("Career:XMLPosting");
            App.RemoveSessionValue("Career:XMLRequirement");
            App.RemoveSessionValue("Career:XMLEosPosting");
            App.RemoveSessionValue("Career:DisplayPosting");
            try
            {
                var post = ServiceClientLocator.PostingClient(App).GetJobPosting(lensPostingId);
                if (post.PostingXml.IsNotNullOrEmpty())
                    App.SetSessionValue("Career:XMLPosting", post.PostingXml);

                if (App.Settings.JobRequirements)
                {
                    if (post.RequirementsXml.IsNotNullOrEmpty())
                        App.SetSessionValue("Career:XMLRequirement", post.RequirementsXml);

                    if (post.EosPostingXml.IsNotNullOrEmpty())
                        App.SetSessionValue("Career:XMLEosPosting", post.EosPostingXml);
                }

                var postingHtml = post.PostingHtml;
				var jobId = ServiceClientLocator.PostingClient(App).GetJobIdFromPosting(LensPostingId);

                if (jobId.HasValue && jobId > 0)
                {
                    var job = ServiceClientLocator.JobClient(App).GetJob(Convert.ToInt64(jobId));

                    postingHtml = Utilities.InsertPostingFooter(postingHtml, job.CriminalBackgroundExclusionRequired, job.CreditCheckRequired);
                }

                App.SetSessionValue("Career:DisplayPosting", post.PostingHtml.AddAltTagWithValueForImg("Posting images"));
            }

            catch (ApplicationException ex)
            {
                ConfirmationBookMarkModal.Show("", ex.Message + "  We are taking you to previous page.", "Ok", "REDIRECT_PREVIOUS", height: 50);
            }

        }

		/// <summary>
		/// Handles the CloseCommand event of the ConfirmationModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		public void ConfirmationModal_CloseCommand(object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "REDIRECT_PREVIOUS":
					try
					{
						Response.Redirect(GetRouteUrl(FromUrl, null));
					}
					catch (Exception)
					{ }

					break;
			}
		}

		/// <summary>
		/// Handles the Click event of the btnReportSpamOrClosed control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void btnReportSpamOrClosed_Click(object sender, EventArgs e)
		{
			if (App.User.IsAuthenticated)
			{
				var xmlposting = App.GetSessionValue<string>("Career:XMLPosting");
				var displayposting = App.GetSessionValue<string>("Career:DisplayPosting");

				if (xmlposting.IsNotNull())
				{
					var jobOriginId = "";
					var jobId = Page.RouteData.Values["jobid"] as string;
					var applicationUrl = Request.Url.Scheme + "://" + Request.Url.Host + ":" + Request.Url.Port;
					var navigateUrl = "";

					var xDoc = new XmlDocument { PreserveWhitespace = true };
					xDoc.LoadXml(xmlposting);

					if (xDoc.SelectSingleNode("//cf012") != null && !String.IsNullOrEmpty(xDoc.SelectSingleNode("//cf012").InnerText))
						jobOriginId = xDoc.SelectSingleNode("//cf012").InnerText;

					if (xDoc.SelectSingleNode("//joburl") != null && !String.IsNullOrEmpty(xDoc.SelectSingleNode("//joburl").InnerText))
						navigateUrl = xDoc.SelectSingleNode("//joburl").InnerText;

					if (ddlReportSpamOrClosed.SelectedValue == "SPAM")  
					{
						if (App.Settings.SpamJobOriginId.IsNotNullOrEmpty() && App.Settings.SpamJobOriginId.Split(',').Contains(jobOriginId))
						{
							var mailBody = "<font face='arial'>This job has been reported as spam by the jobseeker.<br/><br/>";
							mailBody += "<br/><b>Application URL:</b> " + applicationUrl;
							if (App.User.IsAuthenticated)
								mailBody += "<br/><b>Jobseeker Username:</b> " + App.User.EmailAddress;
							if (!String.IsNullOrEmpty(navigateUrl))
								mailBody += "<br/><b>Job URL:</b> " + navigateUrl;
							mailBody += "<br/><b>Job ID:</b> " + jobId;
							mailBody += "<br/><b>Reason:</b> Reported as spam.";
							mailBody += "</font><br/><br/>";
							mailBody += "<b>JOB DETAILS</b><hr/><br/>" + displayposting;

							try
							{
								ServiceClientLocator.CoreClient(App).SendEmail(App.Settings.SpamMailAddress, CodeLocalise("ReportAsJobAsSpam.EmailSubject", "Reported the Job as Spam – ID {0}", jobId), mailBody, true);

								MasterPage.ShowModalAlert(AlertTypes.Info, "Thank you for reporting this job as spam. Your report has been referred to site administrators for further review.", "Thank you");
							}
							catch (Exception ex)
							{
								MasterPage.ShowError(AlertTypes.Error, ex.Message);
							}
						}
					}
					else if (ddlReportSpamOrClosed.SelectedValue == "CLOSED")
					{
						if (App.Settings.ClosedJobOriginId.IsNotNullOrEmpty() && App.Settings.ClosedJobOriginId.Split(',').Contains(jobOriginId))
						{
							var phrase = "";
							var jobClosedAtSource = true;

							if (navigateUrl.IsNotNullOrEmpty())
							{
								try
								{
									var urlCheck = new Uri(navigateUrl);
									var request = (HttpWebRequest)WebRequest.Create(urlCheck);
									//Timeout is set to '30000' to get the response from Https links also.
									request.Timeout = 30000;
									var response = (HttpWebResponse)request.GetResponse();

									if (response.StatusCode == HttpStatusCode.OK)
									{
										var receiveStream = response.GetResponseStream();
										var encode = System.Text.Encoding.GetEncoding("utf-8");

										if (receiveStream.IsNotNull())
										{
											var sReader = new StreamReader(receiveStream, encode);
											var siteData = sReader.ReadToEnd().ToLower();
											sReader.Close();

											string[] jobClosePhrases = { "no longer available", "no longer exists", "no longer running", "job is closed", "job closed", "posting closed", "posting is closed", "job has expired", "position has been filled", "no longer active", "job posting you are looking for has expired", "position has already been filled" };
											foreach (var close_phrase in jobClosePhrases)
											{
												if (siteData.Contains(close_phrase) && (siteData.Contains("job") || siteData.Contains("posting")))
												{
													phrase = close_phrase;
													break;
												}
											}
										}
									}
									else
									{
										phrase = "NO_URL";
									}
								}
								catch (Exception ex)
								{
									//Error: '410' means that the URL is dead or not usable anymore.
									//Error: '404' means that the site can't find the page we are looking for.
									if (ex.Message == "Unable to cast object of type 'System.Net.FileWebRequest' to type 'System.Net.HttpWebRequest'." ||
																																									ex.Message == "The remote server returned an error: (410) Gone." ||
																																									ex.Message == "The remote server returned an error: (404) Not Found.")
										phrase = "NO_URL";
								}

								if (String.IsNullOrEmpty(phrase))
									jobClosedAtSource = false;
							}

							var jobClosed = false;

							if (jobClosedAtSource)
								jobClosed = ServiceClientLocator.PostingClient(App).CloseJobPosting(jobId);

							string mailBody;

							if (jobClosed)
							{
								mailBody = "<font face='arial'>Based on the request from the jobseeker we validated the job posting URL and found to be closed or no longer available.<br/><br/>";
								mailBody += "<b>More information:</b> " + ((phrase == "NO_URL") ? "URL broken" : ("Found keywords '" + phrase + "'"));
								mailBody += "<br/><b>Application URL:</b> " + applicationUrl;
								mailBody += "<br/><b>Jobseeker Username:</b> " + App.User.EmailAddress;
								if (navigateUrl.IsNotNullOrEmpty())
									mailBody += "<br/><b>Job URL:</b> " + navigateUrl;
								mailBody += "<br/><b>Job ID:</b>" + jobId;
								mailBody += "</font><br><br>";
							}
							else
							{
								mailBody = "<font face='arial'>The following job is reported as closed.<br/><br/>";
								mailBody += "<br/><b>Application URL:</b> " + applicationUrl;
								mailBody += "<br/><b>Jobseeker Username:</b> " + App.User.EmailAddress;
								if (navigateUrl.IsNotNullOrEmpty())
									mailBody += "<br/><b>Job URL:</b> " + navigateUrl;
								mailBody += "<br/><b>Job ID:</b>" + jobId;
								mailBody += "</font><br><br>";
							}

							try
							{
								ServiceClientLocator.CoreClient(App).SendEmail(App.Settings.SpamMailAddress,
																							CodeLocalise("ReportAsJobAsClosed.EmailSubject",
																													 "Reported the Job as Closed – ID {0}", jobId), mailBody, true);
								MasterPage.ShowModalAlert(AlertTypes.Info,
																					"Thank you for reporting this job as closed. Your report has been referred to site administrators for further review.",
																					"Thank you");
							}
							catch (Exception ex)
							{
								MasterPage.ShowError(AlertTypes.Error, ex.Message);
							}
						}
					}
				}
			}
		}
	}
}
