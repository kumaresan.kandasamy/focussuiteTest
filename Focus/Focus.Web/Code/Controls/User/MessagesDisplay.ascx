﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessagesDisplay.ascx.cs" Inherits="Focus.Web.Code.Controls.User.MessagesDisplay" %>
<%@ Import Namespace="Focus.Core.Views" %>
<asp:UpdatePanel ID="MessagesUpdatePanel" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
		<asp:Repeater	ID="MessagesRepeater" runat="server" OnItemCommand="MessagesRepeater_ItemCommand">
			<ItemTemplate>
				<p class="messageStatus">
				<asp:ImageButton ID="DismissMessageButton" runat="server" CommandName="Dismiss" CommandArgument="<%# ((MessageView)Container.DataItem).Id%>" ImageUrl="<%# UrlBuilder.ButtonCloseIcon() %>" AlternateText="Close Message" CausesValidation="False" ImageAlign="Top" />
				<%# ((MessageView)Container.DataItem).Text%>	
				</p>
			</ItemTemplate>
		</asp:Repeater>
	</ContentTemplate>
</asp:UpdatePanel>