<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobWizardStep3Path2Part1.ascx.cs" Inherits="Focus.Web.Code.Controls.User.JobWizardStep3Path2Part1" %>
<%@ Register src="~/Code/Controls/User/ScrollableSkillsAccordion.ascx" tagname="SkillsAccordion" tagprefix="uc" %>    
<asp:Panel ID="MainPanel" runat="server"> 
  <asp:UpdatePanel runat="server" ID="TasksUpdatePanel" UpdateMode="Conditional">
  <ContentTemplate>                        
  <table width="100%">
	  <tr>
		<td><h1><asp:Label ID="JobTitleLabel" runat="server" /></h1></td>
	  </tr>                
    <tr>
      <td>
        <asp:UpdatePanel ID="OccupationsUpdatePanel" runat="server" UpdateMode="Conditional">
          <ContentTemplate>
            <asp:RadioButtonList ID="OccupationsRadioButtonList" runat="server" AutoPostBack="true" ClientIDMode="AutoID" RepeatLayout="Flow" RepeatDirection="Vertical" OnSelectedIndexChanged="OccupationsRadioButtonList_SelectedIndexChanged"></asp:RadioButtonList>
          </ContentTemplate>
        </asp:UpdatePanel>
      </td>
      <br/>
    </tr>
  </table>
  <asp:Panel ID="TasksPanel" runat="server" Style="height: 360px;" ScrollBars="Auto">
	  <table width="100%">
		  <tr>
			  <td width="40px" />
			  <td width="10px" />
			  <td width="20px" />
			  <td />
		  </tr>
      <tr id="ItemsRow" runat="server">
		    <td colspan="4"><asp:Literal runat="server" ID="InstructionsLabel"></asp:Literal><br /><br /></td>
	    </tr>
	    <tr id="ApplicantRow" runat="server">
		    <td colspan="4"><%= HtmlLocalise("Instructions.Text", "The applicant will:") %></td>
	    </tr>

		  <asp:Repeater ID="TasksRepeater" runat="server" OnItemDataBound="TasksRepeater_ItemDataBound">
			  <ItemTemplate>
				<tr>
					<td><asp:HiddenField ID="JobTaskId" runat="server" /></td> 
					<td><asp:CheckBox ID="TaskSelectedCheckBox" runat="server" /></td>
					<td colspan="2"><asp:Literal ID="TaskPrompt" runat="server" /></td>
				</tr>
				<tr id="MultiOptionsRow" runat="server">
					<td colspan="3"/>					
					<td><asp:CheckBoxList ID="MultiOptionsCheckBoxList" runat="server" RepeatColumns="2" CellSpacing="5" /></td>
				</tr>
				<tr id="OpenRow" runat="server">
					<td colspan="3"/>
					<td><asp:TextBox ID="OpenTextBox" runat="server" TextMode="MultiLine" Rows="2" Width="90%" /><br/>
          <asp:CustomValidator runat="server" ID="OpenTextBoxValidator" ClientValidationFunction="openTextBoxValidation"  SetFocusOnError="true" Display="Dynamic" CssClass="error" /></td>
				</tr>
				<tr><td colspan="4" style="padding-bottom:10px;"></td></tr>
			</ItemTemplate>
		  </asp:Repeater>
  	</table>
  </asp:Panel>
   <asp:Panel runat="server" ID="SkillsAccordionPanel" ClientIDMode="Static">
    <table width="100%">
     <tr>
			<td height="30px"/>
		</tr>
    <tr>
			<td>
			  <asp:CustomValidator  runat="server" id="SkillsAccordionValidator" ControlToValidate="OccupationsRadioButtonList" ClientValidationFunction="ValidateInternshipSkills" Display="Dynamic" CssClass="error" />
			  <uc:SkillsAccordion runat="Server" ID="SkillsAccordion" WrapperCssClass="skillsAccordionWrapper" AccordionCssClass="skillsAccordion" SummaryCssClass="skillsAccordionSummary"/>
       </td>
		</tr>
    </table>
  </asp:Panel>
   </ContentTemplate>
    <Triggers>
		  <asp:AsyncPostBackTrigger ControlID="OccupationsRadioButtonList" EventName="SelectedIndexChanged" />
	  </Triggers>
   </asp:UpdatePanel> 
</asp:Panel>


<script type="text/javascript">
  $(document).ready(function () {
    $('*[data-disable=1]').attr("disabled", true); ;
  });

  function ValidateInternshipSkills(source, arguments) {
    if ($('#<%=OccupationsRadioButtonList.ClientID %> input[type=radio]:checked').val() == 'Internship') {
      var checkCount = 0;
      $('#SkillsAccordionPanel input:checkbox').each(function () {
        if ($(this).is(':checked')) {
          checkCount++;
        }
      });

      // ReSharper disable ExpressionIsAlwaysConst
      arguments.IsValid = (checkCount > 0);
      // ReSharper restore ExpressionIsAlwaysConst
    } else {
      arguments.IsValid = true;
    }

  }

  function EnableDisableMultiOptionsTask(parentCheckBox, childCheckBoxList) {
    if (!$("#" + parentCheckBox).is(":checked")) {
      $("table[id$=" + childCheckBoxList + "] input").attr('checked', false);
      $("table[id$=" + childCheckBoxList + "] *[data-disable]").attr("disabled", true);
    }
    else
      $("table[id$=" + childCheckBoxList + "] *[data-disable]").attr("disabled", false);

    return false;
  }

  function EnableDisableOpenTask(parentCheckBox, childOpenTextBox) {
    if (!$("#" + parentCheckBox).is(":checked")) {
      $("#" + childOpenTextBox).val("");
      $("#" + childOpenTextBox).attr("disabled", true);
    }
    else
      $("#" + childOpenTextBox).attr("disabled", false);

    return false;
  }
  function openTextBoxValidation(source, arguments) {
    var txtBox = document.getElementById(source.txtId);
    if (txtBox != null) {
      var textEntered = true;
      if (txtBox.disabled == false) {
        if (txtBox.value.length == 0) {
          textEntered = false;
        }
      }
      arguments.IsValid = textEntered;
    } else {
      arguments.IsValid = true;
    }
  }
</script>


