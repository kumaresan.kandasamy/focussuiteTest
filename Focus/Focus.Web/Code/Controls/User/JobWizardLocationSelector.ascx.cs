#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Framework.Core;
using Focus.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
    public partial class JobWizardLocationSelector : UserControlBase
    {
        private JobLocationDto _locationToAdd;

        protected List<JobLocationDto> JobLocations
        {
            get { return GetViewStateValue<List<JobLocationDto>>("JobWizardLocationSelector:JobLocations"); }
            set { SetViewStateValue("JobWizardLocationSelector:JobLocations", value); }
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            LocaliseUI();
            if (!Page.IsPostBack)
            {
                BindControls();
                AddressPostcodeRegexValidator.ValidationExpression = App.Settings.ExtendedPostalCodeRegExPattern;
            }
        }

        /// <summary>
        /// Handles the Click event of the AddButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void AddButton_Click(object sender, EventArgs e)
        {
            if (_locationToAdd.IsNull())
                return;

            UnbindItemsList();
            if (JobLocations.IsNull()) JobLocations = new List<JobLocationDto>();
            JobLocations.Add(_locationToAdd);

            LocationTextBox.Text = string.Empty;
            BindItemsList();
        }

        /// <summary>
        /// Handles the Command event of the ItemsListRemoveButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
        protected void ItemsListRemoveButton_Command(object sender, CommandEventArgs e)
        {
            // This is a hack to fix the fact the ListView doesn't 
            // bind the <%# ... %> tags so we have to place it outside
            locationsTable.Visible = false;

            UnbindItemsList();
            var rowNo = Convert.ToInt32(e.CommandArgument);
            JobLocations.RemoveAt(rowNo);
            BindItemsList();
        }

        /// <summary>
        /// Handles the DataBound event of the ItemsList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
        protected void ItemsList_DataBound(object sender, ListViewItemEventArgs e)
        {
            var jobLocation = (JobLocationDto)e.Item.DataItem;
            var publicTransitCheckBox = (CheckBox)e.Item.FindControl("PublicTransitCheckBox");
            publicTransitCheckBox.Checked = jobLocation.IsPublicTransitAccessible;

            // This is a hack to fix the fact the ListView doesn't 
            // bind the <%# ... %> tags so we have to place it outside
            locationsTable.Visible = true;
        }

        /// <summary>
        /// Binds the specified job locations.
        /// </summary>
        /// <param name="jobLocations">The job locations.</param>
        public void Bind(List<JobLocationDto> jobLocations)
        {
            JobLocations = jobLocations;
            BindItemsList();
        }

        /// <summary>
        /// Unbinds this instance.
        /// </summary>
        /// <returns></returns>
        public List<JobLocationDto> Unbind()
        {
            UnbindItemsList();
            return JobLocations;
        }

        /// <summary>
        /// The number of job locations selected
        /// </summary>
        /// <returns></returns>
        public int JobLocationCount()
        {
            return JobLocations.IsNull() ? 0 : JobLocations.Count();
        }

        public LocationSelectorMode Mode
        {
            get { return GetViewStateValue<LocationSelectorMode>("JobWizardLocationSelector:LocationSelectorMode"); }
            set { SetViewStateValue("JobWizardLocationSelector:LocationSelectorMode", value); }
        }

        public enum LocationSelectorMode
        {
            Standard,
            FullAddressSingle,
            FullAddressMany
        }

        /// <summary>
        /// Localises the UI.
        /// </summary>
        private void LocaliseUI()
        {
            AddButton.Text = CodeLocalise("Global.Add.Text", "Add");
            AddFullAddressButton.Text = CodeLocalise("Global.Add.Text", "Add");
            AddressLine1Required.ErrorMessage = CodeLocalise("AddressLine1.RequiredErrorMessage", "Address (line 1) is required.");
            AddressTownCityRequired.ErrorMessage = CodeLocalise("AddressTownCity.RequiredErrorMessage", "City is required.");
            AddressCountyRequired.ErrorMessage = CodeLocalise("AddressCounty.RequiredErrorMessage", "County is required.");
            AddressStateRequired.ErrorMessage = CodeLocalise("AddressState.RequiredErrorMessage", "State is required.");
            AddressPostcodeZipRequired.ErrorMessage = CodeLocalise("AddressPostcodeZip.RequiredErrorMessage", "ZIP or postal code is required.");
            AddressPostcodeRegexValidator.ErrorMessage = CodeLocalise("AddressPostcodeRegexValidator.ErrorMessage", "ZIP code must be in a 5 or 9-digit format");
        }

        /// <summary>
        /// Binds the items list.
        /// </summary>
        private void BindItemsList()
        {
            ItemsList.DataSource = JobLocations;
            ItemsList.DataBind();

            LocationSelectorHasRowsHidden.Value = (JobLocations.IsNotNullOrEmpty()) ? "true" : "false";
        }

        private void BindControls()
        {
            AddressStateDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States), null, CodeLocalise("Global.State.TopDefault", "- select state -"));

            CountyPlaceHolder.Visible = App.Settings.CountyEnabled;
            if (App.Settings.CountyEnabled)
            {
                AddressCountyDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Counties), null, CodeLocalise("Global.County.TopDefault", "- select county -"));
                AddressCountyDropDownListCascadingDropDown.PromptText = CodeLocalise("Global.County.TopDefault", "- select county -");
                AddressCountyDropDownListCascadingDropDown.LoadingText = CodeLocalise("Global.County.Progress", "[Loading counties ...]");
                AddressCountyDropDownListCascadingDropDown.PromptValue = string.Empty;
            }
        }

        /// <summary>
        /// Unbinds the items list.
        /// </summary>
        private void UnbindItemsList()
        {
            foreach (var item in ItemsList.Items)
            {
                var publicTransitCheckBox = (CheckBox)item.FindControl("PublicTransitCheckBox");
                JobLocations[item.DataItemIndex].IsPublicTransitAccessible = publicTransitCheckBox.Checked;
            }
        }

        /// <summary>
        /// Handles the ServerValidate event of the AddFullAddressButton_OnClick control.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="args">The <see cref="System.Web.UI.WebControls.ServerValidateEventArgs"/> instance containing the event data.</param>
        protected void LocationValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;
           _locationToAdd = null;
            var countyId = App.Settings.CountyEnabled
                ? AddressCountyDropDownList.SelectedValueToNullableLong()
                : null;

            var county = countyId.IsNotNullOrZero()
                ? ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Counties).Where(c => c.Id == countyId).Select(c => c.Text).FirstOrDefault()
                : null;

            _locationToAdd = new JobLocationDto
            {
                AddressLine1 = AddressLine1TextBox.Text,
                City = AddressTownCityTextBox.Text,
                State = AddressStateDropDownList.SelectedItem.Text,
                County = county,
                Zip = AddressPostcodeZipTextBox.Text,
                Location = BuildWorkLocationString(AddressLine1TextBox.TextTrimmed(), AddressTownCityTextBox.TextTrimmed(), county, AddressStateDropDownList.SelectedItem.Text, AddressPostcodeZipTextBox.TextTrimmed())
            };

            if (JobLocations.IsNotNullOrEmpty())
            {
                
                //if (JobLocations.Any(j => j.Zip.Equals(_locationToAdd.Zip)))
                //{
                //    LocationValidator.ErrorMessage = CodeLocalise("LocationValidator.DuplicateLocation", "You have enterted a duplicate zip");
                //    _locationToAdd = null;
                //    args.IsValid = false;

                //}
                //else if (JobLocations.Any(j =>j.City.Equals(_locationToAdd.City)))
                //{
                //    LocationValidator.ErrorMessage = CodeLocalise("LocationValidator.DuplicateLocation", "You have enterted a duplicate city");
                //    _locationToAdd = null;
                //    args.IsValid = false;
                //}
                //else
                if (JobLocations.Any(j => j.Location.Equals(_locationToAdd.Location)))
                {
                    LocationValidator.ErrorMessage = CodeLocalise("LocationValidator.DuplicateLocation", "You have entered a duplicate location");
                       _locationToAdd = null;
                       args.IsValid = false;
                   
                }
            }
        }

        /// <summary>
        /// Handles the ServerValidate event of the LocationTextBoxFormatValidator control.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="args">The <see cref="System.Web.UI.WebControls.ServerValidateEventArgs"/> instance containing the event data.</param>
        protected void LocationTextBoxFormatValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;
            _locationToAdd = null;

            var value = (args.Value.IsNotNullOrEmpty() ? args.Value.Trim() : "");
            if (value.IsNullOrEmpty()) return;

            // Validate that the location is in either "city, state" or "postal code" format
            var isCityState = Regex.IsMatch(value, @"(\w+, \w+)");
            var isPostalCode = Regex.IsMatch(value, App.Settings.PostalCodeRegExPattern);

            if (!isCityState && !isPostalCode)
            {
                LocationTextBoxValidator.ErrorMessage = CodeLocalise("LocationTextBoxValidator.InvalidInputError", "You must enter the location either as city, state or just a zip code");
                args.IsValid = false;
                return;
            }

            PostalCodeViewDto location;
            string zip;

            if (isCityState)
            {
                // Validate that if it is a city, state that we can find the postal for it
                var parts = value.Split(',');
                if (parts[1].IsNotNullOrEmpty())
                    parts[1] = parts[1].Trim();

                location = ServiceClientLocator.CoreClient(App).EstablishLocation(null, parts[0], parts[1]);
                if (location == null)
                {
                    LocationTextBoxValidator.ErrorMessage = CodeLocalise("LocationTextBoxValidator.UnknownCityStateError", "Unable to locate the zip code for the specified city or state");
                    args.IsValid = false;
                    return;
                }

                zip = location.Code;
            }
            else
            {
                // Validate that if it is a zip that we can get the city, state for it
                location = ServiceClientLocator.CoreClient(App).EstablishLocation(value, null, null);
                if (location == null)
                {
                    LocationTextBoxValidator.ErrorMessage = CodeLocalise("LocationTextBoxValidator.UnknownZipCodeError", "Unable to locate the city and state for the specified zip code");
                    args.IsValid = false;
                    return;
                }
                zip = value;
            }

            _locationToAdd = new JobLocationDto
            {
                AddressLine1 = null,
                City = location.CityName,
                County = App.Settings.CountyEnabled ? location.CountyName : null,
                State = location.StateName,
                Zip = zip,
                Location = BuildWorkLocationString(null, location.CityName, location.CountyName, location.StateKey.Replace("State.", ""), zip)
            };

            if (JobLocations.IsNotNullOrEmpty() && _locationToAdd.IsNotNull())
            {
                if (JobLocations.Any(j => j.Location.Equals(_locationToAdd.Location)))
                {
                    LocationTextBoxValidator.ErrorMessage = CodeLocalise("LocationTextBoxValidator.DuplicateLocation", "You have entered a duplicate location");
                    _locationToAdd = null;
                    args.IsValid = false;
                    
                }
            }

           
                       

        }

        protected void AddFullAddressButton_OnClick(object sender, EventArgs e)
        {
            if (_locationToAdd.IsNull())
                    return;
                UnbindItemsList();
                if (JobLocations.IsNull()) JobLocations = new List<JobLocationDto>();
                JobLocations.Add(_locationToAdd);
                AddressLine1TextBox.Text = string.Empty;
                AddressTownCityTextBox.Text = string.Empty;
                AddressCountyDropDownList.SelectedIndex = 0;
                AddressStateDropDownList.SelectedIndex = 0;
                AddressPostcodeZipTextBox.Text = string.Empty;
                BindItemsList();
            
           
        }

        /// <summary>
        /// Builds up the work location string
        /// </summary>
        /// <param name="line1">Address Line 1</param>
        /// <param name="city">The city</param>
        /// <param name="county">The county</param>
        /// <param name="state">The state</param>
        /// <param name="postalCode">The postal code</param>
        /// <returns>The work location string</returns>
        private string BuildWorkLocationString(string line1, string city, string county, string state, string postalCode)
        {
            var addressFields = new List<string>();

            if (line1.IsNotNullOrEmpty())
                addressFields.Add(line1);

            if (city.IsNotNullOrEmpty())
                addressFields.Add(city);

            if (App.Settings.CountyEnabled && county.IsNotNullOrEmpty())
                addressFields.Add(county);

            addressFields.Add(state);

            return string.Concat(string.Join(", ", addressFields), " (", postalCode, ")");
        }
    }
}