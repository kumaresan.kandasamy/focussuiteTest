﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Services.Core.Extensions;
using Framework.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class CandidateSearchCriteriaSummary : UserControlBase
	{
		public CandidateSearchCriteria SearchCriteria { get; set; }

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				SummaryLiteral.Text = (SearchCriteria.IsNotNull()) ? FormatCriteria()  : "";
			}
		}

		/// <summary>
		/// Updates the search criteria.
		/// </summary>
		/// <param name="searchCriteria">The search criteria.</param>
		public void UpdateSearchCriteria(CandidateSearchCriteria searchCriteria)
		{
			SearchCriteria = searchCriteria;
			SummaryLiteral.Text = (SearchCriteria.IsNotNull()) ? FormatCriteria() : "";
		}

		/// <summary>
		/// Formats the criteria.
		/// </summary>
		/// <returns></returns>
		private string FormatCriteria()
		{
			var result = new StringBuilder("");

		  bool isJob = true;
      if (SearchCriteria.AdditionalCriteria.IsNotNull())
      {
        if (SearchCriteria.AdditionalCriteria.JobTypesConsidered.IsNotIn(JobTypes.None, JobTypes.Job))
          isJob = false;
      }

			if (SearchCriteria.IsNotNull())
			{
				if (SearchCriteria.JobDetailsCriteria.IsNotNull())
				{
				  var jobTitleLabel = isJob
				                        ? CodeLocalise("SearchCriteriaJobTitle.Label", "Job title")
				                        : CodeLocalise("SearchCriteriaInternshipTitle.Label", "Internship title");
					if (SearchCriteria.JobDetailsCriteria.JobId.IsNotNull() && SearchCriteria.JobDetailsCriteria.JobId > 0)
					{
						var jobDetails = ServiceClientLocator.JobClient(App).GetJobDetails(SearchCriteria.JobDetailsCriteria.JobId);
            result.AppendFormat("{0}: {1}<br/>", jobTitleLabel, jobDetails.JobTitle);
					}
					else if (SearchCriteria.JobDetailsCriteria.JobTitle.IsNotNullOrEmpty())
            result.AppendFormat("{0}: {1}<br/>", jobTitleLabel, SearchCriteria.JobDetailsCriteria.JobTitle);
				}

				if (SearchCriteria.ResumeCriteria.IsNotNull())
				{
					if (SearchCriteria.ResumeCriteria.ResumeFilename.IsNotNullOrEmpty())
						result.AppendFormat("{0}: {1}<br/>", CodeLocalise("SearchCriteriaResumeFilename.Label", "Resume"), SearchCriteria.ResumeCriteria.ResumeFilename);
				}

				if (SearchCriteria.KeywordCriteria.IsNotNull())
				{
					if (SearchCriteria.KeywordCriteria.Keywords.IsNotNullOrEmpty())
						result.AppendFormat("{0}: {1}", CodeLocalise("SearchCriteriaKeywords.Label", "Keywords"), SearchCriteria.KeywordCriteria.Keywords);

					if (SearchCriteria.KeywordCriteria.Context.IsNotNull())
						result.AppendFormat(" {0} {1}", CodeLocalise("Global.In.Text", "in"), CodeLocalise(SearchCriteria.KeywordCriteria.Context));

					if (SearchCriteria.KeywordCriteria.Scope.IsNotNull() && (SearchCriteria.KeywordCriteria.Context == KeywordContexts.JobTitle ||  SearchCriteria.KeywordCriteria.Context == KeywordContexts.EmployerName))
						result.AppendFormat(" {0} {1}", CodeLocalise("Global.Within.Text", "within"), CodeLocalise(SearchCriteria.KeywordCriteria.Scope));

					result.Append("<br/>");
				}

				if (SearchCriteria.AdditionalCriteria.IsNotNull())
				{
					if (SearchCriteria.AdditionalCriteria.LocationCriteria.IsNotNull())
					{
						if (SearchCriteria.AdditionalCriteria.LocationCriteria.Distance.IsNotNull() &&
							SearchCriteria.AdditionalCriteria.LocationCriteria.DistanceUnits.IsNotNull() &&
							SearchCriteria.AdditionalCriteria.LocationCriteria.PostalCode.IsNotNullOrEmpty())
						{
							result.AppendFormat("{0}: {1} {2}<br/>", CodeLocalise("SearchCriteriaLocationDistance.Label", "Search within"),
																	SearchCriteria.AdditionalCriteria.LocationCriteria.Distance,
																	CodeLocalise(SearchCriteria.AdditionalCriteria.LocationCriteria.DistanceUnits));

							result.AppendFormat("{0}: {1}<br/>", CodeLocalise("SearchCriteriaLocationPostcode.Label", "Search zip"), SearchCriteria.AdditionalCriteria.LocationCriteria.PostalCode);
						}
					}
          else
          {
            result.AppendFormat("{0}: {1}<br/>", CodeLocalise("SearchCriteriaLocationDistance.Label", "Search within"), CodeLocalise("SearchCriteriaLocationAny.Label", "Any location"));
          }

				  var educationCriteria = SearchCriteria.AdditionalCriteria.EducationCriteria;
					if (educationCriteria.IsNotNull())
					{
						if (educationCriteria.EducationLevel.IsNotNull() && educationCriteria.EducationLevel > EducationLevels.None)
							result.AppendFormat("{0}: {1}<br/>", CodeLocalise("SearchCriteriaEducationLevels.Label", "Education level(s)"), BuildEducationLevelCriteria(educationCriteria.EducationLevel));

						if (educationCriteria.EducationLevels.IsNotNull() && educationCriteria.EducationLevels.Count > 0)
							result.AppendFormat("{0}: {1}<br/>", CodeLocalise("SearchCriteriaEducationLevels.Label", "Education level(s)"), BuildLocalisedEducationLevelsCriteria(educationCriteria.EducationLevels));

						if (educationCriteria.MonthsUntilExpectedCompletion.IsNotNull() && educationCriteria.MonthsUntilExpectedCompletion > 0)
						{
							result.AppendFormat("{0}: {1} ", CodeLocalise("SearchCriteriaMonthsUntilExpectedCompletion.Label", "Months until completion"), educationCriteria.MonthsUntilExpectedCompletion);

							if (SearchCriteria.JobDetailsCriteria.IsNotNull() && SearchCriteria.JobDetailsCriteria.JobId > 0)
								result.Append(CodeLocalise("SearchCriteriaMonthsUntilExpectedCompletionPostingDate.Label", "from posting date"));
							else
								result.Append(CodeLocalise("SearchCriteriaMonthsUntilExpectedCompletionNoPostingDate.Label", "from today"));

							result.AppendFormat(" ({0})<br/>", educationCriteria.IncludeAlumni ? CodeLocalise("IncludeAlumni.Label", "alumni included") : CodeLocalise("IncludeAlumni.Label", "alumni not included"));
						}

						if(educationCriteria.EnrollmentStatus.HasValue && educationCriteria.EnrollmentStatus != SchoolStatus.NA)
							result.AppendFormat("{0}: {1}<br/>", CodeLocalise("SearchCriteriaEnrollmentStatus.Label", "Enrollment status"), CodeLocalise(educationCriteria.EnrollmentStatus));

            if (educationCriteria.CurrentlyEnrolledUndergraduate)
              result.Append(CodeLocalise("SearchCriteriaCurrentlyEnrolledUndergraduate.Label", "Currently enrolled - undergraduate student"))
                    .Append("<br/>");

            if (educationCriteria.MinimumUndergraduateLevel == SchoolStatus.Sophomore)
              result.Append(CodeLocalise("SearchCriteriaCurrentlyEnrolledGraduate.Label", "Applicant must be at least Sophomore level"))
                    .Append("<br/>");

            if (educationCriteria.ProgramsOfStudy.IsNotNullOrEmpty())
              result.AppendFormat("{0}: {1}<br/>", 
                CodeLocalise("SearchCriteriaProgramOfStudy.Label", "Programs of study"), 
                string.Join(", ", educationCriteria.ProgramsOfStudy.Select(kv => kv.Value)));

            if (educationCriteria.MinimumGPA.IsNotNull())
              result.Append(CodeLocalise("SearchCriteriaMinimumGPA.Label", "Applicant should have at least a {0} GPA", educationCriteria.MinimumGPA))
                    .Append("<br/>");

            if (educationCriteria.AlsoSearchGPANotSpecified)
              result.Append(CodeLocalise("SearchCriteriaAlsoSearchGPANotSpecified.Label", "Also search applicants who did not specify GPA"))
                    .Append("<br/>");

					  if (educationCriteria.InternshipSkills.IsNotNullOrEmpty())
					    result.AppendFormat("{0}: {1}<br/>",
					      CodeLocalise("SearchCriteriaInternshipSkills.Label", "Skills"),
					      BuildInternshipSkillsCriteria(educationCriteria.InternshipSkills));
					}

				  if (SearchCriteria.AdditionalCriteria.LanguagesWithProficiencies.IsNotNull() &&
				      SearchCriteria.AdditionalCriteria.LanguagesWithProficiencies.LanguageProficiencies.Count > 0)
				  {
				    var languageProficiencies = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.LanguageProficiencies).ToDictionary(l => l.Id, l => l.Text);

					  var separator = string.Format(" {0} ",
					                           SearchCriteria.AdditionalCriteria.LanguagesWithProficiencies.LanguageSearchType
						                           ? CodeLocalise("Global.And.Text", "and")
						                           : CodeLocalise("Global.Or.Text", "or"));

						var lanugageArray = SearchCriteria.AdditionalCriteria.LanguagesWithProficiencies.LanguageProficiencies.Select(
						                                 l => l.Language +
						                                      (l.Proficiency != 0
							                                       ? (l.Proficiency != languageProficiencies.Keys.Max()
								                                          ? CodeLocalise("OrAbove.Text", " ({0} or above)", languageProficiencies[l.Proficiency])
								                                          : string.Format(" ({0})", languageProficiencies[l.Proficiency]))
							                                       : (App.Settings.Theme == FocusThemes.Workforce
								                                          ? CodeLocalise("ResumeSearchLanguageProficiencies.TopDefault", " ({0})", "any proficiency")
								                                          : ""))).ToArray();
						
						var languageList = string.Join(separator,lanugageArray);

				    result.AppendFormat("{0}: {1}<br/>", CodeLocalise("SearchCriteriaLanguages.Label", "Languages"), languageList);
				  }

				  if (SearchCriteria.AdditionalCriteria.StatusCriteria.IsNotNull())
					{
						if (SearchCriteria.AdditionalCriteria.StatusCriteria.Veteran.IsNotNull() && SearchCriteria.AdditionalCriteria.StatusCriteria.Veteran)
							result.AppendFormat("{0}<br/>", CodeLocalise("SearchCriteriaVeteranStatusRequired.Label", "Veteran status: Required"));

						//if (SearchCriteria.AdditionalCriteria.StatusCriteria.USCitizen.IsNotNull() && SearchCriteria.AdditionalCriteria.StatusCriteria.USCitizen)
						//  result.AppendFormat("{0}<br/>", CodeLocalise("SearchCriteriaUSCitizenStatusRequired.Label", "US citizen status: Required"));
					}

					if (SearchCriteria.AdditionalCriteria.DrivingLicenceCriteria.IsNotNull())
					{
						if (SearchCriteria.AdditionalCriteria.DrivingLicenceCriteria.DrivingLicenceClassId.HasValue && SearchCriteria.AdditionalCriteria.DrivingLicenceCriteria.DrivingLicenceClassId > 0)
							result.AppendFormat("{0}: {1}<br/>", CodeLocalise("DrivingLicenceClass.Label", "Driving licence class"), ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceClasses).Where(x => x.Id == SearchCriteria.AdditionalCriteria.DrivingLicenceCriteria.DrivingLicenceClassId).Select(x => x.Text).SingleOrDefault());

						if (SearchCriteria.AdditionalCriteria.DrivingLicenceCriteria.DrivingLicenceEndorsementsIds.IsNotNullOrEmpty())
						{
							var endorsementTexts = SearchCriteria.AdditionalCriteria.DrivingLicenceCriteria.DrivingLicenceEndorsementsIds.Select(endorsementId => ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceEndorsements).Where(x => x.Id == endorsementId).Select(x => x.Text).SingleOrDefault()).Where(endorsementText => endorsementText.IsNotNullOrEmpty()).ToList();
							if (endorsementTexts.IsNotNullOrEmpty())
								result.AppendFormat("{0}: {1}<br/>", CodeLocalise("DrivingLicenceEndorsements.Label", "Driving licence endorsements"), string.Join(", ", endorsementTexts.ToArray()));
						}
					}

					if (SearchCriteria.AdditionalCriteria.Licences.IsNotNullOrEmpty())
						result.AppendFormat("{0}: {1}<br/>", CodeLocalise("SearchCriteriaLicences.Label", "Other Licences"), string.Join(", ", SearchCriteria.AdditionalCriteria.Licences.ToArray()));

					if (SearchCriteria.AdditionalCriteria.Certifications.IsNotNullOrEmpty())
						result.AppendFormat("{0}: {1}<br/>", CodeLocalise("SearchCertifications.Label", "Certifications"), string.Join(", ", SearchCriteria.AdditionalCriteria.Certifications.ToArray()));

          if (SearchCriteria.AdditionalCriteria.NCRCLevel.IsNotNullOrZero())
          {
            var careerLevelText = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.NCRCLevel).Where(x => x.Id == SearchCriteria.AdditionalCriteria.NCRCLevel).Select(x => x.Text).SingleOrDefault();
            result.AppendFormat("{0}: {1}<br/>", CodeLocalise("NCRCLevel.Label", " National Career Readiness Certificate&trade;"), careerLevelText);
          }
            
					if (SearchCriteria.AdditionalCriteria.AvailabilityCriteria.IsNotNull())
					{
						if (SearchCriteria.AdditionalCriteria.AvailabilityCriteria.WorkOvertime.HasValue)
							result.AppendFormat("{0}: {1}<br/>", CodeLocalise("SearchCriteriWorkOvertime.Label", "Willing to work overtime"), SearchCriteria.AdditionalCriteria.AvailabilityCriteria.WorkOvertime.Value);

            if (SearchCriteria.AdditionalCriteria.AvailabilityCriteria.WillingToRelocate.HasValue)
              result.AppendFormat("{0}: {1}<br/>", CodeLocalise("SearchCriteriRelocate.Label", "Willing to relocate"), SearchCriteria.AdditionalCriteria.AvailabilityCriteria.WillingToRelocate.Value);

						if (SearchCriteria.AdditionalCriteria.AvailabilityCriteria.NormalWorkShiftId.IsNotNull() && SearchCriteria.AdditionalCriteria.AvailabilityCriteria.NormalWorkShiftId > 0)
							result.AppendFormat("{0}: {1}<br/>", CodeLocalise("SearchCriteriaNormalWorkShift.Label", "Normal work shift"), ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.WorkShifts).Where(x => x.Id == SearchCriteria.AdditionalCriteria.AvailabilityCriteria.NormalWorkShiftId).Select(x => x.Text).SingleOrDefault());

						if (SearchCriteria.AdditionalCriteria.AvailabilityCriteria.WorkTypeId.HasValue && SearchCriteria.AdditionalCriteria.AvailabilityCriteria.WorkTypeId.Value > 0)
							result.AppendFormat("{0}: {1}<br/>", CodeLocalise("SearchCriteriaWorkType.Label", "Duration"), ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Durations).Where(x => x.Id == SearchCriteria.AdditionalCriteria.AvailabilityCriteria.WorkTypeId).Select(x => x.Text).SingleOrDefault());

						if (SearchCriteria.AdditionalCriteria.AvailabilityCriteria.WorkWeekId.HasValue && SearchCriteria.AdditionalCriteria.AvailabilityCriteria.WorkWeekId.Value > 0)
							result.AppendFormat("{0}: {1}<br/>", CodeLocalise("SearchCriteriaWorkWeek.Label", "Work week"), ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.WorkWeeks).Where(x => x.Id == SearchCriteria.AdditionalCriteria.AvailabilityCriteria.WorkWeekId).Select(x => x.Text).SingleOrDefault());
					}
				}

				if (SearchCriteria.MinimumScore.IsNotNull() && SearchCriteria.MinimumScore > 0)
							result.AppendFormat("{0}: {1}<br/>", CodeLocalise("SearchMinimumStarRating.Label", "Minimum star rating"), SearchCriteria.MinimumScore.ToStarRating(App.Settings.StarRatingMinimumScores));
			}

			var resultString = result.ToString();
			if (resultString.Length > 5) resultString = resultString.Substring(0, resultString.Length - 5);

			return resultString;
		}

		/// <summary>
		/// Builds the education level criteria.
		/// </summary>
		/// <param name="educationLevels">The education levels.</param>
		/// <returns></returns>
		private string BuildEducationLevelCriteria(EducationLevels educationLevels)
		{
			var result = new StringBuilder("");

            if ((educationLevels & EducationLevels.NoDiploma) == EducationLevels.NoDiploma) result.AppendFormat("{0}, ", CodeLocalise(EducationLevels.NoDiploma, true));
            if ((educationLevels & EducationLevels.HighSchoolDiplomaOrEquivalent) == EducationLevels.HighSchoolDiplomaOrEquivalent) result.AppendFormat("{0}, ", CodeLocalise(EducationLevels.HighSchoolDiplomaOrEquivalent, true));
            if ((educationLevels & EducationLevels.SomeCollegeNoDegree) == EducationLevels.SomeCollegeNoDegree) result.AppendFormat("{0}, ", CodeLocalise(EducationLevels.SomeCollegeNoDegree, true));
            if ((educationLevels & EducationLevels.AssociatesDegree) == EducationLevels.AssociatesDegree) result.AppendFormat("{0}, ", CodeLocalise(EducationLevels.AssociatesDegree, true));
            if ((educationLevels & EducationLevels.BachelorsDegree) == EducationLevels.BachelorsDegree) result.AppendFormat("{0}, ", CodeLocalise(EducationLevels.BachelorsDegree, true));
            if ((educationLevels & EducationLevels.MastersDegree) == EducationLevels.MastersDegree) result.AppendFormat("{0}, ", CodeLocalise(EducationLevels.MastersDegree, true));
            if ((educationLevels & EducationLevels.DoctorateDegree) == EducationLevels.DoctorateDegree) result.AppendFormat("{0}, ", CodeLocalise(EducationLevels.DoctorateDegree, true));

            if ((educationLevels & EducationLevels.HighSchoolDiploma) == EducationLevels.HighSchoolDiploma)
            {
                result.AppendFormat("{0}, ", CodeLocalise(EducationLevels.NoDiploma, true));
                result.AppendFormat("{0}, ", CodeLocalise(EducationLevels.HighSchoolDiplomaOrEquivalent, true));
                result.AppendFormat("{0}, ", CodeLocalise(EducationLevels.SomeCollegeNoDegree, true));
            }

            if ((educationLevels & EducationLevels.GraduateDegree) == EducationLevels.GraduateDegree)
            {
                result.AppendFormat("{0}, ", CodeLocalise(EducationLevels.MastersDegree, true));
                result.AppendFormat("{0}, ", CodeLocalise(EducationLevels.DoctorateDegree, true));
            }

            var resultString = result.ToString();
			if (resultString.Length > 2) resultString = resultString.Substring(0, resultString.Length - 2);

			return resultString;
		}

		/// <summary>
		/// Builds the localised education levels criteria.
		/// </summary>
		/// <param name="educationLevels">The education levels.</param>
		/// <returns></returns>
		private string BuildLocalisedEducationLevelsCriteria(List<EducationLevel> educationLevels)
		{
			var result = new StringBuilder("");

			foreach (var educationLevel in educationLevels)
			{
				result.AppendFormat("{0}, ", CodeLocalise(educationLevel, true));
			}

			var resultString = result.ToString();
			if (resultString.Length > 2) resultString = resultString.Substring(0, resultString.Length - 2);

			return resultString;
		}

    /// <summary>
    /// Converts a list of internship skill Ids into a string list
    /// </summary>
    /// <param name="skills">A list of skill ids</param>
    /// <returns>A string containing the concatenated string names</returns>
    private string BuildInternshipSkillsCriteria(IEnumerable<long> skills)
    {
      var internshipSkills = ServiceClientLocator.CoreClient(App).GetEducationInternshipCategories(true);
      var skillLookup = new Dictionary<long, string>();
      internshipSkills.ForEach(s => s.CategorySkills.ForEach(c => skillLookup.Add(c.Id.GetValueOrDefault(0), c.Name)));

      return string.Join(", ", skills.Select(s => skillLookup[s]));
    }
	}
}