﻿
namespace Focus.Web.Code.Controls.User
{
	public partial class PageModal : UserControlBase
	{
		/// <summary>
		/// Shows the specified URL.
		/// </summary>
		/// <param name="url">The URL.</param>
		public void Show(string url)
		{
			var script = string.Format("$(function(){{ PageModalShow('{0}'); }});", url);
			Page.ClientScript.RegisterStartupScript(GetType(), "PageModalShow", script, true);

			PageModalPopup.Show();
		}
	}
}