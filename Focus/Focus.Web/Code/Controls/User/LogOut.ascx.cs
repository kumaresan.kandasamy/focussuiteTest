﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.Security;
using Focus.Common;
using Focus.Core;
using Framework.Core;
using Framework.Logging;

#endregion

namespace Focus.Web.Code.Controls.User
{
	public partial class LogOut : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				LocaliseUI();
		}

		public void Show()
		{
			LogOutModal.Show();
		}

		/// <summary>
		/// Handles the Click event of the CompleteLogOutButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CompleteLogOutButton_Click(object sender, EventArgs e)
		{
            bool redirectSecondarySSO = false;

            if (App.Settings.SSOEnabled || (App.Settings.SamlEnabledForTalent && App.Settings.Module == FocusModules.Talent) || (App.Settings.Module == FocusModules.Assist && App.Settings.SamlEnabledForAssist))
            {
                var user = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId);
                if (!App.Settings.SamlSecExternalClientTag.Equals(string.Empty) && user.IsNotNull())
                    redirectSecondarySSO = App.Settings.SamlSecExternalClientTag.Split(',').ToList<string>().Any(t => user.UserDetails.ExternalId.StartsWith(t));
            }

			try
			{
				ServiceClientLocator.AuthenticationClient(App).LogOut();
			}
			catch (Exception ex)
			{
				Logger.Error(ex.Message, ex);
			}
			finally
			{
				FormsAuthentication.SignOut();
				App.ClearSession();
			}

            if (App.Settings.SSOEnabled || (App.Settings.SamlEnabledForTalent && App.Settings.Module == FocusModules.Talent) || (App.Settings.Module == FocusModules.Assist && App.Settings.SamlEnabledForAssist))
			{
                if (redirectSecondarySSO)
                    Response.Redirect(App.Settings.SamlLogoutRedirect, true);
                else
                    Response.Redirect(App.Settings.SSOReturnUrl, true);
				return;
			}

			Response.Redirect(UrlBuilder.LogIn(), true);
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
		  if (App.User.IsInRole(Constants.RoleKeys.SystemAdmin))
		    LocaliseClose.Text = EditLocalisationLink("Global.Close.Text.NoEdit");
			CompleteLogOutButton.Text = CodeLocalise("Global.LogOut.Text.NoEdit", "Sign out");
			ReturnHomeButton.Text = CodeLocalise("Global.Close.Text.NoEdit", "Close");
		}
	}
}