﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobPostingSearchResults.ascx.cs" Inherits="Focus.Web.Code.Controls.User.JobPostingSearchResults" %>
<%@ Import Namespace="Focus.Core.Models.Career" %>
<%@ Register TagPrefix="ajaxtoolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.50731.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="uc" TagName="Pager" Src="~/Code/Controls/User/Pager.ascx" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagprefix="uc" tagname="ConfirmationModal"  %>

<table style="width:100%;" class="FocusCareer">
	<tr>
		<td>
			
			<h1><%= HtmlLocalise("Heading1.Label", "Job search results")%></h1>
		</td>
	</tr>
	<tr>
		<td>
			<table style="vertical-align: top;">
				<tr>
					<td colspan="3">
						<%= HtmlLocalise("Results.Label", "These results are based on your search criteria.")%>
						<asp:LinkButton ID="lnkReviewSearchCriteria" runat="server" Text="Review & change criteria." OnClick="lnkReviewSearchCriteria_Click"></asp:LinkButton>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div style="height: 10px;">
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<%= HtmlInFieldLabel("SearchTermTextBox", "AddWordsTextBox.Label", "add words to search for", 200)%>
						<asp:TextBox ID="SearchTermTextBox" runat="server" ClientIDMode="Static" Width="200px" />
						<asp:Label ID="lblPostingOptions" runat="server">in</asp:Label>
						<asp:DropDownList ID="ddlSearchLocation" Width="200px" runat="server">
							<asp:ListItem Text="complete job posting" />
						</asp:DropDownList>
					</td>
					<td>
						<asp:Button ID="btnGO" Text="Go &#187;" runat="server" ValidationGroup="Location"
							class="button1" OnClick="btnGO_Click" />
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div style="height: 10px;">
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<%= HtmlLocalise("LocationWithin.Label", "LOCATION: within ")%>
						<asp:DropDownList ID="ddlRadius" runat="server" Width="150px" ClientIDMode="Static">
							<asp:ListItem>5 miles</asp:ListItem>
						</asp:DropDownList>
						<%= HtmlLocalise("OfZip.Label", "of ZIP code:")%>
						<asp:TextBox ID="txtZipCode" runat="server" ClientIDMode="Static" Width="80px" MaxLength="9" />
						<ajaxtoolkit:FilteredTextBoxExtender ID="ftbeZip" runat="server" TargetControlID="txtZipCode"
							FilterType="Numbers" />
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<asp:CustomValidator ID="RadiusValidator" runat="server" SetFocusOnError="true" Display="Dynamic"
							CssClass="error" ValidationGroup="Location" ClientValidationFunction="ValidateRadius"
							ValidateEmptyText="true" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<br />
		</td>
	</tr>
	<tr>
		<td>
			<div class="horizontalRule">
			</div>
		</td>
	</tr>
	<tr>
		<td>
			<asp:Label ID="searchReport" runat="server"></asp:Label>
		</td>
	</tr>
          <tr>
              <td>
                <asp:Label runat="server" ID="JobSeekerAlreadyReferredLabel" class="error" Visible="False"></asp:Label>
              </td>
          </tr>
	<tr>
		<td>
			<asp:UpdatePanel ID="SearchResultUpdatePanel" runat="server" UpdateMode="Conditional">
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="btnGO" EventName="Click" />
				</Triggers>
				<ContentTemplate>
					<table style="width:100%;" class="Pager">
						<tr>
							<td>
								<div style="height: 35px;">
									<uc:Pager ID="TopPager" runat="server" PagedControlID="SearchResultListView"
										OnPagesizeChange="TopPager_PageSizeChange" DisplayRecordCount="True" />
								</div>
							</td>
						</tr>
					</table>
					<div style="height: 10px;">
					</div>
					<asp:ListView ID="SearchResultListView" runat="server" ItemPlaceholderID="SearchResultPlaceHolder"
						DataSourceID="SearchResultDataSource" OnSorting="SearchResultListView_Sorting"
						OnItemDataBound="SearchResultListView_ItemDataBound" OnPagePropertiesChanging="SearchResultListView_PagePropertiesChanging" OnLayoutCreated="SearchResultListView_OnLayoutCreated" DataKeyNames="Id, JobTitle, Rank, OriginId">
						<LayoutTemplate>
							<table style="width:100%;" class="table">
								<tr>
									<th style="width:120px;">
										<table>
											<tr>
												<td>
													<asp:LinkButton ID="RatingSortButton" runat="server" CommandName="Sort" CommandArgument="Rating"><%= HtmlLocalise("Rating.ColumnTitle", "RATING")%></asp:LinkButton>
													<asp:Image ID="RatingSortImage" runat="server" ImageUrl="<%# UrlBuilder.TriangleDownIcon() %>"
														Visible="False" />
												</td>
												<td style="width:10px;"></td>
												<td style="vertical-align: top;">
													<div id="Div1" runat="server" class="tooltip">
								                              <%= HtmlTooltipster("tooltipWithArrow", "ExcludeJobstooltip.ToolTip","Match ratings are based on your skills, qualifications, and work experience, not just job titles and keywords. As such, ratings are not available when you are searching without matching to your resume.")%>
													</div>
												</td>
											</tr>
										</table>
									</th>
									<th style="width:80px;">
										<asp:LinkButton ID="DateSortButton" runat="server" CommandName="Sort" CommandArgument="Date"><%= HtmlLocalise("Date.ColumnTitle", "DATE")%></asp:LinkButton>
										<asp:Image ID="DateSortImage" runat="server" ImageUrl="<%# UrlBuilder.TriangleDownIcon() %>"
											Visible="False" />
									</th>
									<th style="width:370px;">
										<asp:LinkButton ID="JobTitleSortButton" runat="server" CommandName="Sort" CommandArgument="JobTitle"><%= HtmlLocalise("JobTitle.ColumnTitle", "JOB TITLE")%></asp:LinkButton>
										<asp:Image ID="JobTitleSortImage" runat="server" ImageUrl="<%# UrlBuilder.TriangleDownIcon() %>"
											Visible="False" />
									</th>
									<th style="width:200px;">
										<asp:LinkButton ID="EmployerSortButton" runat="server" CommandName="Sort" CommandArgument="Employer"><%= HtmlLocalise("Employer.ColumnTitle", "#BUSINESS#:UPPER")%></asp:LinkButton>
										<asp:Image ID="EmployerSortImage" runat="server" ImageUrl="<%# UrlBuilder.TriangleDownIcon() %>"
											Visible="False" />
									</th>
									<th style="width:150px;">
										<asp:LinkButton ID="JobLocationSortButton" runat="server" CommandName="Sort" CommandArgument="JobLocation"><%= HtmlLocalise("JobLocation.ColumnTitle", "JOB LOCATION")%></asp:LinkButton>
										<asp:Image ID="JobLocationSortImage" runat="server" ImageUrl="<%# UrlBuilder.TriangleDownIcon() %>"
											Visible="False" />
									</th>
									<th style="width:225px;" id="ActionHeader" runat="server">
										ACTIONS
									</th>
								</tr>
								<asp:PlaceHolder ID="SearchResultPlaceHolder" runat="server" />
							</table>
						</LayoutTemplate>
						<ItemTemplate>
							<tr  id="JobSearchResultRow" runat="server">
								<td>
									<%# GetStars(((PostingSearchResultView)Container.DataItem).Rank)%>
								</td>
								<td>
									<%#((PostingSearchResultView)Container.DataItem).JobDate.ToString("MMM dd, yyyy")%>
								</td>
								<td>
									<asp:HyperLink runat="server" ID="JobTitleHyperLink"><span><%# ((PostingSearchResultView)Container.DataItem).JobTitle%></span></asp:HyperLink>
								</td>
								<td>
									<%# ((PostingSearchResultView)Container.DataItem).Employer%>
								</td>
								<td>
									<%#((PostingSearchResultView)Container.DataItem).Location%>
								</td>
								<td id="ActionCell" runat="server">
									<asp:LinkButton ID="AmIGoodMatchHyperLink" runat="server" OnClick="AmIGoodMatchHyperLink_Click">
											<%= HtmlLocalise("HowStrongIsThisMatch.Text", "How strong is this match?") %></asp:LinkButton><br />
									<asp:LinkButton ID="FindMoreJobsHyperLink" runat="server" OnClick="FindMoreJobsHyperLink_Click">
											<%= HtmlLocalise("FindMoreJobs.Text", "Find more jobs like this") %></asp:LinkButton><br />
									<asp:LinkButton ID="DoNotDisplayHyperLink" runat="server" OnClick="DoNotDisplayHyperLink_Click">
											<%= HtmlLocalise("DoNotDisplay.Text", "Do not display this job again") %></asp:LinkButton>
								</td>
							</tr>
						</ItemTemplate>
						<EmptyDataTemplate>
							<table>
								<tr>
									<td colspan="7">
										<b>
											<%= HtmlLocalise("NoResults.Text", "No matching jobs found; please refine your search")%></b>
									</td>
								</tr>
							</table>
						</EmptyDataTemplate>
					</asp:ListView>
					<table style="width:100%;" class="Pager">
						<tr>
							<td colspan="2">
								<div style="height: 10px;">
								</div>
							</td>
						</tr>
						<tr>
							<td style="text-align:right;">
								<uc:Pager ID="BottomPager" runat="server" PagedControlID="SearchResultListView" HidePageSizer="true"/>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div style="height: 15px;">
								</div>
							</td>
						</tr>
					</table>
				</ContentTemplate>
			</asp:UpdatePanel>
		</td>
	</tr>
</table>
<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />
<asp:HiddenField ID="ReviewSearchCriteriaDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ReviewSearchCriteriaModalPopup" runat="server" TargetControlID="ReviewSearchCriteriaDummyTarget"
	PopupControlID="ReviewSearchCriteriaModalPanel" 
	CancelControlID="CriteriaCloseButton" RepositionMode="RepositionOnWindowResizeAndScroll">
	<Animations>
		<OnShown>
          <ScriptAction Script="SetFocusToCriteriaCloseButton();" />  
    </OnShown>
	</Animations>
</act:ModalPopupExtender>
<asp:Panel ID="ReviewSearchCriteriaModalPanel" runat="server" CssClass="modal" Style="display: none;">
	<table style="width:450px;">
		<tr>
			<th style="vertical-align:top;">
				Applied search criteria
			</th>
		</tr>
		<tr>
			<td style="vertical-align:top; text-align:left;height:50px;">
				<asp:Label ID="lblReviewSearchCriteriaInfo" runat="server"></asp:Label>
			</td>
		</tr>
		<tr>
			<td style="height:10px;"></td>
		</tr>
		<tr>
			<td style="text-align:right;">
				<asp:Button ID="CriteriaCloseButton" runat="server" class="button1" ClientIDMode="Static"
					TabIndex="0" />
				<asp:Literal ID="OkButtonSpacer" runat="server" EnableViewState="false" Text="  " />
				<asp:Button ID="ChangeCriteriaButton" runat="server" class="button1" OnClick="ChangeCriteriaButton_Click" />
			</td>
		</tr>
	</table>
</asp:Panel>
<asp:ObjectDataSource ID="SearchResultDataSource" runat="server" TypeName="Focus.Web.Code.Controls.User.JobPostingSearchResults"
	EnablePaging="True" SelectMethod="GetSearchResults" SelectCountMethod="GetSearchResultCount" SortParameterName="orderBy"/>

<script type="text/javascript">
	function SetFocusToCriteriaCloseButton() {
	  setButtonFocus('#CriteriaCloseButton').focus();
	}

	function ValidateRadius(sender, args) {
	  var ddlRadius = document.getElementById("ddlRadius");
	  var Radius = ddlRadius.options[ddlRadius.selectedIndex].value;
	  var txtZipCode = document.getElementById("txtZipCode");
	  if (!((Radius == "" && txtZipCode.value == "") || (Radius != "" && txtZipCode.value != ""))) {
	    args.IsValid = false;
	  }
	}
</script>
