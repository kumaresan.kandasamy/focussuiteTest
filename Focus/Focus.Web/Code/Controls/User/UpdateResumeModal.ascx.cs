﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common.Code;
using Focus.Common.Extensions;
using Focus.Core.Models.Career;
using Focus.Core;
using Focus.Common;
using Focus.Core.Views;
using Framework.Core;
using Framework.Logging;

#endregion

namespace Focus.Web.Code.Controls.User
{
  public partial class UpdateResumeModal : UserControlBase
  {
    protected string EnrollmentStatusErrorMessage = "";
    protected string EducationLevelErrorMessage = "";

    internal long DefaultResumeId
    {
      get { return GetViewStateValue<long>("JobSeekerProfileView:Id"); }
      set { SetViewStateValue("JobSeekerProfileView:Id", value); }
    }

    internal long JobSeekerId
    {
      get { return GetViewStateValue<long>("JobSeekerProfile:JobSeekerId"); }
      set { SetViewStateValue("JobSeekerProfile:JobSeekerId", value); }
    }

    public delegate void UpdatedHandler(object sender, UpdateResumeEventArgs eventArgs);
    public event UpdatedHandler Updated;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        if (Page.RouteData.Values.ContainsKey("jobseekerid"))
        {
          long id;
          Int64.TryParse(Page.RouteData.Values["jobseekerid"].ToString(), out id);
          JobSeekerId = id;
        }
      }
      LocaliseUI();
    }

    public void Show(EmploymentStatus? employmentStatus, EducationLevel? educationlevel,SchoolStatus? enrollmentstatus)
    {
        #region Enable/Disable Controls
        EnrollmentStatusDDL.Visible = !App.Settings.HideEnrollmentStatus;
        #endregion

      BindEnrollmentStatusDropDown(EnrollmentStatusDDL);

      var enrollmentStatusText = enrollmentstatus.ToString();

      if (enrollmentstatus != null && EnrollmentStatusDDL.Items.FindByValue(enrollmentStatusText) != null)
        EnrollmentStatusDDL.SelectedValue = enrollmentStatusText;
      else
        EnrollmentStatusDDL.SelectedIndex = 0;
      
      CommonUtilities.GetEmploymentStatusDropDownList(EmploymentStatusDDL);

      if (employmentStatus.IsNotNull() && EmploymentStatusDDL.Items.FindByValue(employmentStatus.ToString()).IsNotNull())
        EmploymentStatusDDL.SelectedValue = employmentStatus.ToString();
      else
        EmploymentStatusDDL.SelectedIndex = 0;

      CommonUtilities.GetEducationLevelDropDownList(EducationStatusDDL);

      if (educationlevel.IsNotNull() && EducationStatusDDL.Items.FindByValue(educationlevel.ToString()).IsNotNull())
        EducationStatusDDL.SelectedValue = educationlevel.ToString();
      else
        EducationStatusDDL.SelectedIndex = 0;

      UpdateResumeModalPopup.Show();
    }

    protected void UpdateResumeAction_Click(object sender, EventArgs e)
    {
      // May need to add new service method that can update resumes by id and overload by resume model
      // ServiceClientLocator.ResumeClient(App).UpdateResumes(List<long> Ids)
      // ServiceClientLocator.ResumeClient(App).UpdateResumes(List<ResumeModels> resumeModels)

      var candidateResumeIds = ServiceClientLocator.CandidateClient(App).GetResumes(JobSeekerId).Select(x => x.ResumeID).ToList();
      var resumemodels = candidateResumeIds.Select(id => ServiceClientLocator.ResumeClient(App).GetResume(id)).ToList();
      var completedResumes =  resumemodels.Where(x => x.ResumeMetaInfo.CompletionStatus == ResumeCompletionStatuses.Completed).ToList();

      var confirmText = "The job seeker's resume(s) have been successfully updated";

      if (!completedResumes.Any())
      {
          confirmText = "No job seeker's resume(s) have been updated";
      }
      else
      {
            var schoolStatusText = EnrollmentStatusDDL.SelectedItem.Value;
            var schoolStatus = schoolStatusText.Length > 0
              ? (SchoolStatus) Enum.Parse(typeof(SchoolStatus), EnrollmentStatusDDL.SelectedItem.Value, true)
              : SchoolStatus.NA;

            var educationLevel = EducationStatusDDL.SelectedValueToEnum<EducationLevel>();
            var employmentStatus = EmploymentStatusDDL.SelectedValueToEnum<EmploymentStatus>();

            foreach (var resumeModel in completedResumes)
            {            
                if (resumeModel.ResumeContent.Profile.Veteran.History[0].VeteranEra.IsNotNull() && resumeModel.ResumeContent.Profile.Veteran.History[0].VeteranEra.Equals(VeteranEra.TransitioningServiceMember))
                {
                    confirmText = "Transitioning Service Member must be 'Employed with Notice of Termination'";
                }
                else
                {
                    resumeModel.ResumeContent.EducationInfo.SchoolStatus = schoolStatus;
                    resumeModel.ResumeContent.EducationInfo.EducationLevel = educationLevel;
                    resumeModel.ResumeContent.ExperienceInfo.EmploymentStatus = employmentStatus;

                    ServiceClientLocator.ResumeClient(App).SaveResume(resumeModel, false, JobSeekerId);

                    OnUpdated(new UpdateResumeEventArgs
                    {
                        SchoolStatus = schoolStatus,
                        EducationLevel = educationLevel,
                        EmploymentStatus = employmentStatus
                    }); 
                }
            }      
       }
       Confirmation.Show(CodeLocalise("UpdateResumeConfirmation.Title", "Education and Profile information updated"),
       CodeLocalise("UpdateResumeConfirmation.Body", confirmText),
       CodeLocalise("Global.OK.Text", "OK"));
    }

    private void LocaliseUI()
    {
      EnrollmentStatusRequiredValidator.ErrorMessage = CodeLocalise("EnrollmentStatus.Required", "Please select the enrollment status");
      EducationStatusRequiredValidator.ErrorMessage = CodeLocalise("EducationStatus.Required", "Please select the education level");
      EmploymentStatusRequireddValidator.ErrorMessage = CodeLocalise("EmploymentStatus.Required", "Please select the employment status");

      EnrollmentStatusErrorMessage = CodeLocalise("EnrollmentStatus.ErrorMessage", "Enrollment status cannot be greater than education level");
      EducationLevelErrorMessage = CodeLocalise("EducationLevel.ErrorMessage", "Enrollment status cannot be less than education level");
    }

    protected virtual void OnUpdated(UpdateResumeEventArgs eventArgs)
    {
      if (Updated != null)
        Updated(this, eventArgs);
    }
  }

  public class UpdateResumeEventArgs : EventArgs
  {
    public SchoolStatus SchoolStatus;
    public EducationLevel EducationLevel;
    public EmploymentStatus EmploymentStatus;
  }
}