﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using Framework.Core;

using Newtonsoft.Json;

#endregion


namespace Focus.Web.Code.Controls.User
{
	public partial class UpdateableLanguagesList : UserControlBase
	{
		public string Width { get; set; }

		public string ItemListClientId
		{
			get { return ItemList.ClientID; }
		}

		/// <summary>
		/// Gets or sets the items.
		/// </summary>
		/// <value>The items.</value>
		public List<KeyValuePair<string, string[]>> Items
		{
			get
			{
				return ItemList.Value.IsNullOrEmpty() ? null : ItemList.Value.DeserializeJson<List<KeyValuePair<string, string[]>>>();
				//return GetViewStateValue<List<KeyValuePair<string, string[]>>>("UpdateablePair:" + ID + ":Items");
			}
			set
			{
				ItemList.Value = value.IsNull() ? "" : value.SerializeJson();
				//SetViewStateValue("UpdateablePair:" + ID + ":Items", value);
				Bind();
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether to restrict duplicates in the collection.
		/// </summary>
		/// <value><c>true</c> if [restrict duplicates]; otherwise, <c>false</c>.</value>
		public bool RestrictDuplicates { get; set; }

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				ItemsListPanel.Width = Width == null ? 354 : Unit.Parse(Width);

				Bind();
			}
		}

		/// <summary>
		/// Adds the item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns>Duplicate validation check</returns>
		public bool AddItem(KeyValuePair<string, string[]> item)
		{
			var items = Items ?? new List<KeyValuePair<string, string[]>>();

			// Check for Duplicates 
			if (RestrictDuplicates && items.Any(pair => (string.Equals(pair.Value[0], item.Value[0], StringComparison.InvariantCultureIgnoreCase))))
			{
				return false;
			}

			items.Add(item);

			// This is to ensure the hidden field gets updated with the serialised list
			Items = items;

			Bind();

			return true;
		}

		/// <summary>
		/// Handles the Command event of the ItemsListRemoveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ItemsListRemoveButton_Command(object sender, CommandEventArgs e)
		{
			var items = Items;
			var rowNo = Convert.ToInt32(e.CommandArgument);
			items.RemoveAt(rowNo);

			// This is to ensure the hidden field gets updated with the serialised list
			Items = items;

			Bind();
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			ItemsList.DataSource = Items;
			ItemsList.DataBind();
		}
	}
}