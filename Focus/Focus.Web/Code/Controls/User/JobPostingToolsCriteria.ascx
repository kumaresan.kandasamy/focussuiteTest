﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobPostingToolsCriteria.ascx.cs" Inherits="Focus.Web.Code.Controls.User.JobPostingToolsCriteria" %>
<%@ Register Src="~/Code/Controls/User/EmailJobSeekerModal.ascx" TagName="EmailCandidateModal" TagPrefix="uc" %>
<%@ Register src="~/Code/Controls/User/ReferralRequestSentModal.ascx" tagName="ReferralRequestSentModal" tagPrefix="uc" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagprefix="uc" tagname="ConfirmationModal"  %>
<%@ Register src="~/Code/Controls/User/JobApplicationRequirementsModal.ascx" tagPrefix="uc" tagName="JobApplicationRequirementsModal" %>

	
	
	<div class="jobPostingToolsCriteria">
		<asp:PlaceHolder runat="server" ID="ToolLinksPlaceHolder">
			<ul>
				<asp:PlaceHolder runat="server" ID="AmIGoodMatchPlaceHolder">
				<li>
					
					<asp:LinkButton ID="AmIGoodMatchHyperLink" runat="server" OnClick="AmIGoodMatchHyperLink_Click">
						<focus:LocalisedLabel runat="server" ID="AmIGoodMatchText" RenderOuterSpan="False" LocalisationKey="AmIGoodMatch.Text" DefaultText="How strong is this match?"/>
					</asp:LinkButton>
				</li>
				</asp:PlaceHolder>
				<li>
					<asp:LinkButton ID="FindMoreJobsHyperLink" runat="server" OnClick="FindMoreJobsHyperLink_Click">
						<focus:LocalisedLabel runat="server" ID="FindMoreJobsText" RenderOuterSpan="False" LocalisationKey="FindMoreJobs.Text" DefaultText="Find more jobs like this"/>
					</asp:LinkButton>
				</li>
				<li>
					<asp:LinkButton ID="DoNotDisplayHyperLink" runat="server" OnClick="DoNotDisplayHyperLink_Click">
						<focus:LocalisedLabel runat="server" ID="DoNotDisplayText" RenderOuterSpan="False" LocalisationKey="DoNotDisplay.Text" DefaultText="Do not display this job again"/>
					</asp:LinkButton>
				</li>
			</ul>
		</asp:PlaceHolder>
		<asp:Label ID="ViewCountLabel" runat="server" CssClass="instructionalText"></asp:Label>
	</div>
	<div style="margin: 10px 0;">
		<asp:PlaceHolder runat="server" ID="EmailPlaceHolder">
			<div class="tooltip left Email">
				<asp:LinkButton ID="EmailJobButton" runat="server" OnClick="EmailJob_Click" aria-label="Email">
					<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("Email.ToolTip", "Email")%>"></div>
				</asp:LinkButton>
			</div>
		</asp:PlaceHolder>
		<div class="tooltip left Print">
			<a href="#" onclick="return PrintPosting('POSTING');" aria-label="Print"><div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("Print.ToolTip", "Print")%>"></div></a>
		</div>
		<asp:PlaceHolder runat="server" ID="ReferPlaceHolder">
				<div>
						<asp:Button runat="server" ID="ReferJobButton" CssClass="button3" OnClick="ReferJobButton_Link"  ValidationGroup="" />
				</div>
		</asp:PlaceHolder>
		<asp:PlaceHolder runat="server" ID="ViewReferralPlaceHolder" Visible="False">
			<div>
				<asp:Button runat="server" ID="ViewReferralButton" CssClass="button3" OnClick="ViewReferralButton_OnClick"  ValidationGroup="" />
			</div>
		</asp:PlaceHolder>
		<div>
        <asp:Label runat="server" ID="JobSeekerAlreadyReferredLabel" class="error" Visible="False"></asp:Label>
        <uc:ConfirmationModal ID="ConfirmationModal" runat="server" />
    </div>
		<uc:EmailCandidateModal ID="EmailCandidate" runat="server" OnCompleted="EmailCandidateModal_Completed" />		
		<uc:ReferralRequestSentModal ID="AutoApprovalModal" runat="server" />
	</div>
	
	<asp:UpdatePanel runat="server" ID="ConfirmationUpdatePanel" UpdateMode="Conditional">
		<ContentTemplate>
				<uc:ConfirmationModal ID="ReferralConfirmationModal" runat="server" Width="300px" Height="200px" />
		</ContentTemplate>  
	</asp:UpdatePanel>
			
	<uc:ReferralRequestSentModal ID="ReferralRequestSentModal1" runat="server" />
  <uc:EmailCandidateModal ID="EmailCandidateModal1" runat="server" OnCompleted="EmailCandidateModal_Completed" />		
	<uc:JobApplicationRequirementsModal ID="JobApplicationRequirementsModal" runat="server"  OnReferred="JobApplicationRequirementsModal_Referred" />