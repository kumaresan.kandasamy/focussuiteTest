﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;
using Focus.Common.Code;
using Focus.Common.Extensions;
using Focus.Common.Localisation;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.EmailTemplate;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Focus.Services.Core;
using DistanceUnits = Focus.Core.Models.Career.DistanceUnits;

using Framework.Core;

#endregion

namespace Focus.Web.Code
{
  public class Utilities
  {
		/// <summary>
		/// The App
		/// </summary>
	  private static IApp _app;

		private static IApp App
		{
			get { return _app ?? (_app = new HttpApp()); }
		}

		public Utilities(IApp app = null)
		{
			_app = app ?? new HttpApp();
		}

    /// <summary>
    /// Saves the criteria against resume.
    /// </summary>
    /// <param name="model">The model.</param>
    /// <param name="matchingType">Type of the matching.</param>
    public static void SaveCriteriaAgainstResume(ResumeModel model, MatchingType matchingType)
    {
      var criteria = new SearchCriteria();
      var stateId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault();

      #region Job Matching Criteria

      criteria.RequiredResultCriteria = new RequiredResultCriteria
      {
        DocumentsToSearch = DocumentType.Posting,
        MinimumStarMatch = (matchingType == MatchingType.ProgramOfStudy) ? StarMatching.None : (App.Settings.Theme == FocusThemes.Education ? StarMatching.None : StarMatching.ThreeStar),
        MaximumDocumentCount = App.Settings.MaximumNoDocumentToReturnInSearch
      };

      #endregion

      #region Posting Age Criteria

      criteria.PostingAgeCriteria = new PostingAgeCriteria { PostingAgeInDays = Defaults.ConfigurationItemDefaults.PostingAgeInDaysForAssist };

      #endregion

      #region Job Location Criteria

      var joblocation = new JobLocationCriteria();

      if (model.IsNotNull() && model.Special.IsNotNull())
      {
        var preference = model.Special.Preferences;
        if (preference.IsNotNull())
        {
          if (preference.SearchInMyState.HasValue && (bool)preference.SearchInMyState)
          {
            joblocation.OnlyShowJobInMyState = true;
            joblocation.Area = new AreaCriteria { StateId = stateId };
          }

          if (preference.ZipPreference.IsNotNull() && preference.ZipPreference.Count > 0)
          {
            var lookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses).FirstOrDefault(x => x.Id == preference.ZipPreference[0].RadiusId);
            var radiusExternalId = (lookup.IsNotNull()) ? lookup.ExternalId : "0";
            var radiusValue = radiusExternalId.ToLong();

            joblocation.Radius = new RadiusCriteria
            {
              Distance = radiusValue.GetValueOrDefault(),
              DistanceUnits = DistanceUnits.Miles,
              PostalCode = preference.ZipPreference[0].Zip
            };

            if (preference.SearchInMyState.HasValue && (bool)preference.SearchInMyState)
            {
              if (App.Settings.NationWideInstance)
                if (App.UserData.Profile.IsNotNull() && App.UserData.Profile.PostalAddress.IsNotNull())
                  stateId = (long)App.UserData.Profile.PostalAddress.StateId;

              joblocation.Area = new AreaCriteria { StateId = stateId };
            }
          }
          else if (preference.MSAPreference.IsNotNull() && preference.MSAPreference.Count > 0)
          {
            joblocation.Area = new AreaCriteria
            {
              StateId = preference.MSAPreference[0].StateId,
              MsaId = preference.MSAPreference[0].MSAId
            };

            var lookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).FirstOrDefault(x => x.Id == preference.MSAPreference[0].StateId);
            if (lookup.IsNotNull())
              joblocation.SelectedStateInCriteria = lookup.ExternalId;

            //joblocation.Area = new AreaCriteria { State = preference.MSAPreference[0].StateCode };
            //if (preference.MSAPreference[0].MSAName.IsNotNull())
            //  joblocation.Area.MSA = preference.MSAPreference[0].MSAName;
          }

          criteria.JobLocationCriteria = joblocation;
        }
      }

      #endregion

      #region Reference Document Criteria

      if (model.IsNotNull())
      {
        if (matchingType == MatchingType.Resume || matchingType == MatchingType.ResumeAndProgramOfStudy)
        {
          criteria.ReferenceDocumentCriteria = new ReferenceDocumentCriteria
          {
            DocumentId = model.ResumeMetaInfo.ResumeId.ToString(),
            DocumentType = DocumentType.Resume
          };
        }
      }

      #endregion

      #region Program of Study Criteria

      if (matchingType == MatchingType.ProgramOfStudy || matchingType == MatchingType.ResumeAndProgramOfStudy)
      {
        criteria.EducationProgramOfStudyCriteria = new EducationProgramOfStudyCriteria
        {
          ProgramAreaId = App.User.ProgramAreaId,
          DegreeEducationLevelId = App.User.DegreeEducationLevelId
        };
      }

      #endregion

      App.SetSessionValue("Career:SearchCriteria", criteria);
    }

    /// <summary>
    /// Gets program areas, with an optional 'undeclared' at the start of the list
    /// </summary>
    /// <param name="undeclaredOption">The text for the undeclared option</param>
    /// <returns>A list of program areas</returns>
    public static List<ProgramAreaDto> GetProgramAreas(string undeclaredOption = "")
    {
      var programAreas = ServiceClientLocator.ExplorerClient(App).GetProgramAreas();

      if (undeclaredOption.Length > 0)
      {
        var programAreasWithUndeclared = new List<ProgramAreaDto>
        {
          new ProgramAreaDto {Description = undeclaredOption, Id = 0, Name = undeclaredOption}
        };

        programAreasWithUndeclared.AddRange(programAreas);

        return programAreasWithUndeclared;
      }

      return programAreas;
    }

    /// <summary>
    /// Does the print job command
    /// </summary>
    /// <param name="page">The current UI page</param>
    /// <param name="jobId">The Id of the job</param>
    public static void PrintJobForPage(PageBase page, long jobId)
    {
      string fileName;

      // Get file
      var bytes = ServiceClientLocator.JobClient(App).PrintJob(jobId, out fileName);

      // send to screen
      fileName = fileName.Replace(",", " ").Replace(" ", "_");

      page.Response.Clear();
      page.Response.AddHeader("Content-Type", "application/pdf");
      page.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}; size={1}", fileName, bytes.Length));
      page.Response.OutputStream.Write(bytes, 0, bytes.Length);
      page.Response.End();
    }

    /// <summary>
    /// Prints the job preview
    /// </summary>
    /// <param name="page">The current UI page</param>
    /// <param name="jobId">The Id of the job</param>
    public static void PrintJobPreviewForPage(PageBase page, long jobId)
    {
      var baseUrl = page.Request.ApplicationPath.IsNull()
                ? string.Concat(page.Request.Url.Scheme, "://", page.Request.Url.Authority, "/")
                : string.Concat(page.Request.Url.Scheme, "://", page.Request.Url.Authority, page.Request.ApplicationPath.TrimEnd('/'), "/");

      var job = ServiceClientLocator.JobClient(App).GetJob(jobId);

			job.PostingHtml = InsertPostingFooter(job.PostingHtml, job.CriminalBackgroundExclusionRequired, job.CreditCheckRequired);
      List<string> criminalRecordExclusionWordsList = null;
      if (App.Settings.CheckCriminalRecordExclusion)
      {
        criminalRecordExclusionWordsList = ServiceClientLocator.JobClient(App).CheckCensorship(job, CensorshipType.CriminalRecordExclusion);
      }

      var postingHtml = job.PostingHtml;
      if (((job.CriminalBackgroundExclusionRequired.IsNotNull() && (bool)job.CriminalBackgroundExclusionRequired) || job.CriminalBackgroundExclusionRequired.IsNull()) && (criminalRecordExclusionWordsList.IsNotNull() && criminalRecordExclusionWordsList.Count > 0))
        postingHtml = postingHtml.Replace(Constants.PlaceHolders.PostingFooter, Localiser.Instance().Localise("PostingFooter", DefaultText.CriminalBackgroundText));
      else
        postingHtml = postingHtml.Replace(Constants.PlaceHolders.PostingFooter, string.Empty);

      var pdfConverter = new PdfConverter();

      //pdfConverter.LicenseKey = "ZU5XRV1FUEVdS1VFVlRLVFdLXFxcXA==";
      pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
      pdfConverter.LicenseKey = "1f7n9e314+H15Of75fXm5Pvk5/vs7Ozs";
      pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.NoCompression;

      pdfConverter.PdfDocumentOptions.ShowHeader = true;
      pdfConverter.PdfDocumentOptions.ShowFooter = true;
      pdfConverter.PdfDocumentOptions.LeftMargin = 10;
      pdfConverter.PdfDocumentOptions.RightMargin = 10;
      pdfConverter.PdfDocumentOptions.TopMargin = 10;
      pdfConverter.PdfDocumentOptions.BottomMargin = 10;
      pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
      pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
      pdfConverter.PdfDocumentOptions.SinglePage = false;
      pdfConverter.PageWidth = 900;

      pdfConverter.PdfHeaderOptions.HeaderText = job.JobTitle;

      pdfConverter.PdfHeaderOptions.HeaderTextAlign = HorizontalTextAlign.Left; ;
      pdfConverter.PdfHeaderOptions.HeaderTextFontSize = 8;
      pdfConverter.PdfHeaderOptions.ShowOnFirstPage = false;
      pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
      pdfConverter.PdfHeaderOptions.HeaderSubtitleText = "";

      pdfConverter.PdfFooterOptions.FooterText = "";
      pdfConverter.PdfFooterOptions.FooterTextFontSize = 8;
      pdfConverter.PdfFooterOptions.DrawFooterLine = true;
      pdfConverter.PdfFooterOptions.ShowPageNumber = true;
      pdfConverter.PdfFooterOptions.PageNumberTextFontSize = 8;

			var pdfData = pdfConverter.GetPdfBytesFromHtmlString(job.PostingHtml, baseUrl);

      if (pdfData != null)
      {
        var fileName = job.JobTitle.Replace(",", " ").Replace(" ", "_");

        page.Response.Clear();
        page.Response.AddHeader("Content-Type", "application/pdf");
        page.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.pdf; size={1}", fileName, pdfData.Length));
        page.Response.OutputStream.Write(pdfData, 0, pdfData.Length);
        page.Response.End();
      }
    }

    #region Bind common dropdowns

    /// <summary>
    /// Adds the campuses to the campus drop down.
    /// </summary>
    /// <param name="campusDropDown">The drop down to which to bind the campuses.</param>
    /// <param name="topDefaultText">The default text to show as the first entry in the list.</param>
    public static void BindCampusDropDown(DropDownList campusDropDown, string topDefaultText = null)
    {
      campusDropDown.Items.Clear();

      campusDropDown.DataSource = ServiceClientLocator.CandidateClient(App).GetCampuses().OrderBy(campus => campus.Name);
      campusDropDown.DataTextField = "Name";
      campusDropDown.DataValueField = "Id";
      campusDropDown.DataBind();

      if (topDefaultText.IsNullOrEmpty())
        campusDropDown.Items.AddLocalisedTopDefault("UpdateCampusDropDown.TopDefault", "- select campus location -");
      else
        campusDropDown.Items.Insert(0, new ListItem(topDefaultText, ""));
    }

    #endregion

    #region Approvals

    /// <summary>
    /// Approves a candidate referral.
    /// </summary>
    /// <param name="candidateApplication">The candidate application.</param>
    /// <param name="isAutoApproval">Whether this is an auto-approval.</param>
    /// <param name="templateType">The email template to use to send confirmation.</param>
    /// <param name="confirmationModal">The confirmation modal.</param>
    /// <param name="closeLink">The close link.</param>
    /// <returns></returns>
    public static string ApproveCandidateReferral(JobSeekerReferralAllStatusViewDto candidateApplication, bool isAutoApproval, EmailTemplateTypes templateType, IConfirmationModal confirmationModal = null, string closeLink = null)
    {
      var localiser = Localiser.Instance();

      var message = ServiceClientLocator.CandidateClient(App).ApproveCandidateReferral(candidateApplication.Id.GetValueOrDefault(), candidateApplication.LockVersion, isAutoApproval);

      if (message.Length == 0)
      {
        var emailTemplate = GetApproveCandidateReferralEmail(candidateApplication, templateType);
        ServiceClientLocator.CandidateClient(App).SendCandidateEmail(candidateApplication.CandidateId, emailTemplate.Subject, emailTemplate.Body, true);

        SendEmailIfJobContactFocusTalent(candidateApplication.JobId, candidateApplication.PersonId);
	      SendResumeIfJobContactByEmail(candidateApplication.JobId, candidateApplication.PersonId);

        if (confirmationModal != null)
          confirmationModal.Show(localiser.Localise("ReferralApprovedConfirmation.Title", "Referral approved"),
                                 localiser.Localise("ReferralApprovedConfirmation.Body", "The referral has been approved."),
                                 localiser.Localise("CloseModal.Text", "OK"),
                                 closeLink: closeLink);
      }
      else
      {
        if (confirmationModal != null)
          confirmationModal.Show(localiser.Localise("ReferralApprovedInvalid.Title", "Referral failed"),
                                 localiser.Localise("ReferralApprovedInvalid.Body", string.Format("There was a problem approving the referral.<br /><br />{0}", message)),
																 localiser.Localise("CloseModal.Text", "OK"),
																 closeLink: closeLink);
      }

      return message;
    }

		/// <summary>
		/// Sends the resume to the Employee if the contact method for the job is by email
		/// </summary>
		/// <param name="jobId">The id of the job</param>
		/// <param name="candidatePersonId">The id of the job seeker</param>
		public static void SendResumeIfJobContactByEmail(long jobId, long candidatePersonId, DateTime? sendByFutureDate = null)
		{
			var job = ServiceClientLocator.JobClient(App).GetJob(jobId);

			if ((job.InterviewContactPreferences & ContactMethods.Email) > 0)
			{
				var personNameEmail = ServiceClientLocator.JobClient(App).GetHiringManagerPersonNameEmailForJob(jobId);

                var resume = ServiceClientLocator.ResumeClient(App).GetResume(ServiceClientLocator.ResumeClient(App).GetDefaultResumeId(candidatePersonId));

				var talentPath = App.Settings.TalentApplicationPath.EndsWith("/")
						? App.Settings.TalentApplicationPath
						: string.Concat(App.Settings.TalentApplicationPath, "/");

				var jobLinkUrl = string.Format("{0}job/view/{1}", talentPath, job.Id);

				var templateValues = new EmailTemplateData
				{
					RecipientName = personNameEmail.Name,
					JobLinkUrls = new List<string> { jobLinkUrl },
					JobTitle = job.JobTitle,
					SenderName = App.User.FirstName
				};

				var talentPostingApplicationtViaEmailtemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(EmailTemplateTypes.TalentPostingApplicationtViaEmail, templateValues);

                if (resume.Special.PreservedFormatId == 0)
                {
                    #region send original resume if user has DocumentContentType - original resume
                    string fileName;
                    string mimeType;
                    string htmlResume;

                    var resumeBytes = ServiceClientLocator.ResumeClient(App).GetOriginalUploadedResume(resume.ResumeMetaInfo.ResumeId ?? 0, out fileName, out mimeType, out htmlResume);

                    ServiceClientLocator.CoreClient(App).SendEmail(job.InterviewEmailAddress, talentPostingApplicationtViaEmailtemplate.Subject, talentPostingApplicationtViaEmailtemplate.Body, true, resumeBytes, fileName, sendByFutureDate: sendByFutureDate);
                    #endregion
                }
                else
                {
                    var resumeHtml = ServiceClientLocator.CandidateClient(App).GetResumeAsHtml(candidatePersonId, true, false, jobId: (long)job.Id);
                    byte[] pdfData;
                    Export2PDF(resumeHtml, out pdfData, "");

                    ServiceClientLocator.CoreClient(App).SendEmail(job.InterviewEmailAddress, talentPostingApplicationtViaEmailtemplate.Subject, talentPostingApplicationtViaEmailtemplate.Body, false, pdfData, "Resume.pdf", sendByFutureDate: sendByFutureDate);
                }
			}
		}

    /// <summary>
    /// Sends an email to the Employee if the contact method for the job is by Focus Talent
    /// </summary>
    /// <param name="jobId">The id of the job</param>
    /// <param name="candidatePersonId">The id of the job seeker</param>
    public static void SendEmailIfJobContactFocusTalent(long jobId, long candidatePersonId, DateTime? sendByFutureDate = null)
    {
      var job = ServiceClientLocator.JobClient(App).GetJob(jobId);

      if (job.InterviewContactPreferences == ContactMethods.FocusTalent)
      {
        var personNameEmail = ServiceClientLocator.JobClient(App).GetHiringManagerPersonNameEmailForJob(jobId);

        var talentPath = App.Settings.TalentApplicationPath.EndsWith("/")
          ? App.Settings.TalentApplicationPath
          : string.Concat(App.Settings.TalentApplicationPath, "/");

        var jobLinkUrl = string.Format("{0}job/view/{1}", talentPath, jobId);

        var templateValues = new EmailTemplateData
        {
          RecipientName = personNameEmail.Name,
          JobLinkUrls = new List<string> {jobLinkUrl},
          JobTitle = job.JobTitle,
          JobId = jobId.ToString(CultureInfo.InvariantCulture),
          SenderName = string.Concat(App.User.FirstName, " ", App.User.LastName)
        };
        var emailTemplate =
          ServiceClientLocator.CoreClient(App)
            .GetEmailTemplatePreview(EmailTemplateTypes.NewApplicantNotification, templateValues);

        ServiceClientLocator.CoreClient(App)
            .SendEmail(personNameEmail.Email, emailTemplate.Subject, emailTemplate.Body,sendByFutureDate: sendByFutureDate);
      }
    }

		/// <summary>
		/// Export2s the PDF.
		/// </summary>
		/// <param name="resume">The resume.</param>
		/// <param name="pdfBytes">The PDF bytes.</param>
		/// <param name="nameOnResume">The name on resume.</param>
		/// <param name="urlBase">The URL base.</param>
		/// <returns></returns>
		public static int Export2PDF(String resume, out byte[] pdfBytes, string nameOnResume, string urlBase = "")
		{
			pdfBytes = null;

			try
			{
				var pdfConverter = new PdfConverter();
				//pdfConverter.LicenseKey = "ZU5XRV1FUEVdS1VFVlRLVFdLXFxcXA==";
			    pdfConverter.LicenseKey = "1f7n9e314+H15Of75fXm5Pvk5/vs7Ozs";
				pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
				pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.NoCompression;
				pdfConverter.PdfDocumentOptions.ShowHeader = true;
				pdfConverter.PdfDocumentOptions.ShowFooter = true;
				pdfConverter.PdfDocumentOptions.LeftMargin = 10;
				pdfConverter.PdfDocumentOptions.RightMargin = 10;
				pdfConverter.PdfDocumentOptions.TopMargin = 10;
				pdfConverter.PdfDocumentOptions.BottomMargin = 10;
				pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
				pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
				pdfConverter.PdfDocumentOptions.SinglePage = false;
				pdfConverter.AvoidTextBreak = true;

				//pdfConverter.PageWidth = 900;

				pdfConverter.PdfHeaderOptions.HeaderText = !String.IsNullOrEmpty(nameOnResume) ? nameOnResume : "";

				pdfConverter.PdfHeaderOptions.HeaderTextAlign = HorizontalTextAlign.Left; ;
				pdfConverter.PdfHeaderOptions.HeaderTextFontSize = 8;
				pdfConverter.PdfHeaderOptions.ShowOnFirstPage = false;
				pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
				pdfConverter.PdfHeaderOptions.HeaderSubtitleText = "";

				pdfConverter.PdfFooterOptions.FooterText = "";
				pdfConverter.PdfFooterOptions.FooterTextFontSize = 8;
				pdfConverter.PdfFooterOptions.DrawFooterLine = true;
				pdfConverter.PdfFooterOptions.ShowPageNumber = true;
				pdfConverter.PdfFooterOptions.PageNumberTextFontSize = 8;

				pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(resume, urlBase);

			}
			catch (System.Threading.ThreadAbortException)
			{
				// do nothing
			}
			catch (Exception)
			{ }

			return 0;
		}

    /// <summary>
    /// Gets the approve candidate referral email.
    /// </summary>
    /// <param name="candidateApplication">The candidate application.</param>
    /// <param name="templateType">The email template to use.</param>
    /// <returns>
    /// The email template
    /// </returns>
    private static EmailTemplateView GetApproveCandidateReferralEmail(JobSeekerReferralAllStatusViewDto candidateApplication, EmailTemplateTypes templateType)
    {
      var applicationInstructions = ServiceClientLocator.JobClient(App).GetApplicationInstructions(candidateApplication.JobId);
      var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId);
      var phoneNumber = (userDetails.IsNotNull() && userDetails.PrimaryPhoneNumber.IsNotNull() && userDetails.PrimaryPhoneNumber.Number.IsNotNullOrEmpty()) ? userDetails.PrimaryPhoneNumber.Number : "N/A";

      var templateValues = new EmailTemplateData
      {
        RecipientName = candidateApplication.Name,
        JobTitle = candidateApplication.JobTitle,
        EmployerName = candidateApplication.EmployerName,
        SenderPhoneNumber = phoneNumber,
        SenderEmailAddress = App.User.EmailAddress,
        SenderName = String.Format("{0} {1}", App.User.FirstName, App.User.LastName),
        ApplicationInstructions = applicationInstructions
      };

      return ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(templateType, templateValues);
    }

    #endregion

		/// <summary>
		/// Shows the talent pool search.
		/// </summary>
		/// <returns></returns>
		public static bool ShowTalentPoolSearch()
		{
			if (App.User.PersonId.IsNull() || App.User.EmployeeId.IsNull())
				return false;

			var showTalentPoolSearch = true;

			if (App.Settings.Theme == FocusThemes.Workforce)
			{
				var hiringManagerApprovalStatus = ServiceClientLocator.EmployeeClient(App).GetEmployeeByPerson(App.User.PersonId.Value).ApprovalStatus;
				var employeeBusinessUnitViews = ServiceClientLocator.EmployeeClient(App).GetEmployeesBusinessUnits(App.User.EmployeeId.Value);

				if (employeeBusinessUnitViews.Count <= 0)
					return false;

				var employeeBusinessUnitView = employeeBusinessUnitViews.FirstOrDefault();

				if (employeeBusinessUnitView.IsNull() || (App.Settings.NewEmployerApproval == TalentApprovalOptions.ApprovalSystemAvailable && !App.Settings.TalentPoolSearch && employeeBusinessUnitView.BusinessUnitApprovalStatus.IsIn(ApprovalStatuses.WaitingApproval, ApprovalStatuses.OnHold) && employeeBusinessUnitView.EmployerApprovalStatus.IsIn(ApprovalStatuses.WaitingApproval, ApprovalStatuses.OnHold) && hiringManagerApprovalStatus.IsIn(ApprovalStatuses.WaitingApproval, ApprovalStatuses.OnHold)) ||
				    (App.Settings.NewBusinessUnitApproval == TalentApprovalOptions.ApprovalSystemAvailable && !App.Settings.BusinessUnitTalentPoolSearch && employeeBusinessUnitView.BusinessUnitApprovalStatus.IsIn(ApprovalStatuses.WaitingApproval, ApprovalStatuses.OnHold) && employeeBusinessUnitView.EmployerApprovalStatus.IsNotIn(ApprovalStatuses.WaitingApproval, ApprovalStatuses.OnHold) && hiringManagerApprovalStatus.IsIn(ApprovalStatuses.WaitingApproval, ApprovalStatuses.OnHold)) ||
				    (App.Settings.NewHiringManagerApproval == TalentApprovalOptions.ApprovalSystemAvailable && !App.Settings.HiringManagerTalentPoolSearch && employeeBusinessUnitView.BusinessUnitApprovalStatus.IsNotIn(ApprovalStatuses.WaitingApproval, ApprovalStatuses.OnHold) && employeeBusinessUnitView.EmployerApprovalStatus.IsNotIn(ApprovalStatuses.WaitingApproval, ApprovalStatuses.OnHold) && hiringManagerApprovalStatus.IsIn(ApprovalStatuses.WaitingApproval, ApprovalStatuses.OnHold)))
					showTalentPoolSearch = false;
			}

			return showTalentPoolSearch;
		}
		
		/// <summary>
    /// Checks whether an employee can access the job upload
    /// </summary>
    /// <param name="employeeId">The id of the employee</param>
    /// <returns>A boolean indicating if the job upload can be accessed</returns>
    public static bool ShowJobUpload(long? employeeId)
    {
      if (employeeId.IsNull())
        return false;

      if (App.Settings.Theme != FocusThemes.Workforce || !App.Settings.HasJobUpload)
        return false;

      var approvalStatus = ServiceClientLocator.EmployeeClient(App).GetEmployee(employeeId.Value).ApprovalStatus;
      if (approvalStatus.IsIn(ApprovalStatuses.WaitingApproval, ApprovalStatuses.OnHold, ApprovalStatuses.Rejected))
        return false;

      return true;
    }

    /// <summary>
    /// Gets the current office id for a person
    /// </summary>
    /// <param name="personId">The id of the person whose current office is required</param>
    /// <returns>The id of the person's current office</returns>
    public static long? GetCurrentOffice(long personId)
    {
      long? currentOfficeId = null;

      if (App.Settings.OfficesEnabled)
      {
        currentOfficeId = App.GetContextValue<long?>("Utilities:CurrentOfficeId");
        if (currentOfficeId.IsNull())
        {
          var currentOffice = ServiceClientLocator.EmployerClient(App).GetCurrentOfficeForPerson(personId);
          if (currentOffice.IsNotNull())
          {
            currentOfficeId = currentOffice.OfficeId;
            App.SetContextValue("Utilities:CurrentOfficeId", currentOfficeId);
          }
        }
      }

      return currentOfficeId;
    }

		/// <summary>
		/// Inserts the posting footer.
		/// </summary>
		/// <param name="postingHtml">The posting HTML.</param>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
	  public static string InsertPostingFooter(string postingHtml, long jobId)
	  {
			var criminalBackgroundCheckRequired = ServiceClientLocator.JobClient(App).GetCriminalBackgroundExclusionRequired(jobId);
			var creditCheckRequired = ServiceClientLocator.JobClient(App).GetJobCreditCheckRequired(jobId);

			return InsertPostingFooter(postingHtml, criminalBackgroundCheckRequired, creditCheckRequired);
	  }

		/// <summary>
		/// Inserts the posting footer.
		/// </summary>
		/// <param name="postingHtml">The posting HTML.</param>
		/// <param name="criminalBackgroundCheck">The criminal background check.</param>
		/// <param name="creditCheckRequired">if set to <c>true</c> [credit check required].</param>
		/// <returns></returns>
		public static string InsertPostingFooter(string postingHtml, bool? criminalBackgroundCheck, bool? creditCheckRequired)
		{
			var criminalBackgroundCheckRequired = criminalBackgroundCheck.IsNotNull() && (bool)criminalBackgroundCheck;

			postingHtml = postingHtml.Replace("#POSTINGFOOTER#", "#POSTINGFOOTERTITLE##CREDFOOTER# <br>	<br> #CRIMFOOTER#"); // This deals with jobs with the old formatting

			var localiser = Localiser.Instance();

			var module = DocumentFocusModules.Career;
			var category = Convert.ToInt64(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DocumentCategories).Where(x => x.Key.Equals("DocumentCategories.LegalNotice")).Select(x => x.Id).FirstOrDefault());

			#region Posting footer title

			if ((App.Settings.ShowCriminalBackgroundCheck && criminalBackgroundCheckRequired) ||  (App.Settings.ShowCreditCheck && creditCheckRequired.GetValueOrDefault()))
			{
				postingHtml = postingHtml.Replace(Constants.PlaceHolders.PostingFooterTitle, localiser.Localise("PostingFooterTitle.text", App.Settings.PostingFooterTitle));
			}
			else
			{
				postingHtml = postingHtml.Replace(Constants.PlaceHolders.PostingFooterTitle, string.Empty);
			}

			#endregion

			#region Criminal background check footer

			if (App.Settings.ShowCriminalBackgroundCheck && criminalBackgroundCheckRequired)
			{
				// Add the criminal background check TEGL footer
				postingHtml = postingHtml.Replace(Constants.PlaceHolders.CrimFooter, localiser.Localise("JobSeekerCriminalBackgroundJobPostingWarning.text", App.Settings.JobSeekerCriminalBackgroundJobPostingWarning));

				#region Add the URL to the notices page that will show Criminal Background Check legal notices

				var group = Convert.ToInt64(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DocumentGroups).Where(x => x.Key.Equals("DocumentGroups.CriminalBackgroundCheck")).Select(x => x.Id).FirstOrDefault());

				postingHtml = postingHtml.Replace(Constants.PlaceHolders.CriminalBackgroundUrl, UrlBuilder.Notice(Uri.EscapeDataString(localiser.Localise("LegalNotice.Title", "Legal Notices")), category, group, module));

				#endregion
			}
			else
			{
				postingHtml = postingHtml.Replace(Constants.PlaceHolders.CrimFooter, string.Empty);
			}

			#endregion

			#region Credit check footer

			if (App.Settings.ShowCreditCheck && creditCheckRequired.GetValueOrDefault())
			{
				#region Add the URL to the notices page that will show Credit Check legal notices

				var group = Convert.ToInt64(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DocumentGroups).Where(x => x.Key.Equals("DocumentGroups.CreditCheck")).Select(x => x.Id).FirstOrDefault());

				var url = UrlBuilder.Notice(Uri.EscapeDataString(localiser.Localise("LegalNotice.Title", "Legal Notices")), category, group, module);

				#endregion

				// Add the credit check TEGL footer
				postingHtml = postingHtml.Replace(Constants.PlaceHolders.CredFooter, localiser.Localise("JobSeekerCreditCheckJobPostingWarning.text", App.Settings.JobSeekerCreditCheckJobPostingWarning, url));
			}
			else
			{
				postingHtml = postingHtml.Replace(Constants.PlaceHolders.CredFooter, string.Empty);
			}

			#endregion

			return postingHtml;
		}
  }
}