﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Diagnostics;
using System.Globalization;
using System.Web;

using Focus.Common;
using Focus.Core;
using Focus.Core.Models;
using Focus.Web.WebTalent.Controls;

#endregion

namespace Focus.Web.Code
{
	/// <summary>
	/// Central location from where Url's can be retrieved. 
	/// This avoids the situation where URLs need to be hardcoded within pages or in code-behinds.
	///</summary>
	///<remarks>
	/// Used throughout the application. Typically used with 'Response.Redirect' calls.
	/// </remarks>
	public static class UrlBuilder
	{
		public const string refusedAccessToTalentPoolSearchQueryStringKey = "refusedAccessToTalentPoolSearch";

		// Build content
		public static string Content(string virtualPath)
		{
			{ return Path(virtualPath); }
		}

		// Goto links
		public static string Goto(string gotoValue, IApp app)
		{
			switch (gotoValue)
			{
				case "activejobs":
					return TalentJobs(JobStatuses.Active);

				case "allresumes":
					return TalentPool();

				case "default":
				default:
					return Default(app.User, app, app.User.LastLoggedInOn == null);
			}
		}

		// Default page
		public static string Default(IUserContext user, IApp app,  bool first = false, bool refusedAccessToTalentPoolSearch = false)
		{
			// If the user is unknown or not authenticated go to login
			if (user == null || !user.IsAuthenticated)
			{
				return LogIn();
			}
			
			// Assist users go to Assist Dashboard, but if Focus Talent is not present in the instance, the Assist user goes to the 'assist jobseeker' tab
			if (user.IsInRole(Constants.RoleKeys.AssistUser))
			{
				if (!app.Settings.TalentModulePresent)
					return AssistJobSeekers();
				
				return first ? AssistDashboardWithAction("1") : AssistDashboard();
			}

			// Talent only users go to Talent Jobs
			if (user.IsInRole(Constants.RoleKeys.TalentUser))
			{
				if (user.IsMigrated)
				{
					var url = TalentJobs(JobStatuses.Active);
					if (refusedAccessToTalentPoolSearch)
					{
						var uri = new Uri(HttpContext.Current.Request.Url, url);
						var queryString = HttpUtility.ParseQueryString(uri.Query);
						queryString.Add(refusedAccessToTalentPoolSearchQueryStringKey, "True");
						var uriBuilder = new UriBuilder(uri);
						uriBuilder.Query = queryString.ToString();
						url = uriBuilder.Uri.PathAndQuery;
					}
					return url;
				}

				return Register();
			}

			// System Admin only users go to the Admin Dashboard
			if (user.IsInRole(Constants.RoleKeys.SystemAdmin))
				return AdminDashboard();

			return Error();
		}
		
		// Login, logout, forgotten password, change password, account settings
		public static string LogIn() { return Path("~/login"); }		
		public static string ForgottenPassword() { return Path("~/forgottenpassword"); }
    public static string ResetPassword(bool fullPath = true) { return Path(fullPath, "/resetpassword"); }
    public static string AccountSettings(bool isAssist) { return AccountSettings(isAssist, false); }
    public static string AccountSettings(bool isAssist, bool disbleAuthentication, bool disableChangePassword = false)
    {
			if (isAssist)
			{
				return disbleAuthentication || disableChangePassword || OldApp_RefactorIfFound.Settings.SSOEnabled || (OldApp_RefactorIfFound.Settings.SamlEnabledForAssist && OldApp_RefactorIfFound.Settings.Module == FocusModules.Assist) ||(OldApp_RefactorIfFound.Settings.SamlEnabledForTalent && OldApp_RefactorIfFound.Settings.Module == FocusModules.Talent)
					       ? Path("~/assist/account/messages")
					       : Path("~/assist/account/password");
			}
			else
			{
                if (OldApp_RefactorIfFound.Settings.SSOEnabled || (OldApp_RefactorIfFound.Settings.SamlEnabledForAssist && OldApp_RefactorIfFound.Settings.Module == FocusModules.Assist) || (OldApp_RefactorIfFound.Settings.SamlEnabledForTalent && OldApp_RefactorIfFound.Settings.Module == FocusModules.Talent) || disbleAuthentication || disableChangePassword)
				{
					var approved = OldApp_RefactorIfFound.User.PersonId.HasValue
						? ServiceClientLocator.EmployeeClient(new HttpApp()).GetEmployeeByPerson(OldApp_RefactorIfFound.User.PersonId.Value)
						.ApprovalStatus == ApprovalStatuses.Approved
						: false;

					return approved ? ManageCompany() : ManageSavedSearches();
				}
				else
					return Path("~/changelogin");
			}
    }

		// Logos
		public static string ViewLogo(long logoId) { return Path("~/logo/" + logoId); }
		public static string ViewLogo(string sessionKey) { return Path("~/sessionlogo/" + HttpUtility.UrlEncode(sessionKey) + "/" + DateTime.Now.Ticks); }
		public static string ViewLogo(string sessionKey, int arrayIndex) { return Path("~/sessionlogo/" + HttpUtility.UrlEncode(sessionKey) + "/" + arrayIndex + "/" + DateTime.Now.Ticks);  }
		public static string ApplicationImage(ApplicationImageTypes applicationImageType) { return Path("~/applicationimage/" + applicationImageType); }

		// Common
		public static string Notice(string title, long category, DocumentFocusModules module) { return Path("~/notice/" + title + "/" + category + "/" + module); }
		public static string Notice(string title, long category, long group, DocumentFocusModules module) { return Path("~/notice/" + title + "/" + category + "/" + group + "/" + module); }

		// Talent Workforce Theme
		public static string Register() { return Path("~/register"); }

		public static string ChangeLogin() { return Path("~/changelogin"); }
		public static string UpdateContactInformation() { return Path("~/updatecontactinformation"); }
    public static string UpdateCompanyInformation() { return Path("~/updatecompanyinformation"); }
		public static string ManageCompany() { return Path("~/managecompany"); }
		public static string ManageSavedSearches() { return Path("~/managesavedsearches"); }
		
		public static string TalentJobs() { return Path("~/jobs/"); }
    public static string TalentJobs(JobStatuses status) { return Path("~/jobs/" + status.ToString().ToLower()); }
    public static string TalentJobsForBusinessUnit(JobStatuses status, long businessUnitId) { return Path(string.Concat("~/jobs/", status.ToString().ToLower(), "/", businessUnitId.ToString(CultureInfo.InvariantCulture))); }
	  public static string TalentJobsForAllBusinessUnits(JobStatuses status) { return Path(string.Concat("~/jobs/", status.ToString().ToLower(), "/all")); }
    public static string TalentPool(PoolResult.Tabs tab = PoolResult.Tabs.AllResumes, long? jobId = null, long? searchId = null, string searchSession = "0", bool? isAdvancedSearch = false) { return Path("~/pool/" + tab.ToString().ToLower() + (jobId != null ? "/job/" + jobId : (searchId != null ? "/search/" + searchId : "")) + "/" + searchSession + (isAdvancedSearch != null && (bool)isAdvancedSearch ? "/advancedsearch" : string.Empty)); }
		public static string TalentPoolForPathCompareOnly() { return Path("~/pool"); }

		public static string TalentNewJobSearch(long jobseekerId = 0) { return Path("~/jobsearch/" + jobseekerId + "/newsearch"); }
		public static string TalentJobSearch(long? jobseekerId = null) { return Path("~/jobsearch" + (jobseekerId != null ? "/" + jobseekerId : "")); }
		public static string TalentJobSearchResultsPage(long jobSeekerId, long page) { return Path("~/searchresults/" + jobSeekerId + "/" + page); }
		public static string TalentJobSearchResultsPage() { return Path("~/searchresults"); }

		public static string JobWizard(long? jobId = null) { return Path("~/job/wizard" + (jobId != null ? "/" + jobId : "")); }
		public static string JobWizard(long jobId, int step) { return Path(string.Format("~/job/wizard/{0}/{1}", jobId, step)); }
		public static string JobUpload() { return Path("~/job/upload"); }
    public static string JobView(long? jobId = null, bool fullPath = true) { return Path(fullPath, "/job/view" + (jobId != null ? "/" + jobId : "")); }
    public static string TalentPrintJob(long? jobId = null) { return Path("~/printjob" + (jobId != null ? "/" + jobId : "")); }

		// NCRC
		public static string NcrcBronzeIconSmall() { return BrandAssetPath("Images/icon_ncrc_bronze_small.png"); }
		public static string NcrcSilverIconSmall() { return BrandAssetPath("Images/icon_ncrc_silver_small.png"); }
		public static string NcrcGoldIconSmall() { return BrandAssetPath("Images/icon_ncrc_gold_small.png"); }
		public static string NcrcPlatinumIconSmall() { return BrandAssetPath("Images/icon_ncrc_platinum_small.png"); }

	
		// Assist
    public static string AssistDashboard() { return Path("~/assist/dashboard"); }
    public static string AssistDashboardWithAction(string action) { return Path("~/assist/dashbrd/" + action); }

		public static string AssistJobSeekers() { return Path("~/assist/jobseekers"); }
    public static string AssistSearchJobPostings(long? jobseekerId = null) { return Path("~/assist/jobseeker/jobs" + (jobseekerId != null ? "/" + jobseekerId : "")); }
    public static string AssistNewSearchJobPostings(long jobseekerId = 0) { return Path("~/assist/jobseeker/jobs/" + jobseekerId + "/newsearch"); }
		public static string MessageJobSeekers() { return Path("~/assist/jobseekers/message"); }
		public static string ManageJobSeekerLists() { return Path("~/assist/jobseekers/lists"); }
    
		public static string AssistPool(TalentPoolListTypes tab = TalentPoolListTypes.AllResumes, long? jobId = null, string searchId = "") { return Path("~/assist/pool/" + tab.ToString().ToLower() + (jobId != null ? "/job/" + jobId : "/0") + (searchId != "" ? "/" + searchId : "")); }
    public static string AssistPoolReferrals(long jobId) { return Path("~/assist/pool/referrals/job/" + jobId); }
    public static string AssistPoolForPathCompareOnly() { return Path("~/assist/pool"); }

		public static string AssistEmployers() { return Path("~/assist/employers"); }
		public static string MessageEmployers() { return Path("~/assist/employers/message"); }
		public static string AssistRegisterEmployer() { return Path("~/assist/employers/register"); }

		public static string JobOrderDashboard() { return Path("~/assist/joborderdashboard"); }
    public static string JobDevelopment() { return Path("~/assist/businessdevelopment"); }
		public static string ContactEmployer(long businessUnitId, long personId, long otherPersonId) { return Path(string.Format("~/assist/contactemployer/{0}/{1}/{2}", businessUnitId, personId, otherPersonId)); }
		public static string PostingEmployerContact() { return Path("~/assist/contact/"); }
		public static string PostingEmployerContact(long postingId, long personId = 0) { return Path(string.Format("~/assist/contact/{0}/{1}", postingId, personId)); }

		public static string AssistJobPosting(string lensPostingId, string returnPage) { return Path(string.Format("~/assist/jobposting/{0}/{1}", lensPostingId, returnPage)); }
		public static string TalentJobPosting(string lensPostingId, string returnPage) { return Path(string.Format("~/jobposting/{0}/{1}", lensPostingId, returnPage)); }
		

		public static string AssistQueues()
		{
			if (OldApp_RefactorIfFound.Settings.JobSeekerReferralsEnabled && (OldApp_RefactorIfFound.User.IsInRole(Constants.RoleKeys.AssistJobSeekerReferralApprover) || OldApp_RefactorIfFound.User.IsInRole(Constants.RoleKeys.AssistJobSeekerReferralApprovalViewer)))
				return JobSeekerReferrals();

			if (OldApp_RefactorIfFound.User.IsInRole(Constants.RoleKeys.AssistEmployerAccountApprover) || OldApp_RefactorIfFound.User.IsInRole(Constants.RoleKeys.AssistEmployerAccountApprovalViewer))
				return EmployerReferrals();

			return PostingReferrals();
		}

    public static string AssistJobView(long? jobId = null) { return Path("~/assist/jobview" + (jobId != null ? "/" + jobId : "")); }
		public static string AssistJobWizard(long? jobId = null, string source = null, string saveresults = null) { return Path(string.Concat("~/assist/jobwizard", (jobId != null ? "/" + jobId : ""), (source != null ? "/" + source : ""), (saveresults != null ? "/" + saveresults : ""))); }
    public static string AssistPrintJob(long? jobId, string type = null) { return Path("~/assist/printjob/" + jobId + (type != null ? "/" + type : "")); }

		public static string TalentJobWizard(long? jobId = null, string source = null) { return Path(string.Concat("~/job/wizard", (jobId != null ? "/" + jobId : ""), (source != null ? "/" + source : ""))); }

    public static string JobSeekerReferrals() { return Path("~/assist/jobseeker/referrals"); }
		public static string JobSeekerReferral(long? candidateApplicationId) { return Path("~/assist/jobseeker/referral" + (candidateApplicationId != null ? "/" + candidateApplicationId : "")); }
		public static string EmployerReferrals() { return Path("~/assist/employer/referrals"); }
		public static string EmployerReferral(long? employeeId) { return Path("~/assist/employer/referral" + (employeeId != null ? "/" + employeeId : "")); }
    public static string HiringManagerReferral(long? employeeId) { return Path("~/assist/hiringmanager/referral" + (employeeId != null ? "/" + employeeId : "")); }
    public static string BusinessUnitReferral(long? businessunitId) { return Path("~/assist/businessunit/referral" + (businessunitId != null ? "/" + businessunitId : "")); }
		public static string PostingReferrals() { return Path("~/assist/posting/referrals"); }
		public static string PostingReferral(long? jobId) { return Path("~/assist/posting/referral" + (jobId != null ? "/" + jobId : "")); }

		public static string ManageAssist()
		{
			if(OldApp_RefactorIfFound.User.IsInRole(Constants.RoleKeys.AssistSystemAdministrator))
				return ManageSystemDefaults();

			if (OldApp_RefactorIfFound.User.IsInRole(Constants.RoleKeys.AssistAccountAdministrator) || OldApp_RefactorIfFound.User.IsInRole(Constants.RoleKeys.AssistStaffView))
				return ManageStaffAccounts();

			if (OldApp_RefactorIfFound.User.IsInRole(Constants.RoleKeys.AssistBroadcastsAdministrator))
				return ManageAnnouncements();

			return AssistFeedback();
		}

        public static string ManageActivityServiceDefaults() { return Path("~/assist/manageactivitiesservice"); }
		public static string ManageSystemDefaults() { return Path("~/assist/manage/assist"); }
		public static string ManageStaffAccounts() { return Path("~/assist/manage/staffaccounts"); }
    public static string ManageOffices() { return Path("~/assist/manage/offices"); }
    public static string CaseManagementErrors() { return Path("~/assist/casemanagement/errors"); }
		public static string ViewCaseManagementError(long id) { return Path("~/assist/casemanagement/error?errorId=" + id.ToString()); }
    public static string ManageStudentAccounts() { return Path("~/assist/manage/studentaccounts"); }
		public static string ManageAnnouncements() { return Path("~/assist/manage/announcements"); }
		public static string ManageDocuments() { return Path("~/assist/manage/documents"); }
		public static string AssistFeedback() { return Path("~/assist/feedback"); }

		public static string AssistChangePassword() { return Path("~/assist/account/password"); }
		public static string AssistContactInformation() { return Path("~/assist/account/contact"); }
		public static string AssistSavedMessages() { return Path("~/assist/account/messages"); }
    public static string AssistSavedSearches() { return Path("~/assist/account/searches"); }
    public static string AssistEmailAlerts() { return Path("~/assist/account/emailalerts"); }

    public static string AssistJobSeekerProfile(long jobSeekerId) { return Path("~/assist/jobseekerprofile/" + jobSeekerId); }
    public static string AssistJobSeekerProfileForCompareOnly() { return Path("~/assist/jobseekerprofile"); }
    public static string AssistEmployerProfile(long businessUnitId) { return Path("~/assist/employerprofile/" + businessUnitId); }
    public static string AssistEmployerProfileForCompareOnly() { return Path("~/assist/employerprofile"); }
    public static string AssistHiringManagerProfile(long employeeBusinessUnitId) { return Path("~/assist/hiringmanagerprofile/" + employeeBusinessUnitId); }
    public static string AssistHiringManagerProfileForCompareOnly() { return Path("~/assist/hiringmanagerprofile"); }
		
		//Assist Images

		public static string OpenAccordionImage() { return BrandAssetPath("Images/listAccordionOpen.gif"); }
		public static string CloseAccordionImage() { return BrandAssetPath("Images/listAccordionClose.gif"); }
		public static string ExpandedAccordionImage() { return BrandAssetPath("Images/accordion-icon-expanded.png"); }
		public static string AssistFiveStarRatingImage() {return BrandAssetPath("Images/starsfive.png");}
		public static string AssistFourStarRatingImage() { return BrandAssetPath("Images/starsfour.png"); }
		public static string AssistThreeStarRatingImage() { return BrandAssetPath("Images/starsthree.png"); }
		public static string AssistTwoStarRatingImage() { return BrandAssetPath("Images/starstwo.png"); }
		public static string AssistOneStarRatingImage() { return BrandAssetPath("Images/starsone.png"); }
		public static string AssistZeroStarRatingImage() { return BrandAssetPath("Images/starszero.png"); }

		public static string SavedReportChartImage(){return BrandAssetPath("Images/SavedReportChart.png");}
		public static string SavedReportGridImage(){return BrandAssetPath("Images/SavedReportGrid.png");}

		public static string ActivitySortAscImage(){return BrandAssetPath("Images/fa_arrow_up_off.png");}
		public static string ActivitySortDescImage() { return BrandAssetPath("Images/fa_arrow_down_off.png"); }

		public static string CategorySortAscImage() { return BrandAssetPath("Images/arrow_up_on.png"); }
		public static string CategorySortDescImage() { return BrandAssetPath("Images/arrow_down_on.png"); }
    public static string CategorySortedAscImage() { return BrandAssetPath("Images/arrow_up_off.png"); }
    public static string CategorySortedDescImage() { return BrandAssetPath("Images/arrow_down_off.png"); }

		public static string ActivityListSortAscImage(){return BrandAssetPath("Images/arrow_up_off.png");}
		public static string ActivityListSortDescImage() { return BrandAssetPath("Images/arrow_down_off.png"); }
		public static string ActivityOnSortDescImage(){return BrandAssetPath("Images/arrow_down_on.png");}
		public static string ActivityOnSortAscImage() { return BrandAssetPath("Images/arrow_up_on.png"); }

		public static string ActivityOffLeftImage(){return BrandAssetPath("Images/arrow_left_off.png");}

		public static string FlaggedCandidateImage(){return BrandAssetPath("Images/flaggedcandidate.png");}
    public static string VeteranImage() { return BrandAssetPath("Images/veteran.png"); }
    public static string H2AImage(){return BrandAssetPath("Images/icon-h2a.png");}
    public static string H2BImage(){return BrandAssetPath("Images/icon-h2b.png");}
    public static string MigrantFarmWorkerImage() { return BrandAssetPath("Images/migrantfarmworker.png"); }
    public static string BlockedImage() { return BrandAssetPath("Images/blocked.png"); }
    public static string JobSeekerBlocked() { return BrandAssetPath("Images/icon_jobseeker_blocked.png"); }
    public static string UserBlockedFailedPasswordReset() { return BrandAssetPath("Images/icon_user_blocked_failedpasswordreset.png"); }
		public static string JobSeekerInactive() { return BrandAssetPath("Images/icon_jobseeker_inactive.png"); }


		public static string EmployerNewIcon(){return BrandAssetPath("Images/icon_employer_new.png");}
		public static string EmployerRejectedIcon(){return BrandAssetPath("Images/icon_employer_rejected.png");}
		public static string EmployerApprovedIcon(){return BrandAssetPath("Images/icon_employer_approved.png");}
		public static string EmployerOnHoldIcon() { return BrandAssetPath("Images/icon_employer_onhold.png"); }
    public static string EmployerPreferredIcon() { return BrandAssetPath("Images/icon_employer_preferred.png"); }
		public static string EmployerParentIcon() { return BrandAssetPath("Images/icon_employer_parent.png"); }
		public static string EmployerParentIconLarge() { return BrandAssetPath("Images/icon_employer_parent_lg.png"); }

		public static string ButtonCloseIcon(){return BrandAssetPath("Images/button_x_close_off.png");}
		public static string CloseIcon(){return BrandAssetPath("Images/icon_close.png");}
		public static string ButtonDeleteIcon(){return BrandAssetPath("Images/button_x_delete_off.png");}

		public static string SaveIcon(){return BrandAssetPath("Images/save.png");}
		public static string AsteriskOrangeIcon(){return BrandAssetPath("Images/asteriskRequired.gif");}

		public static string EmailIcon(){return BrandAssetPath("Images/icon_email.png");}
		public static string PrintIcon(){return BrandAssetPath("Images/icon_print.png");}
		public static string TriangleDownIcon(){return BrandAssetPath("Images/icon_triangle_down.png");}
		public static string TriangleUpIcon(){return BrandAssetPath("Images/icon_triangle_up.png");}

		public static string StarIconOn(){return BrandAssetPath("Images/icon_star_on.png");}
		public static string StarIconOff(){return BrandAssetPath("Images/icon_star_off.png");}

		public static string ProgressImage(){return BrandAssetPath("Images/indicator.gif");}
    // TODO: temp image used for dev - may need replacing
    public static string EditLocalisation() { return BrandAssetPath("Images/edit.gif"); }

		public static string DefaultOfficeGeneralIconSmall() { return BrandAssetPath("Images/icon_defaultoffice_general_small.png"); }
		public static string DefaultOfficeEmployerIconSmall() { return BrandAssetPath("Images/icon_defaultoffice_employer_small.png"); }
		public static string DefaultOfficeJobOrderIconSmall() { return BrandAssetPath("Images/icon_defaultoffice_joborder_small.png"); }
		public static string DefaultOfficeJobSeekerIconSmall() { return BrandAssetPath("Images/icon_defaultoffice_jobseeker_small.png"); }
		public static string DefaultOfficeGeneralIconLarge() { return BrandAssetPath("Images/icon_defaultoffice_general_large.png"); }
		public static string DefaultOfficeEmployerIconLarge() { return BrandAssetPath("Images/icon_defaultoffice_employer_large.png"); }
		public static string DefaultOfficeJobOrderIconLarge() { return BrandAssetPath("Images/icon_defaultoffice_joborder_large.png"); }
		public static string DefaultOfficeJobSeekerIconLarge() { return BrandAssetPath("Images/icon_defaultoffice_jobseeker_large.png"); }
		
    public static string InformationPanelIcon() { return BrandAssetPath("Images/icon-information-panel.png"); }

		public static string UnderAgeJobSeekerIconSmall() { return BrandAssetPath("Images/icon_underagejobseeker_small.png"); }

		public static string ButtonDefaultIconOff() { return BrandAssetPath("Images/icon_star_off.png"); }
		public static string ButtonDefaultIconOn() { return BrandAssetPath("Images/icon_star_on.png"); }

		public static string DownloadIcon() { return BrandAssetPath("Images/icon_download.png"); }
		public static string DeleteIcon() { return BrandAssetPath("Images/icon_delete.png"); }
		public static string EditIconSmall() { return BrandAssetPath("Images/icon_edit_sm.png"); }
		public static string DeleteIconSmall() { return BrandAssetPath("Images/icon_delete_sm.png"); }


    //Job Search
    public static string AssistJobSearchCriteria(string control) { return Path("~/assist/jobsearch/" + control); }
    public static string AssistJobSearchCriteria() { return Path("~/assist/searchjobpostings"); }
    public static string AssistJobSearchResults(long jobSeekerId) { return Path("~/assist/searchresults/" + jobSeekerId); }
    public static string AssistJobSearchResults() { return Path("~/assist/searchresults"); }
    public static string AssistJobPosting() { return Path("~/assist/jobposting"); }
    public static string AssistJobMatching() { return Path("~/assist/jobmatching"); }
		public static string AssistJobSearchResultsPage(long jobSeekerId, long page) { return Path("~/assist/searchresults/" + jobSeekerId + "/" + page); }
    
		// Single Sign On
    public static string SingleSignOn(Guid singleSignOnKey, bool fullPath = true) { return Path(fullPath, "/sso/" + singleSignOnKey); }
		public static string SingleSignOnWithReactivation(Guid singleSignOnKey, bool fullPath = true) { return Path(fullPath, "/sso/" + singleSignOnKey + "/reactivate"); }
		public static string OAuth() { return Path("~/oauth"); }
		public static string OAuthVerifier(bool fullPath = true) { return Path(fullPath, "/oauth/verify"); }
		public static string OAuthLogin() { return Path("~/oauth/login"); }

		public static string Saml() { return Path("~/saml"); }
		public static string SamlResolveArtifacts(bool fullPath = true) { return Path(fullPath, "/saml/resolveartifacts"); }
		public static string SamlAssertionService(bool fullPath = true) { return Path(fullPath, "/saml/assert"); }
		public static string SamlLogout(bool fullPath = true) { return Path(fullPath, "/saml/logout"); }

		public static string SSOComplete() { return Path("~/ssocomplete"); }

    public static string TalentValidate(Guid singleSignonKey, bool fullPath = true) { return Path(fullPath, "/validate/" + singleSignonKey); }
    public static string AccessReportingOutput(Guid singleSignonKey, long reportId, bool fullPath = true) { return Path(fullPath, "/reportingoutput/" + singleSignonKey + "/" + reportId); }
    public static string ViewJobAsEmployee(Guid singleSignonKey, long jobId, bool fullPath = true) { return Path(fullPath, "/accessjob/" + singleSignonKey + "/" + jobId); }
    public static string AccountValidate(bool fullPath = true) { return Path(fullPath, "/validate/{0}"); }

		// Admin
		public static string AdminDashboard() { return Path("~/admin/dashboard"); }
		public static string AdminConfiguration() { return Path("~/admin/configuration"); }
    public static string AdminImport() { return Path("~/admin/import"); }
				
		// Errors
		public static string Error(ErrorTypes errorType = ErrorTypes.Unknown) { return Path("~/error/" + (int)errorType); }
		public static string NoJavascript() { return Path("~/nojavascript"); }

		// Help
		public static string Help(bool isAssist, HelpTypes helpType)
		{
			switch (helpType)
			{
				case HelpTypes.PrivacyAndSecurity: return (isAssist ? Path("~/assist/privacy") : Path("~/talent/privacy"));
				case HelpTypes.FrequentlyAskedQuestions: return (isAssist ? Path("~/assist/faqs") : Path("~/talent/faqs"));
				case HelpTypes.TermsOfUse: return (isAssist ? Path("~/assist/terms") : Path("~/talent/terms"));
				default: return (isAssist ? Path("~/assist/help") : Path("~/talent/help"));
			}			
		}

		public static string JavascriptHelp() { return Path("~/javascript/help"); }

		// Other content
		public static string MainLogoImage(bool isAssist) { return (isAssist ? ApplicationImage(ApplicationImageTypes.FocusAssistHeader) : ApplicationImage(ApplicationImageTypes.FocusTalentHeader)); }
    public static string MainCss(bool isAssist) { return (isAssist ? Path("~/Assets/Css/FocusAssist.css") : Path("~/Assets/Css/FocusTalent.css")); }
    public static string EditLocalisationLink(string key)
    {
      return
        string.Format(
					@"<a href=""#"" onkeypress=""javascript:OpenLocalisePopup('{0}',{1}'"" onclick=""javascript:OpenLocalisePopup('{0}',{1}'" +
          @");""><img src=""{2}"" border=""0"" alt=""Edit image"" height=""10px"" width=""10px"">", key,HttpContext.Current.Request.ApplicationPath, BrandAssetPath("Images/Edit.gif"));
    }

		// Ajax
		public static string AjaxService() { return Path("~/Services/AjaxService.svc"); }
		public static string CheckNaicsExists() { return Path("~/NAICSExists/"); }
		public static string GetAllActivities() { return AjaxService() + "/GetAllActivities"; }
		public static string SaveActivities() { return AjaxService() + "/SaveActivities"; }

    //Reporting
    public static string ReportingHome() { return Path("~/reporting/home"); }
    public static string ReportingForPathCompareOnly() { return Path("~/reporting"); }
    public static string ReportingSpecific(string report, string sessionId = null) { return Path("~/reporting/" + report + "/" + sessionId); }
    public static string ReportingSavedReports() { return Path("~/reporting/savedreports"); }
    public static string ReportingSavedReport(long reportId) { return Path("~/reporting/savedreport/" + reportId); }
    public static string ReportingOutputReport(long reportId) { return Path("~/reporting/output/" + reportId); }
    public static string ReportingOutputWindow(Guid ssoId, long reportId) { return Path("~/reporting/outputwindow/" + ssoId + "/" + reportId); }
    public static string ReportingEmployerProfile(long businessUnitId, string sessionStateId) { return Path("~/reporting/employerprofile/" + businessUnitId + "/" + sessionStateId); }
    public static string ReportingStaffProfile(Guid sessionStateId) { return Path("~/reporting/staff/{0}/" + sessionStateId); }
    public static string ReportingHiringManagerProfile(string sessionstateId) { return Path("~/reporting/hiringmanagerprofile/{0}/" + sessionstateId); }
    public static string ReportingJobSeekerProfile(long jobseekerId, string sessionStateId) { return Path("~/reporting/jobseekerprofile/" + jobseekerId + "/" + sessionStateId); }
    public static string ReportingJobView(long jobId, string sessionStateId) { return Path("~/reporting/jobview/" + jobId + "/" + sessionStateId); }

    // Career
		public static string CareerInviteToJobPosting(string lensPostingId)
		{
			var path = string.Empty;

			if (!string.IsNullOrEmpty(OldApp_RefactorIfFound.Settings.ClientApplyToPostingUrl))
			{
				path = OldApp_RefactorIfFound.Settings.ClientApplyToPostingUrl.Replace(Constants.PlaceHolders.LensJobId, lensPostingId ?? string.Empty);
			}
			else
			{
				var careerPath = OldApp_RefactorIfFound.Settings.CareerApplicationPath.EndsWith("/")
												 ? OldApp_RefactorIfFound.Settings.CareerApplicationPath
												 : string.Concat(OldApp_RefactorIfFound.Settings.CareerApplicationPath, "/");

				path = string.IsNullOrWhiteSpace(lensPostingId)
							 ? ""
							 : string.Format("{0}jobposting/{1}/invite/email", careerPath, lensPostingId);
			}

			return path;
		}

		public static string PinRegistration(bool fullPath = true) { return Path(fullPath, "/pinregistration/{0}"); }

		public static string UnsubscribeFromJsEmails(bool fullPath = true) { return Path(fullPath, "/unsubscribejobseeker"); }
		public static string UnsubscribeFromHmEmails(bool fullPath = true) { return Path(fullPath, "/unsubscribehiringmanager"); }

		/// <summary>
		/// Url builder helper.
		/// </summary>
		/// <param name="virtualPath">The virtual path.</param>
		/// <returns></returns>
		private static string Path(string virtualPath)
		{
			try
			{
				return VirtualPathUtility.ToAbsolute(virtualPath);
			}
			catch (HttpException)
			{
				return virtualPath;
			}
		}

	  /// <summary>
	  /// Url builder helper.
	  /// </summary>
	  /// <param name="fullPath">Whether to output the full path to the root</param>
	  /// <param name="virtualPath">The virtual path.</param>
	  /// <returns></returns>
	  private static string Path(bool fullPath, string virtualPath)
    {
      if (fullPath)
        virtualPath = string.Concat("~", virtualPath);

      return Path(virtualPath);
    }

		/// <summary>
		/// Builds an absolute path to a brand asset.
		/// </summary>
		/// <param name="assetPathStub">The stub for the </param>
		/// <returns></returns>
		private static string BrandAssetPath(string assetPathStub)
		{
			return VirtualPathUtility.ToAbsolute(string.Format("~/Branding/{0}/{1}", OldApp_RefactorIfFound.Settings.BrandIdentifier, assetPathStub));
		}
    
	}
}
